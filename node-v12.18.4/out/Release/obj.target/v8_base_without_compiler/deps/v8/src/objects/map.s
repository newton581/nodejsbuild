	.file	"map.cc"
	.text
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD1Ev
	.type	_ZN2v88internal8OFStreamD1Ev, @function
_ZN2v88internal8OFStreamD1Ev:
.LFB18649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE18649:
	.size	_ZN2v88internal8OFStreamD1Ev, .-_ZN2v88internal8OFStreamD1Ev
	.section	.text._ZN2v88internal8OFStreamD0Ev,"axG",@progbits,_ZN2v88internal8OFStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD0Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD0Ev:
.LFB23181:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23181:
	.size	_ZTv0_n24_N2v88internal8OFStreamD0Ev, .-_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD0Ev
	.type	_ZN2v88internal8OFStreamD0Ev, @function
_ZN2v88internal8OFStreamD0Ev:
.LFB18650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE18650:
	.size	_ZN2v88internal8OFStreamD0Ev, .-_ZN2v88internal8OFStreamD0Ev
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD1Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD1Ev:
.LFB23182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE23182:
	.size	_ZTv0_n24_N2v88internal8OFStreamD1Ev, .-_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.section	.text._ZN2v88internalL17StopSlackTrackingENS0_3MapEPv,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL17StopSlackTrackingENS0_3MapEPv, @function
_ZN2v88internalL17StopSlackTrackingENS0_3MapEPv:
.LFB18789:
	.cfi_startproc
	endbr64
	movl	15(%rdi), %eax
	andl	$536870911, %eax
	movl	%eax, 15(%rdi)
	ret
	.cfi_endproc
.LFE18789:
	.size	_ZN2v88internalL17StopSlackTrackingENS0_3MapEPv, .-_ZN2v88internalL17StopSlackTrackingENS0_3MapEPv
	.section	.text._ZNK2v88internal3Map14GetBackPointerEPNS0_7IsolateE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal3Map14GetBackPointerEPNS0_7IsolateE.constprop.0, @function
_ZNK2v88internal3Map14GetBackPointerEPNS0_7IsolateE.constprop.0:
.LFB23185:
	.cfi_startproc
	movq	(%rdi), %rax
	movq	31(%rax), %r8
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %r8b
	je	.L12
	movq	-1(%r8), %rdx
	cmpq	%rdx, 136(%rax)
	je	.L13
.L12:
	movq	88(%rax), %r8
.L13:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE23185:
	.size	_ZNK2v88internal3Map14GetBackPointerEPNS0_7IsolateE.constprop.0, .-_ZNK2v88internal3Map14GetBackPointerEPNS0_7IsolateE.constprop.0
	.section	.rodata._ZN2v88internalL18ShrinkInstanceSizeENS0_3MapEPv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"static_cast<unsigned>(value) < 256"
	.section	.rodata._ZN2v88internalL18ShrinkInstanceSizeENS0_3MapEPv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL18ShrinkInstanceSizeENS0_3MapEPv,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL18ShrinkInstanceSizeENS0_3MapEPv, @function
_ZN2v88internalL18ShrinkInstanceSizeENS0_3MapEPv:
.LFB18788:
	.cfi_startproc
	endbr64
	movzbl	7(%rdi), %eax
	subl	(%rsi), %eax
	sall	$3, %eax
	movl	%eax, %edx
	sarl	$3, %edx
	cmpl	$2040, %eax
	ja	.L19
	movb	%dl, 7(%rdi)
	movl	15(%rdi), %eax
	andl	$536870911, %eax
	movl	%eax, 15(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18788:
	.size	_ZN2v88internalL18ShrinkInstanceSizeENS0_3MapEPv, .-_ZN2v88internalL18ShrinkInstanceSizeENS0_3MapEPv
	.section	.text._ZN2v88internalL19GetMinInobjectSlackENS0_3MapEPv,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL19GetMinInobjectSlackENS0_3MapEPv, @function
_ZN2v88internalL19GetMinInobjectSlackENS0_3MapEPv:
.LFB18786:
	.cfi_startproc
	endbr64
	movzbl	9(%rdi), %eax
	cmpl	$2, %eax
	jg	.L23
.L21:
	cmpl	%eax, (%rsi)
	jle	.L20
	movl	%eax, (%rsi)
.L20:
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movzbl	7(%rdi), %edx
	subl	%eax, %edx
	movl	%edx, %eax
	jmp	.L21
	.cfi_endproc
.LFE18786:
	.size	_ZN2v88internalL19GetMinInobjectSlackENS0_3MapEPv, .-_ZN2v88internalL19GetMinInobjectSlackENS0_3MapEPv
	.section	.rodata._ZN2v88internal3Map23DeprecateTransitionTreeEPNS0_7IsolateE.part.0.str1.1,"aMS",@progbits,1
.LC2:
	.string	"unreachable code"
.LC3:
	.string	"Deprecate"
	.section	.text._ZN2v88internal3Map23DeprecateTransitionTreeEPNS0_7IsolateE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal3Map23DeprecateTransitionTreeEPNS0_7IsolateE.part.0, @function
_ZN2v88internal3Map23DeprecateTransitionTreeEPNS0_7IsolateE.part.0:
.LFB23154:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rsi, -96(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -72(%rbp)
	testb	$1, %al
	je	.L25
	cmpl	$3, %eax
	je	.L25
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L54
	cmpq	$1, %rdx
	je	.L55
.L53:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$1, -64(%rbp)
.L27:
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal19TransitionsAccessor19NumberOfTransitionsEv@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jle	.L52
	movl	-64(%rbp), %ecx
	movl	$1, %ebx
	leaq	-104(%rbp), %rdi
.L33:
	cmpl	$3, %ecx
	jne	.L56
.L36:
	movq	-72(%rbp), %rdx
	movl	%ebx, %eax
	andq	$-3, %rdx
	movq	%rdx, -104(%rbp)
	movl	15(%rdx), %edx
	andl	$16777216, %edx
	jne	.L39
.L57:
	movq	%r13, %rsi
	movl	%eax, -116(%rbp)
	movq	%rdi, %r15
	addl	$1, %ebx
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal3Map23DeprecateTransitionTreeEPNS0_7IsolateE.part.0
	movl	-116(%rbp), %eax
	cmpl	%eax, %r14d
	jle	.L40
	movl	-64(%rbp), %ecx
	movq	-128(%rbp), %rdi
	cmpl	$3, %ecx
	je	.L36
.L56:
	cmpl	$4, %ecx
	jne	.L53
	movl	%ebx, %edx
	movq	-72(%rbp), %rsi
	movl	%ebx, %eax
	sall	$4, %edx
	addl	$24, %edx
	movslq	%edx, %rdx
	movq	-1(%rdx,%rsi), %rdx
	andq	$-3, %rdx
	movq	%rdx, -104(%rbp)
	movl	15(%rdx), %edx
	andl	$16777216, %edx
	je	.L57
.L39:
	addl	$1, %ebx
	cmpl	%eax, %r14d
	jg	.L33
.L52:
	leaq	-104(%rbp), %r15
.L40:
	movq	(%r12), %rdx
	movl	15(%rdx), %eax
	orl	$16777216, %eax
	movl	%eax, 15(%rdx)
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	jne	.L58
.L42:
	movq	(%r12), %rax
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	55(%rax), %rax
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE@PLT
	movq	(%r12), %rax
	movl	15(%rax), %edx
	andl	$33554432, %edx
	je	.L59
.L24:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L60
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movl	15(%rax), %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	orl	$33554432, %edx
	movl	%edx, 15(%rax)
	movl	$1, %edx
	movq	(%r12), %rax
	movq	55(%rax), %rax
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L58:
	movq	41016(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L42
	movq	(%r12), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger8MapEventEPKcNS0_3MapES4_S3_NS0_10HeapObjectE@PLT
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$3, -64(%rbp)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L55:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L61
	movl	$4, -64(%rbp)
	jmp	.L27
.L61:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -64(%rbp)
	jmp	.L27
.L60:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23154:
	.size	_ZN2v88internal3Map23DeprecateTransitionTreeEPNS0_7IsolateE.part.0, .-_ZN2v88internal3Map23DeprecateTransitionTreeEPNS0_7IsolateE.part.0
	.section	.text._ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE,"axG",@progbits,_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE,comdat
	.p2align 4
	.weak	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	.type	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE, @function
_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE:
.LFB10539:
	.cfi_startproc
	endbr64
	testb	$1, %dl
	je	.L62
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L68
.L62:
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	jmp	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE10539:
	.size	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE, .-_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	.section	.rodata._ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE,"axG",@progbits,_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	.type	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE, @function
_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE:
.LFB12830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movswq	9(%rax), %rdx
	movl	%edx, -76(%rbp)
	movq	%rdx, %r12
	addl	$1, %edx
	movw	%dx, 9(%rax)
	movq	(%rsi), %rax
	movq	(%rax), %rdx
	movq	16(%rsi), %rax
	movl	8(%rsi), %esi
	testl	%esi, %esi
	jne	.L70
	testq	%rax, %rax
	je	.L73
	movq	(%rax), %rax
	orq	$2, %rax
	movq	%rax, %r13
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L70:
	testq	%rax, %rax
	je	.L73
	movq	(%rax), %r13
.L72:
	movswl	%r12w, %eax
	movq	(%rcx), %rsi
	movl	24(%rbx), %r14d
	leal	3(%rax,%rax,2), %r8d
	sall	$3, %r8d
	movslq	%r8d, %r15
	movq	%rdx, -1(%r15,%rsi)
	movq	(%rcx), %rdi
	testb	$1, %dl
	je	.L90
	movq	%rdx, %r9
	leaq	-1(%rdi,%r15), %r10
	andq	$-262144, %r9
	movq	8(%r9), %rsi
	movq	%r9, -72(%rbp)
	testl	$262144, %esi
	je	.L75
	movq	%r10, %rsi
	movl	%r8d, -80(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rcx
	movq	-72(%rbp), %r9
	movl	-80(%rbp), %r8d
	movq	-88(%rbp), %rdx
	movq	(%rcx), %rdi
	movq	8(%r9), %rsi
	leaq	-1(%rdi,%r15), %r10
.L75:
	andl	$24, %esi
	je	.L90
	movq	%rdi, %rsi
	andq	$-262144, %rsi
	testb	$24, 8(%rsi)
	je	.L108
	.p2align 4,,10
	.p2align 3
.L90:
	addl	%r14d, %r14d
	sarl	%r14d
	salq	$32, %r14
	movq	%r14, 7(%rdi,%r15)
	movq	(%rcx), %rdx
	movq	%r13, 15(%r15,%rdx)
	testb	$1, %r13b
	je	.L89
	cmpl	$3, %r13d
	je	.L89
	movq	%r13, %r14
	movq	(%rcx), %rdi
	addl	$16, %r8d
	movq	%r13, %rdx
	andq	$-262144, %r14
	movslq	%r8d, %r8
	andq	$-3, %rdx
	movq	8(%r14), %rax
	movq	%r8, -72(%rbp)
	leaq	-1(%rdi,%r8), %rsi
	testl	$262144, %eax
	jne	.L109
	testb	$24, %al
	je	.L89
.L114:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L89
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L89:
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movl	7(%rax), %ebx
	testb	$1, %bl
	jne	.L80
	shrl	$2, %ebx
.L81:
	movswl	%r12w, %eax
	movq	(%rcx), %rsi
	testl	%eax, %eax
	jle	.L82
	leal	(%rax,%rax,2), %edx
	subl	$1, %eax
	leaq	-64(%rbp), %r14
	subq	%rax, %r12
	sall	$3, %edx
	leaq	(%r12,%r12,2), %r12
	movslq	%edx, %r13
	salq	$3, %r12
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L111:
	shrl	$2, %eax
	leaq	24(%r13), %r15
	leaq	7(%rsi), %rdi
	cmpl	%eax, %ebx
	jnb	.L84
.L112:
	movq	7(%r13,%rsi), %rdi
	movq	7(%r15,%rsi), %rax
	sarq	$32, %rdi
	sarq	$32, %rax
	andl	$-523777, %eax
	andl	$523776, %edi
	orl	%edi, %eax
	addl	%eax, %eax
	sarl	%eax
	salq	$32, %rax
	movq	%rax, 7(%r15,%rsi)
	movq	(%rcx), %rsi
	cmpq	%r12, %r13
	je	.L110
	subq	$24, %r13
.L83:
	movq	7(%r13,%rsi), %rax
	shrq	$41, %rax
	andl	$1023, %eax
	leal	3(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rax,%rsi), %rdi
	movl	7(%rdi), %eax
	testb	$1, %al
	je	.L111
	movq	%rdi, -64(%rbp)
	movq	%r14, %rdi
	leaq	24(%r13), %r15
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-72(%rbp), %rcx
	movq	(%rcx), %rsi
	leaq	7(%rsi), %rdi
	cmpl	%eax, %ebx
	jb	.L112
.L84:
	movq	(%r15,%rdi), %rax
	movl	-76(%rbp), %r13d
	sarq	$32, %rax
	sall	$9, %r13d
	andl	$-523777, %eax
	orl	%r13d, %eax
	addl	%eax, %eax
	sarl	%eax
	salq	$32, %rax
	movq	%rax, 7(%r15,%rsi)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	leaq	-64(%rbp), %rdi
	movq	%rcx, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-72(%rbp), %rcx
	movl	%eax, %ebx
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L110:
	leaq	7(%rsi), %rdi
	movq	%r13, %r15
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L109:
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	8(%r14), %rax
	movq	-88(%rbp), %rdx
	movq	(%rcx), %rdi
	leaq	-1(%rdi,%r8), %rsi
	testb	$24, %al
	jne	.L114
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%r10, %rsi
	movl	%r8d, -88(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movl	-88(%rbp), %r8d
	movq	(%rcx), %rdi
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L73:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L82:
	leaq	7(%rsi), %rdi
	jmp	.L84
.L113:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12830:
	.size	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE, .-_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	.section	.text._ZN2v88internal19TransitionsAccessor10InitializeEv,"axG",@progbits,_ZN2v88internal19TransitionsAccessor10InitializeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19TransitionsAccessor10InitializeEv
	.type	_ZN2v88internal19TransitionsAccessor10InitializeEv, @function
_ZN2v88internal19TransitionsAccessor10InitializeEv:
.LFB18634:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	71(%rax), %rax
	movq	%rax, 24(%rdi)
	testb	$1, %al
	je	.L116
	cmpl	$3, %eax
	je	.L116
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L127
	cmpq	$1, %rdx
	je	.L128
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$1, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	movl	$3, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L129
	movl	$4, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, 32(%rdi)
	ret
	.cfi_endproc
.LFE18634:
	.size	_ZN2v88internal19TransitionsAccessor10InitializeEv, .-_ZN2v88internal19TransitionsAccessor10InitializeEv
	.section	.text._ZNK2v88internal3Map24GetPrototypeChainRootMapEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map24GetPrototypeChainRootMapEPNS0_7IsolateE
	.type	_ZNK2v88internal3Map24GetPrototypeChainRootMapEPNS0_7IsolateE, @function
_ZNK2v88internal3Map24GetPrototypeChainRootMapEPNS0_7IsolateE:
.LFB18675:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L134
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	movzbl	8(%rax), %eax
	testl	%eax, %eax
	je	.L133
	movq	12464(%rsi), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	39(%rdx), %rdx
	movq	-1(%rdx,%rax), %rax
	movq	55(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	movq	104(%rsi), %rax
	movq	-1(%rax), %rax
	ret
	.cfi_endproc
.LFE18675:
	.size	_ZNK2v88internal3Map24GetPrototypeChainRootMapEPNS0_7IsolateE, .-_ZNK2v88internal3Map24GetPrototypeChainRootMapEPNS0_7IsolateE
	.section	.text._ZN2v88internal3Map22GetConstructorFunctionENS0_6HandleIS1_EENS2_INS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map22GetConstructorFunctionENS0_6HandleIS1_EENS2_INS0_7ContextEEE
	.type	_ZN2v88internal3Map22GetConstructorFunctionENS0_6HandleIS1_EENS2_INS0_7ContextEEE, @function
_ZN2v88internal3Map22GetConstructorFunctionENS0_6HandleIS1_EENS2_INS0_7ContextEEE:
.LFB18676:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	cmpw	$67, 11(%rax)
	jbe	.L148
.L136:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	movzbl	8(%rax), %eax
	testl	%eax, %eax
	je	.L136
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	16(,%rax,8), %eax
	cltq
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rsi), %rdx
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rbx
	movq	-1(%rdx,%rax), %rsi
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L137
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L140:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L149
.L139:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L140
.L149:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L139
	.cfi_endproc
.LFE18676:
	.size	_ZN2v88internal3Map22GetConstructorFunctionENS0_6HandleIS1_EENS2_INS0_7ContextEEE, .-_ZN2v88internal3Map22GetConstructorFunctionENS0_6HandleIS1_EENS2_INS0_7ContextEEE
	.section	.text._ZNK2v88internal3Map18IsMapOfGlobalProxyENS0_6HandleINS0_13NativeContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map18IsMapOfGlobalProxyENS0_6HandleINS0_13NativeContextEEE
	.type	_ZNK2v88internal3Map18IsMapOfGlobalProxyENS0_6HandleINS0_13NativeContextEEE, @function
_ZNK2v88internal3Map18IsMapOfGlobalProxyENS0_6HandleINS0_13NativeContextEEE:
.LFB18680:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	cmpw	$1026, 11(%rax)
	je	.L151
.L153:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	jne	.L154
	.p2align 4,,10
	.p2align 3
.L151:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L159
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L153
	movq	31(%rax), %rax
	movq	39(%rax), %rax
	cmpq	%rax, (%rsi)
	sete	%al
	ret
	.cfi_endproc
.LFE18680:
	.size	_ZNK2v88internal3Map18IsMapOfGlobalProxyENS0_6HandleINS0_13NativeContextEEE, .-_ZNK2v88internal3Map18IsMapOfGlobalProxyENS0_6HandleINS0_13NativeContextEEE
	.section	.rodata._ZN2v88internal3Map20PrintReconfigurationEPNS0_7IsolateEP8_IO_FILEiNS0_12PropertyKindENS0_18PropertyAttributesE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"kData"
.LC6:
	.string	"ACCESSORS"
.LC7:
	.string	"[reconfiguring]"
.LC8:
	.string	"{symbol "
.LC9:
	.string	"}"
.LC10:
	.string	": "
.LC11:
	.string	", attrs: "
.LC12:
	.string	" ["
.LC13:
	.string	"]\n"
	.section	.text._ZN2v88internal3Map20PrintReconfigurationEPNS0_7IsolateEP8_IO_FILEiNS0_12PropertyKindENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map20PrintReconfigurationEPNS0_7IsolateEP8_IO_FILEiNS0_12PropertyKindENS0_18PropertyAttributesE
	.type	_ZN2v88internal3Map20PrintReconfigurationEPNS0_7IsolateEP8_IO_FILEiNS0_12PropertyKindENS0_18PropertyAttributesE, @function
_ZN2v88internal3Map20PrintReconfigurationEPNS0_7IsolateEP8_IO_FILEiNS0_12PropertyKindENS0_18PropertyAttributesE:
.LFB18681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-400(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$392, %rsp
	movq	%rdi, -432(%rbp)
	movq	%r12, %rdi
	movl	%r9d, -420(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movl	$15, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-432(%rbp), %r10
	leal	3(%r15,%r15,2), %eax
	sall	$3, %eax
	movq	(%r10), %rdx
	cltq
	movq	39(%rdx), %rdx
	movq	-1(%rax,%rdx), %r15
	movq	-1(%r15), %rax
	cmpw	$63, 11(%rax)
	ja	.L161
	leaq	-408(%rbp), %rdi
	movq	%r13, %rsi
	movq	%r15, -408(%rbp)
	call	_ZN2v88internal6String7PrintOnEP8_IO_FILE@PLT
.L162:
	movl	$2, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	$1, %ebx
	leaq	.LC6(%rip), %rax
	movq	%r12, %rdi
	sbbq	%rdx, %rdx
	leaq	.LC5(%rip), %rsi
	andq	$-4, %rdx
	addq	$9, %rdx
	testl	%ebx, %ebx
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rbx
	cmovne	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$9, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-420(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internallsERSoRKNS0_18PropertyAttributesE@PLT
	movl	$2, %edx
	leaq	.LC12(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal15JavaScriptFrame8PrintTopEPNS0_7IsolateEP8_IO_FILEbb@PLT
	movl	$2, %edx
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	movq	%rbx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-320(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L167
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movl	$8, %edx
	movq	%r12, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L162
.L167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18681:
	.size	_ZN2v88internal3Map20PrintReconfigurationEPNS0_7IsolateEP8_IO_FILEiNS0_12PropertyKindENS0_18PropertyAttributesE, .-_ZN2v88internal3Map20PrintReconfigurationEPNS0_7IsolateEP8_IO_FILEiNS0_12PropertyKindENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal3Map12GetStructMapEPNS0_7IsolateENS0_12InstanceTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map12GetStructMapEPNS0_7IsolateENS0_12InstanceTypeE
	.type	_ZN2v88internal3Map12GetStructMapEPNS0_7IsolateENS0_12InstanceTypeE, @function
_ZN2v88internal3Map12GetStructMapEPNS0_7IsolateENS0_12InstanceTypeE:
.LFB18682:
	.cfi_startproc
	endbr64
	subl	$77, %esi
	cmpw	$41, %si
	ja	.L169
	leaq	.L171(%rip), %rdx
	movzwl	%si, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal3Map12GetStructMapEPNS0_7IsolateENS0_12InstanceTypeE,"a",@progbits
	.align 4
	.align 4
.L171:
	.long	.L212-.L171
	.long	.L211-.L171
	.long	.L210-.L171
	.long	.L209-.L171
	.long	.L208-.L171
	.long	.L207-.L171
	.long	.L206-.L171
	.long	.L205-.L171
	.long	.L204-.L171
	.long	.L203-.L171
	.long	.L202-.L171
	.long	.L201-.L171
	.long	.L200-.L171
	.long	.L199-.L171
	.long	.L198-.L171
	.long	.L197-.L171
	.long	.L196-.L171
	.long	.L195-.L171
	.long	.L194-.L171
	.long	.L193-.L171
	.long	.L192-.L171
	.long	.L191-.L171
	.long	.L190-.L171
	.long	.L189-.L171
	.long	.L188-.L171
	.long	.L187-.L171
	.long	.L186-.L171
	.long	.L185-.L171
	.long	.L184-.L171
	.long	.L183-.L171
	.long	.L182-.L171
	.long	.L181-.L171
	.long	.L180-.L171
	.long	.L179-.L171
	.long	.L178-.L171
	.long	.L177-.L171
	.long	.L176-.L171
	.long	.L175-.L171
	.long	.L174-.L171
	.long	.L173-.L171
	.long	.L172-.L171
	.long	.L170-.L171
	.section	.text._ZN2v88internal3Map12GetStructMapEPNS0_7IsolateENS0_12InstanceTypeE
	.p2align 4,,10
	.p2align 3
.L172:
	movq	4280(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	movq	4272(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	movq	4264(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	movq	4256(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	movq	4248(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	movq	4240(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	movq	4232(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	movq	4224(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	movq	4216(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	movq	4208(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	movq	4200(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	movq	4192(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	movq	4184(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	movq	4176(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	movq	4168(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	movq	4160(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	movq	4152(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	movq	4144(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	movq	4136(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	movq	4128(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	movq	4120(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	movq	4112(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	movq	4104(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	movq	4096(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	movq	4088(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	movq	4080(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	movq	4072(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	movq	4064(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	movq	4056(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	movq	4048(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	movq	4040(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	movq	4032(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	movq	4024(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	movq	4016(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	movq	4008(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	movq	4000(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	movq	3992(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	movq	3984(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	movq	3976(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	movq	3968(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	movq	3960(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	movq	4288(%rdi), %rax
	ret
.L169:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18682:
	.size	_ZN2v88internal3Map12GetStructMapEPNS0_7IsolateENS0_12InstanceTypeE, .-_ZN2v88internal3Map12GetStructMapEPNS0_7IsolateENS0_12InstanceTypeE
	.section	.text._ZN2v88internal3Map12GetVisitorIdES1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map12GetVisitorIdES1_
	.type	_ZN2v88internal3Map12GetVisitorIdES1_, @function
_ZN2v88internal3Map12GetVisitorIdES1_:
.LFB18683:
	.cfi_startproc
	endbr64
	movzwl	11(%rdi), %ecx
	movl	%ecx, %eax
	cmpl	$63, %ecx
	jg	.L217
	movl	%ecx, %edx
	andl	$7, %edx
	cmpw	$5, %dx
	ja	.L218
	leaq	.L220(%rip), %rsi
	movzwl	%dx, %edx
	movslq	(%rsi,%rdx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal3Map12GetVisitorIdES1_,"a",@progbits
	.align 4
	.align 4
.L220:
	.long	.L224-.L220
	.long	.L223-.L220
	.long	.L267-.L220
	.long	.L221-.L220
	.long	.L218-.L220
	.long	.L276-.L220
	.section	.text._ZN2v88internal3Map12GetVisitorIdES1_
	.p2align 4,,10
	.p2align 3
.L217:
	cmpw	$168, %cx
	ja	.L288
	subl	$65, %eax
	cmpw	$103, %ax
	ja	.L237
	leaq	.L239(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal3Map12GetVisitorIdES1_
	.align 4
	.align 4
.L239:
	.long	.L267-.L239
	.long	.L274-.L239
	.long	.L273-.L239
	.long	.L272-.L239
	.long	.L271-.L239
	.long	.L280-.L239
	.long	.L267-.L239
	.long	.L270-.L239
	.long	.L269-.L239
	.long	.L268-.L239
	.long	.L267-.L239
	.long	.L267-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L266-.L239
	.long	.L265-.L239
	.long	.L264-.L239
	.long	.L263-.L239
	.long	.L262-.L239
	.long	.L260-.L239
	.long	.L260-.L239
	.long	.L260-.L239
	.long	.L260-.L239
	.long	.L260-.L239
	.long	.L260-.L239
	.long	.L260-.L239
	.long	.L260-.L239
	.long	.L260-.L239
	.long	.L260-.L239
	.long	.L260-.L239
	.long	.L260-.L239
	.long	.L261-.L239
	.long	.L260-.L239
	.long	.L260-.L239
	.long	.L258-.L239
	.long	.L258-.L239
	.long	.L258-.L239
	.long	.L258-.L239
	.long	.L258-.L239
	.long	.L258-.L239
	.long	.L258-.L239
	.long	.L259-.L239
	.long	.L258-.L239
	.long	.L258-.L239
	.long	.L240-.L239
	.long	.L257-.L239
	.long	.L236-.L239
	.long	.L255-.L239
	.long	.L254-.L239
	.long	.L253-.L239
	.long	.L252-.L239
	.long	.L251-.L239
	.long	.L243-.L239
	.long	.L250-.L239
	.long	.L249-.L239
	.long	.L248-.L239
	.long	.L247-.L239
	.long	.L246-.L239
	.long	.L245-.L239
	.long	.L244-.L239
	.long	.L243-.L239
	.long	.L242-.L239
	.long	.L241-.L239
	.long	.L240-.L239
	.long	.L238-.L239
	.section	.text._ZN2v88internal3Map12GetVisitorIdES1_
.L236:
	movl	$45, %eax
	ret
.L270:
	movl	$8, %eax
	ret
.L280:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	subw	$1024, %ax
	cmpw	$81, %ax
	ja	.L218
	leaq	.L228(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal3Map12GetVisitorIdES1_
	.align 4
	.align 4
.L228:
	.long	.L236-.L228
	.long	.L235-.L228
	.long	.L235-.L228
	.long	.L229-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L235-.L228
	.long	.L229-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L218-.L228
	.long	.L235-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L279-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L234-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L233-.L228
	.long	.L233-.L228
	.long	.L232-.L228
	.long	.L231-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L230-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L229-.L228
	.long	.L227-.L228
	.section	.text._ZN2v88internal3Map12GetVisitorIdES1_
.L233:
	movl	$30, %eax
	ret
.L279:
	movl	$23, %eax
	ret
.L267:
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	testb	$8, %al
	sete	%al
	movzbl	%al, %eax
	addl	$4, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	movl	$40, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	andl	$-25, %ecx
	movl	$12, %edx
	movl	$39, %eax
	cmpl	$33, %ecx
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	movl	$48, %eax
	ret
.L274:
	xorl	%eax, %eax
	ret
.L241:
	movl	$51, %eax
	ret
.L238:
	movl	$56, %eax
	ret
.L227:
	movl	$25, %eax
	ret
.L230:
	movl	$54, %eax
	ret
.L231:
	movl	$24, %eax
	ret
.L232:
	movl	$28, %eax
	ret
.L234:
	movl	$29, %eax
	ret
.L229:
	movq	47(%rdi), %rax
	testq	%rax, %rax
	sete	%al
	movzbl	%al, %eax
	addl	$26, %eax
	ret
.L235:
	movl	$22, %eax
	ret
.L242:
	movl	$50, %eax
	ret
.L244:
	movl	$43, %eax
	ret
.L245:
	movl	$42, %eax
	ret
.L246:
	movl	$41, %eax
	ret
.L247:
	movl	$38, %eax
	ret
.L248:
	movl	$36, %eax
	ret
.L249:
	movl	$35, %eax
	ret
.L250:
	movl	$34, %eax
	ret
.L243:
	movl	$14, %eax
	ret
.L251:
	movl	$19, %eax
	ret
.L252:
	movl	$18, %eax
	ret
.L253:
	movl	$15, %eax
	ret
.L254:
	movl	$11, %eax
	ret
.L255:
	movl	$9, %eax
	ret
.L257:
	movl	$49, %eax
	ret
.L240:
	movl	$55, %eax
	ret
.L259:
	movl	$32, %eax
	ret
.L258:
	movl	$13, %eax
	ret
.L261:
	movl	$17, %eax
	ret
.L260:
	movl	$20, %eax
	ret
.L262:
	movl	$16, %eax
	ret
.L263:
	movl	$7, %eax
	ret
.L264:
	movl	$47, %eax
	ret
.L265:
	movl	$44, %eax
	ret
.L266:
	cmpl	$95, %ecx
	je	.L282
	cmpl	$104, %ecx
	je	.L283
	xorl	%eax, %eax
	cmpl	$108, %ecx
	sete	%al
	leal	45(,%rax,8), %eax
	ret
.L268:
	movl	$3, %eax
	ret
.L269:
	movl	$21, %eax
	ret
.L271:
	movl	$10, %eax
	ret
.L272:
	movl	$31, %eax
	ret
.L273:
	movl	$33, %eax
	ret
.L218:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
.L237:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$46, %eax
	ret
.L282:
	movl	$37, %eax
	ret
.L283:
	movl	$52, %eax
	ret
	.cfi_endproc
.LFE18683:
	.size	_ZN2v88internal3Map12GetVisitorIdES1_, .-_ZN2v88internal3Map12GetVisitorIdES1_
	.section	.rodata._ZN2v88internal3Map19PrintGeneralizationEPNS0_7IsolateEP8_IO_FILEPKciiibNS0_14RepresentationES8_NS0_17PropertyConstnessES9_NS0_11MaybeHandleINS0_9FieldTypeEEENSA_INS0_6ObjectEEESC_SE_.str1.1,"aMS",@progbits,1
.LC14:
	.string	"v"
.LC15:
	.string	"d"
.LC16:
	.string	"h"
.LC17:
	.string	"t"
.LC18:
	.string	"s"
.LC19:
	.string	"[generalizing]"
.LC20:
	.string	":"
.LC21:
	.string	"c"
.LC22:
	.string	"{"
.LC23:
	.string	";"
.LC24:
	.string	"+"
.LC25:
	.string	" maps"
.LC26:
	.string	") ["
.LC27:
	.string	"} ("
.LC28:
	.string	"->"
	.section	.text._ZN2v88internal3Map19PrintGeneralizationEPNS0_7IsolateEP8_IO_FILEPKciiibNS0_14RepresentationES8_NS0_17PropertyConstnessES9_NS0_11MaybeHandleINS0_9FieldTypeEEENSA_INS0_6ObjectEEESC_SE_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map19PrintGeneralizationEPNS0_7IsolateEP8_IO_FILEPKciiibNS0_14RepresentationES8_NS0_17PropertyConstnessES9_NS0_11MaybeHandleINS0_9FieldTypeEEENSA_INS0_6ObjectEEESC_SE_
	.type	_ZN2v88internal3Map19PrintGeneralizationEPNS0_7IsolateEP8_IO_FILEPKciiibNS0_14RepresentationES8_NS0_17PropertyConstnessES9_NS0_11MaybeHandleINS0_9FieldTypeEEENSA_INS0_6ObjectEEESC_SE_, @function
_ZN2v88internal3Map19PrintGeneralizationEPNS0_7IsolateEP8_IO_FILEPKciiibNS0_14RepresentationES8_NS0_17PropertyConstnessES9_NS0_11MaybeHandleINS0_9FieldTypeEEENSA_INS0_6ObjectEEESC_SE_:
.LFB18684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movq	%rdx, %rsi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-400(%rbp), %r12
	pushq	%rbx
	subq	$408, %rsp
	.cfi_offset 3, -56
	movl	%r8d, -424(%rbp)
	movl	24(%rbp), %ebx
	movq	%rdi, -432(%rbp)
	movq	%r12, %rdi
	movl	%r9d, -436(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movl	$14, %edx
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-432(%rbp), %r10
	movl	-424(%rbp), %r8d
	movq	(%r10), %rdx
	leal	3(%r8,%r8,2), %eax
	sall	$3, %eax
	movq	39(%rdx), %rdx
	cltq
	movq	-1(%rax,%rdx), %r8
	movq	-1(%r8), %rax
	cmpw	$63, 11(%rax)
	ja	.L290
	leaq	-408(%rbp), %rdi
	movq	%r13, %rsi
	movq	%r8, -408(%rbp)
	call	_ZN2v88internal6String7PrintOnEP8_IO_FILE@PLT
.L291:
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testb	%bl, %bl
	je	.L292
	movl	$1, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L293:
	movl	$2, %edx
	leaq	.LC28(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$4, 40(%rbp)
	ja	.L294
	movzbl	40(%rbp), %eax
	leaq	.L314(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal3Map19PrintGeneralizationEPNS0_7IsolateEP8_IO_FILEPKciiibNS0_14RepresentationES8_NS0_17PropertyConstnessES9_NS0_11MaybeHandleINS0_9FieldTypeEEENSA_INS0_6ObjectEEESC_SE_,"a",@progbits
	.align 4
	.align 4
.L314:
	.long	.L306-.L314
	.long	.L315-.L314
	.long	.L318-.L314
	.long	.L304-.L314
	.long	.L313-.L314
	.section	.text._ZN2v88internal3Map19PrintGeneralizationEPNS0_7IsolateEP8_IO_FILEPKciiibNS0_14RepresentationES8_NS0_17PropertyConstnessES9_NS0_11MaybeHandleINS0_9FieldTypeEEENSA_INS0_6ObjectEEESC_SE_
	.p2align 4,,10
	.p2align 3
.L290:
	movl	$8, %edx
	movq	%r12, %rdi
	leaq	.LC8(%rip), %rsi
	movq	%r8, -424(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-424(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L292:
	cmpb	$4, 32(%rbp)
	ja	.L294
	movzbl	32(%rbp), %eax
	leaq	.L296(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal3Map19PrintGeneralizationEPNS0_7IsolateEP8_IO_FILEPKciiibNS0_14RepresentationES8_NS0_17PropertyConstnessES9_NS0_11MaybeHandleINS0_9FieldTypeEEENSA_INS0_6ObjectEEESC_SE_
	.align 4
	.align 4
.L296:
	.long	.L300-.L296
	.long	.L299-.L296
	.long	.L317-.L296
	.long	.L297-.L296
	.long	.L295-.L296
	.section	.text._ZN2v88internal3Map19PrintGeneralizationEPNS0_7IsolateEP8_IO_FILEPKciiibNS0_14RepresentationES8_NS0_17PropertyConstnessES9_NS0_11MaybeHandleINS0_9FieldTypeEEENSA_INS0_6ObjectEEESC_SE_
	.p2align 4,,10
	.p2align 3
.L313:
	leaq	.LC17(%rip), %rsi
.L305:
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	$0, 80(%rbp)
	je	.L325
	movq	80(%rbp), %rax
	leaq	-408(%rbp), %rdi
	movq	%r12, %rsi
	movq	(%rax), %rax
	movq	%rax, -408(%rbp)
	call	_ZNK2v88internal9FieldType7PrintToERSo@PLT
.L309:
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	56(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internallsERSoNS0_17PropertyConstnessE@PLT
	movl	$3, %edx
	leaq	.LC27(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, (%r14)
	jne	.L326
	movl	$1, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	16(%rbp), %esi
	movq	%r12, %rdi
	subl	-436(%rbp), %esi
	call	_ZNSolsEi@PLT
	movl	$5, %edx
	leaq	.LC25(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L310:
	movl	$3, %edx
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rbx
	call	_ZN2v88internal15JavaScriptFrame8PrintTopEPNS0_7IsolateEP8_IO_FILEbb@PLT
	movl	$2, %edx
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	movq	%rbx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-320(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L327
	addq	$408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	cmpq	$0, 88(%rbp)
	je	.L308
	movq	88(%rbp), %rax
	leaq	-408(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -408(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L326:
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L318:
	leaq	.LC15(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L315:
	leaq	.LC18(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L304:
	leaq	.LC16(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L297:
	leaq	.LC16(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L298:
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	$0, 64(%rbp)
	je	.L328
	movq	64(%rbp), %rax
	leaq	-408(%rbp), %rdi
	movq	%r12, %rsi
	movq	(%rax), %rax
	movq	%rax, -408(%rbp)
	call	_ZNK2v88internal9FieldType7PrintToERSo@PLT
.L303:
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC23(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	48(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internallsERSoNS0_17PropertyConstnessE@PLT
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L295:
	leaq	.LC17(%rip), %rsi
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L299:
	leaq	.LC18(%rip), %rsi
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L317:
	leaq	.LC15(%rip), %rsi
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L300:
	leaq	.LC14(%rip), %rsi
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L328:
	cmpq	$0, 72(%rbp)
	je	.L308
	movq	72(%rbp), %rax
	leaq	-408(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -408(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L308:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L306:
	leaq	.LC14(%rip), %rsi
	jmp	.L305
.L294:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L327:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18684:
	.size	_ZN2v88internal3Map19PrintGeneralizationEPNS0_7IsolateEP8_IO_FILEPKciiibNS0_14RepresentationES8_NS0_17PropertyConstnessES9_NS0_11MaybeHandleINS0_9FieldTypeEEENSA_INS0_6ObjectEEESC_SE_, .-_ZN2v88internal3Map19PrintGeneralizationEPNS0_7IsolateEP8_IO_FILEPKciiibNS0_14RepresentationES8_NS0_17PropertyConstnessES9_NS0_11MaybeHandleINS0_9FieldTypeEEENSA_INS0_6ObjectEEESC_SE_
	.section	.text._ZN2v88internal3Map13WrapFieldTypeEPNS0_7IsolateENS0_6HandleINS0_9FieldTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map13WrapFieldTypeEPNS0_7IsolateENS0_6HandleINS0_9FieldTypeEEE
	.type	_ZN2v88internal3Map13WrapFieldTypeEPNS0_7IsolateENS0_6HandleINS0_9FieldTypeEEE, @function
_ZN2v88internal3Map13WrapFieldTypeEPNS0_7IsolateENS0_6HandleINS0_9FieldTypeEEE:
.LFB18685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r13, %rdi
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9FieldType7IsClassEv@PLT
	movq	%rbx, %rdx
	movl	%eax, %r8d
	movl	$1, %eax
	testb	%r8b, %r8b
	jne	.L338
.L334:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L339
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9FieldType7AsClassEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L331
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
	xorl	%eax, %eax
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L331:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L340
.L333:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	xorl	%eax, %eax
	movq	%rsi, (%rdx)
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L340:
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L333
.L339:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18685:
	.size	_ZN2v88internal3Map13WrapFieldTypeEPNS0_7IsolateENS0_6HandleINS0_9FieldTypeEEE, .-_ZN2v88internal3Map13WrapFieldTypeEPNS0_7IsolateENS0_6HandleINS0_9FieldTypeEEE
	.section	.text._ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE
	.type	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE, @function
_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE:
.LFB18686:
	.cfi_startproc
	endbr64
	cmpl	$3, %edi
	je	.L345
	movq	%rdi, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	jne	.L344
	andq	$-3, %rdi
.L344:
	jmp	_ZN2v88internal9FieldType4castENS0_6ObjectE@PLT
	.p2align 4,,10
	.p2align 3
.L345:
	jmp	_ZN2v88internal9FieldType4NoneEv@PLT
	.cfi_endproc
.LFE18686:
	.size	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE, .-_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE
	.section	.rodata._ZNK2v88internal3Map28TransitionRemovesTaggedFieldES1_.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"(!IsSmi() && (*layout_word_index < length())) || (IsSmi() && (*layout_word_index < 1))"
	.section	.text._ZNK2v88internal3Map28TransitionRemovesTaggedFieldES1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map28TransitionRemovesTaggedFieldES1_
	.type	_ZNK2v88internal3Map28TransitionRemovesTaggedFieldES1_, @function
_ZNK2v88internal3Map28TransitionRemovesTaggedFieldES1_:
.LFB18692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r11
	movq	39(%r11), %rbx
	movq	%r11, %rax
	leaq	31(%rbx), %rdx
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L378:
	movq	(%rdx), %rax
	addl	$1, %ecx
	sarq	$32, %rax
	andl	$2, %eax
	cmpl	$1, %eax
	movq	(%rdi), %rax
	adcl	$0, %r8d
	addq	$24, %rdx
.L347:
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%ecx, %eax
	jg	.L378
	movq	39(%rsi), %rcx
	leaq	15(%rsi), %r9
	xorl	%edx, %edx
	xorl	%esi, %esi
	addq	$31, %rcx
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L379:
	movq	(%rcx), %rax
	addl	$1, %esi
	sarq	$32, %rax
	andl	$2, %eax
	cmpl	$1, %eax
	adcl	$0, %edx
	addq	$24, %rcx
.L350:
	movl	(%r9), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%esi, %eax
	jg	.L379
	movl	$2147483648, %esi
	movl	$1, %r9d
	cmpl	%r8d, %edx
	jge	.L369
	.p2align 4,,10
	.p2align 3
.L353:
	movzbl	7(%r11), %r10d
	movzbl	8(%r11), %eax
	subl	%eax, %r10d
	cmpl	%edx, %r10d
	setg	%al
	jg	.L380
	movl	%edx, %ecx
	movq	%rsi, %r12
	subl	%r10d, %ecx
	leal	16(,%rcx,8), %ebx
.L356:
	movzbl	%al, %ecx
	movslq	%r10d, %r10
	movslq	%ebx, %rbx
	salq	$14, %rcx
	salq	$17, %r10
	orq	%r10, %rcx
	orq	%r12, %rcx
	orq	%rbx, %rcx
	testb	$64, %ch
	jne	.L381
.L358:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L381:
	.cfi_restore_state
	movq	47(%r11), %rax
	testq	%rax, %rax
	je	.L358
	movq	%rax, %r11
	movl	$32, %r12d
	notq	%r11
	movl	%r11d, %ebx
	andl	$1, %ebx
	jne	.L360
	movslq	11(%rax), %r12
	sall	$3, %r12d
.L360:
	movq	%rcx, %r10
	sarl	$3, %ecx
	shrq	$30, %r10
	andl	$2047, %ecx
	andl	$15, %r10d
	subl	%r10d, %ecx
	cmpl	%ecx, %r12d
	jbe	.L358
	testl	%ecx, %ecx
	leal	31(%rcx), %r10d
	cmovns	%ecx, %r10d
	sarl	$5, %r10d
	andl	$1, %r11d
	jne	.L361
	cmpl	%r10d, 11(%rax)
	jle	.L361
	movl	%ecx, %r11d
	sarl	$31, %r11d
	shrl	$27, %r11d
	addl	%r11d, %ecx
	andl	$31, %ecx
	subl	%r11d, %ecx
	movl	%r9d, %r11d
	sall	%cl, %r11d
	movl	%r11d, %ecx
	testb	%bl, %bl
	je	.L382
.L365:
	sarq	$32, %rax
	testl	%eax, %ecx
	sete	%al
.L367:
	testb	%al, %al
	jne	.L358
	addl	$1, %edx
	cmpl	%r8d, %edx
	je	.L369
	movq	(%rdi), %r11
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L380:
	movzbl	8(%r11), %ecx
	movzbl	8(%r11), %ebx
	salq	$3, %rcx
	addl	%edx, %ebx
	andl	$2040, %ecx
	sall	$3, %ebx
	salq	$27, %rcx
	movq	%rcx, %r12
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L382:
	sall	$2, %r10d
	movslq	%r10d, %r10
	movl	15(%rax,%r10), %eax
	testl	%eax, %r11d
	sete	%al
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L369:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	cmpl	$31, %ecx
	jg	.L363
	testb	%bl, %bl
	je	.L363
	movl	%r9d, %ebx
	sall	%cl, %ebx
	movl	%ebx, %ecx
	jmp	.L365
.L363:
	leaq	.LC29(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18692:
	.size	_ZNK2v88internal3Map28TransitionRemovesTaggedFieldES1_, .-_ZNK2v88internal3Map28TransitionRemovesTaggedFieldES1_
	.section	.text._ZNK2v88internal3Map43TransitionChangesTaggedFieldToUntaggedFieldES1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map43TransitionChangesTaggedFieldToUntaggedFieldES1_
	.type	_ZNK2v88internal3Map43TransitionChangesTaggedFieldToUntaggedFieldES1_, @function
_ZNK2v88internal3Map43TransitionChangesTaggedFieldToUntaggedFieldES1_:
.LFB18693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	39(%rax), %rbx
	leaq	31(%rbx), %rcx
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L431:
	movq	(%rcx), %rax
	addl	$1, %r8d
	sarq	$32, %rax
	andl	$2, %eax
	cmpl	$1, %eax
	movq	(%rdi), %rax
	adcl	$0, %edx
	addq	$24, %rcx
.L384:
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%r8d, %eax
	jg	.L431
	movq	39(%rsi), %rax
	leaq	15(%rsi), %r10
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	31(%rax), %rcx
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L432:
	movq	(%rcx), %rax
	addl	$1, %r9d
	sarq	$32, %rax
	andl	$2, %eax
	cmpl	$1, %eax
	adcl	$0, %r8d
	addq	$24, %rcx
.L387:
	movl	(%r10), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%r9d, %eax
	jg	.L432
	cmpl	%edx, %r8d
	leaq	7(%rsi), %r10
	leaq	8(%rsi), %r9
	movl	$1, %ebx
	cmovg	%edx, %r8d
	movl	$2147483648, %r11d
	xorl	%edx, %edx
	testl	%r8d, %r8d
	jne	.L415
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L392:
	movl	%edx, %eax
	movq	%r11, %r13
	subl	%r14d, %eax
	leal	16(,%rax,8), %r12d
.L393:
	movzbl	%cl, %eax
	movslq	%r14d, %r14
	movslq	%r12d, %r12
	salq	$14, %rax
	salq	$17, %r14
	orq	%r14, %rax
	orq	%r13, %rax
	orq	%r12, %rax
	testb	$64, %ah
	je	.L395
	movq	(%rdi), %rcx
	movq	47(%rcx), %r12
	movq	%rax, %rcx
	sarl	$3, %eax
	shrq	$30, %rcx
	andl	$2047, %eax
	andl	$15, %ecx
	subl	%ecx, %eax
	testq	%r12, %r12
	je	.L405
	movq	%r12, %rcx
	movl	$32, %r13d
	notq	%rcx
	movl	%ecx, %r14d
	andl	$1, %r14d
	jne	.L397
	movslq	11(%r12), %r13
	sall	$3, %r13d
.L397:
	cmpl	%r13d, %eax
	jnb	.L405
	testl	%eax, %eax
	leal	31(%rax), %r13d
	cmovns	%eax, %r13d
	sarl	$5, %r13d
	andl	$1, %ecx
	jne	.L398
	cmpl	%r13d, 11(%r12)
	jle	.L398
	movl	%eax, %r15d
	sarl	$31, %r15d
	shrl	$27, %r15d
	leal	(%rax,%r15), %ecx
	andl	$31, %ecx
	subl	%r15d, %ecx
	movl	%ebx, %r15d
	sall	%cl, %r15d
	movl	%r15d, %ecx
	testb	%r14b, %r14b
	je	.L433
.L402:
	sarq	$32, %r12
	testl	%r12d, %ecx
	sete	%cl
.L404:
	testb	%cl, %cl
	jne	.L405
.L395:
	addl	$1, %edx
	cmpl	%edx, %r8d
	je	.L414
.L415:
	movzbl	(%r10), %r14d
	movzbl	(%r9), %ecx
	subl	%ecx, %r14d
	cmpl	%r14d, %edx
	setl	%cl
	jge	.L392
	movzbl	(%r9), %eax
	movzbl	(%r9), %r12d
	salq	$3, %rax
	addl	%edx, %r12d
	andl	$2040, %eax
	sall	$3, %r12d
	salq	$27, %rax
	movq	%rax, %r13
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L405:
	movq	47(%rsi), %r12
	testq	%r12, %r12
	je	.L395
	movq	%r12, %rcx
	movl	$32, %r13d
	notq	%rcx
	movl	%ecx, %r14d
	andl	$1, %r14d
	jne	.L407
	movslq	11(%r12), %r13
	sall	$3, %r13d
.L407:
	cmpl	%r13d, %eax
	jnb	.L395
	testl	%eax, %eax
	leal	31(%rax), %r13d
	cmovns	%eax, %r13d
	sarl	$5, %r13d
	andl	$1, %ecx
	jne	.L409
	cmpl	%r13d, 11(%r12)
	jle	.L409
	movl	%eax, %r15d
	sarl	$31, %r15d
	shrl	$27, %r15d
	leal	(%rax,%r15), %ecx
	movl	%ebx, %eax
	andl	$31, %ecx
	subl	%r15d, %ecx
	sall	%cl, %eax
	testb	%r14b, %r14b
	je	.L434
.L412:
	sarq	$32, %r12
	testl	%r12d, %eax
	sete	%al
.L413:
	testb	%al, %al
	jne	.L395
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	sall	$2, %r13d
	movslq	%r13d, %r13
	movl	15(%r12,%r13), %r12d
	testl	%r12d, %r15d
	sete	%cl
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L414:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	sall	$2, %r13d
	movslq	%r13d, %r13
	movl	15(%r12,%r13), %ecx
	testl	%ecx, %eax
	sete	%al
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L398:
	cmpl	$31, %eax
	jg	.L400
	testb	%r14b, %r14b
	je	.L400
	movl	%eax, %ecx
	movl	%ebx, %r15d
	sall	%cl, %r15d
	movl	%r15d, %ecx
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L409:
	testb	%r14b, %r14b
	je	.L400
	cmpl	$31, %eax
	jg	.L400
	movl	%eax, %ecx
	movl	%ebx, %r15d
	sall	%cl, %r15d
	movl	%r15d, %eax
	jmp	.L412
.L400:
	leaq	.LC29(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18693:
	.size	_ZNK2v88internal3Map43TransitionChangesTaggedFieldToUntaggedFieldES1_, .-_ZNK2v88internal3Map43TransitionChangesTaggedFieldToUntaggedFieldES1_
	.section	.text._ZNK2v88internal3Map39TransitionRequiresSynchronizationWithGCES1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map39TransitionRequiresSynchronizationWithGCES1_
	.type	_ZNK2v88internal3Map39TransitionRequiresSynchronizationWithGCES1_, @function
_ZNK2v88internal3Map39TransitionRequiresSynchronizationWithGCES1_:
.LFB18694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZNK2v88internal3Map28TransitionRemovesTaggedFieldES1_
	testb	%al, %al
	je	.L438
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v88internal3Map43TransitionChangesTaggedFieldToUntaggedFieldES1_
	.cfi_endproc
.LFE18694:
	.size	_ZNK2v88internal3Map39TransitionRequiresSynchronizationWithGCES1_, .-_ZNK2v88internal3Map39TransitionRequiresSynchronizationWithGCES1_
	.section	.text._ZNK2v88internal3Map22InstancesNeedRewritingES1_iiiPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map22InstancesNeedRewritingES1_iiiPi
	.type	_ZNK2v88internal3Map22InstancesNeedRewritingES1_iiiPi, @function
_ZNK2v88internal3Map22InstancesNeedRewritingES1_iiiPi:
.LFB18696:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	xorl	%r10d, %r10d
	movq	39(%rax), %r11
	leaq	31(%r11), %r8
	xorl	%r11d, %r11d
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L448:
	movq	(%r8), %rax
	addl	$1, %r10d
	sarq	$32, %rax
	andl	$2, %eax
	cmpl	$1, %eax
	movq	(%rdi), %rax
	adcl	$0, %r11d
	addq	$24, %r8
.L440:
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%r10d, %eax
	jg	.L448
	movl	%r11d, (%r9)
	cmpl	%r11d, %edx
	je	.L443
.L446:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	movq	(%rdi), %rax
	movq	39(%rsi), %rsi
	movq	39(%rax), %r9
	movl	15(%rax), %eax
	leaq	31(%rsi), %r8
	shrl	$10, %eax
	subq	%rsi, %r9
	andl	$1023, %eax
	leaq	(%rax,%rax,2), %rax
	leaq	(%r8,%rax,8), %r10
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L449:
	movq	(%r8), %rax
	movq	(%r9,%r8), %rsi
	addq	$24, %r8
	shrq	$38, %rsi
	andl	$7, %esi
	cmpb	$2, %sil
	sete	%sil
	shrq	$38, %rax
	andl	$7, %eax
	cmpb	$2, %al
	sete	%al
	cmpb	%al, %sil
	jne	.L446
.L447:
	cmpq	%r8, %r10
	jne	.L449
	movq	(%rdi), %rsi
	movzbl	7(%rsi), %eax
	movzbl	8(%rsi), %esi
	subl	%esi, %eax
	cmpl	%ecx, %eax
	sete	%al
	cmpl	%ecx, %edx
	setle	%dl
	orl	%edx, %eax
	xorl	$1, %eax
	ret
	.cfi_endproc
.LFE18696:
	.size	_ZNK2v88internal3Map22InstancesNeedRewritingES1_iiiPi, .-_ZNK2v88internal3Map22InstancesNeedRewritingES1_iiiPi
	.section	.text._ZNK2v88internal3Map22InstancesNeedRewritingES1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map22InstancesNeedRewritingES1_
	.type	_ZNK2v88internal3Map22InstancesNeedRewritingES1_, @function
_ZNK2v88internal3Map22InstancesNeedRewritingES1_:
.LFB18695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	15(%rsi), %r8
	xorl	%ecx, %ecx
	xorl	%r10d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	39(%rsi), %rax
	addq	$31, %rax
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L457:
	movq	(%rax), %rdx
	addl	$1, %ecx
	sarq	$32, %rdx
	andl	$2, %edx
	cmpl	$1, %edx
	adcl	$0, %r10d
	addq	$24, %rax
.L451:
	movl	(%r8), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	cmpl	%ecx, %edx
	jg	.L457
	movzbl	7(%rsi), %ecx
	movzbl	8(%rsi), %eax
	movzbl	9(%rsi), %r8d
	subl	%eax, %ecx
	cmpl	$2, %r8d
	jg	.L458
.L454:
	leaq	-12(%rbp), %r9
	movl	%r10d, %edx
	call	_ZNK2v88internal3Map22InstancesNeedRewritingES1_iiiPi
	movq	-8(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L459
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	.cfi_restore_state
	movzbl	7(%rsi), %eax
	subl	%r8d, %eax
	movl	%eax, %r8d
	jmp	.L454
.L459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18695:
	.size	_ZNK2v88internal3Map22InstancesNeedRewritingES1_, .-_ZNK2v88internal3Map22InstancesNeedRewritingES1_
	.section	.text._ZNK2v88internal3Map14NumberOfFieldsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map14NumberOfFieldsEv
	.type	_ZNK2v88internal3Map14NumberOfFieldsEv, @function
_ZNK2v88internal3Map14NumberOfFieldsEv:
.LFB18697:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	39(%rax), %rsi
	leaq	31(%rsi), %rdx
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L464:
	movq	(%rdx), %rax
	addl	$1, %ecx
	sarq	$32, %rax
	andl	$2, %eax
	cmpl	$1, %eax
	movq	(%rdi), %rax
	adcl	$0, %r8d
	addq	$24, %rdx
.L461:
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%eax, %ecx
	jl	.L464
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE18697:
	.size	_ZNK2v88internal3Map14NumberOfFieldsEv, .-_ZNK2v88internal3Map14NumberOfFieldsEv
	.section	.text._ZNK2v88internal3Map14GetFieldCountsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map14GetFieldCountsEv
	.type	_ZNK2v88internal3Map14GetFieldCountsEv, @function
_ZNK2v88internal3Map14GetFieldCountsEv:
.LFB18698:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	movq	39(%rdx), %rax
	leaq	31(%rax), %rcx
	xorl	%eax, %eax
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L470:
	addl	$1, %r8d
.L468:
	movq	(%rdi), %rdx
	addq	$24, %rcx
.L466:
	movl	15(%rdx), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	cmpl	%edx, %esi
	jge	.L467
	movq	(%rcx), %rdx
	addl	$1, %esi
	sarq	$32, %rdx
	testb	$2, %dl
	jne	.L468
	andl	$4, %edx
	je	.L470
	addl	$1, %eax
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L467:
	movq	%rax, %rdx
	movl	%r8d, %eax
	salq	$32, %rdx
	orq	%rdx, %rax
	ret
	.cfi_endproc
.LFE18698:
	.size	_ZNK2v88internal3Map14GetFieldCountsEv, .-_ZNK2v88internal3Map14GetFieldCountsEv
	.section	.text._ZNK2v88internal3Map24HasOutOfObjectPropertiesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map24HasOutOfObjectPropertiesEv
	.type	_ZNK2v88internal3Map24HasOutOfObjectPropertiesEv, @function
_ZNK2v88internal3Map24HasOutOfObjectPropertiesEv:
.LFB18699:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rsi
	xorl	%r8d, %r8d
	movzbl	7(%rsi), %eax
	movzbl	8(%rsi), %edx
	movq	39(%rsi), %rdi
	subl	%edx, %eax
	leaq	31(%rdi), %rdx
	xorl	%edi, %edi
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L475:
	movq	(%rdx), %rcx
	addl	$1, %r8d
	sarq	$32, %rcx
	andl	$2, %ecx
	cmpl	$1, %ecx
	adcl	$0, %edi
	addq	$24, %rdx
.L472:
	movl	15(%rsi), %ecx
	shrl	$10, %ecx
	andl	$1023, %ecx
	cmpl	%r8d, %ecx
	jg	.L475
	cmpl	%eax, %edi
	setg	%al
	ret
	.cfi_endproc
.LFE18699:
	.size	_ZNK2v88internal3Map24HasOutOfObjectPropertiesEv, .-_ZNK2v88internal3Map24HasOutOfObjectPropertiesEv
	.section	.text._ZN2v88internal3Map23DeprecateTransitionTreeEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map23DeprecateTransitionTreeEPNS0_7IsolateE
	.type	_ZN2v88internal3Map23DeprecateTransitionTreeEPNS0_7IsolateE, @function
_ZN2v88internal3Map23DeprecateTransitionTreeEPNS0_7IsolateE:
.LFB18700:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	je	.L478
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	jmp	_ZN2v88internal3Map23DeprecateTransitionTreeEPNS0_7IsolateE.part.0
	.cfi_endproc
.LFE18700:
	.size	_ZN2v88internal3Map23DeprecateTransitionTreeEPNS0_7IsolateE, .-_ZN2v88internal3Map23DeprecateTransitionTreeEPNS0_7IsolateE
	.section	.text._ZNK2v88internal3Map11FindRootMapEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map11FindRootMapEPNS0_7IsolateE
	.type	_ZNK2v88internal3Map11FindRootMapEPNS0_7IsolateE, @function
_ZNK2v88internal3Map11FindRootMapEPNS0_7IsolateE:
.LFB18702:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L483:
	movq	%rax, %r8
.L482:
	movq	%r8, %rdx
	movq	31(%r8), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	subq	$37592, %rdx
	testb	$1, %al
	je	.L480
	movq	-1(%rax), %rcx
	cmpq	%rcx, 136(%rdx)
	je	.L481
.L480:
	movq	88(%rdx), %rax
.L481:
	cmpq	%rax, 88(%rsi)
	jne	.L483
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE18702:
	.size	_ZNK2v88internal3Map11FindRootMapEPNS0_7IsolateE, .-_ZNK2v88internal3Map11FindRootMapEPNS0_7IsolateE
	.section	.text._ZNK2v88internal3Map14FindFieldOwnerEPNS0_7IsolateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map14FindFieldOwnerEPNS0_7IsolateEi
	.type	_ZNK2v88internal3Map14FindFieldOwnerEPNS0_7IsolateEi, @function
_ZNK2v88internal3Map14FindFieldOwnerEPNS0_7IsolateEi:
.LFB18703:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L491:
	movl	15(%rax), %ecx
	shrl	$10, %ecx
	andl	$1023, %ecx
	cmpl	%ecx, %edx
	jge	.L490
	movq	%rax, %r8
.L489:
	movq	%r8, %rcx
	movq	31(%r8), %rax
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	subq	$37592, %rcx
	testb	$1, %al
	je	.L485
	movq	-1(%rax), %rdi
	cmpq	%rdi, 136(%rcx)
	je	.L486
.L485:
	movq	88(%rcx), %rax
.L486:
	cmpq	%rax, 88(%rsi)
	jne	.L491
.L490:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE18703:
	.size	_ZNK2v88internal3Map14FindFieldOwnerEPNS0_7IsolateEi, .-_ZNK2v88internal3Map14FindFieldOwnerEPNS0_7IsolateEi
	.section	.text._ZN2v88internal18FieldTypeIsClearedENS0_14RepresentationENS0_9FieldTypeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18FieldTypeIsClearedENS0_14RepresentationENS0_9FieldTypeE
	.type	_ZN2v88internal18FieldTypeIsClearedENS0_14RepresentationENS0_9FieldTypeE, @function
_ZN2v88internal18FieldTypeIsClearedENS0_14RepresentationENS0_9FieldTypeE:
.LFB18714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZN2v88internal9FieldType4NoneEv@PLT
	cmpb	$3, %r12b
	movq	%rax, %r8
	sete	%al
	cmpq	%r8, %rbx
	je	.L492
	xorl	%eax, %eax
.L492:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18714:
	.size	_ZN2v88internal18FieldTypeIsClearedENS0_14RepresentationENS0_9FieldTypeE, .-_ZN2v88internal18FieldTypeIsClearedENS0_14RepresentationENS0_9FieldTypeE
	.section	.text._ZN2v88internal3Map19GeneralizeFieldTypeENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEES2_S5_PNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map19GeneralizeFieldTypeENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEES2_S5_PNS0_7IsolateE
	.type	_ZN2v88internal3Map19GeneralizeFieldTypeENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEES2_S5_PNS0_7IsolateE, @function
_ZN2v88internal3Map19GeneralizeFieldTypeENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEES2_S5_PNS0_7IsolateE:
.LFB18715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movb	%dil, -65(%rbp)
	movq	(%rsi), %r15
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	call	_ZN2v88internal9FieldType4NoneEv@PLT
	cmpq	%rax, %r15
	jne	.L506
	cmpb	$3, -65(%rbp)
	je	.L504
.L506:
	movq	(%rbx), %r15
	call	_ZN2v88internal9FieldType4NoneEv@PLT
	cmpb	$3, %r13b
	jne	.L507
	cmpq	%rax, %r15
	je	.L504
.L507:
	movq	(%r12), %rax
	leaq	-64(%rbp), %r13
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9FieldType5NowIsENS0_6HandleIS1_EE@PLT
	movl	%eax, %r8d
	movq	%rbx, %rax
	testb	%r8b, %r8b
	je	.L519
.L503:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L520
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L519:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9FieldType5NowIsENS0_6HandleIS1_EE@PLT
	testb	%al, %al
	jne	.L521
.L504:
	movq	%r14, %rdi
	call	_ZN2v88internal9FieldType3AnyEPNS0_7IsolateE@PLT
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L521:
	movq	%r12, %rax
	jmp	.L503
.L520:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18715:
	.size	_ZN2v88internal3Map19GeneralizeFieldTypeENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEES2_S5_PNS0_7IsolateE, .-_ZN2v88internal3Map19GeneralizeFieldTypeENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEES2_S5_PNS0_7IsolateE
	.section	.text._ZN2v88internal3Map19ReconfigurePropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_12PropertyKindENS0_18PropertyAttributesENS0_14RepresentationENS4_INS0_9FieldTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map19ReconfigurePropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_12PropertyKindENS0_18PropertyAttributesENS0_14RepresentationENS4_INS0_9FieldTypeEEE
	.type	_ZN2v88internal3Map19ReconfigurePropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_12PropertyKindENS0_18PropertyAttributesENS0_14RepresentationENS4_INS0_9FieldTypeEEE, @function
_ZN2v88internal3Map19ReconfigurePropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_12PropertyKindENS0_18PropertyAttributesENS0_14RepresentationENS4_INS0_9FieldTypeEEE:
.LFB18717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-176(%rbp), %r14
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	pushq	%rbx
	movq	%r14, %rdi
	.cfi_offset 3, -48
	movl	%r9d, %ebx
	subq	$144, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal10MapUpdaterC1EPNS0_7IsolateENS0_6HandleINS0_3MapEEE@PLT
	movq	16(%rbp), %r9
	movl	%ebx, %r8d
	movl	%r13d, %edx
	movl	$1, %ecx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal10MapUpdater22ReconfigureToDataFieldEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEE@PLT
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L525
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L525:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18717:
	.size	_ZN2v88internal3Map19ReconfigurePropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_12PropertyKindENS0_18PropertyAttributesENS0_14RepresentationENS4_INS0_9FieldTypeEEE, .-_ZN2v88internal3Map19ReconfigurePropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_12PropertyKindENS0_18PropertyAttributesENS0_14RepresentationENS4_INS0_9FieldTypeEEE
	.section	.text._ZN2v88internal3Map23ReconfigureElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map23ReconfigureElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE
	.type	_ZN2v88internal3Map23ReconfigureElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE, @function
_ZN2v88internal3Map23ReconfigureElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE:
.LFB18718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-160(%rbp), %r12
	movl	%edx, %ebx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	subq	$144, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal10MapUpdaterC1EPNS0_7IsolateENS0_6HandleINS0_3MapEEE@PLT
	movzbl	%bl, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10MapUpdater23ReconfigureElementsKindENS0_12ElementsKindE@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L529
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L529:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18718:
	.size	_ZN2v88internal3Map23ReconfigureElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE, .-_ZN2v88internal3Map23ReconfigureElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE
	.section	.text._ZN2v88internal3Map28TryReplayPropertyTransitionsEPNS0_7IsolateES1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map28TryReplayPropertyTransitionsEPNS0_7IsolateES1_
	.type	_ZN2v88internal3Map28TryReplayPropertyTransitionsEPNS0_7IsolateES1_, @function
_ZN2v88internal3Map28TryReplayPropertyTransitionsEPNS0_7IsolateES1_:
.LFB18732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -112(%rbp)
	movq	%rdx, %rsi
	movq	(%rdi), %rdx
	movq	%rdx, %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	15(%rdx), %ecx
	movl	15(%rsi), %ebx
	movq	39(%rsi), %rsi
	shrl	$10, %ecx
	shrl	$10, %ebx
	andl	$1023, %ecx
	andl	$1023, %ebx
	cmpl	%ecx, %ebx
	jle	.L531
	leal	3(%rcx,%rcx,2), %eax
	movq	$-8, %r13
	sall	$3, %eax
	subq	%rsi, %r13
	cltq
	movq	%r13, -120(%rbp)
	leaq	7(%rsi,%rax), %r12
	leal	-1(%rbx), %eax
	subl	%ecx, %eax
	movslq	%ecx, %rcx
	addq	%rax, %rcx
	leaq	(%rcx,%rcx,2), %rax
	leaq	55(%rsi,%rax,8), %rax
	movq	%rax, -128(%rbp)
.L560:
	movq	-120(%rbp), %rax
	movq	(%r12), %r8
	movq	$0, -72(%rbp)
	movq	$0, -88(%rbp)
	leaq	(%rax,%r12), %r13
	movq	-112(%rbp), %rax
	movq	%r15, -80(%rbp)
	sarq	$32, %r8
	movq	%rax, -96(%rbp)
	movq	71(%r15), %rax
	movq	%rax, -72(%rbp)
	testb	$1, %al
	je	.L532
	cmpl	$3, %eax
	je	.L532
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L588
	cmpq	$1, %rdx
	je	.L589
.L536:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L532:
	movl	$1, -64(%rbp)
.L534:
	movl	%r8d, %ecx
	movl	%r8d, %edx
	movq	-8(%r12), %rsi
	leaq	-96(%rbp), %r14
	shrl	$3, %ecx
	andl	$1, %edx
	movq	%r14, %rdi
	movq	%r8, -104(%rbp)
	andl	$7, %ecx
	call	_ZN2v88internal19TransitionsAccessor16SearchTransitionENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE@PLT
	movq	-104(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L557
	movq	39(%rax), %rcx
	movl	%r8d, %edi
	shrl	$2, %edi
	addq	%r13, %rcx
	movq	8(%rcx), %rax
	movq	%rax, %rsi
	shrq	$34, %rax
	notl	%eax
	sarq	$32, %rsi
	orl	%edi, %eax
	testb	$1, %al
	je	.L557
	movl	%r8d, %r11d
	movl	%esi, %r10d
	shrl	$6, %r11d
	shrl	$6, %r10d
	andl	$7, %r11d
	andl	$7, %r10d
	cmpb	%r11b, %r10b
	movl	%r11d, %r13d
	setg	%al
	cmpl	$3, %r10d
	jne	.L544
	testb	%r11b, %r11b
	sete	%al
.L544:
	cmpl	%r13d, %r10d
	je	.L564
	testb	%al, %al
	je	.L557
.L564:
	testb	$2, %sil
	jne	.L546
	andl	$1, %esi
	jne	.L536
	movq	16(%rcx), %rdi
	cmpl	$3, %edi
	je	.L590
	movq	%rdi, %rax
	movl	%r10d, -136(%rbp)
	andl	$3, %eax
	cmpq	$3, %rax
	jne	.L586
	andq	$-3, %rdi
.L586:
	call	_ZN2v88internal9FieldType4castENS0_6ObjectE@PLT
	movl	-136(%rbp), %r10d
	movq	%rax, -104(%rbp)
.L548:
	movl	%r10d, -136(%rbp)
	call	_ZN2v88internal9FieldType4NoneEv@PLT
	movl	-136(%rbp), %r10d
	cmpl	$3, %r10d
	jne	.L550
	cmpq	%rax, -104(%rbp)
	je	.L557
.L550:
	movq	8(%r12), %rdi
	cmpl	$3, %edi
	je	.L591
	movq	%rdi, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	jne	.L587
	andq	$-3, %rdi
.L587:
	call	_ZN2v88internal9FieldType4castENS0_6ObjectE@PLT
	movq	%rax, %rcx
.L552:
	movq	%rcx, -96(%rbp)
	movq	%rcx, -136(%rbp)
	call	_ZN2v88internal9FieldType4NoneEv@PLT
	cmpl	$3, %r13d
	jne	.L565
	movq	-136(%rbp), %rcx
	cmpq	%rax, %rcx
	je	.L557
.L565:
	movq	-104(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal9FieldType5NowIsES1_@PLT
	testb	%al, %al
	jne	.L556
	.p2align 4,,10
	.p2align 3
.L557:
	xorl	%eax, %eax
.L561:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L592
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	.cfi_restore_state
	andl	$2, %r8d
	je	.L557
	movq	16(%rcx), %rax
	movq	8(%r12), %rcx
	cmpq	%rax, %rcx
	jne	.L557
.L556:
	addq	$24, %r12
	cmpq	-128(%rbp), %r12
	jne	.L560
.L531:
	movl	15(%r15), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%eax, %ebx
	movl	$0, %eax
	cmove	%r15, %rax
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L588:
	movl	$3, -64(%rbp)
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L590:
	movl	%r10d, -136(%rbp)
	call	_ZN2v88internal9FieldType4NoneEv@PLT
	movl	-136(%rbp), %r10d
	movq	%rax, -104(%rbp)
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L591:
	call	_ZN2v88internal9FieldType4NoneEv@PLT
	movq	%rax, %rcx
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L589:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L593
	movl	$4, -64(%rbp)
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L593:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -64(%rbp)
	jmp	.L534
.L592:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18732:
	.size	_ZN2v88internal3Map28TryReplayPropertyTransitionsEPNS0_7IsolateES1_, .-_ZN2v88internal3Map28TryReplayPropertyTransitionsEPNS0_7IsolateES1_
	.section	.text._ZN2v88internal3Map6UpdateEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map6UpdateEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal3Map6UpdateEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal3Map6UpdateEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$152, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L595
	movq	%rsi, %rax
.L596:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L599
	addq	$152, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L595:
	.cfi_restore_state
	leaq	-160(%rbp), %r12
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10MapUpdaterC1EPNS0_7IsolateENS0_6HandleINS0_3MapEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal10MapUpdater6UpdateEv@PLT
	jmp	.L596
.L599:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18733:
	.size	_ZN2v88internal3Map6UpdateEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal3Map6UpdateEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal3Map27LookupElementsTransitionMapEPNS0_7IsolateENS0_12ElementsKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map27LookupElementsTransitionMapEPNS0_7IsolateENS0_12ElementsKindE
	.type	_ZN2v88internal3Map27LookupElementsTransitionMapEPNS0_7IsolateENS0_12ElementsKindE, @function
_ZN2v88internal3Map27LookupElementsTransitionMapEPNS0_7IsolateENS0_12ElementsKindE:
.LFB18741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	14(%rbx), %eax
	shrl	$3, %eax
	cmpb	%al, %dl
	je	.L616
	movq	%rsi, %r14
	movl	%edx, %r12d
	leaq	-96(%rbp), %r15
	movl	%edx, %r13d
.L613:
	movq	$0, -72(%rbp)
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	71(%rbx), %rax
	movq	%rax, -72(%rbp)
	testb	$1, %al
	je	.L602
	cmpl	$3, %eax
	je	.L602
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L622
	cmpq	$1, %rdx
	je	.L623
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L602:
	movl	$1, -64(%rbp)
.L604:
	movq	3672(%r14), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE@PLT
	testq	%rax, %rax
	je	.L624
	movzbl	14(%rax), %edx
	shrl	$3, %edx
	cmpb	%dl, %r13b
	je	.L614
	movq	%rax, %rbx
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L622:
	movl	$3, -64(%rbp)
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L624:
	movzbl	14(%rbx), %eax
	shrl	$3, %eax
	cmpb	%al, %r12b
	je	.L616
	xorl	%eax, %eax
.L614:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L625
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L623:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L626
	movl	$4, -64(%rbp)
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L626:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -64(%rbp)
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L616:
	movq	%rbx, %rax
	jmp	.L614
.L625:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18741:
	.size	_ZN2v88internal3Map27LookupElementsTransitionMapEPNS0_7IsolateENS0_12ElementsKindE, .-_ZN2v88internal3Map27LookupElementsTransitionMapEPNS0_7IsolateENS0_12ElementsKindE
	.section	.text._ZNK2v88internal3Map26IsMapInArrayPrototypeChainEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map26IsMapInArrayPrototypeChainEPNS0_7IsolateE
	.type	_ZNK2v88internal3Map26IsMapInArrayPrototypeChainEPNS0_7IsolateE, @function
_ZNK2v88internal3Map26IsMapInArrayPrototypeChainEPNS0_7IsolateE:
.LFB18742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	12464(%rsi), %rax
	movq	39(%rax), %rax
	movq	463(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L628
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L629:
	movq	-1(%rsi), %rdx
	movl	$1, %eax
	cmpq	(%r12), %rdx
	je	.L627
	movq	12464(%rbx), %rax
	movq	39(%rax), %rax
	movq	519(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L632
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L633:
	movq	-1(%rsi), %rax
	cmpq	%rax, (%r12)
	sete	%al
.L627:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L632:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L637
.L634:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L628:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L638
.L630:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L638:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L637:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L634
	.cfi_endproc
.LFE18742:
	.size	_ZNK2v88internal3Map26IsMapInArrayPrototypeChainEPNS0_7IsolateE, .-_ZNK2v88internal3Map26IsMapInArrayPrototypeChainEPNS0_7IsolateE
	.section	.text._ZNK2v88internal3Map28NumberOfEnumerablePropertiesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map28NumberOfEnumerablePropertiesEv
	.type	_ZNK2v88internal3Map28NumberOfEnumerablePropertiesEv, @function
_ZNK2v88internal3Map28NumberOfEnumerablePropertiesEv:
.LFB18746:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	39(%rax), %rcx
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	je	.L639
	subl	$1, %eax
	leaq	31(%rcx), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	55(%rcx,%rax,8), %rsi
	xorl	%eax, %eax
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L644:
	addq	$24, %rdx
	cmpq	%rdx, %rsi
	je	.L639
.L641:
	movq	(%rdx), %rcx
	btq	$36, %rcx
	jc	.L644
	movq	-8(%rdx), %rcx
	testb	$1, %cl
	jne	.L648
.L643:
	addq	$24, %rdx
	addl	$1, %eax
	cmpq	%rdx, %rsi
	jne	.L641
.L639:
	ret
	.p2align 4,,10
	.p2align 3
.L648:
	movq	-1(%rcx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L643
	jmp	.L644
	.cfi_endproc
.LFE18746:
	.size	_ZNK2v88internal3Map28NumberOfEnumerablePropertiesEv, .-_ZNK2v88internal3Map28NumberOfEnumerablePropertiesEv
	.section	.text._ZNK2v88internal3Map21NextFreePropertyIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map21NextFreePropertyIndexEv
	.type	_ZNK2v88internal3Map21NextFreePropertyIndexEv, @function
_ZNK2v88internal3Map21NextFreePropertyIndexEv:
.LFB18747:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	15(%rdx), %eax
	movq	39(%rdx), %rsi
	shrl	$10, %eax
	andl	$1023, %eax
	je	.L653
	leal	(%rax,%rax,2), %ecx
	subl	$1, %eax
	sall	$3, %ecx
	leaq	(%rax,%rax,2), %rax
	movslq	%ecx, %rcx
	salq	$3, %rax
	leaq	7(%rsi,%rcx), %rdx
	leaq	-17(%rsi,%rcx), %rsi
	subq	%rax, %rsi
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L652:
	subq	$24, %rdx
	cmpq	%rsi, %rdx
	je	.L653
.L654:
	movq	(%rdx), %rax
	sarq	$32, %rax
	movl	%eax, %ecx
	testb	$2, %al
	jne	.L652
	shrl	$19, %ecx
	andl	$1023, %ecx
	leal	1(%rcx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L653:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18747:
	.size	_ZNK2v88internal3Map21NextFreePropertyIndexEv, .-_ZNK2v88internal3Map21NextFreePropertyIndexEv
	.section	.text._ZNK2v88internal3Map23OnlyHasSimplePropertiesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map23OnlyHasSimplePropertiesEv
	.type	_ZNK2v88internal3Map23OnlyHasSimplePropertiesEv, @function
_ZNK2v88internal3Map23OnlyHasSimplePropertiesEv:
.LFB18748:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	xorl	%eax, %eax
	movzbl	14(%rcx), %edx
	shrl	$3, %edx
	subl	$15, %edx
	cmpb	$1, %dl
	jbe	.L655
	cmpw	$1040, 11(%rcx)
	ja	.L659
.L655:
	ret
	.p2align 4,,10
	.p2align 3
.L659:
	movl	15(%rcx), %eax
	shrl	$21, %eax
	xorl	$1, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE18748:
	.size	_ZNK2v88internal3Map23OnlyHasSimplePropertiesEv, .-_ZNK2v88internal3Map23OnlyHasSimplePropertiesEv
	.section	.text._ZN2v88internal3Map38DictionaryElementsInPrototypeChainOnlyEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map38DictionaryElementsInPrototypeChainOnlyEPNS0_7IsolateE
	.type	_ZN2v88internal3Map38DictionaryElementsInPrototypeChainOnlyEPNS0_7IsolateE, @function
_ZN2v88internal3Map38DictionaryElementsInPrototypeChainOnlyEPNS0_7IsolateE:
.LFB18749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movzbl	14(%rdx), %eax
	shrl	$3, %eax
	cmpl	$12, %eax
	je	.L660
	leaq	-16(%rbp), %rdi
	movq	%rdx, -16(%rbp)
	call	_ZNK2v88internal3Map24GetPrototypeChainRootMapEPNS0_7IsolateE
	movq	23(%rax), %rax
	cmpq	104(%rsi), %rax
	jne	.L672
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L666:
	movq	(%rdx), %rcx
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	cmpl	$14, %ecx
	je	.L686
.L669:
	testb	$1, %al
	jne	.L687
.L670:
	movq	(%rdx), %rax
	movq	23(%rax), %rax
	cmpq	104(%rsi), %rax
	je	.L662
.L672:
	movq	-1(%rax), %rcx
	leaq	-1(%rax), %rdx
	cmpw	$1024, 11(%rcx)
	je	.L663
	movq	-1(%rax), %rcx
	cmpw	$1041, 11(%rcx)
	je	.L664
.L667:
	movq	(%rdx), %rcx
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	cmpl	$12, %ecx
	jne	.L666
	movq	15(%rax), %rcx
	movq	39(%rcx), %rcx
	testb	$1, %cl
	jne	.L666
	btq	$32, %rcx
	jc	.L663
	movq	(%rdx), %rcx
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	cmpl	$14, %ecx
	jne	.L669
.L686:
	movq	15(%rax), %rcx
	movq	23(%rcx), %rcx
	movq	39(%rcx), %rcx
	testb	$1, %cl
	jne	.L669
	btq	$32, %rcx
	jc	.L663
	testb	$1, %al
	je	.L670
.L687:
	movq	(%rdx), %rax
	cmpw	$1024, 11(%rax)
	jne	.L670
.L662:
	xorl	%r8d, %r8d
.L660:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L688
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L664:
	.cfi_restore_state
	movq	23(%rax), %rcx
	testb	$1, %cl
	je	.L667
	movq	-1(%rcx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L667
.L663:
	movl	$1, %r8d
	jmp	.L660
.L688:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18749:
	.size	_ZN2v88internal3Map38DictionaryElementsInPrototypeChainOnlyEPNS0_7IsolateE, .-_ZN2v88internal3Map38DictionaryElementsInPrototypeChainOnlyEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal3Map17ConnectTransitionEPNS0_7IsolateENS0_6HandleIS1_EES5_NS4_INS0_4NameEEENS0_20SimpleTransitionFlagE.str1.1,"aMS",@progbits,1
.LC30:
	.string	"prototype"
.LC31:
	.string	"Transition"
.LC32:
	.string	""
	.section	.text._ZN2v88internal3Map17ConnectTransitionEPNS0_7IsolateENS0_6HandleIS1_EES5_NS4_INS0_4NameEEENS0_20SimpleTransitionFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map17ConnectTransitionEPNS0_7IsolateENS0_6HandleIS1_EES5_NS4_INS0_4NameEEENS0_20SimpleTransitionFlagE
	.type	_ZN2v88internal3Map17ConnectTransitionEPNS0_7IsolateENS0_6HandleIS1_EES5_NS4_INS0_4NameEEENS0_20SimpleTransitionFlagE, @function
_ZN2v88internal3Map17ConnectTransitionEPNS0_7IsolateENS0_6HandleIS1_EES5_NS4_INS0_4NameEEENS0_20SimpleTransitionFlagE:
.LFB18759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, %rdx
	movq	31(%rax), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	subq	$37592, %rdi
	testb	$1, %sil
	je	.L690
	movq	-1(%rsi), %rdx
	cmpq	%rdx, 136(%rdi)
	je	.L692
.L690:
	movq	88(%rdi), %rsi
.L692:
	cmpq	%rsi, 88(%r12)
	je	.L693
	movl	15(%rax), %edx
	andl	$-4194305, %edx
	movl	%edx, 15(%rax)
	movq	(%rbx), %rax
.L694:
	movq	%rax, -80(%rbp)
	movl	15(%rax), %edx
	andl	$1048576, %edx
	jne	.L702
	cmpw	$1057, 11(%rax)
	je	.L729
.L701:
	movq	(%rbx), %rax
	movq	%r12, %xmm0
	movq	%rbx, %xmm1
	movq	$0, -56(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -56(%rbp)
	testb	$1, %al
	je	.L703
	cmpl	$3, %eax
	je	.L703
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L730
	cmpq	$1, %rdx
	je	.L731
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L729:
	movl	15(%rax), %eax
	testl	$1047552, %eax
	je	.L701
	leaq	-80(%rbp), %rdi
	movl	%r8d, -84(%rbp)
	call	_ZNK2v88internal3Map14GetBackPointerEPNS0_7IsolateE.constprop.0
	cmpq	88(%r12), %rax
	movl	-84(%rbp), %r8d
	jne	.L701
	.p2align 4,,10
	.p2align 3
.L702:
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	jne	.L732
.L689:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L733
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L703:
	.cfi_restore_state
	movl	$1, -48(%rbp)
.L707:
	leaq	-80(%rbp), %rdi
	movl	%r8d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal19TransitionsAccessor6InsertENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEENS0_20SimpleTransitionFlagE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	je	.L689
	movq	41016(%r12), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L689
	movq	(%r14), %rcx
	movq	(%rbx), %rdx
	leaq	.LC32(%rip), %r8
	movq	%r12, %rdi
	movq	0(%r13), %r9
	leaq	.LC31(%rip), %rsi
	call	_ZN2v88internal6Logger8MapEventEPKcNS0_3MapES4_S3_NS0_10HeapObjectE@PLT
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L732:
	movq	41016(%r12), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L689
	movq	(%r14), %rcx
	movq	(%rbx), %rdx
	leaq	.LC30(%rip), %r8
	movq	%r12, %rdi
	movq	0(%r13), %r9
	leaq	.LC31(%rip), %rsi
	call	_ZN2v88internal6Logger8MapEventEPKcNS0_3MapES4_S3_NS0_10HeapObjectE@PLT
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L693:
	movq	%rax, -80(%rbp)
	movl	15(%rax), %edx
	andl	$1048576, %edx
	jne	.L694
	cmpw	$1057, 11(%rax)
	jne	.L694
	movl	15(%rax), %edx
	andl	$1047552, %edx
	je	.L694
	leaq	-80(%rbp), %rdi
	movl	%r8d, -84(%rbp)
	call	_ZNK2v88internal3Map14GetBackPointerEPNS0_7IsolateE.constprop.0
	movq	(%rbx), %rax
	movl	-84(%rbp), %r8d
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L730:
	movl	$3, -48(%rbp)
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L731:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L734
	movl	$4, -48(%rbp)
	jmp	.L707
.L734:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -48(%rbp)
	jmp	.L707
.L733:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18759:
	.size	_ZN2v88internal3Map17ConnectTransitionEPNS0_7IsolateENS0_6HandleIS1_EES5_NS4_INS0_4NameEEENS0_20SimpleTransitionFlagE, .-_ZN2v88internal3Map17ConnectTransitionEPNS0_7IsolateENS0_6HandleIS1_EES5_NS4_INS0_4NameEEENS0_20SimpleTransitionFlagE
	.section	.text._ZN2v88internal3Map22PrepareForDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS4_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map22PrepareForDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal3Map22PrepareForDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS4_INS0_6ObjectEEE, @function
_ZN2v88internal3Map22PrepareForDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS4_INS0_6ObjectEEE:
.LFB18774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L784
.L736:
	leal	3(%r13,%r13,2), %edx
	movq	(%r12), %rdi
	movq	(%r8), %rsi
	sall	$3, %edx
	movslq	%edx, %rdx
	leaq	-1(%rdx), %rcx
	addq	39(%rdi), %rcx
	movq	8(%rcx), %rax
	movq	%rax, %r9
	shrq	$33, %rax
	sarq	$32, %r9
	orl	%r9d, %eax
	testb	$1, %al
	jne	.L738
	testb	$4, %r9b
	je	.L766
	cmpl	$1, %ebx
	je	.L766
.L738:
	movq	39(%rdi), %rax
	movl	$4, %r8d
	movq	7(%rdx,%rax), %r10
	shrq	$35, %r10
	andl	$7, %r10d
	cmpb	$0, _ZN2v88internal17FLAG_track_fieldsE(%rip)
	jne	.L785
.L757:
	movl	%r8d, %edx
	movq	%rsi, -192(%rbp)
	movq	%r15, %rdi
	movq	%r14, %rsi
	movl	%r10d, -216(%rbp)
	movb	%r8b, -208(%rbp)
	call	_ZN2v88internal6Object11OptimalTypeEPNS0_7IsolateENS0_14RepresentationE@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal10MapUpdaterC1EPNS0_7IsolateENS0_6HandleINS0_3MapEEE@PLT
	movl	%ebx, %ecx
	movl	%r13d, %esi
	movq	%r15, %rdi
	movl	-216(%rbp), %r10d
	movq	-200(%rbp), %r9
	movzbl	-208(%rbp), %r8d
	movl	%r10d, %edx
	call	_ZN2v88internal10MapUpdater22ReconfigureToDataFieldEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEE@PLT
	movq	%rax, %r12
.L756:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L786
	addq	$184, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L784:
	.cfi_restore_state
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r15, %rdi
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal10MapUpdaterC1EPNS0_7IsolateENS0_6HandleINS0_3MapEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal10MapUpdater6UpdateEv@PLT
	movq	-200(%rbp), %r8
	movq	%rax, %r12
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L785:
	movl	$1, %r8d
	testb	$1, %sil
	je	.L757
	cmpb	$0, _ZN2v88internal24FLAG_track_double_fieldsE(%rip)
	jne	.L787
.L758:
	cmpb	$0, _ZN2v88internal26FLAG_track_computed_fieldsE(%rip)
	jne	.L788
.L760:
	cmpb	$0, _ZN2v88internal29FLAG_track_heap_object_fieldsE(%rip)
	sete	%r8b
	addl	$3, %r8d
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L766:
	shrl	$6, %r9d
	movzbl	_ZN2v88internal17FLAG_track_fieldsE(%rip), %eax
	andl	$7, %r9d
	cmpl	$1, %r9d
	jne	.L741
	testb	%al, %al
	jne	.L789
.L741:
	cmpb	$0, _ZN2v88internal24FLAG_track_double_fieldsE(%rip)
	je	.L767
	cmpl	$2, %r9d
	je	.L743
.L767:
	cmpb	$0, _ZN2v88internal29FLAG_track_heap_object_fieldsE(%rip)
	je	.L747
	cmpl	$3, %r9d
	je	.L790
.L747:
	testb	%al, %al
	jne	.L750
.L749:
	movq	16(%rcx), %rdi
	cmpl	$3, %edi
	je	.L791
	movq	%rdi, %rax
	movq	%rsi, -216(%rbp)
	andl	$3, %eax
	movq	%r8, -208(%rbp)
	movq	%rdx, -200(%rbp)
	cmpq	$3, %rax
	je	.L754
	call	_ZN2v88internal9FieldType4castENS0_6ObjectE@PLT
	movq	-200(%rbp), %rdx
	movq	-208(%rbp), %r8
	movq	-216(%rbp), %rsi
.L753:
	movq	%r15, %rdi
	movq	%r8, -208(%rbp)
	movq	%rdx, -200(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZNK2v88internal9FieldType11NowContainsENS0_6ObjectE@PLT
	testb	%al, %al
	jne	.L756
	movq	-208(%rbp), %r8
	movq	(%r12), %rdi
	movq	-200(%rbp), %rdx
	movq	(%r8), %rsi
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L750:
	testl	%r9d, %r9d
	jne	.L749
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L788:
	movq	%rsi, %rax
	xorl	%r8d, %r8d
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37512(%rax), %rsi
	jne	.L760
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L743:
	testb	$1, %sil
	je	.L749
	movq	-1(%rsi), %rax
	cmpw	$65, 11(%rax)
	sete	%al
	.p2align 4,,10
	.p2align 3
.L742:
	testb	%al, %al
	je	.L738
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L790:
	movl	%esi, %eax
	andl	$1, %eax
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L787:
	movq	-1(%rsi), %rax
	movl	$2, %r8d
	cmpw	$65, 11(%rax)
	jne	.L758
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L789:
	movq	%rsi, %rax
	notq	%rax
	andl	$1, %eax
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L754:
	andq	$-3, %rdi
	call	_ZN2v88internal9FieldType4castENS0_6ObjectE@PLT
	movq	-216(%rbp), %rsi
	movq	-208(%rbp), %r8
	movq	-200(%rbp), %rdx
	jmp	.L753
.L791:
	movq	%rsi, -216(%rbp)
	movq	%r8, -208(%rbp)
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal9FieldType4NoneEv@PLT
	movq	-200(%rbp), %rdx
	movq	-208(%rbp), %r8
	movq	-216(%rbp), %rsi
	jmp	.L753
.L786:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18774:
	.size	_ZN2v88internal3Map22PrepareForDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS4_INS0_6ObjectEEE, .-_ZN2v88internal3Map22PrepareForDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal3Map4HashEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map4HashEv
	.type	_ZN2v88internal3Map4HashEv, @function
_ZN2v88internal3Map4HashEv:
.LFB18781:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	movq	31(%rcx), %rax
	testb	$1, %al
	jne	.L795
.L793:
	movl	23(%rcx), %edx
	shrl	$2, %eax
	movzwl	%ax, %eax
	sall	$14, %edx
	xorl	%eax, %edx
	movzbl	14(%rcx), %eax
	xorl	%edx, %eax
	sarl	$16, %edx
	xorl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L798:
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L793
	.p2align 4,,10
	.p2align 3
.L795:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L793
	jmp	.L798
	.cfi_endproc
.LFE18781:
	.size	_ZN2v88internal3Map4HashEv, .-_ZN2v88internal3Map4HashEv
	.section	.rodata._ZNK2v88internal3Map25EquivalentToForTransitionES1_.str1.8,"aMS",@progbits,1
	.align 8
.LC33:
	.string	"GetConstructor() == other.GetConstructor()"
	.align 8
.LC34:
	.string	"instance_type() == other.instance_type()"
	.section	.text._ZNK2v88internal3Map25EquivalentToForTransitionES1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map25EquivalentToForTransitionES1_
	.type	_ZNK2v88internal3Map25EquivalentToForTransitionES1_, @function
_ZNK2v88internal3Map25EquivalentToForTransitionES1_:
.LFB18783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	31(%rsi), %rax
	testb	$1, %al
	jne	.L801
.L800:
	movq	(%rdi), %rdx
.L817:
	movq	31(%rdx), %rdx
	testb	$1, %dl
	jne	.L819
	cmpq	%rax, %rdx
	jne	.L820
.L804:
	movq	(%rdi), %rdx
	movzwl	11(%rdx), %ecx
	cmpw	11(%rsi), %cx
	jne	.L821
	movzbl	13(%rdx), %edi
	xorl	%eax, %eax
	cmpb	%dil, 13(%rsi)
	je	.L822
.L799:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L823
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L822:
	.cfi_restore_state
	movzbl	14(%rsi), %r8d
	movzbl	14(%rdx), %edi
	andl	$1, %r8d
	andl	$1, %edi
	cmpb	%dil, %r8b
	jne	.L799
	movq	23(%rsi), %rdi
	cmpq	%rdi, 23(%rdx)
	jne	.L799
	movl	$1, %eax
	cmpw	$1105, %cx
	jne	.L799
	movl	15(%rsi), %ecx
	movl	15(%rdx), %eax
	leaq	-16(%rbp), %rdi
	movq	39(%rdx), %rdx
	movq	39(%rsi), %rsi
	shrl	$10, %eax
	movq	%rdx, -16(%rbp)
	movl	%eax, %edx
	movl	%ecx, %eax
	shrl	$10, %eax
	andl	$1023, %edx
	andl	$1023, %eax
	cmpl	%eax, %edx
	cmovg	%eax, %edx
	call	_ZN2v88internal15DescriptorArray11IsEqualUpToES1_i@PLT
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L824:
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L800
	.p2align 4,,10
	.p2align 3
.L801:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L800
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L819:
	movq	-1(%rdx), %rcx
	cmpw	$68, 11(%rcx)
	je	.L817
	cmpq	%rax, %rdx
	je	.L804
.L820:
	leaq	.LC33(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L821:
	leaq	.LC34(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L823:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18783:
	.size	_ZNK2v88internal3Map25EquivalentToForTransitionES1_, .-_ZNK2v88internal3Map25EquivalentToForTransitionES1_
	.section	.text._ZN2v88internal3Map31FindElementsKindTransitionedMapEPNS0_7IsolateERKSt6vectorINS0_6HandleIS1_EESaIS6_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map31FindElementsKindTransitionedMapEPNS0_7IsolateERKSt6vectorINS0_6HandleIS1_EESaIS6_EE
	.type	_ZN2v88internal3Map31FindElementsKindTransitionedMapEPNS0_7IsolateERKSt6vectorINS0_6HandleIS1_EESaIS6_EE, @function
_ZN2v88internal3Map31FindElementsKindTransitionedMapEPNS0_7IsolateERKSt6vectorINS0_6HandleIS1_EESaIS6_EE:
.LFB18739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	15(%rax), %edx
	andl	$1048576, %edx
	jne	.L886
	cmpw	$1057, 11(%rax)
	movq	%rdi, %r15
	movq	%rsi, %r14
	je	.L887
.L827:
	movzbl	14(%rax), %eax
	movl	%eax, %r12d
	shrl	$3, %r12d
	cmpl	$47, %eax
	ja	.L859
	cmpl	$3, %r12d
	jne	.L888
.L859:
	xorl	%r12d, %r12d
.L830:
	movq	%r12, %rax
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L886:
	xorl	%eax, %eax
.L829:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L889
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L888:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal3Map11FindRootMapEPNS0_7IsolateE
	movq	%rax, %rsi
	movq	%rax, -152(%rbp)
	call	_ZNK2v88internal3Map25EquivalentToForTransitionES1_
	testb	%al, %al
	je	.L886
	leaq	-152(%rbp), %r13
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Map27LookupElementsTransitionMapEPNS0_7IsolateENS0_12ElementsKindE
	movq	$0, -120(%rbp)
	movq	%rax, -152(%rbp)
	movq	%r14, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	%rax, -128(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -120(%rbp)
	testb	$1, %al
	je	.L832
	cmpl	$3, %eax
	je	.L832
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L890
	cmpq	$1, %rdx
	jne	.L836
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L891
	movl	$4, -112(%rbp)
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L832:
	movl	$1, -112(%rbp)
.L834:
	movq	3672(%r14), %rsi
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE@PLT
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L859
	cmpb	$4, %r12b
	movl	%r12d, %ecx
	setbe	%dl
	notl	%ecx
	xorl	%r12d, %r12d
	andl	%edx, %ecx
	movb	%cl, -169(%rbp)
	leaq	-96(%rbp), %rcx
	movq	%rcx, -168(%rbp)
.L855:
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movl	%eax, %edi
	cmpl	$5, %eax
	ja	.L830
	movq	(%rbx), %rax
	movq	8(%rbx), %rsi
	cmpq	%rsi, %rax
	je	.L842
	.p2align 4,,10
	.p2align 3
.L845:
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L843
	movq	(%rdx), %rdx
	movzbl	14(%rdx), %edx
	shrl	$3, %edx
	cmpb	%dl, %dil
	je	.L844
.L843:
	addq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L845
.L842:
	movq	-152(%rbp), %rax
	movq	$0, -72(%rbp)
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -72(%rbp)
	testb	$1, %al
	je	.L849
	cmpl	$3, %eax
	je	.L849
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L892
	cmpq	$1, %rdx
	je	.L893
.L836:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L849:
	movl	$1, -64(%rbp)
.L851:
	movq	3672(%r14), %rsi
	movq	-168(%rbp), %rdi
	call	_ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE@PLT
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	jne	.L855
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L844:
	movq	(%r15), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Map28TryReplayPropertyTransitionsEPNS0_7IsolateES1_
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L842
	movq	%rdx, %rsi
	movq	%r15, %rdi
	movq	%rdx, -184(%rbp)
	call	_ZNK2v88internal3Map22InstancesNeedRewritingES1_
	testb	%al, %al
	jne	.L842
	movq	(%rbx), %rax
	movq	8(%rbx), %rdi
	cmpq	%rdi, %rax
	je	.L842
	movq	-184(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L848:
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L846
	cmpq	%rdx, (%rsi)
	je	.L847
.L846:
	addq	$8, %rax
	cmpq	%rax, %rdi
	jne	.L848
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L887:
	movl	15(%rax), %edx
	andl	$1047552, %edx
	je	.L827
	call	_ZNK2v88internal3Map14GetBackPointerEPNS0_7IsolateE.constprop.0
	cmpq	88(%rsi), %rax
	je	.L886
	movq	(%rdi), %rax
	jmp	.L827
.L892:
	movl	$3, -64(%rbp)
	jmp	.L851
.L847:
	movzbl	14(%rdx), %eax
	shrl	$3, %eax
	cmpb	$4, %al
	notl	%eax
	setbe	%sil
	andl	%esi, %eax
	cmpb	$0, -169(%rbp)
	je	.L894
	movb	%al, -169(%rbp)
	movq	%rdx, %r12
	jmp	.L842
.L893:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L853
	movl	$4, -64(%rbp)
	jmp	.L851
.L890:
	movl	$3, -112(%rbp)
	jmp	.L834
.L894:
	testb	%al, %al
	cmove	%rdx, %r12
	jmp	.L842
.L853:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -64(%rbp)
	jmp	.L851
.L891:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -112(%rbp)
	jmp	.L834
.L889:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18739:
	.size	_ZN2v88internal3Map31FindElementsKindTransitionedMapEPNS0_7IsolateERKSt6vectorINS0_6HandleIS1_EESaIS6_EE, .-_ZN2v88internal3Map31FindElementsKindTransitionedMapEPNS0_7IsolateERKSt6vectorINS0_6HandleIS1_EESaIS6_EE
	.section	.rodata._ZN2v88internal3Map13TryUpdateSlowEPNS0_7IsolateES1_.str1.8,"aMS",@progbits,1
	.align 8
.LC35:
	.string	"map.NumberOfOwnDescriptors() == source_map.NumberOfOwnDescriptors()"
	.section	.text._ZN2v88internal3Map13TryUpdateSlowEPNS0_7IsolateES1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map13TryUpdateSlowEPNS0_7IsolateES1_
	.type	_ZN2v88internal3Map13TryUpdateSlowEPNS0_7IsolateES1_, @function
_ZN2v88internal3Map13TryUpdateSlowEPNS0_7IsolateES1_:
.LFB18731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%rsi, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L935:
	movq	%rax, %rsi
.L898:
	movq	%rsi, %rdx
	movq	31(%rsi), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	subq	$37592, %rdx
	testb	$1, %al
	je	.L896
	movq	-1(%rax), %rcx
	cmpq	%rcx, 136(%rdx)
	je	.L897
.L896:
	movq	88(%rdx), %rax
.L897:
	cmpq	%rax, 88(%rbx)
	jne	.L935
	movq	%rsi, -216(%rbp)
	movl	15(%rsi), %eax
	testl	$16777216, %eax
	je	.L899
	movq	31(%rsi), %rax
	testb	$1, %al
	jne	.L901
.L900:
	movq	-232(%rbp), %rdx
	movq	55(%rax), %rax
	movzbl	14(%rdx), %ecx
	movzbl	14(%rax), %edx
	shrl	$3, %ecx
	shrl	$3, %edx
	cmpb	%dl, %cl
	je	.L903
.L904:
	xorl	%eax, %eax
.L903:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L956
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L899:
	.cfi_restore_state
	leaq	-232(%rbp), %rdi
	call	_ZNK2v88internal3Map25EquivalentToForTransitionES1_
	movl	%eax, %r12d
	testb	%al, %al
	je	.L904
	movq	-216(%rbp), %rax
	movq	-232(%rbp), %r13
	movq	$0, -192(%rbp)
	movzbl	14(%rax), %r14d
	movzbl	14(%r13), %ecx
	movl	15(%rax), %eax
	movl	15(%r13), %edx
	shrl	$27, %eax
	shrl	$27, %edx
	andl	$1, %edx
	andl	$1, %eax
	cmpb	%al, %dl
	jne	.L905
	movl	%ecx, %edx
	xorl	%r12d, %r12d
	shrl	$3, %edx
	andl	$31, %edx
.L906:
	shrb	$3, %r14b
	cmpb	%dl, %r14b
	je	.L957
	leaq	-216(%rbp), %r14
	movzbl	%dl, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Map27LookupElementsTransitionMapEPNS0_7IsolateENS0_12ElementsKindE
	movq	%rax, -216(%rbp)
	testq	%rax, %rax
	je	.L904
.L932:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Map28TryReplayPropertyTransitionsEPNS0_7IsolateES1_
	testq	%rax, %rax
	je	.L904
	testb	%r12b, %r12b
	je	.L903
	leaq	-96(%rbp), %r12
	movq	%rbx, -96(%rbp)
	movq	%r12, %rdi
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZN2v88internal19TransitionsAccessor10InitializeEv
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE@PLT
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L901:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L900
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L900
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L905:
	movq	%r13, %r15
	movb	$0, -176(%rbp)
	movl	$0, -172(%rbp)
	movq	%r15, %rax
	movq	%r13, -168(%rbp)
	andq	$-262144, %rax
	movq	$0, -160(%rbp)
	movq	31(%r13), %r13
	movq	24(%rax), %rax
	testb	$1, %r13b
	jne	.L907
.L909:
	movq	-37504(%rax), %r13
.L908:
	movq	$0, -120(%rbp)
	movq	%rbx, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	%r13, -128(%rbp)
	movq	71(%r13), %rax
	movq	%rax, -120(%rbp)
	testb	$1, %al
	je	.L910
	cmpl	$3, %eax
	je	.L910
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L958
	cmpq	$1, %rdx
	je	.L959
.L914:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L910:
	movl	$1, -112(%rbp)
.L912:
	leaq	-172(%rbp), %rcx
	leaq	-160(%rbp), %rdx
	movq	%r15, %rsi
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal19TransitionsAccessor29HasIntegrityLevelTransitionToENS0_3MapEPNS0_6SymbolEPNS0_18PropertyAttributesE@PLT
	testb	%al, %al
	je	.L929
	leaq	-96(%rbp), %rax
	leaq	15(%r13), %rdx
	movq	%rax, -248(%rbp)
	movl	15(%r13), %eax
	testl	$134217728, %eax
	jne	.L920
.L961:
	movq	%r13, %rax
	movq	31(%r13), %r8
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %r8b
	je	.L921
	movq	-1(%r8), %rdx
	cmpq	%rdx, 136(%rax)
	je	.L922
.L921:
	movq	88(%rax), %r8
.L922:
	movq	$0, -72(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	71(%r8), %rax
	movq	%rax, -72(%rbp)
	testb	$1, %al
	je	.L923
	cmpl	$3, %eax
	je	.L923
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L960
	cmpq	$1, %rdx
	jne	.L914
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L927
	movl	$4, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L925:
	movq	-248(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r8, -240(%rbp)
	call	_ZN2v88internal19TransitionsAccessor29HasIntegrityLevelTransitionToENS0_3MapEPNS0_6SymbolEPNS0_18PropertyAttributesE@PLT
	testb	%al, %al
	je	.L929
	movq	-240(%rbp), %r8
	movq	%r8, %r13
	movl	15(%r13), %eax
	leaq	15(%r13), %rdx
	testl	$134217728, %eax
	je	.L961
.L920:
	movl	(%rdx), %edx
	movl	15(%r15), %eax
	shrl	$10, %edx
	shrl	$10, %eax
	andl	$1023, %edx
	andl	$1023, %eax
	cmpl	%eax, %edx
	jne	.L962
	movb	$1, -176(%rbp)
	movq	-160(%rbp), %rax
	movq	%r13, -168(%rbp)
	movdqa	-176(%rbp), %xmm1
	movq	%rax, -192(%rbp)
	movaps	%xmm1, -208(%rbp)
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L907:
	movq	-1(%r13), %rdx
	cmpq	%rdx, -37456(%rax)
	jne	.L909
	jmp	.L908
	.p2align 4,,10
	.p2align 3
.L929:
	movdqa	-176(%rbp), %xmm0
	movq	-160(%rbp), %rdx
	movzbl	-176(%rbp), %eax
	movq	-168(%rbp), %r13
	movq	%rdx, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	testb	%al, %al
	je	.L904
.L931:
	movzbl	14(%r13), %edx
	shrl	$3, %edx
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L923:
	movl	$1, -64(%rbp)
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L957:
	leaq	-216(%rbp), %r14
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L960:
	movl	$3, -64(%rbp)
	jmp	.L925
.L958:
	movl	$3, -112(%rbp)
	jmp	.L912
.L959:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L963
	movl	$4, -112(%rbp)
	jmp	.L912
.L927:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -64(%rbp)
	jmp	.L925
.L963:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -112(%rbp)
	jmp	.L912
.L962:
	leaq	.LC35(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L956:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18731:
	.size	_ZN2v88internal3Map13TryUpdateSlowEPNS0_7IsolateES1_, .-_ZN2v88internal3Map13TryUpdateSlowEPNS0_7IsolateES1_
	.section	.text._ZN2v88internal3Map9TryUpdateEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map9TryUpdateEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal3Map9TryUpdateEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal3Map9TryUpdateEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18720:
	.cfi_startproc
	endbr64
	movq	(%rsi), %r8
	movl	15(%r8), %eax
	testl	$16777216, %eax
	jne	.L965
	movq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L965:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal3Map13TryUpdateSlowEPNS0_7IsolateES1_
	movq	%rax, %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L966
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L968
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L966:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L968:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L974
.L970:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L974:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L970
	.cfi_endproc
.LFE18720:
	.size	_ZN2v88internal3Map9TryUpdateEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal3Map9TryUpdateEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZNK2v88internal3Map37EquivalentToForElementsKindTransitionES1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map37EquivalentToForElementsKindTransitionES1_
	.type	_ZNK2v88internal3Map37EquivalentToForElementsKindTransitionES1_, @function
_ZNK2v88internal3Map37EquivalentToForElementsKindTransitionES1_:
.LFB18784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	31(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testb	$1, %dl
	jne	.L977
.L976:
	movq	(%rdi), %rax
.L993:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L995
	cmpq	%rax, %rdx
	jne	.L996
.L980:
	movq	(%rdi), %rdx
	movzwl	11(%rdx), %ecx
	cmpw	%cx, 11(%rsi)
	jne	.L997
	movzbl	13(%rsi), %edi
	xorl	%eax, %eax
	cmpb	%dil, 13(%rdx)
	je	.L998
.L975:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L999
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L998:
	.cfi_restore_state
	movzbl	14(%rdx), %r8d
	movzbl	14(%rsi), %edi
	andl	$1, %r8d
	andl	$1, %edi
	cmpb	%dil, %r8b
	jne	.L975
	movq	23(%rdx), %rdi
	cmpq	%rdi, 23(%rsi)
	jne	.L975
	movl	$1, %eax
	cmpw	$1105, %cx
	jne	.L975
	movl	15(%rsi), %eax
	movl	15(%rdx), %ecx
	leaq	-16(%rbp), %rdi
	movq	39(%rdx), %rdx
	movq	39(%rsi), %rsi
	shrl	$10, %eax
	movq	%rdx, -16(%rbp)
	movl	%eax, %edx
	movl	%ecx, %eax
	shrl	$10, %eax
	andl	$1023, %edx
	andl	$1023, %eax
	cmpl	%eax, %edx
	cmovg	%eax, %edx
	call	_ZN2v88internal15DescriptorArray11IsEqualUpToES1_i@PLT
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L1000:
	movq	31(%rdx), %rdx
	testb	$1, %dl
	je	.L976
	.p2align 4,,10
	.p2align 3
.L977:
	movq	-1(%rdx), %rax
	cmpw	$68, 11(%rax)
	jne	.L976
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L995:
	movq	-1(%rax), %rcx
	cmpw	$68, 11(%rcx)
	je	.L993
	cmpq	%rax, %rdx
	je	.L980
.L996:
	leaq	.LC33(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L997:
	leaq	.LC34(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L999:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18784:
	.size	_ZNK2v88internal3Map37EquivalentToForElementsKindTransitionES1_, .-_ZNK2v88internal3Map37EquivalentToForElementsKindTransitionES1_
	.section	.text._ZNK2v88internal3Map28EquivalentToForNormalizationES1_NS0_12ElementsKindENS0_25PropertyNormalizationModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal3Map28EquivalentToForNormalizationES1_NS0_12ElementsKindENS0_25PropertyNormalizationModeE
	.type	_ZNK2v88internal3Map28EquivalentToForNormalizationES1_NS0_12ElementsKindENS0_25PropertyNormalizationModeE, @function
_ZNK2v88internal3Map28EquivalentToForNormalizationES1_NS0_12ElementsKindENS0_25PropertyNormalizationModeE:
.LFB18785:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	testl	%ecx, %ecx
	jne	.L1034
.L1002:
	movq	31(%rbx), %rcx
	movzbl	14(%rbx), %esi
	movq	(%rdi), %r9
	testb	$1, %cl
	jne	.L1004
.L1003:
	movq	31(%r9), %rax
	testb	$1, %al
	jne	.L1006
.L1005:
	cmpq	%rax, %rcx
	je	.L1007
.L1011:
	xorl	%eax, %eax
.L1001:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1007:
	.cfi_restore_state
	movq	23(%r9), %rax
	cmpq	%rax, 23(%rbx)
	jne	.L1011
	movzwl	11(%rbx), %eax
	cmpw	%ax, 11(%r9)
	jne	.L1011
	movzbl	13(%rbx), %eax
	cmpb	%al, 13(%r9)
	jne	.L1011
	movl	15(%r9), %ecx
	movl	15(%rbx), %eax
	shrl	$27, %ecx
	shrl	$27, %eax
	andl	$1, %ecx
	andl	$1, %eax
	cmpb	%al, %cl
	jne	.L1011
	movzbl	14(%r9), %eax
	movl	%esi, %ecx
	andl	$1, %ecx
	andl	$1, %eax
	cmpb	%cl, %al
	jne	.L1011
	movq	(%rdi), %r12
	movzbl	%dl, %edx
	andl	$7, %esi
	sall	$3, %edx
	movzbl	14(%r12), %eax
	orl	%edx, %esi
	cmpl	%esi, %eax
	jne	.L1011
	movzbl	7(%r12), %eax
	movzbl	8(%r12), %edx
	subl	%edx, %eax
	cmpl	%eax, %r8d
	jne	.L1011
	movzbl	7(%r12), %eax
	sall	$3, %eax
	movl	%eax, %r13d
	je	.L1012
	movzwl	11(%r12), %edx
	movl	$24, %eax
	cmpw	$1057, %dx
	je	.L1013
	movsbl	13(%r12), %esi
	movzwl	%dx, %edi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
.L1013:
	movzbl	7(%r12), %edx
	subl	%eax, %r13d
	movzbl	8(%r12), %ecx
	movl	%r13d, %eax
	movzbl	7(%rbx), %r12d
	sarl	$3, %eax
	subl	%ecx, %edx
	subl	%edx, %eax
	sall	$3, %r12d
	movl	%eax, %r13d
	je	.L1014
.L1016:
	movzwl	11(%rbx), %eax
	movl	$24, %ecx
	cmpw	$1057, %ax
	je	.L1015
	movsbl	13(%rbx), %esi
	movzwl	%ax, %edi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
	movl	%eax, %ecx
.L1015:
	movzbl	7(%rbx), %eax
	movzbl	8(%rbx), %edx
	subl	%ecx, %r12d
	sarl	$3, %r12d
	subl	%edx, %eax
	subl	%eax, %r12d
.L1014:
	cmpl	%r13d, %r12d
	sete	%al
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1035:
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L1005
	.p2align 4,,10
	.p2align 3
.L1006:
	movq	-1(%rax), %r10
	cmpw	$68, 11(%r10)
	jne	.L1005
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1036:
	movq	31(%rcx), %rcx
	testb	$1, %cl
	je	.L1003
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	-1(%rcx), %rax
	cmpw	$68, 11(%rax)
	jne	.L1003
	jmp	.L1036
	.p2align 4,,10
	.p2align 3
.L1034:
	movzbl	7(%rsi), %r8d
	movzbl	8(%rsi), %eax
	subl	%eax, %r8d
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1012:
	movzbl	7(%rbx), %r12d
	movl	$1, %eax
	sall	$3, %r12d
	je	.L1001
	jmp	.L1016
	.cfi_endproc
.LFE18785:
	.size	_ZNK2v88internal3Map28EquivalentToForNormalizationES1_NS0_12ElementsKindENS0_25PropertyNormalizationModeE, .-_ZNK2v88internal3Map28EquivalentToForNormalizationES1_NS0_12ElementsKindENS0_25PropertyNormalizationModeE
	.section	.text._ZN2v88internal3Map21ComputeMinObjectSlackEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map21ComputeMinObjectSlackEPNS0_7IsolateE
	.type	_ZN2v88internal3Map21ComputeMinObjectSlackEPNS0_7IsolateE, @function
_ZN2v88internal3Map21ComputeMinObjectSlackEPNS0_7IsolateE:
.LFB18787:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movzbl	9(%rdx), %eax
	cmpl	$2, %eax
	jg	.L1050
.L1038:
	movl	%eax, -52(%rbp)
	movq	(%rdi), %rax
	movq	$0, -24(%rbp)
	movq	%rsi, -48(%rbp)
	movq	$0, -40(%rbp)
	movq	%rax, -32(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -24(%rbp)
	testb	$1, %al
	je	.L1039
	cmpl	$3, %eax
	je	.L1039
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L1051
	cmpq	$1, %rdx
	je	.L1052
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1039:
	movl	$1, -16(%rbp)
.L1041:
	leaq	-53(%rbp), %rcx
	leaq	-52(%rbp), %rdx
	leaq	-48(%rbp), %rdi
	leaq	_ZN2v88internalL19GetMinInobjectSlackENS0_3MapEPv(%rip), %rsi
	call	_ZN2v88internal19TransitionsAccessor30TraverseTransitionTreeInternalEPFvNS0_3MapEPvES3_PNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movl	-52(%rbp), %eax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1053
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1050:
	.cfi_restore_state
	movzbl	7(%rdx), %edx
	subl	%eax, %edx
	movl	%edx, %eax
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1051:
	movl	$3, -16(%rbp)
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L1054
	movl	$4, -16(%rbp)
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1054:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -16(%rbp)
	jmp	.L1041
.L1053:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18787:
	.size	_ZN2v88internal3Map21ComputeMinObjectSlackEPNS0_7IsolateE, .-_ZN2v88internal3Map21ComputeMinObjectSlackEPNS0_7IsolateE
	.section	.text._ZN2v88internal3Map29CompleteInobjectSlackTrackingEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map29CompleteInobjectSlackTrackingEPNS0_7IsolateE
	.type	_ZN2v88internal3Map29CompleteInobjectSlackTrackingEPNS0_7IsolateE, @function
_ZN2v88internal3Map29CompleteInobjectSlackTrackingEPNS0_7IsolateE:
.LFB18790:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$112, %rsp
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	9(%rdx), %eax
	cmpl	$2, %eax
	jg	.L1076
.L1056:
	movl	%eax, -132(%rbp)
	movq	(%rbx), %rax
	movq	$0, -56(%rbp)
	movq	%r12, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -56(%rbp)
	testb	$1, %al
	je	.L1057
	cmpl	$3, %eax
	je	.L1057
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L1077
	cmpq	$1, %rdx
	je	.L1078
.L1061:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1057:
	movl	$1, -48(%rbp)
.L1059:
	leaq	-128(%rbp), %r13
	leaq	-80(%rbp), %r14
	leaq	-132(%rbp), %rdx
	movq	%r13, %rcx
	leaq	_ZN2v88internalL19GetMinInobjectSlackENS0_3MapEPv(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal19TransitionsAccessor30TraverseTransitionTreeInternalEPFvNS0_3MapEPvES3_PNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	(%rbx), %rax
	movl	-132(%rbp), %edx
	movq	$0, -104(%rbp)
	movq	%r12, -128(%rbp)
	movl	%edx, -136(%rbp)
	movq	$0, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -104(%rbp)
	testb	$1, %al
	jne	.L1079
.L1065:
	movl	$1, -96(%rbp)
.L1067:
	testl	%edx, %edx
	jne	.L1080
	movq	%r14, %rcx
	xorl	%edx, %edx
	leaq	_ZN2v88internalL17StopSlackTrackingENS0_3MapEPv(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19TransitionsAccessor30TraverseTransitionTreeInternalEPFvNS0_3MapEPvES3_PNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
.L1055:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1081
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1079:
	.cfi_restore_state
	cmpl	$3, %eax
	je	.L1065
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$3, %rcx
	je	.L1082
	cmpq	$1, %rcx
	jne	.L1061
	movq	-1(%rax), %rcx
	cmpw	$149, 11(%rcx)
	jne	.L1069
	movl	$4, -96(%rbp)
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1080:
	leaq	-136(%rbp), %rdx
	movq	%r14, %rcx
	leaq	_ZN2v88internalL18ShrinkInstanceSizeENS0_3MapEPv(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19TransitionsAccessor30TraverseTransitionTreeInternalEPFvNS0_3MapEPvES3_PNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1076:
	movzbl	7(%rdx), %edx
	subl	%eax, %edx
	movl	%edx, %eax
	jmp	.L1056
	.p2align 4,,10
	.p2align 3
.L1082:
	movl	$3, -96(%rbp)
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1077:
	movl	$3, -48(%rbp)
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1078:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L1083
	movl	$4, -48(%rbp)
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1083:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -48(%rbp)
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1069:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -96(%rbp)
	jmp	.L1067
.L1081:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18790:
	.size	_ZN2v88internal3Map29CompleteInobjectSlackTrackingEPNS0_7IsolateE, .-_ZN2v88internal3Map29CompleteInobjectSlackTrackingEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi.str1.8,"aMS",@progbits,1
	.align 8
.LC36:
	.string	"static_cast<unsigned>(number) <= static_cast<unsigned>(kMaxNumberOfDescriptors)"
	.section	.text._ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi
	.type	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi, @function
_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi:
.LFB18791:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	%rdx, 39(%rax)
	andl	$1, %edx
	je	.L1090
	movq	%r12, %r15
	movq	(%rdi), %rdi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	leaq	39(%rdi), %rsi
	testl	$262144, %eax
	jne	.L1098
	testb	$24, %al
	je	.L1090
.L1102:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1099
.L1090:
	cmpl	$1020, %r13d
	ja	.L1100
.L1088:
	movq	(%rbx), %rdx
	movl	%r13d, %ecx
	sall	$10, %ecx
	movl	15(%rdx), %eax
	andl	$-1047553, %eax
	orl	%ecx, %eax
	movl	%eax, 15(%rdx)
	movq	%r12, %rax
	andq	$-262144, %rax
	movq	(%rbx), %rsi
	testb	$4, 10(%rax)
	jne	.L1101
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1101:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	37592(%r14), %rdi
	movl	%r13d, %ecx
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal41Heap_MarkingBarrierForDescriptorArraySlowEPNS0_4HeapENS0_10HeapObjectES3_i@PLT
	.p2align 4,,10
	.p2align 3
.L1098:
	.cfi_restore_state
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r15), %rax
	leaq	39(%rdi), %rsi
	testb	$24, %al
	jne	.L1102
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	cmpl	$1020, %r13d
	jbe	.L1088
	.p2align 4,,10
	.p2align 3
.L1100:
	leaq	.LC36(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18791:
	.size	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi, .-_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi
	.section	.rodata._ZN2v88internal3Map18InstallDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EES5_iNS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"static_cast<unsigned>(value) <= 255"
	.align 8
.LC38:
	.string	"static_cast<unsigned>(id) < 256"
	.section	.text._ZN2v88internal3Map18InstallDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EES5_iNS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map18InstallDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EES5_iNS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEE
	.type	_ZN2v88internal3Map18InstallDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EES5_iNS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEE, @function
_ZN2v88internal3Map18InstallDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EES5_iNS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEE:
.LFB18762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-64(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movq	%r13, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	leal	1(%rcx), %ebx
	movl	%ebx, %ecx
	leal	(%rbx,%rbx,2), %ebx
	sall	$3, %ebx
	subq	$56, %rsp
	movq	%r9, -72(%rbp)
	movslq	%ebx, %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	(%r8), %rdx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi
	movq	(%r14), %rdx
	movq	(%r12), %rax
	movzbl	9(%rdx), %edx
	movb	%dl, 9(%rax)
	movq	(%r15), %rax
	movq	7(%rbx,%rax), %rax
	movq	-72(%rbp), %r9
	movq	%rax, %rdx
	sarq	$32, %rdx
	btq	$33, %rax
	jnc	.L1132
.L1104:
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r9, %rcx
	call	_ZN2v88internal16LayoutDescriptor21AppendIfFastOrUseFullEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS0_15PropertyDetailsENS4_IS1_EE@PLT
	movq	(%r12), %rdi
	movq	(%rax), %rdx
	leaq	47(%rdi), %rsi
	movq	%rdx, 47(%rdi)
	testb	$1, %dl
	je	.L1122
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L1133
	testb	$24, %al
	je	.L1122
.L1139:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1134
	.p2align 4,,10
	.p2align 3
.L1122:
	movq	(%r12), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal3Map12GetVisitorIdES1_
	movq	-72(%rbp), %rdx
	cmpl	$255, %eax
	ja	.L1135
	movb	%al, 10(%rdx)
	movq	(%r15), %rax
	movq	-1(%rbx,%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1115
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
	movq	(%r14), %rax
	movl	15(%rax), %eax
	testl	$268435456, %eax
	jne	.L1121
.L1118:
	movq	(%rcx), %rax
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	je	.L1136
.L1119:
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Map17ConnectTransitionEPNS0_7IsolateENS0_6HandleIS1_EES5_NS4_INS0_4NameEEENS0_20SimpleTransitionFlagE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1137
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1115:
	.cfi_restore_state
	movq	41088(%r13), %rcx
	cmpq	41096(%r13), %rcx
	je	.L1138
.L1117:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rcx)
	movq	(%r14), %rax
	movl	15(%rax), %eax
	testl	$268435456, %eax
	je	.L1118
.L1121:
	movq	(%r12), %rdx
	movl	15(%rdx), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rdx)
	jmp	.L1119
	.p2align 4,,10
	.p2align 3
.L1133:
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1139
	jmp	.L1122
	.p2align 4,,10
	.p2align 3
.L1132:
	movq	(%r12), %rcx
	movzbl	9(%rcx), %eax
	cmpl	$2, %eax
	jle	.L1105
	movzbl	7(%rcx), %esi
	cmpl	%esi, %eax
	je	.L1140
	leal	1(%rax), %esi
	cmpl	$255, %eax
	je	.L1141
	movb	%sil, 9(%rcx)
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1105:
	leal	-1(%rax), %esi
	testl	%eax, %eax
	movl	$2, %eax
	cmovne	%esi, %eax
	movb	%al, 9(%rcx)
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1134:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1122
	.p2align 4,,10
	.p2align 3
.L1136:
	testb	$8, 11(%rax)
	jne	.L1121
	jmp	.L1119
	.p2align 4,,10
	.p2align 3
.L1135:
	leaq	.LC38(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1138:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1140:
	movb	$2, 9(%rcx)
	jmp	.L1104
.L1141:
	leaq	.LC37(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1137:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18762:
	.size	_ZN2v88internal3Map18InstallDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EES5_iNS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEE, .-_ZN2v88internal3Map18InstallDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EES5_iNS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEE
	.section	.text._ZN2v88internal3Map18ReplaceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayENS0_16LayoutDescriptorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map18ReplaceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayENS0_16LayoutDescriptorE
	.type	_ZN2v88internal3Map18ReplaceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayENS0_16LayoutDescriptorE, @function
_ZN2v88internal3Map18ReplaceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayENS0_16LayoutDescriptorE:
.LFB18701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%rdi, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	15(%rax), %edx
	andl	$1047552, %edx
	jne	.L1171
.L1142:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1172
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1171:
	.cfi_restore_state
	movq	31(%rax), %rdx
	andq	$-262144, %rax
	movq	%rsi, %r12
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %dl
	je	.L1145
	movq	-1(%rdx), %rcx
	cmpq	%rcx, 136(%rax)
	je	.L1146
.L1145:
	movq	88(%rax), %rdx
.L1146:
	leaq	37592(%r12), %rdi
	cmpq	%rdx, 88(%r12)
	je	.L1142
	movq	-80(%rbp), %rax
	movq	(%rax), %rsi
	movq	39(%rsi), %r14
	movq	%rsi, -64(%rbp)
	movq	%r14, %rax
	movswl	9(%r14), %ecx
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L1173
.L1148:
	movq	-64(%rbp), %r13
	cmpq	39(%r13), %r14
	jne	.L1152
	movq	-72(%rbp), %rax
	leaq	-64(%rbp), %r15
	movq	%rax, %rdx
	andq	$-262144, %rax
	notq	%rdx
	movq	%rax, -96(%rbp)
	movl	%edx, %esi
	andl	$1, %esi
	movb	%sil, -81(%rbp)
	jmp	.L1149
	.p2align 4,,10
	.p2align 3
.L1153:
	movq	%r13, -64(%rbp)
	cmpq	39(%r13), %r14
	jne	.L1152
.L1149:
	movq	31(%r13), %rdx
	testb	$1, %dl
	je	.L1150
	movq	%r13, %rax
	movq	%rdx, %r13
	andq	$-262144, %rax
	movq	24(%rax), %rsi
	movq	-1(%rdx), %rcx
	cmpq	%rcx, -37456(%rsi)
	je	.L1151
	movq	-64(%rbp), %r13
.L1150:
	movq	%r13, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %r13
.L1151:
	cmpq	%r13, 88(%r12)
	je	.L1152
	movq	-64(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	15(%rcx), %edx
	orl	$1023, %edx
	movl	%edx, 15(%rcx)
	movq	-64(%rbp), %rdx
	movl	15(%rdx), %ecx
	movq	%rbx, %rdx
	shrl	$10, %ecx
	andl	$1023, %ecx
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi
	movq	-64(%rbp), %rdx
	movq	47(%rdx), %rdx
	andl	$1, %edx
	je	.L1153
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	movq	%rax, 47(%rdx)
	cmpb	$0, -81(%rbp)
	jne	.L1153
	movq	-96(%rbp), %rcx
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rdx
	leaq	47(%rdi), %rsi
	testl	$262144, %edx
	je	.L1155
	movq	%rax, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rcx
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rdx
	leaq	47(%rdi), %rsi
.L1155:
	andl	$24, %edx
	je	.L1153
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L1153
	movq	-72(%rbp), %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	%r13, -64(%rbp)
	cmpq	39(%r13), %r14
	je	.L1149
	.p2align 4,,10
	.p2align 3
.L1152:
	movq	-80(%rbp), %rax
	movq	(%rax), %rdx
	movl	15(%rdx), %eax
	andl	$-4194305, %eax
	movl	%eax, 15(%rdx)
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1173:
	movq	%r14, %rdx
	call	_ZN2v88internal41Heap_MarkingBarrierForDescriptorArraySlowEPNS0_4HeapENS0_10HeapObjectES3_i@PLT
	jmp	.L1148
.L1172:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18701:
	.size	_ZN2v88internal3Map18ReplaceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayENS0_16LayoutDescriptorE, .-_ZN2v88internal3Map18ReplaceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayENS0_16LayoutDescriptorE
	.section	.text._ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi
	.type	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi, @function
_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi:
.LFB18734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -96(%rbp)
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1175
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L1176:
	movq	-96(%rbp), %r15
	movq	(%r15), %rax
	movl	15(%rax), %edx
	movq	(%rbx), %rcx
	movzwl	7(%rcx), %eax
	movzwl	9(%rcx), %ecx
	subl	%ecx, %eax
	cwtl
	cmpl	%r12d, %eax
	jl	.L1238
.L1174:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1239
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1238:
	.cfi_restore_state
	shrl	$10, %edx
	movl	%r12d, %ecx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	andl	$1023, %edx
	movl	%edx, %r13d
	call	_ZN2v88internal15DescriptorArray8CopyUpToEPNS0_7IsolateENS0_6HandleIS1_EEii@PLT
	movq	%rax, %r12
	movq	(%r15), %rax
	movq	47(%rax), %rax
	movq	%rax, -104(%rbp)
	testl	%r13d, %r13d
	je	.L1240
	movq	(%rbx), %rax
	movq	(%r12), %r15
	movq	15(%rax), %r13
	leaq	15(%r15), %rsi
	movq	%r13, 15(%r15)
	testb	$1, %r13b
	je	.L1203
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L1241
	testb	$24, %al
	je	.L1203
.L1245:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1203
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1203:
	movq	(%rbx), %rdx
	leaq	37592(%r14), %r15
	movswl	9(%rdx), %ecx
	movq	-96(%rbp), %rax
	movq	(%rax), %r13
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L1242
.L1187:
	movq	%r13, -72(%rbp)
	movq	(%rbx), %rax
	cmpq	%rax, 39(%r13)
	jne	.L1188
	movq	-104(%rbp), %rax
	notq	%rax
	andl	$1, %eax
	movb	%al, -112(%rbp)
	leaq	-37592(%r15), %rax
	leaq	-72(%rbp), %r15
	movq	%rax, -88(%rbp)
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1192:
	movq	%r13, -72(%rbp)
	movq	39(%r13), %rax
	cmpq	%rax, (%rbx)
	jne	.L1235
.L1196:
	movq	31(%r13), %rax
	testb	$1, %al
	je	.L1189
	movq	%r13, %r8
	movq	%rax, %r13
	andq	$-262144, %r8
	movq	24(%r8), %rcx
	movq	-1(%rax), %rdx
	cmpq	%rdx, -37456(%rcx)
	je	.L1190
	movq	-72(%rbp), %r13
.L1189:
	movq	%r13, %r8
	andq	$-262144, %r8
	movq	24(%r8), %rax
	movq	-37504(%rax), %r13
.L1190:
	movq	-88(%rbp), %rax
	cmpq	%r13, 88(%rax)
	je	.L1235
	movq	-72(%rbp), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	15(%rax), %ecx
	movq	(%r12), %rdx
	shrl	$10, %ecx
	andl	$1023, %ecx
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi
	movq	-72(%rbp), %rax
	movq	47(%rax), %rax
	testb	$1, %al
	je	.L1192
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rdx
	movq	%rdx, 47(%rax)
	cmpb	$0, -112(%rbp)
	jne	.L1192
	movq	%rdx, %rcx
	movq	-72(%rbp), %rdi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -120(%rbp)
	leaq	47(%rdi), %rsi
	testl	$262144, %eax
	je	.L1194
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-120(%rbp), %rcx
	movq	-72(%rbp), %rdi
	movq	8(%rcx), %rax
	leaq	47(%rdi), %rsi
.L1194:
	testb	$24, %al
	je	.L1192
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1192
	movq	-104(%rbp), %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	%r13, -72(%rbp)
	movq	39(%r13), %rax
	cmpq	%rax, (%rbx)
	je	.L1196
	.p2align 4,,10
	.p2align 3
.L1235:
	movq	-96(%rbp), %rax
	movq	(%rax), %r13
.L1188:
	movq	%r13, -64(%rbp)
	movl	15(%r13), %ecx
	leaq	-64(%rbp), %rdi
	movq	%r14, %rsi
	movq	(%r12), %rdx
	shrl	$10, %ecx
	andl	$1023, %ecx
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi
	movq	-64(%rbp), %rax
	movq	47(%rax), %rax
	testb	$1, %al
	je	.L1174
	movq	-64(%rbp), %rax
	movq	-104(%rbp), %rdx
	movq	%rdx, 47(%rax)
	testb	$1, %dl
	je	.L1174
	movq	%rdx, %rbx
	movq	-64(%rbp), %rdi
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	leaq	47(%rdi), %rsi
	testl	$262144, %eax
	jne	.L1243
.L1199:
	testb	$24, %al
	je	.L1174
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1174
	movq	-104(%rbp), %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1174
	.p2align 4,,10
	.p2align 3
.L1175:
	movq	41088(%r14), %rbx
	cmpq	%rbx, 41096(%r14)
	je	.L1244
.L1177:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rbx)
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1242:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal41Heap_MarkingBarrierForDescriptorArraySlowEPNS0_4HeapENS0_10HeapObjectES3_i@PLT
	movq	-96(%rbp), %rax
	movq	(%rax), %r13
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1241:
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-112(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1245
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1240:
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdi
	movq	%r14, %rsi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	movl	15(%rax), %ecx
	movq	(%r12), %rdx
	shrl	$10, %ecx
	andl	$1023, %ecx
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi
	movq	-64(%rbp), %rax
	movq	47(%rax), %rax
	testb	$1, %al
	je	.L1174
	movq	-64(%rbp), %rax
	movq	-104(%rbp), %rdx
	movq	%rdx, 47(%rax)
	testb	$1, %dl
	je	.L1174
	movq	%rdx, %rbx
	movq	-64(%rbp), %rdi
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	leaq	47(%rdi), %rsi
	testl	$262144, %eax
	je	.L1199
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdi
	movq	8(%rbx), %rax
	leaq	47(%rdi), %rsi
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1244:
	movq	%r14, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L1177
	.p2align 4,,10
	.p2align 3
.L1243:
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdi
	movq	8(%rbx), %rax
	leaq	47(%rdi), %rsi
	jmp	.L1199
.L1239:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18734:
	.size	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi, .-_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.text._ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateE
	.type	_ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateE, @function
_ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateE:
.LFB18792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	movq	71(%rax), %rsi
	testb	$1, %sil
	jne	.L1263
.L1247:
	call	_ZN2v88internal7Factory16NewPrototypeInfoEv@PLT
	movq	%rax, %rbx
	movq	(%r12), %rax
	movq	-1(%rax), %r13
	movq	(%rbx), %r12
	movq	%r12, 71(%r13)
	leaq	71(%r13), %r15
	testb	$1, %r12b
	je	.L1255
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L1264
	testb	$24, %al
	je	.L1255
.L1266:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1265
	.p2align 4,,10
	.p2align 3
.L1255:
	movq	%rbx, %rax
.L1251:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1264:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	testb	$24, %al
	jne	.L1266
	jmp	.L1255
	.p2align 4,,10
	.p2align 3
.L1263:
	movq	-1(%rsi), %rax
	cmpw	$95, 11(%rax)
	jne	.L1247
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	je	.L1248
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1265:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1255
	.p2align 4,,10
	.p2align 3
.L1248:
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L1267
.L1250:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rdi)
	movq	%rsi, (%rax)
	jmp	.L1251
.L1267:
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	jmp	.L1250
	.cfi_endproc
.LFE18792:
	.size	_ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateE, .-_ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateE
	.section	.text._ZN2v88internal3Map21TryGetObjectCreateMapEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map21TryGetObjectCreateMapEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE
	.type	_ZN2v88internal3Map21TryGetObjectCreateMapEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE, @function
_ZN2v88internal3Map21TryGetObjectCreateMapEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE:
.LFB18736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1269
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1270:
	movq	879(%rsi), %rax
	movq	41112(%rbx), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1272
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1273:
	movq	(%r12), %rdx
	cmpq	23(%rsi), %rdx
	je	.L1276
	cmpq	104(%rbx), %rdx
	je	.L1293
	movq	-1(%rdx), %rax
	cmpw	$1024, 11(%rax)
	ja	.L1281
.L1282:
	xorl	%eax, %eax
.L1276:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1293:
	.cfi_restore_state
	movq	12464(%rbx), %rax
	movq	39(%rax), %rax
	movq	1215(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1283
.L1290:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1272:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1294
.L1274:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1273
	.p2align 4,,10
	.p2align 3
.L1269:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1295
.L1271:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1270
	.p2align 4,,10
	.p2align 3
.L1281:
	movq	-1(%rdx), %rax
	movl	15(%rax), %eax
	testl	$1048576, %eax
	je	.L1282
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateE
	movq	(%rax), %rax
	movq	39(%rax), %rsi
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	jne	.L1282
	cmpl	$3, %esi
	je	.L1282
	movq	41112(%rbx), %rdi
	andq	$-3, %rsi
	testq	%rdi, %rdi
	jne	.L1290
	.p2align 4,,10
	.p2align 3
.L1283:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1296
.L1285:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1295:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1271
	.p2align 4,,10
	.p2align 3
.L1294:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1274
	.p2align 4,,10
	.p2align 3
.L1296:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1285
	.cfi_endproc
.LFE18736:
	.size	_ZN2v88internal3Map21TryGetObjectCreateMapEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE, .-_ZN2v88internal3Map21TryGetObjectCreateMapEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE
	.section	.text._ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleIS1_EEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleIS1_EEPNS0_7IsolateE
	.type	_ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleIS1_EEPNS0_7IsolateE, @function
_ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleIS1_EEPNS0_7IsolateE:
.LFB18793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%r12), %rax
	movq	71(%rax), %rsi
	testb	$1, %sil
	jne	.L1314
.L1298:
	call	_ZN2v88internal7Factory16NewPrototypeInfoEv@PLT
	movq	(%r12), %r13
	movq	(%rax), %r12
	movq	%rax, %rbx
	leaq	71(%r13), %r15
	movq	%r12, 71(%r13)
	testb	$1, %r12b
	je	.L1306
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L1315
	testb	$24, %al
	je	.L1306
.L1317:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1316
	.p2align 4,,10
	.p2align 3
.L1306:
	movq	%rbx, %rax
.L1302:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1315:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	testb	$24, %al
	jne	.L1317
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1314:
	movq	-1(%rsi), %rax
	cmpw	$95, 11(%rax)
	jne	.L1298
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	je	.L1299
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1316:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1299:
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L1318
.L1301:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rdi)
	movq	%rsi, (%rax)
	jmp	.L1302
.L1318:
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	jmp	.L1301
	.cfi_endproc
.LFE18793:
	.size	_ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleIS1_EEPNS0_7IsolateE, .-_ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleIS1_EEPNS0_7IsolateE
	.section	.text._ZN2v88internal3Map27SetShouldBeFastPrototypeMapENS0_6HandleIS1_EEbPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map27SetShouldBeFastPrototypeMapENS0_6HandleIS1_EEbPNS0_7IsolateE
	.type	_ZN2v88internal3Map27SetShouldBeFastPrototypeMapENS0_6HandleIS1_EEbPNS0_7IsolateE, @function
_ZN2v88internal3Map27SetShouldBeFastPrototypeMapENS0_6HandleIS1_EEbPNS0_7IsolateE:
.LFB18794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	%r8b, %r8b
	jne	.L1320
	movq	(%rdi), %rax
	movq	71(%rax), %rax
	testb	$1, %al
	jne	.L1327
.L1319:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1327:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	jne	.L1319
	call	_ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleIS1_EEPNS0_7IsolateE
	movq	(%rax), %rax
	leaq	47(%rax), %rdx
	movslq	51(%rax), %rax
	andl	$-2, %eax
.L1325:
	salq	$32, %rax
	movq	%rax, (%rdx)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1320:
	.cfi_restore_state
	call	_ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleIS1_EEPNS0_7IsolateE
	movq	(%rax), %rax
	leaq	47(%rax), %rdx
	movslq	51(%rax), %rax
	orl	$1, %eax
	jmp	.L1325
	.cfi_endproc
.LFE18794:
	.size	_ZN2v88internal3Map27SetShouldBeFastPrototypeMapENS0_6HandleIS1_EEbPNS0_7IsolateE, .-_ZN2v88internal3Map27SetShouldBeFastPrototypeMapENS0_6HandleIS1_EEbPNS0_7IsolateE
	.section	.text._ZN2v88internal3Map37GetOrCreatePrototypeChainValidityCellENS0_6HandleIS1_EEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map37GetOrCreatePrototypeChainValidityCellENS0_6HandleIS1_EEPNS0_7IsolateE
	.type	_ZN2v88internal3Map37GetOrCreatePrototypeChainValidityCellENS0_6HandleIS1_EEPNS0_7IsolateE, @function
_ZN2v88internal3Map37GetOrCreatePrototypeChainValidityCellENS0_6HandleIS1_EEPNS0_7IsolateE:
.LFB18795:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	cmpw	$1025, 11(%rax)
	je	.L1371
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal3Map24GetPrototypeChainRootMapEPNS0_7IsolateE
	movq	41112(%r12), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1334
.L1369:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L1337
.L1340:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1372
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1345:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1373
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1372:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1374
.L1344:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	$0, (%rax)
	jmp	.L1345
	.p2align 4,,10
	.p2align 3
.L1334:
	movq	41088(%r12), %rbx
	cmpq	%rbx, 41096(%r12)
	je	.L1370
.L1336:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	movq	(%rbx), %rax
	testb	$1, %al
	je	.L1340
.L1337:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1340
	movq	-1(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1375
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L1346:
	movq	%r12, %rsi
	call	_ZN2v88internal8JSObject25LazyRegisterPrototypeUserENS0_6HandleINS0_3MapEEEPNS0_7IsolateE@PLT
	movq	(%rbx), %rax
	movq	-1(%rax), %rax
	movq	63(%rax), %rsi
	testb	$1, %sil
	jne	.L1376
.L1348:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1352
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1353:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory7NewCellENS0_6HandleINS0_6ObjectEEE@PLT
	movq	(%rbx), %rdx
	movq	%rax, %r15
	movq	-1(%rdx), %r13
	movq	(%rax), %r12
	movq	%r12, 63(%r13)
	leaq	63(%r13), %r14
	testb	$1, %r12b
	je	.L1358
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rdx
	testl	$262144, %edx
	je	.L1356
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rdx
.L1356:
	andl	$24, %edx
	je	.L1358
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L1377
	.p2align 4,,10
	.p2align 3
.L1358:
	movq	%r15, %rax
	jmp	.L1345
	.p2align 4,,10
	.p2align 3
.L1371:
	movq	12464(%rsi), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	jne	.L1369
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	jne	.L1336
.L1370:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L1336
	.p2align 4,,10
	.p2align 3
.L1375:
	movq	41088(%r12), %rdi
	cmpq	41096(%r12), %rdi
	je	.L1378
.L1347:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L1346
	.p2align 4,,10
	.p2align 3
.L1352:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1379
.L1354:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	$0, (%rsi)
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1374:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1376:
	movq	-1(%rsi), %rax
	cmpw	$151, 11(%rax)
	jne	.L1348
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1349
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1350:
	cmpq	$0, 7(%rsi)
	je	.L1345
	jmp	.L1348
	.p2align 4,,10
	.p2align 3
.L1377:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1358
	.p2align 4,,10
	.p2align 3
.L1378:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1379:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1354
.L1349:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1380
.L1351:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1350
.L1380:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L1351
.L1373:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18795:
	.size	_ZN2v88internal3Map37GetOrCreatePrototypeChainValidityCellENS0_6HandleIS1_EEPNS0_7IsolateE, .-_ZN2v88internal3Map37GetOrCreatePrototypeChainValidityCellENS0_6HandleIS1_EEPNS0_7IsolateE
	.section	.text._ZN2v88internal3Map27IsPrototypeChainInvalidatedES1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map27IsPrototypeChainInvalidatedES1_
	.type	_ZN2v88internal3Map27IsPrototypeChainInvalidatedES1_, @function
_ZN2v88internal3Map27IsPrototypeChainInvalidatedES1_:
.LFB18796:
	.cfi_startproc
	endbr64
	movq	63(%rdi), %rax
	testb	$1, %al
	jne	.L1382
.L1384:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1382:
	movq	-1(%rax), %rdx
	cmpw	$151, 11(%rdx)
	jne	.L1384
	cmpq	$0, 7(%rax)
	setne	%al
	ret
	.cfi_endproc
.LFE18796:
	.size	_ZN2v88internal3Map27IsPrototypeChainInvalidatedES1_, .-_ZN2v88internal3Map27IsPrototypeChainInvalidatedES1_
	.section	.text._ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb
	.type	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb, @function
_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb:
.LFB18797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1411
.L1386:
	movq	(%rbx), %r12
	movq	-1(%r12), %rax
	cmpw	$1024, 11(%rax)
	ja	.L1412
.L1387:
	movq	0(%r13), %r13
	leaq	23(%r13), %r15
	cmpq	%r12, 104(%r14)
	je	.L1413
	movq	%r12, 23(%r13)
	testb	$1, %r12b
	je	.L1393
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1414
.L1390:
	testb	$24, %al
	je	.L1393
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1393
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1393:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1415
.L1385:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1416
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1413:
	.cfi_restore_state
	movq	%r12, 23(%r13)
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1385
.L1415:
	leaq	-88(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1412:
	movzbl	%cl, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8JSObject19OptimizeAsPrototypeENS0_6HandleIS1_EEb@PLT
	movq	(%rbx), %r12
	jmp	.L1387
	.p2align 4,,10
	.p2align 3
.L1411:
	movq	40960(%rdi), %rax
	leaq	-88(%rbp), %rsi
	movl	$165, %edx
	movl	%ecx, -100(%rbp)
	leaq	23240(%rax), %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movl	-100(%rbp), %ecx
	jmp	.L1386
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L1390
.L1416:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18797:
	.size	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb, .-_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb
	.section	.text._ZN2v88internal3Map7RawCopyEPNS0_7IsolateENS0_6HandleIS1_EEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map7RawCopyEPNS0_7IsolateENS0_6HandleIS1_EEii
	.type	_ZN2v88internal3Map7RawCopyEPNS0_7IsolateENS0_6HandleIS1_EEii, @function
_ZN2v88internal3Map7RawCopyEPNS0_7IsolateENS0_6HandleIS1_EEii:
.LFB18750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r8d
	movl	$3, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rax
	movzwl	11(%rax), %esi
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi@PLT
	movq	41112(%r13), %rdi
	movq	%rax, %r12
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1418
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1419:
	movq	%r13, %rdi
	movl	$1, %ecx
	movq	%r12, %rsi
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb
	movq	(%rbx), %rax
	movq	(%r12), %r14
	movq	31(%rax), %r13
	testb	$1, %r13b
	jne	.L1423
.L1421:
	movq	%r13, 31(%r14)
.L1427:
	movq	(%rbx), %rax
	movzbl	13(%rax), %edx
	movq	(%r12), %rax
	movb	%dl, 13(%rax)
	movq	(%rbx), %rax
	movzbl	14(%rax), %edx
	movq	(%r12), %rax
	movb	%dl, 14(%rax)
	movq	(%rbx), %rdx
	movl	15(%rdx), %eax
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	je	.L1425
	andl	$-30408704, %eax
	orl	$4195327, %eax
.L1426:
	movq	(%r12), %rdx
	movl	%eax, 15(%rdx)
	movq	(%r12), %rax
	movl	$0, 19(%rax)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1425:
	.cfi_restore_state
	andl	$-63963136, %eax
	orl	$4195327, %eax
	jmp	.L1426
	.p2align 4,,10
	.p2align 3
.L1439:
	movq	31(%r13), %r13
	testb	$1, %r13b
	je	.L1421
	.p2align 4,,10
	.p2align 3
.L1423:
	movq	-1(%r13), %rax
	cmpw	$68, 11(%rax)
	je	.L1439
	movq	%r13, 31(%r14)
	movq	%r13, %r15
	leaq	31(%r14), %rsi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	je	.L1428
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
.L1428:
	testb	$24, %al
	je	.L1427
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1427
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1427
	.p2align 4,,10
	.p2align 3
.L1418:
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L1440
.L1420:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdx)
	jmp	.L1419
	.p2align 4,,10
	.p2align 3
.L1440:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1420
	.cfi_endproc
.LFE18750:
	.size	_ZN2v88internal3Map7RawCopyEPNS0_7IsolateENS0_6HandleIS1_EEii, .-_ZN2v88internal3Map7RawCopyEPNS0_7IsolateENS0_6HandleIS1_EEii
	.section	.text._ZN2v88internal3Map14CopyNormalizedEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map14CopyNormalizedEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeE
	.type	_ZN2v88internal3Map14CopyNormalizedEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeE, @function
_ZN2v88internal3Map14CopyNormalizedEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeE:
.LFB18752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movl	%edx, %r8d
	movzbl	7(%rax), %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	sall	$3, %edx
	testl	%r8d, %r8d
	je	.L1449
	movzbl	7(%rax), %ecx
	movzbl	8(%rax), %eax
	subl	%eax, %ecx
.L1446:
	call	_ZN2v88internal3Map7RawCopyEPNS0_7IsolateENS0_6HandleIS1_EEii
	movq	(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	ja	.L1443
	movb	$0, 9(%rcx)
.L1444:
	movq	(%rax), %rcx
	movl	15(%rcx), %edx
	orl	$35651584, %edx
	movl	%edx, 15(%rcx)
	movq	(%rax), %rcx
	movl	15(%rcx), %edx
	andl	$-67108865, %edx
	movl	%edx, 15(%rcx)
	movq	(%rax), %rcx
	movl	15(%rcx), %edx
	orl	$268435456, %edx
	movl	%edx, 15(%rcx)
	movq	(%rax), %rcx
	movl	15(%rcx), %edx
	andl	$536870911, %edx
	movl	%edx, 15(%rcx)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1443:
	.cfi_restore_state
	movzbl	7(%rcx), %edx
	movzbl	8(%rcx), %edi
	movzbl	8(%rcx), %esi
	subl	%edi, %edx
	addl	%esi, %edx
	cmpl	$255, %edx
	ja	.L1450
	movb	%dl, 9(%rcx)
	jmp	.L1444
	.p2align 4,,10
	.p2align 3
.L1449:
	movzbl	7(%rax), %ecx
	movzbl	8(%rax), %r8d
	movzbl	%cl, %eax
	movzbl	%r8b, %ecx
	subl	%ecx, %eax
	xorl	%ecx, %ecx
	sall	$3, %eax
	subl	%eax, %edx
	jmp	.L1446
	.p2align 4,,10
	.p2align 3
.L1450:
	leaq	.LC37(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18752:
	.size	_ZN2v88internal3Map14CopyNormalizedEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeE, .-_ZN2v88internal3Map14CopyNormalizedEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeE
	.section	.text._ZN2v88internal3Map24CopyInitialMapNormalizedEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map24CopyInitialMapNormalizedEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeE
	.type	_ZN2v88internal3Map24CopyInitialMapNormalizedEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeE, @function
_ZN2v88internal3Map24CopyInitialMapNormalizedEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeE:
.LFB18755:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal3Map14CopyNormalizedEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeE
	.cfi_endproc
.LFE18755:
	.size	_ZN2v88internal3Map24CopyInitialMapNormalizedEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeE, .-_ZN2v88internal3Map24CopyInitialMapNormalizedEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeE
	.section	.rodata._ZN2v88internal3Map14CopyInitialMapEPNS0_7IsolateENS0_6HandleIS1_EEiii.str1.1,"aMS",@progbits,1
.LC39:
	.string	"0 == value"
.LC40:
	.string	"0 <= value"
	.section	.text._ZN2v88internal3Map14CopyInitialMapEPNS0_7IsolateENS0_6HandleIS1_EEiii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map14CopyInitialMapEPNS0_7IsolateENS0_6HandleIS1_EEiii
	.type	_ZN2v88internal3Map14CopyInitialMapEPNS0_7IsolateENS0_6HandleIS1_EEiii, @function
_ZN2v88internal3Map14CopyInitialMapEPNS0_7IsolateENS0_6HandleIS1_EEiii:
.LFB18756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%r8d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal3Map7RawCopyEPNS0_7IsolateENS0_6HandleIS1_EEii
	movq	%rax, %r13
	movq	(%rax), %rax
	cmpw	$1024, 11(%rax)
	ja	.L1453
	testl	%r14d, %r14d
	jne	.L1475
	movb	$0, 9(%rax)
	jmp	.L1455
	.p2align 4,,10
	.p2align 3
.L1453:
	testl	%r14d, %r14d
	js	.L1476
	movzbl	7(%rax), %edx
	movzbl	8(%rax), %esi
	movzbl	8(%rax), %ecx
	subl	%esi, %edx
	subl	%r14d, %edx
	addl	%ecx, %edx
	cmpl	$255, %edx
	ja	.L1477
	movb	%dl, 9(%rax)
.L1455:
	movq	(%rbx), %rax
	movl	15(%rax), %ecx
	shrl	$10, %ecx
	andl	$1023, %ecx
	jne	.L1478
.L1458:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1479
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1478:
	.cfi_restore_state
	movq	0(%r13), %rdx
	leaq	-48(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdx, -48(%rbp)
	movq	47(%rax), %r14
	movq	(%rbx), %rax
	movq	39(%rax), %rdx
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi
	movq	-48(%rbp), %rax
	movq	47(%rax), %rax
	testb	$1, %al
	je	.L1458
	movq	-48(%rbp), %rax
	movq	%r14, 47(%rax)
	testb	$1, %r14b
	je	.L1458
	movq	%r14, %rbx
	movq	-48(%rbp), %rdi
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	leaq	47(%rdi), %rsi
	testl	$262144, %eax
	jne	.L1480
.L1461:
	testb	$24, %al
	je	.L1458
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1458
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1458
	.p2align 4,,10
	.p2align 3
.L1475:
	leaq	.LC39(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1476:
	leaq	.LC40(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1480:
	movq	%r14, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-48(%rbp), %rdi
	movq	8(%rbx), %rax
	leaq	47(%rdi), %rsi
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1477:
	leaq	.LC37(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1479:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18756:
	.size	_ZN2v88internal3Map14CopyInitialMapEPNS0_7IsolateENS0_6HandleIS1_EEiii, .-_ZN2v88internal3Map14CopyInitialMapEPNS0_7IsolateENS0_6HandleIS1_EEiii
	.section	.rodata._ZN2v88internal3Map14AsLanguageModeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18SharedFunctionInfoEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC41:
	.string	"!constructor_or_backpointer().IsMap()"
	.section	.text._ZN2v88internal3Map14AsLanguageModeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map14AsLanguageModeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal3Map14AsLanguageModeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal3Map14AsLanguageModeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18SharedFunctionInfoEEE:
.LFB18764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movl	47(%rax), %eax
	testb	$64, %al
	jne	.L1482
	movq	%rsi, %rax
.L1483:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1538
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1482:
	.cfi_restore_state
	movq	12464(%rdi), %rax
	movq	%rdi, %r13
	movq	%rdx, %rbx
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1484
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1485:
	movq	(%rbx), %rax
	movl	47(%rax), %eax
	shrl	$15, %eax
	andl	$31, %eax
	leaq	1248(,%rax,8), %rax
	movq	-1(%rsi,%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1487
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L1488:
	movq	(%r12), %rax
	movq	%r13, %xmm0
	movq	%r12, %xmm1
	movq	$0, -72(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	71(%rax), %rax
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -72(%rbp)
	testb	$1, %al
	je	.L1490
	cmpl	$3, %eax
	je	.L1490
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L1539
	cmpq	$1, %rdx
	je	.L1540
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1484:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L1541
.L1486:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1490:
	movl	$1, -64(%rbp)
.L1492:
	movq	3816(%r13), %rsi
	leaq	-96(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1498
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1499
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L1483
	.p2align 4,,10
	.p2align 3
.L1487:
	movq	41088(%r13), %r14
	cmpq	%r14, 41096(%r13)
	je	.L1542
.L1489:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1498:
	movq	(%r12), %rax
	movl	15(%rax), %edx
	andl	$33554432, %edx
	je	.L1543
.L1502:
	movzbl	9(%rax), %r8d
	cmpl	$2, %r8d
	jg	.L1544
.L1503:
	movzbl	7(%rax), %ecx
	movzbl	8(%rax), %esi
	movq	%r13, %rdi
	movzbl	7(%rax), %edx
	movzbl	%sil, %eax
	movq	%r14, %rsi
	sall	$3, %edx
	subl	%eax, %ecx
	call	_ZN2v88internal3Map14CopyInitialMapEPNS0_7IsolateENS0_6HandleIS1_EEiii
	movq	(%rax), %rdi
	movq	%rax, %rbx
	movq	(%r12), %rax
	movq	31(%rax), %r14
	movq	%r14, %rdx
	notq	%rdx
	testb	$1, %r14b
	jne	.L1506
.L1504:
	movq	31(%rdi), %rax
	leaq	31(%rdi), %rsi
	testb	$1, %al
	je	.L1545
.L1518:
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	je	.L1546
.L1507:
	movq	%r14, (%rsi)
	andl	$1, %edx
	jne	.L1516
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -120(%rbp)
	testl	$262144, %eax
	je	.L1509
	movq	%r14, %rdx
	movq	%rsi, -136(%rbp)
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-120(%rbp), %rcx
	movq	-136(%rbp), %rsi
	movq	-128(%rbp), %rdi
	movq	8(%rcx), %rax
.L1509:
	testb	$24, %al
	je	.L1516
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1516
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1516:
	movq	(%r12), %rax
	movq	(%rbx), %rdi
	movq	23(%rax), %r14
	leaq	23(%rdi), %rsi
	movq	%r14, 23(%rdi)
	testb	$1, %r14b
	je	.L1515
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -120(%rbp)
	testl	$262144, %eax
	je	.L1512
	movq	%r14, %rdx
	movq	%rsi, -136(%rbp)
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-120(%rbp), %rcx
	movq	-136(%rbp), %rsi
	movq	-128(%rbp), %rdi
	movq	8(%rcx), %rax
.L1512:
	testb	$24, %al
	je	.L1515
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1515
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1515:
	movq	(%rbx), %rcx
	movq	(%r12), %rax
	movq	%r15, %rdi
	movl	15(%rax), %edx
	movl	15(%rcx), %eax
	andl	$-536870912, %edx
	andl	$536870911, %eax
	orl	%edx, %eax
	movl	%eax, 15(%rcx)
	movq	$0, -72(%rbp)
	movdqa	-112(%rbp), %xmm2
	movq	(%r12), %rax
	movaps	%xmm2, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal19TransitionsAccessor10InitializeEv
	movq	%r15, %rdi
	call	_ZN2v88internal19TransitionsAccessor22CanHaveMoreTransitionsEv@PLT
	testb	%al, %al
	jne	.L1547
.L1514:
	movq	%rbx, %rax
	jmp	.L1483
	.p2align 4,,10
	.p2align 3
.L1499:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L1548
.L1501:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L1483
	.p2align 4,,10
	.p2align 3
.L1545:
	movq	%r14, 31(%rdi)
	jmp	.L1516
	.p2align 4,,10
	.p2align 3
.L1549:
	movq	31(%r14), %r14
	movq	%r14, %rdx
	notq	%rdx
	testb	$1, %r14b
	je	.L1504
	.p2align 4,,10
	.p2align 3
.L1506:
	movq	-1(%r14), %rax
	cmpw	$68, 11(%rax)
	je	.L1549
	movq	31(%rdi), %rax
	leaq	31(%rdi), %rsi
	testb	$1, %al
	je	.L1507
	jmp	.L1518
	.p2align 4,,10
	.p2align 3
.L1539:
	movl	$3, -64(%rbp)
	jmp	.L1492
	.p2align 4,,10
	.p2align 3
.L1544:
	movzbl	7(%rax), %edx
	subl	%r8d, %edx
	movl	%edx, %r8d
	jmp	.L1503
	.p2align 4,,10
	.p2align 3
.L1543:
	movl	15(%rax), %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	orl	$33554432, %edx
	movl	%edx, 15(%rax)
	movl	$1, %edx
	movq	55(%rax), %rax
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE@PLT
	movq	(%r12), %rax
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1542:
	movq	%r13, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1541:
	movq	%r13, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1547:
	leaq	3816(%r13), %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$2, %r8d
	call	_ZN2v88internal3Map17ConnectTransitionEPNS0_7IsolateENS0_6HandleIS1_EES5_NS4_INS0_4NameEEENS0_20SimpleTransitionFlagE
	jmp	.L1514
	.p2align 4,,10
	.p2align 3
.L1540:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L1550
	movl	$4, -64(%rbp)
	jmp	.L1492
	.p2align 4,,10
	.p2align 3
.L1548:
	movq	%r13, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	jmp	.L1501
	.p2align 4,,10
	.p2align 3
.L1546:
	leaq	.LC41(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1550:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -64(%rbp)
	jmp	.L1492
.L1538:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18764:
	.size	_ZN2v88internal3Map14AsLanguageModeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18SharedFunctionInfoEEE, .-_ZN2v88internal3Map14AsLanguageModeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal3Map19CopyDropDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map19CopyDropDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal3Map19CopyDropDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal3Map19CopyDropDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpw	$1024, 11(%rax)
	leaq	7(%rax), %rdx
	ja	.L1559
.L1552:
	movzbl	(%rdx), %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	sall	$3, %edx
	call	_ZN2v88internal3Map7RawCopyEPNS0_7IsolateENS0_6HandleIS1_EEii
	movq	(%rbx), %rdx
	movq	%rax, %r13
	cmpw	$1024, 11(%rdx)
	ja	.L1560
	movl	15(%rdx), %eax
	testl	$33554432, %eax
	je	.L1561
.L1554:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1562
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1559:
	.cfi_restore_state
	movzbl	7(%rax), %ecx
	movzbl	8(%rax), %eax
	subl	%eax, %ecx
	jmp	.L1552
	.p2align 4,,10
	.p2align 3
.L1560:
	movq	(%rax), %rax
	movzbl	9(%rdx), %edx
	movb	%dl, 9(%rax)
	movq	(%rbx), %rdx
	movl	15(%rdx), %eax
	testl	$33554432, %eax
	jne	.L1554
.L1561:
	movl	15(%rdx), %eax
	leaq	-48(%rbp), %rdi
	movq	%r12, %rsi
	orl	$33554432, %eax
	movl	%eax, 15(%rdx)
	movq	55(%rdx), %rax
	movl	$1, %edx
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE@PLT
	jmp	.L1554
.L1562:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18757:
	.size	_ZN2v88internal3Map19CopyDropDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal3Map19CopyDropDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal3Map15ShareDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEEPNS0_10DescriptorE.str1.1,"aMS",@progbits,1
.LC42:
	.string	"0 <= max_slack"
	.section	.text._ZN2v88internal3Map15ShareDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEEPNS0_10DescriptorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map15ShareDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEEPNS0_10DescriptorE
	.type	_ZN2v88internal3Map15ShareDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEEPNS0_10DescriptorE, @function
_ZN2v88internal3Map15ShareDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEEPNS0_10DescriptorE:
.LFB18758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal3Map19CopyDropDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EE
	movq	%rax, %r14
	movq	(%r15), %rax
	movq	%rax, -88(%rbp)
	movq	(%rax), %rax
	movq	-1(%rax), %rcx
	movq	-72(%rbp), %rdx
	cmpw	$64, 11(%rcx)
	je	.L1590
.L1565:
	movq	(%rdx), %rax
	movzwl	7(%rax), %ecx
	movzwl	9(%rax), %edx
	cmpw	%dx, %cx
	jne	.L1567
	movswl	9(%rax), %edx
	testl	%edx, %edx
	je	.L1591
	movl	$1020, %eax
	subl	%edx, %eax
	js	.L1592
	movl	$1, %r9d
	cmpl	$3, %edx
	jle	.L1570
	sarl	$2, %edx
	cmpl	%eax, %edx
	cmovle	%edx, %eax
	movl	%eax, %r9d
.L1570:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%r9d, %edx
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1571
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1567:
	movl	24(%r15), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal16LayoutDescriptor11ShareAppendEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS0_15PropertyDetailsE@PLT
	movq	(%rbx), %rdx
	leaq	-64(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	movq	-72(%rbp), %rax
	movq	(%r14), %rdx
	movq	%r12, %rsi
	movq	%rdx, -64(%rbp)
	movq	(%rax), %r8
	movq	(%rbx), %rdx
	movq	%r8, -72(%rbp)
	movswl	9(%rdx), %ecx
	movq	-80(%rbp), %rdi
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %r8
	movq	%r8, 47(%rax)
	testb	$1, %r8b
	je	.L1577
	movq	%r8, %rbx
	movq	-64(%rbp), %rdi
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	leaq	47(%rdi), %rsi
	testl	$262144, %eax
	jne	.L1593
	testb	$24, %al
	je	.L1577
.L1597:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1594
	.p2align 4,,10
	.p2align 3
.L1577:
	movq	-64(%rbp), %rdi
	call	_ZN2v88internal3Map12GetVisitorIdES1_
	cmpl	$255, %eax
	ja	.L1595
	movq	-64(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	%al, 10(%rdx)
	movq	%r14, %rdx
	movq	-88(%rbp), %rcx
	call	_ZN2v88internal3Map17ConnectTransitionEPNS0_7IsolateENS0_6HandleIS1_EES5_NS4_INS0_4NameEEENS0_20SimpleTransitionFlagE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1596
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1593:
	.cfi_restore_state
	movq	%r8, %rdx
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdi
	movq	8(%rbx), %rax
	movq	-72(%rbp), %r8
	leaq	47(%rdi), %rsi
	testb	$24, %al
	jne	.L1597
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1590:
	testb	$8, 11(%rax)
	je	.L1565
	movq	(%r14), %rcx
	movl	15(%rcx), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rcx)
	jmp	.L1565
	.p2align 4,,10
	.p2align 3
.L1591:
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15DescriptorArray8AllocateEPNS0_7IsolateEiiNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1594:
	movq	%r8, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1571:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L1598
.L1572:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1595:
	leaq	.LC38(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1592:
	leaq	.LC42(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1598:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L1572
.L1596:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18758:
	.size	_ZN2v88internal3Map15ShareDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEEPNS0_10DescriptorE, .-_ZN2v88internal3Map15ShareDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEEPNS0_10DescriptorE
	.section	.rodata._ZN2v88internal3Map22CopyReplaceDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEENS0_14TransitionFlagENS0_11MaybeHandleINS0_4NameEEEPKcNS0_20SimpleTransitionFlagE.str1.1,"aMS",@progbits,1
.LC43:
	.string	"ReplaceDescriptors"
	.section	.text._ZN2v88internal3Map22CopyReplaceDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEENS0_14TransitionFlagENS0_11MaybeHandleINS0_4NameEEEPKcNS0_20SimpleTransitionFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map22CopyReplaceDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEENS0_14TransitionFlagENS0_11MaybeHandleINS0_4NameEEEPKcNS0_20SimpleTransitionFlagE
	.type	_ZN2v88internal3Map22CopyReplaceDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEENS0_14TransitionFlagENS0_11MaybeHandleINS0_4NameEEEPKcNS0_20SimpleTransitionFlagE, @function
_ZN2v88internal3Map22CopyReplaceDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEENS0_14TransitionFlagENS0_11MaybeHandleINS0_4NameEEEPKcNS0_20SimpleTransitionFlagE:
.LFB18760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -136(%rbp)
	movl	%r8d, -116(%rbp)
	movq	%rax, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal3Map19CopyDropDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EE
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.L1637
	movq	(%rbx), %rax
	movq	-1(%rax), %rdx
	movq	%rbx, -128(%rbp)
	cmpw	$64, 11(%rdx)
	je	.L1663
.L1601:
	movq	(%r12), %rax
	movl	15(%rax), %edx
	andl	$1048576, %edx
	jne	.L1664
	movl	-116(%rbp), %edx
	testl	%edx, %edx
	je	.L1608
	leaq	-96(%rbp), %rdi
.L1620:
	movq	(%r15), %rax
	movq	%rdi, -136(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal15DescriptorArray19GeneralizeAllFieldsEv@PLT
	movq	(%r14), %rax
	movq	(%r15), %rdx
	movq	%r13, %rsi
	movq	%rax, -96(%rbp)
	movswl	9(%rdx), %ecx
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi
	movq	-96(%rbp), %rax
	movq	$0, 47(%rax)
	movq	-96(%rbp), %rdi
	call	_ZN2v88internal3Map12GetVisitorIdES1_
	cmpl	$255, %eax
	ja	.L1609
	movq	-96(%rbp), %rdx
	movb	%al, 10(%rdx)
.L1607:
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	jne	.L1665
.L1627:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1666
	addq	$120, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1608:
	.cfi_restore_state
	movq	%r13, %xmm0
	movq	%r12, %xmm1
	movq	$0, -72(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -72(%rbp)
	testb	$1, %al
	je	.L1611
	cmpl	$3, %eax
	je	.L1611
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L1667
	cmpq	$1, %rdx
	je	.L1668
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1611:
	movl	$1, -64(%rbp)
.L1613:
	leaq	-96(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	call	_ZN2v88internal19TransitionsAccessor22CanHaveMoreTransitionsEv@PLT
	movq	-152(%rbp), %rdi
	testb	%al, %al
	je	.L1620
	movq	(%r14), %rax
	movq	(%r15), %rdx
	leaq	-104(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -104(%rbp)
	movq	-136(%rbp), %rax
	movq	(%rax), %r8
	movq	%r8, -136(%rbp)
	movswl	9(%rdx), %ecx
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi
	movq	-104(%rbp), %rax
	movq	-136(%rbp), %r8
	movq	%r8, 47(%rax)
	testb	$1, %r8b
	je	.L1635
	movq	%r8, %r15
	movq	-104(%rbp), %rdi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	leaq	47(%rdi), %rsi
	testl	$262144, %eax
	jne	.L1669
.L1622:
	testb	$24, %al
	je	.L1635
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1635
	movq	%r8, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1635:
	movq	-104(%rbp), %rdi
	call	_ZN2v88internal3Map12GetVisitorIdES1_
	cmpl	$255, %eax
	ja	.L1609
	movq	-104(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movb	%al, 10(%rdx)
	movq	%r14, %rdx
	movl	24(%rbp), %r8d
	movq	-128(%rbp), %rcx
	call	_ZN2v88internal3Map17ConnectTransitionEPNS0_7IsolateENS0_6HandleIS1_EES5_NS4_INS0_4NameEEENS0_20SimpleTransitionFlagE
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1664:
	movq	(%r14), %rax
	movq	(%r15), %rdx
	leaq	-112(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -112(%rbp)
	movq	-136(%rbp), %rax
	movq	(%rax), %r8
	movq	%r8, -136(%rbp)
	movswl	9(%rdx), %ecx
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r8
	movq	%r8, 47(%rax)
	testb	$1, %r8b
	je	.L1634
	movq	%r8, %r15
	movq	-112(%rbp), %rdi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	leaq	47(%rdi), %rsi
	testl	$262144, %eax
	jne	.L1670
	testb	$24, %al
	je	.L1634
.L1671:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1634
	movq	%r8, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1634:
	movq	-112(%rbp), %rdi
	call	_ZN2v88internal3Map12GetVisitorIdES1_
	cmpl	$255, %eax
	ja	.L1609
	movq	-112(%rbp), %rdx
	movb	%al, 10(%rdx)
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	je	.L1627
.L1665:
	movq	(%r12), %rax
	movq	%rax, -104(%rbp)
	movl	15(%rax), %edx
	andl	$1048576, %edx
	jne	.L1629
	cmpw	$1057, 11(%rax)
	je	.L1628
.L1631:
	movl	-116(%rbp), %eax
	testl	%eax, %eax
	jne	.L1629
	movq	(%r12), %rax
	movq	%r12, %xmm2
	leaq	-96(%rbp), %r15
	movq	%r13, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movq	%r15, %rdi
	movq	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal19TransitionsAccessor10InitializeEv
	movq	%r15, %rdi
	call	_ZN2v88internal19TransitionsAccessor22CanHaveMoreTransitionsEv@PLT
	testb	%al, %al
	jne	.L1627
	.p2align 4,,10
	.p2align 3
.L1629:
	movq	41016(%r13), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L1627
	xorl	%r9d, %r9d
	testq	%rbx, %rbx
	je	.L1633
	movq	-128(%rbp), %rax
	movq	(%rax), %r9
.L1633:
	movq	(%r14), %rcx
	movq	(%r12), %rdx
	movq	%r13, %rdi
	leaq	.LC43(%rip), %rsi
	movq	-144(%rbp), %r8
	call	_ZN2v88internal6Logger8MapEventEPKcNS0_3MapES4_S3_NS0_10HeapObjectE@PLT
	jmp	.L1627
	.p2align 4,,10
	.p2align 3
.L1663:
	testb	$8, 11(%rax)
	je	.L1601
	movq	(%r14), %rdx
	movl	15(%rdx), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rdx)
	movq	%rbx, -128(%rbp)
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1670:
	movq	%r8, %rdx
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %rdi
	movq	8(%r15), %rax
	movq	-136(%rbp), %r8
	leaq	47(%rdi), %rsi
	testb	$24, %al
	jne	.L1671
	jmp	.L1634
	.p2align 4,,10
	.p2align 3
.L1637:
	movq	$0, -128(%rbp)
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1609:
	leaq	.LC38(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1669:
	movq	%r8, %rdx
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rdi
	movq	8(%r15), %rax
	movq	-136(%rbp), %r8
	leaq	47(%rdi), %rsi
	jmp	.L1622
	.p2align 4,,10
	.p2align 3
.L1667:
	movl	$3, -64(%rbp)
	jmp	.L1613
	.p2align 4,,10
	.p2align 3
.L1628:
	movl	15(%rax), %eax
	testl	$1047552, %eax
	je	.L1631
	leaq	-104(%rbp), %rdi
	call	_ZNK2v88internal3Map14GetBackPointerEPNS0_7IsolateE.constprop.0
	cmpq	88(%r13), %rax
	jne	.L1631
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1668:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L1672
	movl	$4, -64(%rbp)
	jmp	.L1613
.L1672:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -64(%rbp)
	jmp	.L1613
.L1666:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18760:
	.size	_ZN2v88internal3Map22CopyReplaceDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEENS0_14TransitionFlagENS0_11MaybeHandleINS0_4NameEEEPKcNS0_20SimpleTransitionFlagE, .-_ZN2v88internal3Map22CopyReplaceDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEENS0_14TransitionFlagENS0_11MaybeHandleINS0_4NameEEEPKcNS0_20SimpleTransitionFlagE
	.section	.text._ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc
	.type	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc, @function
_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc:
.LFB18766:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$16, %rsp
	movq	(%rsi), %rax
	movq	41112(%rdi), %rdi
	movq	39(%rax), %r14
	testq	%rdi, %rdi
	je	.L1674
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1675:
	movq	0(%r13), %rax
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	15(%rax), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	call	_ZN2v88internal15DescriptorArray8CopyUpToEPNS0_7IsolateENS0_6HandleIS1_EEii@PLT
	movq	%rax, %r14
	movq	0(%r13), %rax
	movq	47(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1677
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L1678:
	pushq	$2
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	%rbx
	xorl	%r9d, %r9d
	movl	$1, %r8d
	call	_ZN2v88internal3Map22CopyReplaceDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEENS0_14TransitionFlagENS0_11MaybeHandleINS0_4NameEEEPKcNS0_20SimpleTransitionFlagE
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1674:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1681
.L1676:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L1675
	.p2align 4,,10
	.p2align 3
.L1677:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L1682
.L1679:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L1678
	.p2align 4,,10
	.p2align 3
.L1681:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1676
	.p2align 4,,10
	.p2align 3
.L1682:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1679
	.cfi_endproc
.LFE18766:
	.size	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc, .-_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc
	.section	.rodata._ZN2v88internal3Map6CreateEPNS0_7IsolateEi.str1.1,"aMS",@progbits,1
.LC44:
	.string	"MapCreate"
.LC45:
	.string	"IsJSObjectMap()"
	.section	.text._ZN2v88internal3Map6CreateEPNS0_7IsolateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map6CreateEPNS0_7IsolateEi
	.type	_ZN2v88internal3Map6CreateEPNS0_7IsolateEi, @function
_ZN2v88internal3Map6CreateEPNS0_7IsolateEi:
.LFB18770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1684
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1685:
	movq	41112(%r12), %rdi
	movq	55(%rsi), %r13
	testq	%rdi, %rdi
	je	.L1687
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1688:
	leaq	.LC44(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc
	cmpl	$252, %ebx
	movq	%rax, %r12
	movl	$252, %eax
	cmovle	%ebx, %eax
	leal	24(,%rax,8), %ecx
	movl	%ecx, %edx
	sarl	$3, %edx
	cmpl	$2040, %ecx
	ja	.L1699
	movq	(%r12), %rcx
	movb	%dl, 7(%rcx)
	movq	(%r12), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1700
	movb	$3, 8(%rdx)
	movq	(%r12), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L1701
	testl	%ebx, %ebx
	js	.L1702
	movzbl	7(%rcx), %edx
	movzbl	8(%rcx), %edi
	movzbl	8(%rcx), %esi
	subl	%edi, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	movzbl	%sil, %edx
	addl	%edx, %eax
	cmpl	$255, %eax
	ja	.L1703
	movb	%al, 9(%rcx)
.L1694:
	movq	(%r12), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal3Map12GetVisitorIdES1_
	cmpl	$255, %eax
	ja	.L1704
	movb	%al, 10(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1701:
	.cfi_restore_state
	testl	%ebx, %ebx
	jne	.L1705
	movb	$0, 9(%rcx)
	jmp	.L1694
	.p2align 4,,10
	.p2align 3
.L1687:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1706
.L1689:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L1684:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1707
.L1686:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1685
	.p2align 4,,10
	.p2align 3
.L1699:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1707:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L1686
	.p2align 4,,10
	.p2align 3
.L1706:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1700:
	leaq	.LC45(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1704:
	leaq	.LC38(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1705:
	leaq	.LC39(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1702:
	leaq	.LC40(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1703:
	leaq	.LC37(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18770:
	.size	_ZN2v88internal3Map6CreateEPNS0_7IsolateEi, .-_ZN2v88internal3Map6CreateEPNS0_7IsolateEi
	.section	.rodata._ZN2v88internal3Map20TransitionElementsToEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE.str1.1,"aMS",@progbits,1
.LC46:
	.string	"CopyAsElementsKind"
	.section	.rodata._ZN2v88internal3Map20TransitionElementsToEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE.str1.8,"aMS",@progbits,1
	.align 8
.LC47:
	.string	"static_cast<int>(elements_kind) < kElementsKindCount"
	.section	.text._ZN2v88internal3Map20TransitionElementsToEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map20TransitionElementsToEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE
	.type	_ZN2v88internal3Map20TransitionElementsToEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE, @function
_ZN2v88internal3Map20TransitionElementsToEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE:
.LFB18743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movzbl	14(%rax), %r9d
	shrl	$3, %r9d
	cmpb	%r9b, %dl
	jne	.L1709
	movq	%rsi, %rax
.L1710:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1752
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1709:
	.cfi_restore_state
	movl	%edx, %r13d
	movq	12464(%rdi), %rdx
	movq	%rdi, %rbx
	movq	39(%rdx), %rdx
	cmpb	$13, %r9b
	je	.L1753
	cmpb	$14, %r9b
	je	.L1754
	cmpb	$5, %r9b
	ja	.L1712
	movzbl	%r13b, %r14d
	cmpb	$5, %r13b
	jbe	.L1755
.L1720:
	testb	$1, %r9b
	je	.L1736
	cmpb	$1, %r9b
	je	.L1742
	cmpb	$5, %r9b
	je	.L1743
	movl	$2, %edx
	cmpb	$3, %r9b
	je	.L1724
.L1736:
	cmpb	$5, %r13b
	ja	.L1728
	cmpb	$3, %r9b
	je	.L1735
	movl	%r14d, %esi
	movl	%r9d, %edi
	call	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
	testb	%al, %al
	jne	.L1728
	jmp	.L1735
	.p2align 4,,10
	.p2align 3
.L1753:
	movq	383(%rdx), %rax
	cmpq	%rax, (%rsi)
	je	.L1756
.L1712:
	leal	-17(%r9), %eax
	cmpb	$10, %al
	jbe	.L1739
	andl	$-3, %r9d
	cmpb	$13, %r9b
	je	.L1739
.L1735:
	leaq	.LC46(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc
	movq	(%rax), %rcx
	cmpb	$27, %r13b
	ja	.L1757
	movzbl	14(%rcx), %edx
	movzbl	%r13b, %r13d
	sall	$3, %r13d
	andb	$7, %dl
	orl	%r13d, %edx
	movb	%dl, 14(%rcx)
	jmp	.L1710
	.p2align 4,,10
	.p2align 3
.L1739:
	movzbl	%r13b, %r14d
	cmpb	$5, %r13b
	jbe	.L1735
.L1728:
	leaq	-192(%rbp), %r13
	movq	%rbx, %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal10MapUpdaterC1EPNS0_7IsolateENS0_6HandleINS0_3MapEEE@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal10MapUpdater23ReconfigureElementsKindENS0_12ElementsKindE@PLT
	jmp	.L1710
	.p2align 4,,10
	.p2align 3
.L1754:
	movq	1199(%rdx), %rax
	cmpq	%rax, (%rsi)
	jne	.L1712
	movq	383(%rdx), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L1749
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1750
.L1719:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1710
	.p2align 4,,10
	.p2align 3
.L1755:
	leal	664(,%r9,8), %ecx
	movslq	%ecx, %rcx
	movq	-1(%rdx,%rcx), %rcx
	cmpq	%rcx, %rax
	jne	.L1720
	leal	664(,%r14,8), %ecx
	movslq	%ecx, %rcx
	movq	-1(%rdx,%rcx), %r15
	testb	$1, %r15b
	je	.L1720
	movq	-1(%r15), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L1720
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1721
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L1710
	.p2align 4,,10
	.p2align 3
.L1756:
	movq	1199(%rdx), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1713
.L1749:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L1710
	.p2align 4,,10
	.p2align 3
.L1743:
	movl	$4, %edx
.L1724:
	cmpb	%dl, %r13b
	jne	.L1736
	leaq	-200(%rbp), %rdi
	movq	%rax, -200(%rbp)
	call	_ZNK2v88internal3Map14GetBackPointerEPNS0_7IsolateE.constprop.0
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L1736
	movq	(%r12), %rax
	leaq	-192(%rbp), %rdi
	movq	%rax, -192(%rbp)
	call	_ZNK2v88internal3Map14GetBackPointerEPNS0_7IsolateE.constprop.0
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpb	%al, %r13b
	jne	.L1736
	movq	(%r12), %rax
	movq	%rax, -192(%rbp)
	call	_ZNK2v88internal3Map14GetBackPointerEPNS0_7IsolateE.constprop.0
	movq	41112(%rbx), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L1729
	movq	%rax, %rsi
	jmp	.L1749
	.p2align 4,,10
	.p2align 3
.L1742:
	xorl	%edx, %edx
	jmp	.L1724
	.p2align 4,,10
	.p2align 3
.L1757:
	leaq	.LC47(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1713:
	movq	41088(%rbx), %rax
	cmpq	%rax, 41096(%rbx)
	jne	.L1719
.L1750:
	movq	%rbx, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	jmp	.L1719
.L1721:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1758
.L1723:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r15, (%rax)
	jmp	.L1710
.L1752:
	call	__stack_chk_fail@PLT
.L1729:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1759
.L1731:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r12, (%rax)
	jmp	.L1710
.L1758:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1723
.L1759:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1731
	.cfi_endproc
.LFE18743:
	.size	_ZN2v88internal3Map20TransitionElementsToEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE, .-_ZN2v88internal3Map20TransitionElementsToEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE
	.section	.text._ZN2v88internal3Map24CopyForPreventExtensionsEPNS0_7IsolateENS0_6HandleIS1_EENS0_18PropertyAttributesENS4_INS0_6SymbolEEEPKcb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map24CopyForPreventExtensionsEPNS0_7IsolateENS0_6HandleIS1_EENS0_18PropertyAttributesENS4_INS0_6SymbolEEEPKcb
	.type	_ZN2v88internal3Map24CopyForPreventExtensionsEPNS0_7IsolateENS0_6HandleIS1_EENS0_18PropertyAttributesENS4_INS0_6SymbolEEEPKcb, @function
_ZN2v88internal3Map24CopyForPreventExtensionsEPNS0_7IsolateENS0_6HandleIS1_EENS0_18PropertyAttributesENS4_INS0_6SymbolEEEPKcb:
.LFB18771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rax
	movl	15(%rax), %r13d
	movq	41112(%rdi), %rdi
	movq	39(%rax), %r8
	shrl	$10, %r13d
	andl	$1023, %r13d
	testq	%rdi, %rdi
	je	.L1761
	movq	%r8, %rsi
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-56(%rbp), %r9d
	movq	%rax, %rsi
.L1762:
	movl	%r13d, %edx
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movl	%r9d, %ecx
	call	_ZN2v88internal15DescriptorArray21CopyUpToAddAttributesEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_18PropertyAttributesEi@PLT
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	47(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1764
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L1765:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r14, %r9
	movq	40936(%r12), %rax
	movl	8(%rax), %eax
	pushq	$2
	pushq	%r15
	testl	%eax, %eax
	setne	%r8b
	movzbl	%r8b, %r8d
	call	_ZN2v88internal3Map22CopyReplaceDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEENS0_14TransitionFlagENS0_11MaybeHandleINS0_4NameEEEPKcNS0_20SimpleTransitionFlagE
	movq	(%rax), %rcx
	movl	15(%rcx), %edx
	andl	$-134217729, %edx
	movl	%edx, 15(%rcx)
	movq	(%rbx), %rdx
	popq	%rsi
	popq	%rdi
	movzbl	14(%rdx), %edx
	shrl	$3, %edx
	leal	-17(%rdx), %ecx
	cmpb	$10, %cl
	jbe	.L1767
	movq	(%rax), %rsi
	subl	$15, %edx
	cmpb	$2, %dl
	sbbl	%ecx, %ecx
	movzbl	14(%rsi), %edx
	andl	$32, %ecx
	addl	$96, %ecx
	andb	$7, %dl
	orl	%ecx, %edx
	movb	%dl, 14(%rsi)
.L1767:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1761:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1771
.L1763:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1762
	.p2align 4,,10
	.p2align 3
.L1764:
	movq	41088(%r12), %rcx
	cmpq	%rcx, 41096(%r12)
	je	.L1772
.L1766:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L1765
	.p2align 4,,10
	.p2align 3
.L1771:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	movl	%edx, -60(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-60(%rbp), %r9d
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1763
	.p2align 4,,10
	.p2align 3
.L1772:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1766
	.cfi_endproc
.LFE18771:
	.size	_ZN2v88internal3Map24CopyForPreventExtensionsEPNS0_7IsolateENS0_6HandleIS1_EENS0_18PropertyAttributesENS4_INS0_6SymbolEEEPKcb, .-_ZN2v88internal3Map24CopyForPreventExtensionsEPNS0_7IsolateENS0_6HandleIS1_EENS0_18PropertyAttributesENS4_INS0_6SymbolEEEPKcb
	.section	.rodata._ZN2v88internal3Map17CopyAddDescriptorEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_10DescriptorENS0_14TransitionFlagE.str1.1,"aMS",@progbits,1
.LC48:
	.string	"CopyAddDescriptor"
	.section	.text._ZN2v88internal3Map17CopyAddDescriptorEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_10DescriptorENS0_14TransitionFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map17CopyAddDescriptorEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_10DescriptorENS0_14TransitionFlagE
	.type	_ZN2v88internal3Map17CopyAddDescriptorEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_10DescriptorENS0_14TransitionFlagE, @function
_ZN2v88internal3Map17CopyAddDescriptorEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_10DescriptorENS0_14TransitionFlagE:
.LFB18778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	39(%rax), %r8
	testq	%rdi, %rdi
	je	.L1774
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1775:
	movq	(%r14), %rax
	leaq	15(%rax), %rcx
	movq	%rcx, %rdx
	testl	%r15d, %r15d
	jne	.L1798
	movl	15(%rax), %edi
	andl	$4194304, %edi
	jne	.L1778
.L1798:
	leaq	-96(%rbp), %r8
.L1777:
	movq	%r8, -112(%rbp)
	movl	(%rdx), %ebx
	movl	$1, %ecx
	movq	%r13, %rdi
	shrl	$10, %ebx
	andl	$1023, %ebx
	movl	%ebx, %edx
	call	_ZN2v88internal15DescriptorArray8CopyUpToEPNS0_7IsolateENS0_6HandleIS1_EEii@PLT
	movq	-112(%rbp), %r8
	movq	%r12, %rsi
	movq	%rax, %rdx
	movq	(%rax), %rax
	movq	%r8, %rdi
	movq	%rdx, -104(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	movq	-104(%rbp), %rdx
	leal	1(%rbx), %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi@PLT
	pushq	$0
	movq	-104(%rbp), %rdx
	movl	%r15d, %r8d
	movq	%rax, %rcx
	leaq	.LC48(%rip), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	pushq	%rax
	movq	(%r12), %r9
	call	_ZN2v88internal3Map22CopyReplaceDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEENS0_14TransitionFlagENS0_11MaybeHandleINS0_4NameEEEPKcNS0_20SimpleTransitionFlagE
	popq	%rdx
	popq	%rcx
.L1792:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1799
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1774:
	.cfi_restore_state
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L1800
.L1776:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r8, (%rsi)
	jmp	.L1775
	.p2align 4,,10
	.p2align 3
.L1778:
	movq	%rax, %rdi
	movq	31(%rax), %rdx
	andq	$-262144, %rdi
	movq	24(%rdi), %rdi
	subq	$37592, %rdi
	testb	$1, %dl
	je	.L1779
	movq	-1(%rdx), %r8
	cmpq	%r8, 136(%rdi)
	je	.L1781
.L1779:
	movq	88(%rdi), %rdx
.L1781:
	cmpq	%rdx, 88(%r13)
	jne	.L1782
	movq	%rcx, %rdx
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L1800:
	movq	%r13, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1776
	.p2align 4,,10
	.p2align 3
.L1782:
	movq	%r13, %xmm0
	movq	%r14, %xmm1
	movq	$0, -72(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -72(%rbp)
	testb	$1, %al
	je	.L1783
	cmpl	$3, %eax
	je	.L1783
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L1801
	cmpq	$1, %rdx
	je	.L1802
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1783:
	movl	$1, -64(%rbp)
.L1785:
	leaq	-96(%rbp), %r8
	movq	%rsi, -104(%rbp)
	movq	%r8, %rdi
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal19TransitionsAccessor22CanHaveMoreTransitionsEv@PLT
	movq	-104(%rbp), %rsi
	testb	%al, %al
	jne	.L1791
	movq	(%r14), %rax
	movq	-112(%rbp), %r8
	leaq	15(%rax), %rdx
	jmp	.L1777
	.p2align 4,,10
	.p2align 3
.L1791:
	movq	%rsi, %rdx
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Map15ShareDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEEPNS0_10DescriptorE
	jmp	.L1792
.L1801:
	movl	$3, -64(%rbp)
	jmp	.L1785
.L1802:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L1803
	movl	$4, -64(%rbp)
	jmp	.L1785
.L1803:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -64(%rbp)
	jmp	.L1785
.L1799:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18778:
	.size	_ZN2v88internal3Map17CopyAddDescriptorEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_10DescriptorENS0_14TransitionFlagE, .-_ZN2v88internal3Map17CopyAddDescriptorEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_10DescriptorENS0_14TransitionFlagE
	.section	.text._ZN2v88internal3Map13CopyWithFieldEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_9FieldTypeEEENS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_14TransitionFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map13CopyWithFieldEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_9FieldTypeEEENS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_14TransitionFlagE
	.type	_ZN2v88internal3Map13CopyWithFieldEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_9FieldTypeEEENS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_14TransitionFlagE, @function
_ZN2v88internal3Map13CopyWithFieldEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_9FieldTypeEEENS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_14TransitionFlagE:
.LFB18687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	15(%rax), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	cmpl	$1019, %edx
	jle	.L1805
	xorl	%eax, %eax
.L1806:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1818
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1805:
	.cfi_restore_state
	leaq	-96(%rbp), %r15
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movl	%r8d, %r14d
	movq	%r15, %rdi
	movl	%r9d, -120(%rbp)
	movq	%rcx, %r11
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal3Map21NextFreePropertyIndexEv
	movl	-120(%rbp), %r8d
	movl	%eax, -116(%rbp)
	movq	(%r12), %rax
	movzwl	11(%rax), %eax
	cmpw	$1065, %ax
	je	.L1819
	subw	$1041, %ax
	cmpw	$20, %ax
	ja	.L1808
	movl	$1179649, %edx
	btq	%rax, %rdx
	jnc	.L1808
	movq	%rbx, %rdi
	movl	%r8d, -120(%rbp)
	call	_ZN2v88internal9FieldType3AnyEPNS0_7IsolateE@PLT
	movb	$4, 16(%rbp)
	movl	-120(%rbp), %r8d
	movq	%rax, %r11
.L1808:
	movq	%r11, %rsi
	movq	%rbx, %rdi
	movl	%r8d, -120(%rbp)
	call	_ZN2v88internal3Map13WrapFieldTypeEPNS0_7IsolateENS0_6HandleINS0_9FieldTypeEEE
	subq	$8, %rsp
	movl	-120(%rbp), %r8d
	movl	%r14d, %ecx
	movl	%eax, -112(%rbp)
	leaq	-112(%rbp), %rax
	movzbl	16(%rbp), %r9d
	movq	%r13, %rsi
	pushq	%rax
	movq	%r15, %rdi
	movq	%rdx, -104(%rbp)
	movl	-116(%rbp), %edx
	call	_ZN2v88internal10Descriptor9DataFieldENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE@PLT
	movl	24(%rbp), %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal3Map17CopyAddDescriptorEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_10DescriptorENS0_14TransitionFlagE
	movq	(%rax), %rcx
	movzbl	9(%rcx), %edx
	popq	%rsi
	popq	%rdi
	cmpl	$2, %edx
	jle	.L1809
	movzbl	7(%rcx), %esi
	cmpl	%esi, %edx
	je	.L1820
	leal	1(%rdx), %esi
	cmpl	$255, %edx
	je	.L1821
	movb	%sil, 9(%rcx)
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1809:
	leal	-1(%rdx), %esi
	testl	%edx, %edx
	movl	$2, %edx
	cmovne	%esi, %edx
	movb	%dl, 9(%rcx)
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1819:
	movq	%rbx, %rdi
	movb	$4, 16(%rbp)
	call	_ZN2v88internal9FieldType3AnyEPNS0_7IsolateE@PLT
	xorl	%r8d, %r8d
	movq	%rax, %r11
	jmp	.L1808
	.p2align 4,,10
	.p2align 3
.L1820:
	movb	$2, 9(%rcx)
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1821:
	leaq	.LC37(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1818:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18687:
	.size	_ZN2v88internal3Map13CopyWithFieldEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_9FieldTypeEEENS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_14TransitionFlagE, .-_ZN2v88internal3Map13CopyWithFieldEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_9FieldTypeEEENS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_14TransitionFlagE
	.section	.text._ZN2v88internal3Map16CopyWithConstantEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesENS0_14TransitionFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map16CopyWithConstantEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesENS0_14TransitionFlagE
	.type	_ZN2v88internal3Map16CopyWithConstantEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesENS0_14TransitionFlagE, @function
_ZN2v88internal3Map16CopyWithConstantEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesENS0_14TransitionFlagE:
.LFB18691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	$1019, %eax
	jle	.L1823
	xorl	%eax, %eax
.L1824:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1836
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1823:
	.cfi_restore_state
	cmpb	$0, _ZN2v88internal17FLAG_track_fieldsE(%rip)
	movq	%rdx, %r14
	movq	(%rcx), %rax
	movq	%rdi, %r13
	movq	%rsi, %r12
	movl	%r8d, %ebx
	movl	%r9d, %r15d
	movl	$4, %edx
	je	.L1825
	movl	$1, %edx
	testb	$1, %al
	je	.L1825
	cmpb	$0, _ZN2v88internal24FLAG_track_double_fieldsE(%rip)
	jne	.L1837
.L1826:
	cmpb	$0, _ZN2v88internal26FLAG_track_computed_fieldsE(%rip)
	jne	.L1828
.L1829:
	cmpb	$0, _ZN2v88internal29FLAG_track_heap_object_fieldsE(%rip)
	sete	%dl
	addl	$3, %edx
	.p2align 4,,10
	.p2align 3
.L1825:
	leaq	-64(%rbp), %rdi
	movq	%r13, %rsi
	movb	%dl, -65(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object11OptimalTypeEPNS0_7IsolateENS0_14RepresentationE@PLT
	movzbl	-65(%rbp), %edx
	pushq	%r15
	movl	%ebx, %r8d
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	%r12, %rsi
	movq	%r13, %rdi
	pushq	%rdx
	movq	%r14, %rdx
	call	_ZN2v88internal3Map13CopyWithFieldEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_9FieldTypeEEENS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_14TransitionFlagE
	popq	%rdx
	popq	%rcx
	jmp	.L1824
	.p2align 4,,10
	.p2align 3
.L1828:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37512(%rdx), %rax
	jne	.L1829
	xorl	%edx, %edx
	jmp	.L1825
	.p2align 4,,10
	.p2align 3
.L1837:
	movq	-1(%rax), %rcx
	movl	$2, %edx
	cmpw	$65, 11(%rcx)
	jne	.L1826
	jmp	.L1825
.L1836:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18691:
	.size	_ZN2v88internal3Map16CopyWithConstantEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesENS0_14TransitionFlagE, .-_ZN2v88internal3Map16CopyWithConstantEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesENS0_14TransitionFlagE
	.section	.rodata._ZN2v88internal3Map21CopyReplaceDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEEPNS0_10DescriptorEiNS0_14TransitionFlagE.str1.1,"aMS",@progbits,1
.LC49:
	.string	"CopyReplaceDescriptor"
	.section	.text._ZN2v88internal3Map21CopyReplaceDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEEPNS0_10DescriptorEiNS0_14TransitionFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map21CopyReplaceDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEEPNS0_10DescriptorEiNS0_14TransitionFlagE
	.type	_ZN2v88internal3Map21CopyReplaceDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEEPNS0_10DescriptorEiNS0_14TransitionFlagE, @function
_ZN2v88internal3Map21CopyReplaceDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEEPNS0_10DescriptorEiNS0_14TransitionFlagE:
.LFB18780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$56, %rsp
	movl	%r9d, -84(%rbp)
	movq	(%rcx), %r9
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%r9, -72(%rbp)
	movq	%r15, %rsi
	movq	%rcx, -80(%rbp)
	xorl	%ecx, %ecx
	movl	15(%rax), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	call	_ZN2v88internal15DescriptorArray8CopyUpToEPNS0_7IsolateENS0_6HandleIS1_EEii@PLT
	movq	-80(%rbp), %r10
	movl	%ebx, %esi
	leaq	-64(%rbp), %rdi
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	%r10, %rdx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal15DescriptorArray7ReplaceEiPNS0_10DescriptorE@PLT
	movq	(%r12), %rax
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movswl	9(%rax), %ecx
	call	_ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	(%r15), %rax
	movswl	9(%rax), %edx
	xorl	%eax, %eax
	movq	-72(%rbp), %r9
	movl	-84(%rbp), %r8d
	subl	$1, %edx
	cmpl	%ebx, %edx
	movq	%r12, %rdx
	setne	%al
	pushq	%rax
	leaq	.LC49(%rip), %rax
	pushq	%rax
	call	_ZN2v88internal3Map22CopyReplaceDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEENS0_14TransitionFlagENS0_11MaybeHandleINS0_4NameEEEPKcNS0_20SimpleTransitionFlagE
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1841
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1841:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18780:
	.size	_ZN2v88internal3Map21CopyReplaceDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEEPNS0_10DescriptorEiNS0_14TransitionFlagE, .-_ZN2v88internal3Map21CopyReplaceDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEEPNS0_10DescriptorEiNS0_14TransitionFlagE
	.section	.rodata._ZN2v88internal3Map26TransitionToImmutableProtoEPNS0_7IsolateENS0_6HandleIS1_EE.str1.1,"aMS",@progbits,1
.LC50:
	.string	"ImmutablePrototype"
	.section	.text._ZN2v88internal3Map26TransitionToImmutableProtoEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map26TransitionToImmutableProtoEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal3Map26TransitionToImmutableProtoEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal3Map26TransitionToImmutableProtoEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	(%rsi), %rax
	movq	41112(%rdi), %rdi
	movq	39(%rax), %r14
	testq	%rdi, %rdi
	je	.L1843
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1844:
	movq	0(%r13), %rax
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	15(%rax), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	call	_ZN2v88internal15DescriptorArray8CopyUpToEPNS0_7IsolateENS0_6HandleIS1_EEii@PLT
	movq	%rax, %r14
	movq	0(%r13), %rax
	movq	47(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1846
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L1847:
	leaq	.LC50(%rip), %rax
	pushq	$2
	movq	%r14, %rdx
	movq	%r13, %rsi
	pushq	%rax
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movl	$1, %r8d
	call	_ZN2v88internal3Map22CopyReplaceDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEENS0_14TransitionFlagENS0_11MaybeHandleINS0_4NameEEEPKcNS0_20SimpleTransitionFlagE
	movq	(%rax), %rdx
	orb	$2, 14(%rdx)
	leaq	-24(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1843:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1850
.L1845:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L1846:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L1851
.L1848:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L1847
	.p2align 4,,10
	.p2align 3
.L1850:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L1851:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1848
	.cfi_endproc
.LFE18753:
	.size	_ZN2v88internal3Map26TransitionToImmutableProtoEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal3Map26TransitionToImmutableProtoEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal3Map25CopyForElementsTransitionEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map25CopyForElementsTransitionEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal3Map25CopyForElementsTransitionEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal3Map25CopyForElementsTransitionEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal3Map19CopyDropDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EE
	movq	%rax, %r13
	movq	(%rbx), %rax
	movl	15(%rax), %edx
	andl	$4194304, %edx
	jne	.L1885
	movq	41112(%r12), %rdi
	movq	39(%rax), %r14
	testq	%rdi, %rdi
	je	.L1859
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1860:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	15(%rax), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	call	_ZN2v88internal15DescriptorArray8CopyUpToEPNS0_7IsolateENS0_6HandleIS1_EEii@PLT
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	47(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1862
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1863:
	movq	0(%r13), %rdx
	leaq	-64(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdx, -64(%rbp)
	movq	(%r15), %rdx
	movq	(%rax), %r14
	movswl	9(%rdx), %ecx
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi
	movq	-64(%rbp), %rax
	movq	%r14, 47(%rax)
	testb	$1, %r14b
	je	.L1870
	movq	%r14, %rbx
	movq	-64(%rbp), %rdi
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	leaq	47(%rdi), %rsi
	testl	$262144, %eax
	je	.L1866
	movq	%r14, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdi
	movq	8(%rbx), %rax
	leaq	47(%rdi), %rsi
.L1866:
	testb	$24, %al
	je	.L1870
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1886
	.p2align 4,,10
	.p2align 3
.L1870:
	movq	-64(%rbp), %rdi
	call	_ZN2v88internal3Map12GetVisitorIdES1_
	cmpl	$255, %eax
	ja	.L1868
	movq	-64(%rbp), %rdx
	movb	%al, 10(%rdx)
.L1858:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1887
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1859:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1888
.L1861:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L1885:
	movq	0(%r13), %rdx
	leaq	-72(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdx, -72(%rbp)
	movq	47(%rax), %r14
	movq	(%rbx), %rax
	movq	39(%rax), %rdx
	movswl	9(%rdx), %ecx
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi
	movq	-72(%rbp), %rax
	movq	%r14, 47(%rax)
	testb	$1, %r14b
	je	.L1869
	movq	%r14, %rbx
	movq	-72(%rbp), %rdi
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	leaq	47(%rdi), %rsi
	testl	$262144, %eax
	jne	.L1889
	testb	$24, %al
	je	.L1869
.L1891:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1869
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1869:
	movq	-72(%rbp), %rdi
	call	_ZN2v88internal3Map12GetVisitorIdES1_
	cmpl	$255, %eax
	ja	.L1868
	movq	-72(%rbp), %rdx
	movb	%al, 10(%rdx)
	jmp	.L1858
	.p2align 4,,10
	.p2align 3
.L1862:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1890
.L1864:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1863
	.p2align 4,,10
	.p2align 3
.L1889:
	movq	%r14, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rdi
	movq	8(%rbx), %rax
	leaq	47(%rdi), %rsi
	testb	$24, %al
	jne	.L1891
	jmp	.L1869
	.p2align 4,,10
	.p2align 3
.L1886:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1870
	.p2align 4,,10
	.p2align 3
.L1868:
	leaq	.LC38(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1888:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1861
	.p2align 4,,10
	.p2align 3
.L1890:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L1864
.L1887:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18765:
	.size	_ZN2v88internal3Map25CopyForElementsTransitionEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal3Map25CopyForElementsTransitionEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal3Map18CopyAsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_14TransitionFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map18CopyAsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_14TransitionFlagE
	.type	_ZN2v88internal3Map18CopyAsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_14TransitionFlagE, @function
_ZN2v88internal3Map18CopyAsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_14TransitionFlagE:
.LFB18763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.L1920
.L1893:
	movq	%r14, %rsi
	leaq	.LC46(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc
	movq	(%rax), %rsi
	cmpb	$27, %bl
	ja	.L1910
	movzbl	14(%rsi), %ecx
	movzbl	%bl, %edx
	leal	0(,%rdx,8), %ebx
	andb	$7, %cl
	orl	%ecx, %ebx
	movb	%bl, 14(%rsi)
.L1909:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1921
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1920:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	$0, -56(%rbp)
	movq	%rdi, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -56(%rbp)
	testb	$1, %al
	je	.L1894
	cmpl	$3, %eax
	je	.L1894
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L1922
	cmpq	$1, %rdx
	je	.L1923
.L1898:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1894:
	movl	$1, -48(%rbp)
.L1896:
	movq	3672(%r13), %rsi
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE@PLT
	movq	%r13, %xmm0
	movq	%r14, %xmm1
	movq	$0, -104(%rbp)
	movq	%rax, %r12
	movq	(%r14), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rax, -112(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -104(%rbp)
	testb	$1, %al
	je	.L1902
	cmpl	$3, %eax
	je	.L1902
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L1924
	cmpq	$1, %rdx
	jne	.L1898
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L1906
	movl	$4, -96(%rbp)
	jmp	.L1904
	.p2align 4,,10
	.p2align 3
.L1902:
	movl	$1, -96(%rbp)
.L1904:
	leaq	-128(%rbp), %rdi
	call	_ZN2v88internal19TransitionsAccessor22CanHaveMoreTransitionsEv@PLT
	testq	%r12, %r12
	jne	.L1893
	testb	%al, %al
	je	.L1893
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Map25CopyForElementsTransitionEPNS0_7IsolateENS0_6HandleIS1_EE
	movq	(%rax), %rdx
	movq	%rax, %r12
	cmpb	$27, %bl
	ja	.L1910
	movzbl	14(%rdx), %eax
	movzbl	%bl, %ebx
	movl	$2, %r8d
	movq	%r14, %rsi
	sall	$3, %ebx
	leaq	3672(%r13), %rcx
	movq	%r13, %rdi
	andb	$7, %al
	orl	%eax, %ebx
	movb	%bl, 14(%rdx)
	movq	%r12, %rdx
	call	_ZN2v88internal3Map17ConnectTransitionEPNS0_7IsolateENS0_6HandleIS1_EES5_NS4_INS0_4NameEEENS0_20SimpleTransitionFlagE
	movq	%r12, %rax
	jmp	.L1909
	.p2align 4,,10
	.p2align 3
.L1910:
	leaq	.LC47(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1924:
	movl	$3, -96(%rbp)
	jmp	.L1904
	.p2align 4,,10
	.p2align 3
.L1922:
	movl	$3, -48(%rbp)
	jmp	.L1896
	.p2align 4,,10
	.p2align 3
.L1923:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L1925
	movl	$4, -48(%rbp)
	jmp	.L1896
	.p2align 4,,10
	.p2align 3
.L1906:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -96(%rbp)
	jmp	.L1904
.L1925:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -48(%rbp)
	jmp	.L1896
.L1921:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18763:
	.size	_ZN2v88internal3Map18CopyAsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_14TransitionFlagE, .-_ZN2v88internal3Map18CopyAsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_14TransitionFlagE
	.section	.text._ZN2v88internal3Map14AsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map14AsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE
	.type	_ZN2v88internal3Map14AsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE, @function
_ZN2v88internal3Map14AsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE:
.LFB18745:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$72, %rsp
	movq	(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	14(%r12), %eax
	shrl	$3, %eax
	cmpb	%al, %dl
	je	.L1950
	leaq	-96(%rbp), %r15
.L1937:
	movq	$0, -72(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r12, -80(%rbp)
	movq	71(%r12), %rax
	movq	%rax, -72(%rbp)
	testb	$1, %al
	je	.L1928
	cmpl	$3, %eax
	je	.L1928
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L1965
	cmpq	$1, %rdx
	je	.L1966
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1928:
	movl	$1, -64(%rbp)
.L1930:
	movq	3672(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1936
	movzbl	14(%rax), %edx
	shrl	$3, %edx
	cmpb	%dl, %bl
	je	.L1927
	movq	%rax, %r12
	jmp	.L1937
	.p2align 4,,10
	.p2align 3
.L1965:
	movl	$3, -64(%rbp)
	jmp	.L1930
	.p2align 4,,10
	.p2align 3
.L1950:
	movq	%r12, %rcx
.L1927:
	movq	%rcx, %r12
.L1936:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1938
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	movq	%rax, %r15
.L1939:
	movzbl	14(%r12), %ecx
	shrl	$3, %ecx
	movl	%ecx, %r9d
	cmpb	%cl, %r14b
	jne	.L1967
.L1942:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1968
	addq	$72, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1967:
	.cfi_restore_state
	movq	%r12, -96(%rbp)
	movl	15(%r12), %eax
	andl	$1048576, %eax
	movl	%eax, %r8d
	jne	.L1964
	cmpw	$1057, 11(%r12)
	je	.L1969
.L1945:
	cmpb	$5, %cl
	ja	.L1944
.L1947:
	cmpb	$3, %r9b
	je	.L1946
	leal	-17(%r9), %eax
	cmpb	$10, %al
	jbe	.L1946
	movzbl	%r9b, %edi
	movl	%r8d, -104(%rbp)
	call	_ZN2v88internal29GetNextTransitionElementsKindENS0_12ElementsKindE@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movzbl	%al, %edx
	movb	%dl, -97(%rbp)
	call	_ZN2v88internal3Map18CopyAsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_14TransitionFlagE
	movzbl	-97(%rbp), %r9d
	movl	-104(%rbp), %r8d
	movq	%rax, %r15
	cmpb	%r9b, %bl
	jne	.L1947
	jmp	.L1942
	.p2align 4,,10
	.p2align 3
.L1946:
	cmpb	%r9b, %r14b
	je	.L1942
	.p2align 4,,10
	.p2align 3
.L1944:
	movq	%r15, %rsi
	movzbl	%r14b, %edx
	movl	%r8d, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal3Map18CopyAsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_14TransitionFlagE
	movq	%rax, %r15
	jmp	.L1942
	.p2align 4,,10
	.p2align 3
.L1938:
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L1970
.L1940:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%r12, (%r15)
	jmp	.L1939
	.p2align 4,,10
	.p2align 3
.L1966:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L1971
	movl	$4, -64(%rbp)
	jmp	.L1930
	.p2align 4,,10
	.p2align 3
.L1971:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -64(%rbp)
	jmp	.L1930
	.p2align 4,,10
	.p2align 3
.L1969:
	movl	15(%r12), %eax
	testl	$1047552, %eax
	je	.L1945
	leaq	-96(%rbp), %rdi
	movl	%r8d, -104(%rbp)
	movb	%cl, -97(%rbp)
	call	_ZNK2v88internal3Map14GetBackPointerEPNS0_7IsolateE.constprop.0
	cmpq	88(%r13), %rax
	movzbl	-97(%rbp), %r9d
	movl	-104(%rbp), %r8d
	jne	.L1945
	.p2align 4,,10
	.p2align 3
.L1964:
	movl	$1, %r8d
	jmp	.L1944
	.p2align 4,,10
	.p2align 3
.L1970:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r15
	jmp	.L1940
.L1968:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18745:
	.size	_ZN2v88internal3Map14AsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE, .-_ZN2v88internal3Map14AsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE
	.section	.text._ZN2v88internal3Map21AddMissingTransitionsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map21AddMissingTransitionsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEE
	.type	_ZN2v88internal3Map21AddMissingTransitionsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEE, @function
_ZN2v88internal3Map21AddMissingTransitionsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEE:
.LFB18761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	15(%rax), %eax
	movl	%eax, -72(%rbp)
	movq	(%rdx), %rax
	movzwl	9(%rax), %eax
	movw	%ax, -76(%rbp)
	call	_ZN2v88internal3Map19CopyDropDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EE
	movq	(%rbx), %rdx
	movq	(%r14), %r13
	movq	%r12, %rsi
	movq	%rax, -88(%rbp)
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	leaq	-64(%rbp), %rax
	movswl	9(%rdx), %ecx
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi
	movq	-64(%rbp), %rax
	movq	%r13, 47(%rax)
	testb	$1, %r13b
	je	.L1983
	movq	%r13, %rcx
	movq	-64(%rbp), %rdi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -104(%rbp)
	leaq	47(%rdi), %rsi
	testl	$262144, %eax
	jne	.L1993
	testb	$24, %al
	je	.L1983
.L2000:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1994
.L1983:
	movq	-64(%rbp), %rdi
	call	_ZN2v88internal3Map12GetVisitorIdES1_
	cmpl	$255, %eax
	ja	.L1995
.L1976:
	movq	-64(%rbp), %rdx
	movb	%al, 10(%rdx)
	movq	-88(%rbp), %rax
	movq	(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1996
	movzbl	7(%rdx), %eax
	movzbl	8(%rdx), %esi
	movzbl	8(%rdx), %ecx
	subl	%esi, %eax
	addl	%ecx, %eax
	cmpl	$255, %eax
	ja	.L1997
	movb	%al, 9(%rdx)
.L1978:
	movq	-88(%rbp), %rax
	movl	-72(%rbp), %r13d
	movq	(%rax), %rdx
	shrl	$10, %r13d
	andl	$1023, %r13d
	movl	15(%rdx), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rdx)
	movswl	-76(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -76(%rbp)
	cmpl	%eax, %r13d
	jge	.L1980
	.p2align 4,,10
	.p2align 3
.L1981:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Map19CopyDropDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EE
	movl	%r13d, %ecx
	movq	%r15, %rsi
	movq	%r14, %r9
	movq	%rax, %rdx
	movq	%rbx, %r8
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal3Map18InstallDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EES5_iNS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEE
	movq	-72(%rbp), %rax
	addl	$1, %r13d
	movq	%rax, %r15
	cmpl	-76(%rbp), %r13d
	jne	.L1981
.L1980:
	movq	(%r15), %rax
	movl	15(%rax), %edx
	andl	$33554432, %edx
	je	.L1998
.L1982:
	movq	-88(%rbp), %rsi
	movq	%r14, %r9
	movq	%rbx, %r8
	movq	%r12, %rdi
	movq	(%rsi), %rdx
	movl	15(%rdx), %eax
	andl	$-268435457, %eax
	movl	%eax, 15(%rdx)
	movq	%rsi, %rdx
	movq	%r15, %rsi
	movl	-76(%rbp), %ecx
	call	_ZN2v88internal3Map18InstallDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EES5_iNS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1999
	movq	-88(%rbp), %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1996:
	.cfi_restore_state
	movb	$0, 9(%rdx)
	jmp	.L1978
	.p2align 4,,10
	.p2align 3
.L1993:
	movq	%r13, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rcx
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
	leaq	47(%rdi), %rsi
	testb	$24, %al
	jne	.L2000
	jmp	.L1983
	.p2align 4,,10
	.p2align 3
.L1998:
	movl	15(%rax), %edx
	movq	%r12, %rsi
	orl	$33554432, %edx
	movl	%edx, 15(%rax)
	movl	$1, %edx
	movq	55(%rax), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE@PLT
	jmp	.L1982
	.p2align 4,,10
	.p2align 3
.L1994:
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdi
	call	_ZN2v88internal3Map12GetVisitorIdES1_
	cmpl	$255, %eax
	jbe	.L1976
	.p2align 4,,10
	.p2align 3
.L1995:
	leaq	.LC38(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1997:
	leaq	.LC37(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1999:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18761:
	.size	_ZN2v88internal3Map21AddMissingTransitionsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEE, .-_ZN2v88internal3Map21AddMissingTransitionsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEENS4_INS0_16LayoutDescriptorEEE
	.section	.text._ZN2v88internal3Map26StartInobjectSlackTrackingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map26StartInobjectSlackTrackingEv
	.type	_ZN2v88internal3Map26StartInobjectSlackTrackingEv, @function
_ZN2v88internal3Map26StartInobjectSlackTrackingEv:
.LFB18798:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movzbl	9(%rdx), %eax
	cmpl	$2, %eax
	jg	.L2007
.L2002:
	testl	%eax, %eax
	jne	.L2008
	ret
	.p2align 4,,10
	.p2align 3
.L2008:
	movl	15(%rdx), %eax
	orl	$-536870912, %eax
	movl	%eax, 15(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L2007:
	movzbl	7(%rdx), %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	jmp	.L2002
	.cfi_endproc
.LFE18798:
	.size	_ZN2v88internal3Map26StartInobjectSlackTrackingEv, .-_ZN2v88internal3Map26StartInobjectSlackTrackingEv
	.section	.rodata._ZN2v88internal3Map21TransitionToPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEE.str1.1,"aMS",@progbits,1
.LC51:
	.string	"TransitionToPrototype"
	.section	.text._ZN2v88internal3Map21TransitionToPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map21TransitionToPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEE
	.type	_ZN2v88internal3Map21TransitionToPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEE, @function
_ZN2v88internal3Map21TransitionToPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEE:
.LFB18799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	$0, -104(%rbp)
	movq	%rdi, -128(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -104(%rbp)
	testb	$1, %al
	je	.L2010
	cmpl	$3, %eax
	je	.L2010
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L2028
	cmpq	$1, %rdx
	je	.L2029
.L2014:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2010:
	movl	$1, -96(%rbp)
.L2012:
	leaq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal19TransitionsAccessor22GetPrototypeTransitionENS0_6HandleINS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L2030
.L2018:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2031
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2030:
	.cfi_restore_state
	movq	%rbx, %xmm1
	movq	%r12, %xmm0
	movq	%rbx, %rsi
	movq	%r12, %rdi
	punpcklqdq	%xmm1, %xmm0
	leaq	.LC51(%rip), %rdx
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc
	movq	(%rbx), %rdx
	movdqa	-144(%rbp), %xmm0
	movq	$0, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	71(%rdx), %rdx
	movq	%rdx, -56(%rbp)
	testb	$1, %dl
	je	.L2019
	cmpl	$3, %edx
	je	.L2019
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$3, %rcx
	je	.L2032
	cmpq	$1, %rcx
	jne	.L2014
	movq	-1(%rdx), %rcx
	cmpw	$149, 11(%rcx)
	jne	.L2023
	movl	$4, -48(%rbp)
	jmp	.L2021
	.p2align 4,,10
	.p2align 3
.L2019:
	movl	$1, -48(%rbp)
.L2021:
	movq	%rax, %rdx
	leaq	-80(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal19TransitionsAccessor22PutPrototypeTransitionENS0_6HandleINS0_6ObjectEEENS2_INS0_3MapEEE@PLT
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	-144(%rbp), %rax
	movq	%rax, %rsi
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb
	movq	-144(%rbp), %rax
	jmp	.L2018
	.p2align 4,,10
	.p2align 3
.L2028:
	movl	$3, -96(%rbp)
	jmp	.L2012
	.p2align 4,,10
	.p2align 3
.L2029:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L2033
	movl	$4, -96(%rbp)
	jmp	.L2012
	.p2align 4,,10
	.p2align 3
.L2033:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -96(%rbp)
	jmp	.L2012
	.p2align 4,,10
	.p2align 3
.L2032:
	movl	$3, -48(%rbp)
	jmp	.L2021
.L2023:
	movq	-1(%rdx), %rdx
	cmpw	$95, 11(%rdx)
	setne	%dl
	movzbl	%dl, %edx
	addl	%edx, %edx
	movl	%edx, -48(%rbp)
	jmp	.L2021
.L2031:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18799:
	.size	_ZN2v88internal3Map21TransitionToPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEE, .-_ZN2v88internal3Map21TransitionToPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEE
	.section	.text._ZN2v88internal3Map18GetObjectCreateMapEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map18GetObjectCreateMapEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE
	.type	_ZN2v88internal3Map18GetObjectCreateMapEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE, @function
_ZN2v88internal3Map18GetObjectCreateMapEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE:
.LFB18735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	12464(%rdi), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2035
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2036:
	movq	879(%rsi), %rax
	movq	41112(%r12), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2038
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L2039:
	movq	(%r14), %rax
	cmpq	23(%rsi), %rax
	je	.L2051
	cmpq	104(%r12), %rax
	je	.L2065
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L2047
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	testl	$1048576, %eax
	je	.L2066
.L2048:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateE
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	39(%rax), %rsi
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	je	.L2067
.L2049:
	movq	0(%r13), %rax
	movzbl	9(%rax), %r8d
	cmpl	$2, %r8d
	jg	.L2068
.L2053:
	movzbl	7(%rax), %ecx
	movzbl	8(%rax), %esi
	movq	%r12, %rdi
	movzbl	7(%rax), %edx
	movzbl	%sil, %eax
	movq	%r13, %rsi
	subl	%eax, %ecx
	sall	$3, %edx
	call	_ZN2v88internal3Map14CopyInitialMapEPNS0_7IsolateENS0_6HandleIS1_EEiii
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %ecx
	movq	%rax, %r13
	movq	%rax, %rsi
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb
	movq	0(%r13), %rdx
	movq	(%rbx), %r12
	movq	%rdx, %rax
	leaq	39(%r12), %r14
	orq	$2, %rax
	movq	%rax, 39(%r12)
	testb	$1, %al
	je	.L2051
	cmpl	$3, %eax
	je	.L2051
	movq	%rdx, %rbx
	movq	%rdx, %r15
	andq	$-262144, %rbx
	andq	$-3, %r15
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L2069
.L2055:
	testb	$24, %al
	je	.L2051
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2051
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L2051:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2065:
	.cfi_restore_state
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	1215(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2044
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2038:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L2070
.L2040:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L2039
	.p2align 4,,10
	.p2align 3
.L2035:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2071
.L2037:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L2036
	.p2align 4,,10
	.p2align 3
.L2047:
	addq	$24, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal3Map21TransitionToPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEE
	.p2align 4,,10
	.p2align 3
.L2044:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2072
.L2046:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2067:
	.cfi_restore_state
	cmpl	$3, %esi
	je	.L2049
	movq	41112(%r12), %rdi
	andq	$-3, %rsi
	testq	%rdi, %rdi
	je	.L2050
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	jmp	.L2051
	.p2align 4,,10
	.p2align 3
.L2066:
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject19OptimizeAsPrototypeENS0_6HandleIS1_EEb@PLT
	jmp	.L2048
	.p2align 4,,10
	.p2align 3
.L2071:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L2037
	.p2align 4,,10
	.p2align 3
.L2070:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L2040
	.p2align 4,,10
	.p2align 3
.L2068:
	movzbl	7(%rax), %edx
	subl	%r8d, %edx
	movl	%edx, %r8d
	jmp	.L2053
	.p2align 4,,10
	.p2align 3
.L2069:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L2055
	.p2align 4,,10
	.p2align 3
.L2050:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L2073
.L2052:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L2051
	.p2align 4,,10
	.p2align 3
.L2072:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L2046
.L2073:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L2052
	.cfi_endproc
.LFE18735:
	.size	_ZN2v88internal3Map18GetObjectCreateMapEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE, .-_ZN2v88internal3Map18GetObjectCreateMapEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE
	.section	.text._ZN2v88internal18NormalizedMapCache3NewEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18NormalizedMapCache3NewEPNS0_7IsolateE
	.type	_ZN2v88internal18NormalizedMapCache3NewEPNS0_7IsolateE, @function
_ZN2v88internal18NormalizedMapCache3NewEPNS0_7IsolateE:
.LFB18800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movl	$64, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Factory17NewWeakFixedArrayEiNS0_14AllocationTypeE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18800:
	.size	_ZN2v88internal18NormalizedMapCache3NewEPNS0_7IsolateE, .-_ZN2v88internal18NormalizedMapCache3NewEPNS0_7IsolateE
	.section	.text._ZN2v88internal18NormalizedMapCache3GetENS0_6HandleINS0_3MapEEENS0_12ElementsKindENS0_25PropertyNormalizationModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18NormalizedMapCache3GetENS0_6HandleINS0_3MapEEENS0_12ElementsKindENS0_25PropertyNormalizationModeE
	.type	_ZN2v88internal18NormalizedMapCache3GetENS0_6HandleINS0_3MapEEENS0_12ElementsKindENS0_25PropertyNormalizationModeE, @function
_ZN2v88internal18NormalizedMapCache3GetENS0_6HandleINS0_3MapEEENS0_12ElementsKindENS0_25PropertyNormalizationModeE:
.LFB18801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	31(%r8), %rax
	testb	$1, %al
	jne	.L2078
.L2077:
	movl	23(%r8), %esi
	shrl	$2, %eax
	movzwl	%ax, %eax
	sall	$14, %esi
	xorl	%eax, %esi
	movzbl	14(%r8), %eax
	xorl	%esi, %eax
	sarl	$16, %esi
	xorl	%esi, %eax
	movl	%eax, %esi
	sarl	$31, %esi
	shrl	$26, %esi
	addl	%esi, %eax
	andl	$63, %eax
	subl	%esi, %eax
	movq	(%rbx), %rsi
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rsi), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$3, %rsi
	je	.L2089
.L2079:
	xorl	%eax, %eax
.L2081:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2090
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2089:
	.cfi_restore_state
	cmpl	$3, %eax
	je	.L2079
	andq	$-3, %rax
	movzbl	%dl, %edx
	leaq	-32(%rbp), %rdi
	movq	%r8, %rsi
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal3Map28EquivalentToForNormalizationES1_NS0_12ElementsKindENS0_25PropertyNormalizationModeE
	testb	%al, %al
	je	.L2079
	movq	(%rbx), %rax
	movq	-32(%rbp), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L2082
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L2081
	.p2align 4,,10
	.p2align 3
.L2091:
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L2077
	.p2align 4,,10
	.p2align 3
.L2078:
	movq	-1(%rax), %rsi
	cmpw	$68, 11(%rsi)
	jne	.L2077
	jmp	.L2091
	.p2align 4,,10
	.p2align 3
.L2082:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2092
.L2084:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L2081
.L2092:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L2084
.L2090:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18801:
	.size	_ZN2v88internal18NormalizedMapCache3GetENS0_6HandleINS0_3MapEEENS0_12ElementsKindENS0_25PropertyNormalizationModeE, .-_ZN2v88internal18NormalizedMapCache3GetENS0_6HandleINS0_3MapEEENS0_12ElementsKindENS0_25PropertyNormalizationModeE
	.section	.text._ZN2v88internal18NormalizedMapCache3SetENS0_6HandleINS0_3MapEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18NormalizedMapCache3SetENS0_6HandleINS0_3MapEEES4_
	.type	_ZN2v88internal18NormalizedMapCache3SetENS0_6HandleINS0_3MapEEES4_, @function
_ZN2v88internal18NormalizedMapCache3SetENS0_6HandleINS0_3MapEEES4_:
.LFB18802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%r8), %rdi
	movq	(%rdx), %rsi
	movq	31(%rdi), %rax
	movq	%rsi, %rcx
	orq	$2, %rcx
	testb	$1, %al
	jne	.L2095
.L2094:
	movl	23(%rdi), %edx
	shrl	$2, %eax
	movzwl	%ax, %eax
	sall	$14, %edx
	xorl	%eax, %edx
	movzbl	14(%rdi), %eax
	xorl	%edx, %eax
	sarl	$16, %edx
	xorl	%edx, %eax
	cltd
	shrl	$26, %edx
	addl	%edx, %eax
	andl	$63, %eax
	subl	%edx, %eax
	leal	16(,%rax,8), %r12d
	movq	(%rbx), %rax
	movslq	%r12d, %r12
	movq	%rcx, -1(%r12,%rax)
	testb	$1, %cl
	je	.L2093
	cmpl	$3, %ecx
	je	.L2093
	movq	%rsi, %r14
	andq	$-262144, %rsi
	movq	(%rbx), %rdi
	movq	8(%rsi), %rax
	andq	$-3, %r14
	movq	%rsi, %r13
	leaq	-1(%r12,%rdi), %r8
	testl	$262144, %eax
	jne	.L2109
.L2097:
	testb	$24, %al
	je	.L2093
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2110
.L2093:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2111:
	.cfi_restore_state
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L2094
	.p2align 4,,10
	.p2align 3
.L2095:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L2094
	jmp	.L2111
	.p2align 4,,10
	.p2align 3
.L2109:
	movq	%r8, %rsi
	movq	%r14, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r13), %rax
	leaq	-1(%r12,%rdi), %r8
	jmp	.L2097
	.p2align 4,,10
	.p2align 3
.L2110:
	popq	%rbx
	movq	%r14, %rdx
	popq	%r12
	movq	%r8, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE18802:
	.size	_ZN2v88internal18NormalizedMapCache3SetENS0_6HandleINS0_3MapEEES4_, .-_ZN2v88internal18NormalizedMapCache3SetENS0_6HandleINS0_3MapEEES4_
	.section	.rodata._ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc.str1.1,"aMS",@progbits,1
.LC52:
	.string	"Normalize"
	.section	.text._ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc
	.type	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc, @function
_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc:
.LFB18751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	%r8, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdi), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2113
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2114:
	movq	863(%rsi), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2116
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L2117:
	movq	0(%r13), %rax
	movl	15(%rax), %eax
	testl	$1048576, %eax
	jne	.L2130
	movq	%r8, -72(%rbp)
	movq	(%r8), %rax
	cmpq	%rax, 88(%r12)
	je	.L2130
	movzbl	%bl, %edx
	leaq	-64(%rbp), %rdi
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18NormalizedMapCache3GetENS0_6HandleINS0_3MapEEENS0_12ElementsKindENS0_25PropertyNormalizationModeE
	movq	-72(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L2142
.L2120:
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	jne	.L2143
.L2125:
	movq	0(%r13), %rax
	movl	15(%rax), %edx
	andl	$33554432, %edx
	je	.L2144
.L2127:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2145
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2130:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
.L2119:
	movq	%r13, %rsi
	movl	%r14d, %edx
	movq	%r12, %rdi
	movb	%cl, -73(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal3Map14CopyNormalizedEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeE
	cmpb	$27, %bl
	movq	-72(%rbp), %r8
	movzbl	-73(%rbp), %ecx
	movq	(%rax), %rsi
	movq	%rax, %r15
	ja	.L2146
	movzbl	14(%rsi), %eax
	movzbl	%bl, %edx
	leal	0(,%rdx,8), %ebx
	andb	$7, %al
	orl	%eax, %ebx
	movb	%bl, 14(%rsi)
	testb	%cl, %cl
	je	.L2120
	movq	(%r8), %rax
	leaq	-64(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18NormalizedMapCache3SetENS0_6HandleINS0_3MapEEES4_
	movq	40960(%r12), %rbx
	cmpb	$0, 5984(%rbx)
	je	.L2122
	movq	5976(%rbx), %rax
.L2123:
	testq	%rax, %rax
	je	.L2120
	addl	$1, (%rax)
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	je	.L2125
	.p2align 4,,10
	.p2align 3
.L2143:
	movq	41016(%r12), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L2125
	movq	0(%r13), %rdx
	movq	(%r15), %rcx
	xorl	%r9d, %r9d
	movq	%r14, %rdi
	movq	-88(%rbp), %r8
	leaq	.LC52(%rip), %rsi
	call	_ZN2v88internal6Logger8MapEventEPKcNS0_3MapES4_S3_NS0_10HeapObjectE@PLT
	movq	0(%r13), %rax
	movl	15(%rax), %edx
	andl	$33554432, %edx
	jne	.L2127
	.p2align 4,,10
	.p2align 3
.L2144:
	movl	15(%rax), %edx
	leaq	-64(%rbp), %rdi
	movq	%r12, %rsi
	orl	$33554432, %edx
	movl	%edx, 15(%rax)
	movl	$1, %edx
	movq	55(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE@PLT
	jmp	.L2127
	.p2align 4,,10
	.p2align 3
.L2116:
	movq	41088(%r12), %r8
	cmpq	%r8, 41096(%r12)
	je	.L2147
.L2118:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L2117
	.p2align 4,,10
	.p2align 3
.L2113:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2148
.L2115:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L2114
	.p2align 4,,10
	.p2align 3
.L2142:
	movl	$1, %ecx
	jmp	.L2119
	.p2align 4,,10
	.p2align 3
.L2122:
	movb	$1, 5984(%rbx)
	leaq	5960(%rbx), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 5976(%rbx)
	jmp	.L2123
	.p2align 4,,10
	.p2align 3
.L2147:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L2118
	.p2align 4,,10
	.p2align 3
.L2148:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L2115
	.p2align 4,,10
	.p2align 3
.L2146:
	leaq	.LC47(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18751:
	.size	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc, .-_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc
	.section	.rodata._ZN2v88internal3Map24TransitionToDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesENS0_17PropertyConstnessENS0_11StoreOriginE.str1.1,"aMS",@progbits,1
.LC53:
	.string	"TooManyFastProperties"
	.section	.text._ZN2v88internal3Map24TransitionToDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesENS0_17PropertyConstnessENS0_11StoreOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map24TransitionToDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesENS0_17PropertyConstnessENS0_11StoreOriginE
	.type	_ZN2v88internal3Map24TransitionToDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesENS0_17PropertyConstnessENS0_11StoreOriginE, @function
_ZN2v88internal3Map24TransitionToDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesENS0_17PropertyConstnessENS0_11StoreOriginE:
.LFB18775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$232, %rsp
	movq	%rcx, -248(%rbp)
	movl	%r9d, -256(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, -192(%rbp)
	movl	15(%rax), %edx
	andl	$1048576, %edx
	jne	.L2150
	cmpw	$1057, 11(%rax)
	je	.L2151
.L2153:
	movl	$167, %edx
.L2152:
	movq	$0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2276
.L2154:
	movq	(%r15), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L2277
.L2155:
	movq	%r14, %xmm0
	movq	%r12, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	(%r12), %rax
	movq	$0, -168(%rbp)
	movq	%rax, -176(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -168(%rbp)
	testb	$1, %al
	je	.L2156
	cmpl	$3, %eax
	je	.L2156
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L2278
	cmpq	$1, %rdx
	je	.L2279
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2156:
	movl	$1, -160(%rbp)
.L2158:
	movq	0(%r13), %rsi
	leaq	-192(%rbp), %r15
	movl	%ebx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal19TransitionsAccessor16SearchTransitionENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2164
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2165
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L2166:
	movl	15(%rsi), %ebx
	movq	-248(%rbp), %rax
	movq	(%r12), %rdx
	shrl	$10, %ebx
	movq	(%rax), %rsi
	andl	$1023, %ebx
	movq	39(%rdx), %rax
	leal	(%rbx,%rbx,2), %r13d
	sall	$3, %r13d
	movslq	%r13d, %r13
	leaq	-1(%rax,%r13), %rdi
	movq	8(%rdi), %rax
	movq	%rax, %rcx
	shrq	$33, %rax
	sarq	$32, %rcx
	orl	%ecx, %eax
	testb	$1, %al
	jne	.L2169
	testb	$4, %cl
	je	.L2235
	cmpl	$1, -256(%rbp)
	je	.L2235
.L2169:
	movq	39(%rdx), %rax
	movl	$4, %r8d
	movq	7(%r13,%rax), %r13
	shrq	$35, %r13
	andl	$7, %r13d
	cmpb	$0, _ZN2v88internal17FLAG_track_fieldsE(%rip)
	je	.L2188
	movl	$1, %r8d
	testb	$1, %sil
	je	.L2188
	cmpb	$0, _ZN2v88internal24FLAG_track_double_fieldsE(%rip)
	jne	.L2280
.L2189:
	cmpb	$0, _ZN2v88internal26FLAG_track_computed_fieldsE(%rip)
	jne	.L2191
.L2192:
	cmpb	$0, _ZN2v88internal29FLAG_track_heap_object_fieldsE(%rip)
	sete	%r8b
	addl	$3, %r8d
	.p2align 4,,10
	.p2align 3
.L2188:
	movl	%r8d, %edx
	movq	%rsi, -192(%rbp)
	movq	%r15, %rdi
	movq	%r14, %rsi
	movb	%r8b, -264(%rbp)
	call	_ZN2v88internal6Object11OptimalTypeEPNS0_7IsolateENS0_14RepresentationE@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal10MapUpdaterC1EPNS0_7IsolateENS0_6HandleINS0_3MapEEE@PLT
	leal	-1(%rbx), %esi
	movl	%r13d, %edx
	movq	%r15, %rdi
	movq	-248(%rbp), %r9
	movzbl	-264(%rbp), %r8d
	movl	-256(%rbp), %ecx
	call	_ZN2v88internal10MapUpdater22ReconfigureToDataFieldEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEE@PLT
	movq	%rax, %r12
.L2187:
	movq	%r12, %rax
	jmp	.L2193
	.p2align 4,,10
	.p2align 3
.L2164:
	movq	40936(%r14), %rax
	movq	(%r12), %rdx
	movl	8(%rax), %r8d
	movq	%rdx, -192(%rbp)
	movzbl	9(%rdx), %eax
	cmpl	$2, %eax
	jg	.L2281
.L2194:
	testl	%eax, %eax
	je	.L2282
.L2196:
	movq	-248(%rbp), %rax
	cmpb	$0, _ZN2v88internal17FLAG_track_fieldsE(%rip)
	movl	$4, %edx
	movq	(%rax), %rax
	jne	.L2283
.L2202:
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	%r8d, -264(%rbp)
	movb	%dl, -248(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal6Object11OptimalTypeEPNS0_7IsolateENS0_14RepresentationE@PLT
	movl	-264(%rbp), %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movzbl	-248(%rbp), %edx
	movq	%rax, %rcx
	xorl	%eax, %eax
	movl	-256(%rbp), %r9d
	testl	%r8d, %r8d
	movl	%ebx, %r8d
	setne	%al
	pushq	%rax
	pushq	%rdx
	movq	%r13, %rdx
	call	_ZN2v88internal3Map13CopyWithFieldEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_9FieldTypeEEENS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_14TransitionFlagE
	movq	%rax, %rbx
	popq	%rax
	popq	%rdx
	testq	%rbx, %rbx
	je	.L2201
.L2208:
	movq	%rbx, %rax
.L2193:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2284
.L2225:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2285
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2151:
	.cfi_restore_state
	movl	15(%rax), %eax
	testl	$1047552, %eax
	je	.L2153
	leaq	-192(%rbp), %rdi
	call	_ZNK2v88internal3Map14GetBackPointerEPNS0_7IsolateE.constprop.0
	cmpq	88(%r14), %rax
	jne	.L2153
	.p2align 4,,10
	.p2align 3
.L2150:
	movl	$193, %edx
	jmp	.L2152
	.p2align 4,,10
	.p2align 3
.L2277:
	leaq	-192(%rbp), %r12
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10MapUpdaterC1EPNS0_7IsolateENS0_6HandleINS0_3MapEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal10MapUpdater6UpdateEv@PLT
	movq	%rax, %r12
	jmp	.L2155
	.p2align 4,,10
	.p2align 3
.L2282:
	movq	-192(%rbp), %rax
	movl	15(%rax), %edx
	andl	$1048576, %edx
	jne	.L2196
	cmpl	$1, 16(%rbp)
	movl	%r8d, -264(%rbp)
	movq	%r15, %rdi
	movzbl	7(%rax), %r9d
	movzbl	8(%rax), %r11d
	jne	.L2198
	call	_ZNK2v88internal3Map14GetFieldCountsEv
	movq	-192(%rbp), %rcx
	movl	$128, %esi
	subl	%r11d, %r9d
	movzbl	7(%rcx), %edx
	movzbl	8(%rcx), %ecx
	cmpl	$128, %r9d
	cmovl	%esi, %r9d
	subl	%ecx, %edx
	movl	%eax, %ecx
	subl	%edx, %ecx
	cmpl	%ecx, %r9d
	jge	.L2286
.L2201:
	movq	(%r12), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L2210
.L2209:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2211
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L2212:
	movq	(%r12), %rax
	cmpb	$0, _ZN2v88internal27FLAG_feedback_normalizationE(%rip)
	movzbl	14(%rax), %edx
	je	.L2275
	testb	$1, %dl
	je	.L2275
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2287
.L2275:
	shrl	$3, %edx
	leaq	.LC53(%rip), %r8
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc
	movq	%rax, %rbx
	jmp	.L2208
	.p2align 4,,10
	.p2align 3
.L2283:
	movl	$1, %edx
	testb	$1, %al
	je	.L2202
	cmpb	$0, _ZN2v88internal24FLAG_track_double_fieldsE(%rip)
	jne	.L2288
.L2203:
	cmpb	$0, _ZN2v88internal26FLAG_track_computed_fieldsE(%rip)
	jne	.L2205
.L2206:
	cmpb	$0, _ZN2v88internal29FLAG_track_heap_object_fieldsE(%rip)
	sete	%dl
	addl	$3, %edx
	jmp	.L2202
	.p2align 4,,10
	.p2align 3
.L2235:
	shrl	$6, %ecx
	movzbl	_ZN2v88internal17FLAG_track_fieldsE(%rip), %eax
	andl	$7, %ecx
	cmpl	$1, %ecx
	jne	.L2172
	testb	%al, %al
	jne	.L2289
.L2172:
	cmpl	$2, %ecx
	jne	.L2236
	cmpb	$0, _ZN2v88internal24FLAG_track_double_fieldsE(%rip)
	jne	.L2174
.L2236:
	cmpb	$0, _ZN2v88internal29FLAG_track_heap_object_fieldsE(%rip)
	je	.L2178
	cmpl	$3, %ecx
	je	.L2290
.L2178:
	testb	%al, %al
	je	.L2180
	testl	%ecx, %ecx
	je	.L2169
.L2180:
	movq	16(%rdi), %rdi
	cmpl	$3, %edi
	je	.L2291
	movq	%rdi, %rax
	movq	%rsi, -264(%rbp)
	andl	$3, %eax
	cmpq	$3, %rax
	jne	.L2274
	andq	$-3, %rdi
.L2274:
	call	_ZN2v88internal9FieldType4castENS0_6ObjectE@PLT
	movq	-264(%rbp), %rsi
.L2184:
	movq	%r15, %rdi
	movq	%rax, -192(%rbp)
	call	_ZNK2v88internal9FieldType11NowContainsENS0_6ObjectE@PLT
	testb	%al, %al
	jne	.L2187
	movq	-248(%rbp), %rax
	movq	(%r12), %rdx
	movq	(%rax), %rsi
	jmp	.L2169
	.p2align 4,,10
	.p2align 3
.L2165:
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L2292
.L2167:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r12)
	jmp	.L2166
	.p2align 4,,10
	.p2align 3
.L2281:
	movzbl	7(%rdx), %edx
	subl	%eax, %edx
	movl	%edx, %eax
	jmp	.L2194
	.p2align 4,,10
	.p2align 3
.L2278:
	movl	$3, -160(%rbp)
	jmp	.L2158
	.p2align 4,,10
	.p2align 3
.L2276:
	movq	40960(%r14), %rax
	leaq	-232(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2154
	.p2align 4,,10
	.p2align 3
.L2284:
	leaq	-232(%rbp), %rsi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-248(%rbp), %rax
	jmp	.L2225
	.p2align 4,,10
	.p2align 3
.L2198:
	call	_ZNK2v88internal3Map14NumberOfFieldsEv
	movq	-192(%rbp), %rcx
	movl	$12, %esi
	subl	%r11d, %r9d
	movzbl	7(%rcx), %edx
	movzbl	8(%rcx), %ecx
	cmpl	$12, %r9d
	cmovl	%esi, %r9d
	movl	-264(%rbp), %r8d
	subl	%ecx, %edx
	subl	%edx, %eax
	cmpl	%eax, %r9d
	jl	.L2201
	jmp	.L2196
	.p2align 4,,10
	.p2align 3
.L2279:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L2293
	movl	$4, -160(%rbp)
	jmp	.L2158
	.p2align 4,,10
	.p2align 3
.L2211:
	movq	41088(%r14), %r13
	cmpq	41096(%r14), %r13
	je	.L2294
.L2213:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, 0(%r13)
	jmp	.L2212
	.p2align 4,,10
	.p2align 3
.L2286:
	movq	%rax, %rdx
	movl	-264(%rbp), %r8d
	sarq	$32, %rdx
	addl	%edx, %eax
	cmpl	$1020, %eax
	jle	.L2196
	jmp	.L2201
	.p2align 4,,10
	.p2align 3
.L2210:
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L2209
	movq	31(%rsi), %rsi
	testb	$1, %sil
	je	.L2209
	jmp	.L2210
	.p2align 4,,10
	.p2align 3
.L2292:
	movq	%r14, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L2167
	.p2align 4,,10
	.p2align 3
.L2293:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -160(%rbp)
	jmp	.L2158
	.p2align 4,,10
	.p2align 3
.L2280:
	movq	-1(%rsi), %rax
	movl	$2, %r8d
	cmpw	$65, 11(%rax)
	jne	.L2189
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2288:
	movq	-1(%rax), %rcx
	movl	$2, %edx
	cmpw	$65, 11(%rcx)
	jne	.L2203
	jmp	.L2202
	.p2align 4,,10
	.p2align 3
.L2289:
	movq	%rsi, %rax
	notq	%rax
	andl	$1, %eax
.L2173:
	testb	%al, %al
	je	.L2169
	jmp	.L2180
	.p2align 4,,10
	.p2align 3
.L2205:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37512(%rdx), %rax
	jne	.L2206
	xorl	%edx, %edx
	jmp	.L2202
	.p2align 4,,10
	.p2align 3
.L2191:
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37512(%rax), %rsi
	jne	.L2192
	xorl	%r8d, %r8d
	jmp	.L2188
.L2174:
	testb	$1, %sil
	je	.L2180
	movq	-1(%rsi), %rax
	cmpw	$65, 11(%rax)
	sete	%al
	jmp	.L2173
.L2287:
	movq	-1(%rax), %rcx
	cmpw	$1105, 11(%rcx)
	jne	.L2275
	movq	23(%rax), %rcx
	movl	47(%rcx), %ecx
	andl	$32, %ecx
	jne	.L2275
	movq	41112(%r14), %rdi
	movq	55(%rax), %rbx
	testq	%rdi, %rdi
	je	.L2217
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r9
.L2218:
	movq	(%r9), %rax
	movq	%r9, %rsi
	leaq	.LC53(%rip), %r8
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%r9, -248(%rbp)
	movzbl	14(%rax), %edx
	shrl	$3, %edx
	call	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc
	movq	-248(%rbp), %r9
	movq	%rax, %rbx
	movq	(%r9), %rax
	movq	%rax, -192(%rbp)
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L2220
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal3Map23DeprecateTransitionTreeEPNS0_7IsolateE.part.0
	movq	-248(%rbp), %r9
.L2220:
	movq	(%rbx), %rax
	movq	41112(%r14), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2221
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-248(%rbp), %r9
	movq	%rax, %rdx
.L2222:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal10JSFunction13SetInitialMapENS0_6HandleIS1_EENS2_INS0_3MapEEENS2_INS0_10HeapObjectEEE@PLT
	movl	$4, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	-248(%rbp), %r9
	movq	(%r9), %rax
	movq	55(%rax), %rax
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE@PLT
	movq	(%rbx), %rax
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%rax, -192(%rbp)
	movzbl	14(%rax), %edx
	movq	(%r12), %rsi
	shrl	$3, %edx
	call	_ZNK2v88internal3Map28EquivalentToForNormalizationES1_NS0_12ElementsKindENS0_25PropertyNormalizationModeE
	testb	%al, %al
	jne	.L2208
	movq	(%r12), %rax
	movzbl	14(%rax), %edx
	jmp	.L2275
	.p2align 4,,10
	.p2align 3
.L2290:
	movl	%esi, %eax
	andl	$1, %eax
	jmp	.L2173
.L2294:
	movq	%r14, %rdi
	movq	%rsi, -248(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-248(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L2213
.L2291:
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal9FieldType4NoneEv@PLT
	movq	-264(%rbp), %rsi
	jmp	.L2184
.L2221:
	movq	41088(%r14), %rdx
	cmpq	41096(%r14), %rdx
	je	.L2295
.L2223:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L2222
.L2217:
	movq	41088(%r14), %r9
	cmpq	41096(%r14), %r9
	je	.L2296
.L2219:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r14)
	movq	%rbx, (%r9)
	jmp	.L2218
.L2285:
	call	__stack_chk_fail@PLT
.L2295:
	movq	%r14, %rdi
	movq	%rsi, -256(%rbp)
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %rsi
	movq	-248(%rbp), %r9
	movq	%rax, %rdx
	jmp	.L2223
.L2296:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r9
	jmp	.L2219
	.cfi_endproc
.LFE18775:
	.size	_ZN2v88internal3Map24TransitionToDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesENS0_17PropertyConstnessENS0_11StoreOriginE, .-_ZN2v88internal3Map24TransitionToDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesENS0_17PropertyConstnessENS0_11StoreOriginE
	.section	.rodata._ZN2v88internal3Map27ReconfigureExistingPropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_12PropertyKindENS0_18PropertyAttributesENS0_17PropertyConstnessE.str1.8,"aMS",@progbits,1
	.align 8
.LC54:
	.string	"Normalize_AttributesMismatchProtoMap"
	.section	.text._ZN2v88internal3Map27ReconfigureExistingPropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_12PropertyKindENS0_18PropertyAttributesENS0_17PropertyConstnessE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map27ReconfigureExistingPropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_12PropertyKindENS0_18PropertyAttributesENS0_17PropertyConstnessE
	.type	_ZN2v88internal3Map27ReconfigureExistingPropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_12PropertyKindENS0_18PropertyAttributesENS0_17PropertyConstnessE, @function
_ZN2v88internal3Map27ReconfigureExistingPropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_12PropertyKindENS0_18PropertyAttributesENS0_17PropertyConstnessE:
.LFB18776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$168, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	movq	31(%rsi), %rdi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %dil
	je	.L2298
	leaq	-1(%rdi), %rdx
	movq	-1(%rdi), %rdi
	cmpq	%rdi, 136(%rax)
	je	.L2299
.L2298:
	movq	88(%rax), %rdx
	subq	$1, %rdx
.L2299:
	movq	(%rdx), %rax
	cmpw	$68, 11(%rax)
	jne	.L2306
	cmpb	$0, _ZN2v88internal25FLAG_trace_generalizationE(%rip)
	leaq	-192(%rbp), %r10
	jne	.L2307
.L2302:
	movq	%r10, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r10, -200(%rbp)
	call	_ZN2v88internal10MapUpdaterC1EPNS0_7IsolateENS0_6HandleINS0_3MapEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal9FieldType4NoneEPNS0_7IsolateE@PLT
	xorl	%r8d, %r8d
	movl	%ebx, %ecx
	movl	%r15d, %edx
	movq	-200(%rbp), %r10
	movq	%rax, %r9
	movl	%r14d, %esi
	movq	%r10, %rdi
	call	_ZN2v88internal10MapUpdater22ReconfigureToDataFieldEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEE@PLT
.L2301:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2308
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2306:
	.cfi_restore_state
	movzbl	14(%rsi), %edx
	leaq	.LC54(%rip), %r8
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	shrl	$3, %edx
	call	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc
	jmp	.L2301
	.p2align 4,,10
	.p2align 3
.L2307:
	movq	stdout(%rip), %rdx
	movl	%ecx, %r8d
	movq	%r10, %rdi
	movl	%r15d, %r9d
	movq	%rsi, -192(%rbp)
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r10, -200(%rbp)
	call	_ZN2v88internal3Map20PrintReconfigurationEPNS0_7IsolateEP8_IO_FILEiNS0_12PropertyKindENS0_18PropertyAttributesE
	movq	-200(%rbp), %r10
	jmp	.L2302
.L2308:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18776:
	.size	_ZN2v88internal3Map27ReconfigureExistingPropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_12PropertyKindENS0_18PropertyAttributesENS0_17PropertyConstnessE, .-_ZN2v88internal3Map27ReconfigureExistingPropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_12PropertyKindENS0_18PropertyAttributesENS0_17PropertyConstnessE
	.section	.text._ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi,"axG",@progbits,_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi,comdat
	.p2align 4
	.weak	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi
	.type	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi, @function
_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi:
.LFB21216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rbx
	movswl	9(%rbx), %r10d
	movl	7(%rsi), %r9d
	subl	$1, %r10d
	je	.L2320
	movl	%r10d, %r12d
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L2311:
	movl	%r12d, %ecx
	subl	%r8d, %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	addl	%r8d, %eax
	leal	1(%rax), %r11d
	leal	(%r11,%r11,2), %ecx
	sall	$3, %ecx
	movslq	%ecx, %rcx
	movq	7(%rcx,%rbx), %rcx
	shrq	$41, %rcx
	andl	$1023, %ecx
	leal	3(%rcx,%rcx,2), %ecx
	sall	$3, %ecx
	movslq	%ecx, %rcx
	movq	-1(%rcx,%rbx), %rcx
	cmpl	7(%rcx), %r9d
	ja	.L2325
	cmpl	%r8d, %eax
	je	.L2310
	movl	%eax, %r12d
	jmp	.L2311
	.p2align 4,,10
	.p2align 3
.L2325:
	cmpl	%r12d, %r11d
	je	.L2321
	movl	%r11d, %r8d
	jmp	.L2311
	.p2align 4,,10
	.p2align 3
.L2320:
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L2310:
	leal	3(%r8,%r8,2), %r11d
	sall	$3, %r11d
	movslq	%r11d, %r11
	jmp	.L2319
	.p2align 4,,10
	.p2align 3
.L2316:
	movq	(%rdi), %rbx
	addl	$1, %r8d
	movq	7(%r11,%rbx), %rax
	shrq	$41, %rax
	andl	$1023, %eax
	leal	3(%rax,%rax,2), %ecx
	sall	$3, %ecx
	movslq	%ecx, %rcx
	movq	-1(%rcx,%rbx), %rcx
	cmpl	7(%rcx), %r9d
	jne	.L2318
	addq	$24, %r11
	cmpq	%rcx, %rsi
	je	.L2326
.L2319:
	cmpl	%r10d, %r8d
	jle	.L2316
.L2318:
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2321:
	.cfi_restore_state
	movl	%r12d, %r8d
	jmp	.L2310
	.p2align 4,,10
	.p2align 3
.L2326:
	cmpl	%eax, %edx
	jle	.L2318
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21216:
	.size	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi, .-_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi
	.section	.text._ZN2v88internal3Map20CopyInsertDescriptorEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_10DescriptorENS0_14TransitionFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map20CopyInsertDescriptorEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_10DescriptorENS0_14TransitionFlagE
	.type	_ZN2v88internal3Map20CopyInsertDescriptorEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_10DescriptorENS0_14TransitionFlagE, @function
_ZN2v88internal3Map20CopyInsertDescriptorEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_10DescriptorENS0_14TransitionFlagE:
.LFB18779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2328
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-72(%rbp), %r9d
	movq	(%rax), %rsi
	movq	%rax, %r15
.L2329:
	movq	(%r14), %rax
	movq	0(%r13), %rbx
	movq	%rsi, -64(%rbp)
	movq	(%rax), %rsi
	movl	15(%rbx), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	je	.L2332
	movl	7(%rsi), %eax
	movl	%ebx, %r11d
	movq	41080(%r12), %r10
	shrl	$3, %r11d
	xorl	%r11d, %eax
	andl	$63, %eax
	movq	%rax, %rcx
	salq	$4, %rcx
	cmpq	(%r10,%rcx), %rbx
	jne	.L2333
	cmpq	8(%r10,%rcx), %rsi
	jne	.L2333
	movl	1024(%r10,%rax,4), %r8d
	cmpl	$-2, %r8d
	je	.L2333
.L2334:
	cmpl	$-1, %r8d
	jne	.L2349
.L2332:
	movl	%r9d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Map17CopyAddDescriptorEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_10DescriptorENS0_14TransitionFlagE
.L2340:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2350
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2349:
	.cfi_restore_state
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Map21CopyReplaceDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_15DescriptorArrayEEEPNS0_10DescriptorEiNS0_14TransitionFlagE
	jmp	.L2340
	.p2align 4,,10
	.p2align 3
.L2328:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L2351
.L2330:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L2329
	.p2align 4,,10
	.p2align 3
.L2333:
	cmpl	$8, %edx
	jg	.L2352
	movq	-64(%rbp), %rdi
	movq	%r10, -72(%rbp)
	movl	$24, %ecx
	xorl	%eax, %eax
	movq	%rdi, %r10
	.p2align 4,,10
	.p2align 3
.L2335:
	movq	-1(%rcx,%r10), %rdi
	movl	%eax, %r8d
	leal	1(%rax), %eax
	cmpq	%rdi, %rsi
	je	.L2353
	addq	$24, %rcx
	cmpl	%eax, %edx
	jne	.L2335
	movq	-72(%rbp), %r10
	movl	$-1, %r8d
	jmp	.L2339
	.p2align 4,,10
	.p2align 3
.L2352:
	leaq	-64(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	%r9d, -88(%rbp)
	movl	%r11d, -84(%rbp)
	movq	%r10, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi
	movl	-88(%rbp), %r9d
	movl	-84(%rbp), %r11d
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %rsi
	movl	%eax, %r8d
.L2339:
	xorl	7(%rsi), %r11d
	movq	%rbx, %xmm0
	movq	%rsi, %xmm1
	andl	$63, %r11d
	punpcklqdq	%xmm1, %xmm0
	movq	%r11, %rax
	salq	$4, %rax
	movups	%xmm0, (%r10,%rax)
	movl	%r8d, 1024(%r10,%r11,4)
	jmp	.L2334
	.p2align 4,,10
	.p2align 3
.L2351:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	movl	%ecx, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-80(%rbp), %r9d
	movq	-72(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L2330
	.p2align 4,,10
	.p2align 3
.L2353:
	movq	-72(%rbp), %r10
	jmp	.L2339
.L2350:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18779:
	.size	_ZN2v88internal3Map20CopyInsertDescriptorEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_10DescriptorENS0_14TransitionFlagE, .-_ZN2v88internal3Map20CopyInsertDescriptorEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_10DescriptorENS0_14TransitionFlagE
	.section	.rodata._ZN2v88internal3Map28TransitionToAccessorPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEEiNS4_INS0_6ObjectEEES9_NS0_18PropertyAttributesE.str1.8,"aMS",@progbits,1
	.align 8
.LC55:
	.string	"TransitionToAccessorFromNonPair"
	.section	.rodata._ZN2v88internal3Map28TransitionToAccessorPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEEiNS4_INS0_6ObjectEEES9_NS0_18PropertyAttributesE.str1.1,"aMS",@progbits,1
.LC56:
	.string	"TransitionToDifferentAccessor"
.LC57:
	.string	"AccessorsOverwritingNonLast"
	.section	.rodata._ZN2v88internal3Map28TransitionToAccessorPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEEiNS4_INS0_6ObjectEEES9_NS0_18PropertyAttributesE.str1.8
	.align 8
.LC58:
	.string	"AccessorsOverwritingNonAccessors"
	.section	.rodata._ZN2v88internal3Map28TransitionToAccessorPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEEiNS4_INS0_6ObjectEEES9_NS0_18PropertyAttributesE.str1.1
.LC59:
	.string	"AccessorsWithAttributes"
.LC60:
	.string	"AccessorsOverwritingNonPair"
.LC61:
	.string	"AccessorsOverwritingAccessors"
.LC62:
	.string	"TooManyAccessors"
	.section	.text._ZN2v88internal3Map28TransitionToAccessorPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEEiNS4_INS0_6ObjectEEES9_NS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map28TransitionToAccessorPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEEiNS4_INS0_6ObjectEEES9_NS0_18PropertyAttributesE
	.type	_ZN2v88internal3Map28TransitionToAccessorPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEEiNS4_INS0_6ObjectEEES9_NS0_18PropertyAttributesE, @function
_ZN2v88internal3Map28TransitionToAccessorPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEEiNS4_INS0_6ObjectEEES9_NS0_18PropertyAttributesE:
.LFB18777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$248, %rsp
	movq	%r8, -248(%rbp)
	movq	%r9, -256(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, -192(%rbp)
	movl	15(%rax), %edx
	andl	$1048576, %edx
	jne	.L2355
	cmpw	$1057, 11(%rax)
	je	.L2356
.L2358:
	movl	$166, %edx
.L2357:
	movq	$0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2455
.L2359:
	movq	(%r15), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L2456
.L2360:
	movq	0(%r13), %rax
	movl	15(%rax), %edx
	andl	$2097152, %edx
	je	.L2361
.L2454:
	movq	%r13, %rax
.L2362:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2457
.L2408:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2458
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2356:
	.cfi_restore_state
	movl	15(%rax), %eax
	testl	$1047552, %eax
	je	.L2358
	leaq	-192(%rbp), %rdi
	call	_ZNK2v88internal3Map14GetBackPointerEPNS0_7IsolateE.constprop.0
	cmpq	88(%r14), %rax
	jne	.L2358
	.p2align 4,,10
	.p2align 3
.L2355:
	movl	$192, %edx
	jmp	.L2357
	.p2align 4,,10
	.p2align 3
.L2361:
	movq	%r14, %xmm0
	movq	%r13, %xmm1
	movl	15(%rax), %r15d
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	0(%r13), %rax
	shrl	$20, %r15d
	movq	$0, -168(%rbp)
	andl	$1, %r15d
	movq	%rax, -176(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -168(%rbp)
	testb	$1, %al
	je	.L2363
	cmpl	$3, %eax
	je	.L2363
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L2459
	cmpq	$1, %rdx
	je	.L2460
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2363:
	movl	$1, -160(%rbp)
.L2365:
	leaq	-192(%rbp), %r8
	movq	(%r12), %rsi
	movl	16(%rbp), %ecx
	movl	$1, %edx
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal19TransitionsAccessor16SearchTransitionENS0_4NameENS0_12PropertyKindENS0_18PropertyAttributesE@PLT
	movq	-264(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L2371
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2372
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L2373:
	movq	39(%rsi), %rax
	movl	15(%rsi), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	leal	(%rdx,%rdx,2), %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	15(%rax,%rdx), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2375
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2376:
	testb	$1, %sil
	jne	.L2378
.L2379:
	movq	0(%r13), %rax
	movl	%r15d, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	.LC55(%rip), %r8
	movzbl	14(%rax), %edx
	shrl	$3, %edx
	call	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc
	jmp	.L2362
	.p2align 4,,10
	.p2align 3
.L2456:
	leaq	-192(%rbp), %r13
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal10MapUpdaterC1EPNS0_7IsolateENS0_6HandleINS0_3MapEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal10MapUpdater6UpdateEv@PLT
	movq	%rax, %r13
	jmp	.L2360
	.p2align 4,,10
	.p2align 3
.L2371:
	movq	0(%r13), %rdx
	cmpl	$-1, %ebx
	je	.L2381
	movq	39(%rdx), %rcx
	movl	15(%rdx), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	leal	-1(%rax), %esi
	cmpl	%esi, %ebx
	je	.L2382
	movzbl	14(%rdx), %edx
	movl	%r15d, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	.LC57(%rip), %r8
	shrl	$3, %edx
	call	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc
	jmp	.L2362
	.p2align 4,,10
	.p2align 3
.L2455:
	movq	40960(%r14), %rax
	leaq	-232(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2359
	.p2align 4,,10
	.p2align 3
.L2457:
	leaq	-232(%rbp), %rsi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-248(%rbp), %rax
	jmp	.L2408
	.p2align 4,,10
	.p2align 3
.L2375:
	movq	41088(%r14), %rax
	cmpq	%rax, 41096(%r14)
	je	.L2461
.L2377:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2372:
	movq	41088(%r14), %rbx
	cmpq	41096(%r14), %rbx
	je	.L2462
.L2374:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rbx)
	jmp	.L2373
	.p2align 4,,10
	.p2align 3
.L2381:
	movl	15(%rdx), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	$1019, %eax
	jg	.L2394
	movq	%rdx, -192(%rbp)
	movzbl	9(%rdx), %eax
	cmpl	$2, %eax
	jg	.L2463
.L2395:
	testl	%eax, %eax
	je	.L2464
.L2399:
	movq	%r14, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal7Factory15NewAccessorPairEv@PLT
	movq	-264(%rbp), %r8
	movq	%rax, %r15
.L2393:
	movq	-256(%rbp), %rax
	movq	(%r15), %rbx
	movq	(%rax), %r9
	movq	-248(%rbp), %rax
	movq	(%rax), %rdx
	testb	$1, %dl
	jne	.L2465
	movq	%r8, -256(%rbp)
	leaq	7(%rbx), %rsi
	movq	%rbx, %rdi
	movq	%r9, -248(%rbp)
	movq	%rdx, 7(%rbx)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-248(%rbp), %r9
	movq	-256(%rbp), %r8
.L2401:
	testb	$1, %r9b
	jne	.L2466
	movq	%r8, -248(%rbp)
	leaq	15(%rbx), %rsi
	movq	%r9, %rdx
	movq	%rbx, %rdi
	movq	%r9, 15(%rbx)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-248(%rbp), %r8
.L2405:
	movq	40936(%r14), %rax
	movl	16(%rbp), %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	xorl	%ebx, %ebx
	movq	%r8, -248(%rbp)
	movl	8(%rax), %eax
	testl	%eax, %eax
	setne	%bl
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movl	%ebx, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-248(%rbp), %r8
	movq	%r8, %rdx
	call	_ZN2v88internal3Map20CopyInsertDescriptorEPNS0_7IsolateENS0_6HandleIS1_EEPNS0_10DescriptorENS0_14TransitionFlagE
	jmp	.L2362
	.p2align 4,,10
	.p2align 3
.L2378:
	movq	-1(%rsi), %rdx
	cmpw	$79, 11(%rdx)
	jne	.L2379
	movq	(%rax), %rax
	movq	-248(%rbp), %rcx
	movq	7(%rax), %rsi
	cmpq	%rsi, (%rcx)
	je	.L2467
.L2380:
	movq	0(%r13), %rax
	movl	%r15d, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	.LC56(%rip), %r8
	movzbl	14(%rax), %edx
	shrl	$3, %edx
	call	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc
	jmp	.L2362
	.p2align 4,,10
	.p2align 3
.L2464:
	movq	-192(%rbp), %rax
	movl	15(%rax), %edx
	andl	$1048576, %edx
	jne	.L2399
	movq	%r8, %rdi
	movzbl	7(%rax), %r11d
	movzbl	8(%rax), %ebx
	movq	%r8, -264(%rbp)
	call	_ZNK2v88internal3Map14GetFieldCountsEv
	movl	$128, %edi
	movq	%rax, %rcx
	movq	-192(%rbp), %rax
	movzbl	7(%rax), %edx
	movzbl	8(%rax), %esi
	movzbl	%r11b, %eax
	subl	%ebx, %eax
	movl	%ecx, %ebx
	movq	-264(%rbp), %r8
	cmpl	$128, %eax
	cmovl	%edi, %eax
	subl	%esi, %edx
	subl	%edx, %ebx
	cmpl	%ebx, %eax
	jl	.L2453
	movq	%rcx, %rax
	sarq	$32, %rax
	addl	%eax, %ecx
	cmpl	$1020, %ecx
	jle	.L2399
.L2453:
	movq	0(%r13), %rdx
	.p2align 4,,10
	.p2align 3
.L2394:
	movzbl	14(%rdx), %edx
	leaq	.LC62(%rip), %r8
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	shrl	$3, %edx
	call	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc
	jmp	.L2362
	.p2align 4,,10
	.p2align 3
.L2459:
	movl	$3, -160(%rbp)
	jmp	.L2365
	.p2align 4,,10
	.p2align 3
.L2382:
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	leaq	-1(%rcx,%rax), %rcx
	movq	8(%rcx), %rax
	sarq	$32, %rax
	testb	$1, %al
	je	.L2468
	shrl	$3, %eax
	andl	$7, %eax
	cmpl	%eax, 16(%rbp)
	je	.L2384
	movzbl	14(%rdx), %edx
	movl	%r15d, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	.LC59(%rip), %r8
	shrl	$3, %edx
	call	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc
	jmp	.L2362
	.p2align 4,,10
	.p2align 3
.L2460:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L2469
	movl	$4, -160(%rbp)
	jmp	.L2365
	.p2align 4,,10
	.p2align 3
.L2462:
	movq	%r14, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L2374
	.p2align 4,,10
	.p2align 3
.L2461:
	movq	%r14, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	jmp	.L2377
	.p2align 4,,10
	.p2align 3
.L2468:
	movzbl	14(%rdx), %edx
	movl	%r15d, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	.LC58(%rip), %r8
	shrl	$3, %edx
	call	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc
	jmp	.L2362
.L2463:
	movzbl	7(%rdx), %edx
	subl	%eax, %edx
	movl	%edx, %eax
	jmp	.L2395
.L2469:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -160(%rbp)
	jmp	.L2365
.L2384:
	movq	16(%rcx), %rbx
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2385
	movq	%rbx, %rsi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-264(%rbp), %r8
	movq	(%rax), %rbx
	movq	%rax, %rsi
.L2386:
	testb	$1, %bl
	jne	.L2470
.L2388:
	movq	0(%r13), %rax
	movl	%r15d, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	.LC60(%rip), %r8
	movzbl	14(%rax), %edx
	shrl	$3, %edx
	call	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc
	jmp	.L2362
.L2467:
	movq	-256(%rbp), %rcx
	movq	15(%rax), %rax
	cmpq	%rax, (%rcx)
	jne	.L2380
	movq	%rbx, %rax
	jmp	.L2362
.L2385:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L2471
.L2387:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%rbx, (%rsi)
	jmp	.L2386
.L2465:
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rcx
	cmpq	-37488(%rcx), %rdx
	je	.L2401
	movq	%r8, -280(%rbp)
	leaq	7(%rbx), %rsi
	movq	%rbx, %rdi
	movq	%rax, -272(%rbp)
	movq	%r9, -264(%rbp)
	movq	%rdx, 7(%rbx)
	movq	%rdx, -256(%rbp)
	movq	%rsi, -248(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-272(%rbp), %rax
	movq	-248(%rbp), %rsi
	movq	-256(%rbp), %rdx
	movq	-264(%rbp), %r9
	testb	$24, 8(%rax)
	movq	-280(%rbp), %r8
	je	.L2401
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2401
	movq	%rbx, %rdi
	movq	%r8, -256(%rbp)
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-256(%rbp), %r8
	movq	-248(%rbp), %r9
	jmp	.L2401
.L2466:
	movq	%r9, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	cmpq	-37488(%rdx), %r9
	je	.L2405
	movq	%r8, -272(%rbp)
	leaq	15(%rbx), %rsi
	movq	%r9, %rdx
	movq	%rbx, %rdi
	movq	%rax, -264(%rbp)
	movq	%r9, 15(%rbx)
	movq	%r9, -256(%rbp)
	movq	%rsi, -248(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-264(%rbp), %rax
	movq	-248(%rbp), %rsi
	movq	-256(%rbp), %r9
	movq	-272(%rbp), %r8
	testb	$24, 8(%rax)
	je	.L2405
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2405
	movq	%r9, %rdx
	movq	%rbx, %rdi
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-248(%rbp), %r8
	jmp	.L2405
.L2470:
	movq	-1(%rbx), %rax
	cmpw	$79, 11(%rax)
	jne	.L2388
	movq	-256(%rbp), %rax
	movq	(%rsi), %rcx
	movq	(%rax), %rdx
	movq	-248(%rbp), %rax
	movq	7(%rcx), %r11
	movq	(%rax), %rdi
	cmpq	%r11, %rdi
	je	.L2472
	movq	104(%r14), %rax
	cmpq	%r11, %rax
	je	.L2392
	cmpq	%rax, %rdi
	je	.L2392
.L2414:
	movq	0(%r13), %rax
	movl	%r15d, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	.LC61(%rip), %r8
	movzbl	14(%rax), %edx
	shrl	$3, %edx
	call	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc
	jmp	.L2362
.L2471:
	movq	%r14, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L2387
.L2472:
	cmpq	15(%rcx), %rdx
	je	.L2454
	movq	104(%r14), %rax
.L2392:
	cmpq	%rax, %rdx
	je	.L2412
	movq	15(%rcx), %rcx
	cmpq	%rax, %rcx
	je	.L2412
	cmpq	%rcx, %rdx
	jne	.L2414
.L2412:
	movq	%r14, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal12AccessorPair4CopyEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-264(%rbp), %r8
	movq	%rax, %r15
	jmp	.L2393
.L2458:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18777:
	.size	_ZN2v88internal3Map28TransitionToAccessorPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEEiNS4_INS0_6ObjectEEES9_NS0_18PropertyAttributesE, .-_ZN2v88internal3Map28TransitionToAccessorPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEEiNS4_INS0_6ObjectEEES9_NS0_18PropertyAttributesE
	.section	.rodata._ZNSt5dequeIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE16_M_push_back_auxIJRKS2_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC63:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE16_M_push_back_auxIJRKS2_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE16_M_push_back_auxIJRKS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.type	_ZNSt5dequeIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE16_M_push_back_auxIJRKS2_EEEvDpOT_, @function
_ZNSt5dequeIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE16_M_push_back_auxIJRKS2_EEEvDpOT_:
.LFB22477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	movq	64(%rbx), %rdx
	subq	72(%rbx), %rdx
	movq	%r13, %r14
	movq	%rdx, %rcx
	subq	%rsi, %r14
	sarq	$3, %rcx
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	48(%rbx), %rax
	subq	32(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L2491
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L2492
.L2475:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L2483
	cmpq	$63, 8(%rax)
	ja	.L2493
.L2483:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L2494
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2484:
	movq	%rax, 8(%r13)
	movq	(%r12), %rdx
	movq	64(%rbx), %rax
	movq	%rdx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2492:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L2495
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L2496
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L2480:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L2481
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L2481:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L2482
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L2482:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L2478:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L2475
	.p2align 4,,10
	.p2align 3
.L2493:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L2484
	.p2align 4,,10
	.p2align 3
.L2495:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rcx,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L2477
	cmpq	%r13, %rsi
	je	.L2478
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L2478
	.p2align 4,,10
	.p2align 3
.L2477:
	cmpq	%r13, %rsi
	je	.L2478
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L2478
	.p2align 4,,10
	.p2align 3
.L2494:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2484
	.p2align 4,,10
	.p2align 3
.L2496:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L2480
.L2491:
	leaq	.LC63(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22477:
	.size	_ZNSt5dequeIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE16_M_push_back_auxIJRKS2_EEEvDpOT_, .-_ZNSt5dequeIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.section	.text._ZNSt11_Deque_baseIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE17_M_initialize_mapEm:
.LFB22784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$6, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L2511
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L2499:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L2500
	movq	%r15, %rbx
	jmp	.L2505
	.p2align 4,,10
	.p2align 3
.L2501:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L2512
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2502:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L2500
.L2505:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L2501
	cmpq	$63, 8(%rax)
	jbe	.L2501
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L2505
.L2500:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$63, %r13d
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 72(%r12)
	leaq	512(%rax), %rcx
	leaq	(%rax,%r13,8), %rax
	movq	%rcx, 80(%r12)
	movq	%rax, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2512:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2502
	.p2align 4,,10
	.p2align 3
.L2511:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2499
	.cfi_endproc
.LFE22784:
	.size	_ZNSt11_Deque_baseIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE17_M_initialize_mapEm
	.section	.rodata._ZN2v88internal3Map15UpdateFieldTypeEPNS0_7IsolateEiNS0_6HandleINS0_4NameEEENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE.str1.8,"aMS",@progbits,1
	.align 8
.LC64:
	.string	"../deps/v8/src/objects/map.cc:687"
	.section	.text._ZN2v88internal3Map15UpdateFieldTypeEPNS0_7IsolateEiNS0_6HandleINS0_4NameEEENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map15UpdateFieldTypeEPNS0_7IsolateEiNS0_6HandleINS0_4NameEEENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE
	.type	_ZN2v88internal3Map15UpdateFieldTypeEPNS0_7IsolateEiNS0_6HandleINS0_4NameEEENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE, @function
_ZN2v88internal3Map15UpdateFieldTypeEPNS0_7IsolateEiNS0_6HandleINS0_4NameEEENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE:
.LFB18704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rsi, -448(%rbp)
	movq	%rcx, -496(%rbp)
	movq	(%rdi), %rdi
	movb	%r9b, -449(%rbp)
	movq	%rax, -480(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	3(%rdx,%rdx,2), %eax
	sall	$3, %eax
	movslq	%eax, %rbx
	leaq	7(%rbx), %rax
	movq	%rbx, -472(%rbp)
	movq	%rax, -488(%rbp)
	movq	39(%rdi), %rax
	movq	7(%rbx,%rax), %rdx
	movq	%rdx, %rax
	sarq	$32, %rax
	btq	$33, %rdx
	jc	.L2513
	shrl	$2, %eax
	movl	%r8d, %r13d
	andl	$1, %eax
	cmpl	%eax, %r8d
	jne	.L2592
.L2517:
	movq	-448(%rbp), %rax
	leaq	.LC64(%rip), %rdx
	leaq	-256(%rbp), %r12
	movq	41136(%rax), %rsi
	leaq	-416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	%rbx, -256(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	pxor	%xmm0, %xmm0
	je	.L2593
	movdqa	-256(%rbp), %xmm3
	leaq	-160(%rbp), %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE17_M_initialize_mapEm
	movdqa	-112(%rbp), %xmm5
	movq	-200(%rbp), %r11
	movq	-208(%rbp), %xmm0
	movq	-168(%rbp), %rdi
	movq	-192(%rbp), %rcx
	movq	-184(%rbp), %rbx
	movaps	%xmm5, -208(%rbp)
	movq	%r11, %xmm5
	movdqa	-96(%rbp), %xmm6
	punpcklqdq	%xmm5, %xmm0
	movq	%rdi, -440(%rbp)
	movq	-216(%rbp), %r8
	movq	-176(%rbp), %rdx
	movaps	%xmm0, -304(%rbp)
	movq	%rcx, %xmm0
	movdqa	-128(%rbp), %xmm4
	movq	-248(%rbp), %rax
	movaps	%xmm6, -192(%rbp)
	movq	%rbx, %xmm6
	punpcklqdq	%xmm6, %xmm0
	movq	-240(%rbp), %r10
	movq	-224(%rbp), %xmm1
	movq	-256(%rbp), %xmm2
	movq	-232(%rbp), %r9
	movaps	%xmm4, -224(%rbp)
	movq	%rax, %xmm3
	movdqa	-80(%rbp), %xmm7
	movq	-144(%rbp), %rsi
	movaps	%xmm0, -288(%rbp)
	movq	%r8, %xmm4
	movq	-136(%rbp), %rdi
	movq	%rdx, %xmm0
	punpcklqdq	%xmm3, %xmm2
	punpcklqdq	%xmm4, %xmm1
	movhps	-440(%rbp), %xmm0
	movq	%rsi, -240(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%r10, -336(%rbp)
	movq	%r9, -328(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm2, -352(%rbp)
	movaps	%xmm1, -320(%rbp)
	movaps	%xmm0, -272(%rbp)
	testq	%rsi, %rsi
	je	.L2520
	movq	-168(%rbp), %rbx
	movq	-200(%rbp), %rdx
	leaq	8(%rbx), %rcx
	cmpq	%rdx, %rcx
	jbe	.L2521
	.p2align 4,,10
	.p2align 3
.L2524:
	testq	%rax, %rax
	je	.L2522
	cmpq	$64, 8(%rax)
	ja	.L2523
.L2522:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-248(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -248(%rbp)
.L2523:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L2524
	movq	-232(%rbp), %rdi
	movq	-240(%rbp), %rsi
.L2521:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L2525
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L2525:
	movq	-288(%rbp), %rcx
	movq	-272(%rbp), %rdx
.L2520:
	subq	$8, %rdx
	cmpq	%rdx, %rcx
	je	.L2526
.L2596:
	movq	(%r15), %rax
	movq	%rax, (%rcx)
	movq	-288(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -288(%rbp)
.L2527:
	leaq	-160(%rbp), %rdi
	movq	-320(%rbp), %rax
	movq	%rdi, -504(%rbp)
	leaq	-352(%rbp), %rdi
	movq	%rdi, -464(%rbp)
	leaq	-424(%rbp), %rdi
	movq	%rdi, -512(%rbp)
	cmpq	%rdx, %rax
	je	.L2557
	movl	%r14d, -456(%rbp)
	movl	%r13d, -440(%rbp)
.L2528:
	movq	-304(%rbp), %rdi
	movq	(%rax), %rbx
	leaq	-8(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L2531
	addq	$8, %rax
	movq	%rax, -320(%rbp)
.L2532:
	movq	-448(%rbp), %rax
	movq	%rbx, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	%rax, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	71(%rbx), %rax
	movq	%rax, -136(%rbp)
	testb	$1, %al
	je	.L2535
	cmpl	$3, %eax
	je	.L2535
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L2594
	cmpq	$1, %rdx
	je	.L2595
.L2591:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2592:
	movl	15(%rdi), %eax
	testl	$1048576, %eax
	je	.L2517
	call	_ZN2v88internal8JSObject25InvalidatePrototypeChainsENS0_3MapE@PLT
	jmp	.L2517
.L2593:
	movdqa	-256(%rbp), %xmm7
	movq	-176(%rbp), %rdx
	movq	$0, -336(%rbp)
	movq	-232(%rbp), %rax
	movdqa	-208(%rbp), %xmm1
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm4
	subq	$8, %rdx
	movaps	%xmm7, -352(%rbp)
	movq	-192(%rbp), %rcx
	movdqa	-224(%rbp), %xmm7
	movq	%rax, -328(%rbp)
	movaps	%xmm1, -304(%rbp)
	movaps	%xmm7, -320(%rbp)
	movaps	%xmm3, -288(%rbp)
	movaps	%xmm4, -272(%rbp)
	cmpq	%rdx, %rcx
	jne	.L2596
.L2526:
	leaq	-352(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNSt5dequeIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE16_M_push_back_auxIJRKS2_EEEvDpOT_
	movq	-288(%rbp), %rdx
	jmp	.L2527
	.p2align 4,,10
	.p2align 3
.L2535:
	movl	$1, -128(%rbp)
.L2537:
	movq	-504(%rbp), %rdi
	movl	$1, %r15d
	call	_ZN2v88internal19TransitionsAccessor19NumberOfTransitionsEv@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jle	.L2550
	.p2align 4,,10
	.p2align 3
.L2543:
	movl	-128(%rbp), %eax
	cmpl	$3, %eax
	jne	.L2597
	movq	-136(%rbp), %rax
	movl	%r15d, %r13d
	andq	$-3, %rax
.L2548:
	movq	-272(%rbp), %rsi
	movq	-288(%rbp), %rcx
	movq	%rax, -256(%rbp)
	leaq	-8(%rsi), %rdx
	cmpq	%rdx, %rcx
	je	.L2549
	movq	%rax, (%rcx)
	addl	$1, %r15d
	addq	$8, -288(%rbp)
	cmpl	%r13d, %r14d
	jg	.L2543
.L2550:
	movq	39(%rbx), %rdx
	movq	-488(%rbp), %rax
	movq	%rdx, -424(%rbp)
	movq	(%rdx,%rax), %rax
	movq	%rax, %rcx
	shrq	$34, %rax
	sarq	$32, %rcx
	andl	$1, %eax
	cmpl	%eax, -440(%rbp)
	jne	.L2544
	movl	%ecx, %eax
	shrl	$6, %eax
	andl	$7, %eax
	cmpb	%al, -449(%rbp)
	je	.L2598
.L2544:
	movq	-424(%rbp), %rax
	movq	-472(%rbp), %rbx
	shrl	$3, %ecx
	movq	%r12, %rdi
	subq	$8, %rsp
	andl	$7, %ecx
	movq	7(%rbx,%rax), %rdx
	pushq	-480(%rbp)
	movzbl	-449(%rbp), %r9d
	movl	-440(%rbp), %r8d
	movq	-496(%rbp), %rsi
	shrq	$51, %rdx
	andl	$1023, %edx
	call	_ZN2v88internal10Descriptor9DataFieldENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE@PLT
	movl	-456(%rbp), %esi
	movq	-512(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal15DescriptorArray7ReplaceEiPNS0_10DescriptorE@PLT
	popq	%rax
	popq	%rdx
.L2552:
	movq	-320(%rbp), %rax
	cmpq	%rax, -288(%rbp)
	jne	.L2528
.L2557:
	movq	-336(%rbp), %rax
	testq	%rax, %rax
	je	.L2530
	movq	-264(%rbp), %rdi
	movq	-296(%rbp), %rdx
	leaq	8(%rdi), %rcx
	cmpq	%rdx, %rcx
	jbe	.L2558
	movq	-344(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L2561:
	testq	%rax, %rax
	je	.L2559
	cmpq	$64, 8(%rax)
	ja	.L2560
.L2559:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-344(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -344(%rbp)
.L2560:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L2561
	movq	-336(%rbp), %rax
.L2558:
	movq	-328(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L2530
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L2530:
	movq	-520(%rbp), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
.L2513:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2599
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2597:
	.cfi_restore_state
	cmpl	$4, %eax
	jne	.L2591
	movl	%r15d, %eax
	movq	-136(%rbp), %rdx
	movl	%r15d, %r13d
	sall	$4, %eax
	addl	$24, %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	andq	$-3, %rax
	jmp	.L2548
	.p2align 4,,10
	.p2align 3
.L2549:
	movq	-464(%rbp), %rdi
	movq	%r12, %rsi
	addl	$1, %r15d
	call	_ZNSt5dequeIN2v88internal3MapENS1_22RecyclingZoneAllocatorIS2_EEE16_M_push_back_auxIJRKS2_EEEvDpOT_
	cmpl	%r13d, %r14d
	jg	.L2543
	jmp	.L2550
.L2531:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L2533
	cmpq	$64, 8(%rax)
	ja	.L2534
.L2533:
	movq	-312(%rbp), %rax
	movq	$64, 8(%rax)
	movq	-344(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rax, -344(%rbp)
.L2534:
	movq	-296(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -296(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -312(%rbp)
	movq	%rdx, -304(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L2532
.L2598:
	movq	-480(%rbp), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L2600
	movq	(%rax), %rbx
	movq	-472(%rbp), %rax
	movq	15(%rax,%rdx), %rdi
	cmpl	$3, %edi
	je	.L2601
	movq	%rdi, %rax
	movq	%rcx, -528(%rbp)
	andl	$3, %eax
	cmpq	$3, %rax
	je	.L2556
.L2590:
	call	_ZN2v88internal9FieldType4castENS0_6ObjectE@PLT
	movq	-528(%rbp), %rcx
.L2555:
	cmpq	%rax, %rbx
	jne	.L2544
	jmp	.L2552
.L2594:
	movl	$3, -128(%rbp)
	jmp	.L2537
.L2556:
	andq	$-3, %rdi
	jmp	.L2590
.L2595:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L2602
	movl	$4, -128(%rbp)
	jmp	.L2537
.L2601:
	movq	%rcx, -528(%rbp)
	call	_ZN2v88internal9FieldType4NoneEv@PLT
	movq	-528(%rbp), %rcx
	jmp	.L2555
.L2602:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -128(%rbp)
	jmp	.L2537
.L2600:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L2599:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18704:
	.size	_ZN2v88internal3Map15UpdateFieldTypeEPNS0_7IsolateEiNS0_6HandleINS0_4NameEEENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE, .-_ZN2v88internal3Map15UpdateFieldTypeEPNS0_7IsolateEiNS0_6HandleINS0_4NameEEENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE
	.section	.rodata._ZN2v88internal3Map15GeneralizeFieldEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS0_14RepresentationENS4_INS0_9FieldTypeEEE.str1.1,"aMS",@progbits,1
.LC65:
	.string	"field type generalization"
	.section	.text._ZN2v88internal3Map15GeneralizeFieldEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS0_14RepresentationENS4_INS0_9FieldTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal3Map15GeneralizeFieldEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS0_14RepresentationENS4_INS0_9FieldTypeEEE
	.type	_ZN2v88internal3Map15GeneralizeFieldEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS0_14RepresentationENS4_INS0_9FieldTypeEEE, @function
_ZN2v88internal3Map15GeneralizeFieldEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS0_14RepresentationENS4_INS0_9FieldTypeEEE:
.LFB18716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movl	%ecx, -100(%rbp)
	movq	41112(%rdi), %rdi
	movq	%rsi, -152(%rbp)
	movb	%r8b, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2604
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2605:
	leal	3(%r13,%r13,2), %ebx
	sall	$3, %ebx
	movslq	%ebx, %rbx
	movq	7(%rbx,%rsi), %rdx
	movq	(%rax), %rax
	movq	%rdx, %rcx
	shrq	$38, %rdx
	shrq	$34, %rcx
	andl	$7, %edx
	andl	$1, %ecx
	movl	%edx, -144(%rbp)
	movl	%ecx, -120(%rbp)
	movq	15(%rbx,%rax), %rdi
	cmpl	$3, %edi
	je	.L2642
	movq	%rdi, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	jne	.L2641
	andq	$-3, %rdi
.L2641:
	call	_ZN2v88internal9FieldType4castENS0_6ObjectE@PLT
	movq	%rax, %rsi
.L2608:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2610
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -136(%rbp)
.L2611:
	movl	-120(%rbp), %esi
	leaq	-80(%rbp), %rax
	testl	%esi, %esi
	je	.L2613
	cmpl	$1, -100(%rbp)
	jne	.L2643
.L2613:
	movq	%rax, -128(%rbp)
	movzbl	-144(%rbp), %edx
	cmpb	%dl, -112(%rbp)
	je	.L2644
.L2616:
	movq	-152(%rbp), %rax
	movq	-128(%rbp), %rdi
	movq	%r15, %rsi
	movl	%r13d, %edx
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal3Map14FindFieldOwnerEPNS0_7IsolateEi
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2645
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L2619:
	movq	(%r14), %rax
	movq	41112(%r15), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2621
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r11
.L2622:
	movq	-136(%rbp), %rsi
	movzbl	-144(%rbp), %edi
	movq	%r12, %rcx
	movq	%r15, %r8
	movzbl	-112(%rbp), %edx
	movq	%r11, -160(%rbp)
	call	_ZN2v88internal3Map19GeneralizeFieldTypeENS0_14RepresentationENS0_6HandleINS0_9FieldTypeEEES2_S5_PNS0_7IsolateE
	movl	-120(%rbp), %ecx
	movq	-160(%rbp), %r11
	movq	%rax, %r12
	movl	$0, %eax
	testl	%ecx, %ecx
	cmovne	-100(%rbp), %eax
	movl	%eax, -100(%rbp)
	movq	(%r11), %rax
	movq	7(%rbx,%rax), %rcx
	movq	%rcx, -144(%rbp)
	movq	-1(%rbx,%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2625
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L2626:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal3Map13WrapFieldTypeEPNS0_7IsolateENS0_6HandleINS0_9FieldTypeEEE
	subq	$8, %rsp
	movl	-100(%rbp), %r8d
	movq	%rbx, %rcx
	movq	%rdx, -72(%rbp)
	movzbl	-112(%rbp), %r9d
	leaq	-88(%rbp), %rdi
	movl	%r13d, %edx
	movl	%eax, -80(%rbp)
	movq	(%r14), %rax
	movq	%r15, %rsi
	pushq	-128(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal3Map15UpdateFieldTypeEPNS0_7IsolateEiNS0_6HandleINS0_4NameEEENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE
	movq	(%r14), %rax
	movq	-112(%rbp), %rdi
	movq	%r15, %rsi
	movl	$3, %edx
	movq	55(%rax), %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE@PLT
	cmpb	$0, _ZN2v88internal25FLAG_trace_generalizationE(%rip)
	popq	%rax
	movq	-112(%rbp), %rdi
	popq	%rdx
	jne	.L2646
.L2603:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2647
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2643:
	.cfi_restore_state
	movq	%rax, -128(%rbp)
	jmp	.L2616
	.p2align 4,,10
	.p2align 3
.L2610:
	movq	41088(%r15), %rax
	movq	%rax, -136(%rbp)
	cmpq	%rax, 41096(%r15)
	je	.L2648
.L2612:
	movq	-136(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L2611
	.p2align 4,,10
	.p2align 3
.L2604:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L2649
.L2606:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2605
	.p2align 4,,10
	.p2align 3
.L2625:
	movq	41088(%r15), %rbx
	cmpq	41096(%r15), %rbx
	je	.L2650
.L2627:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	jmp	.L2626
	.p2align 4,,10
	.p2align 3
.L2621:
	movq	41088(%r15), %r11
	cmpq	41096(%r15), %r11
	je	.L2651
.L2623:
	leaq	8(%r11), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r11)
	jmp	.L2622
	.p2align 4,,10
	.p2align 3
.L2645:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L2652
.L2620:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L2619
	.p2align 4,,10
	.p2align 3
.L2646:
	movq	-152(%rbp), %rax
	movl	%r13d, %r8d
	movq	%r15, %rsi
	movq	(%rax), %rcx
	movq	-144(%rbp), %rax
	movq	%rcx, -88(%rbp)
	movl	15(%rcx), %edx
	shrq	$38, %rax
	movl	15(%rcx), %r9d
	movl	-100(%rbp), %ebx
	pushq	$0
	andl	$7, %eax
	pushq	%r12
	leaq	.LC65(%rip), %rcx
	pushq	$0
	shrl	$10, %r9d
	pushq	-136(%rbp)
	andl	$1023, %r9d
	pushq	%rbx
	movl	-120(%rbp), %ebx
	pushq	%rbx
	pushq	%rax
	pushq	%rax
	movl	%edx, %eax
	movq	stdout(%rip), %rdx
	shrl	$10, %eax
	pushq	$0
	andl	$1023, %eax
	pushq	%rax
	call	_ZN2v88internal3Map19PrintGeneralizationEPNS0_7IsolateEP8_IO_FILEPKciiibNS0_14RepresentationES8_NS0_17PropertyConstnessES9_NS0_11MaybeHandleINS0_9FieldTypeEEENSA_INS0_6ObjectEEESC_SE_
	addq	$80, %rsp
	jmp	.L2603
	.p2align 4,,10
	.p2align 3
.L2642:
	call	_ZN2v88internal9FieldType4NoneEv@PLT
	movq	%rax, %rsi
	jmp	.L2608
	.p2align 4,,10
	.p2align 3
.L2644:
	movq	(%r12), %r14
	call	_ZN2v88internal9FieldType4NoneEv@PLT
	cmpb	$3, -112(%rbp)
	jne	.L2630
	cmpq	%rax, %r14
	je	.L2616
.L2630:
	movq	(%r12), %rax
	movq	-136(%rbp), %rsi
	movq	%rax, -80(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal9FieldType5NowIsENS0_6HandleIS1_EE@PLT
	testb	%al, %al
	je	.L2616
	jmp	.L2603
	.p2align 4,,10
	.p2align 3
.L2649:
	movq	%r15, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	jmp	.L2606
	.p2align 4,,10
	.p2align 3
.L2648:
	movq	%r15, %rdi
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	%rax, -136(%rbp)
	jmp	.L2612
	.p2align 4,,10
	.p2align 3
.L2650:
	movq	%r15, %rdi
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L2627
	.p2align 4,,10
	.p2align 3
.L2652:
	movq	%r15, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L2620
	.p2align 4,,10
	.p2align 3
.L2651:
	movq	%r15, %rdi
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movq	%rax, %r11
	jmp	.L2623
.L2647:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18716:
	.size	_ZN2v88internal3Map15GeneralizeFieldEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS0_14RepresentationENS4_INS0_9FieldTypeEEE, .-_ZN2v88internal3Map15GeneralizeFieldEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS0_14RepresentationENS4_INS0_9FieldTypeEEE
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal3Map24GetPrototypeChainRootMapEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal3Map24GetPrototypeChainRootMapEPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZNK2v88internal3Map24GetPrototypeChainRootMapEPNS0_7IsolateE:
.LFB22997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22997:
	.size	_GLOBAL__sub_I__ZNK2v88internal3Map24GetPrototypeChainRootMapEPNS0_7IsolateE, .-_GLOBAL__sub_I__ZNK2v88internal3Map24GetPrototypeChainRootMapEPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal3Map24GetPrototypeChainRootMapEPNS0_7IsolateE
	.hidden	_ZTCN2v88internal8OFStreamE0_So
	.weak	_ZTCN2v88internal8OFStreamE0_So
	.section	.rodata._ZTCN2v88internal8OFStreamE0_So,"aG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal8OFStreamE0_So, @object
	.size	_ZTCN2v88internal8OFStreamE0_So, 80
_ZTCN2v88internal8OFStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal8OFStreamE, @object
	.size	_ZTTN2v88internal8OFStreamE, 32
_ZTTN2v88internal8OFStreamE:
	.quad	_ZTVN2v88internal8OFStreamE+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+64
	.quad	_ZTVN2v88internal8OFStreamE+64
	.weak	_ZTVN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal8OFStreamE, @object
	.size	_ZTVN2v88internal8OFStreamE, 80
_ZTVN2v88internal8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8OFStreamD1Ev
	.quad	_ZN2v88internal8OFStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
