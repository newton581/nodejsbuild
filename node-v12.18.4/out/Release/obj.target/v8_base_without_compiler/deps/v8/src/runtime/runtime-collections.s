	.file	"runtime-collections.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L14
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL34Stats_Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC1:
	.string	"V8.Runtime_Runtime_WeakCollectionDelete"
	.section	.rodata._ZN2v88internalL34Stats_Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"args[0].IsJSWeakCollection()"
.LC3:
	.string	"Check failed: %s."
.LC4:
	.string	"args[2].IsSmi()"
	.section	.text._ZN2v88internalL34Stats_Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateE, @function
_ZN2v88internalL34Stats_Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateE:
.LFB20258:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L52
.L16:
	movq	_ZZN2v88internalL34Stats_Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateEE27trace_event_unique_atomic74(%rip), %rbx
	testq	%rbx, %rbx
	je	.L53
.L18:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L54
.L20:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L24
.L26:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L55
.L19:
	movq	%rbx, _ZZN2v88internalL34Stats_Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateEE27trace_event_unique_atomic74(%rip)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L24:
	movq	-1(%rax), %rdx
	cmpw	$1084, 11(%rdx)
	jne	.L56
.L25:
	movq	-16(%r13), %rdx
	leaq	-8(%r13), %rsi
	testb	$1, %dl
	jne	.L57
	sarq	$32, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal16JSWeakCollection6DeleteENS0_6HandleIS1_EENS2_INS0_6ObjectEEEi@PLT
	testb	%al, %al
	je	.L28
	movq	112(%r12), %r13
.L29:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L32
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L32:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L58
.L15:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L59
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	120(%r12), %r13
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L54:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L60
.L21:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	leaq	.LC1(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L56:
	movq	-1(%rax), %rax
	cmpw	$1085, 11(%rax)
	jne	.L26
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L52:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$253, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	.LC4(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L58:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L60:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L21
.L59:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20258:
	.size	_ZN2v88internalL34Stats_Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateE, .-_ZN2v88internalL34Stats_Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL21Stats_Runtime_TheHoleEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC5:
	.string	"V8.Runtime_Runtime_TheHole"
	.section	.text._ZN2v88internalL21Stats_Runtime_TheHoleEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL21Stats_Runtime_TheHoleEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL21Stats_Runtime_TheHoleEiPmPNS0_7IsolateE.isra.0:
.LFB25358:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L90
.L62:
	movq	_ZZN2v88internalL21Stats_Runtime_TheHoleEiPmPNS0_7IsolateEE27trace_event_unique_atomic17(%rip), %rbx
	testq	%rbx, %rbx
	je	.L91
.L64:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L92
.L66:
	leaq	-144(%rbp), %rdi
	movq	96(%r12), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L93
.L61:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L94
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L95
.L65:
	movq	%rbx, _ZZN2v88internalL21Stats_Runtime_TheHoleEiPmPNS0_7IsolateEE27trace_event_unique_atomic17(%rip)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L92:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L96
.L67:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L68
	movq	(%rdi), %rax
	call	*8(%rax)
.L68:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L69
	movq	(%rdi), %rax
	call	*8(%rax)
.L69:
	leaq	.LC5(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L93:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L90:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$252, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L96:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L65
.L94:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25358:
	.size	_ZN2v88internalL21Stats_Runtime_TheHoleEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL21Stats_Runtime_TheHoleEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_WeakCollectionSetEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"V8.Runtime_Runtime_WeakCollectionSet"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_WeakCollectionSetEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC7:
	.string	"args[3].IsSmi()"
	.section	.text._ZN2v88internalL31Stats_Runtime_WeakCollectionSetEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_WeakCollectionSetEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_WeakCollectionSetEiPmPNS0_7IsolateE.isra.0:
.LFB25366:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L131
.L98:
	movq	_ZZN2v88internalL31Stats_Runtime_WeakCollectionSetEiPmPNS0_7IsolateEE27trace_event_unique_atomic96(%rip), %r13
	testq	%r13, %r13
	je	.L132
.L100:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L133
.L102:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L106
.L108:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L132:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L134
.L101:
	movq	%r13, _ZZN2v88internalL31Stats_Runtime_WeakCollectionSetEiPmPNS0_7IsolateEE27trace_event_unique_atomic96(%rip)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L106:
	movq	-1(%rax), %rdx
	cmpw	$1084, 11(%rdx)
	jne	.L135
.L107:
	movq	-24(%rbx), %rcx
	leaq	-8(%rbx), %rsi
	leaq	-16(%rbx), %rdx
	testb	$1, %cl
	jne	.L136
	sarq	$32, %rcx
	movq	%rbx, %rdi
	call	_ZN2v88internal16JSWeakCollection3SetENS0_6HandleIS1_EENS2_INS0_6ObjectEEES5_i@PLT
	movq	(%rbx), %r15
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L110
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L110:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L137
.L97:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L138
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L139
.L103:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L104
	movq	(%rdi), %rax
	call	*8(%rax)
.L104:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L105
	movq	(%rdi), %rax
	call	*8(%rax)
.L105:
	leaq	.LC6(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L135:
	movq	-1(%rax), %rax
	cmpw	$1085, 11(%rax)
	jne	.L108
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L131:
	movq	40960(%rsi), %rax
	movl	$254, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L136:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L137:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L134:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L139:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L103
.L138:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25366:
	.size	_ZN2v88internalL31Stats_Runtime_WeakCollectionSetEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_WeakCollectionSetEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL21Stats_Runtime_SetGrowEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC8:
	.string	"V8.Runtime_Runtime_SetGrow"
.LC9:
	.string	"args[0].IsJSSet()"
	.section	.text._ZN2v88internalL21Stats_Runtime_SetGrowEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL21Stats_Runtime_SetGrowEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL21Stats_Runtime_SetGrowEiPmPNS0_7IsolateE.isra.0:
.LFB25367:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L189
.L141:
	movq	_ZZN2v88internalL21Stats_Runtime_SetGrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic23(%rip), %rbx
	testq	%rbx, %rbx
	je	.L190
.L143:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L191
.L145:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L192
.L149:
	leaq	.LC9(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L190:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L193
.L144:
	movq	%rbx, _ZZN2v88internalL21Stats_Runtime_SetGrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic23(%rip)
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L192:
	movq	-1(%rax), %rdx
	cmpw	$1077, 11(%rdx)
	jne	.L149
	movq	41112(%r12), %rdi
	movq	23(%rax), %r15
	testq	%rdi, %rdi
	je	.L194
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE@PLT
	testq	%rax, %rax
	je	.L195
.L154:
	movq	0(%r13), %r15
	movq	(%rax), %r13
	movq	%r13, 23(%r15)
	leaq	23(%r15), %rsi
	testb	$1, %r13b
	je	.L162
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -168(%rbp)
	testl	$262144, %eax
	je	.L157
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %rsi
	movq	8(%rcx), %rax
.L157:
	testb	$24, %al
	je	.L162
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L196
	.p2align 4,,10
	.p2align 3
.L162:
	movq	88(%r12), %r13
.L155:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L161
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L161:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L197
.L140:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L198
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L199
.L153:
	leaq	8(%rsi), %rax
	movq	%r12, %rdi
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE@PLT
	testq	%rax, %rax
	jne	.L154
.L195:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$221, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L191:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L200
.L146:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L147
	movq	(%rdi), %rax
	call	*8(%rax)
.L147:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L148
	movq	(%rdi), %rax
	call	*8(%rax)
.L148:
	leaq	.LC8(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L196:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L189:
	movq	40960(%rsi), %rax
	movl	$250, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L197:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L199:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L200:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L193:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L144
.L198:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25367:
	.size	_ZN2v88internalL21Stats_Runtime_SetGrowEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL21Stats_Runtime_SetGrowEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL23Stats_Runtime_SetShrinkEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC10:
	.string	"V8.Runtime_Runtime_SetShrink"
	.section	.text._ZN2v88internalL23Stats_Runtime_SetShrinkEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL23Stats_Runtime_SetShrinkEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL23Stats_Runtime_SetShrinkEiPmPNS0_7IsolateE.isra.0:
.LFB25368:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L248
.L202:
	movq	_ZZN2v88internalL23Stats_Runtime_SetShrinkEiPmPNS0_7IsolateEE27trace_event_unique_atomic39(%rip), %rbx
	testq	%rbx, %rbx
	je	.L249
.L204:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L250
.L206:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L251
.L210:
	leaq	.LC9(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L249:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L252
.L205:
	movq	%rbx, _ZZN2v88internalL23Stats_Runtime_SetShrinkEiPmPNS0_7IsolateEE27trace_event_unique_atomic39(%rip)
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L251:
	movq	-1(%rax), %rdx
	cmpw	$1077, 11(%rdx)
	jne	.L210
	movq	41112(%r12), %rdi
	movq	23(%rax), %r15
	testq	%rdi, %rdi
	je	.L253
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L213:
	movq	%r12, %rdi
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE@PLT
	movq	0(%r13), %r15
	movq	(%rax), %r13
	leaq	23(%r15), %rsi
	movq	%r13, 23(%r15)
	testb	$1, %r13b
	je	.L218
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -168(%rbp)
	testl	$262144, %eax
	je	.L216
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %rsi
	movq	8(%rcx), %rax
.L216:
	testb	$24, %al
	je	.L218
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L254
	.p2align 4,,10
	.p2align 3
.L218:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r13
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L221
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L221:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L255
.L201:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L256
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L257
.L214:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L250:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L258
.L207:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L208
	movq	(%rdi), %rax
	call	*8(%rax)
.L208:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L209
	movq	(%rdi), %rax
	call	*8(%rax)
.L209:
	leaq	.LC10(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L254:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L248:
	movq	40960(%rsi), %rax
	movl	$251, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L255:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L257:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L258:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L252:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L205
.L256:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25368:
	.size	_ZN2v88internalL23Stats_Runtime_SetShrinkEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL23Stats_Runtime_SetShrinkEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL23Stats_Runtime_MapShrinkEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC11:
	.string	"V8.Runtime_Runtime_MapShrink"
.LC12:
	.string	"args[0].IsJSMap()"
	.section	.text._ZN2v88internalL23Stats_Runtime_MapShrinkEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL23Stats_Runtime_MapShrinkEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL23Stats_Runtime_MapShrinkEiPmPNS0_7IsolateE.isra.0:
.LFB25369:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L306
.L260:
	movq	_ZZN2v88internalL23Stats_Runtime_MapShrinkEiPmPNS0_7IsolateEE27trace_event_unique_atomic49(%rip), %rbx
	testq	%rbx, %rbx
	je	.L307
.L262:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L308
.L264:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L309
.L268:
	leaq	.LC12(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L307:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L310
.L263:
	movq	%rbx, _ZZN2v88internalL23Stats_Runtime_MapShrinkEiPmPNS0_7IsolateEE27trace_event_unique_atomic49(%rip)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L309:
	movq	-1(%rax), %rdx
	cmpw	$1069, 11(%rdx)
	jne	.L268
	movq	41112(%r12), %rdi
	movq	23(%rax), %r15
	testq	%rdi, %rdi
	je	.L311
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L271:
	movq	%r12, %rdi
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE@PLT
	movq	0(%r13), %r15
	movq	(%rax), %r13
	leaq	23(%r15), %rsi
	movq	%r13, 23(%r15)
	testb	$1, %r13b
	je	.L276
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -168(%rbp)
	testl	$262144, %eax
	je	.L274
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %rsi
	movq	8(%rcx), %rax
.L274:
	testb	$24, %al
	je	.L276
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L312
	.p2align 4,,10
	.p2align 3
.L276:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r13
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L279
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L279:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L313
.L259:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L314
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L315
.L272:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L308:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L316
.L265:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L266
	movq	(%rdi), %rax
	call	*8(%rax)
.L266:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L267
	movq	(%rdi), %rax
	call	*8(%rax)
.L267:
	leaq	.LC11(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L312:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L306:
	movq	40960(%rsi), %rax
	movl	$249, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L313:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L315:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L316:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L310:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L263
.L314:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25369:
	.size	_ZN2v88internalL23Stats_Runtime_MapShrinkEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL23Stats_Runtime_MapShrinkEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL21Stats_Runtime_MapGrowEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC13:
	.string	"V8.Runtime_Runtime_MapGrow"
	.section	.text._ZN2v88internalL21Stats_Runtime_MapGrowEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL21Stats_Runtime_MapGrowEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL21Stats_Runtime_MapGrowEiPmPNS0_7IsolateE.isra.0:
.LFB25370:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L366
.L318:
	movq	_ZZN2v88internalL21Stats_Runtime_MapGrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic59(%rip), %rbx
	testq	%rbx, %rbx
	je	.L367
.L320:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L368
.L322:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L369
.L326:
	leaq	.LC12(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L367:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L370
.L321:
	movq	%rbx, _ZZN2v88internalL21Stats_Runtime_MapGrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic59(%rip)
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L369:
	movq	-1(%rax), %rdx
	cmpw	$1069, 11(%rdx)
	jne	.L326
	movq	41112(%r12), %rdi
	movq	23(%rax), %r15
	testq	%rdi, %rdi
	je	.L371
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE@PLT
	testq	%rax, %rax
	je	.L372
.L331:
	movq	0(%r13), %r15
	movq	(%rax), %r13
	movq	%r13, 23(%r15)
	leaq	23(%r15), %rsi
	testb	$1, %r13b
	je	.L339
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -168(%rbp)
	testl	$262144, %eax
	je	.L334
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %rsi
	movq	8(%rcx), %rax
.L334:
	testb	$24, %al
	je	.L339
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L373
	.p2align 4,,10
	.p2align 3
.L339:
	movq	88(%r12), %r13
.L332:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L338
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L338:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L374
.L317:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L375
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L376
.L330:
	leaq	8(%rsi), %rax
	movq	%r12, %rdi
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE@PLT
	testq	%rax, %rax
	jne	.L331
.L372:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$221, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L368:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L377
.L323:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L324
	movq	(%rdi), %rax
	call	*8(%rax)
.L324:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L325
	movq	(%rdi), %rax
	call	*8(%rax)
.L325:
	leaq	.LC13(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L373:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L366:
	movq	40960(%rsi), %rax
	movl	$248, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L374:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L376:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L377:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L370:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L321
.L375:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25370:
	.size	_ZN2v88internalL21Stats_Runtime_MapGrowEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL21Stats_Runtime_MapGrowEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal15Runtime_TheHoleEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal15Runtime_TheHoleEiPmPNS0_7IsolateE
	.type	_ZN2v88internal15Runtime_TheHoleEiPmPNS0_7IsolateE, @function
_ZN2v88internal15Runtime_TheHoleEiPmPNS0_7IsolateE:
.LFB20244:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L380
	movq	96(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL21Stats_Runtime_TheHoleEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20244:
	.size	_ZN2v88internal15Runtime_TheHoleEiPmPNS0_7IsolateE, .-_ZN2v88internal15Runtime_TheHoleEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal15Runtime_SetGrowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal15Runtime_SetGrowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal15Runtime_SetGrowEiPmPNS0_7IsolateE, @function
_ZN2v88internal15Runtime_SetGrowEiPmPNS0_7IsolateE:
.LFB20247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L404
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	testb	$1, %al
	jne	.L405
.L383:
	leaq	.LC9(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L405:
	movq	-1(%rax), %rdx
	cmpw	$1077, 11(%rdx)
	jne	.L383
	movq	41112(%r12), %rdi
	movq	23(%rax), %r15
	testq	%rdi, %rdi
	je	.L406
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE@PLT
	testq	%rax, %rax
	je	.L407
.L388:
	movq	0(%r13), %r15
	movq	(%rax), %r13
	movq	%r13, 23(%r15)
	leaq	23(%r15), %rsi
	testb	$1, %r13b
	je	.L395
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L391
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L391:
	testb	$24, %al
	je	.L395
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L408
	.p2align 4,,10
	.p2align 3
.L395:
	movq	88(%r12), %r13
.L389:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L381
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L381:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L409
.L387:
	leaq	8(%rsi), %rax
	movq	%r12, %rdi
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE@PLT
	testq	%rax, %rax
	jne	.L388
.L407:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$221, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L408:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L404:
	addq	$24, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL21Stats_Runtime_SetGrowEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L387
	.cfi_endproc
.LFE20247:
	.size	_ZN2v88internal15Runtime_SetGrowEiPmPNS0_7IsolateE, .-_ZN2v88internal15Runtime_SetGrowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal17Runtime_SetShrinkEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17Runtime_SetShrinkEiPmPNS0_7IsolateE
	.type	_ZN2v88internal17Runtime_SetShrinkEiPmPNS0_7IsolateE, @function
_ZN2v88internal17Runtime_SetShrinkEiPmPNS0_7IsolateE:
.LFB20250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L431
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	testb	$1, %al
	jne	.L432
.L412:
	leaq	.LC9(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L432:
	movq	-1(%rax), %rdx
	cmpw	$1077, 11(%rdx)
	jne	.L412
	movq	41112(%r12), %rdi
	movq	23(%rax), %r15
	testq	%rdi, %rdi
	je	.L433
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L415:
	movq	%r12, %rdi
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE@PLT
	movq	0(%r13), %r15
	movq	(%rax), %r13
	leaq	23(%r15), %rsi
	movq	%r13, 23(%r15)
	testb	$1, %r13b
	je	.L420
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L418
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L418:
	testb	$24, %al
	je	.L420
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L434
	.p2align 4,,10
	.p2align 3
.L420:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r13
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L410
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L410:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L435
.L416:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L434:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L431:
	addq	$24, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL23Stats_Runtime_SetShrinkEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L435:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L416
	.cfi_endproc
.LFE20250:
	.size	_ZN2v88internal17Runtime_SetShrinkEiPmPNS0_7IsolateE, .-_ZN2v88internal17Runtime_SetShrinkEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal17Runtime_MapShrinkEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17Runtime_MapShrinkEiPmPNS0_7IsolateE
	.type	_ZN2v88internal17Runtime_MapShrinkEiPmPNS0_7IsolateE, @function
_ZN2v88internal17Runtime_MapShrinkEiPmPNS0_7IsolateE:
.LFB20253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L457
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	testb	$1, %al
	jne	.L458
.L438:
	leaq	.LC12(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L458:
	movq	-1(%rax), %rdx
	cmpw	$1069, 11(%rdx)
	jne	.L438
	movq	41112(%r12), %rdi
	movq	23(%rax), %r15
	testq	%rdi, %rdi
	je	.L459
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L441:
	movq	%r12, %rdi
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE@PLT
	movq	0(%r13), %r15
	movq	(%rax), %r13
	leaq	23(%r15), %rsi
	movq	%r13, 23(%r15)
	testb	$1, %r13b
	je	.L446
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L444
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L444:
	testb	$24, %al
	je	.L446
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L460
	.p2align 4,,10
	.p2align 3
.L446:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r13
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L436
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L436:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L461
.L442:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L460:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L457:
	addq	$24, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL23Stats_Runtime_MapShrinkEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L442
	.cfi_endproc
.LFE20253:
	.size	_ZN2v88internal17Runtime_MapShrinkEiPmPNS0_7IsolateE, .-_ZN2v88internal17Runtime_MapShrinkEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal15Runtime_MapGrowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal15Runtime_MapGrowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal15Runtime_MapGrowEiPmPNS0_7IsolateE, @function
_ZN2v88internal15Runtime_MapGrowEiPmPNS0_7IsolateE:
.LFB20256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L485
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	testb	$1, %al
	jne	.L486
.L464:
	leaq	.LC12(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L486:
	movq	-1(%rax), %rdx
	cmpw	$1069, 11(%rdx)
	jne	.L464
	movq	41112(%r12), %rdi
	movq	23(%rax), %r15
	testq	%rdi, %rdi
	je	.L487
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE@PLT
	testq	%rax, %rax
	je	.L488
.L469:
	movq	0(%r13), %r15
	movq	(%rax), %r13
	movq	%r13, 23(%r15)
	leaq	23(%r15), %rsi
	testb	$1, %r13b
	je	.L476
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L472
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L472:
	testb	$24, %al
	je	.L476
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L489
	.p2align 4,,10
	.p2align 3
.L476:
	movq	88(%r12), %r13
.L470:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L462
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L462:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L490
.L468:
	leaq	8(%rsi), %rax
	movq	%r12, %rdi
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE@PLT
	testq	%rax, %rax
	jne	.L469
.L488:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$221, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L489:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L485:
	addq	$24, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL21Stats_Runtime_MapGrowEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L468
	.cfi_endproc
.LFE20256:
	.size	_ZN2v88internal15Runtime_MapGrowEiPmPNS0_7IsolateE, .-_ZN2v88internal15Runtime_MapGrowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateE:
.LFB20259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %r8d
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L502
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L493
.L495:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L493:
	movq	-1(%rax), %rdx
	cmpw	$1084, 11(%rdx)
	jne	.L503
.L494:
	movq	-16(%rdi), %rdx
	leaq	-8(%rdi), %rsi
	testb	$1, %dl
	jne	.L504
	sarq	$32, %rdx
	call	_ZN2v88internal16JSWeakCollection6DeleteENS0_6HandleIS1_EENS2_INS0_6ObjectEEEi@PLT
	testb	%al, %al
	je	.L497
	movq	112(%r12), %r14
.L498:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L491
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L491:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_restore_state
	movq	120(%r12), %r14
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L503:
	movq	-1(%rax), %rax
	cmpw	$1085, 11(%rax)
	jne	.L495
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L502:
	popq	%rbx
	movl	%r8d, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL34Stats_Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L504:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20259:
	.size	_ZN2v88internal28Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_WeakCollectionSetEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_WeakCollectionSetEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_WeakCollectionSetEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_WeakCollectionSetEiPmPNS0_7IsolateE:
.LFB20262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L513
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L507
.L509:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L507:
	movq	-1(%rax), %rdx
	cmpw	$1084, 11(%rdx)
	jne	.L514
.L508:
	movq	-24(%r13), %rcx
	leaq	-8(%r13), %rsi
	leaq	-16(%r13), %rdx
	testb	$1, %cl
	jne	.L515
	movq	%r13, %rdi
	sarq	$32, %rcx
	call	_ZN2v88internal16JSWeakCollection3SetENS0_6HandleIS1_EENS2_INS0_6ObjectEEES5_i@PLT
	movq	0(%r13), %r13
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L505
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L505:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L514:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1085, 11(%rax)
	jne	.L509
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L513:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movq	%rdx, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_WeakCollectionSetEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L515:
	.cfi_restore_state
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20262:
	.size	_ZN2v88internal25Runtime_WeakCollectionSetEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_WeakCollectionSetEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal15Runtime_TheHoleEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal15Runtime_TheHoleEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal15Runtime_TheHoleEiPmPNS0_7IsolateE:
.LFB25346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25346:
	.size	_GLOBAL__sub_I__ZN2v88internal15Runtime_TheHoleEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal15Runtime_TheHoleEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal15Runtime_TheHoleEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL31Stats_Runtime_WeakCollectionSetEiPmPNS0_7IsolateEE27trace_event_unique_atomic96,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_WeakCollectionSetEiPmPNS0_7IsolateEE27trace_event_unique_atomic96, @object
	.size	_ZZN2v88internalL31Stats_Runtime_WeakCollectionSetEiPmPNS0_7IsolateEE27trace_event_unique_atomic96, 8
_ZZN2v88internalL31Stats_Runtime_WeakCollectionSetEiPmPNS0_7IsolateEE27trace_event_unique_atomic96:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateEE27trace_event_unique_atomic74,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateEE27trace_event_unique_atomic74, @object
	.size	_ZZN2v88internalL34Stats_Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateEE27trace_event_unique_atomic74, 8
_ZZN2v88internalL34Stats_Runtime_WeakCollectionDeleteEiPmPNS0_7IsolateEE27trace_event_unique_atomic74:
	.zero	8
	.section	.bss._ZZN2v88internalL21Stats_Runtime_MapGrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic59,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL21Stats_Runtime_MapGrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic59, @object
	.size	_ZZN2v88internalL21Stats_Runtime_MapGrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic59, 8
_ZZN2v88internalL21Stats_Runtime_MapGrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic59:
	.zero	8
	.section	.bss._ZZN2v88internalL23Stats_Runtime_MapShrinkEiPmPNS0_7IsolateEE27trace_event_unique_atomic49,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL23Stats_Runtime_MapShrinkEiPmPNS0_7IsolateEE27trace_event_unique_atomic49, @object
	.size	_ZZN2v88internalL23Stats_Runtime_MapShrinkEiPmPNS0_7IsolateEE27trace_event_unique_atomic49, 8
_ZZN2v88internalL23Stats_Runtime_MapShrinkEiPmPNS0_7IsolateEE27trace_event_unique_atomic49:
	.zero	8
	.section	.bss._ZZN2v88internalL23Stats_Runtime_SetShrinkEiPmPNS0_7IsolateEE27trace_event_unique_atomic39,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL23Stats_Runtime_SetShrinkEiPmPNS0_7IsolateEE27trace_event_unique_atomic39, @object
	.size	_ZZN2v88internalL23Stats_Runtime_SetShrinkEiPmPNS0_7IsolateEE27trace_event_unique_atomic39, 8
_ZZN2v88internalL23Stats_Runtime_SetShrinkEiPmPNS0_7IsolateEE27trace_event_unique_atomic39:
	.zero	8
	.section	.bss._ZZN2v88internalL21Stats_Runtime_SetGrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic23,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL21Stats_Runtime_SetGrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic23, @object
	.size	_ZZN2v88internalL21Stats_Runtime_SetGrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic23, 8
_ZZN2v88internalL21Stats_Runtime_SetGrowEiPmPNS0_7IsolateEE27trace_event_unique_atomic23:
	.zero	8
	.section	.bss._ZZN2v88internalL21Stats_Runtime_TheHoleEiPmPNS0_7IsolateEE27trace_event_unique_atomic17,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL21Stats_Runtime_TheHoleEiPmPNS0_7IsolateEE27trace_event_unique_atomic17, @object
	.size	_ZZN2v88internalL21Stats_Runtime_TheHoleEiPmPNS0_7IsolateEE27trace_event_unique_atomic17, 8
_ZZN2v88internalL21Stats_Runtime_TheHoleEiPmPNS0_7IsolateEE27trace_event_unique_atomic17:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
