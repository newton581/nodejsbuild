	.file	"heap-snapshot-generator.cc"
	.text
	.section	.text._ZN2v812OutputStream12GetChunkSizeEv,"axG",@progbits,_ZN2v812OutputStream12GetChunkSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v812OutputStream12GetChunkSizeEv
	.type	_ZN2v812OutputStream12GetChunkSizeEv, @function
_ZN2v812OutputStream12GetChunkSizeEv:
.LFB3712:
	.cfi_startproc
	endbr64
	movl	$1024, %eax
	ret
	.cfi_endproc
.LFE3712:
	.size	_ZN2v812OutputStream12GetChunkSizeEv, .-_ZN2v812OutputStream12GetChunkSizeEv
	.section	.text._ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi,"axG",@progbits,_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi
	.type	_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi, @function
_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi:
.LFB3713:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3713:
	.size	_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi, .-_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi
	.section	.text._ZN2v813EmbedderGraph4Node11WrapperNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node11WrapperNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.type	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, @function
_ZN2v813EmbedderGraph4Node11WrapperNodeEv:
.LFB3714:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3714:
	.size	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, .-_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node10IsRootNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node10IsRootNodeEv
	.type	_ZN2v813EmbedderGraph4Node10IsRootNodeEv, @function
_ZN2v813EmbedderGraph4Node10IsRootNodeEv:
.LFB3715:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3715:
	.size	_ZN2v813EmbedderGraph4Node10IsRootNodeEv, .-_ZN2v813EmbedderGraph4Node10IsRootNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node10NamePrefixEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node10NamePrefixEv
	.type	_ZN2v813EmbedderGraph4Node10NamePrefixEv, @function
_ZN2v813EmbedderGraph4Node10NamePrefixEv:
.LFB3717:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3717:
	.size	_ZN2v813EmbedderGraph4Node10NamePrefixEv, .-_ZN2v813EmbedderGraph4Node10NamePrefixEv
	.section	.text._ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB7076:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rcx), %r8
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7076:
	.size	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.text._ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE,"axG",@progbits,_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.type	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE, @function
_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE:
.LFB7077:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7077:
	.size	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE, .-_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.section	.text._ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE, @function
_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE:
.LFB7080:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rdx), %rcx
	movq	24(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7080:
	.size	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE, .-_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.section	.text._ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE:
.LFB7083:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7083:
	.size	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE:
.LFB7084:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7084:
	.size	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.type	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm, @function
_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm:
.LFB7085:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7085:
	.size	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm, .-_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.section	.text._ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE:
.LFB7086:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7086:
	.size	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE:
.LFB7087:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7087:
	.size	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.type	_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, @function
_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_:
.LFB19976:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.cfi_endproc
.LFE19976:
	.size	_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, .-_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.section	.text._ZN2v88internal17EmbedderGraphImpl10V8NodeImpl14IsEmbedderNodeEv,"axG",@progbits,_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl14IsEmbedderNodeEv
	.type	_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl14IsEmbedderNodeEv, @function
_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl14IsEmbedderNodeEv:
.LFB20075:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE20075:
	.size	_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl14IsEmbedderNodeEv, .-_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl14IsEmbedderNodeEv
	.section	.text._ZN2v88internal21HeapSnapshotGenerator12ProgressStepEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21HeapSnapshotGenerator12ProgressStepEv
	.type	_ZN2v88internal21HeapSnapshotGenerator12ProgressStepEv, @function
_ZN2v88internal21HeapSnapshotGenerator12ProgressStepEv:
.LFB20167:
	.cfi_startproc
	endbr64
	addl	$1, 392(%rdi)
	ret
	.cfi_endproc
.LFE20167:
	.size	_ZN2v88internal21HeapSnapshotGenerator12ProgressStepEv, .-_ZN2v88internal21HeapSnapshotGenerator12ProgressStepEv
	.section	.text._ZN2v88internal21HeapSnapshotGenerator14ProgressReportEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21HeapSnapshotGenerator14ProgressReportEb
	.type	_ZN2v88internal21HeapSnapshotGenerator14ProgressReportEb, @function
_ZN2v88internal21HeapSnapshotGenerator14ProgressReportEb:
.LFB20168:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %r8
	movl	$1, %eax
	testq	%r8, %r8
	je	.L25
	movl	392(%rdi), %r9d
	testb	%sil, %sil
	je	.L27
.L20:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%r8), %rax
	movl	%r9d, %esi
	movl	396(%rdi), %edx
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*16(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore 6
	imull	$989560465, %r9d, %edx
	addl	$3435968, %edx
	rorl	$4, %edx
	cmpl	$429496, %edx
	jbe	.L20
.L25:
	ret
	.cfi_endproc
.LFE20168:
	.size	_ZN2v88internal21HeapSnapshotGenerator14ProgressReportEb, .-_ZN2v88internal21HeapSnapshotGenerator14ProgressReportEb
	.section	.text._ZN2v88internal29EmbedderGraphEntriesAllocatorD2Ev,"axG",@progbits,_ZN2v88internal29EmbedderGraphEntriesAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29EmbedderGraphEntriesAllocatorD2Ev
	.type	_ZN2v88internal29EmbedderGraphEntriesAllocatorD2Ev, @function
_ZN2v88internal29EmbedderGraphEntriesAllocatorD2Ev:
.LFB26526:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE26526:
	.size	_ZN2v88internal29EmbedderGraphEntriesAllocatorD2Ev, .-_ZN2v88internal29EmbedderGraphEntriesAllocatorD2Ev
	.weak	_ZN2v88internal29EmbedderGraphEntriesAllocatorD1Ev
	.set	_ZN2v88internal29EmbedderGraphEntriesAllocatorD1Ev,_ZN2v88internal29EmbedderGraphEntriesAllocatorD2Ev
	.section	.text._ZN2v88internal17EmbedderGraphImpl10V8NodeImplD2Ev,"axG",@progbits,_ZN2v88internal17EmbedderGraphImpl10V8NodeImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17EmbedderGraphImpl10V8NodeImplD2Ev
	.type	_ZN2v88internal17EmbedderGraphImpl10V8NodeImplD2Ev, @function
_ZN2v88internal17EmbedderGraphImpl10V8NodeImplD2Ev:
.LFB26534:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE26534:
	.size	_ZN2v88internal17EmbedderGraphImpl10V8NodeImplD2Ev, .-_ZN2v88internal17EmbedderGraphImpl10V8NodeImplD2Ev
	.weak	_ZN2v88internal17EmbedderGraphImpl10V8NodeImplD1Ev
	.set	_ZN2v88internal17EmbedderGraphImpl10V8NodeImplD1Ev,_ZN2v88internal17EmbedderGraphImpl10V8NodeImplD2Ev
	.section	.text._ZN2v88internal24RootsReferencesExtractorD2Ev,"axG",@progbits,_ZN2v88internal24RootsReferencesExtractorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24RootsReferencesExtractorD2Ev
	.type	_ZN2v88internal24RootsReferencesExtractorD2Ev, @function
_ZN2v88internal24RootsReferencesExtractorD2Ev:
.LFB26542:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE26542:
	.size	_ZN2v88internal24RootsReferencesExtractorD2Ev, .-_ZN2v88internal24RootsReferencesExtractorD2Ev
	.weak	_ZN2v88internal24RootsReferencesExtractorD1Ev
	.set	_ZN2v88internal24RootsReferencesExtractorD1Ev,_ZN2v88internal24RootsReferencesExtractorD2Ev
	.section	.text._ZN2v88internal31JSArrayBufferDataEntryAllocatorD2Ev,"axG",@progbits,_ZN2v88internal31JSArrayBufferDataEntryAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal31JSArrayBufferDataEntryAllocatorD2Ev
	.type	_ZN2v88internal31JSArrayBufferDataEntryAllocatorD2Ev, @function
_ZN2v88internal31JSArrayBufferDataEntryAllocatorD2Ev:
.LFB26546:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE26546:
	.size	_ZN2v88internal31JSArrayBufferDataEntryAllocatorD2Ev, .-_ZN2v88internal31JSArrayBufferDataEntryAllocatorD2Ev
	.weak	_ZN2v88internal31JSArrayBufferDataEntryAllocatorD1Ev
	.set	_ZN2v88internal31JSArrayBufferDataEntryAllocatorD1Ev,_ZN2v88internal31JSArrayBufferDataEntryAllocatorD2Ev
	.section	.text._ZN2v88internal26IndexedReferencesExtractorD2Ev,"axG",@progbits,_ZN2v88internal26IndexedReferencesExtractorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26IndexedReferencesExtractorD2Ev
	.type	_ZN2v88internal26IndexedReferencesExtractorD2Ev, @function
_ZN2v88internal26IndexedReferencesExtractorD2Ev:
.LFB26550:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE26550:
	.size	_ZN2v88internal26IndexedReferencesExtractorD2Ev, .-_ZN2v88internal26IndexedReferencesExtractorD2Ev
	.weak	_ZN2v88internal26IndexedReferencesExtractorD1Ev
	.set	_ZN2v88internal26IndexedReferencesExtractorD1Ev,_ZN2v88internal26IndexedReferencesExtractorD2Ev
	.section	.text._ZN2v88internal23GlobalObjectsEnumeratorD2Ev,"axG",@progbits,_ZN2v88internal23GlobalObjectsEnumeratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23GlobalObjectsEnumeratorD2Ev
	.type	_ZN2v88internal23GlobalObjectsEnumeratorD2Ev, @function
_ZN2v88internal23GlobalObjectsEnumeratorD2Ev:
.LFB26538:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal23GlobalObjectsEnumeratorE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L33
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L33:
	ret
	.cfi_endproc
.LFE26538:
	.size	_ZN2v88internal23GlobalObjectsEnumeratorD2Ev, .-_ZN2v88internal23GlobalObjectsEnumeratorD2Ev
	.weak	_ZN2v88internal23GlobalObjectsEnumeratorD1Ev
	.set	_ZN2v88internal23GlobalObjectsEnumeratorD1Ev,_ZN2v88internal23GlobalObjectsEnumeratorD2Ev
	.section	.text._ZN2v88internal17EmbedderGraphImplD2Ev,"axG",@progbits,_ZN2v88internal17EmbedderGraphImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17EmbedderGraphImplD2Ev
	.type	_ZN2v88internal17EmbedderGraphImplD2Ev, @function
_ZN2v88internal17EmbedderGraphImplD2Ev:
.LFB26530:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal17EmbedderGraphImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZdlPv@PLT
.L36:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L37
	.p2align 4,,10
	.p2align 3
.L41:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L38
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %r13
	jne	.L41
.L39:
	movq	8(%rbx), %r12
.L37:
	testq	%r12, %r12
	je	.L35
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L41
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L35:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26530:
	.size	_ZN2v88internal17EmbedderGraphImplD2Ev, .-_ZN2v88internal17EmbedderGraphImplD2Ev
	.weak	_ZN2v88internal17EmbedderGraphImplD1Ev
	.set	_ZN2v88internal17EmbedderGraphImplD1Ev,_ZN2v88internal17EmbedderGraphImplD2Ev
	.section	.rodata._ZN2v88internal17EmbedderGraphImpl10V8NodeImpl11SizeInBytesEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal17EmbedderGraphImpl10V8NodeImpl11SizeInBytesEv,"axG",@progbits,_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl11SizeInBytesEv
	.type	_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl11SizeInBytesEv, @function
_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl11SizeInBytesEv:
.LFB20077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20077:
	.size	_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl11SizeInBytesEv, .-_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl11SizeInBytesEv
	.section	.text._ZN2v88internal17EmbedderGraphImpl10V8NodeImpl4NameEv,"axG",@progbits,_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl4NameEv
	.type	_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl4NameEv, @function
_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl4NameEv:
.LFB20076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20076:
	.size	_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl4NameEv, .-_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl4NameEv
	.section	.text._ZN2v88internal26IndexedReferencesExtractorD0Ev,"axG",@progbits,_ZN2v88internal26IndexedReferencesExtractorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26IndexedReferencesExtractorD0Ev
	.type	_ZN2v88internal26IndexedReferencesExtractorD0Ev, @function
_ZN2v88internal26IndexedReferencesExtractorD0Ev:
.LFB26552:
	.cfi_startproc
	endbr64
	movl	$56, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26552:
	.size	_ZN2v88internal26IndexedReferencesExtractorD0Ev, .-_ZN2v88internal26IndexedReferencesExtractorD0Ev
	.section	.text._ZN2v88internal24RootsReferencesExtractorD0Ev,"axG",@progbits,_ZN2v88internal24RootsReferencesExtractorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24RootsReferencesExtractorD0Ev
	.type	_ZN2v88internal24RootsReferencesExtractorD0Ev, @function
_ZN2v88internal24RootsReferencesExtractorD0Ev:
.LFB26544:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26544:
	.size	_ZN2v88internal24RootsReferencesExtractorD0Ev, .-_ZN2v88internal24RootsReferencesExtractorD0Ev
	.section	.text._ZN2v88internal31JSArrayBufferDataEntryAllocatorD0Ev,"axG",@progbits,_ZN2v88internal31JSArrayBufferDataEntryAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal31JSArrayBufferDataEntryAllocatorD0Ev
	.type	_ZN2v88internal31JSArrayBufferDataEntryAllocatorD0Ev, @function
_ZN2v88internal31JSArrayBufferDataEntryAllocatorD0Ev:
.LFB26548:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26548:
	.size	_ZN2v88internal31JSArrayBufferDataEntryAllocatorD0Ev, .-_ZN2v88internal31JSArrayBufferDataEntryAllocatorD0Ev
	.section	.text._ZN2v88internal29EmbedderGraphEntriesAllocatorD0Ev,"axG",@progbits,_ZN2v88internal29EmbedderGraphEntriesAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29EmbedderGraphEntriesAllocatorD0Ev
	.type	_ZN2v88internal29EmbedderGraphEntriesAllocatorD0Ev, @function
_ZN2v88internal29EmbedderGraphEntriesAllocatorD0Ev:
.LFB26528:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26528:
	.size	_ZN2v88internal29EmbedderGraphEntriesAllocatorD0Ev, .-_ZN2v88internal29EmbedderGraphEntriesAllocatorD0Ev
	.section	.text._ZN2v88internal17EmbedderGraphImpl10V8NodeImplD0Ev,"axG",@progbits,_ZN2v88internal17EmbedderGraphImpl10V8NodeImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17EmbedderGraphImpl10V8NodeImplD0Ev
	.type	_ZN2v88internal17EmbedderGraphImpl10V8NodeImplD0Ev, @function
_ZN2v88internal17EmbedderGraphImpl10V8NodeImplD0Ev:
.LFB26536:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26536:
	.size	_ZN2v88internal17EmbedderGraphImpl10V8NodeImplD0Ev, .-_ZN2v88internal17EmbedderGraphImpl10V8NodeImplD0Ev
	.section	.text._ZN2v88internal23GlobalObjectsEnumeratorD0Ev,"axG",@progbits,_ZN2v88internal23GlobalObjectsEnumeratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23GlobalObjectsEnumeratorD0Ev
	.type	_ZN2v88internal23GlobalObjectsEnumeratorD0Ev, @function
_ZN2v88internal23GlobalObjectsEnumeratorD0Ev:
.LFB26540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal23GlobalObjectsEnumeratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L57
	call	_ZdlPv@PLT
.L57:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26540:
	.size	_ZN2v88internal23GlobalObjectsEnumeratorD0Ev, .-_ZN2v88internal23GlobalObjectsEnumeratorD0Ev
	.section	.text._ZN2v88internal17EmbedderGraphImplD0Ev,"axG",@progbits,_ZN2v88internal17EmbedderGraphImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17EmbedderGraphImplD0Ev
	.type	_ZN2v88internal17EmbedderGraphImplD0Ev, @function
_ZN2v88internal17EmbedderGraphImplD0Ev:
.LFB26532:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal17EmbedderGraphImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L63
	call	_ZdlPv@PLT
.L63:
	movq	16(%r13), %rbx
	movq	8(%r13), %r12
	cmpq	%r12, %rbx
	je	.L64
	.p2align 4,,10
	.p2align 3
.L68:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L65
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L68
.L66:
	movq	8(%r13), %r12
.L64:
	testq	%r12, %r12
	je	.L69
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L69:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$56, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L68
	jmp	.L66
	.cfi_endproc
.LFE26532:
	.size	_ZN2v88internal17EmbedderGraphImplD0Ev, .-_ZN2v88internal17EmbedderGraphImplD0Ev
	.section	.text._ZN2v88internal14V8HeapExplorerD2Ev,"axG",@progbits,_ZN2v88internal14V8HeapExplorerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14V8HeapExplorerD2Ev
	.type	_ZN2v88internal14V8HeapExplorerD2Ev, @function
_ZN2v88internal14V8HeapExplorerD2Ev:
.LFB26554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14V8HeapExplorerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	232(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L78
	call	_ZdlPv@PLT
.L78:
	movq	184(%rbx), %r12
	testq	%r12, %r12
	je	.L79
	.p2align 4,,10
	.p2align 3
.L80:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L80
.L79:
	movq	176(%rbx), %rax
	movq	168(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	168(%rbx), %rdi
	leaq	216(%rbx), %rax
	movq	$0, 192(%rbx)
	movq	$0, 184(%rbx)
	cmpq	%rax, %rdi
	je	.L81
	call	_ZdlPv@PLT
.L81:
	movq	128(%rbx), %r12
	testq	%r12, %r12
	je	.L82
	.p2align 4,,10
	.p2align 3
.L83:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L83
.L82:
	movq	120(%rbx), %rax
	movq	112(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	112(%rbx), %rdi
	leaq	160(%rbx), %rax
	movq	$0, 136(%rbx)
	movq	$0, 128(%rbx)
	cmpq	%rax, %rdi
	je	.L84
	call	_ZdlPv@PLT
.L84:
	movq	72(%rbx), %r12
	testq	%r12, %r12
	je	.L85
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L86
.L85:
	movq	64(%rbx), %rax
	movq	56(%rbx), %rdi
	xorl	%esi, %esi
	addq	$104, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L77
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26554:
	.size	_ZN2v88internal14V8HeapExplorerD2Ev, .-_ZN2v88internal14V8HeapExplorerD2Ev
	.weak	_ZN2v88internal14V8HeapExplorerD1Ev
	.set	_ZN2v88internal14V8HeapExplorerD1Ev,_ZN2v88internal14V8HeapExplorerD2Ev
	.section	.text._ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.type	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, @function
_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_:
.LFB7078:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_(%rip), %r9
	movq	16(%rax), %r8
	cmpq	%r9, %r8
	jne	.L105
	jmp	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L105:
	jmp	*%r8
	.cfi_endproc
.LFE7078:
	.size	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, .-_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.section	.text._ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE, @function
_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE:
.LFB7079:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_(%rip), %r9
	leaq	8(%rdx), %rcx
	movq	16(%rax), %r8
	cmpq	%r9, %r8
	jne	.L107
	jmp	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L107:
	jmp	*%r8
	.cfi_endproc
.LFE7079:
	.size	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE, .-_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.section	.text._ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE, @function
_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE:
.LFB7081:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_(%rip), %r9
	leaq	8(%rdx), %rcx
	movq	32(%rax), %r8
	cmpq	%r9, %r8
	jne	.L109
	movq	16(%rax), %r8
	leaq	_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_(%rip), %r9
	cmpq	%r9, %r8
	jne	.L109
	jmp	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L109:
	jmp	*%r8
	.cfi_endproc
.LFE7081:
	.size	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE, .-_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.section	.text._ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.type	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_, @function
_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_:
.LFB7082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	leaq	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE(%rip), %rbx
	movq	40(%rax), %rcx
	cmpq	%rbx, %rcx
	jne	.L113
	movq	16(%rax), %r8
	leaq	_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_(%rip), %rsi
	leaq	8(%rdx), %rcx
	cmpq	%rsi, %r8
	movq	%r13, %rsi
	jne	.L114
	call	*24(%rax)
.L116:
	movq	(%r12), %rax
	movq	40(%rax), %rcx
	cmpq	%rbx, %rcx
	jne	.L117
.L121:
	movq	16(%rax), %r8
	leaq	_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_(%rip), %rdx
	leaq	8(%r14), %rcx
	cmpq	%rdx, %r8
	jne	.L118
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	24(%rax), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	call	*%rcx
	movq	(%r12), %rax
	movq	40(%rax), %rcx
	cmpq	%rbx, %rcx
	je	.L121
.L117:
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rcx
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r8
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	call	*%r8
	jmp	.L116
	.cfi_endproc
.LFE7082:
	.size	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_, .-_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.section	.text._ZN2v88internal21HeapSnapshotGeneratorD0Ev,"axG",@progbits,_ZN2v88internal21HeapSnapshotGeneratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21HeapSnapshotGeneratorD0Ev
	.type	_ZN2v88internal21HeapSnapshotGeneratorD0Ev, @function
_ZN2v88internal21HeapSnapshotGeneratorD0Ev:
.LFB26524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal21HeapSnapshotGeneratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	352(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%rbx, %rbx
	je	.L123
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L124
.L123:
	movq	344(%r12), %rax
	movq	336(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	336(%r12), %rdi
	leaq	384(%r12), %rax
	movq	$0, 360(%r12)
	movq	$0, 352(%r12)
	cmpq	%rax, %rdi
	je	.L125
	call	_ZdlPv@PLT
.L125:
	movq	320(%r12), %rdi
	testq	%rdi, %rdi
	je	.L126
	movq	(%rdi), %rax
	call	*8(%rax)
.L126:
	movq	256(%r12), %rdi
	leaq	16+_ZTVN2v88internal14V8HeapExplorerE(%rip), %rax
	movq	%rax, 24(%r12)
	testq	%rdi, %rdi
	je	.L127
	call	_ZdlPv@PLT
.L127:
	movq	208(%r12), %rbx
	testq	%rbx, %rbx
	je	.L128
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L129
.L128:
	movq	200(%r12), %rax
	movq	192(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	192(%r12), %rdi
	leaq	240(%r12), %rax
	movq	$0, 216(%r12)
	movq	$0, 208(%r12)
	cmpq	%rax, %rdi
	je	.L130
	call	_ZdlPv@PLT
.L130:
	movq	152(%r12), %rbx
	testq	%rbx, %rbx
	je	.L131
	.p2align 4,,10
	.p2align 3
.L132:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L132
.L131:
	movq	144(%r12), %rax
	movq	136(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	136(%r12), %rdi
	leaq	184(%r12), %rax
	movq	$0, 160(%r12)
	movq	$0, 152(%r12)
	cmpq	%rax, %rdi
	je	.L133
	call	_ZdlPv@PLT
.L133:
	movq	96(%r12), %rbx
	testq	%rbx, %rbx
	je	.L134
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L135
.L134:
	movq	88(%r12), %rax
	movq	80(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	80(%r12), %rdi
	leaq	128(%r12), %rax
	movq	$0, 104(%r12)
	movq	$0, 96(%r12)
	cmpq	%rax, %rdi
	je	.L136
	call	_ZdlPv@PLT
.L136:
	popq	%rbx
	movq	%r12, %rdi
	movl	$408, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26524:
	.size	_ZN2v88internal21HeapSnapshotGeneratorD0Ev, .-_ZN2v88internal21HeapSnapshotGeneratorD0Ev
	.section	.text._ZN2v88internal14V8HeapExplorerD0Ev,"axG",@progbits,_ZN2v88internal14V8HeapExplorerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14V8HeapExplorerD0Ev
	.type	_ZN2v88internal14V8HeapExplorerD0Ev, @function
_ZN2v88internal14V8HeapExplorerD0Ev:
.LFB26556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14V8HeapExplorerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, (%rdi)
	movq	232(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L161
	call	_ZdlPv@PLT
.L161:
	movq	184(%r12), %rbx
	testq	%rbx, %rbx
	je	.L162
	.p2align 4,,10
	.p2align 3
.L163:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L163
.L162:
	movq	176(%r12), %rax
	movq	168(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	168(%r12), %rdi
	leaq	216(%r12), %rax
	movq	$0, 192(%r12)
	movq	$0, 184(%r12)
	cmpq	%rax, %rdi
	je	.L164
	call	_ZdlPv@PLT
.L164:
	movq	128(%r12), %rbx
	testq	%rbx, %rbx
	je	.L165
	.p2align 4,,10
	.p2align 3
.L166:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L166
.L165:
	movq	120(%r12), %rax
	movq	112(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	112(%r12), %rdi
	leaq	160(%r12), %rax
	movq	$0, 136(%r12)
	movq	$0, 128(%r12)
	cmpq	%rax, %rdi
	je	.L167
	call	_ZdlPv@PLT
.L167:
	movq	72(%r12), %rbx
	testq	%rbx, %rbx
	je	.L168
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L169
.L168:
	movq	64(%r12), %rax
	movq	56(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	56(%r12), %rdi
	leaq	104(%r12), %rax
	movq	$0, 80(%r12)
	movq	$0, 72(%r12)
	cmpq	%rax, %rdi
	je	.L170
	call	_ZdlPv@PLT
.L170:
	popq	%rbx
	movq	%r12, %rdi
	movl	$272, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26556:
	.size	_ZN2v88internal14V8HeapExplorerD0Ev, .-_ZN2v88internal14V8HeapExplorerD0Ev
	.section	.text._ZN2v88internal21HeapSnapshotGeneratorD2Ev,"axG",@progbits,_ZN2v88internal21HeapSnapshotGeneratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21HeapSnapshotGeneratorD2Ev
	.type	_ZN2v88internal21HeapSnapshotGeneratorD2Ev, @function
_ZN2v88internal21HeapSnapshotGeneratorD2Ev:
.LFB26522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal21HeapSnapshotGeneratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	352(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L188
	.p2align 4,,10
	.p2align 3
.L189:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L189
.L188:
	movq	344(%rbx), %rax
	movq	336(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	336(%rbx), %rdi
	leaq	384(%rbx), %rax
	movq	$0, 360(%rbx)
	movq	$0, 352(%rbx)
	cmpq	%rax, %rdi
	je	.L190
	call	_ZdlPv@PLT
.L190:
	movq	320(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L191
	movq	(%rdi), %rax
	call	*8(%rax)
.L191:
	movq	256(%rbx), %rdi
	leaq	16+_ZTVN2v88internal14V8HeapExplorerE(%rip), %rax
	movq	%rax, 24(%rbx)
	testq	%rdi, %rdi
	je	.L192
	call	_ZdlPv@PLT
.L192:
	movq	208(%rbx), %r12
	testq	%r12, %r12
	je	.L193
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L194
.L193:
	movq	200(%rbx), %rax
	movq	192(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	192(%rbx), %rdi
	leaq	240(%rbx), %rax
	movq	$0, 216(%rbx)
	movq	$0, 208(%rbx)
	cmpq	%rax, %rdi
	je	.L195
	call	_ZdlPv@PLT
.L195:
	movq	152(%rbx), %r12
	testq	%r12, %r12
	je	.L196
	.p2align 4,,10
	.p2align 3
.L197:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L197
.L196:
	movq	144(%rbx), %rax
	movq	136(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	136(%rbx), %rdi
	leaq	184(%rbx), %rax
	movq	$0, 160(%rbx)
	movq	$0, 152(%rbx)
	cmpq	%rax, %rdi
	je	.L198
	call	_ZdlPv@PLT
.L198:
	movq	96(%rbx), %r12
	testq	%r12, %r12
	je	.L199
	.p2align 4,,10
	.p2align 3
.L200:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L200
.L199:
	movq	88(%rbx), %rax
	movq	80(%rbx), %rdi
	xorl	%esi, %esi
	subq	$-128, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L187
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26522:
	.size	_ZN2v88internal21HeapSnapshotGeneratorD2Ev, .-_ZN2v88internal21HeapSnapshotGeneratorD2Ev
	.weak	_ZN2v88internal21HeapSnapshotGeneratorD1Ev
	.set	_ZN2v88internal21HeapSnapshotGeneratorD1Ev,_ZN2v88internal21HeapSnapshotGeneratorD2Ev
	.section	.rodata._ZN2v88internalL10WriteUCharEPNS0_18OutputStreamWriterEj.str1.1,"aMS",@progbits,1
.LC1:
	.string	"\\u"
	.section	.text._ZN2v88internalL10WriteUCharEPNS0_18OutputStreamWriterEj,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL10WriteUCharEPNS0_18OutputStreamWriterEj, @function
_ZN2v88internalL10WriteUCharEPNS0_18OutputStreamWriterEj:
.LFB20196:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC1(%rip), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	2(%r15), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	32(%rdi), %edx
	movq	16(%rdi), %rsi
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L247:
	movslq	%edx, %rax
	addq	%rsi, %rax
	cmpq	%r13, %r15
	jnb	.L227
.L226:
	movl	8(%rbx), %r14d
	movq	%r13, %rax
	movslq	%edx, %rdi
	subq	%r15, %rax
	subl	%edx, %r14d
	cmpl	%eax, %r14d
	cmovg	%eax, %r14d
	addq	%rsi, %rdi
	movq	%r15, %rsi
	movslq	%r14d, %rcx
	movq	%rcx, %rdx
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movl	32(%rbx), %edx
	movq	-56(%rbp), %rcx
	movq	16(%rbx), %rsi
	addl	%r14d, %edx
	addq	%rcx, %r15
	cmpl	8(%rbx), %edx
	movl	%edx, 32(%rbx)
	jne	.L247
	cmpb	$0, 36(%rbx)
	jne	.L247
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L231
	movb	$1, 36(%rbx)
.L231:
	movq	16(%rbx), %rsi
	movl	$0, 32(%rbx)
	xorl	%edx, %edx
	movq	%rsi, %rax
	cmpq	%r13, %r15
	jb	.L226
.L227:
	movl	%r12d, %ecx
	leaq	_ZZN2v88internalL10WriteUCharEPNS0_18OutputStreamWriterEjE9hex_chars(%rip), %r13
	addl	$1, %edx
	shrl	$12, %ecx
	movl	%edx, 32(%rbx)
	andl	$15, %ecx
	movzbl	0(%r13,%rcx), %ecx
	movb	%cl, (%rax)
	movslq	32(%rbx), %rdx
	cmpl	8(%rbx), %edx
	je	.L232
	leal	1(%rdx), %ecx
	addq	16(%rbx), %rdx
.L233:
	movl	%r12d, %eax
	movl	%ecx, 32(%rbx)
	shrl	$8, %eax
	andl	$15, %eax
	movzbl	0(%r13,%rax), %eax
	movb	%al, (%rdx)
	movslq	32(%rbx), %rdx
	cmpl	8(%rbx), %edx
	je	.L236
	leal	1(%rdx), %ecx
	addq	16(%rbx), %rdx
.L237:
	movl	%r12d, %eax
	movl	%ecx, 32(%rbx)
	shrl	$4, %eax
	andl	$15, %eax
	movzbl	0(%r13,%rax), %eax
	movb	%al, (%rdx)
	movslq	32(%rbx), %rdx
	cmpl	8(%rbx), %edx
	je	.L240
	leal	1(%rdx), %ecx
	addq	16(%rbx), %rdx
.L241:
	andl	$15, %r12d
	movl	%ecx, 32(%rbx)
	movzbl	0(%r13,%r12), %eax
	movb	%al, (%rdx)
	movl	32(%rbx), %edx
	cmpl	8(%rbx), %edx
	je	.L248
.L225:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	cmpb	$0, 36(%rbx)
	movq	16(%rbx), %rsi
	jne	.L249
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	je	.L250
.L235:
	movq	16(%rbx), %rdx
	movl	$1, %ecx
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L248:
	cmpb	$0, 36(%rbx)
	jne	.L225
	movq	(%rbx), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	je	.L251
.L245:
	movl	$0, 32(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	cmpb	$0, 36(%rbx)
	movq	16(%rbx), %rsi
	jne	.L252
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	je	.L253
.L243:
	movq	16(%rbx), %rdx
	movl	$1, %ecx
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L236:
	cmpb	$0, 36(%rbx)
	movq	16(%rbx), %rsi
	jne	.L254
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	je	.L255
.L239:
	movq	16(%rbx), %rdx
	movl	$1, %ecx
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L255:
	movb	$1, 36(%rbx)
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L253:
	movb	$1, 36(%rbx)
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L251:
	movb	$1, 36(%rbx)
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L250:
	movb	$1, 36(%rbx)
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L249:
	leal	1(%rdx), %ecx
	addq	%rsi, %rdx
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L254:
	leal	1(%rdx), %ecx
	addq	%rsi, %rdx
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L252:
	leal	1(%rdx), %ecx
	addq	%rsi, %rdx
	jmp	.L241
	.cfi_endproc
.LFE20196:
	.size	_ZN2v88internalL10WriteUCharEPNS0_18OutputStreamWriterEj, .-_ZN2v88internalL10WriteUCharEPNS0_18OutputStreamWriterEj
	.section	.text._ZNK2v88internal18SharedFunctionInfo4NameEv,"axG",@progbits,_ZNK2v88internal18SharedFunctionInfo4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal18SharedFunctionInfo4NameEv
	.type	_ZNK2v88internal18SharedFunctionInfo4NameEv, @function
_ZNK2v88internal18SharedFunctionInfo4NameEv:
.LFB15582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	15(%rdx), %rax
	testb	$1, %al
	jne	.L266
.L257:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L259:
	testb	%al, %al
	je	.L267
	movq	15(%rdx), %rbx
	testb	$1, %bl
	jne	.L268
.L262:
	movq	%rbx, %rax
.L261:
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L269
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	movq	-1(%rbx), %rax
	cmpw	$136, 11(%rax)
	jne	.L262
	leaq	-48(%rbp), %r13
	movq	%rbx, -48(%rbp)
	movq	%r13, %rdi
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	testb	%al, %al
	je	.L263
	movq	%r13, %rdi
	movq	%rbx, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L267:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	movq	-37464(%rax), %rax
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L266:
	movq	-1(%rax), %rcx
	cmpw	$136, 11(%rcx)
	jne	.L257
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	movq	(%r12), %rdx
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L263:
	movq	(%r12), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37464(%rax), %rax
	jmp	.L261
.L269:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15582:
	.size	_ZNK2v88internal18SharedFunctionInfo4NameEv, .-_ZNK2v88internal18SharedFunctionInfo4NameEv
	.section	.text._ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEPKcPNS0_9HeapEntryES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEPKcPNS0_9HeapEntryES6_
	.type	_ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEPKcPNS0_9HeapEntryES6_, @function
_ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEPKcPNS0_9HeapEntryES6_:
.LFB19772:
	.cfi_startproc
	endbr64
	movl	%esi, %r9d
	movl	(%rcx), %esi
	movq	%r8, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movl	%esi, %eax
	movups	%xmm0, 8(%rdi)
	shrl	$4, %eax
	leal	0(,%rax,8), %esi
	orl	%r9d, %esi
	movl	%esi, (%rdi)
	ret
	.cfi_endproc
.LFE19772:
	.size	_ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEPKcPNS0_9HeapEntryES6_, .-_ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEPKcPNS0_9HeapEntryES6_
	.globl	_ZN2v88internal13HeapGraphEdgeC1ENS1_4TypeEPKcPNS0_9HeapEntryES6_
	.set	_ZN2v88internal13HeapGraphEdgeC1ENS1_4TypeEPKcPNS0_9HeapEntryES6_,_ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEPKcPNS0_9HeapEntryES6_
	.section	.text._ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEiPNS0_9HeapEntryES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEiPNS0_9HeapEntryES4_
	.type	_ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEiPNS0_9HeapEntryES4_, @function
_ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEiPNS0_9HeapEntryES4_:
.LFB19775:
	.cfi_startproc
	endbr64
	movl	%esi, %r9d
	movl	(%rcx), %esi
	movq	%r8, 8(%rdi)
	movl	%edx, 16(%rdi)
	movl	%esi, %eax
	shrl	$4, %eax
	leal	0(,%rax,8), %esi
	orl	%r9d, %esi
	movl	%esi, (%rdi)
	ret
	.cfi_endproc
.LFE19775:
	.size	_ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEiPNS0_9HeapEntryES4_, .-_ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEiPNS0_9HeapEntryES4_
	.globl	_ZN2v88internal13HeapGraphEdgeC1ENS1_4TypeEiPNS0_9HeapEntryES4_
	.set	_ZN2v88internal13HeapGraphEdgeC1ENS1_4TypeEiPNS0_9HeapEntryES4_,_ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEiPNS0_9HeapEntryES4_
	.section	.text._ZN2v88internal9HeapEntryC2EPNS0_12HeapSnapshotEiNS1_4TypeEPKcjmj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9HeapEntryC2EPNS0_12HeapSnapshotEiNS1_4TypeEPKcjmj
	.type	_ZN2v88internal9HeapEntryC2EPNS0_12HeapSnapshotEiNS1_4TypeEPKcjmj, @function
_ZN2v88internal9HeapEntryC2EPNS0_12HeapSnapshotEiNS1_4TypeEPKcjmj:
.LFB19778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	andl	$268435455, %edx
	andl	$15, %ecx
	movq	%rsi, %xmm0
	salq	$4, %rdx
	orq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdx, (%rdi)
	movq	%r8, -8(%rbp)
	movq	16(%rbp), %rax
	movl	%r9d, 32(%rdi)
	movq	%rax, 8(%rdi)
	movl	24(%rbp), %eax
	movl	%eax, 36(%rdi)
	movhps	-8(%rbp), %xmm0
	movups	%xmm0, 16(%rdi)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19778:
	.size	_ZN2v88internal9HeapEntryC2EPNS0_12HeapSnapshotEiNS1_4TypeEPKcjmj, .-_ZN2v88internal9HeapEntryC2EPNS0_12HeapSnapshotEiNS1_4TypeEPKcjmj
	.globl	_ZN2v88internal9HeapEntryC1EPNS0_12HeapSnapshotEiNS1_4TypeEPKcjmj
	.set	_ZN2v88internal9HeapEntryC1EPNS0_12HeapSnapshotEiNS1_4TypeEPKcjmj,_ZN2v88internal9HeapEntryC2EPNS0_12HeapSnapshotEiNS1_4TypeEPKcjmj
	.section	.rodata._ZN2v88internal9HeapEntry5PrintEPKcS3_ii.str1.1,"aMS",@progbits,1
.LC2:
	.string	"/bigint/"
.LC3:
	.string	"/hidden/"
.LC4:
	.string	"/closure/"
.LC5:
	.string	"???"
.LC6:
	.string	"/code/"
.LC7:
	.string	"/array/"
.LC8:
	.string	"/regexp/"
.LC9:
	.string	"/number/"
.LC10:
	.string	"/native/"
.LC11:
	.string	"/synthetic/"
.LC12:
	.string	"/concatenated string/"
.LC13:
	.string	"/sliced string/"
.LC14:
	.string	"/symbol/"
.LC15:
	.string	"/object/"
.LC16:
	.string	"#"
.LC17:
	.string	""
.LC18:
	.string	"$"
.LC19:
	.string	"^"
.LC20:
	.string	"w"
.LC21:
	.string	"%6zu @%6u %*c %s%s: "
.LC22:
	.string	"%s %.40s\n"
.LC23:
	.string	"\""
.LC24:
	.string	"\"\n"
.LC25:
	.string	"%c"
.LC26:
	.string	"\\n"
.LC27:
	.string	"%d"
.LC28:
	.string	"!!! unknown edge type: %d "
	.section	.text._ZN2v88internal9HeapEntry5PrintEPKcS3_ii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9HeapEntry5PrintEPKcS3_ii
	.type	_ZN2v88internal9HeapEntry5PrintEPKcS3_ii, @function
_ZN2v88internal9HeapEntry5PrintEPKcS3_ii:
.LFB19783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r11
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	movl	$32, %r8d
	pushq	%rbx
	movl	%r12d, %ecx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$-128, %rsp
	movl	32(%rdi), %edx
	movq	8(%rdi), %rsi
	leaq	.LC21(%rip), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	pushq	%r11
	call	_ZN2v84base2OS5PrintEPKcz@PLT
	movzbl	(%rbx), %eax
	popq	%rdx
	popq	%rcx
	andl	$15, %eax
	cmpb	$2, %al
	je	.L275
	movq	24(%rbx), %r8
	cmpb	$13, %al
	ja	.L276
	leaq	.L278(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal9HeapEntry5PrintEPKcS3_ii,"a",@progbits
	.align 4
	.align 4
.L278:
	.long	.L318-.L278
	.long	.L289-.L278
	.long	.L276-.L278
	.long	.L288-.L278
	.long	.L287-.L278
	.long	.L286-.L278
	.long	.L285-.L278
	.long	.L284-.L278
	.long	.L283-.L278
	.long	.L282-.L278
	.long	.L281-.L278
	.long	.L280-.L278
	.long	.L279-.L278
	.long	.L277-.L278
	.section	.text._ZN2v88internal9HeapEntry5PrintEPKcS3_ii
	.p2align 4,,10
	.p2align 3
.L275:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rdi
	leaq	.LC26(%rip), %r13
	call	_ZN2v84base2OS5PrintEPKcz@PLT
	movq	24(%rbx), %r14
	movzbl	(%r14), %eax
	testb	%al, %al
	jne	.L292
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L329:
	movsbl	%al, %esi
	leaq	.LC25(%rip), %rdi
	xorl	%eax, %eax
	addq	$1, %r14
	call	_ZN2v84base2OS5PrintEPKcz@PLT
	movzbl	(%r14), %eax
	testb	%al, %al
	je	.L293
.L324:
	movq	%r14, %rdx
	subq	24(%rbx), %rdx
	cmpq	$40, %rdx
	jg	.L293
.L292:
	cmpb	$10, %al
	jne	.L329
	xorl	%eax, %eax
	movq	%r13, %rdi
	addq	$1, %r14
	call	_ZN2v84base2OS5PrintEPKcz@PLT
	movzbl	(%r14), %eax
	testb	%al, %al
	jne	.L324
	.p2align 4,,10
	.p2align 3
.L293:
	leaq	.LC24(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v84base2OS5PrintEPKcz@PLT
.L291:
	subl	$1, %r15d
	movl	%r15d, -148(%rbp)
	je	.L274
	movq	16(%rbx), %rsi
	movl	(%rbx), %eax
	movq	376(%rsi), %rdi
	testl	$-16, %eax
	jne	.L298
	movq	%rdi, %r9
.L305:
	movl	4(%rbx), %eax
	leaq	(%rdi,%rax,8), %rax
	cmpq	%rax, %r9
	je	.L274
	addl	$2, %r12d
	movq	%r9, %r15
	leaq	-128(%rbp), %r14
	leaq	.L308(%rip), %r13
.L316:
	movq	(%r15), %r8
	movq	%r14, -144(%rbp)
	movq	$64, -136(%rbp)
	movl	(%r8), %eax
	andl	$7, %eax
	cmpl	$6, %eax
	ja	.L306
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal9HeapEntry5PrintEPKcS3_ii
	.align 4
	.align 4
.L308:
	.long	.L314-.L308
	.long	.L313-.L308
	.long	.L312-.L308
	.long	.L311-.L308
	.long	.L310-.L308
	.long	.L309-.L308
	.long	.L307-.L308
	.section	.text._ZN2v88internal9HeapEntry5PrintEPKcS3_ii
	.p2align 4,,10
	.p2align 3
.L309:
	movq	16(%r8), %rdx
	leaq	.LC19(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L315:
	movq	8(%r8), %rdi
	movl	-148(%rbp), %ecx
	movl	%r12d, %r8d
	addq	$8, %r15
	call	_ZN2v88internal9HeapEntry5PrintEPKcS3_ii
	movq	16(%rbx), %rax
	movl	4(%rbx), %edx
	movq	376(%rax), %rax
	leaq	(%rax,%rdx,8), %rax
	cmpq	%r15, %rax
	jne	.L316
.L274:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L330
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore_state
	movl	16(%r8), %ecx
	movq	-136(%rbp), %rsi
	leaq	.LC27(%rip), %rdx
	xorl	%eax, %eax
	movq	-144(%rbp), %rdi
	movq	%r8, -160(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-160(%rbp), %r8
	movq	%r14, %rdx
	leaq	.LC18(%rip), %rsi
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L311:
	movq	16(%r8), %rdx
	leaq	.LC18(%rip), %rsi
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L312:
	movq	16(%r8), %rdx
	leaq	.LC17(%rip), %rsi
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L307:
	movq	16(%r8), %rdx
	leaq	.LC20(%rip), %rsi
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L313:
	movq	%r8, -160(%rbp)
	movl	16(%r8), %ecx
	leaq	.LC27(%rip), %rdx
	movq	-144(%rbp), %rdi
	movq	-136(%rbp), %rsi
.L328:
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-160(%rbp), %r8
	movq	%r14, %rdx
	leaq	.LC17(%rip), %rsi
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L314:
	movq	16(%r8), %rdx
	leaq	.LC16(%rip), %rsi
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L298:
	movq	232(%rsi), %rdx
	shrl	$4, %eax
	movabsq	$-3689348814741910323, %r8
	subl	$1, %eax
	movq	%rdx, %rcx
	subq	240(%rsi), %rcx
	cltq
	sarq	$3, %rcx
	imulq	%r8, %rcx
	addq	%rax, %rcx
	js	.L301
	cmpq	$11, %rcx
	jg	.L302
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rax
.L303:
	movq	16(%rax), %rcx
	movl	4(%rax), %edx
	movq	376(%rcx), %rax
	leaq	(%rax,%rdx,8), %r9
	jmp	.L305
.L306:
	movq	%r8, -160(%rbp)
	movl	$7, %ecx
	movq	%r14, %rdi
	movl	$64, %esi
	leaq	.LC28(%rip), %rdx
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L301:
	movabsq	$-6148914691236517205, %r8
	movq	%rcx, %rdx
	notq	%rdx
	movq	%rdx, %rax
	mulq	%r8
	shrq	$3, %rdx
	notq	%rdx
.L304:
	movq	256(%rsi), %rsi
	leaq	(%rdx,%rdx,2), %rax
	salq	$2, %rax
	subq	%rax, %rcx
	movq	(%rsi,%rdx,8), %rax
	leaq	(%rcx,%rcx,4), %rcx
	leaq	(%rax,%rcx,8), %rax
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L302:
	movabsq	$3074457345618258603, %rdx
	movq	%rcx, %rax
	imulq	%rdx
	movq	%rcx, %rax
	sarq	$63, %rax
	sarq	%rdx
	subq	%rax, %rdx
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L318:
	leaq	.LC3(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L290:
	movq	%r8, %rdx
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v84base2OS5PrintEPKcz@PLT
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L288:
	leaq	.LC15(%rip), %rsi
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L289:
	leaq	.LC7(%rip), %rsi
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L286:
	leaq	.LC4(%rip), %rsi
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L287:
	leaq	.LC6(%rip), %rsi
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L282:
	leaq	.LC11(%rip), %rsi
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L283:
	leaq	.LC10(%rip), %rsi
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L280:
	leaq	.LC13(%rip), %rsi
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L281:
	leaq	.LC12(%rip), %rsi
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L284:
	leaq	.LC9(%rip), %rsi
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L285:
	leaq	.LC8(%rip), %rsi
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L277:
	leaq	.LC2(%rip), %rsi
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L279:
	leaq	.LC14(%rip), %rsi
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L276:
	leaq	.LC5(%rip), %rsi
	jmp	.L290
.L330:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19783:
	.size	_ZN2v88internal9HeapEntry5PrintEPKcS3_ii, .-_ZN2v88internal9HeapEntry5PrintEPKcS3_ii
	.section	.rodata._ZN2v88internal9HeapEntry12TypeAsStringEv.str1.1,"aMS",@progbits,1
.LC29:
	.string	"/string/"
	.section	.text._ZN2v88internal9HeapEntry12TypeAsStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9HeapEntry12TypeAsStringEv
	.type	_ZN2v88internal9HeapEntry12TypeAsStringEv, @function
_ZN2v88internal9HeapEntry12TypeAsStringEv:
.LFB19785:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %eax
	andl	$15, %eax
	cmpb	$13, %al
	ja	.L332
	leaq	.L334(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal9HeapEntry12TypeAsStringEv,"a",@progbits
	.align 4
	.align 4
.L334:
	.long	.L348-.L334
	.long	.L346-.L334
	.long	.L345-.L334
	.long	.L344-.L334
	.long	.L343-.L334
	.long	.L342-.L334
	.long	.L341-.L334
	.long	.L340-.L334
	.long	.L339-.L334
	.long	.L338-.L334
	.long	.L337-.L334
	.long	.L336-.L334
	.long	.L335-.L334
	.long	.L333-.L334
	.section	.text._ZN2v88internal9HeapEntry12TypeAsStringEv
	.p2align 4,,10
	.p2align 3
.L344:
	leaq	.LC15(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	leaq	.LC3(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	leaq	.LC2(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	leaq	.LC4(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L341:
	leaq	.LC8(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	leaq	.LC9(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	leaq	.LC10(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	leaq	.LC6(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	leaq	.LC7(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L345:
	leaq	.LC29(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	leaq	.LC11(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	leaq	.LC12(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	leaq	.LC13(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	leaq	.LC14(%rip), %rax
	ret
.L332:
	leaq	.LC5(%rip), %rax
	ret
	.cfi_endproc
.LFE19785:
	.size	_ZN2v88internal9HeapEntry12TypeAsStringEv, .-_ZN2v88internal9HeapEntry12TypeAsStringEv
	.section	.text._ZN2v88internal12HeapSnapshotC2EPNS0_12HeapProfilerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapSnapshotC2EPNS0_12HeapProfilerE
	.type	_ZN2v88internal12HeapSnapshotC2EPNS0_12HeapProfilerE, @function
_ZN2v88internal12HeapSnapshotC2EPNS0_12HeapProfilerE:
.LFB19826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 216(%rdi)
	movq	$8, 224(%rdi)
	movups	%xmm0, 232(%rdi)
	movups	%xmm0, 248(%rdi)
	movups	%xmm0, 264(%rdi)
	movups	%xmm0, 280(%rdi)
	movl	$64, %edi
	call	_Znwm@PLT
	movq	224(%rbx), %rdx
	movl	$480, %edi
	movq	%rax, 216(%rbx)
	leaq	-4(,%rdx,4), %r12
	andq	$-8, %r12
	addq	%rax, %r12
	call	_Znwm@PLT
	movq	%r12, 256(%rbx)
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	leaq	480(%rax), %rdx
	movq	%rax, (%r12)
	movq	%rax, %xmm1
	movq	%rdx, 248(%rbx)
	punpcklqdq	%xmm1, %xmm1
	movq	%r12, 288(%rbx)
	movq	%rdx, 280(%rbx)
	movq	%rax, 272(%rbx)
	movq	%rax, 264(%rbx)
	movq	$0, 296(%rbx)
	movq	$8, 304(%rbx)
	movups	%xmm1, 232(%rbx)
	movups	%xmm0, 312(%rbx)
	movups	%xmm0, 328(%rbx)
	movups	%xmm0, 344(%rbx)
	movups	%xmm0, 360(%rbx)
	call	_Znwm@PLT
	movq	304(%rbx), %rdx
	movl	$504, %edi
	movq	%rax, 296(%rbx)
	leaq	-4(,%rdx,4), %r12
	andq	$-8, %r12
	addq	%rax, %r12
	call	_Znwm@PLT
	leaq	32(%rbx), %rdi
	pxor	%xmm0, %xmm0
	movq	%r12, 336(%rbx)
	movq	%rax, %xmm1
	movq	%rax, (%r12)
	leaq	504(%rax), %rdx
	andq	$-8, %rdi
	movq	%rax, 352(%rbx)
	punpcklqdq	%xmm1, %xmm1
	movq	%rax, 344(%rbx)
	leaq	448(%rbx), %rax
	movq	%rax, 400(%rbx)
	xorl	%eax, %eax
	movq	%rdx, 328(%rbx)
	movq	%r12, 368(%rbx)
	movq	%rdx, 360(%rbx)
	movq	$0, 392(%rbx)
	movq	$1, 408(%rbx)
	movq	$0, 416(%rbx)
	movq	$0, 424(%rbx)
	movl	$0x3f800000, 432(%rbx)
	movq	$0, 440(%rbx)
	movl	$-1, 480(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 208(%rbx)
	movups	%xmm1, 312(%rbx)
	movups	%xmm0, 376(%rbx)
	movups	%xmm0, 448(%rbx)
	movups	%xmm0, 464(%rbx)
	subl	%edi, %ebx
	leal	216(%rbx), %ecx
	shrl	$3, %ecx
	rep stosq
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19826:
	.size	_ZN2v88internal12HeapSnapshotC2EPNS0_12HeapProfilerE, .-_ZN2v88internal12HeapSnapshotC2EPNS0_12HeapProfilerE
	.globl	_ZN2v88internal12HeapSnapshotC1EPNS0_12HeapProfilerE
	.set	_ZN2v88internal12HeapSnapshotC1EPNS0_12HeapProfilerE,_ZN2v88internal12HeapSnapshotC2EPNS0_12HeapProfilerE
	.section	.text._ZN2v88internal12HeapSnapshot6DeleteEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapSnapshot6DeleteEv
	.type	_ZN2v88internal12HeapSnapshot6DeleteEv, @function
_ZN2v88internal12HeapSnapshot6DeleteEv:
.LFB19828:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	movq	(%rdi), %rdi
	jmp	_ZN2v88internal12HeapProfiler14RemoveSnapshotEPNS0_12HeapSnapshotE@PLT
	.cfi_endproc
.LFE19828:
	.size	_ZN2v88internal12HeapSnapshot6DeleteEv, .-_ZN2v88internal12HeapSnapshot6DeleteEv
	.section	.text._ZN2v88internal12HeapSnapshot22RememberLastJSObjectIdEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapSnapshot22RememberLastJSObjectIdEv
	.type	_ZN2v88internal12HeapSnapshot22RememberLastJSObjectIdEv, @function
_ZN2v88internal12HeapSnapshot22RememberLastJSObjectIdEv:
.LFB19829:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
	subl	$2, %eax
	movl	%eax, 480(%rdi)
	ret
	.cfi_endproc
.LFE19829:
	.size	_ZN2v88internal12HeapSnapshot22RememberLastJSObjectIdEv, .-_ZN2v88internal12HeapSnapshot22RememberLastJSObjectIdEv
	.section	.rodata._ZN2v88internal12HeapSnapshot11AddLocationEPNS0_9HeapEntryEiii.str1.1,"aMS",@progbits,1
.LC31:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal12HeapSnapshot11AddLocationEPNS0_9HeapEntryEiii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapSnapshot11AddLocationEPNS0_9HeapEntryEiii
	.type	_ZN2v88internal12HeapSnapshot11AddLocationEPNS0_9HeapEntryEiii, @function
_ZN2v88internal12HeapSnapshot11AddLocationEPNS0_9HeapEntryEiii:
.LFB19834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %eax
	movq	464(%rdi), %rbx
	movl	%edx, -56(%rbp)
	movl	%ecx, -64(%rbp)
	shrl	$4, %eax
	movl	%r8d, -68(%rbp)
	movl	%eax, %r12d
	cmpq	472(%rdi), %rbx
	je	.L354
	movd	%edx, %xmm3
	movd	%r8d, %xmm2
	movd	%ecx, %xmm1
	movd	%eax, %xmm0
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	addq	$16, 464(%rdi)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	movq	456(%rdi), %r15
	movq	%rbx, %rcx
	movabsq	$576460752303423487, %rsi
	subq	%r15, %rcx
	movq	%rcx, %rax
	sarq	$4, %rax
	cmpq	%rsi, %rax
	je	.L371
	testq	%rax, %rax
	je	.L363
	movabsq	$9223372036854775792, %rdx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L372
.L357:
	movq	%rdx, %rdi
	movq	%rcx, -88(%rbp)
	movq	%rdx, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rcx
	movq	%rax, %r14
	leaq	(%rax,%rdx), %rax
	leaq	16(%r14), %rdx
.L358:
	movd	-64(%rbp), %xmm1
	movd	-68(%rbp), %xmm6
	movd	%r12d, %xmm0
	movd	-56(%rbp), %xmm7
	punpckldq	%xmm6, %xmm1
	punpckldq	%xmm7, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r14,%rcx)
	cmpq	%r15, %rbx
	je	.L359
	subq	$16, %rbx
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	subq	%r15, %rbx
	movq	%rbx, %rsi
	shrq	$4, %rsi
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L360:
	movdqu	(%r15,%rdx), %xmm5
	addq	$1, %rcx
	movups	%xmm5, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rcx
	jb	.L360
	leaq	32(%r14,%rbx), %rdx
.L361:
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rax
.L362:
	movq	%r14, %xmm0
	movq	%rdx, %xmm6
	movq	%rax, 472(%r13)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 456(%r13)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	testq	%r15, %r15
	je	.L362
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L372:
	testq	%rdi, %rdi
	jne	.L373
	movl	$16, %edx
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L363:
	movl	$16, %edx
	jmp	.L357
.L371:
	leaq	.LC31(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L373:
	cmpq	%rsi, %rdi
	movq	%rsi, %rax
	cmovbe	%rdi, %rax
	salq	$4, %rax
	movq	%rax, %rdx
	jmp	.L357
	.cfi_endproc
.LFE19834:
	.size	_ZN2v88internal12HeapSnapshot11AddLocationEPNS0_9HeapEntryEiii, .-_ZN2v88internal12HeapSnapshot11AddLocationEPNS0_9HeapEntryEiii
	.section	.rodata._ZN2v88internal12HeapSnapshot8AddEntryENS0_9HeapEntry4TypeEPKcjmj.str1.8,"aMS",@progbits,1
	.align 8
.LC32:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZN2v88internal12HeapSnapshot8AddEntryENS0_9HeapEntry4TypeEPKcjmj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapSnapshot8AddEntryENS0_9HeapEntry4TypeEPKcjmj
	.type	_ZN2v88internal12HeapSnapshot8AddEntryENS0_9HeapEntry4TypeEPKcjmj, @function
_ZN2v88internal12HeapSnapshot8AddEntryENS0_9HeapEntry4TypeEPKcjmj:
.LFB19835:
	.cfi_startproc
	endbr64
	movabsq	$-3689348814741910323, %r11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r9d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	288(%rdi), %r14
	movq	264(%rdi), %rax
	movq	%rdx, -64(%rbp)
	movq	256(%rdi), %rsi
	movq	%r14, %r10
	movq	%rax, %rdi
	subq	272(%rbx), %rdi
	subq	%rsi, %r10
	sarq	$3, %rdi
	imulq	%r11, %rdi
	movq	%r10, %rdx
	sarq	$3, %rdx
	leaq	-3(%rdx,%rdx,2), %r12
	leaq	(%rdi,%r12,4), %rdi
	movq	248(%rbx), %r12
	subq	232(%rbx), %r12
	sarq	$3, %r12
	imulq	%r11, %r12
	addq	%rdi, %r12
	movq	280(%rbx), %rdi
	subq	$40, %rdi
	cmpq	%rdi, %rax
	je	.L375
	salq	$4, %r12
	andl	$15, %r13d
	movq	%rbx, %xmm0
	movl	%ecx, 32(%rax)
	movl	%r12d, %r12d
	movhps	-64(%rbp), %xmm0
	movq	%r8, 8(%rax)
	orq	%r12, %r13
	movl	%r9d, 36(%rax)
	movq	%r13, (%rax)
	movups	%xmm0, 16(%rax)
	movq	264(%rbx), %rax
	movq	288(%rbx), %rdx
	addq	$40, %rax
	movq	%rax, 264(%rbx)
	cmpq	272(%rbx), %rax
	je	.L376
.L377:
	addq	$72, %rsp
	subq	$40, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	movabsq	$230584300921369395, %rax
	cmpq	%rax, %r12
	je	.L386
	movq	216(%rbx), %r11
	movq	224(%rbx), %rdi
	movq	%r14, %rax
	subq	%r11, %rax
	movq	%rdi, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L387
.L379:
	movq	%rbx, %xmm0
	movl	%ecx, -72(%rbp)
	salq	$4, %r12
	andl	$15, %r13d
	movhps	-64(%rbp), %xmm0
	movl	$480, %edi
	movq	%r8, -80(%rbp)
	movl	%r12d, %r12d
	movaps	%xmm0, -64(%rbp)
	orq	%r12, %r13
	call	_Znwm@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %r8
	movq	%rax, 8(%r14)
	movdqa	-64(%rbp), %xmm0
	movq	264(%rbx), %rax
	movq	%r8, 8(%rax)
	movl	%ecx, 32(%rax)
	movq	%r13, (%rax)
	movl	%r15d, 36(%rax)
	movups	%xmm0, 16(%rax)
	movq	288(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 288(%rbx)
	movq	8(%rax), %rax
	leaq	480(%rax), %rcx
	movq	%rax, 272(%rbx)
	movq	%rcx, 280(%rbx)
	movq	%rax, 264(%rbx)
.L376:
	movq	-8(%rdx), %rax
	addq	$480, %rax
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L387:
	addq	$2, %rdx
	leaq	(%rdx,%rdx), %rax
	cmpq	%rax, %rdi
	ja	.L388
	testq	%rdi, %rdi
	movl	$1, %eax
	cmovne	%rdi, %rax
	leaq	2(%rdi,%rax), %r14
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r14
	ja	.L389
	leaq	0(,%r14,8), %rdi
	movq	%r8, -104(%rbp)
	movl	%ecx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %r10
	movq	%rax, %rsi
	movl	-96(%rbp), %ecx
	movq	-104(%rbp), %r8
	movq	%rax, -72(%rbp)
	movq	%r14, %rax
	subq	%rdx, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r11
	movq	288(%rbx), %rax
	movq	256(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L384
	movq	%r11, %rdi
	subq	%rsi, %rdx
	movq	%r8, -96(%rbp)
	movl	%ecx, -88(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %ecx
	movq	-80(%rbp), %r10
	movq	%rax, %r11
.L384:
	movq	216(%rbx), %rdi
	movq	%r8, -104(%rbp)
	movl	%ecx, -96(%rbp)
	movq	%r11, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rax
	movq	%r14, 224(%rbx)
	movq	-104(%rbp), %r8
	movl	-96(%rbp), %ecx
	movq	-88(%rbp), %r11
	movq	-80(%rbp), %r10
	movq	%rax, 216(%rbx)
.L382:
	movq	%r11, 256(%rbx)
	movq	(%r11), %rax
	leaq	(%r11,%r10), %r14
	movq	(%r11), %xmm0
	movq	%r14, 288(%rbx)
	addq	$480, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 240(%rbx)
	movq	(%r14), %rax
	movq	%rax, 272(%rbx)
	addq	$480, %rax
	movq	%rax, 280(%rbx)
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L388:
	subq	%rdx, %rdi
	addq	$8, %r14
	shrq	%rdi
	movq	%r14, %rdx
	leaq	(%r11,%rdi,8), %r11
	subq	%rsi, %rdx
	cmpq	%r11, %rsi
	jbe	.L381
	cmpq	%r14, %rsi
	je	.L382
	movq	%r11, %rdi
	movq	%r8, -88(%rbp)
	movl	%ecx, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %r10
	movl	-80(%rbp), %ecx
	movq	-88(%rbp), %r8
	movq	%rax, %r11
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L381:
	cmpq	%r14, %rsi
	je	.L382
	leaq	8(%r10), %rdi
	movq	%r8, -96(%rbp)
	subq	%rdx, %rdi
	movl	%ecx, -88(%rbp)
	addq	%r11, %rdi
	movq	%r10, -80(%rbp)
	movq	%r11, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %r11
	movq	-80(%rbp), %r10
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %r8
	jmp	.L382
.L386:
	leaq	.LC32(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L389:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE19835:
	.size	_ZN2v88internal12HeapSnapshot8AddEntryENS0_9HeapEntry4TypeEPKcjmj, .-_ZN2v88internal12HeapSnapshot8AddEntryENS0_9HeapEntry4TypeEPKcjmj
	.section	.text._ZN2v88internal12HeapSnapshot12AddRootEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapSnapshot12AddRootEntryEv
	.type	_ZN2v88internal12HeapSnapshot12AddRootEntryEv, @function
_ZN2v88internal12HeapSnapshot12AddRootEntryEv:
.LFB19831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	leaq	.LC17(%rip), %rdx
	movl	$9, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal12HeapSnapshot8AddEntryENS0_9HeapEntry4TypeEPKcjmj
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19831:
	.size	_ZN2v88internal12HeapSnapshot12AddRootEntryEv, .-_ZN2v88internal12HeapSnapshot12AddRootEntryEv
	.section	.rodata._ZN2v88internal12HeapSnapshot15AddGcRootsEntryEv.str1.1,"aMS",@progbits,1
.LC33:
	.string	"(GC roots)"
	.section	.text._ZN2v88internal12HeapSnapshot15AddGcRootsEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapSnapshot15AddGcRootsEntryEv
	.type	_ZN2v88internal12HeapSnapshot15AddGcRootsEntryEv, @function
_ZN2v88internal12HeapSnapshot15AddGcRootsEntryEv:
.LFB19832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$3, %ecx
	leaq	.LC33(%rip), %rdx
	movl	$9, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal12HeapSnapshot8AddEntryENS0_9HeapEntry4TypeEPKcjmj
	movq	%rax, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19832:
	.size	_ZN2v88internal12HeapSnapshot15AddGcRootsEntryEv, .-_ZN2v88internal12HeapSnapshot15AddGcRootsEntryEv
	.section	.text._ZN2v88internal12HeapSnapshot17AddGcSubrootEntryENS0_4RootEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapSnapshot17AddGcSubrootEntryENS0_4RootEj
	.type	_ZN2v88internal12HeapSnapshot17AddGcSubrootEntryENS0_4RootEj, @function
_ZN2v88internal12HeapSnapshot17AddGcSubrootEntryENS0_4RootEj:
.LFB19833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movslq	%esi, %rbx
	movl	%ebx, %edi
	subq	$8, %rsp
	call	_ZN2v88internal11RootVisitor8RootNameENS0_4RootE@PLT
	movl	%r13d, %ecx
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movq	%rax, %rdx
	xorl	%r8d, %r8d
	movl	$9, %esi
	call	_ZN2v88internal12HeapSnapshot8AddEntryENS0_9HeapEntry4TypeEPKcjmj
	movq	%rax, 24(%r12,%rbx,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19833:
	.size	_ZN2v88internal12HeapSnapshot17AddGcSubrootEntryENS0_4RootEj, .-_ZN2v88internal12HeapSnapshot17AddGcSubrootEntryENS0_4RootEj
	.section	.text._ZN2v88internal12HeapSnapshot23AddSyntheticRootEntriesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapSnapshot23AddSyntheticRootEntriesEv
	.type	_ZN2v88internal12HeapSnapshot23AddSyntheticRootEntriesEv, @function
_ZN2v88internal12HeapSnapshot23AddSyntheticRootEntriesEv:
.LFB19830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	leaq	.LC17(%rip), %rdx
	movl	$9, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r15, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movabsq	$-3689348814741910323, %rbx
	subq	$56, %rsp
	call	_ZN2v88internal12HeapSnapshot8AddEntryENS0_9HeapEntry4TypeEPKcjmj
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movq	%rax, 8(%r13)
	leaq	.LC33(%rip), %rdx
	movl	$9, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal12HeapSnapshot8AddEntryENS0_9HeapEntry4TypeEPKcjmj
	movq	%rax, 16(%r13)
	.p2align 4,,10
	.p2align 3
.L409:
	leal	5(%r12,%r12), %eax
	movl	%r12d, %edi
	movl	%eax, -68(%rbp)
	call	_ZN2v88internal11RootVisitor8RootNameENS0_4RootE@PLT
	movq	288(%r13), %r14
	movq	264(%r13), %rdx
	movq	256(%r13), %rsi
	movq	%rax, -64(%rbp)
	movq	%r14, %r8
	movq	%rdx, %rax
	subq	272(%r13), %rax
	movq	280(%r13), %rcx
	subq	%rsi, %r8
	sarq	$3, %rax
	imulq	%rbx, %rax
	movq	%r8, %rdi
	sarq	$3, %rdi
	leaq	-3(%rdi,%rdi,2), %r9
	leaq	(%rax,%r9,4), %r15
	movq	248(%r13), %rax
	subq	232(%r13), %rax
	sarq	$3, %rax
	imulq	%rbx, %rax
	addq	%rax, %r15
	leaq	-40(%rcx), %rax
	cmpq	%rax, %rdx
	je	.L397
	salq	$4, %r15
	movq	%r13, %xmm0
	leal	5(%r12,%r12), %eax
	movq	$0, 8(%rdx)
	movl	%r15d, %r15d
	movhps	-64(%rbp), %xmm0
	movl	%eax, 32(%rdx)
	orq	$9, %r15
	movl	$0, 36(%rdx)
	movq	%r15, (%rdx)
	movups	%xmm0, 16(%rdx)
	movq	264(%r13), %rdx
	movq	288(%r13), %rsi
	leaq	40(%rdx), %rax
	movq	%rax, 264(%r13)
	cmpq	272(%r13), %rax
	je	.L398
	movq	%rdx, 24(%r13,%r12,8)
	addq	$1, %r12
	cmpq	$24, %r12
	jne	.L409
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L397:
	.cfi_restore_state
	movabsq	$230584300921369395, %rax
	cmpq	%rax, %r15
	je	.L418
	movq	216(%r13), %r10
	movq	224(%r13), %rdx
	movq	%r14, %rax
	subq	%r10, %rax
	movq	%rdx, %rcx
	sarq	$3, %rax
	subq	%rax, %rcx
	cmpq	$1, %rcx
	jbe	.L419
.L401:
	movq	%r13, %xmm0
	movl	$480, %edi
	salq	$4, %r15
	movhps	-64(%rbp), %xmm0
	movl	%r15d, %r15d
	movaps	%xmm0, -64(%rbp)
	orq	$9, %r15
	call	_Znwm@PLT
	movdqa	-64(%rbp), %xmm0
	movl	-68(%rbp), %ecx
	movq	%rax, 8(%r14)
	movq	264(%r13), %rax
	movq	$0, 8(%rax)
	movq	%r15, (%rax)
	movl	%ecx, 32(%rax)
	movl	$0, 36(%rax)
	movups	%xmm0, 16(%rax)
	movq	288(%r13), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, 288(%r13)
	movq	8(%rax), %rax
	leaq	480(%rax), %rdx
	movq	%rax, 272(%r13)
	movq	%rdx, 280(%r13)
	movq	%rax, 264(%r13)
.L398:
	movq	-8(%rsi), %rax
	addq	$440, %rax
	movq	%rax, 24(%r13,%r12,8)
	addq	$1, %r12
	cmpq	$24, %r12
	jne	.L409
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L419:
	.cfi_restore_state
	leaq	2(%rdi), %r9
	leaq	(%r9,%r9), %rax
	cmpq	%rax, %rdx
	jbe	.L402
	subq	%r9, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r10,%rdx,8), %r9
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r9, %rsi
	jbe	.L403
	cmpq	%r14, %rsi
	je	.L404
	movq	%r9, %rdi
	movq	%r8, -80(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r8
	movq	%rax, %r9
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L402:
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r14
	ja	.L420
	leaq	0(,%r14,8), %rdi
	movq	%r9, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %r9
	movq	256(%r13), %rsi
	movq	%rax, %rcx
	movq	%rax, -80(%rbp)
	movq	%r14, %rax
	movq	-88(%rbp), %r8
	subq	%r9, %rax
	shrq	%rax
	leaq	(%rcx,%rax,8), %r9
	movq	288(%r13), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L406
	movq	%r9, %rdi
	subq	%rsi, %rdx
	call	memmove@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %r9
.L406:
	movq	216(%r13), %rdi
	movq	%r9, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rax
	movq	%r14, 224(%r13)
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r8
	movq	%rax, 216(%r13)
.L404:
	movq	%r9, 256(%r13)
	movq	(%r9), %rax
	leaq	(%r9,%r8), %r14
	movq	(%r9), %xmm0
	movq	%r14, 288(%r13)
	addq	$480, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 240(%r13)
	movq	(%r14), %rax
	movq	%rax, 272(%r13)
	addq	$480, %rax
	movq	%rax, 280(%r13)
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L403:
	cmpq	%r14, %rsi
	je	.L404
	leaq	8(%r8), %rdi
	movq	%r8, -88(%rbp)
	subq	%rdx, %rdi
	movq	%r9, -80(%rbp)
	addq	%r9, %rdi
	call	memmove@PLT
	movq	-80(%rbp), %r9
	movq	-88(%rbp), %r8
	jmp	.L404
.L418:
	leaq	.LC32(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L420:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE19830:
	.size	_ZN2v88internal12HeapSnapshot23AddSyntheticRootEntriesEv, .-_ZN2v88internal12HeapSnapshot23AddSyntheticRootEntriesEv
	.section	.rodata._ZN2v88internal29EmbedderGraphEntriesAllocator13AllocateEntryEPv.str1.1,"aMS",@progbits,1
.LC34:
	.string	"%s %s"
	.section	.text._ZN2v88internal29EmbedderGraphEntriesAllocator13AllocateEntryEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29EmbedderGraphEntriesAllocator13AllocateEntryEPv
	.type	_ZN2v88internal29EmbedderGraphEntriesAllocator13AllocateEntryEPv, @function
_ZN2v88internal29EmbedderGraphEntriesAllocator13AllocateEntryEPv:
.LFB20092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	leal	(%r12,%r12), %r14d
	subq	$24, %rsp
	movq	(%rsi), %rax
	call	*24(%rax)
	leaq	_ZN2v813EmbedderGraph4Node10NamePrefixEv(%rip), %rcx
	movq	8(%rbx), %r13
	movq	16(%rbx), %rbx
	movslq	%eax, %r15
	movq	(%r12), %rax
	movq	56(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L432
.L422:
	movq	%r12, %rdi
	call	*16(%rax)
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14StringsStorage7GetCopyEPKc@PLT
	movq	%rax, %rdx
.L424:
	movq	(%r12), %rax
	leaq	_ZN2v813EmbedderGraph4Node10IsRootNodeEv(%rip), %rcx
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L425
.L427:
	movl	$8, %esi
.L426:
	addq	$24, %rsp
	movq	%r15, %r8
	movl	%r14d, %ecx
	movq	%r13, %rdi
	popq	%rbx
	xorl	%r9d, %r9d
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12HeapSnapshot8AddEntryENS0_9HeapEntry4TypeEPKcjmj
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rdx
	testq	%rax, %rax
	je	.L433
	movq	%rax, -56(%rbp)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movq	-56(%rbp), %rdx
	leaq	.LC34(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal14StringsStorage12GetFormattedEPKcz@PLT
	movq	%rax, %rdx
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L425:
	movq	%rdx, -56(%rbp)
	movq	%r12, %rdi
	call	*%rax
	movq	-56(%rbp), %rdx
	testb	%al, %al
	je	.L427
	movl	$9, %esi
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L433:
	movq	(%r12), %rax
	jmp	.L422
	.cfi_endproc
.LFE20092:
	.size	_ZN2v88internal29EmbedderGraphEntriesAllocator13AllocateEntryEPv, .-_ZN2v88internal29EmbedderGraphEntriesAllocator13AllocateEntryEPv
	.section	.text._ZN2v88internal12HeapSnapshot5PrintEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapSnapshot5PrintEi
	.type	_ZN2v88internal12HeapSnapshot5PrintEi, @function
_ZN2v88internal12HeapSnapshot5PrintEi:
.LFB19838:
	.cfi_startproc
	endbr64
	leaq	.LC17(%rip), %rdx
	movq	8(%rdi), %rdi
	movl	%esi, %ecx
	xorl	%r8d, %r8d
	movq	%rdx, %rsi
	jmp	_ZN2v88internal9HeapEntry5PrintEPKcS3_ii
	.cfi_endproc
.LFE19838:
	.size	_ZN2v88internal12HeapSnapshot5PrintEi, .-_ZN2v88internal12HeapSnapshot5PrintEi
	.section	.rodata._ZN2v88internal14HeapObjectsMapC2EPNS0_4HeapE.str1.8,"aMS",@progbits,1
	.align 8
.LC35:
	.string	"Out of memory: HashMap::Initialize"
	.section	.text._ZN2v88internal14HeapObjectsMapC2EPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14HeapObjectsMapC2EPNS0_4HeapE
	.type	_ZN2v88internal14HeapObjectsMapC2EPNS0_4HeapE, @function
_ZN2v88internal14HeapObjectsMapC2EPNS0_4HeapE:
.LFB19861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$53, (%rdi)
	movl	$192, %edi
	call	malloc@PLT
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.L438
	movq	%r12, 80(%rbx)
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	$8, 16(%rbx)
	movq	$0, (%rax)
	movq	$0, 24(%rax)
	movq	$0, 48(%rax)
	movq	$0, 72(%rax)
	movq	$0, 96(%rax)
	movq	$0, 120(%rax)
	movq	$0, 144(%rax)
	movq	$0, 168(%rax)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 64(%rbx)
	call	_Znwm@PLT
	leaq	24(%rax), %rdx
	movl	$0, (%rax)
	movq	$0, 8(%rax)
	movl	$0, 16(%rax)
	movb	$1, 20(%rax)
	movq	%rax, 32(%rbx)
	movq	%rdx, 40(%rbx)
	movq	%rdx, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L438:
	.cfi_restore_state
	leaq	.LC35(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19861:
	.size	_ZN2v88internal14HeapObjectsMapC2EPNS0_4HeapE, .-_ZN2v88internal14HeapObjectsMapC2EPNS0_4HeapE
	.globl	_ZN2v88internal14HeapObjectsMapC1EPNS0_4HeapE
	.set	_ZN2v88internal14HeapObjectsMapC1EPNS0_4HeapE,_ZN2v88internal14HeapObjectsMapC2EPNS0_4HeapE
	.section	.rodata._ZN2v88internal14HeapObjectsMap9FindEntryEm.str1.8,"aMS",@progbits,1
	.align 8
.LC36:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internal14HeapObjectsMap9FindEntryEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14HeapObjectsMap9FindEntryEm
	.type	_ZN2v88internal14HeapObjectsMap9FindEntryEm, @function
_ZN2v88internal14HeapObjectsMap9FindEntryEm:
.LFB19865:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	movl	16(%rdi), %ecx
	movq	8(%rdi), %r9
	sall	$15, %eax
	subl	%esi, %eax
	leal	-1(%rcx), %r8d
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	%r8d, %eax
	andl	$1073741823, %eax
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L452:
	cmpq	%rdx, %rsi
	je	.L441
	addq	$1, %rax
	andq	%r8, %rax
.L451:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r9,%rdx,8), %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L452
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	movq	32(%rdi), %rax
	movq	40(%rdi), %rdx
	movslq	8(%rcx), %rsi
	movabsq	$-6148914691236517205, %rcx
	subq	%rax, %rdx
	sarq	$3, %rdx
	imulq	%rcx, %rdx
	cmpq	%rdx, %rsi
	jnb	.L453
	leaq	(%rsi,%rsi,2), %rdx
	movl	(%rax,%rdx,8), %eax
	ret
.L453:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC36(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE19865:
	.size	_ZN2v88internal14HeapObjectsMap9FindEntryEm, .-_ZN2v88internal14HeapObjectsMap9FindEntryEm
	.section	.text._ZN2v88internal14HeapObjectsMap23StopHeapObjectsTrackingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14HeapObjectsMap23StopHeapObjectsTrackingEv
	.type	_ZN2v88internal14HeapObjectsMap23StopHeapObjectsTrackingEv, @function
_ZN2v88internal14HeapObjectsMap23StopHeapObjectsTrackingEv:
.LFB19867:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	cmpq	64(%rdi), %rax
	je	.L454
	movq	%rax, 64(%rdi)
.L454:
	ret
	.cfi_endproc
.LFE19867:
	.size	_ZN2v88internal14HeapObjectsMap23StopHeapObjectsTrackingEv, .-_ZN2v88internal14HeapObjectsMap23StopHeapObjectsTrackingEv
	.section	.text._ZN2v88internal14V8HeapExplorerC2EPNS0_12HeapSnapshotEPNS0_38SnapshottingProgressReportingInterfaceEPNS_12HeapProfiler18ObjectNameResolverE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorerC2EPNS0_12HeapSnapshotEPNS0_38SnapshottingProgressReportingInterfaceEPNS_12HeapProfiler18ObjectNameResolverE
	.type	_ZN2v88internal14V8HeapExplorerC2EPNS0_12HeapSnapshotEPNS0_38SnapshottingProgressReportingInterfaceEPNS_12HeapProfiler18ObjectNameResolverE, @function
_ZN2v88internal14V8HeapExplorerC2EPNS0_12HeapSnapshotEPNS0_38SnapshottingProgressReportingInterfaceEPNS_12HeapProfiler18ObjectNameResolverE:
.LFB19963:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14V8HeapExplorerE(%rip), %rax
	movss	.LC30(%rip), %xmm0
	movq	%rax, (%rdi)
	movq	(%rsi), %rax
	movq	8(%rax), %r8
	movq	80(%r8), %r8
	movq	%rsi, 16(%rdi)
	movq	%r8, 8(%rdi)
	movq	40(%rax), %rsi
	movq	%rsi, 24(%rdi)
	movq	8(%rax), %rax
	movq	%rdx, 40(%rdi)
	movq	%rax, 32(%rdi)
	leaq	104(%rdi), %rax
	movq	%rax, 56(%rdi)
	leaq	160(%rdi), %rax
	movq	%rax, 112(%rdi)
	leaq	216(%rdi), %rax
	movq	$0, 48(%rdi)
	movq	$1, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 96(%rdi)
	movq	$0, 104(%rdi)
	movq	$1, 120(%rdi)
	movq	$0, 128(%rdi)
	movq	$0, 136(%rdi)
	movq	$0, 152(%rdi)
	movq	$0, 160(%rdi)
	movq	%rax, 168(%rdi)
	movq	$1, 176(%rdi)
	movq	$0, 184(%rdi)
	movq	$0, 192(%rdi)
	movss	%xmm0, 88(%rdi)
	movss	%xmm0, 144(%rdi)
	movss	%xmm0, 200(%rdi)
	movq	$0, 208(%rdi)
	movq	$0, 216(%rdi)
	movq	%rcx, 224(%rdi)
	movq	$0, 232(%rdi)
	movl	$0, 240(%rdi)
	movq	$0, 248(%rdi)
	movl	$0, 256(%rdi)
	movq	$0, 264(%rdi)
	ret
	.cfi_endproc
.LFE19963:
	.size	_ZN2v88internal14V8HeapExplorerC2EPNS0_12HeapSnapshotEPNS0_38SnapshottingProgressReportingInterfaceEPNS_12HeapProfiler18ObjectNameResolverE, .-_ZN2v88internal14V8HeapExplorerC2EPNS0_12HeapSnapshotEPNS0_38SnapshottingProgressReportingInterfaceEPNS_12HeapProfiler18ObjectNameResolverE
	.globl	_ZN2v88internal14V8HeapExplorerC1EPNS0_12HeapSnapshotEPNS0_38SnapshottingProgressReportingInterfaceEPNS_12HeapProfiler18ObjectNameResolverE
	.set	_ZN2v88internal14V8HeapExplorerC1EPNS0_12HeapSnapshotEPNS0_38SnapshottingProgressReportingInterfaceEPNS_12HeapProfiler18ObjectNameResolverE,_ZN2v88internal14V8HeapExplorerC2EPNS0_12HeapSnapshotEPNS0_38SnapshottingProgressReportingInterfaceEPNS_12HeapProfiler18ObjectNameResolverE
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.1,"aMS",@progbits,1
.LC37:
	.string	"system / Cell"
.LC38:
	.string	"system / Map (String)"
.LC39:
	.string	"system / Map (ConsString)"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.8,"aMS",@progbits,1
	.align 8
.LC40:
	.string	"system / Map (ConsOneByteString)"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.1
.LC41:
	.string	"system / Map (SlicedString)"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.8
	.align 8
.LC42:
	.string	"system / Map (SlicedOneByteString)"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.1
.LC43:
	.string	"system / Map (ExternalString)"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.8
	.align 8
.LC44:
	.string	"system / Map (ExternalOneByteString)"
	.align 8
.LC45:
	.string	"system / Map (UncachedExternalString)"
	.align 8
.LC46:
	.string	"system / Map (UncachedExternalOneByteString)"
	.align 8
.LC47:
	.string	"system / Map (InternalizedString)"
	.align 8
.LC48:
	.string	"system / Map (OneByteInternalizedString)"
	.align 8
.LC49:
	.string	"system / Map (ExternalInternalizedString)"
	.align 8
.LC50:
	.string	"system / Map (ExternalOneByteInternalizedString)"
	.align 8
.LC51:
	.string	"system / Map (UncachedExternalInternalizedString)"
	.align 8
.LC52:
	.string	"system / Map (UncachedExternalOneByteInternalizedString)"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.1
.LC53:
	.string	"system / Map (ThinString)"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.8
	.align 8
.LC54:
	.string	"system / Map (ThinOneByteString)"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.1
.LC55:
	.string	"system / Map"
.LC56:
	.string	"system"
.LC57:
	.string	"system / PropertyCell"
.LC58:
	.string	"system / Foreign"
.LC59:
	.string	"system / Oddball"
.LC60:
	.string	"system / AllocationSite"
.LC61:
	.string	"system / AccessCheckInfo"
.LC62:
	.string	"system / AccessorInfo"
.LC63:
	.string	"system / AccessorPair"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.8
	.align 8
.LC64:
	.string	"system / AliasedArgumentsEntry"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.1
.LC65:
	.string	"system / AllocationMemento"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.8
	.align 8
.LC66:
	.string	"system / ArrayBoilerplateDescription"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.1
.LC67:
	.string	"system / AsmWasmData"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.8
	.align 8
.LC68:
	.string	"system / AsyncGeneratorRequest"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.1
.LC69:
	.string	"system / ClassPositions"
.LC70:
	.string	"system / DebugInfo"
.LC71:
	.string	"system / EnumCache"
.LC72:
	.string	"system / FunctionTemplateInfo"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.8
	.align 8
.LC73:
	.string	"system / FunctionTemplateRareData"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.1
.LC74:
	.string	"system / InterceptorInfo"
.LC75:
	.string	"system / InterpreterData"
.LC76:
	.string	"system / ObjectTemplateInfo"
.LC77:
	.string	"system / PromiseCapability"
.LC78:
	.string	"system / PromiseReaction"
.LC79:
	.string	"system / PrototypeInfo"
.LC80:
	.string	"system / Script"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.8
	.align 8
.LC81:
	.string	"system / SourcePositionTableWithFrameCache"
	.align 8
.LC82:
	.string	"system / SourceTextModuleInfoEntry"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.1
.LC83:
	.string	"system / StackFrameInfo"
.LC84:
	.string	"system / StackTraceFrame"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.8
	.align 8
.LC85:
	.string	"system / TemplateObjectDescription"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.1
.LC86:
	.string	"system / Tuple2"
.LC87:
	.string	"system / Tuple3"
.LC88:
	.string	"system / WasmCapiFunctionData"
.LC89:
	.string	"system / WasmDebugInfo"
.LC90:
	.string	"system / WasmExceptionTag"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.8
	.align 8
.LC91:
	.string	"system / WasmExportedFunctionData"
	.align 8
.LC92:
	.string	"system / WasmIndirectFunctionTable"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.1
.LC93:
	.string	"system / WasmJSFunctionData"
.LC94:
	.string	"system / CallableTask"
.LC95:
	.string	"system / CallbackTask"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.8
	.align 8
.LC96:
	.string	"system / PromiseFulfillReactionJobTask"
	.align 8
.LC97:
	.string	"system / PromiseRejectReactionJobTask"
	.align 8
.LC98:
	.string	"system / PromiseResolveThenableJobTask"
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE.str1.1
.LC99:
	.string	"system / SortState"
.LC100:
	.string	"system / InternalClass"
.LC101:
	.string	"system / SmiPair"
.LC102:
	.string	"system / SmiBox"
.LC103:
	.string	"system / Map (OneByteString)"
	.section	.text._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE
	.type	_ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE, @function
_ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE:
.LFB19971:
	.cfi_startproc
	endbr64
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subl	$67, %eax
	cmpw	$92, %ax
	ja	.L458
	leaq	.L460(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE,"a",@progbits
	.align 4
	.align 4
.L460:
	.long	.L507-.L460
	.long	.L506-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L505-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L504-.L460
	.long	.L503-.L460
	.long	.L502-.L460
	.long	.L501-.L460
	.long	.L500-.L460
	.long	.L499-.L460
	.long	.L498-.L460
	.long	.L497-.L460
	.long	.L496-.L460
	.long	.L495-.L460
	.long	.L494-.L460
	.long	.L493-.L460
	.long	.L492-.L460
	.long	.L491-.L460
	.long	.L490-.L460
	.long	.L489-.L460
	.long	.L488-.L460
	.long	.L487-.L460
	.long	.L486-.L460
	.long	.L485-.L460
	.long	.L484-.L460
	.long	.L483-.L460
	.long	.L482-.L460
	.long	.L481-.L460
	.long	.L480-.L460
	.long	.L479-.L460
	.long	.L478-.L460
	.long	.L477-.L460
	.long	.L476-.L460
	.long	.L475-.L460
	.long	.L474-.L460
	.long	.L473-.L460
	.long	.L472-.L460
	.long	.L471-.L460
	.long	.L470-.L460
	.long	.L469-.L460
	.long	.L468-.L460
	.long	.L467-.L460
	.long	.L466-.L460
	.long	.L465-.L460
	.long	.L464-.L460
	.long	.L463-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L462-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L527-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L458-.L460
	.long	.L459-.L460
	.section	.text._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE
	.p2align 4,,10
	.p2align 3
.L458:
	leaq	.LC56(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L462:
	leaq	.LC60(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	leaq	.LC102(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	leaq	.LC101(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L465:
	leaq	.LC100(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	leaq	.LC99(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	leaq	.LC98(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L468:
	leaq	.LC97(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	leaq	.LC96(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	leaq	.LC95(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L471:
	leaq	.LC94(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L472:
	leaq	.LC93(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	leaq	.LC92(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L474:
	leaq	.LC91(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L475:
	leaq	.LC90(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L476:
	leaq	.LC89(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	leaq	.LC88(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	leaq	.LC87(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L479:
	leaq	.LC86(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L480:
	leaq	.LC85(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	leaq	.LC84(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L482:
	leaq	.LC83(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	leaq	.LC82(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	leaq	.LC81(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L485:
	leaq	.LC80(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L486:
	leaq	.LC79(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	leaq	.LC78(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	leaq	.LC77(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L489:
	leaq	.LC76(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	leaq	.LC75(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L491:
	leaq	.LC74(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	leaq	.LC73(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L493:
	leaq	.LC72(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	leaq	.LC71(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	leaq	.LC70(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L496:
	leaq	.LC69(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L497:
	leaq	.LC68(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	leaq	.LC67(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	leaq	.LC66(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L500:
	leaq	.LC65(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	leaq	.LC64(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L502:
	leaq	.LC63(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	leaq	.LC62(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L504:
	leaq	.LC61(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L505:
	leaq	.LC58(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L506:
	cmpw	$58, 11(%rsi)
	ja	.L508
	movzwl	11(%rsi), %eax
	leaq	.L510(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE
	.align 4
	.align 4
.L510:
	.long	.L526-.L510
	.long	.L508-.L510
	.long	.L525-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L524-.L510
	.long	.L508-.L510
	.long	.L523-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L522-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L521-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L528-.L510
	.long	.L520-.L510
	.long	.L519-.L510
	.long	.L518-.L510
	.long	.L508-.L510
	.long	.L517-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L516-.L510
	.long	.L515-.L510
	.long	.L514-.L510
	.long	.L513-.L510
	.long	.L508-.L510
	.long	.L512-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L511-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L508-.L510
	.long	.L509-.L510
	.section	.text._ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE
	.p2align 4,,10
	.p2align 3
.L507:
	leaq	.LC59(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	leaq	.LC37(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	leaq	.LC57(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L508:
	leaq	.LC55(%rip), %rax
	ret
.L509:
	leaq	.LC46(%rip), %rax
	ret
.L526:
	leaq	.LC47(%rip), %rax
	ret
.L511:
	leaq	.LC45(%rip), %rax
	ret
.L512:
	leaq	.LC54(%rip), %rax
	ret
.L513:
	leaq	.LC42(%rip), %rax
	ret
.L514:
	leaq	.LC44(%rip), %rax
	ret
.L522:
	leaq	.LC51(%rip), %rax
	ret
.L523:
	leaq	.LC50(%rip), %rax
	ret
.L524:
	leaq	.LC48(%rip), %rax
	ret
.L525:
	leaq	.LC49(%rip), %rax
	ret
.L515:
	leaq	.LC40(%rip), %rax
	ret
.L517:
	leaq	.LC53(%rip), %rax
	ret
.L518:
	leaq	.LC41(%rip), %rax
	ret
.L519:
	leaq	.LC43(%rip), %rax
	ret
.L520:
	leaq	.LC39(%rip), %rax
	ret
.L521:
	leaq	.LC52(%rip), %rax
	ret
.L528:
	leaq	.LC38(%rip), %rax
	ret
.L516:
	leaq	.LC103(%rip), %rax
	ret
	.cfi_endproc
.LFE19971:
	.size	_ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE, .-_ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE
	.section	.text._ZN2v88internal14V8HeapExplorer20EstimateObjectsCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer20EstimateObjectsCountEv
	.type	_ZN2v88internal14V8HeapExplorer20EstimateObjectsCountEv, @function
_ZN2v88internal14V8HeapExplorer20EstimateObjectsCountEv:
.LFB19972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	xorl	%r12d, %r12d
	subq	$80, %rsp
	movq	8(%rdi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal26CombinedHeapObjectIteratorC1EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE@PLT
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L534:
	addl	$1, %r12d
.L531:
	movq	%r13, %rdi
	call	_ZN2v88internal26CombinedHeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	jne	.L534
	movq	%r13, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L535
	addq	$80, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L535:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19972:
	.size	_ZN2v88internal14V8HeapExplorer20EstimateObjectsCountEv, .-_ZN2v88internal14V8HeapExplorer20EstimateObjectsCountEv
	.section	.text._ZN2v88internal14V8HeapExplorer14GetConstructorENS0_10JSReceiverE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer14GetConstructorENS0_10JSReceiverE
	.type	_ZN2v88internal14V8HeapExplorer14GetConstructorENS0_10JSReceiverE, @function
_ZN2v88internal14V8HeapExplorer14GetConstructorENS0_10JSReceiverE:
.LFB20017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rdi, %rsi
	andq	$-262144, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rax), %r12
	movq	3520(%r12), %rdi
	subq	$37592, %r12
	movq	41088(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r13
	testq	%rdi, %rdi
	je	.L537
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L538:
	call	_ZN2v88internal10JSReceiver14GetConstructorENS0_6HandleIS1_EE@PLT
	xorl	%r14d, %r14d
	testq	%rax, %rax
	je	.L540
	movq	(%rax), %r14
.L540:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L546
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L546:
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L537:
	.cfi_restore_state
	movq	%rbx, %rdi
	cmpq	%r13, %rbx
	je	.L548
.L539:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L548:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L539
	.cfi_endproc
.LFE20017:
	.size	_ZN2v88internal14V8HeapExplorer14GetConstructorENS0_10JSReceiverE, .-_ZN2v88internal14V8HeapExplorer14GetConstructorENS0_10JSReceiverE
	.section	.text._ZN2v88internal14V8HeapExplorer18GetConstructorNameENS0_8JSObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer18GetConstructorNameENS0_8JSObjectE
	.type	_ZN2v88internal14V8HeapExplorer18GetConstructorNameENS0_8JSObjectE, @function
_ZN2v88internal14V8HeapExplorer18GetConstructorNameENS0_8JSObjectE:
.LFB20018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	andq	$-262144, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rax), %r12
	movq	-1(%rdi), %rax
	subq	$37592, %r12
	cmpw	$1105, 11(%rax)
	je	.L560
	movq	%rdi, %rsi
	movq	41112(%r12), %rdi
	addl	$1, 41104(%r12)
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r13
	testq	%rdi, %rdi
	je	.L552
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L553:
	call	_ZN2v88internal10JSReceiver18GetConstructorNameENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L558
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L558:
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L552:
	.cfi_restore_state
	movq	%rbx, %rdi
	cmpq	%r13, %rbx
	je	.L561
.L554:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L560:
	movq	2248(%r12), %r14
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L561:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L554
	.cfi_endproc
.LFE20018:
	.size	_ZN2v88internal14V8HeapExplorer18GetConstructorNameENS0_8JSObjectE, .-_ZN2v88internal14V8HeapExplorer18GetConstructorNameENS0_8JSObjectE
	.section	.text._ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	.type	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE, @function
_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE:
.LFB20027:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	movq	8(%rdi), %rdx
	notq	%rax
	andl	$1, %eax
	je	.L563
.L567:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L563:
	movq	-1(%rsi), %rcx
	cmpw	$67, 11(%rcx)
	je	.L562
	cmpq	%rsi, -36616(%rdx)
	je	.L567
	cmpq	%rsi, -37304(%rdx)
	je	.L567
	cmpq	%rsi, -36520(%rdx)
	je	.L567
	cmpq	%rsi, -37296(%rdx)
	je	.L567
	cmpq	%rsi, -37440(%rdx)
	je	.L567
	cmpq	%rsi, -37360(%rdx)
	je	.L567
	cmpq	%rsi, -37352(%rdx)
	je	.L567
	cmpq	%rsi, -37384(%rdx)
	je	.L567
	cmpq	%rsi, -37536(%rdx)
	je	.L567
	cmpq	%rsi, -37528(%rdx)
	je	.L567
	cmpq	%rsi, -37520(%rdx)
	setne	%al
.L562:
	ret
	.cfi_endproc
.LFE20027:
	.size	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE, .-_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	.section	.text._ZN2v88internal14V8HeapExplorer26IsEssentialHiddenReferenceENS0_6ObjectEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer26IsEssentialHiddenReferenceENS0_6ObjectEi
	.type	_ZN2v88internal14V8HeapExplorer26IsEssentialHiddenReferenceENS0_6ObjectEi, @function
_ZN2v88internal14V8HeapExplorer26IsEssentialHiddenReferenceENS0_6ObjectEi:
.LFB20028:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	notq	%rax
	andl	$1, %eax
	je	.L583
	ret
	.p2align 4,,10
	.p2align 3
.L583:
	movq	-1(%rsi), %rcx
	cmpw	$121, 11(%rcx)
	je	.L584
.L572:
	movq	-1(%rsi), %rcx
	cmpw	$152, 11(%rcx)
	je	.L585
.L573:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subw	$138, %ax
	cmpw	$9, %ax
	ja	.L586
	cmpl	$1960, %edx
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L586:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L584:
	cmpl	$40, %edx
	jne	.L572
	ret
	.p2align 4,,10
	.p2align 3
.L585:
	cmpl	$8, %edx
	jne	.L573
	ret
	.cfi_endproc
.LFE20028:
	.size	_ZN2v88internal14V8HeapExplorer26IsEssentialHiddenReferenceENS0_6ObjectEi, .-_ZN2v88internal14V8HeapExplorer26IsEssentialHiddenReferenceENS0_6ObjectEi
	.section	.text._ZN2v88internal14V8HeapExplorer16MarkVisitedFieldEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer16MarkVisitedFieldEi
	.type	_ZN2v88internal14V8HeapExplorer16MarkVisitedFieldEi, @function
_ZN2v88internal14V8HeapExplorer16MarkVisitedFieldEi:
.LFB20030:
	.cfi_startproc
	endbr64
	movl	%esi, %ecx
	testl	%esi, %esi
	js	.L587
	sarl	$3, %ecx
	movq	232(%rdi), %rsi
	movl	$1, %edx
	movslq	%ecx, %rax
	salq	%cl, %rdx
	shrq	$6, %rax
	orq	%rdx, (%rsi,%rax,8)
.L587:
	ret
	.cfi_endproc
.LFE20030:
	.size	_ZN2v88internal14V8HeapExplorer16MarkVisitedFieldEi, .-_ZN2v88internal14V8HeapExplorer16MarkVisitedFieldEi
	.section	.rodata._ZN2v88internal14V8HeapExplorer22GetStrongGcSubrootNameENS0_6ObjectE.str1.8,"aMS",@progbits,1
	.align 8
.LC104:
	.string	"!strong_gc_subroot_names_.empty()"
	.section	.rodata._ZN2v88internal14V8HeapExplorer22GetStrongGcSubrootNameENS0_6ObjectE.str1.1,"aMS",@progbits,1
.LC105:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal14V8HeapExplorer22GetStrongGcSubrootNameENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer22GetStrongGcSubrootNameENS0_6ObjectE
	.type	_ZN2v88internal14V8HeapExplorer22GetStrongGcSubrootNameENS0_6ObjectE, @function
_ZN2v88internal14V8HeapExplorer22GetStrongGcSubrootNameENS0_6ObjectE:
.LFB20046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	cmpq	$0, 136(%rdi)
	movq	%rsi, -88(%rbp)
	jne	.L590
	movq	8(%rdi), %rax
	leaq	_ZN2v88internal10RootsTable11root_names_E(%rip), %r14
	leaq	-37536(%rax), %r15
	leaq	144(%rdi), %rax
	movq	%rax, -72(%rbp)
	leaq	128(%rdi), %rax
	movq	%rax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L608:
	movq	(%r14), %rdx
	movl	$32, %edi
	movq	(%r15), %r13
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	120(%rbx), %rsi
	movq	$0, (%rax)
	movq	%rax, %r12
	movq	%rdx, 16(%rax)
	xorl	%edx, %edx
	movq	%r13, 8(%rax)
	movq	%r13, %rax
	divq	%rsi
	movq	112(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	leaq	0(,%rdx,8), %r8
	testq	%rax, %rax
	je	.L591
	movq	(%rax), %rcx
	movq	24(%rcx), %rdi
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L592:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L591
	movq	24(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L591
.L594:
	cmpq	%rdi, %r13
	jne	.L592
	cmpq	8(%rcx), %r13
	jne	.L592
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L613:
	addq	$8, %r14
	leaq	4736+_ZN2v88internal10RootsTable11root_names_E(%rip), %rax
	addq	$8, %r15
	cmpq	%r14, %rax
	jne	.L608
	cmpq	$0, 136(%rbx)
	je	.L643
.L590:
	movq	120(%rbx), %rsi
	movq	-88(%rbp), %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	112(%rbx), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %rdi
	testq	%r8, %r8
	je	.L589
	movq	(%r8), %r8
	movq	-88(%rbp), %r9
	movq	24(%r8), %rcx
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L610:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L589
	movq	24(%r8), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %rdi
	jne	.L644
.L612:
	cmpq	%rcx, %r9
	jne	.L610
	cmpq	8(%r8), %r9
	jne	.L610
	movq	16(%r8), %r8
.L589:
	addq	$56, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L591:
	.cfi_restore_state
	movq	136(%rbx), %rdx
	movq	-72(%rbp), %rdi
	movl	$1, %ecx
	movq	%r8, -56(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r9
	testb	%al, %al
	jne	.L595
	movq	112(%rbx), %r10
	movq	-56(%rbp), %r8
.L596:
	leaq	(%r10,%r8), %rax
	movq	%r13, 24(%r12)
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L605
	movq	(%rdx), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rax
	movq	%r12, (%rax)
.L606:
	addq	$1, 136(%rbx)
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L595:
	cmpq	$1, %rdx
	je	.L645
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L646
	leaq	0(,%rdx,8), %rdx
	movq	%r9, -80(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-80(%rbp), %r9
	leaq	160(%rbx), %r8
	movq	%rax, %r10
.L598:
	movq	128(%rbx), %rsi
	movq	$0, 128(%rbx)
	testq	%rsi, %rsi
	je	.L600
	xorl	%r11d, %r11d
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L602:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%rdi), %rax
	movq	%rcx, (%rax)
.L603:
	testq	%rsi, %rsi
	je	.L600
.L601:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	24(%rcx), %rax
	divq	%r9
	leaq	(%r10,%rdx,8), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L602
	movq	128(%rbx), %rax
	movq	%rax, (%rcx)
	movq	-64(%rbp), %rax
	movq	%rcx, 128(%rbx)
	movq	%rax, (%rdi)
	cmpq	$0, (%rcx)
	je	.L614
	movq	%rcx, (%r10,%r11,8)
	movq	%rdx, %r11
	testq	%rsi, %rsi
	jne	.L601
	.p2align 4,,10
	.p2align 3
.L600:
	movq	112(%rbx), %rdi
	cmpq	%r8, %rdi
	je	.L604
	movq	%r9, -80(%rbp)
	movq	%r10, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %r9
	movq	-56(%rbp), %r10
.L604:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r9, 120(%rbx)
	divq	%r9
	movq	%r10, 112(%rbx)
	leaq	0(,%rdx,8), %r8
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L605:
	movq	128(%rbx), %rdx
	movq	%r12, 128(%rbx)
	movq	%rdx, (%r12)
	testq	%rdx, %rdx
	je	.L607
	movq	24(%rdx), %rax
	xorl	%edx, %edx
	divq	120(%rbx)
	movq	%r12, (%r10,%rdx,8)
	movq	112(%rbx), %rax
	addq	%r8, %rax
.L607:
	movq	-64(%rbp), %rsi
	movq	%rsi, (%rax)
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L614:
	movq	%rdx, %r11
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L645:
	leaq	160(%rbx), %r10
	movq	$0, 160(%rbx)
	movq	%r10, %r8
	jmp	.L598
.L644:
	xorl	%r8d, %r8d
	jmp	.L589
.L643:
	leaq	.LC104(%rip), %rsi
	leaq	.LC105(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L646:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE20046:
	.size	_ZN2v88internal14V8HeapExplorer22GetStrongGcSubrootNameENS0_6ObjectE, .-_ZN2v88internal14V8HeapExplorer22GetStrongGcSubrootNameENS0_6ObjectE
	.section	.rodata._ZN2v88internal14V8HeapExplorer16TagGlobalObjectsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC106:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal14V8HeapExplorer16TagGlobalObjectsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer16TagGlobalObjectsEv
	.type	_ZN2v88internal14V8HeapExplorer16TagGlobalObjectsEv, @function
_ZN2v88internal14V8HeapExplorer16TagGlobalObjectsEv:
.LFB20051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	subq	$37592, %rax
	movq	41088(%rax), %rcx
	movq	41096(%rax), %rsi
	movq	$0, -72(%rbp)
	addl	$1, 41104(%rax)
	movq	41152(%rax), %rdi
	movq	%rcx, -152(%rbp)
	leaq	16+_ZTVN2v88internal23GlobalObjectsEnumeratorE(%rip), %rcx
	movq	%rsi, -136(%rbp)
	leaq	-96(%rbp), %rsi
	movq	%rax, -144(%rbp)
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal13GlobalHandles15IterateAllRootsEPNS0_11RootVisitorE@PLT
	movq	-80(%rbp), %rdx
	subq	-88(%rbp), %rdx
	movabsq	$1152921504606846975, %rax
	sarq	$3, %rdx
	movslq	%edx, %rdx
	cmpq	%rax, %rdx
	ja	.L709
	testq	%rdx, %rdx
	je	.L649
	leaq	0(,%rdx,8), %r12
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	testl	%edx, %edx
	jle	.L655
	subl	$1, %edx
	xorl	%r13d, %r13d
	leaq	8(,%rdx,8), %r12
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L710:
	movq	(%rdi), %rdx
	movq	(%rax,%r13), %rsi
	call	*(%rdx)
	movq	%rax, %r8
	movq	-88(%rbp), %rax
	movq	%r8, (%r15,%r13)
	addq	$8, %r13
	cmpq	%r12, %r13
	je	.L652
.L708:
	movq	224(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L710
.L651:
	movq	$0, (%r15,%r13)
	addq	$8, %r13
	cmpq	%r13, %r12
	jne	.L651
.L652:
	movq	-80(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	testl	%eax, %eax
	jle	.L655
	subl	$1, %eax
	xorl	%r12d, %r12d
	leaq	8(,%rax,8), %rax
	movq	%rax, -104(%rbp)
	leaq	88(%rbx), %rax
	movq	%rax, -128(%rbp)
	leaq	72(%rbx), %rax
	movq	%rax, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L674:
	cmpq	$0, (%r15,%r12)
	je	.L656
	movq	-88(%rbp), %rax
	movl	$32, %edi
	movq	(%rax,%r12), %rax
	movq	(%rax), %r13
	call	_Znwm@PLT
	movq	64(%rbx), %r9
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %r14
	movq	(%r15,%r12), %rax
	movq	%r13, 8(%r14)
	movq	%rax, 16(%r14)
	movq	%r13, %rax
	divq	%r9
	movq	56(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rdi
	leaq	0(,%rdx,8), %r11
	testq	%rax, %rax
	je	.L657
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L658:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L657
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r9
	cmpq	%rdx, %rdi
	jne	.L657
.L660:
	cmpq	%rsi, %r13
	jne	.L658
	cmpq	8(%rcx), %r13
	jne	.L658
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L656:
	addq	$8, %r12
	cmpq	-104(%rbp), %r12
	jne	.L674
.L655:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L649:
	movq	-88(%rbp), %rdi
	leaq	16+_ZTVN2v88internal23GlobalObjectsEnumeratorE(%rip), %rax
	movq	%rax, -96(%rbp)
	testq	%rdi, %rdi
	je	.L675
	call	_ZdlPv@PLT
.L675:
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rbx
	subl	$1, 41104(%rax)
	movq	%rbx, 41088(%rax)
	movq	-136(%rbp), %rbx
	cmpq	41096(%rax), %rbx
	je	.L647
	movq	%rbx, 41096(%rax)
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L647:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L711
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L657:
	.cfi_restore_state
	movq	80(%rbx), %rdx
	movq	-128(%rbp), %rdi
	movq	%r9, %rsi
	movl	$1, %ecx
	movq	%r11, -112(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r9
	testb	%al, %al
	jne	.L661
	movq	56(%rbx), %r10
	movq	-112(%rbp), %r11
	movq	%r13, 24(%r14)
	leaq	(%r10,%r11), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L671
.L714:
	movq	(%rdx), %rdx
	movq	%rdx, (%r14)
	movq	(%rax), %rax
	movq	%r14, (%rax)
.L672:
	addq	$1, 80(%rbx)
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L661:
	cmpq	$1, %rdx
	je	.L712
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L713
	leaq	0(,%rdx,8), %rdx
	movq	%r9, -160(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -112(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-160(%rbp), %r9
	leaq	104(%rbx), %r8
	movq	%rax, %r10
.L664:
	movq	72(%rbx), %rsi
	movq	$0, 72(%rbx)
	testq	%rsi, %rsi
	je	.L666
	xorl	%r11d, %r11d
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L668:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%rdi), %rax
	movq	%rcx, (%rax)
.L669:
	testq	%rsi, %rsi
	je	.L666
.L667:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	24(%rcx), %rax
	divq	%r9
	leaq	(%r10,%rdx,8), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L668
	movq	72(%rbx), %rax
	movq	%rax, (%rcx)
	movq	-120(%rbp), %rax
	movq	%rcx, 72(%rbx)
	movq	%rax, (%rdi)
	cmpq	$0, (%rcx)
	je	.L680
	movq	%rcx, (%r10,%r11,8)
	movq	%rdx, %r11
	testq	%rsi, %rsi
	jne	.L667
	.p2align 4,,10
	.p2align 3
.L666:
	movq	56(%rbx), %rdi
	cmpq	%rdi, %r8
	je	.L670
	movq	%r9, -160(%rbp)
	movq	%r10, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-160(%rbp), %r9
	movq	-112(%rbp), %r10
.L670:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r9, 64(%rbx)
	divq	%r9
	movq	%r10, 56(%rbx)
	movq	%r13, 24(%r14)
	leaq	0(,%rdx,8), %r11
	leaq	(%r10,%r11), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L714
.L671:
	movq	72(%rbx), %rdx
	movq	%r14, 72(%rbx)
	movq	%rdx, (%r14)
	testq	%rdx, %rdx
	je	.L673
	movq	24(%rdx), %rax
	xorl	%edx, %edx
	divq	64(%rbx)
	movq	%r14, (%r10,%rdx,8)
	movq	56(%rbx), %rax
	addq	%r11, %rax
.L673:
	movq	-120(%rbp), %rsi
	movq	%rsi, (%rax)
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L680:
	movq	%rdx, %r11
	jmp	.L669
.L712:
	leaq	104(%rbx), %r10
	movq	$0, 104(%rbx)
	movq	%r10, %r8
	jmp	.L664
.L713:
	call	_ZSt17__throw_bad_allocv@PLT
.L711:
	call	__stack_chk_fail@PLT
.L709:
	leaq	.LC106(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE20051:
	.size	_ZN2v88internal14V8HeapExplorer16TagGlobalObjectsEv, .-_ZN2v88internal14V8HeapExplorer16TagGlobalObjectsEv
	.section	.text._ZN2v88internal21NativeObjectsExplorerC2EPNS0_12HeapSnapshotEPNS0_38SnapshottingProgressReportingInterfaceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21NativeObjectsExplorerC2EPNS0_12HeapSnapshotEPNS0_38SnapshottingProgressReportingInterfaceE
	.type	_ZN2v88internal21NativeObjectsExplorerC2EPNS0_12HeapSnapshotEPNS0_38SnapshottingProgressReportingInterfaceE, @function
_ZN2v88internal21NativeObjectsExplorerC2EPNS0_12HeapSnapshotEPNS0_38SnapshottingProgressReportingInterfaceE:
.LFB20094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	8(%rdx), %rax
	movq	%rdi, %rbx
	movq	%rsi, 8(%rdi)
	movq	80(%rax), %rax
	subq	$37592, %rax
	movq	%rax, (%rdi)
	movq	40(%rdx), %rax
	movq	%rax, 16(%rdi)
	movl	$32, %edi
	call	_Znwm@PLT
	movq	(%r12), %rdx
	leaq	16+_ZTVN2v88internal29EmbedderGraphEntriesAllocatorE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	40(%rdx), %rcx
	movq	8(%rdx), %rdx
	movq	%r12, 8(%rax)
	movq	%rax, 24(%rbx)
	movq	%rcx, 16(%rax)
	movq	%rdx, 24(%rax)
	movq	$0, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20094:
	.size	_ZN2v88internal21NativeObjectsExplorerC2EPNS0_12HeapSnapshotEPNS0_38SnapshottingProgressReportingInterfaceE, .-_ZN2v88internal21NativeObjectsExplorerC2EPNS0_12HeapSnapshotEPNS0_38SnapshottingProgressReportingInterfaceE
	.globl	_ZN2v88internal21NativeObjectsExplorerC1EPNS0_12HeapSnapshotEPNS0_38SnapshottingProgressReportingInterfaceE
	.set	_ZN2v88internal21NativeObjectsExplorerC1EPNS0_12HeapSnapshotEPNS0_38SnapshottingProgressReportingInterfaceE,_ZN2v88internal21NativeObjectsExplorerC2EPNS0_12HeapSnapshotEPNS0_38SnapshottingProgressReportingInterfaceE
	.section	.text._ZN2v88internal21HeapSnapshotGeneratorC2EPNS0_12HeapSnapshotEPNS_15ActivityControlEPNS_12HeapProfiler18ObjectNameResolverEPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21HeapSnapshotGeneratorC2EPNS0_12HeapSnapshotEPNS_15ActivityControlEPNS_12HeapProfiler18ObjectNameResolverEPNS0_4HeapE
	.type	_ZN2v88internal21HeapSnapshotGeneratorC2EPNS0_12HeapSnapshotEPNS_15ActivityControlEPNS_12HeapProfiler18ObjectNameResolverEPNS0_4HeapE, @function
_ZN2v88internal21HeapSnapshotGeneratorC2EPNS0_12HeapSnapshotEPNS_15ActivityControlEPNS_12HeapProfiler18ObjectNameResolverEPNS0_4HeapE:
.LFB20158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal21HeapSnapshotGeneratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	leaq	16+_ZTVN2v88internal14V8HeapExplorerE(%rip), %rax
	movss	.LC30(%rip), %xmm0
	movq	%rsi, 8(%rdi)
	movq	%rdx, 16(%rdi)
	movq	%rax, 24(%rdi)
	movq	(%rsi), %rax
	movq	8(%rax), %rdx
	movq	80(%rdx), %rdx
	movq	%rsi, 40(%rdi)
	movq	%rdx, 32(%rdi)
	movq	40(%rax), %rdx
	movq	%rdx, 48(%rdi)
	movq	8(%rax), %rax
	movss	%xmm0, 112(%rdi)
	movq	%rax, 56(%rdi)
	leaq	128(%rdi), %rax
	movq	%rax, 80(%rdi)
	leaq	184(%rdi), %rax
	movq	%rax, 136(%rdi)
	leaq	240(%rdi), %rax
	movq	%rdi, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	$1, 88(%rdi)
	movq	$0, 96(%rdi)
	movq	$0, 104(%rdi)
	movq	$0, 120(%rdi)
	movq	$0, 128(%rdi)
	movq	$1, 144(%rdi)
	movq	$0, 152(%rdi)
	movq	$0, 160(%rdi)
	movss	%xmm0, 168(%rdi)
	movq	%rcx, 248(%rdi)
	movss	%xmm0, 224(%rdi)
	movq	$0, 176(%rdi)
	movq	$0, 184(%rdi)
	movq	%rax, 192(%rdi)
	movq	$1, 200(%rdi)
	movq	$0, 208(%rdi)
	movq	$0, 216(%rdi)
	movq	$0, 232(%rdi)
	movq	$0, 240(%rdi)
	movq	$0, 256(%rdi)
	movl	$0, 264(%rdi)
	movq	$0, 272(%rdi)
	movl	$0, 280(%rdi)
	movq	$0, 288(%rdi)
	movq	(%rsi), %rdx
	movq	%rsi, 304(%rdi)
	movq	8(%rdx), %rax
	movq	80(%rax), %rax
	subq	$37592, %rax
	movq	%rax, 296(%rdi)
	movq	40(%rdx), %rax
	movq	%rax, 312(%rdi)
	movl	$32, %edi
	call	_Znwm@PLT
	movq	(%r12), %rdx
	movss	.LC30(%rip), %xmm0
	leaq	16+_ZTVN2v88internal29EmbedderGraphEntriesAllocatorE(%rip), %rsi
	movq	%rsi, (%rax)
	movq	40(%rdx), %rcx
	movq	8(%rdx), %rdx
	movq	%r12, 8(%rax)
	movq	%rax, 320(%rbx)
	movq	%rcx, 16(%rax)
	movq	%rdx, 24(%rax)
	leaq	384(%rbx), %rax
	movq	%r13, 400(%rbx)
	movq	$0, 328(%rbx)
	movq	%rax, 336(%rbx)
	movq	$1, 344(%rbx)
	movq	$0, 352(%rbx)
	movq	$0, 360(%rbx)
	movq	$0, 376(%rbx)
	movq	$0, 384(%rbx)
	movss	%xmm0, 368(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20158:
	.size	_ZN2v88internal21HeapSnapshotGeneratorC2EPNS0_12HeapSnapshotEPNS_15ActivityControlEPNS_12HeapProfiler18ObjectNameResolverEPNS0_4HeapE, .-_ZN2v88internal21HeapSnapshotGeneratorC2EPNS0_12HeapSnapshotEPNS_15ActivityControlEPNS_12HeapProfiler18ObjectNameResolverEPNS0_4HeapE
	.globl	_ZN2v88internal21HeapSnapshotGeneratorC1EPNS0_12HeapSnapshotEPNS_15ActivityControlEPNS_12HeapProfiler18ObjectNameResolverEPNS0_4HeapE
	.set	_ZN2v88internal21HeapSnapshotGeneratorC1EPNS0_12HeapSnapshotEPNS_15ActivityControlEPNS_12HeapProfiler18ObjectNameResolverEPNS0_4HeapE,_ZN2v88internal21HeapSnapshotGeneratorC2EPNS0_12HeapSnapshotEPNS_15ActivityControlEPNS_12HeapProfiler18ObjectNameResolverEPNS0_4HeapE
	.section	.text._ZN2v88internal21HeapSnapshotGenerator19InitProgressCounterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21HeapSnapshotGenerator19InitProgressCounterEv
	.type	_ZN2v88internal21HeapSnapshotGenerator19InitProgressCounterEv, @function
_ZN2v88internal21HeapSnapshotGenerator19InitProgressCounterEv:
.LFB20169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 16(%rdi)
	je	.L719
	movq	32(%rdi), %rsi
	leaq	-112(%rbp), %r13
	movq	%rdi, %rbx
	movl	$1, %edx
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v88internal26CombinedHeapObjectIteratorC1EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE@PLT
	.p2align 4,,10
	.p2align 3
.L721:
	movq	%r13, %rdi
	addl	$1, %r12d
	call	_ZN2v88internal26CombinedHeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	jne	.L721
	movq	%r13, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movl	%r12d, 396(%rbx)
	movl	$0, 392(%rbx)
.L719:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L725
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L725:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20169:
	.size	_ZN2v88internal21HeapSnapshotGenerator19InitProgressCounterEv, .-_ZN2v88internal21HeapSnapshotGenerator19InitProgressCounterEv
	.section	.text._ZN2v88internal18OutputStreamWriter12AddCharacterEc,"axG",@progbits,_ZN2v88internal18OutputStreamWriter12AddCharacterEc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18OutputStreamWriter12AddCharacterEc
	.type	_ZN2v88internal18OutputStreamWriter12AddCharacterEc, @function
_ZN2v88internal18OutputStreamWriter12AddCharacterEc:
.LFB20175:
	.cfi_startproc
	endbr64
	movslq	32(%rdi), %rax
	leal	1(%rax), %edx
	movl	%edx, 32(%rdi)
	movq	16(%rdi), %rdx
	movb	%sil, (%rdx,%rax)
	movl	32(%rdi), %edx
	cmpl	8(%rdi), %edx
	je	.L733
	ret
	.p2align 4,,10
	.p2align 3
.L733:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 36(%rdi)
	jne	.L726
	movq	(%rdi), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	je	.L734
.L728:
	movl	$0, 32(%rbx)
.L726:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L734:
	.cfi_restore_state
	movb	$1, 36(%rbx)
	jmp	.L728
	.cfi_endproc
.LFE20175:
	.size	_ZN2v88internal18OutputStreamWriter12AddCharacterEc, .-_ZN2v88internal18OutputStreamWriter12AddCharacterEc
	.section	.text._ZN2v88internal18OutputStreamWriter12AddSubstringEPKci,"axG",@progbits,_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	.type	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci, @function
_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci:
.LFB20177:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jle	.L741
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	(%rsi,%rdx), %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%r12, %rsi
	jnb	.L735
	movl	32(%rdi), %edx
	movq	%rdi, %r13
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L738:
	cmpq	%r14, %r12
	jbe	.L735
.L737:
	movl	8(%r13), %ebx
	movq	%r12, %rax
	movslq	%edx, %rdi
	movq	%r14, %rsi
	subq	%r14, %rax
	subl	%edx, %ebx
	cmpl	%eax, %ebx
	cmovg	%eax, %ebx
	addq	16(%r13), %rdi
	movslq	%ebx, %r15
	movq	%r15, %rdx
	addq	%r15, %r14
	call	memcpy@PLT
	movl	32(%r13), %edx
	addl	%ebx, %edx
	movl	%edx, 32(%r13)
	cmpl	8(%r13), %edx
	jne	.L738
	cmpb	$0, 36(%r13)
	jne	.L738
	movq	0(%r13), %rdi
	movq	16(%r13), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L739
	movb	$1, 36(%r13)
.L739:
	movl	$0, 32(%r13)
	xorl	%edx, %edx
	cmpq	%r14, %r12
	ja	.L737
.L735:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L741:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE20177:
	.size	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci, .-_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	.section	.rodata._ZN2v88internal26HeapSnapshotJSONSerializer17SerializeSnapshotEv.str1.1,"aMS",@progbits,1
.LC107:
	.string	"\"meta\":"
	.section	.rodata._ZN2v88internal26HeapSnapshotJSONSerializer17SerializeSnapshotEv.str1.8,"aMS",@progbits,1
	.align 8
.LC108:
	.ascii	"{\"node_fields\":[\"type\",\"name\",\"id\",\"self_size\",\"e"
	.ascii	"dge_count\",\"trace_node_id\"],\"node_types\":[[\"hidden\",\""
	.ascii	"array\",\"string\",\"object\",\"code\",\"closure\",\"regexp\""
	.ascii	",\"number\",\"native\",\"synthetic\",\"concatenated string\""
	.ascii	",\"sliced string\",\"symbol\",\"bigint\"],\"string\",\"numbe"
	.ascii	"r\",\"number\",\"number\",\"number\",\"number\"],\"edge_fiel"
	.ascii	"ds\":[\"type\",\"name_or_index\",\"to_node\"],\"edge_types\""
	.ascii	":[[\"context\",\"element\",\"property\",\"internal\",\"hidde"
	.ascii	"n\",\"shortcut\",\"weak\"],\"string_or_number\",\"node\"],\""
	.ascii	"trace_function_info_fields\""
	.string	":[\"function_id\",\"name\",\"script_name\",\"script_id\",\"line\",\"column\"],\"trace_node_fields\":[\"id\",\"function_info_index\",\"count\",\"size\",\"children\"],\"sample_fields\":[\"timestamp_us\",\"last_assigned_id\"],\"location_fields\":[\"object_index\",\"script_id\",\"line\",\"column\"]}"
	.section	.rodata._ZN2v88internal26HeapSnapshotJSONSerializer17SerializeSnapshotEv.str1.1
.LC109:
	.string	",\"node_count\":"
.LC110:
	.string	",\"edge_count\":"
.LC111:
	.string	",\"trace_function_count\":"
.LC112:
	.string	"%u"
	.section	.text._ZN2v88internal26HeapSnapshotJSONSerializer17SerializeSnapshotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26HeapSnapshotJSONSerializer17SerializeSnapshotEv
	.type	_ZN2v88internal26HeapSnapshotJSONSerializer17SerializeSnapshotEv, @function
_ZN2v88internal26HeapSnapshotJSONSerializer17SerializeSnapshotEv:
.LFB20195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC107(%rip), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	7(%r15), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	40(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	32(%r13), %edx
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L747:
	cmpq	%r12, %r15
	jnb	.L746
.L745:
	movl	8(%r13), %r14d
	movq	%r12, %rax
	movslq	%edx, %rdi
	movq	%r15, %rsi
	subq	%r15, %rax
	subl	%edx, %r14d
	cmpl	%eax, %r14d
	cmovg	%eax, %r14d
	addq	16(%r13), %rdi
	movslq	%r14d, %rcx
	movq	%rcx, %rdx
	movq	%rcx, -104(%rbp)
	call	memcpy@PLT
	movl	32(%r13), %edx
	movq	-104(%rbp), %rcx
	addl	%r14d, %edx
	addq	%rcx, %r15
	movl	%edx, 32(%r13)
	cmpl	8(%r13), %edx
	jne	.L747
	cmpb	$0, 36(%r13)
	jne	.L747
	movq	0(%r13), %rdi
	movq	16(%r13), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L748
	movb	$1, 36(%r13)
.L748:
	movl	$0, 32(%r13)
	xorl	%edx, %edx
	cmpq	%r12, %r15
	jb	.L745
.L746:
	movq	40(%rbx), %r13
	leaq	.LC108(%rip), %r15
	leaq	740(%r15), %r12
	movl	32(%r13), %edx
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L751:
	cmpq	%r12, %r15
	jnb	.L750
.L749:
	movl	8(%r13), %r14d
	movq	%r12, %rax
	movslq	%edx, %rdi
	movq	%r15, %rsi
	subq	%r15, %rax
	subl	%edx, %r14d
	cmpl	%eax, %r14d
	cmovg	%eax, %r14d
	addq	16(%r13), %rdi
	movslq	%r14d, %rcx
	movq	%rcx, %rdx
	movq	%rcx, -104(%rbp)
	call	memcpy@PLT
	movl	32(%r13), %edx
	movq	-104(%rbp), %rcx
	addl	%r14d, %edx
	addq	%rcx, %r15
	movl	%edx, 32(%r13)
	cmpl	8(%r13), %edx
	jne	.L751
	cmpb	$0, 36(%r13)
	jne	.L751
	movq	0(%r13), %rdi
	movq	16(%r13), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L752
	movb	$1, 36(%r13)
.L752:
	movl	$0, 32(%r13)
	xorl	%edx, %edx
	cmpq	%r12, %r15
	jb	.L749
.L750:
	movq	40(%rbx), %r13
	leaq	.LC109(%rip), %r15
	leaq	14(%r15), %r12
	movl	32(%r13), %edx
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L755:
	cmpq	%r12, %r15
	jnb	.L754
.L753:
	movl	8(%r13), %r14d
	movq	%r12, %rax
	movslq	%edx, %rdi
	movq	%r15, %rsi
	subq	%r15, %rax
	subl	%edx, %r14d
	cmpl	%eax, %r14d
	cmovg	%eax, %r14d
	addq	16(%r13), %rdi
	movslq	%r14d, %rcx
	movq	%rcx, %rdx
	movq	%rcx, -104(%rbp)
	call	memcpy@PLT
	movl	32(%r13), %edx
	movq	-104(%rbp), %rcx
	addl	%r14d, %edx
	addq	%rcx, %r15
	movl	%edx, 32(%r13)
	cmpl	8(%r13), %edx
	jne	.L755
	cmpb	$0, 36(%r13)
	jne	.L755
	movq	0(%r13), %rdi
	movq	16(%r13), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L756
	movb	$1, 36(%r13)
.L756:
	movl	$0, 32(%r13)
	xorl	%edx, %edx
	cmpq	%r12, %r15
	jb	.L753
.L754:
	movq	(%rbx), %rdx
	movq	40(%rbx), %r12
	movabsq	$-3689348814741910323, %rsi
	movq	288(%rdx), %rax
	subq	256(%rdx), %rax
	sarq	$3, %rax
	leaq	-3(%rax,%rax,2), %rcx
	movq	264(%rdx), %rax
	subq	272(%rdx), %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	leaq	(%rax,%rcx,4), %rax
	movq	248(%rdx), %rcx
	subq	232(%rdx), %rcx
	sarq	$3, %rcx
	imulq	%rsi, %rcx
	movslq	8(%r12), %rsi
	movl	%esi, %edx
	addq	%rax, %rcx
	movslq	32(%r12), %rax
	subl	%eax, %edx
	cmpl	$10, %edx
	jle	.L757
	movq	16(%r12), %rdi
	subq	%rax, %rsi
	leaq	.LC112(%rip), %rdx
	addq	%rax, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	addl	32(%r12), %eax
	movl	%eax, 32(%r12)
	movl	%eax, %edx
	cmpl	8(%r12), %eax
	je	.L781
.L758:
	movq	40(%rbx), %r13
	leaq	.LC110(%rip), %r15
	leaq	14(%r15), %r12
	movl	32(%r13), %edx
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L762:
	cmpq	%r12, %r15
	jnb	.L761
.L760:
	movl	8(%r13), %r14d
	movq	%r12, %rax
	movslq	%edx, %rdi
	movq	%r15, %rsi
	subq	%r15, %rax
	subl	%edx, %r14d
	cmpl	%eax, %r14d
	cmovg	%eax, %r14d
	addq	16(%r13), %rdi
	movslq	%r14d, %rcx
	movq	%rcx, %rdx
	movq	%rcx, -104(%rbp)
	call	memcpy@PLT
	movl	32(%r13), %edx
	movq	-104(%rbp), %rcx
	addl	%r14d, %edx
	addq	%rcx, %r15
	movl	%edx, 32(%r13)
	cmpl	8(%r13), %edx
	jne	.L762
	cmpb	$0, 36(%r13)
	jne	.L762
	movq	0(%r13), %rdi
	movq	16(%r13), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L763
	movb	$1, 36(%r13)
.L763:
	movl	$0, 32(%r13)
	xorl	%edx, %edx
	cmpq	%r12, %r15
	jb	.L760
.L761:
	movq	(%rbx), %rsi
	movq	40(%rbx), %r12
	movabsq	$-6148914691236517205, %rdi
	movq	368(%rsi), %rax
	movq	344(%rsi), %rcx
	subq	336(%rsi), %rax
	subq	352(%rsi), %rcx
	sarq	$3, %rax
	sarq	$3, %rcx
	subq	$1, %rax
	imulq	%rdi, %rcx
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rdx
	leaq	(%rdx,%rcx), %rax
	movq	328(%rsi), %rdx
	subq	312(%rsi), %rdx
	sarq	$3, %rdx
	imulq	%rdi, %rdx
	addq	%rdx, %rax
	js	.L764
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L765:
	movslq	8(%r12), %rsi
	movslq	32(%r12), %rax
	cvttsd2siq	%xmm0, %rcx
	movl	%esi, %edx
	subl	%eax, %edx
	cmpl	$10, %edx
	jle	.L766
	movq	16(%r12), %rdi
	subq	%rax, %rsi
	leaq	.LC112(%rip), %rdx
	addq	%rax, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	addl	32(%r12), %eax
	movl	%eax, 32(%r12)
	movl	%eax, %edx
	cmpl	8(%r12), %eax
	je	.L782
.L767:
	movq	40(%rbx), %r13
	leaq	.LC111(%rip), %r15
	leaq	24(%r15), %r12
	movl	32(%r13), %edx
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L771:
	cmpq	%r12, %r15
	jnb	.L770
.L769:
	movl	8(%r13), %r14d
	movq	%r12, %rax
	movslq	%edx, %rdi
	movq	%r15, %rsi
	subq	%r15, %rax
	subl	%edx, %r14d
	cmpl	%eax, %r14d
	cmovg	%eax, %r14d
	addq	16(%r13), %rdi
	movslq	%r14d, %rcx
	movq	%rcx, %rdx
	movq	%rcx, -104(%rbp)
	call	memcpy@PLT
	movl	32(%r13), %edx
	movq	-104(%rbp), %rcx
	addl	%r14d, %edx
	addq	%rcx, %r15
	movl	%edx, 32(%r13)
	cmpl	8(%r13), %edx
	jne	.L771
	cmpb	$0, 36(%r13)
	jne	.L771
	movq	0(%r13), %rdi
	movq	16(%r13), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L772
	movb	$1, 36(%r13)
.L772:
	movl	$0, 32(%r13)
	xorl	%edx, %edx
	cmpq	%r12, %r15
	jb	.L769
.L770:
	movq	(%rbx), %rax
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L773
	movq	336(%rax), %rcx
	subq	328(%rax), %rcx
	sarq	$3, %rcx
.L773:
	movq	40(%rbx), %r12
	movslq	8(%r12), %rsi
	movslq	32(%r12), %rax
	movl	%esi, %edx
	subl	%eax, %edx
	cmpl	$10, %edx
	jle	.L774
	movq	16(%r12), %rdi
	subq	%rax, %rsi
	leaq	.LC112(%rip), %rdx
	addq	%rax, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	addl	32(%r12), %eax
	movl	%eax, 32(%r12)
	movl	%eax, %edx
	cmpl	8(%r12), %eax
	je	.L783
.L744:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L784
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L774:
	.cfi_restore_state
	leaq	-80(%rbp), %rdi
	leaq	.LC112(%rip), %rdx
	movl	$11, %esi
	xorl	%eax, %eax
	movq	%rdi, -96(%rbp)
	movq	$11, -88(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L766:
	leaq	-80(%rbp), %rdi
	leaq	.LC112(%rip), %rdx
	movl	$11, %esi
	xorl	%eax, %eax
	movq	%rdi, -96(%rbp)
	movq	$11, -88(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L757:
	leaq	-80(%rbp), %rdi
	leaq	.LC112(%rip), %rdx
	movl	$11, %esi
	xorl	%eax, %eax
	movq	%rdi, -96(%rbp)
	movq	$11, -88(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L764:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L783:
	cmpb	$0, 36(%r12)
	jne	.L744
	movq	(%r12), %rdi
	movq	16(%r12), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L776
	movb	$1, 36(%r12)
.L776:
	movl	$0, 32(%r12)
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L781:
	cmpb	$0, 36(%r12)
	jne	.L758
	movq	(%r12), %rdi
	movq	16(%r12), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L759
	movb	$1, 36(%r12)
.L759:
	movl	$0, 32(%r12)
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L782:
	cmpb	$0, 36(%r12)
	jne	.L767
	movq	(%r12), %rdi
	movq	16(%r12), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L768
	movb	$1, 36(%r12)
.L768:
	movl	$0, 32(%r12)
	jmp	.L767
.L784:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20195:
	.size	_ZN2v88internal26HeapSnapshotJSONSerializer17SerializeSnapshotEv, .-_ZN2v88internal26HeapSnapshotJSONSerializer17SerializeSnapshotEv
	.section	.text._ZN2v88internal26HeapSnapshotJSONSerializer18SerializeTraceNodeEPNS0_19AllocationTraceNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeTraceNodeEPNS0_19AllocationTraceNodeE
	.type	_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeTraceNodeEPNS0_19AllocationTraceNodeE, @function
_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeTraceNodeEPNS0_19AllocationTraceNodeE:
.LFB20198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3435973837, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movl	20(%rsi), %r9d
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$46, -120(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, -128(%rbp)
	movl	%r9d, %eax
	.p2align 4,,10
	.p2align 3
.L786:
	movl	%eax, %eax
	movl	%esi, %edi
	addl	$1, %esi
	movq	%rax, %rcx
	imulq	%rdx, %rax
	shrq	$35, %rax
	cmpl	$9, %ecx
	ja	.L786
	movl	%r9d, %eax
	movl	%r9d, %ecx
	movslq	%edi, %r8
	imulq	%rax, %rdx
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %eax
	addl	%eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %eax
	addl	$48, %eax
	movb	%al, -112(%rbp,%r8)
	cmpl	$9, %r9d
	jbe	.L787
	subq	$1, %r8
	movl	$3435973837, %r9d
	.p2align 4,,10
	.p2align 3
.L788:
	movl	%edx, %eax
	movl	%edx, %r11d
	movq	-128(%rbp), %r10
	imulq	%r9, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %r11d
	movl	%r11d, %ecx
	addl	$48, %ecx
	movb	%cl, (%r10,%r8)
	movl	%edx, %ecx
	subq	$1, %r8
	movl	%eax, %edx
	cmpl	$9, %ecx
	ja	.L788
.L787:
	movq	-128(%rbp), %rax
	movslq	%esi, %rsi
	addl	$2, %edi
	movl	$3435973837, %r8d
	movb	$44, (%rax,%rsi)
	movl	8(%r12), %edx
	xorl	%esi, %esi
	movl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L789:
	movl	%eax, %eax
	addl	$1, %esi
	movq	%rax, %rcx
	imulq	%r8, %rax
	shrq	$35, %rax
	cmpl	$9, %ecx
	ja	.L789
	addl	%esi, %edi
	movl	$3435973837, %r8d
	movslq	%edi, %r9
	leaq	-1(%r9), %rsi
	.p2align 4,,10
	.p2align 3
.L790:
	movl	%edx, %eax
	movl	%edx, %r11d
	movq	-128(%rbp), %r10
	imulq	%r8, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %r11d
	movl	%r11d, %ecx
	addl	$48, %ecx
	movb	%cl, (%r10,%rsi)
	movl	%edx, %ecx
	subq	$1, %rsi
	movl	%eax, %edx
	cmpl	$9, %ecx
	ja	.L790
	movq	-128(%rbp), %rax
	addl	$1, %edi
	xorl	%esi, %esi
	movl	$3435973837, %r8d
	movb	$44, (%rax,%r9)
	movl	16(%r12), %edx
	movl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L791:
	movl	%eax, %eax
	addl	$1, %esi
	movq	%rax, %rcx
	imulq	%r8, %rax
	shrq	$35, %rax
	cmpl	$9, %ecx
	ja	.L791
	addl	%esi, %edi
	movl	$3435973837, %r8d
	movslq	%edi, %r9
	leaq	-1(%r9), %rsi
	.p2align 4,,10
	.p2align 3
.L792:
	movl	%edx, %eax
	movl	%edx, %r14d
	movq	-128(%rbp), %r10
	imulq	%r8, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %r14d
	movl	%r14d, %ecx
	addl	$48, %ecx
	movb	%cl, (%r10,%rsi)
	movl	%edx, %ecx
	subq	$1, %rsi
	movl	%eax, %edx
	cmpl	$9, %ecx
	ja	.L792
	movq	-128(%rbp), %rax
	addl	$1, %edi
	xorl	%esi, %esi
	movl	$3435973837, %r8d
	movb	$44, (%rax,%r9)
	movl	12(%r12), %edx
	movl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L793:
	movl	%eax, %eax
	addl	$1, %esi
	movq	%rax, %rcx
	imulq	%r8, %rax
	shrq	$35, %rax
	cmpl	$9, %ecx
	ja	.L793
	addl	%esi, %edi
	movl	$3435973837, %r8d
	movslq	%edi, %r9
	leaq	-1(%r9), %rsi
	.p2align 4,,10
	.p2align 3
.L794:
	movl	%edx, %eax
	movl	%edx, %r15d
	movq	-128(%rbp), %r10
	imulq	%r8, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %r15d
	movl	%r15d, %ecx
	addl	$48, %ecx
	movb	%cl, (%r10,%rsi)
	movl	%edx, %ecx
	subq	$1, %rsi
	movl	%eax, %edx
	cmpl	$9, %ecx
	ja	.L794
	movq	-128(%rbp), %rax
	leal	2(%rdi), %esi
	movslq	%esi, %rsi
	movb	$44, (%rax,%r9)
	leal	1(%rdi), %eax
	movq	-128(%rbp), %rdx
	cltq
	movb	$91, (%rdx,%rax)
	movq	-128(%rbp), %rax
	movb	$0, (%rax,%rsi)
	movq	-128(%rbp), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	40(%rbx), %rdi
	movq	%r13, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	movq	24(%r12), %r13
	movq	32(%r12), %r14
	cmpq	%r14, %r13
	je	.L801
	movq	0(%r13), %r15
.L803:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	addq	$8, %r13
	call	_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeTraceNodeEPNS0_19AllocationTraceNodeE
	cmpq	%r13, %r14
	je	.L801
	movq	40(%rbx), %r12
	movq	0(%r13), %r15
	movslq	32(%r12), %rax
	leal	1(%rax), %edx
	movl	%edx, 32(%r12)
	movq	16(%r12), %rdx
	movb	$44, (%rdx,%rax)
	movl	32(%r12), %edx
	cmpl	8(%r12), %edx
	jne	.L803
	cmpb	$0, 36(%r12)
	jne	.L803
	movq	(%r12), %rdi
	movq	16(%r12), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L799
	movb	$1, 36(%r12)
.L799:
	movl	$0, 32(%r12)
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L801:
	movq	40(%rbx), %rbx
	movslq	32(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, 32(%rbx)
	movq	16(%rbx), %rdx
	movb	$93, (%rdx,%rax)
	movl	32(%rbx), %edx
	cmpl	8(%rbx), %edx
	je	.L814
.L785:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L815
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L814:
	.cfi_restore_state
	cmpb	$0, 36(%rbx)
	jne	.L785
	movq	(%rbx), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	je	.L816
.L802:
	movl	$0, 32(%rbx)
	jmp	.L785
.L816:
	movb	$1, 36(%rbx)
	jmp	.L802
.L815:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20198:
	.size	_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeTraceNodeEPNS0_19AllocationTraceNodeE, .-_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeTraceNodeEPNS0_19AllocationTraceNodeE
	.section	.text._ZN2v88internal26HeapSnapshotJSONSerializer18SerializeTraceTreeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeTraceTreeEv
	.type	_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeTraceTreeEv, @function
_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeTraceTreeEv:
.LFB20197:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	48(%rax), %rsi
	testq	%rsi, %rsi
	je	.L817
	addq	$24, %rsi
	jmp	_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeTraceNodeEPNS0_19AllocationTraceNodeE
	.p2align 4,,10
	.p2align 3
.L817:
	ret
	.cfi_endproc
.LFE20197:
	.size	_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeTraceTreeEv, .-_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeTraceTreeEv
	.section	.text._ZN2v88internal26HeapSnapshotJSONSerializer16SerializeSamplesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26HeapSnapshotJSONSerializer16SerializeSamplesEv
	.type	_ZN2v88internal26HeapSnapshotJSONSerializer16SerializeSamplesEv, @function
_ZN2v88internal26HeapSnapshotJSONSerializer16SerializeSamplesEv:
.LFB20201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	%rdi, -184(%rbp)
	movq	(%rax), %rax
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	8(%rax), %rax
	movq	64(%rax), %rbx
	movq	56(%rax), %r14
	movq	%rbx, -160(%rbp)
	cmpq	%rbx, %r14
	je	.L819
	movq	16(%r14), %rax
	movq	$34, -104(%rbp)
	xorl	%ebx, %ebx
	movl	$3435973837, %r12d
	movq	%r14, -152(%rbp)
	movabsq	$-3689348814741910323, %r13
	movq	%rax, -168(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -112(%rbp)
	leaq	-120(%rbp), %rax
	movq	%rax, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L821:
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %rdi
	movq	16(%rax), %rax
	movq	%rax, -136(%rbp)
	subq	-168(%rbp), %rax
	movq	%rax, -120(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	xorl	%edi, %edi
	movq	%rax, %rcx
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L822:
	movq	%rdx, %rax
	movq	%rdx, %rsi
	addl	$1, %edi
	mulq	%r13
	shrq	$3, %rdx
	cmpq	$9, %rsi
	ja	.L822
	addl	%edi, %ebx
	movslq	%ebx, %rdi
	leaq	-1(%rdi), %rsi
	.p2align 4,,10
	.p2align 3
.L823:
	movq	%rcx, %rax
	movq	%rcx, %r9
	movq	-112(%rbp), %r8
	mulq	%r13
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %r9
	movq	%r9, %rax
	addl	$48, %eax
	movb	%al, (%r8,%rsi)
	movq	%rcx, %rax
	subq	$1, %rsi
	movq	%rdx, %rcx
	cmpq	$9, %rax
	ja	.L823
	movq	-112(%rbp), %rax
	addl	$1, %ebx
	xorl	%esi, %esi
	movb	$44, (%rax,%rdi)
	movq	-152(%rbp), %rax
	movl	(%rax), %eax
	leal	-2(%rax), %edx
	movl	%eax, -136(%rbp)
	movl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L824:
	movl	%eax, %eax
	addl	$1, %esi
	movq	%rax, %rcx
	imulq	%r12, %rax
	shrq	$35, %rax
	cmpl	$9, %ecx
	ja	.L824
	addl	%esi, %ebx
	movslq	%ebx, %rdi
	leaq	-1(%rdi), %rsi
	.p2align 4,,10
	.p2align 3
.L825:
	movl	%edx, %eax
	movl	%edx, %r10d
	movq	-112(%rbp), %r8
	imulq	%r12, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %r10d
	movl	%r10d, %ecx
	addl	$48, %ecx
	movb	%cl, (%r8,%rsi)
	movl	%edx, %ecx
	subq	$1, %rsi
	movl	%eax, %edx
	cmpl	$9, %ecx
	ja	.L825
	movq	-112(%rbp), %rax
	addl	$1, %ebx
	movslq	%ebx, %rbx
	movb	$10, (%rax,%rdi)
	movq	-112(%rbp), %rax
	movb	$0, (%rax,%rbx)
	movq	-112(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	testl	%eax, %eax
	jle	.L826
	cltq
	addq	%r14, %rax
	movq	%rax, -136(%rbp)
	cmpq	%rax, %r14
	jnb	.L826
	movq	-184(%rbp), %rax
	movq	%r14, %rbx
	movq	40(%rax), %r15
	movl	32(%r15), %edx
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L828:
	cmpq	%rbx, -136(%rbp)
	jbe	.L826
.L827:
	movl	8(%r15), %eax
	movq	-136(%rbp), %r14
	movq	%rbx, %rsi
	movq	16(%r15), %rdi
	subl	%edx, %eax
	subq	%rbx, %r14
	movslq	%edx, %rdx
	cmpl	%r14d, %eax
	cmovle	%eax, %r14d
	addq	%rdx, %rdi
	movslq	%r14d, %r8
	movq	%r8, %rdx
	movq	%r8, -144(%rbp)
	call	memcpy@PLT
	movl	32(%r15), %edx
	movq	-144(%rbp), %r8
	addl	%r14d, %edx
	addq	%r8, %rbx
	movl	%edx, 32(%r15)
	cmpl	8(%r15), %edx
	jne	.L828
	cmpb	$0, 36(%r15)
	jne	.L828
	movq	(%r15), %rdi
	movq	16(%r15), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L829
	movb	$1, 36(%r15)
.L829:
	movl	$0, 32(%r15)
	xorl	%edx, %edx
	cmpq	%rbx, -136(%rbp)
	ja	.L827
	.p2align 4,,10
	.p2align 3
.L826:
	addq	$24, -152(%rbp)
	movq	-152(%rbp), %rax
	cmpq	%rax, -160(%rbp)
	je	.L819
	movq	-112(%rbp), %rax
	movl	$1, %ebx
	movb	$44, (%rax)
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L819:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L838
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L838:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20201:
	.size	_ZN2v88internal26HeapSnapshotJSONSerializer16SerializeSamplesEv, .-_ZN2v88internal26HeapSnapshotJSONSerializer16SerializeSamplesEv
	.section	.rodata._ZN2v88internal26HeapSnapshotJSONSerializer15SerializeStringEPKh.str1.1,"aMS",@progbits,1
.LC113:
	.string	"\\b"
.LC114:
	.string	"\\f"
.LC115:
	.string	"\\r"
.LC116:
	.string	"\\t"
	.section	.text._ZN2v88internal26HeapSnapshotJSONSerializer15SerializeStringEPKh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26HeapSnapshotJSONSerializer15SerializeStringEPKh
	.type	_ZN2v88internal26HeapSnapshotJSONSerializer15SerializeStringEPKh, @function
_ZN2v88internal26HeapSnapshotJSONSerializer15SerializeStringEPKh:
.LFB20202:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%rdi, -72(%rbp)
	movq	40(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	32(%r12), %rax
	leal	1(%rax), %edx
	movl	%edx, 32(%r12)
	movq	16(%r12), %rdx
	movb	$10, (%rdx,%rax)
	movl	32(%r12), %edx
	cmpl	8(%r12), %edx
	je	.L905
.L840:
	movq	-72(%rbp), %rax
	movq	40(%rax), %r12
	movslq	32(%r12), %rax
	leal	1(%rax), %edx
	movl	%edx, 32(%r12)
	movq	16(%r12), %rdx
	movb	$34, (%rdx,%rax)
	movl	32(%r12), %edx
	cmpl	8(%r12), %edx
	je	.L906
.L842:
	movzbl	(%rbx), %esi
	testb	%sil, %sil
	je	.L881
	cmpb	$34, %sil
	ja	.L847
	.p2align 4,,10
	.p2align 3
.L908:
	cmpb	$7, %sil
	jbe	.L848
	leal	-8(%rsi), %eax
	cmpb	$26, %al
	ja	.L848
	leaq	.L850(%rip), %rcx
	movzbl	%al, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal26HeapSnapshotJSONSerializer15SerializeStringEPKh,"a",@progbits
	.align 4
	.align 4
.L850:
	.long	.L855-.L850
	.long	.L854-.L850
	.long	.L853-.L850
	.long	.L848-.L850
	.long	.L852-.L850
	.long	.L851-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L848-.L850
	.long	.L849-.L850
	.section	.text._ZN2v88internal26HeapSnapshotJSONSerializer15SerializeStringEPKh
.L849:
	movq	-72(%rbp), %rax
	movq	40(%rax), %r12
	movslq	32(%r12), %rax
	leal	1(%rax), %edx
	movl	%edx, 32(%r12)
	movq	16(%r12), %rdx
	movb	$92, (%rdx,%rax)
	movl	32(%r12), %edx
	cmpl	8(%r12), %edx
	je	.L907
.L873:
	movq	-72(%rbp), %rax
	movzbl	(%rbx), %ecx
	movq	40(%rax), %r12
	movslq	32(%r12), %rax
	leal	1(%rax), %edx
	movl	%edx, 32(%r12)
	movq	16(%r12), %rdx
	movb	%cl, (%rdx,%rax)
	movl	32(%r12), %edx
	cmpl	8(%r12), %edx
	je	.L904
	.p2align 4,,10
	.p2align 3
.L858:
	movzbl	1(%rbx), %esi
	leaq	1(%rbx), %rax
	testb	%sil, %sil
	je	.L881
.L910:
	movq	%rax, %rbx
	cmpb	$34, %sil
	jbe	.L908
.L847:
	cmpb	$92, %sil
	je	.L849
	leal	-32(%rsi), %eax
	cmpb	$95, %al
	ja	.L909
.L883:
	movq	-72(%rbp), %rax
	movq	40(%rax), %r12
	movslq	32(%r12), %rax
	leal	1(%rax), %edx
	movl	%edx, 32(%r12)
	movq	16(%r12), %rdx
	movb	%sil, (%rdx,%rax)
	movl	32(%r12), %edx
	cmpl	8(%r12), %edx
	jne	.L858
.L904:
	cmpb	$0, 36(%r12)
	jne	.L858
	movq	(%r12), %rdi
	movq	16(%r12), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L877
	movb	$1, 36(%r12)
.L877:
	movl	$0, 32(%r12)
	movzbl	1(%rbx), %esi
	leaq	1(%rbx), %rax
	testb	%sil, %sil
	jne	.L910
	.p2align 4,,10
	.p2align 3
.L881:
	movq	-72(%rbp), %rax
	movq	40(%rax), %rbx
	movslq	32(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, 32(%rbx)
	movq	16(%rbx), %rdx
	movb	$34, (%rdx,%rax)
	movl	32(%rbx), %edx
	cmpl	8(%rbx), %edx
	je	.L911
.L839:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L912
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L851:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	leaq	.LC115(%rip), %r15
	movq	40(%rax), %r13
	movl	32(%r13), %edx
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L868:
	leaq	2+.LC115(%rip), %rax
	cmpq	%rax, %r15
	jnb	.L858
.L867:
	movl	8(%r13), %eax
	leaq	2+.LC115(%rip), %r9
	movslq	%edx, %rdi
	movq	%r15, %rsi
	subq	%r15, %r9
	subl	%edx, %eax
	movl	%r9d, %r14d
	cmpl	%r9d, %eax
	cmovle	%eax, %r14d
	addq	16(%r13), %rdi
	movslq	%r14d, %r12
	movq	%r12, %rdx
	addq	%r12, %r15
	call	memcpy@PLT
	movl	32(%r13), %edx
	addl	%r14d, %edx
	movl	%edx, 32(%r13)
	cmpl	8(%r13), %edx
	jne	.L868
	cmpb	$0, 36(%r13)
	jne	.L868
	movq	0(%r13), %rdi
	movq	16(%r13), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L869
	movb	$1, 36(%r13)
.L869:
	movl	$0, 32(%r13)
	xorl	%edx, %edx
	jmp	.L868
.L852:
	movq	-72(%rbp), %rax
	leaq	.LC114(%rip), %r13
	movq	40(%rax), %r15
	movl	32(%r15), %edx
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L862:
	leaq	2+.LC114(%rip), %rax
	cmpq	%rax, %r13
	jnb	.L858
.L861:
	movl	8(%r15), %eax
	leaq	2+.LC114(%rip), %r9
	movslq	%edx, %rdi
	movq	%r13, %rsi
	subq	%r13, %r9
	subl	%edx, %eax
	movl	%r9d, %r14d
	cmpl	%r9d, %eax
	cmovle	%eax, %r14d
	addq	16(%r15), %rdi
	movslq	%r14d, %r12
	movq	%r12, %rdx
	addq	%r12, %r13
	call	memcpy@PLT
	movl	32(%r15), %edx
	addl	%r14d, %edx
	movl	%edx, 32(%r15)
	cmpl	8(%r15), %edx
	jne	.L862
	cmpb	$0, 36(%r15)
	jne	.L862
	movq	(%r15), %rdi
	movq	16(%r15), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L863
	movb	$1, 36(%r15)
.L863:
	movl	$0, 32(%r15)
	xorl	%edx, %edx
	jmp	.L862
.L853:
	movq	-72(%rbp), %rax
	leaq	.LC26(%rip), %r13
	movq	40(%rax), %r15
	movl	32(%r15), %edx
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L865:
	leaq	2+.LC26(%rip), %rax
	cmpq	%rax, %r13
	jnb	.L858
.L864:
	movl	8(%r15), %eax
	leaq	2+.LC26(%rip), %r9
	movslq	%edx, %rdi
	movq	%r13, %rsi
	subq	%r13, %r9
	subl	%edx, %eax
	movl	%r9d, %r14d
	cmpl	%r9d, %eax
	cmovle	%eax, %r14d
	addq	16(%r15), %rdi
	movslq	%r14d, %r12
	movq	%r12, %rdx
	addq	%r12, %r13
	call	memcpy@PLT
	movl	32(%r15), %edx
	addl	%r14d, %edx
	movl	%edx, 32(%r15)
	cmpl	8(%r15), %edx
	jne	.L865
	cmpb	$0, 36(%r15)
	jne	.L865
	movq	(%r15), %rdi
	movq	16(%r15), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L866
	movb	$1, 36(%r15)
.L866:
	movl	$0, 32(%r15)
	xorl	%edx, %edx
	jmp	.L865
.L854:
	movq	-72(%rbp), %rax
	leaq	.LC116(%rip), %r15
	movq	40(%rax), %r12
	movl	32(%r12), %edx
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L871:
	leaq	2+.LC116(%rip), %rax
	cmpq	%rax, %r15
	jnb	.L858
.L870:
	movl	8(%r12), %eax
	leaq	2+.LC116(%rip), %r8
	movslq	%edx, %rdi
	movq	%r15, %rsi
	subq	%r15, %r8
	subl	%edx, %eax
	movl	%r8d, %r13d
	cmpl	%r8d, %eax
	cmovle	%eax, %r13d
	addq	16(%r12), %rdi
	movslq	%r13d, %r14
	movq	%r14, %rdx
	addq	%r14, %r15
	call	memcpy@PLT
	movl	32(%r12), %edx
	addl	%r13d, %edx
	movl	%edx, 32(%r12)
	cmpl	8(%r12), %edx
	jne	.L871
	cmpb	$0, 36(%r12)
	jne	.L871
	movq	(%r12), %rdi
	movq	16(%r12), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L872
	movb	$1, 36(%r12)
.L872:
	movl	$0, 32(%r12)
	xorl	%edx, %edx
	jmp	.L871
.L855:
	movq	-72(%rbp), %rax
	leaq	.LC113(%rip), %r13
	movq	40(%rax), %r15
	movl	32(%r15), %edx
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L859:
	leaq	2+.LC113(%rip), %rax
	cmpq	%rax, %r13
	jnb	.L858
.L857:
	movl	8(%r15), %eax
	leaq	2+.LC113(%rip), %r9
	movslq	%edx, %rdi
	movq	%r13, %rsi
	subq	%r13, %r9
	subl	%edx, %eax
	movl	%r9d, %r14d
	cmpl	%r9d, %eax
	cmovle	%eax, %r14d
	addq	16(%r15), %rdi
	movslq	%r14d, %r12
	movq	%r12, %rdx
	addq	%r12, %r13
	call	memcpy@PLT
	movl	32(%r15), %edx
	addl	%r14d, %edx
	movl	%edx, 32(%r15)
	cmpl	8(%r15), %edx
	jne	.L859
	cmpb	$0, 36(%r15)
	jne	.L859
	movq	(%r15), %rdi
	movq	16(%r15), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L860
	movb	$1, 36(%r15)
.L860:
	movl	$0, 32(%r15)
	xorl	%edx, %edx
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L907:
	cmpb	$0, 36(%r12)
	jne	.L873
	movq	(%r12), %rdi
	movq	16(%r12), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	je	.L913
.L874:
	movl	$0, 32(%r12)
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L911:
	cmpb	$0, 36(%rbx)
	jne	.L839
	movq	(%rbx), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	je	.L914
.L882:
	movl	$0, 32(%rbx)
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L906:
	cmpb	$0, 36(%r12)
	jne	.L842
	movq	(%r12), %rdi
	movq	16(%r12), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	je	.L915
.L843:
	movl	$0, 32(%r12)
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L905:
	cmpb	$0, 36(%r12)
	jne	.L840
	movq	(%r12), %rdi
	movq	16(%r12), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	je	.L916
.L841:
	movl	$0, 32(%r12)
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L909:
	cmpb	$0, 1(%rbx)
	movq	$0, -64(%rbp)
	je	.L886
	cmpb	$0, 2(%rbx)
	je	.L887
	cmpb	$0, 3(%rbx)
	je	.L888
	xorl	%esi, %esi
	cmpb	$0, 4(%rbx)
	setne	%sil
	addq	$4, %rsi
.L878:
	movq	%rbx, %rdi
	leaq	-64(%rbp), %rdx
	call	_ZN7unibrow4Utf814CalculateValueEPKhmPm@PLT
	movl	%eax, %esi
	movq	-72(%rbp), %rax
	movq	40(%rax), %rdi
	cmpl	$65533, %esi
	je	.L879
	call	_ZN2v88internalL10WriteUCharEPNS0_18OutputStreamWriterEj
	movq	-64(%rbp), %rax
	leaq	-1(%rbx,%rax), %rbx
	jmp	.L858
.L913:
	movb	$1, 36(%r12)
	jmp	.L874
.L914:
	movb	$1, 36(%rbx)
	jmp	.L882
.L916:
	movb	$1, 36(%r12)
	jmp	.L841
.L915:
	movb	$1, 36(%r12)
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L879:
	movl	$63, %esi
	call	_ZN2v88internal18OutputStreamWriter12AddCharacterEc
	jmp	.L858
.L848:
	leal	-32(%rsi), %eax
	cmpb	$95, %al
	jbe	.L883
	movq	-72(%rbp), %rax
	movq	40(%rax), %rdi
	call	_ZN2v88internalL10WriteUCharEPNS0_18OutputStreamWriterEj
	jmp	.L858
.L886:
	movl	$1, %esi
	jmp	.L878
.L887:
	movl	$2, %esi
	jmp	.L878
.L888:
	movl	$3, %esi
	jmp	.L878
.L912:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20202:
	.size	_ZN2v88internal26HeapSnapshotJSONSerializer15SerializeStringEPKh, .-_ZN2v88internal26HeapSnapshotJSONSerializer15SerializeStringEPKh
	.section	.rodata._ZN2v88internal26HeapSnapshotJSONSerializer16SerializeStringsEv.str1.1,"aMS",@progbits,1
.LC117:
	.string	"\"<dummy>\""
.LC118:
	.string	"NewArray"
	.section	.text._ZN2v88internal26HeapSnapshotJSONSerializer16SerializeStringsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26HeapSnapshotJSONSerializer16SerializeStringsEv
	.type	_ZN2v88internal26HeapSnapshotJSONSerializer16SerializeStringsEv, @function
_ZN2v88internal26HeapSnapshotJSONSerializer16SerializeStringsEv:
.LFB20203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	20(%rdi), %eax
	leal	1(%rax), %r15d
	movl	%eax, -72(%rbp)
	movl	%r15d, -68(%rbp)
	salq	$3, %r15
	movq	%r15, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L942
.L918:
	movl	16(%rbx), %edx
	movq	8(%rbx), %rax
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rsi
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L919:
	movq	(%rax), %rdx
	movq	%rax, %rcx
	addq	$24, %rax
	testq	%rdx, %rdx
	jne	.L921
.L939:
	cmpq	%rax, %rsi
	ja	.L919
	.p2align 4,,10
	.p2align 3
.L922:
	movq	40(%rbx), %r13
	leaq	.LC117(%rip), %r14
	leaq	9(%r14), %r15
	movl	32(%r13), %edx
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L925:
	cmpq	%r15, %r14
	jnb	.L924
.L920:
	movl	8(%r13), %eax
	movq	%r15, %r9
	movslq	%edx, %rdi
	movq	%r14, %rsi
	subq	%r14, %r9
	subl	%edx, %eax
	movl	%r9d, %r12d
	cmpl	%r9d, %eax
	cmovle	%eax, %r12d
	addq	16(%r13), %rdi
	movslq	%r12d, %r10
	movq	%r10, %rdx
	movq	%r10, -56(%rbp)
	call	memcpy@PLT
	movl	32(%r13), %edx
	movq	-56(%rbp), %r10
	addl	%r12d, %edx
	addq	%r10, %r14
	movl	%edx, 32(%r13)
	cmpl	8(%r13), %edx
	jne	.L925
	cmpb	$0, 36(%r13)
	jne	.L925
	movq	0(%r13), %rdi
	movq	16(%r13), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L926
	movb	$1, 36(%r13)
.L926:
	movl	$0, 32(%r13)
	xorl	%edx, %edx
	cmpq	%r15, %r14
	jb	.L920
.L924:
	cmpl	$1, -68(%rbp)
	jle	.L941
	movl	-72(%rbp), %eax
	movq	-64(%rbp), %rdi
	movq	40(%rbx), %r15
	subl	$1, %eax
	leaq	8(%rdi), %r14
	leaq	16(%rdi,%rax,8), %r13
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L928:
	movq	(%r14), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal26HeapSnapshotJSONSerializer15SerializeStringEPKh
	movq	40(%rbx), %r15
	cmpb	$0, 36(%r15)
	jne	.L941
	addq	$8, %r14
	cmpq	%r14, %r13
	je	.L941
.L931:
	movslq	32(%r15), %rax
	leal	1(%rax), %edx
	movl	%edx, 32(%r15)
	movq	16(%r15), %rdx
	movb	$44, (%rdx,%rax)
	movl	32(%r15), %edx
	cmpl	8(%r15), %edx
	jne	.L928
	cmpb	$0, 36(%r15)
	jne	.L928
	movq	(%r15), %rdi
	movq	16(%r15), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L929
	movb	$1, 36(%r15)
.L929:
	movl	$0, 32(%r15)
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L921:
	movslq	8(%rcx), %rax
	movq	-64(%rbp), %rdi
	movq	%rdx, (%rdi,%rax,8)
	leaq	24(%rcx), %rax
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L943:
	movq	(%rax), %rdx
	movq	%rax, %rcx
	addq	$24, %rax
	testq	%rdx, %rdx
	jne	.L921
.L940:
	cmpq	%rax, %rsi
	ja	.L943
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L941:
	movq	-64(%rbp), %rdi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdaPv@PLT
.L942:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r15, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	jne	.L918
	leaq	.LC118(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE20203:
	.size	_ZN2v88internal26HeapSnapshotJSONSerializer16SerializeStringsEv, .-_ZN2v88internal26HeapSnapshotJSONSerializer16SerializeStringsEv
	.section	.text._ZN2v88internal26HeapSnapshotJSONSerializer17SerializeLocationERKNS0_14SourceLocationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26HeapSnapshotJSONSerializer17SerializeLocationERKNS0_14SourceLocationE
	.type	_ZN2v88internal26HeapSnapshotJSONSerializer17SerializeLocationERKNS0_14SourceLocationE, @function
_ZN2v88internal26HeapSnapshotJSONSerializer17SerializeLocationERKNS0_14SourceLocationE:
.LFB20204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3435973837, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	xorl	%edi, %edi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-112(%rbp), %rax
	movq	$45, -120(%rbp)
	movq	%rax, -128(%rbp)
	movl	(%rsi), %eax
	leal	(%rax,%rax,2), %r10d
	addl	%r10d, %r10d
	movl	%r10d, %eax
	.p2align 4,,10
	.p2align 3
.L945:
	movl	%eax, %eax
	movl	%edi, %r8d
	addl	$1, %edi
	movq	%rax, %rcx
	imulq	%rdx, %rax
	shrq	$35, %rax
	cmpl	$9, %ecx
	ja	.L945
	movl	%r10d, %eax
	movl	%r10d, %ecx
	movslq	%r8d, %r9
	imulq	%rax, %rdx
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %eax
	addl	%eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %eax
	addl	$48, %eax
	movb	%al, -112(%rbp,%r9)
	cmpl	$9, %r10d
	jbe	.L946
	subq	$1, %r9
	movl	$3435973837, %r10d
	.p2align 4,,10
	.p2align 3
.L947:
	movl	%edx, %eax
	movl	%edx, %r15d
	movq	-128(%rbp), %r11
	imulq	%r10, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %r15d
	movl	%r15d, %ecx
	addl	$48, %ecx
	movb	%cl, (%r11,%r9)
	movl	%edx, %ecx
	subq	$1, %r9
	movl	%eax, %edx
	cmpl	$9, %ecx
	ja	.L947
.L946:
	movq	-128(%rbp), %rax
	movslq	%edi, %rdi
	addl	$2, %r8d
	movl	$3435973837, %r9d
	movb	$44, (%rax,%rdi)
	movl	4(%rsi), %edx
	xorl	%edi, %edi
	movl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L948:
	movl	%eax, %eax
	addl	$1, %edi
	movq	%rax, %rcx
	imulq	%r9, %rax
	shrq	$35, %rax
	cmpl	$9, %ecx
	ja	.L948
	addl	%edi, %r8d
	movl	$3435973837, %r9d
	movslq	%r8d, %r10
	leaq	-1(%r10), %rdi
	.p2align 4,,10
	.p2align 3
.L949:
	movl	%edx, %eax
	movl	%edx, %r15d
	movq	-128(%rbp), %r11
	imulq	%r9, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %r15d
	movl	%r15d, %ecx
	addl	$48, %ecx
	movb	%cl, (%r11,%rdi)
	movl	%edx, %ecx
	subq	$1, %rdi
	movl	%eax, %edx
	cmpl	$9, %ecx
	ja	.L949
	movq	-128(%rbp), %rax
	addl	$1, %r8d
	xorl	%edi, %edi
	movl	$3435973837, %r9d
	movb	$44, (%rax,%r10)
	movl	8(%rsi), %edx
	movl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L950:
	movl	%eax, %eax
	addl	$1, %edi
	movq	%rax, %rcx
	imulq	%r9, %rax
	shrq	$35, %rax
	cmpl	$9, %ecx
	ja	.L950
	addl	%edi, %r8d
	movl	$3435973837, %r9d
	movslq	%r8d, %r10
	leaq	-1(%r10), %rdi
	.p2align 4,,10
	.p2align 3
.L951:
	movl	%edx, %eax
	movl	%edx, %r14d
	movq	-128(%rbp), %r11
	imulq	%r9, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %r14d
	movl	%r14d, %ecx
	addl	$48, %ecx
	movb	%cl, (%r11,%rdi)
	movl	%edx, %ecx
	subq	$1, %rdi
	movl	%eax, %edx
	cmpl	$9, %ecx
	ja	.L951
	movq	-128(%rbp), %rax
	addl	$1, %r8d
	xorl	%edi, %edi
	movl	$3435973837, %r9d
	movb	$44, (%rax,%r10)
	movl	12(%rsi), %edx
	movl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L952:
	movl	%eax, %eax
	addl	$1, %edi
	movq	%rax, %rcx
	imulq	%r9, %rax
	shrq	$35, %rax
	cmpl	$9, %ecx
	ja	.L952
	leal	(%r8,%rdi), %esi
	movl	$3435973837, %r8d
	movslq	%esi, %r9
	leaq	-1(%r9), %rdi
	.p2align 4,,10
	.p2align 3
.L953:
	movl	%edx, %eax
	movl	%edx, %r10d
	imulq	%r8, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %r10d
	movl	%r10d, %ecx
	movq	-128(%rbp), %r10
	addl	$48, %ecx
	movb	%cl, (%r10,%rdi)
	movl	%edx, %ecx
	subq	$1, %rdi
	movl	%eax, %edx
	cmpl	$9, %ecx
	ja	.L953
	movq	-128(%rbp), %rax
	addl	$1, %esi
	movslq	%esi, %rsi
	movb	$10, (%rax,%r9)
	movq	-128(%rbp), %rax
	movb	$0, (%rax,%rsi)
	movq	-128(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	testl	%eax, %eax
	jle	.L944
	cltq
	leaq	(%r14,%rax), %r12
	cmpq	%r12, %r14
	jnb	.L944
	movq	40(%rbx), %r13
	movl	32(%r13), %edx
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L956:
	cmpq	%r14, %r12
	jbe	.L944
.L955:
	movl	8(%r13), %ebx
	movq	%r12, %rax
	movslq	%edx, %rdi
	movq	%r14, %rsi
	subq	%r14, %rax
	subl	%edx, %ebx
	cmpl	%eax, %ebx
	cmovg	%eax, %ebx
	addq	16(%r13), %rdi
	movslq	%ebx, %r15
	movq	%r15, %rdx
	addq	%r15, %r14
	call	memcpy@PLT
	addl	32(%r13), %ebx
	movl	%ebx, 32(%r13)
	movl	%ebx, %edx
	cmpl	8(%r13), %ebx
	jne	.L956
	cmpb	$0, 36(%r13)
	jne	.L956
	movq	0(%r13), %rdi
	movq	16(%r13), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L957
	movb	$1, 36(%r13)
.L957:
	movl	$0, 32(%r13)
	xorl	%edx, %edx
	cmpq	%r14, %r12
	ja	.L955
	.p2align 4,,10
	.p2align 3
.L944:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L968
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L968:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20204:
	.size	_ZN2v88internal26HeapSnapshotJSONSerializer17SerializeLocationERKNS0_14SourceLocationE, .-_ZN2v88internal26HeapSnapshotJSONSerializer17SerializeLocationERKNS0_14SourceLocationE
	.section	.text._ZN2v88internal26HeapSnapshotJSONSerializer18SerializeLocationsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeLocationsEv
	.type	_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeLocationsEv, @function
_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeLocationsEv:
.LFB20205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r13
	xorl	%ebx, %ebx
	movq	456(%r13), %rsi
	cmpq	464(%r13), %rsi
	je	.L969
	.p2align 4,,10
	.p2align 3
.L970:
	movq	%rbx, %rax
	movq	%r14, %rdi
	salq	$4, %rax
	addq	%rax, %rsi
	call	_ZN2v88internal26HeapSnapshotJSONSerializer17SerializeLocationERKNS0_14SourceLocationE
	movq	40(%r14), %r12
	cmpb	$0, 36(%r12)
	jne	.L969
	movq	456(%r13), %rsi
	movq	464(%r13), %rax
	addq	$1, %rbx
	subq	%rsi, %rax
	sarq	$4, %rax
	cmpq	%rax, %rbx
	jnb	.L969
	testq	%rbx, %rbx
	je	.L970
	movslq	32(%r12), %rax
	leal	1(%rax), %edx
	movl	%edx, 32(%r12)
	movq	16(%r12), %rdx
	movb	$44, (%rdx,%rax)
	movl	32(%r12), %edx
	cmpl	8(%r12), %edx
	je	.L972
.L983:
	movq	456(%r13), %rsi
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L969:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L972:
	.cfi_restore_state
	cmpb	$0, 36(%r12)
	jne	.L983
	movq	(%r12), %rdi
	movq	16(%r12), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	je	.L984
	movl	$0, 32(%r12)
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L984:
	movb	$1, 36(%r12)
	movl	$0, 32(%r12)
	jmp	.L983
	.cfi_endproc
.LFE20205:
	.size	_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeLocationsEv, .-_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeLocationsEv
	.section	.text._ZNSt6vectorIN2v88internal14SourceLocationESaIS2_EE12emplace_backIJiRiS6_S6_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal14SourceLocationESaIS2_EE12emplace_backIJiRiS6_S6_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal14SourceLocationESaIS2_EE12emplace_backIJiRiS6_S6_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal14SourceLocationESaIS2_EE12emplace_backIJiRiS6_S6_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal14SourceLocationESaIS2_EE12emplace_backIJiRiS6_S6_EEEvDpOT_:
.LFB22458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L986
	movd	(%rdx), %xmm0
	movd	(%rsi), %xmm3
	movd	(%r8), %xmm1
	movd	(%rcx), %xmm2
	punpckldq	%xmm0, %xmm3
	punpckldq	%xmm1, %xmm2
	movdqa	%xmm3, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	addq	$16, 8(%rdi)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L986:
	.cfi_restore_state
	movq	(%rdi), %r15
	movq	%r12, %r9
	movabsq	$576460752303423487, %rdi
	subq	%r15, %r9
	movq	%r9, %rax
	sarq	$4, %rax
	cmpq	%rdi, %rax
	je	.L1003
	testq	%rax, %rax
	je	.L995
	movabsq	$9223372036854775792, %rsi
	leaq	(%rax,%rax), %r10
	cmpq	%r10, %rax
	jbe	.L1004
.L989:
	movq	%rsi, %rdi
	movq	%r8, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	%rax, %r14
	movq	-88(%rbp), %r8
	addq	%rax, %rsi
	leaq	16(%rax), %rax
.L990:
	movd	(%rdx), %xmm0
	movd	0(%r13), %xmm6
	movd	(%r8), %xmm1
	movd	(%rcx), %xmm5
	punpckldq	%xmm0, %xmm6
	punpckldq	%xmm1, %xmm5
	movdqa	%xmm6, %xmm0
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, (%r14,%r9)
	cmpq	%r15, %r12
	je	.L991
	subq	$16, %r12
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	subq	%r15, %r12
	movq	%r12, %rax
	shrq	$4, %rax
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L992:
	movdqu	(%r15,%rdx), %xmm4
	addq	$1, %rcx
	movups	%xmm4, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rcx
	jb	.L992
	leaq	32(%r14,%r12), %rax
.L993:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rax
.L994:
	movq	%r14, %xmm0
	movq	%rax, %xmm7
	movq	%rsi, 16(%rbx)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, (%rbx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L991:
	.cfi_restore_state
	testq	%r15, %r15
	je	.L994
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1004:
	testq	%r10, %r10
	jne	.L1005
	movl	$16, %eax
	xorl	%esi, %esi
	xorl	%r14d, %r14d
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L995:
	movl	$16, %esi
	jmp	.L989
.L1003:
	leaq	.LC31(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1005:
	cmpq	%rdi, %r10
	cmovbe	%r10, %rdi
	movq	%rdi, %rsi
	salq	$4, %rsi
	jmp	.L989
	.cfi_endproc
.LFE22458:
	.size	_ZNSt6vectorIN2v88internal14SourceLocationESaIS2_EE12emplace_backIJiRiS6_S6_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal14SourceLocationESaIS2_EE12emplace_backIJiRiS6_S6_EEEvDpOT_
	.section	.text._ZN2v88internal14V8HeapExplorer28ExtractLocationForJSFunctionEPNS0_9HeapEntryENS0_10JSFunctionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer28ExtractLocationForJSFunctionEPNS0_9HeapEntryENS0_10JSFunctionE
	.type	_ZN2v88internal14V8HeapExplorer28ExtractLocationForJSFunctionEPNS0_9HeapEntryENS0_10JSFunctionE, @function
_ZN2v88internal14V8HeapExplorer28ExtractLocationForJSFunctionEPNS0_9HeapEntryENS0_10JSFunctionE:
.LFB19967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	23(%rdx), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1014
.L1006:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1015
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1014:
	.cfi_restore_state
	movq	%rsi, %rbx
	movq	-1(%rax), %rsi
	movq	%rdi, %r12
	leaq	-1(%rax), %rcx
	cmpw	$86, 11(%rsi)
	je	.L1016
.L1008:
	movq	(%rcx), %rax
	cmpw	$96, 11(%rax)
	jne	.L1006
	movq	23(%rdx), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1017
.L1011:
	movq	%rax, -72(%rbp)
	movslq	67(%rax), %rcx
	leaq	-64(%rbp), %r14
	movq	%r14, %rdi
	movq	%rcx, -112(%rbp)
	movq	23(%rdx), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	leaq	-72(%rbp), %rdi
	movl	%eax, %esi
	movq	%rdi, -104(%rbp)
	movl	%eax, %r13d
	call	_ZNK2v88internal6Script13GetLineNumberEi@PLT
	movq	-104(%rbp), %rdi
	movl	%r13d, %esi
	movl	%eax, %r15d
	call	_ZNK2v88internal6Script15GetColumnNumberEi@PLT
	movq	-112(%rbp), %rcx
	leaq	-84(%rbp), %rdx
	movq	%r14, %rsi
	movl	%eax, -76(%rbp)
	movq	16(%r12), %rdi
	leaq	-76(%rbp), %r8
	movl	(%rbx), %eax
	movl	%ecx, -84(%rbp)
	leaq	-80(%rbp), %rcx
	addq	$456, %rdi
	movl	%r15d, -80(%rbp)
	shrl	$4, %eax
	movl	%eax, -64(%rbp)
	call	_ZNSt6vectorIN2v88internal14SourceLocationESaIS2_EE12emplace_backIJiRiS6_S6_EEEvDpOT_
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	23(%rax), %rcx
	testb	$1, %cl
	je	.L1006
	subq	$1, %rcx
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L1011
	movq	23(%rax), %rax
	jmp	.L1011
.L1015:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19967:
	.size	_ZN2v88internal14V8HeapExplorer28ExtractLocationForJSFunctionEPNS0_9HeapEntryENS0_10JSFunctionE, .-_ZN2v88internal14V8HeapExplorer28ExtractLocationForJSFunctionEPNS0_9HeapEntryENS0_10JSFunctionE
	.section	.text._ZN2v88internal14V8HeapExplorer15ExtractLocationEPNS0_9HeapEntryENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer15ExtractLocationEPNS0_9HeapEntryENS0_10HeapObjectE
	.type	_ZN2v88internal14V8HeapExplorer15ExtractLocationEPNS0_9HeapEntryENS0_10HeapObjectE, @function
_ZN2v88internal14V8HeapExplorer15ExtractLocationEPNS0_9HeapEntryENS0_10HeapObjectE:
.LFB19966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	-1(%rdx), %rax
	cmpw	$1105, 11(%rax)
	je	.L1028
	movq	-1(%rdx), %rax
	cmpw	$1068, 11(%rax)
	jne	.L1029
.L1020:
	movq	23(%rdx), %rdx
.L1028:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer28ExtractLocationForJSFunctionEPNS0_9HeapEntryENS0_10JSFunctionE
	.p2align 4,,10
	.p2align 3
.L1029:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	cmpw	$1063, 11(%rax)
	je	.L1020
	movq	-1(%rdx), %rax
	cmpw	$1064, 11(%rax)
	je	.L1020
	movq	-1(%rdx), %rax
	cmpw	$1024, 11(%rax)
	ja	.L1030
.L1018:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1030:
	.cfi_restore_state
	movq	%rdx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal14V8HeapExplorer14GetConstructorENS0_10JSReceiverE
	movq	-24(%rbp), %rsi
	testq	%rax, %rax
	je	.L1018
	movq	%rax, %rdx
	jmp	.L1028
	.cfi_endproc
.LFE19966:
	.size	_ZN2v88internal14V8HeapExplorer15ExtractLocationEPNS0_9HeapEntryENS0_10HeapObjectE, .-_ZN2v88internal14V8HeapExplorer15ExtractLocationEPNS0_9HeapEntryENS0_10HeapObjectE
	.section	.text._ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6RemoveERKS2_j,"axG",@progbits,_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6RemoveERKS2_j,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6RemoveERKS2_j
	.type	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6RemoveERKS2_j, @function
_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6RemoveERKS2_j:
.LFB22527:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %r10d
	movq	%rsi, %r9
	movq	(%rdi), %rcx
	leal	-1(%r10), %esi
	andl	%esi, %edx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	je	.L1031
	movq	(%r9), %r9
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1051:
	addq	$1, %rdx
	andq	%rsi, %rdx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	je	.L1031
.L1034:
	cmpq	%r8, %r9
	jne	.L1051
	movq	8(%rax), %r8
	movq	%rax, %rsi
	movl	%r10d, %r9d
	.p2align 4,,10
	.p2align 3
.L1040:
	leaq	(%r9,%r9,2), %rdx
	addq	$24, %rax
	leaq	(%rcx,%rdx,8), %rdx
	cmpq	%rdx, %rax
	cmove	%rcx, %rax
	cmpq	$0, (%rax)
	je	.L1036
	leal	-1(%r10), %edx
	andl	16(%rax), %edx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rcx,%rdx,8), %rdx
	cmpq	%rax, %rsi
	jnb	.L1037
	cmpq	%rdx, %rsi
	jnb	.L1038
	cmpq	%rdx, %rax
	jnb	.L1040
.L1038:
	movdqu	(%rax), %xmm0
	movups	%xmm0, (%rsi)
	movl	16(%rax), %edx
	movl	%edx, 16(%rsi)
	movl	8(%rdi), %r9d
	movq	%rax, %rsi
	movq	(%rdi), %rcx
	movq	%r9, %r10
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1037:
	jbe	.L1040
	cmpq	%rdx, %rsi
	jb	.L1040
	cmpq	%rdx, %rax
	jb	.L1038
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1036:
	movq	$0, (%rsi)
	subl	$1, 12(%rdi)
.L1031:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE22527:
	.size	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6RemoveERKS2_j, .-_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6RemoveERKS2_j
	.section	.text.unlikely._ZN2v88internal14HeapObjectsMap17RemoveDeadEntriesEv,"ax",@progbits
	.align 2
.LCOLDB119:
	.section	.text._ZN2v88internal14HeapObjectsMap17RemoveDeadEntriesEv,"ax",@progbits
.LHOTB119:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14HeapObjectsMap17RemoveDeadEntriesEv
	.type	_ZN2v88internal14HeapObjectsMap17RemoveDeadEntriesEv, @function
_ZN2v88internal14HeapObjectsMap17RemoveDeadEntriesEv:
.LFB19882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movabsq	$-6148914691236517205, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	32(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	%rax, %rdx
	subq	%rsi, %rdx
	sarq	$3, %rdx
	imulq	%r14, %rdx
	cmpq	$1, %rdx
	jbe	.L1065
	movl	$1, %ebx
	movl	$1, %r12d
	.p2align 4,,10
	.p2align 3
.L1062:
	leaq	(%rbx,%rbx,2), %rcx
	leaq	(%rsi,%rcx,8), %rcx
	cmpb	$0, 20(%rcx)
	je	.L1054
	cmpq	%rbx, %r12
	je	.L1055
	cmpq	%rdx, %r12
	jnb	.L1076
	movdqu	(%rcx), %xmm0
	leaq	(%r12,%r12,2), %rax
	leaq	(%rsi,%rax,8), %rax
	movups	%xmm0, (%rax)
	movl	16(%rcx), %edx
	movl	%edx, 16(%rax)
	movzbl	20(%rcx), %edx
	movb	%dl, 20(%rax)
	movq	32(%r13), %rsi
	movq	40(%r13), %rax
.L1055:
	subq	%rsi, %rax
	sarq	$3, %rax
	imulq	%r14, %rax
	cmpq	%r12, %rax
	jbe	.L1077
	leaq	(%r12,%r12,2), %rax
	movb	$0, 20(%rsi,%rax,8)
	movq	8(%rcx), %rsi
	movl	16(%r13), %ecx
	movq	8(%r13), %rdi
	movl	%esi, %eax
	sall	$15, %eax
	leal	-1(%rcx), %r8d
	subl	%esi, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	%r8d, %eax
	andl	$1073741823, %eax
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1078:
	addq	$1, %rax
	andq	%r8, %rax
.L1075:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L1058
	cmpq	%rdx, %rsi
	jne	.L1078
	movq	%r12, 8(%rcx)
	movq	32(%r13), %rsi
	addq	$1, %r12
	movq	40(%r13), %rax
.L1061:
	movq	%rax, %rdx
	addq	$1, %rbx
	subq	%rsi, %rdx
	sarq	$3, %rdx
	imulq	%r14, %rdx
	cmpq	%rbx, %rdx
	ja	.L1062
	leaq	(%r12,%r12,2), %rdx
	salq	$3, %rdx
.L1053:
	addq	%rsi, %rdx
	cmpq	%rax, %rdx
	je	.L1052
	movq	%rdx, 40(%r13)
.L1052:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1079
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1054:
	.cfi_restore_state
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1061
	movl	%ecx, %eax
	leaq	-48(%rbp), %rsi
	leaq	8(%r13), %rdi
	movq	%rcx, -48(%rbp)
	sall	$15, %eax
	subl	%ecx, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %edx
	movl	%edx, %eax
	shrl	$4, %eax
	xorl	%eax, %edx
	imull	$2057, %edx, %edx
	movl	%edx, %eax
	shrl	$16, %eax
	xorl	%eax, %edx
	andl	$1073741823, %edx
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6RemoveERKS2_j
	movq	32(%r13), %rsi
	movq	40(%r13), %rax
	jmp	.L1061
.L1065:
	movl	$24, %edx
	jmp	.L1053
.L1077:
	movq	%rax, %rdx
.L1076:
	movq	%r12, %rsi
	leaq	.LC36(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1079:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal14HeapObjectsMap17RemoveDeadEntriesEv
	.cfi_startproc
	.type	_ZN2v88internal14HeapObjectsMap17RemoveDeadEntriesEv.cold, @function
_ZN2v88internal14HeapObjectsMap17RemoveDeadEntriesEv.cold:
.LFSB19882:
.L1058:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	%r12, 8
	ud2
	.cfi_endproc
.LFE19882:
	.section	.text._ZN2v88internal14HeapObjectsMap17RemoveDeadEntriesEv
	.size	_ZN2v88internal14HeapObjectsMap17RemoveDeadEntriesEv, .-_ZN2v88internal14HeapObjectsMap17RemoveDeadEntriesEv
	.section	.text.unlikely._ZN2v88internal14HeapObjectsMap17RemoveDeadEntriesEv
	.size	_ZN2v88internal14HeapObjectsMap17RemoveDeadEntriesEv.cold, .-_ZN2v88internal14HeapObjectsMap17RemoveDeadEntriesEv.cold
.LCOLDE119:
	.section	.text._ZN2v88internal14HeapObjectsMap17RemoveDeadEntriesEv
.LHOTE119:
	.section	.rodata._ZNSt6vectorIPN2v88internal13HeapGraphEdgeESaIS3_EE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC120:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIPN2v88internal13HeapGraphEdgeESaIS3_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIPN2v88internal13HeapGraphEdgeESaIS3_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal13HeapGraphEdgeESaIS3_EE17_M_default_appendEm
	.type	_ZNSt6vectorIPN2v88internal13HeapGraphEdgeESaIS3_EE17_M_default_appendEm, @function
_ZNSt6vectorIPN2v88internal13HeapGraphEdgeESaIS3_EE17_M_default_appendEm:
.LFB24125:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1099
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%r12), %rax
	movq	%rcx, %rsi
	subq	(%rdi), %rsi
	subq	%rcx, %rax
	movq	%rdx, %rdi
	movq	%rsi, %r13
	sarq	$3, %rax
	sarq	$3, %r13
	subq	%r13, %rdi
	cmpq	%rbx, %rax
	jb	.L1082
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1099:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1082:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L1102
	cmpq	%rbx, %r13
	movq	%rbx, %r14
	movq	%rsi, -56(%rbp)
	cmovnb	%r13, %r14
	addq	%r13, %r14
	cmpq	%rdx, %r14
	cmova	%rdx, %r14
	salq	$3, %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	leaq	0(,%rbx,8), %rdx
	movq	%rax, %r15
	leaq	(%rax,%rsi), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%r12), %r8
	movq	8(%r12), %rdx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L1103
	testq	%r8, %r8
	jne	.L1086
.L1087:
	addq	%r13, %rbx
	addq	%r15, %r14
	movq	%r15, (%r12)
	leaq	(%r15,%rbx,8), %rax
	movq	%r14, 16(%r12)
	movq	%rax, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1103:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L1086:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L1087
.L1102:
	leaq	.LC120(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24125:
	.size	_ZNSt6vectorIPN2v88internal13HeapGraphEdgeESaIS3_EE17_M_default_appendEm, .-_ZNSt6vectorIPN2v88internal13HeapGraphEdgeESaIS3_EE17_M_default_appendEm
	.section	.text._ZN2v88internal12HeapSnapshot12FillChildrenEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapSnapshot12FillChildrenEv
	.type	_ZN2v88internal12HeapSnapshot12FillChildrenEv, @function
_ZN2v88internal12HeapSnapshot12FillChildrenEv:
.LFB19836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	232(%rdi), %rax
	movq	248(%rdi), %rsi
	movq	256(%rdi), %r8
	movq	264(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L1106
	.p2align 4,,10
	.p2align 3
.L1105:
	movl	%edx, %ecx
	addq	$40, %rax
	addl	-36(%rax), %edx
	movl	%ecx, -36(%rax)
	cmpq	%rsi, %rax
	je	.L1123
	cmpq	%rax, %rdi
	jne	.L1105
.L1106:
	movq	336(%rbx), %r11
	movq	368(%rbx), %rax
	movabsq	$-6148914691236517205, %rdi
	movq	344(%rbx), %r9
	movq	328(%rbx), %r8
	subq	%r11, %rax
	movq	312(%rbx), %rsi
	sarq	$3, %rax
	subq	$1, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rcx
	movq	%r9, %rdx
	subq	352(%rbx), %rdx
	movq	%r8, %rax
	sarq	$3, %rdx
	subq	%rsi, %rax
	imulq	%rdi, %rdx
	sarq	$3, %rax
	imulq	%rdi, %rax
	movq	376(%rbx), %rdi
	addq	%rcx, %rdx
	movq	384(%rbx), %rcx
	addq	%rdx, %rax
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	ja	.L1124
	jb	.L1125
.L1110:
	movabsq	$-3689348814741910323, %r10
	movabsq	$-6148914691236517205, %r12
	movabsq	$3074457345618258603, %rbx
	.p2align 4,,10
	.p2align 3
.L1116:
	cmpq	%r9, %rsi
	je	.L1104
.L1126:
	movq	8(%rsi), %rdx
	movl	(%rsi), %eax
	movq	16(%rdx), %rdi
	shrl	$3, %eax
	movq	232(%rdi), %rdx
	movq	%rdx, %rcx
	subq	240(%rdi), %rcx
	sarq	$3, %rcx
	imulq	%r10, %rcx
	addq	%rax, %rcx
	js	.L1112
	cmpq	$11, %rcx
	jg	.L1113
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdx,%rax,8), %rdx
.L1114:
	movl	4(%rdx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 4(%rdx)
	movq	16(%rdx), %rdx
	movq	376(%rdx), %rdx
	movq	%rsi, (%rdx,%rax,8)
	addq	$24, %rsi
	cmpq	%rsi, %r8
	jne	.L1116
	movq	8(%r11), %rsi
	addq	$8, %r11
	leaq	504(%rsi), %r8
	cmpq	%r9, %rsi
	jne	.L1126
.L1104:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1113:
	.cfi_restore_state
	movq	%rcx, %rax
	imulq	%rbx
	movq	%rcx, %rax
	sarq	$63, %rax
	sarq	%rdx
	subq	%rax, %rdx
.L1115:
	leaq	(%rdx,%rdx,2), %rax
	movq	256(%rdi), %rdi
	salq	$2, %rax
	subq	%rax, %rcx
	movq	(%rdi,%rdx,8), %rax
	leaq	(%rcx,%rcx,4), %rcx
	leaq	(%rax,%rcx,8), %rdx
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1112:
	movq	%rcx, %rdx
	notq	%rdx
	movq	%rdx, %rax
	mulq	%r12
	shrq	$3, %rdx
	notq	%rdx
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1123:
	movq	8(%r8), %rax
	leaq	8(%r8), %rcx
	leaq	480(%rax), %rsi
	cmpq	%rax, %rdi
	je	.L1106
	movq	%rcx, %r8
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1125:
	leaq	(%rdi,%rax,8), %rax
	cmpq	%rax, %rcx
	je	.L1110
	movq	%rax, 384(%rbx)
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1124:
	subq	%rdx, %rax
	leaq	376(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZNSt6vectorIPN2v88internal13HeapGraphEdgeESaIS3_EE17_M_default_appendEm
	movq	344(%rbx), %r9
	movq	312(%rbx), %rsi
	movq	328(%rbx), %r8
	movq	336(%rbx), %r11
	jmp	.L1110
	.cfi_endproc
.LFE19836:
	.size	_ZN2v88internal12HeapSnapshot12FillChildrenEv, .-_ZN2v88internal12HeapSnapshot12FillChildrenEv
	.section	.text._ZNSt6vectorIN2v88internal14HeapObjectsMap12TimeIntervalESaIS3_EE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal14HeapObjectsMap12TimeIntervalESaIS3_EE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal14HeapObjectsMap12TimeIntervalESaIS3_EE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal14HeapObjectsMap12TimeIntervalESaIS3_EE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal14HeapObjectsMap12TimeIntervalESaIS3_EE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB24180:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r15
	movabsq	$384307168202282325, %rdi
	movq	%r13, %rax
	subq	%r15, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rdi, %rax
	je	.L1144
	movq	%rsi, %rcx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L1136
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L1145
.L1129:
	movq	%r14, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	addq	%rax, %r14
	leaq	24(%rax), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
.L1135:
	movl	(%rdx), %eax
	addq	%rbx, %rcx
	movq	%rsi, -72(%rbp)
	movq	$0, 4(%rcx)
	movl	%eax, (%rcx)
	movq	%rcx, -64(%rbp)
	call	_ZN2v84base9TimeTicks3NowEv@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	%rax, 16(%rcx)
	cmpq	%r15, %rsi
	je	.L1131
	movq	%rbx, %rdx
	movq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L1132:
	movdqu	(%rax), %xmm1
	addq	$24, %rax
	addq	$24, %rdx
	movups	%xmm1, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rsi
	jne	.L1132
	leaq	-24(%rsi), %rax
	subq	%r15, %rax
	shrq	$3, %rax
	leaq	48(%rbx,%rax,8), %rax
	movq	%rax, -56(%rbp)
.L1131:
	cmpq	%r13, %rsi
	je	.L1133
	subq	%rsi, %r13
	movq	-56(%rbp), %rdi
	leaq	-24(%r13), %rax
	shrq	$3, %rax
	leaq	24(,%rax,8), %r13
	movq	%r13, %rdx
	call	memcpy@PLT
	addq	%r13, -56(%rbp)
.L1133:
	testq	%r15, %r15
	je	.L1134
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1134:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1145:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L1130
	movq	$24, -56(%rbp)
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1135
	.p2align 4,,10
	.p2align 3
.L1136:
	movl	$24, %r14d
	jmp	.L1129
.L1130:
	cmpq	%rdi, %r9
	cmovbe	%r9, %rdi
	imulq	$24, %rdi, %r14
	jmp	.L1129
.L1144:
	leaq	.LC31(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24180:
	.size	_ZNSt6vectorIN2v88internal14HeapObjectsMap12TimeIntervalESaIS3_EE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal14HeapObjectsMap12TimeIntervalESaIS3_EE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_14JSGlobalObjectEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_14JSGlobalObjectEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_14JSGlobalObjectEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_14JSGlobalObjectEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal6HandleINS1_14JSGlobalObjectEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_:
.LFB24297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L1147
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	addq	$8, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1147:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rcx
	movq	(%rdi), %r14
	movq	%r12, %rdx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1173
	testq	%rax, %rax
	je	.L1158
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1174
.L1150:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	8(%rax), %rax
.L1151:
	movq	(%rsi), %rcx
	movq	%rcx, 0(%r13,%rdx)
	cmpq	%r14, %r12
	je	.L1152
	leaq	-8(%r12), %rsi
	leaq	15(%r13), %rax
	subq	%r14, %rsi
	subq	%r14, %rax
	movq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L1161
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L1161
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L1154:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L1154
	movq	%rcx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r14,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%rdi, %rcx
	je	.L1156
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L1156:
	leaq	16(%r13,%rsi), %rax
.L1152:
	testq	%r14, %r14
	je	.L1157
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L1157:
	movq	%r13, %xmm0
	movq	%rax, %xmm2
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1174:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L1175
	movl	$8, %eax
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1158:
	movl	$8, %r15d
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1161:
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L1153:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L1153
	jmp	.L1156
.L1173:
	leaq	.LC31(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1175:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$3, %r15
	jmp	.L1150
	.cfi_endproc
.LFE24297:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_14JSGlobalObjectEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal6HandleINS1_14JSGlobalObjectEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_
	.section	.text._ZN2v88internal23GlobalObjectsEnumerator17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_,"axG",@progbits,_ZN2v88internal23GlobalObjectsEnumerator17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23GlobalObjectsEnumerator17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.type	_ZN2v88internal23GlobalObjectsEnumerator17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_, @function
_ZN2v88internal23GlobalObjectsEnumerator17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_:
.LFB20048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r8, %rcx
	jnb	.L1176
	movq	%rdi, %r12
	movq	%r8, %r13
	leaq	-64(%rbp), %r14
	movq	%rcx, %rbx
	jmp	.L1184
	.p2align 4,,10
	.p2align 3
.L1179:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jbe	.L1176
.L1184:
	movq	(%rbx), %rax
	testb	$1, %al
	je	.L1179
	movq	-1(%rax), %rdx
	cmpw	$145, 11(%rdx)
	jne	.L1179
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context12global_proxyEv@PLT
	movq	-1(%rax), %rdx
	cmpw	$1026, 11(%rdx)
	jne	.L1179
	movq	-1(%rax), %rdx
	movq	23(%rdx), %rsi
	testb	$1, %sil
	je	.L1179
	movq	-1(%rsi), %rdx
	cmpw	$1025, 11(%rdx)
	jne	.L1179
	andq	$-262144, %rax
	leaq	8(%r12), %r15
	movq	24(%rax), %rdx
	movq	3520(%rdx), %rdi
	subq	$37592, %rdx
	testq	%rdi, %rdi
	je	.L1181
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1182:
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNSt6vectorIN2v88internal6HandleINS1_14JSGlobalObjectEEESaIS4_EE12emplace_backIJS4_EEEvDpOT_
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1176:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1188
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1181:
	.cfi_restore_state
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1189
.L1183:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1182
.L1189:
	movq	%rdx, %rdi
	movq	%rsi, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	jmp	.L1183
.L1188:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20048:
	.size	_ZN2v88internal23GlobalObjectsEnumerator17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_, .-_ZN2v88internal23GlobalObjectsEnumerator17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.section	.text._ZNSt6vectorIN2v88internal14HeapObjectsMap9EntryInfoESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal14HeapObjectsMap9EntryInfoESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal14HeapObjectsMap9EntryInfoESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal14HeapObjectsMap9EntryInfoESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal14HeapObjectsMap9EntryInfoESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB25086:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r14
	movq	%rsi, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movabsq	$384307168202282325, %rcx
	cmpq	%rcx, %rax
	je	.L1207
	movq	%r12, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L1199
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1208
.L1192:
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r8
	movq	%rax, %r13
	leaq	(%rax,%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	24(%r13), %rbx
.L1198:
	movdqu	(%rdx), %xmm2
	movq	16(%rdx), %rax
	movups	%xmm2, 0(%r13,%r8)
	movq	%rax, 16(%r13,%r8)
	cmpq	%r14, %r12
	je	.L1194
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L1195:
	movdqu	(%rax), %xmm1
	addq	$24, %rax
	addq	$24, %rdx
	movups	%xmm1, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L1195
	leaq	-24(%r12), %rax
	subq	%r14, %rax
	shrq	$3, %rax
	leaq	48(%r13,%rax,8), %rbx
.L1194:
	cmpq	%rsi, %r12
	je	.L1196
	subq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-24(%rsi), %rax
	movq	%r12, %rsi
	shrq	$3, %rax
	leaq	24(,%rax,8), %rdx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, %rbx
.L1196:
	testq	%r14, %r14
	je	.L1197
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1197:
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rbx, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1208:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L1193
	movq	$0, -56(%rbp)
	movl	$24, %ebx
	xorl	%r13d, %r13d
	jmp	.L1198
	.p2align 4,,10
	.p2align 3
.L1199:
	movl	$24, %ebx
	jmp	.L1192
.L1193:
	cmpq	%rcx, %rdi
	movq	%rcx, %rbx
	cmovbe	%rdi, %rbx
	imulq	$24, %rbx, %rbx
	jmp	.L1192
.L1207:
	leaq	.LC31(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE25086:
	.size	_ZNSt6vectorIN2v88internal14HeapObjectsMap9EntryInfoESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal14HeapObjectsMap9EntryInfoESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.rodata._ZNSt6vectorIbSaIbEE14_M_fill_insertESt13_Bit_iteratormb.str1.1,"aMS",@progbits,1
.LC121:
	.string	"vector<bool>::_M_fill_insert"
	.section	.text._ZNSt6vectorIbSaIbEE14_M_fill_insertESt13_Bit_iteratormb,"axG",@progbits,_ZNSt6vectorIbSaIbEE14_M_fill_insertESt13_Bit_iteratormb,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIbSaIbEE14_M_fill_insertESt13_Bit_iteratormb
	.type	_ZNSt6vectorIbSaIbEE14_M_fill_insertESt13_Bit_iteratormb, @function
_ZNSt6vectorIbSaIbEE14_M_fill_insertESt13_Bit_iteratormb:
.LFB25152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	testq	%rcx, %rcx
	je	.L1209
	movq	%rdi, %rbx
	movq	(%rdi), %rax
	movq	%rsi, -80(%rbp)
	movq	%rsi, %r12
	movq	16(%rbx), %r9
	movq	32(%rbx), %rsi
	movq	%rcx, %r13
	movl	24(%rdi), %edx
	movl	-56(%rbp), %r14d
	movq	%r9, %rcx
	subq	%rax, %rsi
	subq	%rax, %rcx
	movq	%rsi, %rax
	movq	%rdx, %rdi
	leaq	(%rdx,%rcx,8), %rcx
	salq	$3, %rax
	subq	%rcx, %rax
	cmpq	%r13, %rax
	jb	.L1211
	leaq	0(%r13,%rdx), %rax
	testq	%rax, %rax
	leaq	63(%rax), %rcx
	cmovns	%rax, %rcx
	sarq	$6, %rcx
	leaq	(%r9,%rcx,8), %r8
	movq	%rax, %rcx
	sarq	$63, %rcx
	shrq	$58, %rcx
	addq	%rcx, %rax
	andl	$63, %eax
	subq	%rcx, %rax
	jns	.L1212
	addq	$64, %rax
	subq	$8, %r8
.L1212:
	movabsq	$-9223372036854775808, %r15
	movq	%r9, %rcx
	movl	%r14d, %r11d
	subq	%r12, %rcx
	movq	%r11, -88(%rbp)
	leaq	(%rdx,%rcx,8), %rsi
	subq	%r11, %rsi
	movl	$1, %r11d
	testq	%rsi, %rsi
	jle	.L1221
	.p2align 4,,10
	.p2align 3
.L1213:
	testl	%edi, %edi
	je	.L1216
.L1293:
	subl	$1, %edi
	movq	%r11, %r10
	movl	%edi, %ecx
	salq	%cl, %r10
	testl	%eax, %eax
	je	.L1218
.L1294:
	subl	$1, %eax
	movq	%r11, %rdx
	movl	%eax, %ecx
	salq	%cl, %rdx
.L1219:
	movq	(%r8), %rcx
	testq	%r10, (%r9)
	je	.L1220
	orq	%rcx, %rdx
	movq	%rdx, (%r8)
	subq	$1, %rsi
	jne	.L1213
.L1221:
	movq	-88(%rbp), %rax
	addq	%r13, %rax
	leaq	63(%rax), %rdx
	cmovns	%rax, %rdx
	sarq	$6, %rdx
	leaq	(%r12,%rdx,8), %r8
	cqto
	shrq	$58, %rdx
	leaq	(%rax,%rdx), %r15
	andl	$63, %r15d
	subq	%rdx, %r15
	jns	.L1215
	addq	$64, %r15
	subq	$8, %r8
.L1215:
	movl	%r15d, %r9d
	cmpq	%r12, %r8
	je	.L1223
	testl	%r14d, %r14d
	jne	.L1288
	movq	%r8, %r10
	subq	%r12, %r10
	cmpb	$0, -72(%rbp)
	je	.L1227
.L1226:
	movq	-80(%rbp), %rdi
	movq	%r10, %rdx
	movl	$-1, %esi
	movl	%r9d, -88(%rbp)
	movq	%r8, -72(%rbp)
	call	memset@PLT
	testq	%r15, %r15
	movq	-72(%rbp), %r8
	movl	-88(%rbp), %r9d
	je	.L1229
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r9d, %ecx
	shrq	%cl, %rax
	orq	%rax, (%r8)
	.p2align 4,,10
	.p2align 3
.L1229:
	movl	24(%rbx), %ecx
	movq	16(%rbx), %rdx
	addq	%rcx, %r13
	leaq	63(%r13), %rax
	cmovns	%r13, %rax
	sarq	$6, %rax
	leaq	(%rdx,%rax,8), %rdx
	movq	%r13, %rax
	sarq	$63, %rax
	shrq	$58, %rax
	addq	%rax, %r13
	andl	$63, %r13d
	subq	%rax, %r13
	jns	.L1286
	addq	$64, %r13
	subq	$8, %rdx
.L1286:
	movq	%rdx, 16(%rbx)
	movl	%r13d, 24(%rbx)
.L1209:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1211:
	.cfi_restore_state
	movabsq	$9223372036854775744, %rax
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %r13
	ja	.L1289
	cmpq	%rcx, %r13
	movq	%rcx, %rdx
	cmovnb	%r13, %rdx
	addq	%rdx, %rcx
	jc	.L1237
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	addq	$63, %rcx
	shrq	$6, %rcx
	leaq	0(,%rcx,8), %rax
	movq	%rax, -88(%rbp)
	movq	%rax, %rdi
.L1238:
	call	_Znwm@PLT
	movq	(%rbx), %rsi
	movq	%r12, %r15
	movq	%rax, -96(%rbp)
	subq	%rsi, %r15
	cmpq	%r12, %rsi
	je	.L1240
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	memmove@PLT
.L1240:
	movq	-96(%rbp), %rax
	movl	%r14d, %esi
	movq	%rsi, -104(%rbp)
	leaq	(%rax,%r15), %rdi
	testq	%rsi, %rsi
	je	.L1268
	xorl	%edx, %edx
	movq	%r12, %r9
	movl	$1, %r10d
	movl	%edx, %ecx
	jmp	.L1246
	.p2align 4,,10
	.p2align 3
.L1244:
	addl	$1, %ecx
	subq	$1, %rsi
	je	.L1290
.L1246:
	movq	%r10, %rdx
	movq	(%rdi), %r8
	salq	%cl, %rdx
	movq	%rdx, %rax
	movq	%r8, %r11
	notq	%rax
	orq	%rdx, %r11
	andq	%r8, %rax
	testq	%rdx, (%r9)
	cmovne	%r11, %rax
	movq	%rax, (%rdi)
	cmpl	$63, %ecx
	jne	.L1244
	addq	$8, %r9
	addq	$8, %rdi
	xorl	%ecx, %ecx
	subq	$1, %rsi
	jne	.L1246
.L1290:
	movl	%ecx, %ecx
	movq	%rcx, %rdx
.L1241:
	addq	%r13, %rcx
	leaq	63(%rcx), %rax
	cmovns	%rcx, %rax
	sarq	$6, %rax
	leaq	(%rdi,%rax,8), %r13
	movq	%rcx, %rax
	sarq	$63, %rax
	shrq	$58, %rax
	leaq	(%rcx,%rax), %r8
	andl	$63, %r8d
	subq	%rax, %r8
	jns	.L1247
	addq	$64, %r8
	subq	$8, %r13
.L1247:
	movl	%r8d, %r15d
	cmpq	%r13, %rdi
	je	.L1248
	testl	%edx, %edx
	je	.L1249
	movl	%edx, %ecx
	leaq	8(%rdi), %rsi
	movq	$-1, %rax
	movq	%r13, %rdx
	salq	%cl, %rax
	subq	%rsi, %rdx
	cmpb	$0, -72(%rbp)
	movq	(%rdi), %rcx
	je	.L1250
	orq	%rcx, %rax
	movq	%rax, (%rdi)
	movq	%rsi, %rdi
.L1251:
	movl	$-1, %esi
	movq	%r8, -72(%rbp)
	call	memset@PLT
	movq	-72(%rbp), %r8
	testq	%r8, %r8
	jne	.L1291
	.p2align 4,,10
	.p2align 3
.L1254:
	movq	16(%rbx), %rax
	movl	24(%rbx), %edx
	subq	%r12, %rax
	leaq	(%rdx,%rax,8), %rdx
	subq	-104(%rbp), %rdx
	testq	%rdx, %rdx
	jle	.L1258
	movq	-80(%rbp), %r8
	movl	$1, %edi
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1261:
	addl	$1, %r14d
	cmpl	$63, %r15d
	je	.L1292
.L1263:
	addl	$1, %r15d
	subq	$1, %rdx
	je	.L1258
.L1265:
	movq	0(%r13), %rsi
	movl	%r15d, %ecx
	movq	%rdi, %rax
	movq	%rdi, %r9
	salq	%cl, %rax
	movl	%r14d, %ecx
	salq	%cl, %r9
	movq	%rsi, %rcx
	orq	%rax, %rcx
	notq	%rax
	andq	%rsi, %rax
	testq	%r9, (%r8)
	cmovne	%rcx, %rax
	movq	%rax, 0(%r13)
	cmpl	$63, %r14d
	jne	.L1261
	addq	$8, %r8
	xorl	%r14d, %r14d
	cmpl	$63, %r15d
	jne	.L1263
.L1292:
	addq	$8, %r13
	xorl	%r15d, %r15d
	subq	$1, %rdx
	jne	.L1265
.L1258:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1266
	call	_ZdlPv@PLT
	movq	$0, 16(%rbx)
	movl	$0, 24(%rbx)
	movq	$0, (%rbx)
	movl	$0, 8(%rbx)
.L1266:
	movq	-96(%rbp), %rdi
	movq	-88(%rbp), %rax
	movl	$0, 8(%rbx)
	movq	%r13, 16(%rbx)
	addq	%rdi, %rax
	movq	%rdi, (%rbx)
	movq	%rax, 32(%rbx)
	movl	%r15d, 24(%rbx)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1220:
	.cfi_restore_state
	notq	%rdx
	andq	%rcx, %rdx
	movq	%rdx, (%r8)
	subq	$1, %rsi
	je	.L1221
	testl	%edi, %edi
	jne	.L1293
.L1216:
	subq	$8, %r9
	movq	%r15, %r10
	movl	$63, %edi
	testl	%eax, %eax
	jne	.L1294
.L1218:
	subq	$8, %r8
	movq	%r15, %rdx
	movl	$63, %eax
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1288:
	movl	%r14d, %ecx
	leaq	8(%r12), %rdx
	movq	$-1, %rax
	movq	%r8, %r10
	salq	%cl, %rax
	subq	%rdx, %r10
	cmpb	$0, -72(%rbp)
	movq	(%r12), %rcx
	je	.L1225
	orq	%rcx, %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, (%r12)
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1225:
	notq	%rax
	movq	%rdx, -80(%rbp)
	andq	%rcx, %rax
	movq	%rax, (%r12)
.L1227:
	movq	-80(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r10, %rdx
	movl	%r9d, -88(%rbp)
	movq	%r8, -72(%rbp)
	call	memset@PLT
	testq	%r15, %r15
	movq	-72(%rbp), %r8
	movl	-88(%rbp), %r9d
	je	.L1229
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r9d, %ecx
	shrq	%cl, %rax
	notq	%rax
	andq	%rax, (%r8)
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1223:
	cmpl	%r14d, %r15d
	je	.L1229
	movq	$-1, %rdx
	movl	$64, %ecx
	subl	%r15d, %ecx
	movq	%rdx, %rax
	shrq	%cl, %rax
	movl	%r14d, %ecx
	salq	%cl, %rdx
	movq	(%r8), %rcx
	andq	%rdx, %rax
	movq	%rax, %rdx
	orq	%rcx, %rax
	notq	%rdx
	andq	%rcx, %rdx
	cmpb	$0, -72(%rbp)
	cmove	%rdx, %rax
	movq	%rax, (%r8)
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1249:
	movq	%r13, %rdx
	subq	%rdi, %rdx
	cmpb	$0, -72(%rbp)
	jne	.L1251
.L1252:
	xorl	%esi, %esi
	movq	%r8, -72(%rbp)
	call	memset@PLT
	movq	-72(%rbp), %r8
	testq	%r8, %r8
	je	.L1254
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r15d, %ecx
	shrq	%cl, %rax
	notq	%rax
	andq	%rax, 0(%r13)
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1248:
	cmpl	%edx, %r8d
	je	.L1254
	movq	$-1, %rsi
	movl	$64, %ecx
	subl	%r8d, %ecx
	movq	%rsi, %rax
	shrq	%cl, %rax
	movl	%edx, %ecx
	salq	%cl, %rsi
	movq	%rsi, %rdx
	andq	%rax, %rdx
	movq	0(%r13), %rax
	movq	%rdx, %rcx
	notq	%rcx
	andq	%rax, %rcx
	orq	%rdx, %rax
	cmpb	$0, -72(%rbp)
	cmove	%rcx, %rax
	movq	%rax, 0(%r13)
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1291:
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r15d, %ecx
	shrq	%cl, %rax
	orq	%rax, 0(%r13)
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1250:
	notq	%rax
	andq	%rcx, %rax
	movq	%rax, (%rdi)
	movq	%rsi, %rdi
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1268:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.L1241
.L1289:
	leaq	.LC121(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1237:
	movabsq	$1152921504606846968, %rax
	movq	%rax, -88(%rbp)
	movq	%rax, %rdi
	jmp	.L1238
	.cfi_endproc
.LFE25152:
	.size	_ZNSt6vectorIbSaIbEE14_M_fill_insertESt13_Bit_iteratormb, .-_ZNSt6vectorIbSaIbEE14_M_fill_insertESt13_Bit_iteratormb
	.section	.text._ZNSt10_HashtableIN2v88internal14JSGlobalObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal14JSGlobalObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal14JSGlobalObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.type	_ZNSt10_HashtableIN2v88internal14JSGlobalObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, @function
_ZNSt10_HashtableIN2v88internal14JSGlobalObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm:
.LFB25166:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %r13
	movq	8(%rdi), %r8
	movq	%r13, %rax
	divq	%r8
	movq	(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r15
	testq	%rax, %rax
	je	.L1296
	movq	(%rax), %rdi
	movq	%rdx, %rsi
	movq	16(%rdi), %r9
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1297:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1296
	movq	16(%rdi), %r9
	xorl	%edx, %edx
	movq	%r9, %rax
	divq	%r8
	cmpq	%rdx, %rsi
	jne	.L1296
.L1299:
	cmpq	%r9, %r13
	jne	.L1297
	cmpq	8(%rdi), %r13
	jne	.L1297
	addq	$24, %rsp
	movq	%rdi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1296:
	.cfi_restore_state
	movl	$24, %edi
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	24(%r12), %rdx
	movq	8(%r12), %rsi
	leaq	32(%r12), %rdi
	movq	$0, (%rax)
	movq	%rax, %rbx
	movq	-56(%rbp), %rcx
	movq	(%r14), %rax
	movq	%rax, 8(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L1300
	movq	(%r12), %r8
	movq	%r13, 16(%rbx)
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1310
.L1335:
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	(%rax), %rax
	movq	%rbx, (%rax)
.L1311:
	addq	$1, 24(%r12)
	addq	$24, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1300:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L1333
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1334
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r12), %r10
	movq	%rax, %r8
.L1303:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L1305
	xorl	%edi, %edi
	leaq	16(%r12), %r9
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1307:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1308:
	testq	%rsi, %rsi
	je	.L1305
.L1306:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	16(%rcx), %rax
	divq	%r14
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L1307
	movq	16(%r12), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L1314
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1306
	.p2align 4,,10
	.p2align 3
.L1305:
	movq	(%r12), %rdi
	cmpq	%r10, %rdi
	je	.L1309
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1309:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r14, 8(%r12)
	divq	%r14
	movq	%r8, (%r12)
	movq	%r13, 16(%rbx)
	leaq	0(,%rdx,8), %r15
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L1335
.L1310:
	movq	16(%r12), %rdx
	movq	%rbx, 16(%r12)
	movq	%rdx, (%rbx)
	testq	%rdx, %rdx
	je	.L1312
	movq	16(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	%rbx, (%r8,%rdx,8)
	movq	(%r12), %rax
	addq	%r15, %rax
.L1312:
	leaq	16(%r12), %rdx
	movq	%rdx, (%rax)
	jmp	.L1311
	.p2align 4,,10
	.p2align 3
.L1314:
	movq	%rdx, %rdi
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1333:
	movq	$0, 48(%r12)
	leaq	48(%r12), %r8
	movq	%r8, %r10
	jmp	.L1303
.L1334:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE25166:
	.size	_ZNSt10_HashtableIN2v88internal14JSGlobalObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, .-_ZNSt10_HashtableIN2v88internal14JSGlobalObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_:
.LFB25249:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1360
	movq	%rsi, %r9
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L1351
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1361
.L1338:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rcx
	movq	%r13, -64(%rbp)
.L1350:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rsi
	movq	$0, (%rdx)
	movq	%rax, (%rsi,%r9)
	cmpq	%r12, %r15
	je	.L1340
	movq	%rsi, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L1344:
	movq	(%r14), %rcx
	movq	$0, (%r14)
	movq	%rcx, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1341
	movq	(%rdi), %rcx
	addq	$8, %r14
	addq	$8, %r13
	call	*8(%rcx)
	cmpq	%r14, %r15
	jne	.L1344
.L1342:
	movq	-56(%rbp), %rsi
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rsi,%rax), %rcx
.L1340:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L1345
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L1353
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1347:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1347
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L1348
.L1346:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L1348:
	leaq	8(%rcx,%r9), %rcx
.L1345:
	testq	%r12, %r12
	je	.L1349
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L1349:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm2
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1341:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L1344
	jmp	.L1342
	.p2align 4,,10
	.p2align 3
.L1361:
	testq	%rdi, %rdi
	jne	.L1339
	movq	$0, -64(%rbp)
	movl	$8, %ecx
	movq	$0, -56(%rbp)
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1351:
	movl	$8, %r13d
	jmp	.L1338
.L1353:
	movq	%rcx, %rdx
	jmp	.L1346
.L1339:
	cmpq	%rcx, %rdi
	movq	%rcx, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L1338
.L1360:
	leaq	.LC31(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE25249:
	.size	_ZNSt6vectorISt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.section	.text._ZN2v88internal17EmbedderGraphImpl6V8NodeERKNS_5LocalINS_5ValueEEE,"axG",@progbits,_ZN2v88internal17EmbedderGraphImpl6V8NodeERKNS_5LocalINS_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17EmbedderGraphImpl6V8NodeERKNS_5LocalINS_5ValueEEE
	.type	_ZN2v88internal17EmbedderGraphImpl6V8NodeERKNS_5LocalINS_5ValueEEE, @function
_ZN2v88internal17EmbedderGraphImpl6V8NodeERKNS_5LocalINS_5ValueEEE:
.LFB20078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$16, %edi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	(%rax), %r13
	call	_Znwm@PLT
	movq	16(%rbx), %rsi
	movq	%rax, %r12
	leaq	16+_ZTVN2v88internal17EmbedderGraphImpl10V8NodeImplE(%rip), %rax
	movq	%rax, (%r12)
	movq	%r13, 8(%r12)
	movq	%r12, -48(%rbp)
	cmpq	24(%rbx), %rsi
	je	.L1363
	movq	$0, -48(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 16(%rbx)
.L1364:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1362
	movq	(%rdi), %rax
	call	*8(%rax)
.L1362:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1371
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1363:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	leaq	8(%rbx), %rdi
	call	_ZNSt6vectorISt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	jmp	.L1364
.L1371:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20078:
	.size	_ZN2v88internal17EmbedderGraphImpl6V8NodeERKNS_5LocalINS_5ValueEEE, .-_ZN2v88internal17EmbedderGraphImpl6V8NodeERKNS_5LocalINS_5ValueEEE
	.section	.text._ZN2v88internal17EmbedderGraphImpl7AddNodeESt10unique_ptrINS_13EmbedderGraph4NodeESt14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal17EmbedderGraphImpl7AddNodeESt10unique_ptrINS_13EmbedderGraph4NodeESt14default_deleteIS4_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17EmbedderGraphImpl7AddNodeESt10unique_ptrINS_13EmbedderGraph4NodeESt14default_deleteIS4_EE
	.type	_ZN2v88internal17EmbedderGraphImpl7AddNodeESt10unique_ptrINS_13EmbedderGraph4NodeESt14default_deleteIS4_EE, @function
_ZN2v88internal17EmbedderGraphImpl7AddNodeESt10unique_ptrINS_13EmbedderGraph4NodeESt14default_deleteIS4_EE:
.LFB20081:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	%rsi, %rdx
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L1373
	movq	$0, (%rdx)
	movq	%rax, (%rsi)
	addq	$8, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1373:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rax, -8(%rbp)
	call	_ZNSt6vectorISt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20081:
	.size	_ZN2v88internal17EmbedderGraphImpl7AddNodeESt10unique_ptrINS_13EmbedderGraph4NodeESt14default_deleteIS4_EE, .-_ZN2v88internal17EmbedderGraphImpl7AddNodeESt10unique_ptrINS_13EmbedderGraph4NodeESt14default_deleteIS4_EE
	.section	.text._ZNSt6vectorIN2v88internal17EmbedderGraphImpl4EdgeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal17EmbedderGraphImpl4EdgeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal17EmbedderGraphImpl4EdgeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal17EmbedderGraphImpl4EdgeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal17EmbedderGraphImpl4EdgeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB25254:
	.cfi_startproc
	endbr64
	movabsq	$384307168202282325, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$-6148914691236517205, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	(%rdi), %r8
	movq	%r9, %rax
	subq	%r8, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L1392
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r13
	subq	%r8, %rdx
	testq	%rax, %rax
	je	.L1388
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1393
.L1380:
	movq	%r14, %rdi
	movq	%rdx, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %rdx
	movq	%rax, %rbx
	addq	%rax, %r14
.L1387:
	movq	16(%r15), %rax
	movdqu	(%r15), %xmm1
	subq	%r13, %r9
	leaq	24(%rbx,%rdx), %r10
	movq	%r9, %r15
	movq	%rax, 16(%rbx,%rdx)
	leaq	(%r10,%r9), %rax
	movq	%rax, -56(%rbp)
	movups	%xmm1, (%rbx,%rdx)
	testq	%rdx, %rdx
	jg	.L1394
	testq	%r9, %r9
	jg	.L1383
	testq	%r8, %r8
	jne	.L1386
.L1384:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1394:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%rbx, %rdi
	movq	%r10, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	memmove@PLT
	testq	%r15, %r15
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %r10
	jg	.L1383
.L1386:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L1384
	.p2align 4,,10
	.p2align 3
.L1393:
	testq	%rsi, %rsi
	jne	.L1381
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1387
	.p2align 4,,10
	.p2align 3
.L1383:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r8, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	testq	%r8, %r8
	je	.L1384
	jmp	.L1386
	.p2align 4,,10
	.p2align 3
.L1388:
	movl	$24, %r14d
	jmp	.L1380
.L1392:
	leaq	.LC31(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1381:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	imulq	$24, %rcx, %r14
	jmp	.L1380
	.cfi_endproc
.LFE25254:
	.size	_ZNSt6vectorIN2v88internal17EmbedderGraphImpl4EdgeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal17EmbedderGraphImpl4EdgeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZN2v88internal17EmbedderGraphImpl7AddEdgeEPNS_13EmbedderGraph4NodeES4_PKc,"axG",@progbits,_ZN2v88internal17EmbedderGraphImpl7AddEdgeEPNS_13EmbedderGraph4NodeES4_PKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17EmbedderGraphImpl7AddEdgeEPNS_13EmbedderGraph4NodeES4_PKc
	.type	_ZN2v88internal17EmbedderGraphImpl7AddEdgeEPNS_13EmbedderGraph4NodeES4_PKc, @function
_ZN2v88internal17EmbedderGraphImpl7AddEdgeEPNS_13EmbedderGraph4NodeES4_PKc:
.LFB20083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	40(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rcx, -16(%rbp)
	movaps	%xmm0, -32(%rbp)
	cmpq	48(%rdi), %rsi
	je	.L1396
	movdqa	-32(%rbp), %xmm2
	movups	%xmm2, (%rsi)
	movq	-16(%rbp), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 40(%rdi)
.L1395:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1400
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1396:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	addq	$32, %rdi
	call	_ZNSt6vectorIN2v88internal17EmbedderGraphImpl4EdgeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1395
.L1400:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20083:
	.size	_ZN2v88internal17EmbedderGraphImpl7AddEdgeEPNS_13EmbedderGraph4NodeES4_PKc, .-_ZN2v88internal17EmbedderGraphImpl7AddEdgeEPNS_13EmbedderGraph4NodeES4_PKc
	.section	.text._ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	.type	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm, @function
_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm:
.LFB25383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L1402
	movq	(%rbx), %r8
.L1403:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L1412
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L1413:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1402:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L1426
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1427
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L1405:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1407
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L1408
	.p2align 4,,10
	.p2align 3
.L1409:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1410:
	testq	%rsi, %rsi
	je	.L1407
.L1408:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L1409
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L1415
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1408
	.p2align 4,,10
	.p2align 3
.L1407:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L1411
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1411:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L1403
	.p2align 4,,10
	.p2align 3
.L1412:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L1414
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L1414:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L1413
	.p2align 4,,10
	.p2align 3
.L1415:
	movq	%rdx, %rdi
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1426:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L1405
.L1427:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE25383:
	.size	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm, .-_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	.section	.text._ZN2v88internal21HeapSnapshotGenerator14FindOrAddEntryEPvPNS0_20HeapEntriesAllocatorE,"axG",@progbits,_ZN2v88internal21HeapSnapshotGenerator14FindOrAddEntryEPvPNS0_20HeapEntriesAllocatorE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21HeapSnapshotGenerator14FindOrAddEntryEPvPNS0_20HeapEntriesAllocatorE
	.type	_ZN2v88internal21HeapSnapshotGenerator14FindOrAddEntryEPvPNS0_20HeapEntriesAllocatorE, @function
_ZN2v88internal21HeapSnapshotGenerator14FindOrAddEntryEPvPNS0_20HeapEntriesAllocatorE:
.LFB7138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rax
	movq	%rdx, %rdi
	xorl	%edx, %edx
	subq	$8, %rsp
	movq	344(%rbx), %rsi
	divq	%rsi
	movq	336(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L1429
	movq	(%rax), %rcx
	movq	%rdx, %r9
	movq	8(%rcx), %r8
	jmp	.L1431
	.p2align 4,,10
	.p2align 3
.L1455:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1429
	movq	8(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L1429
.L1431:
	cmpq	%r8, %r12
	jne	.L1455
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1429
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1429:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, %r13
	call	_Znwm@PLT
	movq	344(%rbx), %rsi
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%r12, 8(%rax)
	movq	%r13, 16(%rax)
	movq	%r12, %rax
	divq	%rsi
	movq	336(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1432
	movq	(%rax), %r13
	movq	8(%r13), %rcx
	jmp	.L1434
	.p2align 4,,10
	.p2align 3
.L1456:
	movq	0(%r13), %r13
	testq	%r13, %r13
	je	.L1432
	movq	8(%r13), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L1432
.L1434:
	cmpq	%rcx, %r12
	jne	.L1456
	call	_ZdlPv@PLT
.L1435:
	movq	16(%r13), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1432:
	.cfi_restore_state
	leaq	336(%rbx), %r10
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r9, %rsi
	movl	$1, %r8d
	movq	%r10, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %r13
	jmp	.L1435
	.cfi_endproc
.LFE7138:
	.size	_ZN2v88internal21HeapSnapshotGenerator14FindOrAddEntryEPvPNS0_20HeapEntriesAllocatorE, .-_ZN2v88internal21HeapSnapshotGenerator14FindOrAddEntryEPvPNS0_20HeapEntriesAllocatorE
	.section	.text.unlikely._ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc,"ax",@progbits
	.align 2
.LCOLDB122:
	.section	.text._ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc,"ax",@progbits
.LHOTB122:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	.type	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc, @function
_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc:
.LFB20047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	testb	%al, %al
	je	.L1457
	testb	$1, %sil
	je	.L1460
	movq	%rdi, %r8
	movq	48(%rdi), %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal21HeapSnapshotGenerator14FindOrAddEntryEPvPNS0_20HeapEntriesAllocatorE
	movq	24(%rax), %rdx
	cmpb	$0, (%rdx)
	jne	.L1457
	movq	%rbx, 24(%rax)
.L1457:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	.cfi_startproc
	.type	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.cold, @function
_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.cold:
.LFSB20047:
.L1460:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	24, %rax
	ud2
	.cfi_endproc
.LFE20047:
	.section	.text._ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	.size	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc, .-_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	.section	.text.unlikely._ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	.size	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.cold, .-_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.cold
.LCOLDE122:
	.section	.text._ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
.LHOTE122:
	.section	.rodata._ZN2v88internal14V8HeapExplorer20TagBuiltinCodeObjectENS0_4CodeEPKc.str1.1,"aMS",@progbits,1
.LC123:
	.string	"(%s builtin)"
	.section	.text._ZN2v88internal14V8HeapExplorer20TagBuiltinCodeObjectENS0_4CodeEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer20TagBuiltinCodeObjectENS0_4CodeEPKc
	.type	_ZN2v88internal14V8HeapExplorer20TagBuiltinCodeObjectENS0_4CodeEPKc, @function
_ZN2v88internal14V8HeapExplorer20TagBuiltinCodeObjectENS0_4CodeEPKc:
.LFB19995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC123(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	24(%rdi), %rdi
	call	_ZN2v88internal14StringsStorage12GetFormattedEPKcz@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	.cfi_endproc
.LFE19995:
	.size	_ZN2v88internal14V8HeapExplorer20TagBuiltinCodeObjectENS0_4CodeEPKc, .-_ZN2v88internal14V8HeapExplorer20TagBuiltinCodeObjectENS0_4CodeEPKc
	.section	.text._ZN2v88internal21NativeObjectsExplorer25EntryForEmbedderGraphNodeEPNS_13EmbedderGraph4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21NativeObjectsExplorer25EntryForEmbedderGraphNodeEPNS_13EmbedderGraph4NodeE
	.type	_ZN2v88internal21NativeObjectsExplorer25EntryForEmbedderGraphNodeEPNS_13EmbedderGraph4NodeE, @function
_ZN2v88internal21NativeObjectsExplorer25EntryForEmbedderGraphNodeEPNS_13EmbedderGraph4NodeE:
.LFB20096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v813EmbedderGraph4Node11WrapperNodeEv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rsi), %rdx
	movq	%rdi, %rbx
	movq	32(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L1481
.L1466:
	movq	%r12, %rdi
	call	*48(%rdx)
	testb	%al, %al
	jne	.L1482
	movq	8(%r12), %rdi
	xorl	%r8d, %r8d
	testb	$1, %dil
	je	.L1465
	movq	32(%rbx), %rcx
	movq	%rdi, %rax
	xorl	%edx, %edx
	movq	344(%rcx), %rsi
	divq	%rsi
	movq	336(%rcx), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r9
	testq	%r8, %r8
	je	.L1465
	movq	(%r8), %r8
	movq	8(%r8), %rcx
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1484:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L1465
	movq	8(%r8), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L1483
.L1471:
	cmpq	%rcx, %rdi
	jne	.L1484
	movq	16(%r8), %r8
.L1465:
	popq	%rbx
	movq	%r8, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1482:
	.cfi_restore_state
	movq	24(%rbx), %rdx
	movq	32(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21HeapSnapshotGenerator14FindOrAddEntryEPvPNS0_20HeapEntriesAllocatorE
	.p2align 4,,10
	.p2align 3
.L1481:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	*%rax
	testq	%rax, %rax
	je	.L1467
	movq	(%rax), %rdx
	movq	%rax, %r12
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1483:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1467:
	.cfi_restore_state
	movq	(%r12), %rdx
	jmp	.L1466
	.cfi_endproc
.LFE20096:
	.size	_ZN2v88internal21NativeObjectsExplorer25EntryForEmbedderGraphNodeEPNS_13EmbedderGraph4NodeE, .-_ZN2v88internal21NativeObjectsExplorer25EntryForEmbedderGraphNodeEPNS_13EmbedderGraph4NodeE
	.section	.text._ZN2v88internal14V8HeapExplorer8GetEntryENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer8GetEntryENS0_6ObjectE
	.type	_ZN2v88internal14V8HeapExplorer8GetEntryENS0_6ObjectE, @function
_ZN2v88internal14V8HeapExplorer8GetEntryENS0_6ObjectE:
.LFB20019:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testb	$1, %sil
	je	.L1513
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	48(%rdi), %r13
	movq	344(%r13), %r8
	divq	%r8
	movq	336(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1487
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1516:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1487
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L1487
.L1489:
	cmpq	%rsi, %r12
	jne	.L1516
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1487
.L1485:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1513:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L1487:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, %rbx
	call	_Znwm@PLT
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%r12, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	344(%r13), %rsi
	movq	%r12, %rax
	divq	%rsi
	movq	336(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1490
	movq	(%rax), %rbx
	movq	8(%rbx), %rcx
	jmp	.L1492
	.p2align 4,,10
	.p2align 3
.L1517:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1490
	movq	8(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L1490
.L1492:
	cmpq	%rcx, %r12
	jne	.L1517
	call	_ZdlPv@PLT
.L1493:
	movq	16(%rbx), %rax
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1490:
	addq	$336, %r13
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r9, %rsi
	movl	$1, %r8d
	movq	%r13, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %rbx
	jmp	.L1493
	.cfi_endproc
.LFE20019:
	.size	_ZN2v88internal14V8HeapExplorer8GetEntryENS0_6ObjectE, .-_ZN2v88internal14V8HeapExplorer8GetEntryENS0_6ObjectE
	.section	.text.unlikely._ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.constprop.0,"ax",@progbits
	.align 2
.LCOLDB124:
	.section	.text._ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.constprop.0,"ax",@progbits
.LHOTB124:
	.align 2
	.p2align 4
	.type	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.constprop.0, @function
_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.constprop.0:
.LFB27386:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	call	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	testb	%al, %al
	je	.L1518
	movq	%rsi, %r12
	testb	$1, %sil
	je	.L1521
	movq	48(%rdi), %r14
	movq	%rsi, %rax
	xorl	%edx, %edx
	movq	344(%r14), %r8
	divq	%r8
	movq	336(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1522
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1524
	.p2align 4,,10
	.p2align 3
.L1549:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1522
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L1522
.L1524:
	cmpq	%rsi, %r12
	jne	.L1549
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1522
.L1530:
	movq	24(%rax), %rdx
	cmpb	$0, (%rdx)
	jne	.L1518
	movq	%rbx, 24(%rax)
.L1518:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1522:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, %r13
	call	_Znwm@PLT
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%r12, 8(%rax)
	movq	%r13, 16(%rax)
	movq	344(%r14), %rsi
	movq	%r12, %rax
	divq	%rsi
	movq	336(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1525
	movq	(%rax), %r13
	movq	8(%r13), %rcx
	jmp	.L1527
	.p2align 4,,10
	.p2align 3
.L1550:
	movq	0(%r13), %r13
	testq	%r13, %r13
	je	.L1525
	movq	8(%r13), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L1525
.L1527:
	cmpq	%rcx, %r12
	jne	.L1550
	call	_ZdlPv@PLT
.L1529:
	movq	16(%r13), %rax
	jmp	.L1530
	.p2align 4,,10
	.p2align 3
.L1525:
	addq	$336, %r14
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r9, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %r13
	jmp	.L1529
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.constprop.0
	.cfi_startproc
	.type	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.constprop.0.cold, @function
_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.constprop.0.cold:
.LFSB27386:
.L1521:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	24, %rax
	ud2
	.cfi_endproc
.LFE27386:
	.section	.text._ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.constprop.0
	.size	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.constprop.0, .-_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.constprop.0
	.section	.text.unlikely._ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.constprop.0
	.size	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.constprop.0.cold, .-_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.constprop.0.cold
.LCOLDE124:
	.section	.text._ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.constprop.0
.LHOTE124:
	.section	.text._ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE17_M_reallocate_mapEmb,"axG",@progbits,_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE17_M_reallocate_mapEmb,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE17_M_reallocate_mapEmb
	.type	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE17_M_reallocate_mapEmb, @function
_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE17_M_reallocate_mapEmb:
.LFB25491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	72(%rdi), %rax
	movq	40(%rdi), %r8
	movq	8(%rdi), %rbx
	movq	%rax, %r12
	subq	%r8, %r12
	movq	%r12, %rcx
	sarq	$3, %rcx
	leaq	1(%rsi,%rcx), %rcx
	leaq	(%rcx,%rcx), %rdi
	cmpq	%rdi, %rbx
	jbe	.L1552
	subq	%rcx, %rbx
	shrq	%rbx
	salq	$3, %rbx
	testb	%dl, %dl
	leaq	(%rbx,%rsi,8), %rcx
	cmovne	%rcx, %rbx
	addq	$8, %rax
	addq	0(%r13), %rbx
	movq	%rax, %rdx
	subq	%r8, %rdx
	cmpq	%rbx, %r8
	jbe	.L1554
	cmpq	%rax, %r8
	je	.L1555
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L1555
	.p2align 4,,10
	.p2align 3
.L1552:
	cmpq	%rsi, %rbx
	movq	%rsi, %rax
	cmovnb	%rbx, %rax
	leaq	2(%rbx,%rax), %r14
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r14
	ja	.L1560
	leaq	0(,%r14,8), %rdi
	movl	%edx, -68(%rbp)
	movq	%r14, %rbx
	movq	%rsi, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movl	-68(%rbp), %edx
	movq	%rax, %r15
	subq	%rcx, %rbx
	shrq	%rbx
	salq	$3, %rbx
	testb	%dl, %dl
	leaq	(%rbx,%rsi,8), %rax
	movq	40(%r13), %rsi
	cmovne	%rax, %rbx
	movq	72(%r13), %rax
	leaq	8(%rax), %rdx
	addq	%r15, %rbx
	cmpq	%rsi, %rdx
	je	.L1558
	subq	%rsi, %rdx
	movq	%rbx, %rdi
	call	memmove@PLT
.L1558:
	movq	0(%r13), %rdi
	call	_ZdlPv@PLT
	movq	%r15, 0(%r13)
	movq	%r14, 8(%r13)
.L1555:
	movq	%rbx, 40(%r13)
	movq	(%rbx), %rax
	movq	(%rbx), %xmm0
	addq	%r12, %rbx
	addq	$504, %rax
	movq	%rbx, 72(%r13)
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%r13)
	movq	(%rbx), %rax
	movq	%rax, 56(%r13)
	addq	$504, %rax
	movq	%rax, 64(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1554:
	.cfi_restore_state
	cmpq	%rax, %r8
	je	.L1555
	leaq	8(%r12), %rdi
	movq	%r8, %rsi
	subq	%rdx, %rdi
	addq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L1555
.L1560:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE25491:
	.size	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE17_M_reallocate_mapEmb, .-_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE17_M_reallocate_mapEmb
	.section	.text._ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERiPNS1_9HeapEntryERSA_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERiPNS1_9HeapEntryERSA_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERiPNS1_9HeapEntryERSA_EEEvDpOT_
	.type	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERiPNS1_9HeapEntryERSA_EEEvDpOT_, @function
_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERiPNS1_9HeapEntryERSA_EEEvDpOT_:
.LFB22406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	64(%rdi), %rax
	movq	48(%rdi), %rdx
	subq	$24, %rax
	cmpq	%rax, %rdx
	je	.L1562
	movq	(%r14), %rax
	movl	0(%r13), %ecx
	movq	(%r8), %rsi
	movl	(%rax), %eax
	shrl	$4, %eax
	sall	$3, %eax
	orl	(%r12), %eax
	movq	%rsi, 8(%rdx)
	movl	%eax, (%rdx)
	movl	%ecx, 16(%rdx)
	addq	$24, 48(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1562:
	.cfi_restore_state
	movq	72(%rdi), %rcx
	subq	56(%rdi), %rdx
	sarq	$3, %rdx
	movq	%rcx, %rax
	subq	40(%rdi), %rax
	movabsq	$-6148914691236517205, %rdi
	sarq	$3, %rax
	imulq	%rdi, %rdx
	subq	$1, %rax
	leaq	(%rax,%rax,4), %rsi
	leaq	(%rax,%rsi,4), %rsi
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	sarq	$3, %rax
	addq	%rsi, %rdx
	imulq	%rdi, %rax
	addq	%rdx, %rax
	movabsq	$384307168202282325, %rdx
	cmpq	%rdx, %rax
	je	.L1567
	movq	8(%rbx), %rdi
	movq	%rcx, %rax
	subq	(%rbx), %rax
	sarq	$3, %rax
	subq	%rax, %rdi
	cmpq	$1, %rdi
	jbe	.L1568
.L1565:
	movl	$504, %edi
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, 8(%rcx)
	movq	(%r14), %rax
	movl	0(%r13), %ecx
	movq	(%r15), %rsi
	movl	(%rax), %eax
	movq	48(%rbx), %rdx
	shrl	$4, %eax
	sall	$3, %eax
	orl	(%r12), %eax
	movq	%rsi, 8(%rdx)
	movl	%eax, (%rdx)
	movl	%ecx, 16(%rdx)
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 72(%rbx)
	movq	8(%rax), %rax
	leaq	504(%rax), %rdx
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1568:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE17_M_reallocate_mapEmb
	movq	72(%rbx), %rcx
	jmp	.L1565
.L1567:
	leaq	.LC32(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22406:
	.size	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERiPNS1_9HeapEntryERSA_EEEvDpOT_, .-_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERiPNS1_9HeapEntryERSA_EEEvDpOT_
	.section	.text._ZN2v88internal14V8HeapExplorer19SetElementReferenceEPNS0_9HeapEntryEiNS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer19SetElementReferenceEPNS0_9HeapEntryEiNS0_6ObjectE
	.type	_ZN2v88internal14V8HeapExplorer19SetElementReferenceEPNS0_9HeapEntryEiNS0_6ObjectE, @function
_ZN2v88internal14V8HeapExplorer19SetElementReferenceEPNS0_9HeapEntryEiNS0_6ObjectE:
.LFB20032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %cl
	je	.L1569
	movq	48(%rdi), %r15
	movl	%edx, %r14d
	movq	%rcx, %rax
	xorl	%edx, %edx
	movq	%rsi, %rbx
	movq	%rcx, %r12
	movq	344(%r15), %r8
	divq	%r8
	movq	336(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1572
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1574
	.p2align 4,,10
	.p2align 3
.L1603:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1572
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L1572
.L1574:
	cmpq	%rsi, %r12
	jne	.L1603
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1572
.L1580:
	addl	$1, 4(%rbx)
	leaq	-64(%rbp), %rcx
	leaq	-76(%rbp), %rdx
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	leaq	-80(%rbp), %rsi
	leaq	-72(%rbp), %r8
	movl	$1, -80(%rbp)
	leaq	296(%rax), %rdi
	movl	%r14d, -76(%rbp)
	movq	%rbx, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERiPNS1_9HeapEntryERSA_EEEvDpOT_
.L1569:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1604
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1572:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, %r13
	call	_Znwm@PLT
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%r12, 8(%rax)
	movq	%r13, 16(%rax)
	movq	344(%r15), %rsi
	movq	%r12, %rax
	divq	%rsi
	movq	336(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1575
	movq	(%rax), %r13
	movq	8(%r13), %rcx
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	0(%r13), %r13
	testq	%r13, %r13
	je	.L1575
	movq	8(%r13), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L1575
.L1577:
	cmpq	%rcx, %r12
	jne	.L1605
	call	_ZdlPv@PLT
.L1579:
	movq	16(%r13), %rax
	testq	%rax, %rax
	je	.L1569
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1575:
	addq	$336, %r15
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r9, %rsi
	movl	$1, %r8d
	movq	%r15, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %r13
	jmp	.L1579
.L1604:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20032:
	.size	_ZN2v88internal14V8HeapExplorer19SetElementReferenceEPNS0_9HeapEntryEiNS0_6ObjectE, .-_ZN2v88internal14V8HeapExplorer19SetElementReferenceEPNS0_9HeapEntryEiNS0_6ObjectE
	.section	.text._ZN2v88internal14V8HeapExplorer18SetHiddenReferenceENS0_10HeapObjectEPNS0_9HeapEntryEiNS0_6ObjectEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer18SetHiddenReferenceENS0_10HeapObjectEPNS0_9HeapEntryEiNS0_6ObjectEi
	.type	_ZN2v88internal14V8HeapExplorer18SetHiddenReferenceENS0_10HeapObjectEPNS0_9HeapEntryEiNS0_6ObjectEi, @function
_ZN2v88internal14V8HeapExplorer18SetHiddenReferenceENS0_10HeapObjectEPNS0_9HeapEntryEiNS0_6ObjectEi:
.LFB20035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %r8b
	je	.L1606
	movq	%r8, %r12
	movq	48(%rdi), %r8
	movq	%rdi, %r13
	movq	%rdx, %r14
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rsi, %r15
	movl	%ecx, %ebx
	movq	344(%r8), %rdi
	divq	%rdi
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L1609
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1611
	.p2align 4,,10
	.p2align 3
.L1646:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1609
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L1609
.L1611:
	cmpq	%rsi, %r12
	jne	.L1646
	movq	16(%rcx), %r10
	testq	%r10, %r10
	je	.L1609
.L1617:
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%r9d, -88(%rbp)
	call	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	movl	-88(%rbp), %r9d
	testb	%al, %al
	jne	.L1647
	.p2align 4,,10
	.p2align 3
.L1606:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1648
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1647:
	.cfi_restore_state
	movl	%r9d, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal14V8HeapExplorer26IsEssentialHiddenReferenceENS0_6ObjectEi
	testb	%al, %al
	je	.L1606
	movq	16(%r14), %rax
	leaq	-64(%rbp), %rcx
	leaq	-76(%rbp), %rdx
	movl	%ebx, -76(%rbp)
	addl	$1, 4(%r14)
	leaq	-80(%rbp), %rsi
	leaq	-72(%rbp), %r8
	leaq	296(%rax), %rdi
	movl	$4, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%r14, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERiPNS1_9HeapEntryERSA_EEEvDpOT_
	jmp	.L1606
	.p2align 4,,10
	.p2align 3
.L1609:
	movq	0(%r13), %rax
	movl	%r9d, -100(%rbp)
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, -96(%rbp)
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %r8
	movq	$0, (%rax)
	movq	%rax, %rdi
	movl	-100(%rbp), %r9d
	movq	%rdx, 16(%rax)
	xorl	%edx, %edx
	movq	%r12, 8(%rax)
	movq	344(%r8), %r11
	movq	%r12, %rax
	divq	%r11
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L1612
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1614
	.p2align 4,,10
	.p2align 3
.L1649:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1612
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r11
	cmpq	%rdx, %r10
	jne	.L1612
.L1614:
	cmpq	%rsi, %r12
	jne	.L1649
	movl	%r9d, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %rcx
	movl	-96(%rbp), %r9d
.L1616:
	movq	16(%rcx), %r10
	testq	%r10, %r10
	je	.L1606
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1612:
	leaq	336(%r8), %r11
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	movq	%r11, %rdi
	movl	%r9d, -88(%rbp)
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movl	-88(%rbp), %r9d
	movq	%rax, %rcx
	jmp	.L1616
.L1648:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20035:
	.size	_ZN2v88internal14V8HeapExplorer18SetHiddenReferenceENS0_10HeapObjectEPNS0_9HeapEntryEiNS0_6ObjectEi, .-_ZN2v88internal14V8HeapExplorer18SetHiddenReferenceENS0_10HeapObjectEPNS0_9HeapEntryEiNS0_6ObjectEi
	.section	.rodata._ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_.str1.1,"aMS",@progbits,1
.LC125:
	.string	"parent_start_ <= start"
.LC126:
	.string	"end <= parent_end_"
	.section	.text._ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_,"axG",@progbits,_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	.type	_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_, @function
_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_:
.LFB19977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	cmpq	%rdx, %rax
	ja	.L1698
	movq	%rdi, %r15
	cmpq	32(%rdi), %rcx
	ja	.L1699
	cmpq	%rcx, %rdx
	jnb	.L1650
	movl	$1, %r9d
	movq	%rdx, %rbx
	movq	%rcx, %r10
	.p2align 4,,10
	.p2align 3
.L1665:
	movq	%rbx, %r12
	movq	8(%r15), %r13
	subq	%rax, %r12
	shrq	$3, %r12
	movq	232(%r13), %rax
	movslq	%r12d, %rcx
	movq	%rcx, %rdx
	shrq	$6, %rdx
	leaq	(%rax,%rdx,8), %rdx
	movq	%r9, %rax
	salq	%cl, %rax
	movq	(%rdx), %rcx
	testq	%rax, %rcx
	jne	.L1700
	movq	(%rbx), %rax
	testb	$1, %al
	je	.L1655
	cmpl	$3, %eax
	je	.L1655
	movl	48(%r15), %edi
	movq	16(%r15), %rsi
	movq	%rax, %r14
	andq	$-3, %r14
	leal	1(%rdi), %edx
	movl	%edi, -92(%rbp)
	movq	40(%r15), %rdi
	movl	%edx, 48(%r15)
	movq	%rdi, -104(%rbp)
	movq	%rsi, -88(%rbp)
	testb	$1, %al
	je	.L1655
	movq	48(%r13), %r11
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	344(%r11), %rdi
	divq	%rdi
	movq	336(%r11), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L1658
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1701:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1658
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L1658
.L1660:
	cmpq	%rsi, %r14
	jne	.L1701
	movq	16(%rcx), %r8
	testq	%r8, %r8
	je	.L1658
.L1667:
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r10, -112(%rbp)
	call	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	movq	-112(%rbp), %r10
	movl	$1, %r9d
	testb	%al, %al
	jne	.L1702
	.p2align 4,,10
	.p2align 3
.L1655:
	addq	$8, %rbx
	cmpq	%rbx, %r10
	jbe	.L1650
.L1703:
	movq	24(%r15), %rax
	jmp	.L1665
	.p2align 4,,10
	.p2align 3
.L1700:
	notq	%rax
	addq	$8, %rbx
	andq	%rcx, %rax
	movq	%rax, (%rdx)
	cmpq	%rbx, %r10
	ja	.L1703
.L1650:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1704
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1702:
	.cfi_restore_state
	movq	-88(%rbp), %rsi
	leal	0(,%r12,8), %edx
	call	_ZN2v88internal14V8HeapExplorer26IsEssentialHiddenReferenceENS0_6ObjectEi
	movq	-112(%rbp), %r10
	movl	$1, %r9d
	testb	%al, %al
	je	.L1655
	movl	-92(%rbp), %eax
	movq	%r8, -72(%rbp)
	leaq	-64(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	leaq	-76(%rbp), %rsi
	leaq	-72(%rbp), %r8
	movl	$4, -76(%rbp)
	movl	%eax, -80(%rbp)
	movq	-104(%rbp), %rax
	addl	$1, 4(%rax)
	movq	16(%rax), %rdi
	movq	%rax, -64(%rbp)
	movq	%rdi, -88(%rbp)
	addq	$296, %rdi
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERiPNS1_9HeapEntryERSA_EEEvDpOT_
	movq	-112(%rbp), %r10
	movl	$1, %r9d
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1658:
	movq	0(%r13), %rax
	movq	%r11, -128(%rbp)
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r10, -136(%rbp)
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, -120(%rbp)
	call	_Znwm@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %r11
	movq	$0, (%rax)
	movq	-136(%rbp), %r10
	movq	%rdx, 16(%rax)
	xorl	%edx, %edx
	movq	%r14, 8(%rax)
	movq	344(%r11), %r8
	movq	%rax, -112(%rbp)
	movq	%r14, %rax
	divq	%r8
	movq	336(%r11), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rdi
	testq	%rax, %rax
	je	.L1661
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1663
	.p2align 4,,10
	.p2align 3
.L1705:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1661
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %rdi
	jne	.L1661
.L1663:
	cmpq	%rsi, %r14
	jne	.L1705
	movq	-112(%rbp), %rdi
	movq	%rcx, -128(%rbp)
	movq	%r10, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %r10
	movq	-128(%rbp), %rcx
	movl	$1, %r9d
.L1666:
	movq	16(%rcx), %r8
	testq	%r8, %r8
	je	.L1655
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1698:
	leaq	.LC125(%rip), %rsi
	leaq	.LC105(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1699:
	leaq	.LC126(%rip), %rsi
	leaq	.LC105(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1661:
	movq	-112(%rbp), %rcx
	movq	%rdi, %rsi
	movl	$1, %r8d
	movq	%r14, %rdx
	addq	$336, %r11
	movq	%r10, -120(%rbp)
	movq	%r11, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	-120(%rbp), %r10
	movl	$1, %r9d
	movq	%rax, %rcx
	jmp	.L1666
.L1704:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19977:
	.size	_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_, .-_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	.section	.text._ZN2v88internal26IndexedReferencesExtractor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal26IndexedReferencesExtractor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26IndexedReferencesExtractor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal26IndexedReferencesExtractor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal26IndexedReferencesExtractor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE:
.LFB19979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	48(%rdi), %r14d
	movq	8(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	40(%rdi), %r15
	movq	16(%rdi), %r9
	movq	(%rax), %r12
	leal	1(%r14), %eax
	movl	%eax, 48(%rdi)
	testb	$1, %r12b
	je	.L1706
	movq	48(%r13), %r11
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	344(%r11), %rdi
	divq	%rdi
	movq	336(%r11), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L1709
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1711
	.p2align 4,,10
	.p2align 3
.L1746:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1709
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L1709
.L1711:
	cmpq	%rsi, %r12
	jne	.L1746
	movq	16(%rcx), %r10
	testq	%r10, %r10
	je	.L1709
.L1717:
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	movq	-88(%rbp), %r9
	testb	%al, %al
	jne	.L1747
	.p2align 4,,10
	.p2align 3
.L1706:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1748
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1747:
	.cfi_restore_state
	movl	$-8, %edx
	movq	%r9, %rsi
	call	_ZN2v88internal14V8HeapExplorer26IsEssentialHiddenReferenceENS0_6ObjectEi
	testb	%al, %al
	je	.L1706
	movq	%r10, -72(%rbp)
	movq	16(%r15), %rax
	leaq	-64(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	addl	$1, 4(%r15)
	leaq	-76(%rbp), %rsi
	leaq	-72(%rbp), %r8
	leaq	296(%rax), %rdi
	movl	$4, -76(%rbp)
	movl	%r14d, -80(%rbp)
	movq	%r15, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERiPNS1_9HeapEntryERSA_EEEvDpOT_
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1709:
	movq	0(%r13), %rax
	movq	%r9, -96(%rbp)
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r11, -88(%rbp)
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, %rbx
	call	_Znwm@PLT
	movq	-88(%rbp), %r11
	xorl	%edx, %edx
	movq	-96(%rbp), %r9
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%r12, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	344(%r11), %rsi
	movq	%r12, %rax
	divq	%rsi
	movq	336(%r11), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L1712
	movq	(%rax), %rbx
	movq	8(%rbx), %rcx
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L1749:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1712
	movq	8(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L1712
.L1714:
	cmpq	%rcx, %r12
	jne	.L1749
	movq	%r9, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %r9
.L1716:
	movq	16(%rbx), %r10
	testq	%r10, %r10
	je	.L1706
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L1712:
	addq	$336, %r11
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	movq	%r11, %rdi
	movq	%r9, -88(%rbp)
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	-88(%rbp), %r9
	movq	%rax, %rbx
	jmp	.L1716
.L1748:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19979:
	.size	_ZN2v88internal26IndexedReferencesExtractor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal26IndexedReferencesExtractor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.section	.rodata._ZN2v88internal26IndexedReferencesExtractor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE.str1.8,"aMS",@progbits,1
	.align 8
.LC127:
	.string	"address < start || address >= end"
	.section	.text._ZN2v88internal26IndexedReferencesExtractor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal26IndexedReferencesExtractor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26IndexedReferencesExtractor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal26IndexedReferencesExtractor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal26IndexedReferencesExtractor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE:
.LFB19978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movslq	(%rax), %r12
	addq	%rax, %r12
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	leaq	4(%r12), %r14
	movq	%rax, %r13
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %eax
	addq	%r13, %rax
	cmpq	%rax, %r14
	jnb	.L1751
	cmpq	%r14, %r13
	jbe	.L1797
.L1751:
	movl	48(%rbx), %r15d
	leaq	-59(%r12), %r13
	andl	$1, %r12d
	movq	8(%rbx), %r14
	movq	40(%rbx), %r9
	leal	1(%r15), %eax
	movl	%eax, 48(%rbx)
	movq	16(%rbx), %rbx
	jne	.L1750
	movq	48(%r14), %r11
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	344(%r11), %rdi
	divq	%rdi
	movq	336(%r11), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L1754
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1756
	.p2align 4,,10
	.p2align 3
.L1798:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1754
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L1754
.L1756:
	cmpq	%rsi, %r13
	jne	.L1798
	movq	16(%rcx), %r10
	testq	%r10, %r10
	je	.L1754
.L1762:
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	testb	%al, %al
	jne	.L1799
	.p2align 4,,10
	.p2align 3
.L1750:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1800
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1797:
	.cfi_restore_state
	leaq	.LC127(%rip), %rsi
	leaq	.LC105(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1799:
	movl	$-8, %edx
	movq	%rbx, %rsi
	call	_ZN2v88internal14V8HeapExplorer26IsEssentialHiddenReferenceENS0_6ObjectEi
	movq	-88(%rbp), %r9
	testb	%al, %al
	je	.L1750
	movq	%r10, -72(%rbp)
	movq	16(%r9), %rax
	leaq	-64(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	addl	$1, 4(%r9)
	leaq	-76(%rbp), %rsi
	leaq	-72(%rbp), %r8
	leaq	296(%rax), %rdi
	movl	$4, -76(%rbp)
	movl	%r15d, -80(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERiPNS1_9HeapEntryERSA_EEEvDpOT_
	jmp	.L1750
	.p2align 4,,10
	.p2align 3
.L1754:
	movq	(%r14), %rax
	movq	%r11, -96(%rbp)
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r9, -88(%rbp)
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, %r12
	call	_Znwm@PLT
	movq	-96(%rbp), %r11
	xorl	%edx, %edx
	movq	-88(%rbp), %r9
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%r13, 8(%rax)
	movq	%r12, 16(%rax)
	movq	344(%r11), %rsi
	movq	%r13, %rax
	divq	%rsi
	movq	336(%r11), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L1757
	movq	(%rax), %r12
	movq	8(%r12), %rcx
	jmp	.L1759
	.p2align 4,,10
	.p2align 3
.L1801:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L1757
	movq	8(%r12), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L1757
.L1759:
	cmpq	%rcx, %r13
	jne	.L1801
	movq	%r9, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %r9
.L1761:
	movq	16(%r12), %r10
	testq	%r10, %r10
	je	.L1750
	jmp	.L1762
	.p2align 4,,10
	.p2align 3
.L1757:
	addq	$336, %r11
	movq	%rdi, %rcx
	movq	%r13, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	movq	%r11, %rdi
	movq	%r9, -88(%rbp)
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	-88(%rbp), %r9
	movq	%rax, %r12
	jmp	.L1761
.L1800:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19978:
	.size	_ZN2v88internal26IndexedReferencesExtractor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal26IndexedReferencesExtractor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal14V8HeapExplorer24ExtractElementReferencesENS0_8JSObjectEPNS0_9HeapEntryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer24ExtractElementReferencesENS0_8JSObjectEPNS0_9HeapEntryE
	.type	_ZN2v88internal14V8HeapExplorer24ExtractElementReferencesENS0_8JSObjectEPNS0_9HeapEntryE, @function
_ZN2v88internal14V8HeapExplorer24ExtractElementReferencesENS0_8JSObjectEPNS0_9HeapEntryE:
.LFB20015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	%rdi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	24(%rax), %r12
	movq	-1(%rsi), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$2, %eax
	cmpb	$1, %al
	ja	.L1803
	leaq	-37592(%r12), %r9
	movq	15(%rsi), %r12
	movq	-1(%rsi), %rax
	cmpw	$1061, 11(%rax)
	je	.L1859
	movl	11(%r12), %r11d
.L1805:
	leaq	-72(%rbp), %rax
	addq	$15, %r12
	xorl	%r13d, %r13d
	movq	%rax, -112(%rbp)
	testl	%r11d, %r11d
	jle	.L1802
	movq	%rbx, %r10
	movl	%r11d, %ebx
	.p2align 4,,10
	.p2align 3
.L1818:
	movq	(%r12), %rax
	cmpq	%rax, 96(%r9)
	je	.L1807
	movq	(%r12), %r14
	testb	$1, %r14b
	je	.L1807
	movq	-88(%rbp), %rax
	xorl	%edx, %edx
	movq	48(%rax), %r8
	movq	%r14, %rax
	movq	344(%r8), %rdi
	divq	%rdi
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r11
	testq	%rax, %rax
	je	.L1810
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1812
	.p2align 4,,10
	.p2align 3
.L1860:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1810
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r11
	jne	.L1810
.L1812:
	cmpq	%rsi, %r14
	jne	.L1860
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1810
.L1827:
	movq	%rax, -72(%rbp)
	movq	16(%r10), %rax
	leaq	-64(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	addl	$1, 4(%r10)
	movq	-112(%rbp), %r8
	leaq	-76(%rbp), %rsi
	leaq	296(%rax), %rdi
	movq	%r9, -104(%rbp)
	movq	%r10, -64(%rbp)
	movq	%r10, -96(%rbp)
	movl	$1, -76(%rbp)
	movl	%r13d, -80(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERiPNS1_9HeapEntryERSA_EEEvDpOT_
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %r10
	.p2align 4,,10
	.p2align 3
.L1807:
	addl	$1, %r13d
	addq	$8, %r12
	cmpl	%r13d, %ebx
	jne	.L1818
.L1802:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1861
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1803:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$12, %eax
	jne	.L1802
	movq	15(%rsi), %rax
	movslq	35(%rax), %rdx
	testq	%rdx, %rdx
	jle	.L1802
	subl	$1, %edx
	movq	%rdi, %r15
	leaq	47(%rax), %r14
	subq	$37536, %r12
	leaq	(%rdx,%rdx,2), %rdx
	leaq	-1(%rax), %rdi
	movq	%r14, -112(%rbp)
	leaq	71(%rax,%rdx,8), %r13
	movq	%rdi, -104(%rbp)
	movq	%r13, -96(%rbp)
	jmp	.L1825
	.p2align 4,,10
	.p2align 3
.L1862:
	movq	%r13, %rdx
	pxor	%xmm0, %xmm0
	sarq	$32, %rdx
	cvtsi2sdl	%edx, %xmm0
.L1821:
	movl	$56, %eax
	subl	-112(%rbp), %eax
	movq	-104(%rbp), %rdi
	movsd	%xmm0, -88(%rbp)
	addl	%r14d, %eax
	cltq
	movq	(%rax,%rdi), %rsi
	testb	$1, %sil
	je	.L1819
	movq	48(%r15), %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal21HeapSnapshotGenerator14FindOrAddEntryEPvPNS0_20HeapEntriesAllocatorE
	testq	%rax, %rax
	je	.L1819
	movsd	-88(%rbp), %xmm0
	addl	$1, 4(%rbx)
	leaq	-64(%rbp), %rcx
	leaq	-76(%rbp), %rsi
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	leaq	-72(%rbp), %r8
	cvttsd2siq	%xmm0, %rdx
	movl	$1, -76(%rbp)
	leaq	296(%rax), %rdi
	movq	%rbx, -64(%rbp)
	movl	%edx, -80(%rbp)
	leaq	-80(%rbp), %rdx
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERiPNS1_9HeapEntryERSA_EEEvDpOT_
.L1819:
	addq	$24, %r14
	cmpq	%r14, -96(%rbp)
	je	.L1802
.L1825:
	movq	(%r14), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal9HashTableINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE5IsKeyENS0_13ReadOnlyRootsENS0_6ObjectE@PLT
	testb	%al, %al
	je	.L1819
	testb	$1, %r13b
	je	.L1862
	movsd	7(%r13), %xmm0
	jmp	.L1821
	.p2align 4,,10
	.p2align 3
.L1859:
	movl	27(%rsi), %r11d
	jmp	.L1805
	.p2align 4,,10
	.p2align 3
.L1810:
	movq	-88(%rbp), %rdi
	movq	%r10, -120(%rbp)
	movq	%r14, %rsi
	movq	%r9, -104(%rbp)
	movq	(%rdi), %rax
	movq	%r8, -96(%rbp)
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, %r15
	call	_Znwm@PLT
	movq	-96(%rbp), %r8
	xorl	%edx, %edx
	movq	-104(%rbp), %r9
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	-120(%rbp), %r10
	movq	%r14, 8(%rax)
	movq	%r15, 16(%rax)
	movq	344(%r8), %r11
	movq	%r14, %rax
	divq	%r11
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rsi
	testq	%rax, %rax
	je	.L1813
	movq	(%rax), %r15
	movq	8(%r15), %rcx
	jmp	.L1815
	.p2align 4,,10
	.p2align 3
.L1863:
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.L1813
	movq	8(%r15), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r11
	cmpq	%rdx, %rsi
	jne	.L1813
.L1815:
	cmpq	%r14, %rcx
	jne	.L1863
	movq	%r9, -104(%rbp)
	movq	%r10, -96(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %r10
	movq	-104(%rbp), %r9
.L1826:
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.L1807
	jmp	.L1827
	.p2align 4,,10
	.p2align 3
.L1813:
	leaq	336(%r8), %r11
	movq	%rdi, %rcx
	movl	$1, %r8d
	movq	%r14, %rdx
	movq	%r11, %rdi
	movq	%r10, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	-104(%rbp), %r10
	movq	-96(%rbp), %r9
	movq	%rax, %r15
	jmp	.L1826
.L1861:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20015:
	.size	_ZN2v88internal14V8HeapExplorer24ExtractElementReferencesENS0_8JSObjectEPNS0_9HeapEntryE, .-_ZN2v88internal14V8HeapExplorer24ExtractElementReferencesENS0_8JSObjectEPNS0_9HeapEntryE
	.section	.text._ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	.type	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_, @function
_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_:
.LFB22402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	64(%rdi), %rax
	movq	48(%rdi), %rdx
	subq	$24, %rax
	cmpq	%rax, %rdx
	je	.L1865
	movq	(%r14), %rax
	movq	0(%r13), %rcx
	movq	(%r8), %xmm0
	movl	(%rax), %eax
	movq	%rcx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	shrl	$4, %eax
	sall	$3, %eax
	orl	(%rsi), %eax
	movups	%xmm0, 8(%rdx)
	movl	%eax, (%rdx)
	addq	$24, 48(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1865:
	.cfi_restore_state
	movq	72(%rdi), %rcx
	subq	56(%rdi), %rdx
	sarq	$3, %rdx
	movq	%rcx, %rax
	subq	40(%rdi), %rax
	movabsq	$-6148914691236517205, %rdi
	sarq	$3, %rax
	imulq	%rdi, %rdx
	subq	$1, %rax
	leaq	(%rax,%rax,4), %rsi
	leaq	(%rax,%rsi,4), %rsi
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	sarq	$3, %rax
	addq	%rsi, %rdx
	imulq	%rdi, %rax
	addq	%rdx, %rax
	movabsq	$384307168202282325, %rdx
	cmpq	%rdx, %rax
	je	.L1870
	movq	8(%rbx), %rsi
	movq	%rcx, %rax
	subq	(%rbx), %rax
	sarq	$3, %rax
	subq	%rax, %rsi
	cmpq	$1, %rsi
	jbe	.L1871
.L1868:
	movl	$504, %edi
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, 8(%rcx)
	movq	(%r14), %rax
	movq	0(%r13), %rcx
	movq	(%r15), %xmm0
	movl	(%rax), %eax
	movq	48(%rbx), %rdx
	movq	%rcx, %xmm2
	shrl	$4, %eax
	punpcklqdq	%xmm2, %xmm0
	sall	$3, %eax
	orl	(%r12), %eax
	movups	%xmm0, 8(%rdx)
	movl	%eax, (%rdx)
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 72(%rbx)
	movq	8(%rax), %rax
	leaq	504(%rax), %rdx
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1871:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE17_M_reallocate_mapEmb
	movq	72(%rbx), %rcx
	jmp	.L1868
.L1870:
	leaq	.LC32(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22402:
	.size	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_, .-_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	.section	.text._ZN2v88internal9HeapEntry17SetNamedReferenceENS0_13HeapGraphEdge4TypeEPKcPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9HeapEntry17SetNamedReferenceENS0_13HeapGraphEdge4TypeEPKcPS1_
	.type	_ZN2v88internal9HeapEntry17SetNamedReferenceENS0_13HeapGraphEdge4TypeEPKcPS1_, @function
_ZN2v88internal9HeapEntry17SetNamedReferenceENS0_13HeapGraphEdge4TypeEPKcPS1_:
.LFB19780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movl	%esi, -20(%rbp)
	leaq	-40(%rbp), %r8
	leaq	-20(%rbp), %rsi
	movq	%rdx, -32(%rbp)
	leaq	-32(%rbp), %rdx
	movq	%rcx, -40(%rbp)
	leaq	-16(%rbp), %rcx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	addl	$1, 4(%rdi)
	movq	%rdi, -16(%rbp)
	leaq	296(%rax), %r9
	movq	%r9, %rdi
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1875
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1875:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19780:
	.size	_ZN2v88internal9HeapEntry17SetNamedReferenceENS0_13HeapGraphEdge4TypeEPKcPS1_, .-_ZN2v88internal9HeapEntry17SetNamedReferenceENS0_13HeapGraphEdge4TypeEPKcPS1_
	.section	.rodata._ZN2v88internal9HeapEntry26SetNamedAutoIndexReferenceENS0_13HeapGraphEdge4TypeEPKcPS1_PNS0_14StringsStorageE.str1.1,"aMS",@progbits,1
.LC128:
	.string	"%d / %s"
	.section	.text._ZN2v88internal9HeapEntry26SetNamedAutoIndexReferenceENS0_13HeapGraphEdge4TypeEPKcPS1_PNS0_14StringsStorageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9HeapEntry26SetNamedAutoIndexReferenceENS0_13HeapGraphEdge4TypeEPKcPS1_PNS0_14StringsStorageE
	.type	_ZN2v88internal9HeapEntry26SetNamedAutoIndexReferenceENS0_13HeapGraphEdge4TypeEPKcPS1_PNS0_14StringsStorageE, @function
_ZN2v88internal9HeapEntry26SetNamedAutoIndexReferenceENS0_13HeapGraphEdge4TypeEPKcPS1_PNS0_14StringsStorageE:
.LFB19782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r8, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	4(%rbx), %eax
	leal	1(%rax), %edx
	testq	%r9, %r9
	je	.L1877
	movq	%r9, %rcx
	leaq	.LC128(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal14StringsStorage12GetFormattedEPKcz@PLT
.L1878:
	addl	$1, 4(%rbx)
	leaq	-48(%rbp), %rcx
	leaq	-64(%rbp), %rdx
	movq	%rax, -64(%rbp)
	movq	16(%rbx), %rax
	leaq	-68(%rbp), %rsi
	leaq	-56(%rbp), %r8
	movl	%r13d, -68(%rbp)
	leaq	296(%rax), %rdi
	movq	%r12, -56(%rbp)
	movq	%rbx, -48(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1881
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1877:
	.cfi_restore_state
	movl	%edx, %esi
	call	_ZN2v88internal14StringsStorage7GetNameEi@PLT
	jmp	.L1878
.L1881:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19782:
	.size	_ZN2v88internal9HeapEntry26SetNamedAutoIndexReferenceENS0_13HeapGraphEdge4TypeEPKcPS1_PNS0_14StringsStorageE, .-_ZN2v88internal9HeapEntry26SetNamedAutoIndexReferenceENS0_13HeapGraphEdge4TypeEPKcPS1_PNS0_14StringsStorageE
	.section	.text._ZN2v88internal14V8HeapExplorer19SetContextReferenceEPNS0_9HeapEntryENS0_6StringENS0_6ObjectEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer19SetContextReferenceEPNS0_9HeapEntryENS0_6StringENS0_6ObjectEi
	.type	_ZN2v88internal14V8HeapExplorer19SetContextReferenceEPNS0_9HeapEntryENS0_6StringENS0_6ObjectEi, @function
_ZN2v88internal14V8HeapExplorer19SetContextReferenceEPNS0_9HeapEntryENS0_6StringENS0_6ObjectEi:
.LFB20029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %cl
	je	.L1882
	movl	%r8d, %r13d
	movq	48(%rdi), %r8
	movq	%rcx, %r12
	movq	%rdx, %r15
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rdi, %r14
	movq	%rsi, %rbx
	movq	344(%r8), %rcx
	divq	%rcx
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1885
	movq	(%rax), %rsi
	movq	8(%rsi), %rdi
	jmp	.L1887
	.p2align 4,,10
	.p2align 3
.L1916:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L1885
	movq	8(%rsi), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L1885
.L1887:
	cmpq	%rdi, %r12
	jne	.L1916
	movq	16(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L1885
.L1893:
	movq	24(%r14), %rdi
	movq	%r15, %rsi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	addl	$1, 4(%rbx)
	movq	-104(%rbp), %rdx
	leaq	-64(%rbp), %rcx
	movq	%rax, -80(%rbp)
	movq	16(%rbx), %rax
	leaq	-84(%rbp), %rsi
	leaq	-72(%rbp), %r8
	movq	%rdx, -72(%rbp)
	leaq	-80(%rbp), %rdx
	leaq	296(%rax), %rdi
	movl	$0, -84(%rbp)
	movq	%rbx, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	testl	%r13d, %r13d
	js	.L1882
	movl	%r13d, %ecx
	movq	232(%r14), %rsi
	movl	$1, %edx
	sarl	$3, %ecx
	movslq	%ecx, %rax
	salq	%cl, %rdx
	shrq	$6, %rax
	orq	%rdx, (%rsi,%rax,8)
.L1882:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1917
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1885:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	%r8, -112(%rbp)
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r8
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%rdx, 16(%rax)
	xorl	%edx, %edx
	movq	%r12, 8(%rax)
	movq	344(%r8), %r9
	movq	%r12, %rax
	divq	%r9
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L1888
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1890
	.p2align 4,,10
	.p2align 3
.L1918:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1888
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r9
	cmpq	%rdx, %r10
	jne	.L1888
.L1890:
	cmpq	%rsi, %r12
	jne	.L1918
	movq	%rcx, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rcx
.L1892:
	movq	16(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L1882
	jmp	.L1893
	.p2align 4,,10
	.p2align 3
.L1888:
	leaq	336(%r8), %r9
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	movq	%r9, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %rcx
	jmp	.L1892
.L1917:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20029:
	.size	_ZN2v88internal14V8HeapExplorer19SetContextReferenceEPNS0_9HeapEntryENS0_6StringENS0_6ObjectEi, .-_ZN2v88internal14V8HeapExplorer19SetContextReferenceEPNS0_9HeapEntryENS0_6StringENS0_6ObjectEi
	.section	.text._ZN2v88internal14V8HeapExplorer22SetNativeBindReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer22SetNativeBindReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectE
	.type	_ZN2v88internal14V8HeapExplorer22SetNativeBindReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectE, @function
_ZN2v88internal14V8HeapExplorer22SetNativeBindReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectE:
.LFB20031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %cl
	je	.L1919
	movq	48(%rdi), %r15
	movq	%rdx, %r14
	movq	%rcx, %rax
	xorl	%edx, %edx
	movq	%rsi, %rbx
	movq	%rcx, %r12
	movq	344(%r15), %r8
	divq	%r8
	movq	336(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1922
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1924
	.p2align 4,,10
	.p2align 3
.L1953:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1922
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L1922
.L1924:
	cmpq	%rsi, %r12
	jne	.L1953
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L1922
.L1930:
	addl	$1, 4(%rbx)
	leaq	-64(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	leaq	-84(%rbp), %rsi
	leaq	-72(%rbp), %r8
	movl	$5, -84(%rbp)
	leaq	296(%rax), %rdi
	movq	%r14, -80(%rbp)
	movq	%rbx, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
.L1919:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1954
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1922:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, %r13
	call	_Znwm@PLT
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%r12, 8(%rax)
	movq	%r13, 16(%rax)
	movq	344(%r15), %rsi
	movq	%r12, %rax
	divq	%rsi
	movq	336(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1925
	movq	(%rax), %r13
	movq	8(%r13), %rcx
	jmp	.L1927
	.p2align 4,,10
	.p2align 3
.L1955:
	movq	0(%r13), %r13
	testq	%r13, %r13
	je	.L1925
	movq	8(%r13), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L1925
.L1927:
	cmpq	%rcx, %r12
	jne	.L1955
	call	_ZdlPv@PLT
.L1929:
	movq	16(%r13), %rax
	testq	%rax, %rax
	je	.L1919
	jmp	.L1930
	.p2align 4,,10
	.p2align 3
.L1925:
	addq	$336, %r15
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r9, %rsi
	movl	$1, %r8d
	movq	%r15, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %r13
	jmp	.L1929
.L1954:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20031:
	.size	_ZN2v88internal14V8HeapExplorer22SetNativeBindReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectE, .-_ZN2v88internal14V8HeapExplorer22SetNativeBindReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectE
	.section	.text._ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	.type	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi, @function
_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi:
.LFB20033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %cl
	je	.L1956
	movl	%r8d, %ebx
	movq	48(%rdi), %r8
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rdi, %r14
	movq	%rsi, %r15
	movq	344(%r8), %rcx
	divq	%rcx
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1959
	movq	(%rax), %rsi
	movq	8(%rsi), %rdi
	jmp	.L1961
	.p2align 4,,10
	.p2align 3
.L1994:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L1959
	movq	8(%rsi), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L1959
.L1961:
	cmpq	%rdi, %r12
	jne	.L1994
	movq	16(%rsi), %r9
	testq	%r9, %r9
	je	.L1959
.L1968:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	testb	%al, %al
	jne	.L1995
.L1966:
	testl	%ebx, %ebx
	js	.L1956
	sarl	$3, %ebx
	movq	232(%r14), %rsi
	movl	$1, %edx
	movslq	%ebx, %rax
	movq	%rax, %rcx
	shrq	$6, %rax
	salq	%cl, %rdx
	orq	%rdx, (%rsi,%rax,8)
.L1956:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1996
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1995:
	.cfi_restore_state
	movq	16(%r15), %rax
	leaq	-64(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	movq	%r13, -80(%rbp)
	addl	$1, 4(%r15)
	leaq	-84(%rbp), %rsi
	leaq	-72(%rbp), %r8
	leaq	296(%rax), %rdi
	movl	$3, -84(%rbp)
	movq	%r9, -72(%rbp)
	movq	%r15, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	jmp	.L1966
	.p2align 4,,10
	.p2align 3
.L1959:
	movq	(%r14), %rax
	movq	%r8, -112(%rbp)
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r8
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%rdx, 16(%rax)
	xorl	%edx, %edx
	movq	%r12, 8(%rax)
	movq	344(%r8), %r9
	movq	%r12, %rax
	divq	%r9
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L1962
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L1997:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1962
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r9
	cmpq	%rdx, %r10
	jne	.L1962
.L1964:
	cmpq	%rsi, %r12
	jne	.L1997
	movq	%rcx, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rcx
.L1967:
	movq	16(%rcx), %r9
	testq	%r9, %r9
	je	.L1956
	jmp	.L1968
	.p2align 4,,10
	.p2align 3
.L1962:
	leaq	336(%r8), %r9
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	movq	%r9, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %rcx
	jmp	.L1967
.L1996:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20033:
	.size	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi, .-_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	.section	.rodata._ZN2v88internal14V8HeapExplorer23ExtractStringReferencesEPNS0_9HeapEntryENS0_6StringE.str1.1,"aMS",@progbits,1
.LC129:
	.string	"first"
.LC130:
	.string	"second"
.LC131:
	.string	"parent"
.LC132:
	.string	"actual"
	.section	.text._ZN2v88internal14V8HeapExplorer23ExtractStringReferencesEPNS0_9HeapEntryENS0_6StringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer23ExtractStringReferencesEPNS0_9HeapEntryENS0_6StringE
	.type	_ZN2v88internal14V8HeapExplorer23ExtractStringReferencesEPNS0_9HeapEntryENS0_6StringE, @function
_ZN2v88internal14V8HeapExplorer23ExtractStringReferencesEPNS0_9HeapEntryENS0_6StringE:
.LFB19984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	jbe	.L2005
.L1999:
	movq	-1(%rbx), %rax
	cmpw	$63, 11(%rax)
	jbe	.L2006
.L2000:
	movq	-1(%rbx), %rax
	cmpw	$63, 11(%rax)
	jbe	.L2007
.L1998:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2006:
	.cfi_restore_state
	movq	-1(%rbx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$3, %ax
	jne	.L2000
	movq	15(%rbx), %rcx
	addq	$24, %rsp
	movl	$16, %r8d
	leaq	.LC131(%rip), %rdx
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	.p2align 4,,10
	.p2align 3
.L2007:
	.cfi_restore_state
	movq	-1(%rbx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L1998
	movq	15(%rbx), %rcx
	addq	$24, %rsp
	movl	$16, %r8d
	leaq	.LC132(%rip), %rdx
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	.p2align 4,,10
	.p2align 3
.L2005:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L1999
	movq	15(%rdx), %rcx
	movl	$16, %r8d
	leaq	.LC129(%rip), %rdx
	movq	%rsi, -32(%rbp)
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	23(%rbx), %rcx
	movq	-32(%rbp), %rsi
	movl	$24, %r8d
	movq	-24(%rbp), %rdi
	addq	$24, %rsp
	leaq	.LC130(%rip), %rdx
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	.cfi_endproc
.LFE19984:
	.size	_ZN2v88internal14V8HeapExplorer23ExtractStringReferencesEPNS0_9HeapEntryENS0_6StringE, .-_ZN2v88internal14V8HeapExplorer23ExtractStringReferencesEPNS0_9HeapEntryENS0_6StringE
	.section	.rodata._ZN2v88internal14V8HeapExplorer29ExtractAccessorInfoReferencesEPNS0_9HeapEntryENS0_12AccessorInfoE.str1.1,"aMS",@progbits,1
.LC133:
	.string	"name"
.LC134:
	.string	"expected_receiver_type"
.LC135:
	.string	"getter"
.LC136:
	.string	"setter"
.LC137:
	.string	"data"
	.section	.text._ZN2v88internal14V8HeapExplorer29ExtractAccessorInfoReferencesEPNS0_9HeapEntryENS0_12AccessorInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer29ExtractAccessorInfoReferencesEPNS0_9HeapEntryENS0_12AccessorInfoE
	.type	_ZN2v88internal14V8HeapExplorer29ExtractAccessorInfoReferencesEPNS0_9HeapEntryENS0_12AccessorInfoE, @function
_ZN2v88internal14V8HeapExplorer29ExtractAccessorInfoReferencesEPNS0_9HeapEntryENS0_12AccessorInfoE:
.LFB19993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	7(%rdx), %rcx
	leaq	.LC133(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	23(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$24, %r8d
	leaq	.LC134(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	39(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$40, %r8d
	leaq	.LC135(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	31(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %r8d
	leaq	.LC136(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	55(%rbx), %rcx
	addq	$8, %rsp
	movq	%r13, %rsi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movl	$56, %r8d
	popq	%r13
	leaq	.LC137(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	.cfi_endproc
.LFE19993:
	.size	_ZN2v88internal14V8HeapExplorer29ExtractAccessorInfoReferencesEPNS0_9HeapEntryENS0_12AccessorInfoE, .-_ZN2v88internal14V8HeapExplorer29ExtractAccessorInfoReferencesEPNS0_9HeapEntryENS0_12AccessorInfoE
	.section	.text._ZN2v88internal14V8HeapExplorer29ExtractAccessorPairReferencesEPNS0_9HeapEntryENS0_12AccessorPairE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer29ExtractAccessorPairReferencesEPNS0_9HeapEntryENS0_12AccessorPairE
	.type	_ZN2v88internal14V8HeapExplorer29ExtractAccessorPairReferencesEPNS0_9HeapEntryENS0_12AccessorPairE, @function
_ZN2v88internal14V8HeapExplorer29ExtractAccessorPairReferencesEPNS0_9HeapEntryENS0_12AccessorPairE:
.LFB19994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	7(%rdx), %rcx
	leaq	.LC135(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	15(%rbx), %rcx
	addq	$8, %rsp
	movq	%r13, %rsi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movl	$16, %r8d
	popq	%r13
	leaq	.LC136(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	.cfi_endproc
.LFE19994:
	.size	_ZN2v88internal14V8HeapExplorer29ExtractAccessorPairReferencesEPNS0_9HeapEntryENS0_12AccessorPairE, .-_ZN2v88internal14V8HeapExplorer29ExtractAccessorPairReferencesEPNS0_9HeapEntryENS0_12AccessorPairE
	.section	.rodata._ZN2v88internal14V8HeapExplorer34ExtractJSGeneratorObjectReferencesEPNS0_9HeapEntryENS0_17JSGeneratorObjectE.str1.1,"aMS",@progbits,1
.LC138:
	.string	"function"
.LC139:
	.string	"context"
.LC140:
	.string	"receiver"
.LC141:
	.string	"parameters_and_registers"
	.section	.text._ZN2v88internal14V8HeapExplorer34ExtractJSGeneratorObjectReferencesEPNS0_9HeapEntryENS0_17JSGeneratorObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer34ExtractJSGeneratorObjectReferencesEPNS0_9HeapEntryENS0_17JSGeneratorObjectE
	.type	_ZN2v88internal14V8HeapExplorer34ExtractJSGeneratorObjectReferencesEPNS0_9HeapEntryENS0_17JSGeneratorObjectE, @function
_ZN2v88internal14V8HeapExplorer34ExtractJSGeneratorObjectReferencesEPNS0_9HeapEntryENS0_17JSGeneratorObjectE:
.LFB20008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	23(%rdx), %rcx
	leaq	.LC138(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	31(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %r8d
	leaq	.LC139(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	39(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$40, %r8d
	leaq	.LC140(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	71(%rbx), %rcx
	addq	$8, %rsp
	movq	%r13, %rsi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movl	$72, %r8d
	popq	%r13
	leaq	.LC141(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	.cfi_endproc
.LFE20008:
	.size	_ZN2v88internal14V8HeapExplorer34ExtractJSGeneratorObjectReferencesEPNS0_9HeapEntryENS0_17JSGeneratorObjectE, .-_ZN2v88internal14V8HeapExplorer34ExtractJSGeneratorObjectReferencesEPNS0_9HeapEntryENS0_17JSGeneratorObjectE
	.section	.rodata._ZN2v88internal14V8HeapExplorer35ExtractSharedFunctionInfoReferencesEPNS0_9HeapEntryENS0_18SharedFunctionInfoE.str1.1,"aMS",@progbits,1
.LC142:
	.string	"(code for %s)"
.LC143:
	.string	"(%s code)"
.LC144:
	.string	"(function scope info)"
.LC145:
	.string	"name_or_scope_info"
.LC146:
	.string	"script_or_debug_info"
.LC147:
	.string	"function_data"
	.section	.rodata._ZN2v88internal14V8HeapExplorer35ExtractSharedFunctionInfoReferencesEPNS0_9HeapEntryENS0_18SharedFunctionInfoE.str1.8,"aMS",@progbits,1
	.align 8
.LC148:
	.string	"raw_outer_scope_info_or_feedback_metadata"
	.section	.text._ZN2v88internal14V8HeapExplorer35ExtractSharedFunctionInfoReferencesEPNS0_9HeapEntryENS0_18SharedFunctionInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer35ExtractSharedFunctionInfoReferencesEPNS0_9HeapEntryENS0_18SharedFunctionInfoE
	.type	_ZN2v88internal14V8HeapExplorer35ExtractSharedFunctionInfoReferencesEPNS0_9HeapEntryENS0_18SharedFunctionInfoE, @function
_ZN2v88internal14V8HeapExplorer35ExtractSharedFunctionInfoReferencesEPNS0_9HeapEntryENS0_18SharedFunctionInfoE:
.LFB19991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-40(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$16, %rsp
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	%rax, %rsi
	movq	8(%r12), %rax
	cmpq	%rsi, -37464(%rax)
	je	.L2015
	movq	24(%r12), %rdi
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	movq	24(%r12), %rdi
	leaq	.LC142(%rip), %rsi
	movq	%rax, %rdx
.L2020:
	xorl	%eax, %eax
	call	_ZN2v88internal14StringsStorage12GetFormattedEPKcz@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal18SharedFunctionInfo7GetCodeEv@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	-40(%rbp), %rax
	movq	15(%rax), %rcx
	testb	$1, %cl
	jne	.L2021
.L2017:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$16, %r8d
	leaq	.LC145(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	-40(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %r8d
	leaq	.LC146(%rip), %rdx
	movq	31(%rax), %rcx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	-40(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$8, %r8d
	leaq	.LC147(%rip), %rdx
	movq	7(%rax), %rcx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	-40(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$24, %r8d
	leaq	.LC148(%rip), %rdx
	movq	23(%rax), %rcx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2015:
	.cfi_restore_state
	movq	%r14, %rdi
	movq	24(%r12), %r15
	call	_ZNK2v88internal18SharedFunctionInfo7GetCodeEv@PLT
	movl	43(%rax), %edi
	shrl	%edi
	andl	$31, %edi
	call	_ZN2v88internal4Code11Kind2StringENS1_4KindE@PLT
	leaq	.LC143(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	jmp	.L2020
	.p2align 4,,10
	.p2align 3
.L2021:
	movq	-1(%rcx), %rax
	cmpw	$136, 11(%rax)
	jne	.L2017
	movq	%rcx, %rsi
	leaq	.LC144(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	-40(%rbp), %rax
	movq	15(%rax), %rcx
	jmp	.L2017
	.cfi_endproc
.LFE19991:
	.size	_ZN2v88internal14V8HeapExplorer35ExtractSharedFunctionInfoReferencesEPNS0_9HeapEntryENS0_18SharedFunctionInfoE, .-_ZN2v88internal14V8HeapExplorer35ExtractSharedFunctionInfoReferencesEPNS0_9HeapEntryENS0_18SharedFunctionInfoE
	.section	.rodata._ZN2v88internal14V8HeapExplorer23ExtractScriptReferencesEPNS0_9HeapEntryENS0_6ScriptE.str1.1,"aMS",@progbits,1
.LC149:
	.string	"source"
.LC150:
	.string	"context_data"
.LC151:
	.string	"(script line ends)"
.LC152:
	.string	"line_ends"
	.section	.text._ZN2v88internal14V8HeapExplorer23ExtractScriptReferencesEPNS0_9HeapEntryENS0_6ScriptE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer23ExtractScriptReferencesEPNS0_9HeapEntryENS0_6ScriptE
	.type	_ZN2v88internal14V8HeapExplorer23ExtractScriptReferencesEPNS0_9HeapEntryENS0_6ScriptE, @function
_ZN2v88internal14V8HeapExplorer23ExtractScriptReferencesEPNS0_9HeapEntryENS0_6ScriptE:
.LFB19992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	7(%rdx), %rcx
	leaq	.LC149(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	15(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$16, %r8d
	leaq	.LC133(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	39(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$40, %r8d
	leaq	.LC150(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	55(%rbx), %rsi
	movq	%r12, %rdi
	leaq	.LC151(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	55(%rbx), %rcx
	addq	$8, %rsp
	movq	%r13, %rsi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movl	$56, %r8d
	popq	%r13
	leaq	.LC152(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	.cfi_endproc
.LFE19992:
	.size	_ZN2v88internal14V8HeapExplorer23ExtractScriptReferencesEPNS0_9HeapEntryENS0_6ScriptE, .-_ZN2v88internal14V8HeapExplorer23ExtractScriptReferencesEPNS0_9HeapEntryENS0_6ScriptE
	.section	.rodata._ZN2v88internal14V8HeapExplorer21ExtractCodeReferencesEPNS0_9HeapEntryENS0_4CodeE.str1.1,"aMS",@progbits,1
.LC153:
	.string	"(code relocation info)"
.LC154:
	.string	"relocation_info"
.LC155:
	.string	"(code deopt data)"
.LC156:
	.string	"deoptimization_data"
.LC157:
	.string	"(source position table)"
.LC158:
	.string	"source_position_table"
	.section	.text._ZN2v88internal14V8HeapExplorer21ExtractCodeReferencesEPNS0_9HeapEntryENS0_4CodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer21ExtractCodeReferencesEPNS0_9HeapEntryENS0_4CodeE
	.type	_ZN2v88internal14V8HeapExplorer21ExtractCodeReferencesEPNS0_9HeapEntryENS0_4CodeE, @function
_ZN2v88internal14V8HeapExplorer21ExtractCodeReferencesEPNS0_9HeapEntryENS0_4CodeE:
.LFB19996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	7(%rdx), %rsi
	leaq	.LC153(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	7(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$8, %r8d
	leaq	.LC154(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	15(%rbx), %rsi
	movq	%r12, %rdi
	leaq	.LC155(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	15(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$16, %r8d
	leaq	.LC156(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	23(%rbx), %rsi
	movq	%r12, %rdi
	leaq	.LC157(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	23(%rbx), %rcx
	addq	$8, %rsp
	movq	%r13, %rsi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movl	$24, %r8d
	popq	%r13
	leaq	.LC158(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	.cfi_endproc
.LFE19996:
	.size	_ZN2v88internal14V8HeapExplorer21ExtractCodeReferencesEPNS0_9HeapEntryENS0_4CodeE, .-_ZN2v88internal14V8HeapExplorer21ExtractCodeReferencesEPNS0_9HeapEntryENS0_4CodeE
	.section	.rodata._ZN2v88internal14V8HeapExplorer29ExtractFeedbackCellReferencesEPNS0_9HeapEntryENS0_12FeedbackCellE.str1.1,"aMS",@progbits,1
.LC159:
	.string	"(feedback cell)"
.LC160:
	.string	"value"
	.section	.text._ZN2v88internal14V8HeapExplorer29ExtractFeedbackCellReferencesEPNS0_9HeapEntryENS0_12FeedbackCellE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer29ExtractFeedbackCellReferencesEPNS0_9HeapEntryENS0_12FeedbackCellE
	.type	_ZN2v88internal14V8HeapExplorer29ExtractFeedbackCellReferencesEPNS0_9HeapEntryENS0_12FeedbackCellE, @function
_ZN2v88internal14V8HeapExplorer29ExtractFeedbackCellReferencesEPNS0_9HeapEntryENS0_12FeedbackCellE:
.LFB19998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	leaq	.LC159(%rip), %rdx
	movq	%rbx, %rsi
	subq	$8, %rsp
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	7(%rbx), %rcx
	addq	$8, %rsp
	movq	%r13, %rsi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movl	$8, %r8d
	popq	%r13
	leaq	.LC160(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	.cfi_endproc
.LFE19998:
	.size	_ZN2v88internal14V8HeapExplorer29ExtractFeedbackCellReferencesEPNS0_9HeapEntryENS0_12FeedbackCellE, .-_ZN2v88internal14V8HeapExplorer29ExtractFeedbackCellReferencesEPNS0_9HeapEntryENS0_12FeedbackCellE
	.section	.text._ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi
	.type	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi, @function
_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi:
.LFB20034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %cl
	je	.L2028
	movl	%r8d, %r13d
	movq	48(%rdi), %r8
	movq	%rcx, %r12
	movl	%edx, %r15d
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rdi, %rbx
	movq	%rsi, %r14
	movq	344(%r8), %rcx
	divq	%rcx
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2031
	movq	(%rax), %rsi
	movq	8(%rsi), %rdi
	jmp	.L2033
	.p2align 4,,10
	.p2align 3
.L2066:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L2031
	movq	8(%rsi), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L2031
.L2033:
	cmpq	%rdi, %r12
	jne	.L2066
	movq	16(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L2031
.L2040:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	testb	%al, %al
	jne	.L2067
.L2038:
	testl	%r13d, %r13d
	js	.L2028
	movl	%r13d, %ecx
	movq	232(%rbx), %rsi
	movl	$1, %edx
	sarl	$3, %ecx
	movslq	%ecx, %rax
	salq	%cl, %rdx
	shrq	$6, %rax
	orq	%rdx, (%rsi,%rax,8)
.L2028:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2068
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2067:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal14StringsStorage7GetNameEi@PLT
	addl	$1, 4(%r14)
	movq	-104(%rbp), %rdx
	leaq	-64(%rbp), %rcx
	movq	%rax, -80(%rbp)
	movq	16(%r14), %rax
	leaq	-84(%rbp), %rsi
	leaq	-72(%rbp), %r8
	movq	%rdx, -72(%rbp)
	leaq	-80(%rbp), %rdx
	leaq	296(%rax), %rdi
	movl	$3, -84(%rbp)
	movq	%r14, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	jmp	.L2038
	.p2align 4,,10
	.p2align 3
.L2031:
	movq	(%rbx), %rax
	movq	%r8, -112(%rbp)
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r8
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%rdx, 16(%rax)
	xorl	%edx, %edx
	movq	%r12, 8(%rax)
	movq	344(%r8), %r9
	movq	%r12, %rax
	divq	%r9
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2034
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2036
	.p2align 4,,10
	.p2align 3
.L2069:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2034
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r9
	cmpq	%rdx, %r10
	jne	.L2034
.L2036:
	cmpq	%rsi, %r12
	jne	.L2069
	movq	%rcx, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rcx
.L2039:
	movq	16(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L2028
	jmp	.L2040
	.p2align 4,,10
	.p2align 3
.L2034:
	leaq	336(%r8), %r9
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	movq	%r9, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %rcx
	jmp	.L2039
.L2068:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20034:
	.size	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi, .-_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi
	.section	.text._ZN2v88internal14V8HeapExplorer25ExtractInternalReferencesENS0_8JSObjectEPNS0_9HeapEntryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer25ExtractInternalReferencesENS0_8JSObjectEPNS0_9HeapEntryE
	.type	_ZN2v88internal14V8HeapExplorer25ExtractInternalReferencesENS0_8JSObjectEPNS0_9HeapEntryE, @function
_ZN2v88internal14V8HeapExplorer25ExtractInternalReferencesENS0_8JSObjectEPNS0_9HeapEntryE:
.LFB20016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	-1(%rsi), %r15
	movzbl	7(%r15), %r12d
	sall	$3, %r12d
	jne	.L2086
.L2070:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2086:
	.cfi_restore_state
	movq	%rdi, %r13
	movzwl	11(%r15), %edi
	movq	%rdx, %r14
	leaq	-1(%rsi), %rbx
	movl	$24, %ecx
	cmpw	$1057, %di
	je	.L2073
	movsbl	13(%r15), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
	movl	%eax, %ecx
.L2073:
	movzbl	7(%r15), %eax
	movzbl	8(%r15), %edx
	subl	%ecx, %r12d
	xorl	%r15d, %r15d
	sarl	$3, %r12d
	subl	%edx, %eax
	subl	%eax, %r12d
	movl	%r12d, -60(%rbp)
	testl	%r12d, %r12d
	jle	.L2070
	.p2align 4,,10
	.p2align 3
.L2074:
	movq	(%rbx), %rdx
	movl	$24, %eax
	movzwl	11(%rdx), %edi
	cmpw	$1057, %di
	je	.L2075
	movsbl	13(%rdx), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
.L2075:
	leal	0(,%r15,8), %r12d
	addl	%r12d, %eax
	cltq
	movq	(%rax,%rbx), %rcx
	movq	(%rbx), %rax
	movzwl	11(%rax), %edi
	cmpw	$1057, %di
	je	.L2076
	movq	%rcx, -56(%rbp)
	movsbl	13(%rax), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
	movq	-56(%rbp), %rcx
	movl	%r15d, %edx
	movq	%r14, %rsi
	leal	(%r12,%rax), %r8d
	movq	%r13, %rdi
	addl	$1, %r15d
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi
	cmpl	%r15d, -60(%rbp)
	jne	.L2074
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2076:
	.cfi_restore_state
	movl	%r15d, %edx
	leal	24(%r12), %r8d
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi
	addl	$1, %r15d
	cmpl	-60(%rbp), %r15d
	jne	.L2074
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20016:
	.size	_ZN2v88internal14V8HeapExplorer25ExtractInternalReferencesENS0_8JSObjectEPNS0_9HeapEntryE, .-_ZN2v88internal14V8HeapExplorer25ExtractInternalReferencesENS0_8JSObjectEPNS0_9HeapEntryE
	.section	.text._ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	.type	_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi, @function
_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi:
.LFB20036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %cl
	je	.L2087
	movl	%r8d, %ebx
	movq	48(%rdi), %r8
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rdi, %r14
	movq	%rsi, %r15
	movq	344(%r8), %rcx
	divq	%rcx
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2090
	movq	(%rax), %rsi
	movq	8(%rsi), %rdi
	jmp	.L2092
	.p2align 4,,10
	.p2align 3
.L2125:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L2090
	movq	8(%rsi), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L2090
.L2092:
	cmpq	%rdi, %r12
	jne	.L2125
	movq	16(%rsi), %r9
	testq	%r9, %r9
	je	.L2090
.L2099:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	testb	%al, %al
	jne	.L2126
.L2097:
	testl	%ebx, %ebx
	js	.L2087
	sarl	$3, %ebx
	movq	232(%r14), %rsi
	movl	$1, %edx
	movslq	%ebx, %rax
	movq	%rax, %rcx
	shrq	$6, %rax
	salq	%cl, %rdx
	orq	%rdx, (%rsi,%rax,8)
.L2087:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2127
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2126:
	.cfi_restore_state
	movq	16(%r15), %rax
	leaq	-64(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	movq	%r13, -80(%rbp)
	addl	$1, 4(%r15)
	leaq	-84(%rbp), %rsi
	leaq	-72(%rbp), %r8
	leaq	296(%rax), %rdi
	movl	$6, -84(%rbp)
	movq	%r9, -72(%rbp)
	movq	%r15, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	jmp	.L2097
	.p2align 4,,10
	.p2align 3
.L2090:
	movq	(%r14), %rax
	movq	%r8, -112(%rbp)
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r8
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%rdx, 16(%rax)
	xorl	%edx, %edx
	movq	%r12, 8(%rax)
	movq	344(%r8), %r9
	movq	%r12, %rax
	divq	%r9
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2093
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2095
	.p2align 4,,10
	.p2align 3
.L2128:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2093
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r9
	cmpq	%rdx, %r10
	jne	.L2093
.L2095:
	cmpq	%rsi, %r12
	jne	.L2128
	movq	%rcx, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rcx
.L2098:
	movq	16(%rcx), %r9
	testq	%r9, %r9
	je	.L2087
	jmp	.L2099
	.p2align 4,,10
	.p2align 3
.L2093:
	leaq	336(%r8), %r9
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	movq	%r9, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %rcx
	jmp	.L2098
.L2127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20036:
	.size	_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi, .-_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	.section	.rodata._ZN2v88internal14V8HeapExplorer31ExtractFeedbackVectorReferencesEPNS0_9HeapEntryENS0_14FeedbackVectorE.str1.1,"aMS",@progbits,1
.LC161:
	.string	"optimized code"
	.section	.text._ZN2v88internal14V8HeapExplorer31ExtractFeedbackVectorReferencesEPNS0_9HeapEntryENS0_14FeedbackVectorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer31ExtractFeedbackVectorReferencesEPNS0_9HeapEntryENS0_14FeedbackVectorE
	.type	_ZN2v88internal14V8HeapExplorer31ExtractFeedbackVectorReferencesEPNS0_9HeapEntryENS0_14FeedbackVectorE, @function
_ZN2v88internal14V8HeapExplorer31ExtractFeedbackVectorReferencesEPNS0_9HeapEntryENS0_14FeedbackVectorE:
.LFB20010:
	.cfi_startproc
	endbr64
	movq	15(%rdx), %rcx
	movq	%rcx, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	je	.L2131
.L2129:
	ret
	.p2align 4,,10
	.p2align 3
.L2131:
	cmpl	$3, %ecx
	je	.L2129
	andq	$-3, %rcx
	movl	$16, %r8d
	leaq	.LC161(%rip), %rdx
	jmp	_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	.cfi_endproc
.LFE20010:
	.size	_ZN2v88internal14V8HeapExplorer31ExtractFeedbackVectorReferencesEPNS0_9HeapEntryENS0_14FeedbackVectorE, .-_ZN2v88internal14V8HeapExplorer31ExtractFeedbackVectorReferencesEPNS0_9HeapEntryENS0_14FeedbackVectorE
	.section	.rodata._ZN2v88internal14V8HeapExplorer20ExtractMapReferencesEPNS0_9HeapEntryENS0_3MapE.str1.1,"aMS",@progbits,1
.LC162:
	.string	"transition"
.LC163:
	.string	"(prototype transitions)"
.LC164:
	.string	"(transition array)"
.LC165:
	.string	"transitions"
.LC166:
	.string	"(transition)"
.LC167:
	.string	"prototype_info"
.LC168:
	.string	"(map descriptors)"
.LC169:
	.string	"descriptors"
.LC170:
	.string	"prototype"
.LC171:
	.string	"layout_descriptor"
.LC172:
	.string	"(back pointer)"
.LC173:
	.string	"back_pointer"
.LC174:
	.string	"(constructor function data)"
.LC175:
	.string	"constructor_function_data"
.LC176:
	.string	"constructor"
.LC177:
	.string	"(dependent code)"
.LC178:
	.string	"dependent_code"
	.section	.text._ZN2v88internal14V8HeapExplorer20ExtractMapReferencesEPNS0_9HeapEntryENS0_3MapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer20ExtractMapReferencesEPNS0_9HeapEntryENS0_3MapE
	.type	_ZN2v88internal14V8HeapExplorer20ExtractMapReferencesEPNS0_9HeapEntryENS0_3MapE, @function
_ZN2v88internal14V8HeapExplorer20ExtractMapReferencesEPNS0_9HeapEntryENS0_3MapE:
.LFB19990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$16, %rsp
	movq	71(%rdx), %r14
	movq	%r14, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	je	.L2151
	cmpq	$1, %rax
	je	.L2152
.L2135:
	movq	39(%rbx), %r14
	leaq	.LC168(%rip), %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$40, %r8d
	leaq	.LC169(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	23(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$24, %r8d
	leaq	.LC170(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	47(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$48, %r8d
	leaq	.LC171(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	31(%rbx), %rcx
	testb	$1, %cl
	jne	.L2153
.L2143:
	movl	$32, %r8d
	leaq	.LC176(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
.L2145:
	movq	55(%rbx), %rsi
	movq	%r12, %rdi
	leaq	.LC177(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	55(%rbx), %rcx
	addq	$16, %rsp
	movq	%r13, %rsi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movl	$56, %r8d
	popq	%r13
	leaq	.LC178(%rip), %rdx
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	.p2align 4,,10
	.p2align 3
.L2151:
	.cfi_restore_state
	cmpl	$3, %r14d
	je	.L2135
	movq	%r14, %rcx
	movl	$72, %r8d
	leaq	.LC162(%rip), %rdx
	andq	$-3, %rcx
	call	_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	jmp	.L2135
	.p2align 4,,10
	.p2align 3
.L2153:
	movq	-1(%rcx), %rax
	cmpw	$68, 11(%rax)
	je	.L2154
	movq	-1(%rcx), %rax
	cmpw	$88, 11(%rax)
	jne	.L2143
	movq	%rcx, %rsi
	movq	%r12, %rdi
	leaq	.LC174(%rip), %rdx
	movq	%rcx, -40(%rbp)
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	-40(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %r8d
	leaq	.LC175(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	jmp	.L2145
	.p2align 4,,10
	.p2align 3
.L2152:
	movq	-1(%r14), %rax
	cmpw	$149, 11(%rax)
	jne	.L2137
	cmpw	$1024, 11(%rdx)
	ja	.L2155
.L2139:
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	.LC164(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$72, %r8d
	leaq	.LC165(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	jmp	.L2135
	.p2align 4,,10
	.p2align 3
.L2154:
	movq	%rcx, %rsi
	movq	%r12, %rdi
	leaq	.LC172(%rip), %rdx
	movq	%rcx, -40(%rbp)
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	-40(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %r8d
	leaq	.LC173(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	jmp	.L2145
	.p2align 4,,10
	.p2align 3
.L2137:
	movq	-1(%r14), %rax
	cmpw	$103, 11(%rax)
	je	.L2141
	movq	-1(%r14), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L2156
.L2141:
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	.LC166(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$72, %r8d
	leaq	.LC162(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	jmp	.L2135
	.p2align 4,,10
	.p2align 3
.L2156:
	movl	15(%rdx), %eax
	testl	$1048576, %eax
	je	.L2135
	movq	%r14, %rsi
	leaq	.LC167(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$72, %r8d
	leaq	.LC167(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	jmp	.L2135
	.p2align 4,,10
	.p2align 3
.L2155:
	movq	15(%r14), %rax
	testq	%rax, %rax
	je	.L2139
	movq	15(%r14), %rsi
	leaq	.LC163(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	jmp	.L2139
	.cfi_endproc
.LFE19990:
	.size	_ZN2v88internal14V8HeapExplorer20ExtractMapReferencesEPNS0_9HeapEntryENS0_3MapE, .-_ZN2v88internal14V8HeapExplorer20ExtractMapReferencesEPNS0_9HeapEntryENS0_3MapE
	.section	.text._ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi
	.type	_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi, @function
_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi:
.LFB20037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %cl
	je	.L2157
	movl	%r8d, %r13d
	movq	48(%rdi), %r8
	movq	%rcx, %r12
	movl	%edx, %r15d
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rdi, %rbx
	movq	%rsi, %r14
	movq	344(%r8), %rcx
	divq	%rcx
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2160
	movq	(%rax), %rsi
	movq	8(%rsi), %rdi
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2195:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L2160
	movq	8(%rsi), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L2160
.L2162:
	cmpq	%rdi, %r12
	jne	.L2195
	movq	16(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L2160
.L2169:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	testb	%al, %al
	jne	.L2196
.L2167:
	testl	%r13d, %r13d
	js	.L2157
	movl	%r13d, %ecx
	movq	232(%rbx), %rsi
	movl	$1, %edx
	sarl	$3, %ecx
	movslq	%ecx, %rax
	salq	%cl, %rdx
	shrq	$6, %rax
	orq	%rdx, (%rsi,%rax,8)
.L2157:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2197
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2196:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	movl	%r15d, %edx
	leaq	.LC27(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal14StringsStorage12GetFormattedEPKcz@PLT
	addl	$1, 4(%r14)
	movq	-104(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	movq	%rax, -80(%rbp)
	movq	16(%r14), %rax
	leaq	-84(%rbp), %rsi
	leaq	-72(%rbp), %r8
	movq	%rcx, -72(%rbp)
	leaq	-64(%rbp), %rcx
	leaq	296(%rax), %rdi
	movl	$6, -84(%rbp)
	movq	%r14, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	jmp	.L2167
	.p2align 4,,10
	.p2align 3
.L2160:
	movq	(%rbx), %rax
	movq	%r8, -112(%rbp)
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r8
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%rdx, 16(%rax)
	xorl	%edx, %edx
	movq	%r12, 8(%rax)
	movq	344(%r8), %r9
	movq	%r12, %rax
	divq	%r9
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2163
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2165
	.p2align 4,,10
	.p2align 3
.L2198:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2163
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r9
	cmpq	%rdx, %r10
	jne	.L2163
.L2165:
	cmpq	%rsi, %r12
	jne	.L2198
	movq	%rcx, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rcx
.L2168:
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2157
	jmp	.L2169
	.p2align 4,,10
	.p2align 3
.L2163:
	leaq	336(%r8), %r9
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	movq	%r9, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %rcx
	jmp	.L2168
.L2197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20037:
	.size	_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi, .-_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi
	.section	.text._ZN2v88internal14V8HeapExplorer20SetPropertyReferenceEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer20SetPropertyReferenceEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci
	.type	_ZN2v88internal14V8HeapExplorer20SetPropertyReferenceEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci, @function
_ZN2v88internal14V8HeapExplorer20SetPropertyReferenceEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci:
.LFB20039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %cl
	je	.L2199
	movq	%rdi, %rbx
	movq	48(%rdi), %rdi
	movq	%rsi, %r12
	movq	%rdx, %r14
	movq	%rcx, %rsi
	movq	%rbx, %rdx
	movl	%r9d, %r13d
	movq	%r8, %r15
	call	_ZN2v88internal21HeapSnapshotGenerator14FindOrAddEntryEPvPNS0_20HeapEntriesAllocatorE
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L2199
	movq	-1(%r14), %rax
	movl	$2, %r10d
	cmpw	$64, 11(%rax)
	je	.L2203
	movl	11(%r14), %eax
	xorl	%r10d, %r10d
	testl	%eax, %eax
	setle	%r10b
	addl	$2, %r10d
.L2203:
	testq	%r15, %r15
	je	.L2204
	movq	-1(%r14), %rax
	cmpw	$63, 11(%rax)
	jbe	.L2217
.L2204:
	movq	24(%rbx), %rdi
	movq	%r14, %rsi
	movq	%r9, -112(%rbp)
	leaq	-64(%rbp), %r14
	movl	%r10d, -104(%rbp)
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	movl	-104(%rbp), %r10d
	movq	-112(%rbp), %r9
	leaq	-72(%rbp), %r11
.L2206:
	addl	$1, 4(%r12)
	leaq	-80(%rbp), %rdx
	movq	%r11, %r8
	movq	%r14, %rcx
	movq	%rax, -80(%rbp)
	movq	16(%r12), %rax
	leaq	-84(%rbp), %rsi
	movl	%r10d, -84(%rbp)
	leaq	296(%rax), %rdi
	movq	%r9, -72(%rbp)
	movq	%r12, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	testl	%r13d, %r13d
	js	.L2199
	movl	%r13d, %ecx
	movq	232(%rbx), %rsi
	movl	$1, %edx
	sarl	$3, %ecx
	movslq	%ecx, %rax
	salq	%cl, %rdx
	shrq	$6, %rax
	orq	%rdx, (%rsi,%rax,8)
.L2199:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2218
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2217:
	.cfi_restore_state
	leaq	-72(%rbp), %r11
	movq	24(%rbx), %rax
	movq	%r14, -72(%rbp)
	leaq	-64(%rbp), %r14
	movq	%r11, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%r9, -128(%rbp)
	movl	%r10d, -120(%rbp)
	movq	%r11, -104(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-112(%rbp), %rax
	movq	-64(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal14StringsStorage12GetFormattedEPKcz@PLT
	movq	-64(%rbp), %rdi
	movq	-104(%rbp), %r11
	movl	-120(%rbp), %r10d
	movq	-128(%rbp), %r9
	testq	%rdi, %rdi
	je	.L2206
	movq	%r11, -128(%rbp)
	movq	%r9, -120(%rbp)
	movq	%rax, -112(%rbp)
	movl	%r10d, -104(%rbp)
	call	_ZdaPv@PLT
	movq	-128(%rbp), %r11
	movq	-120(%rbp), %r9
	movq	-112(%rbp), %rax
	movl	-104(%rbp), %r10d
	jmp	.L2206
.L2218:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20039:
	.size	_ZN2v88internal14V8HeapExplorer20SetPropertyReferenceEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci, .-_ZN2v88internal14V8HeapExplorer20SetPropertyReferenceEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci
	.section	.rodata._ZN2v88internal14V8HeapExplorer27ExtractAccessorPairPropertyEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEi.str1.1,"aMS",@progbits,1
.LC179:
	.string	"get %s"
.LC180:
	.string	"set %s"
	.section	.text._ZN2v88internal14V8HeapExplorer27ExtractAccessorPairPropertyEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer27ExtractAccessorPairPropertyEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEi
	.type	_ZN2v88internal14V8HeapExplorer27ExtractAccessorPairPropertyEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEi, @function
_ZN2v88internal14V8HeapExplorer27ExtractAccessorPairPropertyEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEi:
.LFB20014:
	.cfi_startproc
	endbr64
	testb	$1, %cl
	jne	.L2232
	ret
	.p2align 4,,10
	.p2align 3
.L2232:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	-1(%rcx), %rax
	movq	%rcx, %rbx
	cmpw	$79, 11(%rax)
	je	.L2233
.L2219:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2233:
	.cfi_restore_state
	movl	%r8d, %r9d
	xorl	%r8d, %r8d
	movq	%rdi, %r12
	movq	%rsi, %r13
	movq	%rdx, %r14
	call	_ZN2v88internal14V8HeapExplorer20SetPropertyReferenceEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci
	movq	7(%rbx), %rcx
	testb	$1, %cl
	jne	.L2223
.L2225:
	movl	$-1, %r9d
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC179(%rip), %r8
	call	_ZN2v88internal14V8HeapExplorer20SetPropertyReferenceEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci
.L2224:
	movq	15(%rbx), %rcx
	testb	$1, %cl
	jne	.L2226
.L2227:
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$-1, %r9d
	popq	%r12
	.cfi_restore 12
	leaq	.LC180(%rip), %r8
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer20SetPropertyReferenceEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci
	.p2align 4,,10
	.p2align 3
.L2223:
	.cfi_restore_state
	movq	-1(%rcx), %rax
	cmpw	$67, 11(%rax)
	jne	.L2225
	jmp	.L2224
	.p2align 4,,10
	.p2align 3
.L2226:
	movq	-1(%rcx), %rax
	cmpw	$67, 11(%rax)
	jne	.L2227
	jmp	.L2219
	.cfi_endproc
.LFE20014:
	.size	_ZN2v88internal14V8HeapExplorer27ExtractAccessorPairPropertyEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEi, .-_ZN2v88internal14V8HeapExplorer27ExtractAccessorPairPropertyEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEi
	.section	.text._ZN2v88internal14V8HeapExplorer34SetDataOrAccessorPropertyReferenceENS0_12PropertyKindEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer34SetDataOrAccessorPropertyReferenceENS0_12PropertyKindEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci
	.type	_ZN2v88internal14V8HeapExplorer34SetDataOrAccessorPropertyReferenceENS0_12PropertyKindEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci, @function
_ZN2v88internal14V8HeapExplorer34SetDataOrAccessorPropertyReferenceENS0_12PropertyKindEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci:
.LFB20038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r10d
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	%r8, %rcx
	movq	%r9, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	16(%rbp), %r9d
	cmpl	$1, %r10d
	je	.L2237
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer20SetPropertyReferenceEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci
	.p2align 4,,10
	.p2align 3
.L2237:
	.cfi_restore_state
	movl	%r9d, %r8d
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer27ExtractAccessorPairPropertyEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEi
	.cfi_endproc
.LFE20038:
	.size	_ZN2v88internal14V8HeapExplorer34SetDataOrAccessorPropertyReferenceENS0_12PropertyKindEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci, .-_ZN2v88internal14V8HeapExplorer34SetDataOrAccessorPropertyReferenceENS0_12PropertyKindEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci
	.section	.text._ZN2v88internal14V8HeapExplorer25ExtractPropertyReferencesENS0_8JSObjectEPNS0_9HeapEntryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer25ExtractPropertyReferencesENS0_8JSObjectEPNS0_9HeapEntryE
	.type	_ZN2v88internal14V8HeapExplorer25ExtractPropertyReferencesENS0_8JSObjectEPNS0_9HeapEntryE, @function
_ZN2v88internal14V8HeapExplorer25ExtractPropertyReferencesENS0_8JSObjectEPNS0_9HeapEntryE:
.LFB20013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	24(%rax), %rdi
	subq	$40, %rsp
	movq	%rdi, -64(%rbp)
	movq	24(%rax), %rdx
	movq	-1(%rsi), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	jne	.L2239
	movq	-1(%rsi), %rax
	movq	39(%rax), %rdi
	movq	%rdi, -56(%rbp)
	movq	-1(%rsi), %rax
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	je	.L2238
	subl	$1, %eax
	subq	$1, %rsi
	movq	%r15, -72(%rbp)
	leaq	31(%rdi), %r14
	leaq	(%rax,%rax,2), %rax
	leaq	55(%rdi,%rax,8), %r13
	movq	%r13, %r15
	movq	%rsi, %r13
	jmp	.L2241
	.p2align 4,,10
	.p2align 3
.L2288:
	movl	%eax, %edx
	shrl	$6, %edx
	andl	$7, %edx
	cmpl	$2, %edx
	je	.L2256
	cmpl	$1, %edx
	je	.L2256
	movq	-8(%r14), %r11
	movq	0(%r13), %r9
	movq	%r14, %rdx
	subq	-56(%rbp), %rdx
	movq	39(%r9), %rcx
	movq	(%rcx,%rdx), %rsi
	movzbl	7(%r9), %ecx
	movzbl	8(%r9), %edx
	movq	%rsi, %rdi
	shrq	$38, %rsi
	shrq	$51, %rdi
	subl	%edx, %ecx
	andl	$7, %esi
	andl	$1023, %edi
	cmpl	%ecx, %edi
	setl	%dl
	jl	.L2285
	subl	%ecx, %edi
	movl	$16, %r8d
	leal	16(,%rdi,8), %edi
.L2245:
	cmpl	$2, %esi
	jne	.L2286
	movl	$32768, %esi
.L2246:
	movzbl	%dl, %edx
	movslq	%ecx, %rcx
	movslq	%edi, %rdi
	movslq	%r8d, %r8
	salq	$14, %rdx
	salq	$17, %rcx
	orq	%rcx, %rdx
	salq	$27, %r8
	orq	%rdx, %rdi
	orq	%rdi, %r8
	orq	%r8, %rsi
	testl	$16384, %r8d
	je	.L2249
	movl	%esi, %r9d
	andl	$16376, %esi
	movq	(%rsi,%r13), %rcx
	andl	$16383, %r9d
.L2250:
	testb	$1, %al
	jne	.L2287
	xorl	%r8d, %r8d
	movq	%r11, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal14V8HeapExplorer20SetPropertyReferenceEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci
	.p2align 4,,10
	.p2align 3
.L2256:
	addq	$24, %r14
	cmpq	%r15, %r14
	je	.L2238
.L2241:
	movq	(%r14), %rax
	sarq	$32, %rax
	testb	$2, %al
	je	.L2288
	movq	8(%r14), %rcx
	movq	-8(%r14), %rdx
	testb	$1, %al
	jne	.L2289
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal14V8HeapExplorer20SetPropertyReferenceEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci
	addq	$24, %r14
	cmpq	%r15, %r14
	jne	.L2241
.L2238:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2239:
	.cfi_restore_state
	movq	-1(%rsi), %rcx
	leaq	-37592(%rdx), %rax
	cmpw	$1025, 11(%rcx)
	je	.L2290
	movq	7(%rsi), %r14
	testb	$1, %r14b
	je	.L2291
.L2268:
	movslq	35(%r14), %rdx
	testq	%rdx, %rdx
	jle	.L2238
	leal	(%rdx,%rdx,2), %edx
	leaq	-1(%r14), %r11
	movq	%rbx, -56(%rbp)
	addq	$55, %r14
	leal	64(,%rdx,8), %r15d
	movq	%r12, -64(%rbp)
	movl	$64, %r13d
	movq	%r11, %r12
	movl	%r15d, %ebx
	movq	%rax, %r15
	jmp	.L2275
	.p2align 4,,10
	.p2align 3
.L2271:
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	call	_ZN2v88internal14V8HeapExplorer20SetPropertyReferenceEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci
.L2273:
	addl	$24, %r13d
	addq	$24, %r14
	cmpl	%r13d, %ebx
	je	.L2238
.L2275:
	movq	(%r14), %rdx
	cmpq	%rdx, 96(%r15)
	je	.L2273
	cmpq	%rdx, 88(%r15)
	je	.L2273
	movl	%r13d, %ecx
	leal	8(%r13), %esi
	movabsq	$4294967296, %rax
	movq	(%rcx,%r12), %rcx
	movq	(%rsi,%r12), %rsi
	testq	%rax, %rsi
	je	.L2271
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	movl	$-1, %r8d
	call	_ZN2v88internal14V8HeapExplorer27ExtractAccessorPairPropertyEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEi
	jmp	.L2273
	.p2align 4,,10
	.p2align 3
.L2286:
	cmpb	$2, %sil
	jg	.L2247
	je	.L2248
.L2277:
	xorl	%esi, %esi
	jmp	.L2246
	.p2align 4,,10
	.p2align 3
.L2247:
	subl	$3, %esi
	cmpb	$1, %sil
	jbe	.L2277
.L2248:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2287:
	movl	%r9d, %r8d
	movq	%r11, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal14V8HeapExplorer27ExtractAccessorPairPropertyEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEi
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2249:
	movq	-72(%rbp), %rdi
	movq	7(%rdi), %rdx
	movq	-64(%rbp), %rdi
	movq	(%rdi), %rcx
	movq	%rcx, -80(%rbp)
	leaq	-37592(%rcx), %rcx
	testb	$1, %dl
	jne	.L2251
.L2253:
	movq	968(%rcx), %rdx
.L2252:
	movq	%rsi, %rcx
	sarl	$3, %esi
	movl	$-1, %r9d
	shrq	$30, %rcx
	andl	$2047, %esi
	andl	$15, %ecx
	subl	%ecx, %esi
	leal	16(,%rsi,8), %ecx
	movslq	%ecx, %rcx
	movq	-1(%rdx,%rcx), %rcx
	jmp	.L2250
	.p2align 4,,10
	.p2align 3
.L2285:
	movzbl	8(%r9), %r8d
	movzbl	8(%r9), %r9d
	addl	%r9d, %edi
	sall	$3, %r8d
	sall	$3, %edi
	jmp	.L2245
	.p2align 4,,10
	.p2align 3
.L2289:
	movl	$-1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal14V8HeapExplorer27ExtractAccessorPairPropertyEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEi
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2251:
	cmpq	288(%rcx), %rdx
	jne	.L2252
	jmp	.L2253
	.p2align 4,,10
	.p2align 3
.L2290:
	movq	7(%rsi), %rdx
	movslq	35(%rdx), %rcx
	testq	%rcx, %rcx
	jle	.L2238
	subl	$1, %ecx
	movq	%rbx, -56(%rbp)
	leaq	55(%rdx), %r13
	movabsq	$4294967296, %r14
	leaq	63(%rdx,%rcx,8), %r15
	movq	%r15, %rbx
	movq	%rax, %r15
	jmp	.L2266
	.p2align 4,,10
	.p2align 3
.L2263:
	movq	-56(%rbp), %rdi
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	movq	%r10, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal14V8HeapExplorer20SetPropertyReferenceEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci
.L2262:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L2238
.L2266:
	movq	0(%r13), %rdx
	cmpq	%rdx, 88(%r15)
	je	.L2262
	movq	96(%r15), %rax
	cmpq	%rax, 23(%rdx)
	je	.L2262
	movq	0(%r13), %rdx
	movq	23(%rdx), %rcx
	movq	7(%rdx), %r10
	testq	%r14, 15(%rdx)
	je	.L2263
	movq	-56(%rbp), %rdi
	movl	$-1, %r8d
	movq	%r10, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal14V8HeapExplorer27ExtractAccessorPairPropertyEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEi
	jmp	.L2262
	.p2align 4,,10
	.p2align 3
.L2291:
	movq	-36536(%rdx), %r14
	jmp	.L2268
	.cfi_endproc
.LFE20013:
	.size	_ZN2v88internal14V8HeapExplorer25ExtractPropertyReferencesENS0_8JSObjectEPNS0_9HeapEntryE, .-_ZN2v88internal14V8HeapExplorer25ExtractPropertyReferencesENS0_8JSObjectEPNS0_9HeapEntryE
	.section	.text._ZN2v88internal21NativeObjectsExplorer27IterateAndExtractReferencesEPNS0_21HeapSnapshotGeneratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21NativeObjectsExplorer27IterateAndExtractReferencesEPNS0_21HeapSnapshotGeneratorE
	.type	_ZN2v88internal21NativeObjectsExplorer27IterateAndExtractReferencesEPNS0_21HeapSnapshotGeneratorE, @function
_ZN2v88internal21NativeObjectsExplorer27IterateAndExtractReferencesEPNS0_21HeapSnapshotGeneratorE:
.LFB20097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal37FLAG_heap_profiler_use_embedder_graphE(%rip)
	movq	%rsi, 32(%rdi)
	je	.L2293
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	120(%rax), %rcx
	cmpq	%rcx, 112(%rax)
	je	.L2293
	leaq	-144(%rbp), %rax
	movq	(%rdi), %rsi
	leaq	_ZN2v813EmbedderGraph4Node10IsRootNodeEv(%rip), %r13
	movq	%rax, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	16+_ZTVN2v88internal17EmbedderGraphImplE(%rip), %rax
	movq	(%rbx), %rsi
	movq	$0, -104(%rbp)
	movq	%rax, -112(%rbp)
	movq	8(%rbx), %rax
	leaq	-112(%rbp), %rdx
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	(%rax), %rdi
	call	_ZN2v88internal12HeapProfiler18BuildEmbedderGraphEPNS0_7IsolateEPNS_13EmbedderGraphE@PLT
	movq	-96(%rbp), %rax
	movq	-104(%rbp), %r12
	movq	%rax, -184(%rbp)
	cmpq	%rax, %r12
	jne	.L2309
	jmp	.L2310
	.p2align 4,,10
	.p2align 3
.L2297:
	movq	32(%rax), %rax
	leaq	_ZN2v813EmbedderGraph4Node11WrapperNodeEv(%rip), %rcx
	cmpq	%rcx, %rax
	jne	.L2350
.L2300:
	addq	$8, %r12
	cmpq	%r12, -184(%rbp)
	je	.L2310
.L2309:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rdx
	cmpq	%r13, %rdx
	je	.L2297
	call	*%rdx
	testb	%al, %al
	je	.L2349
	movq	8(%rbx), %rax
	movq	(%r12), %rsi
	movq	%rbx, %rdi
	movq	8(%rax), %r14
	call	_ZN2v88internal21NativeObjectsExplorer25EntryForEmbedderGraphNodeEPNS_13EmbedderGraph4NodeE
	leaq	-152(%rbp), %rcx
	leaq	-172(%rbp), %rdx
	movq	%rax, %r8
	movl	4(%r14), %eax
	leaq	-168(%rbp), %rsi
	movl	$1, -168(%rbp)
	movq	%r8, -160(%rbp)
	leaq	-160(%rbp), %r8
	addl	$1, %eax
	movl	%eax, -172(%rbp)
	movl	%eax, 4(%r14)
	movq	16(%r14), %rax
	movq	%r14, -152(%rbp)
	leaq	296(%rax), %rdi
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERiPNS1_9HeapEntryERSA_EEEvDpOT_
.L2349:
	movq	(%r12), %rdi
	leaq	_ZN2v813EmbedderGraph4Node11WrapperNodeEv(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2300
.L2350:
	call	*%rax
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2300
	movq	%rbx, %rdi
	call	_ZN2v88internal21NativeObjectsExplorer25EntryForEmbedderGraphNodeEPNS_13EmbedderGraph4NodeE
	movq	(%r12), %rdi
	leaq	_ZN2v813EmbedderGraph4Node10NamePrefixEv(%rip), %rcx
	movq	16(%rbx), %r15
	movq	24(%rax), %r8
	movq	%rax, %r14
	movq	(%rdi), %rax
	movq	56(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L2351
.L2302:
	movq	%r8, -192(%rbp)
	call	*16(%rax)
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14StringsStorage7GetCopyEPKc@PLT
	movq	-192(%rbp), %r8
	movq	%rax, %r15
.L2304:
	movl	$47, %esi
	movq	%r8, %rdi
	call	strchr@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2305
	movq	16(%rbx), %rdi
	movq	%r15, %rdx
	leaq	.LC34(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal14StringsStorage12GetFormattedEPKcz@PLT
	movq	%rax, %r15
.L2305:
	movq	%r15, 24(%r14)
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%r13, %rax
	jne	.L2306
.L2308:
	movl	$8, %edx
.L2307:
	movzbl	(%r14), %eax
	addq	$8, %r12
	andl	$-16, %eax
	orl	%edx, %eax
	movb	%al, (%r14)
	cmpq	%r12, -184(%rbp)
	jne	.L2309
.L2310:
	movq	-72(%rbp), %r12
	movq	-80(%rbp), %r15
	leaq	-160(%rbp), %r13
	cmpq	%r12, %r15
	je	.L2296
	movq	%r13, %rax
	movq	%r12, -184(%rbp)
	movq	%r15, %r13
	movq	%rax, %r15
	jmp	.L2315
	.p2align 4,,10
	.p2align 3
.L2314:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal14StringsStorage7GetCopyEPKc@PLT
	movq	%r12, -160(%rbp)
	movq	%r15, %r8
	leaq	-152(%rbp), %rcx
	movq	%rax, -168(%rbp)
	leaq	-168(%rbp), %rdx
	leaq	-172(%rbp), %rsi
	movl	$3, -172(%rbp)
	movq	16(%r14), %rax
	addl	$1, 4(%r14)
	leaq	296(%rax), %rdi
	movq	%r14, -152(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
.L2312:
	addq	$24, %r13
	cmpq	%r13, -184(%rbp)
	je	.L2352
.L2315:
	movq	0(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal21NativeObjectsExplorer25EntryForEmbedderGraphNodeEPNS_13EmbedderGraph4NodeE
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2312
	movq	8(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal21NativeObjectsExplorer25EntryForEmbedderGraphNodeEPNS_13EmbedderGraph4NodeE
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2312
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	jne	.L2314
	movl	4(%r14), %eax
	movq	%r12, -160(%rbp)
	leaq	-152(%rbp), %rcx
	leaq	-172(%rbp), %rdx
	leaq	-168(%rbp), %rsi
	movq	%r15, %r8
	addq	$24, %r13
	movl	$1, -168(%rbp)
	addl	$1, %eax
	movl	%eax, -172(%rbp)
	movl	%eax, 4(%r14)
	movq	16(%r14), %rax
	movq	%r14, -152(%rbp)
	leaq	296(%rax), %rdi
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERiPNS1_9HeapEntryERSA_EEEvDpOT_
	cmpq	%r13, -184(%rbp)
	jne	.L2315
	.p2align 4,,10
	.p2align 3
.L2352:
	movq	-80(%rbp), %r12
.L2296:
	leaq	16+_ZTVN2v88internal17EmbedderGraphImplE(%rip), %rax
	movq	%rax, -112(%rbp)
	testq	%r12, %r12
	je	.L2316
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2316:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	cmpq	%r12, %r13
	je	.L2317
	.p2align 4,,10
	.p2align 3
.L2321:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2318
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r13, %r12
	jne	.L2321
.L2319:
	movq	-104(%rbp), %r12
.L2317:
	testq	%r12, %r12
	je	.L2322
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2322:
	movq	-208(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L2293:
	movq	$0, 32(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2353
	addq	$168, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2318:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L2321
	jmp	.L2319
	.p2align 4,,10
	.p2align 3
.L2351:
	movq	%r8, -200(%rbp)
	movq	%rdi, -192(%rbp)
	call	*%rdx
	movq	-192(%rbp), %rdi
	movq	-200(%rbp), %r8
	testq	%rax, %rax
	je	.L2354
	movq	%rax, -200(%rbp)
	movq	(%rdi), %rax
	movq	%r8, -192(%rbp)
	call	*16(%rax)
	movq	-200(%rbp), %rdx
	movq	%r15, %rdi
	leaq	.LC34(%rip), %rsi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal14StringsStorage12GetFormattedEPKcz@PLT
	movq	-192(%rbp), %r8
	movq	%rax, %r15
	jmp	.L2304
	.p2align 4,,10
	.p2align 3
.L2306:
	call	*%rax
	testb	%al, %al
	je	.L2308
	movl	$9, %edx
	jmp	.L2307
.L2354:
	movq	(%rdi), %rax
	jmp	.L2302
.L2353:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20097:
	.size	_ZN2v88internal21NativeObjectsExplorer27IterateAndExtractReferencesEPNS0_21HeapSnapshotGeneratorE, .-_ZN2v88internal21NativeObjectsExplorer27IterateAndExtractReferencesEPNS0_21HeapSnapshotGeneratorE
	.section	.text._ZN2v88internal14V8HeapExplorer19SetContextReferenceEPNS0_9HeapEntryENS0_6StringENS0_6ObjectEi.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal14V8HeapExplorer19SetContextReferenceEPNS0_9HeapEntryENS0_6StringENS0_6ObjectEi.constprop.0, @function
_ZN2v88internal14V8HeapExplorer19SetContextReferenceEPNS0_9HeapEntryENS0_6StringENS0_6ObjectEi.constprop.0:
.LFB27387:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %cl
	je	.L2355
	movl	%r8d, %r13d
	movq	48(%rdi), %r8
	movq	%rcx, %r12
	movq	%rdx, %r15
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rdi, %r14
	movq	%rsi, %rbx
	movq	344(%r8), %rcx
	divq	%rcx
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2358
	movq	(%rax), %rsi
	movq	8(%rsi), %rdi
	jmp	.L2360
	.p2align 4,,10
	.p2align 3
.L2389:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L2358
	movq	8(%rsi), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L2358
.L2360:
	cmpq	%rdi, %r12
	jne	.L2389
	movq	16(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L2358
.L2366:
	movq	24(%r14), %rdi
	movq	%r15, %rsi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	addl	$1, 4(%rbx)
	movq	-104(%rbp), %rdx
	leaq	-64(%rbp), %rcx
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	leaq	-84(%rbp), %rsi
	leaq	-80(%rbp), %r8
	movq	%rdx, -80(%rbp)
	leaq	-72(%rbp), %rdx
	leaq	296(%rax), %rdi
	movl	$0, -84(%rbp)
	movq	%rbx, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	testl	%r13d, %r13d
	js	.L2355
	movl	%r13d, %ecx
	movq	232(%r14), %rsi
	movl	$1, %edx
	sarl	$3, %ecx
	movslq	%ecx, %rax
	salq	%cl, %rdx
	shrq	$6, %rax
	orq	%rdx, (%rsi,%rax,8)
.L2355:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2390
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2358:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	%r8, -112(%rbp)
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r8
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%rdx, 16(%rax)
	xorl	%edx, %edx
	movq	%r12, 8(%rax)
	movq	344(%r8), %r9
	movq	%r12, %rax
	divq	%r9
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2361
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2363
	.p2align 4,,10
	.p2align 3
.L2391:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2361
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r9
	cmpq	%rdx, %r10
	jne	.L2361
.L2363:
	cmpq	%rsi, %r12
	jne	.L2391
	movq	%rcx, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rcx
.L2365:
	movq	16(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L2355
	jmp	.L2366
	.p2align 4,,10
	.p2align 3
.L2361:
	leaq	336(%r8), %r9
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	movq	%r9, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %rcx
	jmp	.L2365
.L2390:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27387:
	.size	_ZN2v88internal14V8HeapExplorer19SetContextReferenceEPNS0_9HeapEntryENS0_6StringENS0_6ObjectEi.constprop.0, .-_ZN2v88internal14V8HeapExplorer19SetContextReferenceEPNS0_9HeapEntryENS0_6StringENS0_6ObjectEi.constprop.0
	.section	.text._ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0, @function
_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0:
.LFB27385:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %cl
	je	.L2392
	movl	%r8d, %ebx
	movq	48(%rdi), %r8
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rdi, %r14
	movq	%rsi, %r15
	movq	344(%r8), %rcx
	divq	%rcx
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2395
	movq	(%rax), %rsi
	movq	8(%rsi), %rdi
	jmp	.L2397
	.p2align 4,,10
	.p2align 3
.L2430:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L2395
	movq	8(%rsi), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L2395
.L2397:
	cmpq	%rdi, %r12
	jne	.L2430
	movq	16(%rsi), %r9
	testq	%r9, %r9
	je	.L2395
.L2404:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	testb	%al, %al
	jne	.L2431
.L2402:
	testl	%ebx, %ebx
	js	.L2392
	sarl	$3, %ebx
	movq	232(%r14), %rsi
	movl	$1, %edx
	movslq	%ebx, %rax
	movq	%rax, %rcx
	shrq	$6, %rax
	salq	%cl, %rdx
	orq	%rdx, (%rsi,%rax,8)
.L2392:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2432
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2431:
	.cfi_restore_state
	movq	16(%r15), %rax
	leaq	-64(%rbp), %rcx
	leaq	-72(%rbp), %rdx
	movq	%r13, -72(%rbp)
	addl	$1, 4(%r15)
	leaq	-84(%rbp), %rsi
	leaq	-80(%rbp), %r8
	leaq	296(%rax), %rdi
	movl	$3, -84(%rbp)
	movq	%r9, -80(%rbp)
	movq	%r15, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	jmp	.L2402
	.p2align 4,,10
	.p2align 3
.L2395:
	movq	(%r14), %rax
	movq	%r8, -112(%rbp)
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r8
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%rdx, 16(%rax)
	xorl	%edx, %edx
	movq	%r12, 8(%rax)
	movq	344(%r8), %r9
	movq	%r12, %rax
	divq	%r9
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2398
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2400
	.p2align 4,,10
	.p2align 3
.L2433:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2398
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r9
	cmpq	%rdx, %r10
	jne	.L2398
.L2400:
	cmpq	%rsi, %r12
	jne	.L2433
	movq	%rcx, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rcx
.L2403:
	movq	16(%rcx), %r9
	testq	%r9, %r9
	je	.L2392
	jmp	.L2404
	.p2align 4,,10
	.p2align 3
.L2398:
	leaq	336(%r8), %r9
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	movq	%r9, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %rcx
	jmp	.L2403
.L2432:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27385:
	.size	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0, .-_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	.section	.rodata._ZN2v88internal14V8HeapExplorer24ExtractContextReferencesEPNS0_9HeapEntryENS0_7ContextE.str1.1,"aMS",@progbits,1
.LC181:
	.string	"scope_info"
.LC182:
	.string	"previous"
.LC183:
	.string	"extension"
.LC184:
	.string	"native_context"
.LC185:
	.string	"(context norm. map cache)"
.LC186:
	.string	"(context data)"
.LC187:
	.string	"optimized_code_list"
.LC188:
	.string	"deoptimized_code_list"
	.section	.text._ZN2v88internal14V8HeapExplorer24ExtractContextReferencesEPNS0_9HeapEntryENS0_7ContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer24ExtractContextReferencesEPNS0_9HeapEntryENS0_7ContextE
	.type	_ZN2v88internal14V8HeapExplorer24ExtractContextReferencesEPNS0_9HeapEntryENS0_7ContextE, @function
_ZN2v88internal14V8HeapExplorer24ExtractContextReferencesEPNS0_9HeapEntryENS0_7ContextE:
.LFB19989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -72(%rbp)
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movq	-1(%rdx), %rdx
	cmpw	$145, 11(%rdx)
	jne	.L2457
.L2436:
	movq	15(%rax), %rcx
	movl	$16, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC181(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	movq	-72(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$24, %r8d
	leaq	.LC182(%rip), %rdx
	movq	23(%rax), %rcx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	movq	-72(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %r8d
	leaq	.LC183(%rip), %rdx
	movq	31(%rax), %rcx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	movq	-72(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC184(%rip), %rdx
	movl	$40, %r8d
	movq	39(%rax), %rcx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	movq	-72(%rbp), %rax
	movq	-1(%rax), %rdx
	cmpw	$145, 11(%rdx)
	je	.L2458
.L2434:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2459
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2457:
	.cfi_restore_state
	leaq	-72(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal7Context22is_declaration_contextEv@PLT
	movl	%eax, %r8d
	movq	-72(%rbp), %rax
	testb	%r8b, %r8b
	je	.L2436
	movq	%r14, %rdi
	leaq	-64(%rbp), %r15
	call	_ZN2v88internal7Context10scope_infoEv@PLT
	movq	%rax, -64(%rbp)
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L2438
	movq	31(%rax), %rax
	sarq	$32, %rax
	movl	%eax, -76(%rbp)
	testq	%rax, %rax
	jle	.L2438
	movl	$48, %r14d
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L2439:
	movl	%ebx, %esi
	movq	%r15, %rdi
	addl	$1, %ebx
	call	_ZNK2v88internal9ScopeInfo16ContextLocalNameEi@PLT
	movl	%r14d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	$8, %r14
	movq	-9(%r14,%rax), %rcx
	call	_ZN2v88internal14V8HeapExplorer19SetContextReferenceEPNS0_9HeapEntryENS0_6StringENS0_6ObjectEi.constprop.0
	cmpl	-76(%rbp), %ebx
	jne	.L2439
.L2438:
	movq	%r15, %rdi
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	movl	%eax, %r8d
	movq	-72(%rbp), %rax
	testb	%r8b, %r8b
	je	.L2436
	movq	%r15, %rdi
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZNK2v88internal9ScopeInfo24FunctionContextSlotIndexENS0_6StringE@PLT
	movl	%eax, %edx
	movq	-72(%rbp), %rax
	testl	%edx, %edx
	js	.L2436
	leal	16(,%rdx,8), %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movslq	%r8d, %rdx
	movq	-1(%rdx,%rax), %rcx
	movq	%r14, %rdx
	call	_ZN2v88internal14V8HeapExplorer19SetContextReferenceEPNS0_9HeapEntryENS0_6StringENS0_6ObjectEi.constprop.0
	movq	-72(%rbp), %rax
	jmp	.L2436
.L2458:
	movq	863(%rax), %rsi
	leaq	.LC185(%rip), %rdx
	movq	%r12, %rdi
	leaq	_ZN2v88internalL20native_context_namesE(%rip), %rbx
	leaq	3792(%rbx), %r14
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.constprop.0
	movq	-72(%rbp), %rax
	leaq	.LC186(%rip), %rdx
	movq	%r12, %rdi
	movq	55(%rax), %rsi
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc.constprop.0
	.p2align 4,,10
	.p2align 3
.L2443:
	movl	(%rbx), %eax
	movq	8(%rbx), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	addq	$16, %rbx
	leal	16(,%rax,8), %r8d
	movq	-72(%rbp), %rax
	movslq	%r8d, %rcx
	movq	-1(%rcx,%rax), %rcx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	cmpq	%rbx, %r14
	jne	.L2443
	movq	-72(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1944, %r8d
	leaq	.LC187(%rip), %rdx
	movq	1943(%rax), %rcx
	call	_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	-72(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1952, %r8d
	leaq	.LC188(%rip), %rdx
	movq	1951(%rax), %rcx
	call	_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	jmp	.L2434
.L2459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19989:
	.size	_ZN2v88internal14V8HeapExplorer24ExtractContextReferencesEPNS0_9HeapEntryENS0_7ContextE, .-_ZN2v88internal14V8HeapExplorer24ExtractContextReferencesEPNS0_9HeapEntryENS0_7ContextE
	.section	.rodata._ZN2v88internal14V8HeapExplorer31ExtractAllocationSiteReferencesEPNS0_9HeapEntryENS0_14AllocationSiteE.str1.1,"aMS",@progbits,1
.LC189:
	.string	"transition_info"
.LC190:
	.string	"nested_site"
	.section	.text.unlikely._ZN2v88internal14V8HeapExplorer31ExtractAllocationSiteReferencesEPNS0_9HeapEntryENS0_14AllocationSiteE,"ax",@progbits
	.align 2
.LCOLDB191:
	.section	.text._ZN2v88internal14V8HeapExplorer31ExtractAllocationSiteReferencesEPNS0_9HeapEntryENS0_14AllocationSiteE,"ax",@progbits
.LHOTB191:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer31ExtractAllocationSiteReferencesEPNS0_9HeapEntryENS0_14AllocationSiteE
	.type	_ZN2v88internal14V8HeapExplorer31ExtractAllocationSiteReferencesEPNS0_9HeapEntryENS0_14AllocationSiteE, @function
_ZN2v88internal14V8HeapExplorer31ExtractAllocationSiteReferencesEPNS0_9HeapEntryENS0_14AllocationSiteE:
.LFB20000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	7(%rdx), %rcx
	leaq	.LC189(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	movq	15(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$16, %r8d
	leaq	.LC190(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	movq	23(%rbx), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	testb	%al, %al
	je	.L2462
	testb	$1, %r14b
	je	.L2463
	movq	48(%r12), %r10
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	344(%r10), %rdi
	divq	%rdi
	movq	336(%r10), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2464
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2466
	.p2align 4,,10
	.p2align 3
.L2491:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2464
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L2464
.L2466:
	cmpq	%rsi, %r14
	jne	.L2491
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L2464
.L2472:
	movq	24(%rax), %rdx
	cmpb	$0, (%rdx)
	jne	.L2462
	leaq	.LC177(%rip), %rdi
	movq	%rdi, 24(%rax)
.L2462:
	movq	23(%rbx), %rcx
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	movl	$24, %r8d
	leaq	.LC178(%rip), %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	.p2align 4,,10
	.p2align 3
.L2464:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r10, -56(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, %r15
	call	_Znwm@PLT
	movq	-56(%rbp), %r10
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%r14, 8(%rax)
	movq	%r15, 16(%rax)
	movq	344(%r10), %rsi
	movq	%r14, %rax
	divq	%rsi
	movq	336(%r10), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2467
	movq	(%rax), %r15
	movq	8(%r15), %rcx
	jmp	.L2469
	.p2align 4,,10
	.p2align 3
.L2492:
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.L2467
	movq	8(%r15), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L2467
.L2469:
	cmpq	%rcx, %r14
	jne	.L2492
	call	_ZdlPv@PLT
.L2471:
	movq	16(%r15), %rax
	jmp	.L2472
	.p2align 4,,10
	.p2align 3
.L2467:
	addq	$336, %r10
	movq	%rdi, %rcx
	movq	%r14, %rdx
	movq	%r9, %rsi
	movl	$1, %r8d
	movq	%r10, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %r15
	jmp	.L2471
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal14V8HeapExplorer31ExtractAllocationSiteReferencesEPNS0_9HeapEntryENS0_14AllocationSiteE
	.cfi_startproc
	.type	_ZN2v88internal14V8HeapExplorer31ExtractAllocationSiteReferencesEPNS0_9HeapEntryENS0_14AllocationSiteE.cold, @function
_ZN2v88internal14V8HeapExplorer31ExtractAllocationSiteReferencesEPNS0_9HeapEntryENS0_14AllocationSiteE.cold:
.LFSB20000:
.L2463:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	24, %rax
	ud2
	.cfi_endproc
.LFE20000:
	.section	.text._ZN2v88internal14V8HeapExplorer31ExtractAllocationSiteReferencesEPNS0_9HeapEntryENS0_14AllocationSiteE
	.size	_ZN2v88internal14V8HeapExplorer31ExtractAllocationSiteReferencesEPNS0_9HeapEntryENS0_14AllocationSiteE, .-_ZN2v88internal14V8HeapExplorer31ExtractAllocationSiteReferencesEPNS0_9HeapEntryENS0_14AllocationSiteE
	.section	.text.unlikely._ZN2v88internal14V8HeapExplorer31ExtractAllocationSiteReferencesEPNS0_9HeapEntryENS0_14AllocationSiteE
	.size	_ZN2v88internal14V8HeapExplorer31ExtractAllocationSiteReferencesEPNS0_9HeapEntryENS0_14AllocationSiteE.cold, .-_ZN2v88internal14V8HeapExplorer31ExtractAllocationSiteReferencesEPNS0_9HeapEntryENS0_14AllocationSiteE.cold
.LCOLDE191:
	.section	.text._ZN2v88internal14V8HeapExplorer31ExtractAllocationSiteReferencesEPNS0_9HeapEntryENS0_14AllocationSiteE
.LHOTE191:
	.section	.text.unlikely._ZN2v88internal14V8HeapExplorer29ExtractPropertyCellReferencesEPNS0_9HeapEntryENS0_12PropertyCellE,"ax",@progbits
	.align 2
.LCOLDB192:
	.section	.text._ZN2v88internal14V8HeapExplorer29ExtractPropertyCellReferencesEPNS0_9HeapEntryENS0_12PropertyCellE,"ax",@progbits
.LHOTB192:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer29ExtractPropertyCellReferencesEPNS0_9HeapEntryENS0_12PropertyCellE
	.type	_ZN2v88internal14V8HeapExplorer29ExtractPropertyCellReferencesEPNS0_9HeapEntryENS0_12PropertyCellE, @function
_ZN2v88internal14V8HeapExplorer29ExtractPropertyCellReferencesEPNS0_9HeapEntryENS0_12PropertyCellE:
.LFB19999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	23(%rdx), %rcx
	leaq	.LC160(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	movq	31(%rbx), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	testb	%al, %al
	je	.L2495
	testb	$1, %r14b
	je	.L2496
	movq	48(%r12), %r10
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	344(%r10), %rdi
	divq	%rdi
	movq	336(%r10), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2497
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2499
	.p2align 4,,10
	.p2align 3
.L2524:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2497
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L2497
.L2499:
	cmpq	%rsi, %r14
	jne	.L2524
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L2497
.L2505:
	movq	24(%rax), %rdx
	cmpb	$0, (%rdx)
	jne	.L2495
	leaq	.LC177(%rip), %rdi
	movq	%rdi, 24(%rax)
.L2495:
	movq	31(%rbx), %rcx
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	movl	$32, %r8d
	leaq	.LC178(%rip), %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	.p2align 4,,10
	.p2align 3
.L2497:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r10, -56(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, %r15
	call	_Znwm@PLT
	movq	-56(%rbp), %r10
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%r14, 8(%rax)
	movq	%r15, 16(%rax)
	movq	344(%r10), %rsi
	movq	%r14, %rax
	divq	%rsi
	movq	336(%r10), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2500
	movq	(%rax), %r15
	movq	8(%r15), %rcx
	jmp	.L2502
	.p2align 4,,10
	.p2align 3
.L2525:
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.L2500
	movq	8(%r15), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L2500
.L2502:
	cmpq	%rcx, %r14
	jne	.L2525
	call	_ZdlPv@PLT
.L2504:
	movq	16(%r15), %rax
	jmp	.L2505
	.p2align 4,,10
	.p2align 3
.L2500:
	addq	$336, %r10
	movq	%rdi, %rcx
	movq	%r14, %rdx
	movq	%r9, %rsi
	movl	$1, %r8d
	movq	%r10, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %r15
	jmp	.L2504
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal14V8HeapExplorer29ExtractPropertyCellReferencesEPNS0_9HeapEntryENS0_12PropertyCellE
	.cfi_startproc
	.type	_ZN2v88internal14V8HeapExplorer29ExtractPropertyCellReferencesEPNS0_9HeapEntryENS0_12PropertyCellE.cold, @function
_ZN2v88internal14V8HeapExplorer29ExtractPropertyCellReferencesEPNS0_9HeapEntryENS0_12PropertyCellE.cold:
.LFSB19999:
.L2496:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	24, %rax
	ud2
	.cfi_endproc
.LFE19999:
	.section	.text._ZN2v88internal14V8HeapExplorer29ExtractPropertyCellReferencesEPNS0_9HeapEntryENS0_12PropertyCellE
	.size	_ZN2v88internal14V8HeapExplorer29ExtractPropertyCellReferencesEPNS0_9HeapEntryENS0_12PropertyCellE, .-_ZN2v88internal14V8HeapExplorer29ExtractPropertyCellReferencesEPNS0_9HeapEntryENS0_12PropertyCellE
	.section	.text.unlikely._ZN2v88internal14V8HeapExplorer29ExtractPropertyCellReferencesEPNS0_9HeapEntryENS0_12PropertyCellE
	.size	_ZN2v88internal14V8HeapExplorer29ExtractPropertyCellReferencesEPNS0_9HeapEntryENS0_12PropertyCellE.cold, .-_ZN2v88internal14V8HeapExplorer29ExtractPropertyCellReferencesEPNS0_9HeapEntryENS0_12PropertyCellE.cold
.LCOLDE192:
	.section	.text._ZN2v88internal14V8HeapExplorer29ExtractPropertyCellReferencesEPNS0_9HeapEntryENS0_12PropertyCellE
.LHOTE192:
	.section	.text._ZN2v88internal14V8HeapExplorer30ExtractJSGlobalProxyReferencesEPNS0_9HeapEntryENS0_13JSGlobalProxyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer30ExtractJSGlobalProxyReferencesEPNS0_9HeapEntryENS0_13JSGlobalProxyE
	.type	_ZN2v88internal14V8HeapExplorer30ExtractJSGlobalProxyReferencesEPNS0_9HeapEntryENS0_13JSGlobalProxyE, @function
_ZN2v88internal14V8HeapExplorer30ExtractJSGlobalProxyReferencesEPNS0_9HeapEntryENS0_13JSGlobalProxyE:
.LFB19982:
	.cfi_startproc
	endbr64
	movq	23(%rdx), %rcx
	movl	$24, %r8d
	leaq	.LC184(%rip), %rdx
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	.cfi_endproc
.LFE19982:
	.size	_ZN2v88internal14V8HeapExplorer30ExtractJSGlobalProxyReferencesEPNS0_9HeapEntryENS0_13JSGlobalProxyE, .-_ZN2v88internal14V8HeapExplorer30ExtractJSGlobalProxyReferencesEPNS0_9HeapEntryENS0_13JSGlobalProxyE
	.section	.text._ZN2v88internal14V8HeapExplorer23ExtractSymbolReferencesEPNS0_9HeapEntryENS0_6SymbolE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer23ExtractSymbolReferencesEPNS0_9HeapEntryENS0_6SymbolE
	.type	_ZN2v88internal14V8HeapExplorer23ExtractSymbolReferencesEPNS0_9HeapEntryENS0_6SymbolE, @function
_ZN2v88internal14V8HeapExplorer23ExtractSymbolReferencesEPNS0_9HeapEntryENS0_6SymbolE:
.LFB19985:
	.cfi_startproc
	endbr64
	movq	15(%rdx), %rcx
	movl	$16, %r8d
	leaq	.LC133(%rip), %rdx
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	.cfi_endproc
.LFE19985:
	.size	_ZN2v88internal14V8HeapExplorer23ExtractSymbolReferencesEPNS0_9HeapEntryENS0_6SymbolE, .-_ZN2v88internal14V8HeapExplorer23ExtractSymbolReferencesEPNS0_9HeapEntryENS0_6SymbolE
	.section	.rodata._ZN2v88internal14V8HeapExplorer29ExtractJSCollectionReferencesEPNS0_9HeapEntryENS0_12JSCollectionE.str1.1,"aMS",@progbits,1
.LC193:
	.string	"table"
	.section	.text._ZN2v88internal14V8HeapExplorer29ExtractJSCollectionReferencesEPNS0_9HeapEntryENS0_12JSCollectionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer29ExtractJSCollectionReferencesEPNS0_9HeapEntryENS0_12JSCollectionE
	.type	_ZN2v88internal14V8HeapExplorer29ExtractJSCollectionReferencesEPNS0_9HeapEntryENS0_12JSCollectionE, @function
_ZN2v88internal14V8HeapExplorer29ExtractJSCollectionReferencesEPNS0_9HeapEntryENS0_12JSCollectionE:
.LFB19986:
	.cfi_startproc
	endbr64
	movq	23(%rdx), %rcx
	movl	$24, %r8d
	leaq	.LC193(%rip), %rdx
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	.cfi_endproc
.LFE19986:
	.size	_ZN2v88internal14V8HeapExplorer29ExtractJSCollectionReferencesEPNS0_9HeapEntryENS0_12JSCollectionE, .-_ZN2v88internal14V8HeapExplorer29ExtractJSCollectionReferencesEPNS0_9HeapEntryENS0_12JSCollectionE
	.section	.text._ZN2v88internal14V8HeapExplorer33ExtractJSWeakCollectionReferencesEPNS0_9HeapEntryENS0_16JSWeakCollectionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer33ExtractJSWeakCollectionReferencesEPNS0_9HeapEntryENS0_16JSWeakCollectionE
	.type	_ZN2v88internal14V8HeapExplorer33ExtractJSWeakCollectionReferencesEPNS0_9HeapEntryENS0_16JSWeakCollectionE, @function
_ZN2v88internal14V8HeapExplorer33ExtractJSWeakCollectionReferencesEPNS0_9HeapEntryENS0_16JSWeakCollectionE:
.LFB19987:
	.cfi_startproc
	endbr64
	movq	23(%rdx), %rcx
	movl	$24, %r8d
	leaq	.LC193(%rip), %rdx
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	.cfi_endproc
.LFE19987:
	.size	_ZN2v88internal14V8HeapExplorer33ExtractJSWeakCollectionReferencesEPNS0_9HeapEntryENS0_16JSWeakCollectionE, .-_ZN2v88internal14V8HeapExplorer33ExtractJSWeakCollectionReferencesEPNS0_9HeapEntryENS0_16JSWeakCollectionE
	.section	.text._ZN2v88internal14V8HeapExplorer21ExtractCellReferencesEPNS0_9HeapEntryENS0_4CellE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer21ExtractCellReferencesEPNS0_9HeapEntryENS0_4CellE
	.type	_ZN2v88internal14V8HeapExplorer21ExtractCellReferencesEPNS0_9HeapEntryENS0_4CellE, @function
_ZN2v88internal14V8HeapExplorer21ExtractCellReferencesEPNS0_9HeapEntryENS0_4CellE:
.LFB19997:
	.cfi_startproc
	endbr64
	movq	7(%rdx), %rcx
	movl	$8, %r8d
	leaq	.LC160(%rip), %rdx
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	.cfi_endproc
.LFE19997:
	.size	_ZN2v88internal14V8HeapExplorer21ExtractCellReferencesEPNS0_9HeapEntryENS0_4CellE, .-_ZN2v88internal14V8HeapExplorer21ExtractCellReferencesEPNS0_9HeapEntryENS0_4CellE
	.section	.rodata._ZN2v88internal14V8HeapExplorer44ExtractArrayBoilerplateDescriptionReferencesEPNS0_9HeapEntryENS0_27ArrayBoilerplateDescriptionE.str1.1,"aMS",@progbits,1
.LC194:
	.string	"constant_elements"
	.section	.text._ZN2v88internal14V8HeapExplorer44ExtractArrayBoilerplateDescriptionReferencesEPNS0_9HeapEntryENS0_27ArrayBoilerplateDescriptionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer44ExtractArrayBoilerplateDescriptionReferencesEPNS0_9HeapEntryENS0_27ArrayBoilerplateDescriptionE
	.type	_ZN2v88internal14V8HeapExplorer44ExtractArrayBoilerplateDescriptionReferencesEPNS0_9HeapEntryENS0_27ArrayBoilerplateDescriptionE, @function
_ZN2v88internal14V8HeapExplorer44ExtractArrayBoilerplateDescriptionReferencesEPNS0_9HeapEntryENS0_27ArrayBoilerplateDescriptionE:
.LFB20001:
	.cfi_startproc
	endbr64
	movq	15(%rdx), %rcx
	movl	$16, %r8d
	leaq	.LC194(%rip), %rdx
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	.cfi_endproc
.LFE20001:
	.size	_ZN2v88internal14V8HeapExplorer44ExtractArrayBoilerplateDescriptionReferencesEPNS0_9HeapEntryENS0_27ArrayBoilerplateDescriptionE, .-_ZN2v88internal14V8HeapExplorer44ExtractArrayBoilerplateDescriptionReferencesEPNS0_9HeapEntryENS0_27ArrayBoilerplateDescriptionE
	.section	.rodata._ZN2v88internal14V8HeapExplorer26ExtractJSPromiseReferencesEPNS0_9HeapEntryENS0_9JSPromiseE.str1.1,"aMS",@progbits,1
.LC195:
	.string	"reactions_or_result"
	.section	.text._ZN2v88internal14V8HeapExplorer26ExtractJSPromiseReferencesEPNS0_9HeapEntryENS0_9JSPromiseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer26ExtractJSPromiseReferencesEPNS0_9HeapEntryENS0_9JSPromiseE
	.type	_ZN2v88internal14V8HeapExplorer26ExtractJSPromiseReferencesEPNS0_9HeapEntryENS0_9JSPromiseE, @function
_ZN2v88internal14V8HeapExplorer26ExtractJSPromiseReferencesEPNS0_9HeapEntryENS0_9JSPromiseE:
.LFB20007:
	.cfi_startproc
	endbr64
	movq	23(%rdx), %rcx
	movl	$24, %r8d
	leaq	.LC195(%rip), %rdx
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	.cfi_endproc
.LFE20007:
	.size	_ZN2v88internal14V8HeapExplorer26ExtractJSPromiseReferencesEPNS0_9HeapEntryENS0_9JSPromiseE, .-_ZN2v88internal14V8HeapExplorer26ExtractJSPromiseReferencesEPNS0_9HeapEntryENS0_9JSPromiseE
	.section	.text._ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi.constprop.0, @function
_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi.constprop.0:
.LFB27384:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %cl
	je	.L2533
	movl	%r8d, %r13d
	movq	48(%rdi), %r8
	movq	%rcx, %r12
	movl	%edx, %r15d
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rdi, %rbx
	movq	%rsi, %r14
	movq	344(%r8), %rcx
	divq	%rcx
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2536
	movq	(%rax), %rsi
	movq	8(%rsi), %rdi
	jmp	.L2538
	.p2align 4,,10
	.p2align 3
.L2571:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L2536
	movq	8(%rsi), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L2536
.L2538:
	cmpq	%rdi, %r12
	jne	.L2571
	movq	16(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L2536
.L2545:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	testb	%al, %al
	jne	.L2572
.L2543:
	testl	%r13d, %r13d
	js	.L2533
	movl	%r13d, %ecx
	movq	232(%rbx), %rsi
	movl	$1, %edx
	sarl	$3, %ecx
	movslq	%ecx, %rax
	salq	%cl, %rdx
	shrq	$6, %rax
	orq	%rdx, (%rsi,%rax,8)
.L2533:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2573
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2572:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	movl	%r15d, %edx
	leaq	.LC27(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal14StringsStorage12GetFormattedEPKcz@PLT
	addl	$1, 4(%r14)
	movq	-104(%rbp), %rcx
	leaq	-72(%rbp), %rdx
	movq	%rax, -72(%rbp)
	movq	16(%r14), %rax
	leaq	-84(%rbp), %rsi
	leaq	-80(%rbp), %r8
	movq	%rcx, -80(%rbp)
	leaq	-64(%rbp), %rcx
	leaq	296(%rax), %rdi
	movl	$6, -84(%rbp)
	movq	%r14, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2536:
	movq	(%rbx), %rax
	movq	%r8, -112(%rbp)
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r8
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%rdx, 16(%rax)
	xorl	%edx, %edx
	movq	%r12, 8(%rax)
	movq	344(%r8), %r9
	movq	%r12, %rax
	divq	%r9
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2539
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2541
	.p2align 4,,10
	.p2align 3
.L2574:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2539
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r9
	cmpq	%rdx, %r10
	jne	.L2539
.L2541:
	cmpq	%rsi, %r12
	jne	.L2574
	movq	%rcx, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rcx
.L2544:
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2533
	jmp	.L2545
	.p2align 4,,10
	.p2align 3
.L2539:
	leaq	336(%r8), %r9
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	movq	%r9, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %rcx
	jmp	.L2544
.L2573:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27384:
	.size	_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi.constprop.0, .-_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi.constprop.0
	.section	.text._ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi.constprop.0, @function
_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi.constprop.0:
.LFB27383:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %cl
	je	.L2575
	movl	%r8d, %r13d
	movq	48(%rdi), %r8
	movq	%rcx, %r12
	movl	%edx, %r15d
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rdi, %rbx
	movq	%rsi, %r14
	movq	344(%r8), %rcx
	divq	%rcx
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2578
	movq	(%rax), %rsi
	movq	8(%rsi), %rdi
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L2613:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L2578
	movq	8(%rsi), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L2578
.L2580:
	cmpq	%rdi, %r12
	jne	.L2613
	movq	16(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L2578
.L2587:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	testb	%al, %al
	jne	.L2614
.L2585:
	testl	%r13d, %r13d
	js	.L2575
	movl	%r13d, %ecx
	movq	232(%rbx), %rsi
	movl	$1, %edx
	sarl	$3, %ecx
	movslq	%ecx, %rax
	salq	%cl, %rdx
	shrq	$6, %rax
	orq	%rdx, (%rsi,%rax,8)
.L2575:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2615
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2614:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal14StringsStorage7GetNameEi@PLT
	addl	$1, 4(%r14)
	movq	-104(%rbp), %rdx
	leaq	-64(%rbp), %rcx
	movq	%rax, -72(%rbp)
	movq	16(%r14), %rax
	leaq	-84(%rbp), %rsi
	leaq	-80(%rbp), %r8
	movq	%rdx, -80(%rbp)
	leaq	-72(%rbp), %rdx
	leaq	296(%rax), %rdi
	movl	$3, -84(%rbp)
	movq	%r14, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	jmp	.L2585
	.p2align 4,,10
	.p2align 3
.L2578:
	movq	(%rbx), %rax
	movq	%r8, -112(%rbp)
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r8
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%rdx, 16(%rax)
	xorl	%edx, %edx
	movq	%r12, 8(%rax)
	movq	344(%r8), %r9
	movq	%r12, %rax
	divq	%r9
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2581
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2583
	.p2align 4,,10
	.p2align 3
.L2616:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2581
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r9
	cmpq	%rdx, %r10
	jne	.L2581
.L2583:
	cmpq	%rsi, %r12
	jne	.L2616
	movq	%rcx, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rcx
.L2586:
	movq	16(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L2575
	jmp	.L2587
	.p2align 4,,10
	.p2align 3
.L2581:
	leaq	336(%r8), %r9
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	movq	%r9, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %rcx
	jmp	.L2586
.L2615:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27383:
	.size	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi.constprop.0, .-_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi.constprop.0
	.section	.rodata._ZN2v88internal14V8HeapExplorer32ExtractDescriptorArrayReferencesEPNS0_9HeapEntryENS0_15DescriptorArrayE.str1.1,"aMS",@progbits,1
.LC196:
	.string	"enum_cache"
	.section	.text._ZN2v88internal14V8HeapExplorer32ExtractDescriptorArrayReferencesEPNS0_9HeapEntryENS0_15DescriptorArrayE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer32ExtractDescriptorArrayReferencesEPNS0_9HeapEntryENS0_15DescriptorArrayE
	.type	_ZN2v88internal14V8HeapExplorer32ExtractDescriptorArrayReferencesEPNS0_9HeapEntryENS0_15DescriptorArrayE, @function
_ZN2v88internal14V8HeapExplorer32ExtractDescriptorArrayReferencesEPNS0_9HeapEntryENS0_15DescriptorArrayE:
.LFB20011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$24, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	15(%rdx), %rcx
	leaq	.LC196(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi.constprop.0
	movswl	7(%rbx), %eax
	leal	3(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	leaq	-1(%rbx,%rax), %r9
	addq	$23, %rbx
	subl	%ebx, %r15d
	cmpq	%rbx, %r9
	jbe	.L2617
	xorl	%r14d, %r14d
	jmp	.L2618
	.p2align 4,,10
	.p2align 3
.L2619:
	cmpq	$1, %rax
	je	.L2626
.L2620:
	addq	$8, %rbx
	addl	$1, %r14d
	cmpq	%rbx, %r9
	jbe	.L2617
.L2618:
	movq	(%rbx), %rcx
	leal	(%r15,%rbx), %r8d
	movq	%rcx, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	jne	.L2619
	cmpl	$3, %ecx
	je	.L2620
	movl	%r14d, %edx
	andq	$-3, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	addq	$8, %rbx
	addl	$1, %r14d
	call	_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi.constprop.0
	movq	-56(%rbp), %r9
	cmpq	%rbx, %r9
	ja	.L2618
.L2617:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2626:
	.cfi_restore_state
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi.constprop.0
	movq	-56(%rbp), %r9
	jmp	.L2620
	.cfi_endproc
.LFE20011:
	.size	_ZN2v88internal14V8HeapExplorer32ExtractDescriptorArrayReferencesEPNS0_9HeapEntryENS0_15DescriptorArrayE, .-_ZN2v88internal14V8HeapExplorer32ExtractDescriptorArrayReferencesEPNS0_9HeapEntryENS0_15DescriptorArrayE
	.section	.rodata._ZN2v88internal14V8HeapExplorer25ExtractJSObjectReferencesEPNS0_9HeapEntryENS0_8JSObjectE.str1.1,"aMS",@progbits,1
.LC197:
	.string	"(bound arguments)"
.LC198:
	.string	"bindings"
.LC199:
	.string	"bound_this"
.LC200:
	.string	"bound_function"
.LC201:
	.string	"bound_argument_%d"
.LC202:
	.string	"initial_map"
.LC203:
	.string	"(function feedback cell)"
.LC204:
	.string	"feedback_cell"
.LC205:
	.string	"(shared function info)"
.LC206:
	.string	"shared"
.LC207:
	.string	"(context)"
.LC208:
	.string	"code"
.LC209:
	.string	"global_proxy"
.LC210:
	.string	"buffer"
.LC211:
	.string	"(object properties)"
.LC212:
	.string	"properties"
.LC213:
	.string	"(object elements)"
.LC214:
	.string	"elements"
	.section	.text._ZN2v88internal14V8HeapExplorer25ExtractJSObjectReferencesEPNS0_9HeapEntryENS0_8JSObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer25ExtractJSObjectReferencesEPNS0_9HeapEntryENS0_8JSObjectE
	.type	_ZN2v88internal14V8HeapExplorer25ExtractJSObjectReferencesEPNS0_9HeapEntryENS0_8JSObjectE, @function
_ZN2v88internal14V8HeapExplorer25ExtractJSObjectReferencesEPNS0_9HeapEntryENS0_8JSObjectE:
.LFB19983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movq	%rsi, %rdx
	movq	%rbx, %rsi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal14V8HeapExplorer25ExtractPropertyReferencesENS0_8JSObjectEPNS0_9HeapEntryE
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14V8HeapExplorer24ExtractElementReferencesENS0_8JSObjectEPNS0_9HeapEntryE
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14V8HeapExplorer25ExtractInternalReferencesENS0_8JSObjectEPNS0_9HeapEntryE
	movq	8(%r12), %rax
	leaq	-37592(%rax), %r14
	testb	$1, %bl
	jne	.L2689
.L2628:
	movq	-1(%rbx), %rax
	movq	23(%rax), %rcx
.L2629:
	movq	3096(%r14), %rdx
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$-1, %r9d
	call	_ZN2v88internal14V8HeapExplorer20SetPropertyReferenceEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci
	movq	-1(%rbx), %rax
	cmpw	$1104, 11(%rax)
	je	.L2690
	movq	-1(%rbx), %rax
	cmpw	$1105, 11(%rax)
	movq	-1(%rbx), %rax
	je	.L2691
	cmpw	$1025, 11(%rax)
	je	.L2692
	movq	-1(%rbx), %rax
	cmpw	$1087, 11(%rax)
	jne	.L2654
.L2655:
	movq	23(%rbx), %rcx
	movl	$24, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC210(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	jmp	.L2632
	.p2align 4,,10
	.p2align 3
.L2691:
	movzbl	13(%rax), %eax
	testb	%al, %al
	js	.L2693
.L2645:
	movq	23(%rbx), %r14
	movq	39(%rbx), %rsi
	leaq	.LC203(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	39(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$40, %r8d
	leaq	.LC204(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	leaq	.LC205(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$24, %r8d
	leaq	.LC206(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	31(%rbx), %rsi
	leaq	.LC207(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	31(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %r8d
	leaq	.LC139(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	47(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$48, %r8d
	leaq	.LC208(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
.L2632:
	movq	7(%rbx), %rsi
	leaq	.LC211(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	7(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$8, %r8d
	leaq	.LC212(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	15(%rbx), %rsi
	leaq	.LC213(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	15(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$16, %r8d
	leaq	.LC214(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2694
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2690:
	.cfi_restore_state
	movq	39(%rbx), %rsi
	leaq	.LC197(%rip), %rdx
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	39(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$40, %r8d
	leaq	.LC198(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	31(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %r8d
	leaq	.LC199(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	23(%rbx), %rcx
	movq	%r12, %rdi
	movq	%r13, %rsi
	leaq	.LC200(%rip), %rdx
	movl	$24, %r8d
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	39(%rbx), %rax
	leaq	7(%rax), %rdx
	leaq	15(%rax), %rdi
	movl	11(%rax), %eax
	movq	%rdx, -104(%rbp)
	leaq	-80(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	testl	%eax, %eax
	jle	.L2632
	movq	%rbx, -120(%rbp)
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L2642:
	movq	24(%r12), %rdi
	movl	%r15d, %edx
	leaq	.LC201(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal14StringsStorage12GetFormattedEPKcz@PLT
	movq	(%rbx), %r14
	movq	%rax, %r10
	testb	$1, %r14b
	je	.L2634
	movq	48(%r12), %r8
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	344(%r8), %rdi
	divq	%rdi
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r11
	testq	%rax, %rax
	je	.L2635
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2637
	.p2align 4,,10
	.p2align 3
.L2695:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2635
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r11
	jne	.L2635
.L2637:
	cmpq	%rsi, %r14
	jne	.L2695
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L2635
.L2657:
	movq	%rax, -80(%rbp)
	movq	16(%r13), %rax
	leaq	-64(%rbp), %rcx
	leaq	-72(%rbp), %rdx
	addl	$1, 4(%r13)
	movq	-112(%rbp), %r8
	leaq	-84(%rbp), %rsi
	leaq	296(%rax), %rdi
	movl	$5, -84(%rbp)
	movq	%r10, -72(%rbp)
	movq	%r13, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
.L2634:
	movq	-104(%rbp), %rax
	addl	$1, %r15d
	addq	$8, %rbx
	cmpl	%r15d, 4(%rax)
	jg	.L2642
	movq	-120(%rbp), %rbx
	jmp	.L2632
	.p2align 4,,10
	.p2align 3
.L2635:
	movq	(%r12), %rax
	movq	%r8, -144(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r10, -136(%rbp)
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, -128(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rdx
	movq	-144(%rbp), %r8
	movq	%r14, 8(%rax)
	movq	%rax, %r9
	movq	-136(%rbp), %r10
	movq	%rdx, 16(%rax)
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	344(%r8), %r11
	movq	%r14, %rax
	divq	%r11
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rsi
	testq	%rax, %rax
	je	.L2638
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L2640
	.p2align 4,,10
	.p2align 3
.L2696:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2638
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%r11
	cmpq	%rdx, %rsi
	jne	.L2638
.L2640:
	cmpq	%rdi, %r14
	jne	.L2696
	movq	%r9, %rdi
	movq	%r10, -136(%rbp)
	movq	%rcx, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %r10
.L2656:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L2634
	jmp	.L2657
	.p2align 4,,10
	.p2align 3
.L2654:
	movq	-1(%rbx), %rax
	cmpw	$1086, 11(%rax)
	jne	.L2632
	jmp	.L2655
	.p2align 4,,10
	.p2align 3
.L2693:
	movq	55(%rbx), %r15
	cmpq	%r15, 96(%r14)
	je	.L2645
	testb	$1, %r15b
	jne	.L2697
.L2646:
	movq	3104(%r14), %rdx
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	$56, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal14V8HeapExplorer20SetPropertyReferenceEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci
	jmp	.L2645
	.p2align 4,,10
	.p2align 3
.L2692:
	movq	23(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$24, %r8d
	leaq	.LC184(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	31(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %r8d
	leaq	.LC209(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	jmp	.L2632
	.p2align 4,,10
	.p2align 3
.L2689:
	movq	-1(%rbx), %rax
	cmpw	$1024, 11(%rax)
	jne	.L2628
	movq	104(%r14), %rcx
	jmp	.L2629
	.p2align 4,,10
	.p2align 3
.L2638:
	leaq	336(%r8), %rdi
	movq	%r9, %rcx
	movl	$1, %r8d
	movq	%r14, %rdx
	movq	%r10, -128(%rbp)
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	-128(%rbp), %r10
	movq	%rax, %rcx
	jmp	.L2656
.L2697:
	movq	-1(%r15), %rax
	cmpw	$68, 11(%rax)
	jne	.L2646
	movq	-1(%rbx), %rax
	movzbl	13(%rax), %eax
	testb	$1, %al
	je	.L2647
	movq	-1(%rbx), %rax
	movq	31(%rax), %rcx
	testb	$1, %cl
	jne	.L2649
.L2650:
	movq	3104(%r14), %rdx
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$-1, %r9d
	call	_ZN2v88internal14V8HeapExplorer20SetPropertyReferenceEPNS0_9HeapEntryENS0_4NameENS0_6ObjectEPKci
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$56, %r8d
	leaq	.LC202(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	jmp	.L2645
.L2649:
	movq	-1(%rcx), %rax
	cmpw	$68, 11(%rax)
	jne	.L2650
	movq	31(%rcx), %rcx
	testb	$1, %cl
	je	.L2650
	jmp	.L2649
	.p2align 4,,10
	.p2align 3
.L2647:
	movq	-1(%r15), %rax
	movq	%r15, %rcx
	cmpw	$68, 11(%rax)
	jne	.L2650
	movq	23(%r15), %rcx
	jmp	.L2650
.L2694:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19983:
	.size	_ZN2v88internal14V8HeapExplorer25ExtractJSObjectReferencesEPNS0_9HeapEntryENS0_8JSObjectE, .-_ZN2v88internal14V8HeapExplorer25ExtractJSObjectReferencesEPNS0_9HeapEntryENS0_8JSObjectE
	.section	.text._ZN2v88internal14V8HeapExplorer22SetUserGlobalReferenceENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer22SetUserGlobalReferenceENS0_6ObjectE
	.type	_ZN2v88internal14V8HeapExplorer22SetUserGlobalReferenceENS0_6ObjectE, @function
_ZN2v88internal14V8HeapExplorer22SetUserGlobalReferenceENS0_6ObjectE:
.LFB20043:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testb	$1, %sil
	je	.L2699
	movq	48(%rdi), %r14
	movq	%rsi, %rax
	xorl	%edx, %edx
	movq	%rsi, %r12
	movq	344(%r14), %rdi
	divq	%rdi
	movq	336(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L2700
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2702
	.p2align 4,,10
	.p2align 3
.L2728:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2700
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L2700
.L2702:
	cmpq	%rsi, %r12
	jne	.L2728
	movq	16(%rcx), %r13
	testq	%r13, %r13
	je	.L2700
.L2699:
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdi
	movq	8(%rax), %r12
	movl	4(%r12), %eax
	leal	1(%rax), %esi
	call	_ZN2v88internal14StringsStorage7GetNameEi@PLT
	movq	%r13, -64(%rbp)
	leaq	-48(%rbp), %rcx
	leaq	-56(%rbp), %rdx
	movq	%rax, -56(%rbp)
	movq	16(%r12), %rax
	leaq	-68(%rbp), %rsi
	leaq	-64(%rbp), %r8
	addl	$1, 4(%r12)
	leaq	296(%rax), %rdi
	movl	$5, -68(%rbp)
	movq	%r12, -48(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2729
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2700:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, %r13
	call	_Znwm@PLT
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%r12, 8(%rax)
	movq	%r13, 16(%rax)
	movq	344(%r14), %rsi
	movq	%r12, %rax
	divq	%rsi
	movq	336(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2703
	movq	(%rax), %r13
	movq	8(%r13), %rcx
	jmp	.L2705
	.p2align 4,,10
	.p2align 3
.L2730:
	movq	0(%r13), %r13
	testq	%r13, %r13
	je	.L2703
	movq	8(%r13), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L2703
.L2705:
	cmpq	%rcx, %r12
	jne	.L2730
	call	_ZdlPv@PLT
.L2706:
	movq	16(%r13), %r13
	jmp	.L2699
	.p2align 4,,10
	.p2align 3
.L2703:
	addq	$336, %r14
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r9, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %r13
	jmp	.L2706
.L2729:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20043:
	.size	_ZN2v88internal14V8HeapExplorer22SetUserGlobalReferenceENS0_6ObjectE, .-_ZN2v88internal14V8HeapExplorer22SetUserGlobalReferenceENS0_6ObjectE
	.section	.text._ZN2v88internal14V8HeapExplorer21SetGcSubrootReferenceENS0_4RootEPKcbNS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer21SetGcSubrootReferenceENS0_4RootEPKcbNS0_6ObjectE
	.type	_ZN2v88internal14V8HeapExplorer21SetGcSubrootReferenceENS0_4RootEPKcbNS0_6ObjectE, @function
_ZN2v88internal14V8HeapExplorer21SetGcSubrootReferenceENS0_4RootEPKcbNS0_6ObjectE:
.LFB20045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %r8b
	je	.L2731
	movq	%rdi, %r12
	movq	48(%rdi), %rdi
	movslq	%esi, %r13
	movq	%rdx, %r15
	movq	%r8, %rsi
	movq	%r12, %rdx
	movl	%ecx, %r14d
	movq	%r8, %rbx
	call	_ZN2v88internal21HeapSnapshotGenerator14FindOrAddEntryEPvPNS0_20HeapEntriesAllocatorE
	testq	%rax, %rax
	movq	%rax, -104(%rbp)
	je	.L2731
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14V8HeapExplorer22GetStrongGcSubrootNameENS0_6ObjectE
	cmpb	$1, %r14b
	movq	16(%r12), %rdx
	movq	-104(%rbp), %rcx
	sbbl	%esi, %esi
	andl	$-3, %esi
	movq	24(%rdx,%r13,8), %rdi
	addl	$6, %esi
	testq	%rax, %rax
	je	.L2734
	movq	%rcx, -72(%rbp)
	leaq	-80(%rbp), %rdx
	leaq	-64(%rbp), %rcx
	movq	%rax, -80(%rbp)
	movq	16(%rdi), %rax
	leaq	-72(%rbp), %r8
	addl	$1, 4(%rdi)
	leaq	296(%rax), %r9
	movl	%esi, -84(%rbp)
	leaq	-84(%rbp), %rsi
	movq	%rdi, -64(%rbp)
	movq	%r9, %rdi
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
.L2735:
	testb	%r14b, %r14b
	je	.L2743
	.p2align 4,,10
	.p2align 3
.L2731:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2744
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2743:
	.cfi_restore_state
	movq	-1(%rbx), %rax
	cmpw	$145, 11(%rax)
	jne	.L2731
	leaq	-64(%rbp), %r13
	movq	%rbx, -64(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	%rax, -72(%rbp)
	movq	-1(%rax), %rax
	cmpw	$1025, 11(%rax)
	jne	.L2731
	leaq	168(%r12), %rdi
	leaq	-72(%rbp), %rsi
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%rdi, -64(%rbp)
	call	_ZNSt10_HashtableIN2v88internal14JSGlobalObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	testb	%dl, %dl
	je	.L2731
	movq	-72(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14V8HeapExplorer22SetUserGlobalReferenceENS0_6ObjectE
	jmp	.L2731
	.p2align 4,,10
	.p2align 3
.L2734:
	movq	24(%r12), %r8
	movq	%r15, %rdx
	call	_ZN2v88internal9HeapEntry26SetNamedAutoIndexReferenceENS0_13HeapGraphEdge4TypeEPKcPS1_PNS0_14StringsStorageE
	jmp	.L2735
.L2744:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20045:
	.size	_ZN2v88internal14V8HeapExplorer21SetGcSubrootReferenceENS0_4RootEPKcbNS0_6ObjectE, .-_ZN2v88internal14V8HeapExplorer21SetGcSubrootReferenceENS0_4RootEPKcbNS0_6ObjectE
	.section	.text._ZN2v88internal24RootsReferencesExtractor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal24RootsReferencesExtractor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24RootsReferencesExtractor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal24RootsReferencesExtractor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal24RootsReferencesExtractor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB20024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r15
	movq	(%rcx), %r8
	cmpl	$12, %esi
	je	.L2748
.L2746:
	movzbl	16(%rbx), %ecx
	addq	$24, %rsp
	movq	%r13, %rdx
	movl	%r12d, %esi
	popq	%rbx
	movq	%r15, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer21SetGcSubrootReferenceENS0_4RootEPKcbNS0_6ObjectE
	.p2align 4,,10
	.p2align 3
.L2748:
	.cfi_restore_state
	movq	24(%r15), %rdi
	leaq	.LC123(%rip), %rsi
	xorl	%eax, %eax
	movq	%rcx, %r14
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal14StringsStorage12GetFormattedEPKcz@PLT
	movq	-56(%rbp), %r8
	movq	%r15, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	8(%rbx), %r15
	movq	(%r14), %r8
	jmp	.L2746
	.cfi_endproc
.LFE20024:
	.size	_ZN2v88internal24RootsReferencesExtractor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal24RootsReferencesExtractor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.rodata._ZN2v88internal14V8HeapExplorer35ExtractEphemeronHashTableReferencesEPNS0_9HeapEntryENS0_18EphemeronHashTableE.str1.1,"aMS",@progbits,1
.LC215:
	.string	"key %s in WeakMap"
	.section	.text._ZN2v88internal14V8HeapExplorer35ExtractEphemeronHashTableReferencesEPNS0_9HeapEntryENS0_18EphemeronHashTableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer35ExtractEphemeronHashTableReferencesEPNS0_9HeapEntryENS0_18EphemeronHashTableE
	.type	_ZN2v88internal14V8HeapExplorer35ExtractEphemeronHashTableReferencesEPNS0_9HeapEntryENS0_18EphemeronHashTableE, @function
_ZN2v88internal14V8HeapExplorer35ExtractEphemeronHashTableReferencesEPNS0_9HeapEntryENS0_18EphemeronHashTableE:
.LFB19988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	35(%rdx), %rax
	testq	%rax, %rax
	jle	.L2749
	leal	3(%rax,%rax), %eax
	movq	%rdi, %r14
	leaq	39(%rdx), %r13
	movl	$3, %ebx
	movl	%eax, -116(%rbp)
	movl	$40, %r15d
	.p2align 4,,10
	.p2align 3
.L2770:
	movq	0(%r13), %r11
	movq	8(%r13), %r12
	movl	%r15d, %r8d
	movl	%ebx, %edx
	movq	-104(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r11, %rcx
	movq	%r11, -112(%rbp)
	call	_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi.constprop.0
	movq	-104(%rbp), %rsi
	leal	1(%rbx), %edx
	leal	8(%r15), %r8d
	movq	%r12, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi.constprop.0
	movq	-112(%rbp), %r11
	xorl	%r10d, %r10d
	testb	$1, %r11b
	je	.L2751
	movq	48(%r14), %r8
	movq	%r11, %rax
	xorl	%edx, %edx
	movq	344(%r8), %rdi
	divq	%rdi
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2752
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2754
	.p2align 4,,10
	.p2align 3
.L2821:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2752
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L2752
.L2754:
	cmpq	%rsi, %r11
	jne	.L2821
	movq	16(%rcx), %r10
	testq	%r10, %r10
	je	.L2752
.L2751:
	testb	$1, %r12b
	je	.L2759
	movq	48(%r14), %r8
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	344(%r8), %rdi
	divq	%rdi
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r11
	testq	%rax, %rax
	je	.L2760
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2762
	.p2align 4,,10
	.p2align 3
.L2822:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2760
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r11
	jne	.L2760
.L2762:
	cmpq	%rsi, %r12
	jne	.L2822
	movq	16(%rcx), %r11
	testq	%r11, %r11
	je	.L2760
.L2772:
	testq	%r10, %r10
	je	.L2759
	testq	%r11, %r11
	movq	%r11, -112(%rbp)
	je	.L2759
	movq	24(%r10), %rdx
	movq	24(%r14), %rdi
	xorl	%eax, %eax
	movq	%r10, -128(%rbp)
	leaq	.LC215(%rip), %rsi
	call	_ZN2v88internal14StringsStorage12GetFormattedEPKcz@PLT
	movq	-128(%rbp), %r10
	movq	24(%r14), %rdi
	movq	%rax, %rcx
	movq	-112(%rbp), %r11
	movl	4(%r10), %eax
	testq	%rcx, %rcx
	leal	1(%rax), %edx
	je	.L2767
	leaq	.LC128(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal14StringsStorage12GetFormattedEPKcz@PLT
	movq	-112(%rbp), %r11
	movq	-128(%rbp), %r10
.L2768:
	movq	%rax, -72(%rbp)
	movq	16(%r10), %rax
	leaq	-64(%rbp), %rcx
	leaq	-72(%rbp), %rdx
	movq	%r11, -80(%rbp)
	leaq	-84(%rbp), %rsi
	leaq	-80(%rbp), %r8
	addl	$1, 4(%r10)
	leaq	296(%rax), %rdi
	movl	$3, -84(%rbp)
	movq	%r10, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
.L2759:
	addq	$16, %r13
	addl	$2, %ebx
	addl	$16, %r15d
	cmpl	%ebx, -116(%rbp)
	jne	.L2770
.L2749:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2823
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2767:
	.cfi_restore_state
	movl	%edx, %esi
	movq	%r10, -128(%rbp)
	movq	%r11, -112(%rbp)
	call	_ZN2v88internal14StringsStorage7GetNameEi@PLT
	movq	-128(%rbp), %r10
	movq	-112(%rbp), %r11
	jmp	.L2768
	.p2align 4,,10
	.p2align 3
.L2752:
	movq	(%r14), %rax
	movq	%r11, %rsi
	movq	%r8, -136(%rbp)
	movq	%r14, %rdi
	movq	%r11, -128(%rbp)
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, -112(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %r11
	movq	-112(%rbp), %rdx
	movq	-136(%rbp), %r8
	movq	$0, (%rax)
	movq	%rax, %r9
	movq	%rdx, 16(%rax)
	xorl	%edx, %edx
	movq	%r11, 8(%rax)
	movq	344(%r8), %r10
	movq	%r11, %rax
	divq	%r10
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rsi
	testq	%rax, %rax
	je	.L2755
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L2757
	.p2align 4,,10
	.p2align 3
.L2824:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2755
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%r10
	cmpq	%rdx, %rsi
	jne	.L2755
.L2757:
	cmpq	%rdi, %r11
	jne	.L2824
	movq	%r9, %rdi
	movq	%rcx, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rcx
.L2773:
	movq	16(%rcx), %r10
	jmp	.L2751
	.p2align 4,,10
	.p2align 3
.L2760:
	movq	(%r14), %rax
	movq	%r10, -136(%rbp)
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r8, -128(%rbp)
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, -112(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rdx
	movq	-128(%rbp), %r8
	movq	%r12, 8(%rax)
	movq	%rax, %r9
	movq	-136(%rbp), %r10
	movq	%rdx, 16(%rax)
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	344(%r8), %r11
	movq	%r12, %rax
	divq	%r11
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rsi
	testq	%rax, %rax
	je	.L2763
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L2765
	.p2align 4,,10
	.p2align 3
.L2825:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2763
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%r11
	cmpq	%rdx, %rsi
	jne	.L2763
.L2765:
	cmpq	%rdi, %r12
	jne	.L2825
	movq	%r9, %rdi
	movq	%r10, -128(%rbp)
	movq	%rcx, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rcx
	movq	-128(%rbp), %r10
.L2771:
	movq	16(%rcx), %r11
	jmp	.L2772
	.p2align 4,,10
	.p2align 3
.L2755:
	leaq	336(%r8), %rdi
	movq	%r9, %rcx
	movq	%r11, %rdx
	movl	$1, %r8d
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %rcx
	jmp	.L2773
	.p2align 4,,10
	.p2align 3
.L2763:
	leaq	336(%r8), %rdi
	movq	%r9, %rcx
	movl	$1, %r8d
	movq	%r12, %rdx
	movq	%r10, -112(%rbp)
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	-112(%rbp), %r10
	movq	%rax, %rcx
	jmp	.L2771
.L2823:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19988:
	.size	_ZN2v88internal14V8HeapExplorer35ExtractEphemeronHashTableReferencesEPNS0_9HeapEntryENS0_18EphemeronHashTableE, .-_ZN2v88internal14V8HeapExplorer35ExtractEphemeronHashTableReferencesEPNS0_9HeapEntryENS0_18EphemeronHashTableE
	.section	.text._ZN2v88internal14V8HeapExplorer27ExtractFixedArrayReferencesEPNS0_9HeapEntryENS0_10FixedArrayE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer27ExtractFixedArrayReferencesEPNS0_9HeapEntryENS0_10FixedArrayE
	.type	_ZN2v88internal14V8HeapExplorer27ExtractFixedArrayReferencesEPNS0_9HeapEntryENS0_10FixedArrayE, @function
_ZN2v88internal14V8HeapExplorer27ExtractFixedArrayReferencesEPNS0_9HeapEntryENS0_10FixedArrayE:
.LFB20009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	11(%rdx), %rax
	movl	%eax, -104(%rbp)
	testq	%rax, %rax
	jle	.L2826
	leaq	15(%rdx), %r13
	xorl	%ebx, %ebx
	movq	%r13, %r15
	movq	%rdi, %r13
	.p2align 4,,10
	.p2align 3
.L2837:
	leal	16(,%rbx,8), %eax
	movl	%eax, -100(%rbp)
	movq	(%r15), %r12
	testb	$1, %r12b
	je	.L2838
	movq	48(%r13), %r8
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	344(%r8), %rdi
	divq	%rdi
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r11
	testq	%rax, %rax
	je	.L2829
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2831
	.p2align 4,,10
	.p2align 3
.L2869:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2829
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r11
	jne	.L2829
.L2831:
	cmpq	%rsi, %r12
	jne	.L2869
	movq	16(%rcx), %r14
	testq	%r14, %r14
	je	.L2829
.L2840:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	testb	%al, %al
	jne	.L2870
.L2835:
	movl	-100(%rbp), %ecx
	movq	232(%r13), %rdx
	movl	$1, %edi
	sarl	$3, %ecx
	movslq	%ecx, %rax
	salq	%cl, %rdi
	shrq	$6, %rax
	orq	%rdi, (%rdx,%rax,8)
.L2838:
	addl	$1, %ebx
	addq	$8, %r15
	cmpl	-104(%rbp), %ebx
	jne	.L2837
.L2826:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2871
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2870:
	.cfi_restore_state
	movq	24(%r13), %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal14StringsStorage7GetNameEi@PLT
	leaq	-64(%rbp), %rcx
	leaq	-72(%rbp), %rdx
	movl	$3, -84(%rbp)
	movq	%rax, -72(%rbp)
	movq	-112(%rbp), %rax
	leaq	-84(%rbp), %rsi
	leaq	-80(%rbp), %r8
	movq	%r14, -80(%rbp)
	addl	$1, 4(%rax)
	movq	16(%rax), %rdi
	movq	%rax, -64(%rbp)
	movq	%rdi, -120(%rbp)
	leaq	296(%rdi), %rdi
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	jmp	.L2835
	.p2align 4,,10
	.p2align 3
.L2829:
	movq	0(%r13), %rax
	movq	%r8, -120(%rbp)
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, %r14
	call	_Znwm@PLT
	movq	-120(%rbp), %r8
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%r12, 8(%rax)
	movq	%r14, 16(%rax)
	movq	344(%r8), %r11
	movq	%r12, %rax
	divq	%r11
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rsi
	testq	%rax, %rax
	je	.L2832
	movq	(%rax), %r14
	movq	8(%r14), %rcx
	jmp	.L2834
	.p2align 4,,10
	.p2align 3
.L2872:
	movq	(%r14), %r14
	testq	%r14, %r14
	je	.L2832
	movq	8(%r14), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r11
	cmpq	%rdx, %rsi
	jne	.L2832
.L2834:
	cmpq	%rcx, %r12
	jne	.L2872
	call	_ZdlPv@PLT
.L2839:
	movq	16(%r14), %r14
	testq	%r14, %r14
	je	.L2838
	jmp	.L2840
	.p2align 4,,10
	.p2align 3
.L2832:
	leaq	336(%r8), %r11
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movl	$1, %r8d
	movq	%r11, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %r14
	jmp	.L2839
.L2871:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20009:
	.size	_ZN2v88internal14V8HeapExplorer27ExtractFixedArrayReferencesEPNS0_9HeapEntryENS0_10FixedArrayE, .-_ZN2v88internal14V8HeapExplorer27ExtractFixedArrayReferencesEPNS0_9HeapEntryENS0_10FixedArrayE
	.section	.text._ZN2v88internal24RootsReferencesExtractor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_,"axG",@progbits,_ZN2v88internal24RootsReferencesExtractor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24RootsReferencesExtractor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.type	_ZN2v88internal24RootsReferencesExtractor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_, @function
_ZN2v88internal24RootsReferencesExtractor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_:
.LFB20025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -100(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%r8, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	%esi, %rax
	movq	%rax, -136(%rbp)
	cmpq	%r8, %rcx
	jnb	.L2873
	movq	%rdi, %r13
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L2874:
	movq	0(%r13), %rax
	leaq	_ZN2v88internal24RootsReferencesExtractor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %rdi
	movq	24(%rax), %rax
	cmpq	%rdi, %rax
	jne	.L2875
	cmpl	$12, -100(%rbp)
	movq	8(%r13), %r15
	movq	(%rbx), %r12
	je	.L2928
.L2876:
	movzbl	16(%r13), %r14d
	testb	$1, %r12b
	je	.L2895
	movq	48(%r15), %r8
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	344(%r8), %rdi
	divq	%rdi
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2882
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2884
	.p2align 4,,10
	.p2align 3
.L2929:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2882
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L2882
.L2884:
	cmpq	%rsi, %r12
	jne	.L2929
	movq	16(%rcx), %r11
	testq	%r11, %r11
	je	.L2882
.L2898:
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%r11, -120(%rbp)
	call	_ZN2v88internal14V8HeapExplorer22GetStrongGcSubrootNameENS0_6ObjectE
	cmpb	$1, %r14b
	movq	16(%r15), %rdx
	movq	-136(%rbp), %rdi
	sbbl	%r8d, %r8d
	movq	-120(%rbp), %r11
	andl	$-3, %r8d
	movq	24(%rdx,%rdi,8), %r10
	addl	$6, %r8d
	testq	%rax, %rax
	je	.L2889
	movl	%r8d, -88(%rbp)
	leaq	-88(%rbp), %rsi
	leaq	-80(%rbp), %r8
	movq	%r11, -80(%rbp)
	leaq	-72(%rbp), %r11
	movq	%rax, -72(%rbp)
	movq	16(%r10), %rax
	movq	%r11, %rdx
	addl	$1, 4(%r10)
	movq	%r10, -64(%rbp)
	leaq	-64(%rbp), %r10
	leaq	296(%rax), %rdi
	movq	%r10, %rcx
	movq	%r8, -160(%rbp)
	movq	%r10, -152(%rbp)
	movq	%r11, -144(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	movq	-120(%rbp), %rsi
	movq	-144(%rbp), %r11
	movq	-152(%rbp), %r10
	movq	-160(%rbp), %r8
.L2890:
	testb	%r14b, %r14b
	jne	.L2895
	movq	-1(%r12), %rax
	cmpw	$145, 11(%rax)
	je	.L2930
	.p2align 4,,10
	.p2align 3
.L2895:
	addq	$8, %rbx
	cmpq	-112(%rbp), %rbx
	jb	.L2874
.L2873:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2931
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2875:
	.cfi_restore_state
	movq	-128(%rbp), %rdx
	movl	-100(%rbp), %esi
	movq	%rbx, %rcx
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2895
	.p2align 4,,10
	.p2align 3
.L2928:
	movq	24(%r15), %rdi
	movq	-128(%rbp), %rdx
	leaq	.LC123(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal14StringsStorage12GetFormattedEPKcz@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal14V8HeapExplorer17IsEssentialObjectENS0_6ObjectE
	testb	%al, %al
	je	.L2878
	testb	$1, %r12b
	je	.L2879
	movq	48(%r15), %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal21HeapSnapshotGenerator14FindOrAddEntryEPvPNS0_20HeapEntriesAllocatorE
	movq	24(%rax), %rdx
	cmpb	$0, (%rdx)
	jne	.L2878
	movq	%r14, 24(%rax)
.L2878:
	movq	8(%r13), %r15
	movq	(%rbx), %r12
	jmp	.L2876
	.p2align 4,,10
	.p2align 3
.L2889:
	movl	4(%r10), %eax
	movq	-128(%rbp), %rcx
	movl	%r8d, -120(%rbp)
	movq	%r10, -152(%rbp)
	movq	24(%r15), %rdi
	movq	%r11, -144(%rbp)
	leal	1(%rax), %edx
	testq	%rcx, %rcx
	je	.L2891
	leaq	.LC128(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal14StringsStorage12GetFormattedEPKcz@PLT
	movl	-120(%rbp), %r8d
	movq	-144(%rbp), %r11
	movq	-152(%rbp), %r10
.L2892:
	movl	%r8d, -88(%rbp)
	leaq	-88(%rbp), %rsi
	leaq	-80(%rbp), %r8
	movq	%r11, -80(%rbp)
	leaq	-72(%rbp), %r11
	movq	%rax, -72(%rbp)
	movq	16(%r10), %rax
	movq	%r11, %rdx
	addl	$1, 4(%r10)
	movq	%r10, -64(%rbp)
	leaq	-64(%rbp), %r10
	leaq	296(%rax), %rdi
	movq	%r10, %rcx
	movq	%r8, -160(%rbp)
	movq	%r10, -152(%rbp)
	movq	%r11, -144(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	movq	-160(%rbp), %r8
	movq	-152(%rbp), %r10
	movq	-144(%rbp), %r11
	movq	-120(%rbp), %rsi
	jmp	.L2890
	.p2align 4,,10
	.p2align 3
.L2882:
	movq	(%r15), %rax
	movq	%r8, -144(%rbp)
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, -120(%rbp)
	call	_Znwm@PLT
	movq	-120(%rbp), %rdx
	movq	-144(%rbp), %r8
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%rdx, 16(%rax)
	xorl	%edx, %edx
	movq	%r12, 8(%rax)
	movq	344(%r8), %r11
	movq	%r12, %rax
	divq	%r11
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2885
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2887
	.p2align 4,,10
	.p2align 3
.L2932:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2885
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r11
	cmpq	%rdx, %r10
	jne	.L2885
.L2887:
	cmpq	%rsi, %r12
	jne	.L2932
	movq	%rcx, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rcx
.L2897:
	movq	16(%rcx), %r11
	testq	%r11, %r11
	je	.L2895
	jmp	.L2898
	.p2align 4,,10
	.p2align 3
.L2930:
	movq	%r10, %rdi
	movq	%r11, -160(%rbp)
	movq	%r8, -152(%rbp)
	movq	%rsi, -144(%rbp)
	movq	%r10, -120(%rbp)
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	%rax, -88(%rbp)
	movq	-1(%rax), %rax
	movq	-120(%rbp), %r10
	movq	-144(%rbp), %rsi
	cmpw	$1025, 11(%rax)
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %r11
	jne	.L2895
	leaq	168(%r15), %rdi
	movl	$1, %ecx
	movq	%r10, %rdx
	movq	%r11, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZNSt10_HashtableIN2v88internal14JSGlobalObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	testb	%dl, %dl
	je	.L2895
	movq	-88(%rbp), %rsi
	xorl	%r14d, %r14d
	movq	-120(%rbp), %r10
	movq	-144(%rbp), %r8
	movq	-152(%rbp), %r11
	testb	$1, %sil
	je	.L2893
	movq	48(%r15), %rdi
	movq	%r15, %rdx
	movq	%r8, -120(%rbp)
	movq	%r10, -152(%rbp)
	movq	%r11, -144(%rbp)
	call	_ZN2v88internal21HeapSnapshotGenerator14FindOrAddEntryEPvPNS0_20HeapEntriesAllocatorE
	movq	-152(%rbp), %r10
	movq	-144(%rbp), %r11
	movq	-120(%rbp), %r8
	movq	%rax, %r14
.L2893:
	movq	16(%r15), %rax
	movq	24(%r15), %rdi
	movq	%r8, -120(%rbp)
	movq	%r10, -152(%rbp)
	movq	8(%rax), %r12
	movq	%r11, -144(%rbp)
	movl	4(%r12), %eax
	leal	1(%rax), %esi
	call	_ZN2v88internal14StringsStorage7GetNameEi@PLT
	movq	%r14, -80(%rbp)
	movq	-152(%rbp), %r10
	leaq	-92(%rbp), %rsi
	movq	-144(%rbp), %r11
	movq	-120(%rbp), %r8
	movq	%rax, -72(%rbp)
	addl	$1, 4(%r12)
	movq	16(%r12), %rax
	movq	%r10, %rcx
	movq	%r11, %rdx
	movl	$5, -92(%rbp)
	leaq	296(%rax), %rdi
	movq	%r12, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	jmp	.L2895
	.p2align 4,,10
	.p2align 3
.L2891:
	movl	%edx, %esi
	call	_ZN2v88internal14StringsStorage7GetNameEi@PLT
	movq	-152(%rbp), %r10
	movq	-144(%rbp), %r11
	movl	-120(%rbp), %r8d
	jmp	.L2892
	.p2align 4,,10
	.p2align 3
.L2885:
	leaq	336(%r8), %r11
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	movq	%r11, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %rcx
	jmp	.L2897
.L2931:
	call	__stack_chk_fail@PLT
.L2879:
	movq	24, %rax
	ud2
	.cfi_endproc
.LFE20025:
	.size	_ZN2v88internal24RootsReferencesExtractor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_, .-_ZN2v88internal24RootsReferencesExtractor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.section	.text._ZN2v88internal14V8HeapExplorer23SetRootGcRootsReferenceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer23SetRootGcRootsReferenceEv
	.type	_ZN2v88internal14V8HeapExplorer23SetRootGcRootsReferenceEv, @function
_ZN2v88internal14V8HeapExplorer23SetRootGcRootsReferenceEv:
.LFB20042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	8(%rax), %r13
	movq	16(%rax), %r14
	movl	4(%r13), %eax
	movq	16(%r13), %rbx
	leal	1(%rax), %r12d
	movl	%r12d, 4(%r13)
	movq	360(%rbx), %rsi
	movq	344(%rbx), %rax
	leaq	-24(%rsi), %rdx
	cmpq	%rdx, %rax
	je	.L2934
	movl	0(%r13), %edx
	movq	%r14, 8(%rax)
	movl	%r12d, 16(%rax)
	shrl	$4, %edx
	leal	1(,%rdx,8), %edx
	movl	%edx, (%rax)
	addq	$24, 344(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2934:
	.cfi_restore_state
	movq	368(%rbx), %r15
	subq	352(%rbx), %rax
	movabsq	$-6148914691236517205, %rsi
	sarq	$3, %rax
	movq	%r15, %rdx
	imulq	%rsi, %rax
	subq	336(%rbx), %rdx
	sarq	$3, %rdx
	subq	$1, %rdx
	leaq	(%rdx,%rdx,4), %rcx
	leaq	(%rdx,%rcx,4), %rcx
	movq	328(%rbx), %rdx
	subq	312(%rbx), %rdx
	sarq	$3, %rdx
	addq	%rcx, %rax
	imulq	%rsi, %rdx
	addq	%rax, %rdx
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rdx
	je	.L2939
	movq	304(%rbx), %rcx
	movq	%r15, %rax
	subq	296(%rbx), %rax
	sarq	$3, %rax
	subq	%rax, %rcx
	cmpq	$1, %rcx
	jbe	.L2940
.L2937:
	movl	$504, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r15)
	movl	0(%r13), %eax
	movq	344(%rbx), %rdx
	shrl	$4, %eax
	leal	1(,%rax,8), %eax
	movq	%r14, 8(%rdx)
	movl	%eax, (%rdx)
	movl	%r12d, 16(%rdx)
	movq	368(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 368(%rbx)
	movq	8(%rax), %rax
	leaq	504(%rax), %rdx
	movq	%rax, 352(%rbx)
	movq	%rdx, 360(%rbx)
	movq	%rax, 344(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2940:
	.cfi_restore_state
	leaq	296(%rbx), %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE17_M_reallocate_mapEmb
	movq	368(%rbx), %r15
	jmp	.L2937
.L2939:
	leaq	.LC32(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE20042:
	.size	_ZN2v88internal14V8HeapExplorer23SetRootGcRootsReferenceEv, .-_ZN2v88internal14V8HeapExplorer23SetRootGcRootsReferenceEv
	.section	.text._ZN2v88internal14V8HeapExplorer19SetGcRootsReferenceENS0_4RootE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer19SetGcRootsReferenceENS0_4RootE
	.type	_ZN2v88internal14V8HeapExplorer19SetGcRootsReferenceENS0_4RootE, @function
_ZN2v88internal14V8HeapExplorer19SetGcRootsReferenceENS0_4RootE:
.LFB20044:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	16(%rax), %r13
	movq	24(%rax,%rsi,8), %r14
	movl	4(%r13), %eax
	movq	16(%r13), %rbx
	leal	1(%rax), %r12d
	movl	%r12d, 4(%r13)
	movq	360(%rbx), %rcx
	movq	344(%rbx), %rax
	leaq	-24(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L2942
	movl	0(%r13), %edx
	movq	%r14, 8(%rax)
	movl	%r12d, 16(%rax)
	shrl	$4, %edx
	leal	1(,%rdx,8), %edx
	movl	%edx, (%rax)
	addq	$24, 344(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2942:
	.cfi_restore_state
	movq	368(%rbx), %r15
	subq	352(%rbx), %rax
	movabsq	$-6148914691236517205, %rsi
	sarq	$3, %rax
	movq	%r15, %rdx
	imulq	%rsi, %rax
	subq	336(%rbx), %rdx
	sarq	$3, %rdx
	subq	$1, %rdx
	leaq	(%rdx,%rdx,4), %rcx
	leaq	(%rdx,%rcx,4), %rcx
	movq	328(%rbx), %rdx
	subq	312(%rbx), %rdx
	sarq	$3, %rdx
	addq	%rcx, %rax
	imulq	%rsi, %rdx
	addq	%rax, %rdx
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rdx
	je	.L2947
	movq	304(%rbx), %rcx
	movq	%r15, %rax
	subq	296(%rbx), %rax
	sarq	$3, %rax
	subq	%rax, %rcx
	cmpq	$1, %rcx
	jbe	.L2948
.L2945:
	movl	$504, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r15)
	movl	0(%r13), %eax
	movq	344(%rbx), %rdx
	shrl	$4, %eax
	leal	1(,%rax,8), %eax
	movq	%r14, 8(%rdx)
	movl	%eax, (%rdx)
	movl	%r12d, 16(%rdx)
	movq	368(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 368(%rbx)
	movq	8(%rax), %rax
	leaq	504(%rax), %rdx
	movq	%rax, 352(%rbx)
	movq	%rdx, 360(%rbx)
	movq	%rax, 344(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2948:
	.cfi_restore_state
	leaq	296(%rbx), %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE17_M_reallocate_mapEmb
	movq	368(%rbx), %r15
	jmp	.L2945
.L2947:
	leaq	.LC32(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE20044:
	.size	_ZN2v88internal14V8HeapExplorer19SetGcRootsReferenceENS0_4RootE, .-_ZN2v88internal14V8HeapExplorer19SetGcRootsReferenceENS0_4RootE
	.section	.text._ZN2v88internal9HeapEntry19SetIndexedReferenceENS0_13HeapGraphEdge4TypeEiPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9HeapEntry19SetIndexedReferenceENS0_13HeapGraphEdge4TypeEiPS1_
	.type	_ZN2v88internal9HeapEntry19SetIndexedReferenceENS0_13HeapGraphEdge4TypeEiPS1_, @function
_ZN2v88internal9HeapEntry19SetIndexedReferenceENS0_13HeapGraphEdge4TypeEiPS1_:
.LFB19781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	addl	$1, 4(%rdi)
	movq	360(%rbx), %rdi
	movq	344(%rbx), %rax
	leaq	-24(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L2950
	movl	(%r12), %esi
	movq	%rcx, 8(%rax)
	movl	%r13d, 16(%rax)
	shrl	$4, %esi
	sall	$3, %esi
	orl	%r15d, %esi
	movl	%esi, (%rax)
	addq	$24, 344(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2950:
	.cfi_restore_state
	movq	368(%rbx), %rcx
	subq	352(%rbx), %rax
	movabsq	$-6148914691236517205, %rdi
	sarq	$3, %rax
	movq	%rcx, %rdx
	imulq	%rdi, %rax
	subq	336(%rbx), %rdx
	sarq	$3, %rdx
	subq	$1, %rdx
	leaq	(%rdx,%rdx,4), %rsi
	leaq	(%rdx,%rsi,4), %rsi
	movq	328(%rbx), %rdx
	subq	312(%rbx), %rdx
	sarq	$3, %rdx
	addq	%rsi, %rax
	imulq	%rdi, %rdx
	addq	%rax, %rdx
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rdx
	je	.L2955
	movq	304(%rbx), %rdi
	movq	%rcx, %rax
	subq	296(%rbx), %rax
	sarq	$3, %rax
	subq	%rax, %rdi
	cmpq	$1, %rdi
	jbe	.L2956
.L2953:
	movl	$504, %edi
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, 8(%rcx)
	movl	(%r12), %esi
	movq	344(%rbx), %rax
	shrl	$4, %esi
	sall	$3, %esi
	movq	%r14, 8(%rax)
	orl	%r15d, %esi
	movl	%r13d, 16(%rax)
	movl	%esi, (%rax)
	movq	368(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 368(%rbx)
	movq	8(%rax), %rax
	leaq	504(%rax), %rdx
	movq	%rax, 352(%rbx)
	movq	%rdx, 360(%rbx)
	movq	%rax, 344(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2956:
	.cfi_restore_state
	leaq	296(%rbx), %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE17_M_reallocate_mapEmb
	movq	368(%rbx), %rcx
	jmp	.L2953
.L2955:
	leaq	.LC32(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE19781:
	.size	_ZN2v88internal9HeapEntry19SetIndexedReferenceENS0_13HeapGraphEdge4TypeEiPS1_, .-_ZN2v88internal9HeapEntry19SetIndexedReferenceENS0_13HeapGraphEdge4TypeEiPS1_
	.section	.text._ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_
	.type	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_, @function
_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_:
.LFB25613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	movq	(%rdi), %r12
	movl	12(%rdi), %r14d
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rdi
	movq	%rax, %rbx
	salq	$3, %rdi
	call	malloc@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L2997
	movl	%ebx, 8(%r13)
	testl	%ebx, %ebx
	je	.L2959
	movq	$0, (%rax)
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L2960:
	movq	0(%r13), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	8(%r13), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L2960
.L2959:
	movl	$0, 12(%r13)
	movq	%r12, %r15
	testl	%r14d, %r14d
	je	.L2967
.L2961:
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	jne	.L2998
.L2962:
	movq	24(%r15), %rsi
	addq	$24, %r15
	testq	%rsi, %rsi
	je	.L2962
.L2998:
	movl	8(%r13), %eax
	movl	16(%r15), %ebx
	movq	0(%r13), %r8
	leal	-1(%rax), %edi
	movl	%ebx, %eax
	andl	%edi, %eax
	jmp	.L2996
	.p2align 4,,10
	.p2align 3
.L2999:
	testq	%rdx, %rdx
	je	.L2963
	addq	$1, %rax
	andq	%rdi, %rax
.L2996:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r8,%rdx,8), %rcx
	movq	(%rcx), %rdx
	cmpq	%rdx, %rsi
	jne	.L2999
.L2963:
	movq	8(%r15), %rax
	movq	%rsi, (%rcx)
	movl	%ebx, 16(%rcx)
	movq	%rax, 8(%rcx)
	movl	12(%r13), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%r13)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%r13), %eax
	jnb	.L2966
.L2969:
	addq	$24, %r15
	subl	$1, %r14d
	jne	.L2961
.L2967:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L2966:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_
	movl	8(%r13), %eax
	movq	0(%r13), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	movq	(%rsi,%rdx,8), %rdx
	testq	%rdx, %rdx
	je	.L2969
	movq	(%r15), %rdi
	jmp	.L2970
	.p2align 4,,10
	.p2align 3
.L3000:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	movq	(%rsi,%rdx,8), %rdx
	testq	%rdx, %rdx
	je	.L2969
.L2970:
	cmpq	%rdx, %rdi
	jne	.L3000
	jmp	.L2969
.L2997:
	leaq	.LC35(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25613:
	.size	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_, .-_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_
	.section	.rodata._ZN2v88internal14HeapObjectsMap10MoveObjectEmmi.str1.8,"aMS",@progbits,1
	.align 8
.LC216:
	.string	"Move object from %p to %p old size %6d new size %6d\n"
	.section	.text._ZN2v88internal14HeapObjectsMap10MoveObjectEmmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14HeapObjectsMap10MoveObjectEmmi
	.type	_ZN2v88internal14HeapObjectsMap10MoveObjectEmmi, @function
_ZN2v88internal14HeapObjectsMap10MoveObjectEmmi:
.LFB19863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rdx, %rsi
	je	.L3019
	movl	%esi, %eax
	movq	%rdx, %r12
	leaq	8(%rdi), %r15
	movq	%rsi, -64(%rbp)
	sall	$15, %eax
	movq	%rdi, %rbx
	movq	%rsi, %r14
	movq	%r15, %rdi
	subl	%esi, %eax
	leaq	-64(%rbp), %rsi
	movl	%ecx, %r13d
	subl	$1, %eax
	movq	%rsi, -72(%rbp)
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %edx
	movl	%edx, %eax
	shrl	$4, %eax
	xorl	%eax, %edx
	imull	$2057, %edx, %edx
	movl	%edx, %eax
	shrl	$16, %eax
	xorl	%eax, %edx
	andl	$1073741823, %edx
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6RemoveERKS2_j
	movq	-72(%rbp), %rsi
	movq	%rax, %r10
	movl	%r12d, %eax
	sall	$15, %eax
	subl	%r12d, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%eax, %edx
	imull	$2057, %edx, %edx
	movl	%edx, %eax
	shrl	$16, %eax
	xorl	%eax, %edx
	andl	$1073741823, %edx
	testq	%r10, %r10
	je	.L3046
	movl	16(%rbx), %eax
	movq	8(%rbx), %rdi
	leal	-1(%rax), %esi
	movl	%esi, %eax
	andl	%edx, %eax
	jmp	.L3041
	.p2align 4,,10
	.p2align 3
.L3047:
	cmpq	%r12, %rcx
	je	.L3007
	addq	$1, %rax
	andq	%rsi, %rax
.L3041:
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdi,%rcx,8), %r9
	movq	(%r9), %rcx
	testq	%rcx, %rcx
	jne	.L3047
	movq	%r12, (%r9)
	movq	$0, 8(%r9)
	movl	%edx, 16(%r9)
	movl	20(%rbx), %eax
	addl	$1, %eax
	movl	%eax, %ecx
	movl	%eax, 20(%rbx)
	shrl	$2, %ecx
	addl	%ecx, %eax
	cmpl	16(%rbx), %eax
	jnb	.L3048
.L3007:
	movq	32(%rbx), %rax
	movq	40(%rbx), %rdx
	movabsq	$-6148914691236517205, %rcx
	movq	8(%r9), %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	imulq	%rcx, %rdx
	movq	%rdx, %r8
	testq	%rsi, %rsi
	je	.L3011
	movslq	%esi, %rsi
	cmpq	%rdx, %rsi
	jnb	.L3044
	leaq	(%rsi,%rsi,2), %rdx
	movq	$0, 8(%rax,%rdx,8)
	movq	32(%rbx), %rax
	movq	40(%rbx), %r8
	subq	%rax, %r8
	sarq	$3, %r8
	imulq	%rcx, %r8
.L3011:
	movslq	%r10d, %r15
	cmpq	%r8, %r15
	jnb	.L3049
	leaq	(%r15,%r15,2), %r11
	salq	$3, %r11
	cmpb	$0, _ZN2v88internal32FLAG_heap_profiler_trace_objectsE(%rip)
	movq	%r12, 8(%rax,%r11)
	je	.L3014
	movq	32(%rbx), %rax
	movq	40(%rbx), %rdx
	movabsq	$-6148914691236517205, %rcx
	subq	%rax, %rdx
	sarq	$3, %rdx
	imulq	%rcx, %rdx
	cmpq	%rdx, %r15
	jnb	.L3045
	movl	16(%rax,%r11), %ecx
	movl	%r13d, %r8d
	movq	%r12, %rdx
	movq	%r14, %rsi
	leaq	.LC216(%rip), %rdi
	xorl	%eax, %eax
	movq	%r9, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r11, -72(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r11
.L3014:
	movq	32(%rbx), %rax
	movq	40(%rbx), %rdx
	movabsq	$-6148914691236517205, %rcx
	subq	%rax, %rdx
	sarq	$3, %rdx
	imulq	%rcx, %rdx
	cmpq	%rdx, %r15
	jnb	.L3045
	movl	%r13d, 16(%rax,%r11)
	movq	%r10, 8(%r9)
.L3004:
	testq	%r10, %r10
	setne	%al
.L3001:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L3050
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3019:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L3001
	.p2align 4,,10
	.p2align 3
.L3046:
	movq	%r15, %rdi
	movq	%r10, -72(%rbp)
	movq	%r12, -64(%rbp)
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6RemoveERKS2_j
	movq	-72(%rbp), %r10
	testq	%rax, %rax
	je	.L3004
	movq	40(%rbx), %rdx
	movslq	%eax, %rsi
	movq	32(%rbx), %rax
	movabsq	$-6148914691236517205, %rcx
	subq	%rax, %rdx
	sarq	$3, %rdx
	imulq	%rcx, %rdx
	cmpq	%rdx, %rsi
	jnb	.L3044
	leaq	(%rsi,%rsi,2), %rdx
	movq	$0, 8(%rax,%rdx,8)
	jmp	.L3004
	.p2align 4,,10
	.p2align 3
.L3048:
	movq	%r15, %rdi
	movl	%edx, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_
	movl	16(%rbx), %eax
	movl	-80(%rbp), %edx
	movq	8(%rbx), %rsi
	movq	-72(%rbp), %r10
	leal	-1(%rax), %ecx
	andl	%ecx, %edx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rsi,%rax,8), %r9
	movq	(%r9), %rax
	testq	%rax, %rax
	jne	.L3042
	jmp	.L3007
	.p2align 4,,10
	.p2align 3
.L3051:
	addq	$1, %rdx
	andq	%rcx, %rdx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rsi,%rax,8), %r9
	movq	(%r9), %rax
	testq	%rax, %rax
	je	.L3007
.L3042:
	cmpq	%r12, %rax
	jne	.L3051
	jmp	.L3007
.L3049:
	movq	%r8, %rdx
.L3045:
	movq	%r15, %rsi
.L3044:
	leaq	.LC36(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L3050:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19863:
	.size	_ZN2v88internal14HeapObjectsMap10MoveObjectEmmi, .-_ZN2v88internal14HeapObjectsMap10MoveObjectEmmi
	.section	.rodata._ZN2v88internal14HeapObjectsMap14FindOrAddEntryEmjb.str1.8,"aMS",@progbits,1
	.align 8
.LC217:
	.string	"Update object size : %p with old size %d and new size %d\n"
	.section	.text._ZN2v88internal14HeapObjectsMap14FindOrAddEntryEmjb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14HeapObjectsMap14FindOrAddEntryEmjb
	.type	_ZN2v88internal14HeapObjectsMap14FindOrAddEntryEmjb, @function
_ZN2v88internal14HeapObjectsMap14FindOrAddEntryEmjb:
.LFB19866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%esi, %eax
	sall	$15, %eax
	subl	%esi, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	$1073741823, %eax
	movl	%eax, %r15d
	movl	16(%rdi), %eax
	movq	8(%rdi), %rdi
	leal	-1(%rax), %edx
	movl	%r15d, %eax
	andl	%edx, %eax
	jmp	.L3081
	.p2align 4,,10
	.p2align 3
.L3082:
	cmpq	%rsi, %r13
	je	.L3054
	addq	$1, %rax
	andq	%rdx, %rax
.L3081:
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdi,%rcx,8), %rcx
	movq	(%rcx), %rsi
	testq	%rsi, %rsi
	jne	.L3082
	movq	%r13, (%rcx)
	movq	$0, 8(%rcx)
	movl	%r15d, 16(%rcx)
	movl	20(%r12), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 20(%r12)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	16(%r12), %eax
	jnb	.L3083
.L3054:
	movq	32(%r12), %rax
	movq	40(%r12), %rdx
	movabsq	$-6148914691236517205, %rdi
	movq	8(%rcx), %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	imulq	%rdi, %rdx
	testq	%rsi, %rsi
	je	.L3058
	movslq	%esi, %rsi
	cmpq	%rdx, %rsi
	jnb	.L3084
	leaq	(%rsi,%rsi,2), %rdx
	cmpb	$0, _ZN2v88internal32FLAG_heap_profiler_trace_objectsE(%rip)
	leaq	(%rax,%rdx,8), %r12
	movb	%r14b, 20(%r12)
	jne	.L3085
.L3060:
	movl	%ebx, 16(%r12)
	movl	(%r12), %eax
.L3052:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L3086
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3085:
	.cfi_restore_state
	movl	16(%r12), %edx
	movl	%ebx, %ecx
	movq	%r13, %rsi
	xorl	%eax, %eax
	leaq	.LC217(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3060
	.p2align 4,,10
	.p2align 3
.L3083:
	leaq	8(%r12), %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_18KeyEqualityMatcherIS2_EENS0_23DefaultAllocationPolicyEE6ResizeES5_
	movl	16(%r12), %eax
	movq	8(%r12), %rdi
	leal	-1(%rax), %esi
	movl	%r15d, %eax
	andl	%esi, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L3054
	cmpq	%r13, %rdx
	jne	.L3056
	jmp	.L3054
	.p2align 4,,10
	.p2align 3
.L3087:
	testq	%rdx, %rdx
	je	.L3054
.L3056:
	addq	$1, %rax
	andq	%rsi, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rcx
	movq	(%rcx), %rdx
	cmpq	%r13, %rdx
	jne	.L3087
	jmp	.L3054
	.p2align 4,,10
	.p2align 3
.L3058:
	movq	%rdx, 8(%rcx)
	movl	(%r12), %eax
	movq	%r13, -72(%rbp)
	movq	40(%r12), %rsi
	leal	2(%rax), %edx
	movl	%eax, -80(%rbp)
	movl	%edx, (%r12)
	movl	%ebx, -64(%rbp)
	movb	%r14b, -60(%rbp)
	cmpq	48(%r12), %rsi
	je	.L3062
	movdqa	-80(%rbp), %xmm0
	movups	%xmm0, (%rsi)
	movq	-64(%rbp), %rdx
	movq	%rdx, 16(%rsi)
	addq	$24, 40(%r12)
	jmp	.L3052
	.p2align 4,,10
	.p2align 3
.L3062:
	leaq	-80(%rbp), %rdx
	leaq	32(%r12), %rdi
	movl	%eax, -84(%rbp)
	call	_ZNSt6vectorIN2v88internal14HeapObjectsMap9EntryInfoESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movl	-84(%rbp), %eax
	jmp	.L3052
.L3086:
	call	__stack_chk_fail@PLT
.L3084:
	leaq	.LC36(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE19866:
	.size	_ZN2v88internal14HeapObjectsMap14FindOrAddEntryEmjb, .-_ZN2v88internal14HeapObjectsMap14FindOrAddEntryEmjb
	.section	.text._ZN2v88internal14HeapObjectsMap16UpdateObjectSizeEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14HeapObjectsMap16UpdateObjectSizeEmi
	.type	_ZN2v88internal14HeapObjectsMap16UpdateObjectSizeEmi, @function
_ZN2v88internal14HeapObjectsMap16UpdateObjectSizeEmi:
.LFB19864:
	.cfi_startproc
	endbr64
	xorl	%ecx, %ecx
	jmp	_ZN2v88internal14HeapObjectsMap14FindOrAddEntryEmjb
	.cfi_endproc
.LFE19864:
	.size	_ZN2v88internal14HeapObjectsMap16UpdateObjectSizeEmi, .-_ZN2v88internal14HeapObjectsMap16UpdateObjectSizeEmi
	.section	.rodata._ZN2v88internal14HeapObjectsMap20UpdateHeapObjectsMapEv.str1.8,"aMS",@progbits,1
	.align 8
.LC218:
	.string	"Begin HeapObjectsMap::UpdateHeapObjectsMap. map has %d entries.\n"
	.align 8
.LC219:
	.string	"Update object      : %p %6d. Next address is %p\n"
	.align 8
.LC220:
	.string	"End HeapObjectsMap::UpdateHeapObjectsMap. map has %d entries.\n"
	.section	.text._ZN2v88internal14HeapObjectsMap20UpdateHeapObjectsMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14HeapObjectsMap20UpdateHeapObjectsMapEv
	.type	_ZN2v88internal14HeapObjectsMap20UpdateHeapObjectsMapEv, @function
_ZN2v88internal14HeapObjectsMap20UpdateHeapObjectsMapEv:
.LFB19868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal32FLAG_heap_profiler_trace_objectsE(%rip)
	jne	.L3104
.L3090:
	movq	80(%r12), %rdi
	xorl	%ecx, %ecx
	movl	$11, %edx
	xorl	%esi, %esi
	leaq	-128(%rbp), %r14
	call	_ZN2v88internal4Heap24PreciseCollectAllGarbageEiNS0_23GarbageCollectionReasonENS_15GCCallbackFlagsE@PLT
	movq	80(%r12), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal26CombinedHeapObjectIteratorC1EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal26CombinedHeapObjectIterator4NextEv@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L3091
	leaq	-136(%rbp), %r13
	leaq	.LC219(%rip), %r15
	jmp	.L3094
	.p2align 4,,10
	.p2align 3
.L3092:
	movq	%r14, %rdi
	call	_ZN2v88internal26CombinedHeapObjectIterator4NextEv@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L3091
.L3094:
	movq	-1(%rax), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	%eax, %edx
	movq	-136(%rbp), %rax
	leaq	-1(%rax), %rsi
	call	_ZN2v88internal14HeapObjectsMap14FindOrAddEntryEmjb
	cmpb	$0, _ZN2v88internal32FLAG_heap_profiler_trace_objectsE(%rip)
	je	.L3092
	movq	-136(%rbp), %rax
	movq	%r13, %rdi
	leaq	-1(%rax), %rbx
	movq	%rbx, -152(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	%r13, %rdi
	movl	%eax, %ebx
	movq	-136(%rbp), %rax
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movslq	%ebx, %rcx
	movq	%r15, %rdi
	addq	-152(%rbp), %rcx
	movl	%eax, %edx
	movq	-136(%rbp), %rax
	leaq	-1(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3092
	.p2align 4,,10
	.p2align 3
.L3091:
	movq	%r12, %rdi
	call	_ZN2v88internal14HeapObjectsMap17RemoveDeadEntriesEv
	cmpb	$0, _ZN2v88internal32FLAG_heap_profiler_trace_objectsE(%rip)
	jne	.L3105
.L3095:
	movq	%r14, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3106
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3104:
	.cfi_restore_state
	movl	20(%rdi), %esi
	leaq	.LC218(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3090
	.p2align 4,,10
	.p2align 3
.L3105:
	movl	20(%r12), %esi
	leaq	.LC220(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3095
.L3106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19868:
	.size	_ZN2v88internal14HeapObjectsMap20UpdateHeapObjectsMapEv, .-_ZN2v88internal14HeapObjectsMap20UpdateHeapObjectsMapEv
	.section	.text._ZN2v88internal14HeapObjectsMap20PushHeapObjectsStatsEPNS_12OutputStreamEPl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14HeapObjectsMap20PushHeapObjectsStatsEPNS_12OutputStreamEPl
	.type	_ZN2v88internal14HeapObjectsMap20PushHeapObjectsStatsEPNS_12OutputStreamEPl, @function
_ZN2v88internal14HeapObjectsMap20PushHeapObjectsStatsEPNS_12OutputStreamEPl:
.LFB19872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -80(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal14HeapObjectsMap20UpdateHeapObjectsMapEv
	movq	-72(%rbp), %r8
	movq	64(%r8), %r12
	cmpq	72(%r8), %r12
	je	.L3108
	movl	(%r8), %eax
	movq	$0, 4(%r12)
	movl	%eax, (%r12)
	call	_ZN2v84base9TimeTicks3NowEv@PLT
	movq	-72(%rbp), %r8
	movq	%rax, 16(%r12)
	addq	$24, 64(%r8)
.L3109:
	movq	-80(%rbp), %rax
	leaq	_ZN2v812OutputStream12GetChunkSizeEv(%rip), %rdx
	movl	$1024, -72(%rbp)
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3162
.L3110:
	movq	32(%r8), %r15
	movq	40(%r8), %rbx
	movq	56(%r8), %rdi
	cmpq	%rdi, 64(%r8)
	je	.L3111
	xorl	%r11d, %r11d
	xorl	%r12d, %r12d
	xorl	%r9d, %r9d
	xorl	%r13d, %r13d
	movabsq	$-6148914691236517205, %rcx
	.p2align 4,,10
	.p2align 3
.L3128:
	leaq	0(%r13,%r13,2), %rax
	leaq	(%rdi,%rax,8), %r14
	movl	(%r14), %esi
	cmpq	%r15, %rbx
	jbe	.L3135
	movq	%r15, %rax
	xorl	%edx, %edx
	jmp	.L3114
	.p2align 4,,10
	.p2align 3
.L3113:
	addl	16(%rax), %edx
	addq	$24, %rax
	cmpq	%rax, %rbx
	jbe	.L3161
.L3114:
	cmpl	%esi, (%rax)
	jb	.L3113
.L3161:
	movq	%rax, %rsi
	subq	%r15, %rsi
	movq	%rax, %r15
	sarq	$3, %rsi
	imull	%ecx, %esi
.L3112:
	cmpl	%esi, 8(%r14)
	jne	.L3115
	cmpl	%edx, 4(%r14)
	je	.L3116
.L3115:
	movl	%edx, 4(%r14)
	movl	%r13d, %r10d
	movl	%esi, 8(%r14)
	cmpq	%r11, %r12
	je	.L3117
	movl	%r13d, (%r12)
	addq	$12, %r12
	movl	%esi, -8(%r12)
	movl	%edx, -4(%r12)
	movq	%r12, %r14
	subq	%r9, %r14
	sarq	$2, %r14
	imull	%ecx, %r14d
.L3118:
	cmpl	%r14d, -72(%rbp)
	jle	.L3159
	movq	56(%r8), %rdi
.L3116:
	movq	64(%r8), %rax
	addq	$1, %r13
	subq	%rdi, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%r13, %rax
	ja	.L3128
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	cmpq	%r9, %r12
	je	.L3129
	movq	40(%rax), %rax
	leaq	_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L3163
	.p2align 4,,10
	.p2align 3
.L3131:
	movl	(%r8), %eax
	leal	-2(%rax), %r12d
	testq	%r9, %r9
	je	.L3107
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3107:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3164
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3159:
	.cfi_restore_state
	movq	-80(%rbp), %rax
	leaq	_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi(%rip), %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdi, %rax
	je	.L3131
	movq	%r8, -104(%rbp)
	movq	%r9, %rsi
	movq	-80(%rbp), %rdi
	movl	%r14d, %edx
	movq	%r9, -88(%rbp)
	movq	%r11, -96(%rbp)
	call	*%rax
	movq	-88(%rbp), %r9
	movq	-104(%rbp), %r8
	cmpl	$1, %eax
	je	.L3131
	movq	56(%r8), %rdi
	movq	-96(%rbp), %r11
	movq	%r9, %r12
	movabsq	$-6148914691236517205, %rcx
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3117:
	movabsq	$768614336404564650, %rdi
	movq	%r12, %rax
	subq	%r9, %rax
	movq	%rax, -88(%rbp)
	sarq	$2, %rax
	imulq	%rcx, %rax
	cmpq	%rdi, %rax
	je	.L3165
	testq	%rax, %rax
	je	.L3136
	movabsq	$9223372036854775800, %rdi
	leaq	(%rax,%rax), %r11
	cmpq	%r11, %rax
	jbe	.L3166
.L3120:
	movq	%r8, -120(%rbp)
	movq	%r9, -112(%rbp)
	movl	%r10d, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rdi
	movl	8(%r14), %esi
	movabsq	$-6148914691236517205, %rcx
	movl	4(%r14), %edx
	movl	-104(%rbp), %r10d
	movq	-112(%rbp), %r9
	movq	-120(%rbp), %r8
	leaq	(%rax,%rdi), %r11
	leaq	12(%rax), %rdi
.L3121:
	movq	-88(%rbp), %r14
	addq	%rax, %r14
	movl	%r10d, (%r14)
	movl	%esi, 4(%r14)
	movl	%edx, 8(%r14)
	cmpq	%r9, %r12
	je	.L3139
	movq	%rax, %rsi
	movq	%r9, %rdx
	.p2align 4,,10
	.p2align 3
.L3123:
	movq	(%rdx), %rdi
	addq	$12, %rdx
	addq	$12, %rsi
	movq	%rdi, -12(%rsi)
	movl	-4(%rdx), %edi
	movl	%edi, -4(%rsi)
	cmpq	%r12, %rdx
	jne	.L3123
	movabsq	$3074457345618258603, %rdi
	leaq	-12(%r12), %rdx
	subq	%r9, %rdx
	shrq	$2, %rdx
	imulq	%rdi, %rdx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	24(,%rdx,4), %r14
	leaq	(%rax,%r14), %r12
	sarq	$2, %r14
	imull	%ecx, %r14d
.L3122:
	testq	%r9, %r9
	je	.L3124
	movq	%r9, %rdi
	movq	%r8, -104(%rbp)
	movq	%r11, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %r11
	movabsq	$-6148914691236517205, %rcx
	movq	-88(%rbp), %rax
.L3124:
	movq	%rax, %r9
	jmp	.L3118
.L3166:
	testq	%r11, %r11
	jne	.L3167
	movl	$12, %edi
	xorl	%r11d, %r11d
	xorl	%eax, %eax
	jmp	.L3121
.L3135:
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L3112
.L3136:
	movl	$12, %edi
	jmp	.L3120
.L3163:
	movq	%r12, %rdx
	movq	-80(%rbp), %rbx
	movq	%r8, -88(%rbp)
	movq	%r9, %rsi
	subq	%r9, %rdx
	movq	%r9, -72(%rbp)
	sarq	$2, %rdx
	movq	%rbx, %rdi
	imulq	%rcx, %rdx
	call	*%rax
	movq	-72(%rbp), %r9
	movq	-88(%rbp), %r8
	cmpl	$1, %eax
	je	.L3131
	movq	(%rbx), %rax
.L3129:
	movq	%r8, -88(%rbp)
	movq	-80(%rbp), %rdi
	movq	%r9, -72(%rbp)
	call	*16(%rax)
	movq	-128(%rbp), %rbx
	movq	-72(%rbp), %r9
	movq	-88(%rbp), %r8
	testq	%rbx, %rbx
	je	.L3131
	movq	64(%r8), %rax
	movq	56(%r8), %rdx
	leaq	-64(%rbp), %rdi
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	-8(%rax), %rax
	subq	16(%rdx), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r8
	movq	%rax, (%rbx)
	jmp	.L3131
.L3108:
	leaq	56(%r8), %rdi
	movq	%r8, %rdx
	movq	%r12, %rsi
	movq	%r8, -72(%rbp)
	call	_ZNSt6vectorIN2v88internal14HeapObjectsMap12TimeIntervalESaIS3_EE17_M_realloc_insertIJRjEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	-72(%rbp), %r8
	jmp	.L3109
.L3162:
	movq	%r8, -88(%rbp)
	movq	-80(%rbp), %rdi
	call	*%rax
	movq	-88(%rbp), %r8
	movl	%eax, -72(%rbp)
	jmp	.L3110
.L3139:
	movq	%rdi, %r12
	movl	$1, %r14d
	jmp	.L3122
.L3111:
	movq	-80(%rbp), %rax
	xorl	%r9d, %r9d
	movq	(%rax), %rax
	jmp	.L3129
.L3164:
	call	__stack_chk_fail@PLT
.L3167:
	movabsq	$768614336404564650, %rax
	cmpq	%rax, %r11
	movq	%rax, %rdi
	cmovbe	%r11, %rdi
	imulq	$12, %rdi, %rdi
	jmp	.L3120
.L3165:
	leaq	.LC31(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE19872:
	.size	_ZN2v88internal14HeapObjectsMap20PushHeapObjectsStatsEPNS_12OutputStreamEPl, .-_ZN2v88internal14HeapObjectsMap20PushHeapObjectsStatsEPNS_12OutputStreamEPl
	.section	.text._ZN2v88internal14V8HeapExplorer8AddEntryEmNS0_9HeapEntry4TypeEPKcm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer8AddEntryEmNS0_9HeapEntry4TypeEPKcm
	.type	_ZN2v88internal14V8HeapExplorer8AddEntryEmNS0_9HeapEntry4TypeEPKcm, @function
_ZN2v88internal14V8HeapExplorer8AddEntryEmNS0_9HeapEntry4TypeEPKcm:
.LFB19970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	movl	$1, %ecx
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	movl	%r8d, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	32(%rdi), %rdi
	call	_ZN2v88internal14HeapObjectsMap14FindOrAddEntryEmjb
	movq	16(%rbx), %r10
	xorl	%r9d, %r9d
	movl	%eax, %ecx
	movq	(%r10), %rax
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3169
	addq	$408, %rdi
	movq	%r13, %rsi
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal17AddressToTraceMap14GetTraceNodeIdEm@PLT
	movq	16(%rbx), %r10
	movl	-52(%rbp), %ecx
	movl	%eax, %r9d
.L3169:
	addq	$24, %rsp
	movq	%r12, %r8
	movq	%r15, %rdx
	movl	%r14d, %esi
	popq	%rbx
	movq	%r10, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12HeapSnapshot8AddEntryENS0_9HeapEntry4TypeEPKcjmj
	.cfi_endproc
.LFE19970:
	.size	_ZN2v88internal14V8HeapExplorer8AddEntryEmNS0_9HeapEntry4TypeEPKcm, .-_ZN2v88internal14V8HeapExplorer8AddEntryEmNS0_9HeapEntry4TypeEPKcm
	.section	.rodata._ZN2v88internal31JSArrayBufferDataEntryAllocator13AllocateEntryEPv.str1.1,"aMS",@progbits,1
.LC221:
	.string	"system / JSArrayBufferData"
	.section	.text._ZN2v88internal31JSArrayBufferDataEntryAllocator13AllocateEntryEPv,"axG",@progbits,_ZN2v88internal31JSArrayBufferDataEntryAllocator13AllocateEntryEPv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal31JSArrayBufferDataEntryAllocator13AllocateEntryEPv
	.type	_ZN2v88internal31JSArrayBufferDataEntryAllocator13AllocateEntryEPv, @function
_ZN2v88internal31JSArrayBufferDataEntryAllocator13AllocateEntryEPv:
.LFB20005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	16(%rdi), %rbx
	movq	8(%rdi), %r14
	movq	32(%rbx), %rdi
	movl	%r14d, %edx
	call	_ZN2v88internal14HeapObjectsMap14FindOrAddEntryEmjb
	movq	16(%rbx), %r10
	xorl	%r9d, %r9d
	movl	%eax, %r13d
	movq	(%r10), %rax
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3175
	addq	$408, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal17AddressToTraceMap14GetTraceNodeIdEm@PLT
	movq	16(%rbx), %r10
	movl	%eax, %r9d
.L3175:
	popq	%rbx
	movq	%r14, %r8
	popq	%r12
	movl	%r13d, %ecx
	leaq	.LC221(%rip), %rdx
	popq	%r13
	movl	$8, %esi
	popq	%r14
	movq	%r10, %rdi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12HeapSnapshot8AddEntryENS0_9HeapEntry4TypeEPKcjmj
	.cfi_endproc
.LFE20005:
	.size	_ZN2v88internal31JSArrayBufferDataEntryAllocator13AllocateEntryEPv, .-_ZN2v88internal31JSArrayBufferDataEntryAllocator13AllocateEntryEPv
	.section	.text._ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
	.type	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc, @function
_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc:
.LFB19969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-56(%rbp), %rdi
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	movq	-1(%rsi), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	32(%rbx), %rdi
	movl	$1, %ecx
	movslq	%eax, %r15
	movq	-56(%rbp), %rax
	movq	%r15, %rdx
	leaq	-1(%rax), %r14
	movq	%r14, %rsi
	call	_ZN2v88internal14HeapObjectsMap14FindOrAddEntryEmjb
	movq	16(%rbx), %r10
	xorl	%r9d, %r9d
	movl	%eax, %ecx
	movq	(%r10), %rax
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3181
	addq	$408, %rdi
	movq	%r14, %rsi
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal17AddressToTraceMap14GetTraceNodeIdEm@PLT
	movq	16(%rbx), %r10
	movl	-60(%rbp), %ecx
	movl	%eax, %r9d
.L3181:
	movq	%r15, %r8
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	%r10, %rdi
	call	_ZN2v88internal12HeapSnapshot8AddEntryENS0_9HeapEntry4TypeEPKcjmj
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19969:
	.size	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc, .-_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
	.section	.rodata._ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectE.str1.1,"aMS",@progbits,1
.LC222:
	.string	"native_bind"
.LC223:
	.string	"(concatenated string)"
.LC224:
	.string	"(sliced string)"
.LC225:
	.string	"private symbol"
.LC226:
	.string	"symbol"
.LC227:
	.string	"bigint"
.LC228:
	.string	"system / NativeContext"
.LC229:
	.string	"system / Context"
.LC230:
	.string	"number"
.LC231:
	.string	"%s / %s"
	.section	.text._ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectE
	.type	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectE, @function
_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectE:
.LFB19968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-1(%rsi), %rax
	cmpw	$1105, 11(%rax)
	je	.L3225
	movq	-1(%rsi), %rax
	cmpw	$1104, 11(%rax)
	je	.L3226
	movq	-1(%rsi), %rax
	cmpw	$1075, 11(%rax)
	je	.L3227
	movq	-1(%rsi), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L3191
	movq	24(%rdi), %r14
	movq	%rsi, %rdi
	call	_ZN2v88internal14V8HeapExplorer18GetConstructorNameENS0_8JSObjectE
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	movq	%rax, %r8
	movq	-1(%r12), %rax
	cmpw	$1025, 11(%rax)
	je	.L3228
.L3192:
	movq	%r8, %rcx
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
	jmp	.L3186
	.p2align 4,,10
	.p2align 3
.L3226:
	leaq	.LC222(%rip), %rcx
	movl	$5, %edx
	call	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
.L3186:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L3229
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3225:
	.cfi_restore_state
	movq	23(%rsi), %rax
	movq	24(%rdi), %r14
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo4NameEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
	jmp	.L3186
	.p2align 4,,10
	.p2align 3
.L3191:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	movq	-1(%rsi), %rax
	ja	.L3197
	cmpw	$63, 11(%rax)
	jbe	.L3230
.L3198:
	movq	-1(%r12), %rax
	cmpw	$63, 11(%rax)
	jbe	.L3231
.L3199:
	movq	24(%r13), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
	jmp	.L3186
	.p2align 4,,10
	.p2align 3
.L3227:
	movq	23(%rsi), %rax
	movq	24(%rdi), %rdi
	movq	23(%rax), %rsi
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
	jmp	.L3186
	.p2align 4,,10
	.p2align 3
.L3197:
	cmpw	$64, 11(%rax)
	jne	.L3200
	testb	$1, 11(%rsi)
	je	.L3201
	leaq	.LC225(%rip), %rcx
	xorl	%edx, %edx
	call	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
	jmp	.L3186
	.p2align 4,,10
	.p2align 3
.L3230:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L3198
	leaq	.LC223(%rip), %rcx
	movl	$10, %edx
	call	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
	jmp	.L3186
	.p2align 4,,10
	.p2align 3
.L3200:
	movq	-1(%rsi), %rax
	cmpw	$66, 11(%rax)
	je	.L3232
	movq	-1(%rsi), %rax
	cmpw	$69, 11(%rax)
	je	.L3233
	movq	-1(%rsi), %rax
	cmpw	$160, 11(%rax)
	je	.L3234
	movq	-1(%rsi), %rax
	cmpw	$96, 11(%rax)
	jne	.L3205
	movq	15(%rsi), %rsi
	testb	$1, %sil
	jne	.L3206
.L3208:
	leaq	.LC17(%rip), %rcx
.L3207:
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
	jmp	.L3186
	.p2align 4,,10
	.p2align 3
.L3231:
	movq	-1(%r12), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$3, %ax
	jne	.L3199
	leaq	.LC224(%rip), %rcx
	movl	$11, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
	jmp	.L3186
	.p2align 4,,10
	.p2align 3
.L3228:
	movq	64(%r13), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	56(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L3192
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L3196
	.p2align 4,,10
	.p2align 3
.L3194:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L3192
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L3192
.L3196:
	cmpq	%rsi, %r12
	jne	.L3194
	cmpq	8(%rcx), %r12
	jne	.L3194
	movq	16(%rcx), %rcx
	movq	24(%r13), %rdi
	movq	%r8, %rdx
	xorl	%eax, %eax
	leaq	.LC231(%rip), %rsi
	call	_ZN2v88internal14StringsStorage12GetFormattedEPKcz@PLT
	movq	%rax, %r8
	jmp	.L3192
	.p2align 4,,10
	.p2align 3
.L3201:
	leaq	.LC226(%rip), %rcx
	movl	$12, %edx
	call	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
	jmp	.L3186
	.p2align 4,,10
	.p2align 3
.L3232:
	leaq	.LC227(%rip), %rcx
	movl	$13, %edx
	call	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
	jmp	.L3186
.L3233:
	leaq	.LC17(%rip), %rcx
	movl	$4, %edx
	call	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
	jmp	.L3186
.L3234:
	leaq	-48(%rbp), %rdi
	movq	%rsi, -48(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo4NameEv
	movq	%rax, %rsi
.L3224:
	movq	24(%r13), %rdi
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	movq	%rax, %rcx
	jmp	.L3207
.L3205:
	movq	-1(%rsi), %rax
	cmpw	$145, 11(%rax)
	je	.L3235
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subw	$138, %ax
	cmpw	$9, %ax
	ja	.L3210
	leaq	.LC229(%rip), %rcx
	movl	$3, %edx
	call	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
	jmp	.L3186
.L3206:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L3208
	jmp	.L3224
.L3235:
	leaq	.LC228(%rip), %rcx
	xorl	%edx, %edx
	call	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
	jmp	.L3186
.L3210:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	jbe	.L3211
	movq	-1(%rsi), %rax
	cmpw	$74, 11(%rax)
	je	.L3211
	movq	-1(%rsi), %rax
	cmpw	$70, 11(%rax)
	je	.L3211
	movq	-1(%rsi), %rax
	cmpw	$65, 11(%rax)
	je	.L3236
	call	_ZN2v88internal14V8HeapExplorer18GetSystemEntryNameENS0_10HeapObjectE
	xorl	%edx, %edx
	movq	%rax, %rcx
	call	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
	jmp	.L3186
	.p2align 4,,10
	.p2align 3
.L3211:
	leaq	.LC17(%rip), %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
	jmp	.L3186
.L3229:
	call	__stack_chk_fail@PLT
.L3236:
	leaq	.LC230(%rip), %rcx
	movl	$7, %edx
	call	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectENS0_9HeapEntry4TypeEPKc
	jmp	.L3186
	.cfi_endproc
.LFE19968:
	.size	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectE, .-_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectE
	.section	.text._ZN2v88internal14V8HeapExplorer13AllocateEntryEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer13AllocateEntryEPv
	.type	_ZN2v88internal14V8HeapExplorer13AllocateEntryEPv, @function
_ZN2v88internal14V8HeapExplorer13AllocateEntryEPv:
.LFB19965:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal14V8HeapExplorer8AddEntryENS0_10HeapObjectE
	.cfi_endproc
.LFE19965:
	.size	_ZN2v88internal14V8HeapExplorer13AllocateEntryEPv, .-_ZN2v88internal14V8HeapExplorer13AllocateEntryEPv
	.section	.rodata._ZN2v88internal14V8HeapExplorer30ExtractJSArrayBufferReferencesEPNS0_9HeapEntryENS0_13JSArrayBufferE.str1.1,"aMS",@progbits,1
.LC232:
	.string	"backing_store"
	.section	.text._ZN2v88internal14V8HeapExplorer30ExtractJSArrayBufferReferencesEPNS0_9HeapEntryENS0_13JSArrayBufferE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer30ExtractJSArrayBufferReferencesEPNS0_9HeapEntryENS0_13JSArrayBufferE
	.type	_ZN2v88internal14V8HeapExplorer30ExtractJSArrayBufferReferencesEPNS0_9HeapEntryENS0_13JSArrayBufferE, @function
_ZN2v88internal14V8HeapExplorer30ExtractJSArrayBufferReferencesEPNS0_9HeapEntryENS0_13JSArrayBufferE:
.LFB20006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	31(%rdx), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	jne	.L3271
.L3238:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3272
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3271:
	.cfi_restore_state
	movq	48(%rdi), %r14
	movq	%rdi, %r13
	movq	23(%rdx), %r15
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rsi, %rbx
	movq	344(%r14), %rdi
	divq	%rdi
	movq	336(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L3241
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L3243
	.p2align 4,,10
	.p2align 3
.L3273:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L3241
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L3241
.L3243:
	cmpq	%rsi, %r12
	jne	.L3273
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L3241
.L3249:
	addl	$1, 4(%rbx)
	leaq	.LC232(%rip), %rdi
	leaq	-64(%rbp), %rcx
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	leaq	-80(%rbp), %rdx
	leaq	-84(%rbp), %rsi
	movq	%rdi, -80(%rbp)
	leaq	-72(%rbp), %r8
	leaq	296(%rax), %rdi
	movl	$3, -84(%rbp)
	movq	%rbx, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERPKcPNS1_9HeapEntryERSC_EEEvDpOT_
	jmp	.L3238
	.p2align 4,,10
	.p2align 3
.L3241:
	movq	32(%r13), %rdi
	movl	$1, %ecx
	movl	%r15d, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal14HeapObjectsMap14FindOrAddEntryEmjb
	movq	16(%r13), %r10
	xorl	%r9d, %r9d
	movl	%eax, %ecx
	movq	(%r10), %rax
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3244
	addq	$408, %rdi
	movq	%r12, %rsi
	movl	%ecx, -100(%rbp)
	call	_ZN2v88internal17AddressToTraceMap14GetTraceNodeIdEm@PLT
	movq	16(%r13), %r10
	movl	-100(%rbp), %ecx
	movl	%eax, %r9d
.L3244:
	leaq	.LC221(%rip), %rdx
	movl	$8, %esi
	movq	%r10, %rdi
	movq	%r15, %r8
	call	_ZN2v88internal12HeapSnapshot8AddEntryENS0_9HeapEntry4TypeEPKcjmj
	movl	$24, %edi
	movq	%rax, %r13
	call	_Znwm@PLT
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%r12, 8(%rax)
	movq	%r13, 16(%rax)
	movq	344(%r14), %rsi
	movq	%r12, %rax
	divq	%rsi
	movq	336(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L3245
	movq	(%rax), %r13
	movq	8(%r13), %rcx
	jmp	.L3247
	.p2align 4,,10
	.p2align 3
.L3274:
	movq	0(%r13), %r13
	testq	%r13, %r13
	je	.L3245
	movq	8(%r13), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L3245
.L3247:
	cmpq	%rcx, %r12
	jne	.L3274
	call	_ZdlPv@PLT
.L3248:
	movq	16(%r13), %rax
	jmp	.L3249
	.p2align 4,,10
	.p2align 3
.L3245:
	addq	$336, %r14
	movq	%rdi, %rcx
	movq	%r12, %rdx
	movq	%r9, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %r13
	jmp	.L3248
.L3272:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20006:
	.size	_ZN2v88internal14V8HeapExplorer30ExtractJSArrayBufferReferencesEPNS0_9HeapEntryENS0_13JSArrayBufferE, .-_ZN2v88internal14V8HeapExplorer30ExtractJSArrayBufferReferencesEPNS0_9HeapEntryENS0_13JSArrayBufferE
	.section	.text._ZN2v88internal14V8HeapExplorer17ExtractReferencesEPNS0_9HeapEntryENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer17ExtractReferencesEPNS0_9HeapEntryENS0_10HeapObjectE
	.type	_ZN2v88internal14V8HeapExplorer17ExtractReferencesEPNS0_9HeapEntryENS0_10HeapObjectE, @function
_ZN2v88internal14V8HeapExplorer17ExtractReferencesEPNS0_9HeapEntryENS0_10HeapObjectE:
.LFB19981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	-1(%rdx), %rax
	cmpw	$1026, 11(%rax)
	je	.L3322
	movq	-1(%rdx), %rax
	cmpw	$1059, 11(%rax)
	je	.L3323
	movq	-1(%rdx), %rax
	cmpw	$1024, 11(%rax)
	movq	-1(%rdx), %rax
	jbe	.L3278
	cmpw	$1085, 11(%rax)
	je	.L3319
	movq	-1(%rdx), %rax
	cmpw	$1084, 11(%rax)
	je	.L3319
	movq	-1(%rdx), %rax
	cmpw	$1077, 11(%rax)
	je	.L3319
	movq	-1(%rdx), %rax
	cmpw	$1069, 11(%rax)
	je	.L3319
	movq	-1(%rdx), %rax
	cmpw	$1074, 11(%rax)
	je	.L3324
	movq	-1(%rdx), %rax
	cmpw	$1068, 11(%rax)
	je	.L3286
	movq	-1(%rdx), %rax
	cmpw	$1063, 11(%rax)
	je	.L3286
	movq	-1(%rdx), %rax
	cmpw	$1064, 11(%rax)
	jne	.L3280
	.p2align 4,,10
	.p2align 3
.L3286:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal14V8HeapExplorer34ExtractJSGeneratorObjectReferencesEPNS0_9HeapEntryENS0_17JSGeneratorObjectE
	movq	-40(%rbp), %rdi
	jmp	.L3280
	.p2align 4,,10
	.p2align 3
.L3323:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer30ExtractJSArrayBufferReferencesEPNS0_9HeapEntryENS0_13JSArrayBufferE
	.p2align 4,,10
	.p2align 3
.L3322:
	.cfi_restore_state
	movq	23(%rdx), %rcx
	movl	$24, %r8d
	leaq	.LC184(%rip), %rdx
.L3320:
	movq	%r13, %rsi
.L3321:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	.p2align 4,,10
	.p2align 3
.L3278:
	.cfi_restore_state
	cmpw	$63, 11(%rax)
	jbe	.L3325
	movq	-1(%rdx), %rax
	cmpw	$64, 11(%rax)
	je	.L3326
	movq	-1(%rdx), %rax
	cmpw	$68, 11(%rax)
	je	.L3327
	movq	-1(%rdx), %rax
	cmpw	$160, 11(%rax)
	je	.L3328
	movq	-1(%rdx), %rax
	cmpw	$96, 11(%rax)
	je	.L3329
	movq	-1(%rdx), %rax
	cmpw	$78, 11(%rax)
	je	.L3330
	movq	-1(%rdx), %rax
	cmpw	$79, 11(%rax)
	je	.L3331
	movq	-1(%rdx), %rax
	cmpw	$69, 11(%rax)
	je	.L3332
	movq	-1(%rdx), %rax
	cmpw	$151, 11(%rax)
	je	.L3333
	movq	-1(%rdx), %rax
	cmpw	$154, 11(%rax)
	je	.L3334
	movq	-1(%rdx), %rax
	cmpw	$159, 11(%rax)
	je	.L3335
	movq	-1(%rdx), %rax
	cmpw	$121, 11(%rax)
	je	.L3336
	movq	-1(%rdx), %rax
	cmpw	$82, 11(%rax)
	je	.L3337
	movq	-1(%rdx), %rax
	cmpw	$155, 11(%rax)
	je	.L3338
	movq	-1(%rdx), %rax
	cmpw	$153, 11(%rax)
	je	.L3339
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	subw	$148, %ax
	cmpw	$1, %ax
	jbe	.L3340
	movq	-1(%rdx), %rax
	cmpw	$167, 11(%rax)
	je	.L3341
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	subw	$138, %ax
	cmpw	$9, %ax
	ja	.L3313
	popq	%r11
	popq	%rbx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer24ExtractContextReferencesEPNS0_9HeapEntryENS0_7ContextE
	.p2align 4,,10
	.p2align 3
.L3325:
	.cfi_restore_state
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer23ExtractStringReferencesEPNS0_9HeapEntryENS0_6StringE
	.p2align 4,,10
	.p2align 3
.L3319:
	.cfi_restore_state
	movq	23(%r12), %rcx
	movl	$24, %r8d
	movq	%r13, %rsi
	movq	%rdi, -40(%rbp)
	leaq	.LC193(%rip), %rdx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	-40(%rbp), %rdi
.L3280:
	addq	$16, %rsp
	movq	%r12, %rdx
	movq	%r13, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer25ExtractJSObjectReferencesEPNS0_9HeapEntryENS0_8JSObjectE
	.p2align 4,,10
	.p2align 3
.L3327:
	.cfi_restore_state
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer20ExtractMapReferencesEPNS0_9HeapEntryENS0_3MapE
	.p2align 4,,10
	.p2align 3
.L3326:
	.cfi_restore_state
	movq	15(%rdx), %rcx
	movl	$16, %r8d
	leaq	.LC133(%rip), %rdx
	jmp	.L3320
	.p2align 4,,10
	.p2align 3
.L3328:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer35ExtractSharedFunctionInfoReferencesEPNS0_9HeapEntryENS0_18SharedFunctionInfoE
	.p2align 4,,10
	.p2align 3
.L3324:
	.cfi_restore_state
	movq	23(%rdx), %rcx
	movl	$24, %r8d
	leaq	.LC195(%rip), %rdx
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	-40(%rbp), %rdi
	jmp	.L3280
	.p2align 4,,10
	.p2align 3
.L3329:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer23ExtractScriptReferencesEPNS0_9HeapEntryENS0_6ScriptE
.L3330:
	.cfi_restore_state
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer29ExtractAccessorInfoReferencesEPNS0_9HeapEntryENS0_12AccessorInfoE
.L3331:
	.cfi_restore_state
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer29ExtractAccessorPairReferencesEPNS0_9HeapEntryENS0_12AccessorPairE
.L3332:
	.cfi_restore_state
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer21ExtractCodeReferencesEPNS0_9HeapEntryENS0_4CodeE
.L3333:
	.cfi_restore_state
	movq	7(%rdx), %rcx
	movl	$8, %r8d
	leaq	.LC160(%rip), %rdx
	jmp	.L3320
.L3334:
	leaq	.LC159(%rip), %rdx
	movq	%r12, %rsi
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal14V8HeapExplorer9TagObjectENS0_6ObjectEPKc
	movq	7(%r12), %rcx
	movq	-40(%rbp), %rdi
	movl	$8, %r8d
	leaq	.LC160(%rip), %rdx
	movq	%r13, %rsi
	jmp	.L3321
.L3335:
	popq	%r8
	popq	%r9
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer29ExtractPropertyCellReferencesEPNS0_9HeapEntryENS0_12PropertyCellE
.L3336:
	.cfi_restore_state
	popq	%rax
	popq	%rcx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer31ExtractAllocationSiteReferencesEPNS0_9HeapEntryENS0_14AllocationSiteE
.L3337:
	.cfi_restore_state
	movq	15(%rdx), %rcx
	movl	$16, %r8d
	leaq	.LC194(%rip), %rdx
	jmp	.L3320
.L3338:
	popq	%r14
	popq	%rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer31ExtractFeedbackVectorReferencesEPNS0_9HeapEntryENS0_14FeedbackVectorE
.L3339:
	.cfi_restore_state
	popq	%r12
	popq	%r13
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer32ExtractDescriptorArrayReferencesEPNS0_9HeapEntryENS0_15DescriptorArrayE
.L3313:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	cmpw	$135, 11(%rax)
	je	.L3342
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	jbe	.L3343
.L3275:
	popq	%rax
	popq	%rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3341:
	.cfi_restore_state
	movq	15(%rdx), %rax
	leaq	23(%rdx), %r14
	xorl	%ebx, %ebx
	jmp	.L3312
.L3310:
	subq	$1, %rdx
	je	.L3344
.L3311:
	addl	$1, %ebx
	addq	$8, %r14
.L3312:
	movq	%rax, %rdx
	sarq	$32, %rdx
	cmpl	%edx, %ebx
	jge	.L3275
	movq	(%r14), %rcx
	movq	%rcx, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	jne	.L3310
	cmpl	$3, %ecx
	je	.L3311
	andq	$-3, %rcx
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rdi, -40(%rbp)
	leal	24(,%rbx,8), %r8d
	call	_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi.constprop.0
	movq	15(%r12), %rax
	movq	-40(%rbp), %rdi
	jmp	.L3311
.L3340:
	cmpl	$0, 11(%rdx)
	leaq	7(%rdx), %r14
	jle	.L3275
	addq	$15, %r12
	xorl	%ebx, %ebx
	jmp	.L3306
.L3304:
	subq	$1, %rax
	je	.L3345
.L3305:
	addl	$1, %ebx
	addq	$8, %r12
	cmpl	%ebx, 4(%r14)
	jle	.L3275
.L3306:
	movq	(%r12), %rcx
	movq	%rcx, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	jne	.L3304
	cmpl	$3, %ecx
	je	.L3305
	andq	$-3, %rcx
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rdi, -40(%rbp)
	leal	16(,%rbx,8), %r8d
	call	_ZN2v88internal14V8HeapExplorer16SetWeakReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi.constprop.0
	movq	-40(%rbp), %rdi
	jmp	.L3305
.L3345:
	leal	16(,%rbx,8), %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi.constprop.0
	movq	-40(%rbp), %rdi
	jmp	.L3305
.L3344:
	leal	24(,%rbx,8), %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEiNS0_6ObjectEi.constprop.0
	movq	15(%r12), %rax
	movq	-40(%rbp), %rdi
	jmp	.L3311
.L3343:
	popq	%rcx
	popq	%r8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer27ExtractFixedArrayReferencesEPNS0_9HeapEntryENS0_10FixedArrayE
.L3342:
	.cfi_restore_state
	popq	%r9
	popq	%r10
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14V8HeapExplorer35ExtractEphemeronHashTableReferencesEPNS0_9HeapEntryENS0_18EphemeronHashTableE
	.cfi_endproc
.LFE19981:
	.size	_ZN2v88internal14V8HeapExplorer17ExtractReferencesEPNS0_9HeapEntryENS0_10HeapObjectE, .-_ZN2v88internal14V8HeapExplorer17ExtractReferencesEPNS0_9HeapEntryENS0_10HeapObjectE
	.section	.rodata._ZN2v88internal14V8HeapExplorer27IterateAndExtractReferencesEPNS0_21HeapSnapshotGeneratorE.str1.1,"aMS",@progbits,1
.LC233:
	.string	"map"
	.section	.text._ZN2v88internal14V8HeapExplorer27IterateAndExtractReferencesEPNS0_21HeapSnapshotGeneratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14V8HeapExplorer27IterateAndExtractReferencesEPNS0_21HeapSnapshotGeneratorE
	.type	_ZN2v88internal14V8HeapExplorer27IterateAndExtractReferencesEPNS0_21HeapSnapshotGeneratorE, @function
_ZN2v88internal14V8HeapExplorer27IterateAndExtractReferencesEPNS0_21HeapSnapshotGeneratorE:
.LFB20026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	leaq	-192(%rbp), %r8
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, 48(%rdi)
	movq	8(%rdx), %rax
	movq	16(%rdx), %rcx
	movq	%r8, -280(%rbp)
	movl	4(%rax), %ebx
	movq	%rcx, -192(%rbp)
	movq	%r12, %rcx
	movl	$1, -224(%rbp)
	leal	1(%rbx), %edx
	movl	%edx, -240(%rbp)
	movq	16(%rax), %rbx
	movl	%edx, 4(%rax)
	leaq	-240(%rbp), %rdx
	movq	%rax, -128(%rbp)
	leaq	-224(%rbp), %rax
	leaq	296(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, -288(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE12emplace_backIJRNS2_4TypeERiPNS1_9HeapEntryERSA_EEEvDpOT_
	movq	%r12, -296(%rbp)
	movq	%r14, %r12
	movl	%r15d, %r14d
	jmp	.L3351
	.p2align 4,,10
	.p2align 3
.L3414:
	movl	(%r15), %edx
	addl	$1, %r14d
	movq	%rcx, 8(%rax)
	movl	%r13d, 16(%rax)
	shrl	$4, %edx
	leal	1(,%rdx,8), %edx
	movl	%edx, (%rax)
	addq	$24, 344(%rbx)
	cmpl	$24, %r14d
	je	.L3413
.L3351:
	movq	16(%r12), %rax
	movslq	%r14d, %rdx
	movq	16(%rax), %r15
	movq	24(%rax,%rdx,8), %rcx
	movl	4(%r15), %eax
	movq	16(%r15), %rbx
	leal	1(%rax), %r13d
	movl	%r13d, 4(%r15)
	movq	360(%rbx), %rdi
	movq	344(%rbx), %rax
	leaq	-24(%rdi), %rdx
	cmpq	%rdx, %rax
	jne	.L3414
	movq	368(%rbx), %rdx
	subq	352(%rbx), %rax
	sarq	$3, %rax
	movq	%rdx, %rsi
	subq	336(%rbx), %rsi
	sarq	$3, %rsi
	subq	$1, %rsi
	leaq	(%rsi,%rsi,4), %rdi
	leaq	(%rsi,%rdi,4), %rsi
	movabsq	$-6148914691236517205, %rdi
	imulq	%rdi, %rax
	addq	%rsi, %rax
	movq	328(%rbx), %rsi
	subq	312(%rbx), %rsi
	sarq	$3, %rsi
	imulq	%rdi, %rsi
	addq	%rax, %rsi
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rsi
	je	.L3415
	movq	304(%rbx), %rsi
	movq	%rdx, %rax
	subq	296(%rbx), %rax
	sarq	$3, %rax
	subq	%rax, %rsi
	cmpq	$1, %rsi
	jbe	.L3416
.L3350:
	movl	$504, %edi
	movq	%rdx, -272(%rbp)
	addl	$1, %r14d
	movq	%rcx, -264(%rbp)
	call	_Znwm@PLT
	movq	-272(%rbp), %rdx
	movq	-264(%rbp), %rcx
	movq	%rax, 8(%rdx)
	movl	(%r15), %eax
	movq	344(%rbx), %rdx
	shrl	$4, %eax
	leal	1(,%rax,8), %eax
	movq	%rcx, 8(%rdx)
	movl	%eax, (%rdx)
	movl	%r13d, 16(%rdx)
	movq	368(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 368(%rbx)
	movq	8(%rax), %rax
	leaq	504(%rax), %rdx
	movq	%rax, 352(%rbx)
	movq	%rdx, 360(%rbx)
	movq	%rax, 344(%rbx)
	cmpl	$24, %r14d
	jne	.L3351
	.p2align 4,,10
	.p2align 3
.L3413:
	movq	%r12, %r14
	leaq	16+_ZTVN2v88internal24RootsReferencesExtractorE(%rip), %rax
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %r12
	movq	%rax, -224(%rbp)
	movq	8(%r14), %rax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r14, -216(%rbp)
	subq	$37536, %rax
	movb	$0, -208(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal13ReadOnlyRoots7IterateEPNS0_11RootVisitorE@PLT
	movq	8(%r14), %rdi
	movl	$5, %edx
	movq	%rbx, %rsi
	call	_ZN2v88internal4Heap12IterateRootsEPNS0_11RootVisitorENS0_9VisitModeE@PLT
	movq	8(%r14), %rdi
	movq	%rbx, %rsi
	movb	$1, -208(%rbp)
	xorl	%ebx, %ebx
	call	_ZN2v88internal4Heap24IterateWeakGlobalHandlesEPNS0_11RootVisitorE@PLT
	movq	8(%r14), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal26CombinedHeapObjectIteratorC1EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal26CombinedHeapObjectIterator4NextEv@PLT
	leaq	-248(%rbp), %rcx
	movq	%rax, -248(%rbp)
	movq	%rcx, -264(%rbp)
	testq	%rax, %rax
	je	.L3408
	.p2align 4,,10
	.p2align 3
.L3352:
	testb	%bl, %bl
	jne	.L3355
	movq	-1(%rax), %rsi
	movq	-264(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	232(%r14), %rdi
	movl	256(%r14), %edx
	testl	%eax, %eax
	leal	7(%rax), %r15d
	cmovns	%eax, %r15d
	movq	248(%r14), %rax
	sarl	$3, %r15d
	subq	%rdi, %rax
	movslq	%r15d, %r15
	leaq	(%rdx,%rax,8), %rax
	cmpq	%rax, %r15
	ja	.L3417
.L3356:
	movq	-248(%rbp), %r15
	xorl	%r13d, %r13d
	testb	$1, %r15b
	je	.L3360
	movq	48(%r14), %r8
	movq	%r15, %rax
	xorl	%edx, %edx
	movq	344(%r8), %rdi
	divq	%rdi
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L3361
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L3363
	.p2align 4,,10
	.p2align 3
.L3418:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L3361
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L3361
.L3363:
	cmpq	%rsi, %r15
	jne	.L3418
	movq	16(%rcx), %r13
	testq	%r13, %r13
	je	.L3361
.L3360:
	movq	-248(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14V8HeapExplorer17ExtractReferencesEPNS0_9HeapEntryENS0_10HeapObjectE
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-248(%rbp), %rax
	leaq	.LC233(%rip), %rdx
	movq	-1(%rax), %rcx
	call	_ZN2v88internal14V8HeapExplorer20SetInternalReferenceEPNS0_9HeapEntryEPKcNS0_6ObjectEi
	movq	-248(%rbp), %rax
	leaq	16+_ZTVN2v88internal26IndexedReferencesExtractorE(%rip), %rcx
	movq	%r14, -184(%rbp)
	movq	%rcx, -192(%rbp)
	leaq	-176(%rbp), %rdi
	leaq	-1(%rax), %rdx
	movq	%rax, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-176(%rbp), %rdx
	movq	-280(%rbp), %rsi
	movq	%r13, -152(%rbp)
	movq	-264(%rbp), %rdi
	cltq
	movl	$0, -144(%rbp)
	leaq	-1(%rdx,%rax), %rax
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal10HeapObject7IterateEPNS0_13ObjectVisitorE@PLT
	movq	-248(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal14V8HeapExplorer15ExtractLocationEPNS0_9HeapEntryENS0_10HeapObjectE
	movq	40(%r14), %rdi
	leaq	_ZN2v88internal21HeapSnapshotGenerator14ProgressReportEb(%rip), %rcx
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3367
	movq	16(%rdi), %r8
	testq	%r8, %r8
	je	.L3355
	movl	392(%rdi), %esi
	imull	$989560465, %esi, %eax
	addl	$3435968, %eax
	rorl	$4, %eax
	cmpl	$429496, %eax
	ja	.L3355
	movq	(%r8), %rax
	movl	396(%rdi), %edx
	movq	%r8, %rdi
	call	*16(%rax)
	testl	%eax, %eax
	sete	%bl
.L3369:
	xorl	$1, %ebx
.L3355:
	movq	%r12, %rdi
	call	_ZN2v88internal26CombinedHeapObjectIterator4NextEv@PLT
	movq	40(%r14), %rdi
	leaq	_ZN2v88internal21HeapSnapshotGenerator12ProgressStepEv(%rip), %rcx
	movq	%rax, -248(%rbp)
	movq	(%rdi), %rdx
	movq	16(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L3370
	addl	$1, 392(%rdi)
	testq	%rax, %rax
	jne	.L3352
	movq	$0, 48(%r14)
	xorl	%r13d, %r13d
	testb	%bl, %bl
	je	.L3373
.L3374:
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3419
	addq	$264, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3416:
	.cfi_restore_state
	xorl	%edx, %edx
	leaq	296(%rbx), %rdi
	movl	$1, %esi
	movq	%rcx, -264(%rbp)
	call	_ZNSt5dequeIN2v88internal13HeapGraphEdgeESaIS2_EE17_M_reallocate_mapEmb
	movq	368(%rbx), %rdx
	movq	-264(%rbp), %rcx
	jmp	.L3350
	.p2align 4,,10
	.p2align 3
.L3370:
	call	*%rdx
	movq	-248(%rbp), %rax
	testq	%rax, %rax
	jne	.L3352
	movq	$0, 48(%r14)
	xorl	%r13d, %r13d
	testb	%bl, %bl
	jne	.L3374
	.p2align 4,,10
	.p2align 3
.L3373:
	movq	40(%r14), %rdi
	leaq	_ZN2v88internal21HeapSnapshotGenerator14ProgressReportEb(%rip), %rdx
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3375
	movq	16(%rdi), %r8
	movl	$1, %r13d
	testq	%r8, %r8
	je	.L3374
	movq	(%r8), %rax
	movl	396(%rdi), %edx
	movl	392(%rdi), %esi
	movq	%r8, %rdi
	call	*16(%rax)
	testl	%eax, %eax
	sete	%r13b
	jmp	.L3374
	.p2align 4,,10
	.p2align 3
.L3417:
	movq	$0, -192(%rbp)
	movl	$0, -184(%rbp)
	movq	$0, 232(%r14)
	movl	$0, 240(%r14)
	movq	$0, 248(%r14)
	movl	$0, 256(%r14)
	movq	$0, 264(%r14)
	testq	%rdi, %rdi
	je	.L3357
	call	_ZdlPv@PLT
.L3357:
	movq	248(%r14), %rsi
	movq	232(%r14), %rdx
	movl	256(%r14), %eax
	movq	%rsi, %rdi
	subq	%rdx, %rdi
	movq	%rax, %rcx
	leaq	(%rax,%rdi,8), %rax
	cmpq	%rax, %r15
	jnb	.L3358
	testq	%r15, %r15
	leaq	63(%r15), %rax
	cmovns	%r15, %rax
	sarq	$6, %rax
	leaq	(%rdx,%rax,8), %rdx
	movq	%r15, %rax
	sarq	$63, %rax
	shrq	$58, %rax
	addq	%rax, %r15
	andl	$63, %r15d
	subq	%rax, %r15
	jns	.L3359
	addq	$64, %r15
	subq	$8, %rdx
.L3359:
	movq	%rdx, 248(%r14)
	movl	%r15d, 256(%r14)
	jmp	.L3356
	.p2align 4,,10
	.p2align 3
.L3367:
	xorl	%esi, %esi
	call	*%rax
	movl	%eax, %ebx
	jmp	.L3369
	.p2align 4,,10
	.p2align 3
.L3358:
	movl	%ecx, -232(%rbp)
	movq	%r15, %rcx
	xorl	%r8d, %r8d
	movq	-232(%rbp), %rdx
	subq	%rax, %rcx
	leaq	232(%r14), %rdi
	movq	%rsi, -240(%rbp)
	call	_ZNSt6vectorIbSaIbEE14_M_fill_insertESt13_Bit_iteratormb
	jmp	.L3356
	.p2align 4,,10
	.p2align 3
.L3361:
	movq	(%r14), %rax
	movq	%r8, -272(%rbp)
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*16(%rax)
	movl	$24, %edi
	movq	%rax, %r13
	call	_Znwm@PLT
	movq	-272(%rbp), %r8
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%r15, 8(%rax)
	movq	%r13, 16(%rax)
	movq	344(%r8), %rsi
	movq	%r15, %rax
	divq	%rsi
	movq	336(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L3364
	movq	(%rax), %r13
	movq	8(%r13), %rcx
	jmp	.L3366
	.p2align 4,,10
	.p2align 3
.L3420:
	movq	0(%r13), %r13
	testq	%r13, %r13
	je	.L3364
	movq	8(%r13), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L3364
.L3366:
	cmpq	%rcx, %r15
	jne	.L3420
	call	_ZdlPv@PLT
.L3376:
	movq	16(%r13), %r13
	jmp	.L3360
	.p2align 4,,10
	.p2align 3
.L3364:
	leaq	336(%r8), %r9
	movq	%rdi, %rcx
	movq	%r15, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	movq	%r9, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_PN2v88internal9HeapEntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	%rax, %r13
	jmp	.L3376
.L3408:
	movq	$0, 48(%r14)
	jmp	.L3373
.L3375:
	movl	$1, %esi
	call	*%rax
	movl	%eax, %r13d
	jmp	.L3374
.L3415:
	leaq	.LC32(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3419:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20026:
	.size	_ZN2v88internal14V8HeapExplorer27IterateAndExtractReferencesEPNS0_21HeapSnapshotGeneratorE, .-_ZN2v88internal14V8HeapExplorer27IterateAndExtractReferencesEPNS0_21HeapSnapshotGeneratorE
	.section	.text._ZN2v88internal21HeapSnapshotGenerator14FillReferencesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21HeapSnapshotGenerator14FillReferencesEv
	.type	_ZN2v88internal21HeapSnapshotGenerator14FillReferencesEv, @function
_ZN2v88internal21HeapSnapshotGenerator14FillReferencesEv:
.LFB20170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$24, %rdi
	movq	%r12, %rsi
	subq	$8, %rsp
	call	_ZN2v88internal14V8HeapExplorer27IterateAndExtractReferencesEPNS0_21HeapSnapshotGeneratorE
	testb	%al, %al
	jne	.L3424
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3424:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	296(%r12), %rdi
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21NativeObjectsExplorer27IterateAndExtractReferencesEPNS0_21HeapSnapshotGeneratorE
	.cfi_endproc
.LFE20170:
	.size	_ZN2v88internal21HeapSnapshotGenerator14FillReferencesEv, .-_ZN2v88internal21HeapSnapshotGenerator14FillReferencesEv
	.section	.text._ZN2v88internal21HeapSnapshotGenerator16GenerateSnapshotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21HeapSnapshotGenerator16GenerateSnapshotEv
	.type	_ZN2v88internal21HeapSnapshotGenerator16GenerateSnapshotEv, @function
_ZN2v88internal21HeapSnapshotGenerator16GenerateSnapshotEv:
.LFB20166:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	24(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal14V8HeapExplorer16TagGlobalObjectsEv
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	$11, %edx
	movq	400(%r12), %rdi
	call	_ZN2v88internal4Heap24PreciseCollectAllGarbageEiNS0_23GarbageCollectionReasonENS_15GCCallbackFlagsE@PLT
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	$11, %edx
	movq	400(%r12), %rdi
	call	_ZN2v88internal4Heap24PreciseCollectAllGarbageEiNS0_23GarbageCollectionReasonENS_15GCCallbackFlagsE@PLT
	movq	400(%r12), %rax
	leaq	-37592(%rax), %rbx
	movq	12464(%rbx), %rax
	movq	$0, 12464(%rbx)
	cmpq	$0, 16(%r12)
	movq	%rax, -136(%rbp)
	je	.L3426
	movq	32(%r12), %rsi
	leaq	-128(%rbp), %r14
	movl	$1, %edx
	xorl	%r13d, %r13d
	movq	%r14, %rdi
	call	_ZN2v88internal26CombinedHeapObjectIteratorC1EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE@PLT
	.p2align 4,,10
	.p2align 3
.L3427:
	movq	%r14, %rdi
	addl	$1, %r13d
	call	_ZN2v88internal26CombinedHeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	jne	.L3427
	movq	%r14, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movl	%r13d, 396(%r12)
	movl	$0, 392(%r12)
.L3426:
	movq	8(%r12), %rdi
	call	_ZN2v88internal12HeapSnapshot23AddSyntheticRootEntriesEv
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14V8HeapExplorer27IterateAndExtractReferencesEPNS0_21HeapSnapshotGeneratorE
	testb	%al, %al
	jne	.L3428
.L3430:
	xorl	%r13d, %r13d
.L3429:
	movq	-136(%rbp), %rax
	movq	%rax, 12464(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3442
	addq	$104, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3428:
	.cfi_restore_state
	leaq	296(%r12), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal21NativeObjectsExplorer27IterateAndExtractReferencesEPNS0_21HeapSnapshotGeneratorE
	movl	%eax, %r13d
	testb	%al, %al
	je	.L3430
	movq	8(%r12), %rdi
	call	_ZN2v88internal12HeapSnapshot12FillChildrenEv
	movq	8(%r12), %rdx
	movq	(%rdx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
	subl	$2, %eax
	movl	%eax, 480(%rdx)
	movl	396(%r12), %esi
	leaq	_ZN2v88internal21HeapSnapshotGenerator14ProgressReportEb(%rip), %rdx
	movq	(%r12), %rax
	movl	%esi, 392(%r12)
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3431
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3429
	movq	(%rdi), %rax
	movl	%esi, %edx
	call	*16(%rax)
	testl	%eax, %eax
	sete	%r13b
	jmp	.L3429
	.p2align 4,,10
	.p2align 3
.L3431:
	movl	$1, %esi
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %r13d
	jmp	.L3429
.L3442:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20166:
	.size	_ZN2v88internal21HeapSnapshotGenerator16GenerateSnapshotEv, .-_ZN2v88internal21HeapSnapshotGenerator16GenerateSnapshotEv
	.section	.text._ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS0_23DefaultAllocationPolicyEE6ResizeES7_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS0_23DefaultAllocationPolicyEE6ResizeES7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS0_23DefaultAllocationPolicyEE6ResizeES7_
	.type	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS0_23DefaultAllocationPolicyEE6ResizeES7_, @function
_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS0_23DefaultAllocationPolicyEE6ResizeES7_:
.LFB25820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	%rax, -64(%rbp)
	movl	12(%rdi), %eax
	movl	%eax, -52(%rbp)
	movl	8(%rdi), %eax
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rdi
	movq	%rax, %rbx
	salq	$3, %rdi
	call	malloc@PLT
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L3478
	movl	%ebx, 8(%r14)
	testl	%ebx, %ebx
	je	.L3445
	movq	$0, (%rax)
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L3446:
	movq	(%r14), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	8(%r14), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L3446
.L3445:
	movl	-52(%rbp), %eax
	movq	-64(%rbp), %r13
	movl	$0, 12(%r14)
	testl	%eax, %eax
	je	.L3453
	.p2align 4,,10
	.p2align 3
.L3447:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3448
	movl	8(%r14), %ecx
	movl	16(%r13), %r12d
	movq	(%r14), %r9
	subl	$1, %ecx
	movl	%ecx, %r15d
	andl	%r12d, %r15d
	leaq	(%r15,%r15,2), %rbx
	salq	$3, %rbx
	leaq	(%r9,%rbx), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3449
.L3455:
	cmpl	16(%rax), %r12d
	je	.L3479
.L3450:
	addq	$1, %r15
	andl	%ecx, %r15d
	leaq	(%r15,%r15,2), %rbx
	salq	$3, %rbx
	leaq	(%r9,%rbx), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L3455
	movl	16(%r13), %r12d
.L3449:
	movq	8(%r13), %rcx
	movq	%rdi, (%rax)
	movl	%r12d, 16(%rax)
	movq	%rcx, 8(%rax)
	movl	12(%r14), %eax
	addl	$1, %eax
	movl	%eax, %ecx
	movl	%eax, 12(%r14)
	shrl	$2, %ecx
	addl	%ecx, %eax
	cmpl	8(%r14), %eax
	jnb	.L3452
.L3456:
	addq	$24, %r13
	subl	$1, -52(%rbp)
	jne	.L3447
.L3453:
	movq	-64(%rbp), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L3479:
	.cfi_restore_state
	call	*16(%r14)
	testb	%al, %al
	jne	.L3451
	movl	8(%r14), %ecx
	movq	(%r14), %r9
	movq	0(%r13), %rdi
	subl	$1, %ecx
	jmp	.L3450
	.p2align 4,,10
	.p2align 3
.L3448:
	addq	$24, %r13
	jmp	.L3447
	.p2align 4,,10
	.p2align 3
.L3451:
	movq	(%r14), %rax
	movl	16(%r13), %r12d
	movq	0(%r13), %rdi
	addq	%rbx, %rax
	jmp	.L3449
	.p2align 4,,10
	.p2align 3
.L3452:
	movq	%r14, %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS0_23DefaultAllocationPolicyEE6ResizeES7_
	movl	8(%r14), %ecx
	movq	(%r14), %rdi
	subl	$1, %ecx
	movl	%ecx, %ebx
	andl	%r12d, %ebx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rdi,%rax,8), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3456
	cmpl	%r12d, 16(%rax)
	je	.L3480
	.p2align 4,,10
	.p2align 3
.L3457:
	addq	$1, %rbx
	andl	%ecx, %ebx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rdi,%rax,8), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3456
	cmpl	%r12d, 16(%rax)
	jne	.L3457
.L3480:
	movq	0(%r13), %rdi
	call	*16(%r14)
	testb	%al, %al
	jne	.L3456
	movl	8(%r14), %ecx
	movq	(%r14), %rdi
	subl	$1, %ecx
	jmp	.L3457
.L3478:
	leaq	.LC35(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25820:
	.size	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS0_23DefaultAllocationPolicyEE6ResizeES7_, .-_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS0_23DefaultAllocationPolicyEE6ResizeES7_
	.section	.text._ZN2v88internal26HeapSnapshotJSONSerializer11GetStringIdEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26HeapSnapshotJSONSerializer11GetStringIdEPKc
	.type	_ZN2v88internal26HeapSnapshotJSONSerializer11GetStringIdEPKc, @function
_ZN2v88internal26HeapSnapshotJSONSerializer11GetStringIdEPKc:
.LFB20188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	strlen@PLT
	leal	-1(%rax), %edx
	cmpl	$9, %edx
	ja	.L3482
	movsbl	(%r14), %edi
	leal	-48(%rdi), %ecx
	cmpl	$9, %ecx
	jbe	.L3520
.L3483:
	movslq	%eax, %rbx
	addq	%r14, %rbx
	cmpq	%rbx, %r14
	je	.L3503
	movq	%r14, %rcx
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L3490:
	movsbw	(%rcx), %ax
	addq	$1, %rcx
	movzwl	%ax, %eax
	addl	%edx, %eax
	movl	%eax, %edx
	sall	$10, %edx
	addl	%eax, %edx
	movl	%edx, %eax
	shrl	$6, %eax
	xorl	%eax, %edx
	cmpq	%rcx, %rbx
	jne	.L3490
	leal	(%rdx,%rdx,8), %ebx
	movl	%ebx, %eax
	shrl	$11, %eax
	xorl	%ebx, %eax
	movl	%eax, %ebx
	sall	$15, %ebx
	addl	%ebx, %eax
	movl	%eax, %edx
	andl	$1073741823, %edx
	leal	-1(%rdx), %ebx
	sarl	$31, %ebx
	andl	$27, %ebx
	orl	%eax, %ebx
	leal	2(,%rbx,4), %ebx
.L3486:
	movl	16(%r12), %eax
	movq	8(%r12), %rcx
	subl	$1, %eax
	movl	%eax, %r15d
	andl	%ebx, %r15d
.L3518:
	leaq	(%r15,%r15,2), %r13
	salq	$3, %r13
	leaq	(%rcx,%r13), %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L3495
	cmpl	16(%rdx), %ebx
	je	.L3521
.L3492:
	addq	$1, %r15
	andl	%eax, %r15d
	jmp	.L3518
	.p2align 4,,10
	.p2align 3
.L3521:
	movq	%r14, %rdi
	call	*24(%r12)
	testb	%al, %al
	jne	.L3493
	movl	16(%r12), %eax
	movq	8(%r12), %rcx
	subl	$1, %eax
	jmp	.L3492
	.p2align 4,,10
	.p2align 3
.L3493:
	movq	8(%r12), %rdx
	addq	%r13, %rdx
	cmpq	$0, (%rdx)
	je	.L3495
.L3494:
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L3522
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3482:
	.cfi_restore_state
	leal	2(,%rax,4), %ebx
	cmpl	$16383, %eax
	jg	.L3486
	jmp	.L3483
	.p2align 4,,10
	.p2align 3
.L3520:
	cmpb	$48, %dil
	jne	.L3504
	cmpl	$1, %eax
	jne	.L3483
.L3504:
	subl	$48, %edi
	cmpl	$1, %eax
	je	.L3487
	leal	-2(%rax), %edx
	leaq	1(%r14), %rcx
	movl	$429496729, %r8d
	leaq	2(%r14,%rdx), %r9
.L3488:
	movsbl	(%rcx), %edx
	leal	-48(%rdx), %esi
	cmpl	$9, %esi
	ja	.L3483
	subl	$45, %edx
	movl	%r8d, %ebx
	sarl	$3, %edx
	subl	%edx, %ebx
	cmpl	%ebx, %edi
	ja	.L3483
	leal	(%rdi,%rdi,4), %edx
	addq	$1, %rcx
	leal	(%rsi,%rdx,2), %edi
	cmpq	%rcx, %r9
	jne	.L3488
.L3487:
	movl	%eax, %esi
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movl	%eax, %ebx
	jmp	.L3486
	.p2align 4,,10
	.p2align 3
.L3495:
	movq	%r14, (%rdx)
	movq	$0, 8(%rdx)
	movl	%ebx, 16(%rdx)
	movl	20(%r12), %eax
	addl	$1, %eax
	movl	%eax, %ecx
	movl	%eax, 20(%r12)
	shrl	$2, %ecx
	addl	%ecx, %eax
	cmpl	16(%r12), %eax
	jb	.L3494
	leaq	8(%r12), %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS0_23DefaultAllocationPolicyEE6ResizeES7_
	movl	16(%r12), %eax
	movq	8(%r12), %rcx
	subl	$1, %eax
	movl	%eax, %r15d
	andl	%ebx, %r15d
.L3519:
	leaq	(%r15,%r15,2), %r13
	salq	$3, %r13
	leaq	(%rcx,%r13), %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L3494
	cmpl	16(%rdx), %ebx
	je	.L3523
.L3498:
	addq	$1, %r15
	andl	%eax, %r15d
	jmp	.L3519
	.p2align 4,,10
	.p2align 3
.L3522:
	movl	36(%r12), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 36(%r12)
	movslq	%eax, %rcx
	movq	%rcx, 8(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3523:
	.cfi_restore_state
	movq	%r14, %rdi
	call	*24(%r12)
	testb	%al, %al
	jne	.L3499
	movl	16(%r12), %eax
	movq	8(%r12), %rcx
	subl	$1, %eax
	jmp	.L3498
	.p2align 4,,10
	.p2align 3
.L3499:
	movq	8(%r12), %rdx
	addq	%r13, %rdx
	jmp	.L3494
	.p2align 4,,10
	.p2align 3
.L3503:
	movl	$110, %ebx
	jmp	.L3486
	.cfi_endproc
.LFE20188:
	.size	_ZN2v88internal26HeapSnapshotJSONSerializer11GetStringIdEPKc, .-_ZN2v88internal26HeapSnapshotJSONSerializer11GetStringIdEPKc
	.section	.text._ZN2v88internal26HeapSnapshotJSONSerializer13SerializeEdgeEPNS0_13HeapGraphEdgeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeEdgeEPNS0_13HeapGraphEdgeEb
	.type	_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeEdgeEPNS0_13HeapGraphEdgeEb, @function
_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeEdgeEPNS0_13HeapGraphEdgeEb:
.LFB20191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-96(%rbp), %rcx
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	movq	%rcx, -112(%rbp)
	movq	$35, -104(%rbp)
	andl	$7, %eax
	cmpl	$1, %eax
	je	.L3539
	cmpl	$4, %eax
	jne	.L3525
.L3539:
	movl	16(%r12), %eax
.L3527:
	testb	%r13b, %r13b
	jne	.L3538
	movb	$44, (%rcx)
	movq	-112(%rbp), %rsi
	movl	$3, %edi
	leaq	1(%rsi), %rcx
	movl	$2, %esi
.L3528:
	movl	(%r12), %edx
	movl	$3435973837, %r8d
	andl	$7, %edx
	addl	$48, %edx
	movb	%dl, (%rcx)
	movq	-112(%rbp), %rdx
	movb	$44, (%rdx,%rsi)
	movl	%eax, %edx
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L3529:
	movl	%edx, %edx
	addl	$1, %esi
	movq	%rdx, %rcx
	imulq	%r8, %rdx
	shrq	$35, %rdx
	cmpl	$9, %ecx
	ja	.L3529
	addl	%esi, %edi
	movl	$3435973837, %r8d
	movslq	%edi, %r9
	leaq	-1(%r9), %rsi
	.p2align 4,,10
	.p2align 3
.L3530:
	movl	%eax, %edx
	movl	%eax, %r11d
	movq	-112(%rbp), %r10
	imulq	%r8, %rdx
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %r11d
	movl	%r11d, %ecx
	addl	$48, %ecx
	movb	%cl, (%r10,%rsi)
	movl	%eax, %ecx
	subq	$1, %rsi
	movl	%edx, %eax
	cmpl	$9, %ecx
	ja	.L3530
	movq	-112(%rbp), %rax
	addl	$1, %edi
	xorl	%esi, %esi
	movl	$3435973837, %r8d
	movb	$44, (%rax,%r9)
	movq	8(%r12), %rax
	movl	(%rax), %eax
	shrl	$4, %eax
	leal	(%rax,%rax,2), %edx
	addl	%edx, %edx
	movl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L3531:
	movl	%eax, %eax
	addl	$1, %esi
	movq	%rax, %rcx
	imulq	%r8, %rax
	shrq	$35, %rax
	cmpl	$9, %ecx
	ja	.L3531
	addl	%edi, %esi
	movl	$3435973837, %r8d
	movslq	%esi, %r9
	leaq	-1(%r9), %rdi
	.p2align 4,,10
	.p2align 3
.L3532:
	movl	%edx, %eax
	movl	%edx, %r15d
	movq	-112(%rbp), %r10
	imulq	%r8, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %r15d
	movl	%r15d, %ecx
	addl	$48, %ecx
	movb	%cl, (%r10,%rdi)
	movl	%edx, %ecx
	subq	$1, %rdi
	movl	%eax, %edx
	cmpl	$9, %ecx
	ja	.L3532
	movq	-112(%rbp), %rax
	addl	$1, %esi
	movslq	%esi, %rsi
	movb	$10, (%rax,%r9)
	movq	-112(%rbp), %rax
	movb	$0, (%rax,%rsi)
	movq	-112(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	testl	%eax, %eax
	jle	.L3524
	cltq
	leaq	(%r14,%rax), %r12
	cmpq	%r12, %r14
	jnb	.L3524
	movq	40(%rbx), %r13
	movl	32(%r13), %edx
	jmp	.L3534
	.p2align 4,,10
	.p2align 3
.L3535:
	cmpq	%r14, %r12
	jbe	.L3524
.L3534:
	movl	8(%r13), %ebx
	movq	%r12, %rax
	movslq	%edx, %rdi
	movq	%r14, %rsi
	subq	%r14, %rax
	subl	%edx, %ebx
	cmpl	%eax, %ebx
	cmovg	%eax, %ebx
	addq	16(%r13), %rdi
	movslq	%ebx, %r15
	movq	%r15, %rdx
	addq	%r15, %r14
	call	memcpy@PLT
	addl	32(%r13), %ebx
	movl	%ebx, 32(%r13)
	movl	%ebx, %edx
	cmpl	8(%r13), %ebx
	jne	.L3535
	cmpb	$0, 36(%r13)
	jne	.L3535
	movq	0(%r13), %rdi
	movq	16(%r13), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L3536
	movb	$1, 36(%r13)
.L3536:
	movl	$0, 32(%r13)
	xorl	%edx, %edx
	cmpq	%r14, %r12
	ja	.L3534
	.p2align 4,,10
	.p2align 3
.L3524:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3545
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3538:
	.cfi_restore_state
	movl	$2, %edi
	movl	$1, %esi
	jmp	.L3528
	.p2align 4,,10
	.p2align 3
.L3525:
	movq	16(%rsi), %rsi
	call	_ZN2v88internal26HeapSnapshotJSONSerializer11GetStringIdEPKc
	movq	-112(%rbp), %rcx
	jmp	.L3527
.L3545:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20191:
	.size	_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeEdgeEPNS0_13HeapGraphEdgeEb, .-_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeEdgeEPNS0_13HeapGraphEdgeEb
	.section	.text._ZN2v88internal26HeapSnapshotJSONSerializer14SerializeEdgesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26HeapSnapshotJSONSerializer14SerializeEdgesEv
	.type	_ZN2v88internal26HeapSnapshotJSONSerializer14SerializeEdgesEv, @function
_ZN2v88internal26HeapSnapshotJSONSerializer14SerializeEdgesEv:
.LFB20192:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r13
	movq	376(%r13), %rdx
	cmpq	384(%r13), %rdx
	je	.L3546
	movq	%rdi, %r12
	xorl	%ebx, %ebx
	jmp	.L3548
	.p2align 4,,10
	.p2align 3
.L3551:
	movq	376(%r13), %rdx
	movq	384(%r13), %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L3546
.L3548:
	xorl	%r8d, %r8d
	testq	%rbx, %rbx
	movq	(%rdx,%rbx,8), %rsi
	movq	%r12, %rdi
	sete	%r8b
	movl	%r8d, %edx
	call	_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeEdgeEPNS0_13HeapGraphEdgeEb
	movq	40(%r12), %rax
	cmpb	$0, 36(%rax)
	je	.L3551
.L3546:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20192:
	.size	_ZN2v88internal26HeapSnapshotJSONSerializer14SerializeEdgesEv, .-_ZN2v88internal26HeapSnapshotJSONSerializer14SerializeEdgesEv
	.section	.text._ZN2v88internal26HeapSnapshotJSONSerializer13SerializeNodeEPKNS0_9HeapEntryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeNodeEPKNS0_9HeapEntryE
	.type	_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeNodeEPKNS0_9HeapEntryE, @function
_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeNodeEPKNS0_9HeapEntryE:
.LFB20193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	-128(%rbp), %rax
	movq	$78, -136(%rbp)
	movq	%rax, -144(%rbp)
	movl	(%rsi), %eax
	shrl	$4, %eax
	je	.L3586
	movb	$44, -128(%rbp)
	movl	$1, %r8d
.L3553:
	movzbl	(%r12), %edi
	xorl	%ecx, %ecx
	movl	$3435973837, %esi
	andl	$15, %edi
	movl	%edi, %eax
	.p2align 4,,10
	.p2align 3
.L3554:
	movl	%eax, %eax
	addl	$1, %ecx
	movq	%rax, %rdx
	imulq	%rsi, %rax
	shrq	$35, %rax
	cmpl	$9, %edx
	ja	.L3554
	movl	%edi, %eax
	movl	%edi, %r9d
	addl	%r8d, %ecx
	imulq	%rax, %rsi
	leal	-1(%rcx), %edx
	movslq	%edx, %rdx
	shrq	$35, %rsi
	leal	0(,%rsi,4), %eax
	addl	%esi, %eax
	addl	%eax, %eax
	subl	%eax, %r9d
	movl	%r9d, %eax
	addl	$48, %eax
	movb	%al, -128(%rbp,%rdx)
	cmpl	$9, %edi
	jbe	.L3555
	movq	-144(%rbp), %rax
	addl	$48, %esi
	movb	%sil, -1(%rax,%rdx)
.L3555:
	movq	-144(%rbp), %rax
	leal	1(%rcx), %r13d
	movslq	%ecx, %rcx
	movq	%rbx, %rdi
	movb	$44, (%rax,%rcx)
	movq	24(%r12), %rsi
	call	_ZN2v88internal26HeapSnapshotJSONSerializer11GetStringIdEPKc
	xorl	%esi, %esi
	movl	$3435973837, %edi
	movl	%eax, %edx
	.p2align 4,,10
	.p2align 3
.L3556:
	movl	%edx, %edx
	addl	$1, %esi
	movq	%rdx, %rcx
	imulq	%rdi, %rdx
	shrq	$35, %rdx
	cmpl	$9, %ecx
	ja	.L3556
	addl	%esi, %r13d
	movl	$3435973837, %edi
	movslq	%r13d, %r8
	leaq	-1(%r8), %rsi
	.p2align 4,,10
	.p2align 3
.L3557:
	movl	%eax, %edx
	movl	%eax, %r11d
	movq	-144(%rbp), %r9
	imulq	%rdi, %rdx
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %r11d
	movl	%r11d, %ecx
	addl	$48, %ecx
	movb	%cl, (%r9,%rsi)
	movl	%eax, %ecx
	subq	$1, %rsi
	movl	%edx, %eax
	cmpl	$9, %ecx
	ja	.L3557
	movq	-144(%rbp), %rax
	addl	$1, %r13d
	xorl	%esi, %esi
	movl	$3435973837, %edi
	movb	$44, (%rax,%r8)
	movl	32(%r12), %edx
	movl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L3558:
	movl	%eax, %eax
	addl	$1, %esi
	movq	%rax, %rcx
	imulq	%rdi, %rax
	shrq	$35, %rax
	cmpl	$9, %ecx
	ja	.L3558
	addl	%r13d, %esi
	movl	$3435973837, %r8d
	movslq	%esi, %r9
	leaq	-1(%r9), %rdi
	.p2align 4,,10
	.p2align 3
.L3559:
	movl	%edx, %eax
	movl	%edx, %r11d
	movq	-144(%rbp), %r10
	imulq	%r8, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %r11d
	movl	%r11d, %ecx
	addl	$48, %ecx
	movb	%cl, (%r10,%rdi)
	movl	%edx, %ecx
	subq	$1, %rdi
	movl	%eax, %edx
	cmpl	$9, %ecx
	ja	.L3559
	movq	-144(%rbp), %rax
	leal	1(%rsi), %r13d
	xorl	%esi, %esi
	movabsq	$-3689348814741910323, %r8
	movb	$44, (%rax,%r9)
	movq	8(%r12), %rcx
	movq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L3560:
	movq	%rdx, %rax
	movq	%rdx, %rdi
	addl	$1, %esi
	mulq	%r8
	shrq	$3, %rdx
	cmpq	$9, %rdi
	ja	.L3560
	movabsq	$-3689348814741910323, %r8
	addl	%r13d, %esi
	movslq	%esi, %r9
	leaq	-1(%r9), %rdi
	.p2align 4,,10
	.p2align 3
.L3561:
	movq	%rcx, %rax
	movq	%rcx, %r11
	movq	-144(%rbp), %r10
	mulq	%r8
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %r11
	movq	%r11, %rax
	addl	$48, %eax
	movb	%al, (%r10,%rdi)
	movq	%rcx, %rax
	subq	$1, %rdi
	movq	%rdx, %rcx
	cmpq	$9, %rax
	ja	.L3561
	movq	-144(%rbp), %rax
	addl	$1, %esi
	movb	$44, (%rax,%r9)
	movl	(%r12), %eax
	testl	$-16, %eax
	jne	.L3562
	movq	16(%r12), %rax
	movq	376(%rax), %rdx
	movq	%rdx, %rax
.L3563:
	movl	4(%r12), %ecx
	xorl	%edi, %edi
	movl	$3435973837, %r8d
	leaq	(%rax,%rcx,8), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	movl	%eax, %edx
	.p2align 4,,10
	.p2align 3
.L3568:
	movl	%eax, %eax
	addl	$1, %edi
	movq	%rax, %rcx
	imulq	%r8, %rax
	shrq	$35, %rax
	cmpl	$9, %ecx
	ja	.L3568
	addl	%edi, %esi
	movl	$3435973837, %edi
	movslq	%esi, %r9
	leaq	-1(%r9), %r8
	.p2align 4,,10
	.p2align 3
.L3569:
	movl	%edx, %eax
	movl	%edx, %r10d
	imulq	%rdi, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %r10d
	movl	%r10d, %ecx
	movq	-144(%rbp), %r10
	addl	$48, %ecx
	movb	%cl, (%r10,%r8)
	movl	%edx, %ecx
	subq	$1, %r8
	movl	%eax, %edx
	cmpl	$9, %ecx
	ja	.L3569
	movq	-144(%rbp), %rax
	addl	$1, %esi
	xorl	%edi, %edi
	movl	$3435973837, %r8d
	movb	$44, (%rax,%r9)
	movl	36(%r12), %edx
	movl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L3570:
	movl	%eax, %eax
	addl	$1, %edi
	movq	%rax, %rcx
	imulq	%r8, %rax
	shrq	$35, %rax
	cmpl	$9, %ecx
	ja	.L3570
	addl	%esi, %edi
	movl	$3435973837, %r8d
	movslq	%edi, %r9
	leaq	-1(%r9), %rsi
	.p2align 4,,10
	.p2align 3
.L3571:
	movl	%edx, %eax
	movl	%edx, %r11d
	movq	-144(%rbp), %r10
	imulq	%r8, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %r11d
	movl	%r11d, %ecx
	addl	$48, %ecx
	movb	%cl, (%r10,%rsi)
	movl	%edx, %ecx
	subq	$1, %rsi
	movl	%eax, %edx
	cmpl	$9, %ecx
	ja	.L3571
	movq	-144(%rbp), %rax
	addl	$1, %edi
	movslq	%edi, %rdi
	movb	$10, (%rax,%r9)
	movq	-144(%rbp), %rax
	movb	$0, (%rax,%rdi)
	movq	-144(%rbp), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	movq	40(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3588
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3562:
	.cfi_restore_state
	movq	16(%r12), %rcx
	shrl	$4, %eax
	subl	$1, %eax
	movq	232(%rcx), %rdi
	cltq
	movq	%rdi, %rdx
	subq	240(%rcx), %rdx
	sarq	$3, %rdx
	imulq	%rdx, %r8
	addq	%rax, %r8
	js	.L3564
	cmpq	$11, %r8
	jle	.L3589
	movabsq	$3074457345618258603, %rdx
	movq	%r8, %rax
	imulq	%rdx
	movq	%r8, %rax
	sarq	$63, %rax
	sarq	%rdx
	subq	%rax, %rdx
.L3567:
	movq	256(%rcx), %r9
	leaq	(%rdx,%rdx,2), %rax
	salq	$2, %rax
	subq	%rax, %r8
	movq	(%r9,%rdx,8), %rax
	leaq	(%r8,%r8,4), %rdi
	leaq	(%rax,%rdi,8), %rax
.L3566:
	movq	16(%rax), %rdi
	movl	4(%rax), %edx
	movq	376(%rdi), %rax
	leaq	(%rax,%rdx,8), %rdx
	movq	376(%rcx), %rax
	jmp	.L3563
	.p2align 4,,10
	.p2align 3
.L3586:
	xorl	%r8d, %r8d
	jmp	.L3553
	.p2align 4,,10
	.p2align 3
.L3564:
	movabsq	$-6148914691236517205, %rdi
	movq	%r8, %rdx
	notq	%rdx
	movq	%rdx, %rax
	mulq	%rdi
	shrq	$3, %rdx
	notq	%rdx
	jmp	.L3567
	.p2align 4,,10
	.p2align 3
.L3589:
	leaq	(%rax,%rax,4), %rax
	leaq	(%rdi,%rax,8), %rax
	jmp	.L3566
.L3588:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20193:
	.size	_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeNodeEPKNS0_9HeapEntryE, .-_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeNodeEPKNS0_9HeapEntryE
	.section	.text._ZN2v88internal26HeapSnapshotJSONSerializer14SerializeNodesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26HeapSnapshotJSONSerializer14SerializeNodesEv
	.type	_ZN2v88internal26HeapSnapshotJSONSerializer14SerializeNodesEv, @function
_ZN2v88internal26HeapSnapshotJSONSerializer14SerializeNodesEv:
.LFB20194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	256(%rax), %r14
	movq	232(%rax), %rbx
	movq	248(%rax), %r12
	movq	264(%rax), %r13
	addq	$8, %r14
	.p2align 4,,10
	.p2align 3
.L3593:
	cmpq	%rbx, %r13
	je	.L3590
.L3597:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeNodeEPKNS0_9HeapEntryE
	movq	40(%r15), %rax
	cmpb	$0, 36(%rax)
	jne	.L3590
	addq	$40, %rbx
	cmpq	%rbx, %r12
	jne	.L3593
	movq	(%r14), %rbx
	addq	$8, %r14
	leaq	480(%rbx), %r12
	cmpq	%rbx, %r13
	jne	.L3597
.L3590:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20194:
	.size	_ZN2v88internal26HeapSnapshotJSONSerializer14SerializeNodesEv, .-_ZN2v88internal26HeapSnapshotJSONSerializer14SerializeNodesEv
	.section	.text._ZN2v88internal26HeapSnapshotJSONSerializer23SerializeTraceNodeInfosEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26HeapSnapshotJSONSerializer23SerializeTraceNodeInfosEv
	.type	_ZN2v88internal26HeapSnapshotJSONSerializer23SerializeTraceNodeInfosEv, @function
_ZN2v88internal26HeapSnapshotJSONSerializer23SerializeTraceNodeInfosEv:
.LFB20200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	%rdi, -168(%rbp)
	movq	(%rax), %rax
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L3598
	movq	328(%rax), %r12
	leaq	-128(%rbp), %r10
	movq	336(%rax), %rax
	movq	$68, -136(%rbp)
	movq	%r10, -144(%rbp)
	movq	%rax, -176(%rbp)
	cmpq	%rax, %r12
	je	.L3598
	leaq	8(%r12), %rax
	movq	(%r12), %r14
	xorl	%edi, %edi
	movl	$3435973837, %ebx
	movq	%rax, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L3625:
	movl	8(%r14), %r9d
	xorl	%esi, %esi
	movl	%r9d, %eax
	.p2align 4,,10
	.p2align 3
.L3602:
	movl	%eax, %eax
	addl	$1, %esi
	movq	%rax, %rdx
	imulq	%rbx, %rax
	shrq	$35, %rax
	cmpl	$9, %edx
	ja	.L3602
	addl	%esi, %edi
	movl	%r9d, %esi
	movl	%r9d, %ecx
	imulq	%rbx, %rsi
	leal	-1(%rdi), %r8d
	movslq	%r8d, %r8
	shrq	$35, %rsi
	leal	(%rsi,%rsi,4), %eax
	addl	%eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %eax
	addl	$48, %eax
	movb	%al, (%r10,%r8)
	cmpl	$9, %r9d
	jbe	.L3603
	subq	$1, %r8
	.p2align 4,,10
	.p2align 3
.L3604:
	movl	%esi, %eax
	movl	%esi, %ecx
	movq	-144(%rbp), %r9
	imulq	%rbx, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %edx
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, %edx
	addl	$48, %edx
	movb	%dl, (%r9,%r8)
	movl	%esi, %edx
	subq	$1, %r8
	movl	%eax, %esi
	cmpl	$9, %edx
	ja	.L3604
.L3603:
	movq	-144(%rbp), %rax
	leal	1(%rdi), %r15d
	movslq	%edi, %rdi
	movb	$44, (%rax,%rdi)
	movq	-168(%rbp), %rdi
	movq	(%r14), %rsi
	call	_ZN2v88internal26HeapSnapshotJSONSerializer11GetStringIdEPKc
	xorl	%edi, %edi
	movl	%eax, %edx
	.p2align 4,,10
	.p2align 3
.L3605:
	movl	%edx, %edx
	addl	$1, %edi
	movq	%rdx, %rsi
	imulq	%rbx, %rdx
	shrq	$35, %rdx
	cmpl	$9, %esi
	ja	.L3605
	addl	%edi, %r15d
	movslq	%r15d, %r8
	leaq	-1(%r8), %rdi
	.p2align 4,,10
	.p2align 3
.L3606:
	movl	%eax, %edx
	movl	%eax, %ecx
	movq	-144(%rbp), %r9
	imulq	%rbx, %rdx
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %esi
	addl	%esi, %esi
	subl	%esi, %ecx
	movl	%ecx, %esi
	addl	$48, %esi
	movb	%sil, (%r9,%rdi)
	movl	%eax, %esi
	subq	$1, %rdi
	movl	%edx, %eax
	cmpl	$9, %esi
	ja	.L3606
	movq	-144(%rbp), %rax
	movq	-168(%rbp), %rdi
	addl	$1, %r15d
	movb	$44, (%rax,%r8)
	movq	16(%r14), %rsi
	call	_ZN2v88internal26HeapSnapshotJSONSerializer11GetStringIdEPKc
	xorl	%edx, %edx
	movl	%eax, %esi
	.p2align 4,,10
	.p2align 3
.L3607:
	movl	%esi, %esi
	addl	$1, %edx
	movq	%rsi, %rdi
	imulq	%rbx, %rsi
	shrq	$35, %rsi
	cmpl	$9, %edi
	ja	.L3607
	addl	%edx, %r15d
	movslq	%r15d, %r8
	leaq	-1(%r8), %rdi
	.p2align 4,,10
	.p2align 3
.L3608:
	movl	%eax, %edx
	movl	%eax, %ecx
	movq	-144(%rbp), %r9
	imulq	%rbx, %rdx
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %esi
	addl	%esi, %esi
	subl	%esi, %ecx
	movl	%ecx, %esi
	addl	$48, %esi
	movb	%sil, (%r9,%rdi)
	movl	%eax, %esi
	subq	$1, %rdi
	movl	%edx, %eax
	cmpl	$9, %esi
	ja	.L3608
	movq	-144(%rbp), %rax
	addl	$1, %r15d
	movb	$44, (%rax,%r8)
	movl	24(%r14), %esi
	xorl	%r8d, %r8d
	movl	%esi, %eax
	.p2align 4,,10
	.p2align 3
.L3609:
	movl	%eax, %eax
	addl	$1, %r8d
	movq	%rax, %rdi
	imulq	%rbx, %rax
	shrq	$35, %rax
	cmpl	$9, %edi
	ja	.L3609
	leal	(%r15,%r8), %edx
	movslq	%edx, %r9
	leaq	-1(%r9), %r8
	.p2align 4,,10
	.p2align 3
.L3610:
	movl	%esi, %eax
	movl	%esi, %ecx
	movq	-144(%rbp), %r10
	imulq	%rbx, %rax
	shrq	$35, %rax
	leal	(%rax,%rax,4), %edi
	addl	%edi, %edi
	subl	%edi, %ecx
	movl	%ecx, %edi
	addl	$48, %edi
	movb	%dil, (%r10,%r8)
	movl	%esi, %edi
	subq	$1, %r8
	movl	%eax, %esi
	cmpl	$9, %edi
	ja	.L3610
	movq	-144(%rbp), %rax
	leal	1(%rdx), %r8d
	movb	$44, (%rax,%r9)
	movl	28(%r14), %esi
	cmpl	$-1, %esi
	je	.L3640
	addl	$1, %esi
	xorl	%edx, %edx
	movl	%esi, %eax
	.p2align 4,,10
	.p2align 3
.L3613:
	movl	%eax, %eax
	addl	$1, %edx
	movq	%rax, %rdi
	imulq	%rbx, %rax
	shrq	$35, %rax
	cmpl	$9, %edi
	ja	.L3613
	addl	%r8d, %edx
	movslq	%edx, %r8
	leaq	-1(%r8), %rdi
	.p2align 4,,10
	.p2align 3
.L3614:
	movl	%esi, %eax
	movl	%esi, %ecx
	movq	-144(%rbp), %r10
	imulq	%rbx, %rax
	shrq	$35, %rax
	movq	%rax, %r9
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %eax
	addl	$48, %eax
	movb	%al, (%r10,%rdi)
	movl	%esi, %eax
	subq	$1, %rdi
	movl	%r9d, %esi
	cmpl	$9, %eax
	ja	.L3614
.L3612:
	movq	-144(%rbp), %rax
	leal	1(%rdx), %edi
	movb	$44, (%rax,%r8)
	movl	32(%r14), %ecx
	cmpl	$-1, %ecx
	je	.L3641
	addl	$1, %ecx
	xorl	%edx, %edx
	movl	%ecx, %eax
	.p2align 4,,10
	.p2align 3
.L3617:
	movl	%eax, %eax
	addl	$1, %edx
	movq	%rax, %rsi
	imulq	%rbx, %rax
	shrq	$35, %rax
	cmpl	$9, %esi
	ja	.L3617
	addl	%edi, %edx
	movslq	%edx, %rdi
	leaq	-1(%rdi), %rsi
	.p2align 4,,10
	.p2align 3
.L3618:
	movl	%ecx, %eax
	movl	%ecx, %r11d
	movq	-144(%rbp), %r9
	imulq	%rbx, %rax
	shrq	$35, %rax
	movq	%rax, %r8
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	subl	%eax, %r11d
	movl	%r11d, %eax
	addl	$48, %eax
	movb	%al, (%r9,%rsi)
	movl	%ecx, %eax
	subq	$1, %rsi
	movl	%r8d, %ecx
	cmpl	$9, %eax
	ja	.L3618
.L3616:
	movq	-144(%rbp), %rax
	addl	$1, %edx
	movslq	%edx, %rdx
	movb	$10, (%rax,%rdi)
	movq	-144(%rbp), %rax
	movb	$0, (%rax,%rdx)
	movq	-144(%rbp), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	testl	%eax, %eax
	jle	.L3619
	cltq
	addq	%r12, %rax
	movq	%rax, -152(%rbp)
	cmpq	%rax, %r12
	jnb	.L3619
	movq	-168(%rbp), %rax
	movq	40(%rax), %r14
	movl	32(%r14), %edx
	jmp	.L3620
	.p2align 4,,10
	.p2align 3
.L3621:
	cmpq	%r12, -152(%rbp)
	jbe	.L3619
.L3620:
	movl	8(%r14), %eax
	movq	-152(%rbp), %r9
	movslq	%edx, %rdi
	movq	%r12, %rsi
	subl	%edx, %eax
	subq	%r12, %r9
	cmpl	%r9d, %eax
	movl	%r9d, %r13d
	cmovle	%eax, %r13d
	addq	16(%r14), %rdi
	movslq	%r13d, %r15
	movq	%r15, %rdx
	addq	%r15, %r12
	call	memcpy@PLT
	movl	32(%r14), %edx
	addl	%r13d, %edx
	movl	%edx, 32(%r14)
	cmpl	8(%r14), %edx
	jne	.L3621
	cmpb	$0, 36(%r14)
	jne	.L3621
	movq	(%r14), %rdi
	movq	16(%r14), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	jne	.L3622
	movb	$1, 36(%r14)
.L3622:
	movl	$0, 32(%r14)
	xorl	%edx, %edx
	cmpq	%r12, -152(%rbp)
	ja	.L3620
	.p2align 4,,10
	.p2align 3
.L3619:
	movq	-160(%rbp), %rcx
	cmpq	%rcx, -176(%rbp)
	je	.L3598
	movq	-144(%rbp), %rax
	movq	(%rcx), %r14
	addq	$8, %rcx
	movl	$1, %edi
	movq	%rcx, -160(%rbp)
	movb	$44, (%rax)
	movq	-144(%rbp), %r10
	jmp	.L3625
	.p2align 4,,10
	.p2align 3
.L3641:
	movq	-144(%rbp), %rax
	movslq	%edi, %rdi
	addl	$2, %edx
	movb	$48, (%rax,%rdi)
	movslq	%edx, %rdi
	jmp	.L3616
	.p2align 4,,10
	.p2align 3
.L3640:
	movq	-144(%rbp), %rax
	movslq	%r8d, %r8
	addl	$2, %edx
	movb	$48, (%rax,%r8)
	movslq	%edx, %r8
	jmp	.L3612
	.p2align 4,,10
	.p2align 3
.L3598:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3642
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3642:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20200:
	.size	_ZN2v88internal26HeapSnapshotJSONSerializer23SerializeTraceNodeInfosEv, .-_ZN2v88internal26HeapSnapshotJSONSerializer23SerializeTraceNodeInfosEv
	.section	.rodata._ZN2v88internal26HeapSnapshotJSONSerializer13SerializeImplEv.str1.1,"aMS",@progbits,1
.LC234:
	.string	"\"snapshot\":{"
.LC235:
	.string	"},\n"
.LC236:
	.string	"\"nodes\":["
.LC237:
	.string	"],\n"
.LC238:
	.string	"\"edges\":["
.LC239:
	.string	"\"trace_function_infos\":["
.LC240:
	.string	"\"trace_tree\":["
.LC241:
	.string	"\"samples\":["
.LC242:
	.string	"\"locations\":["
.LC243:
	.string	"\"strings\":["
	.section	.text._ZN2v88internal26HeapSnapshotJSONSerializer13SerializeImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeImplEv
	.type	_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeImplEv, @function
_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeImplEv:
.LFB20187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %r12
	movslq	32(%r12), %rax
	leal	1(%rax), %edx
	movl	%edx, 32(%r12)
	movq	16(%r12), %rdx
	movb	$123, (%rdx,%rax)
	movl	32(%r12), %edx
	cmpl	8(%r12), %edx
	je	.L3668
.L3644:
	movq	40(%rbx), %rdi
	movl	$12, %edx
	leaq	.LC234(%rip), %rsi
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	movq	%rbx, %rdi
	call	_ZN2v88internal26HeapSnapshotJSONSerializer17SerializeSnapshotEv
	movq	40(%rbx), %rdi
	cmpb	$0, 36(%rdi)
	je	.L3669
.L3643:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3669:
	.cfi_restore_state
	movl	$3, %edx
	leaq	.LC235(%rip), %rsi
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	movq	40(%rbx), %rdi
	movl	$9, %edx
	leaq	.LC236(%rip), %rsi
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	movq	(%rbx), %rax
	movq	232(%rax), %r12
	movq	248(%rax), %r15
	movq	256(%rax), %r13
	movq	264(%rax), %r14
	.p2align 4,,10
	.p2align 3
.L3650:
	cmpq	%r12, %r14
	je	.L3648
.L3670:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeNodeEPKNS0_9HeapEntryE
	movq	40(%rbx), %rax
	cmpb	$0, 36(%rax)
	jne	.L3643
	addq	$40, %r12
	cmpq	%r12, %r15
	jne	.L3650
	movq	8(%r13), %r12
	addq	$8, %r13
	leaq	480(%r12), %r15
	cmpq	%r12, %r14
	jne	.L3670
.L3648:
	movq	40(%rbx), %rdi
	cmpb	$0, 36(%rdi)
	jne	.L3643
	movl	$3, %edx
	leaq	.LC237(%rip), %rsi
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	movq	40(%rbx), %rdi
	movl	$9, %edx
	leaq	.LC238(%rip), %rsi
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	movq	(%rbx), %r13
	movq	376(%r13), %rdx
	cmpq	384(%r13), %rdx
	je	.L3653
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L3654:
	xorl	%r8d, %r8d
	testq	%r12, %r12
	movq	(%rdx,%r12,8), %rsi
	movq	%rbx, %rdi
	sete	%r8b
	movl	%r8d, %edx
	call	_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeEdgeEPNS0_13HeapGraphEdgeEb
	movq	40(%rbx), %rdi
	cmpb	$0, 36(%rdi)
	jne	.L3643
	movq	376(%r13), %rdx
	movq	384(%r13), %rax
	addq	$1, %r12
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r12, %rax
	ja	.L3654
.L3655:
	movl	$3, %edx
	leaq	.LC237(%rip), %rsi
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	movq	40(%rbx), %rdi
	movl	$24, %edx
	leaq	.LC239(%rip), %rsi
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	movq	%rbx, %rdi
	call	_ZN2v88internal26HeapSnapshotJSONSerializer23SerializeTraceNodeInfosEv
	movq	40(%rbx), %rdi
	cmpb	$0, 36(%rdi)
	jne	.L3643
	movl	$3, %edx
	leaq	.LC237(%rip), %rsi
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	movq	40(%rbx), %rdi
	movl	$14, %edx
	leaq	.LC240(%rip), %rsi
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	48(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3656
	addq	$24, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeTraceNodeEPNS0_19AllocationTraceNodeE
.L3656:
	movq	40(%rbx), %rdi
	cmpb	$0, 36(%rdi)
	jne	.L3643
	movl	$3, %edx
	leaq	.LC237(%rip), %rsi
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	movq	40(%rbx), %rdi
	movl	$11, %edx
	leaq	.LC241(%rip), %rsi
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	movq	%rbx, %rdi
	call	_ZN2v88internal26HeapSnapshotJSONSerializer16SerializeSamplesEv
	movq	40(%rbx), %rdi
	cmpb	$0, 36(%rdi)
	jne	.L3643
	movl	$3, %edx
	leaq	.LC237(%rip), %rsi
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	movq	40(%rbx), %rdi
	movl	$13, %edx
	leaq	.LC242(%rip), %rsi
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	movq	%rbx, %rdi
	call	_ZN2v88internal26HeapSnapshotJSONSerializer18SerializeLocationsEv
	movq	40(%rbx), %rdi
	cmpb	$0, 36(%rdi)
	jne	.L3643
	movl	$3, %edx
	leaq	.LC237(%rip), %rsi
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	movq	40(%rbx), %rdi
	movl	$11, %edx
	leaq	.LC243(%rip), %rsi
	call	_ZN2v88internal18OutputStreamWriter12AddSubstringEPKci
	movq	%rbx, %rdi
	call	_ZN2v88internal26HeapSnapshotJSONSerializer16SerializeStringsEv
	movq	40(%rbx), %rdi
	cmpb	$0, 36(%rdi)
	jne	.L3643
	movl	$93, %esi
	call	_ZN2v88internal18OutputStreamWriter12AddCharacterEc
	movq	40(%rbx), %rdi
	movl	$125, %esi
	call	_ZN2v88internal18OutputStreamWriter12AddCharacterEc
	movq	40(%rbx), %rbx
	cmpb	$0, 36(%rbx)
	jne	.L3643
	movq	(%rbx), %rdi
	movl	32(%rbx), %edx
	movq	(%rdi), %rax
	testl	%edx, %edx
	jne	.L3671
.L3657:
	movq	16(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L3668:
	.cfi_restore_state
	cmpb	$0, 36(%r12)
	jne	.L3644
	movq	(%r12), %rdi
	movq	16(%r12), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpl	$1, %eax
	je	.L3672
.L3645:
	movl	$0, 32(%r12)
	jmp	.L3644
	.p2align 4,,10
	.p2align 3
.L3672:
	movb	$1, 36(%r12)
	jmp	.L3645
	.p2align 4,,10
	.p2align 3
.L3653:
	movq	40(%rbx), %rdi
	cmpb	$0, 36(%rdi)
	je	.L3655
	jmp	.L3643
.L3671:
	movq	16(%rbx), %rsi
	call	*32(%rax)
	subl	$1, %eax
	jne	.L3658
	movb	$1, 36(%rbx)
.L3658:
	movq	(%rbx), %rdi
	movl	$0, 32(%rbx)
	movq	(%rdi), %rax
	jmp	.L3657
	.cfi_endproc
.LFE20187:
	.size	_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeImplEv, .-_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeImplEv
	.section	.text._ZN2v88internal26HeapSnapshotJSONSerializer9SerializeEPNS_12OutputStreamE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26HeapSnapshotJSONSerializer9SerializeEPNS_12OutputStreamE
	.type	_ZN2v88internal26HeapSnapshotJSONSerializer9SerializeEPNS_12OutputStreamE, @function
_ZN2v88internal26HeapSnapshotJSONSerializer9SerializeEPNS_12OutputStreamE:
.LFB20183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3674
	call	_ZN2v88internal17AllocationTracker23PrepareForSerializationEv@PLT
.L3674:
	movl	$40, %edi
	movl	$1024, %r14d
	call	_Znwm@PLT
	leaq	_ZN2v812OutputStream12GetChunkSizeEv(%rip), %rcx
	movq	%r13, (%rax)
	movq	%rax, %rbx
	movq	0(%r13), %rax
	movq	24(%rax), %rdx
	movl	$1024, %eax
	cmpq	%rcx, %rdx
	jne	.L3690
.L3675:
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r14, %rdi
	movl	%eax, 8(%rbx)
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L3691
.L3676:
	movq	%rbx, 40(%r12)
	movq	%r12, %rdi
	movq	%rax, 16(%rbx)
	movq	%r14, 24(%rbx)
	movl	$0, 32(%rbx)
	movb	$0, 36(%rbx)
	call	_ZN2v88internal26HeapSnapshotJSONSerializer13SerializeImplEv
	movq	40(%r12), %r13
	testq	%r13, %r13
	je	.L3677
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3678
	call	_ZdaPv@PLT
.L3678:
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3677:
	movq	$0, 40(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3690:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rdx
	movslq	%eax, %r14
	jmp	.L3675
.L3691:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r14, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L3676
	leaq	.LC118(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE20183:
	.size	_ZN2v88internal26HeapSnapshotJSONSerializer9SerializeEPNS_12OutputStreamE, .-_ZN2v88internal26HeapSnapshotJSONSerializer9SerializeEPNS_12OutputStreamE
	.section	.text._ZNSt10_HashtableIjSt4pairIKjPN2v88internal9HeapEntryEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE13_M_rehash_auxEmSt17integral_constantIbLb1EE,"axG",@progbits,_ZNSt10_HashtableIjSt4pairIKjPN2v88internal9HeapEntryEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE13_M_rehash_auxEmSt17integral_constantIbLb1EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIjSt4pairIKjPN2v88internal9HeapEntryEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE13_M_rehash_auxEmSt17integral_constantIbLb1EE
	.type	_ZNSt10_HashtableIjSt4pairIKjPN2v88internal9HeapEntryEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE13_M_rehash_auxEmSt17integral_constantIbLb1EE, @function
_ZNSt10_HashtableIjSt4pairIKjPN2v88internal9HeapEntryEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE13_M_rehash_auxEmSt17integral_constantIbLb1EE:
.LFB25993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	cmpq	$1, %rsi
	je	.L3709
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rsi
	ja	.L3710
	leaq	0(,%rsi,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	leaq	48(%r12), %r10
.L3694:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L3696
	xorl	%r8d, %r8d
	leaq	16(%r12), %r9
	jmp	.L3697
	.p2align 4,,10
	.p2align 3
.L3698:
	movq	(%rdi), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L3699:
	testq	%rsi, %rsi
	je	.L3696
.L3697:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movl	8(%rcx), %eax
	divq	%rbx
	leaq	0(%r13,%rdx,8), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L3698
	movq	16(%r12), %rdi
	movq	%rdi, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L3701
	movq	%rcx, 0(%r13,%r8,8)
	movq	%rdx, %r8
	testq	%rsi, %rsi
	jne	.L3697
	.p2align 4,,10
	.p2align 3
.L3696:
	movq	(%r12), %rdi
	cmpq	%r10, %rdi
	je	.L3700
	call	_ZdlPv@PLT
.L3700:
	movq	%rbx, 8(%r12)
	popq	%rbx
	movq	%r13, (%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3701:
	.cfi_restore_state
	movq	%rdx, %r8
	jmp	.L3699
	.p2align 4,,10
	.p2align 3
.L3709:
	leaq	48(%rdi), %r13
	movq	$0, 48(%rdi)
	movq	%r13, %r10
	jmp	.L3694
.L3710:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE25993:
	.size	_ZNSt10_HashtableIjSt4pairIKjPN2v88internal9HeapEntryEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE13_M_rehash_auxEmSt17integral_constantIbLb1EE, .-_ZNSt10_HashtableIjSt4pairIKjPN2v88internal9HeapEntryEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE13_M_rehash_auxEmSt17integral_constantIbLb1EE
	.section	.rodata._ZN2v88internal12HeapSnapshot12GetEntryByIdEj.str1.1,"aMS",@progbits,1
.LC244:
	.string	"is_complete()"
	.section	.text._ZN2v88internal12HeapSnapshot12GetEntryByIdEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapSnapshot12GetEntryByIdEj
	.type	_ZN2v88internal12HeapSnapshot12GetEntryByIdEj, @function
_ZN2v88internal12HeapSnapshot12GetEntryByIdEj:
.LFB19837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$0, 424(%rdi)
	movl	%esi, -92(%rbp)
	jne	.L3712
	movq	384(%rdi), %rax
	cmpq	%rax, 376(%rdi)
	je	.L3762
	movq	264(%rdi), %rcx
	subq	272(%rdi), %rcx
	leaq	400(%rdi), %rax
	movabsq	$-3689348814741910323, %rsi
	movq	248(%rdi), %rdx
	sarq	$3, %rcx
	subq	232(%rdi), %rdx
	movq	%rax, -104(%rbp)
	imulq	%rsi, %rcx
	movq	288(%rdi), %rax
	sarq	$3, %rdx
	subq	256(%rdi), %rax
	sarq	$3, %rax
	imulq	%rsi, %rdx
	movss	432(%rdi), %xmm4
	leaq	-3(%rax,%rax,2), %rax
	leaq	(%rcx,%rax,4), %rax
	addq	%rdx, %rax
	js	.L3714
	pxor	%xmm1, %xmm1
	cvtsi2ssq	%rax, %xmm1
.L3715:
	divss	%xmm4, %xmm1
	movss	.LC246(%rip), %xmm3
	movss	.LC245(%rip), %xmm5
	movaps	%xmm1, %xmm2
	movaps	%xmm1, %xmm0
	andps	%xmm3, %xmm2
	ucomiss	%xmm2, %xmm5
	jbe	.L3716
	cvttss2sil	%xmm1, %eax
	pxor	%xmm2, %xmm2
	movss	.LC30(%rip), %xmm5
	andnps	%xmm1, %xmm3
	cvtsi2ssl	%eax, %xmm2
	cmpnless	%xmm2, %xmm0
	andps	%xmm5, %xmm0
	addss	%xmm2, %xmm0
	orps	%xmm3, %xmm0
.L3716:
	movss	%xmm4, -56(%rbp)
	fld1
	flds	-56(%rbp)
	movsd	.LC249(%rip), %xmm3
	movsd	.LC248(%rip), %xmm4
	movq	440(%r15), %rbx
	fdivrp	%st, %st(1)
	fstpl	-56(%rbp)
	movsd	-56(%rbp), %xmm1
	movapd	%xmm1, %xmm2
	andpd	%xmm3, %xmm2
	ucomisd	%xmm2, %xmm4
	jbe	.L3717
	cvttsd2siq	%xmm1, %rax
	pxor	%xmm2, %xmm2
	movapd	%xmm1, %xmm6
	movsd	.LC250(%rip), %xmm4
	andnpd	%xmm6, %xmm3
	cvtsi2sdq	%rax, %xmm2
	cmpnlesd	%xmm2, %xmm1
	andpd	%xmm4, %xmm1
	addsd	%xmm2, %xmm1
	orpd	%xmm3, %xmm1
.L3717:
	comiss	.LC251(%rip), %xmm0
	jnb	.L3718
	cvttss2siq	%xmm0, %rsi
	movsd	.LC252(%rip), %xmm0
	comisd	%xmm0, %xmm1
	jnb	.L3720
.L3767:
	cvttsd2siq	%xmm1, %rax
.L3721:
	cmpq	%rax, %rsi
	cmovb	%rax, %rsi
	leaq	432(%r15), %rax
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	movq	%rax, %rsi
	cmpq	408(%r15), %rax
	je	.L3722
	leaq	400(%r15), %rdi
	call	_ZNSt10_HashtableIjSt4pairIKjPN2v88internal9HeapEntryEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE13_M_rehash_auxEmSt17integral_constantIbLb1EE
.L3723:
	movq	264(%r15), %rax
	movq	232(%r15), %rbx
	movq	248(%r15), %r13
	movq	%rax, -64(%rbp)
	movq	256(%r15), %rax
	addq	$8, %rax
	movq	%rax, -72(%rbp)
	leaq	416(%r15), %rax
	movq	%rax, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L3733:
	movq	%r13, -56(%rbp)
.L3732:
	cmpq	%rbx, -64(%rbp)
	je	.L3712
	movl	32(%rbx), %r13d
	movl	$24, %edi
	call	_Znwm@PLT
	movq	408(%r15), %rsi
	xorl	%edx, %edx
	movl	%r13d, %r8d
	movq	$0, (%rax)
	movq	%rax, %r12
	movl	%r13d, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%r8, %rax
	divq	%rsi
	movq	400(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r11
	leaq	0(,%rdx,8), %r14
	testq	%rax, %rax
	je	.L3725
	movq	(%rax), %rcx
	movl	8(%rcx), %edi
	jmp	.L3727
	.p2align 4,,10
	.p2align 3
.L3763:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L3725
	movl	8(%rcx), %eax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rsi
	cmpq	%rdx, %r11
	jne	.L3725
.L3727:
	cmpl	%edi, %r13d
	jne	.L3763
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3737:
	addq	$40, %rbx
	cmpq	%rbx, -56(%rbp)
	jne	.L3732
	movq	-72(%rbp), %rax
	movq	(%rax), %rbx
	addq	$8, %rax
	movq	%rax, -72(%rbp)
	leaq	480(%rbx), %r13
	jmp	.L3733
	.p2align 4,,10
	.p2align 3
.L3712:
	movq	408(%r15), %rsi
	movl	-92(%rbp), %eax
	xorl	%edx, %edx
	divq	%rsi
	movq	400(%r15), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %rdi
	testq	%r8, %r8
	je	.L3711
	movq	(%r8), %r8
	movl	-92(%rbp), %r9d
	movl	8(%r8), %ecx
	jmp	.L3736
	.p2align 4,,10
	.p2align 3
.L3765:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L3711
	movl	8(%r8), %eax
	xorl	%edx, %edx
	movq	%rax, %rcx
	divq	%rsi
	cmpq	%rdi, %rdx
	jne	.L3764
.L3736:
	cmpl	%r9d, %ecx
	jne	.L3765
	movq	16(%r8), %r8
.L3711:
	addq	$72, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3714:
	.cfi_restore_state
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm1, %xmm1
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2ssq	%rdx, %xmm1
	addss	%xmm1, %xmm1
	jmp	.L3715
	.p2align 4,,10
	.p2align 3
.L3725:
	movq	424(%r15), %rdx
	movq	-88(%rbp), %rdi
	movl	$1, %ecx
	movq	%r8, -80(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L3766
.L3728:
	movq	400(%r15), %rcx
	leaq	(%rcx,%r14), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L3729
	movq	(%rdx), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rax
	movq	%r12, (%rax)
.L3730:
	addq	$1, 424(%r15)
	jmp	.L3737
	.p2align 4,,10
	.p2align 3
.L3766:
	movq	-104(%rbp), %rdi
	movq	%rdx, %rsi
	call	_ZNSt10_HashtableIjSt4pairIKjPN2v88internal9HeapEntryEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE13_M_rehash_auxEmSt17integral_constantIbLb1EE
	movq	-80(%rbp), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	408(%r15)
	leaq	0(,%rdx,8), %r14
	jmp	.L3728
	.p2align 4,,10
	.p2align 3
.L3729:
	movq	416(%r15), %rdx
	movq	%r12, 416(%r15)
	movq	%rdx, (%r12)
	testq	%rdx, %rdx
	je	.L3731
	movl	8(%rdx), %eax
	xorl	%edx, %edx
	divq	408(%r15)
	movq	%r12, (%rcx,%rdx,8)
	movq	400(%r15), %rax
	addq	%r14, %rax
.L3731:
	movq	-112(%rbp), %rsi
	movq	%rsi, (%rax)
	jmp	.L3730
	.p2align 4,,10
	.p2align 3
.L3722:
	movq	%rbx, 440(%r15)
	jmp	.L3723
	.p2align 4,,10
	.p2align 3
.L3718:
	subss	.LC251(%rip), %xmm0
	cvttss2siq	%xmm0, %rsi
	movsd	.LC252(%rip), %xmm0
	btcq	$63, %rsi
	comisd	%xmm0, %xmm1
	jb	.L3767
.L3720:
	subsd	%xmm0, %xmm1
	cvttsd2siq	%xmm1, %rax
	btcq	$63, %rax
	jmp	.L3721
	.p2align 4,,10
	.p2align 3
.L3764:
	xorl	%r8d, %r8d
	jmp	.L3711
.L3762:
	leaq	.LC244(%rip), %rsi
	leaq	.LC105(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19837:
	.size	_ZN2v88internal12HeapSnapshot12GetEntryByIdEj, .-_ZN2v88internal12HeapSnapshot12GetEntryByIdEj
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEPKcPNS0_9HeapEntryES6_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEPKcPNS0_9HeapEntryES6_, @function
_GLOBAL__sub_I__ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEPKcPNS0_9HeapEntryES6_:
.LFB26582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26582:
	.size	_GLOBAL__sub_I__ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEPKcPNS0_9HeapEntryES6_, .-_GLOBAL__sub_I__ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEPKcPNS0_9HeapEntryES6_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal13HeapGraphEdgeC2ENS1_4TypeEPKcPNS0_9HeapEntryES6_
	.weak	_ZTVN2v88internal14V8HeapExplorerE
	.section	.data.rel.ro.local._ZTVN2v88internal14V8HeapExplorerE,"awG",@progbits,_ZTVN2v88internal14V8HeapExplorerE,comdat
	.align 8
	.type	_ZTVN2v88internal14V8HeapExplorerE, @object
	.size	_ZTVN2v88internal14V8HeapExplorerE, 40
_ZTVN2v88internal14V8HeapExplorerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14V8HeapExplorerD1Ev
	.quad	_ZN2v88internal14V8HeapExplorerD0Ev
	.quad	_ZN2v88internal14V8HeapExplorer13AllocateEntryEPv
	.weak	_ZTVN2v88internal26IndexedReferencesExtractorE
	.section	.data.rel.ro._ZTVN2v88internal26IndexedReferencesExtractorE,"awG",@progbits,_ZTVN2v88internal26IndexedReferencesExtractorE,comdat
	.align 8
	.type	_ZTVN2v88internal26IndexedReferencesExtractorE, @object
	.size	_ZTVN2v88internal26IndexedReferencesExtractorE, 152
_ZTVN2v88internal26IndexedReferencesExtractorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal26IndexedReferencesExtractorD1Ev
	.quad	_ZN2v88internal26IndexedReferencesExtractorD0Ev
	.quad	_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal26IndexedReferencesExtractor13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	.quad	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal26IndexedReferencesExtractor15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal26IndexedReferencesExtractor20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.quad	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE
	.weak	_ZTVN2v88internal24RootsReferencesExtractorE
	.section	.data.rel.ro.local._ZTVN2v88internal24RootsReferencesExtractorE,"awG",@progbits,_ZTVN2v88internal24RootsReferencesExtractorE,comdat
	.align 8
	.type	_ZTVN2v88internal24RootsReferencesExtractorE, @object
	.size	_ZTVN2v88internal24RootsReferencesExtractorE, 56
_ZTVN2v88internal24RootsReferencesExtractorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal24RootsReferencesExtractorD1Ev
	.quad	_ZN2v88internal24RootsReferencesExtractorD0Ev
	.quad	_ZN2v88internal24RootsReferencesExtractor17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.quad	_ZN2v88internal24RootsReferencesExtractor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.quad	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.weak	_ZTVN2v88internal23GlobalObjectsEnumeratorE
	.section	.data.rel.ro.local._ZTVN2v88internal23GlobalObjectsEnumeratorE,"awG",@progbits,_ZTVN2v88internal23GlobalObjectsEnumeratorE,comdat
	.align 8
	.type	_ZTVN2v88internal23GlobalObjectsEnumeratorE, @object
	.size	_ZTVN2v88internal23GlobalObjectsEnumeratorE, 56
_ZTVN2v88internal23GlobalObjectsEnumeratorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23GlobalObjectsEnumeratorD1Ev
	.quad	_ZN2v88internal23GlobalObjectsEnumeratorD0Ev
	.quad	_ZN2v88internal23GlobalObjectsEnumerator17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.quad	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.quad	_ZN2v88internal11RootVisitor11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.weak	_ZTVN2v88internal17EmbedderGraphImpl10V8NodeImplE
	.section	.data.rel.ro.local._ZTVN2v88internal17EmbedderGraphImpl10V8NodeImplE,"awG",@progbits,_ZTVN2v88internal17EmbedderGraphImpl10V8NodeImplE,comdat
	.align 8
	.type	_ZTVN2v88internal17EmbedderGraphImpl10V8NodeImplE, @object
	.size	_ZTVN2v88internal17EmbedderGraphImpl10V8NodeImplE, 80
_ZTVN2v88internal17EmbedderGraphImpl10V8NodeImplE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal17EmbedderGraphImpl10V8NodeImplD1Ev
	.quad	_ZN2v88internal17EmbedderGraphImpl10V8NodeImplD0Ev
	.quad	_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl4NameEv
	.quad	_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN2v813EmbedderGraph4Node10IsRootNodeEv
	.quad	_ZN2v88internal17EmbedderGraphImpl10V8NodeImpl14IsEmbedderNodeEv
	.quad	_ZN2v813EmbedderGraph4Node10NamePrefixEv
	.weak	_ZTVN2v88internal17EmbedderGraphImplE
	.section	.data.rel.ro.local._ZTVN2v88internal17EmbedderGraphImplE,"awG",@progbits,_ZTVN2v88internal17EmbedderGraphImplE,comdat
	.align 8
	.type	_ZTVN2v88internal17EmbedderGraphImplE, @object
	.size	_ZTVN2v88internal17EmbedderGraphImplE, 56
_ZTVN2v88internal17EmbedderGraphImplE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal17EmbedderGraphImpl6V8NodeERKNS_5LocalINS_5ValueEEE
	.quad	_ZN2v88internal17EmbedderGraphImpl7AddNodeESt10unique_ptrINS_13EmbedderGraph4NodeESt14default_deleteIS4_EE
	.quad	_ZN2v88internal17EmbedderGraphImpl7AddEdgeEPNS_13EmbedderGraph4NodeES4_PKc
	.quad	_ZN2v88internal17EmbedderGraphImplD1Ev
	.quad	_ZN2v88internal17EmbedderGraphImplD0Ev
	.weak	_ZTVN2v88internal29EmbedderGraphEntriesAllocatorE
	.section	.data.rel.ro.local._ZTVN2v88internal29EmbedderGraphEntriesAllocatorE,"awG",@progbits,_ZTVN2v88internal29EmbedderGraphEntriesAllocatorE,comdat
	.align 8
	.type	_ZTVN2v88internal29EmbedderGraphEntriesAllocatorE, @object
	.size	_ZTVN2v88internal29EmbedderGraphEntriesAllocatorE, 40
_ZTVN2v88internal29EmbedderGraphEntriesAllocatorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal29EmbedderGraphEntriesAllocatorD1Ev
	.quad	_ZN2v88internal29EmbedderGraphEntriesAllocatorD0Ev
	.quad	_ZN2v88internal29EmbedderGraphEntriesAllocator13AllocateEntryEPv
	.weak	_ZTVN2v88internal21HeapSnapshotGeneratorE
	.section	.data.rel.ro.local._ZTVN2v88internal21HeapSnapshotGeneratorE,"awG",@progbits,_ZTVN2v88internal21HeapSnapshotGeneratorE,comdat
	.align 8
	.type	_ZTVN2v88internal21HeapSnapshotGeneratorE, @object
	.size	_ZTVN2v88internal21HeapSnapshotGeneratorE, 48
_ZTVN2v88internal21HeapSnapshotGeneratorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal21HeapSnapshotGeneratorD1Ev
	.quad	_ZN2v88internal21HeapSnapshotGeneratorD0Ev
	.quad	_ZN2v88internal21HeapSnapshotGenerator12ProgressStepEv
	.quad	_ZN2v88internal21HeapSnapshotGenerator14ProgressReportEb
	.section	.rodata._ZZN2v88internalL10WriteUCharEPNS0_18OutputStreamWriterEjE9hex_chars,"a"
	.align 16
	.type	_ZZN2v88internalL10WriteUCharEPNS0_18OutputStreamWriterEjE9hex_chars, @object
	.size	_ZZN2v88internalL10WriteUCharEPNS0_18OutputStreamWriterEjE9hex_chars, 17
_ZZN2v88internalL10WriteUCharEPNS0_18OutputStreamWriterEjE9hex_chars:
	.string	"0123456789ABCDEF"
	.globl	_ZN2v88internal26HeapSnapshotJSONSerializer16kNodeFieldsCountE
	.section	.rodata._ZN2v88internal26HeapSnapshotJSONSerializer16kNodeFieldsCountE,"a"
	.align 4
	.type	_ZN2v88internal26HeapSnapshotJSONSerializer16kNodeFieldsCountE, @object
	.size	_ZN2v88internal26HeapSnapshotJSONSerializer16kNodeFieldsCountE, 4
_ZN2v88internal26HeapSnapshotJSONSerializer16kNodeFieldsCountE:
	.long	6
	.globl	_ZN2v88internal26HeapSnapshotJSONSerializer16kEdgeFieldsCountE
	.section	.rodata._ZN2v88internal26HeapSnapshotJSONSerializer16kEdgeFieldsCountE,"a"
	.align 4
	.type	_ZN2v88internal26HeapSnapshotJSONSerializer16kEdgeFieldsCountE, @object
	.size	_ZN2v88internal26HeapSnapshotJSONSerializer16kEdgeFieldsCountE, 4
_ZN2v88internal26HeapSnapshotJSONSerializer16kEdgeFieldsCountE:
	.long	3
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC254:
	.string	"global_proxy_object"
.LC255:
	.string	"embedder_data"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC256:
	.string	"accessor_property_descriptor_map"
	.section	.rodata.str1.1
.LC257:
	.string	"allow_code_gen_from_strings"
.LC258:
	.string	"array_buffer_fun"
.LC259:
	.string	"array_buffer_map"
.LC260:
	.string	"array_buffer_noinit_fun"
.LC261:
	.string	"array_function"
.LC262:
	.string	"array_join_stack"
.LC263:
	.string	"async_from_sync_iterator_map"
	.section	.rodata.str1.8
	.align 8
.LC264:
	.string	"async_function_await_reject_shared_fun"
	.align 8
.LC265:
	.string	"async_function_await_resolve_shared_fun"
	.section	.rodata.str1.1
.LC266:
	.string	"async_function_constructor"
.LC267:
	.string	"async_function_object_map"
	.section	.rodata.str1.8
	.align 8
.LC268:
	.string	"async_generator_function_function"
	.align 8
.LC269:
	.string	"async_iterator_value_unwrap_shared_fun"
	.align 8
.LC270:
	.string	"async_generator_await_reject_shared_fun"
	.align 8
.LC271:
	.string	"async_generator_await_resolve_shared_fun"
	.align 8
.LC272:
	.string	"async_generator_yield_resolve_shared_fun"
	.align 8
.LC273:
	.string	"async_generator_return_resolve_shared_fun"
	.align 8
.LC274:
	.string	"async_generator_return_closed_resolve_shared_fun"
	.align 8
.LC275:
	.string	"async_generator_return_closed_reject_shared_fun"
	.section	.rodata.str1.1
.LC276:
	.string	"atomics_object"
.LC277:
	.string	"bigint_function"
.LC278:
	.string	"bigint64_array_fun"
.LC279:
	.string	"biguint64_array_fun"
.LC280:
	.string	"boolean_function"
	.section	.rodata.str1.8
	.align 8
.LC281:
	.string	"bound_function_with_constructor_map"
	.align 8
.LC282:
	.string	"bound_function_without_constructor_map"
	.section	.rodata.str1.1
.LC283:
	.string	"call_as_constructor_delegate"
.LC284:
	.string	"call_as_function_delegate"
.LC285:
	.string	"callsite_function"
.LC286:
	.string	"context_extension_function"
.LC287:
	.string	"data_property_descriptor_map"
.LC288:
	.string	"data_view_fun"
.LC289:
	.string	"date_function"
.LC290:
	.string	"debug_context_id"
.LC291:
	.string	"empty_function"
	.section	.rodata.str1.8
	.align 8
.LC292:
	.string	"error_message_for_code_gen_from_strings"
	.section	.rodata.str1.1
.LC293:
	.string	"errors_thrown"
.LC294:
	.string	"extras_binding_object"
.LC295:
	.string	"extras_utils_object"
.LC296:
	.string	"fast_aliased_arguments_map"
	.section	.rodata.str1.8
	.align 8
.LC297:
	.string	"fast_template_instantiations_cache"
	.section	.rodata.str1.1
.LC298:
	.string	"float32_array_fun"
.LC299:
	.string	"float64_array_fun"
.LC300:
	.string	"function_function"
.LC301:
	.string	"generator_function_function"
	.section	.rodata.str1.8
	.align 8
.LC302:
	.string	"generator_object_prototype_map"
	.align 8
.LC303:
	.string	"async_generator_object_prototype_map"
	.section	.rodata.str1.1
.LC304:
	.string	"initial_array_iterator_map"
	.section	.rodata.str1.8
	.align 8
.LC305:
	.string	"initial_array_iterator_prototype"
	.section	.rodata.str1.1
.LC306:
	.string	"initial_array_prototype"
.LC307:
	.string	"initial_error_prototype"
.LC308:
	.string	"initial_generator_prototype"
	.section	.rodata.str1.8
	.align 8
.LC309:
	.string	"initial_async_generator_prototype"
	.section	.rodata.str1.1
.LC310:
	.string	"initial_iterator_prototype"
	.section	.rodata.str1.8
	.align 8
.LC311:
	.string	"initial_map_iterator_prototype"
	.section	.rodata.str1.1
.LC312:
	.string	"initial_map_prototype_map"
.LC313:
	.string	"initial_object_prototype"
	.section	.rodata.str1.8
	.align 8
.LC314:
	.string	"initial_set_iterator_prototype"
	.section	.rodata.str1.1
.LC315:
	.string	"initial_set_prototype"
.LC316:
	.string	"initial_set_prototype_map"
.LC317:
	.string	"initial_string_iterator_map"
	.section	.rodata.str1.8
	.align 8
.LC318:
	.string	"initial_string_iterator_prototype"
	.section	.rodata.str1.1
.LC319:
	.string	"initial_string_prototype"
.LC320:
	.string	"initial_weakmap_prototype_map"
.LC321:
	.string	"initial_weakset_prototype_map"
.LC322:
	.string	"int16_array_fun"
.LC323:
	.string	"int32_array_fun"
.LC324:
	.string	"int8_array_fun"
.LC325:
	.string	"intl_collator_function"
	.section	.rodata.str1.8
	.align 8
.LC326:
	.string	"intl_date_time_format_function"
	.section	.rodata.str1.1
.LC327:
	.string	"intl_number_format_function"
.LC328:
	.string	"intl_locale_function"
.LC329:
	.string	"intl_segment_iterator_map"
.LC330:
	.string	"iterator_result_map"
	.section	.rodata.str1.8
	.align 8
.LC331:
	.string	"js_array_packed_smi_elements_map"
	.align 8
.LC332:
	.string	"js_array_holey_smi_elements_map"
	.section	.rodata.str1.1
.LC333:
	.string	"js_array_packed_elements_map"
.LC334:
	.string	"js_array_holey_elements_map"
	.section	.rodata.str1.8
	.align 8
.LC335:
	.string	"js_array_packed_double_elements_map"
	.align 8
.LC336:
	.string	"js_array_holey_double_elements_map"
	.section	.rodata.str1.1
.LC337:
	.string	"js_map_fun"
.LC338:
	.string	"js_map_map"
.LC339:
	.string	"js_module_namespace_map"
.LC340:
	.string	"js_set_fun"
.LC341:
	.string	"js_set_map"
.LC342:
	.string	"weak_cell_map"
	.section	.rodata.str1.8
	.align 8
.LC343:
	.string	"js_finalization_group_cleanup_iterator_map"
	.section	.rodata.str1.1
.LC344:
	.string	"js_weak_map_fun"
.LC345:
	.string	"js_weak_set_fun"
.LC346:
	.string	"js_weak_ref_fun"
.LC347:
	.string	"js_finalization_group_fun"
.LC348:
	.string	"map_cache"
.LC349:
	.string	"map_key_iterator_map"
.LC350:
	.string	"map_key_value_iterator_map"
.LC351:
	.string	"map_value_iterator_map"
.LC352:
	.string	"math_random_index"
.LC353:
	.string	"math_random_state"
.LC354:
	.string	"math_random_cache"
.LC355:
	.string	"message_listeners"
.LC356:
	.string	"normalized_map_cache"
.LC357:
	.string	"number_function"
.LC358:
	.string	"object_function"
.LC359:
	.string	"object_function_prototype_map"
.LC360:
	.string	"opaque_reference_function"
.LC361:
	.string	"proxy_callable_map"
.LC362:
	.string	"proxy_constructor_map"
.LC363:
	.string	"proxy_function"
.LC364:
	.string	"proxy_map"
.LC365:
	.string	"proxy_revocable_result_map"
.LC366:
	.string	"proxy_revoke_shared_fun"
	.section	.rodata.str1.8
	.align 8
.LC367:
	.string	"promise_get_capabilities_executor_shared_fun"
	.align 8
.LC368:
	.string	"promise_capability_default_reject_shared_fun"
	.align 8
.LC369:
	.string	"promise_capability_default_resolve_shared_fun"
	.align 8
.LC370:
	.string	"promise_then_finally_shared_fun"
	.align 8
.LC371:
	.string	"promise_catch_finally_shared_fun"
	.align 8
.LC372:
	.string	"promise_value_thunk_finally_shared_fun"
	.align 8
.LC373:
	.string	"promise_thrower_finally_shared_fun"
	.align 8
.LC374:
	.string	"promise_all_resolve_element_shared_fun"
	.align 8
.LC375:
	.string	"promise_all_settled_resolve_element_shared_fun"
	.align 8
.LC376:
	.string	"promise_all_settled_reject_element_shared_fun"
	.section	.rodata.str1.1
.LC377:
	.string	"promise_prototype"
.LC378:
	.string	"regexp_exec_function"
.LC379:
	.string	"regexp_function"
.LC380:
	.string	"regexp_last_match_info"
.LC381:
	.string	"regexp_match_all_function"
.LC382:
	.string	"regexp_match_function"
.LC383:
	.string	"regexp_prototype"
.LC384:
	.string	"regexp_prototype_map"
.LC385:
	.string	"regexp_replace_function"
.LC386:
	.string	"regexp_result_map"
.LC387:
	.string	"regexp_search_function"
.LC388:
	.string	"regexp_split_function"
	.section	.rodata.str1.8
	.align 8
.LC389:
	.string	"initial_regexp_string_iterator_prototype_map"
	.section	.rodata.str1.1
.LC390:
	.string	"script_context_table"
.LC391:
	.string	"script_execution_callback"
.LC392:
	.string	"security_token"
.LC393:
	.string	"serialized_objects"
.LC394:
	.string	"set_value_iterator_map"
.LC395:
	.string	"set_key_value_iterator_map"
.LC396:
	.string	"shared_array_buffer_fun"
.LC397:
	.string	"sloppy_arguments_map"
.LC398:
	.string	"slow_aliased_arguments_map"
.LC399:
	.string	"strict_arguments_map"
	.section	.rodata.str1.8
	.align 8
.LC400:
	.string	"slow_object_with_null_prototype_map"
	.align 8
.LC401:
	.string	"slow_object_with_object_prototype_map"
	.align 8
.LC402:
	.string	"slow_template_instantiations_cache"
	.section	.rodata.str1.1
.LC403:
	.string	"regexp_species_protector"
.LC404:
	.string	"sloppy_function_map"
.LC405:
	.string	"sloppy_function_with_name_map"
	.section	.rodata.str1.8
	.align 8
.LC406:
	.string	"sloppy_function_without_prototype_map"
	.align 8
.LC407:
	.string	"sloppy_function_with_readonly_prototype_map"
	.section	.rodata.str1.1
.LC408:
	.string	"strict_function_map"
.LC409:
	.string	"strict_function_with_name_map"
	.section	.rodata.str1.8
	.align 8
.LC410:
	.string	"strict_function_with_readonly_prototype_map"
	.align 8
.LC411:
	.string	"strict_function_without_prototype_map"
	.section	.rodata.str1.1
.LC412:
	.string	"method_with_name_map"
.LC413:
	.string	"method_with_home_object_map"
	.section	.rodata.str1.8
	.align 8
.LC414:
	.string	"method_with_name_and_home_object_map"
	.section	.rodata.str1.1
.LC415:
	.string	"async_function_map"
.LC416:
	.string	"async_function_with_name_map"
	.section	.rodata.str1.8
	.align 8
.LC417:
	.string	"async_function_with_home_object_map"
	.align 8
.LC418:
	.string	"async_function_with_name_and_home_object_map"
	.section	.rodata.str1.1
.LC419:
	.string	"generator_function_map"
	.section	.rodata.str1.8
	.align 8
.LC420:
	.string	"generator_function_with_name_map"
	.align 8
.LC421:
	.string	"generator_function_with_home_object_map"
	.align 8
.LC422:
	.string	"generator_function_with_name_and_home_object_map"
	.section	.rodata.str1.1
.LC423:
	.string	"async_generator_function_map"
	.section	.rodata.str1.8
	.align 8
.LC424:
	.string	"async_generator_function_with_name_map"
	.align 8
.LC425:
	.string	"async_generator_function_with_home_object_map"
	.align 8
.LC426:
	.string	"async_generator_function_with_name_and_home_object_map"
	.section	.rodata.str1.1
.LC427:
	.string	"class_function_map"
.LC428:
	.string	"string_function"
.LC429:
	.string	"string_function_prototype_map"
.LC430:
	.string	"symbol_function"
.LC431:
	.string	"wasm_exported_function_map"
.LC432:
	.string	"wasm_exception_constructor"
.LC433:
	.string	"wasm_global_constructor"
.LC434:
	.string	"wasm_instance_constructor"
.LC435:
	.string	"wasm_memory_constructor"
.LC436:
	.string	"wasm_module_constructor"
.LC437:
	.string	"wasm_table_constructor"
.LC438:
	.string	"template_weakmap"
.LC439:
	.string	"typed_array_function"
.LC440:
	.string	"typed_array_prototype"
.LC441:
	.string	"uint16_array_fun"
.LC442:
	.string	"uint32_array_fun"
.LC443:
	.string	"uint8_array_fun"
.LC444:
	.string	"uint8_clamped_array_fun"
.LC445:
	.string	"array_entries_iterator"
.LC446:
	.string	"array_for_each_iterator"
.LC447:
	.string	"array_keys_iterator"
.LC448:
	.string	"array_values_iterator"
.LC449:
	.string	"error_function"
.LC450:
	.string	"error_to_string"
.LC451:
	.string	"eval_error_function"
.LC452:
	.string	"global_eval_fun"
.LC453:
	.string	"global_proxy_function"
.LC454:
	.string	"map_delete"
.LC455:
	.string	"map_get"
.LC456:
	.string	"map_has"
.LC457:
	.string	"map_set"
.LC458:
	.string	"function_has_instance"
.LC459:
	.string	"object_to_string"
.LC460:
	.string	"promise_all"
.LC461:
	.string	"promise_catch"
.LC462:
	.string	"promise_function"
.LC463:
	.string	"range_error_function"
.LC464:
	.string	"reference_error_function"
.LC465:
	.string	"set_add"
.LC466:
	.string	"set_delete"
.LC467:
	.string	"set_has"
.LC468:
	.string	"syntax_error_function"
.LC469:
	.string	"type_error_function"
.LC470:
	.string	"uri_error_function"
.LC471:
	.string	"wasm_compile_error_function"
.LC472:
	.string	"wasm_link_error_function"
.LC473:
	.string	"wasm_runtime_error_function"
.LC474:
	.string	"weakmap_set"
.LC475:
	.string	"weakmap_get"
.LC476:
	.string	"weakset_add"
.LC477:
	.string	"generator_next_internal"
.LC478:
	.string	"make_error"
.LC479:
	.string	"make_range_error"
.LC480:
	.string	"make_syntax_error"
.LC481:
	.string	"make_type_error"
.LC482:
	.string	"make_uri_error"
.LC483:
	.string	"object_create"
.LC484:
	.string	"reflect_apply"
.LC485:
	.string	"reflect_construct"
.LC486:
	.string	"math_floor"
.LC487:
	.string	"math_pow"
.LC488:
	.string	"promise_internal_constructor"
.LC489:
	.string	"is_promise"
.LC490:
	.string	"promise_then"
	.section	.data.rel.ro.local._ZN2v88internalL20native_context_namesE,"aw"
	.align 32
	.type	_ZN2v88internalL20native_context_namesE, @object
	.size	_ZN2v88internalL20native_context_namesE, 3792
_ZN2v88internalL20native_context_namesE:
	.long	4
	.zero	4
	.quad	.LC254
	.long	5
	.zero	4
	.quad	.LC255
	.long	6
	.zero	4
	.quad	.LC256
	.long	7
	.zero	4
	.quad	.LC257
	.long	8
	.zero	4
	.quad	.LC258
	.long	9
	.zero	4
	.quad	.LC259
	.long	10
	.zero	4
	.quad	.LC260
	.long	11
	.zero	4
	.quad	.LC261
	.long	12
	.zero	4
	.quad	.LC262
	.long	13
	.zero	4
	.quad	.LC263
	.long	14
	.zero	4
	.quad	.LC264
	.long	15
	.zero	4
	.quad	.LC265
	.long	16
	.zero	4
	.quad	.LC266
	.long	17
	.zero	4
	.quad	.LC267
	.long	18
	.zero	4
	.quad	.LC268
	.long	19
	.zero	4
	.quad	.LC269
	.long	20
	.zero	4
	.quad	.LC270
	.long	21
	.zero	4
	.quad	.LC271
	.long	22
	.zero	4
	.quad	.LC272
	.long	23
	.zero	4
	.quad	.LC273
	.long	24
	.zero	4
	.quad	.LC274
	.long	25
	.zero	4
	.quad	.LC275
	.long	26
	.zero	4
	.quad	.LC276
	.long	27
	.zero	4
	.quad	.LC277
	.long	28
	.zero	4
	.quad	.LC278
	.long	29
	.zero	4
	.quad	.LC279
	.long	30
	.zero	4
	.quad	.LC280
	.long	31
	.zero	4
	.quad	.LC281
	.long	32
	.zero	4
	.quad	.LC282
	.long	33
	.zero	4
	.quad	.LC283
	.long	34
	.zero	4
	.quad	.LC284
	.long	35
	.zero	4
	.quad	.LC285
	.long	36
	.zero	4
	.quad	.LC286
	.long	37
	.zero	4
	.quad	.LC287
	.long	38
	.zero	4
	.quad	.LC288
	.long	39
	.zero	4
	.quad	.LC289
	.long	40
	.zero	4
	.quad	.LC290
	.long	41
	.zero	4
	.quad	.LC291
	.long	42
	.zero	4
	.quad	.LC292
	.long	43
	.zero	4
	.quad	.LC293
	.long	44
	.zero	4
	.quad	.LC294
	.long	45
	.zero	4
	.quad	.LC295
	.long	46
	.zero	4
	.quad	.LC296
	.long	47
	.zero	4
	.quad	.LC297
	.long	48
	.zero	4
	.quad	.LC298
	.long	49
	.zero	4
	.quad	.LC299
	.long	50
	.zero	4
	.quad	.LC300
	.long	51
	.zero	4
	.quad	.LC301
	.long	52
	.zero	4
	.quad	.LC302
	.long	53
	.zero	4
	.quad	.LC303
	.long	54
	.zero	4
	.quad	.LC304
	.long	55
	.zero	4
	.quad	.LC305
	.long	56
	.zero	4
	.quad	.LC306
	.long	57
	.zero	4
	.quad	.LC307
	.long	58
	.zero	4
	.quad	.LC308
	.long	59
	.zero	4
	.quad	.LC309
	.long	60
	.zero	4
	.quad	.LC310
	.long	61
	.zero	4
	.quad	.LC311
	.long	62
	.zero	4
	.quad	.LC312
	.long	63
	.zero	4
	.quad	.LC313
	.long	64
	.zero	4
	.quad	.LC314
	.long	65
	.zero	4
	.quad	.LC315
	.long	66
	.zero	4
	.quad	.LC316
	.long	67
	.zero	4
	.quad	.LC317
	.long	68
	.zero	4
	.quad	.LC318
	.long	69
	.zero	4
	.quad	.LC319
	.long	70
	.zero	4
	.quad	.LC320
	.long	71
	.zero	4
	.quad	.LC321
	.long	72
	.zero	4
	.quad	.LC322
	.long	73
	.zero	4
	.quad	.LC323
	.long	74
	.zero	4
	.quad	.LC324
	.long	75
	.zero	4
	.quad	.LC325
	.long	76
	.zero	4
	.quad	.LC326
	.long	77
	.zero	4
	.quad	.LC327
	.long	78
	.zero	4
	.quad	.LC328
	.long	79
	.zero	4
	.quad	.LC329
	.long	80
	.zero	4
	.quad	.LC330
	.long	81
	.zero	4
	.quad	.LC331
	.long	82
	.zero	4
	.quad	.LC332
	.long	83
	.zero	4
	.quad	.LC333
	.long	84
	.zero	4
	.quad	.LC334
	.long	85
	.zero	4
	.quad	.LC335
	.long	86
	.zero	4
	.quad	.LC336
	.long	87
	.zero	4
	.quad	.LC337
	.long	88
	.zero	4
	.quad	.LC338
	.long	89
	.zero	4
	.quad	.LC339
	.long	90
	.zero	4
	.quad	.LC340
	.long	91
	.zero	4
	.quad	.LC341
	.long	92
	.zero	4
	.quad	.LC342
	.long	93
	.zero	4
	.quad	.LC343
	.long	94
	.zero	4
	.quad	.LC344
	.long	95
	.zero	4
	.quad	.LC345
	.long	96
	.zero	4
	.quad	.LC346
	.long	97
	.zero	4
	.quad	.LC347
	.long	98
	.zero	4
	.quad	.LC348
	.long	99
	.zero	4
	.quad	.LC349
	.long	100
	.zero	4
	.quad	.LC350
	.long	101
	.zero	4
	.quad	.LC351
	.long	102
	.zero	4
	.quad	.LC352
	.long	103
	.zero	4
	.quad	.LC353
	.long	104
	.zero	4
	.quad	.LC354
	.long	105
	.zero	4
	.quad	.LC355
	.long	106
	.zero	4
	.quad	.LC356
	.long	107
	.zero	4
	.quad	.LC357
	.long	108
	.zero	4
	.quad	.LC358
	.long	109
	.zero	4
	.quad	.LC359
	.long	110
	.zero	4
	.quad	.LC360
	.long	111
	.zero	4
	.quad	.LC361
	.long	112
	.zero	4
	.quad	.LC362
	.long	113
	.zero	4
	.quad	.LC363
	.long	114
	.zero	4
	.quad	.LC364
	.long	115
	.zero	4
	.quad	.LC365
	.long	116
	.zero	4
	.quad	.LC366
	.long	117
	.zero	4
	.quad	.LC367
	.long	118
	.zero	4
	.quad	.LC368
	.long	119
	.zero	4
	.quad	.LC369
	.long	120
	.zero	4
	.quad	.LC370
	.long	121
	.zero	4
	.quad	.LC371
	.long	122
	.zero	4
	.quad	.LC372
	.long	123
	.zero	4
	.quad	.LC373
	.long	124
	.zero	4
	.quad	.LC374
	.long	125
	.zero	4
	.quad	.LC375
	.long	126
	.zero	4
	.quad	.LC376
	.long	127
	.zero	4
	.quad	.LC377
	.long	128
	.zero	4
	.quad	.LC378
	.long	129
	.zero	4
	.quad	.LC379
	.long	130
	.zero	4
	.quad	.LC380
	.long	131
	.zero	4
	.quad	.LC381
	.long	132
	.zero	4
	.quad	.LC382
	.long	133
	.zero	4
	.quad	.LC383
	.long	134
	.zero	4
	.quad	.LC384
	.long	135
	.zero	4
	.quad	.LC385
	.long	136
	.zero	4
	.quad	.LC386
	.long	137
	.zero	4
	.quad	.LC387
	.long	138
	.zero	4
	.quad	.LC388
	.long	139
	.zero	4
	.quad	.LC389
	.long	140
	.zero	4
	.quad	.LC390
	.long	141
	.zero	4
	.quad	.LC391
	.long	142
	.zero	4
	.quad	.LC392
	.long	143
	.zero	4
	.quad	.LC393
	.long	144
	.zero	4
	.quad	.LC394
	.long	145
	.zero	4
	.quad	.LC395
	.long	146
	.zero	4
	.quad	.LC396
	.long	147
	.zero	4
	.quad	.LC397
	.long	148
	.zero	4
	.quad	.LC398
	.long	149
	.zero	4
	.quad	.LC399
	.long	150
	.zero	4
	.quad	.LC400
	.long	151
	.zero	4
	.quad	.LC401
	.long	152
	.zero	4
	.quad	.LC402
	.long	153
	.zero	4
	.quad	.LC403
	.long	154
	.zero	4
	.quad	.LC404
	.long	155
	.zero	4
	.quad	.LC405
	.long	156
	.zero	4
	.quad	.LC406
	.long	157
	.zero	4
	.quad	.LC407
	.long	158
	.zero	4
	.quad	.LC408
	.long	159
	.zero	4
	.quad	.LC409
	.long	160
	.zero	4
	.quad	.LC410
	.long	161
	.zero	4
	.quad	.LC411
	.long	162
	.zero	4
	.quad	.LC412
	.long	163
	.zero	4
	.quad	.LC413
	.long	164
	.zero	4
	.quad	.LC414
	.long	165
	.zero	4
	.quad	.LC415
	.long	166
	.zero	4
	.quad	.LC416
	.long	167
	.zero	4
	.quad	.LC417
	.long	168
	.zero	4
	.quad	.LC418
	.long	169
	.zero	4
	.quad	.LC419
	.long	170
	.zero	4
	.quad	.LC420
	.long	171
	.zero	4
	.quad	.LC421
	.long	172
	.zero	4
	.quad	.LC422
	.long	173
	.zero	4
	.quad	.LC423
	.long	174
	.zero	4
	.quad	.LC424
	.long	175
	.zero	4
	.quad	.LC425
	.long	176
	.zero	4
	.quad	.LC426
	.long	177
	.zero	4
	.quad	.LC427
	.long	178
	.zero	4
	.quad	.LC428
	.long	179
	.zero	4
	.quad	.LC429
	.long	180
	.zero	4
	.quad	.LC430
	.long	181
	.zero	4
	.quad	.LC431
	.long	182
	.zero	4
	.quad	.LC432
	.long	183
	.zero	4
	.quad	.LC433
	.long	184
	.zero	4
	.quad	.LC434
	.long	185
	.zero	4
	.quad	.LC435
	.long	186
	.zero	4
	.quad	.LC436
	.long	187
	.zero	4
	.quad	.LC437
	.long	188
	.zero	4
	.quad	.LC438
	.long	189
	.zero	4
	.quad	.LC439
	.long	190
	.zero	4
	.quad	.LC440
	.long	191
	.zero	4
	.quad	.LC441
	.long	192
	.zero	4
	.quad	.LC442
	.long	193
	.zero	4
	.quad	.LC443
	.long	194
	.zero	4
	.quad	.LC444
	.long	195
	.zero	4
	.quad	.LC445
	.long	196
	.zero	4
	.quad	.LC446
	.long	197
	.zero	4
	.quad	.LC447
	.long	198
	.zero	4
	.quad	.LC448
	.long	199
	.zero	4
	.quad	.LC449
	.long	200
	.zero	4
	.quad	.LC450
	.long	201
	.zero	4
	.quad	.LC451
	.long	202
	.zero	4
	.quad	.LC452
	.long	203
	.zero	4
	.quad	.LC453
	.long	204
	.zero	4
	.quad	.LC454
	.long	205
	.zero	4
	.quad	.LC455
	.long	206
	.zero	4
	.quad	.LC456
	.long	207
	.zero	4
	.quad	.LC457
	.long	208
	.zero	4
	.quad	.LC458
	.long	209
	.zero	4
	.quad	.LC459
	.long	210
	.zero	4
	.quad	.LC460
	.long	211
	.zero	4
	.quad	.LC461
	.long	212
	.zero	4
	.quad	.LC462
	.long	213
	.zero	4
	.quad	.LC463
	.long	214
	.zero	4
	.quad	.LC464
	.long	215
	.zero	4
	.quad	.LC465
	.long	216
	.zero	4
	.quad	.LC466
	.long	217
	.zero	4
	.quad	.LC467
	.long	218
	.zero	4
	.quad	.LC468
	.long	219
	.zero	4
	.quad	.LC469
	.long	220
	.zero	4
	.quad	.LC470
	.long	221
	.zero	4
	.quad	.LC471
	.long	222
	.zero	4
	.quad	.LC472
	.long	223
	.zero	4
	.quad	.LC473
	.long	224
	.zero	4
	.quad	.LC474
	.long	225
	.zero	4
	.quad	.LC475
	.long	226
	.zero	4
	.quad	.LC476
	.long	227
	.zero	4
	.quad	.LC477
	.long	228
	.zero	4
	.quad	.LC478
	.long	229
	.zero	4
	.quad	.LC479
	.long	230
	.zero	4
	.quad	.LC480
	.long	231
	.zero	4
	.quad	.LC481
	.long	232
	.zero	4
	.quad	.LC482
	.long	233
	.zero	4
	.quad	.LC483
	.long	234
	.zero	4
	.quad	.LC484
	.long	235
	.zero	4
	.quad	.LC485
	.long	236
	.zero	4
	.quad	.LC486
	.long	237
	.zero	4
	.quad	.LC487
	.long	238
	.zero	4
	.quad	.LC488
	.long	239
	.zero	4
	.quad	.LC489
	.long	240
	.zero	4
	.quad	.LC490
	.globl	_ZN2v88internal14HeapObjectsMap23kFirstAvailableObjectIdE
	.section	.rodata._ZN2v88internal14HeapObjectsMap23kFirstAvailableObjectIdE,"a"
	.align 4
	.type	_ZN2v88internal14HeapObjectsMap23kFirstAvailableObjectIdE, @object
	.size	_ZN2v88internal14HeapObjectsMap23kFirstAvailableObjectIdE, 4
_ZN2v88internal14HeapObjectsMap23kFirstAvailableObjectIdE:
	.long	53
	.globl	_ZN2v88internal14HeapObjectsMap22kGcRootsFirstSubrootIdE
	.section	.rodata._ZN2v88internal14HeapObjectsMap22kGcRootsFirstSubrootIdE,"a"
	.align 4
	.type	_ZN2v88internal14HeapObjectsMap22kGcRootsFirstSubrootIdE, @object
	.size	_ZN2v88internal14HeapObjectsMap22kGcRootsFirstSubrootIdE, 4
_ZN2v88internal14HeapObjectsMap22kGcRootsFirstSubrootIdE:
	.long	5
	.globl	_ZN2v88internal14HeapObjectsMap16kGcRootsObjectIdE
	.section	.rodata._ZN2v88internal14HeapObjectsMap16kGcRootsObjectIdE,"a"
	.align 4
	.type	_ZN2v88internal14HeapObjectsMap16kGcRootsObjectIdE, @object
	.size	_ZN2v88internal14HeapObjectsMap16kGcRootsObjectIdE, 4
_ZN2v88internal14HeapObjectsMap16kGcRootsObjectIdE:
	.long	3
	.globl	_ZN2v88internal14HeapObjectsMap21kInternalRootObjectIdE
	.section	.rodata._ZN2v88internal14HeapObjectsMap21kInternalRootObjectIdE,"a"
	.align 4
	.type	_ZN2v88internal14HeapObjectsMap21kInternalRootObjectIdE, @object
	.size	_ZN2v88internal14HeapObjectsMap21kInternalRootObjectIdE, 4
_ZN2v88internal14HeapObjectsMap21kInternalRootObjectIdE:
	.long	1
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC30:
	.long	1065353216
	.align 4
.LC245:
	.long	1258291200
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC246:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC248:
	.long	0
	.long	1127219200
	.section	.rodata.cst16
	.align 16
.LC249:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC250:
	.long	0
	.long	1072693248
	.section	.rodata.cst4
	.align 4
.LC251:
	.long	1593835520
	.section	.rodata.cst8
	.align 8
.LC252:
	.long	0
	.long	1138753536
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
