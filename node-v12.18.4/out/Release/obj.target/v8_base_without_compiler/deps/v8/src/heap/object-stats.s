	.file	"object-stats.cc"
	.text
	.section	.text._ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE:
.LFB9535:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9535:
	.size	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE:
.LFB9536:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9536:
	.size	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.type	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm, @function
_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm:
.LFB9537:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9537:
	.size	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm, .-_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.section	.text._ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE:
.LFB9538:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9538:
	.size	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE:
.LFB9539:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9539:
	.size	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.type	_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, @function
_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_:
.LFB21626:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	subq	%rdx, %rcx
	shrq	$3, %rcx
	addq	%rcx, (%rax)
	ret
	.cfi_endproc
.LFE21626:
	.size	_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, .-_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.section	.text._ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_,"axG",@progbits,_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	.type	_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_, @function
_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_:
.LFB21627:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	subq	%rdx, %rcx
	shrq	$3, %rcx
	addq	%rcx, (%rax)
	ret
	.cfi_endproc
.LFE21627:
	.size	_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_, .-_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	.section	.text._ZN2v88internal19FieldStatsCollector15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal19FieldStatsCollector15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19FieldStatsCollector15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal19FieldStatsCollector15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal19FieldStatsCollector15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE:
.LFB21628:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21628:
	.size	_ZN2v88internal19FieldStatsCollector15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal19FieldStatsCollector15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZN2v88internal19FieldStatsCollector20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE,"axG",@progbits,_ZN2v88internal19FieldStatsCollector20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19FieldStatsCollector20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.type	_ZN2v88internal19FieldStatsCollector20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE, @function
_ZN2v88internal19FieldStatsCollector20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE:
.LFB21629:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	addq	$1, (%rax)
	ret
	.cfi_endproc
.LFE21629:
	.size	_ZN2v88internal19FieldStatsCollector20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE, .-_ZN2v88internal19FieldStatsCollector20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB26949:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE26949:
	.size	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB26950:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L13
	cmpl	$3, %edx
	je	.L14
	cmpl	$1, %edx
	je	.L18
.L14:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26950:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv,"axG",@progbits,_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv,comdat
	.p2align 4
	.weak	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv
	.type	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv:
.LFB26097:
	.cfi_startproc
	endbr64
	jmp	_ZN2v84base5MutexC1Ev@PLT
	.cfi_endproc
.LFE26097:
	.size	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv
	.section	.rodata._ZN2v88internalL14PrintJSONArrayEPmi.constprop.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"[ "
.LC1:
	.string	"%zu"
.LC2:
	.string	" ]"
.LC3:
	.string	", "
	.section	.text._ZN2v88internalL14PrintJSONArrayEPmi.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL14PrintJSONArrayEPmi.constprop.0, @function
_ZN2v88internalL14PrintJSONArrayEPmi.constprop.0:
.LFB27486:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	.LC3(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	.LC0(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	8(%r12), %rbx
	subq	$-128, %r12
	subq	$8, %rsp
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-128(%r12), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L21:
	movq	%r13, %rdi
	xorl	%eax, %eax
	addq	$8, %rbx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-8(%rbx), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpq	%rbx, %r12
	jne	.L21
	addq	$8, %rsp
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6PrintFEPKcz@PLT
	.cfi_endproc
.LFE27486:
	.size	_ZN2v88internalL14PrintJSONArrayEPmi.constprop.0, .-_ZN2v88internalL14PrintJSONArrayEPmi.constprop.0
	.section	.text._ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE, @function
_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE:
.LFB9532:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_(%rip), %rcx
	leaq	8(%rdx), %r8
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L27
	movq	64(%rdi), %rax
	addq	$1, (%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%r8, %rcx
	jmp	*%rax
	.cfi_endproc
.LFE9532:
	.size	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE, .-_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.section	.text._ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.type	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, @function
_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_:
.LFB9530:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_(%rip), %r8
	movq	16(%rax), %rax
	cmpq	%r8, %rax
	jne	.L31
	movq	64(%rdi), %rax
	subq	%rdx, %rcx
	shrq	$3, %rcx
	addq	%rcx, (%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	jmp	*%rax
	.cfi_endproc
.LFE9530:
	.size	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_, .-_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.section	.text._ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE, @function
_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE:
.LFB9531:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_(%rip), %rcx
	leaq	8(%rdx), %r8
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L35
	movq	64(%rdi), %rax
	addq	$1, (%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%r8, %rcx
	jmp	*%rax
	.cfi_endproc
.LFE9531:
	.size	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE, .-_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.section	.text._ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.type	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE, @function
_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE:
.LFB9533:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_(%rip), %rcx
	leaq	8(%rdx), %r9
	movq	32(%rax), %r8
	cmpq	%rcx, %r8
	jne	.L39
	movq	16(%rax), %rax
	leaq	_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_(%rip), %rcx
	cmpq	%rcx, %rax
	jne	.L40
	movq	64(%rdi), %rax
	addq	$1, (%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%r9, %rcx
	jmp	*%r8
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%r9, %rcx
	jmp	*%rax
	.cfi_endproc
.LFE9533:
	.size	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE, .-_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.section	.text._ZN2v88internal19FieldStatsCollectorD2Ev,"axG",@progbits,_ZN2v88internal19FieldStatsCollectorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19FieldStatsCollectorD2Ev
	.type	_ZN2v88internal19FieldStatsCollectorD2Ev, @function
_ZN2v88internal19FieldStatsCollectorD2Ev:
.LFB27158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal19FieldStatsCollectorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L45
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L46
.L45:
	movq	16(%rbx), %rax
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	addq	$56, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L44
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27158:
	.size	_ZN2v88internal19FieldStatsCollectorD2Ev, .-_ZN2v88internal19FieldStatsCollectorD2Ev
	.weak	_ZN2v88internal19FieldStatsCollectorD1Ev
	.set	_ZN2v88internal19FieldStatsCollectorD1Ev,_ZN2v88internal19FieldStatsCollectorD2Ev
	.section	.text._ZN2v88internal19FieldStatsCollectorD0Ev,"axG",@progbits,_ZN2v88internal19FieldStatsCollectorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19FieldStatsCollectorD0Ev
	.type	_ZN2v88internal19FieldStatsCollectorD0Ev, @function
_ZN2v88internal19FieldStatsCollectorD0Ev:
.LFB27160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal19FieldStatsCollectorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	24(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%rbx, %rbx
	je	.L54
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L55
.L54:
	movq	16(%r12), %rax
	movq	8(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	8(%r12), %rdi
	leaq	56(%r12), %rax
	movq	$0, 32(%r12)
	movq	$0, 24(%r12)
	cmpq	%rax, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	popq	%rbx
	movq	%r12, %rdi
	movl	$120, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27160:
	.size	_ZN2v88internal19FieldStatsCollectorD0Ev, .-_ZN2v88internal19FieldStatsCollectorD0Ev
	.section	.text._ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_,"axG",@progbits,_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.type	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_, @function
_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_:
.LFB9534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	leaq	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE(%rip), %rbx
	movq	40(%rax), %rcx
	cmpq	%rbx, %rcx
	jne	.L63
	movq	16(%rax), %rax
	leaq	_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_(%rip), %rcx
	leaq	8(%rdx), %r8
	cmpq	%rcx, %rax
	jne	.L64
	movq	64(%rdi), %rax
	addq	$1, (%rax)
.L66:
	movq	(%r12), %rax
	movq	40(%rax), %rcx
	cmpq	%rbx, %rcx
	jne	.L67
.L73:
	movq	16(%rax), %rax
	leaq	_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_(%rip), %rdx
	leaq	8(%r13), %rcx
	cmpq	%rdx, %rax
	jne	.L68
	movq	64(%r12), %rax
	addq	$1, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	call	*%rcx
	movq	(%r12), %rax
	movq	40(%rax), %rcx
	cmpq	%rbx, %rcx
	je	.L73
.L67:
	popq	%rbx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rcx
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	popq	%rbx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movq	%r8, %rcx
	call	*%rax
	jmp	.L66
	.cfi_endproc
.LFE9534:
	.size	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_, .-_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.section	.rodata._ZN2v88internalL13DumpJSONArrayERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPmi.constprop.0.str1.1,"aMS",@progbits,1
.LC4:
	.string	""
.LC5:
	.string	"["
.LC6:
	.string	"]"
	.section	.text._ZN2v88internalL13DumpJSONArrayERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPmi.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL13DumpJSONArrayERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPmi.constprop.0, @function
_ZN2v88internalL13DumpJSONArrayERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPmi.constprop.0:
.LFB27487:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	.LC3(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%r12, %rdi
	movq	%rsi, %rbx
	leaq	.LC5(%rip), %rsi
	leaq	128(%rbx), %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%edx, %edx
	leaq	.LC4(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%r13, %rsi
	movl	$2, %edx
	cmpq	%rbx, %r14
	jne	.L75
	popq	%rbx
	movq	%r12, %rdi
	movl	$1, %edx
	popq	%r12
	leaq	.LC6(%rip), %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.cfi_endproc
.LFE27487:
	.size	_ZN2v88internalL13DumpJSONArrayERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPmi.constprop.0, .-_ZN2v88internalL13DumpJSONArrayERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPmi.constprop.0
	.section	.text._ZN2v88internal11ObjectStats16ClearObjectStatsEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ObjectStats16ClearObjectStatsEb
	.type	_ZN2v88internal11ObjectStats16ClearObjectStatsEb, @function
_ZN2v88internal11ObjectStats16ClearObjectStatsEb:
.LFB21653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$9480, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$8, %rdi
	call	memset@PLT
	xorl	%esi, %esi
	movl	$9480, %edx
	leaq	18968(%rbx), %rdi
	call	memset@PLT
	xorl	%esi, %esi
	movl	$9480, %edx
	leaq	37928(%rbx), %rdi
	call	memset@PLT
	xorl	%esi, %esi
	movl	$151680, %edx
	leaq	47408(%rbx), %rdi
	call	memset@PLT
	xorl	%esi, %esi
	movl	$151680, %edx
	leaq	199088(%rbx), %rdi
	call	memset@PLT
	testb	%r12b, %r12b
	jne	.L81
.L79:
	movq	$0, 350816(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 350768(%rbx)
	movups	%xmm0, 350784(%rbx)
	movups	%xmm0, 350800(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	leaq	9488(%rbx), %rdi
	xorl	%esi, %esi
	movl	$9480, %edx
	call	memset@PLT
	leaq	28448(%rbx), %rdi
	movl	$9480, %edx
	xorl	%esi, %esi
	call	memset@PLT
	jmp	.L79
	.cfi_endproc
.LFE21653:
	.size	_ZN2v88internal11ObjectStats16ClearObjectStatsEb, .-_ZN2v88internal11ObjectStats16ClearObjectStatsEb
	.section	.rodata._ZN2v88internal11ObjectStats13PrintKeyAndIdEPKci.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"\"isolate\": \"%p\", \"id\": %d, \"key\": \"%s\", "
	.section	.text._ZN2v88internal11ObjectStats13PrintKeyAndIdEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ObjectStats13PrintKeyAndIdEPKci
	.type	_ZN2v88internal11ObjectStats13PrintKeyAndIdEPKci, @function
_ZN2v88internal11ObjectStats13PrintKeyAndIdEPKci:
.LFB21656:
	.cfi_startproc
	endbr64
	movq	%rsi, %rcx
	movq	(%rdi), %rsi
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rdi
	subq	$37592, %rsi
	jmp	_ZN2v88internal6PrintFEPKcz@PLT
	.cfi_endproc
.LFE21656:
	.size	_ZN2v88internal11ObjectStats13PrintKeyAndIdEPKci, .-_ZN2v88internal11ObjectStats13PrintKeyAndIdEPKci
	.section	.rodata._ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i.str1.1,"aMS",@progbits,1
.LC8:
	.string	"{ "
	.section	.rodata._ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"\"type\": \"instance_type_data\", "
	.section	.rodata._ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i.str1.1
.LC10:
	.string	"\"instance_type\": %d, "
.LC11:
	.string	"\"instance_type_name\": \"%s\", "
.LC12:
	.string	"\"overall\": %zu, "
.LC13:
	.string	"\"count\": %zu, "
.LC14:
	.string	"\"over_allocated\": %zu, "
.LC15:
	.string	"\"histogram\": "
.LC16:
	.string	","
.LC17:
	.string	"\"over_allocated_histogram\": "
.LC18:
	.string	" }\n"
	.section	.text._ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	.type	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i, @function
_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i:
.LFB21657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	.LC8(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movslq	%r8d, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rax
	movq	%r15, %rcx
	movl	%r14d, %edx
	leaq	.LC7(%rip), %rdi
	leaq	-37592(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	%ebx, %esi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	%r13, %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	(%r12,%rbx,8), %r13
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	movq	18968(%r13), %rsi
	salq	$7, %rbx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r13), %rsi
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	37928(%r13), %rsi
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	xorl	%eax, %eax
	leaq	.LC15(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	47408(%r12,%rbx), %rdi
	call	_ZN2v88internalL14PrintJSONArrayEPmi.constprop.0
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	xorl	%eax, %eax
	leaq	.LC17(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	199088(%r12,%rbx), %rdi
	call	_ZN2v88internalL14PrintJSONArrayEPmi.constprop.0
	addq	$8, %rsp
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6PrintFEPKcz@PLT
	.cfi_endproc
.LFE21657:
	.size	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i, .-_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"\"type\": \"gc_descriptor\", \"time\": %f }\n"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1,"aMS",@progbits,1
.LC20:
	.string	"\"type\": \"field_data\""
.LC21:
	.string	", \"tagged_fields\": %zu"
.LC22:
	.string	", \"embedder_fields\": %zu"
.LC23:
	.string	", \"inobject_smi_fields\": %zu"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC24:
	.string	", \"unboxed_double_fields\": %zu"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC25:
	.string	", \"boxed_double_fields\": %zu"
.LC26:
	.string	", \"string_data\": %zu"
.LC27:
	.string	", \"other_raw_fields\": %zu"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC28:
	.string	"\"type\": \"bucket_sizes\", \"sizes\": [ "
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC29:
	.string	"%d"
.LC30:
	.string	" ] }\n"
.LC31:
	.string	"INTERNALIZED_STRING_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC32:
	.string	"EXTERNAL_INTERNALIZED_STRING_TYPE"
	.align 8
.LC33:
	.string	"ONE_BYTE_INTERNALIZED_STRING_TYPE"
	.align 8
.LC34:
	.string	"EXTERNAL_ONE_BYTE_INTERNALIZED_STRING_TYPE"
	.align 8
.LC35:
	.string	"UNCACHED_EXTERNAL_INTERNALIZED_STRING_TYPE"
	.align 8
.LC36:
	.string	"UNCACHED_EXTERNAL_ONE_BYTE_INTERNALIZED_STRING_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC37:
	.string	"STRING_TYPE"
.LC38:
	.string	"CONS_STRING_TYPE"
.LC39:
	.string	"EXTERNAL_STRING_TYPE"
.LC40:
	.string	"SLICED_STRING_TYPE"
.LC41:
	.string	"THIN_STRING_TYPE"
.LC42:
	.string	"ONE_BYTE_STRING_TYPE"
.LC43:
	.string	"CONS_ONE_BYTE_STRING_TYPE"
.LC44:
	.string	"EXTERNAL_ONE_BYTE_STRING_TYPE"
.LC45:
	.string	"SLICED_ONE_BYTE_STRING_TYPE"
.LC46:
	.string	"THIN_ONE_BYTE_STRING_TYPE"
.LC47:
	.string	"UNCACHED_EXTERNAL_STRING_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC48:
	.string	"UNCACHED_EXTERNAL_ONE_BYTE_STRING_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC49:
	.string	"SYMBOL_TYPE"
.LC50:
	.string	"HEAP_NUMBER_TYPE"
.LC51:
	.string	"BIGINT_TYPE"
.LC52:
	.string	"ODDBALL_TYPE"
.LC53:
	.string	"MAP_TYPE"
.LC54:
	.string	"CODE_TYPE"
.LC55:
	.string	"BYTE_ARRAY_TYPE"
.LC56:
	.string	"FOREIGN_TYPE"
.LC57:
	.string	"BYTECODE_ARRAY_TYPE"
.LC58:
	.string	"FREE_SPACE_TYPE"
.LC59:
	.string	"FIXED_DOUBLE_ARRAY_TYPE"
.LC60:
	.string	"FEEDBACK_METADATA_TYPE"
.LC61:
	.string	"FILLER_TYPE"
.LC62:
	.string	"ACCESS_CHECK_INFO_TYPE"
.LC63:
	.string	"ACCESSOR_INFO_TYPE"
.LC64:
	.string	"ACCESSOR_PAIR_TYPE"
.LC65:
	.string	"ALIASED_ARGUMENTS_ENTRY_TYPE"
.LC66:
	.string	"ALLOCATION_MEMENTO_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC67:
	.string	"ARRAY_BOILERPLATE_DESCRIPTION_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC68:
	.string	"ASM_WASM_DATA_TYPE"
.LC69:
	.string	"ASYNC_GENERATOR_REQUEST_TYPE"
.LC70:
	.string	"CLASS_POSITIONS_TYPE"
.LC71:
	.string	"DEBUG_INFO_TYPE"
.LC72:
	.string	"ENUM_CACHE_TYPE"
.LC73:
	.string	"FUNCTION_TEMPLATE_INFO_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC74:
	.string	"FUNCTION_TEMPLATE_RARE_DATA_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC75:
	.string	"INTERCEPTOR_INFO_TYPE"
.LC76:
	.string	"INTERPRETER_DATA_TYPE"
.LC77:
	.string	"OBJECT_TEMPLATE_INFO_TYPE"
.LC78:
	.string	"PROMISE_CAPABILITY_TYPE"
.LC79:
	.string	"PROMISE_REACTION_TYPE"
.LC80:
	.string	"PROTOTYPE_INFO_TYPE"
.LC81:
	.string	"SCRIPT_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC82:
	.string	"SOURCE_POSITION_TABLE_WITH_FRAME_CACHE_TYPE"
	.align 8
.LC83:
	.string	"SOURCE_TEXT_MODULE_INFO_ENTRY_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC84:
	.string	"STACK_FRAME_INFO_TYPE"
.LC85:
	.string	"STACK_TRACE_FRAME_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC86:
	.string	"TEMPLATE_OBJECT_DESCRIPTION_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC87:
	.string	"TUPLE2_TYPE"
.LC88:
	.string	"TUPLE3_TYPE"
.LC89:
	.string	"WASM_CAPI_FUNCTION_DATA_TYPE"
.LC90:
	.string	"WASM_DEBUG_INFO_TYPE"
.LC91:
	.string	"WASM_EXCEPTION_TAG_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC92:
	.string	"WASM_EXPORTED_FUNCTION_DATA_TYPE"
	.align 8
.LC93:
	.string	"WASM_INDIRECT_FUNCTION_TABLE_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC94:
	.string	"WASM_JS_FUNCTION_DATA_TYPE"
.LC95:
	.string	"CALLABLE_TASK_TYPE"
.LC96:
	.string	"CALLBACK_TASK_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC97:
	.string	"PROMISE_FULFILL_REACTION_JOB_TASK_TYPE"
	.align 8
.LC98:
	.string	"PROMISE_REJECT_REACTION_JOB_TASK_TYPE"
	.align 8
.LC99:
	.string	"PROMISE_RESOLVE_THENABLE_JOB_TASK_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC100:
	.string	"SORT_STATE_TYPE"
.LC101:
	.string	"INTERNAL_CLASS_TYPE"
.LC102:
	.string	"SMI_PAIR_TYPE"
.LC103:
	.string	"SMI_BOX_TYPE"
.LC104:
	.string	"SOURCE_TEXT_MODULE_TYPE"
.LC105:
	.string	"SYNTHETIC_MODULE_TYPE"
.LC106:
	.string	"ALLOCATION_SITE_TYPE"
.LC107:
	.string	"EMBEDDER_DATA_ARRAY_TYPE"
.LC108:
	.string	"FIXED_ARRAY_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC109:
	.string	"OBJECT_BOILERPLATE_DESCRIPTION_TYPE"
	.align 8
.LC110:
	.string	"CLOSURE_FEEDBACK_CELL_ARRAY_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC111:
	.string	"HASH_TABLE_TYPE"
.LC112:
	.string	"ORDERED_HASH_MAP_TYPE"
.LC113:
	.string	"ORDERED_HASH_SET_TYPE"
.LC114:
	.string	"ORDERED_NAME_DICTIONARY_TYPE"
.LC115:
	.string	"NAME_DICTIONARY_TYPE"
.LC116:
	.string	"GLOBAL_DICTIONARY_TYPE"
.LC117:
	.string	"NUMBER_DICTIONARY_TYPE"
.LC118:
	.string	"SIMPLE_NUMBER_DICTIONARY_TYPE"
.LC119:
	.string	"STRING_TABLE_TYPE"
.LC120:
	.string	"EPHEMERON_HASH_TABLE_TYPE"
.LC121:
	.string	"SCOPE_INFO_TYPE"
.LC122:
	.string	"SCRIPT_CONTEXT_TABLE_TYPE"
.LC123:
	.string	"AWAIT_CONTEXT_TYPE"
.LC124:
	.string	"BLOCK_CONTEXT_TYPE"
.LC125:
	.string	"CATCH_CONTEXT_TYPE"
.LC126:
	.string	"DEBUG_EVALUATE_CONTEXT_TYPE"
.LC127:
	.string	"EVAL_CONTEXT_TYPE"
.LC128:
	.string	"FUNCTION_CONTEXT_TYPE"
.LC129:
	.string	"MODULE_CONTEXT_TYPE"
.LC130:
	.string	"NATIVE_CONTEXT_TYPE"
.LC131:
	.string	"SCRIPT_CONTEXT_TYPE"
.LC132:
	.string	"WITH_CONTEXT_TYPE"
.LC133:
	.string	"WEAK_FIXED_ARRAY_TYPE"
.LC134:
	.string	"TRANSITION_ARRAY_TYPE"
.LC135:
	.string	"CALL_HANDLER_INFO_TYPE"
.LC136:
	.string	"CELL_TYPE"
.LC137:
	.string	"CODE_DATA_CONTAINER_TYPE"
.LC138:
	.string	"DESCRIPTOR_ARRAY_TYPE"
.LC139:
	.string	"FEEDBACK_CELL_TYPE"
.LC140:
	.string	"FEEDBACK_VECTOR_TYPE"
.LC141:
	.string	"LOAD_HANDLER_TYPE"
.LC142:
	.string	"PREPARSE_DATA_TYPE"
.LC143:
	.string	"PROPERTY_ARRAY_TYPE"
.LC144:
	.string	"PROPERTY_CELL_TYPE"
.LC145:
	.string	"SHARED_FUNCTION_INFO_TYPE"
.LC146:
	.string	"SMALL_ORDERED_HASH_MAP_TYPE"
.LC147:
	.string	"SMALL_ORDERED_HASH_SET_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC148:
	.string	"SMALL_ORDERED_NAME_DICTIONARY_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC149:
	.string	"STORE_HANDLER_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC150:
	.string	"UNCOMPILED_DATA_WITHOUT_PREPARSE_DATA_TYPE"
	.align 8
.LC151:
	.string	"UNCOMPILED_DATA_WITH_PREPARSE_DATA_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC152:
	.string	"WEAK_ARRAY_LIST_TYPE"
.LC153:
	.string	"WEAK_CELL_TYPE"
.LC154:
	.string	"JS_PROXY_TYPE"
.LC155:
	.string	"JS_GLOBAL_OBJECT_TYPE"
.LC156:
	.string	"JS_GLOBAL_PROXY_TYPE"
.LC157:
	.string	"JS_MODULE_NAMESPACE_TYPE"
.LC158:
	.string	"JS_SPECIAL_API_OBJECT_TYPE"
.LC159:
	.string	"JS_PRIMITIVE_WRAPPER_TYPE"
.LC160:
	.string	"JS_API_OBJECT_TYPE"
.LC161:
	.string	"JS_OBJECT_TYPE"
.LC162:
	.string	"JS_ARGUMENTS_TYPE"
.LC163:
	.string	"JS_ARRAY_BUFFER_TYPE"
.LC164:
	.string	"JS_ARRAY_ITERATOR_TYPE"
.LC165:
	.string	"JS_ARRAY_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC166:
	.string	"JS_ASYNC_FROM_SYNC_ITERATOR_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC167:
	.string	"JS_ASYNC_FUNCTION_OBJECT_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC168:
	.string	"JS_ASYNC_GENERATOR_OBJECT_TYPE"
	.align 8
.LC169:
	.string	"JS_CONTEXT_EXTENSION_OBJECT_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC170:
	.string	"JS_DATE_TYPE"
.LC171:
	.string	"JS_ERROR_TYPE"
.LC172:
	.string	"JS_GENERATOR_OBJECT_TYPE"
.LC173:
	.string	"JS_MAP_TYPE"
.LC174:
	.string	"JS_MAP_KEY_ITERATOR_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC175:
	.string	"JS_MAP_KEY_VALUE_ITERATOR_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC176:
	.string	"JS_MAP_VALUE_ITERATOR_TYPE"
.LC177:
	.string	"JS_MESSAGE_OBJECT_TYPE"
.LC178:
	.string	"JS_PROMISE_TYPE"
.LC179:
	.string	"JS_REGEXP_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC180:
	.string	"JS_REGEXP_STRING_ITERATOR_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC181:
	.string	"JS_SET_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC182:
	.string	"JS_SET_KEY_VALUE_ITERATOR_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC183:
	.string	"JS_SET_VALUE_ITERATOR_TYPE"
.LC184:
	.string	"JS_STRING_ITERATOR_TYPE"
.LC185:
	.string	"JS_WEAK_REF_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC186:
	.string	"JS_FINALIZATION_GROUP_CLEANUP_ITERATOR_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC187:
	.string	"JS_FINALIZATION_GROUP_TYPE"
.LC188:
	.string	"JS_WEAK_MAP_TYPE"
.LC189:
	.string	"JS_WEAK_SET_TYPE"
.LC190:
	.string	"JS_TYPED_ARRAY_TYPE"
.LC191:
	.string	"JS_DATA_VIEW_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC192:
	.string	"JS_INTL_V8_BREAK_ITERATOR_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC193:
	.string	"JS_INTL_COLLATOR_TYPE"
.LC194:
	.string	"JS_INTL_DATE_TIME_FORMAT_TYPE"
.LC195:
	.string	"JS_INTL_LIST_FORMAT_TYPE"
.LC196:
	.string	"JS_INTL_LOCALE_TYPE"
.LC197:
	.string	"JS_INTL_NUMBER_FORMAT_TYPE"
.LC198:
	.string	"JS_INTL_PLURAL_RULES_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC199:
	.string	"JS_INTL_RELATIVE_TIME_FORMAT_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC200:
	.string	"JS_INTL_SEGMENT_ITERATOR_TYPE"
.LC201:
	.string	"JS_INTL_SEGMENTER_TYPE"
.LC202:
	.string	"WASM_EXCEPTION_TYPE"
.LC203:
	.string	"WASM_GLOBAL_TYPE"
.LC204:
	.string	"WASM_INSTANCE_TYPE"
.LC205:
	.string	"WASM_MEMORY_TYPE"
.LC206:
	.string	"WASM_MODULE_TYPE"
.LC207:
	.string	"WASM_TABLE_TYPE"
.LC208:
	.string	"JS_BOUND_FUNCTION_TYPE"
.LC209:
	.string	"JS_FUNCTION_TYPE"
.LC210:
	.string	"OPTIMIZED_FUNCTION"
.LC211:
	.string	"BYTECODE_HANDLER"
.LC212:
	.string	"STUB"
.LC213:
	.string	"BUILTIN"
.LC214:
	.string	"REGEXP"
.LC215:
	.string	"WASM_FUNCTION"
.LC216:
	.string	"WASM_TO_CAPI_FUNCTION"
.LC217:
	.string	"WASM_TO_JS_FUNCTION"
.LC218:
	.string	"JS_TO_WASM_FUNCTION"
.LC219:
	.string	"JS_TO_JS_FUNCTION"
.LC220:
	.string	"WASM_INTERPRETER_ENTRY"
.LC221:
	.string	"C_WASM_ENTRY"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC222:
	.string	"ARRAY_BOILERPLATE_DESCRIPTION_ELEMENTS_TYPE"
	.align 8
.LC223:
	.string	"ARRAY_DICTIONARY_ELEMENTS_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC224:
	.string	"ARRAY_ELEMENTS_TYPE"
.LC225:
	.string	"BOILERPLATE_ELEMENTS_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC226:
	.string	"BOILERPLATE_PROPERTY_ARRAY_TYPE"
	.align 8
.LC227:
	.string	"BOILERPLATE_PROPERTY_DICTIONARY_TYPE"
	.align 8
.LC228:
	.string	"BYTECODE_ARRAY_CONSTANT_POOL_TYPE"
	.align 8
.LC229:
	.string	"BYTECODE_ARRAY_HANDLER_TABLE_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC230:
	.string	"COW_ARRAY_TYPE"
.LC231:
	.string	"DEOPTIMIZATION_DATA_TYPE"
.LC232:
	.string	"DEPENDENT_CODE_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC233:
	.string	"DEPRECATED_DESCRIPTOR_ARRAY_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC234:
	.string	"EMBEDDED_OBJECT_TYPE"
.LC235:
	.string	"ENUM_KEYS_CACHE_TYPE"
.LC236:
	.string	"ENUM_INDICES_CACHE_TYPE"
.LC237:
	.string	"FEEDBACK_VECTOR_ENTRY_TYPE"
.LC238:
	.string	"FEEDBACK_VECTOR_HEADER_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC239:
	.string	"FEEDBACK_VECTOR_SLOT_CALL_TYPE"
	.align 8
.LC240:
	.string	"FEEDBACK_VECTOR_SLOT_CALL_UNUSED_TYPE"
	.align 8
.LC241:
	.string	"FEEDBACK_VECTOR_SLOT_ENUM_TYPE"
	.align 8
.LC242:
	.string	"FEEDBACK_VECTOR_SLOT_LOAD_TYPE"
	.align 8
.LC243:
	.string	"FEEDBACK_VECTOR_SLOT_LOAD_UNUSED_TYPE"
	.align 8
.LC244:
	.string	"FEEDBACK_VECTOR_SLOT_OTHER_TYPE"
	.align 8
.LC245:
	.string	"FEEDBACK_VECTOR_SLOT_STORE_TYPE"
	.align 8
.LC246:
	.string	"FEEDBACK_VECTOR_SLOT_STORE_UNUSED_TYPE"
	.align 8
.LC247:
	.string	"FUNCTION_TEMPLATE_INFO_ENTRIES_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC248:
	.string	"GLOBAL_ELEMENTS_TYPE"
.LC249:
	.string	"GLOBAL_PROPERTIES_TYPE"
.LC250:
	.string	"JS_ARRAY_BOILERPLATE_TYPE"
.LC251:
	.string	"JS_COLLECTION_TABLE_TYPE"
.LC252:
	.string	"JS_OBJECT_BOILERPLATE_TYPE"
.LC253:
	.string	"JS_UNCOMPILED_FUNCTION_TYPE"
.LC254:
	.string	"MAP_ABANDONED_PROTOTYPE_TYPE"
.LC255:
	.string	"MAP_DEPRECATED_TYPE"
.LC256:
	.string	"MAP_DICTIONARY_TYPE"
.LC257:
	.string	"MAP_PROTOTYPE_DICTIONARY_TYPE"
.LC258:
	.string	"MAP_PROTOTYPE_TYPE"
.LC259:
	.string	"MAP_STABLE_TYPE"
.LC260:
	.string	"NUMBER_STRING_CACHE_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC261:
	.string	"OBJECT_DICTIONARY_ELEMENTS_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC262:
	.string	"OBJECT_ELEMENTS_TYPE"
.LC263:
	.string	"OBJECT_PROPERTY_ARRAY_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC264:
	.string	"OBJECT_PROPERTY_DICTIONARY_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC265:
	.string	"OBJECT_TO_CODE_TYPE"
.LC266:
	.string	"OPTIMIZED_CODE_LITERALS_TYPE"
.LC267:
	.string	"OTHER_CONTEXT_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC268:
	.string	"PROTOTYPE_DESCRIPTOR_ARRAY_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC269:
	.string	"PROTOTYPE_PROPERTY_ARRAY_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC270:
	.string	"PROTOTYPE_PROPERTY_DICTIONARY_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC271:
	.string	"PROTOTYPE_USERS_TYPE"
.LC272:
	.string	"REGEXP_MULTIPLE_CACHE_TYPE"
.LC273:
	.string	"RELOC_INFO_TYPE"
.LC274:
	.string	"RETAINED_MAPS_TYPE"
.LC275:
	.string	"SCRIPT_LIST_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC276:
	.string	"SCRIPT_SHARED_FUNCTION_INFOS_TYPE"
	.align 8
.LC277:
	.string	"SCRIPT_SOURCE_EXTERNAL_ONE_BYTE_TYPE"
	.align 8
.LC278:
	.string	"SCRIPT_SOURCE_EXTERNAL_TWO_BYTE_TYPE"
	.align 8
.LC279:
	.string	"SCRIPT_SOURCE_NON_EXTERNAL_ONE_BYTE_TYPE"
	.align 8
.LC280:
	.string	"SCRIPT_SOURCE_NON_EXTERNAL_TWO_BYTE_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC281:
	.string	"SERIALIZED_OBJECTS_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC282:
	.string	"SINGLE_CHARACTER_STRING_CACHE_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC283:
	.string	"STRING_SPLIT_CACHE_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC284:
	.string	"STRING_EXTERNAL_RESOURCE_ONE_BYTE_TYPE"
	.align 8
.LC285:
	.string	"STRING_EXTERNAL_RESOURCE_TWO_BYTE_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.1
.LC286:
	.string	"SOURCE_POSITION_TABLE_TYPE"
	.section	.rodata._ZN2v88internal11ObjectStats9PrintJSONEPKc.str1.8
	.align 8
.LC287:
	.string	"UNCOMPILED_SHARED_FUNCTION_INFO_TYPE"
	.align 8
.LC288:
	.string	"WEAK_NEW_SPACE_OBJECT_TO_CODE_TYPE"
	.section	.text._ZN2v88internal11ObjectStats9PrintJSONEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ObjectStats9PrintJSONEPKc
	.type	_ZN2v88internal11ObjectStats9PrintJSONEPKc, @function
_ZN2v88internal11ObjectStats9PrintJSONEPKc:
.LFB21658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$1, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	leaq	-37592(%rdi), %rbx
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movq	(%r12), %rax
	subsd	41464(%rbx), %xmm0
	leaq	.LC8(%rip), %rdi
	movl	$6, %ebx
	movl	452(%rax), %r14d
	xorl	%eax, %eax
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rax
	movq	%r13, %rcx
	movl	%r14d, %edx
	leaq	.LC7(%rip), %rdi
	leaq	-37592(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movsd	-56(%rbp), %xmm0
	movl	$1, %eax
	leaq	.LC19(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rax
	movq	%r13, %rcx
	movl	%r14d, %edx
	leaq	.LC7(%rip), %rdi
	leaq	-37592(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	xorl	%eax, %eax
	leaq	.LC20(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	350768(%r12), %rax
	leaq	.LC21(%rip), %rdi
	leaq	0(,%rax,8), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	350776(%r12), %rax
	leaq	.LC22(%rip), %rdi
	leaq	0(,%rax,8), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	350784(%r12), %rax
	leaq	.LC23(%rip), %rdi
	leaq	0(,%rax,8), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	350792(%r12), %rax
	leaq	.LC24(%rip), %rdi
	leaq	0(,%rax,8), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	350800(%r12), %rax
	leaq	.LC25(%rip), %rdi
	leaq	0(,%rax,8), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	350808(%r12), %rax
	leaq	.LC26(%rip), %rdi
	leaq	0(,%rax,8), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	350816(%r12), %rax
	leaq	.LC27(%rip), %rdi
	leaq	0(,%rax,8), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	xorl	%eax, %eax
	leaq	.LC18(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rax
	movq	%r13, %rcx
	movl	%r14d, %edx
	leaq	.LC7(%rip), %rdi
	leaq	-37592(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	.LC28(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	$32, %esi
	leaq	.LC29(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	%ebx, %ecx
	movl	%r15d, %esi
	xorl	%eax, %eax
	sall	%cl, %esi
	leaq	.LC29(%rip), %rdi
	addl	$1, %ebx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpl	$21, %ebx
	jne	.L86
	xorl	%eax, %eax
	leaq	.LC30(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	xorl	%r8d, %r8d
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC31(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$2, %r8d
	leaq	.LC32(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$8, %r8d
	leaq	.LC33(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$10, %r8d
	leaq	.LC34(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$18, %r8d
	leaq	.LC35(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$26, %r8d
	leaq	.LC36(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %r8d
	leaq	.LC37(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$33, %r8d
	leaq	.LC38(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$34, %r8d
	leaq	.LC39(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$35, %r8d
	leaq	.LC40(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$37, %r8d
	leaq	.LC41(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$40, %r8d
	leaq	.LC42(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$41, %r8d
	leaq	.LC43(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$42, %r8d
	leaq	.LC44(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$43, %r8d
	leaq	.LC45(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$45, %r8d
	leaq	.LC46(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$50, %r8d
	leaq	.LC47(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$58, %r8d
	leaq	.LC48(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$64, %r8d
	leaq	.LC49(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$65, %r8d
	leaq	.LC50(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$66, %r8d
	leaq	.LC51(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$67, %r8d
	leaq	.LC52(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$68, %r8d
	leaq	.LC53(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$69, %r8d
	leaq	.LC54(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$70, %r8d
	leaq	.LC55(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$71, %r8d
	leaq	.LC56(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$72, %r8d
	leaq	.LC57(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$73, %r8d
	leaq	.LC58(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$74, %r8d
	leaq	.LC59(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$75, %r8d
	leaq	.LC60(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$76, %r8d
	leaq	.LC61(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$77, %r8d
	leaq	.LC62(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$78, %r8d
	leaq	.LC63(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$79, %r8d
	leaq	.LC64(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$80, %r8d
	leaq	.LC65(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$81, %r8d
	leaq	.LC66(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$82, %r8d
	leaq	.LC67(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$83, %r8d
	leaq	.LC68(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$84, %r8d
	leaq	.LC69(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$85, %r8d
	leaq	.LC70(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$86, %r8d
	leaq	.LC71(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$87, %r8d
	leaq	.LC72(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$88, %r8d
	leaq	.LC73(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$89, %r8d
	leaq	.LC74(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$90, %r8d
	leaq	.LC75(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$91, %r8d
	leaq	.LC76(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$92, %r8d
	leaq	.LC77(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$93, %r8d
	leaq	.LC78(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$94, %r8d
	leaq	.LC79(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$95, %r8d
	leaq	.LC80(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$96, %r8d
	leaq	.LC81(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$97, %r8d
	leaq	.LC82(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$98, %r8d
	leaq	.LC83(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$99, %r8d
	leaq	.LC84(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$100, %r8d
	leaq	.LC85(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$101, %r8d
	leaq	.LC86(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$102, %r8d
	leaq	.LC87(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$103, %r8d
	leaq	.LC88(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$104, %r8d
	leaq	.LC89(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$105, %r8d
	leaq	.LC90(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$106, %r8d
	leaq	.LC91(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$107, %r8d
	leaq	.LC92(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$108, %r8d
	leaq	.LC93(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$109, %r8d
	leaq	.LC94(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$110, %r8d
	leaq	.LC95(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$111, %r8d
	leaq	.LC96(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$112, %r8d
	leaq	.LC97(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$113, %r8d
	leaq	.LC98(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$114, %r8d
	leaq	.LC99(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$115, %r8d
	leaq	.LC100(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$116, %r8d
	leaq	.LC101(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$117, %r8d
	leaq	.LC102(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$118, %r8d
	leaq	.LC103(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$119, %r8d
	leaq	.LC104(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$120, %r8d
	leaq	.LC105(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$121, %r8d
	leaq	.LC106(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$122, %r8d
	leaq	.LC107(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$123, %r8d
	leaq	.LC108(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$124, %r8d
	leaq	.LC109(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$125, %r8d
	leaq	.LC110(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$126, %r8d
	leaq	.LC111(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$127, %r8d
	leaq	.LC112(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$128, %r8d
	leaq	.LC113(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$129, %r8d
	leaq	.LC114(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$130, %r8d
	leaq	.LC115(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$131, %r8d
	leaq	.LC116(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$132, %r8d
	leaq	.LC117(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$133, %r8d
	leaq	.LC118(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$134, %r8d
	leaq	.LC119(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$135, %r8d
	leaq	.LC120(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$136, %r8d
	leaq	.LC121(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$137, %r8d
	leaq	.LC122(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$138, %r8d
	leaq	.LC123(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$139, %r8d
	leaq	.LC124(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$140, %r8d
	leaq	.LC125(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$141, %r8d
	leaq	.LC126(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$142, %r8d
	leaq	.LC127(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$143, %r8d
	leaq	.LC128(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$144, %r8d
	leaq	.LC129(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$145, %r8d
	leaq	.LC130(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$146, %r8d
	leaq	.LC131(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$147, %r8d
	leaq	.LC132(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$148, %r8d
	leaq	.LC133(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$149, %r8d
	leaq	.LC134(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$150, %r8d
	leaq	.LC135(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$151, %r8d
	leaq	.LC136(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$152, %r8d
	leaq	.LC137(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$153, %r8d
	leaq	.LC138(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$154, %r8d
	leaq	.LC139(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$155, %r8d
	leaq	.LC140(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$156, %r8d
	leaq	.LC141(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$157, %r8d
	leaq	.LC142(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$158, %r8d
	leaq	.LC143(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$159, %r8d
	leaq	.LC144(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$160, %r8d
	leaq	.LC145(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$161, %r8d
	leaq	.LC146(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$162, %r8d
	leaq	.LC147(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$163, %r8d
	leaq	.LC148(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$164, %r8d
	leaq	.LC149(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$165, %r8d
	leaq	.LC150(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$166, %r8d
	leaq	.LC151(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$167, %r8d
	leaq	.LC152(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$168, %r8d
	leaq	.LC153(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1024, %r8d
	leaq	.LC154(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1025, %r8d
	leaq	.LC155(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1026, %r8d
	leaq	.LC156(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1027, %r8d
	leaq	.LC157(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1040, %r8d
	leaq	.LC158(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1041, %r8d
	leaq	.LC159(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1056, %r8d
	leaq	.LC160(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1057, %r8d
	leaq	.LC161(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1058, %r8d
	leaq	.LC162(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1059, %r8d
	leaq	.LC163(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1060, %r8d
	leaq	.LC164(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1061, %r8d
	leaq	.LC165(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1062, %r8d
	leaq	.LC166(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1063, %r8d
	leaq	.LC167(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1064, %r8d
	leaq	.LC168(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1065, %r8d
	leaq	.LC169(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1066, %r8d
	leaq	.LC170(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1067, %r8d
	leaq	.LC171(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1068, %r8d
	leaq	.LC172(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1069, %r8d
	leaq	.LC173(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1070, %r8d
	leaq	.LC174(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1071, %r8d
	leaq	.LC175(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1072, %r8d
	leaq	.LC176(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1073, %r8d
	leaq	.LC177(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1074, %r8d
	leaq	.LC178(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1075, %r8d
	leaq	.LC179(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1076, %r8d
	leaq	.LC180(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1077, %r8d
	leaq	.LC181(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1078, %r8d
	leaq	.LC182(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1079, %r8d
	leaq	.LC183(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1080, %r8d
	leaq	.LC184(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1081, %r8d
	leaq	.LC185(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1082, %r8d
	leaq	.LC186(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1083, %r8d
	leaq	.LC187(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1084, %r8d
	leaq	.LC188(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1085, %r8d
	leaq	.LC189(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1086, %r8d
	leaq	.LC190(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1087, %r8d
	leaq	.LC191(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1088, %r8d
	leaq	.LC192(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1089, %r8d
	leaq	.LC193(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1090, %r8d
	leaq	.LC194(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1091, %r8d
	leaq	.LC195(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1092, %r8d
	leaq	.LC196(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1093, %r8d
	leaq	.LC197(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1094, %r8d
	leaq	.LC198(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1095, %r8d
	leaq	.LC199(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1096, %r8d
	leaq	.LC200(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1097, %r8d
	leaq	.LC201(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1098, %r8d
	leaq	.LC202(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1099, %r8d
	leaq	.LC203(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1100, %r8d
	leaq	.LC204(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1101, %r8d
	leaq	.LC205(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1102, %r8d
	leaq	.LC206(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1103, %r8d
	leaq	.LC207(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1104, %r8d
	leaq	.LC208(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1105, %r8d
	leaq	.LC209(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1106, %r8d
	leaq	.LC210(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1107, %r8d
	leaq	.LC211(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1108, %r8d
	leaq	.LC212(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1109, %r8d
	leaq	.LC213(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1110, %r8d
	leaq	.LC214(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1111, %r8d
	leaq	.LC215(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1112, %r8d
	leaq	.LC216(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1113, %r8d
	leaq	.LC217(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1114, %r8d
	leaq	.LC218(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1115, %r8d
	leaq	.LC219(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1116, %r8d
	leaq	.LC220(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1117, %r8d
	leaq	.LC221(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1118, %r8d
	leaq	.LC222(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1119, %r8d
	leaq	.LC223(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1120, %r8d
	leaq	.LC224(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1121, %r8d
	leaq	.LC225(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1122, %r8d
	leaq	.LC226(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1123, %r8d
	leaq	.LC227(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1124, %r8d
	leaq	.LC228(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1125, %r8d
	leaq	.LC229(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1126, %r8d
	leaq	.LC230(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1127, %r8d
	leaq	.LC231(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1128, %r8d
	leaq	.LC232(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1129, %r8d
	leaq	.LC233(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1130, %r8d
	leaq	.LC234(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1131, %r8d
	leaq	.LC235(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1132, %r8d
	leaq	.LC236(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1133, %r8d
	leaq	.LC237(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1134, %r8d
	leaq	.LC238(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1135, %r8d
	leaq	.LC239(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1136, %r8d
	leaq	.LC240(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1137, %r8d
	leaq	.LC241(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1138, %r8d
	leaq	.LC242(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1139, %r8d
	leaq	.LC243(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1140, %r8d
	leaq	.LC244(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1141, %r8d
	leaq	.LC245(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1142, %r8d
	leaq	.LC246(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1143, %r8d
	leaq	.LC247(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1144, %r8d
	leaq	.LC248(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1145, %r8d
	leaq	.LC249(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1146, %r8d
	leaq	.LC250(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1147, %r8d
	leaq	.LC251(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1148, %r8d
	leaq	.LC252(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1149, %r8d
	leaq	.LC253(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1150, %r8d
	leaq	.LC254(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1151, %r8d
	leaq	.LC255(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1152, %r8d
	leaq	.LC256(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1153, %r8d
	leaq	.LC257(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1154, %r8d
	leaq	.LC258(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1155, %r8d
	leaq	.LC259(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1156, %r8d
	leaq	.LC260(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1157, %r8d
	leaq	.LC261(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1158, %r8d
	leaq	.LC262(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1159, %r8d
	leaq	.LC263(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1160, %r8d
	leaq	.LC264(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1161, %r8d
	leaq	.LC265(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1162, %r8d
	leaq	.LC266(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1163, %r8d
	leaq	.LC267(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1164, %r8d
	leaq	.LC268(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1165, %r8d
	leaq	.LC269(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1166, %r8d
	leaq	.LC270(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1167, %r8d
	leaq	.LC271(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1168, %r8d
	leaq	.LC272(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1169, %r8d
	leaq	.LC273(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1170, %r8d
	leaq	.LC274(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1171, %r8d
	leaq	.LC275(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1172, %r8d
	leaq	.LC276(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1173, %r8d
	leaq	.LC277(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1174, %r8d
	leaq	.LC278(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1175, %r8d
	leaq	.LC279(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1176, %r8d
	leaq	.LC280(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1177, %r8d
	leaq	.LC281(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1178, %r8d
	leaq	.LC282(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1179, %r8d
	leaq	.LC283(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1180, %r8d
	leaq	.LC284(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1181, %r8d
	leaq	.LC285(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1182, %r8d
	leaq	.LC286(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1183, %r8d
	leaq	.LC287(%rip), %rcx
	call	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	addq	$24, %rsp
	movl	%r14d, %edx
	movq	%r13, %rsi
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movl	$1184, %r8d
	popq	%r13
	leaq	.LC288(%rip), %rcx
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11ObjectStats21PrintInstanceTypeJSONEPKciS3_i
	.cfi_endproc
.LFE21658:
	.size	_ZN2v88internal11ObjectStats9PrintJSONEPKc, .-_ZN2v88internal11ObjectStats9PrintJSONEPKc
	.section	.rodata._ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci.str1.1,"aMS",@progbits,1
.LC289:
	.string	"\""
.LC290:
	.string	"\":{"
.LC291:
	.string	"\"type\":"
.LC292:
	.string	"\"overall\":"
.LC293:
	.string	"\"count\":"
.LC294:
	.string	"\"over_allocated\":"
.LC295:
	.string	"\"histogram\":"
.LC296:
	.string	",\"over_allocated_histogram\":"
.LC297:
	.string	"},"
	.section	.text._ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	.type	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci, @function
_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci:
.LFB21659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movl	$1, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	16(%rsi), %r12
	leaq	.LC289(%rip), %rsi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movslq	%ecx, %rbx
	subq	$8, %rsp
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r15, %r15
	je	.L95
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L93:
	movq	%r12, %rdi
	movl	$3, %edx
	leaq	.LC290(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$7, %edx
	movq	%r12, %rdi
	leaq	(%r14,%rbx,8), %r15
	leaq	.LC291(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	salq	$7, %rbx
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$10, %edx
	movq	%r12, %rdi
	leaq	.LC292(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	18968(%r15), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$8, %edx
	movq	%r12, %rdi
	leaq	.LC293(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r15), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$17, %edx
	movq	%r12, %rdi
	leaq	.LC294(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	37928(%r15), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$12, %edx
	movq	%r12, %rdi
	leaq	.LC295(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	47408(%r14,%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internalL13DumpJSONArrayERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPmi.constprop.0
	movl	$28, %edx
	movq	%r12, %rdi
	leaq	.LC296(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	199088(%r14,%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internalL13DumpJSONArrayERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPmi.constprop.0
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$2, %edx
	popq	%rbx
	leaq	.LC297(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movq	16(%r13), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L93
	.cfi_endproc
.LFE21659:
	.size	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci, .-_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	.section	.rodata._ZN2v88internal11ObjectStats4DumpERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE.str1.1,"aMS",@progbits,1
.LC298:
	.string	"{"
.LC299:
	.string	"\"isolate\":\""
.LC300:
	.string	"\","
.LC301:
	.string	"\"id\":"
.LC302:
	.string	"\"time\":"
.LC303:
	.string	"\"field_data\":{"
.LC304:
	.string	"\"tagged_fields\":"
.LC305:
	.string	",\"embedder_fields\":"
.LC306:
	.string	",\"inobject_smi_fields\": "
.LC307:
	.string	",\"unboxed_double_fields\": "
.LC308:
	.string	",\"boxed_double_fields\": "
.LC309:
	.string	",\"string_data\": "
.LC310:
	.string	",\"other_raw_fields\":"
.LC311:
	.string	"}, "
.LC312:
	.string	"\"bucket_sizes\":["
.LC313:
	.string	"],"
.LC314:
	.string	"\"type_data\":{"
.LC315:
	.string	"\"END\":{}}}"
	.section	.text._ZN2v88internal11ObjectStats4DumpERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ObjectStats4DumpERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal11ObjectStats4DumpERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal11ObjectStats4DumpERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE:
.LFB21660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	16(%r13), %r14
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	leaq	-37592(%rdi), %rbx
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	subsd	41464(%rbx), %xmm0
	movq	(%r12), %rax
	movl	$1, %edx
	leaq	.LC298(%rip), %rsi
	movq	%r14, %rdi
	movl	$6, %ebx
	movl	452(%rax), %r15d
	movsd	%xmm0, -56(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$11, %edx
	leaq	.LC299(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	%r14, %rdi
	leaq	-37592(%rax), %rsi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$2, %edx
	leaq	.LC300(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$5, %edx
	leaq	.LC301(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r15d, %esi
	movq	%r14, %rdi
	movl	$1, %r15d
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$7, %edx
	leaq	.LC302(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movsd	-56(%rbp), %xmm0
	movq	%r14, %rdi
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$14, %edx
	leaq	.LC303(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$16, %edx
	leaq	.LC304(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	350768(%r12), %rax
	movq	%r14, %rdi
	leaq	0(,%rax,8), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$19, %edx
	leaq	.LC305(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	350776(%r12), %rax
	movq	%r14, %rdi
	leaq	0(,%rax,8), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$24, %edx
	leaq	.LC306(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	350784(%r12), %rax
	movq	%r14, %rdi
	leaq	0(,%rax,8), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$26, %edx
	leaq	.LC307(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	350792(%r12), %rax
	movq	%r14, %rdi
	leaq	0(,%rax,8), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$24, %edx
	leaq	.LC308(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	350800(%r12), %rax
	movq	%r14, %rdi
	leaq	0(,%rax,8), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$16, %edx
	leaq	.LC309(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	350808(%r12), %rax
	movq	%r14, %rdi
	leaq	0(,%rax,8), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$20, %edx
	leaq	.LC310(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	350816(%r12), %rax
	movq	%r14, %rdi
	leaq	0(,%rax,8), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$3, %edx
	leaq	.LC311(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$16, %edx
	leaq	.LC312(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZNSolsEi@PLT
	.p2align 4,,10
	.p2align 3
.L97:
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%ebx, %ecx
	movl	%r15d, %esi
	movq	%r14, %rdi
	sall	%cl, %esi
	addl	$1, %ebx
	call	_ZNSolsEi@PLT
	cmpl	$21, %ebx
	jne	.L97
	movq	%r14, %rdi
	movl	$2, %edx
	leaq	.LC313(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rdi
	movl	$13, %edx
	leaq	.LC314(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC31(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$2, %ecx
	leaq	.LC32(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$8, %ecx
	leaq	.LC33(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$10, %ecx
	leaq	.LC34(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$18, %ecx
	leaq	.LC35(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$26, %ecx
	leaq	.LC36(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %ecx
	leaq	.LC37(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$33, %ecx
	leaq	.LC38(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$34, %ecx
	leaq	.LC39(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$35, %ecx
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$37, %ecx
	leaq	.LC41(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$40, %ecx
	leaq	.LC42(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$41, %ecx
	leaq	.LC43(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$42, %ecx
	leaq	.LC44(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$43, %ecx
	leaq	.LC45(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$45, %ecx
	leaq	.LC46(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$50, %ecx
	leaq	.LC47(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$58, %ecx
	leaq	.LC48(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$64, %ecx
	leaq	.LC49(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$65, %ecx
	leaq	.LC50(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$66, %ecx
	leaq	.LC51(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$67, %ecx
	leaq	.LC52(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$68, %ecx
	leaq	.LC53(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$69, %ecx
	leaq	.LC54(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$70, %ecx
	leaq	.LC55(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$71, %ecx
	leaq	.LC56(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$72, %ecx
	leaq	.LC57(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$73, %ecx
	leaq	.LC58(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$74, %ecx
	leaq	.LC59(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$75, %ecx
	leaq	.LC60(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$76, %ecx
	leaq	.LC61(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$77, %ecx
	leaq	.LC62(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$78, %ecx
	leaq	.LC63(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$79, %ecx
	leaq	.LC64(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$80, %ecx
	leaq	.LC65(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$81, %ecx
	leaq	.LC66(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$82, %ecx
	leaq	.LC67(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$83, %ecx
	leaq	.LC68(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$84, %ecx
	leaq	.LC69(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$85, %ecx
	leaq	.LC70(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$86, %ecx
	leaq	.LC71(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$87, %ecx
	leaq	.LC72(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$88, %ecx
	leaq	.LC73(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$89, %ecx
	leaq	.LC74(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$90, %ecx
	leaq	.LC75(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$91, %ecx
	leaq	.LC76(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$92, %ecx
	leaq	.LC77(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$93, %ecx
	leaq	.LC78(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$94, %ecx
	leaq	.LC79(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$95, %ecx
	leaq	.LC80(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$96, %ecx
	leaq	.LC81(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$97, %ecx
	leaq	.LC82(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$98, %ecx
	leaq	.LC83(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$99, %ecx
	leaq	.LC84(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$100, %ecx
	leaq	.LC85(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$101, %ecx
	leaq	.LC86(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$102, %ecx
	leaq	.LC87(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$103, %ecx
	leaq	.LC88(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$104, %ecx
	leaq	.LC89(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$105, %ecx
	leaq	.LC90(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$106, %ecx
	leaq	.LC91(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$107, %ecx
	leaq	.LC92(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$108, %ecx
	leaq	.LC93(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$109, %ecx
	leaq	.LC94(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$110, %ecx
	leaq	.LC95(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$111, %ecx
	leaq	.LC96(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$112, %ecx
	leaq	.LC97(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$113, %ecx
	leaq	.LC98(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$114, %ecx
	leaq	.LC99(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$115, %ecx
	leaq	.LC100(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$116, %ecx
	leaq	.LC101(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$117, %ecx
	leaq	.LC102(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$118, %ecx
	leaq	.LC103(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$119, %ecx
	leaq	.LC104(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$120, %ecx
	leaq	.LC105(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$121, %ecx
	leaq	.LC106(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$122, %ecx
	leaq	.LC107(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$123, %ecx
	leaq	.LC108(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$124, %ecx
	leaq	.LC109(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$125, %ecx
	leaq	.LC110(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$126, %ecx
	leaq	.LC111(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$127, %ecx
	leaq	.LC112(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$128, %ecx
	leaq	.LC113(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$129, %ecx
	leaq	.LC114(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$130, %ecx
	leaq	.LC115(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$131, %ecx
	leaq	.LC116(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$132, %ecx
	leaq	.LC117(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$133, %ecx
	leaq	.LC118(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$134, %ecx
	leaq	.LC119(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$135, %ecx
	leaq	.LC120(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$136, %ecx
	leaq	.LC121(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$137, %ecx
	leaq	.LC122(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$138, %ecx
	leaq	.LC123(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$139, %ecx
	leaq	.LC124(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$140, %ecx
	leaq	.LC125(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$141, %ecx
	leaq	.LC126(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$142, %ecx
	leaq	.LC127(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$143, %ecx
	leaq	.LC128(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$144, %ecx
	leaq	.LC129(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$145, %ecx
	leaq	.LC130(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC131(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$147, %ecx
	leaq	.LC132(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$148, %ecx
	leaq	.LC133(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$149, %ecx
	leaq	.LC134(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$150, %ecx
	leaq	.LC135(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$151, %ecx
	leaq	.LC136(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$152, %ecx
	leaq	.LC137(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$153, %ecx
	leaq	.LC138(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$154, %ecx
	leaq	.LC139(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$155, %ecx
	leaq	.LC140(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$156, %ecx
	leaq	.LC141(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$157, %ecx
	leaq	.LC142(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$158, %ecx
	leaq	.LC143(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$159, %ecx
	leaq	.LC144(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$160, %ecx
	leaq	.LC145(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$161, %ecx
	leaq	.LC146(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$162, %ecx
	leaq	.LC147(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$163, %ecx
	leaq	.LC148(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$164, %ecx
	leaq	.LC149(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$165, %ecx
	leaq	.LC150(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$166, %ecx
	leaq	.LC151(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$167, %ecx
	leaq	.LC152(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$168, %ecx
	leaq	.LC153(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1024, %ecx
	leaq	.LC154(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1025, %ecx
	leaq	.LC155(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1026, %ecx
	leaq	.LC156(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1027, %ecx
	leaq	.LC157(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1040, %ecx
	leaq	.LC158(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1041, %ecx
	leaq	.LC159(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1056, %ecx
	leaq	.LC160(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1057, %ecx
	leaq	.LC161(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1058, %ecx
	leaq	.LC162(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1059, %ecx
	leaq	.LC163(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1060, %ecx
	leaq	.LC164(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1061, %ecx
	leaq	.LC165(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1062, %ecx
	leaq	.LC166(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1063, %ecx
	leaq	.LC167(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1064, %ecx
	leaq	.LC168(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1065, %ecx
	leaq	.LC169(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1066, %ecx
	leaq	.LC170(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1067, %ecx
	leaq	.LC171(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1068, %ecx
	leaq	.LC172(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1069, %ecx
	leaq	.LC173(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1070, %ecx
	leaq	.LC174(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1071, %ecx
	leaq	.LC175(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1072, %ecx
	leaq	.LC176(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1073, %ecx
	leaq	.LC177(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1074, %ecx
	leaq	.LC178(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1075, %ecx
	leaq	.LC179(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1076, %ecx
	leaq	.LC180(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1077, %ecx
	leaq	.LC181(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1078, %ecx
	leaq	.LC182(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1079, %ecx
	leaq	.LC183(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1080, %ecx
	leaq	.LC184(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1081, %ecx
	leaq	.LC185(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1082, %ecx
	leaq	.LC186(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1083, %ecx
	leaq	.LC187(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1084, %ecx
	leaq	.LC188(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1085, %ecx
	leaq	.LC189(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1086, %ecx
	leaq	.LC190(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1087, %ecx
	leaq	.LC191(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1088, %ecx
	leaq	.LC192(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1089, %ecx
	leaq	.LC193(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1090, %ecx
	leaq	.LC194(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1091, %ecx
	leaq	.LC195(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1092, %ecx
	leaq	.LC196(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1093, %ecx
	leaq	.LC197(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1094, %ecx
	leaq	.LC198(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1095, %ecx
	leaq	.LC199(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1096, %ecx
	leaq	.LC200(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1097, %ecx
	leaq	.LC201(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1098, %ecx
	leaq	.LC202(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1099, %ecx
	leaq	.LC203(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1100, %ecx
	leaq	.LC204(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1101, %ecx
	leaq	.LC205(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1102, %ecx
	leaq	.LC206(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1103, %ecx
	leaq	.LC207(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1104, %ecx
	leaq	.LC208(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1105, %ecx
	leaq	.LC209(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1106, %ecx
	leaq	.LC210(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1107, %ecx
	leaq	.LC211(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1108, %ecx
	leaq	.LC212(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1109, %ecx
	leaq	.LC213(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1110, %ecx
	leaq	.LC214(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1111, %ecx
	leaq	.LC215(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1112, %ecx
	leaq	.LC216(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1113, %ecx
	leaq	.LC217(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1114, %ecx
	leaq	.LC218(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1115, %ecx
	leaq	.LC219(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1116, %ecx
	leaq	.LC220(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1117, %ecx
	leaq	.LC221(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1118, %ecx
	leaq	.LC222(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1119, %ecx
	leaq	.LC223(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1120, %ecx
	leaq	.LC224(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1121, %ecx
	leaq	.LC225(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1122, %ecx
	leaq	.LC226(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1123, %ecx
	leaq	.LC227(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1124, %ecx
	leaq	.LC228(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1125, %ecx
	leaq	.LC229(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1126, %ecx
	leaq	.LC230(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1127, %ecx
	leaq	.LC231(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1128, %ecx
	leaq	.LC232(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1129, %ecx
	leaq	.LC233(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1130, %ecx
	leaq	.LC234(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1131, %ecx
	leaq	.LC235(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1132, %ecx
	leaq	.LC236(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1133, %ecx
	leaq	.LC237(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1134, %ecx
	leaq	.LC238(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1135, %ecx
	leaq	.LC239(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1136, %ecx
	leaq	.LC240(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1137, %ecx
	leaq	.LC241(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1138, %ecx
	leaq	.LC242(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1139, %ecx
	leaq	.LC243(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1140, %ecx
	leaq	.LC244(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1141, %ecx
	leaq	.LC245(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1142, %ecx
	leaq	.LC246(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1143, %ecx
	leaq	.LC247(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1144, %ecx
	leaq	.LC248(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1145, %ecx
	leaq	.LC249(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1146, %ecx
	leaq	.LC250(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1147, %ecx
	leaq	.LC251(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1148, %ecx
	leaq	.LC252(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1149, %ecx
	leaq	.LC253(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1150, %ecx
	leaq	.LC254(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1151, %ecx
	leaq	.LC255(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1152, %ecx
	leaq	.LC256(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1153, %ecx
	leaq	.LC257(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1154, %ecx
	leaq	.LC258(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1155, %ecx
	leaq	.LC259(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1156, %ecx
	leaq	.LC260(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1157, %ecx
	leaq	.LC261(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1158, %ecx
	leaq	.LC262(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1159, %ecx
	leaq	.LC263(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1160, %ecx
	leaq	.LC264(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1161, %ecx
	leaq	.LC265(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1162, %ecx
	leaq	.LC266(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1163, %ecx
	leaq	.LC267(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1164, %ecx
	leaq	.LC268(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1165, %ecx
	leaq	.LC269(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1166, %ecx
	leaq	.LC270(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1167, %ecx
	leaq	.LC271(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1168, %ecx
	leaq	.LC272(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1169, %ecx
	leaq	.LC273(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1170, %ecx
	leaq	.LC274(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1171, %ecx
	leaq	.LC275(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1172, %ecx
	leaq	.LC276(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1173, %ecx
	leaq	.LC277(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1174, %ecx
	leaq	.LC278(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1175, %ecx
	leaq	.LC279(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1176, %ecx
	leaq	.LC280(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1177, %ecx
	leaq	.LC281(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1178, %ecx
	leaq	.LC282(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1179, %ecx
	leaq	.LC283(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1180, %ecx
	leaq	.LC284(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1181, %ecx
	leaq	.LC285(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1182, %ecx
	leaq	.LC286(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1183, %ecx
	leaq	.LC287(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1184, %ecx
	leaq	.LC288(%rip), %rdx
	call	_ZN2v88internal11ObjectStats20DumpInstanceTypeDataERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEEPKci
	addq	$24, %rsp
	movq	%r14, %rdi
	movl	$10, %edx
	popq	%rbx
	leaq	.LC315(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.cfi_endproc
.LFE21660:
	.size	_ZN2v88internal11ObjectStats4DumpERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal11ObjectStats4DumpERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal11ObjectStats21CheckpointObjectStatsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ObjectStats21CheckpointObjectStatsEv
	.type	_ZN2v88internal11ObjectStats21CheckpointObjectStatsEv, @function
_ZN2v88internal11ObjectStats21CheckpointObjectStatsEv:
.LFB21661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	_ZN2v88internalL18object_stats_mutexE(%rip), %eax
	cmpb	$2, %al
	jne	.L113
.L103:
	leaq	8+_ZN2v88internalL18object_stats_mutexE(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	8(%r12), %rsi
	movl	$9480, %edx
	leaq	9488(%r12), %rdi
	call	memcpy@PLT
	movl	$9480, %edx
	leaq	28448(%r12), %rdi
	leaq	18968(%r12), %rsi
	call	memcpy@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11ObjectStats16ClearObjectStatsEb
	leaq	8+_ZN2v88internalL18object_stats_mutexE(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internalL18object_stats_mutexE(%rip), %rax
	leaq	-64(%rbp), %r13
	movq	%rax, -56(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r13, %rsi
	leaq	_ZN2v88internalL18object_stats_mutexE(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L103
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L103
.L114:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21661:
	.size	_ZN2v88internal11ObjectStats21CheckpointObjectStatsEv, .-_ZN2v88internal11ObjectStats21CheckpointObjectStatsEv
	.section	.text._ZN2v88internal11ObjectStats22HistogramIndexFromSizeEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ObjectStats22HistogramIndexFromSizeEm
	.type	_ZN2v88internal11ObjectStats22HistogramIndexFromSizeEm, @function
_ZN2v88internal11ObjectStats22HistogramIndexFromSizeEm:
.LFB21663:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L115
	bsrq	%rsi, %rsi
	movl	$59, %eax
	movl	$0, %edx
	xorq	$63, %rsi
	subl	%esi, %eax
	cmovs	%edx, %eax
	movl	$15, %edx
	cmpl	$15, %eax
	cmovg	%edx, %eax
.L115:
	ret
	.cfi_endproc
.LFE21663:
	.size	_ZN2v88internal11ObjectStats22HistogramIndexFromSizeEm, .-_ZN2v88internal11ObjectStats22HistogramIndexFromSizeEm
	.section	.text._ZN2v88internal11ObjectStats17RecordObjectStatsENS0_12InstanceTypeEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ObjectStats17RecordObjectStatsENS0_12InstanceTypeEmm
	.type	_ZN2v88internal11ObjectStats17RecordObjectStatsENS0_12InstanceTypeEmm, @function
_ZN2v88internal11ObjectStats17RecordObjectStatsENS0_12InstanceTypeEmm:
.LFB21664:
	.cfi_startproc
	endbr64
	movzwl	%si, %esi
	xorl	%eax, %eax
	leaq	(%rdi,%rsi,8), %r8
	addq	$1, 8(%r8)
	addq	%rdx, 18968(%r8)
	testq	%rdx, %rdx
	je	.L120
	bsrq	%rdx, %rdx
	movl	$59, %eax
	xorq	$63, %rdx
	subl	%edx, %eax
	movl	$0, %edx
	cmovs	%edx, %eax
	movl	$15, %edx
	cmpl	$15, %eax
	cmovg	%edx, %eax
.L120:
	salq	$4, %rsi
	cltq
	addq	%rsi, %rax
	leaq	(%rdi,%rax,8), %rax
	addq	$1, 47408(%rax)
	addq	%rcx, 37928(%r8)
	addq	$1, 199088(%rax)
	ret
	.cfi_endproc
.LFE21664:
	.size	_ZN2v88internal11ObjectStats17RecordObjectStatsENS0_12InstanceTypeEmm, .-_ZN2v88internal11ObjectStats17RecordObjectStatsENS0_12InstanceTypeEmm
	.section	.text._ZN2v88internal11ObjectStats24RecordVirtualObjectStatsENS1_19VirtualInstanceTypeEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ObjectStats24RecordVirtualObjectStatsENS1_19VirtualInstanceTypeEmm
	.type	_ZN2v88internal11ObjectStats24RecordVirtualObjectStatsENS1_19VirtualInstanceTypeEmm, @function
_ZN2v88internal11ObjectStats24RecordVirtualObjectStatsENS1_19VirtualInstanceTypeEmm:
.LFB21665:
	.cfi_startproc
	endbr64
	leal	1106(%rsi), %r8d
	movslq	%esi, %rsi
	xorl	%eax, %eax
	leaq	(%rdi,%rsi,8), %rsi
	addq	$1, 8856(%rsi)
	addq	%rdx, 27816(%rsi)
	testq	%rdx, %rdx
	je	.L124
	bsrq	%rdx, %rdx
	movl	$59, %eax
	xorq	$63, %rdx
	subl	%edx, %eax
	movl	$0, %edx
	cmovs	%edx, %eax
	movl	$15, %edx
	cmpl	$15, %eax
	cmovg	%edx, %eax
.L124:
	movslq	%r8d, %rdx
	cltq
	salq	$4, %rdx
	addq	%rdx, %rax
	leaq	(%rdi,%rax,8), %rax
	addq	$1, 47408(%rax)
	addq	%rcx, 46776(%rsi)
	addq	$1, 199088(%rax)
	ret
	.cfi_endproc
.LFE21665:
	.size	_ZN2v88internal11ObjectStats24RecordVirtualObjectStatsENS1_19VirtualInstanceTypeEmm, .-_ZN2v88internal11ObjectStats24RecordVirtualObjectStatsENS1_19VirtualInstanceTypeEmm
	.section	.text._ZN2v88internal11ObjectStats7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ObjectStats7isolateEv
	.type	_ZN2v88internal11ObjectStats7isolateEv, @function
_ZN2v88internal11ObjectStats7isolateEv:
.LFB21666:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	subq	$37592, %rax
	ret
	.cfi_endproc
.LFE21666:
	.size	_ZN2v88internal11ObjectStats7isolateEv, .-_ZN2v88internal11ObjectStats7isolateEv
	.section	.text._ZN2v88internal24ObjectStatsCollectorImplC2EPNS0_4HeapEPNS0_11ObjectStatsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImplC2EPNS0_4HeapEPNS0_11ObjectStatsE
	.type	_ZN2v88internal24ObjectStatsCollectorImplC2EPNS0_4HeapEPNS0_11ObjectStatsE, @function
_ZN2v88internal24ObjectStatsCollectorImplC2EPNS0_4HeapEPNS0_11ObjectStatsE:
.LFB21691:
	.cfi_startproc
	endbr64
	movq	2016(%rsi), %rax
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	movq	$1, 32(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 40(%rdi)
	addq	$9993, %rax
	movups	%xmm0, (%rdi)
	movq	%rax, %xmm0
	leaq	72(%rdi), %rax
	movq	$0, 48(%rdi)
	movq	%rax, %xmm2
	leaq	128(%rdi), %rax
	movq	$0, 64(%rdi)
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVN2v88internal19FieldStatsCollectorE(%rip), %rax
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 136(%rdi)
	leaq	192(%rdi), %rax
	movq	%rax, 144(%rdi)
	leaq	350768(%rdx), %rax
	movq	%rax, 200(%rdi)
	leaq	350776(%rdx), %rax
	movq	%rax, 208(%rdi)
	leaq	350784(%rdx), %rax
	movq	%rax, 216(%rdi)
	leaq	350792(%rdx), %rax
	movq	%rax, 224(%rdi)
	leaq	350800(%rdx), %rax
	movups	%xmm0, 16(%rdi)
	movss	.LC316(%rip), %xmm0
	movq	%rax, 232(%rdi)
	leaq	350808(%rdx), %rax
	addq	$350816, %rdx
	movq	$0, 72(%rdi)
	movq	$1, 88(%rdi)
	movq	$0, 96(%rdi)
	movq	$0, 104(%rdi)
	movq	$0, 120(%rdi)
	movq	$0, 128(%rdi)
	movq	$1, 152(%rdi)
	movq	$0, 160(%rdi)
	movq	$0, 168(%rdi)
	movq	$0, 184(%rdi)
	movq	$0, 192(%rdi)
	movq	%rax, 240(%rdi)
	movq	%rdx, 248(%rdi)
	movss	%xmm0, 56(%rdi)
	movss	%xmm0, 112(%rdi)
	movss	%xmm0, 176(%rdi)
	ret
	.cfi_endproc
.LFE21691:
	.size	_ZN2v88internal24ObjectStatsCollectorImplC2EPNS0_4HeapEPNS0_11ObjectStatsE, .-_ZN2v88internal24ObjectStatsCollectorImplC2EPNS0_4HeapEPNS0_11ObjectStatsE
	.globl	_ZN2v88internal24ObjectStatsCollectorImplC1EPNS0_4HeapEPNS0_11ObjectStatsE
	.set	_ZN2v88internal24ObjectStatsCollectorImplC1EPNS0_4HeapEPNS0_11ObjectStatsE,_ZN2v88internal24ObjectStatsCollectorImplC2EPNS0_4HeapEPNS0_11ObjectStatsE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl18ShouldRecordObjectENS0_10HeapObjectENS1_7CowModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl18ShouldRecordObjectENS0_10HeapObjectENS1_7CowModeE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl18ShouldRecordObjectENS0_10HeapObjectENS1_7CowModeE, @function
_ZN2v88internal24ObjectStatsCollectorImpl18ShouldRecordObjectENS0_10HeapObjectENS1_7CowModeE:
.LFB21693:
	.cfi_startproc
	endbr64
	movq	-1(%rsi), %rax
	cmpw	$123, 11(%rax)
	je	.L137
	movq	(%rdi), %rax
	cmpq	%rsi, -36624(%rax)
	setne	%al
.L129:
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	movq	(%rdi), %rcx
	movl	$1, %eax
	subq	$37592, %rcx
	cmpl	$1, %edx
	jne	.L138
.L131:
	cmpq	288(%rcx), %rsi
	je	.L134
	cmpq	1008(%rcx), %rsi
	je	.L134
	cmpq	1016(%rcx), %rsi
	je	.L134
	cmpq	1056(%rcx), %rsi
	jne	.L129
.L134:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	movq	-1(%rsi), %rax
	cmpq	%rax, 160(%rcx)
	setne	%al
	jmp	.L131
	.cfi_endproc
.LFE21693:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl18ShouldRecordObjectENS0_10HeapObjectENS1_7CowModeE, .-_ZN2v88internal24ObjectStatsCollectorImpl18ShouldRecordObjectENS0_10HeapObjectENS1_7CowModeE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl17RecordObjectStatsENS0_10HeapObjectENS0_12InstanceTypeEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl17RecordObjectStatsENS0_10HeapObjectENS0_12InstanceTypeEmm
	.type	_ZN2v88internal24ObjectStatsCollectorImpl17RecordObjectStatsENS0_10HeapObjectENS0_12InstanceTypeEmm, @function
_ZN2v88internal24ObjectStatsCollectorImpl17RecordObjectStatsENS0_10HeapObjectENS0_12InstanceTypeEmm:
.LFB21707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	xorl	%edx, %edx
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rdi), %r10
	divq	%r10
	movq	24(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L140
	movq	(%rax), %r9
	movq	%rdx, %rbx
	movq	16(%r9), %r11
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L141:
	movq	(%r9), %r9
	testq	%r9, %r9
	je	.L140
	movq	16(%r9), %r11
	xorl	%edx, %edx
	movq	%r11, %rax
	divq	%r10
	cmpq	%rdx, %rbx
	jne	.L140
.L143:
	cmpq	%r11, %rsi
	jne	.L141
	cmpq	8(%r9), %rsi
	jne	.L141
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movzwl	%r12w, %edx
	xorl	%esi, %esi
	leaq	(%rdi,%rdx,8), %rax
	addq	$1, 8(%rax)
	addq	%rcx, 18968(%rax)
	testq	%rcx, %rcx
	je	.L144
	bsrq	%rcx, %rcx
	movl	$59, %esi
	xorq	$63, %rcx
	subl	%ecx, %esi
	movl	$0, %ecx
	cmovs	%ecx, %esi
	movl	$15, %ecx
	cmpl	$15, %esi
	cmovg	%ecx, %esi
.L144:
	salq	$4, %rdx
	movslq	%esi, %rsi
	addq	%rsi, %rdx
	leaq	(%rdi,%rdx,8), %rdx
	addq	$1, 47408(%rdx)
	addq	%r8, 37928(%rax)
	addq	$1, 199088(%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21707:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl17RecordObjectStatsENS0_10HeapObjectENS0_12InstanceTypeEmm, .-_ZN2v88internal24ObjectStatsCollectorImpl17RecordObjectStatsENS0_10HeapObjectENS0_12InstanceTypeEmm
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl19CanRecordFixedArrayENS0_14FixedArrayBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl19CanRecordFixedArrayENS0_14FixedArrayBaseE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl19CanRecordFixedArrayENS0_14FixedArrayBaseE, @function
_ZN2v88internal24ObjectStatsCollectorImpl19CanRecordFixedArrayENS0_14FixedArrayBaseE:
.LFB21708:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	xorl	%r8d, %r8d
	cmpq	%rsi, -37304(%rax)
	je	.L155
	cmpq	%rsi, -36584(%rax)
	je	.L155
	cmpq	%rsi, -36576(%rax)
	je	.L155
	cmpq	%rsi, -36536(%rax)
	setne	%r8b
.L155:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE21708:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl19CanRecordFixedArrayENS0_14FixedArrayBaseE, .-_ZN2v88internal24ObjectStatsCollectorImpl19CanRecordFixedArrayENS0_14FixedArrayBaseE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl10IsCowArrayENS0_14FixedArrayBaseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl10IsCowArrayENS0_14FixedArrayBaseE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl10IsCowArrayENS0_14FixedArrayBaseE, @function
_ZN2v88internal24ObjectStatsCollectorImpl10IsCowArrayENS0_14FixedArrayBaseE:
.LFB21709:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	-1(%rsi), %rax
	cmpq	%rax, -37432(%rdx)
	sete	%al
	ret
	.cfi_endproc
.LFE21709:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl10IsCowArrayENS0_14FixedArrayBaseE, .-_ZN2v88internal24ObjectStatsCollectorImpl10IsCowArrayENS0_14FixedArrayBaseE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl12SameLivenessENS0_10HeapObjectES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl12SameLivenessENS0_10HeapObjectES2_
	.type	_ZN2v88internal24ObjectStatsCollectorImpl12SameLivenessENS0_10HeapObjectES2_, @function
_ZN2v88internal24ObjectStatsCollectorImpl12SameLivenessENS0_10HeapObjectES2_:
.LFB21710:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	testq	%rsi, %rsi
	je	.L161
	testq	%rdx, %rdx
	jne	.L178
.L161:
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%rsi, %r8
	andl	$262143, %esi
	movl	$1, %edi
	movl	%esi, %ecx
	andq	$-262144, %r8
	shrl	$8, %esi
	shrl	$3, %ecx
	sall	%cl, %edi
	movl	%edi, %ecx
	movq	16(%r8), %rdi
	xorl	%r8d, %r8d
	leaq	(%rdi,%rsi,4), %r9
	xorl	%edi, %edi
	movl	(%r9), %esi
	testl	%esi, %ecx
	jne	.L179
.L163:
	movq	%rdx, %r9
	andl	$262143, %edx
	movl	$1, %esi
	movl	%edx, %ecx
	andq	$-262144, %r9
	shrl	$8, %edx
	shrl	$3, %ecx
	sall	%cl, %esi
	movl	%esi, %ecx
	movq	16(%r9), %rsi
	leaq	(%rsi,%rdx,4), %rsi
	movl	(%rsi), %edx
	testl	%edx, %ecx
	je	.L161
	addl	%ecx, %ecx
	jne	.L166
	testb	$1, 4(%rsi)
	movl	%r8d, %eax
	cmovne	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	addl	%ecx, %ecx
	jne	.L164
	movl	4(%r9), %esi
	movl	$1, %ecx
.L164:
	andl	%esi, %ecx
	movl	$0, %eax
	cmpl	$1, %ecx
	setb	%r8b
	sbbl	%edi, %edi
	addl	$1, %edi
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L166:
	testl	%ecx, %edx
	movl	%r8d, %eax
	cmovne	%edi, %eax
	ret
	.cfi_endproc
.LFE21710:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl12SameLivenessENS0_10HeapObjectES2_, .-_ZN2v88internal24ObjectStatsCollectorImpl12SameLivenessENS0_10HeapObjectES2_
	.section	.text._ZNSt10_HashtableIN2v88internal3MapESt4pairIKS2_NS1_19FieldStatsCollector18JSObjectFieldStatsEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_6Object6HasherENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS3_IS2_S6_EEEES3_INS9_14_Node_iteratorIS7_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIN2v88internal3MapESt4pairIKS2_NS1_19FieldStatsCollector18JSObjectFieldStatsEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_6Object6HasherENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS3_IS2_S6_EEEES3_INS9_14_Node_iteratorIS7_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal3MapESt4pairIKS2_NS1_19FieldStatsCollector18JSObjectFieldStatsEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_6Object6HasherENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS3_IS2_S6_EEEES3_INS9_14_Node_iteratorIS7_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIN2v88internal3MapESt4pairIKS2_NS1_19FieldStatsCollector18JSObjectFieldStatsEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_6Object6HasherENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS3_IS2_S6_EEEES3_INS9_14_Node_iteratorIS7_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIN2v88internal3MapESt4pairIKS2_NS1_19FieldStatsCollector18JSObjectFieldStatsEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_6Object6HasherENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS3_IS2_S6_EEEES3_INS9_14_Node_iteratorIS7_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB26096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$32, %edi
	subq	$24, %rsp
	call	_Znwm@PLT
	movq	(%r15), %r14
	movq	8(%rbx), %rsi
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %r13
	movq	%r14, 8(%rax)
	movl	8(%r15), %eax
	movl	%eax, 16(%r13)
	movq	%r14, %rax
	divq	%rsi
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r15
	testq	%rax, %rax
	je	.L181
	movq	(%rax), %r12
	movq	%rdx, %rdi
	movq	24(%r12), %rcx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L182:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L181
	movq	24(%r12), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %rdi
	jne	.L181
.L184:
	cmpq	%rcx, %r14
	jne	.L182
	cmpq	8(%r12), %r14
	jne	.L182
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movq	24(%rbx), %rdx
	leaq	32(%rbx), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r12
	testb	%al, %al
	jne	.L185
	movq	(%rbx), %r8
	movq	%r14, 24(%r13)
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L195
.L220:
	movq	(%rdx), %rdx
	movq	%rdx, 0(%r13)
	movq	(%rax), %rax
	movq	%r13, (%rax)
.L196:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L218
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L219
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L188:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L190
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L192:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L193:
	testq	%rsi, %rsi
	je	.L190
.L191:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	24(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L192
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L199
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L191
	.p2align 4,,10
	.p2align 3
.L190:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L194
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L194:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%r14, 24(%r13)
	leaq	0(,%rdx,8), %r15
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L220
.L195:
	movq	16(%rbx), %rdx
	movq	%r13, 16(%rbx)
	movq	%rdx, 0(%r13)
	testq	%rdx, %rdx
	je	.L197
	movq	24(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r8,%rdx,8)
	movq	(%rbx), %rax
	addq	%r15, %rax
.L197:
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax)
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L199:
	movq	%rdx, %rdi
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L218:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L188
.L219:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE26096:
	.size	_ZNSt10_HashtableIN2v88internal3MapESt4pairIKS2_NS1_19FieldStatsCollector18JSObjectFieldStatsEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_6Object6HasherENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS3_IS2_S6_EEEES3_INS9_14_Node_iteratorIS7_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIN2v88internal3MapESt4pairIKS2_NS1_19FieldStatsCollector18JSObjectFieldStatsEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_6Object6HasherENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS3_IS2_S6_EEEES3_INS9_14_Node_iteratorIS7_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.rodata._ZN2v88internal19FieldStatsCollector21GetInobjectFieldStatsENS0_3MapE.str1.1,"aMS",@progbits,1
.LC317:
	.string	"unreachable code"
	.section	.rodata._ZN2v88internal19FieldStatsCollector21GetInobjectFieldStatsENS0_3MapE.str1.8,"aMS",@progbits,1
	.align 8
.LC318:
	.string	"(!IsSmi() && (*layout_word_index < length())) || (IsSmi() && (*layout_word_index < 1))"
	.section	.rodata._ZN2v88internal19FieldStatsCollector21GetInobjectFieldStatsENS0_3MapE.str1.1
.LC319:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal19FieldStatsCollector21GetInobjectFieldStatsENS0_3MapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19FieldStatsCollector21GetInobjectFieldStatsENS0_3MapE
	.type	_ZN2v88internal19FieldStatsCollector21GetInobjectFieldStatsENS0_3MapE, @function
_ZN2v88internal19FieldStatsCollector21GetInobjectFieldStatsENS0_3MapE:
.LFB21633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, %rax
	divq	%rsi
	movq	8(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L222
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movq	24(%rcx), %rdi
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L223:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L222
	movq	24(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L222
.L225:
	cmpq	%rdi, %rbx
	jne	.L223
	cmpq	8(%rcx), %rbx
	jne	.L223
	movl	16(%rcx), %eax
.L247:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L278
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	movzbl	7(%rbx), %eax
	xorl	%r13d, %r13d
	leaq	7(%rbx), %r14
	sall	$3, %eax
	movl	%eax, %r15d
	je	.L226
	movzwl	11(%rbx), %edi
	movl	$24, %ecx
	cmpw	$1057, %di
	je	.L227
	movsbl	13(%rbx), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
	movl	%eax, %ecx
.L227:
	movzbl	7(%rbx), %eax
	subl	%ecx, %r15d
	movzbl	8(%rbx), %edx
	movl	%r15d, %r13d
	sarl	$3, %r13d
	subl	%edx, %eax
	subl	%eax, %r13d
	andw	$1023, %r13w
.L226:
	movl	15(%rbx), %eax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movw	%dx, -82(%rbp)
	testl	$2097152, %eax
	je	.L279
.L228:
	movzwl	-72(%rbp), %eax
	movzwl	-82(%rbp), %r14d
	movq	%rbx, -80(%rbp)
	leaq	8(%r12), %rdi
	andw	$-1024, %ax
	orl	%r13d, %eax
	movw	%ax, -72(%rbp)
	movl	%esi, %eax
	movl	-72(%rbp), %edx
	leaq	-80(%rbp), %rsi
	andl	$1023, %eax
	sall	$10, %eax
	andl	$-1047553, %edx
	orl	%eax, %edx
	movl	%eax, %ebx
	movl	%r14d, %eax
	movl	%edx, -72(%rbp)
	andw	$1023, %ax
	shrl	$16, %edx
	sall	$4, %eax
	andw	$-16369, %dx
	orl	%eax, %edx
	movw	%dx, -70(%rbp)
	call	_ZNSt10_HashtableIN2v88internal3MapESt4pairIKS2_NS1_19FieldStatsCollector18JSObjectFieldStatsEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS2_ENS1_6Object6HasherENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS3_IS2_S6_EEEES3_INS9_14_Node_iteratorIS7_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	movl	%r13d, %eax
	movl	%r14d, %r13d
	andl	$1023, %eax
	andl	$1023, %r13d
	sall	$20, %r13d
	orl	%ebx, %eax
	orl	%r13d, %eax
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L279:
	movl	15(%rbx), %eax
	leaq	39(%rbx), %rdx
	movq	39(%rbx), %r10
	shrl	$10, %eax
	andl	$1023, %eax
	je	.L252
	subl	$1, %eax
	movw	%r13w, -84(%rbp)
	movl	$31, %esi
	movq	%rdx, %r13
	leaq	(%rax,%rax,2), %rax
	movq	%r12, -96(%rbp)
	leaq	55(,%rax,8), %r11
	xorl	%eax, %eax
	movl	%eax, %r12d
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L231:
	addq	$24, %rsi
	cmpq	%r11, %rsi
	je	.L276
.L230:
	movq	(%r10,%rsi), %rdx
	sarq	$32, %rdx
	testb	$2, %dl
	jne	.L231
	movq	0(%r13), %rax
	movq	(%rax,%rsi), %rcx
	movq	%rcx, %rax
	shrq	$38, %rcx
	shrq	$51, %rax
	andl	$7, %ecx
	movq	%rax, %rdi
	movzbl	(%r14), %eax
	movzbl	8(%rbx), %r8d
	andl	$1023, %edi
	subl	%r8d, %eax
	cmpl	%eax, %edi
	setl	%r9b
	jl	.L280
	subl	%eax, %edi
	movl	$16, %r8d
	leal	16(,%rdi,8), %edi
.L233:
	cmpl	$2, %ecx
	jne	.L281
	movl	$32768, %ecx
.L234:
	movzbl	%r9b, %r9d
	cltq
	movslq	%edi, %rdi
	movslq	%r8d, %r8
	salq	$14, %r9
	salq	$17, %rax
	orq	%r9, %rax
	salq	$27, %r8
	orq	%rdi, %rax
	orq	%r8, %rax
	orq	%rax, %rcx
	testb	$64, %ah
	je	.L276
	shrl	$6, %edx
	andl	$7, %edx
	cmpl	$2, %edx
	je	.L282
	cmpl	$1, %edx
	jne	.L231
	addl	$1, %r12d
	andw	$1023, %r12w
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L281:
	cmpb	$2, %cl
	jg	.L235
	je	.L236
.L254:
	xorl	%ecx, %ecx
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L235:
	subl	$3, %ecx
	cmpb	$1, %cl
	jbe	.L254
.L236:
	leaq	.LC317(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L280:
	movzbl	8(%rbx), %r8d
	movzbl	8(%rbx), %r15d
	addl	%r15d, %edi
	sall	$3, %r8d
	sall	$3, %edi
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L276:
	movl	%r12d, %esi
	movzwl	-84(%rbp), %r13d
	movq	-96(%rbp), %r12
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L282:
	movq	47(%rbx), %rax
	testq	%rax, %rax
	je	.L231
	movq	%rax, %rdx
	movl	$32, %r8d
	notq	%rdx
	movl	%edx, %edi
	andl	$1, %edi
	jne	.L239
	movslq	11(%rax), %r8
	sall	$3, %r8d
.L239:
	movq	%rcx, %r9
	sarl	$3, %ecx
	shrq	$30, %r9
	andl	$2047, %ecx
	andl	$15, %r9d
	subl	%r9d, %ecx
	cmpl	%r8d, %ecx
	jnb	.L231
	testl	%ecx, %ecx
	leal	31(%rcx), %r8d
	cmovns	%ecx, %r8d
	sarl	$5, %r8d
	andl	$1, %edx
	jne	.L240
	cmpl	%r8d, 11(%rax)
	jle	.L240
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$27, %edx
	addl	%edx, %ecx
	andl	$31, %ecx
	subl	%edx, %ecx
	movl	$1, %edx
	sall	%cl, %edx
	movl	%edx, %ecx
	testb	%dil, %dil
	je	.L283
.L244:
	sarq	$32, %rax
	testl	%eax, %ecx
	sete	%al
.L246:
	testb	%al, %al
	jne	.L231
	addw	$1, -82(%rbp)
	movzwl	-82(%rbp), %eax
	andw	$1023, %ax
	movw	%ax, -82(%rbp)
	jmp	.L231
.L252:
	xorl	%eax, %eax
	movl	%eax, %esi
	jmp	.L228
.L283:
	leal	0(,%r8,4), %edx
	movslq	%edx, %rdx
	movl	15(%rax,%rdx), %eax
	testl	%eax, %ecx
	sete	%al
	jmp	.L246
.L240:
	cmpl	$31, %ecx
	jg	.L242
	testb	%dil, %dil
	je	.L242
	movl	$1, %edx
	sall	%cl, %edx
	movl	%edx, %ecx
	jmp	.L244
.L278:
	call	__stack_chk_fail@PLT
.L242:
	leaq	.LC318(%rip), %rsi
	leaq	.LC319(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21633:
	.size	_ZN2v88internal19FieldStatsCollector21GetInobjectFieldStatsENS0_3MapE, .-_ZN2v88internal19FieldStatsCollector21GetInobjectFieldStatsENS0_3MapE
	.section	.text._ZNSt10_HashtableIN2v88internal10HeapObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal10HeapObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal10HeapObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.type	_ZNSt10_HashtableIN2v88internal10HeapObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, @function
_ZNSt10_HashtableIN2v88internal10HeapObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm:
.LFB26112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %r13
	movq	8(%rdi), %r8
	movq	%r13, %rax
	divq	%r8
	movq	(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r15
	testq	%rax, %rax
	je	.L285
	movq	(%rax), %rdi
	movq	%rdx, %rsi
	movq	16(%rdi), %r9
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L286:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L285
	movq	16(%rdi), %r9
	xorl	%edx, %edx
	movq	%r9, %rax
	divq	%r8
	cmpq	%rdx, %rsi
	jne	.L285
.L288:
	cmpq	%r9, %r13
	jne	.L286
	cmpq	8(%rdi), %r13
	jne	.L286
	addq	$24, %rsp
	movq	%rdi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	movl	$24, %edi
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	24(%r12), %rdx
	movq	8(%r12), %rsi
	leaq	32(%r12), %rdi
	movq	$0, (%rax)
	movq	%rax, %rbx
	movq	-56(%rbp), %rcx
	movq	(%r14), %rax
	movq	%rax, 8(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L289
	movq	(%r12), %r8
	movq	%r13, 16(%rbx)
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L299
.L324:
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	(%rax), %rax
	movq	%rbx, (%rax)
.L300:
	addq	$1, 24(%r12)
	addq	$24, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L322
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L323
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r12), %r10
	movq	%rax, %r8
.L292:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L294
	xorl	%edi, %edi
	leaq	16(%r12), %r9
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L296:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L297:
	testq	%rsi, %rsi
	je	.L294
.L295:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	16(%rcx), %rax
	divq	%r14
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L296
	movq	16(%r12), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L303
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L295
	.p2align 4,,10
	.p2align 3
.L294:
	movq	(%r12), %rdi
	cmpq	%r10, %rdi
	je	.L298
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L298:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r14, 8(%r12)
	divq	%r14
	movq	%r8, (%r12)
	movq	%r13, 16(%rbx)
	leaq	0(,%rdx,8), %r15
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L324
.L299:
	movq	16(%r12), %rdx
	movq	%rbx, 16(%r12)
	movq	%rdx, (%rbx)
	testq	%rdx, %rdx
	je	.L301
	movq	16(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	%rbx, (%r8,%rdx,8)
	movq	(%r12), %rax
	addq	%r15, %rax
.L301:
	leaq	16(%r12), %rdx
	movq	%rdx, (%rax)
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L303:
	movq	%rdx, %rdi
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L322:
	movq	$0, 48(%r12)
	leaq	48(%r12), %r8
	movq	%r8, %r10
	jmp	.L292
.L323:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE26112:
	.size	_ZNSt10_HashtableIN2v88internal10HeapObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, .-_ZNSt10_HashtableIN2v88internal10HeapObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.section	.rodata._ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE.str1.1,"aMS",@progbits,1
.LC320:
	.string	"over_allocated < size"
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE, @function
_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE:
.LFB21696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r8, %r9
	jnb	.L360
	movq	%rdi, %r12
	movslq	%ecx, %rbx
	movq	%r8, %r14
	movq	%r9, %r15
	testq	%rsi, %rsi
	jne	.L327
.L330:
	movl	16(%rbp), %edx
	movq	-72(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl18ShouldRecordObjectENS0_10HeapObjectENS1_7CowModeE
	movl	%eax, %r13d
	testb	%al, %al
	jne	.L361
.L329:
	xorl	%r13d, %r13d
.L325:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L362
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L330
	movq	%rsi, %rdi
	andl	$262143, %esi
	movl	$1, %edx
	movl	%esi, %ecx
	andq	$-262144, %rdi
	shrl	$8, %esi
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%rdi), %rcx
	leaq	(%rcx,%rsi,4), %rdi
	movl	$1, %esi
	movl	(%rdi), %ecx
	testl	%ecx, %edx
	jne	.L363
.L331:
	movq	%rax, %rdi
	andl	$262143, %eax
	movl	$1, %edx
	movl	%eax, %ecx
	andq	$-262144, %rdi
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%rdi), %rcx
	leaq	(%rcx,%rax,4), %rdi
	movl	$1, %eax
	movl	(%rdi), %ecx
	testl	%ecx, %edx
	jne	.L364
.L333:
	cmpl	%eax, %esi
	jne	.L329
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L361:
	movq	-72(%rbp), %r8
	movq	32(%r12), %rdi
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	movq	24(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L336
	movq	(%rax), %rcx
	movq	16(%rcx), %rsi
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L337:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L336
	movq	16(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L336
.L338:
	cmpq	%rsi, %r8
	jne	.L337
	cmpq	8(%rcx), %r8
	jne	.L337
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L360:
	leaq	.LC320(%rip), %rsi
	leaq	.LC319(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L336:
	leaq	24(%r12), %rdi
	leaq	-72(%rbp), %rsi
	movl	$1, %ecx
	leaq	-64(%rbp), %rdx
	movq	%rdi, -64(%rbp)
	call	_ZNSt10_HashtableIN2v88internal10HeapObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	movq	8(%r12), %rdi
	leal	1106(%rbx), %esi
	xorl	%edx, %edx
	leaq	(%rdi,%rbx,8), %rax
	addq	$1, 8856(%rax)
	addq	%r14, 27816(%rax)
	testq	%r14, %r14
	je	.L339
	bsrq	%r14, %r14
	movl	$59, %edx
	movl	$0, %ecx
	xorq	$63, %r14
	subl	%r14d, %edx
	cmovs	%ecx, %edx
	movl	$15, %ecx
	cmpl	$15, %edx
	cmovg	%ecx, %edx
.L339:
	movslq	%esi, %rsi
	movslq	%edx, %rdx
	salq	$4, %rsi
	addq	%rsi, %rdx
	leaq	(%rdi,%rdx,8), %rdx
	addq	$1, 47408(%rdx)
	addq	%r15, 46776(%rax)
	addq	$1, 199088(%rdx)
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L363:
	addl	%edx, %edx
	jne	.L332
	movl	4(%rdi), %ecx
	movl	$1, %edx
.L332:
	xorl	%esi, %esi
	testl	%ecx, %edx
	sete	%sil
	addl	%esi, %esi
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L364:
	movl	%edx, %eax
	addl	%eax, %eax
	jne	.L334
	testb	$1, 4(%rdi)
	movl	$2, %edx
	cmove	%edx, %eax
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L334:
	andl	%ecx, %eax
	cmpl	$1, %eax
	sbbl	%eax, %eax
	andl	$2, %eax
	jmp	.L333
.L362:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21696:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE, .-_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl30RecordVirtualFixedArrayDetailsENS0_10FixedArrayE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl30RecordVirtualFixedArrayDetailsENS0_10FixedArrayE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl30RecordVirtualFixedArrayDetailsENS0_10FixedArrayE, @function
_ZN2v88internal24ObjectStatsCollectorImpl30RecordVirtualFixedArrayDetailsENS0_10FixedArrayE:
.LFB21704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rcx
	movq	%rsi, -24(%rbp)
	movq	-1(%rsi), %rdx
	cmpq	%rdx, -37432(%rcx)
	je	.L368
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movq	-1(%rsi), %rsi
	movq	%rdi, %r12
	leaq	-24(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-24(%rbp), %rdx
	movq	%r12, %rdi
	pushq	$1
	xorl	%r9d, %r9d
	movslq	%eax, %r8
	movl	$20, %ecx
	xorl	%esi, %esi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rax
	movq	-8(%rbp), %r12
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21704:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl30RecordVirtualFixedArrayDetailsENS0_10FixedArrayE, .-_ZN2v88internal24ObjectStatsCollectorImpl30RecordVirtualFixedArrayDetailsENS0_10FixedArrayE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl38RecordVirtualSharedFunctionInfoDetailsENS0_18SharedFunctionInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl38RecordVirtualSharedFunctionInfoDetailsENS0_18SharedFunctionInfoE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl38RecordVirtualSharedFunctionInfoDetailsENS0_18SharedFunctionInfoE, @function
_ZN2v88internal24ObjectStatsCollectorImpl38RecordVirtualSharedFunctionInfoDetailsENS0_18SharedFunctionInfoE:
.LFB21714:
	.cfi_startproc
	endbr64
	movabsq	$287762808832, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	7(%rsi), %rax
	cmpq	%rdx, %rax
	je	.L373
	testb	$1, %al
	jne	.L377
.L369:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L378
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	jne	.L379
.L373:
	movq	%rsi, -32(%rbp)
	movq	-1(%rsi), %rsi
	leaq	-32(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-32(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	$0
	movslq	%eax, %r8
	movl	$77, %ecx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rax
	popq	%rdx
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L379:
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L369
	jmp	.L373
.L378:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21714:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl38RecordVirtualSharedFunctionInfoDetailsENS0_18SharedFunctionInfoE, .-_ZN2v88internal24ObjectStatsCollectorImpl38RecordVirtualSharedFunctionInfoDetailsENS0_18SharedFunctionInfoE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl20RecordVirtualContextENS0_7ContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl20RecordVirtualContextENS0_7ContextE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl20RecordVirtualContextENS0_7ContextE, @function
_ZN2v88internal24ObjectStatsCollectorImpl20RecordVirtualContextENS0_7ContextE:
.LFB21720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%rsi, -40(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -24(%rbp)
	xorl	%ecx, %ecx
	movq	-1(%rsi), %rdx
	cmpw	$145, 11(%rdx)
	je	.L386
	movq	-1(%rsi), %rdx
	cmpw	$143, 11(%rdx)
	je	.L387
	movq	%rsi, -32(%rbp)
	movq	-1(%rsi), %rsi
	leaq	-32(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-32(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	$0
	movslq	%eax, %r8
	movl	$57, %ecx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rax
	popq	%rdx
.L380:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L388
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L387:
	.cfi_restore_state
	movq	-1(%rsi), %rsi
	leaq	-40(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-40(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movslq	%eax, %rcx
	movl	$143, %edx
	call	_ZN2v88internal24ObjectStatsCollectorImpl17RecordObjectStatsENS0_10HeapObjectENS0_12InstanceTypeEmm
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L386:
	movq	-1(%rsi), %rsi
	leaq	-40(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-40(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movslq	%eax, %rcx
	movl	$145, %edx
	call	_ZN2v88internal24ObjectStatsCollectorImpl17RecordObjectStatsENS0_10HeapObjectENS0_12InstanceTypeEmm
	jmp	.L380
.L388:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21720:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl20RecordVirtualContextENS0_7ContextE, .-_ZN2v88internal24ObjectStatsCollectorImpl20RecordVirtualContextENS0_7ContextE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl40RecordVirtualFunctionTemplateInfoDetailsENS0_20FunctionTemplateInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl40RecordVirtualFunctionTemplateInfoDetailsENS0_20FunctionTemplateInfoE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl40RecordVirtualFunctionTemplateInfoDetailsENS0_20FunctionTemplateInfoE, @function
_ZN2v88internal24ObjectStatsCollectorImpl40RecordVirtualFunctionTemplateInfoDetailsENS0_20FunctionTemplateInfoE:
.LFB21699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$16, %rsp
	movq	47(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	cmpq	%rdx, -37504(%rax)
	je	.L390
	movq	%rdx, -32(%rbp)
	movq	-1(%rdx), %rsi
	leaq	-32(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-32(%rbp), %rdx
	movq	%r12, %rsi
	pushq	$0
	movl	$37, %ecx
	xorl	%r9d, %r9d
	movslq	%eax, %r8
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	movq	0(%r13), %rax
	popq	%rcx
	popq	%rsi
	movq	-37504(%rax), %rdx
.L390:
	movq	%r12, %rax
	movq	71(%r12), %rcx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	cmpq	%rax, %rcx
	je	.L391
	movq	55(%rcx), %rax
.L391:
	cmpq	%rax, %rdx
	je	.L389
	movq	%rax, -32(%rbp)
	movq	-1(%rax), %rsi
	leaq	-32(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-32(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	$0
	movslq	%eax, %r8
	movl	$37, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rax
	popq	%rdx
.L389:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L395
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L395:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21699:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl40RecordVirtualFunctionTemplateInfoDetailsENS0_20FunctionTemplateInfoE, .-_ZN2v88internal24ObjectStatsCollectorImpl40RecordVirtualFunctionTemplateInfoDetailsENS0_20FunctionTemplateInfoE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualAllocationSiteDetailsENS0_14AllocationSiteE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualAllocationSiteDetailsENS0_14AllocationSiteE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualAllocationSiteDetailsENS0_14AllocationSiteE, @function
_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualAllocationSiteDetailsENS0_14AllocationSiteE:
.LFB21698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	7(%rsi), %rax
	testb	$1, %al
	jne	.L413
.L396:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L414
	leaq	-24(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	movq	%rax, -56(%rbp)
	movq	-1(%rax), %rdx
	movq	%rdi, %r13
	movq	%rsi, %r12
	cmpw	$1061, 11(%rdx)
	je	.L415
	movq	-1(%rax), %rsi
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-56(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	$0
	movslq	%eax, %r8
	movq	%r13, %rdi
	movl	$42, %ecx
	movq	%r12, %rsi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	movq	-56(%rbp), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	popq	%rdi
	popq	%r8
	andl	$2097152, %edx
	movq	7(%rax), %rdx
	je	.L416
	testb	$1, %dl
	jne	.L406
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %rdx
.L406:
	movq	%rdx, -48(%rbp)
	leaq	-48(%rbp), %r14
	movq	-1(%rdx), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movl	$17, %ecx
	pushq	$0
	movslq	%eax, %r8
.L412:
	movq	-48(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rax
	movq	-56(%rbp), %rax
	movq	%r14, %rdi
	popq	%rdx
	movq	15(%rax), %rax
	movq	%rax, -48(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-48(%rbp), %rdx
	movq	%r12, %rsi
	pushq	$0
	movl	$15, %ecx
	xorl	%r9d, %r9d
	movslq	%eax, %r8
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rcx
	popq	%rsi
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L416:
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %dl
	je	.L404
	cmpq	288(%rax), %rdx
	je	.L404
.L403:
	movq	%rdx, -48(%rbp)
	leaq	-48(%rbp), %r14
	movq	-1(%rdx), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movl	$16, %ecx
	pushq	$0
	movslq	%eax, %r8
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L415:
	movq	%rax, -48(%rbp)
	leaq	-48(%rbp), %r14
	movq	-1(%rax), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movl	$40, %ecx
	pushq	$0
	movslq	%eax, %r8
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L404:
	movq	968(%rax), %rdx
	jmp	.L403
.L414:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21698:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualAllocationSiteDetailsENS0_14AllocationSiteE, .-_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualAllocationSiteDetailsENS0_14AllocationSiteE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl23CollectGlobalStatisticsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl23CollectGlobalStatisticsEv
	.type	_ZN2v88internal24ObjectStatsCollectorImpl23CollectGlobalStatisticsEv, @function
_ZN2v88internal24ObjectStatsCollectorImpl23CollectGlobalStatisticsEv:
.LFB21706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	1536(%rax), %r13
	testb	$1, %r13b
	jne	.L420
.L418:
	movq	-32864(%rax), %rax
	leaq	-32(%rbp), %r13
	movq	%r13, %rdi
	movq	%rax, -32(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-32(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	$0
	movslq	%eax, %r8
	movl	$71, %ecx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	-32944(%rax), %rax
	movq	%rax, -32(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-32(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movslq	%eax, %r8
	movl	$50, %ecx
	movq	%r12, %rdi
	movl	$0, (%rsp)
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	-32976(%rax), %rax
	movq	%rax, -32(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-32(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movslq	%eax, %r8
	movl	$72, %ecx
	movq	%r12, %rdi
	movl	$0, (%rsp)
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	-32968(%rax), %rax
	movq	%rax, -32(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-32(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movslq	%eax, %r8
	movl	$73, %ecx
	movq	%r12, %rdi
	movl	$0, (%rsp)
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	-32960(%rax), %rax
	movq	%rax, -32(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-32(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movslq	%eax, %r8
	movl	$62, %ecx
	movq	%r12, %rdi
	movl	$0, (%rsp)
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	-32880(%rax), %rax
	movq	%rax, -32(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-32(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movslq	%eax, %r8
	movl	$64, %ecx
	movq	%r12, %rdi
	movl	$0, (%rsp)
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	-32912(%rax), %rax
	movq	%rax, -32(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-32(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movslq	%eax, %r8
	movl	$65, %ecx
	movq	%r12, %rdi
	movl	$0, (%rsp)
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rax
	popq	%rdx
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L426
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L419:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualAllocationSiteDetailsENS0_14AllocationSiteE
	movq	39(%r13), %r13
	testb	$1, %r13b
	je	.L425
	.p2align 4,,10
	.p2align 3
.L420:
	movq	-1(%r13), %rax
	cmpw	$121, 11(%rax)
	je	.L419
.L425:
	movq	(%r12), %rax
	jmp	.L418
.L426:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21706:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl23CollectGlobalStatisticsEv, .-_ZN2v88internal24ObjectStatsCollectorImpl23CollectGlobalStatisticsEv
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl28RecordVirtualJSObjectDetailsENS0_8JSObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl28RecordVirtualJSObjectDetailsENS0_8JSObjectE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl28RecordVirtualJSObjectDetailsENS0_8JSObjectE, @function
_ZN2v88internal24ObjectStatsCollectorImpl28RecordVirtualJSObjectDetailsENS0_8JSObjectE:
.LFB21701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-1(%rsi), %rax
	cmpw	$1025, 11(%rax)
	je	.L427
	movq	-1(%rsi), %rax
	movq	%rdi, %r13
	movq	%rsi, %r12
	cmpw	$1105, 11(%rax)
	je	.L465
.L434:
	movq	-1(%r12), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	movq	7(%r12), %rax
	jne	.L435
	movq	%r12, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	subq	$37592, %rdx
	testb	$1, %al
	jne	.L436
.L438:
	movq	968(%rdx), %rax
.L437:
	movq	0(%r13), %rdx
	movq	%rax, -48(%rbp)
	cmpq	%rax, -36624(%rdx)
	je	.L442
	movq	-1(%r12), %rax
	movzbl	9(%rax), %r9d
	cmpl	$2, %r9d
	jg	.L466
.L440:
	movq	-48(%rbp), %rax
	sall	$3, %r9d
	leaq	-48(%rbp), %rdi
	movslq	%r9d, %rbx
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-1(%r12), %rdx
	movq	%rbx, %r9
	movslq	%eax, %r8
	movl	15(%rdx), %edx
	andl	$1048576, %edx
	cmpl	$1, %edx
	sbbl	%ecx, %ecx
	subq	$8, %rsp
	andl	$-6, %ecx
	pushq	$0
	addl	$59, %ecx
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L435:
	testb	$1, %al
	jne	.L444
	movq	%r12, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %rax
.L444:
	movq	-1(%r12), %rdx
	leaq	-48(%rbp), %rdi
	movl	15(%rdx), %edx
	movq	%rax, -48(%rbp)
	andl	$1048576, %edx
	cmpl	$1, %edx
	movslq	35(%rax), %rdx
	sbbl	%r14d, %r14d
	subl	19(%rax), %edx
	subl	27(%rax), %edx
	movq	-1(%rax), %rsi
	leal	(%rdx,%rdx,2), %ebx
	andl	$-6, %r14d
	sall	$3, %ebx
	addl	$60, %r14d
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movslq	%ebx, %rbx
	movl	%r14d, %ecx
	pushq	$0
	movq	%rbx, %r9
	movslq	%eax, %r8
.L463:
	movq	-48(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%r9
	popq	%r10
.L442:
	movq	15(%r12), %rax
	movq	%rax, -56(%rbp)
	movq	-1(%r12), %rdx
	movzbl	14(%rdx), %edx
	shrl	$3, %edx
	cmpl	$12, %edx
	movq	-1(%r12), %rdx
	je	.L467
	cmpw	$1061, 11(%rdx)
	jne	.L449
	movq	0(%r13), %rdx
	cmpq	-37304(%rdx), %rax
	je	.L448
	movq	-1(%rax), %rsi
	leaq	-56(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-56(%rbp), %rcx
	subl	$16, %eax
	movslq	11(%rcx), %r9
	cltd
	idivl	%r9d
	movq	23(%r12), %rdx
	cltq
	testb	$1, %dl
	jne	.L450
	sarq	$32, %rdx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
.L451:
	cvttsd2siq	%xmm0, %rdx
	movq	-1(%rcx), %rsi
	movq	%r14, %rdi
	subl	%edx, %r9d
	movq	%r9, %rbx
	imulq	%rax, %rbx
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-56(%rbp), %rdx
	movq	%r13, %rdi
	pushq	$0
	movslq	%eax, %r8
	movl	$14, %ecx
	movq	%r12, %rsi
	movq	%rbx, %r9
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rdi
	popq	%r8
	.p2align 4,,10
	.p2align 3
.L448:
	movq	-1(%r12), %rax
	cmpw	$1069, 11(%rax)
	je	.L454
	movq	-1(%r12), %rax
	cmpw	$1077, 11(%rax)
	jne	.L427
.L454:
	movq	23(%r12), %rax
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	$0
	movslq	%eax, %r8
	movl	$41, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rax
	popq	%rdx
.L427:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L468
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	.cfi_restore_state
	cmpw	$1061, 11(%rdx)
	movl	$51, %ecx
	movq	%rax, -48(%rbp)
	leaq	-48(%rbp), %rdi
	movl	$13, %r14d
	movslq	35(%rax), %rdx
	cmovne	%ecx, %r14d
	subl	19(%rax), %edx
	subl	27(%rax), %edx
	movq	-1(%rax), %rsi
	leal	(%rdx,%rdx,2), %ebx
	sall	$3, %ebx
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movslq	%ebx, %rbx
	movl	%r14d, %ecx
	pushq	$0
	movq	%rbx, %r9
	movslq	%eax, %r8
.L464:
	movq	-48(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rcx
	popq	%rsi
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L449:
	movq	%rax, -48(%rbp)
	movq	-1(%rax), %rsi
	leaq	-48(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movl	$52, %ecx
	pushq	$0
	movslq	%eax, %r8
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L436:
	cmpq	288(%rdx), %rax
	jne	.L437
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L465:
	movq	47(%rsi), %rax
	cmpl	$67, 59(%rax)
	jne	.L469
.L430:
	movq	%r12, -48(%rbp)
	movq	-1(%r12), %rsi
	leaq	-48(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	$0
	movslq	%eax, %r8
	movl	$43, %ecx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%r11
	popq	%rbx
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L469:
	movabsq	$287762808832, %rdx
	movq	23(%rsi), %rax
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L430
	testb	$1, %al
	je	.L434
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L430
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L434
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L450:
	movsd	7(%rdx), %xmm0
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L466:
	movzbl	7(%rax), %eax
	subl	%r9d, %eax
	movl	%eax, %r9d
	jmp	.L440
.L468:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21701:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl28RecordVirtualJSObjectDetailsENS0_8JSObjectE, .-_ZN2v88internal24ObjectStatsCollectorImpl28RecordVirtualJSObjectDetailsENS0_8JSObjectE
	.section	.rodata._ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualFeedbackVectorDetailsENS0_14FeedbackVectorE.str1.8,"aMS",@progbits,1
	.align 8
.LC321:
	.string	"calculated_size == vector.Size()"
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualFeedbackVectorDetailsENS0_14FeedbackVectorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualFeedbackVectorDetailsENS0_14FeedbackVectorE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualFeedbackVectorDetailsENS0_14FeedbackVectorE, @function
_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualFeedbackVectorDetailsENS0_14FeedbackVectorE:
.LFB21703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -120(%rbp)
	movq	32(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	divq	%r8
	movq	24(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L471
	movq	(%rax), %rcx
	movq	%rsi, %rdi
	movq	%rdx, %r9
	movq	16(%rcx), %rsi
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L472:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L471
	movq	16(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L471
.L474:
	cmpq	%rdi, %rsi
	jne	.L472
	cmpq	8(%rcx), %rdi
	jne	.L472
.L470:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L526
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L471:
	.cfi_restore_state
	leaq	-120(%rbp), %rax
	leaq	24(%r14), %rdi
	movl	$1, %ecx
	leaq	-96(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rdi, -96(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt10_HashtableIN2v88internal10HeapObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	movq	8(%r14), %rax
	addq	$1, 9080(%rax)
	addq	$48, 28040(%rax)
	addq	$1, 192568(%rax)
	addq	$1, 344248(%rax)
	movq	-120(%rbp), %rax
	movq	7(%rax), %rdx
	movq	23(%rdx), %rdi
	movq	%rdi, -152(%rbp)
	movq	-1(%rdi), %rdx
	cmpw	$75, 11(%rdx)
	jne	.L470
	movl	7(%rdi), %ecx
	leaq	7(%rdi), %rsi
	movq	%rsi, -168(%rbp)
	testl	%ecx, %ecx
	jle	.L504
	xorl	%ebx, %ebx
	leaq	-104(%rbp), %r13
	movq	$48, -160(%rbp)
	leaq	.L491(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L500:
	movq	-152(%rbp), %rax
	movl	%ebx, %esi
	movq	%r13, %rdi
	movl	%ebx, -80(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal16FeedbackMetadata7GetKindENS0_12FeedbackSlotE@PLT
	movl	%eax, %r10d
	cmpl	$23, %eax
	ja	.L505
	leaq	.L478(%rip), %rdi
	movl	%eax, %eax
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualFeedbackVectorDetailsENS0_14FeedbackVectorE,"a",@progbits
	.align 4
	.align 4
.L478:
	.long	.L477-.L478
	.long	.L479-.L478
	.long	.L479-.L478
	.long	.L479-.L478
	.long	.L479-.L478
	.long	.L479-.L478
	.long	.L479-.L478
	.long	.L479-.L478
	.long	.L479-.L478
	.long	.L479-.L478
	.long	.L479-.L478
	.long	.L479-.L478
	.long	.L479-.L478
	.long	.L479-.L478
	.long	.L479-.L478
	.long	.L505-.L478
	.long	.L505-.L478
	.long	.L479-.L478
	.long	.L505-.L478
	.long	.L505-.L478
	.long	.L505-.L478
	.long	.L505-.L478
	.long	.L479-.L478
	.long	.L477-.L478
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualFeedbackVectorDetailsENS0_14FeedbackVectorE
	.p2align 4,,10
	.p2align 3
.L479:
	movl	$2, %eax
.L476:
	addl	%ebx, %eax
	movl	%eax, -140(%rbp)
	cmpl	$23, %r10d
	ja	.L506
	leaq	.L481(%rip), %rcx
	movl	%r10d, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualFeedbackVectorDetailsENS0_14FeedbackVectorE
	.align 4
	.align 4
.L481:
	.long	.L477-.L481
	.long	.L482-.L481
	.long	.L482-.L481
	.long	.L482-.L481
	.long	.L482-.L481
	.long	.L482-.L481
	.long	.L482-.L481
	.long	.L482-.L481
	.long	.L482-.L481
	.long	.L482-.L481
	.long	.L482-.L481
	.long	.L482-.L481
	.long	.L482-.L481
	.long	.L482-.L481
	.long	.L482-.L481
	.long	.L506-.L481
	.long	.L506-.L481
	.long	.L482-.L481
	.long	.L506-.L481
	.long	.L506-.L481
	.long	.L506-.L481
	.long	.L506-.L481
	.long	.L482-.L481
	.long	.L477-.L481
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualFeedbackVectorDetailsENS0_14FeedbackVectorE
	.p2align 4,,10
	.p2align 3
.L482:
	movl	$16, %esi
.L480:
	movq	(%r14), %rax
	leal	48(,%rbx,8), %ebx
	movq	8(%r14), %rdx
	movslq	%ebx, %rbx
	leaq	-37592(%rax), %rdi
	movq	-120(%rbp), %rax
	movq	-1(%rbx,%rax), %rcx
	movl	$1140, %eax
	cmpl	$3, %ecx
	je	.L483
	movq	%rcx, %rax
	andq	$-3, %rax
	testb	$1, %cl
	cmovne	%rax, %rcx
	cmpl	$16, %r10d
	ja	.L485
	leaq	.L486(%rip), %r9
	movl	%r10d, %r8d
	movslq	(%r9,%r8,4), %rax
	addq	%r9, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualFeedbackVectorDetailsENS0_14FeedbackVectorE
	.align 4
	.align 4
.L486:
	.long	.L485-.L486
	.long	.L487-.L486
	.long	.L487-.L486
	.long	.L487-.L486
	.long	.L489-.L486
	.long	.L488-.L486
	.long	.L488-.L486
	.long	.L488-.L486
	.long	.L488-.L486
	.long	.L488-.L486
	.long	.L487-.L486
	.long	.L487-.L486
	.long	.L487-.L486
	.long	.L487-.L486
	.long	.L485-.L486
	.long	.L508-.L486
	.long	.L508-.L486
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualFeedbackVectorDetailsENS0_14FeedbackVectorE
	.p2align 4,,10
	.p2align 3
.L487:
	movl	$1142, %eax
	cmpq	3840(%rdi), %rcx
	je	.L483
	xorl	%eax, %eax
	cmpq	3768(%rdi), %rcx
	sete	%al
	addl	$1141, %eax
	.p2align 4,,10
	.p2align 3
.L483:
	cltq
	addq	%rsi, -160(%rbp)
	xorl	%r12d, %r12d
	leaq	(%rdx,%rax,8), %rcx
	salq	$7, %rax
	addq	%rdx, %rax
	addq	$1, 8(%rcx)
	addq	%rsi, 18968(%rcx)
	addq	$1, 47408(%rax)
	addq	$1, 199088(%rax)
	movq	%r13, %rax
	movl	%r10d, %r13d
	movq	%rax, %r10
	cmpl	$23, %r13d
	ja	.L515
	.p2align 4,,10
	.p2align 3
.L527:
	movl	%r13d, %eax
	movslq	(%r15,%rax,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualFeedbackVectorDetailsENS0_14FeedbackVectorE
	.align 4
	.align 4
.L491:
	.long	.L477-.L491
	.long	.L492-.L491
	.long	.L492-.L491
	.long	.L492-.L491
	.long	.L492-.L491
	.long	.L492-.L491
	.long	.L492-.L491
	.long	.L492-.L491
	.long	.L492-.L491
	.long	.L492-.L491
	.long	.L492-.L491
	.long	.L492-.L491
	.long	.L492-.L491
	.long	.L492-.L491
	.long	.L492-.L491
	.long	.L515-.L491
	.long	.L515-.L491
	.long	.L492-.L491
	.long	.L515-.L491
	.long	.L515-.L491
	.long	.L515-.L491
	.long	.L515-.L491
	.long	.L492-.L491
	.long	.L477-.L491
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualFeedbackVectorDetailsENS0_14FeedbackVectorE
	.p2align 4,,10
	.p2align 3
.L492:
	movl	$2, %eax
	cmpl	%eax, %r12d
	jge	.L493
.L528:
	movq	-120(%rbp), %rax
	movq	-1(%rbx,%rax), %rax
	testb	$1, %al
	je	.L495
	cmpl	$3, %eax
	je	.L495
	andq	$-3, %rax
	movq	-1(%rax), %rdx
	cmpw	$151, 11(%rdx)
	je	.L498
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	subw	$148, %dx
	cmpw	$1, %dx
	jbe	.L498
	.p2align 4,,10
	.p2align 3
.L495:
	addl	$1, %r12d
	addq	$8, %rbx
	cmpl	$23, %r13d
	jbe	.L527
.L515:
	movl	$1, %eax
	cmpl	%eax, %r12d
	jl	.L528
	.p2align 4,,10
	.p2align 3
.L493:
	movq	-168(%rbp), %rax
	movl	-140(%rbp), %esi
	movq	%r10, %r13
	cmpl	%esi, (%rax)
	jle	.L529
	movl	%esi, %ebx
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L488:
	movl	$1139, %eax
	cmpq	3840(%rdi), %rcx
	je	.L483
	xorl	%eax, %eax
	cmpq	3768(%rdi), %rcx
	sete	%al
	addl	$1138, %eax
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L506:
	movl	$8, %esi
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L505:
	movl	$1, %eax
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L508:
	movl	$1137, %eax
	jmp	.L483
.L489:
	movl	$1136, %eax
	cmpq	%rcx, 3840(%rdi)
	je	.L483
	xorl	%eax, %eax
	cmpq	3768(%rdi), %rcx
	sete	%al
	addl	$1135, %eax
	jmp	.L483
.L485:
	movl	$1140, %eax
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L498:
	movq	-120(%rbp), %r11
	movq	%rax, -104(%rbp)
	movq	%r10, %rdi
	movq	%r11, -136(%rbp)
	movq	-1(%rax), %rsi
	movq	%r10, -128(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-104(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	$0
	movq	-136(%rbp), %r11
	movslq	%eax, %r8
	movq	%r14, %rdi
	movl	$27, %ecx
	movq	%r11, %rsi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rax
	movq	-128(%rbp), %r10
	popq	%rdx
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L529:
	movq	-120(%rbp), %rax
.L475:
	movq	-1(%rax), %rsi
	movq	-176(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	testl	%eax, %eax
	js	.L501
	cltq
	cmpq	-160(%rbp), %rax
	je	.L470
.L501:
	leaq	.LC321(%rip), %rsi
	leaq	.LC319(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L477:
	leaq	.LC317(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L504:
	movq	$48, -160(%rbp)
	jmp	.L475
.L526:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21703:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualFeedbackVectorDetailsENS0_14FeedbackVectorE, .-_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualFeedbackVectorDetailsENS0_14FeedbackVectorE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualJSGlobalObjectDetailsENS0_14JSGlobalObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualJSGlobalObjectDetailsENS0_14JSGlobalObjectE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualJSGlobalObjectDetailsENS0_14JSGlobalObjectE, @function
_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualJSGlobalObjectDetailsENS0_14JSGlobalObjectE:
.LFB21700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	7(%rsi), %rax
	movq	%rax, -80(%rbp)
	movslq	35(%rax), %rbx
	subl	19(%rax), %ebx
	subl	27(%rax), %ebx
	sall	$3, %ebx
	movslq	%ebx, %rbx
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-80(%rbp), %r10
	movslq	%eax, %r14
	movq	%r10, -72(%rbp)
	cmpq	%r14, %rbx
	jnb	.L555
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl12SameLivenessENS0_10HeapObjectES2_
	testb	%al, %al
	jne	.L532
.L554:
	leaq	-64(%rbp), %r15
.L533:
	movq	15(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	$0
	movslq	%eax, %r8
	movl	$38, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L556
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L532:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%r10, %rsi
	leaq	-64(%rbp), %r15
	movq	%r12, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl18ShouldRecordObjectENS0_10HeapObjectENS1_7CowModeE
	testb	%al, %al
	je	.L533
	movq	-72(%rbp), %r8
	movq	32(%r12), %rdi
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	movq	24(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L535
	movq	(%rax), %rcx
	movq	16(%rcx), %rsi
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L536:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L535
	movq	16(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L535
.L537:
	cmpq	%rsi, %r8
	jne	.L536
	cmpq	8(%rcx), %r8
	jne	.L536
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L535:
	leaq	-64(%rbp), %r15
	leaq	24(%r12), %rdi
	movl	$1, %ecx
	leaq	-72(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rdi, -64(%rbp)
	call	_ZNSt10_HashtableIN2v88internal10HeapObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	movq	8(%r12), %rax
	xorl	%edx, %edx
	addq	$1, 9168(%rax)
	addq	%r14, 28128(%rax)
	testq	%r14, %r14
	je	.L538
	bsrq	%r14, %r14
	movl	$59, %edx
	movl	$0, %ecx
	xorq	$63, %r14
	subl	%r14d, %edx
	cmovs	%ecx, %edx
	movl	$15, %ecx
	cmpl	$15, %edx
	cmovg	%ecx, %edx
.L538:
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,8), %rdx
	addq	$1, 193968(%rdx)
	addq	%rbx, 47088(%rax)
	addq	$1, 345648(%rdx)
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L555:
	leaq	.LC320(%rip), %rsi
	leaq	.LC319(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L556:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21700:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualJSGlobalObjectDetailsENS0_14JSGlobalObjectE, .-_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualJSGlobalObjectDetailsENS0_14JSGlobalObjectE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl30RecordSimpleVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl30RecordSimpleVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl30RecordSimpleVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE, @function
_ZN2v88internal24ObjectStatsCollectorImpl30RecordSimpleVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE:
.LFB21695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-88(%rbp), %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rdx, -88(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movslq	%eax, %r12
	movq	-88(%rbp), %rax
	movq	%rax, -72(%rbp)
	testq	%r12, %r12
	je	.L589
	testq	%rbx, %rbx
	jne	.L559
.L562:
	movq	-72(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl18ShouldRecordObjectENS0_10HeapObjectENS1_7CowModeE
	movl	%eax, %r14d
	testb	%al, %al
	jne	.L590
.L561:
	xorl	%r14d, %r14d
.L557:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L591
	addq	$56, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L559:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L562
	movq	%rbx, %rsi
	andl	$262143, %ebx
	movl	$1, %edx
	movl	%ebx, %ecx
	andq	$-262144, %rsi
	shrl	$8, %ebx
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%rsi), %rcx
	movl	$1, %esi
	leaq	(%rcx,%rbx,4), %rdi
	movl	(%rdi), %ecx
	testl	%ecx, %edx
	jne	.L592
.L563:
	movq	%rax, %rdi
	andl	$262143, %eax
	movl	$1, %edx
	movl	%eax, %ecx
	andq	$-262144, %rdi
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%rdi), %rcx
	leaq	(%rcx,%rax,4), %rdi
	movl	$1, %eax
	movl	(%rdi), %ecx
	testl	%ecx, %edx
	jne	.L593
.L565:
	cmpl	%eax, %esi
	jne	.L561
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L590:
	movq	-72(%rbp), %r8
	movq	32(%r13), %rdi
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	movq	24(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L568
	movq	(%rax), %rcx
	movq	16(%rcx), %rsi
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L569:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L568
	movq	16(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L568
.L570:
	cmpq	%r8, %rsi
	jne	.L569
	cmpq	8(%rcx), %r8
	jne	.L569
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L589:
	leaq	.LC320(%rip), %rsi
	leaq	.LC319(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L568:
	leaq	24(%r13), %rdi
	movl	$1, %ecx
	leaq	-64(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	movq	%rdi, -64(%rbp)
	call	_ZNSt10_HashtableIN2v88internal10HeapObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	movq	8(%r13), %rcx
	movslq	%r15d, %rax
	movl	$0, %edx
	leaq	(%rcx,%rax,8), %rax
	addq	%r12, 27816(%rax)
	bsrq	%r12, %r12
	addq	$1, 8856(%rax)
	xorq	$63, %r12
	movl	$59, %eax
	subl	%r12d, %eax
	cmovs	%edx, %eax
	movl	$15, %edx
	cmpl	$15, %eax
	cmovg	%edx, %eax
	movslq	%eax, %rdx
	leal	1106(%r15), %eax
	cltq
	salq	$4, %rax
	addq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rax
	addq	$1, 47408(%rax)
	addq	$1, 199088(%rax)
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L592:
	addl	%edx, %edx
	jne	.L564
	movl	4(%rdi), %ecx
	movl	$1, %edx
.L564:
	xorl	%esi, %esi
	testl	%ecx, %edx
	sete	%sil
	addl	%esi, %esi
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L593:
	movl	%edx, %eax
	addl	%eax, %eax
	jne	.L566
	testb	$1, 4(%rdi)
	movl	$2, %edx
	cmove	%edx, %eax
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L566:
	andl	%ecx, %eax
	cmpl	$1, %eax
	sbbl	%eax, %eax
	andl	$2, %eax
	jmp	.L565
.L591:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21695:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl30RecordSimpleVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE, .-_ZN2v88internal24ObjectStatsCollectorImpl30RecordSimpleVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl23RecordVirtualMapDetailsENS0_3MapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl23RecordVirtualMapDetailsENS0_3MapE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl23RecordVirtualMapDetailsENS0_3MapE, @function
_ZN2v88internal24ObjectStatsCollectorImpl23RecordVirtualMapDetailsENS0_3MapE:
.LFB21711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	15(%rsi), %eax
	testl	$1048576, %eax
	movl	15(%rsi), %eax
	je	.L595
	testl	$2097152, %eax
	je	.L596
	movq	%rsi, -64(%rbp)
	leaq	-64(%rbp), %r14
	movq	-1(%rsi), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movl	$47, %ecx
	pushq	$0
	movslq	%eax, %r8
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L595:
	testl	$16777216, %eax
	je	.L599
	movq	%rsi, -64(%rbp)
	leaq	-64(%rbp), %r14
	movq	-1(%rsi), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movl	$45, %ecx
	pushq	$0
	movslq	%eax, %r8
.L618:
	movq	-64(%rbp), %rdx
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rdi
	popq	%r8
.L597:
	movq	39(%r12), %r15
	movl	15(%r12), %eax
	testl	$4194304, %eax
	je	.L601
	movq	0(%r13), %rax
	cmpq	%r15, -37296(%rax)
	je	.L601
	movl	15(%r12), %eax
	testl	$1048576, %eax
	jne	.L619
	movl	15(%r12), %eax
	leaq	-64(%rbp), %r14
	testl	$16777216, %eax
	jne	.L620
.L603:
	movq	15(%r15), %rbx
	movq	%r14, %rdi
	movq	7(%rbx), %rax
	movq	%rax, -64(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	$0
	movslq	%eax, %r8
	movl	$25, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	movq	15(%rbx), %rax
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	movq	%r15, %rsi
	movslq	%eax, %r8
	movl	$26, %ecx
	movq	%r13, %rdi
	movl	$0, (%rsp)
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rax
	popq	%rdx
.L601:
	movl	15(%r12), %eax
	testl	$1048576, %eax
	je	.L594
	movq	71(%r12), %rax
	testb	$1, %al
	jne	.L621
.L594:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L622
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L599:
	.cfi_restore_state
	movl	15(%rsi), %eax
	testl	$2097152, %eax
	je	.L600
	movq	%rsi, -64(%rbp)
	leaq	-64(%rbp), %r14
	movq	-1(%rsi), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movl	$46, %ecx
	pushq	$0
	movslq	%eax, %r8
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L596:
	movl	15(%rsi), %eax
	testl	$1048576, %eax
	jne	.L623
.L598:
	movq	%r12, -64(%rbp)
	leaq	-64(%rbp), %r14
	movq	-1(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movl	$48, %ecx
	pushq	$0
	movslq	%eax, %r8
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L600:
	movl	15(%rsi), %eax
	testl	$33554432, %eax
	jne	.L597
	movq	%rsi, -64(%rbp)
	leaq	-64(%rbp), %r14
	movq	-1(%rsi), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movl	$49, %ecx
	pushq	$0
	movslq	%eax, %r8
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L619:
	movq	%r15, -64(%rbp)
	leaq	-64(%rbp), %r14
	movq	-1(%r15), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-64(%rbp), %rdx
	movq	%r12, %rsi
	pushq	$0
	movl	$58, %ecx
	xorl	%r9d, %r9d
	movslq	%eax, %r8
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rcx
	popq	%rsi
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L623:
	movl	15(%rsi), %eax
	testl	$4194304, %eax
	jne	.L598
	movq	%rsi, -64(%rbp)
	leaq	-64(%rbp), %r14
	movq	-1(%rsi), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movl	$44, %ecx
	pushq	$0
	movslq	%eax, %r8
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L621:
	movq	-1(%rax), %rdx
	cmpw	$95, 11(%rdx)
	jne	.L594
	movq	15(%rax), %rdx
	testb	$1, %dl
	je	.L594
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	subw	$148, %ax
	cmpw	$1, %ax
	ja	.L594
	movl	$61, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl30RecordSimpleVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L620:
	movl	$23, %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl30RecordSimpleVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE
	jmp	.L603
.L622:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21711:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl23RecordVirtualMapDetailsENS0_3MapE, .-_ZN2v88internal24ObjectStatsCollectorImpl23RecordVirtualMapDetailsENS0_3MapE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl52RecordVirtualObjectsForConstantPoolOrEmbeddedObjectsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl52RecordVirtualObjectsForConstantPoolOrEmbeddedObjectsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl52RecordVirtualObjectsForConstantPoolOrEmbeddedObjectsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE, @function
_ZN2v88internal24ObjectStatsCollectorImpl52RecordVirtualObjectsForConstantPoolOrEmbeddedObjectsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE:
.LFB21716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-80(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -80(%rbp)
	movq	-1(%rdx), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movslq	%eax, %rbx
	movq	-80(%rbp), %rax
	movq	%rax, -72(%rbp)
	testq	%rbx, %rbx
	je	.L666
	testq	%r15, %r15
	jne	.L667
.L626:
	movq	-72(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl18ShouldRecordObjectENS0_10HeapObjectENS1_7CowModeE
	testb	%al, %al
	jne	.L668
.L624:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L669
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L667:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L626
	movq	%r15, %rsi
	andl	$262143, %r15d
	movl	$1, %edx
	movl	%r15d, %ecx
	andq	$-262144, %rsi
	shrl	$8, %r15d
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%rsi), %rcx
	movl	$1, %esi
	leaq	(%rcx,%r15,4), %rdi
	movl	(%rdi), %ecx
	testl	%ecx, %edx
	jne	.L670
.L627:
	movq	%rax, %rdi
	andl	$262143, %eax
	movl	$1, %edx
	movl	%eax, %ecx
	andq	$-262144, %rdi
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%rdi), %rcx
	leaq	(%rcx,%rax,4), %rdi
	movl	$1, %eax
	movl	(%rdi), %ecx
	testl	%ecx, %edx
	jne	.L671
.L629:
	cmpl	%eax, %esi
	jne	.L624
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L668:
	movq	-72(%rbp), %r8
	movq	32(%r13), %rdi
	xorl	%edx, %edx
	leaq	24(%r13), %r9
	movq	%r8, %rax
	divq	%rdi
	movq	24(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L633
	movq	(%rax), %rcx
	movq	16(%rcx), %rsi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L634:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L633
	movq	16(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L633
.L635:
	cmpq	%r8, %rsi
	jne	.L634
	cmpq	8(%rcx), %r8
	jne	.L634
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L633:
	movl	$1, %ecx
	leaq	-64(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	movq	%r9, %rdi
	movq	%r9, -64(%rbp)
	call	_ZNSt10_HashtableIN2v88internal10HeapObjectES2_SaIS2_ENSt8__detail9_IdentityESt8equal_toIS2_ENS1_6Object6HasherENS4_18_Mod_range_hashingENS4_20_Default_ranged_hashENS4_20_Prime_rehash_policyENS4_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIRKS2_NS4_10_AllocNodeISaINS4_10_Hash_nodeIS2_Lb1EEEEEEEESt4pairINS4_14_Node_iteratorIS2_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	movq	8(%r13), %rcx
	movslq	%r14d, %rax
	movl	$0, %edx
	leaq	(%rcx,%rax,8), %rax
	addq	%rbx, 27816(%rax)
	bsrq	%rbx, %rbx
	addq	$1, 8856(%rax)
	xorq	$63, %rbx
	movl	$59, %eax
	subl	%ebx, %eax
	cmovs	%edx, %eax
	movl	$15, %edx
	cmpl	$15, %eax
	cmovg	%edx, %eax
	movslq	%eax, %rdx
	leal	1106(%r14), %eax
	cltq
	salq	$4, %rax
	addq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rax
	addq	$1, 47408(%rax)
	addq	$1, 199088(%rax)
	movq	-1(%r12), %rax
	cmpw	$123, 11(%rax)
	jne	.L624
	movl	11(%r12), %edx
	leaq	7(%r12), %rax
	testl	%edx, %edx
	jle	.L624
	leaq	15(%r12), %r15
	xorl	%ebx, %ebx
.L640:
	movq	(%r15), %rdx
	testb	$1, %dl
	je	.L638
.L672:
	movl	%r14d, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal24ObjectStatsCollectorImpl52RecordVirtualObjectsForConstantPoolOrEmbeddedObjectsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE
	movq	-88(%rbp), %rax
	addl	$1, %ebx
	addq	$8, %r15
	cmpl	%ebx, 4(%rax)
	jle	.L624
	movq	(%r15), %rdx
	testb	$1, %dl
	jne	.L672
.L638:
	addl	$1, %ebx
	addq	$8, %r15
	cmpl	%ebx, 4(%rax)
	jg	.L640
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L666:
	leaq	.LC320(%rip), %rsi
	leaq	.LC319(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L670:
	addl	%edx, %edx
	jne	.L628
	movl	4(%rdi), %ecx
	movl	$1, %edx
.L628:
	xorl	%esi, %esi
	testl	%ecx, %edx
	sete	%sil
	addl	%esi, %esi
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L671:
	movl	%edx, %eax
	addl	%eax, %eax
	jne	.L630
	testb	$1, 4(%rdi)
	movl	$2, %edx
	cmove	%edx, %eax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L630:
	andl	%ecx, %eax
	cmpl	$1, %eax
	sbbl	%eax, %eax
	andl	$2, %eax
	jmp	.L629
.L669:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21716:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl52RecordVirtualObjectsForConstantPoolOrEmbeddedObjectsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE, .-_ZN2v88internal24ObjectStatsCollectorImpl52RecordVirtualObjectsForConstantPoolOrEmbeddedObjectsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl33RecordVirtualBytecodeArrayDetailsENS0_13BytecodeArrayE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl33RecordVirtualBytecodeArrayDetailsENS0_13BytecodeArrayE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl33RecordVirtualBytecodeArrayDetailsENS0_13BytecodeArrayE, @function
_ZN2v88internal24ObjectStatsCollectorImpl33RecordVirtualBytecodeArrayDetailsENS0_13BytecodeArrayE:
.LFB21717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	15(%rsi), %rax
	movq	%rax, -64(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	$0
	movslq	%eax, %r8
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	$18, %ecx
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	movq	15(%r15), %rsi
	popq	%rdi
	popq	%r8
	movl	11(%rsi), %r9d
	leaq	7(%rsi), %rax
	leaq	15(%rsi), %r12
	testl	%r9d, %r9d
	jg	.L680
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L678:
	addl	$1, %ebx
	addq	$8, %r12
	cmpl	%ebx, 4(%rax)
	jle	.L681
.L680:
	movq	(%r12), %rdx
	testb	$1, %dl
	je	.L678
	movq	-1(%rdx), %rcx
	cmpw	$123, 11(%rcx)
	jne	.L678
	movl	$24, %ecx
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	addl	$1, %ebx
	movq	%rsi, -72(%rbp)
	addq	$8, %r12
	call	_ZN2v88internal24ObjectStatsCollectorImpl52RecordVirtualObjectsForConstantPoolOrEmbeddedObjectsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rsi
	cmpl	%ebx, 4(%rax)
	jg	.L680
	.p2align 4,,10
	.p2align 3
.L681:
	movq	23(%r15), %rax
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-64(%rbp), %rdx
	movq	%r15, %rsi
	pushq	$0
	movl	$19, %ecx
	xorl	%r9d, %r9d
	movslq	%eax, %r8
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	movq	31(%r15), %rax
	popq	%rcx
	popq	%rsi
	testb	$1, %al
	jne	.L697
.L675:
	movq	%r15, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	leaq	-37592(%rdx), %rcx
	cmpq	-37280(%rdx), %rax
	je	.L698
	movq	7(%rax), %rax
	leaq	-1(%rax), %rdx
.L686:
	movq	%rax, -64(%rbp)
	movq	(%rdx), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	$0
	movslq	%eax, %r8
	movl	$76, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rax
	popq	%rdx
.L673:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L699
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L697:
	.cfi_restore_state
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	leaq	-37592(%rdx), %rcx
	cmpq	-37504(%rdx), %rax
	je	.L673
	cmpq	312(%rcx), %rax
	je	.L673
	movq	-1(%rax), %rcx
	leaq	-1(%rax), %rdx
	cmpw	$70, 11(%rcx)
	jne	.L675
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L698:
	movq	976(%rcx), %rax
	leaq	-1(%rax), %rdx
	jmp	.L686
.L699:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21717:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl33RecordVirtualBytecodeArrayDetailsENS0_13BytecodeArrayE, .-_ZN2v88internal24ObjectStatsCollectorImpl33RecordVirtualBytecodeArrayDetailsENS0_13BytecodeArrayE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualCodeDetailsENS0_4CodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualCodeDetailsENS0_4CodeE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualCodeDetailsENS0_4CodeE, @function
_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualCodeDetailsENS0_4CodeE:
.LFB21719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	43(%rsi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	shrl	%r14d
	andl	$31, %r14d
	cmpl	$11, %r14d
	ja	.L701
	movq	%rsi, %rbx
	leaq	-128(%rbp), %r12
	movq	%rsi, -128(%rbp)
	leaq	43(%rsi), %r15
	movq	-1(%rsi), %rsi
	movq	%rdi, %r13
	movq	%r12, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-128(%rbp), %rdx
	movl	%r14d, %ecx
	pushq	$0
	xorl	%r9d, %r9d
	movslq	%eax, %r8
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	15(%rbx), %r14
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	movq	15(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-128(%rbp), %rdx
	xorl	%r9d, %r9d
	movq	%rbx, %rsi
	movslq	%eax, %r8
	movl	$21, %ecx
	movq	%r13, %rdi
	movl	$0, (%rsp)
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	movq	7(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-128(%rbp), %rdx
	xorl	%r9d, %r9d
	movq	%rbx, %rsi
	movslq	%eax, %r8
	movl	$63, %ecx
	movq	%r13, %rdi
	movl	$0, (%rsp)
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	movq	23(%rbx), %rax
	popq	%r8
	popq	%r9
	testb	$1, %al
	jne	.L716
.L702:
	testb	$62, (%r15)
	je	.L717
.L705:
	movl	$12, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	cmpb	$0, -72(%rbp)
	je	.L710
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L708:
	movq	%r12, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -72(%rbp)
	jne	.L700
.L710:
	movq	-112(%rbp), %rax
	movq	(%rax), %rdx
	testb	$1, %dl
	je	.L708
	movq	-1(%rdx), %rax
	cmpw	$123, 11(%rax)
	jne	.L708
	movl	$24, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl52RecordVirtualObjectsForConstantPoolOrEmbeddedObjectsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE
	movq	%r12, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -72(%rbp)
	je	.L710
	.p2align 4,,10
	.p2align 3
.L700:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L718
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L717:
	.cfi_restore_state
	movq	(%r14), %r14
	movl	11(%r14), %ecx
	testl	%ecx, %ecx
	jle	.L705
	movq	31(%r14), %rax
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-128(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	$0
	movslq	%eax, %r8
	movl	$56, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rax
	popq	%rdx
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L716:
	movq	-1(%rax), %rdx
	cmpw	$97, 11(%rdx)
	jne	.L715
	movq	7(%rax), %rax
.L715:
	movq	%rax, -128(%rbp)
	movq	-1(%rax), %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-128(%rbp), %rdx
	movq	%rbx, %rsi
	pushq	$0
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	movslq	%eax, %r8
	movl	$76, %ecx
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rsi
	popq	%rdi
	jmp	.L702
.L701:
	leaq	.LC317(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L718:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21719:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualCodeDetailsENS0_4CodeE, .-_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualCodeDetailsENS0_4CodeE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl40RecordVirtualArrayBoilerplateDescriptionENS0_27ArrayBoilerplateDescriptionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl40RecordVirtualArrayBoilerplateDescriptionENS0_27ArrayBoilerplateDescriptionE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl40RecordVirtualArrayBoilerplateDescriptionENS0_27ArrayBoilerplateDescriptionE, @function
_ZN2v88internal24ObjectStatsCollectorImpl40RecordVirtualArrayBoilerplateDescriptionENS0_27ArrayBoilerplateDescriptionE:
.LFB21715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-64(%rbp), %rdi
	subq	$24, %rsp
	movq	15(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, -64(%rbp)
	movq	-1(%r13), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	$0
	movl	$12, %ecx
	movslq	%eax, %r8
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	jne	.L730
.L719:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L731
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L730:
	.cfi_restore_state
	movq	-1(%r13), %rax
	cmpw	$123, 11(%rax)
	jne	.L719
	movl	11(%r13), %eax
	leaq	7(%r13), %r15
	testl	%eax, %eax
	jle	.L719
	leaq	15(%r13), %r12
	xorl	%r14d, %r14d
.L723:
	movq	(%r12), %rdx
	testb	$1, %dl
	je	.L721
.L732:
	movl	$12, %ecx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	addl	$1, %r14d
	call	_ZN2v88internal24ObjectStatsCollectorImpl52RecordVirtualObjectsForConstantPoolOrEmbeddedObjectsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE
	addq	$8, %r12
	cmpl	%r14d, 4(%r15)
	jle	.L719
	movq	(%r12), %rdx
	testb	$1, %dl
	jne	.L732
.L721:
	addl	$1, %r14d
	addq	$8, %r12
	cmpl	%r14d, 4(%r15)
	jg	.L723
	jmp	.L719
.L731:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21715:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl40RecordVirtualArrayBoilerplateDescriptionENS0_27ArrayBoilerplateDescriptionE, .-_ZN2v88internal24ObjectStatsCollectorImpl40RecordVirtualArrayBoilerplateDescriptionENS0_27ArrayBoilerplateDescriptionE
	.section	.text._ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm,"axG",@progbits,_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm
	.type	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm, @function
_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm:
.LFB26531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L734
	movq	(%rbx), %r8
.L735:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L744
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L745:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L734:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L758
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L759
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L737:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L739
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L741:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L742:
	testq	%rsi, %rsi
	je	.L739
.L740:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L741
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L747
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L740
	.p2align 4,,10
	.p2align 3
.L739:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r10
	je	.L743
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L743:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L744:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L746
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L746:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L747:
	movq	%rdx, %rdi
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L758:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L737
.L759:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE26531:
	.size	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm, .-_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl27RecordExternalResourceStatsEmNS0_11ObjectStats19VirtualInstanceTypeEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl27RecordExternalResourceStatsEmNS0_11ObjectStats19VirtualInstanceTypeEm
	.type	_ZN2v88internal24ObjectStatsCollectorImpl27RecordExternalResourceStatsEmNS0_11ObjectStats19VirtualInstanceTypeEm, @function
_ZN2v88internal24ObjectStatsCollectorImpl27RecordExternalResourceStatsEmNS0_11ObjectStats19VirtualInstanceTypeEm:
.LFB21697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edx, %r14
	xorl	%edx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	88(%rdi), %rcx
	divq	%rcx
	movq	80(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r15
	testq	%rax, %rax
	je	.L766
	movq	(%rax), %rsi
	movq	8(%rsi), %rdi
	movq	%rsi, %r8
	movq	%rdi, %r9
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L778:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L767
	movq	8(%r8), %r9
	xorl	%edx, %edx
	movq	%r9, %rax
	divq	%rcx
	cmpq	%rdx, %r15
	jne	.L767
.L765:
	cmpq	%r9, %r12
	jne	.L778
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L779:
	.cfi_restore_state
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L766
	movq	8(%rsi), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r15
	jne	.L766
.L767:
	cmpq	%r12, %rdi
	jne	.L779
.L762:
	movq	8(%rbx), %rcx
	leal	1106(%r14), %edx
	leaq	(%rcx,%r14,8), %rax
	addq	$1, 8856(%rax)
	addq	%r13, 27816(%rax)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L768
	bsrq	%r13, %r13
	movl	$59, %eax
	movl	$0, %esi
	xorq	$63, %r13
	subl	%r13d, %eax
	cmovs	%esi, %eax
	movl	$15, %esi
	cmpl	$15, %eax
	cmovg	%esi, %eax
.L768:
	movslq	%edx, %rdx
	cltq
	salq	$4, %rdx
	addq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rax
	addq	$1, 47408(%rax)
	addq	$1, 199088(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L766:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	leaq	80(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r12, 8(%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	$0, (%rax)
	call	_ZNSt10_HashtableImmSaImENSt8__detail9_IdentityESt8equal_toImESt4hashImENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeImLb0EEEm
	jmp	.L762
	.cfi_endproc
.LFE21697:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl27RecordExternalResourceStatsEmNS0_11ObjectStats19VirtualInstanceTypeEm, .-_ZN2v88internal24ObjectStatsCollectorImpl27RecordExternalResourceStatsEmNS0_11ObjectStats19VirtualInstanceTypeEm
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl26RecordVirtualScriptDetailsENS0_6ScriptE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl26RecordVirtualScriptDetailsENS0_6ScriptE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl26RecordVirtualScriptDetailsENS0_6ScriptE, @function
_ZN2v88internal24ObjectStatsCollectorImpl26RecordVirtualScriptDetailsENS0_6ScriptE:
.LFB21712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-48(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	87(%rsi), %rax
	movq	%rax, -48(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-48(%rbp), %rdx
	movq	%r12, %rsi
	pushq	$0
	movl	$66, %ecx
	xorl	%r9d, %r9d
	movslq	%eax, %r8
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	movq	7(%r12), %rax
	popq	%rcx
	popq	%rsi
	testb	$1, %al
	jne	.L792
.L780:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L793
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L792:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L794
.L783:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L780
	movq	-1(%rax), %rdx
	xorl	%r15d, %r15d
	movq	%r14, %rdi
	testb	$8, 11(%rdx)
	movq	%rax, -48(%rbp)
	movq	-1(%rax), %rsi
	sete	%r15b
	addl	$69, %r15d
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	subq	$8, %rsp
	movq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	$0
	movslq	%eax, %r8
	movl	%r15d, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualObjectStatsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeEmmNS1_7CowModeE
	popq	%rax
	popq	%rdx
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L794:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$2, %dx
	jne	.L783
	movq	%r14, %rdi
	movq	%rax, -48(%rbp)
	movq	15(%rax), %r12
	call	_ZNK2v88internal14ExternalString19ExternalPayloadSizeEv@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movslq	%eax, %rcx
	movq	-48(%rbp), %rax
	movq	%r12, %rsi
	movq	-1(%rax), %rax
	testb	$8, 11(%rax)
	sete	%dl
	addl	$67, %edx
	call	_ZN2v88internal24ObjectStatsCollectorImpl27RecordExternalResourceStatsEmNS0_11ObjectStats19VirtualInstanceTypeEm
	jmp	.L780
.L793:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21712:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl26RecordVirtualScriptDetailsENS0_6ScriptE, .-_ZN2v88internal24ObjectStatsCollectorImpl26RecordVirtualScriptDetailsENS0_6ScriptE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualExternalStringDetailsENS0_14ExternalStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualExternalStringDetailsENS0_14ExternalStringE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualExternalStringDetailsENS0_14ExternalStringE, @function
_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualExternalStringDetailsENS0_14ExternalStringE:
.LFB21713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	-24(%rbp), %rdi
	subq	$16, %rsp
	movq	%rsi, -24(%rbp)
	movq	15(%rsi), %r13
	call	_ZNK2v88internal14ExternalString19ExternalPayloadSizeEv@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movslq	%eax, %rcx
	movq	-24(%rbp), %rax
	movq	%r13, %rsi
	movq	-1(%rax), %rax
	testb	$8, 11(%rax)
	sete	%dl
	addl	$74, %edx
	call	_ZN2v88internal24ObjectStatsCollectorImpl27RecordExternalResourceStatsEmNS0_11ObjectStats19VirtualInstanceTypeEm
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21713:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualExternalStringDetailsENS0_14ExternalStringE, .-_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualExternalStringDetailsENS0_14ExternalStringE
	.section	.text._ZN2v88internal24ObjectStatsCollectorImpl17CollectStatisticsENS0_10HeapObjectENS1_5PhaseENS1_17CollectFieldStatsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ObjectStatsCollectorImpl17CollectStatisticsENS0_10HeapObjectENS1_5PhaseENS1_17CollectFieldStatsE
	.type	_ZN2v88internal24ObjectStatsCollectorImpl17CollectStatisticsENS0_10HeapObjectENS1_5PhaseENS1_17CollectFieldStatsE, @function
_ZN2v88internal24ObjectStatsCollectorImpl17CollectStatisticsENS0_10HeapObjectENS1_5PhaseENS1_17CollectFieldStatsE:
.LFB21705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%rsi), %r14
	testl	%edx, %edx
	je	.L800
	cmpl	$1, %edx
	jne	.L799
	movq	-1(%rsi), %rax
	movl	%ecx, %ebx
	cmpw	$63, 11(%rax)
	jbe	.L831
.L816:
	movq	-1(%rsi), %rax
	xorl	%r13d, %r13d
	cmpw	$1024, 11(%rax)
	ja	.L832
.L819:
	movq	-1(%rsi), %rsi
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movzwl	11(%r14), %edx
	movq	%r13, %r8
	movq	%r12, %rdi
	movq	-72(%rbp), %rsi
	movslq	%eax, %rcx
	call	_ZN2v88internal24ObjectStatsCollectorImpl17RecordObjectStatsENS0_10HeapObjectENS0_12InstanceTypeEmm
	cmpl	$1, %ebx
	je	.L833
.L799:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L834
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L800:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	cmpw	$155, 11(%rax)
	je	.L835
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	je	.L836
	movq	-1(%rsi), %rax
	cmpw	$72, 11(%rax)
	je	.L837
	movq	-1(%rsi), %rax
	cmpw	$69, 11(%rax)
	je	.L838
	movq	-1(%rsi), %rax
	cmpw	$88, 11(%rax)
	je	.L839
	movq	-1(%rsi), %rax
	cmpw	$1025, 11(%rax)
	je	.L840
	movq	-1(%rsi), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L809
	call	_ZN2v88internal24ObjectStatsCollectorImpl28RecordVirtualJSObjectDetailsENS0_8JSObjectE
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L831:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	jne	.L816
	leaq	-64(%rbp), %rdi
	movq	%rsi, -64(%rbp)
	movq	15(%rsi), %r13
	call	_ZNK2v88internal14ExternalString19ExternalPayloadSizeEv@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movslq	%eax, %rcx
	movq	-64(%rbp), %rax
	movq	%r13, %rsi
	movq	-1(%rax), %rax
	testb	$8, 11(%rax)
	sete	%dl
	addl	$74, %edx
	call	_ZN2v88internal24ObjectStatsCollectorImpl27RecordExternalResourceStatsEmNS0_11ObjectStats19VirtualInstanceTypeEm
	movq	-72(%rbp), %rsi
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L836:
	call	_ZN2v88internal24ObjectStatsCollectorImpl23RecordVirtualMapDetailsENS0_3MapE
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L833:
	movq	-72(%rbp), %rax
	leaq	-64(%rbp), %rbx
	leaq	136(%r12), %r15
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rax, -64(%rbp)
	movq	200(%r12), %rax
	movq	(%rax), %r13
	call	_ZN2v88internal10HeapObject7IterateEPNS0_13ObjectVisitorE@PLT
	movq	200(%r12), %rax
	movq	%rbx, %rdi
	movq	(%rax), %r14
	movq	-64(%rbp), %rax
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movl	%eax, %ebx
	leal	7(%rax), %eax
	testl	%ebx, %ebx
	cmovns	%ebx, %eax
	subq	%r14, %r13
	sarl	$3, %eax
	cltq
	addq	%rax, %r13
	movq	-64(%rbp), %rax
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	ja	.L841
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L825
	movq	232(%r12), %rax
	subq	$1, %r13
	addq	$1, (%rax)
.L824:
	movq	248(%r12), %rax
	addq	%r13, (%rax)
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L835:
	call	_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualFeedbackVectorDetailsENS0_14FeedbackVectorE
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L832:
	movzbl	7(%r14), %r13d
	movzbl	9(%r14), %eax
	sall	$3, %r13d
	leal	0(,%rax,8), %edx
	cmpl	$2, %eax
	jle	.L842
.L821:
	subl	%edx, %r13d
	movslq	%r13d, %r13
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L841:
	movq	-1(%rax), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19FieldStatsCollector21GetInobjectFieldStatsENS0_3MapE
	movq	200(%r12), %rsi
	movl	%eax, %ecx
	movl	%eax, %edx
	andl	$1023, %eax
	shrl	$10, %ecx
	subq	%rax, (%rsi)
	movq	208(%r12), %rsi
	shrl	$20, %edx
	addq	%rax, (%rsi)
	movl	%ecx, %eax
	movq	200(%r12), %rcx
	andl	$1023, %eax
	subq	%rax, (%rcx)
	movq	216(%r12), %rcx
	addq	%rax, (%rcx)
	movl	%edx, %eax
	movq	224(%r12), %rdx
	andl	$1023, %eax
	subq	%rax, %r13
	addq	%rax, (%rdx)
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L839:
	call	_ZN2v88internal24ObjectStatsCollectorImpl40RecordVirtualFunctionTemplateInfoDetailsENS0_20FunctionTemplateInfoE
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L837:
	call	_ZN2v88internal24ObjectStatsCollectorImpl33RecordVirtualBytecodeArrayDetailsENS0_13BytecodeArrayE
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L842:
	movzbl	7(%r14), %edx
	sall	$3, %edx
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L838:
	call	_ZN2v88internal24ObjectStatsCollectorImpl24RecordVirtualCodeDetailsENS0_4CodeE
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L825:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L824
	movq	-1(%rax), %rdx
	testb	$7, 11(%rdx)
	jne	.L824
	movl	11(%rax), %edx
	movq	-64(%rbp), %rax
	movq	-1(%rax), %rcx
	leal	(%rdx,%rdx), %eax
	testb	$8, 11(%rcx)
	cmove	%eax, %edx
	testl	%edx, %edx
	leal	7(%rdx), %eax
	cmovns	%edx, %eax
	movq	240(%r12), %rdx
	sarl	$3, %eax
	cltq
	addq	%rax, (%rdx)
	subq	%rax, %r13
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L840:
	call	_ZN2v88internal24ObjectStatsCollectorImpl34RecordVirtualJSGlobalObjectDetailsENS0_14JSGlobalObjectE
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L809:
	movq	-1(%rsi), %rax
	cmpw	$160, 11(%rax)
	je	.L843
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subw	$138, %ax
	cmpw	$9, %ax
	ja	.L811
	movq	-72(%rbp), %rsi
	call	_ZN2v88internal24ObjectStatsCollectorImpl20RecordVirtualContextENS0_7ContextE
	jmp	.L799
.L843:
	call	_ZN2v88internal24ObjectStatsCollectorImpl38RecordVirtualSharedFunctionInfoDetailsENS0_18SharedFunctionInfoE
	jmp	.L799
.L811:
	movq	-1(%rsi), %rax
	cmpw	$96, 11(%rax)
	je	.L844
	movq	-1(%rsi), %rax
	cmpw	$82, 11(%rax)
	je	.L845
	movq	-1(%rsi), %rax
	cmpw	$123, 11(%rax)
	jne	.L799
	call	_ZN2v88internal24ObjectStatsCollectorImpl30RecordVirtualFixedArrayDetailsENS0_10FixedArrayE
	jmp	.L799
.L844:
	call	_ZN2v88internal24ObjectStatsCollectorImpl26RecordVirtualScriptDetailsENS0_6ScriptE
	jmp	.L799
.L845:
	movq	15(%rsi), %rdx
	movl	$12, %ecx
	call	_ZN2v88internal24ObjectStatsCollectorImpl52RecordVirtualObjectsForConstantPoolOrEmbeddedObjectsENS0_10HeapObjectES2_NS0_11ObjectStats19VirtualInstanceTypeE
	jmp	.L799
.L834:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21705:
	.size	_ZN2v88internal24ObjectStatsCollectorImpl17CollectStatisticsENS0_10HeapObjectENS1_5PhaseENS1_17CollectFieldStatsE, .-_ZN2v88internal24ObjectStatsCollectorImpl17CollectStatisticsENS0_10HeapObjectENS1_5PhaseENS1_17CollectFieldStatsE
	.section	.text._ZN2v88internal20ObjectStatsCollector7CollectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20ObjectStatsCollector7CollectEv
	.type	_ZN2v88internal20ObjectStatsCollector7CollectEv, @function
_ZN2v88internal20ObjectStatsCollector7CollectEv:
.LFB21729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-504(%rbp), %rsi
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-576(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-648(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16+_ZTVN2v88internal19FieldStatsCollectorE(%rip), %rbx
	subq	$680, %rsp
	movq	(%rdi), %rcx
	movdqu	(%rdi), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movss	.LC316(%rip), %xmm0
	movq	8(%rdi), %rax
	movq	%rsi, -704(%rbp)
	movq	2016(%rcx), %rdx
	movq	%rsi, -552(%rbp)
	leaq	-448(%rbp), %rsi
	movq	%rsi, -672(%rbp)
	addq	$9993, %rdx
	movq	%rsi, -496(%rbp)
	leaq	-384(%rbp), %rsi
	movq	%rdx, -560(%rbp)
	movaps	%xmm1, -576(%rbp)
	movss	%xmm0, -520(%rbp)
	movss	%xmm0, -464(%rbp)
	movq	$1, -544(%rbp)
	movq	$0, -536(%rbp)
	movq	$0, -528(%rbp)
	movq	$0, -512(%rbp)
	movq	$0, -504(%rbp)
	movq	$1, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	$0, -472(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rbx, -440(%rbp)
	movq	%rsi, -688(%rbp)
	movq	%rsi, -432(%rbp)
	leaq	350768(%rax), %rsi
	movq	%rsi, -376(%rbp)
	leaq	350776(%rax), %rsi
	movq	%rsi, -368(%rbp)
	leaq	350784(%rax), %rsi
	movq	%rsi, -360(%rbp)
	leaq	350792(%rax), %rsi
	movq	%rsi, -352(%rbp)
	leaq	350800(%rax), %rsi
	movq	%rsi, -344(%rbp)
	leaq	350808(%rax), %rsi
	addq	$350816, %rax
	movq	%rax, -328(%rbp)
	movq	16(%rdi), %rax
	movq	%r15, %rdi
	movq	%rcx, -320(%rbp)
	leaq	-248(%rbp), %rcx
	movq	%rdx, -304(%rbp)
	leaq	350768(%rax), %rdx
	movq	%rcx, -712(%rbp)
	movq	%rcx, -296(%rbp)
	leaq	-192(%rbp), %rcx
	movq	%rsi, -336(%rbp)
	movq	%rax, -312(%rbp)
	movq	%rcx, -680(%rbp)
	movq	%rcx, -240(%rbp)
	leaq	-128(%rbp), %rcx
	movss	%xmm0, -400(%rbp)
	movss	%xmm0, -264(%rbp)
	movq	$1, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	$0, -408(%rbp)
	movq	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	movq	$1, -288(%rbp)
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	$0, -256(%rbp)
	movq	$0, -248(%rbp)
	movq	$1, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movss	%xmm0, -208(%rbp)
	movq	%rdx, -120(%rbp)
	leaq	350776(%rax), %rdx
	movq	%rdx, -112(%rbp)
	leaq	350784(%rax), %rdx
	movq	%rdx, -104(%rbp)
	leaq	350792(%rax), %rdx
	movq	%rdx, -96(%rbp)
	leaq	350800(%rax), %rdx
	movq	%rdx, -88(%rbp)
	leaq	350808(%rax), %rdx
	addq	$350816, %rax
	movq	%rbx, -184(%rbp)
	leaq	-640(%rbp), %rbx
	movq	$0, -200(%rbp)
	movq	$0, -192(%rbp)
	movq	%rcx, -696(%rbp)
	movq	%rcx, -176(%rbp)
	movq	$1, -168(%rbp)
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movss	%xmm0, -144(%rbp)
	call	_ZN2v88internal24ObjectStatsCollectorImpl23CollectGlobalStatisticsEv
	movq	%r13, -664(%rbp)
.L849:
	movq	-664(%rbp), %rax
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r13d
	movq	(%rax), %rsi
	call	_ZN2v88internal26CombinedHeapObjectIteratorC1EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal26CombinedHeapObjectIterator4NextEv@PLT
	movq	%rax, -648(%rbp)
	testq	%rax, %rax
	jne	.L847
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L850:
	leaq	-320(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	%r12d, %edx
	call	_ZN2v88internal24ObjectStatsCollectorImpl17CollectStatisticsENS0_10HeapObjectENS1_5PhaseENS1_17CollectFieldStatsE
.L852:
	movq	%rbx, %rdi
	call	_ZN2v88internal26CombinedHeapObjectIterator4NextEv@PLT
	movq	%rax, -648(%rbp)
	testq	%rax, %rax
	je	.L853
.L847:
	movq	-1(%rax), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	-648(%rbp), %rsi
	movl	%r13d, %edx
	movl	%esi, %eax
	movq	%rsi, %rdi
	andl	$262143, %eax
	andq	$-262144, %rdi
	movl	%eax, %ecx
	shrl	$8, %eax
	shrl	$3, %ecx
	sall	%cl, %edx
	movq	16(%rdi), %rcx
	leaq	(%rcx,%rax,4), %rcx
	movl	(%rcx), %eax
	testl	%eax, %edx
	je	.L850
	addl	%edx, %edx
	jne	.L851
	movl	4(%rcx), %eax
	movl	$1, %edx
.L851:
	testl	%eax, %edx
	je	.L850
	movl	$1, %ecx
	movl	%r12d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal24ObjectStatsCollectorImpl17CollectStatisticsENS0_10HeapObjectENS1_5PhaseENS1_17CollectFieldStatsE
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L853:
	movq	%rbx, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	cmpl	$1, %r12d
	je	.L848
	movl	$1, %r12d
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L848:
	movq	-160(%rbp), %rbx
	leaq	16+_ZTVN2v88internal19FieldStatsCollectorE(%rip), %rax
	movq	%rax, -184(%rbp)
	testq	%rbx, %rbx
	je	.L857
	.p2align 4,,10
	.p2align 3
.L854:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L854
.L857:
	movq	-168(%rbp), %rax
	movq	-176(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-176(%rbp), %rdi
	movq	$0, -152(%rbp)
	movq	$0, -160(%rbp)
	cmpq	-696(%rbp), %rdi
	je	.L855
	call	_ZdlPv@PLT
.L855:
	movq	-224(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L861
	.p2align 4,,10
	.p2align 3
.L858:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L858
.L861:
	movq	-232(%rbp), %rax
	movq	-240(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-240(%rbp), %rdi
	movq	$0, -216(%rbp)
	movq	$0, -224(%rbp)
	cmpq	-680(%rbp), %rdi
	je	.L859
	call	_ZdlPv@PLT
.L859:
	movq	-280(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L865
	.p2align 4,,10
	.p2align 3
.L862:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L862
.L865:
	movq	-288(%rbp), %rax
	movq	-296(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-296(%rbp), %rdi
	movq	$0, -272(%rbp)
	movq	$0, -280(%rbp)
	cmpq	-712(%rbp), %rdi
	je	.L863
	call	_ZdlPv@PLT
.L863:
	movq	-416(%rbp), %rbx
	leaq	16+_ZTVN2v88internal19FieldStatsCollectorE(%rip), %rax
	movq	%rax, -440(%rbp)
	testq	%rbx, %rbx
	je	.L869
	.p2align 4,,10
	.p2align 3
.L866:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L866
.L869:
	movq	-424(%rbp), %rax
	movq	-432(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-432(%rbp), %rdi
	movq	$0, -408(%rbp)
	movq	$0, -416(%rbp)
	cmpq	-688(%rbp), %rdi
	je	.L867
	call	_ZdlPv@PLT
.L867:
	movq	-480(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L873
	.p2align 4,,10
	.p2align 3
.L870:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L870
.L873:
	movq	-488(%rbp), %rax
	movq	-496(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-496(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	$0, -480(%rbp)
	cmpq	-672(%rbp), %rdi
	je	.L871
	call	_ZdlPv@PLT
.L871:
	movq	-536(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L877
	.p2align 4,,10
	.p2align 3
.L874:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L874
.L877:
	movq	-544(%rbp), %rax
	movq	-552(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-552(%rbp), %rdi
	movq	$0, -528(%rbp)
	movq	$0, -536(%rbp)
	cmpq	-704(%rbp), %rdi
	je	.L846
	call	_ZdlPv@PLT
.L846:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L926
	addq	$680, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L926:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21729:
	.size	_ZN2v88internal20ObjectStatsCollector7CollectEv, .-_ZN2v88internal20ObjectStatsCollector7CollectEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal19FieldStatsCollector21GetInobjectFieldStatsENS0_3MapE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal19FieldStatsCollector21GetInobjectFieldStatsENS0_3MapE, @function
_GLOBAL__sub_I__ZN2v88internal19FieldStatsCollector21GetInobjectFieldStatsENS0_3MapE:
.LFB27222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27222:
	.size	_GLOBAL__sub_I__ZN2v88internal19FieldStatsCollector21GetInobjectFieldStatsENS0_3MapE, .-_GLOBAL__sub_I__ZN2v88internal19FieldStatsCollector21GetInobjectFieldStatsENS0_3MapE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal19FieldStatsCollector21GetInobjectFieldStatsENS0_3MapE
	.weak	_ZTVN2v88internal19FieldStatsCollectorE
	.section	.data.rel.ro._ZTVN2v88internal19FieldStatsCollectorE,"awG",@progbits,_ZTVN2v88internal19FieldStatsCollectorE,comdat
	.align 8
	.type	_ZTVN2v88internal19FieldStatsCollectorE, @object
	.size	_ZTVN2v88internal19FieldStatsCollectorE, 152
_ZTVN2v88internal19FieldStatsCollectorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal19FieldStatsCollectorD1Ev
	.quad	_ZN2v88internal19FieldStatsCollectorD0Ev
	.quad	_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal19FieldStatsCollector13VisitPointersENS0_10HeapObjectENS0_19FullMaybeObjectSlotES3_
	.quad	_ZN2v88internal13ObjectVisitor23VisitCustomWeakPointersENS0_10HeapObjectENS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor12VisitPointerENS0_10HeapObjectENS0_19FullMaybeObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor22VisitCustomWeakPointerENS0_10HeapObjectENS0_14FullObjectSlotE
	.quad	_ZN2v88internal13ObjectVisitor14VisitEphemeronENS0_10HeapObjectEiNS0_14FullObjectSlotES3_
	.quad	_ZN2v88internal19FieldStatsCollector15VisitCodeTargetENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal19FieldStatsCollector20VisitEmbeddedPointerENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor17VisitRuntimeEntryENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor22VisitExternalReferenceENS0_7ForeignEPm
	.quad	_ZN2v88internal13ObjectVisitor22VisitInternalReferenceENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor18VisitOffHeapTargetENS0_4CodeEPNS0_9RelocInfoE
	.quad	_ZN2v88internal13ObjectVisitor14VisitRelocInfoEPNS0_13RelocIteratorE
	.section	.bss._ZN2v88internalL18object_stats_mutexE,"aw",@nobits
	.align 32
	.type	_ZN2v88internalL18object_stats_mutexE, @object
	.size	_ZN2v88internalL18object_stats_mutexE, 48
_ZN2v88internalL18object_stats_mutexE:
	.zero	48
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC316:
	.long	1065353216
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
