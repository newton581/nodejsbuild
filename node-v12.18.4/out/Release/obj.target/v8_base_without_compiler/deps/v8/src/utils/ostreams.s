	.file	"ostreams.cc"
	.text
	.section	.text._ZN2v88internal12DbgStreamBuf8overflowEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12DbgStreamBuf8overflowEi
	.type	_ZN2v88internal12DbgStreamBuf8overflowEi, @function
_ZN2v88internal12DbgStreamBuf8overflowEi:
.LFB5931:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5931:
	.size	_ZN2v88internal12DbgStreamBuf8overflowEi, .-_ZN2v88internal12DbgStreamBuf8overflowEi
	.section	.text._ZN2v88internal12DbgStreamBuf4syncEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12DbgStreamBuf4syncEv
	.type	_ZN2v88internal12DbgStreamBuf4syncEv, @function
_ZN2v88internal12DbgStreamBuf4syncEv:
.LFB5932:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5932:
	.size	_ZN2v88internal12DbgStreamBuf4syncEv, .-_ZN2v88internal12DbgStreamBuf4syncEv
	.section	.text._ZN2v88internal12DbgStreamBufD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12DbgStreamBufD2Ev
	.type	_ZN2v88internal12DbgStreamBufD2Ev, @function
_ZN2v88internal12DbgStreamBufD2Ev:
.LFB5928:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	addq	$56, %rdi
	movq	%rax, -56(%rdi)
	jmp	_ZNSt6localeD1Ev@PLT
	.cfi_endproc
.LFE5928:
	.size	_ZN2v88internal12DbgStreamBufD2Ev, .-_ZN2v88internal12DbgStreamBufD2Ev
	.globl	_ZN2v88internal12DbgStreamBufD1Ev
	.set	_ZN2v88internal12DbgStreamBufD1Ev,_ZN2v88internal12DbgStreamBufD2Ev
	.section	.text._ZN2v88internal12DbgStreamBufD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12DbgStreamBufD0Ev
	.type	_ZN2v88internal12DbgStreamBufD0Ev, @function
_ZN2v88internal12DbgStreamBufD0Ev:
.LFB5930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	56(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -56(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$320, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5930:
	.size	_ZN2v88internal12DbgStreamBufD0Ev, .-_ZN2v88internal12DbgStreamBufD0Ev
	.section	.text._ZN2v88internal12OFStreamBaseD2Ev,"axG",@progbits,_ZN2v88internal12OFStreamBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12OFStreamBaseD2Ev
	.type	_ZN2v88internal12OFStreamBaseD2Ev, @function
_ZN2v88internal12OFStreamBaseD2Ev:
.LFB6795:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	addq	$56, %rdi
	movq	%rax, -56(%rdi)
	jmp	_ZNSt6localeD1Ev@PLT
	.cfi_endproc
.LFE6795:
	.size	_ZN2v88internal12OFStreamBaseD2Ev, .-_ZN2v88internal12OFStreamBaseD2Ev
	.weak	_ZN2v88internal12OFStreamBaseD1Ev
	.set	_ZN2v88internal12OFStreamBaseD1Ev,_ZN2v88internal12OFStreamBaseD2Ev
	.section	.text._ZN2v88internal12OFStreamBaseD0Ev,"axG",@progbits,_ZN2v88internal12OFStreamBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12OFStreamBaseD0Ev
	.type	_ZN2v88internal12OFStreamBaseD0Ev, @function
_ZN2v88internal12OFStreamBaseD0Ev:
.LFB6797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	56(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -56(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6797:
	.size	_ZN2v88internal12OFStreamBaseD0Ev, .-_ZN2v88internal12OFStreamBaseD0Ev
	.section	.text._ZN2v88internal15DbgStdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal15DbgStdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15DbgStdoutStreamD1Ev
	.type	_ZN2v88internal15DbgStdoutStreamD1Ev, @function
_ZN2v88internal15DbgStdoutStreamD1Ev:
.LFB6804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal15DbgStdoutStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal15DbgStdoutStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 264(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal15DbgStdoutStreamE0_So(%rip), %rax
	leaq	328(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 328(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE6804:
	.size	_ZN2v88internal15DbgStdoutStreamD1Ev, .-_ZN2v88internal15DbgStdoutStreamD1Ev
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD1Ev
	.type	_ZN2v88internal8OFStreamD1Ev, @function
_ZN2v88internal8OFStreamD1Ev:
.LFB3143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE3143:
	.size	_ZN2v88internal8OFStreamD1Ev, .-_ZN2v88internal8OFStreamD1Ev
	.section	.text._ZN2v88internal12OFStreamBase4syncEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12OFStreamBase4syncEv
	.type	_ZN2v88internal12OFStreamBase4syncEv, @function
_ZN2v88internal12OFStreamBase4syncEv:
.LFB5939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	64(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	fflush@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5939:
	.size	_ZN2v88internal12OFStreamBase4syncEv, .-_ZN2v88internal12OFStreamBase4syncEv
	.section	.text._ZN2v88internal12OFStreamBase6xsputnEPKcl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12OFStreamBase6xsputnEPKcl
	.type	_ZN2v88internal12OFStreamBase6xsputnEPKcl, @function
_ZN2v88internal12OFStreamBase6xsputnEPKcl:
.LFB5941:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movl	$1, %esi
	movq	64(%r8), %rcx
	jmp	fwrite@PLT
	.cfi_endproc
.LFE5941:
	.size	_ZN2v88internal12OFStreamBase6xsputnEPKcl, .-_ZN2v88internal12OFStreamBase6xsputnEPKcl
	.section	.text._ZN2v88internal12OFStreamBase8overflowEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12OFStreamBase8overflowEi
	.type	_ZN2v88internal12OFStreamBase8overflowEi, @function
_ZN2v88internal12OFStreamBase8overflowEi:
.LFB5940:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movl	%esi, %edi
	cmpl	$-1, %esi
	jne	.L19
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	64(%rax), %rsi
	jmp	fputc@PLT
	.cfi_endproc
.LFE5940:
	.size	_ZN2v88internal12OFStreamBase8overflowEi, .-_ZN2v88internal12OFStreamBase8overflowEi
	.section	.text._ZN2v88internal15DbgStdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal15DbgStdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal15DbgStdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal15DbgStdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal15DbgStdoutStreamD0Ev:
.LFB6836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal15DbgStdoutStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTVN2v88internal15DbgStdoutStreamE(%rip), %rax
	movq	%rax, 328(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal15DbgStdoutStreamE0_So(%rip), %rax
	leaq	328(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 328(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$592, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6836:
	.size	_ZTv0_n24_N2v88internal15DbgStdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal15DbgStdoutStreamD0Ev
	.section	.text._ZN2v88internal8OFStreamD0Ev,"axG",@progbits,_ZN2v88internal8OFStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD0Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD0Ev:
.LFB6837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6837:
	.size	_ZTv0_n24_N2v88internal8OFStreamD0Ev, .-_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD0Ev
	.type	_ZN2v88internal8OFStreamD0Ev, @function
_ZN2v88internal8OFStreamD0Ev:
.LFB3144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE3144:
	.size	_ZN2v88internal8OFStreamD0Ev, .-_ZN2v88internal8OFStreamD0Ev
	.section	.text._ZN2v88internal15DbgStdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal15DbgStdoutStreamD0Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15DbgStdoutStreamD0Ev
	.type	_ZN2v88internal15DbgStdoutStreamD0Ev, @function
_ZN2v88internal15DbgStdoutStreamD0Ev:
.LFB6805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal15DbgStdoutStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal15DbgStdoutStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 264(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal15DbgStdoutStreamE0_So(%rip), %rax
	leaq	328(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 328(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$592, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6805:
	.size	_ZN2v88internal15DbgStdoutStreamD0Ev, .-_ZN2v88internal15DbgStdoutStreamD0Ev
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD1Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD1Ev:
.LFB6838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE6838:
	.size	_ZTv0_n24_N2v88internal8OFStreamD1Ev, .-_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.section	.text._ZN2v88internal15DbgStdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal15DbgStdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal15DbgStdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal15DbgStdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal15DbgStdoutStreamD1Ev:
.LFB6839:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal15DbgStdoutStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTVN2v88internal15DbgStdoutStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 328(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal15DbgStdoutStreamE0_So(%rip), %rax
	leaq	328(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 328(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE6839:
	.size	_ZTv0_n24_N2v88internal15DbgStdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal15DbgStdoutStreamD1Ev
	.section	.text._ZN2v88internal12DbgStreamBufC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12DbgStreamBufC2Ev
	.type	_ZN2v88internal12DbgStreamBufC2Ev, @function
_ZN2v88internal12DbgStreamBufC2Ev:
.LFB5925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	56(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -56(%rdi)
	movq	$0, -48(%rdi)
	movq	$0, -40(%rdi)
	movq	$0, -32(%rdi)
	movq	$0, -24(%rdi)
	movq	$0, -16(%rdi)
	movq	$0, -8(%rdi)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVN2v88internal12DbgStreamBufE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	64(%rbx), %rax
	movq	%rax, 40(%rbx)
	movq	%rax, 32(%rbx)
	leaq	320(%rbx), %rax
	movq	%rax, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5925:
	.size	_ZN2v88internal12DbgStreamBufC2Ev, .-_ZN2v88internal12DbgStreamBufC2Ev
	.globl	_ZN2v88internal12DbgStreamBufC1Ev
	.set	_ZN2v88internal12DbgStreamBufC1Ev,_ZN2v88internal12DbgStreamBufC2Ev
	.section	.text._ZN2v88internal15DbgStdoutStreamC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15DbgStdoutStreamC2Ev
	.type	_ZN2v88internal15DbgStdoutStreamC2Ev, @function
_ZN2v88internal15DbgStdoutStreamC2Ev:
.LFB5934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	8(%rsi), %rax
	movq	%rdi, %rbx
	movq	16(%rsi), %rdx
	leaq	8(%rdi), %rsi
	movq	%rax, (%rdi)
	movq	-24(%rax), %rax
	movq	%rdx, (%rdi,%rax)
	movq	(%rdi), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	(%r12), %rax
	movq	24(%r12), %rdx
	leaq	64(%rbx), %rdi
	movq	%rax, (%rbx)
	movq	-24(%rax), %rax
	movq	%rdx, (%rbx,%rax)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVN2v88internal12DbgStreamBufE(%rip), %rax
	movq	%rax, 8(%rbx)
	leaq	72(%rbx), %rax
	movq	%rax, 48(%rbx)
	movq	%rax, 40(%rbx)
	leaq	328(%rbx), %rax
	movq	%rax, 56(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5934:
	.size	_ZN2v88internal15DbgStdoutStreamC2Ev, .-_ZN2v88internal15DbgStdoutStreamC2Ev
	.section	.text._ZN2v88internal15DbgStdoutStreamC1Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15DbgStdoutStreamC1Ev
	.type	_ZN2v88internal15DbgStdoutStreamC1Ev, @function
_ZN2v88internal15DbgStdoutStreamC1Ev:
.LFB5935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	328(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	leaq	8(%rbx), %rsi
	movw	%ax, 552(%rbx)
	leaq	24+_ZTCN2v88internal15DbgStdoutStreamE0_So(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%rbx)
	addq	$40, %rax
	movq	%rax, 328(%rbx)
	movq	$0, 544(%rbx)
	movups	%xmm0, 560(%rbx)
	movups	%xmm0, 576(%rbx)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVN2v88internal15DbgStdoutStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal15DbgStdoutStreamE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movq	%rax, 328(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdx, %xmm1
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm2
	movups	%xmm0, 16(%rbx)
	punpcklqdq	%xmm2, %xmm1
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm1, (%rbx)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVN2v88internal12DbgStreamBufE(%rip), %rax
	movq	%r12, 56(%rbx)
	movq	%rax, 8(%rbx)
	leaq	72(%rbx), %rax
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5935:
	.size	_ZN2v88internal15DbgStdoutStreamC1Ev, .-_ZN2v88internal15DbgStdoutStreamC1Ev
	.section	.text._ZN2v88internal12OFStreamBaseC2EP8_IO_FILE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12OFStreamBaseC2EP8_IO_FILE
	.type	_ZN2v88internal12OFStreamBaseC2EP8_IO_FILE, @function
_ZN2v88internal12OFStreamBaseC2EP8_IO_FILE:
.LFB5937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	56(%rdi), %rdi
	movq	%rax, -56(%rdi)
	movq	$0, -48(%rdi)
	movq	$0, -40(%rdi)
	movq	$0, -32(%rdi)
	movq	$0, -24(%rdi)
	movq	$0, -16(%rdi)
	movq	$0, -8(%rdi)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVN2v88internal12OFStreamBaseE(%rip), %rax
	movq	%r12, 64(%rbx)
	movq	%rax, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5937:
	.size	_ZN2v88internal12OFStreamBaseC2EP8_IO_FILE, .-_ZN2v88internal12OFStreamBaseC2EP8_IO_FILE
	.globl	_ZN2v88internal12OFStreamBaseC1EP8_IO_FILE
	.set	_ZN2v88internal12OFStreamBaseC1EP8_IO_FILE,_ZN2v88internal12OFStreamBaseC2EP8_IO_FILE
	.section	.text._ZN2v88internal8OFStreamC2EP8_IO_FILE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8OFStreamC2EP8_IO_FILE
	.type	_ZN2v88internal8OFStreamC2EP8_IO_FILE, @function
_ZN2v88internal8OFStreamC2EP8_IO_FILE:
.LFB5943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %rax
	movq	16(%rsi), %rdx
	xorl	%esi, %esi
	movq	%rax, (%rdi)
	movq	-24(%rax), %rax
	movq	%rdx, (%rdi,%rax)
	movq	(%rdi), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	(%r12), %rax
	movq	24(%r12), %rdx
	leaq	64(%rbx), %rdi
	leaq	8(%rbx), %r12
	movq	%rax, (%rbx)
	movq	-24(%rax), %rax
	movq	%rdx, (%rbx,%rax)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVN2v88internal12OFStreamBaseE(%rip), %rax
	movq	%r13, 72(%rbx)
	movq	%r12, %rsi
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	$8, %rsp
	addq	%rbx, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt9basic_iosIcSt11char_traitsIcEE5rdbufEPSt15basic_streambufIcS1_E@PLT
	.cfi_endproc
.LFE5943:
	.size	_ZN2v88internal8OFStreamC2EP8_IO_FILE, .-_ZN2v88internal8OFStreamC2EP8_IO_FILE
	.section	.text._ZN2v88internal8OFStreamC1EP8_IO_FILE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8OFStreamC1EP8_IO_FILE
	.type	_ZN2v88internal8OFStreamC1EP8_IO_FILE, @function
_ZN2v88internal8OFStreamC1EP8_IO_FILE:
.LFB5944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	80(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rdi
	leaq	8(%rbx), %r14
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movw	%ax, 304(%rbx)
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%rbx)
	addq	$40, %rax
	movq	%rax, 80(%rbx)
	movq	$0, 296(%rbx)
	movups	%xmm0, 312(%rbx)
	movups	%xmm0, 328(%rbx)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdx, %xmm1
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm2
	movups	%xmm0, 16(%rbx)
	punpcklqdq	%xmm2, %xmm1
	movups	%xmm0, 32(%rbx)
	movups	%xmm1, (%rbx)
	movups	%xmm0, 48(%rbx)
	call	_ZNSt6localeC1Ev@PLT
	movq	%r13, 72(%rbx)
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal12OFStreamBaseE(%rip), %rax
	movq	%rax, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt9basic_iosIcSt11char_traitsIcEE5rdbufEPSt15basic_streambufIcS1_E@PLT
	.cfi_endproc
.LFE5944:
	.size	_ZN2v88internal8OFStreamC1EP8_IO_FILE, .-_ZN2v88internal8OFStreamC1EP8_IO_FILE
	.section	.rodata._ZN2v88internallsERSoRKNS0_23AsReversiblyEscapedUC16E.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%c"
.LC1:
	.string	"\\x%02x"
.LC2:
	.string	"\\u%04x"
	.section	.text._ZN2v88internallsERSoRKNS0_23AsReversiblyEscapedUC16E,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoRKNS0_23AsReversiblyEscapedUC16E
	.type	_ZN2v88internallsERSoRKNS0_23AsReversiblyEscapedUC16E, @function
_ZN2v88internallsERSoRKNS0_23AsReversiblyEscapedUC16E:
.LFB5951:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movzwl	(%rsi), %r9d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leal	-32(%r9), %edx
	cmpw	$94, %dx
	jbe	.L45
	leal	-9(%r9), %edx
	leaq	.LC0(%rip), %r8
	cmpw	$4, %dx
	ja	.L54
.L46:
	leaq	-34(%rbp), %r13
	movl	$10, %ecx
	movl	$1, %edx
	xorl	%eax, %eax
	movl	$10, %esi
	movq	%r13, %rdi
	call	__snprintf_chk@PLT
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	cmpw	$92, %r9w
	leaq	.LC0(%rip), %r8
	leaq	.LC1(%rip), %rax
	cmove	%rax, %r8
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L54:
	cmpw	$256, %r9w
	leaq	.LC1(%rip), %r8
	leaq	.LC2(%rip), %rax
	cmovnb	%rax, %r8
	jmp	.L46
.L55:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5951:
	.size	_ZN2v88internallsERSoRKNS0_23AsReversiblyEscapedUC16E, .-_ZN2v88internallsERSoRKNS0_23AsReversiblyEscapedUC16E
	.section	.rodata._ZN2v88internallsERSoRKNS0_20AsEscapedUC16ForJSONE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"\\n"
.LC4:
	.string	"\\r"
.LC5:
	.string	"\\t"
.LC6:
	.string	"\\\""
	.section	.text._ZN2v88internallsERSoRKNS0_20AsEscapedUC16ForJSONE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoRKNS0_20AsEscapedUC16ForJSONE
	.type	_ZN2v88internallsERSoRKNS0_20AsEscapedUC16ForJSONE, @function
_ZN2v88internallsERSoRKNS0_20AsEscapedUC16ForJSONE:
.LFB5952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzwl	(%rsi), %eax
	cmpw	$10, %ax
	je	.L68
	cmpw	$13, %ax
	je	.L69
	cmpw	$9, %ax
	je	.L70
	cmpw	$34, %ax
	je	.L71
	leal	-32(%rax), %edx
	movzwl	%ax, %r9d
	cmpw	$94, %dx
	ja	.L72
.L62:
	cmpw	$92, %ax
	leaq	.LC0(%rip), %r8
	leaq	.LC2(%rip), %rax
	cmove	%rax, %r8
.L63:
	leaq	-34(%rbp), %r13
	movl	$1, %edx
	movl	$10, %esi
	movl	$10, %ecx
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L58:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movl	$2, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$2, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L72:
	leal	-9(%rax), %edx
	leaq	.LC2(%rip), %r8
	cmpw	$4, %dx
	ja	.L63
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L69:
	movl	$2, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L70:
	movl	$2, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L58
.L73:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5952:
	.size	_ZN2v88internallsERSoRKNS0_20AsEscapedUC16ForJSONE, .-_ZN2v88internallsERSoRKNS0_20AsEscapedUC16ForJSONE
	.section	.text._ZN2v88internallsERSoRKNS0_6AsUC16E,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoRKNS0_6AsUC16E
	.type	_ZN2v88internallsERSoRKNS0_6AsUC16E, @function
_ZN2v88internallsERSoRKNS0_6AsUC16E:
.LFB5953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movzwl	(%rsi), %r9d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leal	-32(%r9), %edx
	cmpw	$94, %dx
	jbe	.L75
	cmpw	$256, %r9w
	leaq	.LC1(%rip), %r8
	leaq	.LC2(%rip), %rax
	cmovnb	%rax, %r8
.L75:
	leaq	-34(%rbp), %r13
	movl	$10, %ecx
	movl	$1, %edx
	movl	$10, %esi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L81:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5953:
	.size	_ZN2v88internallsERSoRKNS0_6AsUC16E, .-_ZN2v88internallsERSoRKNS0_6AsUC16E
	.section	.rodata._ZN2v88internallsERSoRKNS0_6AsUC32E.str1.1,"aMS",@progbits,1
.LC7:
	.string	"\\u{%06x}"
	.section	.text._ZN2v88internallsERSoRKNS0_6AsUC32E,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoRKNS0_6AsUC32E
	.type	_ZN2v88internallsERSoRKNS0_6AsUC32E, @function
_ZN2v88internallsERSoRKNS0_6AsUC32E:
.LFB5954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	cmpl	$65535, %eax
	jg	.L83
	leal	-32(%rax), %edx
	movzwl	%ax, %r9d
	leaq	.LC0(%rip), %r8
	cmpw	$94, %dx
	ja	.L91
.L84:
	leaq	-37(%rbp), %r13
	movl	$10, %ecx
	movl	$1, %edx
	movl	$10, %esi
.L90:
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L92
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	leaq	-37(%rbp), %r13
	movl	%eax, %r9d
	movl	$13, %ecx
	movl	$1, %edx
	leaq	.LC7(%rip), %r8
	movl	$13, %esi
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L91:
	cmpw	$256, %ax
	leaq	.LC1(%rip), %r8
	leaq	.LC2(%rip), %rax
	cmovnb	%rax, %r8
	jmp	.L84
.L92:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5954:
	.size	_ZN2v88internallsERSoRKNS0_6AsUC32E, .-_ZN2v88internallsERSoRKNS0_6AsUC32E
	.section	.rodata._ZN2v88internallsERSoRKNS0_5AsHexE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"0x"
.LC9:
	.string	""
.LC10:
	.string	"%s%.*lx"
	.section	.text._ZN2v88internallsERSoRKNS0_5AsHexE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoRKNS0_5AsHexE
	.type	_ZN2v88internallsERSoRKNS0_5AsHexE, @function
_ZN2v88internallsERSoRKNS0_5AsHexE:
.LFB5955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC9(%rip), %rdx
	leaq	.LC8(%rip), %r9
	movl	$19, %ecx
	leaq	.LC10(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rsi), %eax
	cmpb	$0, 9(%rsi)
	pushq	(%rsi)
	cmove	%rdx, %r9
	movl	$19, %esi
	movl	$1, %edx
	pushq	%rax
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	movq	%r12, %rdx
.L95:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L95
	movl	%eax, %ecx
	movq	%r12, %rsi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %edi
	addb	%al, %dil
	movq	%r13, %rdi
	sbbq	$3, %rdx
	subq	%r12, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	leaq	-16(%rbp), %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L102:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5955:
	.size	_ZN2v88internallsERSoRKNS0_5AsHexE, .-_ZN2v88internallsERSoRKNS0_5AsHexE
	.section	.rodata._ZN2v88internallsERSoRKNS0_10AsHexBytesE.str1.1,"aMS",@progbits,1
.LC11:
	.string	" "
	.section	.text._ZN2v88internallsERSoRKNS0_10AsHexBytesE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoRKNS0_10AsHexBytesE
	.type	_ZN2v88internallsERSoRKNS0_10AsHexBytesE, @function
_ZN2v88internallsERSoRKNS0_10AsHexBytesE:
.LFB5956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	8(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$7, %dl
	ja	.L115
	movq	(%rsi), %rax
	movzbl	%dl, %ecx
	sall	$3, %ecx
	movq	%rax, %rbx
	shrq	%cl, %rbx
	testq	%rbx, %rbx
	je	.L105
	leal	1(%rdx), %r14d
	cmpb	$7, %dl
	je	.L104
	movzbl	%r14b, %ecx
	movq	%rax, %rbx
	sall	$3, %ecx
	shrq	%cl, %rbx
	testq	%rbx, %rbx
	je	.L104
	leal	2(%rdx), %r14d
	cmpb	$6, %dl
	je	.L104
	movzbl	%r14b, %ecx
	movq	%rax, %rbx
	sall	$3, %ecx
	shrq	%cl, %rbx
	testq	%rbx, %rbx
	je	.L104
	leal	3(%rdx), %r14d
	cmpb	$5, %dl
	je	.L104
	movzbl	%r14b, %ecx
	movq	%rax, %rbx
	sall	$3, %ecx
	shrq	%cl, %rbx
	testq	%rbx, %rbx
	je	.L104
	leal	4(%rdx), %r14d
	cmpb	$4, %dl
	je	.L104
	movzbl	%r14b, %ecx
	movq	%rax, %rsi
	sall	$3, %ecx
	shrq	%cl, %rsi
	testq	%rsi, %rsi
	je	.L104
	leal	5(%rdx), %r14d
	cmpb	$3, %dl
	je	.L104
	movzbl	%r14b, %ecx
	movq	%rax, %rbx
	sall	$3, %ecx
	shrq	%cl, %rbx
	testq	%rbx, %rbx
	je	.L104
	leal	6(%rdx), %r14d
	cmpb	$2, %dl
	je	.L104
	movzbl	%r14b, %ecx
	movq	%rax, %rdi
	sall	$3, %ecx
	shrq	%cl, %rdi
	testq	%rdi, %rdi
	je	.L104
	cmpb	$1, %dl
	je	.L116
	movq	%rax, %rsi
	shrq	$56, %rsi
	setne	%r14b
	addl	$7, %r14d
.L104:
	leal	-1(%r14), %edi
	xorl	%ebx, %ebx
	leaq	-80(%rbp), %r12
	movb	%dil, -81(%rbp)
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L165:
	movzbl	-81(%rbp), %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC9(%rip), %r9
	leaq	.LC10(%rip), %r8
	movl	$19, %esi
	subl	%ebx, %ecx
	movzbl	%cl, %ecx
	sall	$3, %ecx
	shrq	%cl, %rax
	movl	$19, %ecx
	movzbl	%al, %eax
	pushq	%rax
	xorl	%eax, %eax
	pushq	$2
	call	__snprintf_chk@PLT
	movq	%r12, %rdx
.L108:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L108
	movl	%eax, %ecx
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %esi
	addb	%al, %sil
.L164:
	sbbq	$3, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	addl	$1, %ebx
	subq	%r12, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	popq	%rax
	popq	%rdx
	cmpb	%bl, %r14b
	jbe	.L106
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
.L113:
	movl	12(%r15), %ecx
	testl	%ecx, %ecx
	jne	.L165
	movzbl	%bl, %ecx
	movl	$1, %edx
	movl	$19, %esi
	movq	%r12, %rdi
	sall	$3, %ecx
	leaq	.LC9(%rip), %r9
	leaq	.LC10(%rip), %r8
	shrq	%cl, %rax
	movl	$19, %ecx
	movzbl	%al, %eax
	pushq	%rax
	xorl	%eax, %eax
	pushq	$2
	call	__snprintf_chk@PLT
	movq	%r12, %rdx
.L111:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L111
	movl	%eax, %ecx
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %edi
	addb	%al, %dil
	jmp	.L164
.L105:
	testb	%dl, %dl
	jne	.L166
	.p2align 4,,10
	.p2align 3
.L106:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L167
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L115:
	.cfi_restore_state
	movq	(%rsi), %rax
	movl	%edx, %r14d
	jmp	.L104
.L116:
	movl	$8, %r14d
	jmp	.L104
.L166:
	movl	%edx, %r14d
	jmp	.L104
.L167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5956:
	.size	_ZN2v88internallsERSoRKNS0_10AsHexBytesE, .-_ZN2v88internallsERSoRKNS0_10AsHexBytesE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal12DbgStreamBufC2Ev,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal12DbgStreamBufC2Ev, @function
_GLOBAL__sub_I__ZN2v88internal12DbgStreamBufC2Ev:
.LFB6828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE6828:
	.size	_GLOBAL__sub_I__ZN2v88internal12DbgStreamBufC2Ev, .-_GLOBAL__sub_I__ZN2v88internal12DbgStreamBufC2Ev
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal12DbgStreamBufC2Ev
	.hidden	_ZTCN2v88internal15DbgStdoutStreamE0_So
	.weak	_ZTCN2v88internal15DbgStdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal15DbgStdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal15DbgStdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal15DbgStdoutStreamE0_So, @object
	.size	_ZTCN2v88internal15DbgStdoutStreamE0_So, 80
_ZTCN2v88internal15DbgStdoutStreamE0_So:
	.quad	328
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-328
	.quad	-328
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal15DbgStdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal15DbgStdoutStreamE,"awG",@progbits,_ZTVN2v88internal15DbgStdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal15DbgStdoutStreamE, @object
	.size	_ZTTN2v88internal15DbgStdoutStreamE, 32
_ZTTN2v88internal15DbgStdoutStreamE:
	.quad	_ZTVN2v88internal15DbgStdoutStreamE+24
	.quad	_ZTCN2v88internal15DbgStdoutStreamE0_So+24
	.quad	_ZTCN2v88internal15DbgStdoutStreamE0_So+64
	.quad	_ZTVN2v88internal15DbgStdoutStreamE+64
	.weak	_ZTVN2v88internal15DbgStdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal15DbgStdoutStreamE,"awG",@progbits,_ZTVN2v88internal15DbgStdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal15DbgStdoutStreamE, @object
	.size	_ZTVN2v88internal15DbgStdoutStreamE, 80
_ZTVN2v88internal15DbgStdoutStreamE:
	.quad	328
	.quad	0
	.quad	0
	.quad	_ZN2v88internal15DbgStdoutStreamD1Ev
	.quad	_ZN2v88internal15DbgStdoutStreamD0Ev
	.quad	-328
	.quad	-328
	.quad	0
	.quad	_ZTv0_n24_N2v88internal15DbgStdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal15DbgStdoutStreamD0Ev
	.hidden	_ZTCN2v88internal8OFStreamE0_So
	.weak	_ZTCN2v88internal8OFStreamE0_So
	.section	.rodata._ZTCN2v88internal8OFStreamE0_So,"aG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal8OFStreamE0_So, @object
	.size	_ZTCN2v88internal8OFStreamE0_So, 80
_ZTCN2v88internal8OFStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal8OFStreamE, @object
	.size	_ZTTN2v88internal8OFStreamE, 32
_ZTTN2v88internal8OFStreamE:
	.quad	_ZTVN2v88internal8OFStreamE+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+64
	.quad	_ZTVN2v88internal8OFStreamE+64
	.weak	_ZTVN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal8OFStreamE, @object
	.size	_ZTVN2v88internal8OFStreamE, 80
_ZTVN2v88internal8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8OFStreamD1Ev
	.quad	_ZN2v88internal8OFStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.weak	_ZTVN2v88internal12DbgStreamBufE
	.section	.data.rel.ro._ZTVN2v88internal12DbgStreamBufE,"awG",@progbits,_ZTVN2v88internal12DbgStreamBufE,comdat
	.align 8
	.type	_ZTVN2v88internal12DbgStreamBufE, @object
	.size	_ZTVN2v88internal12DbgStreamBufE, 128
_ZTVN2v88internal12DbgStreamBufE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12DbgStreamBufD1Ev
	.quad	_ZN2v88internal12DbgStreamBufD0Ev
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE5imbueERKSt6locale
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE6setbufEPcl
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE7seekoffElSt12_Ios_SeekdirSt13_Ios_Openmode
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE7seekposESt4fposI11__mbstate_tESt13_Ios_Openmode
	.quad	_ZN2v88internal12DbgStreamBuf4syncEv
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE9showmanycEv
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE6xsgetnEPcl
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE9underflowEv
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE5uflowEv
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE9pbackfailEi
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE6xsputnEPKcl
	.quad	_ZN2v88internal12DbgStreamBuf8overflowEi
	.weak	_ZTVN2v88internal12OFStreamBaseE
	.section	.data.rel.ro._ZTVN2v88internal12OFStreamBaseE,"awG",@progbits,_ZTVN2v88internal12OFStreamBaseE,comdat
	.align 8
	.type	_ZTVN2v88internal12OFStreamBaseE, @object
	.size	_ZTVN2v88internal12OFStreamBaseE, 128
_ZTVN2v88internal12OFStreamBaseE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12OFStreamBaseD1Ev
	.quad	_ZN2v88internal12OFStreamBaseD0Ev
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE5imbueERKSt6locale
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE6setbufEPcl
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE7seekoffElSt12_Ios_SeekdirSt13_Ios_Openmode
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE7seekposESt4fposI11__mbstate_tESt13_Ios_Openmode
	.quad	_ZN2v88internal12OFStreamBase4syncEv
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE9showmanycEv
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE6xsgetnEPcl
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE9underflowEv
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE5uflowEv
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE9pbackfailEi
	.quad	_ZN2v88internal12OFStreamBase6xsputnEPKcl
	.quad	_ZN2v88internal12OFStreamBase8overflowEi
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
