	.file	"constant-array-builder.cc"
	.text
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySliceC2EPNS0_4ZoneEmmNS1_11OperandSizeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySliceC2EPNS0_4ZoneEmmNS1_11OperandSizeE
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySliceC2EPNS0_4ZoneEmmNS1_11OperandSizeE, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySliceC2EPNS0_4ZoneEmmNS1_11OperandSizeE:
.LFB19271:
	.cfi_startproc
	endbr64
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	$0, 16(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movb	%r8b, 24(%rdi)
	movq	%rsi, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE19271:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySliceC2EPNS0_4ZoneEmmNS1_11OperandSizeE, .-_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySliceC2EPNS0_4ZoneEmmNS1_11OperandSizeE
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySliceC1EPNS0_4ZoneEmmNS1_11OperandSizeE
	.set	_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySliceC1EPNS0_4ZoneEmmNS1_11OperandSizeE,_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySliceC2EPNS0_4ZoneEmmNS1_11OperandSizeE
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice7ReserveEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice7ReserveEv
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice7ReserveEv, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice7ReserveEv:
.LFB19273:
	.cfi_startproc
	endbr64
	addq	$1, 16(%rdi)
	ret
	.cfi_endproc
.LFE19273:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice7ReserveEv, .-_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice7ReserveEv
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice9UnreserveEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice9UnreserveEv
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice9UnreserveEv, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice9UnreserveEv:
.LFB19274:
	.cfi_startproc
	endbr64
	subq	$1, 16(%rdi)
	ret
	.cfi_endproc
.LFE19274:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice9UnreserveEv, .-_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice9UnreserveEv
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice2AtEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice2AtEm
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice2AtEm, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice2AtEm:
.LFB19276:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	subq	(%rdi), %rax
	salq	$4, %rax
	addq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE19276:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice2AtEm, .-_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice2AtEm
	.section	.text._ZNK2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice2AtEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice2AtEm
	.type	_ZNK2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice2AtEm, @function
_ZNK2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice2AtEm:
.LFB24168:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	subq	(%rdi), %rax
	salq	$4, %rax
	addq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE24168:
	.size	_ZNK2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice2AtEm, .-_ZNK2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice2AtEm
	.section	.rodata._ZN2v88internal11interpreter20ConstantArrayBuilderC2EPNS0_4ZoneE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Out of memory: HashMap::Initialize"
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilderC2EPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilderC2EPNS0_4ZoneE
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilderC2EPNS0_4ZoneE, @function
_ZN2v88internal11interpreter20ConstantArrayBuilderC2EPNS0_4ZoneE:
.LFB19294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	16(%rsi), %rax
	movq	%rdi, %rbx
	movq	24(%rsi), %rdx
	subq	%rax, %rdx
	cmpq	$383, %rdx
	jbe	.L21
	leaq	384(%rax), %rdx
	movq	%rdx, 16(%rsi)
.L9:
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.L22
	movl	$16, 32(%rbx)
	movb	$0, 16(%rax)
	cmpl	$1, 32(%rbx)
	jbe	.L11
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L12:
	movq	24(%rbx), %rcx
	addq	$1, %rax
	movb	$0, 16(%rcx,%rdx)
	movl	32(%rbx), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L12
.L11:
	leaq	64(%rbx), %rax
	movq	%r12, 48(%rbx)
	pcmpeqd	%xmm0, %xmm0
	movq	%rax, 80(%rbx)
	movq	%rax, 88(%rbx)
	leaq	152(%rbx), %rax
	movl	$0, 64(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 96(%rbx)
	movq	%r12, 104(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 128(%rbx)
	movq	%r12, 136(%rbx)
	movl	$0, 152(%rbx)
	movq	$0, 160(%rbx)
	movq	%rax, 168(%rbx)
	movq	%rax, 176(%rbx)
	movq	$0, 184(%rbx)
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	movl	$0, 36(%rbx)
	movl	$-1, 224(%rbx)
	subq	%rax, %rdx
	movq	%r12, 232(%rbx)
	movups	%xmm0, 192(%rbx)
	movups	%xmm0, 208(%rbx)
	cmpq	$63, %rdx
	jbe	.L23
	leaq	64(%rax), %rdx
	movq	%rdx, 16(%r12)
.L14:
	movdqa	.LC1(%rip), %xmm0
	movq	$0, 16(%rax)
	movb	$1, 24(%rax)
	movq	%r12, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movups	%xmm0, (%rax)
	movq	24(%r12), %rdx
	movq	%rax, (%rbx)
	movq	16(%r12), %rax
	subq	%rax, %rdx
	cmpq	$63, %rdx
	jbe	.L24
	leaq	64(%rax), %rdx
	movq	%rdx, 16(%r12)
.L16:
	movdqa	.LC2(%rip), %xmm0
	movq	$0, 16(%rax)
	movb	$2, 24(%rax)
	movq	%r12, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movups	%xmm0, (%rax)
	movq	24(%r12), %rdx
	movq	%rax, 8(%rbx)
	movq	16(%r12), %rax
	subq	%rax, %rdx
	cmpq	$63, %rdx
	jbe	.L25
	leaq	64(%rax), %rdx
	movq	%rdx, 16(%r12)
.L18:
	movdqa	.LC3(%rip), %xmm0
	movq	$0, 16(%rax)
	movb	$4, 24(%rax)
	movq	%r12, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movl	$384, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L18
.L22:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19294:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilderC2EPNS0_4ZoneE, .-_ZN2v88internal11interpreter20ConstantArrayBuilderC2EPNS0_4ZoneE
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilderC1EPNS0_4ZoneE
	.set	_ZN2v88internal11interpreter20ConstantArrayBuilderC1EPNS0_4ZoneE,_ZN2v88internal11interpreter20ConstantArrayBuilderC2EPNS0_4ZoneE
	.section	.text._ZNK2v88internal11interpreter20ConstantArrayBuilder4sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter20ConstantArrayBuilder4sizeEv
	.type	_ZNK2v88internal11interpreter20ConstantArrayBuilder4sizeEv, @function
_ZNK2v88internal11interpreter20ConstantArrayBuilder4sizeEv:
.LFB19296:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	movq	40(%rdx), %rcx
	movq	48(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L27
	movq	8(%rdi), %rdx
	movq	40(%rdx), %rcx
	movq	48(%rdx), %rax
	cmpq	%rax, %rcx
	jne	.L27
	movq	(%rdi), %rdx
	movq	40(%rdx), %rcx
	movq	48(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L27
	subq	%rcx, %rax
	sarq	$4, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	subq	%rcx, %rax
	sarq	$4, %rax
	addq	(%rdx), %rax
	ret
	.cfi_endproc
.LFE19296:
	.size	_ZNK2v88internal11interpreter20ConstantArrayBuilder4sizeEv, .-_ZNK2v88internal11interpreter20ConstantArrayBuilder4sizeEv
	.section	.rodata._ZNK2v88internal11interpreter20ConstantArrayBuilder12IndexToSliceEm.str1.1,"aMS",@progbits,1
.LC4:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal11interpreter20ConstantArrayBuilder12IndexToSliceEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter20ConstantArrayBuilder12IndexToSliceEm
	.type	_ZNK2v88internal11interpreter20ConstantArrayBuilder12IndexToSliceEm, @function
_ZNK2v88internal11interpreter20ConstantArrayBuilder12IndexToSliceEm:
.LFB19297:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rdx
	addq	8(%rax), %rdx
	subq	$1, %rdx
	cmpq	%rdx, %rsi
	jbe	.L29
	movq	8(%rdi), %rax
	movq	8(%rax), %rdx
	addq	(%rax), %rdx
	subq	$1, %rdx
	cmpq	%rdx, %rsi
	jbe	.L29
	movq	16(%rdi), %rax
	movq	8(%rax), %rdx
	addq	(%rax), %rdx
	subq	$1, %rdx
	cmpq	%rdx, %rsi
	jbe	.L29
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE19297:
	.size	_ZNK2v88internal11interpreter20ConstantArrayBuilder12IndexToSliceEm, .-_ZNK2v88internal11interpreter20ConstantArrayBuilder12IndexToSliceEm
	.section	.rodata._ZN2v88internal11interpreter20ConstantArrayBuilder12ToFixedArrayEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"(location_) != nullptr"
.LC6:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder12ToFixedArrayEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder12ToFixedArrayEPNS0_7IsolateE
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder12ToFixedArrayEPNS0_7IsolateE, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder12ToFixedArrayEPNS0_7IsolateE:
.LFB19299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	16(%rdi), %rax
	movq	%rsi, -64(%rbp)
	movq	48(%rax), %rdx
	movq	40(%rax), %rsi
	cmpq	%rsi, %rdx
	jne	.L36
	movq	8(%rdi), %rax
	movq	40(%rax), %rsi
	movq	48(%rax), %rdx
	cmpq	%rsi, %rdx
	jne	.L36
	movq	(%rdi), %rax
	movq	40(%rax), %rsi
	movq	48(%rax), %rdx
	cmpq	%rdx, %rsi
	jne	.L36
	subq	%rsi, %rsi
	sarq	$4, %rsi
.L37:
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	leaq	.L41(%rip), %r14
	call	_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE@PLT
	movq	%rbx, -104(%rbp)
	xorl	%r9d, %r9d
	movq	%rax, %r15
	movq	%rbx, %rax
	movl	%r9d, %r13d
	addq	$24, %rax
	movq	%rax, -112(%rbp)
.L67:
	movq	-104(%rbp), %rax
	movq	(%rax), %r8
	movq	40(%r8), %rdx
	cmpq	48(%r8), %rdx
	je	.L69
	leal	16(,%r13,8), %ecx
	movl	%r13d, -52(%rbp)
	xorl	%ebx, %ebx
	movq	%r8, %r12
	movslq	%ecx, %rcx
	movq	%rcx, %r13
	.p2align 4,,10
	.p2align 3
.L65:
	movq	%rbx, %rax
	salq	$4, %rax
	addq	%rax, %rdx
	cmpb	$17, 8(%rdx)
	ja	.L39
	movzbl	8(%rdx), %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter20ConstantArrayBuilder12ToFixedArrayEPNS0_7IsolateE,"a",@progbits
	.align 4
	.align 4
.L41:
	.long	.L39-.L41
	.long	.L56-.L41
	.long	.L50-.L41
	.long	.L55-.L41
	.long	.L54-.L41
	.long	.L53-.L41
	.long	.L52-.L41
	.long	.L51-.L41
	.long	.L50-.L41
	.long	.L49-.L41
	.long	.L48-.L41
	.long	.L47-.L41
	.long	.L46-.L41
	.long	.L45-.L41
	.long	.L44-.L41
	.long	.L43-.L41
	.long	.L42-.L41
	.long	.L40-.L41
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder12ToFixedArrayEPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L50:
	movq	-64(%rbp), %rax
	movq	(%rdx), %rsi
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L58
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	.p2align 4,,10
	.p2align 3
.L57:
	movq	(%r15), %rdi
	movq	(%rax), %rdx
	movl	-52(%rbp), %eax
	leaq	-1(%rdi,%r13), %rsi
	movq	%rdx, (%rsi)
	leal	1(%rax,%rbx), %r11d
	testb	$1, %dl
	je	.L68
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r9
	movq	%rax, -72(%rbp)
	testl	$262144, %r9d
	je	.L63
	movl	%r11d, -56(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rax
	movl	-56(%rbp), %r11d
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	8(%rax), %r9
	movq	-80(%rbp), %rdi
.L63:
	andl	$24, %r9d
	je	.L68
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L68
	movl	%r11d, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	-72(%rbp), %r11d
	.p2align 4,,10
	.p2align 3
.L68:
	movq	40(%r12), %rdx
	movq	48(%r12), %rax
	addq	$1, %rbx
	addq	$8, %r13
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rbx
	jb	.L65
	movq	%r12, %r8
.L38:
	movq	8(%r8), %r9
	subq	%rax, %r9
	movq	(%r15), %rax
	movslq	11(%rax), %rax
	subl	%r11d, %eax
	cltq
	cmpq	%r9, %rax
	jbe	.L66
	addq	$8, -104(%rbp)
	leal	(%r11,%r9), %r13d
	movq	-104(%rbp), %rax
	cmpq	%rax, -112(%rbp)
	jne	.L67
.L66:
	addq	$72, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movq	(%rdx), %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L40:
	movq	-64(%rbp), %rax
	addq	$1088, %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L42:
	movq	-64(%rbp), %rax
	addq	$3728, %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L43:
	movq	-64(%rbp), %rax
	addq	$3856, %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L44:
	movq	-64(%rbp), %rax
	addq	$3720, %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L45:
	movq	-64(%rbp), %rax
	addq	$288, %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L46:
	movq	-64(%rbp), %rax
	addq	$992, %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L47:
	movq	-64(%rbp), %rax
	addq	$984, %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L48:
	movq	-64(%rbp), %rax
	addq	$3648, %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L49:
	movq	-64(%rbp), %rax
	addq	$3848, %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L51:
	movq	-64(%rbp), %rax
	addq	$96, %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L52:
	movq	(%rdx), %rax
	movq	104(%rax), %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L53:
	movq	(%rdx), %rsi
	movq	-64(%rbp), %rdi
	call	_ZN2v88internal13BigIntLiteralEPNS0_7IsolateEPKc@PLT
	testq	%rax, %rax
	jne	.L57
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L54:
	movsd	(%rdx), %xmm0
	movq	-64(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L55:
	movq	(%rdx), %rax
	movq	(%rax), %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%rax, %rcx
	movq	41088(%rax), %rax
	cmpq	41096(%rcx), %rax
	je	.L79
.L60:
	movq	-64(%rbp), %rcx
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L69:
	movl	%r13d, %r11d
	xorl	%eax, %eax
	jmp	.L38
.L36:
	subq	%rsi, %rdx
	movq	(%rax), %rsi
	sarq	$4, %rdx
	addq	%rdx, %rsi
	jmp	.L37
.L79:
	movq	%rcx, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L60
.L39:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19299:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder12ToFixedArrayEPNS0_7IsolateE, .-_ZN2v88internal11interpreter20ConstantArrayBuilder12ToFixedArrayEPNS0_7IsolateE
	.section	.text._ZNK2v88internal11interpreter20ConstantArrayBuilder18OperandSizeToSliceENS1_11OperandSizeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter20ConstantArrayBuilder18OperandSizeToSliceENS1_11OperandSizeE
	.type	_ZNK2v88internal11interpreter20ConstantArrayBuilder18OperandSizeToSliceENS1_11OperandSizeE, @function
_ZNK2v88internal11interpreter20ConstantArrayBuilder18OperandSizeToSliceENS1_11OperandSizeE:
.LFB19319:
	.cfi_startproc
	endbr64
	cmpb	$2, %sil
	je	.L81
	ja	.L82
	testb	%sil, %sil
	je	.L91
	movq	(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	cmpb	$4, %sil
	jne	.L92
	movq	16(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	movq	8(%rdi), %rax
	ret
.L91:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19319:
	.size	_ZNK2v88internal11interpreter20ConstantArrayBuilder18OperandSizeToSliceENS1_11OperandSizeE, .-_ZNK2v88internal11interpreter20ConstantArrayBuilder18OperandSizeToSliceENS1_11OperandSizeE
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder13SetDeferredAtEmNS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder13SetDeferredAtEmNS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder13SetDeferredAtEmNS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder13SetDeferredAtEmNS0_6HandleINS0_6ObjectEEE:
.LFB19322:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %r8
	addq	%rcx, %r8
	subq	$1, %r8
	cmpq	%r8, %rsi
	jbe	.L94
	movq	8(%rdi), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %r8
	addq	%rcx, %r8
	subq	$1, %r8
	cmpq	%r8, %rsi
	jbe	.L94
	movq	16(%rdi), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	addq	%rcx, %rdi
	subq	$1, %rdi
	cmpq	%rdi, %rsi
	jbe	.L94
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	subq	%rcx, %rsi
	salq	$4, %rsi
	addq	40(%rax), %rsi
	movb	$1, 8(%rsi)
	movq	%rdx, (%rsi)
	ret
	.cfi_endproc
.LFE19322:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder13SetDeferredAtEmNS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal11interpreter20ConstantArrayBuilder13SetDeferredAtEmNS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder15SetJumpTableSmiEmNS0_3SmiE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder15SetJumpTableSmiEmNS0_3SmiE
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder15SetJumpTableSmiEmNS0_3SmiE, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder15SetJumpTableSmiEmNS0_3SmiE:
.LFB19323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	(%rbx), %rax
	addq	8(%rbx), %rax
	subq	$1, %rax
	cmpq	%rax, %rsi
	jbe	.L100
	movq	8(%rdi), %rbx
	movq	(%rbx), %rax
	addq	8(%rbx), %rax
	subq	$1, %rax
	cmpq	%rax, %rsi
	jbe	.L100
	movq	16(%rdi), %rbx
	movq	8(%rbx), %rax
	addq	(%rbx), %rax
	subq	$1, %rax
	cmpq	%rax, %rsi
	jbe	.L100
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L100:
	movq	48(%r13), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$47, %rax
	jbe	.L125
	leaq	48(%rsi), %rax
	movq	%rax, 16(%rdi)
.L104:
	movq	%r14, 32(%rsi)
	leaq	64(%r13), %r8
	movl	%r12d, 40(%rsi)
	movq	72(%r13), %r15
	testq	%r15, %r15
	jne	.L106
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L127:
	movq	16(%r15), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L107
.L128:
	movq	%rax, %r15
.L106:
	movq	32(%r15), %rdx
	cmpq	%rdx, %r14
	jb	.L127
	movq	24(%r15), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L128
.L107:
	testb	%cl, %cl
	jne	.L129
	cmpq	%rdx, %r14
	jbe	.L112
.L114:
	movl	$1, %edi
	cmpq	%r8, %r15
	jne	.L130
.L113:
	movq	%r8, %rcx
	movq	%r15, %rdx
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 96(%r13)
.L112:
	subq	(%rbx), %r12
	salq	$4, %r12
	addq	40(%rbx), %r12
	movb	$8, 8(%r12)
	movq	%r14, (%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	cmpq	%r15, 80(%r13)
	je	.L114
.L115:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpq	32(%rax), %r14
	jbe	.L112
	testq	%r15, %r15
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rsi
	jne	.L114
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L130:
	xorl	%edi, %edi
	cmpq	32(%r15), %r14
	setb	%dil
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L126:
	movq	%r8, %r15
	cmpq	80(%r13), %r8
	jne	.L115
	movl	$1, %edi
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L125:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L104
	.cfi_endproc
.LFE19323:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder15SetJumpTableSmiEmNS0_3SmiE, .-_ZN2v88internal11interpreter20ConstantArrayBuilder15SetJumpTableSmiEmNS0_3SmiE
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder19CreateReservedEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder19CreateReservedEntryEv
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder19CreateReservedEntryEv, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder19CreateReservedEntryEv:
.LFB19324:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	16(%rax), %rdx
	movq	8(%rax), %rsi
	movq	48(%rax), %rcx
	subq	40(%rax), %rcx
	subq	%rdx, %rsi
	sarq	$4, %rcx
	cmpq	%rcx, %rsi
	jne	.L134
	movq	8(%rdi), %rax
	movq	16(%rax), %rdx
	movq	8(%rax), %rsi
	movq	48(%rax), %rcx
	subq	40(%rax), %rcx
	subq	%rdx, %rsi
	sarq	$4, %rcx
	cmpq	%rcx, %rsi
	jne	.L135
	movq	16(%rdi), %rax
	movq	16(%rax), %rdx
	movq	8(%rax), %rsi
	movq	48(%rax), %rcx
	subq	40(%rax), %rcx
	subq	%rdx, %rsi
	sarq	$4, %rcx
	cmpq	%rcx, %rsi
	jne	.L139
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	xorl	%ecx, %ecx
.L132:
	addq	$1, %rdx
	movq	%rdx, 16(%rax)
	movq	(%rdi,%rcx,8), %rax
	movzbl	24(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	movl	$1, %ecx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L139:
	movl	$2, %ecx
	jmp	.L132
	.cfi_endproc
.LFE19324:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder19CreateReservedEntryEv, .-_ZN2v88internal11interpreter20ConstantArrayBuilder19CreateReservedEntryEv
	.section	.text.unlikely._ZN2v88internal11interpreter20ConstantArrayBuilder20DiscardReservedEntryENS1_11OperandSizeE,"ax",@progbits
	.align 2
.LCOLDB7:
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder20DiscardReservedEntryENS1_11OperandSizeE,"ax",@progbits
.LHOTB7:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder20DiscardReservedEntryENS1_11OperandSizeE
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder20DiscardReservedEntryENS1_11OperandSizeE, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder20DiscardReservedEntryENS1_11OperandSizeE:
.LFB19327:
	.cfi_startproc
	endbr64
	cmpb	$2, %sil
	je	.L141
	ja	.L142
	testb	%sil, %sil
	je	.L154
	movq	(%rdi), %rax
	subq	$1, 16(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	cmpb	$4, %sil
	jne	.L155
	movq	16(%rdi), %rax
	subq	$1, 16(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	movq	8(%rdi), %rax
	subq	$1, 16(%rax)
	ret
.L154:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
.L155:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	jmp	.L148
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter20ConstantArrayBuilder20DiscardReservedEntryENS1_11OperandSizeE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder20DiscardReservedEntryENS1_11OperandSizeE.cold, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder20DiscardReservedEntryENS1_11OperandSizeE.cold:
.LFSB19327:
.L148:
	movq	16, %rax
	ud2
	.cfi_endproc
.LFE19327:
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder20DiscardReservedEntryENS1_11OperandSizeE
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder20DiscardReservedEntryENS1_11OperandSizeE, .-_ZN2v88internal11interpreter20ConstantArrayBuilder20DiscardReservedEntryENS1_11OperandSizeE
	.section	.text.unlikely._ZN2v88internal11interpreter20ConstantArrayBuilder20DiscardReservedEntryENS1_11OperandSizeE
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder20DiscardReservedEntryENS1_11OperandSizeE.cold, .-_ZN2v88internal11interpreter20ConstantArrayBuilder20DiscardReservedEntryENS1_11OperandSizeE.cold
.LCOLDE7:
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder20DiscardReservedEntryENS1_11OperandSizeE
.LHOTE7:
	.section	.text._ZNK2v88internal11interpreter20ConstantArrayBuilder5Entry8ToHandleEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter20ConstantArrayBuilder5Entry8ToHandleEPNS0_7IsolateE
	.type	_ZNK2v88internal11interpreter20ConstantArrayBuilder5Entry8ToHandleEPNS0_7IsolateE, @function
_ZNK2v88internal11interpreter20ConstantArrayBuilder5Entry8ToHandleEPNS0_7IsolateE:
.LFB19328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	cmpb	$17, 8(%rdi)
	ja	.L157
	movzbl	8(%rax), %edx
	leaq	.L159(%rip), %rcx
	movq	%rsi, %rdi
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZNK2v88internal11interpreter20ConstantArrayBuilder5Entry8ToHandleEPNS0_7IsolateE,"a",@progbits
	.align 4
	.align 4
.L159:
	.long	.L157-.L159
	.long	.L181-.L159
	.long	.L168-.L159
	.long	.L173-.L159
	.long	.L172-.L159
	.long	.L171-.L159
	.long	.L170-.L159
	.long	.L169-.L159
	.long	.L168-.L159
	.long	.L167-.L159
	.long	.L166-.L159
	.long	.L165-.L159
	.long	.L164-.L159
	.long	.L163-.L159
	.long	.L162-.L159
	.long	.L161-.L159
	.long	.L160-.L159
	.long	.L158-.L159
	.section	.text._ZNK2v88internal11interpreter20ConstantArrayBuilder5Entry8ToHandleEPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L168:
	movq	41112(%rdi), %r8
	movq	(%rax), %rsi
	testq	%r8, %r8
	je	.L176
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L175:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movq	(%rax), %rax
.L181:
	movq	(%rax), %rax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	1088(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	3856(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	3728(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	984(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	992(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	288(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	3720(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movsd	(%rax), %xmm0
	movl	$1, %esi
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movq	(%rax), %rsi
	call	_ZN2v88internal13BigIntLiteralEPNS0_7IsolateEPKc@PLT
	testq	%rax, %rax
	jne	.L175
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L170:
	movq	(%rax), %rax
	movq	104(%rax), %rax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	96(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	3848(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	3648(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L182
.L178:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rdi)
	movq	%rsi, (%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L182:
	.cfi_restore_state
	movq	%rsi, -16(%rbp)
	movq	%rdi, -8(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-16(%rbp), %rsi
	movq	-8(%rbp), %rdi
	jmp	.L178
.L157:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19328:
	.size	_ZNK2v88internal11interpreter20ConstantArrayBuilder5Entry8ToHandleEPNS0_7IsolateE, .-_ZNK2v88internal11interpreter20ConstantArrayBuilder5Entry8ToHandleEPNS0_7IsolateE
	.section	.text._ZNK2v88internal11interpreter20ConstantArrayBuilder2AtEmPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter20ConstantArrayBuilder2AtEmPNS0_7IsolateE
	.type	_ZNK2v88internal11interpreter20ConstantArrayBuilder2AtEmPNS0_7IsolateE, @function
_ZNK2v88internal11interpreter20ConstantArrayBuilder2AtEmPNS0_7IsolateE:
.LFB19298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	addq	%rcx, %r8
	subq	$1, %r8
	cmpq	%r8, %rsi
	jbe	.L184
	movq	8(%rdi), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %r8
	addq	%rcx, %r8
	subq	$1, %r8
	cmpq	%r8, %rsi
	jbe	.L184
	movq	16(%rdi), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	addq	%rcx, %rdi
	subq	$1, %rdi
	cmpq	%rdi, %rsi
	jbe	.L184
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L184:
	movq	40(%rax), %rdi
	movq	48(%rax), %rax
	subq	%rdi, %rax
	sarq	$4, %rax
	addq	%rcx, %rax
	cmpq	%rsi, %rax
	jbe	.L187
	subq	%rcx, %rsi
	salq	$4, %rsi
	addq	%rsi, %rdi
	cmpb	$0, 8(%rdi)
	jne	.L190
.L187:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	movq	%rdx, %rsi
	call	_ZNK2v88internal11interpreter20ConstantArrayBuilder5Entry8ToHandleEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19298:
	.size	_ZNK2v88internal11interpreter20ConstantArrayBuilder2AtEmPNS0_7IsolateE, .-_ZNK2v88internal11interpreter20ConstantArrayBuilder2AtEmPNS0_7IsolateE
	.section	.rodata._ZNSt6vectorIN2v88internal11interpreter20ConstantArrayBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC8:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal11interpreter20ConstantArrayBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal11interpreter20ConstantArrayBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal11interpreter20ConstantArrayBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal11interpreter20ConstantArrayBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIN2v88internal11interpreter20ConstantArrayBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB22587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	8(%rdi), %r14
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L207
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L201
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L208
	movl	$2147483632, %esi
	movl	$2147483632, %ecx
.L193:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%rsi, %r8
	jb	.L209
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L196:
	leaq	(%rax,%rcx), %rdi
	leaq	16(%rax), %rsi
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L208:
	testq	%rcx, %rcx
	jne	.L210
	movl	$16, %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
.L194:
	movdqu	(%r15), %xmm3
	movups	%xmm3, (%rax,%rdx)
	cmpq	%r14, %rbx
	je	.L197
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L198:
	movdqu	(%rdx), %xmm1
	addq	$16, %rdx
	addq	$16, %rcx
	movups	%xmm1, -16(%rcx)
	cmpq	%rdx, %rbx
	jne	.L198
	movq	%rbx, %rdx
	subq	%r14, %rdx
	leaq	16(%rax,%rdx), %rsi
.L197:
	cmpq	%r12, %rbx
	je	.L199
	movq	%rbx, %rdx
	movq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L200:
	movdqu	(%rdx), %xmm2
	addq	$16, %rdx
	addq	$16, %rcx
	movups	%xmm2, -16(%rcx)
	cmpq	%rdx, %r12
	jne	.L200
	subq	%rbx, %r12
	addq	%r12, %rsi
.L199:
	movq	%rax, %xmm0
	movq	%rsi, %xmm4
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movl	$16, %esi
	movl	$16, %ecx
	jmp	.L193
.L209:
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	jmp	.L196
.L210:
	cmpq	$134217727, %rcx
	movl	$134217727, %eax
	cmova	%rax, %rcx
	salq	$4, %rcx
	movq	%rcx, %rsi
	jmp	.L193
.L207:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22587:
	.size	_ZNSt6vectorIN2v88internal11interpreter20ConstantArrayBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIN2v88internal11interpreter20ConstantArrayBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm:
.LFB19318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	48(%rbx), %rsi
	movq	8(%rbx), %rax
	subq	16(%rbx), %rax
	movq	%rsi, %r12
	subq	40(%rbx), %r12
	sarq	$4, %r12
	subq	%r12, %rax
	cmpq	%rax, %rcx
	jbe	.L212
	movq	8(%rdi), %rbx
	movq	48(%rbx), %rsi
	movq	8(%rbx), %rax
	subq	16(%rbx), %rax
	movq	%rsi, %r12
	subq	40(%rbx), %r12
	sarq	$4, %r12
	subq	%r12, %rax
	cmpq	%rax, %rcx
	jbe	.L213
	movq	16(%rdi), %rbx
	movq	48(%rbx), %rsi
	movq	8(%rbx), %rax
	subq	16(%rbx), %rax
	movq	%rsi, %r12
	subq	40(%rbx), %r12
	sarq	$4, %r12
	subq	%r12, %rax
	cmpq	%rax, %rcx
	jbe	.L213
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L213:
	movq	%r8, -80(%rbp)
	movq	%rdx, -72(%rbp)
.L219:
	leaq	32(%rbx), %rax
	xorl	%r14d, %r14d
	leaq	-80(%rbp), %r15
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L227:
	movdqa	-80(%rbp), %xmm0
	addq	$1, %r14
	movups	%xmm0, (%rsi)
	addq	$16, 48(%rbx)
	cmpq	%r14, %r13
	je	.L215
.L228:
	movq	48(%rbx), %rsi
.L218:
	cmpq	%rsi, 56(%rbx)
	jne	.L227
	movq	%rax, %rdi
	movq	%r15, %rdx
	addq	$1, %r14
	movq	%rax, -88(%rbp)
	call	_ZNSt6vectorIN2v88internal11interpreter20ConstantArrayBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-88(%rbp), %rax
	cmpq	%r14, %r13
	jne	.L228
	.p2align 4,,10
	.p2align 3
.L215:
	movq	(%rbx), %rax
	addq	%r12, %rax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L229
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	movq	%r8, -80(%rbp)
	movq	%rdx, -72(%rbp)
	testq	%rcx, %rcx
	jne	.L219
	jmp	.L215
.L229:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19318:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm, .-_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice8AllocateENS2_5EntryEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice8AllocateENS2_5EntryEm
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice8AllocateENS2_5EntryEm, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice8AllocateENS2_5EntryEm:
.LFB19275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%rsi, -64(%rbp)
	movq	48(%rdi), %rsi
	movq	%rdx, -56(%rbp)
	movq	%rsi, %r14
	subq	40(%rdi), %r14
	sarq	$4, %r14
	movq	%r14, -72(%rbp)
	testq	%rcx, %rcx
	je	.L231
	movq	%rcx, %r13
	leaq	32(%rdi), %r14
	leaq	-64(%rbp), %r15
	xorl	%r12d, %r12d
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L239:
	movdqa	-64(%rbp), %xmm0
	addq	$1, %r12
	movups	%xmm0, (%rsi)
	addq	$16, 48(%rbx)
	cmpq	%r12, %r13
	je	.L231
.L240:
	movq	48(%rbx), %rsi
.L234:
	cmpq	%rsi, 56(%rbx)
	jne	.L239
	movq	%r15, %rdx
	movq	%r14, %rdi
	addq	$1, %r12
	call	_ZNSt6vectorIN2v88internal11interpreter20ConstantArrayBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	cmpq	%r12, %r13
	jne	.L240
.L231:
	movq	-72(%rbp), %rax
	addq	(%rbx), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19275:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice8AllocateENS2_5EntryEm, .-_ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySlice8AllocateENS2_5EntryEm
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0:
.LFB24171:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	48(%r12), %rsi
	movq	8(%r12), %rax
	subq	16(%r12), %rax
	movq	%rsi, %rbx
	subq	40(%r12), %rbx
	sarq	$4, %rbx
	cmpq	%rax, %rbx
	jne	.L242
	movq	8(%rdi), %r12
	movq	48(%r12), %rsi
	movq	8(%r12), %rax
	subq	16(%r12), %rax
	movq	%rsi, %rbx
	subq	40(%r12), %rbx
	sarq	$4, %rbx
	cmpq	%rbx, %rax
	jne	.L242
	movq	16(%rdi), %r12
	movq	48(%r12), %rsi
	movq	8(%r12), %rax
	subq	16(%r12), %rax
	movq	%rsi, %rbx
	subq	40(%r12), %rbx
	sarq	$4, %rbx
	cmpq	%rbx, %rax
	jne	.L242
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L242:
	movq	%rcx, -48(%rbp)
	leaq	32(%r12), %rdi
	movq	%rdx, -40(%rbp)
	cmpq	%rsi, 56(%r12)
	je	.L244
	movdqa	-48(%rbp), %xmm0
	movups	%xmm0, (%rsi)
	addq	$16, 48(%r12)
.L245:
	movq	(%r12), %rax
	addq	%rbx, %rax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L249
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L244:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal11interpreter20ConstantArrayBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L245
.L249:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24171:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0, .-_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder25InsertAsyncIteratorSymbolEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder25InsertAsyncIteratorSymbolEv
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder25InsertAsyncIteratorSymbolEv, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder25InsertAsyncIteratorSymbolEv:
.LFB19308:
	.cfi_startproc
	endbr64
	movl	192(%rdi), %eax
	testl	%eax, %eax
	js	.L256
	cltq
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movl	$9, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0
	movl	%eax, 192(%rbx)
	addq	$8, %rsp
	cltq
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19308:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder25InsertAsyncIteratorSymbolEv, .-_ZN2v88internal11interpreter20ConstantArrayBuilder25InsertAsyncIteratorSymbolEv
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder23InsertClassFieldsSymbolEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder23InsertClassFieldsSymbolEv
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder23InsertClassFieldsSymbolEv, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder23InsertClassFieldsSymbolEv:
.LFB19309:
	.cfi_startproc
	endbr64
	movl	196(%rdi), %eax
	testl	%eax, %eax
	js	.L263
	cltq
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0
	movl	%eax, 196(%rbx)
	addq	$8, %rsp
	cltq
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19309:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder23InsertClassFieldsSymbolEv, .-_ZN2v88internal11interpreter20ConstantArrayBuilder23InsertClassFieldsSymbolEv
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder39InsertEmptyObjectBoilerplateDescriptionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder39InsertEmptyObjectBoilerplateDescriptionEv
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder39InsertEmptyObjectBoilerplateDescriptionEv, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder39InsertEmptyObjectBoilerplateDescriptionEv:
.LFB19310:
	.cfi_startproc
	endbr64
	movl	200(%rdi), %eax
	testl	%eax, %eax
	js	.L270
	cltq
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movl	$11, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0
	movl	%eax, 200(%rbx)
	addq	$8, %rsp
	cltq
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19310:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder39InsertEmptyObjectBoilerplateDescriptionEv, .-_ZN2v88internal11interpreter20ConstantArrayBuilder39InsertEmptyObjectBoilerplateDescriptionEv
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder38InsertEmptyArrayBoilerplateDescriptionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder38InsertEmptyArrayBoilerplateDescriptionEv
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder38InsertEmptyArrayBoilerplateDescriptionEv, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder38InsertEmptyArrayBoilerplateDescriptionEv:
.LFB19311:
	.cfi_startproc
	endbr64
	movl	204(%rdi), %eax
	testl	%eax, %eax
	js	.L277
	cltq
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movl	$12, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0
	movl	%eax, 204(%rbx)
	addq	$8, %rsp
	cltq
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19311:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder38InsertEmptyArrayBoilerplateDescriptionEv, .-_ZN2v88internal11interpreter20ConstantArrayBuilder38InsertEmptyArrayBoilerplateDescriptionEv
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder21InsertEmptyFixedArrayEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder21InsertEmptyFixedArrayEv
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder21InsertEmptyFixedArrayEv, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder21InsertEmptyFixedArrayEv:
.LFB19312:
	.cfi_startproc
	endbr64
	movl	208(%rdi), %eax
	testl	%eax, %eax
	js	.L284
	cltq
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movl	$13, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0
	movl	%eax, 208(%rbx)
	addq	$8, %rsp
	cltq
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19312:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder21InsertEmptyFixedArrayEv, .-_ZN2v88internal11interpreter20ConstantArrayBuilder21InsertEmptyFixedArrayEv
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder22InsertHomeObjectSymbolEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder22InsertHomeObjectSymbolEv
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder22InsertHomeObjectSymbolEv, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder22InsertHomeObjectSymbolEv:
.LFB19313:
	.cfi_startproc
	endbr64
	movl	212(%rdi), %eax
	testl	%eax, %eax
	js	.L291
	cltq
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movl	$14, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0
	movl	%eax, 212(%rbx)
	addq	$8, %rsp
	cltq
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19313:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder22InsertHomeObjectSymbolEv, .-_ZN2v88internal11interpreter20ConstantArrayBuilder22InsertHomeObjectSymbolEv
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder20InsertIteratorSymbolEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder20InsertIteratorSymbolEv
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder20InsertIteratorSymbolEv, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder20InsertIteratorSymbolEv:
.LFB19314:
	.cfi_startproc
	endbr64
	movl	216(%rdi), %eax
	testl	%eax, %eax
	js	.L298
	cltq
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movl	$15, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0
	movl	%eax, 216(%rbx)
	addq	$8, %rsp
	cltq
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19314:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder20InsertIteratorSymbolEv, .-_ZN2v88internal11interpreter20ConstantArrayBuilder20InsertIteratorSymbolEv
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder33InsertInterpreterTrampolineSymbolEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder33InsertInterpreterTrampolineSymbolEv
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder33InsertInterpreterTrampolineSymbolEv, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder33InsertInterpreterTrampolineSymbolEv:
.LFB19315:
	.cfi_startproc
	endbr64
	movl	220(%rdi), %eax
	testl	%eax, %eax
	js	.L305
	cltq
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movl	$16, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0
	movl	%eax, 220(%rbx)
	addq	$8, %rsp
	cltq
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19315:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder33InsertInterpreterTrampolineSymbolEv, .-_ZN2v88internal11interpreter20ConstantArrayBuilder33InsertInterpreterTrampolineSymbolEv
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder9InsertNaNEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder9InsertNaNEv
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder9InsertNaNEv, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder9InsertNaNEv:
.LFB19316:
	.cfi_startproc
	endbr64
	movl	224(%rdi), %eax
	testl	%eax, %eax
	js	.L312
	cltq
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movl	$17, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0
	movl	%eax, 224(%rbx)
	addq	$8, %rsp
	cltq
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19316:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder9InsertNaNEv, .-_ZN2v88internal11interpreter20ConstantArrayBuilder9InsertNaNEv
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder15InsertJumpTableEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder15InsertJumpTableEm
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder15InsertJumpTableEm, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder15InsertJumpTableEm:
.LFB19321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	48(%rbx), %rsi
	movq	8(%rbx), %rax
	subq	16(%rbx), %rax
	movq	%rsi, %r12
	subq	40(%rbx), %r12
	sarq	$4, %r12
	subq	%r12, %rax
	cmpq	%rax, %r13
	jbe	.L314
	movq	8(%rdi), %rbx
	movq	48(%rbx), %rsi
	movq	8(%rbx), %rax
	subq	16(%rbx), %rax
	movq	%rsi, %r12
	subq	40(%rbx), %r12
	sarq	$4, %r12
	subq	%r12, %rax
	cmpq	%rax, %r13
	jbe	.L315
	movq	16(%rdi), %rbx
	movq	48(%rbx), %rsi
	movq	8(%rbx), %rax
	subq	16(%rbx), %rax
	movq	%rsi, %r12
	subq	40(%rbx), %r12
	sarq	$4, %r12
	subq	%r12, %rax
	cmpq	%rax, %r13
	jbe	.L315
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L315:
	movb	$7, -72(%rbp)
.L321:
	leaq	32(%rbx), %rax
	xorl	%r14d, %r14d
	leaq	-80(%rbp), %r15
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L329:
	movdqa	-80(%rbp), %xmm0
	addq	$1, %r14
	movups	%xmm0, (%rsi)
	addq	$16, 48(%rbx)
	cmpq	%r14, %r13
	je	.L317
.L330:
	movq	48(%rbx), %rsi
.L320:
	cmpq	%rsi, 56(%rbx)
	jne	.L329
	movq	%rax, %rdi
	movq	%r15, %rdx
	addq	$1, %r14
	movq	%rax, -88(%rbp)
	call	_ZNSt6vectorIN2v88internal11interpreter20ConstantArrayBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-88(%rbp), %rax
	cmpq	%r14, %r13
	jne	.L330
	.p2align 4,,10
	.p2align 3
.L317:
	movl	%r12d, %eax
	addl	(%rbx), %eax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L331
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	movb	$7, -72(%rbp)
	testq	%r13, %r13
	jne	.L321
	jmp	.L317
.L331:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19321:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder15InsertJumpTableEm, .-_ZN2v88internal11interpreter20ConstantArrayBuilder15InsertJumpTableEm
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder13AllocateIndexENS2_5EntryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder13AllocateIndexENS2_5EntryE
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder13AllocateIndexENS2_5EntryE, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder13AllocateIndexENS2_5EntryE:
.LFB19317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	48(%r12), %rsi
	movq	8(%r12), %rax
	subq	16(%r12), %rax
	movq	%rsi, %rbx
	subq	40(%r12), %rbx
	sarq	$4, %rbx
	cmpq	%rax, %rbx
	jne	.L333
	movq	8(%rdi), %r12
	movq	48(%r12), %rsi
	movq	8(%r12), %rax
	subq	16(%r12), %rax
	movq	%rsi, %rbx
	subq	40(%r12), %rbx
	sarq	$4, %rbx
	cmpq	%rbx, %rax
	jne	.L333
	movq	16(%rdi), %r12
	movq	48(%r12), %rsi
	movq	8(%r12), %rax
	subq	16(%r12), %rax
	movq	%rsi, %rbx
	subq	40(%r12), %rbx
	sarq	$4, %rbx
	cmpq	%rbx, %rax
	jne	.L333
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L333:
	movq	%rcx, -48(%rbp)
	leaq	32(%r12), %rdi
	movq	%rdx, -40(%rbp)
	cmpq	%rsi, 56(%r12)
	je	.L335
	movdqa	-48(%rbp), %xmm0
	movups	%xmm0, (%rsi)
	addq	$16, 48(%r12)
.L336:
	movq	(%r12), %rax
	addq	%rbx, %rax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L340
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal11interpreter20ConstantArrayBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L336
.L340:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19317:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder13AllocateIndexENS2_5EntryE, .-_ZN2v88internal11interpreter20ConstantArrayBuilder13AllocateIndexENS2_5EntryE
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder14InsertDeferredEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder14InsertDeferredEv
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder14InsertDeferredEv, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder14InsertDeferredEv:
.LFB19320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	48(%r12), %rsi
	movq	8(%r12), %rax
	subq	16(%r12), %rax
	movq	%rsi, %rbx
	subq	40(%r12), %rbx
	sarq	$4, %rbx
	cmpq	%rax, %rbx
	jne	.L342
	movq	8(%rdi), %r12
	movq	48(%r12), %rsi
	movq	8(%r12), %rax
	subq	16(%r12), %rax
	movq	%rsi, %rbx
	subq	40(%r12), %rbx
	sarq	$4, %rbx
	cmpq	%rbx, %rax
	jne	.L342
	movq	16(%rdi), %r12
	movq	48(%r12), %rsi
	movq	8(%r12), %rax
	subq	16(%r12), %rax
	movq	%rsi, %rbx
	subq	40(%r12), %rbx
	sarq	$4, %rbx
	cmpq	%rbx, %rax
	jne	.L342
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L342:
	movb	$0, -40(%rbp)
	leaq	32(%r12), %rdi
	cmpq	%rsi, 56(%r12)
	je	.L344
	movdqa	-48(%rbp), %xmm0
	movups	%xmm0, (%rsi)
	addq	$16, 48(%r12)
.L345:
	movl	%ebx, %eax
	addl	(%r12), %eax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L349
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal11interpreter20ConstantArrayBuilder5EntryENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L345
.L349:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19320:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder14InsertDeferredEv, .-_ZN2v88internal11interpreter20ConstantArrayBuilder14InsertDeferredEv
	.section	.text._ZNSt8_Rb_treeIdSt4pairIKdjESt10_Select1stIS2_ESt4lessIdEN2v88internal13ZoneAllocatorIS2_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_,"axG",@progbits,_ZNSt8_Rb_treeIdSt4pairIKdjESt10_Select1stIS2_ESt4lessIdEN2v88internal13ZoneAllocatorIS2_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIdSt4pairIKdjESt10_Select1stIS2_ESt4lessIdEN2v88internal13ZoneAllocatorIS2_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_
	.type	_ZNSt8_Rb_treeIdSt4pairIKdjESt10_Select1stIS2_ESt4lessIdEN2v88internal13ZoneAllocatorIS2_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_, @function
_ZNSt8_Rb_treeIdSt4pairIKdjESt10_Select1stIS2_ESt4lessIdEN2v88internal13ZoneAllocatorIS2_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_:
.LFB23339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %r15
	je	.L429
	movsd	(%rdx), %xmm0
	movsd	32(%rsi), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L421
	movq	32(%rdi), %rbx
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rsi, %rbx
	je	.L354
	movq	%rsi, %rdi
	movsd	%xmm0, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movsd	-56(%rbp), %xmm0
	comisd	32(%rax), %xmm0
	movq	%rax, %rdx
	jbe	.L422
	movl	$0, %ebx
	cmpq	$0, 24(%rax)
	movq	%rbx, %rax
	cmovne	%r12, %rdx
	cmovne	%r12, %rax
.L354:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	comisd	%xmm1, %xmm0
	jbe	.L424
	movq	40(%rdi), %rdx
	cmpq	%rsi, %rdx
	je	.L428
	movq	%rsi, %rdi
	movsd	%xmm0, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movsd	-56(%rbp), %xmm0
	movsd	32(%rax), %xmm1
	movq	%rax, %rdx
	comisd	%xmm0, %xmm1
	jbe	.L425
	movl	$0, %ebx
	cmpq	$0, 24(%r12)
	movq	%rbx, %rax
	cmovne	%rdx, %rax
	cmove	%r12, %rdx
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L429:
	cmpq	$0, 48(%rdi)
	je	.L352
	movq	40(%rdi), %rdx
	movsd	(%r14), %xmm0
	comisd	32(%rdx), %xmm0
	ja	.L428
.L352:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L394
	movsd	(%r14), %xmm1
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L430:
	movq	16(%rbx), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L357
.L431:
	movq	%rax, %rbx
.L356:
	movsd	32(%rbx), %xmm0
	comisd	%xmm1, %xmm0
	ja	.L430
	movq	24(%rbx), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L431
.L357:
	movq	%rbx, %r12
	testb	%dl, %dl
	jne	.L355
.L361:
	xorl	%eax, %eax
	comisd	%xmm0, %xmm1
	cmovbe	%rax, %r12
	cmova	%rax, %rbx
.L362:
	addq	$24, %rsp
	movq	%rbx, %rax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	addq	$24, %rsp
	movq	%rsi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L371
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L433:
	movq	16(%r12), %rax
	movl	$1, %ecx
.L375:
	testq	%rax, %rax
	je	.L372
	movq	%rax, %r12
.L371:
	movsd	32(%r12), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L433
	movq	24(%r12), %rax
	xorl	%ecx, %ecx
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L394:
	movq	%r12, %rbx
.L355:
	cmpq	%rbx, 32(%r13)
	je	.L396
	movq	%rbx, %rdi
	movq	%rbx, %r12
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movsd	(%r14), %xmm1
	movsd	32(%rax), %xmm0
	movq	%rax, %rbx
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L372:
	movq	%r12, %rdx
	testb	%cl, %cl
	jne	.L370
.L376:
	xorl	%eax, %eax
	comisd	%xmm1, %xmm0
	cmovbe	%rax, %rdx
	cmova	%rax, %r12
.L377:
	movq	%r12, %rax
	jmp	.L354
.L432:
	movq	%r15, %r12
.L370:
	movsd	%xmm0, -56(%rbp)
	cmpq	%r12, %rbx
	je	.L400
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r12, %rdx
	movsd	-56(%rbp), %xmm0
	movsd	32(%rax), %xmm1
	movq	%rax, %r12
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L425:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L386
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L435:
	movq	16(%rbx), %rax
	movl	$1, %ecx
.L390:
	testq	%rax, %rax
	je	.L387
	movq	%rax, %rbx
.L386:
	movsd	32(%rbx), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L435
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L387:
	movq	%rbx, %rdx
	testb	%cl, %cl
	jne	.L385
.L391:
	xorl	%eax, %eax
	comisd	%xmm1, %xmm0
	cmovbe	%rax, %rdx
	cmova	%rax, %rbx
.L392:
	movq	%rbx, %rax
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L396:
	movq	%rbx, %r12
	xorl	%ebx, %ebx
	jmp	.L362
.L434:
	movq	%r15, %rbx
.L385:
	movsd	%xmm0, -56(%rbp)
	cmpq	%rbx, 32(%r13)
	je	.L404
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rdx
	movsd	-56(%rbp), %xmm0
	movsd	32(%rax), %xmm1
	movq	%rax, %rbx
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L400:
	movq	%r12, %rdx
	xorl	%r12d, %r12d
	jmp	.L377
.L404:
	movq	%rbx, %rdx
	xorl	%ebx, %ebx
	jmp	.L392
	.cfi_endproc
.LFE23339:
	.size	_ZNSt8_Rb_treeIdSt4pairIKdjESt10_Select1stIS2_ESt4lessIdEN2v88internal13ZoneAllocatorIS2_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_, .-_ZNSt8_Rb_treeIdSt4pairIKdjESt10_Select1stIS2_ESt4lessIdEN2v88internal13ZoneAllocatorIS2_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEd
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEd, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEd:
.LFB19301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	ucomisd	%xmm0, %xmm0
	jp	.L468
	movq	160(%rdi), %rax
	leaq	152(%rdi), %r13
	testq	%rax, %rax
	je	.L440
	movq	%r13, %rdx
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L469:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L442
.L441:
	comisd	32(%rax), %xmm0
	jbe	.L469
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L441
.L442:
	cmpq	%rdx, %r13
	je	.L440
	movsd	32(%rdx), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L466
.L440:
	movq	%xmm0, %rsi
	movl	$4, %edx
	movq	%rbx, %rdi
	movq	%r13, %r14
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0
	movsd	-56(%rbp), %xmm0
	movl	%eax, %r12d
	movq	160(%rbx), %rax
	testq	%rax, %rax
	jne	.L448
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L470:
	movq	%rax, %r14
	movq	16(%rax), %rax
.L451:
	testq	%rax, %rax
	je	.L449
.L448:
	comisd	32(%rax), %xmm0
	jbe	.L470
	movq	24(%rax), %rax
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L466:
	movl	40(%rdx), %eax
.L436:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L468:
	.cfi_restore_state
	movl	224(%rdi), %eax
	testl	%eax, %eax
	js	.L471
	cltq
.L474:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L449:
	.cfi_restore_state
	cmpq	%r14, %r13
	je	.L447
	movsd	32(%r14), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L452
.L447:
	movq	136(%rbx), %rdi
	leaq	136(%rbx), %r8
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$47, %rax
	jbe	.L472
	leaq	48(%r15), %rax
	movq	%rax, 16(%rdi)
.L455:
	movq	%r14, %rsi
	movsd	%xmm0, 32(%r15)
	leaq	32(%r15), %rdx
	movq	%r8, %rdi
	movl	$0, 40(%r15)
	call	_ZNSt8_Rb_treeIdSt4pairIKdjESt10_Select1stIS2_ESt4lessIdEN2v88internal13ZoneAllocatorIS2_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_
	movq	%rax, %r14
	testq	%rdx, %rdx
	je	.L452
	cmpq	%rdx, %r13
	je	.L459
	testq	%rax, %rax
	je	.L473
.L459:
	movl	$1, %edi
.L457:
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r15, %r14
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 184(%rbx)
.L452:
	movl	%r12d, 40(%r14)
	movl	%r12d, %eax
	jmp	.L436
.L471:
	xorl	%esi, %esi
	movl	$17, %edx
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0
	movl	%eax, 224(%rbx)
	cltq
	jmp	.L474
.L472:
	movl	$48, %esi
	movq	%r8, -56(%rbp)
	movsd	%xmm0, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movsd	-64(%rbp), %xmm0
	movq	%rax, %r15
	jmp	.L455
.L473:
	xorl	%edi, %edi
	movsd	32(%rdx), %xmm0
	comisd	32(%r15), %xmm0
	seta	%dil
	jmp	.L457
	.cfi_endproc
.LFE19301:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEd, .-_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEd
	.section	.text._ZN2v84base19TemplateHashMapImplIljNS0_18KeyEqualityMatcherIlEENS_8internal20ZoneAllocationPolicyEE6ResizeES5_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplIljNS0_18KeyEqualityMatcherIlEENS_8internal20ZoneAllocationPolicyEE6ResizeES5_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplIljNS0_18KeyEqualityMatcherIlEENS_8internal20ZoneAllocationPolicyEE6ResizeES5_
	.type	_ZN2v84base19TemplateHashMapImplIljNS0_18KeyEqualityMatcherIlEENS_8internal20ZoneAllocationPolicyEE6ResizeES5_, @function
_ZN2v84base19TemplateHashMapImplIljNS0_18KeyEqualityMatcherIlEENS_8internal20ZoneAllocationPolicyEE6ResizeES5_:
.LFB23353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	movq	24(%r15), %rdx
	movq	(%rdi), %r13
	movl	12(%rdi), %r14d
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rsi
	movq	%rax, %rbx
	movq	16(%r15), %rax
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L503
	addq	%rax, %rsi
	movq	%rsi, 16(%r15)
.L477:
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L504
	movl	%ebx, 8(%r12)
	testl	%ebx, %ebx
	je	.L479
	movb	$0, 16(%rax)
	cmpl	$1, 8(%r12)
	movl	$24, %edx
	movl	$1, %eax
	jbe	.L479
	.p2align 4,,10
	.p2align 3
.L480:
	movq	(%r12), %rcx
	addq	$1, %rax
	movb	$0, 16(%rcx,%rdx)
	movl	8(%r12), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L480
.L479:
	movl	$0, 12(%r12)
	testl	%r14d, %r14d
	je	.L475
.L482:
	cmpb	$0, 16(%r13)
	jne	.L505
.L483:
	addq	$24, %r13
	cmpb	$0, 16(%r13)
	je	.L483
.L505:
	movl	8(%r12), %eax
	movl	12(%r13), %ebx
	movq	(%r12), %rdi
	movq	0(%r13), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	jne	.L485
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L506:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L484
.L485:
	cmpq	%rsi, (%rdx)
	jne	.L506
.L484:
	movl	8(%r13), %eax
	movq	%rsi, (%rdx)
	movl	%ebx, 12(%rdx)
	movl	%eax, 8(%rdx)
	movb	$1, 16(%rdx)
	movl	12(%r12), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%r12)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%r12), %eax
	jnb	.L486
.L489:
	addq	$24, %r13
	subl	$1, %r14d
	jne	.L482
.L475:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L486:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v84base19TemplateHashMapImplIljNS0_18KeyEqualityMatcherIlEENS_8internal20ZoneAllocationPolicyEE6ResizeES5_
	movl	8(%r12), %eax
	movq	(%r12), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L489
	movq	0(%r13), %rdi
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L507:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L489
.L490:
	cmpq	(%rdx), %rdi
	jne	.L507
	jmp	.L489
.L503:
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L477
.L504:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23353:
	.size	_ZN2v84base19TemplateHashMapImplIljNS0_18KeyEqualityMatcherIlEENS_8internal20ZoneAllocationPolicyEE6ResizeES5_, .-_ZN2v84base19TemplateHashMapImplIljNS0_18KeyEqualityMatcherIlEENS_8internal20ZoneAllocationPolicyEE6ResizeES5_
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_5ScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_5ScopeE
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_5ScopeE, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_5ScopeE:
.LFB19306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	232(%rdi), %r12
	movq	%rsi, %rdi
	call	_ZN2v84base10hash_valueEm@PLT
	movq	24(%r15), %rcx
	movq	%rax, %r13
	movl	32(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, %eax
	andl	%r13d, %eax
	leaq	(%rax,%rax,2), %rsi
	leaq	(%rcx,%rsi,8), %rbx
	cmpb	$0, 16(%rbx)
	jne	.L511
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L519:
	addq	$1, %rax
	andq	%rdx, %rax
	leaq	(%rax,%rax,2), %rsi
	leaq	(%rcx,%rsi,8), %rbx
	cmpb	$0, 16(%rbx)
	je	.L509
.L511:
	cmpq	(%rbx), %r14
	jne	.L519
.L510:
	movl	8(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L509:
	.cfi_restore_state
	movl	$6, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0
	movq	%r14, (%rbx)
	movl	%eax, 8(%rbx)
	movl	%r13d, 12(%rbx)
	movb	$1, 16(%rbx)
	movl	36(%r15), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 36(%r15)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	32(%r15), %eax
	jb	.L510
	movq	%r12, %rsi
	leaq	24(%r15), %rdi
	call	_ZN2v84base19TemplateHashMapImplIljNS0_18KeyEqualityMatcherIlEENS_8internal20ZoneAllocationPolicyEE6ResizeES5_
	movl	32(%r15), %eax
	movq	24(%r15), %rsi
	leal	-1(%rax), %ecx
	movl	%r13d, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rbx
	cmpb	$0, 16(%rbx)
	jne	.L513
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L520:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rbx
	cmpb	$0, 16(%rbx)
	je	.L510
.L513:
	cmpq	(%rbx), %r14
	jne	.L520
	jmp	.L510
	.cfi_endproc
.LFE19306:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_5ScopeE, .-_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_5ScopeE
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE:
.LFB19302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	24(%rsi), %r12d
	movl	32(%rdi), %eax
	movq	24(%rdi), %rcx
	shrl	$2, %r12d
	leal	-1(%rax), %edx
	movl	%r12d, %eax
	andl	%edx, %eax
	leaq	(%rax,%rax,2), %rsi
	leaq	(%rcx,%rsi,8), %rbx
	cmpb	$0, 16(%rbx)
	jne	.L524
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L532:
	addq	$1, %rax
	andq	%rdx, %rax
	leaq	(%rax,%rax,2), %rsi
	leaq	(%rcx,%rsi,8), %rbx
	cmpb	$0, 16(%rbx)
	je	.L522
.L524:
	cmpq	(%rbx), %r13
	jne	.L532
.L523:
	movl	8(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	.cfi_restore_state
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	232(%r14), %r15
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0
	movq	%r13, (%rbx)
	movl	%eax, 8(%rbx)
	movl	%r12d, 12(%rbx)
	movb	$1, 16(%rbx)
	movl	36(%r14), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 36(%r14)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	32(%r14), %eax
	jb	.L523
	movq	%r15, %rsi
	leaq	24(%r14), %rdi
	call	_ZN2v84base19TemplateHashMapImplIljNS0_18KeyEqualityMatcherIlEENS_8internal20ZoneAllocationPolicyEE6ResizeES5_
	movl	32(%r14), %eax
	movq	24(%r14), %rsi
	leal	-1(%rax), %ecx
	andl	%ecx, %r12d
	movl	%r12d, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rbx
	cmpb	$0, 16(%rbx)
	jne	.L526
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L533:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rbx
	cmpb	$0, 16(%rbx)
	je	.L523
.L526:
	cmpq	(%rbx), %r13
	jne	.L533
	jmp	.L523
	.cfi_endproc
.LFE19302:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE, .-_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder6InsertENS0_9AstBigIntE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertENS0_9AstBigIntE
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertENS0_9AstBigIntE, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertENS0_9AstBigIntE:
.LFB19304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	232(%rdi), %r12
	movq	%rsi, %rdi
	call	_ZN2v84base10hash_valueEm@PLT
	movq	24(%r15), %rcx
	movq	%rax, %r13
	movl	32(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, %eax
	andl	%r13d, %eax
	leaq	(%rax,%rax,2), %rsi
	leaq	(%rcx,%rsi,8), %rbx
	cmpb	$0, 16(%rbx)
	jne	.L537
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L545:
	addq	$1, %rax
	andq	%rdx, %rax
	leaq	(%rax,%rax,2), %rsi
	leaq	(%rcx,%rsi,8), %rbx
	cmpb	$0, 16(%rbx)
	je	.L535
.L537:
	cmpq	(%rbx), %r14
	jne	.L545
.L536:
	movl	8(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	.cfi_restore_state
	movl	$5, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0
	movq	%r14, (%rbx)
	movl	%eax, 8(%rbx)
	movl	%r13d, 12(%rbx)
	movb	$1, 16(%rbx)
	movl	36(%r15), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 36(%r15)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	32(%r15), %eax
	jb	.L536
	movq	%r12, %rsi
	leaq	24(%r15), %rdi
	call	_ZN2v84base19TemplateHashMapImplIljNS0_18KeyEqualityMatcherIlEENS_8internal20ZoneAllocationPolicyEE6ResizeES5_
	movl	32(%r15), %eax
	movq	24(%r15), %rsi
	leal	-1(%rax), %ecx
	movl	%r13d, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rbx
	cmpb	$0, 16(%rbx)
	jne	.L539
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L546:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rbx
	cmpb	$0, 16(%rbx)
	je	.L536
.L539:
	cmpq	(%rbx), %r14
	jne	.L546
	jmp	.L536
	.cfi_endproc
.LFE19304:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertENS0_9AstBigIntE, .-_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertENS0_9AstBigIntE
	.section	.text._ZNSt8_Rb_treeIN2v88internal3SmiESt4pairIKS2_jESt10_Select1stIS5_ESt4lessIS2_ENS1_13ZoneAllocatorIS5_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS4_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal3SmiESt4pairIKS2_jESt10_Select1stIS5_ESt4lessIS2_ENS1_13ZoneAllocatorIS5_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal3SmiESt4pairIKS2_jESt10_Select1stIS5_ESt4lessIS2_ENS1_13ZoneAllocatorIS5_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS4_
	.type	_ZNSt8_Rb_treeIN2v88internal3SmiESt4pairIKS2_jESt10_Select1stIS5_ESt4lessIS2_ENS1_13ZoneAllocatorIS5_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS4_, @function
_ZNSt8_Rb_treeIN2v88internal3SmiESt4pairIKS2_jESt10_Select1stIS5_ESt4lessIS2_ENS1_13ZoneAllocatorIS5_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS4_:
.LFB23369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %r15
	je	.L597
	movq	(%rdx), %r14
	cmpq	%r14, 32(%rsi)
	jbe	.L558
	movq	32(%rdi), %rbx
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rsi, %rbx
	je	.L550
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpq	%r14, 32(%rax)
	jnb	.L560
	movl	$0, %ebx
	cmpq	$0, 24(%rax)
	movq	%rbx, %rax
	cmovne	%r12, %rdx
	cmovne	%r12, %rax
.L550:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L558:
	.cfi_restore_state
	jnb	.L569
	movq	40(%rdi), %rdx
	cmpq	%rsi, %rdx
	je	.L596
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpq	32(%rax), %r14
	jnb	.L571
	movl	$0, %ebx
	cmpq	$0, 24(%r12)
	movq	%rbx, %rax
	cmovne	%rdx, %rax
	cmove	%r12, %rdx
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L597:
	cmpq	$0, 48(%rdi)
	je	.L549
	movq	40(%rdi), %rdx
	movq	32(%rdx), %rax
	cmpq	%rax, (%r14)
	ja	.L596
.L549:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L580
	movq	(%r14), %rsi
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L598:
	movq	16(%rbx), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L553
.L599:
	movq	%rax, %rbx
.L552:
	movq	32(%rbx), %rdx
	cmpq	%rdx, %rsi
	jb	.L598
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L599
.L553:
	movq	%rbx, %r12
	testb	%cl, %cl
	jne	.L551
.L556:
	xorl	%eax, %eax
	cmpq	%rsi, %rdx
	cmovb	%rax, %rbx
	cmovnb	%rax, %r12
.L557:
	addq	$8, %rsp
	movq	%rbx, %rax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L569:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rsi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L596:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L560:
	.cfi_restore_state
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L563
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L601:
	movq	16(%r12), %rax
	movl	$1, %esi
.L566:
	testq	%rax, %rax
	je	.L564
	movq	%rax, %r12
.L563:
	movq	32(%r12), %rcx
	cmpq	%rcx, %r14
	jb	.L601
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L580:
	movq	%r12, %rbx
.L551:
	cmpq	%rbx, 32(%r13)
	je	.L582
	movq	%rbx, %rdi
	movq	%rbx, %r12
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	(%r14), %rsi
	movq	32(%rax), %rdx
	movq	%rax, %rbx
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L564:
	movq	%r12, %rdx
	testb	%sil, %sil
	jne	.L562
.L567:
	xorl	%eax, %eax
	cmpq	%rcx, %r14
	cmova	%rax, %r12
	cmovbe	%rax, %rdx
.L568:
	movq	%r12, %rax
	jmp	.L550
.L600:
	movq	%r15, %r12
.L562:
	cmpq	%r12, %rbx
	je	.L586
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r12, %rdx
	movq	32(%rax), %rcx
	movq	%rax, %r12
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L571:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L574
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L603:
	movq	16(%rbx), %rax
	movl	$1, %esi
.L577:
	testq	%rax, %rax
	je	.L575
	movq	%rax, %rbx
.L574:
	movq	32(%rbx), %rcx
	cmpq	%rcx, %r14
	jb	.L603
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L575:
	movq	%rbx, %rdx
	testb	%sil, %sil
	jne	.L573
.L578:
	xorl	%eax, %eax
	cmpq	%rcx, %r14
	cmova	%rax, %rbx
	cmovbe	%rax, %rdx
.L579:
	movq	%rbx, %rax
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L582:
	movq	%rbx, %r12
	xorl	%ebx, %ebx
	jmp	.L557
.L602:
	movq	%r15, %rbx
.L573:
	cmpq	%rbx, 32(%r13)
	je	.L590
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rdx
	movq	32(%rax), %rcx
	movq	%rax, %rbx
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L586:
	movq	%r12, %rdx
	xorl	%r12d, %r12d
	jmp	.L568
.L590:
	movq	%rbx, %rdx
	xorl	%ebx, %ebx
	jmp	.L579
	.cfi_endproc
.LFE23369:
	.size	_ZNSt8_Rb_treeIN2v88internal3SmiESt4pairIKS2_jESt10_Select1stIS5_ESt4lessIS2_ENS1_13ZoneAllocatorIS5_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS4_, .-_ZNSt8_Rb_treeIN2v88internal3SmiESt4pairIKS2_jESt10_Select1stIS5_ESt4lessIS2_ENS1_13ZoneAllocatorIS5_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS4_
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder21AllocateReservedEntryENS0_3SmiE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder21AllocateReservedEntryENS0_3SmiE
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder21AllocateReservedEntryENS0_3SmiE, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder21AllocateReservedEntryENS0_3SmiE:
.LFB19325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	64(%rbx), %r15
	movq	%r15, %r12
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder18AllocateIndexArrayENS2_5EntryEm.constprop.0
	movq	-56(%rbp), %rcx
	movl	%eax, %r13d
	movq	72(%rbx), %rax
	testq	%rax, %rax
	jne	.L606
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L621:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L607
.L606:
	cmpq	32(%rax), %rcx
	jbe	.L621
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L606
.L607:
	cmpq	%r12, %r15
	je	.L605
	cmpq	32(%r12), %rcx
	jnb	.L610
.L605:
	movq	48(%rbx), %rdi
	leaq	48(%rbx), %r8
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$47, %rax
	jbe	.L622
	leaq	48(%r14), %rax
	movq	%rax, 16(%rdi)
.L612:
	movq	%rcx, 32(%r14)
	movq	%r12, %rsi
	leaq	32(%r14), %rdx
	movq	%r8, %rdi
	movl	$0, 40(%r14)
	call	_ZNSt8_Rb_treeIN2v88internal3SmiESt4pairIKS2_jESt10_Select1stIS5_ESt4lessIS2_ENS1_13ZoneAllocatorIS5_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERS4_
	movq	%rax, %r12
	testq	%rdx, %rdx
	je	.L610
	cmpq	%rdx, %r15
	je	.L616
	testq	%rax, %rax
	jne	.L616
	xorl	%edi, %edi
	movq	32(%rdx), %rax
	cmpq	%rax, 32(%r14)
	setb	%dil
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L616:
	movl	$1, %edi
.L614:
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%r14, %r12
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 96(%rbx)
.L610:
	movl	%r13d, 40(%r12)
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L622:
	.cfi_restore_state
	movl	$48, %esi
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	jmp	.L612
	.cfi_endproc
.LFE19325:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder21AllocateReservedEntryENS0_3SmiE, .-_ZN2v88internal11interpreter20ConstantArrayBuilder21AllocateReservedEntryENS0_3SmiE
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder6InsertENS0_3SmiE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertENS0_3SmiE
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertENS0_3SmiE, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertENS0_3SmiE:
.LFB19300:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	testq	%rax, %rax
	je	.L624
	leaq	64(%rdi), %rcx
	movq	%rcx, %rdx
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L637:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L626
.L625:
	cmpq	%rsi, 32(%rax)
	jnb	.L637
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L625
.L626:
	cmpq	%rdx, %rcx
	je	.L624
	cmpq	%rsi, 32(%rdx)
	jbe	.L629
.L624:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder21AllocateReservedEntryENS0_3SmiE
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L629:
	.cfi_restore 6
	movl	40(%rdx), %eax
	ret
	.cfi_endproc
.LFE19300:
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertENS0_3SmiE, .-_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertENS0_3SmiE
	.section	.text.unlikely._ZN2v88internal11interpreter20ConstantArrayBuilder19CommitReservedEntryENS1_11OperandSizeENS0_3SmiE,"ax",@progbits
	.align 2
.LCOLDB9:
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder19CommitReservedEntryENS1_11OperandSizeENS0_3SmiE,"ax",@progbits
.LHOTB9:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder19CommitReservedEntryENS1_11OperandSizeENS0_3SmiE
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder19CommitReservedEntryENS1_11OperandSizeENS0_3SmiE, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder19CommitReservedEntryENS1_11OperandSizeENS0_3SmiE:
.LFB19326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpb	$2, %cl
	je	.L639
	ja	.L640
	testb	%cl, %cl
	je	.L641
	movq	(%rdi), %rax
.L645:
	subq	$1, 16(%rax)
	movq	72(%rdi), %rax
	leaq	64(%rdi), %r8
	testq	%rax, %rax
	je	.L646
	movq	%r8, %rdx
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L668:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L648
.L647:
	cmpq	32(%rax), %rsi
	jbe	.L668
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L647
.L648:
	cmpq	%rdx, %r8
	je	.L646
	cmpq	32(%rdx), %rsi
	jnb	.L651
.L646:
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder21AllocateReservedEntryENS0_3SmiE
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L640:
	.cfi_restore_state
	cmpb	$4, %cl
	jne	.L669
	movq	16(%rdi), %rax
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L651:
	cmpb	$2, %cl
	je	.L653
	ja	.L654
	testb	%cl, %cl
	je	.L641
	movq	(%rdi), %rcx
.L658:
	movl	40(%rdx), %eax
	movq	8(%rcx), %rdx
	addq	(%rcx), %rdx
	subq	$1, %rdx
	cmpq	%rdx, %rax
	ja	.L646
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L654:
	.cfi_restore_state
	cmpb	$4, %cl
	jne	.L670
	movq	16(%rdi), %rcx
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L639:
	movq	8(%rdi), %rax
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L653:
	movq	8(%rdi), %rcx
	jmp	.L658
.L641:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L670:
	jmp	.L663
.L669:
	jmp	.L662
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal11interpreter20ConstantArrayBuilder19CommitReservedEntryENS1_11OperandSizeENS0_3SmiE
	.cfi_startproc
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder19CommitReservedEntryENS1_11OperandSizeENS0_3SmiE.cold, @function
_ZN2v88internal11interpreter20ConstantArrayBuilder19CommitReservedEntryENS1_11OperandSizeENS0_3SmiE.cold:
.LFSB19326:
.L663:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movq	0, %rax
	ud2
.L662:
	movq	16, %rax
	ud2
	.cfi_endproc
.LFE19326:
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder19CommitReservedEntryENS1_11OperandSizeENS0_3SmiE
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder19CommitReservedEntryENS1_11OperandSizeENS0_3SmiE, .-_ZN2v88internal11interpreter20ConstantArrayBuilder19CommitReservedEntryENS1_11OperandSizeENS0_3SmiE
	.section	.text.unlikely._ZN2v88internal11interpreter20ConstantArrayBuilder19CommitReservedEntryENS1_11OperandSizeENS0_3SmiE
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder19CommitReservedEntryENS1_11OperandSizeENS0_3SmiE.cold, .-_ZN2v88internal11interpreter20ConstantArrayBuilder19CommitReservedEntryENS1_11OperandSizeENS0_3SmiE.cold
.LCOLDE9:
	.section	.text._ZN2v88internal11interpreter20ConstantArrayBuilder19CommitReservedEntryENS1_11OperandSizeENS0_3SmiE
.LHOTE9:
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySliceC2EPNS0_4ZoneEmmNS1_11OperandSizeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySliceC2EPNS0_4ZoneEmmNS1_11OperandSizeE, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySliceC2EPNS0_4ZoneEmmNS1_11OperandSizeE:
.LFB24053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24053:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySliceC2EPNS0_4ZoneEmmNS1_11OperandSizeE, .-_GLOBAL__sub_I__ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySliceC2EPNS0_4ZoneEmmNS1_11OperandSizeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter20ConstantArrayBuilder18ConstantArraySliceC2EPNS0_4ZoneEmmNS1_11OperandSizeE
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder14k32BitCapacityE
	.section	.rodata._ZN2v88internal11interpreter20ConstantArrayBuilder14k32BitCapacityE,"a"
	.align 8
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder14k32BitCapacityE, @object
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder14k32BitCapacityE, 8
_ZN2v88internal11interpreter20ConstantArrayBuilder14k32BitCapacityE:
	.quad	4294901760
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder14k16BitCapacityE
	.section	.rodata._ZN2v88internal11interpreter20ConstantArrayBuilder14k16BitCapacityE,"a"
	.align 8
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder14k16BitCapacityE, @object
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder14k16BitCapacityE, 8
_ZN2v88internal11interpreter20ConstantArrayBuilder14k16BitCapacityE:
	.quad	65280
	.globl	_ZN2v88internal11interpreter20ConstantArrayBuilder13k8BitCapacityE
	.section	.rodata._ZN2v88internal11interpreter20ConstantArrayBuilder13k8BitCapacityE,"a"
	.align 8
	.type	_ZN2v88internal11interpreter20ConstantArrayBuilder13k8BitCapacityE, @object
	.size	_ZN2v88internal11interpreter20ConstantArrayBuilder13k8BitCapacityE, 8
_ZN2v88internal11interpreter20ConstantArrayBuilder13k8BitCapacityE:
	.quad	256
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	0
	.quad	256
	.align 16
.LC2:
	.quad	256
	.quad	65280
	.align 16
.LC3:
	.quad	65536
	.quad	4294901760
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
