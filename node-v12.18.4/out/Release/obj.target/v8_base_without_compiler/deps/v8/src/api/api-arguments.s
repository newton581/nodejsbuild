	.file	"api-arguments.cc"
	.text
	.section	.text._ZN2v88internal11Relocatable21PostGarbageCollectionEv,"axG",@progbits,_ZN2v88internal11Relocatable21PostGarbageCollectionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11Relocatable21PostGarbageCollectionEv
	.type	_ZN2v88internal11Relocatable21PostGarbageCollectionEv, @function
_ZN2v88internal11Relocatable21PostGarbageCollectionEv:
.LFB6461:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6461:
	.size	_ZN2v88internal11Relocatable21PostGarbageCollectionEv, .-_ZN2v88internal11Relocatable21PostGarbageCollectionEv
	.section	.text._ZN2v88internal25FunctionCallbackArgumentsD2Ev,"axG",@progbits,_ZN2v88internal25FunctionCallbackArgumentsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25FunctionCallbackArgumentsD2Ev
	.type	_ZN2v88internal25FunctionCallbackArgumentsD2Ev, @function
_ZN2v88internal25FunctionCallbackArgumentsD2Ev:
.LFB22970:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	%rdx, 41704(%rax)
	ret
	.cfi_endproc
.LFE22970:
	.size	_ZN2v88internal25FunctionCallbackArgumentsD2Ev, .-_ZN2v88internal25FunctionCallbackArgumentsD2Ev
	.weak	_ZN2v88internal25FunctionCallbackArgumentsD1Ev
	.set	_ZN2v88internal25FunctionCallbackArgumentsD1Ev,_ZN2v88internal25FunctionCallbackArgumentsD2Ev
	.section	.text._ZN2v88internal25PropertyCallbackArgumentsD2Ev,"axG",@progbits,_ZN2v88internal25PropertyCallbackArgumentsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25PropertyCallbackArgumentsD2Ev
	.type	_ZN2v88internal25PropertyCallbackArgumentsD2Ev, @function
_ZN2v88internal25PropertyCallbackArgumentsD2Ev:
.LFB22974:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	%rdx, 41704(%rax)
	ret
	.cfi_endproc
.LFE22974:
	.size	_ZN2v88internal25PropertyCallbackArgumentsD2Ev, .-_ZN2v88internal25PropertyCallbackArgumentsD2Ev
	.weak	_ZN2v88internal25PropertyCallbackArgumentsD1Ev
	.set	_ZN2v88internal25PropertyCallbackArgumentsD1Ev,_ZN2v88internal25PropertyCallbackArgumentsD2Ev
	.section	.text._ZN2v88internal15CustomArgumentsINS_20FunctionCallbackInfoINS_5ValueEEEE15IterateInstanceEPNS0_11RootVisitorE,"axG",@progbits,_ZN2v88internal15CustomArgumentsINS_20FunctionCallbackInfoINS_5ValueEEEE15IterateInstanceEPNS0_11RootVisitorE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CustomArgumentsINS_20FunctionCallbackInfoINS_5ValueEEEE15IterateInstanceEPNS0_11RootVisitorE
	.type	_ZN2v88internal15CustomArgumentsINS_20FunctionCallbackInfoINS_5ValueEEEE15IterateInstanceEPNS0_11RootVisitorE, @function
_ZN2v88internal15CustomArgumentsINS_20FunctionCallbackInfoINS_5ValueEEEE15IterateInstanceEPNS0_11RootVisitorE:
.LFB23002:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	movq	%rdi, %rax
	movq	%rsi, %rdi
	movl	$7, %esi
	leaq	24(%rax), %rcx
	leaq	72(%rax), %r8
	movq	16(%rdx), %r9
	xorl	%edx, %edx
	jmp	*%r9
	.cfi_endproc
.LFE23002:
	.size	_ZN2v88internal15CustomArgumentsINS_20FunctionCallbackInfoINS_5ValueEEEE15IterateInstanceEPNS0_11RootVisitorE, .-_ZN2v88internal15CustomArgumentsINS_20FunctionCallbackInfoINS_5ValueEEEE15IterateInstanceEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal15CustomArgumentsINS_20PropertyCallbackInfoINS_5ValueEEEE15IterateInstanceEPNS0_11RootVisitorE,"axG",@progbits,_ZN2v88internal15CustomArgumentsINS_20PropertyCallbackInfoINS_5ValueEEEE15IterateInstanceEPNS0_11RootVisitorE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CustomArgumentsINS_20PropertyCallbackInfoINS_5ValueEEEE15IterateInstanceEPNS0_11RootVisitorE
	.type	_ZN2v88internal15CustomArgumentsINS_20PropertyCallbackInfoINS_5ValueEEEE15IterateInstanceEPNS0_11RootVisitorE, @function
_ZN2v88internal15CustomArgumentsINS_20PropertyCallbackInfoINS_5ValueEEEE15IterateInstanceEPNS0_11RootVisitorE:
.LFB23003:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	movq	%rdi, %rax
	movq	%rsi, %rdi
	movl	$7, %esi
	leaq	24(%rax), %rcx
	leaq	80(%rax), %r8
	movq	16(%rdx), %r9
	xorl	%edx, %edx
	jmp	*%r9
	.cfi_endproc
.LFE23003:
	.size	_ZN2v88internal15CustomArgumentsINS_20PropertyCallbackInfoINS_5ValueEEEE15IterateInstanceEPNS0_11RootVisitorE, .-_ZN2v88internal15CustomArgumentsINS_20PropertyCallbackInfoINS_5ValueEEEE15IterateInstanceEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal25PropertyCallbackArgumentsD0Ev,"axG",@progbits,_ZN2v88internal25PropertyCallbackArgumentsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25PropertyCallbackArgumentsD0Ev
	.type	_ZN2v88internal25PropertyCallbackArgumentsD0Ev, @function
_ZN2v88internal25PropertyCallbackArgumentsD0Ev:
.LFB22976:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movl	$80, %esi
	movq	%rdx, 41704(%rax)
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22976:
	.size	_ZN2v88internal25PropertyCallbackArgumentsD0Ev, .-_ZN2v88internal25PropertyCallbackArgumentsD0Ev
	.section	.text._ZN2v88internal25FunctionCallbackArgumentsD0Ev,"axG",@progbits,_ZN2v88internal25FunctionCallbackArgumentsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25FunctionCallbackArgumentsD0Ev
	.type	_ZN2v88internal25FunctionCallbackArgumentsD0Ev, @function
_ZN2v88internal25FunctionCallbackArgumentsD0Ev:
.LFB22972:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	movl	$88, %esi
	movq	%rdx, 41704(%rax)
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22972:
	.size	_ZN2v88internal25FunctionCallbackArgumentsD0Ev, .-_ZN2v88internal25FunctionCallbackArgumentsD0Ev
	.section	.text._ZN2v88internal25PropertyCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25PropertyCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE
	.type	_ZN2v88internal25PropertyCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE, @function
_ZN2v88internal25PropertyCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE:
.LFB18702:
	.cfi_startproc
	endbr64
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	%rsi, %xmm2
	movq	%r9, %rdx
	movq	41704(%rsi), %rax
	punpcklqdq	%xmm1, %xmm0
	sarq	$32, %rdx
	movq	%rsi, 8(%rdi)
	movq	%rdi, 41704(%rsi)
	salq	$32, %rdx
	testb	%r9b, %r9b
	movups	%xmm0, 64(%rdi)
	movq	%r8, %xmm0
	movq	%rax, 16(%rdi)
	punpcklqdq	%xmm2, %xmm0
	leaq	16+_ZTVN2v88internal25PropertyCallbackArgumentsE(%rip), %rax
	movq	%rax, (%rdi)
	movabsq	$8589934592, %rax
	movups	%xmm0, 32(%rdi)
	movq	96(%rsi), %xmm0
	cmovne	%rdx, %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 24(%rdi)
	movups	%xmm0, 48(%rdi)
	ret
	.cfi_endproc
.LFE18702:
	.size	_ZN2v88internal25PropertyCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE, .-_ZN2v88internal25PropertyCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE
	.globl	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE
	.set	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE,_ZN2v88internal25PropertyCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE
	.section	.text._ZN2v88internal25FunctionCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectENS0_10HeapObjectES4_S5_Pmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25FunctionCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectENS0_10HeapObjectES4_S5_Pmi
	.type	_ZN2v88internal25FunctionCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectENS0_10HeapObjectES4_S5_Pmi, @function
_ZN2v88internal25FunctionCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectENS0_10HeapObjectES4_S5_Pmi:
.LFB18705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %xmm1
	movq	%rsi, %xmm2
	movq	%rdx, %xmm0
	punpcklqdq	%xmm2, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rsi, 8(%rdi)
	movq	%r9, -8(%rbp)
	movq	41704(%rsi), %rax
	movq	%rdi, 41704(%rsi)
	movups	%xmm1, 24(%rdi)
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVN2v88internal25FunctionCallbackArgumentsE(%rip), %rax
	movq	96(%rsi), %xmm1
	movq	%rax, (%rdi)
	movq	16(%rbp), %rax
	movhps	-8(%rbp), %xmm0
	punpcklqdq	%xmm1, %xmm1
	movq	%rax, 72(%rdi)
	movl	24(%rbp), %eax
	movups	%xmm1, 40(%rdi)
	movl	%eax, 80(%rdi)
	movups	%xmm0, 56(%rdi)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18705:
	.size	_ZN2v88internal25FunctionCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectENS0_10HeapObjectES4_S5_Pmi, .-_ZN2v88internal25FunctionCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectENS0_10HeapObjectES4_S5_Pmi
	.globl	_ZN2v88internal25FunctionCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectENS0_10HeapObjectES4_S5_Pmi
	.set	_ZN2v88internal25FunctionCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectENS0_10HeapObjectES4_S5_Pmi,_ZN2v88internal25FunctionCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectENS0_10HeapObjectES4_S5_Pmi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal25PropertyCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal25PropertyCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE, @function
_GLOBAL__sub_I__ZN2v88internal25PropertyCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE:
.LFB23004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23004:
	.size	_GLOBAL__sub_I__ZN2v88internal25PropertyCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE, .-_GLOBAL__sub_I__ZN2v88internal25PropertyCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal25PropertyCallbackArgumentsC2EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE
	.weak	_ZTVN2v88internal25PropertyCallbackArgumentsE
	.section	.data.rel.ro.local._ZTVN2v88internal25PropertyCallbackArgumentsE,"awG",@progbits,_ZTVN2v88internal25PropertyCallbackArgumentsE,comdat
	.align 8
	.type	_ZTVN2v88internal25PropertyCallbackArgumentsE, @object
	.size	_ZTVN2v88internal25PropertyCallbackArgumentsE, 48
_ZTVN2v88internal25PropertyCallbackArgumentsE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal25PropertyCallbackArgumentsD1Ev
	.quad	_ZN2v88internal25PropertyCallbackArgumentsD0Ev
	.quad	_ZN2v88internal15CustomArgumentsINS_20PropertyCallbackInfoINS_5ValueEEEE15IterateInstanceEPNS0_11RootVisitorE
	.quad	_ZN2v88internal11Relocatable21PostGarbageCollectionEv
	.weak	_ZTVN2v88internal25FunctionCallbackArgumentsE
	.section	.data.rel.ro.local._ZTVN2v88internal25FunctionCallbackArgumentsE,"awG",@progbits,_ZTVN2v88internal25FunctionCallbackArgumentsE,comdat
	.align 8
	.type	_ZTVN2v88internal25FunctionCallbackArgumentsE, @object
	.size	_ZTVN2v88internal25FunctionCallbackArgumentsE, 48
_ZTVN2v88internal25FunctionCallbackArgumentsE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal25FunctionCallbackArgumentsD1Ev
	.quad	_ZN2v88internal25FunctionCallbackArgumentsD0Ev
	.quad	_ZN2v88internal15CustomArgumentsINS_20FunctionCallbackInfoINS_5ValueEEEE15IterateInstanceEPNS0_11RootVisitorE
	.quad	_ZN2v88internal11Relocatable21PostGarbageCollectionEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
