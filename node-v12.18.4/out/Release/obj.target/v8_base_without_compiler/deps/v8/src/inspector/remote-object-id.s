	.file	"remote-object-id.cc"
	.text
	.section	.text._ZN12v8_inspector18RemoteObjectIdBaseC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18RemoteObjectIdBaseC2Ev
	.type	_ZN12v8_inspector18RemoteObjectIdBaseC2Ev, @function
_ZN12v8_inspector18RemoteObjectIdBaseC2Ev:
.LFB4434:
	.cfi_startproc
	endbr64
	movl	$0, (%rdi)
	ret
	.cfi_endproc
.LFE4434:
	.size	_ZN12v8_inspector18RemoteObjectIdBaseC2Ev, .-_ZN12v8_inspector18RemoteObjectIdBaseC2Ev
	.globl	_ZN12v8_inspector18RemoteObjectIdBaseC1Ev
	.set	_ZN12v8_inspector18RemoteObjectIdBaseC1Ev,_ZN12v8_inspector18RemoteObjectIdBaseC2Ev
	.section	.rodata._ZN12v8_inspector18RemoteObjectIdBase21parseInjectedScriptIdERKNS_8String16E.str1.1,"aMS",@progbits,1
.LC0:
	.string	"injectedScriptId"
	.section	.text._ZN12v8_inspector18RemoteObjectIdBase21parseInjectedScriptIdERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18RemoteObjectIdBase21parseInjectedScriptIdERKNS_8String16E
	.type	_ZN12v8_inspector18RemoteObjectIdBase21parseInjectedScriptIdERKNS_8String16E, @function
_ZN12v8_inspector18RemoteObjectIdBase21parseInjectedScriptIdERKNS_8String16E:
.LFB4436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-88(%rbp), %rdi
	pushq	%r12
	subq	$64, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_8String16E@PLT
	movq	-88(%rbp), %r12
	testq	%r12, %r12
	je	.L4
	cmpl	$6, 8(%r12)
	je	.L5
.L4:
	movq	$0, 0(%r13)
.L6:
	testq	%r12, %r12
	je	.L3
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
.L3:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L20
	addq	$64, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	-80(%rbp), %r15
	leaq	.LC0(%rip), %rsi
	movq	$0, -88(%rbp)
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue10getIntegerERKNS_8String16EPi@PLT
	movq	-80(%rbp), %rdi
	movl	%eax, %r14d
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L7
	call	_ZdlPv@PLT
.L7:
	testb	%r14b, %r14b
	je	.L21
	movq	%r12, 0(%r13)
	movq	-88(%rbp), %r12
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L21:
	movq	$0, 0(%r13)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	-88(%rbp), %r12
	jmp	.L6
.L20:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4436:
	.size	_ZN12v8_inspector18RemoteObjectIdBase21parseInjectedScriptIdERKNS_8String16E, .-_ZN12v8_inspector18RemoteObjectIdBase21parseInjectedScriptIdERKNS_8String16E
	.section	.text._ZN12v8_inspector14RemoteObjectIdC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector14RemoteObjectIdC2Ev
	.type	_ZN12v8_inspector14RemoteObjectIdC2Ev, @function
_ZN12v8_inspector14RemoteObjectIdC2Ev:
.LFB4438:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	ret
	.cfi_endproc
.LFE4438:
	.size	_ZN12v8_inspector14RemoteObjectIdC2Ev, .-_ZN12v8_inspector14RemoteObjectIdC2Ev
	.globl	_ZN12v8_inspector14RemoteObjectIdC1Ev
	.set	_ZN12v8_inspector14RemoteObjectIdC1Ev,_ZN12v8_inspector14RemoteObjectIdC2Ev
	.section	.rodata._ZN12v8_inspector14RemoteObjectId5parseERKNS_8String16EPSt10unique_ptrIS0_St14default_deleteIS0_EE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Invalid remote object id"
.LC2:
	.string	"id"
	.section	.text._ZN12v8_inspector14RemoteObjectId5parseERKNS_8String16EPSt10unique_ptrIS0_St14default_deleteIS0_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector14RemoteObjectId5parseERKNS_8String16EPSt10unique_ptrIS0_St14default_deleteIS0_EE
	.type	_ZN12v8_inspector14RemoteObjectId5parseERKNS_8String16EPSt10unique_ptrIS0_St14default_deleteIS0_EE, @function
_ZN12v8_inspector14RemoteObjectId5parseERKNS_8String16EPSt10unique_ptrIS0_St14default_deleteIS0_EE:
.LFB4440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$8, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%r14, %rdx
	leaq	-104(%rbp), %rdi
	movq	$0, (%rax)
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN12v8_inspector18RemoteObjectIdBase21parseInjectedScriptIdERKNS_8String16E
	movq	-104(%rbp), %r14
	testq	%r14, %r14
	je	.L44
	leaq	-96(%rbp), %r15
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	leaq	4(%r12), %rdx
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue10getIntegerERKNS_8String16EPi@PLT
	movq	-96(%rbp), %rdi
	movl	%eax, %r14d
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L27
	movq	%rax, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rax
.L27:
	testb	%r14b, %r14b
	je	.L45
	movq	(%rbx), %rdi
	movq	%r12, (%rbx)
	testq	%rdi, %rdi
	je	.L30
	movl	$8, %esi
	call	_ZdlPvm@PLT
.L30:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*24(%rax)
.L23:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L46
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	-120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L26
.L43:
	call	_ZdlPv@PLT
.L26:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L32
	movq	(%rdi), %rax
	call	*24(%rax)
.L32:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	-96(%rbp), %r14
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L43
	jmp	.L26
.L46:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4440:
	.size	_ZN12v8_inspector14RemoteObjectId5parseERKNS_8String16EPSt10unique_ptrIS0_St14default_deleteIS0_EE, .-_ZN12v8_inspector14RemoteObjectId5parseERKNS_8String16EPSt10unique_ptrIS0_St14default_deleteIS0_EE
	.section	.text._ZN12v8_inspector17RemoteCallFrameIdC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector17RemoteCallFrameIdC2Ev
	.type	_ZN12v8_inspector17RemoteCallFrameIdC2Ev, @function
_ZN12v8_inspector17RemoteCallFrameIdC2Ev:
.LFB4451:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	ret
	.cfi_endproc
.LFE4451:
	.size	_ZN12v8_inspector17RemoteCallFrameIdC2Ev, .-_ZN12v8_inspector17RemoteCallFrameIdC2Ev
	.globl	_ZN12v8_inspector17RemoteCallFrameIdC1Ev
	.set	_ZN12v8_inspector17RemoteCallFrameIdC1Ev,_ZN12v8_inspector17RemoteCallFrameIdC2Ev
	.section	.rodata._ZN12v8_inspector17RemoteCallFrameId5parseERKNS_8String16EPSt10unique_ptrIS0_St14default_deleteIS0_EE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"Invalid call frame id"
.LC4:
	.string	"ordinal"
	.section	.text._ZN12v8_inspector17RemoteCallFrameId5parseERKNS_8String16EPSt10unique_ptrIS0_St14default_deleteIS0_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector17RemoteCallFrameId5parseERKNS_8String16EPSt10unique_ptrIS0_St14default_deleteIS0_EE
	.type	_ZN12v8_inspector17RemoteCallFrameId5parseERKNS_8String16EPSt10unique_ptrIS0_St14default_deleteIS0_EE, @function
_ZN12v8_inspector17RemoteCallFrameId5parseERKNS_8String16EPSt10unique_ptrIS0_St14default_deleteIS0_EE:
.LFB4453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$8, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%r14, %rdx
	leaq	-104(%rbp), %rdi
	movq	$0, (%rax)
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN12v8_inspector18RemoteObjectIdBase21parseInjectedScriptIdERKNS_8String16E
	movq	-104(%rbp), %r14
	testq	%r14, %r14
	je	.L69
	leaq	-96(%rbp), %r15
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	leaq	4(%r12), %rdx
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue10getIntegerERKNS_8String16EPi@PLT
	movq	-96(%rbp), %rdi
	movl	%eax, %r14d
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L52
	movq	%rax, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rax
.L52:
	testb	%r14b, %r14b
	je	.L70
	movq	(%rbx), %rdi
	movq	%r12, (%rbx)
	testq	%rdi, %rdi
	je	.L55
	movl	$8, %esi
	call	_ZdlPvm@PLT
.L55:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L48
	movq	(%rdi), %rax
	call	*24(%rax)
.L48:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L71
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	-120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L51
.L68:
	call	_ZdlPv@PLT
.L51:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L57
	movq	(%rdi), %rax
	call	*24(%rax)
.L57:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L69:
	leaq	-96(%rbp), %r14
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L68
	jmp	.L51
.L71:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4453:
	.size	_ZN12v8_inspector17RemoteCallFrameId5parseERKNS_8String16EPSt10unique_ptrIS0_St14default_deleteIS0_EE, .-_ZN12v8_inspector17RemoteCallFrameId5parseERKNS_8String16EPSt10unique_ptrIS0_St14default_deleteIS0_EE
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm.str1.1,"aMS",@progbits,1
.LC5:
	.string	"basic_string::_M_create"
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm:
.LFB6030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	subq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	addq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, %r13
	leaq	(%r9,%rax), %r15
	subq	%rsi, %r13
	cmpq	(%rdi), %r14
	je	.L85
	movq	16(%rdi), %rax
.L73:
	movabsq	$2305843009213693951, %rdx
	cmpq	%rdx, %r15
	ja	.L106
	cmpq	%rax, %r15
	jbe	.L75
	addq	%rax, %rax
	cmpq	%rax, %r15
	jnb	.L75
	movabsq	$4611686018427387904, %rdi
	movq	%rdx, %r15
	cmpq	%rdx, %rax
	ja	.L76
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	2(%r15,%r15), %rdi
.L76:
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	testq	%r12, %r12
	movq	(%rbx), %r11
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	movq	%rax, %r10
	je	.L78
	cmpq	$1, %r12
	je	.L107
	movq	%r12, %rdx
	addq	%rdx, %rdx
	je	.L78
	movq	%r11, %rsi
	movq	%rax, %rdi
	movq	%r8, -80(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r11
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L78:
	testq	%rcx, %rcx
	je	.L80
	testq	%r8, %r8
	je	.L80
	leaq	(%r10,%r12,2), %rdi
	cmpq	$1, %r8
	je	.L108
	movq	%r8, %rdx
	addq	%rdx, %rdx
	je	.L80
	movq	%rcx, %rsi
	movq	%r8, -80(%rbp)
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
.L80:
	testq	%r13, %r13
	jne	.L109
.L82:
	cmpq	%r11, %r14
	je	.L84
	movq	%r11, %rdi
	movq	%r10, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r10
.L84:
	movq	%r15, 16(%rbx)
	movq	%r10, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	addq	%r12, %r8
	leaq	(%r10,%r8,2), %rdi
	leaq	(%r11,%rax,2), %rsi
	cmpq	$1, %r13
	je	.L110
	addq	%r13, %r13
	je	.L82
	movq	%r13, %rdx
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L85:
	movl	$7, %eax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L110:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L107:
	movzwl	(%r11), %eax
	movw	%ax, (%r10)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L108:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	testq	%r13, %r13
	je	.L82
	jmp	.L109
.L106:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6030:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.section	.rodata._ZN12v8_inspector17RemoteCallFrameId9serializeEii.str1.1,"aMS",@progbits,1
.LC6:
	.string	"}"
.LC7:
	.string	",\"injectedScriptId\":"
.LC8:
	.string	"{\"ordinal\":"
	.section	.rodata._ZN12v8_inspector17RemoteCallFrameId9serializeEii.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZN12v8_inspector17RemoteCallFrameId9serializeEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector17RemoteCallFrameId9serializeEii
	.type	_ZN12v8_inspector17RemoteCallFrameId9serializeEii, @function
_ZN12v8_inspector17RemoteCallFrameId9serializeEii:
.LFB4457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	leaq	.LC6(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-144(%rbp), %rdi
	pushq	%rbx
	subq	$520, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	%r14d, %esi
	leaq	-240(%rbp), %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	leaq	-336(%rbp), %rdi
	leaq	.LC7(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	%r13d, %esi
	leaq	-432(%rbp), %rdi
	leaq	-448(%rbp), %r13
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	leaq	-96(%rbp), %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %r14
	movq	%r13, -464(%rbp)
	leaq	(%rax,%rax), %rbx
	movq	%r14, %rax
	addq	%rbx, %rax
	je	.L112
	testq	%r14, %r14
	je	.L123
.L112:
	movq	%rbx, %r15
	movq	%r13, %rdi
	sarq	%r15
	cmpq	$14, %rbx
	ja	.L223
.L113:
	cmpq	$2, %rbx
	je	.L224
	testq	%rbx, %rbx
	je	.L116
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	memmove@PLT
	movq	-464(%rbp), %rdi
.L116:
	xorl	%r10d, %r10d
	movq	%r15, -456(%rbp)
	movl	$7, %eax
	leaq	-464(%rbp), %r14
	movw	%r10w, (%rdi,%rbx)
	movq	-464(%rbp), %rdx
	movq	-424(%rbp), %r8
	movq	-456(%rbp), %rsi
	cmpq	%r13, %rdx
	cmovne	-448(%rbp), %rax
	movq	-432(%rbp), %rcx
	leaq	(%r8,%rsi), %rbx
	cmpq	%rax, %rbx
	ja	.L118
	testq	%r8, %r8
	jne	.L225
.L119:
	xorl	%r9d, %r9d
	movq	%rbx, -456(%rbp)
	leaq	-384(%rbp), %rdi
	movq	%r14, %rsi
	movw	%r9w, (%rdx,%rbx,2)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-464(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L121
	call	_ZdlPv@PLT
.L121:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L122
	call	_ZdlPv@PLT
.L122:
	movq	-376(%rbp), %rax
	movq	-384(%rbp), %r14
	leaq	-480(%rbp), %r13
	movq	%r13, -496(%rbp)
	leaq	(%rax,%rax), %rbx
	movq	%r14, %rax
	addq	%rbx, %rax
	je	.L168
	testq	%r14, %r14
	je	.L123
.L168:
	movq	%rbx, %r15
	movq	%r13, %rdi
	sarq	%r15
	cmpq	$14, %rbx
	ja	.L226
.L125:
	cmpq	$2, %rbx
	je	.L227
	testq	%rbx, %rbx
	je	.L128
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	memmove@PLT
	movq	-496(%rbp), %rdi
.L128:
	xorl	%r8d, %r8d
	movq	%r15, -488(%rbp)
	movl	$7, %eax
	leaq	-496(%rbp), %r14
	movw	%r8w, (%rdi,%rbx)
	movq	-496(%rbp), %rdx
	movq	-328(%rbp), %r8
	movq	-488(%rbp), %rsi
	cmpq	%r13, %rdx
	cmovne	-480(%rbp), %rax
	movq	-336(%rbp), %rcx
	leaq	(%r8,%rsi), %rbx
	cmpq	%rax, %rbx
	ja	.L130
	testq	%r8, %r8
	jne	.L228
.L131:
	xorl	%edi, %edi
	movq	%rbx, -488(%rbp)
	movq	%r14, %rsi
	movw	%di, (%rdx,%rbx,2)
	leaq	-288(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-496(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L133
	call	_ZdlPv@PLT
.L133:
	movq	-280(%rbp), %rax
	movq	-288(%rbp), %r14
	leaq	-512(%rbp), %r13
	movq	%r13, -528(%rbp)
	leaq	(%rax,%rax), %rbx
	movq	%r14, %rax
	addq	%rbx, %rax
	je	.L169
	testq	%r14, %r14
	je	.L123
.L169:
	movq	%rbx, %r15
	movq	%r13, %rdi
	sarq	%r15
	cmpq	$14, %rbx
	ja	.L229
.L135:
	cmpq	$2, %rbx
	je	.L230
	testq	%rbx, %rbx
	je	.L137
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	memmove@PLT
	movq	-528(%rbp), %rdi
.L137:
	xorl	%esi, %esi
	movq	%r15, -520(%rbp)
	movl	$7, %eax
	leaq	-528(%rbp), %r14
	movw	%si, (%rdi,%rbx)
	movq	-528(%rbp), %rdx
	movq	-232(%rbp), %r8
	movq	-520(%rbp), %rsi
	cmpq	%r13, %rdx
	cmovne	-512(%rbp), %rax
	movq	-240(%rbp), %rcx
	leaq	(%r8,%rsi), %rbx
	cmpq	%rax, %rbx
	ja	.L139
	testq	%r8, %r8
	jne	.L231
.L140:
	xorl	%ecx, %ecx
	movq	%rbx, -520(%rbp)
	leaq	-192(%rbp), %rdi
	movq	%r14, %rsi
	movw	%cx, (%rdx,%rbx,2)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-528(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L142
	call	_ZdlPv@PLT
.L142:
	movq	-184(%rbp), %rax
	movq	-192(%rbp), %r14
	leaq	-544(%rbp), %r13
	movq	%r13, -560(%rbp)
	leaq	(%rax,%rax), %rbx
	movq	%r14, %rax
	addq	%rbx, %rax
	je	.L170
	testq	%r14, %r14
	je	.L123
.L170:
	movq	%rbx, %r15
	movq	%r13, %rdi
	sarq	%r15
	cmpq	$14, %rbx
	ja	.L232
.L144:
	cmpq	$2, %rbx
	je	.L233
	testq	%rbx, %rbx
	je	.L146
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	memmove@PLT
	movq	-560(%rbp), %rdi
.L146:
	xorl	%edx, %edx
	movq	%r15, -552(%rbp)
	movl	$7, %eax
	leaq	-560(%rbp), %r14
	movw	%dx, (%rdi,%rbx)
	movq	-560(%rbp), %rdx
	movq	-136(%rbp), %r8
	movq	-552(%rbp), %rsi
	cmpq	%r13, %rdx
	cmovne	-544(%rbp), %rax
	movq	-144(%rbp), %rcx
	leaq	(%r8,%rsi), %rbx
	cmpq	%rax, %rbx
	ja	.L148
	testq	%r8, %r8
	jne	.L234
.L149:
	xorl	%eax, %eax
	movq	%rbx, -552(%rbp)
	movq	%r12, %rdi
	movq	%r14, %rsi
	movw	%ax, (%rdx,%rbx,2)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-560(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L151
	call	_ZdlPv@PLT
.L151:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L152
	call	_ZdlPv@PLT
.L152:
	movq	-288(%rbp), %rdi
	leaq	-272(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L153
	call	_ZdlPv@PLT
.L153:
	movq	-384(%rbp), %rdi
	leaq	-368(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L154
	call	_ZdlPv@PLT
.L154:
	movq	-432(%rbp), %rdi
	leaq	-416(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L155
	call	_ZdlPv@PLT
.L155:
	movq	-336(%rbp), %rdi
	leaq	-320(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L156
	call	_ZdlPv@PLT
.L156:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L157
	call	_ZdlPv@PLT
.L157:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L111
	call	_ZdlPv@PLT
.L111:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L235
	addq	$520, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	leaq	(%rdx,%rsi,2), %rdi
	cmpq	$1, %r8
	je	.L236
	addq	%r8, %r8
	je	.L119
	movq	%r8, %rdx
	movq	%rcx, %rsi
	call	memmove@PLT
	movq	-464(%rbp), %rdx
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L228:
	leaq	(%rdx,%rsi,2), %rdi
	cmpq	$1, %r8
	je	.L237
	addq	%r8, %r8
	je	.L131
	movq	%r8, %rdx
	movq	%rcx, %rsi
	call	memmove@PLT
	movq	-496(%rbp), %rdx
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L231:
	leaq	(%rdx,%rsi,2), %rdi
	cmpq	$1, %r8
	je	.L238
	addq	%r8, %r8
	je	.L140
	movq	%r8, %rdx
	movq	%rcx, %rsi
	call	memmove@PLT
	movq	-528(%rbp), %rdx
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L234:
	leaq	(%rdx,%rsi,2), %rdi
	cmpq	$1, %r8
	je	.L239
	addq	%r8, %r8
	je	.L149
	movq	%r8, %rdx
	movq	%rcx, %rsi
	call	memmove@PLT
	movq	-560(%rbp), %rdx
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L224:
	movzwl	(%r14), %eax
	movw	%ax, (%rdi)
	movq	-464(%rbp), %rdi
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L227:
	movzwl	(%r14), %eax
	movw	%ax, (%rdi)
	movq	-496(%rbp), %rdi
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L230:
	movzwl	(%r14), %eax
	movw	%ax, (%rdi)
	movq	-528(%rbp), %rdi
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L233:
	movzwl	(%r14), %eax
	movw	%ax, (%rdi)
	movq	-560(%rbp), %rdi
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L223:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r15
	ja	.L126
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r15, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	%rax, %rdi
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L118:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-464(%rbp), %rdx
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L226:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r15
	ja	.L126
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r15, -480(%rbp)
	movq	%rax, -496(%rbp)
	movq	%rax, %rdi
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L229:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r15
	ja	.L126
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r15, -512(%rbp)
	movq	%rax, -528(%rbp)
	movq	%rax, %rdi
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L130:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-496(%rbp), %rdx
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L232:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r15
	ja	.L126
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r15, -544(%rbp)
	movq	%rax, -560(%rbp)
	movq	%rax, %rdi
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L139:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-528(%rbp), %rdx
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L148:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-560(%rbp), %rdx
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L236:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	-464(%rbp), %rdx
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L238:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	-528(%rbp), %rdx
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L237:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	-496(%rbp), %rdx
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L239:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	-560(%rbp), %rdx
	jmp	.L149
.L123:
	leaq	.LC9(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L126:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L235:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4457:
	.size	_ZN12v8_inspector17RemoteCallFrameId9serializeEii, .-_ZN12v8_inspector17RemoteCallFrameId9serializeEii
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
