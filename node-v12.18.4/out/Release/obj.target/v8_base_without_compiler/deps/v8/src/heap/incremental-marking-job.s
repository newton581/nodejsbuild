	.file	"incremental-marking-job.cc"
	.text
	.section	.text._ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB2493:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2493:
	.size	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE,"axG",@progbits,_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.type	_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE, @function
_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE:
.LFB2769:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2769:
	.size	_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE, .-_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.section	.text._ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd,"axG",@progbits,_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd
	.type	_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd, @function
_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd:
.LFB2770:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2770:
	.size	_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd, .-_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd
	.section	.text._ZNK2v810TaskRunner23NonNestableTasksEnabledEv,"axG",@progbits,_ZNK2v810TaskRunner23NonNestableTasksEnabledEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v810TaskRunner23NonNestableTasksEnabledEv
	.type	_ZNK2v810TaskRunner23NonNestableTasksEnabledEv, @function
_ZNK2v810TaskRunner23NonNestableTasksEnabledEv:
.LFB2771:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2771:
	.size	_ZNK2v810TaskRunner23NonNestableTasksEnabledEv, .-_ZNK2v810TaskRunner23NonNestableTasksEnabledEv
	.section	.text._ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv,"axG",@progbits,_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv
	.type	_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv, @function
_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv:
.LFB2772:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2772:
	.size	_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv, .-_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB2773:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE2773:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CancelableTask3RunEv
	.type	_ZN2v88internal14CancelableTask3RunEv, @function
_ZN2v88internal14CancelableTask3RunEv:
.LFB3729:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, 16(%rdi)
	jne	.L8
	movq	(%rdi), %rax
	jmp	*24(%rax)
.L8:
	ret
	.cfi_endproc
.LFE3729:
	.size	_ZN2v88internal14CancelableTask3RunEv, .-_ZN2v88internal14CancelableTask3RunEv
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB23221:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE23221:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN2v88internal21IncrementalMarkingJob4TaskD2Ev,"axG",@progbits,_ZN2v88internal21IncrementalMarkingJob4TaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21IncrementalMarkingJob4TaskD2Ev
	.type	_ZN2v88internal21IncrementalMarkingJob4TaskD2Ev, @function
_ZN2v88internal21IncrementalMarkingJob4TaskD2Ev:
.LFB23135:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE23135:
	.size	_ZN2v88internal21IncrementalMarkingJob4TaskD2Ev, .-_ZN2v88internal21IncrementalMarkingJob4TaskD2Ev
	.weak	_ZN2v88internal21IncrementalMarkingJob4TaskD1Ev
	.set	_ZN2v88internal21IncrementalMarkingJob4TaskD1Ev,_ZN2v88internal21IncrementalMarkingJob4TaskD2Ev
	.section	.text._ZN2v88internal21IncrementalMarkingJob4TaskD0Ev,"axG",@progbits,_ZN2v88internal21IncrementalMarkingJob4TaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21IncrementalMarkingJob4TaskD0Ev
	.type	_ZN2v88internal21IncrementalMarkingJob4TaskD0Ev, @function
_ZN2v88internal21IncrementalMarkingJob4TaskD0Ev:
.LFB23137:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23137:
	.size	_ZN2v88internal21IncrementalMarkingJob4TaskD0Ev, .-_ZN2v88internal21IncrementalMarkingJob4TaskD0Ev
	.section	.text._ZN2v88internal21IncrementalMarkingJob4TaskD2Ev,"axG",@progbits,_ZN2v88internal21IncrementalMarkingJob4TaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal21IncrementalMarkingJob4TaskD1Ev
	.type	_ZThn32_N2v88internal21IncrementalMarkingJob4TaskD1Ev, @function
_ZThn32_N2v88internal21IncrementalMarkingJob4TaskD1Ev:
.LFB24933:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	subq	$32, %rdi
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE24933:
	.size	_ZThn32_N2v88internal21IncrementalMarkingJob4TaskD1Ev, .-_ZThn32_N2v88internal21IncrementalMarkingJob4TaskD1Ev
	.section	.text._ZN2v88internal21IncrementalMarkingJob4TaskD0Ev,"axG",@progbits,_ZN2v88internal21IncrementalMarkingJob4TaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal21IncrementalMarkingJob4TaskD0Ev
	.type	_ZThn32_N2v88internal21IncrementalMarkingJob4TaskD0Ev, @function
_ZThn32_N2v88internal21IncrementalMarkingJob4TaskD0Ev:
.LFB24934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24934:
	.size	_ZThn32_N2v88internal21IncrementalMarkingJob4TaskD0Ev, .-_ZThn32_N2v88internal21IncrementalMarkingJob4TaskD0Ev
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal14CancelableTask3RunEv
	.type	_ZThn32_N2v88internal14CancelableTask3RunEv, @function
_ZThn32_N2v88internal14CancelableTask3RunEv:
.LFB24935:
	.cfi_startproc
	endbr64
	leaq	-32(%rdi), %r8
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, -16(%rdi)
	jne	.L17
	movq	-32(%rdi), %rax
	movq	%r8, %rdi
	movq	24(%rax), %rax
	jmp	*%rax
.L17:
	ret
	.cfi_endproc
.LFE24935:
	.size	_ZThn32_N2v88internal14CancelableTask3RunEv, .-_ZThn32_N2v88internal14CancelableTask3RunEv
	.section	.text._ZN2v88internal21IncrementalMarkingJob12ScheduleTaskEPNS0_4HeapENS1_8TaskTypeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal21IncrementalMarkingJob12ScheduleTaskEPNS0_4HeapENS1_8TaskTypeE.part.0, @function
_ZN2v88internal21IncrementalMarkingJob12ScheduleTaskEPNS0_4HeapENS1_8TaskTypeE.part.0:
.LFB24932:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-37592(%rsi), %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L20
	movb	$1, (%rdi)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rbx, %rdx
	leaq	-80(%rbp), %rdi
	movq	%rax, %rsi
	movq	(%rax), %rax
	call	*48(%rax)
	movq	-80(%rbp), %r14
	leaq	_ZNK2v810TaskRunner23NonNestableTasksEnabledEv(%rip), %rcx
	movq	(%r14), %rax
	movq	48(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L63
.L21:
	movl	$64, %edi
	movq	(%rax), %r15
	call	_Znwm@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	addq	$32, %r13
	movq	%rbx, 8(%r13)
	movq	%r14, %rdi
	leaq	16+_ZTVN2v88internal21IncrementalMarkingJob4TaskE(%rip), %rax
	movq	%r12, 16(%r13)
	leaq	-88(%rbp), %rsi
	movq	%rax, -32(%r13)
	addq	$48, %rax
	movq	%rax, 0(%r13)
	movq	$0, 24(%r13)
	movq	%r13, -88(%rbp)
	call	*%r15
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L28
.L61:
	movq	(%rdi), %rax
	call	*8(%rax)
.L28:
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	je	.L19
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L36
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L64
	.p2align 4,,10
	.p2align 3
.L19:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movb	$1, 1(%rdi)
	movl	%edx, %r13d
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rbx, %rdx
	leaq	-80(%rbp), %rdi
	movq	%rax, %rsi
	movq	(%rax), %rax
	call	*48(%rax)
	movq	-80(%rbp), %r15
	leaq	_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv(%rip), %rcx
	movq	(%r15), %rax
	movq	56(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L66
.L23:
	movq	16(%rax), %rdx
	movl	$64, %edi
	movq	%rdx, -104(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	addq	$32, %r14
	movq	%rbx, 8(%r14)
	movq	%r15, %rdi
	leaq	16+_ZTVN2v88internal21IncrementalMarkingJob4TaskE(%rip), %rax
	movq	%r12, 16(%r14)
	movq	-104(%rbp), %rdx
	leaq	-88(%rbp), %rsi
	movq	%rax, -32(%r14)
	addq	$48, %rax
	movsd	.LC0(%rip), %xmm0
	movq	%rax, (%r14)
	movl	$0, 24(%r14)
	movl	%r13d, 28(%r14)
	movq	%r14, -88(%rbp)
	call	*%rdx
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L61
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L36:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L19
.L64:
	movq	(%r12), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L67
.L39:
	testq	%rbx, %rbx
	je	.L40
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L41:
	cmpl	$1, %eax
	jne	.L19
	movq	(%r12), %rax
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L42
	call	*8(%rax)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%r14, %rdi
	call	*%rdx
	testb	%al, %al
	je	.L68
	movq	-80(%rbp), %r15
	movl	$64, %edi
	movq	(%r15), %rax
	movq	8(%rax), %r14
	call	_Znwm@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	leaq	16+_ZTVN2v88internal21IncrementalMarkingJob4TaskE(%rip), %rax
	leaq	32(%r13), %rdi
	movq	%rbx, 40(%r13)
	movq	%rax, 0(%r13)
	addq	$48, %rax
	leaq	_ZThn32_N2v88internal21IncrementalMarkingJob4TaskD0Ev(%rip), %rdx
	movq	%rax, 32(%r13)
	leaq	_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE(%rip), %rax
	movq	%r12, 48(%r13)
	movq	$2, 56(%r13)
	movq	%rdi, -88(%rbp)
	cmpq	%rax, %r14
	jne	.L69
.L31:
	call	*%rdx
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%r15, %rdi
	call	*%rdx
	testb	%al, %al
	je	.L70
	movq	-80(%rbp), %r8
	movl	$64, %edi
	movq	(%r8), %rax
	movq	%r8, -104(%rbp)
	movq	24(%rax), %r15
	call	_Znwm@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	leaq	16+_ZTVN2v88internal21IncrementalMarkingJob4TaskE(%rip), %rax
	leaq	32(%r14), %rdi
	movq	%rbx, 40(%r14)
	movq	%rax, (%r14)
	addq	$48, %rax
	movq	-104(%rbp), %r8
	leaq	_ZThn32_N2v88internal21IncrementalMarkingJob4TaskD0Ev(%rip), %rdx
	movq	%rax, 32(%r14)
	leaq	_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd(%rip), %rax
	cmpq	%rax, %r15
	movq	%r12, 48(%r14)
	movl	$2, 56(%r14)
	movl	%r13d, 60(%r14)
	movq	%rdi, -88(%rbp)
	je	.L31
	movq	%r8, %rdi
	movsd	.LC0(%rip), %xmm0
	leaq	-88(%rbp), %rsi
	call	*%r15
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L28
.L62:
	movq	(%rdi), %rax
	movq	8(%rax), %rdx
	call	*%rdx
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%r15, %rdi
	leaq	-88(%rbp), %rsi
	call	*%r14
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L28
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L40:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L42:
	call	*%rdx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L70:
	movq	-80(%rbp), %r15
	movq	(%r15), %rax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L68:
	movq	-80(%rbp), %r14
	movq	(%r14), %rax
	jmp	.L21
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24932:
	.size	_ZN2v88internal21IncrementalMarkingJob12ScheduleTaskEPNS0_4HeapENS1_8TaskTypeE.part.0, .-_ZN2v88internal21IncrementalMarkingJob12ScheduleTaskEPNS0_4HeapENS1_8TaskTypeE.part.0
	.section	.rodata._ZN2v88internal21IncrementalMarkingJob4Task11RunInternalEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"v8"
.LC2:
	.string	"V8.Task"
	.section	.text._ZN2v88internal21IncrementalMarkingJob4Task11RunInternalEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21IncrementalMarkingJob4Task11RunInternalEv
	.type	_ZN2v88internal21IncrementalMarkingJob4Task11RunInternalEv, @function
_ZN2v88internal21IncrementalMarkingJob4Task11RunInternalEv:
.LFB19792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	40(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	12616(%r13), %r12d
	movl	$1, 12616(%r13)
	movq	_ZZN2v88internal21IncrementalMarkingJob4Task11RunInternalEvE28trace_event_unique_atomic102(%rip), %rdx
	testq	%rdx, %rdx
	je	.L96
	movq	$0, -88(%rbp)
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L97
.L75:
	movq	40(%rbx), %r14
	movq	39656(%r14), %r8
	leaq	37592(%r14), %r15
	movl	80(%r8), %edi
	testl	%edi, %edi
	je	.L98
.L77:
	movl	60(%rbx), %esi
	movq	48(%rbx), %rax
	testl	%esi, %esi
	jne	.L79
	movb	$0, (%rax)
	movl	80(%r8), %ecx
	testl	%ecx, %ecx
	jne	.L99
.L82:
	cmpq	$0, -88(%rbp)
	jne	.L100
.L86:
	movl	%r12d, 12616(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movb	$0, 1(%rax)
	movl	80(%r8), %ecx
	testl	%ecx, %ecx
	je	.L82
.L99:
	movl	56(%rbx), %ecx
	movq	%r15, %rdi
	movq	%r8, -120(%rbp)
	movl	%ecx, -104(%rbp)
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movl	$1, %edx
	movl	$1, %esi
	addsd	.LC3(%rip), %xmm0
	movq	39656(%r14), %rdi
	call	_ZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginE@PLT
	movl	-104(%rbp), %ecx
	movl	$9, %esi
	movq	%r15, %rdi
	movl	%eax, -112(%rbp)
	movq	39720(%r14), %rax
	movl	24(%rax), %edx
	movl	%ecx, 24(%rax)
	movq	%rax, -104(%rbp)
	movl	%edx, -108(%rbp)
	call	_ZN2v88internal4Heap36FinalizeIncrementalMarkingIfCompleteENS0_23GarbageCollectionReasonE@PLT
	movl	-108(%rbp), %edx
	movq	-104(%rbp), %rax
	movq	-120(%rbp), %r8
	movl	%edx, 24(%rax)
	movl	80(%r8), %edx
	testl	%edx, %edx
	je	.L82
	movl	-112(%rbp), %eax
	movq	48(%rbx), %rdi
	testl	%eax, %eax
	jne	.L84
	movzbl	1(%rdi), %eax
	movl	$1, %edx
.L85:
	testb	%al, %al
	jne	.L82
	cmpl	$4, 37984(%r14)
	je	.L82
	movq	%r15, %rsi
	call	_ZN2v88internal21IncrementalMarkingJob12ScheduleTaskEPNS0_4HeapENS1_8TaskTypeE.part.0
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L96:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L102
.L74:
	movq	%rdx, _ZZN2v88internal21IncrementalMarkingJob4Task11RunInternalEvE28trace_event_unique_atomic102(%rip)
	movq	$0, -88(%rbp)
	movzbl	(%rdx), %eax
	testb	$5, %al
	je	.L75
.L97:
	movq	40(%rbx), %rsi
	leaq	-96(%rbp), %rdi
	leaq	.LC2(%rip), %rcx
	call	_ZN2v88internal7tracing21CallStatsScopedTracer10InitializeEPNS0_7IsolateEPKhPKc@PLT
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%r15, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal4Heap30IncrementalMarkingLimitReachedEv@PLT
	movq	-104(%rbp), %r8
	testl	%eax, %eax
	je	.L77
	movq	%r15, %rdi
	call	_ZN2v88internal4Heap28ShouldOptimizeForMemoryUsageEv@PLT
	movl	$64, %ecx
	movl	$12, %edx
	movq	%r15, %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal4Heap23StartIncrementalMarkingEiNS0_23GarbageCollectionReasonENS_15GCCallbackFlagsE@PLT
	movq	-104(%rbp), %r8
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L100:
	movq	-80(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L86
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing21CallStatsScopedTracer16AddEndTraceEventEv@PLT
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L84:
	movzbl	(%rdi), %eax
	xorl	%edx, %edx
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L102:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L74
.L101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19792:
	.size	_ZN2v88internal21IncrementalMarkingJob4Task11RunInternalEv, .-_ZN2v88internal21IncrementalMarkingJob4Task11RunInternalEv
	.section	.text._ZN2v88internal21IncrementalMarkingJob5StartEPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21IncrementalMarkingJob5StartEPNS0_4HeapE
	.type	_ZN2v88internal21IncrementalMarkingJob5StartEPNS0_4HeapE, @function
_ZN2v88internal21IncrementalMarkingJob5StartEPNS0_4HeapE:
.LFB19781:
	.cfi_startproc
	endbr64
	cmpb	$0, (%rdi)
	jne	.L103
	cmpl	$4, 392(%rsi)
	je	.L103
	xorl	%edx, %edx
	jmp	_ZN2v88internal21IncrementalMarkingJob12ScheduleTaskEPNS0_4HeapENS1_8TaskTypeE.part.0
	.p2align 4,,10
	.p2align 3
.L103:
	ret
	.cfi_endproc
.LFE19781:
	.size	_ZN2v88internal21IncrementalMarkingJob5StartEPNS0_4HeapE, .-_ZN2v88internal21IncrementalMarkingJob5StartEPNS0_4HeapE
	.section	.text._ZN2v88internal21IncrementalMarkingJob12ScheduleTaskEPNS0_4HeapENS1_8TaskTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21IncrementalMarkingJob12ScheduleTaskEPNS0_4HeapENS1_8TaskTypeE
	.type	_ZN2v88internal21IncrementalMarkingJob12ScheduleTaskEPNS0_4HeapENS1_8TaskTypeE, @function
_ZN2v88internal21IncrementalMarkingJob12ScheduleTaskEPNS0_4HeapENS1_8TaskTypeE:
.LFB19782:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %eax
	testl	%edx, %edx
	je	.L106
	movzbl	1(%rdi), %eax
.L106:
	testb	%al, %al
	jne	.L105
	cmpl	$4, 392(%rsi)
	je	.L105
	jmp	_ZN2v88internal21IncrementalMarkingJob12ScheduleTaskEPNS0_4HeapENS1_8TaskTypeE.part.0
	.p2align 4,,10
	.p2align 3
.L105:
	ret
	.cfi_endproc
.LFE19782:
	.size	_ZN2v88internal21IncrementalMarkingJob12ScheduleTaskEPNS0_4HeapENS1_8TaskTypeE, .-_ZN2v88internal21IncrementalMarkingJob12ScheduleTaskEPNS0_4HeapENS1_8TaskTypeE
	.section	.text._ZN2v88internal21IncrementalMarkingJob4Task4StepEPNS0_4HeapENS_18EmbedderHeapTracer18EmbedderStackStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21IncrementalMarkingJob4Task4StepEPNS0_4HeapENS_18EmbedderHeapTracer18EmbedderStackStateE
	.type	_ZN2v88internal21IncrementalMarkingJob4Task4StepEPNS0_4HeapENS_18EmbedderHeapTracer18EmbedderStackStateE, @function
_ZN2v88internal21IncrementalMarkingJob4Task4StepEPNS0_4HeapENS_18EmbedderHeapTracer18EmbedderStackStateE:
.LFB19791:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movl	$1, %edx
	movl	$1, %esi
	addsd	.LC3(%rip), %xmm0
	movq	2064(%r12), %rdi
	call	_ZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginE@PLT
	movq	2128(%r12), %rbx
	movq	%r12, %rdi
	movl	$9, %esi
	movl	%eax, %r13d
	movl	24(%rbx), %r15d
	movl	%r14d, 24(%rbx)
	call	_ZN2v88internal4Heap36FinalizeIncrementalMarkingIfCompleteENS0_23GarbageCollectionReasonE@PLT
	movl	%r15d, 24(%rbx)
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19791:
	.size	_ZN2v88internal21IncrementalMarkingJob4Task4StepEPNS0_4HeapENS_18EmbedderHeapTracer18EmbedderStackStateE, .-_ZN2v88internal21IncrementalMarkingJob4Task4StepEPNS0_4HeapENS_18EmbedderHeapTracer18EmbedderStackStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal21IncrementalMarkingJob5StartEPNS0_4HeapE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal21IncrementalMarkingJob5StartEPNS0_4HeapE, @function
_GLOBAL__sub_I__ZN2v88internal21IncrementalMarkingJob5StartEPNS0_4HeapE:
.LFB24921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24921:
	.size	_GLOBAL__sub_I__ZN2v88internal21IncrementalMarkingJob5StartEPNS0_4HeapE, .-_GLOBAL__sub_I__ZN2v88internal21IncrementalMarkingJob5StartEPNS0_4HeapE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal21IncrementalMarkingJob5StartEPNS0_4HeapE
	.weak	_ZTVN2v88internal14CancelableTaskE
	.section	.data.rel.ro._ZTVN2v88internal14CancelableTaskE,"awG",@progbits,_ZTVN2v88internal14CancelableTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal14CancelableTaskE, @object
	.size	_ZTVN2v88internal14CancelableTaskE, 88
_ZTVN2v88internal14CancelableTaskE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	__cxa_pure_virtual
	.quad	-32
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.weak	_ZTVN2v88internal21IncrementalMarkingJob4TaskE
	.section	.data.rel.ro.local._ZTVN2v88internal21IncrementalMarkingJob4TaskE,"awG",@progbits,_ZTVN2v88internal21IncrementalMarkingJob4TaskE,comdat
	.align 8
	.type	_ZTVN2v88internal21IncrementalMarkingJob4TaskE, @object
	.size	_ZTVN2v88internal21IncrementalMarkingJob4TaskE, 88
_ZTVN2v88internal21IncrementalMarkingJob4TaskE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal21IncrementalMarkingJob4TaskD1Ev
	.quad	_ZN2v88internal21IncrementalMarkingJob4TaskD0Ev
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	_ZN2v88internal21IncrementalMarkingJob4Task11RunInternalEv
	.quad	-32
	.quad	0
	.quad	_ZThn32_N2v88internal21IncrementalMarkingJob4TaskD1Ev
	.quad	_ZThn32_N2v88internal21IncrementalMarkingJob4TaskD0Ev
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.section	.bss._ZZN2v88internal21IncrementalMarkingJob4Task11RunInternalEvE28trace_event_unique_atomic102,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal21IncrementalMarkingJob4Task11RunInternalEvE28trace_event_unique_atomic102, @object
	.size	_ZZN2v88internal21IncrementalMarkingJob4Task11RunInternalEvE28trace_event_unique_atomic102, 8
_ZZN2v88internal21IncrementalMarkingJob4Task11RunInternalEvE28trace_event_unique_atomic102:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	1202590843
	.long	1065646817
	.align 8
.LC3:
	.long	0
	.long	1072693248
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
