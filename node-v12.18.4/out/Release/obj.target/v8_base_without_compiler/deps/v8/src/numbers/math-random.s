	.file	"math-random.cc"
	.text
	.section	.text._ZN2v88internal10MathRandom12ResetContextENS0_7ContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10MathRandom12ResetContextENS0_7ContextE
	.type	_ZN2v88internal10MathRandom12ResetContextENS0_7ContextE, @function
_ZN2v88internal10MathRandom12ResetContextENS0_7ContextE:
.LFB17783:
	.cfi_startproc
	endbr64
	movq	$0, 831(%rdi)
	pxor	%xmm0, %xmm0
	movq	839(%rdi), %rax
	movups	%xmm0, 15(%rax)
	ret
	.cfi_endproc
.LFE17783:
	.size	_ZN2v88internal10MathRandom12ResetContextENS0_7ContextE, .-_ZN2v88internal10MathRandom12ResetContextENS0_7ContextE
	.section	.text._ZN2v88internal10MathRandom17InitializeContextEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10MathRandom17InitializeContextEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE
	.type	_ZN2v88internal10MathRandom17InitializeContextEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE, @function
_ZN2v88internal10MathRandom17InitializeContextEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE:
.LFB17782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$64, %esi
	subq	$24, %rsp
	call	_ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE@PLT
	movl	$16, %edx
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rax), %rcx
	addq	$8, %rdx
	movq	$0, -9(%rdx,%rcx)
	cmpq	$528, %rdx
	jne	.L4
	movq	(%rbx), %r14
	movq	(%rax), %r13
	movq	%r13, 847(%r14)
	leaq	847(%r14), %rsi
	testb	$1, %r13b
	je	.L12
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L27
	testb	$24, %al
	je	.L12
.L32:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L28
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%r12, %rdi
	movl	$1, %edx
	movl	$16, %esi
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rbx), %r13
	movq	(%rax), %r12
	leaq	839(%r13), %r15
	movq	%r12, 839(%r13)
	testb	$1, %r12b
	je	.L11
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L29
	testb	$24, %al
	je	.L11
.L31:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L30
.L11:
	movq	(%rbx), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10MathRandom12ResetContextENS0_7ContextE
.L29:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	testb	$24, %al
	jne	.L31
	jmp	.L11
.L27:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L32
	jmp	.L12
.L28:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L12
.L30:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L11
	.cfi_endproc
.LFE17782:
	.size	_ZN2v88internal10MathRandom17InitializeContextEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE, .-_ZN2v88internal10MathRandom17InitializeContextEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE
	.section	.rodata._ZN2v88internal10MathRandom11RefillCacheEPNS0_7IsolateEm.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"state.s0 != 0 || state.s1 != 0"
	.section	.rodata._ZN2v88internal10MathRandom11RefillCacheEPNS0_7IsolateEm.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal10MathRandom11RefillCacheEPNS0_7IsolateEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10MathRandom11RefillCacheEPNS0_7IsolateEm
	.type	_ZN2v88internal10MathRandom11RefillCacheEPNS0_7IsolateEm, @function
_ZN2v88internal10MathRandom11RefillCacheEPNS0_7IsolateEm:
.LFB17784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	839(%rsi), %r13
	movq	23(%r13), %rcx
	movq	15(%r13), %rbx
	movq	%rcx, %rax
	orq	%rbx, %rax
	je	.L44
.L34:
	movq	847(%r12), %rdi
	movsd	.LC2(%rip), %xmm1
	movabsq	$4607182418800017408, %r9
	movabsq	$9221120237041090560, %r8
	leaq	15(%rdi), %rsi
	addq	$527, %rdi
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%rbx, %rax
	salq	$23, %rax
	xorq	%rax, %rbx
	movq	%rcx, %rax
	shrq	$26, %rax
	movq	%rbx, %rdx
	xorq	%rbx, %rax
	shrq	$17, %rdx
	movq	%rcx, %rbx
	xorq	%rcx, %rax
	xorq	%rdx, %rax
	movq	%rbx, %rdx
	shrq	$12, %rdx
	movq	%rax, %rcx
	orq	%r9, %rdx
	movq	%rdx, %xmm0
	subsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	movq	%xmm0, %rdx
	cmovp	%r8, %rdx
	addq	$8, %rsi
	movq	%rdx, -8(%rsi)
	cmpq	%rsi, %rdi
	jne	.L40
	movq	%rax, %xmm2
	movq	%rbx, %xmm0
	movabsq	$274877906944, %rax
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 15(%r13)
	movq	%rax, 831(%r12)
	movq	-40(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L45
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L44:
	.cfi_restore_state
	movl	_ZN2v88internal16FLAG_random_seedE(%rip), %eax
	testl	%eax, %eax
	je	.L35
	movslq	%eax, %rdi
	movq	%rdi, -48(%rbp)
.L36:
	call	_ZN2v84base21RandomNumberGenerator11MurmurHash3Em@PLT
	movq	-48(%rbp), %rdi
	movq	%rax, %rbx
	notq	%rdi
	call	_ZN2v84base21RandomNumberGenerator11MurmurHash3Em@PLT
	movq	%rax, %rcx
	movq	%rbx, %rax
	orq	%rcx, %rax
	jne	.L34
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	call	_ZN2v88internal7Isolate23random_number_generatorEv@PLT
	leaq	-48(%rbp), %rsi
	movl	$8, %edx
	movq	%rax, %rdi
	call	_ZN2v84base21RandomNumberGenerator9NextBytesEPvm@PLT
	movq	-48(%rbp), %rdi
	jmp	.L36
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17784:
	.size	_ZN2v88internal10MathRandom11RefillCacheEPNS0_7IsolateEm, .-_ZN2v88internal10MathRandom11RefillCacheEPNS0_7IsolateEm
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10MathRandom17InitializeContextEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10MathRandom17InitializeContextEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE, @function
_GLOBAL__sub_I__ZN2v88internal10MathRandom17InitializeContextEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE:
.LFB21466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21466:
	.size	_GLOBAL__sub_I__ZN2v88internal10MathRandom17InitializeContextEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE, .-_GLOBAL__sub_I__ZN2v88internal10MathRandom17InitializeContextEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10MathRandom17InitializeContextEPNS0_7IsolateENS0_6HandleINS0_7ContextEEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	1072693248
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
