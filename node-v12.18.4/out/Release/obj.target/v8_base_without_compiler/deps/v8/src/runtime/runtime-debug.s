	.file	"runtime-debug.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5104:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5104:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5105:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5105:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5107:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5107:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZZN2v88internalL31__RT_impl_Runtime_ScheduleBreakENS0_9ArgumentsEPNS0_7IsolateEENUlPNS_7IsolateEPvE_4_FUNES5_S6_,"ax",@progbits
	.p2align 4
	.type	_ZZN2v88internalL31__RT_impl_Runtime_ScheduleBreakENS0_9ArgumentsEPNS0_7IsolateEENUlPNS_7IsolateEPvE_4_FUNES5_S6_, @function
_ZZN2v88internalL31__RT_impl_Runtime_ScheduleBreakENS0_9ArgumentsEPNS0_7IsolateEENUlPNS_7IsolateEPvE_4_FUNES5_S6_:
.LFB21643:
	.cfi_startproc
	endbr64
	jmp	_ZN2v85debug13BreakRightNowEPNS_7IsolateE@PLT
	.cfi_endproc
.LFE21643:
	.size	_ZZN2v88internalL31__RT_impl_Runtime_ScheduleBreakENS0_9ArgumentsEPNS0_7IsolateEENUlPNS_7IsolateEPvE_4_FUNES5_S6_, .-_ZZN2v88internalL31__RT_impl_Runtime_ScheduleBreakENS0_9ArgumentsEPNS0_7IsolateEENUlPNS_7IsolateEPvE_4_FUNES5_S6_
	.section	.text._ZN2v88internal12_GLOBAL__N_118ScriptLinePositionENS0_6HandleINS0_6ScriptEEEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118ScriptLinePositionENS0_6HandleINS0_6ScriptEEEi, @function
_ZN2v88internal12_GLOBAL__N_118ScriptLinePositionENS0_6HandleINS0_6ScriptEEEi:
.LFB21678:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	js	.L10
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movl	%esi, %r12d
	cmpl	$3, 51(%rax)
	je	.L15
	call	_ZN2v88internal6Script12InitLineEndsENS0_6HandleIS1_EE@PLT
	xorl	%eax, %eax
	testl	%r12d, %r12d
	je	.L6
	movq	(%rbx), %rax
	movq	55(%rax), %rdx
	cmpl	%r12d, 11(%rdx)
	jl	.L10
	leal	8(,%r12,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	sarq	$32, %rax
	addl	$1, %eax
.L6:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L16
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	71(%rax), %rax
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal16WasmModuleObject17GetFunctionOffsetEj@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$-1, %eax
	jmp	.L6
.L16:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21678:
	.size	_ZN2v88internal12_GLOBAL__N_118ScriptLinePositionENS0_6HandleINS0_6ScriptEEEi, .-_ZN2v88internal12_GLOBAL__N_118ScriptLinePositionENS0_6HandleINS0_6ScriptEEEi
	.section	.rodata._ZN2v88internalL29Stats_Runtime_IncBlockCounterEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC1:
	.string	"V8.Runtime_Runtime_IncBlockCounter"
	.section	.rodata._ZN2v88internalL29Stats_Runtime_IncBlockCounterEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC2:
	.string	"unreachable code"
	.section	.text._ZN2v88internalL29Stats_Runtime_IncBlockCounterEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_IncBlockCounterEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL29Stats_Runtime_IncBlockCounterEiPmPNS0_7IsolateE.isra.0:
.LFB27910:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$88, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L38
.L18:
	movq	_ZZN2v88internalL29Stats_Runtime_IncBlockCounterEiPmPNS0_7IsolateEE28trace_event_unique_atomic747(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L39
.L20:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L40
.L21:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L39:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL29Stats_Runtime_IncBlockCounterEiPmPNS0_7IsolateEE28trace_event_unique_atomic747(%rip)
	movq	%rax, %r12
	jmp	.L20
.L40:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	movq	-40(%rbp), %rdi
	addq	$64, %rsp
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L21
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L21
.L38:
	movq	40960(%rdi), %rdi
	leaq	-88(%rbp), %rsi
	movl	$292, %edx
	addq	$23240, %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L18
	.cfi_endproc
.LFE27910:
	.size	_ZN2v88internalL29Stats_Runtime_IncBlockCounterEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL29Stats_Runtime_IncBlockCounterEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internal12_GLOBAL__N_115MakeRangeObjectEPNS0_7IsolateERKNS0_13CoverageBlockE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"start"
.LC4:
	.string	"end"
.LC5:
	.string	"count"
	.section	.text._ZN2v88internal12_GLOBAL__N_115MakeRangeObjectEPNS0_7IsolateERKNS0_13CoverageBlockE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_115MakeRangeObjectEPNS0_7IsolateERKNS0_13CoverageBlockE, @function
_ZN2v88internal12_GLOBAL__N_115MakeRangeObjectEPNS0_7IsolateERKNS0_13CoverageBlockE:
.LFB21698:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%r13, %rsi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rax
	movq	$5, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$3, -72(%rbp)
	movq	%rax, -88(%rbp)
	leaq	.LC4(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$5, -72(%rbp)
	movq	%rax, %r15
	leaq	.LC5(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movl	(%rbx), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE@PLT
	movq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r12, %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movl	4(%rbx), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movl	8(%rbx), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L44:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21698:
	.size	_ZN2v88internal12_GLOBAL__N_115MakeRangeObjectEPNS0_7IsolateERKNS0_13CoverageBlockE, .-_ZN2v88internal12_GLOBAL__N_115MakeRangeObjectEPNS0_7IsolateERKNS0_13CoverageBlockE
	.section	.rodata._ZN2v88internalL29GetIteratorInternalPropertiesINS0_13JSMapIteratorEEENS0_11MaybeHandleINS0_7JSArrayEEEPNS0_7IsolateENS0_6HandleIT_EE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"values"
.LC7:
	.string	"keys"
.LC8:
	.string	"entries"
.LC9:
	.string	"[[IteratorHasMore]]"
.LC10:
	.string	"(location_) != nullptr"
.LC11:
	.string	"Check failed: %s."
.LC12:
	.string	"[[IteratorIndex]]"
.LC13:
	.string	"[[IteratorKind]]"
	.section	.text._ZN2v88internalL29GetIteratorInternalPropertiesINS0_13JSMapIteratorEEENS0_11MaybeHandleINS0_7JSArrayEEEPNS0_7IsolateENS0_6HandleIT_EE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29GetIteratorInternalPropertiesINS0_13JSMapIteratorEEENS0_11MaybeHandleINS0_7JSArrayEEEPNS0_7IsolateENS0_6HandleIT_EE, @function
_ZN2v88internalL29GetIteratorInternalPropertiesINS0_13JSMapIteratorEEENS0_11MaybeHandleINS0_7JSArrayEEEPNS0_7IsolateENS0_6HandleIT_EE:
.LFB24200:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subw	$1070, %ax
	cmpw	$9, %ax
	ja	.L46
	leaq	.L48(%rip), %rdx
	movzwl	%ax, %eax
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internalL29GetIteratorInternalPropertiesINS0_13JSMapIteratorEEENS0_11MaybeHandleINS0_7JSArrayEEEPNS0_7IsolateENS0_6HandleIT_EE,"a",@progbits
	.align 4
	.align 4
.L48:
	.long	.L50-.L48
	.long	.L49-.L48
	.long	.L80-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L49-.L48
	.long	.L80-.L48
	.section	.text._ZN2v88internalL29GetIteratorInternalPropertiesINS0_13JSMapIteratorEEENS0_11MaybeHandleINS0_7JSArrayEEEPNS0_7IsolateENS0_6HandleIT_EE
	.p2align 4,,10
	.p2align 3
.L80:
	leaq	.LC6(%rip), %r14
.L47:
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	xorl	%edx, %edx
	leaq	-128(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	leaq	.LC9(%rip), %rax
	movq	$19, -120(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L60
	movq	(%r12), %rdi
	movq	(%rax), %r15
	movq	%r15, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %r15b
	je	.L78
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testl	$262144, %eax
	je	.L53
	movq	%r15, %rdx
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	8(%rcx), %rax
.L53:
	testb	$24, %al
	je	.L78
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L127
	.p2align 4,,10
	.p2align 3
.L78:
	movq	(%r12), %r8
	leaq	-80(%rbp), %r9
	movq	(%rbx), %rax
	movq	%r9, %rdi
	movq	%r9, -136(%rbp)
	movq	%r8, -144(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE7HasMoreEv@PLT
	movq	-136(%rbp), %r9
	movq	-144(%rbp), %r8
	testb	%al, %al
	je	.L55
	movq	112(%r13), %r15
.L56:
	movq	%r15, 23(%r8)
	leaq	23(%r8), %rsi
	testb	$1, %r15b
	je	.L77
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testl	$262144, %eax
	je	.L58
	movq	%r8, %rdi
	movq	%r15, %rdx
	movq	%r9, -160(%rbp)
	movq	%rsi, -152(%rbp)
	movq	%r8, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	-160(%rbp), %r9
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %r8
	movq	8(%rcx), %rax
.L58:
	testb	$24, %al
	je	.L77
	movq	%r8, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L128
	.p2align 4,,10
	.p2align 3
.L77:
	leaq	.LC12(%rip), %rax
	xorl	%edx, %edx
	movq	%r9, %rsi
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	movq	$17, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L60
	movq	(%r12), %rdi
	movq	(%rax), %r15
	movq	%r15, 31(%rdi)
	leaq	31(%rdi), %rsi
	testb	$1, %r15b
	je	.L76
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testl	$262144, %eax
	je	.L62
	movq	%r15, %rdx
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	8(%rcx), %rax
.L62:
	testb	$24, %al
	je	.L76
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L129
	.p2align 4,,10
	.p2align 3
.L76:
	movq	(%rbx), %rax
	movq	(%r12), %rdi
	movq	31(%rax), %r15
	leaq	39(%rdi), %rsi
	movq	%r15, 39(%rdi)
	testb	$1, %r15b
	je	.L75
	movq	%r15, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L65
	movq	%r15, %rdx
	movq	%rsi, -144(%rbp)
	movq	%rdi, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
.L65:
	testb	$24, %al
	je	.L75
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L130
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	.LC13(%rip), %rax
	xorl	%edx, %edx
	leaq	-96(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	movq	$16, -88(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L60
	movq	(%r12), %rbx
	movq	(%rax), %r15
	movq	%r15, 47(%rbx)
	leaq	47(%rbx), %rsi
	testb	$1, %r15b
	je	.L74
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -136(%rbp)
	testl	$262144, %eax
	je	.L68
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	-144(%rbp), %rsi
	movq	8(%rcx), %rax
.L68:
	testb	$24, %al
	je	.L74
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L131
	.p2align 4,,10
	.p2align 3
.L74:
	movq	%r14, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	leaq	-112(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r14, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L60
	movq	(%r12), %r15
	movq	(%rax), %r14
	movq	%r14, 55(%r15)
	leaq	55(%r15), %rsi
	testb	$1, %r14b
	je	.L73
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L71
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-136(%rbp), %rsi
.L71:
	testb	$24, %al
	je	.L73
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L132
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%r12), %rax
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L133
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	leaq	.LC8(%rip), %r14
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	.LC7(%rip), %r14
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L55:
	movq	120(%r13), %r15
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L60:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%r15, %rdx
	movq	%r8, %rdi
	movq	%r9, -136(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %r9
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L130:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L132:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L73
.L46:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L133:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24200:
	.size	_ZN2v88internalL29GetIteratorInternalPropertiesINS0_13JSMapIteratorEEENS0_11MaybeHandleINS0_7JSArrayEEEPNS0_7IsolateENS0_6HandleIT_EE, .-_ZN2v88internalL29GetIteratorInternalPropertiesINS0_13JSMapIteratorEEENS0_11MaybeHandleINS0_7JSArrayEEEPNS0_7IsolateENS0_6HandleIT_EE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_122ScriptLocationFromLineEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6ObjectEEES8_i.str1.1,"aMS",@progbits,1
.LC14:
	.string	"opt_line->IsNumber()"
.LC19:
	.string	"opt_column->IsNumber()"
	.section	.text._ZN2v88internal12_GLOBAL__N_122ScriptLocationFromLineEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6ObjectEEES8_i,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_122ScriptLocationFromLineEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6ObjectEEES8_i, @function
_ZN2v88internal12_GLOBAL__N_122ScriptLocationFromLineEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6ObjectEEES8_i:
.LFB21681:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rcx, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	(%rdx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	104(%r13), %rax
	cmpq	%rdx, %rax
	jne	.L210
.L135:
	movq	(%rdi), %rax
	cmpq	%rdx, %rax
	jne	.L211
.L147:
	movl	%r12d, %eax
	xorl	%ebx, %ebx
	shrl	$31, %eax
.L149:
	testl	%r15d, %r15d
	js	.L190
	testb	%al, %al
	jne	.L190
	testl	%r15d, %r15d
	je	.L162
	testl	%r12d, %r12d
	je	.L162
	pcmpeqd	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %rdx
	movl	%r12d, %esi
	movq	%r14, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal6Script15GetPositionInfoENS0_6HandleIS1_EEiPNS1_12PositionInfoENS1_10OffsetFlagE@PLT
	testb	%al, %al
	je	.L190
	movl	-80(%rbp), %esi
	movq	%r14, %rdi
	addl	%r15d, %esi
	call	_ZN2v88internal12_GLOBAL__N_118ScriptLinePositionENS0_6HandleINS0_6ScriptEEEi
.L165:
	testl	%eax, %eax
	js	.L190
	testl	%ebx, %ebx
	js	.L190
	leal	(%rax,%rbx), %r12d
	pcmpeqd	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	leaq	-80(%rbp), %rdx
	movl	%r12d, %esi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal6Script15GetPositionInfoENS0_6HandleIS1_EEiPNS1_12PositionInfoENS1_10OffsetFlagE@PLT
	testb	%al, %al
	je	.L212
	movq	(%r14), %rax
	movq	41112(%r13), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L171
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L172:
	movq	(%r14), %rax
	cmpl	$3, 51(%rax)
	jne	.L174
	leaq	128(%r13), %r15
.L175:
	movq	12464(%r13), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r8
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L178
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L179:
	xorl	%edx, %edx
	movq	%r13, %rdi
	salq	$32, %r12
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	leaq	3232(%r13), %rdx
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L181
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L182:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	leaq	3064(%r13), %rdx
	xorl	%r8d, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movslq	-80(%rbp), %rsi
	movq	41112(%r13), %rdi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L184
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L185:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	leaq	2784(%r13), %rdx
	xorl	%r8d, %r8d
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movslq	-76(%rbp), %rsi
	movq	41112(%r13), %rdi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L187
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L188:
	leaq	2264(%r13), %rdx
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	leaq	3312(%r13), %rdx
	movq	%rbx, %rsi
	movq	%rbx, %r13
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
.L170:
	movq	%r13, %rax
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L211:
	cmpq	%rax, 88(%r13)
	je	.L147
	movq	%rax, %rbx
	shrq	$32, %rbx
	testb	$1, %al
	je	.L153
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L213
	movq	7(%rax), %rdx
	movsd	.LC16(%rip), %xmm2
	movq	%rdx, %xmm1
	andpd	.LC15(%rip), %xmm1
	movq	%rdx, %xmm0
	ucomisd	%xmm1, %xmm2
	jnb	.L214
.L154:
	movabsq	$9218868437227405312, %rax
	testq	%rax, %rdx
	je	.L198
	movq	%rdx, %rax
	xorl	%ebx, %ebx
	shrq	$52, %rax
	andl	$2047, %eax
	movl	%eax, %ecx
	subl	$1075, %ecx
	js	.L215
	cmpl	$31, %ecx
	jg	.L153
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rax
	andq	%rdx, %rbx
	addq	%rax, %rbx
	salq	%cl, %rbx
	movl	%ebx, %ebx
.L159:
	sarq	$63, %rdx
	orl	$1, %edx
	imull	%edx, %ebx
.L153:
	movl	%r12d, %eax
	shrl	$31, %eax
	testl	%r15d, %r15d
	jne	.L149
	movq	(%r14), %rax
	subl	35(%rax), %ebx
	testl	%r12d, %r12d
	jns	.L162
	.p2align 4,,10
	.p2align 3
.L190:
	leaq	104(%r13), %rax
.L168:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L216
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118ScriptLinePositionENS0_6HandleINS0_6ScriptEEEi
	addl	%r12d, %eax
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L210:
	cmpq	%rdx, 88(%r13)
	je	.L193
	testb	$1, %dl
	je	.L136
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L217
	movq	%rdx, %rcx
	movq	%rax, %rdx
	testb	$1, %cl
	je	.L218
	movq	7(%rcx), %r15
	movsd	.LC16(%rip), %xmm2
	movq	%r15, %xmm1
	andpd	.LC15(%rip), %xmm1
	movq	%r15, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L140
	movsd	.LC17(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L140
	comisd	.LC18(%rip), %xmm0
	jb	.L140
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L140
	je	.L139
	.p2align 4,,10
	.p2align 3
.L140:
	movabsq	$9218868437227405312, %rax
	testq	%rax, %r15
	je	.L194
	movq	%r15, %rax
	xorl	%esi, %esi
	shrq	$52, %rax
	andl	$2047, %eax
	movl	%eax, %ecx
	subl	$1075, %ecx
	js	.L219
	cmpl	$31, %ecx
	jg	.L139
	movabsq	$4503599627370495, %rsi
	movq	%rsi, %rax
	addq	$1, %rsi
	andq	%r15, %rax
	addq	%rax, %rsi
	salq	%cl, %rsi
	movl	%esi, %esi
.L145:
	sarq	$63, %r15
	orl	$1, %r15d
	imull	%r15d, %esi
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%rax, %rdx
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L212:
	addq	$104, %r13
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L174:
	movl	-72(%rbp), %edx
	movl	-68(%rbp), %ecx
	testl	%edx, %edx
	jne	.L176
	movq	(%r15), %rax
	cmpl	11(%rax), %ecx
	je	.L175
.L176:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	%rax, %r15
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L171:
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L220
.L173:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L181:
	movq	41088(%r13), %rcx
	cmpq	41096(%r13), %rcx
	je	.L221
.L183:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r13)
	movq	%r12, (%rcx)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L178:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L222
.L180:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r8, (%rsi)
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L187:
	movq	41088(%r13), %rcx
	cmpq	41096(%r13), %rcx
	je	.L223
.L189:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rcx)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L184:
	movq	41088(%r13), %rcx
	cmpq	41096(%r13), %rcx
	je	.L224
.L186:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rcx)
	jmp	.L185
.L218:
	movq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L136:
	shrq	$32, %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdx
.L139:
	movq	(%r14), %rax
	subl	27(%rax), %esi
	movl	%esi, %r15d
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L214:
	movsd	.LC17(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L154
	comisd	.LC18(%rip), %xmm0
	jb	.L154
	cvttsd2sil	%xmm0, %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L154
	je	.L153
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L219:
	cmpl	$-52, %ecx
	jl	.L139
	movabsq	$4503599627370495, %rsi
	movabsq	$4503599627370496, %rcx
	andq	%r15, %rsi
	addq	%rcx, %rsi
	movl	$1075, %ecx
	subl	%eax, %ecx
	shrq	%cl, %rsi
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L215:
	cmpl	$-52, %ecx
	jl	.L153
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %rbx
	addq	%rcx, %rbx
	movl	$1075, %ecx
	subl	%eax, %ecx
	shrq	%cl, %rbx
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L217:
	leaq	.LC14(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L213:
	leaq	.LC19(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L224:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L223:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L222:
	movq	%r13, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L221:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L220:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L173
.L194:
	xorl	%esi, %esi
	jmp	.L139
.L198:
	xorl	%ebx, %ebx
	jmp	.L153
.L216:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21681:
	.size	_ZN2v88internal12_GLOBAL__N_122ScriptLocationFromLineEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6ObjectEEES8_i, .-_ZN2v88internal12_GLOBAL__N_122ScriptLocationFromLineEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6ObjectEEES8_i
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB10394:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L231
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L234
.L225:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L225
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE10394:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL37Stats_Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"V8.Runtime_Runtime_HandleDebuggerStatement"
	.section	.text._ZN2v88internalL37Stats_Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL37Stats_Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateE.isra.0:
.LFB27908:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L265
.L236:
	movq	_ZZN2v88internalL37Stats_Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateEE28trace_event_unique_atomic133(%rip), %r12
	testq	%r12, %r12
	je	.L266
.L238:
	movq	$0, -144(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L267
.L240:
	movq	41472(%rbx), %rdi
	cmpb	$0, 13(%rdi)
	jne	.L268
.L244:
	leaq	37512(%rbx), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	leaq	-144(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L269
.L235:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L270
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L271
.L239:
	movq	%r12, _ZZN2v88internalL37Stats_Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateEE28trace_event_unique_atomic133(%rip)
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L268:
	movl	$1, %esi
	call	_ZN2v88internal5Debug16HandleDebugBreakENS0_15IgnoreBreakModeE@PLT
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L267:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L272
.L241:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L242
	movq	(%rdi), %rax
	call	*8(%rax)
.L242:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L243
	movq	(%rdi), %rax
	call	*8(%rax)
.L243:
	leaq	.LC20(%rip), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L265:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$285, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L269:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L272:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC20(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L271:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L239
.L270:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27908:
	.size	_ZN2v88internalL37Stats_Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL37Stats_Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL44Stats_Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"V8.Runtime_Runtime_SetGeneratorScopeVariableValue"
	.section	.rodata._ZN2v88internalL44Stats_Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC22:
	.string	"args[0].IsJSGeneratorObject()"
.LC23:
	.string	"args[1].IsNumber()"
.LC24:
	.string	"args[2].IsString()"
	.section	.text._ZN2v88internalL44Stats_Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Stats_Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateE, @function
_ZN2v88internalL44Stats_Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateE:
.LFB21654:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -224(%rbp)
	movq	$0, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L339
.L274:
	movq	_ZZN2v88internalL44Stats_Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateEE28trace_event_unique_atomic379(%rip), %rbx
	testq	%rbx, %rbx
	je	.L340
.L276:
	movq	$0, -256(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L341
.L278:
	movq	41088(%r14), %rax
	addl	$1, 41104(%r14)
	movq	41096(%r14), %r13
	movq	%rax, -264(%rbp)
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L342
.L283:
	leaq	.LC22(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L340:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L343
.L277:
	movq	%rbx, _ZZN2v88internalL44Stats_Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateEE28trace_event_unique_atomic379(%rip)
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L342:
	movq	-1(%rax), %rdx
	cmpw	$1068, 11(%rdx)
	jne	.L284
.L287:
	movq	-8(%r12), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	movq	%rcx, %rbx
	testb	$1, %al
	je	.L291
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L344
	movq	7(%rax), %rax
	movsd	.LC16(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC15(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L292
	movsd	.LC17(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L345
.L292:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L314
	movq	%rax, %rsi
	xorl	%ebx, %ebx
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L346
	cmpl	$31, %ecx
	jg	.L291
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rdx
	andq	%rax, %rbx
	addq	%rdx, %rbx
	salq	%cl, %rbx
	movl	%ebx, %edx
.L297:
	sarq	$63, %rax
	movq	%rax, %rbx
	orl	$1, %ebx
	imull	%edx, %ebx
.L291:
	leaq	-16(%r12), %rax
	movq	%rax, -272(%rbp)
	movq	-16(%r12), %rax
	testb	$1, %al
	jne	.L347
.L299:
	leaq	.LC24(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L341:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L348
.L279:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L280
	movq	(%rdi), %rax
	call	*8(%rax)
.L280:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L281
	movq	(%rdi), %rax
	call	*8(%rax)
.L281:
	leaq	.LC21(%rip), %rax
	movq	%rbx, -248(%rbp)
	movq	%rax, -240(%rbp)
	leaq	-248(%rbp), %rax
	movq	%r13, -232(%rbp)
	movq	%rax, -256(%rbp)
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L347:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L299
	leaq	-176(%rbp), %r15
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal13ScopeIteratorC1EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE@PLT
	leaq	-24(%r12), %rax
	cmpq	$0, -136(%rbp)
	movq	%rax, -280(%rbp)
	je	.L309
	xorl	%r12d, %r12d
	testl	%ebx, %ebx
	jg	.L302
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L304:
	cmpl	%r12d, %ebx
	je	.L303
.L302:
	movq	%r15, %rdi
	addl	$1, %r12d
	call	_ZN2v88internal13ScopeIterator4NextEv@PLT
	cmpq	$0, -136(%rbp)
	jne	.L304
.L309:
	movq	120(%r14), %r12
.L305:
	movq	%r15, %rdi
	call	_ZN2v88internal13ScopeIteratorD1Ev@PLT
	movq	-264(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	cmpq	41096(%r14), %r13
	je	.L308
	movq	%r13, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L308:
	leaq	-256(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L349
.L273:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L350
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	movq	-280(%rbp), %rdx
	movq	-272(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal13ScopeIterator16SetVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L309
	movq	112(%r14), %r12
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L284:
	movq	-1(%rax), %rdx
	cmpw	$1063, 11(%rdx)
	je	.L287
	movq	-1(%rax), %rax
	cmpw	$1064, 11(%rax)
	jne	.L283
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L345:
	comisd	.LC18(%rip), %xmm0
	jb	.L292
	cvttsd2sil	%xmm0, %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L292
	je	.L291
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L346:
	cmpl	$-52, %ecx
	jl	.L291
	movabsq	$4503599627370495, %rbx
	movl	$1075, %ecx
	movabsq	$4503599627370496, %rdx
	andq	%rax, %rbx
	subl	%esi, %ecx
	addq	%rdx, %rbx
	movq	%rbx, %rdx
	shrq	%cl, %rdx
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L339:
	movq	40960(%rdx), %rax
	leaq	-216(%rbp), %rsi
	movl	$291, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -224(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L344:
	leaq	.LC23(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L348:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC21(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L343:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L349:
	leaq	-216(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L273
.L314:
	xorl	%ebx, %ebx
	jmp	.L291
.L350:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21654:
	.size	_ZN2v88internalL44Stats_Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateE, .-_ZN2v88internalL44Stats_Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Stats_Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"V8.Runtime_Runtime_IsBreakOnException"
	.section	.rodata._ZN2v88internalL32Stats_Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC26:
	.string	"args[0].IsNumber()"
	.section	.text._ZN2v88internalL32Stats_Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateE.isra.0:
.LFB27909:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L404
.L352:
	movq	_ZZN2v88internalL32Stats_Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateEE28trace_event_unique_atomic413(%rip), %rbx
	testq	%rbx, %rbx
	je	.L405
.L354:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L406
.L356:
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	(%r14), %rsi
	testb	$1, %sil
	jne	.L407
	shrq	$32, %rsi
.L364:
	movq	41472(%r12), %rdi
	call	_ZN2v88internal5Debug18IsBreakOnExceptionENS0_18ExceptionBreakTypeE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movzbl	%al, %eax
	salq	$32, %rax
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L375
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L375:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L408
.L351:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L409
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	cmpw	$65, 11(%rax)
	jne	.L410
	movq	7(%rsi), %rdx
	movsd	.LC16(%rip), %xmm2
	movq	%rdx, %xmm1
	andpd	.LC15(%rip), %xmm1
	movq	%rdx, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L365
	movsd	.LC17(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L411
.L365:
	movabsq	$9218868437227405312, %rax
	testq	%rax, %rdx
	je	.L379
	movq	%rdx, %rdi
	xorl	%esi, %esi
	shrq	$52, %rdi
	andl	$2047, %edi
	movl	%edi, %ecx
	subl	$1075, %ecx
	js	.L412
	cmpl	$31, %ecx
	jg	.L364
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rsi
	andq	%rdx, %rax
	addq	%rax, %rsi
	salq	%cl, %rsi
	movl	%esi, %esi
.L371:
	sarq	$63, %rdx
	orl	$1, %edx
	imull	%edx, %esi
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L406:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L413
.L357:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L358
	movq	(%rdi), %rax
	call	*8(%rax)
.L358:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L359
	movq	(%rdi), %rax
	call	*8(%rax)
.L359:
	leaq	.LC25(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L405:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L414
.L355:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateEE28trace_event_unique_atomic413(%rip)
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L411:
	comisd	.LC18(%rip), %xmm0
	jb	.L365
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L365
	je	.L364
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L412:
	cmpl	$-52, %ecx
	jl	.L364
	movabsq	$4503599627370495, %rsi
	movl	$1075, %ecx
	movq	%rsi, %rax
	addq	$1, %rsi
	subl	%edi, %ecx
	andq	%rdx, %rax
	addq	%rax, %rsi
	shrq	%cl, %rsi
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L410:
	leaq	.LC26(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L404:
	movq	40960(%rsi), %rax
	movl	$286, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L408:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L414:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L413:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC25(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L357
.L379:
	xorl	%esi, %esi
	jmp	.L364
.L409:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27909:
	.size	_ZN2v88internalL32Stats_Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL26Stats_Runtime_GetHeapUsageEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"V8.Runtime_Runtime_GetHeapUsage"
	.section	.text._ZN2v88internalL26Stats_Runtime_GetHeapUsageEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL26Stats_Runtime_GetHeapUsageEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL26Stats_Runtime_GetHeapUsageEiPmPNS0_7IsolateE.isra.0:
.LFB27913:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L444
.L416:
	movq	_ZZN2v88internalL26Stats_Runtime_GetHeapUsageEiPmPNS0_7IsolateEE28trace_event_unique_atomic478(%rip), %r12
	testq	%r12, %r12
	je	.L445
.L418:
	movq	$0, -144(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L446
.L420:
	leaq	37592(%rbx), %rdi
	call	_ZN2v88internal4Heap13SizeOfObjectsEv@PLT
	leaq	-144(%rbp), %rdi
	salq	$32, %rax
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L447
.L415:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L448
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L449
.L419:
	movq	%r12, _ZZN2v88internalL26Stats_Runtime_GetHeapUsageEiPmPNS0_7IsolateEE28trace_event_unique_atomic478(%rip)
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L446:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L450
.L421:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L422
	movq	(%rdi), %rax
	call	*8(%rax)
.L422:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L423
	movq	(%rdi), %rax
	call	*8(%rax)
.L423:
	leaq	.LC27(%rip), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L447:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L444:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$284, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L450:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC27(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L449:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L419
.L448:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27913:
	.size	_ZN2v88internalL26Stats_Runtime_GetHeapUsageEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL26Stats_Runtime_GetHeapUsageEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL37Stats_Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC28:
	.string	"V8.Runtime_Runtime_ScriptLocationFromLine2"
	.section	.rodata._ZN2v88internalL37Stats_Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC29:
	.string	"args[3].IsNumber()"
	.section	.rodata._ZN2v88internalL37Stats_Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateE.isra.0.str1.8
	.align 8
.LC30:
	.string	"GetScriptById(isolate, scriptid, &script)"
	.section	.text._ZN2v88internalL37Stats_Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL37Stats_Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateE.isra.0:
.LFB27916:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L532
.L452:
	movq	_ZZN2v88internalL37Stats_Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateEE28trace_event_unique_atomic601(%rip), %rbx
	testq	%rbx, %rbx
	je	.L533
.L454:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L534
.L456:
	addl	$1, 41104(%r15)
	movq	41088(%r15), %rax
	movq	(%r12), %rbx
	movq	41096(%r15), %r14
	movq	%rax, -184(%rbp)
	testb	$1, %bl
	jne	.L535
	shrq	$32, %rbx
.L464:
	leaq	-8(%r12), %rax
	movq	%rax, -192(%rbp)
	leaq	-16(%r12), %rax
	movq	-24(%r12), %r12
	movq	%rax, -200(%rbp)
	testb	$1, %r12b
	jne	.L536
	shrq	$32, %r12
.L476:
	leaq	-176(%rbp), %r13
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Script8IteratorC1EPNS0_7IsolateE@PLT
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L484:
	cmpl	%ebx, 67(%rax)
	je	.L537
.L531:
	movq	%r13, %rdi
	call	_ZN2v88internal6Script8Iterator4NextEv@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L484
	leaq	.LC30(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L536:
	movq	-1(%r12), %rax
	cmpw	$65, 11(%rax)
	jne	.L538
	movq	7(%r12), %rax
	movsd	.LC16(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC15(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L477
	movsd	.LC17(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L539
.L477:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L500
	movq	%rax, %rsi
	xorl	%r12d, %r12d
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L540
	cmpl	$31, %ecx
	jg	.L476
	movabsq	$4503599627370495, %r12
	movabsq	$4503599627370496, %rdx
	andq	%rax, %r12
	addq	%rdx, %r12
	salq	%cl, %r12
	movl	%r12d, %edx
.L482:
	sarq	$63, %rax
	movq	%rax, %r12
	orl	$1, %r12d
	imull	%edx, %r12d
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L535:
	movq	-1(%rbx), %rax
	cmpw	$65, 11(%rax)
	jne	.L541
	movq	7(%rbx), %rax
	movsd	.LC16(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC15(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L465
	movsd	.LC17(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L542
.L465:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L496
	movq	%rax, %rsi
	xorl	%ebx, %ebx
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L543
	cmpl	$31, %ecx
	jg	.L464
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rdx
	andq	%rax, %rbx
	addq	%rdx, %rbx
	salq	%cl, %rbx
	movl	%ebx, %edx
.L470:
	sarq	$63, %rax
	movq	%rax, %rbx
	orl	$1, %ebx
	imull	%edx, %ebx
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L537:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L486
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r9
.L487:
	movq	-200(%rbp), %rcx
	movl	%r12d, %r8d
	movq	%r9, %rsi
	movq	%r15, %rdi
	movq	-192(%rbp), %rdx
	call	_ZN2v88internal12_GLOBAL__N_122ScriptLocationFromLineEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6ObjectEEES8_i
	movq	(%rax), %r12
	movq	-184(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %r14
	je	.L490
	movq	%r14, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L490:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L544
.L451:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L545
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L534:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L546
.L457:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L458
	movq	(%rdi), %rax
	call	*8(%rax)
.L458:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L459
	movq	(%rdi), %rax
	call	*8(%rax)
.L459:
	leaq	.LC28(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L533:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L547
.L455:
	movq	%rbx, _ZZN2v88internalL37Stats_Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateEE28trace_event_unique_atomic601(%rip)
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L542:
	comisd	.LC18(%rip), %xmm0
	jb	.L465
	cvttsd2sil	%xmm0, %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L465
	je	.L464
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L539:
	comisd	.LC18(%rip), %xmm0
	jb	.L477
	cvttsd2sil	%xmm0, %r12d
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%r12d, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L477
	je	.L476
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L486:
	movq	41088(%r15), %r9
	cmpq	41096(%r15), %r9
	je	.L548
.L488:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r9)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L543:
	cmpl	$-52, %ecx
	jl	.L464
	movabsq	$4503599627370495, %rbx
	movl	$1075, %ecx
	movabsq	$4503599627370496, %rdx
	andq	%rax, %rbx
	subl	%esi, %ecx
	addq	%rdx, %rbx
	movq	%rbx, %rdx
	shrq	%cl, %rdx
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L540:
	cmpl	$-52, %ecx
	jl	.L476
	movabsq	$4503599627370495, %r12
	movl	$1075, %ecx
	movabsq	$4503599627370496, %rdx
	andq	%rax, %r12
	subl	%esi, %ecx
	addq	%rdx, %r12
	movq	%r12, %rdx
	shrq	%cl, %rdx
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L541:
	leaq	.LC26(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L532:
	movq	40960(%rsi), %rax
	movl	$290, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L538:
	leaq	.LC29(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L544:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L547:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L546:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC28(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L548:
	movq	%r15, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L488
.L496:
	xorl	%ebx, %ebx
	jmp	.L464
.L500:
	xorl	%r12d, %r12d
	jmp	.L476
.L545:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27916:
	.size	_ZN2v88internalL37Stats_Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL37Stats_Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC31:
	.string	"V8.Runtime_Runtime_DebugBreakAtEntry"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC32:
	.string	"args[0].IsJSFunction()"
	.section	.text._ZN2v88internalL31Stats_Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Stats_Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateE:
.LFB21633:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$1560, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1568(%rbp)
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L587
.L550:
	movq	_ZZN2v88internalL31Stats_Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateEE28trace_event_unique_atomic109(%rip), %rbx
	testq	%rbx, %rbx
	je	.L588
.L552:
	movq	$0, -1600(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L589
.L554:
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L558
.L559:
	leaq	.LC32(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L588:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L590
.L553:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateEE28trace_event_unique_atomic109(%rip)
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L558:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L559
	leaq	-1520(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -104(%rbp)
	je	.L560
	movq	%r13, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
.L560:
	movq	%r13, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-104(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L561
	movq	12528(%r12), %rax
	cmpq	%rax, 32(%rsi)
	jb	.L591
.L561:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r13
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L564
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L564:
	leaq	-1600(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L592
.L549:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L593
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L589:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L594
.L555:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L556
	movq	(%rdi), %rax
	call	*8(%rax)
.L556:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L557
	movq	(%rdi), %rax
	call	*8(%rax)
.L557:
	leaq	.LC31(%rip), %rax
	movq	%rbx, -1592(%rbp)
	movq	%rax, -1584(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%r13, -1576(%rbp)
	movq	%rax, -1600(%rbp)
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L591:
	movq	41472(%r12), %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal5Debug5BreakEPNS0_15JavaScriptFrameENS0_6HandleINS0_10JSFunctionEEE@PLT
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L587:
	movq	40960(%rdx), %rax
	leaq	-1560(%rbp), %rsi
	movl	$271, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -1568(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L592:
	leaq	-1560(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L590:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L594:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC31(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L555
.L593:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21633:
	.size	_ZN2v88internalL31Stats_Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Stats_Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL27Stats_Runtime_ScheduleBreakEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC33:
	.string	"V8.Runtime_Runtime_ScheduleBreak"
	.section	.text._ZN2v88internalL27Stats_Runtime_ScheduleBreakEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_ScheduleBreakEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL27Stats_Runtime_ScheduleBreakEiPmPNS0_7IsolateE.isra.0:
.LFB27924:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L624
.L596:
	movq	_ZZN2v88internalL27Stats_Runtime_ScheduleBreakEiPmPNS0_7IsolateEE28trace_event_unique_atomic142(%rip), %rbx
	testq	%rbx, %rbx
	je	.L625
.L598:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L626
.L600:
	movq	%r12, %rdi
	xorl	%edx, %edx
	leaq	_ZZN2v88internalL31__RT_impl_Runtime_ScheduleBreakENS0_9ArgumentsEPNS0_7IsolateEENUlPNS_7IsolateEPvE_4_FUNES5_S6_(%rip), %rsi
	call	_ZN2v88internal7Isolate16RequestInterruptEPFvPNS_7IsolateEPvES4_@PLT
	leaq	-144(%rbp), %rdi
	movq	88(%r12), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L627
.L595:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L628
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L625:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L629
.L599:
	movq	%rbx, _ZZN2v88internalL27Stats_Runtime_ScheduleBreakEiPmPNS0_7IsolateEE28trace_event_unique_atomic142(%rip)
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L626:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L630
.L601:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L602
	movq	(%rdi), %rax
	call	*8(%rax)
.L602:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L603
	movq	(%rdi), %rax
	call	*8(%rax)
.L603:
	leaq	.LC33(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L627:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L624:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$289, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L630:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC33(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L629:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L599
.L628:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27924:
	.size	_ZN2v88internalL27Stats_Runtime_ScheduleBreakEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL27Stats_Runtime_ScheduleBreakEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_GetBreakLocationsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC34:
	.string	"V8.Runtime_Runtime_GetBreakLocations"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_GetBreakLocationsEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC35:
	.string	"isolate->debug()->is_active()"
	.section	.text._ZN2v88internalL31Stats_Runtime_GetBreakLocationsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_GetBreakLocationsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Stats_Runtime_GetBreakLocationsEiPmPNS0_7IsolateE:
.LFB21657:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L670
.L632:
	movq	_ZZN2v88internalL31Stats_Runtime_GetBreakLocationsEiPmPNS0_7IsolateEE28trace_event_unique_atomic392(%rip), %rbx
	testq	%rbx, %rbx
	je	.L671
.L634:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L672
.L636:
	movq	41472(%r12), %rax
	movq	41088(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r14
	cmpb	$0, 8(%rax)
	je	.L673
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L641
.L642:
	leaq	.LC32(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L671:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L674
.L635:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_GetBreakLocationsEiPmPNS0_7IsolateEE28trace_event_unique_atomic392(%rip)
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L672:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L675
.L637:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L638
	movq	(%rdi), %rax
	call	*8(%rax)
.L638:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L639
	movq	(%rdi), %rax
	call	*8(%rax)
.L639:
	leaq	.LC34(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L641:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L642
	movq	23(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L643
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L644:
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug23GetSourceBreakLocationsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movq	%rax, %rsi
	movq	(%rax), %rax
	movq	%rax, %r13
	cmpq	88(%r12), %rax
	je	.L647
	movslq	11(%rax), %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
.L647:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L650
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L650:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L676
.L631:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L677
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L643:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L678
.L645:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L673:
	leaq	.LC35(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L670:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$281, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L676:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L674:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L675:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC34(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L678:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L645
.L677:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21657:
	.size	_ZN2v88internalL31Stats_Runtime_GetBreakLocationsEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Stats_Runtime_GetBreakLocationsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL27Stats_Runtime_ClearSteppingEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC36:
	.string	"V8.Runtime_Runtime_ClearStepping"
	.section	.text._ZN2v88internalL27Stats_Runtime_ClearSteppingEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_ClearSteppingEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL27Stats_Runtime_ClearSteppingEiPmPNS0_7IsolateE.isra.0:
.LFB27925:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L710
.L680:
	movq	_ZZN2v88internalL27Stats_Runtime_ClearSteppingEiPmPNS0_7IsolateEE28trace_event_unique_atomic424(%rip), %rbx
	testq	%rbx, %rbx
	je	.L711
.L682:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L712
.L684:
	movq	41472(%r12), %rdi
	movq	41088(%r12), %r13
	addl	$1, 41104(%r12)
	movq	41096(%r12), %rbx
	cmpb	$0, 8(%rdi)
	je	.L713
	call	_ZN2v88internal5Debug13ClearSteppingEv@PLT
	movq	%r13, 41088(%r12)
	movq	88(%r12), %r14
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L689
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L689:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L714
.L679:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L715
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L712:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L716
.L685:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L686
	movq	(%rdi), %rax
	call	*8(%rax)
.L686:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L687
	movq	(%rdi), %rax
	call	*8(%rax)
.L687:
	leaq	.LC36(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L711:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L717
.L683:
	movq	%rbx, _ZZN2v88internalL27Stats_Runtime_ClearSteppingEiPmPNS0_7IsolateEE28trace_event_unique_atomic424(%rip)
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L713:
	leaq	.LC35(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L710:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$265, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L714:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L717:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L716:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC36(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L685
.L715:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27925:
	.size	_ZN2v88internalL27Stats_Runtime_ClearSteppingEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL27Stats_Runtime_ClearSteppingEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL28Stats_Runtime_CollectGarbageEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"V8.Runtime_Runtime_CollectGarbage"
	.section	.text._ZN2v88internalL28Stats_Runtime_CollectGarbageEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_CollectGarbageEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL28Stats_Runtime_CollectGarbageEiPmPNS0_7IsolateE.isra.0:
.LFB27926:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L747
.L719:
	movq	_ZZN2v88internalL28Stats_Runtime_CollectGarbageEiPmPNS0_7IsolateEE28trace_event_unique_atomic468(%rip), %rbx
	testq	%rbx, %rbx
	je	.L748
.L721:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L749
.L723:
	leaq	37592(%r12), %rdi
	xorl	%ecx, %ecx
	movl	$18, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal4Heap24PreciseCollectAllGarbageEiNS0_23GarbageCollectionReasonENS_15GCCallbackFlagsE@PLT
	leaq	-144(%rbp), %rdi
	movq	88(%r12), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L750
.L718:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L751
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L748:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L752
.L722:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_CollectGarbageEiPmPNS0_7IsolateEE28trace_event_unique_atomic468(%rip)
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L749:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L753
.L724:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L725
	movq	(%rdi), %rax
	call	*8(%rax)
.L725:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L726
	movq	(%rdi), %rax
	call	*8(%rax)
.L726:
	leaq	.LC37(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L750:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L747:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$266, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L753:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC37(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L752:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L722
.L751:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27926:
	.size	_ZN2v88internalL28Stats_Runtime_CollectGarbageEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL28Stats_Runtime_CollectGarbageEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL50Stats_Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC38:
	.string	"V8.Runtime_Runtime_DebugPrepareStepInSuspendedGenerator"
	.section	.text._ZN2v88internalL50Stats_Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL50Stats_Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL50Stats_Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateE.isra.0:
.LFB27927:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L785
.L755:
	movq	_ZZN2v88internalL50Stats_Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateEE28trace_event_unique_atomic639(%rip), %rbx
	testq	%rbx, %rbx
	je	.L786
.L757:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L787
.L759:
	movq	41088(%r12), %r14
	movq	41472(%r12), %rdi
	addl	$1, 41104(%r12)
	movq	41096(%r12), %rbx
	call	_ZN2v88internal5Debug31PrepareStepInSuspendedGeneratorEv@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L765
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L765:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L788
.L754:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L789
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L787:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L790
.L760:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L761
	movq	(%rdi), %rax
	call	*8(%rax)
.L761:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L762
	movq	(%rdi), %rax
	call	*8(%rax)
.L762:
	leaq	.LC38(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L786:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L791
.L758:
	movq	%rbx, _ZZN2v88internalL50Stats_Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateEE28trace_event_unique_atomic639(%rip)
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L785:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$276, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L788:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L791:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L790:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC38(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L760
.L789:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27927:
	.size	_ZN2v88internalL50Stats_Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL50Stats_Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL30Stats_Runtime_DebugPushPromiseEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC39:
	.string	"V8.Runtime_Runtime_DebugPushPromise"
	.section	.rodata._ZN2v88internalL30Stats_Runtime_DebugPushPromiseEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC40:
	.string	"args[0].IsJSObject()"
	.section	.text._ZN2v88internalL30Stats_Runtime_DebugPushPromiseEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_DebugPushPromiseEiPmPNS0_7IsolateE, @function
_ZN2v88internalL30Stats_Runtime_DebugPushPromiseEiPmPNS0_7IsolateE:
.LFB21692:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L824
.L793:
	movq	_ZZN2v88internalL30Stats_Runtime_DebugPushPromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic646(%rip), %rbx
	testq	%rbx, %rbx
	je	.L825
.L795:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L826
.L797:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L801
.L802:
	leaq	.LC40(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L825:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L827
.L796:
	movq	%rbx, _ZZN2v88internalL30Stats_Runtime_DebugPushPromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic646(%rip)
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L801:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L802
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate11PushPromiseENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L803
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L803:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L828
.L792:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L829
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L826:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L830
.L798:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L799
	movq	(%rdi), %rax
	call	*8(%rax)
.L799:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L800
	movq	(%rdi), %rax
	call	*8(%rax)
.L800:
	leaq	.LC39(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L824:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$277, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L828:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L827:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L830:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC39(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L798
.L829:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21692:
	.size	_ZN2v88internalL30Stats_Runtime_DebugPushPromiseEiPmPNS0_7IsolateE, .-_ZN2v88internalL30Stats_Runtime_DebugPushPromiseEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL29Stats_Runtime_DebugPopPromiseEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC41:
	.string	"V8.Runtime_Runtime_DebugPopPromise"
	.section	.text._ZN2v88internalL29Stats_Runtime_DebugPopPromiseEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_DebugPopPromiseEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL29Stats_Runtime_DebugPopPromiseEiPmPNS0_7IsolateE.isra.0:
.LFB27928:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L860
.L832:
	movq	_ZZN2v88internalL29Stats_Runtime_DebugPopPromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic655(%rip), %rbx
	testq	%rbx, %rbx
	je	.L861
.L834:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L862
.L836:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10PopPromiseEv@PLT
	leaq	-144(%rbp), %rdi
	movq	88(%r12), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L863
.L831:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L864
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L861:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L865
.L835:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_DebugPopPromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic655(%rip)
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L862:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L866
.L837:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L838
	movq	(%rdi), %rax
	call	*8(%rax)
.L838:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L839
	movq	(%rdi), %rax
	call	*8(%rax)
.L839:
	leaq	.LC41(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L863:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L860:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$275, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L866:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC41(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L865:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L835
.L864:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27928:
	.size	_ZN2v88internalL29Stats_Runtime_DebugPopPromiseEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL29Stats_Runtime_DebugPopPromiseEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL39Stats_Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC42:
	.string	"V8.Runtime_Runtime_DebugAsyncFunctionEntered"
	.section	.rodata._ZN2v88internalL39Stats_Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC43:
	.string	"args[0].IsJSPromise()"
	.section	.text._ZN2v88internalL39Stats_Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Stats_Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Stats_Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateE:
.LFB21738:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L901
.L868:
	movq	_ZZN2v88internalL39Stats_Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateEE28trace_event_unique_atomic751(%rip), %rbx
	testq	%rbx, %rbx
	je	.L902
.L870:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L903
.L872:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L876
.L877:
	leaq	.LC43(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L902:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L904
.L871:
	movq	%rbx, _ZZN2v88internalL39Stats_Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateEE28trace_event_unique_atomic751(%rip)
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L876:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L877
	xorl	%esi, %esi
	leaq	88(%r12), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE@PLT
	movq	41472(%r12), %rax
	cmpb	$0, 8(%rax)
	jne	.L905
.L878:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r13
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L881
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L881:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L906
.L867:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L907
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L903:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L908
.L873:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L874
	movq	(%rdi), %rax
	call	*8(%rax)
.L874:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L875
	movq	(%rdi), %rax
	call	*8(%rax)
.L875:
	leaq	.LC42(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L905:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate11PushPromiseENS0_6HandleINS0_8JSObjectEEE@PLT
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L901:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$267, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L906:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L904:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L908:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC42(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L873
.L907:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21738:
	.size	_ZN2v88internalL39Stats_Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Stats_Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Stats_Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC44:
	.string	"V8.Runtime_Runtime_DebugAsyncFunctionSuspended"
	.section	.text._ZN2v88internalL41Stats_Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Stats_Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateE, @function
_ZN2v88internalL41Stats_Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateE:
.LFB21741:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L941
.L910:
	movq	_ZZN2v88internalL41Stats_Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateEE28trace_event_unique_atomic761(%rip), %rbx
	testq	%rbx, %rbx
	je	.L942
.L912:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L943
.L914:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L918
.L919:
	leaq	.LC43(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L942:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L944
.L913:
	movq	%rbx, _ZZN2v88internalL41Stats_Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateEE28trace_event_unique_atomic761(%rip)
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L918:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L919
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10PopPromiseEv@PLT
	movq	%r13, %rsi
	movl	$5, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate27OnAsyncFunctionStateChangedENS0_6HandleINS0_9JSPromiseEEENS_5debug20DebugAsyncActionTypeE@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L920
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L920:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L945
.L909:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L946
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L943:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L947
.L915:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L916
	movq	(%rdi), %rax
	call	*8(%rax)
.L916:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L917
	movq	(%rdi), %rax
	call	*8(%rax)
.L917:
	leaq	.LC44(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L941:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$268, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L910
	.p2align 4,,10
	.p2align 3
.L945:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L944:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L947:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC44(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L915
.L946:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21741:
	.size	_ZN2v88internalL41Stats_Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateE, .-_ZN2v88internalL41Stats_Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Stats_Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC45:
	.string	"V8.Runtime_Runtime_DebugAsyncFunctionResumed"
	.section	.text._ZN2v88internalL39Stats_Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Stats_Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Stats_Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateE:
.LFB21744:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L980
.L949:
	movq	_ZZN2v88internalL39Stats_Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateEE28trace_event_unique_atomic770(%rip), %rbx
	testq	%rbx, %rbx
	je	.L981
.L951:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L982
.L953:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L957
.L958:
	leaq	.LC43(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L981:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L983
.L952:
	movq	%rbx, _ZZN2v88internalL39Stats_Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateEE28trace_event_unique_atomic770(%rip)
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L957:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L958
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate11PushPromiseENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L959
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L959:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L984
.L948:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L985
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L982:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L986
.L954:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L955
	movq	(%rdi), %rax
	call	*8(%rax)
.L955:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L956
	movq	(%rdi), %rax
	call	*8(%rax)
.L956:
	leaq	.LC45(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L980:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$269, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L984:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L983:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L986:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC45(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L954
.L985:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21744:
	.size	_ZN2v88internalL39Stats_Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Stats_Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Stats_Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC46:
	.string	"V8.Runtime_Runtime_ProfileCreateSnapshotDataBlob"
	.section	.rodata._ZN2v88internalL43Stats_Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC47:
	.string	"Embedded blob is %d bytes\n"
	.section	.text._ZN2v88internalL43Stats_Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Stats_Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL43Stats_Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateE.isra.0:
.LFB27929:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1022
.L988:
	movq	_ZZN2v88internalL43Stats_Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateEE28trace_event_unique_atomic843(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1023
.L990:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1024
.L992:
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	call	_ZN2v88internal30DisableEmbeddedBlobRefcountingEv@PLT
	xorl	%edi, %edi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal30CreateSnapshotDataBlobInternalENS_15SnapshotCreator20FunctionCodeHandlingEPKcPNS_7IsolateE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L996
	call	_ZdaPv@PLT
.L996:
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %r14d
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	%r14d, %esi
	leaq	.LC47(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	call	_ZN2v88internal23FreeCurrentEmbeddedBlobEv@PLT
	movq	%r13, 41088(%r12)
	movq	88(%r12), %r14
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L999
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L999:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1025
.L987:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1026
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1024:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1027
.L993:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L994
	movq	(%rdi), %rax
	call	*8(%rax)
.L994:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L995
	movq	(%rdi), %rax
	call	*8(%rax)
.L995:
	leaq	.LC46(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L1023:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1028
.L991:
	movq	%rbx, _ZZN2v88internalL43Stats_Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateEE28trace_event_unique_atomic843(%rip)
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L1022:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$288, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1025:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L1028:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L1027:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC46(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L993
.L1026:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27929:
	.size	_ZN2v88internalL43Stats_Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL43Stats_Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL34Stats_Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC48:
	.string	"V8.Runtime_Runtime_DebugBreakOnBytecode"
	.section	.text._ZN2v88internalL34Stats_Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL34Stats_Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE.isra.0:
.LFB27930:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1608, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1568(%rbp)
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1085
.L1030:
	movq	_ZZN2v88internalL34Stats_Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateEE27trace_event_unique_atomic36(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1086
.L1032:
	movq	$0, -1600(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1087
.L1034:
	addl	$1, 41104(%r15)
	movq	41088(%r15), %rax
	leaq	-1616(%rbp), %r13
	movq	41472(%r15), %rsi
	movq	%r13, %rdi
	movq	41096(%r15), %rbx
	movq	%rax, -1624(%rbp)
	call	_ZN2v88internal16ReturnValueScopeC1EPNS0_5DebugE@PLT
	movq	(%r12), %rdx
	movq	41472(%r15), %rax
	movq	%r15, %rsi
	leaq	-1520(%rbp), %r12
	movq	%rdx, 104(%rax)
	movq	%r12, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -104(%rbp)
	je	.L1038
	movq	%r12, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
.L1038:
	movl	41828(%r15), %eax
	movq	41472(%r15), %r12
	testl	%eax, %eax
	je	.L1088
.L1039:
	cmpq	$0, 120(%r12)
	jne	.L1089
	movq	-104(%rbp), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*152(%rax)
	movq	23(%rax), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L1090
.L1045:
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L1091
.L1047:
	movq	7(%rax), %rax
	movq	7(%rax), %r9
.L1046:
	movq	%r12, %rdi
	movq	%r9, -1632(%rbp)
	call	_ZNK2v88internal16InterpretedFrame17GetBytecodeOffsetEv@PLT
	movq	-1632(%rbp), %r9
	addl	$54, %eax
	cmpl	$32, 41828(%r15)
	cltq
	movzbl	-1(%r9,%rax), %r14d
	je	.L1092
	cmpb	$-80, %r14b
	je	.L1060
	cmpb	$-85, %r14b
	je	.L1060
	movq	41504(%r15), %rdi
	movl	$1, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter11Interpreter18GetBytecodeHandlerENS1_8BytecodeENS1_12OperandScaleE@PLT
	movzbl	%r14b, %edx
.L1051:
	leaq	37512(%r15), %rdi
	movq	%rdx, -1632(%rbp)
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	movq	-1632(%rbp), %rdx
	movq	%rax, %r12
	salq	$32, %rdx
	movq	%rdx, %r14
	cmpq	%rax, 312(%r15)
	jne	.L1093
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	%r13, %rdi
	call	_ZN2v88internal16ReturnValueScopeD1Ev@PLT
	movq	-1624(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %rbx
	je	.L1054
	movq	%rbx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1054:
	leaq	-1600(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1094
.L1052:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1095
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	movq	%r14, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1090:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L1045
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L1045
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L1045
	movq	31(%rdx), %r9
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1089:
	movabsq	$781684047872, %r14
	movq	88(%r15), %r12
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1088:
	movq	-104(%rbp), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1040
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1041:
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug5BreakEPNS0_15JavaScriptFrameENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	41472(%r15), %r12
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1087:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1096
.L1035:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1036
	movq	(%rdi), %rax
	call	*8(%rax)
.L1036:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1037
	movq	(%rdi), %rax
	call	*8(%rax)
.L1037:
	leaq	.LC48(%rip), %rax
	movq	%rbx, -1592(%rbp)
	movq	%rax, -1584(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%r13, -1576(%rbp)
	movq	%rax, -1600(%rbp)
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1086:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1097
.L1033:
	movq	%rbx, _ZZN2v88internalL34Stats_Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateEE27trace_event_unique_atomic36(%rip)
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1093:
	movq	41472(%r15), %rax
	movq	104(%rax), %r12
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1040:
	movq	41088(%r15), %rdx
	cmpq	41096(%r15), %rdx
	je	.L1098
.L1042:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdx)
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1060:
	movb	$0, -1632(%rbp)
.L1055:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal16InterpretedFrame18PatchBytecodeArrayENS0_13BytecodeArrayE@PLT
.L1049:
	movq	41504(%r15), %rdi
	movl	$1, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter11Interpreter18GetBytecodeHandlerENS1_8BytecodeENS1_12OperandScaleE@PLT
	movzbl	%r14b, %edx
	movq	%rdx, %r14
	salq	$32, %r14
	cmpb	$0, -1632(%rbp)
	je	.L1051
	movq	312(%r15), %r12
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1091:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L1047
	movq	7(%rax), %r9
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1092:
	movq	41472(%r15), %rdi
	movq	%r12, %rsi
	movq	%r9, -1640(%rbp)
	call	_ZN2v88internal5Debug32PerformSideEffectCheckAtBytecodeEPNS0_16InterpretedFrameE@PLT
	movq	-1640(%rbp), %r9
	xorl	$1, %eax
	cmpb	$-85, %r14b
	movb	%al, -1632(%rbp)
	je	.L1055
	cmpb	$-80, %r14b
	jne	.L1049
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1085:
	movq	40960(%rsi), %rax
	movl	$202, %edx
	leaq	-1560(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -1568(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1094:
	leaq	-1560(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1096:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC48(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1097:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1098:
	movq	%r15, %rdi
	movq	%rax, -1632(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1632(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1042
.L1095:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27930:
	.size	_ZN2v88internalL34Stats_Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL34Stats_Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL33Stats_Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC49:
	.string	"V8.Runtime_Runtime_DebugOnFunctionCall"
	.section	.text._ZN2v88internalL33Stats_Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateE, @function
_ZN2v88internalL33Stats_Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateE:
.LFB21686:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1136
.L1100:
	movq	_ZZN2v88internalL33Stats_Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic617(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1137
.L1102:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1138
.L1104:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r15
	addl	$1, 41104(%r12)
	movq	0(%r13), %rdi
	testb	$1, %dil
	jne	.L1108
.L1109:
	leaq	.LC32(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1137:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1139
.L1103:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic617(%rip)
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1108:
	movq	-1(%rdi), %rax
	cmpw	$1105, 11(%rax)
	jne	.L1109
	movq	41472(%r12), %rax
	leaq	-8(%r13), %r14
	cmpb	$0, 9(%rax)
	je	.L1110
	xorl	%esi, %esi
	call	_ZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeE@PLT
	movq	41472(%r12), %rdi
	cmpb	$1, 76(%rdi)
	jle	.L1140
.L1111:
	movq	%r13, %rsi
	call	_ZN2v88internal5Debug13PrepareStepInENS0_6HandleINS0_10JSFunctionEEE@PLT
.L1112:
	cmpl	$32, 41828(%r12)
	je	.L1141
.L1110:
	movq	88(%r12), %r13
.L1113:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L1116
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1116:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1142
.L1099:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1143
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1138:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1144
.L1105:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1106
	movq	(%rdi), %rax
	call	*8(%rax)
.L1106:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1107
	movq	(%rdi), %rax
	call	*8(%rax)
.L1107:
	leaq	.LC49(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1140:
	cmpb	$0, 132(%rdi)
	je	.L1112
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1136:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$274, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1141:
	movq	41472(%r12), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal5Debug22PerformSideEffectCheckENS0_6HandleINS0_10JSFunctionEEENS2_INS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L1110
	movq	312(%r12), %r13
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1142:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1139:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1103
	.p2align 4,,10
	.p2align 3
.L1144:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC49(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1105
.L1143:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21686:
	.size	_ZN2v88internalL33Stats_Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateE, .-_ZN2v88internalL33Stats_Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL45Stats_Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC50:
	.string	"V8.Runtime_Runtime_PerformSideEffectCheckForObject"
	.section	.rodata._ZN2v88internalL45Stats_Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC51:
	.string	"args[0].IsJSReceiver()"
	.section	.text._ZN2v88internalL45Stats_Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Stats_Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateE, @function
_ZN2v88internalL45Stats_Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateE:
.LFB21756:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1180
.L1146:
	movq	_ZZN2v88internalL45Stats_Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic830(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1181
.L1148:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1182
.L1150:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1154
.L1155:
	leaq	.LC51(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1181:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1183
.L1149:
	movq	%rbx, _ZZN2v88internalL45Stats_Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic830(%rip)
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1154:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1155
	movq	41472(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal5Debug31PerformSideEffectCheckForObjectENS0_6HandleINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L1156
	movq	312(%r12), %r13
.L1157:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1160
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1160:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1184
.L1145:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1185
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1156:
	.cfi_restore_state
	movq	88(%r12), %r13
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1182:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1186
.L1151:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1152
	movq	(%rdi), %rax
	call	*8(%rax)
.L1152:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1153
	movq	(%rdi), %rax
	call	*8(%rax)
.L1153:
	leaq	.LC50(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1180:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$451, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1146
	.p2align 4,,10
	.p2align 3
.L1184:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1145
	.p2align 4,,10
	.p2align 3
.L1183:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1149
	.p2align 4,,10
	.p2align 3
.L1186:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC50(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1151
.L1185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21756:
	.size	_ZN2v88internalL45Stats_Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateE, .-_ZN2v88internalL45Stats_Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Stats_Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC52:
	.string	"V8.Runtime_Runtime_DebugTogglePreciseCoverage"
	.section	.rodata._ZN2v88internalL40Stats_Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC53:
	.string	"args[0].IsBoolean()"
	.section	.text._ZN2v88internalL40Stats_Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL40Stats_Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateE.isra.0:
.LFB27931:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1218
.L1188:
	movq	_ZZN2v88internalL40Stats_Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateEE28trace_event_unique_atomic731(%rip), %r12
	testq	%r12, %r12
	je	.L1219
.L1190:
	movq	$0, -144(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L1220
.L1192:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1221
.L1197:
	leaq	.LC53(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1219:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1222
.L1191:
	movq	%r12, _ZZN2v88internalL40Stats_Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateEE28trace_event_unique_atomic731(%rip)
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1221:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L1197
	testb	$-2, 43(%rax)
	jne	.L1197
	xorl	%esi, %esi
	cmpq	%rax, 112(%rbx)
	movq	%rbx, %rdi
	sete	%sil
	call	_ZN2v88internal8Coverage10SelectModeEPNS0_7IsolateENS_5debug12CoverageModeE@PLT
	leaq	-144(%rbp), %rdi
	movq	88(%rbx), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1223
.L1187:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1224
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1220:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1225
.L1193:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1194
	movq	(%rdi), %rax
	call	*8(%rax)
.L1194:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1195
	movq	(%rdi), %rax
	call	*8(%rax)
.L1195:
	leaq	.LC52(%rip), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1192
	.p2align 4,,10
	.p2align 3
.L1218:
	movq	40960(%rsi), %rax
	movl	$279, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1223:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1222:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1191
	.p2align 4,,10
	.p2align 3
.L1225:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC52(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1193
.L1224:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27931:
	.size	_ZN2v88internalL40Stats_Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL40Stats_Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL38Stats_Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC54:
	.string	"V8.Runtime_Runtime_DebugToggleBlockCoverage"
	.section	.text._ZN2v88internalL38Stats_Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Stats_Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL38Stats_Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateE.isra.0:
.LFB27932:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1263
.L1227:
	movq	_ZZN2v88internalL38Stats_Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateEE28trace_event_unique_atomic739(%rip), %r12
	testq	%r12, %r12
	je	.L1264
.L1229:
	movq	$0, -144(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L1265
.L1231:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1266
.L1236:
	leaq	.LC53(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1264:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1267
.L1230:
	movq	%r12, _ZZN2v88internalL38Stats_Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateEE28trace_event_unique_atomic739(%rip)
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1266:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L1236
	testb	$-2, 43(%rax)
	jne	.L1236
	xorl	%esi, %esi
	cmpq	112(%rbx), %rax
	movq	%rbx, %rdi
	sete	%sil
	leal	(%rsi,%rsi,2), %esi
	call	_ZN2v88internal8Coverage10SelectModeEPNS0_7IsolateENS_5debug12CoverageModeE@PLT
	leaq	-144(%rbp), %rdi
	movq	88(%rbx), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1268
.L1226:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1269
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1265:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1270
.L1232:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1233
	movq	(%rdi), %rax
	call	*8(%rax)
.L1233:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1234
	movq	(%rdi), %rax
	call	*8(%rax)
.L1234:
	leaq	.LC54(%rip), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1263:
	movq	40960(%rsi), %rax
	movl	$278, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1227
	.p2align 4,,10
	.p2align 3
.L1268:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1267:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1270:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC54(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1232
.L1269:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27932:
	.size	_ZN2v88internalL38Stats_Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL38Stats_Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL33Stats_Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC55:
	.string	"V8.Runtime_Runtime_LiveEditPatchScript"
	.section	.rodata._ZN2v88internalL33Stats_Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC56:
	.string	"args[1].IsString()"
	.section	.rodata._ZN2v88internalL33Stats_Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE.str1.8
	.align 8
.LC57:
	.string	"LiveEdit failed: COMPILE_ERROR"
	.align 8
.LC58:
	.string	"LiveEdit failed: BLOCKED_BY_RUNNING_GENERATOR"
	.align 8
.LC59:
	.string	"LiveEdit failed: BLOCKED_BY_FUNCTION_ABOVE_BREAK_FRAME"
	.align 8
.LC60:
	.string	"LiveEdit failed: BLOCKED_BY_FUNCTION_BELOW_NON_DROPPABLE_FRAME"
	.align 8
.LC61:
	.string	"LiveEdit failed: BLOCKED_BY_ACTIVE_FUNCTION"
	.align 8
.LC62:
	.string	"LiveEdit failed: BLOCKED_BY_NEW_TARGET_IN_RESTART_FRAME"
	.align 8
.LC63:
	.string	"LiveEdit failed: FRAME_RESTART_IS_NOT_SUPPORTED"
	.section	.text._ZN2v88internalL33Stats_Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE, @function
_ZN2v88internalL33Stats_Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE:
.LFB21750:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$280, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1342
.L1272:
	movq	_ZZN2v88internalL33Stats_Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateEE28trace_event_unique_atomic791(%rip), %r13
	testq	%r13, %r13
	je	.L1343
.L1274:
	movq	$0, -192(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L1344
.L1276:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L1280
.L1281:
	leaq	.LC32(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1343:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1345
.L1275:
	movq	%r13, _ZZN2v88internalL33Stats_Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateEE28trace_event_unique_atomic791(%rip)
	jmp	.L1274
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1281
	movq	-8(%rbx), %rdx
	leaq	-8(%rbx), %r15
	testb	$1, %dl
	jne	.L1346
.L1282:
	leaq	.LC56(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1344:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1347
.L1277:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1278
	movq	(%rdi), %rax
	call	*8(%rax)
.L1278:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1279
	movq	(%rdi), %rax
	call	*8(%rax)
.L1279:
	leaq	.LC55(%rip), %rax
	movq	%r13, -184(%rbp)
	movq	%rax, -176(%rbp)
	leaq	-184(%rbp), %rax
	movq	%r14, -168(%rbp)
	movq	%rax, -192(%rbp)
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1346:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1282
	movq	23(%rax), %rax
	movq	31(%rax), %r8
	testb	$1, %r8b
	jne	.L1348
.L1284:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1285
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1286:
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-160(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$0, -160(%rbp)
	movb	$0, -156(%rbp)
	movq	$-1, -136(%rbp)
	movups	%xmm0, -152(%rbp)
	call	_ZN2v88internal8LiveEdit11PatchScriptEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6StringEEEbPNS_5debug14LiveEditResultE@PLT
	cmpl	$7, -160(%rbp)
	ja	.L1288
	movl	-160(%rbp), %eax
	leaq	.L1290(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internalL33Stats_Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE,"a",@progbits
	.align 4
	.align 4
.L1290:
	.long	.L1288-.L1290
	.long	.L1296-.L1290
	.long	.L1295-.L1290
	.long	.L1294-.L1290
	.long	.L1293-.L1290
	.long	.L1292-.L1290
	.long	.L1291-.L1290
	.long	.L1289-.L1290
	.section	.text._ZN2v88internalL33Stats_Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1285:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1349
.L1287:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1348:
	movq	-1(%r8), %rax
	cmpw	$86, 11(%rax)
	jne	.L1284
	movq	23(%r8), %r8
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1289:
	leaq	.LC63(%rip), %rax
	leaq	-208(%rbp), %rsi
	movq	$47, -200(%rbp)
	movq	%rax, -208(%rbp)
	.p2align 4,,10
	.p2align 3
.L1341:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1350
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r15
.L1298:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1302
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1302:
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1351
.L1271:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1352
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1291:
	.cfi_restore_state
	leaq	.LC62(%rip), %rax
	leaq	-224(%rbp), %rsi
	movq	$55, -216(%rbp)
	movq	%rax, -224(%rbp)
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1293:
	leaq	.LC60(%rip), %rax
	leaq	-256(%rbp), %rsi
	movq	$62, -248(%rbp)
	movq	%rax, -256(%rbp)
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1294:
	leaq	.LC59(%rip), %rax
	leaq	-272(%rbp), %rsi
	movq	$54, -264(%rbp)
	movq	%rax, -272(%rbp)
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1292:
	leaq	.LC61(%rip), %rax
	leaq	-240(%rbp), %rsi
	movq	$43, -232(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1295:
	leaq	.LC58(%rip), %rax
	leaq	-288(%rbp), %rsi
	movq	$45, -280(%rbp)
	movq	%rax, -288(%rbp)
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1296:
	leaq	.LC57(%rip), %rax
	leaq	-304(%rbp), %rsi
	movq	$30, -296(%rbp)
	movq	%rax, -304(%rbp)
	jmp	.L1341
.L1288:
	movq	88(%r12), %r15
	jmp	.L1298
	.p2align 4,,10
	.p2align 3
.L1342:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$287, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1272
	.p2align 4,,10
	.p2align 3
.L1351:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1271
	.p2align 4,,10
	.p2align 3
.L1345:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1347:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC55(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1349:
	movq	%r12, %rdi
	movq	%r8, -312(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1350:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1352:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21750:
	.size	_ZN2v88internalL33Stats_Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE, .-_ZN2v88internalL33Stats_Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Stats_Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC64:
	.string	"V8.Runtime_Runtime_FunctionGetInferredName"
	.section	.text._ZN2v88internalL37Stats_Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL37Stats_Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateE.isra.0:
.LFB27933:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1395
.L1354:
	movq	_ZZN2v88internalL37Stats_Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic454(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1396
.L1356:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1397
.L1358:
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L1362
.L1364:
	movq	128(%r13), %r12
.L1363:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1398
.L1353:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1399
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1396:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1400
.L1357:
	movq	%rbx, _ZZN2v88internalL37Stats_Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic454(%rip)
	jmp	.L1356
	.p2align 4,,10
	.p2align 3
.L1362:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1364
	movq	23(%rax), %rbx
	movq	15(%rbx), %rax
	testb	$1, %al
	jne	.L1401
.L1365:
	movq	7(%rbx), %rax
	testb	$1, %al
	jne	.L1402
.L1368:
	andq	$-262144, %rbx
	movq	24(%rbx), %rax
	movq	-37464(%rax), %r12
	jmp	.L1363
	.p2align 4,,10
	.p2align 3
.L1397:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1403
.L1359:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1360
	movq	(%rdi), %rax
	call	*8(%rax)
.L1360:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1361
	movq	(%rdi), %rax
	call	*8(%rax)
.L1361:
	leaq	.LC64(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1358
	.p2align 4,,10
	.p2align 3
.L1398:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1395:
	movq	40960(%rsi), %rax
	movl	$280, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1354
	.p2align 4,,10
	.p2align 3
.L1403:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC64(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1359
	.p2align 4,,10
	.p2align 3
.L1400:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1401:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L1365
	leaq	-152(%rbp), %r12
	movq	%rax, -152(%rbp)
	movq	%r12, %rdi
	call	_ZNK2v88internal9ScopeInfo23HasInferredFunctionNameEv@PLT
	testb	%al, %al
	je	.L1368
	movq	%r12, %rdi
	call	_ZNK2v88internal9ScopeInfo20InferredFunctionNameEv@PLT
	movq	%rax, %r12
	testb	$1, %al
	je	.L1368
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1368
	jmp	.L1363
	.p2align 4,,10
	.p2align 3
.L1402:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L1371
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L1368
.L1371:
	movq	7(%rbx), %rax
	movq	7(%rax), %r12
	jmp	.L1363
.L1399:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27933:
	.size	_ZN2v88internalL37Stats_Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL37Stats_Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL37Stats_Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC65:
	.string	"V8.Runtime_Runtime_DebugGetLoadedScriptIds"
	.section	.text._ZN2v88internalL37Stats_Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL37Stats_Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateE.isra.0:
.LFB27958:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -208(%rbp)
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1440
.L1405:
	movq	_ZZN2v88internalL37Stats_Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateEE28trace_event_unique_atomic432(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1441
.L1407:
	movq	$0, -240(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1442
.L1409:
	movq	41088(%r12), %rax
	movq	41472(%r12), %rsi
	leaq	-160(%rbp), %r14
	addl	$1, 41104(%r12)
	movq	%r14, %rdi
	movq	%rax, -248(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal10DebugScopeC1EPNS0_5DebugE@PLT
	movq	41472(%r12), %rdi
	call	_ZN2v88internal5Debug16GetLoadedScriptsEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal10DebugScopeD1Ev@PLT
	movq	(%r15), %rax
	movslq	11(%rax), %rdx
	movl	%edx, %r9d
	testq	%rdx, %rdx
	jle	.L1413
	movl	$15, %ebx
	xorl	%r14d, %r14d
	jmp	.L1417
	.p2align 4,,10
	.p2align 3
.L1443:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1415:
	movslq	67(%rsi), %rax
	movq	(%r15), %rdi
	addl	$1, %r14d
	addq	$8, %rbx
	salq	$32, %rax
	movq	%rax, -1(%r13,%rdi)
	movq	(%r15), %rax
	movslq	11(%rax), %rcx
	movl	%ecx, %r9d
	cmpl	%ecx, %r14d
	jge	.L1413
.L1417:
	movq	(%rax,%rbx), %rsi
	movq	41112(%r12), %rdi
	leaq	1(%rbx), %r13
	testq	%rdi, %rdi
	jne	.L1443
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1444
.L1416:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1413:
	movq	%r15, %rsi
	xorl	%r8d, %r8d
	movl	%r9d, %ecx
	movl	$3, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r15
	movq	-248(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	movq	-256(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L1418
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1418:
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1445
.L1404:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1446
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1444:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1441:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1447
.L1408:
	movq	%rbx, _ZZN2v88internalL37Stats_Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateEE28trace_event_unique_atomic432(%rip)
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1442:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1448
.L1410:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1411
	movq	(%rdi), %rax
	call	*8(%rax)
.L1411:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1412
	movq	(%rdi), %rax
	call	*8(%rax)
.L1412:
	leaq	.LC65(%rip), %rax
	movq	%rbx, -232(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%r13, -216(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L1409
	.p2align 4,,10
	.p2align 3
.L1445:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1440:
	movq	40960(%rdi), %rax
	leaq	-200(%rbp), %rsi
	movl	$273, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1405
.L1448:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC65(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1410
.L1447:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1408
.L1446:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27958:
	.size	_ZN2v88internalL37Stats_Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL37Stats_Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL40Stats_Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC66:
	.string	"V8.Runtime_Runtime_DebugAsyncFunctionFinished"
	.section	.rodata._ZN2v88internalL40Stats_Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC67:
	.string	"args[1].IsJSPromise()"
	.section	.text._ZN2v88internalL40Stats_Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL40Stats_Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateE.isra.0:
.LFB27998:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1486
.L1450:
	movq	_ZZN2v88internalL40Stats_Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateEE28trace_event_unique_atomic778(%rip), %r12
	testq	%r12, %r12
	je	.L1487
.L1452:
	movq	$0, -160(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L1488
.L1454:
	addl	$1, 41104(%r15)
	movq	(%rbx), %r14
	movq	41088(%r15), %r13
	movq	41096(%r15), %r12
	testb	$1, %r14b
	jne	.L1489
.L1458:
	leaq	.LC53(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1487:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1490
.L1453:
	movq	%r12, _ZZN2v88internalL40Stats_Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateEE28trace_event_unique_atomic778(%rip)
	jmp	.L1452
	.p2align 4,,10
	.p2align 3
.L1489:
	movq	-1(%r14), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L1458
	testb	$-2, 43(%r14)
	jne	.L1458
	movq	-8(%rbx), %rdx
	testb	$1, %dl
	jne	.L1491
.L1459:
	leaq	.LC67(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1488:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1492
.L1455:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1456
	movq	(%rdi), %rax
	call	*8(%rax)
.L1456:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1457
	movq	(%rdi), %rax
	call	*8(%rax)
.L1457:
	leaq	.LC66(%rip), %rax
	movq	%r12, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1454
	.p2align 4,,10
	.p2align 3
.L1491:
	movq	112(%r15), %rax
	movq	%rax, -168(%rbp)
	movq	-1(%rdx), %rdx
	cmpw	$1074, 11(%rdx)
	jne	.L1459
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate10PopPromiseEv@PLT
	cmpq	-168(%rbp), %r14
	je	.L1493
.L1462:
	movq	-8(%rbx), %r14
	subl	$1, 41104(%r15)
	movq	%r13, 41088(%r15)
	cmpq	41096(%r15), %r12
	je	.L1465
	movq	%r12, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1465:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1494
.L1449:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1495
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1493:
	.cfi_restore_state
	movl	$6, %edx
	leaq	-8(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate27OnAsyncFunctionStateChangedENS0_6HandleINS0_9JSPromiseEEENS_5debug20DebugAsyncActionTypeE@PLT
	jmp	.L1462
	.p2align 4,,10
	.p2align 3
.L1486:
	movq	40960(%rsi), %rax
	movl	$270, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1450
	.p2align 4,,10
	.p2align 3
.L1494:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1449
	.p2align 4,,10
	.p2align 3
.L1492:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC66(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1455
	.p2align 4,,10
	.p2align 3
.L1490:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1453
.L1495:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27998:
	.size	_ZN2v88internalL40Stats_Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL40Stats_Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL36Stats_Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC68:
	.string	"V8.Runtime_Runtime_GetGeneratorScopeCount"
	.section	.text._ZN2v88internalL36Stats_Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL36Stats_Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateE.isra.0:
.LFB28009:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -224(%rbp)
	movq	$0, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1538
.L1497:
	movq	_ZZN2v88internalL36Stats_Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateEE28trace_event_unique_atomic307(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1539
.L1499:
	movq	$0, -256(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1540
.L1501:
	movl	41104(%r12), %eax
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L1541
	movl	%eax, 41104(%r12)
	xorl	%r13d, %r13d
.L1516:
	leaq	-256(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1542
.L1496:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1543
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1539:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1544
.L1500:
	movq	%rbx, _ZZN2v88internalL36Stats_Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateEE28trace_event_unique_atomic307(%rip)
	jmp	.L1499
	.p2align 4,,10
	.p2align 3
.L1541:
	movq	-1(%rdx), %rcx
	cmpw	$1068, 11(%rcx)
	jne	.L1545
.L1506:
	testb	$1, %dl
	jne	.L1508
.L1510:
	leaq	.LC22(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1540:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1546
.L1502:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1503
	movq	(%rdi), %rax
	call	*8(%rax)
.L1503:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1504
	movq	(%rdi), %rax
	call	*8(%rax)
.L1504:
	leaq	.LC68(%rip), %rax
	movq	%rbx, -248(%rbp)
	movq	%rax, -240(%rbp)
	leaq	-248(%rbp), %rax
	movq	%r14, -232(%rbp)
	movq	%rax, -256(%rbp)
	jmp	.L1501
	.p2align 4,,10
	.p2align 3
.L1545:
	movq	-1(%rdx), %rcx
	cmpw	$1063, 11(%rcx)
	je	.L1506
	movq	-1(%rdx), %rcx
	cmpw	$1064, 11(%rcx)
	je	.L1506
	movq	%r14, %rdx
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1507:
	movq	%rbx, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %r14
	je	.L1516
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L1516
	.p2align 4,,10
	.p2align 3
.L1508:
	movq	-1(%rdx), %rax
	cmpw	$1068, 11(%rax)
	jne	.L1547
.L1509:
	testb	$-128, 70(%rdx)
	je	.L1511
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	xorl	%r13d, %r13d
	subl	$1, %eax
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1538:
	movq	40960(%rsi), %rax
	movl	$282, %edx
	leaq	-216(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -224(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1497
	.p2align 4,,10
	.p2align 3
.L1542:
	leaq	-216(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1496
	.p2align 4,,10
	.p2align 3
.L1511:
	leaq	-176(%rbp), %r15
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal13ScopeIteratorC1EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE@PLT
	cmpq	$0, -136(%rbp)
	je	.L1520
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1513:
	movq	%r15, %rdi
	addl	$1, %r13d
	call	_ZN2v88internal13ScopeIterator4NextEv@PLT
	cmpq	$0, -136(%rbp)
	jne	.L1513
	salq	$32, %r13
.L1512:
	movq	%r15, %rdi
	call	_ZN2v88internal13ScopeIteratorD1Ev@PLT
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1546:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC68(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1544:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1500
	.p2align 4,,10
	.p2align 3
.L1547:
	movq	-1(%rdx), %rax
	cmpw	$1063, 11(%rax)
	je	.L1509
	movq	-1(%rdx), %rax
	cmpw	$1064, 11(%rax)
	jne	.L1510
	jmp	.L1509
.L1520:
	xorl	%r13d, %r13d
	jmp	.L1512
.L1543:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28009:
	.size	_ZN2v88internalL36Stats_Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL36Stats_Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL38Stats_Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC69:
	.string	"V8.Runtime_Runtime_GetGeneratorScopeDetails"
	.section	.text._ZN2v88internalL38Stats_Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Stats_Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL38Stats_Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateE.isra.0:
.LFB28010:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -224(%rbp)
	movq	$0, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1613
.L1549:
	movq	_ZZN2v88internalL38Stats_Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateEE28trace_event_unique_atomic330(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1614
.L1551:
	movq	$0, -256(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1615
.L1553:
	movl	41104(%r12), %eax
	movq	41096(%r12), %r15
	movq	41088(%r12), %rbx
	leal	1(%rax), %edx
	movq	%r15, %rcx
	movl	%edx, 41104(%r12)
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L1616
.L1557:
	movq	88(%r12), %r13
.L1559:
	movq	%rbx, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rcx, %r15
	je	.L1582
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1582:
	leaq	-256(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1617
.L1548:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1618
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1616:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	cmpw	$1068, 11(%rcx)
	jne	.L1619
.L1558:
	testb	$1, %dl
	jne	.L1560
.L1562:
	leaq	.LC22(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1615:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1620
.L1554:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1555
	movq	(%rdi), %rax
	call	*8(%rax)
.L1555:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1556
	movq	(%rdi), %rax
	call	*8(%rax)
.L1556:
	leaq	.LC69(%rip), %rax
	movq	%rbx, -248(%rbp)
	movq	%rax, -240(%rbp)
	leaq	-248(%rbp), %rax
	movq	%r14, -232(%rbp)
	movq	%rax, -256(%rbp)
	jmp	.L1553
	.p2align 4,,10
	.p2align 3
.L1614:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1621
.L1552:
	movq	%rbx, _ZZN2v88internalL38Stats_Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateEE28trace_event_unique_atomic330(%rip)
	jmp	.L1551
	.p2align 4,,10
	.p2align 3
.L1619:
	movq	-1(%rdx), %rcx
	cmpw	$1063, 11(%rcx)
	je	.L1558
	movq	-1(%rdx), %rcx
	cmpw	$1064, 11(%rcx)
	je	.L1558
	movq	%r15, %rcx
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1560:
	movq	-1(%rdx), %rax
	cmpw	$1068, 11(%rax)
	jne	.L1622
.L1561:
	movq	-8(%r13), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	movq	%rcx, -264(%rbp)
	testb	$1, %al
	je	.L1566
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L1623
	movq	7(%rax), %rax
	movsd	.LC16(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC15(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L1567
	movsd	.LC17(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L1567
	comisd	.LC18(%rip), %xmm0
	jb	.L1567
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	movl	%esi, -264(%rbp)
	ucomisd	%xmm1, %xmm0
	jp	.L1567
	jne	.L1567
	.p2align 4,,10
	.p2align 3
.L1566:
	testb	$-128, 70(%rdx)
	je	.L1574
	movl	41104(%r12), %eax
	movq	88(%r12), %r13
	movq	41096(%r12), %rcx
	subl	$1, %eax
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1567:
	movabsq	$9218868437227405312, %rcx
	testq	%rcx, %rax
	je	.L1587
	movl	$0, -264(%rbp)
	movq	%rax, %rsi
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L1624
	cmpl	$31, %ecx
	jg	.L1566
	movabsq	$4503599627370495, %r14
	movabsq	$4503599627370496, %rsi
	andq	%rax, %r14
	addq	%rsi, %r14
	salq	%cl, %r14
	movl	%r14d, %r14d
.L1572:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%r14d, %eax
	movl	%eax, -264(%rbp)
	jmp	.L1566
	.p2align 4,,10
	.p2align 3
.L1574:
	leaq	-176(%rbp), %r14
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13ScopeIteratorC1EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE@PLT
	cmpq	$0, -136(%rbp)
	je	.L1575
	movl	-264(%rbp), %eax
	xorl	%r13d, %r13d
	testl	%eax, %eax
	jg	.L1576
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1611:
	cmpl	%r13d, -264(%rbp)
	je	.L1577
.L1576:
	movq	%r14, %rdi
	addl	$1, %r13d
	call	_ZN2v88internal13ScopeIterator4NextEv@PLT
	cmpq	$0, -136(%rbp)
	jne	.L1611
.L1575:
	movq	88(%r12), %r13
.L1578:
	movq	%r14, %rdi
	call	_ZN2v88internal13ScopeIteratorD1Ev@PLT
	movl	41104(%r12), %eax
	movq	41096(%r12), %rcx
	subl	$1, %eax
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1613:
	movq	40960(%rsi), %rax
	movl	$283, %edx
	leaq	-216(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -224(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1549
	.p2align 4,,10
	.p2align 3
.L1617:
	leaq	-216(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1548
	.p2align 4,,10
	.p2align 3
.L1621:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1552
	.p2align 4,,10
	.p2align 3
.L1620:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC69(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1577:
	movq	%r14, %rdi
	call	_ZN2v88internal13ScopeIterator23MaterializeScopeDetailsEv@PLT
	movq	(%rax), %r13
	jmp	.L1578
	.p2align 4,,10
	.p2align 3
.L1622:
	movq	-1(%rdx), %rax
	cmpw	$1063, 11(%rax)
	je	.L1561
	movq	-1(%rdx), %rax
	cmpw	$1064, 11(%rax)
	jne	.L1562
	jmp	.L1561
	.p2align 4,,10
	.p2align 3
.L1624:
	cmpl	$-52, %ecx
	jl	.L1566
	movabsq	$4503599627370495, %r14
	movabsq	$4503599627370496, %rcx
	andq	%rax, %r14
	addq	%rcx, %r14
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %r14
	jmp	.L1572
	.p2align 4,,10
	.p2align 3
.L1623:
	leaq	.LC23(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1587:
	movl	$0, -264(%rbp)
	jmp	.L1566
.L1618:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28010:
	.size	_ZN2v88internalL38Stats_Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL38Stats_Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal28Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE:
.LFB21628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$1512, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1656
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	leaq	-1520(%rbp), %r13
	movq	41472(%rdx), %rsi
	movq	%r13, %rdi
	movq	%rax, -1536(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -1528(%rbp)
	call	_ZN2v88internal16ReturnValueScopeC1EPNS0_5DebugE@PLT
	movq	(%r12), %rdx
	movq	41472(%r14), %rax
	movq	%r14, %rsi
	leaq	-1504(%rbp), %r12
	movq	%rdx, 104(%rax)
	movq	%r12, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -88(%rbp)
	je	.L1628
	movq	%r12, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
.L1628:
	movl	41828(%r14), %eax
	movq	41472(%r14), %r12
	testl	%eax, %eax
	je	.L1657
.L1629:
	cmpq	$0, 120(%r12)
	jne	.L1658
	movq	-88(%rbp), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*152(%rax)
	movq	23(%rax), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L1659
.L1635:
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L1660
.L1637:
	movq	7(%rax), %rax
	movq	7(%rax), %rbx
.L1636:
	movq	%r15, %rdi
	call	_ZNK2v88internal16InterpretedFrame17GetBytecodeOffsetEv@PLT
	addl	$54, %eax
	cltq
	movzbl	-1(%rbx,%rax), %r12d
	cmpb	$-85, %r12b
	sete	%dl
	cmpb	$-80, %r12b
	sete	%al
	orl	%eax, %edx
	cmpl	$32, 41828(%r14)
	je	.L1661
	testb	%dl, %dl
	jne	.L1662
	movq	41504(%r14), %rdi
	movl	$1, %edx
	movl	%r12d, %esi
	movzbl	%r12b, %r15d
	call	_ZN2v88internal11interpreter11Interpreter18GetBytecodeHandlerENS1_8BytecodeENS1_12OperandScaleE@PLT
.L1640:
	leaq	37512(%r14), %rdi
	salq	$32, %r15
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	movq	%r15, %r12
	movq	%rax, %rbx
	cmpq	%rax, 312(%r14)
	jne	.L1663
	.p2align 4,,10
	.p2align 3
.L1634:
	movq	%r13, %rdi
	call	_ZN2v88internal16ReturnValueScopeD1Ev@PLT
	movq	-1536(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-1528(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L1642
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1642:
	movq	%rbx, %rax
	movq	%r12, %rdx
.L1627:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1664
	addq	$1512, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1658:
	.cfi_restore_state
	movabsq	$781684047872, %r12
	movq	88(%r14), %rbx
	jmp	.L1634
	.p2align 4,,10
	.p2align 3
.L1657:
	movq	-88(%rbp), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1630
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1631:
	movq	-88(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug5BreakEPNS0_15JavaScriptFrameENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	41472(%r14), %r12
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1659:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L1635
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L1635
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L1635
	movq	31(%rdx), %rbx
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L1663:
	movq	41472(%r14), %rax
	movq	104(%rax), %rbx
	jmp	.L1634
	.p2align 4,,10
	.p2align 3
.L1630:
	movq	41088(%r14), %rdx
	cmpq	41096(%r14), %rdx
	je	.L1665
.L1632:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L1631
	.p2align 4,,10
	.p2align 3
.L1661:
	movq	41472(%r14), %rdi
	movq	%r15, %rsi
	movb	%dl, -1545(%rbp)
	call	_ZN2v88internal5Debug32PerformSideEffectCheckAtBytecodeEPNS0_16InterpretedFrameE@PLT
	movzbl	-1545(%rbp), %edx
	xorl	$1, %eax
	movb	%al, -1544(%rbp)
	testb	%dl, %dl
	jne	.L1643
.L1639:
	movq	41504(%r14), %rdi
	movzbl	%r12b, %r15d
	movl	%r12d, %esi
	movl	$1, %edx
	movq	%r15, %r12
	call	_ZN2v88internal11interpreter11Interpreter18GetBytecodeHandlerENS1_8BytecodeENS1_12OperandScaleE@PLT
	salq	$32, %r12
	cmpb	$0, -1544(%rbp)
	je	.L1640
	movq	312(%r14), %rbx
	jmp	.L1634
	.p2align 4,,10
	.p2align 3
.L1662:
	movb	$0, -1544(%rbp)
.L1643:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal16InterpretedFrame18PatchBytecodeArrayENS0_13BytecodeArrayE@PLT
	jmp	.L1639
	.p2align 4,,10
	.p2align 3
.L1660:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L1637
	movq	7(%rax), %rbx
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L1656:
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internalL34Stats_Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE.isra.0
	jmp	.L1627
	.p2align 4,,10
	.p2align 3
.L1665:
	movq	%r14, %rdi
	movq	%rax, -1544(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1544(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1632
.L1664:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21628:
	.size	_ZN2v88internal28Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateE:
.LFB21634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$1464, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1680
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r15
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1669
.L1670:
	leaq	.LC32(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1669:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L1670
	leaq	-1504(%rbp), %r14
	movq	%rdx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -88(%rbp)
	je	.L1671
	movq	%r14, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
.L1671:
	movq	%r14, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L1672
	movq	12528(%r12), %rax
	cmpq	%rax, 32(%rsi)
	jb	.L1681
.L1672:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r13
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1666
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1666:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1682
	addq	$1464, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1681:
	.cfi_restore_state
	movq	41472(%r12), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal5Debug5BreakEPNS0_15JavaScriptFrameENS0_6HandleINS0_10JSFunctionEEE@PLT
	jmp	.L1672
	.p2align 4,,10
	.p2align 3
.L1680:
	call	_ZN2v88internalL31Stats_Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L1666
.L1682:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21634:
	.size	_ZN2v88internal25Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateE:
.LFB21637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1687
	movq	41472(%rdx), %rdi
	cmpb	$0, 13(%rdi)
	jne	.L1688
.L1685:
	leaq	37512(%r12), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1688:
	.cfi_restore_state
	movl	$1, %esi
	call	_ZN2v88internal5Debug16HandleDebugBreakENS0_15IgnoreBreakModeE@PLT
	jmp	.L1685
	.p2align 4,,10
	.p2align 3
.L1687:
	addq	$8, %rsp
	movq	%rdx, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL37Stats_Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21637:
	.size	_ZN2v88internal31Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_ScheduleBreakEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_ScheduleBreakEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_ScheduleBreakEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_ScheduleBreakEiPmPNS0_7IsolateE:
.LFB21640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1692
	movq	%r12, %rdi
	xorl	%edx, %edx
	leaq	_ZZN2v88internalL31__RT_impl_Runtime_ScheduleBreakENS0_9ArgumentsEPNS0_7IsolateEENUlPNS_7IsolateEPvE_4_FUNES5_S6_(%rip), %rsi
	call	_ZN2v88internal7Isolate16RequestInterruptEPFvPNS_7IsolateEPvES4_@PLT
	movq	88(%r12), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1692:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rdx, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL27Stats_Runtime_ScheduleBreakEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21640:
	.size	_ZN2v88internal21Runtime_ScheduleBreakEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_ScheduleBreakEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internal7Runtime21GetInternalPropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC70:
	.string	"closed"
.LC71:
	.string	"suspended"
.LC72:
	.string	"running"
.LC73:
	.string	"[[TargetFunction]]"
.LC74:
	.string	"[[BoundThis]]"
.LC75:
	.string	"[[BoundArgs]]"
.LC76:
	.string	"[[GeneratorStatus]]"
.LC77:
	.string	"[[GeneratorFunction]]"
.LC78:
	.string	"[[GeneratorReceiver]]"
.LC79:
	.string	"[[PromiseStatus]]"
.LC80:
	.string	"[[PromiseValue]]"
.LC81:
	.string	"[[Handler]]"
.LC82:
	.string	"[[Target]]"
.LC83:
	.string	"[[IsRevoked]]"
.LC84:
	.string	"[[PrimitiveValue]]"
	.section	.text._ZN2v88internal7Runtime21GetInternalPropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Runtime21GetInternalPropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal7Runtime21GetInternalPropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal7Runtime21GetInternalPropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB21646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L2108
.L1799:
	xorl	%r9d, %r9d
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
.L1721:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2109
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2108:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movq	%rsi, %r13
	cmpw	$1104, 11(%rdx)
	je	.L1696
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	subw	$1070, %dx
	cmpw	$2, %dx
	ja	.L2110
	call	_ZN2v88internalL29GetIteratorInternalPropertiesINS0_13JSMapIteratorEEENS0_11MaybeHandleINS0_7JSArrayEEEPNS0_7IsolateENS0_6HandleIT_EE
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L2110:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	subw	$1078, %dx
	cmpw	$1, %dx
	ja	.L2111
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subw	$1070, %ax
	cmpw	$9, %ax
	ja	.L1728
	leaq	.L1730(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal7Runtime21GetInternalPropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"a",@progbits
	.align 4
	.align 4
.L1730:
	.long	.L1732-.L1730
	.long	.L1731-.L1730
	.long	.L1860-.L1730
	.long	.L1728-.L1730
	.long	.L1728-.L1730
	.long	.L1728-.L1730
	.long	.L1728-.L1730
	.long	.L1728-.L1730
	.long	.L1731-.L1730
	.long	.L1860-.L1730
	.section	.text._ZN2v88internal7Runtime21GetInternalPropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.p2align 4,,10
	.p2align 3
.L2111:
	movq	-1(%rax), %rdx
	cmpw	$1068, 11(%rdx)
	jne	.L2112
.L1756:
	movslq	67(%rax), %rax
	leaq	.LC70(%rip), %rbx
	cmpq	$-1, %rax
	je	.L1754
	cmpq	$-2, %rax
	leaq	.LC71(%rip), %rbx
	leaq	.LC72(%rip), %rax
	cmove	%rax, %rbx
.L1754:
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	xorl	%edx, %edx
	leaq	-192(%rbp), %rsi
	movq	%r12, %rdi
	movq	$19, -184(%rbp)
	movq	%rax, %r14
	leaq	.LC76(%rip), %rax
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1705
	movq	(%r14), %rdi
	movq	(%rax), %r15
	movq	%r15, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %r15b
	je	.L1846
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	je	.L1760
	movq	%r15, %rdx
	movq	%rsi, -376(%rbp)
	movq	%rdi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-376(%rbp), %rsi
	movq	-368(%rbp), %rdi
	movq	8(%rcx), %rax
.L1760:
	testb	$24, %al
	je	.L1846
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2113
	.p2align 4,,10
	.p2align 3
.L1846:
	movq	%rbx, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	-208(%rbp), %rsi
	movq	%rbx, -208(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1705
	movq	(%r14), %rbx
	movq	(%rax), %r15
	movq	%r15, 23(%rbx)
	leaq	23(%rbx), %rsi
	testb	$1, %r15b
	je	.L1845
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	jne	.L2114
.L1763:
	testb	$24, %al
	je	.L1845
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2115
	.p2align 4,,10
	.p2align 3
.L1845:
	leaq	.LC77(%rip), %rax
	xorl	%edx, %edx
	leaq	-224(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	movq	$21, -216(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1705
	movq	(%r14), %rbx
	movq	(%rax), %r15
	movq	%r15, 31(%rbx)
	leaq	31(%rbx), %rsi
	testb	$1, %r15b
	je	.L1844
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	jne	.L2116
.L1766:
	testb	$24, %al
	je	.L1844
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2117
	.p2align 4,,10
	.p2align 3
.L1844:
	movq	0(%r13), %rax
	movq	(%r14), %rbx
	movq	23(%rax), %r15
	leaq	39(%rbx), %rsi
	movq	%r15, 39(%rbx)
	testb	$1, %r15b
	je	.L1843
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	jne	.L2118
.L1769:
	testb	$24, %al
	je	.L1843
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2119
	.p2align 4,,10
	.p2align 3
.L1843:
	leaq	.LC78(%rip), %rax
	xorl	%edx, %edx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	$21, -232(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1705
	movq	(%r14), %rbx
	movq	(%rax), %r15
	movq	%r15, 47(%rbx)
	leaq	47(%rbx), %rsi
	testb	$1, %r15b
	je	.L1842
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	jne	.L2120
.L1772:
	testb	$24, %al
	je	.L1842
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1842
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1842:
	movq	0(%r13), %rax
	movq	(%r14), %r15
	movq	39(%rax), %r13
	leaq	55(%r15), %rsi
	movq	%r13, 55(%r15)
	testb	$1, %r13b
	je	.L1857
.L2106:
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L1827
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -360(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-360(%rbp), %rsi
.L1827:
	testb	$24, %al
	je	.L1857
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1857
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1857:
	movq	(%r14), %rax
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L2112:
	movq	-1(%rax), %rdx
	cmpw	$1063, 11(%rdx)
	je	.L1756
	movq	-1(%rax), %rdx
	cmpw	$1064, 11(%rdx)
	je	.L1756
	movq	-1(%rax), %rdx
	cmpw	$1074, 11(%rdx)
	je	.L2121
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L2122
	movq	-1(%rax), %rax
	cmpw	$1041, 11(%rax)
	jne	.L1799
	xorl	%edx, %edx
	movl	$2, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	leaq	.LC84(%rip), %rax
	movq	$18, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1705
	movq	(%r14), %rbx
	movq	(%rax), %r15
	movq	%r15, 15(%rbx)
	leaq	15(%rbx), %rsi
	testb	$1, %r15b
	je	.L1858
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	je	.L1824
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	8(%rcx), %rax
.L1824:
	testb	$24, %al
	je	.L1858
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1858
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1858:
	movq	0(%r13), %rax
	movq	(%r14), %r15
	movq	23(%rax), %r13
	leaq	23(%r15), %rsi
	movq	%r13, 23(%r15)
	testb	$1, %r13b
	jne	.L2106
	jmp	.L1857
	.p2align 4,,10
	.p2align 3
.L1860:
	leaq	.LC6(%rip), %rbx
.L1729:
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	xorl	%edx, %edx
	leaq	-304(%rbp), %rsi
	movq	%r12, %rdi
	movq	$19, -296(%rbp)
	movq	%rax, %r14
	leaq	.LC9(%rip), %rax
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1705
	movq	(%r14), %rdi
	movq	(%rax), %r15
	movq	%r15, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %r15b
	je	.L1840
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	je	.L1734
	movq	%r15, %rdx
	movq	%rsi, -376(%rbp)
	movq	%rdi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-376(%rbp), %rsi
	movq	-368(%rbp), %rdi
	movq	8(%rcx), %rax
.L1734:
	testb	$24, %al
	je	.L1840
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1840
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1840:
	movq	(%r14), %r8
	movq	0(%r13), %rax
	leaq	-80(%rbp), %rdi
	movq	%r8, -360(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE7HasMoreEv@PLT
	movq	-360(%rbp), %r8
	testb	%al, %al
	je	.L1736
	movq	112(%r12), %r15
.L1737:
	movq	%r15, 23(%r8)
	leaq	23(%r8), %rsi
	testb	$1, %r15b
	je	.L1839
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	je	.L1739
	movq	%r8, %rdi
	movq	%r15, %rdx
	movq	%rsi, -376(%rbp)
	movq	%r8, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-376(%rbp), %rsi
	movq	-368(%rbp), %r8
	movq	8(%rcx), %rax
.L1739:
	testb	$24, %al
	je	.L1839
	movq	%r8, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1839
	movq	%r15, %rdx
	movq	%r8, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1839:
	leaq	.LC12(%rip), %rax
	xorl	%edx, %edx
	leaq	-256(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$17, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1705
	movq	(%r14), %rdi
	movq	(%rax), %r15
	movq	%r15, 31(%rdi)
	leaq	31(%rdi), %rsi
	testb	$1, %r15b
	je	.L1838
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	je	.L1742
	movq	%r15, %rdx
	movq	%rsi, -376(%rbp)
	movq	%rdi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-376(%rbp), %rsi
	movq	-368(%rbp), %rdi
	movq	8(%rcx), %rax
.L1742:
	testb	$24, %al
	je	.L1838
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1838
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1838:
	movq	0(%r13), %rax
	movq	(%r14), %r15
	movq	31(%rax), %r13
	leaq	39(%r15), %rsi
	movq	%r13, 39(%r15)
	testb	$1, %r13b
	je	.L1837
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	je	.L1745
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	8(%rcx), %rax
.L1745:
	testb	$24, %al
	je	.L1837
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1837
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1837:
	leaq	.LC13(%rip), %rax
	xorl	%edx, %edx
	leaq	-272(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	movq	$16, -264(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1705
	movq	(%r14), %r15
	movq	(%rax), %r13
	movq	%r13, 47(%r15)
	leaq	47(%r15), %rsi
	testb	$1, %r13b
	je	.L1836
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	je	.L1748
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	8(%rcx), %rax
.L1748:
	testb	$24, %al
	je	.L1836
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1836
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1836:
	movq	%rbx, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	-288(%rbp), %rsi
	movq	%rbx, -288(%rbp)
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	jne	.L2107
	.p2align 4,,10
	.p2align 3
.L1705:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1696:
	xorl	%edx, %edx
	movl	$6, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	xorl	%edx, %edx
	leaq	-320(%rbp), %rsi
	movq	%r12, %rdi
	movq	$18, -312(%rbp)
	movq	%rax, %r14
	leaq	.LC73(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1705
	movq	(%r14), %rbx
	movq	(%rax), %r15
	movq	%r15, 15(%rbx)
	leaq	15(%rbx), %rsi
	testb	$1, %r15b
	je	.L1834
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	jne	.L2123
.L1700:
	testb	$24, %al
	je	.L1834
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1834
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1834:
	movq	0(%r13), %rax
	movq	(%r14), %rbx
	movq	23(%rax), %r15
	leaq	23(%rbx), %rsi
	movq	%r15, 23(%rbx)
	testb	$1, %r15b
	je	.L1833
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	jne	.L2124
.L1703:
	testb	$24, %al
	je	.L1833
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1833
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1833:
	leaq	.LC74(%rip), %rax
	xorl	%edx, %edx
	leaq	-336(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -336(%rbp)
	movq	$13, -328(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1705
	movq	(%r14), %rbx
	movq	(%rax), %r15
	movq	%r15, 31(%rbx)
	leaq	31(%rbx), %rsi
	testb	$1, %r15b
	je	.L1832
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	jne	.L2125
.L1707:
	testb	$24, %al
	je	.L1832
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1832
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1832:
	movq	0(%r13), %rax
	movq	(%r14), %rbx
	movq	31(%rax), %r15
	leaq	39(%rbx), %rsi
	movq	%r15, 39(%rbx)
	testb	$1, %r15b
	je	.L1831
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	jne	.L2126
.L1710:
	testb	$24, %al
	je	.L1831
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1831
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1831:
	leaq	.LC75(%rip), %rax
	xorl	%edx, %edx
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	movq	$13, -344(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1705
	movq	(%r14), %rbx
	movq	(%rax), %r15
	movq	%r15, 47(%rbx)
	leaq	47(%rbx), %rsi
	testb	$1, %r15b
	je	.L1830
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	jne	.L2127
.L1713:
	testb	$24, %al
	je	.L1830
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1830
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1830:
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	39(%rax), %r13
	testq	%rdi, %rdi
	je	.L1715
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1716:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14CopyFixedArrayENS0_6HandleINS0_10FixedArrayEEE@PLT
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	(%rax), %rax
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
.L2107:
	movq	(%r14), %r15
	movq	(%rax), %r13
	movq	%r13, 55(%r15)
	leaq	55(%r15), %rsi
	testb	$1, %r13b
	jne	.L2106
	jmp	.L1857
	.p2align 4,,10
	.p2align 3
.L2114:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L1763
	.p2align 4,,10
	.p2align 3
.L2118:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L1769
	.p2align 4,,10
	.p2align 3
.L2116:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L1766
	.p2align 4,,10
	.p2align 3
.L1715:
	movq	41088(%r12), %rsi
	cmpq	%rsi, 41096(%r12)
	je	.L2128
.L1717:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L2120:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L1772
	.p2align 4,,10
	.p2align 3
.L2123:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L1700
	.p2align 4,,10
	.p2align 3
.L2124:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L1703
.L1728:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2126:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L1710
	.p2align 4,,10
	.p2align 3
.L2125:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L2127:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L1713
	.p2align 4,,10
	.p2align 3
.L1731:
	leaq	.LC8(%rip), %rbx
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L2113:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1846
	.p2align 4,,10
	.p2align 3
.L2115:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L2117:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L2119:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1843
	.p2align 4,,10
	.p2align 3
.L1732:
	leaq	.LC7(%rip), %rbx
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L1736:
	movq	120(%r12), %r15
	jmp	.L1737
	.p2align 4,,10
	.p2align 3
.L2121:
	leaq	-80(%rbp), %r15
	movq	%rax, -80(%rbp)
	movq	%r15, %rdi
	call	_ZNK2v88internal9JSPromise6statusEv@PLT
	movl	%eax, %edi
	call	_ZN2v88internal9JSPromise6StatusENS_7Promise12PromiseStateE@PLT
	xorl	%edx, %edx
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	$17, -152(%rbp)
	movq	%rax, %r14
	leaq	.LC79(%rip), %rax
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1705
	movq	(%r14), %rdi
	movq	(%rax), %rdx
	movq	%rdx, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %dl
	je	.L1850
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	je	.L1781
	movq	%rdx, -384(%rbp)
	movq	%rsi, -376(%rbp)
	movq	%rdi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-384(%rbp), %rdx
	movq	-376(%rbp), %rsi
	movq	-368(%rbp), %rdi
	movq	8(%rcx), %rax
.L1781:
	testb	$24, %al
	je	.L1850
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1850
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1850:
	movq	%rbx, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	-176(%rbp), %rsi
	movq	%rbx, -176(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1705
	movq	(%r14), %rdi
	movq	(%rax), %rbx
	movq	%rbx, 23(%rdi)
	leaq	23(%rdi), %rsi
	testb	$1, %bl
	je	.L1849
	movq	%rbx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	je	.L1784
	movq	%rbx, %rdx
	movq	%rsi, -376(%rbp)
	movq	%rdi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-376(%rbp), %rsi
	movq	-368(%rbp), %rdi
	movq	8(%rcx), %rax
.L1784:
	testb	$24, %al
	je	.L1849
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1849
	movq	%rbx, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1849:
	movq	0(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal9JSPromise6statusEv@PLT
	testl	%eax, %eax
	jne	.L2100
	movq	88(%r12), %rsi
.L1788:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1789
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L1790:
	leaq	.LC80(%rip), %rax
	xorl	%edx, %edx
	leaq	-144(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	movq	$16, -136(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1705
	movq	(%r14), %r15
	movq	(%rax), %r13
	movq	%r13, 31(%r15)
	leaq	31(%r15), %rsi
	testb	$1, %r13b
	je	.L1848
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	je	.L1793
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	8(%rcx), %rax
.L1793:
	testb	$24, %al
	je	.L1848
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1848
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1848:
	movq	(%r14), %r15
	movq	(%rbx), %r13
	movq	%r13, 39(%r15)
	leaq	39(%r15), %rsi
	testb	$1, %r13b
	jne	.L2106
	jmp	.L1857
	.p2align 4,,10
	.p2align 3
.L2122:
	xorl	%edx, %edx
	movl	$6, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	xorl	%edx, %edx
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	leaq	.LC81(%rip), %rax
	movq	$11, -88(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1705
	movq	(%r14), %rbx
	movq	(%rax), %r15
	movq	%r15, 15(%rbx)
	leaq	15(%rbx), %rsi
	testb	$1, %r15b
	je	.L1856
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	je	.L1802
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	8(%rcx), %rax
.L1802:
	testb	$24, %al
	je	.L1856
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1856
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1856:
	movq	0(%r13), %rax
	movq	(%r14), %rbx
	movq	23(%rax), %r15
	leaq	23(%rbx), %rsi
	movq	%r15, 23(%rbx)
	testb	$1, %r15b
	je	.L1855
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	je	.L1805
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	8(%rcx), %rax
.L1805:
	testb	$24, %al
	je	.L1855
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1855
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1855:
	leaq	.LC82(%rip), %rax
	xorl	%edx, %edx
	leaq	-112(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	movq	$10, -104(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1705
	movq	(%r14), %rbx
	movq	(%rax), %r15
	movq	%r15, 31(%rbx)
	leaq	31(%rbx), %rsi
	testb	$1, %r15b
	je	.L1854
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	je	.L1808
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	8(%rcx), %rax
.L1808:
	testb	$24, %al
	je	.L1854
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1854
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1854:
	movq	0(%r13), %rax
	movq	(%r14), %rbx
	movq	15(%rax), %r15
	leaq	39(%rbx), %rsi
	movq	%r15, 39(%rbx)
	testb	$1, %r15b
	je	.L1853
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	je	.L1811
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	8(%rcx), %rax
.L1811:
	testb	$24, %al
	je	.L1853
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1853
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1853:
	leaq	.LC83(%rip), %rax
	xorl	%edx, %edx
	leaq	-128(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	movq	$13, -120(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L1705
	movq	(%r14), %rbx
	movq	(%rax), %r15
	movq	%r15, 47(%rbx)
	leaq	47(%rbx), %rsi
	testb	$1, %r15b
	je	.L1852
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -360(%rbp)
	testl	$262144, %eax
	je	.L1814
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	8(%rcx), %rax
.L1814:
	testb	$24, %al
	je	.L1852
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1852
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1852:
	movq	0(%r13), %rax
	movq	(%r14), %r15
	movq	23(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	ja	.L2101
	movq	112(%r12), %r13
.L1818:
	movq	%r13, 55(%r15)
	leaq	55(%r15), %rsi
	testb	$1, %r13b
	jne	.L2106
	jmp	.L1857
	.p2align 4,,10
	.p2align 3
.L2100:
	movq	0(%r13), %rax
	movq	23(%rax), %rsi
	jmp	.L1788
	.p2align 4,,10
	.p2align 3
.L1789:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L2129
.L1791:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L1790
	.p2align 4,,10
	.p2align 3
.L2101:
	movq	120(%r12), %r13
	jmp	.L1818
.L2128:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1717
.L2129:
	movq	%r12, %rdi
	movq	%rsi, -360(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-360(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L1791
.L2109:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21646:
	.size	_ZN2v88internal7Runtime21GetInternalPropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal7Runtime21GetInternalPropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal30Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateE:
.LFB21648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2148
	movl	41104(%rdx), %eax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L2149
	movl	%eax, 41104(%r12)
	xorl	%r13d, %r13d
.L2130:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2150
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2149:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	cmpw	$1068, 11(%rcx)
	jne	.L2151
.L2134:
	testb	$1, %dl
	jne	.L2136
.L2138:
	leaq	.LC22(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2151:
	movq	-1(%rdx), %rcx
	cmpw	$1063, 11(%rcx)
	je	.L2134
	movq	-1(%rdx), %rcx
	cmpw	$1064, 11(%rcx)
	je	.L2134
	movq	%r14, %rdx
	xorl	%r13d, %r13d
	jmp	.L2135
	.p2align 4,,10
	.p2align 3
.L2136:
	movq	-1(%rdx), %rax
	cmpw	$1068, 11(%rax)
	jne	.L2152
.L2137:
	testb	$-128, 70(%rdx)
	je	.L2139
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	xorl	%r13d, %r13d
	subl	$1, %eax
.L2135:
	movq	%rbx, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %r14
	je	.L2130
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L2130
	.p2align 4,,10
	.p2align 3
.L2148:
	movq	%rdx, %rsi
	call	_ZN2v88internalL36Stats_Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L2130
	.p2align 4,,10
	.p2align 3
.L2139:
	leaq	-160(%rbp), %r15
	movq	%rdi, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal13ScopeIteratorC1EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE@PLT
	cmpq	$0, -120(%rbp)
	je	.L2145
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L2141:
	movq	%r15, %rdi
	addl	$1, %r13d
	call	_ZN2v88internal13ScopeIterator4NextEv@PLT
	cmpq	$0, -120(%rbp)
	jne	.L2141
	salq	$32, %r13
.L2140:
	movq	%r15, %rdi
	call	_ZN2v88internal13ScopeIteratorD1Ev@PLT
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L2135
	.p2align 4,,10
	.p2align 3
.L2152:
	movq	-1(%rdx), %rax
	cmpw	$1063, 11(%rax)
	je	.L2137
	movq	-1(%rdx), %rax
	cmpw	$1064, 11(%rax)
	jne	.L2138
	jmp	.L2137
.L2145:
	xorl	%r13d, %r13d
	jmp	.L2140
.L2150:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21648:
	.size	_ZN2v88internal30Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateE:
.LFB21651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2194
	movq	41088(%rdx), %rax
	movq	41096(%rdx), %r15
	movl	41104(%rdx), %edx
	movq	%rax, -168(%rbp)
	movq	%r15, %rcx
	leal	1(%rdx), %eax
	movl	%eax, 41104(%r12)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L2195
.L2156:
	movq	88(%r12), %r13
.L2158:
	movq	-168(%rbp), %rax
	movl	%edx, 41104(%r12)
	movq	%rax, 41088(%r12)
	cmpq	%rcx, %r15
	je	.L2153
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2153:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2196
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2195:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$1068, 11(%rcx)
	jne	.L2197
.L2157:
	testb	$1, %al
	jne	.L2159
.L2161:
	leaq	.LC22(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2197:
	movq	-1(%rax), %rcx
	cmpw	$1063, 11(%rcx)
	je	.L2157
	movq	-1(%rax), %rcx
	cmpw	$1064, 11(%rcx)
	je	.L2157
	movq	%r15, %rcx
	jmp	.L2156
	.p2align 4,,10
	.p2align 3
.L2159:
	movq	-1(%rax), %rdx
	cmpw	$1068, 11(%rdx)
	jne	.L2198
.L2160:
	movq	-8(%rdi), %rdx
	movq	%rdx, %rbx
	shrq	$32, %rbx
	movq	%rbx, %r13
	testb	$1, %dl
	je	.L2165
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L2199
	movq	7(%rdx), %rdx
	movsd	.LC16(%rip), %xmm2
	movq	%rdx, %xmm1
	andpd	.LC15(%rip), %xmm1
	movq	%rdx, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L2166
	movsd	.LC17(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L2166
	comisd	.LC18(%rip), %xmm0
	jb	.L2166
	cvttsd2sil	%xmm0, %r13d
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%r13d, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L2166
	jne	.L2166
	.p2align 4,,10
	.p2align 3
.L2165:
	testb	$-128, 70(%rax)
	je	.L2173
	movl	41104(%r12), %eax
	movq	88(%r12), %r13
	movq	41096(%r12), %rcx
	leal	-1(%rax), %edx
	jmp	.L2158
	.p2align 4,,10
	.p2align 3
.L2166:
	movabsq	$9218868437227405312, %rcx
	testq	%rcx, %rdx
	je	.L2183
	movq	%rdx, %rsi
	xorl	%r13d, %r13d
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L2200
	cmpl	$31, %ecx
	jg	.L2165
	movabsq	$4503599627370495, %r13
	movabsq	$4503599627370496, %rsi
	andq	%rdx, %r13
	addq	%rsi, %r13
	salq	%cl, %r13
	movl	%r13d, %r13d
.L2171:
	sarq	$63, %rdx
	orl	$1, %edx
	imull	%edx, %r13d
	jmp	.L2165
	.p2align 4,,10
	.p2align 3
.L2173:
	leaq	-160(%rbp), %rbx
	movq	%rdi, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal13ScopeIteratorC1EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE@PLT
	cmpq	$0, -120(%rbp)
	je	.L2174
	xorl	%r14d, %r14d
	testl	%r13d, %r13d
	jg	.L2175
	jmp	.L2176
	.p2align 4,,10
	.p2align 3
.L2192:
	cmpl	%r13d, %r14d
	je	.L2176
.L2175:
	movq	%rbx, %rdi
	addl	$1, %r14d
	call	_ZN2v88internal13ScopeIterator4NextEv@PLT
	cmpq	$0, -120(%rbp)
	jne	.L2192
.L2174:
	movq	88(%r12), %r13
.L2177:
	movq	%rbx, %rdi
	call	_ZN2v88internal13ScopeIteratorD1Ev@PLT
	movl	41104(%r12), %eax
	movq	41096(%r12), %rcx
	leal	-1(%rax), %edx
	jmp	.L2158
	.p2align 4,,10
	.p2align 3
.L2194:
	movq	%rdx, %rsi
	call	_ZN2v88internalL38Stats_Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L2153
	.p2align 4,,10
	.p2align 3
.L2176:
	movq	%rbx, %rdi
	call	_ZN2v88internal13ScopeIterator23MaterializeScopeDetailsEv@PLT
	movq	(%rax), %r13
	jmp	.L2177
	.p2align 4,,10
	.p2align 3
.L2198:
	movq	-1(%rax), %rdx
	cmpw	$1063, 11(%rdx)
	je	.L2160
	movq	-1(%rax), %rdx
	cmpw	$1064, 11(%rdx)
	jne	.L2161
	jmp	.L2160
	.p2align 4,,10
	.p2align 3
.L2200:
	cmpl	$-52, %ecx
	jl	.L2165
	movabsq	$4503599627370495, %r13
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %r13
	addq	%rcx, %r13
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %r13
	jmp	.L2171
	.p2align 4,,10
	.p2align 3
.L2199:
	leaq	.LC23(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2183:
	xorl	%r13d, %r13d
	jmp	.L2165
.L2196:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21651:
	.size	_ZN2v88internal32Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateE, .-_ZN2v88internal32Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal38Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateE
	.type	_ZN2v88internal38Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateE, @function
_ZN2v88internal38Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateE:
.LFB21655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2243
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41096(%rdx), %r13
	movq	%rax, -168(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L2244
.L2205:
	leaq	.LC22(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2244:
	movq	-1(%rax), %rdx
	cmpw	$1068, 11(%rdx)
	jne	.L2206
.L2209:
	movq	-8(%r12), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	movq	%rcx, %rbx
	testb	$1, %al
	je	.L2213
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L2245
	movq	7(%rax), %rax
	movsd	.LC16(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC15(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jnb	.L2246
.L2214:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L2233
	movq	%rax, %rdx
	xorl	%ebx, %ebx
	shrq	$52, %rdx
	andl	$2047, %edx
	movl	%edx, %ecx
	subl	$1075, %ecx
	js	.L2247
	cmpl	$31, %ecx
	jg	.L2213
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rdx
	andq	%rax, %rbx
	addq	%rdx, %rbx
	salq	%cl, %rbx
	movl	%ebx, %ebx
.L2219:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%eax, %ebx
.L2213:
	leaq	-16(%r12), %rax
	movq	%rax, -176(%rbp)
	movq	-16(%r12), %rax
	testb	$1, %al
	jne	.L2248
.L2221:
	leaq	.LC24(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2248:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2221
	leaq	-160(%rbp), %r15
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal13ScopeIteratorC1EPNS0_7IsolateENS0_6HandleINS0_17JSGeneratorObjectEEE@PLT
	leaq	-24(%r12), %rax
	cmpq	$0, -120(%rbp)
	movq	%rax, -184(%rbp)
	je	.L2230
	xorl	%r12d, %r12d
	testl	%ebx, %ebx
	jg	.L2224
	jmp	.L2225
	.p2align 4,,10
	.p2align 3
.L2226:
	cmpl	%r12d, %ebx
	je	.L2225
.L2224:
	movq	%r15, %rdi
	addl	$1, %r12d
	call	_ZN2v88internal13ScopeIterator4NextEv@PLT
	cmpq	$0, -120(%rbp)
	jne	.L2226
.L2230:
	movq	120(%r14), %r12
.L2227:
	movq	%r15, %rdi
	call	_ZN2v88internal13ScopeIteratorD1Ev@PLT
	movq	-168(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	cmpq	41096(%r14), %r13
	je	.L2201
	movq	%r13, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2201:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2249
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2225:
	.cfi_restore_state
	movq	-184(%rbp), %rdx
	movq	-176(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal13ScopeIterator16SetVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L2230
	movq	112(%r14), %r12
	jmp	.L2227
	.p2align 4,,10
	.p2align 3
.L2246:
	movsd	.LC17(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L2214
	comisd	.LC18(%rip), %xmm0
	jb	.L2214
	cvttsd2sil	%xmm0, %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L2214
	je	.L2213
	jmp	.L2214
	.p2align 4,,10
	.p2align 3
.L2206:
	movq	-1(%rax), %rdx
	cmpw	$1063, 11(%rdx)
	je	.L2209
	movq	-1(%rax), %rax
	cmpw	$1064, 11(%rax)
	jne	.L2205
	jmp	.L2209
	.p2align 4,,10
	.p2align 3
.L2247:
	cmpl	$-52, %ecx
	jl	.L2213
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rcx
	andq	%rax, %rbx
	addq	%rcx, %rbx
	movl	$1075, %ecx
	subl	%edx, %ecx
	shrq	%cl, %rbx
	jmp	.L2219
	.p2align 4,,10
	.p2align 3
.L2243:
	call	_ZN2v88internalL44Stats_Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateE
	movq	%rax, %r12
	jmp	.L2201
	.p2align 4,,10
	.p2align 3
.L2245:
	leaq	.LC23(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2233:
	xorl	%ebx, %ebx
	jmp	.L2213
.L2249:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21655:
	.size	_ZN2v88internal38Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateE, .-_ZN2v88internal38Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_GetBreakLocationsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_GetBreakLocationsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_GetBreakLocationsEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_GetBreakLocationsEiPmPNS0_7IsolateE:
.LFB21658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2263
	movq	41472(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	cmpb	$0, 8(%rax)
	je	.L2264
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L2253
.L2254:
	leaq	.LC32(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2253:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L2254
	movq	23(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2255
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2256:
	movq	%r12, %rdi
	call	_ZN2v88internal5Debug23GetSourceBreakLocationsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movq	%rax, %rsi
	movq	(%rax), %rax
	movq	%rax, %r14
	cmpq	88(%r12), %rax
	je	.L2259
	movslq	11(%rax), %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
.L2259:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L2250
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2250:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2255:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	41096(%r12), %rbx
	je	.L2265
.L2257:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2263:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_GetBreakLocationsEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L2264:
	.cfi_restore_state
	leaq	.LC35(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2265:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2257
	.cfi_endproc
.LFE21658:
	.size	_ZN2v88internal25Runtime_GetBreakLocationsEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_GetBreakLocationsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateE:
.LFB21661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %esi
	testl	%esi, %esi
	jne	.L2293
	addl	$1, 41104(%rdx)
	movq	(%rdi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L2294
	shrq	$32, %rax
	movq	%rax, %rsi
.L2272:
	movq	41472(%r12), %rdi
	call	_ZN2v88internal5Debug18IsBreakOnExceptionENS0_18ExceptionBreakTypeE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movzbl	%al, %r14d
	salq	$32, %r14
	cmpq	41096(%r12), %rbx
	je	.L2266
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2266:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2294:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L2295
	movq	7(%rax), %rdx
	movsd	.LC16(%rip), %xmm2
	movq	%rdx, %xmm1
	andpd	.LC15(%rip), %xmm1
	movq	%rdx, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L2273
	movsd	.LC17(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L2296
.L2273:
	movabsq	$9218868437227405312, %rax
	testq	%rax, %rdx
	je	.L2272
	movq	%rdx, %rax
	shrq	$52, %rax
	andl	$2047, %eax
	movl	%eax, %ecx
	subl	$1075, %ecx
	js	.L2297
	cmpl	$31, %ecx
	jg	.L2272
	movabsq	$4503599627370495, %rsi
	movq	%rsi, %rax
	addq	$1, %rsi
	andq	%rdx, %rax
	addq	%rax, %rsi
	salq	%cl, %rsi
	movl	%esi, %esi
.L2279:
	sarq	$63, %rdx
	orl	$1, %edx
	imull	%edx, %esi
	jmp	.L2272
	.p2align 4,,10
	.p2align 3
.L2296:
	comisd	.LC18(%rip), %xmm0
	jb	.L2273
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L2273
	jne	.L2273
	movl	%eax, %esi
	jmp	.L2272
	.p2align 4,,10
	.p2align 3
.L2297:
	cmpl	$-52, %ecx
	jl	.L2272
	movabsq	$4503599627370495, %rsi
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %rsi
	addq	%rcx, %rsi
	movl	$1075, %ecx
	subl	%eax, %ecx
	shrq	%cl, %rsi
	jmp	.L2279
	.p2align 4,,10
	.p2align 3
.L2293:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Stats_Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L2295:
	.cfi_restore_state
	leaq	.LC26(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21661:
	.size	_ZN2v88internal26Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_ClearSteppingEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_ClearSteppingEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_ClearSteppingEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_ClearSteppingEiPmPNS0_7IsolateE:
.LFB21664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2303
	movq	41472(%rdx), %rdi
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	cmpb	$0, 8(%rdi)
	je	.L2304
	call	_ZN2v88internal5Debug13ClearSteppingEv@PLT
	movq	%r13, 41088(%r12)
	movq	88(%r12), %r14
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2298
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2298:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2303:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL27Stats_Runtime_ClearSteppingEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L2304:
	.cfi_restore_state
	leaq	.LC35(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21664:
	.size	_ZN2v88internal21Runtime_ClearSteppingEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_ClearSteppingEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateE:
.LFB21667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2317
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	leaq	-128(%rbp), %r14
	movq	41472(%rdx), %rsi
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal10DebugScopeC1EPNS0_5DebugE@PLT
	movq	41472(%r12), %rdi
	call	_ZN2v88internal5Debug16GetLoadedScriptsEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal10DebugScopeD1Ev@PLT
	movq	(%r15), %rax
	movslq	11(%rax), %rdx
	movl	%edx, %r9d
	testq	%rdx, %rdx
	jle	.L2308
	movl	$15, %ebx
	xorl	%r14d, %r14d
	jmp	.L2312
	.p2align 4,,10
	.p2align 3
.L2318:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2310:
	movslq	67(%rsi), %rax
	movq	(%r15), %rdi
	addl	$1, %r14d
	addq	$8, %rbx
	salq	$32, %rax
	movq	%rax, -1(%r13,%rdi)
	movq	(%r15), %rax
	movslq	11(%rax), %rcx
	movl	%ecx, %r9d
	cmpl	%ecx, %r14d
	jge	.L2308
.L2312:
	movq	(%rbx,%rax), %rsi
	movq	41112(%r12), %rdi
	leaq	1(%rbx), %r13
	testq	%rdi, %rdi
	jne	.L2318
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2319
.L2311:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L2310
	.p2align 4,,10
	.p2align 3
.L2308:
	movq	%r15, %rsi
	xorl	%r8d, %r8d
	movl	%r9d, %ecx
	movl	$3, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r15
	movq	-136(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	movq	-144(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L2305
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2305:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2320
	addq	$120, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2319:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	jmp	.L2311
	.p2align 4,,10
	.p2align 3
.L2317:
	movq	%rdx, %rdi
	call	_ZN2v88internalL37Stats_Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r15
	jmp	.L2305
.L2320:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21667:
	.size	_ZN2v88internal31Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateE:
.LFB21670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2338
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L2324
.L2325:
	movq	128(%rdx), %rax
.L2321:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2339
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2324:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$1105, 11(%rcx)
	jne	.L2325
	movq	23(%rax), %rbx
	movq	15(%rbx), %rax
	testb	$1, %al
	jne	.L2340
.L2326:
	movq	7(%rbx), %rax
	testb	$1, %al
	jne	.L2341
.L2329:
	andq	$-262144, %rbx
	movq	24(%rbx), %rax
	movq	-37464(%rax), %rax
	jmp	.L2321
	.p2align 4,,10
	.p2align 3
.L2338:
	movq	%rdx, %rsi
	call	_ZN2v88internalL37Stats_Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateE.isra.0
	jmp	.L2321
	.p2align 4,,10
	.p2align 3
.L2340:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L2326
	leaq	-32(%rbp), %r12
	movq	%rax, -32(%rbp)
	movq	%r12, %rdi
	call	_ZNK2v88internal9ScopeInfo23HasInferredFunctionNameEv@PLT
	testb	%al, %al
	je	.L2329
	movq	%r12, %rdi
	call	_ZNK2v88internal9ScopeInfo20InferredFunctionNameEv@PLT
	testb	$1, %al
	je	.L2329
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2329
	jmp	.L2321
	.p2align 4,,10
	.p2align 3
.L2341:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L2332
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L2329
.L2332:
	movq	7(%rbx), %rax
	movq	7(%rax), %rax
	jmp	.L2321
.L2339:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21670:
	.size	_ZN2v88internal31Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_CollectGarbageEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_CollectGarbageEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_CollectGarbageEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_CollectGarbageEiPmPNS0_7IsolateE:
.LFB21673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2345
	leaq	37592(%rdx), %rdi
	xorl	%ecx, %ecx
	movl	$18, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal4Heap24PreciseCollectAllGarbageEiNS0_23GarbageCollectionReasonENS_15GCCallbackFlagsE@PLT
	movq	88(%r12), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2345:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rdx, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL28Stats_Runtime_CollectGarbageEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21673:
	.size	_ZN2v88internal22Runtime_CollectGarbageEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_CollectGarbageEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Runtime_GetHeapUsageEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Runtime_GetHeapUsageEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Runtime_GetHeapUsageEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Runtime_GetHeapUsageEiPmPNS0_7IsolateE:
.LFB21676:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2350
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	37592(%rdx), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal4Heap13SizeOfObjectsEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	salq	$32, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2350:
	.cfi_restore 6
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL26Stats_Runtime_GetHeapUsageEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21676:
	.size	_ZN2v88internal20Runtime_GetHeapUsageEiPmPNS0_7IsolateE, .-_ZN2v88internal20Runtime_GetHeapUsageEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateE:
.LFB21684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2408
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rax
	movq	(%rsi), %rbx
	movq	41096(%rdx), %r14
	movq	%rax, -88(%rbp)
	testb	$1, %bl
	jne	.L2409
	shrq	$32, %rbx
.L2358:
	leaq	-8(%rdi), %rax
	movq	-24(%rdi), %r12
	movq	%rax, -96(%rbp)
	leaq	-16(%rdi), %rax
	movq	%rax, -104(%rbp)
	testb	$1, %r12b
	je	.L2403
	movq	-1(%r12), %rax
	cmpw	$65, 11(%rax)
	jne	.L2410
	movq	7(%r12), %rax
	movsd	.LC16(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC15(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jnb	.L2411
.L2371:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L2391
	movq	%rax, %rdx
	xorl	%r12d, %r12d
	shrq	$52, %rdx
	andl	$2047, %edx
	movl	%edx, %ecx
	subl	$1075, %ecx
	js	.L2412
	cmpl	$31, %ecx
	jg	.L2370
	movabsq	$4503599627370495, %r12
	movabsq	$4503599627370496, %rdx
	andq	%rax, %r12
	addq	%rdx, %r12
	salq	%cl, %r12
	movl	%r12d, %r12d
.L2376:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%eax, %r12d
.L2370:
	leaq	-80(%rbp), %r13
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Script8IteratorC1EPNS0_7IsolateE@PLT
	jmp	.L2407
	.p2align 4,,10
	.p2align 3
.L2378:
	cmpl	%ebx, 67(%rax)
	je	.L2413
.L2407:
	movq	%r13, %rdi
	call	_ZN2v88internal6Script8Iterator4NextEv@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L2378
	leaq	.LC30(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2409:
	movq	-1(%rbx), %rax
	cmpw	$65, 11(%rax)
	jne	.L2414
	movq	7(%rbx), %rax
	movsd	.LC16(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC15(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L2359
	movsd	.LC17(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L2415
.L2359:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L2387
	movq	%rax, %rdx
	xorl	%ebx, %ebx
	shrq	$52, %rdx
	andl	$2047, %edx
	movl	%edx, %ecx
	subl	$1075, %ecx
	js	.L2416
	cmpl	$31, %ecx
	jg	.L2358
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rdx
	andq	%rax, %rbx
	addq	%rdx, %rbx
	salq	%cl, %rbx
	movl	%ebx, %ebx
.L2364:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%eax, %ebx
	jmp	.L2358
	.p2align 4,,10
	.p2align 3
.L2403:
	shrq	$32, %r12
	jmp	.L2370
	.p2align 4,,10
	.p2align 3
.L2413:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2380
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
.L2381:
	movq	-104(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movl	%r12d, %r8d
	movq	%r10, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122ScriptLocationFromLineEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6ObjectEEES8_i
	movq	(%rax), %r12
	movq	-88(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %r14
	je	.L2351
	movq	%r14, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2351:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2417
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2411:
	.cfi_restore_state
	movsd	.LC17(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L2371
	comisd	.LC18(%rip), %xmm0
	jb	.L2371
	cvttsd2sil	%xmm0, %r12d
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%r12d, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L2371
	je	.L2370
	jmp	.L2371
	.p2align 4,,10
	.p2align 3
.L2380:
	movq	41088(%r15), %r10
	cmpq	41096(%r15), %r10
	je	.L2418
.L2382:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r10)
	jmp	.L2381
	.p2align 4,,10
	.p2align 3
.L2415:
	comisd	.LC18(%rip), %xmm0
	jb	.L2359
	cvttsd2sil	%xmm0, %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L2359
	je	.L2358
	jmp	.L2359
	.p2align 4,,10
	.p2align 3
.L2416:
	cmpl	$-52, %ecx
	jl	.L2358
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rcx
	andq	%rax, %rbx
	addq	%rcx, %rbx
	movl	$1075, %ecx
	subl	%edx, %ecx
	shrq	%cl, %rbx
	jmp	.L2364
	.p2align 4,,10
	.p2align 3
.L2412:
	cmpl	$-52, %ecx
	jl	.L2370
	movabsq	$4503599627370495, %r12
	movabsq	$4503599627370496, %rcx
	andq	%rax, %r12
	addq	%rcx, %r12
	movl	$1075, %ecx
	subl	%edx, %ecx
	shrq	%cl, %r12
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2408:
	movq	%rdx, %rsi
	call	_ZN2v88internalL37Stats_Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r12
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2414:
	leaq	.LC26(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2410:
	leaq	.LC29(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2418:
	movq	%r15, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L2382
.L2387:
	xorl	%ebx, %ebx
	jmp	.L2358
.L2391:
	xorl	%r12d, %r12d
	jmp	.L2370
.L2417:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21684:
	.size	_ZN2v88internal31Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateE:
.LFB21687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2430
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rdi
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r15
	testb	$1, %dil
	jne	.L2421
.L2422:
	leaq	.LC32(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2421:
	movq	-1(%rdi), %rax
	cmpw	$1105, 11(%rax)
	jne	.L2422
	movq	41472(%rdx), %rax
	leaq	-8(%rsi), %r14
	cmpb	$0, 9(%rax)
	je	.L2423
	xorl	%esi, %esi
	call	_ZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeE@PLT
	movq	41472(%r12), %rdi
	cmpb	$1, 76(%rdi)
	jle	.L2431
.L2424:
	movq	%r13, %rsi
	call	_ZN2v88internal5Debug13PrepareStepInENS0_6HandleINS0_10JSFunctionEEE@PLT
.L2425:
	cmpl	$32, 41828(%r12)
	je	.L2432
.L2423:
	movq	88(%r12), %r13
.L2426:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L2419
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2419:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2431:
	.cfi_restore_state
	cmpb	$0, 132(%rdi)
	je	.L2425
	jmp	.L2424
	.p2align 4,,10
	.p2align 3
.L2430:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L2432:
	.cfi_restore_state
	movq	41472(%r12), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal5Debug22PerformSideEffectCheckENS0_6HandleINS0_10JSFunctionEEENS2_INS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L2423
	movq	312(%r12), %r13
	jmp	.L2426
	.cfi_endproc
.LFE21687:
	.size	_ZN2v88internal27Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal44Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal44Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal44Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateE, @function
_ZN2v88internal44Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateE:
.LFB21690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2437
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	41472(%rdx), %rdi
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal5Debug31PrepareStepInSuspendedGeneratorEv@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2433
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2433:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2437:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL50Stats_Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21690:
	.size	_ZN2v88internal44Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateE, .-_ZN2v88internal44Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_DebugPushPromiseEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_DebugPushPromiseEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_DebugPushPromiseEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_DebugPushPromiseEiPmPNS0_7IsolateE:
.LFB21693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2444
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L2440
.L2441:
	leaq	.LC40(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2440:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L2441
	movq	%rdx, %rdi
	call	_ZN2v88internal7Isolate11PushPromiseENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	%r13, 41088(%r12)
	movq	88(%r12), %r14
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2438
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2438:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2444:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL30Stats_Runtime_DebugPushPromiseEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21693:
	.size	_ZN2v88internal24Runtime_DebugPushPromiseEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_DebugPushPromiseEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_DebugPopPromiseEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_DebugPopPromiseEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_DebugPopPromiseEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_DebugPopPromiseEiPmPNS0_7IsolateE:
.LFB21696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2448
	call	_ZN2v88internal7Isolate10PopPromiseEv@PLT
	movq	88(%r12), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2448:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL29Stats_Runtime_DebugPopPromiseEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21696:
	.size	_ZN2v88internal23Runtime_DebugPopPromiseEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_DebugPopPromiseEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateE:
.LFB21730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2458
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L2459
.L2452:
	leaq	.LC53(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2459:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L2452
	testb	$-2, 43(%rax)
	jne	.L2452
	xorl	%esi, %esi
	cmpq	%rax, 112(%r12)
	movq	%r12, %rdi
	sete	%sil
	call	_ZN2v88internal8Coverage10SelectModeEPNS0_7IsolateENS_5debug12CoverageModeE@PLT
	movq	88(%r12), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2458:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rdx, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL40Stats_Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21730:
	.size	_ZN2v88internal34Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateE:
.LFB21733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2472
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L2473
.L2463:
	leaq	.LC53(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2473:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L2463
	testb	$-2, 43(%rax)
	jne	.L2463
	xorl	%esi, %esi
	cmpq	%rax, 112(%r12)
	movq	%r12, %rdi
	sete	%sil
	leal	(%rsi,%rsi,2), %esi
	call	_ZN2v88internal8Coverage10SelectModeEPNS0_7IsolateENS_5debug12CoverageModeE@PLT
	movq	88(%r12), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2472:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rdx, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL38Stats_Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21733:
	.size	_ZN2v88internal32Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateE, .-_ZN2v88internal32Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_IncBlockCounterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_IncBlockCounterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_IncBlockCounterEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_IncBlockCounterEiPmPNS0_7IsolateE:
.LFB21736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L2477
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2477:
	movq	%rdx, %rdi
	call	_ZN2v88internalL29Stats_Runtime_IncBlockCounterEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21736:
	.size	_ZN2v88internal23Runtime_IncBlockCounterEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_IncBlockCounterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateE:
.LFB21739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2486
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L2480
.L2481:
	leaq	.LC43(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2480:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L2481
	leaq	88(%rdx), %rcx
	movq	%r12, %rdi
	movq	%rsi, %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE@PLT
	movq	41472(%r12), %rax
	cmpb	$0, 8(%rax)
	jne	.L2487
.L2482:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r13
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2478
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2478:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2487:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate11PushPromiseENS0_6HandleINS0_8JSObjectEEE@PLT
	jmp	.L2482
	.p2align 4,,10
	.p2align 3
.L2486:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Stats_Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21739:
	.size	_ZN2v88internal33Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateE, .-_ZN2v88internal33Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal35Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateE:
.LFB21742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2494
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L2490
.L2491:
	leaq	.LC43(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2490:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L2491
	movq	%rdx, %rdi
	call	_ZN2v88internal7Isolate10PopPromiseEv@PLT
	movq	%r13, %rsi
	movl	$5, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate27OnAsyncFunctionStateChangedENS0_6HandleINS0_9JSPromiseEEENS_5debug20DebugAsyncActionTypeE@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2488
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2488:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2494:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL41Stats_Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21742:
	.size	_ZN2v88internal35Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateE, .-_ZN2v88internal35Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateE:
.LFB21745:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2501
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L2497
.L2498:
	leaq	.LC43(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2497:
	movq	-1(%rax), %rax
	cmpw	$1074, 11(%rax)
	jne	.L2498
	movq	%rdx, %rdi
	call	_ZN2v88internal7Isolate11PushPromiseENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	%r13, 41088(%r12)
	movq	88(%r12), %r14
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2495
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2495:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2501:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Stats_Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21745:
	.size	_ZN2v88internal33Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateE, .-_ZN2v88internal33Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateE:
.LFB21748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2513
	addl	$1, 41104(%rdx)
	movq	(%rsi), %r14
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %r14b
	jne	.L2514
.L2504:
	leaq	.LC53(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2514:
	movq	-1(%r14), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L2504
	testb	$-2, 43(%r14)
	jne	.L2504
	movq	-8(%rsi), %rdx
	testb	$1, %dl
	jne	.L2515
.L2505:
	leaq	.LC67(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2515:
	movq	112(%r12), %rax
	movq	%rax, -56(%rbp)
	movq	-1(%rdx), %rdx
	cmpw	$1074, 11(%rdx)
	jne	.L2505
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10PopPromiseEv@PLT
	cmpq	%r14, -56(%rbp)
	je	.L2516
.L2508:
	movq	-8(%r15), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2502
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2502:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2516:
	.cfi_restore_state
	movl	$6, %edx
	leaq	-8(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate27OnAsyncFunctionStateChangedENS0_6HandleINS0_9JSPromiseEEENS_5debug20DebugAsyncActionTypeE@PLT
	jmp	.L2508
	.p2align 4,,10
	.p2align 3
.L2513:
	addq	$24, %rsp
	movq	%r15, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL40Stats_Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21748:
	.size	_ZN2v88internal34Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE:
.LFB21751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2564
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L2520
.L2521:
	leaq	.LC32(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2520:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L2521
	movq	-8(%rsi), %rdx
	leaq	-8(%rsi), %r14
	testb	$1, %dl
	jne	.L2565
.L2522:
	leaq	.LC56(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2565:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2522
	movq	23(%rax), %rax
	movq	31(%rax), %r15
	testb	$1, %r15b
	jne	.L2566
.L2524:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2525
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2526:
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-96(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$0, -96(%rbp)
	movb	$0, -92(%rbp)
	movq	$-1, -72(%rbp)
	movups	%xmm0, -88(%rbp)
	call	_ZN2v88internal8LiveEdit11PatchScriptEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6StringEEEbPNS_5debug14LiveEditResultE@PLT
	cmpl	$7, -96(%rbp)
	ja	.L2528
	movl	-96(%rbp), %eax
	leaq	.L2530(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal27Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE,"a",@progbits
	.align 4
	.align 4
.L2530:
	.long	.L2528-.L2530
	.long	.L2536-.L2530
	.long	.L2535-.L2530
	.long	.L2534-.L2530
	.long	.L2533-.L2530
	.long	.L2532-.L2530
	.long	.L2531-.L2530
	.long	.L2529-.L2530
	.section	.text._ZN2v88internal27Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L2525:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2567
.L2527:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L2526
	.p2align 4,,10
	.p2align 3
.L2566:
	movq	-1(%r15), %rax
	cmpw	$86, 11(%rax)
	jne	.L2524
	movq	23(%r15), %r15
	jmp	.L2524
	.p2align 4,,10
	.p2align 3
.L2529:
	leaq	.LC63(%rip), %rax
	movq	$47, -104(%rbp)
	leaq	-112(%rbp), %rsi
	movq	%rax, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L2563:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L2568
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
.L2538:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2517
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2517:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2569
	addq	$168, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2531:
	.cfi_restore_state
	leaq	.LC62(%rip), %rax
	movq	$55, -120(%rbp)
	leaq	-128(%rbp), %rsi
	movq	%rax, -128(%rbp)
	jmp	.L2563
	.p2align 4,,10
	.p2align 3
.L2532:
	leaq	.LC61(%rip), %rax
	leaq	-144(%rbp), %rsi
	movq	$43, -136(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2563
	.p2align 4,,10
	.p2align 3
.L2533:
	leaq	.LC60(%rip), %rax
	leaq	-160(%rbp), %rsi
	movq	$62, -152(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2563
	.p2align 4,,10
	.p2align 3
.L2534:
	leaq	.LC59(%rip), %rax
	leaq	-176(%rbp), %rsi
	movq	$54, -168(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L2563
	.p2align 4,,10
	.p2align 3
.L2535:
	leaq	.LC58(%rip), %rax
	leaq	-192(%rbp), %rsi
	movq	$45, -184(%rbp)
	movq	%rax, -192(%rbp)
	jmp	.L2563
	.p2align 4,,10
	.p2align 3
.L2536:
	leaq	.LC57(%rip), %rax
	leaq	-208(%rbp), %rsi
	movq	$30, -200(%rbp)
	movq	%rax, -208(%rbp)
	jmp	.L2563
.L2528:
	movq	88(%r12), %r14
	jmp	.L2538
	.p2align 4,,10
	.p2align 3
.L2564:
	call	_ZN2v88internalL33Stats_Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE
	movq	%rax, %r14
	jmp	.L2517
	.p2align 4,,10
	.p2align 3
.L2567:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2527
	.p2align 4,,10
	.p2align 3
.L2568:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2569:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21751:
	.size	_ZN2v88internal27Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal39Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal39Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateE
	.type	_ZN2v88internal39Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateE, @function
_ZN2v88internal39Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateE:
.LFB21757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2579
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L2572
.L2573:
	leaq	.LC51(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2572:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2573
	movq	41472(%rdx), %rdi
	call	_ZN2v88internal5Debug31PerformSideEffectCheckForObjectENS0_6HandleINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L2574
	movq	312(%r12), %r14
.L2575:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2570
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2570:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2574:
	.cfi_restore_state
	movq	88(%r12), %r14
	jmp	.L2575
	.p2align 4,,10
	.p2align 3
.L2579:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL45Stats_Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21757:
	.size	_ZN2v88internal39Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateE, .-_ZN2v88internal39Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal37Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal37Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateE
	.type	_ZN2v88internal37Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateE, @function
_ZN2v88internal37Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateE:
.LFB21760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2590
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal30DisableEmbeddedBlobRefcountingEv@PLT
	xorl	%edi, %edi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal30CreateSnapshotDataBlobInternalENS_15SnapshotCreator20FunctionCodeHandlingEPKcPNS_7IsolateE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2582
	call	_ZdaPv@PLT
.L2582:
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %r14d
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	%r14d, %esi
	leaq	.LC47(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	call	_ZN2v88internal23FreeCurrentEmbeddedBlobEv@PLT
	movq	%r13, 41088(%r12)
	movq	88(%r12), %r14
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2588
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2588:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2590:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL43Stats_Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21760:
	.size	_ZN2v88internal37Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateE, .-_ZN2v88internal37Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateE
	.section	.rodata._ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRKiS7_RKjEEEvDpOT_.str1.1,"aMS",@progbits,1
.LC85:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRKiS7_RKjEEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRKiS7_RKjEEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRKiS7_RKjEEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRKiS7_RKjEEEvDpOT_, @function
_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRKiS7_RKjEEEvDpOT_:
.LFB24252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L2592
	movl	(%rsi), %esi
	movl	(%rdx), %edx
	movl	(%rcx), %eax
	movl	%esi, (%r12)
	movl	%edx, 4(%r12)
	movl	%eax, 8(%r12)
	addq	$12, 8(%rdi)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2592:
	.cfi_restore_state
	movq	(%rdi), %r14
	movq	%r12, %r8
	movabsq	$-6148914691236517205, %rdi
	subq	%r14, %r8
	movq	%r8, %rax
	sarq	$2, %rax
	imulq	%rdi, %rax
	movabsq	$768614336404564650, %rdi
	cmpq	%rdi, %rax
	je	.L2608
	testq	%rax, %rax
	je	.L2600
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L2609
.L2595:
	movq	%r15, %rdi
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	12(%rax), %r9
.L2596:
	movl	(%rdx), %edi
	movl	(%rcx), %edx
	leaq	0(%r13,%r8), %rax
	movl	(%rsi), %ecx
	movl	%edi, 4(%rax)
	movl	%ecx, (%rax)
	movl	%edx, 8(%rax)
	cmpq	%r14, %r12
	je	.L2597
	movq	%r13, %rcx
	movq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L2598:
	movq	(%rdx), %rsi
	addq	$12, %rdx
	addq	$12, %rcx
	movq	%rsi, -12(%rcx)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rcx)
	cmpq	%rdx, %r12
	jne	.L2598
	subq	$12, %r12
	subq	%r14, %r12
	shrq	$2, %r12
	leaq	24(%r13,%r12,4), %r9
.L2597:
	testq	%r14, %r14
	je	.L2599
	movq	%r14, %rdi
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r9
.L2599:
	movq	%r13, %xmm0
	movq	%r9, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2609:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L2610
	movl	$12, %r9d
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L2596
	.p2align 4,,10
	.p2align 3
.L2600:
	movl	$12, %r15d
	jmp	.L2595
.L2608:
	leaq	.LC85(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2610:
	cmpq	%rdi, %r9
	cmovbe	%r9, %rdi
	imulq	$12, %rdi, %r15
	jmp	.L2595
	.cfi_endproc
.LFE24252:
	.size	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRKiS7_RKjEEEvDpOT_, .-_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRKiS7_RKjEEEvDpOT_
	.section	.rodata._ZN2v88internalL34Stats_Runtime_DebugCollectCoverageEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC86:
	.string	"V8.Runtime_Runtime_DebugCollectCoverage"
	.align 8
.LC87:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internalL34Stats_Runtime_DebugCollectCoverageEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_DebugCollectCoverageEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL34Stats_Runtime_DebugCollectCoverageEiPmPNS0_7IsolateE.isra.0:
.LFB27967:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2727
.L2612:
	movq	_ZZN2v88internalL34Stats_Runtime_DebugCollectCoverageEiPmPNS0_7IsolateEE28trace_event_unique_atomic682(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2728
.L2614:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2729
.L2616:
	movq	41088(%r15), %rax
	movl	41832(%r15), %edx
	movq	%r15, %rsi
	addl	$1, 41104(%r15)
	movq	%rax, -320(%rbp)
	movq	41096(%r15), %rax
	testl	%edx, %edx
	movq	%rax, -328(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	%rax, %rdi
	jne	.L2620
	call	_ZN2v88internal8Coverage17CollectBestEffortEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %rax
	movq	%rax, -288(%rbp)
.L2621:
	movq	8(%rax), %rbx
	subq	(%rax), %rbx
	xorl	%edx, %edx
	movq	%r15, %rdi
	sarq	$5, %rbx
	movl	%ebx, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	$0, -256(%rbp)
	movq	%rax, -296(%rbp)
	leaq	3232(%r15), %rax
	movq	%rax, -304(%rbp)
	leal	-1(%rbx), %eax
	movq	%rax, -312(%rbp)
	testl	%ebx, %ebx
	jle	.L2653
	.p2align 4,,10
	.p2align 3
.L2655:
	movq	-288(%rbp), %rdi
	movq	(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	%rdi, %rdx
	movq	%rdi, -200(%rbp)
	subq	%rax, %rdx
	sarq	$5, %rdx
	cmpq	-256(%rbp), %rdx
	jbe	.L2730
	movq	-256(%rbp), %rdx
	movq	41088(%r15), %rdi
	pxor	%xmm0, %xmm0
	movq	$0, -176(%rbp)
	addl	$1, 41104(%r15)
	salq	$5, %rdx
	movaps	%xmm0, -192(%rbp)
	addq	%rdx, %rax
	movq	%rdi, -272(%rbp)
	movq	41096(%r15), %rdi
	movq	8(%rax), %rsi
	movq	%rax, -232(%rbp)
	movq	16(%rax), %rax
	movq	%rdi, -264(%rbp)
	movabsq	$7905747460161236407, %rdi
	movq	%rax, -200(%rbp)
	subq	%rsi, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	testl	%eax, %eax
	jle	.L2626
	leal	-1(%rax), %edx
	xorl	%r12d, %r12d
	movq	%r15, -280(%rbp)
	movabsq	$-6148914691236517205, %r14
	leaq	0(,%rdx,8), %rax
	subq	%rdx, %rax
	salq	$3, %rax
	movq	%rax, -240(%rbp)
	.p2align 4,,10
	.p2align 3
.L2640:
	leaq	(%rsi,%r12), %r15
	movq	-248(%rbp), %rdi
	xorl	%r13d, %r13d
	leaq	8(%r15), %rcx
	leaq	4(%r15), %rdx
	movq	%r15, %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRKiS7_RKjEEEvDpOT_
	movq	24(%r15), %rcx
	movq	-184(%rbp), %rbx
	cmpq	32(%r15), %rcx
	je	.L2639
	movq	%r12, -224(%rbp)
	movq	%r15, %r12
	jmp	.L2638
	.p2align 4,,10
	.p2align 3
.L2732:
	movl	8(%r15), %eax
	movq	(%r15), %rdx
	movl	%eax, 8(%rbx)
	movq	%rdx, (%rbx)
	movq	-184(%rbp), %rax
	leaq	12(%rax), %rbx
	movq	%rbx, -184(%rbp)
.L2631:
	movq	24(%r12), %rcx
	movq	32(%r12), %rax
	addq	$1, %r13
	subq	%rcx, %rax
	sarq	$2, %rax
	imulq	%r14, %rax
	cmpq	%r13, %rax
	jbe	.L2731
.L2638:
	leaq	0(%r13,%r13,2), %rax
	leaq	(%rcx,%rax,4), %r15
	cmpq	%rbx, -176(%rbp)
	jne	.L2732
	movq	-192(%rbp), %r8
	movq	%rbx, %rdx
	movabsq	$768614336404564650, %rdi
	subq	%r8, %rdx
	movq	%rdx, %rax
	sarq	$2, %rax
	imulq	%r14, %rax
	cmpq	%rdi, %rax
	je	.L2733
	testq	%rax, %rax
	je	.L2674
	movabsq	$9223372036854775800, %r9
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L2734
.L2633:
	movq	%r9, %rdi
	movq	%rdx, -216(%rbp)
	movq	%r8, -208(%rbp)
	movq	%r9, -200(%rbp)
	call	_Znwm@PLT
	movq	-200(%rbp), %r9
	movq	-208(%rbp), %r8
	movq	-216(%rbp), %rdx
	leaq	12(%rax), %r10
	addq	%rax, %r9
.L2634:
	movl	8(%r15), %ecx
	movq	(%r15), %rdi
	addq	%rax, %rdx
	movq	%rdi, (%rdx)
	movl	%ecx, 8(%rdx)
	cmpq	%rbx, %r8
	je	.L2677
	movq	%rax, %rcx
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L2636:
	movq	(%rdx), %rsi
	addq	$12, %rdx
	addq	$12, %rcx
	movq	%rsi, -12(%rcx)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rcx)
	cmpq	%rbx, %rdx
	jne	.L2636
	movabsq	$3074457345618258603, %rdi
	subq	%r8, %rbx
	leaq	-12(%rbx), %rdx
	shrq	$2, %rdx
	imulq	%rdi, %rdx
	movabsq	$4611686018427387903, %rdi
	andq	%rdi, %rdx
	leaq	6(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,4), %rbx
.L2635:
	testq	%r8, %r8
	je	.L2637
	movq	%r8, %rdi
	movq	%rax, -208(%rbp)
	movq	%r9, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-208(%rbp), %rax
	movq	-200(%rbp), %r9
.L2637:
	movq	%rax, %xmm0
	movq	%rbx, %xmm1
	movq	%r9, -176(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -192(%rbp)
	jmp	.L2631
	.p2align 4,,10
	.p2align 3
.L2731:
	movq	-224(%rbp), %r12
.L2639:
	cmpq	-240(%rbp), %r12
	je	.L2735
	movq	-232(%rbp), %rax
	addq	$56, %r12
	movq	8(%rax), %rsi
	jmp	.L2640
	.p2align 4,,10
	.p2align 3
.L2734:
	testq	%rcx, %rcx
	jne	.L2736
	movl	$12, %r10d
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	jmp	.L2634
	.p2align 4,,10
	.p2align 3
.L2674:
	movl	$12, %r9d
	jmp	.L2633
	.p2align 4,,10
	.p2align 3
.L2677:
	movq	%r10, %rbx
	jmp	.L2635
	.p2align 4,,10
	.p2align 3
.L2735:
	movq	-184(%rbp), %rbx
	subq	-192(%rbp), %rbx
	xorl	%edx, %edx
	movabsq	$-6148914691236517205, %rax
	sarq	$2, %rbx
	movq	-280(%rbp), %r15
	imulq	%rax, %rbx
	movq	%r15, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.L2641
	leal	-1(%rbx), %eax
	movl	$16, %ebx
	leaq	3(%rax,%rax,2), %r13
	leaq	0(,%r13,4), %rax
	xorl	%r13d, %r13d
	movq	%rax, -200(%rbp)
	.p2align 4,,10
	.p2align 3
.L2645:
	movq	-192(%rbp), %rsi
	movq	%r15, %rdi
	addq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_115MakeRangeObjectEPNS0_7IsolateERKNS0_13CoverageBlockE
	movq	(%r12), %rdi
	movq	(%rax), %r14
	leaq	-1(%rdi,%rbx), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L2670
	movq	%r14, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -208(%rbp)
	testl	$262144, %eax
	je	.L2643
	movq	%r14, %rdx
	movq	%rsi, -224(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-208(%rbp), %r8
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdi
	movq	8(%r8), %rax
.L2643:
	testb	$24, %al
	je	.L2670
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2670
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2670:
	addq	$12, %r13
	addq	$8, %rbx
	cmpq	-200(%rbp), %r13
	jne	.L2645
.L2641:
	movq	(%r12), %rax
	movq	%r12, %rsi
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movl	$2, %edx
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rbx
	movq	-232(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2646
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2647:
	movq	-304(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-296(%rbp), %rax
	movq	(%rbx), %r12
	movq	(%rax), %r13
	movq	-256(%rbp), %rax
	leaq	15(%r13,%rax,8), %r14
	movq	%r12, (%r14)
	testb	$1, %r12b
	je	.L2669
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L2650
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L2650:
	testb	$24, %al
	je	.L2669
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2669
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2669:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2652
	call	_ZdlPv@PLT
.L2652:
	movq	-272(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-264(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L2737
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %rdi
	leaq	1(%rdi), %rax
	cmpq	%rdi, -312(%rbp)
	je	.L2653
.L2654:
	movq	%rax, -256(%rbp)
	jmp	.L2655
.L2737:
	movq	-256(%rbp), %rdi
	leaq	1(%rdi), %rax
	cmpq	%rdi, -312(%rbp)
	jne	.L2654
.L2653:
	movq	-296(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r15, %rdi
	movq	(%rsi), %rax
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %rax
	movq	%rax, -200(%rbp)
	movq	-288(%rbp), %rax
	movq	8(%rax), %r13
	movq	(%rax), %r12
	cmpq	%r12, %r13
	je	.L2623
.L2624:
	movq	16(%r12), %rbx
	movq	8(%r12), %r14
	cmpq	%r14, %rbx
	je	.L2656
	.p2align 4,,10
	.p2align 3
.L2660:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2657
	call	_ZdlPv@PLT
	addq	$56, %r14
	cmpq	%rbx, %r14
	jne	.L2660
.L2658:
	movq	8(%r12), %r14
.L2656:
	testq	%r14, %r14
	je	.L2661
	movq	%r14, %rdi
	addq	$32, %r12
	call	_ZdlPv@PLT
	cmpq	%r13, %r12
	jne	.L2624
	movq	-288(%rbp), %rax
	movq	(%rax), %r12
.L2623:
	testq	%r12, %r12
	je	.L2664
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2664:
	movq	-288(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	-320(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-328(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L2665
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2665:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2738
.L2611:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2739
	movq	-200(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2657:
	.cfi_restore_state
	addq	$56, %r14
	cmpq	%r14, %rbx
	jne	.L2660
	jmp	.L2658
.L2646:
	movq	41088(%r15), %rcx
	cmpq	41096(%r15), %rcx
	je	.L2740
.L2648:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L2647
.L2661:
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L2624
	movq	-288(%rbp), %rax
	movq	(%rax), %r12
	jmp	.L2623
.L2626:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	jmp	.L2641
.L2740:
	movq	%r15, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L2648
.L2620:
	call	_ZN2v88internal8Coverage14CollectPreciseEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %rax
	movq	%rax, -288(%rbp)
	jmp	.L2621
.L2729:
	pxor	%xmm0, %xmm0
	xorl	%r12d, %r12d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2741
.L2617:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2618
	movq	(%rdi), %rax
	call	*8(%rax)
.L2618:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2619
	movq	(%rdi), %rax
	call	*8(%rax)
.L2619:
	leaq	.LC86(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2616
.L2728:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2742
.L2615:
	movq	%rbx, _ZZN2v88internalL34Stats_Runtime_DebugCollectCoverageEiPmPNS0_7IsolateEE28trace_event_unique_atomic682(%rip)
	jmp	.L2614
.L2727:
	movq	40960(%rdi), %rax
	leaq	-120(%rbp), %rsi
	movl	$272, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2612
.L2738:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2611
.L2741:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC86(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r12
	addq	$64, %rsp
	jmp	.L2617
.L2742:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2615
.L2733:
	leaq	.LC85(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2736:
	movabsq	$768614336404564650, %rax
	cmpq	%rax, %rcx
	movq	%rax, %r9
	cmovbe	%rcx, %r9
	imulq	$12, %r9, %r9
	jmp	.L2633
	.p2align 4,,10
	.p2align 3
.L2730:
	movq	-256(%rbp), %rsi
	leaq	.LC87(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2739:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27967:
	.size	_ZN2v88internalL34Stats_Runtime_DebugCollectCoverageEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL34Stats_Runtime_DebugCollectCoverageEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal28Runtime_DebugCollectCoverageEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_DebugCollectCoverageEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_DebugCollectCoverageEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_DebugCollectCoverageEiPmPNS0_7IsolateE:
.LFB21700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2835
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%r15, %rsi
	movq	%rax, -208(%rbp)
	movq	41096(%rdx), %rax
	movl	41832(%rdx), %edx
	movq	%rax, -216(%rbp)
	leaq	-80(%rbp), %rax
	testl	%edx, %edx
	movq	%rax, -136(%rbp)
	movq	%rax, %rdi
	je	.L2836
	call	_ZN2v88internal8Coverage14CollectPreciseEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rax
	movq	%rax, -176(%rbp)
.L2747:
	movq	8(%rax), %rbx
	subq	(%rax), %rbx
	xorl	%edx, %edx
	movq	%r15, %rdi
	sarq	$5, %rbx
	movl	%ebx, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	$0, -144(%rbp)
	movq	%rax, -184(%rbp)
	leaq	3232(%r15), %rax
	movq	%rax, -192(%rbp)
	leal	-1(%rbx), %eax
	movq	%rax, -200(%rbp)
	testl	%ebx, %ebx
	jle	.L2779
	.p2align 4,,10
	.p2align 3
.L2781:
	movq	-176(%rbp), %rdi
	movq	(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	%rdi, %rdx
	movq	%rdi, -88(%rbp)
	subq	%rax, %rdx
	sarq	$5, %rdx
	cmpq	-144(%rbp), %rdx
	jbe	.L2837
	movq	-144(%rbp), %rdx
	movq	41088(%r15), %rdi
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	addl	$1, 41104(%r15)
	salq	$5, %rdx
	movaps	%xmm0, -80(%rbp)
	addq	%rdx, %rax
	movq	%rdi, -160(%rbp)
	movq	41096(%r15), %rdi
	movq	8(%rax), %rsi
	movq	%rax, -120(%rbp)
	movq	16(%rax), %rax
	movq	%rdi, -152(%rbp)
	movabsq	$7905747460161236407, %rdi
	movq	%rax, -88(%rbp)
	subq	%rsi, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	testl	%eax, %eax
	jle	.L2752
	leal	-1(%rax), %edx
	xorl	%r12d, %r12d
	movq	%r15, -168(%rbp)
	movabsq	$-6148914691236517205, %r14
	leaq	0(,%rdx,8), %rax
	subq	%rdx, %rax
	salq	$3, %rax
	movq	%rax, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L2766:
	leaq	(%rsi,%r12), %r15
	movq	-136(%rbp), %rdi
	xorl	%r13d, %r13d
	leaq	8(%r15), %rcx
	leaq	4(%r15), %rdx
	movq	%r15, %rsi
	call	_ZNSt6vectorIN2v88internal13CoverageBlockESaIS2_EE12emplace_backIJRKiS7_RKjEEEvDpOT_
	movq	24(%r15), %rcx
	movq	-72(%rbp), %rbx
	cmpq	32(%r15), %rcx
	je	.L2765
	movq	%r12, -112(%rbp)
	movq	%r15, %r12
	jmp	.L2764
	.p2align 4,,10
	.p2align 3
.L2839:
	movl	8(%r15), %eax
	movq	(%r15), %rdx
	movl	%eax, 8(%rbx)
	movq	%rdx, (%rbx)
	movq	-72(%rbp), %rax
	leaq	12(%rax), %rbx
	movq	%rbx, -72(%rbp)
.L2757:
	movq	24(%r12), %rcx
	movq	32(%r12), %rax
	addq	$1, %r13
	subq	%rcx, %rax
	sarq	$2, %rax
	imulq	%r14, %rax
	cmpq	%rax, %r13
	jnb	.L2838
.L2764:
	leaq	0(%r13,%r13,2), %rax
	leaq	(%rcx,%rax,4), %r15
	cmpq	%rbx, -64(%rbp)
	jne	.L2839
	movq	-80(%rbp), %r8
	movq	%rbx, %rdx
	movabsq	$768614336404564650, %rdi
	subq	%r8, %rdx
	movq	%rdx, %rax
	sarq	$2, %rax
	imulq	%r14, %rax
	cmpq	%rdi, %rax
	je	.L2840
	testq	%rax, %rax
	je	.L2797
	movabsq	$9223372036854775800, %r9
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L2841
.L2759:
	movq	%r9, %rdi
	movq	%rdx, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %rdx
	leaq	12(%rax), %r10
	addq	%rax, %r9
.L2760:
	movl	8(%r15), %ecx
	movq	(%r15), %rdi
	addq	%rax, %rdx
	movq	%rdi, (%rdx)
	movl	%ecx, 8(%rdx)
	cmpq	%rbx, %r8
	je	.L2800
	movq	%rax, %rcx
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L2762:
	movq	(%rdx), %rsi
	addq	$12, %rdx
	addq	$12, %rcx
	movq	%rsi, -12(%rcx)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rcx)
	cmpq	%rbx, %rdx
	jne	.L2762
	movabsq	$3074457345618258603, %rdi
	subq	%r8, %rbx
	leaq	-12(%rbx), %rdx
	shrq	$2, %rdx
	imulq	%rdi, %rdx
	movabsq	$4611686018427387903, %rdi
	andq	%rdi, %rdx
	leaq	6(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,4), %rbx
.L2761:
	testq	%r8, %r8
	je	.L2763
	movq	%r8, %rdi
	movq	%rax, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %r9
.L2763:
	movq	%rax, %xmm0
	movq	%rbx, %xmm1
	movq	%r9, -64(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	jmp	.L2757
.L2836:
	call	_ZN2v88internal8Coverage17CollectBestEffortEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rax
	movq	%rax, -176(%rbp)
	jmp	.L2747
	.p2align 4,,10
	.p2align 3
.L2838:
	movq	-112(%rbp), %r12
.L2765:
	cmpq	%r12, -128(%rbp)
	je	.L2842
	movq	-120(%rbp), %rax
	addq	$56, %r12
	movq	8(%rax), %rsi
	jmp	.L2766
	.p2align 4,,10
	.p2align 3
.L2841:
	testq	%rcx, %rcx
	jne	.L2843
	movl	$12, %r10d
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	jmp	.L2760
	.p2align 4,,10
	.p2align 3
.L2797:
	movl	$12, %r9d
	jmp	.L2759
	.p2align 4,,10
	.p2align 3
.L2800:
	movq	%r10, %rbx
	jmp	.L2761
	.p2align 4,,10
	.p2align 3
.L2842:
	movq	-72(%rbp), %rbx
	subq	-80(%rbp), %rbx
	xorl	%edx, %edx
	movabsq	$-6148914691236517205, %rax
	sarq	$2, %rbx
	movq	-168(%rbp), %r15
	imulq	%rax, %rbx
	movq	%r15, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.L2767
	leal	-1(%rbx), %eax
	movl	$16, %ebx
	leaq	3(%rax,%rax,2), %r13
	leaq	0(,%r13,4), %rax
	xorl	%r13d, %r13d
	movq	%rax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L2771:
	movq	-80(%rbp), %rsi
	movq	%r15, %rdi
	addq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_115MakeRangeObjectEPNS0_7IsolateERKNS0_13CoverageBlockE
	movq	(%r12), %rdi
	movq	(%rax), %r14
	leaq	-1(%rdi,%rbx), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L2795
	movq	%r14, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -96(%rbp)
	testl	$262144, %eax
	je	.L2769
	movq	%r14, %rdx
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %r8
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	8(%r8), %rax
.L2769:
	testb	$24, %al
	je	.L2795
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2795
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2795:
	addq	$12, %r13
	addq	$8, %rbx
	cmpq	-88(%rbp), %r13
	jne	.L2771
.L2767:
	movq	(%r12), %rax
	movq	%r12, %rsi
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movl	$2, %edx
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rbx
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2772
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2773:
	movq	-192(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-184(%rbp), %rax
	movq	(%rbx), %r12
	movq	(%rax), %r13
	movq	-144(%rbp), %rax
	leaq	15(%r13,%rax,8), %r14
	movq	%r12, (%r14)
	testb	$1, %r12b
	je	.L2794
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L2776
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L2776:
	testb	$24, %al
	je	.L2794
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2794
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2794:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2778
	call	_ZdlPv@PLT
.L2778:
	movq	-160(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-152(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L2844
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rdi
	leaq	1(%rdi), %rax
	cmpq	%rdi, -200(%rbp)
	je	.L2779
.L2780:
	movq	%rax, -144(%rbp)
	jmp	.L2781
.L2844:
	movq	-144(%rbp), %rdi
	leaq	1(%rdi), %rax
	cmpq	%rdi, -200(%rbp)
	jne	.L2780
.L2779:
	movq	-184(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r15, %rdi
	movq	(%rsi), %rax
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
	movq	-176(%rbp), %rax
	movq	8(%rax), %r13
	movq	(%rax), %r12
	cmpq	%r12, %r13
	je	.L2749
.L2750:
	movq	16(%r12), %rbx
	movq	8(%r12), %r14
	cmpq	%r14, %rbx
	je	.L2782
	.p2align 4,,10
	.p2align 3
.L2786:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2783
	call	_ZdlPv@PLT
	addq	$56, %r14
	cmpq	%rbx, %r14
	jne	.L2786
.L2784:
	movq	8(%r12), %r14
.L2782:
	testq	%r14, %r14
	je	.L2787
	movq	%r14, %rdi
	addq	$32, %r12
	call	_ZdlPv@PLT
	cmpq	%r13, %r12
	jne	.L2750
.L2788:
	movq	-176(%rbp), %rax
	movq	(%rax), %r12
.L2749:
	testq	%r12, %r12
	je	.L2790
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2790:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	-208(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-216(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L2743
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2743:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2845
	movq	-88(%rbp), %rax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2783:
	.cfi_restore_state
	addq	$56, %r14
	cmpq	%r14, %rbx
	jne	.L2786
	jmp	.L2784
.L2772:
	movq	41088(%r15), %rcx
	cmpq	41096(%r15), %rcx
	je	.L2846
.L2774:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L2773
.L2787:
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L2750
	jmp	.L2788
.L2752:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	jmp	.L2767
.L2846:
	movq	%r15, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L2774
.L2835:
	movq	%rdx, %rdi
	call	_ZN2v88internalL34Stats_Runtime_DebugCollectCoverageEiPmPNS0_7IsolateE.isra.0
	movq	%rax, -88(%rbp)
	jmp	.L2743
.L2840:
	leaq	.LC85(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2843:
	movabsq	$768614336404564650, %rax
	cmpq	%rax, %rcx
	movq	%rax, %r9
	cmovbe	%rcx, %r9
	imulq	$12, %r9, %r9
	jmp	.L2759
.L2845:
	call	__stack_chk_fail@PLT
.L2837:
	movq	-144(%rbp), %rsi
	leaq	.LC87(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE21700:
	.size	_ZN2v88internal28Runtime_DebugCollectCoverageEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_DebugCollectCoverageEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal28Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal28Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal28Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE:
.LFB27877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27877:
	.size	_GLOBAL__sub_I__ZN2v88internal28Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal28Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal28Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL43Stats_Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateEE28trace_event_unique_atomic843,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Stats_Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateEE28trace_event_unique_atomic843, @object
	.size	_ZZN2v88internalL43Stats_Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateEE28trace_event_unique_atomic843, 8
_ZZN2v88internalL43Stats_Runtime_ProfileCreateSnapshotDataBlobEiPmPNS0_7IsolateEE28trace_event_unique_atomic843:
	.zero	8
	.section	.bss._ZZN2v88internalL45Stats_Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic830,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL45Stats_Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic830, @object
	.size	_ZZN2v88internalL45Stats_Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic830, 8
_ZZN2v88internalL45Stats_Runtime_PerformSideEffectCheckForObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic830:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateEE28trace_event_unique_atomic791,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateEE28trace_event_unique_atomic791, @object
	.size	_ZZN2v88internalL33Stats_Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateEE28trace_event_unique_atomic791, 8
_ZZN2v88internalL33Stats_Runtime_LiveEditPatchScriptEiPmPNS0_7IsolateEE28trace_event_unique_atomic791:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateEE28trace_event_unique_atomic778,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateEE28trace_event_unique_atomic778, @object
	.size	_ZZN2v88internalL40Stats_Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateEE28trace_event_unique_atomic778, 8
_ZZN2v88internalL40Stats_Runtime_DebugAsyncFunctionFinishedEiPmPNS0_7IsolateEE28trace_event_unique_atomic778:
	.zero	8
	.section	.bss._ZZN2v88internalL39Stats_Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateEE28trace_event_unique_atomic770,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Stats_Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateEE28trace_event_unique_atomic770, @object
	.size	_ZZN2v88internalL39Stats_Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateEE28trace_event_unique_atomic770, 8
_ZZN2v88internalL39Stats_Runtime_DebugAsyncFunctionResumedEiPmPNS0_7IsolateEE28trace_event_unique_atomic770:
	.zero	8
	.section	.bss._ZZN2v88internalL41Stats_Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateEE28trace_event_unique_atomic761,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Stats_Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateEE28trace_event_unique_atomic761, @object
	.size	_ZZN2v88internalL41Stats_Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateEE28trace_event_unique_atomic761, 8
_ZZN2v88internalL41Stats_Runtime_DebugAsyncFunctionSuspendedEiPmPNS0_7IsolateEE28trace_event_unique_atomic761:
	.zero	8
	.section	.bss._ZZN2v88internalL39Stats_Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateEE28trace_event_unique_atomic751,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Stats_Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateEE28trace_event_unique_atomic751, @object
	.size	_ZZN2v88internalL39Stats_Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateEE28trace_event_unique_atomic751, 8
_ZZN2v88internalL39Stats_Runtime_DebugAsyncFunctionEnteredEiPmPNS0_7IsolateEE28trace_event_unique_atomic751:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_IncBlockCounterEiPmPNS0_7IsolateEE28trace_event_unique_atomic747,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_IncBlockCounterEiPmPNS0_7IsolateEE28trace_event_unique_atomic747, @object
	.size	_ZZN2v88internalL29Stats_Runtime_IncBlockCounterEiPmPNS0_7IsolateEE28trace_event_unique_atomic747, 8
_ZZN2v88internalL29Stats_Runtime_IncBlockCounterEiPmPNS0_7IsolateEE28trace_event_unique_atomic747:
	.zero	8
	.section	.bss._ZZN2v88internalL38Stats_Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateEE28trace_event_unique_atomic739,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Stats_Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateEE28trace_event_unique_atomic739, @object
	.size	_ZZN2v88internalL38Stats_Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateEE28trace_event_unique_atomic739, 8
_ZZN2v88internalL38Stats_Runtime_DebugToggleBlockCoverageEiPmPNS0_7IsolateEE28trace_event_unique_atomic739:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateEE28trace_event_unique_atomic731,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateEE28trace_event_unique_atomic731, @object
	.size	_ZZN2v88internalL40Stats_Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateEE28trace_event_unique_atomic731, 8
_ZZN2v88internalL40Stats_Runtime_DebugTogglePreciseCoverageEiPmPNS0_7IsolateEE28trace_event_unique_atomic731:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_DebugCollectCoverageEiPmPNS0_7IsolateEE28trace_event_unique_atomic682,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_DebugCollectCoverageEiPmPNS0_7IsolateEE28trace_event_unique_atomic682, @object
	.size	_ZZN2v88internalL34Stats_Runtime_DebugCollectCoverageEiPmPNS0_7IsolateEE28trace_event_unique_atomic682, 8
_ZZN2v88internalL34Stats_Runtime_DebugCollectCoverageEiPmPNS0_7IsolateEE28trace_event_unique_atomic682:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_DebugPopPromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic655,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_DebugPopPromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic655, @object
	.size	_ZZN2v88internalL29Stats_Runtime_DebugPopPromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic655, 8
_ZZN2v88internalL29Stats_Runtime_DebugPopPromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic655:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_DebugPushPromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic646,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_DebugPushPromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic646, @object
	.size	_ZZN2v88internalL30Stats_Runtime_DebugPushPromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic646, 8
_ZZN2v88internalL30Stats_Runtime_DebugPushPromiseEiPmPNS0_7IsolateEE28trace_event_unique_atomic646:
	.zero	8
	.section	.bss._ZZN2v88internalL50Stats_Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateEE28trace_event_unique_atomic639,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL50Stats_Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateEE28trace_event_unique_atomic639, @object
	.size	_ZZN2v88internalL50Stats_Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateEE28trace_event_unique_atomic639, 8
_ZZN2v88internalL50Stats_Runtime_DebugPrepareStepInSuspendedGeneratorEiPmPNS0_7IsolateEE28trace_event_unique_atomic639:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic617,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic617, @object
	.size	_ZZN2v88internalL33Stats_Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic617, 8
_ZZN2v88internalL33Stats_Runtime_DebugOnFunctionCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic617:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateEE28trace_event_unique_atomic601,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateEE28trace_event_unique_atomic601, @object
	.size	_ZZN2v88internalL37Stats_Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateEE28trace_event_unique_atomic601, 8
_ZZN2v88internalL37Stats_Runtime_ScriptLocationFromLine2EiPmPNS0_7IsolateEE28trace_event_unique_atomic601:
	.zero	8
	.section	.bss._ZZN2v88internalL26Stats_Runtime_GetHeapUsageEiPmPNS0_7IsolateEE28trace_event_unique_atomic478,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL26Stats_Runtime_GetHeapUsageEiPmPNS0_7IsolateEE28trace_event_unique_atomic478, @object
	.size	_ZZN2v88internalL26Stats_Runtime_GetHeapUsageEiPmPNS0_7IsolateEE28trace_event_unique_atomic478, 8
_ZZN2v88internalL26Stats_Runtime_GetHeapUsageEiPmPNS0_7IsolateEE28trace_event_unique_atomic478:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_CollectGarbageEiPmPNS0_7IsolateEE28trace_event_unique_atomic468,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_CollectGarbageEiPmPNS0_7IsolateEE28trace_event_unique_atomic468, @object
	.size	_ZZN2v88internalL28Stats_Runtime_CollectGarbageEiPmPNS0_7IsolateEE28trace_event_unique_atomic468, 8
_ZZN2v88internalL28Stats_Runtime_CollectGarbageEiPmPNS0_7IsolateEE28trace_event_unique_atomic468:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic454,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic454, @object
	.size	_ZZN2v88internalL37Stats_Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic454, 8
_ZZN2v88internalL37Stats_Runtime_FunctionGetInferredNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic454:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateEE28trace_event_unique_atomic432,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateEE28trace_event_unique_atomic432, @object
	.size	_ZZN2v88internalL37Stats_Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateEE28trace_event_unique_atomic432, 8
_ZZN2v88internalL37Stats_Runtime_DebugGetLoadedScriptIdsEiPmPNS0_7IsolateEE28trace_event_unique_atomic432:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_ClearSteppingEiPmPNS0_7IsolateEE28trace_event_unique_atomic424,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_ClearSteppingEiPmPNS0_7IsolateEE28trace_event_unique_atomic424, @object
	.size	_ZZN2v88internalL27Stats_Runtime_ClearSteppingEiPmPNS0_7IsolateEE28trace_event_unique_atomic424, 8
_ZZN2v88internalL27Stats_Runtime_ClearSteppingEiPmPNS0_7IsolateEE28trace_event_unique_atomic424:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateEE28trace_event_unique_atomic413,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateEE28trace_event_unique_atomic413, @object
	.size	_ZZN2v88internalL32Stats_Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateEE28trace_event_unique_atomic413, 8
_ZZN2v88internalL32Stats_Runtime_IsBreakOnExceptionEiPmPNS0_7IsolateEE28trace_event_unique_atomic413:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_GetBreakLocationsEiPmPNS0_7IsolateEE28trace_event_unique_atomic392,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_GetBreakLocationsEiPmPNS0_7IsolateEE28trace_event_unique_atomic392, @object
	.size	_ZZN2v88internalL31Stats_Runtime_GetBreakLocationsEiPmPNS0_7IsolateEE28trace_event_unique_atomic392, 8
_ZZN2v88internalL31Stats_Runtime_GetBreakLocationsEiPmPNS0_7IsolateEE28trace_event_unique_atomic392:
	.zero	8
	.section	.bss._ZZN2v88internalL44Stats_Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateEE28trace_event_unique_atomic379,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Stats_Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateEE28trace_event_unique_atomic379, @object
	.size	_ZZN2v88internalL44Stats_Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateEE28trace_event_unique_atomic379, 8
_ZZN2v88internalL44Stats_Runtime_SetGeneratorScopeVariableValueEiPmPNS0_7IsolateEE28trace_event_unique_atomic379:
	.zero	8
	.section	.bss._ZZN2v88internalL38Stats_Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateEE28trace_event_unique_atomic330,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Stats_Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateEE28trace_event_unique_atomic330, @object
	.size	_ZZN2v88internalL38Stats_Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateEE28trace_event_unique_atomic330, 8
_ZZN2v88internalL38Stats_Runtime_GetGeneratorScopeDetailsEiPmPNS0_7IsolateEE28trace_event_unique_atomic330:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateEE28trace_event_unique_atomic307,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateEE28trace_event_unique_atomic307, @object
	.size	_ZZN2v88internalL36Stats_Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateEE28trace_event_unique_atomic307, 8
_ZZN2v88internalL36Stats_Runtime_GetGeneratorScopeCountEiPmPNS0_7IsolateEE28trace_event_unique_atomic307:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_ScheduleBreakEiPmPNS0_7IsolateEE28trace_event_unique_atomic142,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_ScheduleBreakEiPmPNS0_7IsolateEE28trace_event_unique_atomic142, @object
	.size	_ZZN2v88internalL27Stats_Runtime_ScheduleBreakEiPmPNS0_7IsolateEE28trace_event_unique_atomic142, 8
_ZZN2v88internalL27Stats_Runtime_ScheduleBreakEiPmPNS0_7IsolateEE28trace_event_unique_atomic142:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateEE28trace_event_unique_atomic133,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateEE28trace_event_unique_atomic133, @object
	.size	_ZZN2v88internalL37Stats_Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateEE28trace_event_unique_atomic133, 8
_ZZN2v88internalL37Stats_Runtime_HandleDebuggerStatementEiPmPNS0_7IsolateEE28trace_event_unique_atomic133:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateEE28trace_event_unique_atomic109,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateEE28trace_event_unique_atomic109, @object
	.size	_ZZN2v88internalL31Stats_Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateEE28trace_event_unique_atomic109, 8
_ZZN2v88internalL31Stats_Runtime_DebugBreakAtEntryEiPmPNS0_7IsolateEE28trace_event_unique_atomic109:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateEE27trace_event_unique_atomic36,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateEE27trace_event_unique_atomic36, @object
	.size	_ZZN2v88internalL34Stats_Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateEE27trace_event_unique_atomic36, 8
_ZZN2v88internalL34Stats_Runtime_DebugBreakOnBytecodeEiPmPNS0_7IsolateEE27trace_event_unique_atomic36:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC15:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC16:
	.long	4294967295
	.long	2146435071
	.align 8
.LC17:
	.long	4290772992
	.long	1105199103
	.align 8
.LC18:
	.long	0
	.long	-1042284544
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
