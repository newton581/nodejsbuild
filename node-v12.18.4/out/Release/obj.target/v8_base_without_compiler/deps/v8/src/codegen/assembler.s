	.file	"assembler.cc"
	.text
	.section	.text._ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv,"axG",@progbits,_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.type	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv, @function
_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv:
.LFB6884:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6884:
	.size	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv, .-_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.section	.text._ZNK2v88internal12_GLOBAL__N_122DefaultAssemblerBuffer5startEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal12_GLOBAL__N_122DefaultAssemblerBuffer5startEv, @function
_ZNK2v88internal12_GLOBAL__N_122DefaultAssemblerBuffer5startEv:
.LFB21512:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE21512:
	.size	_ZNK2v88internal12_GLOBAL__N_122DefaultAssemblerBuffer5startEv, .-_ZNK2v88internal12_GLOBAL__N_122DefaultAssemblerBuffer5startEv
	.section	.text._ZNK2v88internal12_GLOBAL__N_122DefaultAssemblerBuffer4sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal12_GLOBAL__N_122DefaultAssemblerBuffer4sizeEv, @function
_ZNK2v88internal12_GLOBAL__N_122DefaultAssemblerBuffer4sizeEv:
.LFB21513:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE21513:
	.size	_ZNK2v88internal12_GLOBAL__N_122DefaultAssemblerBuffer4sizeEv, .-_ZNK2v88internal12_GLOBAL__N_122DefaultAssemblerBuffer4sizeEv
	.section	.text._ZNK2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl5startEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl5startEv, @function
_ZNK2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl5startEv:
.LFB21520:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE21520:
	.size	_ZNK2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl5startEv, .-_ZNK2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl5startEv
	.section	.text._ZNK2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl4sizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl4sizeEv, @function
_ZNK2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl4sizeEv:
.LFB21521:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	ret
	.cfi_endproc
.LFE21521:
	.size	_ZNK2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl4sizeEv, .-_ZNK2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl4sizeEv
	.section	.text._ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplD2Ev, @function
_ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplD2Ev:
.LFB25388:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE25388:
	.size	_ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplD2Ev, .-_ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplD2Ev
	.set	_ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplD1Ev,_ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplD2Ev
	.section	.rodata._ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl4GrowEi.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Cannot grow external assembler buffer"
	.section	.text._ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl4GrowEi,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl4GrowEi, @function
_ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl4GrowEi:
.LFB21522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21522:
	.size	_ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl4GrowEi, .-_ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl4GrowEi
	.section	.text._ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplD0Ev, @function
_ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplD0Ev:
.LFB25390:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25390:
	.size	_ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplD0Ev, .-_ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplD0Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferD2Ev, @function
_ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferD2Ev:
.LFB25354:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L11
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	ret
	.cfi_endproc
.LFE25354:
	.size	_ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferD2Ev, .-_ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferD2Ev
	.set	_ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferD1Ev,_ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferD2Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferD0Ev, @function
_ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferD0Ev:
.LFB25356:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L14
	call	_ZdaPv@PLT
.L14:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25356:
	.size	_ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferD0Ev, .-_ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferD0Ev
	.section	.text._ZN2v88internal13AssemblerBaseD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13AssemblerBaseD2Ev
	.type	_ZN2v88internal13AssemblerBaseD2Ev, @function
_ZN2v88internal13AssemblerBaseD2Ev:
.LFB21595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal13AssemblerBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	136(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L23
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L20
.L23:
	movq	128(%rbx), %rax
	movq	120(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	120(%rbx), %rdi
	leaq	168(%rbx), %rax
	movq	$0, 144(%rbx)
	movq	$0, 136(%rbx)
	cmpq	%rax, %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	_ZdlPv@PLT
.L25:
	movq	56(%rbx), %r13
	movq	48(%rbx), %r12
	cmpq	%r12, %r13
	je	.L26
	.p2align 4,,10
	.p2align 3
.L30:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L27
	call	_ZdlPv@PLT
	addq	$40, %r12
	cmpq	%r13, %r12
	jne	.L30
.L28:
	movq	48(%rbx), %r12
.L26:
	testq	%r12, %r12
	je	.L31
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L31:
	movq	24(%rbx), %r12
	testq	%r12, %r12
	je	.L35
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L32
.L35:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L56
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	addq	$40, %r12
	cmpq	%r12, %r13
	jne	.L30
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L56:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21595:
	.size	_ZN2v88internal13AssemblerBaseD2Ev, .-_ZN2v88internal13AssemblerBaseD2Ev
	.globl	_ZN2v88internal13AssemblerBaseD1Ev
	.set	_ZN2v88internal13AssemblerBaseD1Ev,_ZN2v88internal13AssemblerBaseD2Ev
	.section	.text._ZN2v88internal13AssemblerBaseD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13AssemblerBaseD0Ev
	.type	_ZN2v88internal13AssemblerBaseD0Ev, @function
_ZN2v88internal13AssemblerBaseD0Ev:
.LFB21597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal13AssemblerBaseD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE21597:
	.size	_ZN2v88internal13AssemblerBaseD0Ev, .-_ZN2v88internal13AssemblerBaseD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB27788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE27788:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB27997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27997:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB27789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27789:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB27998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE27998:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBuffer4GrowEi,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBuffer4GrowEi, @function
_ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBuffer4GrowEi:
.LFB21514:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$24, %edi
	pushq	%r12
	.cfi_offset 12, -32
	movslq	%edx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_Znwm@PLT
	movq	%rax, %rbx
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	jne	.L68
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
.L69:
	movq	%rbx, 0(%r13)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_Znam@PLT
	movq	%r12, 16(%rbx)
	movq	%rax, 8(%rbx)
	jmp	.L69
	.cfi_endproc
.LFE21514:
	.size	_ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBuffer4GrowEi, .-_ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBuffer4GrowEi
	.section	.text._ZN2v88internal16AssemblerOptions7DefaultEPNS0_7IsolateEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16AssemblerOptions7DefaultEPNS0_7IsolateEb
	.type	_ZN2v88internal16AssemblerOptions7DefaultEPNS0_7IsolateEb, @function
_ZN2v88internal16AssemblerOptions7DefaultEPNS0_7IsolateEb:
.LFB21490:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	orb	41456(%rsi), %dl
	xorl	%ecx, %ecx
	movb	$0, 1(%rdi)
	movw	%cx, 3(%rdi)
	xorl	%edi, %edi
	movb	%dl, (%rax)
	xorl	$1, %edx
	cmpq	$0, 45520(%rsi)
	sete	%cl
	movw	%di, 16(%rax)
	andl	%ecx, %edx
	movb	%dl, 2(%rax)
	movb	%dl, 5(%rax)
	movq	39640(%rsi), %rdx
	movq	48(%rdx), %rdx
	movq	%rdx, 8(%rax)
	ret
	.cfi_endproc
.LFE21490:
	.size	_ZN2v88internal16AssemblerOptions7DefaultEPNS0_7IsolateEb, .-_ZN2v88internal16AssemblerOptions7DefaultEPNS0_7IsolateEb
	.section	.text._ZN2v88internal23ExternalAssemblerBufferEPvi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23ExternalAssemblerBufferEPvi
	.type	_ZN2v88internal23ExternalAssemblerBufferEPvi, @function
_ZN2v88internal23ExternalAssemblerBufferEPvi:
.LFB21523:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$24, %edi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%r13, 8(%rax)
	movl	%ebx, 16(%rax)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21523:
	.size	_ZN2v88internal23ExternalAssemblerBufferEPvi, .-_ZN2v88internal23ExternalAssemblerBufferEPvi
	.section	.text._ZN2v88internal18NewAssemblerBufferEi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18NewAssemblerBufferEi
	.type	_ZN2v88internal18NewAssemblerBufferEi, @function
_ZN2v88internal18NewAssemblerBufferEi:
.LFB21526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$24, %edi
	pushq	%r12
	.cfi_offset 12, -32
	movslq	%esi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_Znwm@PLT
	movq	%rax, %rbx
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	jne	.L75
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
.L76:
	movq	%rbx, 0(%r13)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_Znam@PLT
	movq	%r12, 16(%rbx)
	movq	%rax, 8(%rbx)
	jmp	.L76
	.cfi_endproc
.LFE21526:
	.size	_ZN2v88internal18NewAssemblerBufferEi, .-_ZN2v88internal18NewAssemblerBufferEi
	.section	.text._ZN2v88internal13AssemblerBaseC2ERKNS0_16AssemblerOptionsESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13AssemblerBaseC2ERKNS0_16AssemblerOptionsESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE
	.type	_ZN2v88internal13AssemblerBaseC2ERKNS0_16AssemblerOptionsESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE, @function
_ZN2v88internal13AssemblerBaseC2ERKNS0_16AssemblerOptionsESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE:
.LFB21592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal13AssemblerBaseE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	(%rdx), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	leaq	168(%rdi), %rax
	movq	$0, (%rdx)
	movq	%r12, 8(%rdi)
	movq	$0, 24(%rdi)
	movl	$0, 40(%rdi)
	movq	$0, 112(%rdi)
	movq	%rax, 120(%rdi)
	movq	$1, 128(%rdi)
	movq	$0, 136(%rdi)
	movq	$0, 144(%rdi)
	movl	$0x3f800000, 152(%rdi)
	movq	$0, 160(%rdi)
	movq	$0, 168(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 96(%rdi)
	movq	16(%rsi), %rax
	movdqu	(%rsi), %xmm1
	movq	$0, 200(%rdi)
	movq	%rax, 192(%rdi)
	movzbl	_ZN2v88internal15FLAG_debug_codeE(%rip), %eax
	movq	$0, 216(%rdi)
	movb	%al, 208(%rdi)
	xorl	%eax, %eax
	movw	%ax, 209(%rdi)
	movups	%xmm1, 176(%rdi)
	testq	%r12, %r12
	je	.L84
.L79:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movq	%rax, 16(%rbx)
	movq	%rax, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$4096, %edi
	movq	%rax, %r12
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferE(%rip), %rax
	movq	%rax, (%r12)
	call	_Znam@PLT
	movq	8(%rbx), %rdi
	movq	%r12, 8(%rbx)
	movq	%rax, 8(%r12)
	movq	$4096, 16(%r12)
	testq	%rdi, %rdi
	je	.L79
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	8(%rbx), %r12
	jmp	.L79
	.cfi_endproc
.LFE21592:
	.size	_ZN2v88internal13AssemblerBaseC2ERKNS0_16AssemblerOptionsESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE, .-_ZN2v88internal13AssemblerBaseC2ERKNS0_16AssemblerOptionsESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE
	.globl	_ZN2v88internal13AssemblerBaseC1ERKNS0_16AssemblerOptionsESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE
	.set	_ZN2v88internal13AssemblerBaseC1ERKNS0_16AssemblerOptionsESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE,_ZN2v88internal13AssemblerBaseC2ERKNS0_16AssemblerOptionsESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE
	.section	.text._ZN2v88internal13AssemblerBase5PrintEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13AssemblerBase5PrintEPNS0_7IsolateE
	.type	_ZN2v88internal13AssemblerBase5PrintEPNS0_7IsolateE, @function
_ZN2v88internal13AssemblerBase5PrintEPNS0_7IsolateE:
.LFB21598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-400(%rbp), %r15
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$360, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	%r14, -320(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	subq	$8, %rsp
	movq	16(%rbx), %rdx
	xorl	%r8d, %r8d
	pushq	$0
	movq	32(%rbx), %rcx
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	xorl	%r9d, %r9d
	movq	%r15, %rsi
	movq	%rax, -400(%rbp)
	movq	%r12, %rdi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal12Disassembler6DecodeEPNS0_7IsolateEPSoPhS5_NS0_13CodeReferenceEm@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%r14, -320(%rbp)
	movq	%rax, -400(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	popq	%rdx
	popq	%rcx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L88
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L88:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21598:
	.size	_ZN2v88internal13AssemblerBase5PrintEPNS0_7IsolateE, .-_ZN2v88internal13AssemblerBase5PrintEPNS0_7IsolateE
	.section	.text._ZN2v88internal17HeapObjectRequestC2Edi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17HeapObjectRequestC2Edi
	.type	_ZN2v88internal17HeapObjectRequestC2Edi, @function
_ZN2v88internal17HeapObjectRequestC2Edi:
.LFB21600:
	.cfi_startproc
	endbr64
	movl	$0, (%rdi)
	movl	%esi, 16(%rdi)
	movsd	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE21600:
	.size	_ZN2v88internal17HeapObjectRequestC2Edi, .-_ZN2v88internal17HeapObjectRequestC2Edi
	.globl	_ZN2v88internal17HeapObjectRequestC1Edi
	.set	_ZN2v88internal17HeapObjectRequestC1Edi,_ZN2v88internal17HeapObjectRequestC2Edi
	.section	.text._ZN2v88internal17HeapObjectRequestC2EPKNS0_18StringConstantBaseEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17HeapObjectRequestC2EPKNS0_18StringConstantBaseEi
	.type	_ZN2v88internal17HeapObjectRequestC2EPKNS0_18StringConstantBaseEi, @function
_ZN2v88internal17HeapObjectRequestC2EPKNS0_18StringConstantBaseEi:
.LFB21603:
	.cfi_startproc
	endbr64
	movl	$1, (%rdi)
	movl	%edx, 16(%rdi)
	movq	%rsi, 8(%rdi)
	ret
	.cfi_endproc
.LFE21603:
	.size	_ZN2v88internal17HeapObjectRequestC2EPKNS0_18StringConstantBaseEi, .-_ZN2v88internal17HeapObjectRequestC2EPKNS0_18StringConstantBaseEi
	.globl	_ZN2v88internal17HeapObjectRequestC1EPKNS0_18StringConstantBaseEi
	.set	_ZN2v88internal17HeapObjectRequestC1EPKNS0_18StringConstantBaseEi,_ZN2v88internal17HeapObjectRequestC2EPKNS0_18StringConstantBaseEi
	.section	.text._ZN2v88internal9Assembler17RecordDeoptReasonENS0_16DeoptimizeReasonENS0_14SourcePositionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Assembler17RecordDeoptReasonENS0_16DeoptimizeReasonENS0_14SourcePositionEi
	.type	_ZN2v88internal9Assembler17RecordDeoptReasonENS0_16DeoptimizeReasonENS0_14SourcePositionEi, @function
_ZN2v88internal9Assembler17RecordDeoptReasonENS0_16DeoptimizeReasonENS0_14SourcePositionEi:
.LFB21605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	224(%rdi), %rax
	movq	%rdx, %rbx
	subq	$32, %rax
	cmpq	%rax, 32(%rdi)
	jnb	.L94
.L92:
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movl	$13, %esi
	shrq	$31, %rbx
	shrq	%rdx
	andl	$1073741823, %edx
	subl	$1, %edx
	movslq	%edx, %rdx
	call	_ZN2v88internal9Assembler15RecordRelocInfoENS0_9RelocInfo4ModeEl@PLT
	movzwl	%bx, %edx
	movq	%r14, %rdi
	movl	$14, %esi
	subl	$1, %edx
	movslq	%edx, %rdx
	call	_ZN2v88internal9Assembler15RecordRelocInfoENS0_9RelocInfo4ModeEl@PLT
	movzbl	%r13b, %edx
	movq	%r14, %rdi
	movl	$15, %esi
	call	_ZN2v88internal9Assembler15RecordRelocInfoENS0_9RelocInfo4ModeEl@PLT
	popq	%rbx
	movslq	%r12d, %rdx
	movq	%r14, %rdi
	popq	%r12
	movl	$16, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler15RecordRelocInfoENS0_9RelocInfo4ModeEl@PLT
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	call	_ZN2v88internal9Assembler10GrowBufferEv@PLT
	jmp	.L92
	.cfi_endproc
.LFE21605:
	.size	_ZN2v88internal9Assembler17RecordDeoptReasonENS0_16DeoptimizeReasonENS0_14SourcePositionEi, .-_ZN2v88internal9Assembler17RecordDeoptReasonENS0_16DeoptimizeReasonENS0_14SourcePositionEi
	.section	.text._ZN2v88internal9Assembler9DataAlignEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Assembler9DataAlignEi
	.type	_ZN2v88internal9Assembler9DataAlignEi, @function
_ZN2v88internal9Assembler9DataAlignEi:
.LFB21606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leal	-1(%rsi), %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rdi), %rax
	subq	16(%rdi), %rax
	testl	%eax, %r12d
	je	.L95
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L97:
	movl	$204, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler2dbEh@PLT
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	testl	%eax, %r12d
	jne	.L97
.L95:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21606:
	.size	_ZN2v88internal9Assembler9DataAlignEi, .-_ZN2v88internal9Assembler9DataAlignEi
	.section	.text._ZN2v88internal13AssemblerBase17RequestHeapObjectENS0_17HeapObjectRequestE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13AssemblerBase17RequestHeapObjectENS0_17HeapObjectRequestE
	.type	_ZN2v88internal13AssemblerBase17RequestHeapObjectENS0_17HeapObjectRequestE, @function
_ZN2v88internal13AssemblerBase17RequestHeapObjectENS0_17HeapObjectRequestE:
.LFB21607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	32(%rdi), %r12
	subq	16(%rdi), %r12
	movl	$32, %edi
	call	_Znwm@PLT
	movl	%r12d, 32(%rbp)
	movq	32(%rbp), %rdx
	movdqu	16(%rbp), %xmm0
	movq	%rdx, 24(%rax)
	movq	24(%rbx), %rdx
	movups	%xmm0, 8(%rax)
	movq	%rdx, (%rax)
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21607:
	.size	_ZN2v88internal13AssemblerBase17RequestHeapObjectENS0_17HeapObjectRequestE, .-_ZN2v88internal13AssemblerBase17RequestHeapObjectENS0_17HeapObjectRequestE
	.section	.rodata._ZN2v88internal13AssemblerBase13AddCodeTargetENS0_6HandleINS0_4CodeEEE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal13AssemblerBase13AddCodeTargetENS0_6HandleINS0_4CodeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13AssemblerBase13AddCodeTargetENS0_6HandleINS0_4CodeEEE
	.type	_ZN2v88internal13AssemblerBase13AddCodeTargetENS0_6HandleINS0_4CodeEEE, @function
_ZN2v88internal13AssemblerBase13AddCodeTargetENS0_6HandleINS0_4CodeEEE:
.LFB21608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	80(%rdi), %r14
	movq	72(%rdi), %r15
	movq	%r14, %r13
	subq	%r15, %r13
	movq	%r13, %rax
	sarq	$3, %rax
	movl	%eax, %r8d
	testl	%eax, %eax
	jle	.L106
	testq	%rsi, %rsi
	je	.L106
	cmpq	-8(%r14), %rsi
	je	.L136
.L106:
	cmpq	88(%rbx), %r14
	je	.L108
	movq	%r12, (%r14)
	addq	$8, 80(%rbx)
.L105:
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	leal	-1(%rax), %r8d
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L108:
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	je	.L137
	testq	%rax, %rax
	je	.L118
	movabsq	$9223372036854775800, %rsi
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L138
.L110:
	movq	%rsi, %rdi
	movl	%r8d, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movl	-64(%rbp), %r8d
	leaq	8(%rax), %rdx
	addq	%rax, %rsi
.L111:
	movq	%r12, (%rax,%r13)
	cmpq	%r14, %r15
	je	.L112
	leaq	-8(%r14), %rdi
	leaq	15(%r15), %rdx
	subq	%r15, %rdi
	subq	%rax, %rdx
	movq	%rdi, %r9
	shrq	$3, %r9
	cmpq	$30, %rdx
	jbe	.L121
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %r9
	je	.L121
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L114:
	movdqu	(%r15,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L114
	movq	%r9, %r10
	andq	$-2, %r10
	leaq	0(,%r10,8), %rcx
	leaq	(%r15,%rcx), %rdx
	addq	%rax, %rcx
	cmpq	%r10, %r9
	je	.L116
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L116:
	leaq	16(%rax,%rdi), %rdx
.L112:
	testq	%r15, %r15
	je	.L117
	movq	%r15, %rdi
	movl	%r8d, -76(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZdlPv@PLT
	movl	-76(%rbp), %r8d
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rsi
.L117:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rsi, 88(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 72(%rbx)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L138:
	testq	%rcx, %rcx
	jne	.L139
	movl	$8, %edx
	xorl	%esi, %esi
	xorl	%eax, %eax
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L118:
	movl	$8, %esi
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%rax, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L113:
	movq	(%rdx), %r9
	addq	$8, %rdx
	addq	$8, %rcx
	movq	%r9, -8(%rcx)
	cmpq	%rdx, %r14
	jne	.L113
	jmp	.L116
.L137:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L139:
	cmpq	%rdx, %rcx
	movq	%rdx, %rsi
	cmovbe	%rcx, %rsi
	salq	$3, %rsi
	jmp	.L110
	.cfi_endproc
.LFE21608:
	.size	_ZN2v88internal13AssemblerBase13AddCodeTargetENS0_6HandleINS0_4CodeEEE, .-_ZN2v88internal13AssemblerBase13AddCodeTargetENS0_6HandleINS0_4CodeEEE
	.section	.text._ZNK2v88internal13AssemblerBase13GetCodeTargetEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13AssemblerBase13GetCodeTargetEl
	.type	_ZNK2v88internal13AssemblerBase13GetCodeTargetEl, @function
_ZNK2v88internal13AssemblerBase13GetCodeTargetEl:
.LFB21609:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	movq	(%rax,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE21609:
	.size	_ZNK2v88internal13AssemblerBase13GetCodeTargetEl, .-_ZNK2v88internal13AssemblerBase13GetCodeTargetEl
	.section	.text._ZNK2v88internal13AssemblerBase17GetEmbeddedObjectEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13AssemblerBase17GetEmbeddedObjectEm
	.type	_ZNK2v88internal13AssemblerBase17GetEmbeddedObjectEm, @function
_ZNK2v88internal13AssemblerBase17GetEmbeddedObjectEm:
.LFB21611:
	.cfi_startproc
	endbr64
	movq	96(%rdi), %rax
	movq	(%rax,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE21611:
	.size	_ZNK2v88internal13AssemblerBase17GetEmbeddedObjectEm, .-_ZNK2v88internal13AssemblerBase17GetEmbeddedObjectEm
	.section	.text._ZN2v88internal9Assembler17WriteCodeCommentsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Assembler17WriteCodeCommentsEv
	.type	_ZN2v88internal9Assembler17WriteCodeCommentsEv, @function
_ZN2v88internal9Assembler17WriteCodeCommentsEv:
.LFB21612:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L143
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	40(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal18CodeCommentsWriter11entry_countEv@PLT
	testq	%rax, %rax
	je	.L145
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	32(%rbx), %r13
	subq	16(%rbx), %r13
	call	_ZN2v88internal18CodeCommentsWriter4EmitEPNS0_9AssemblerE@PLT
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	subl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21612:
	.size	_ZN2v88internal9Assembler17WriteCodeCommentsEv, .-_ZN2v88internal9Assembler17WriteCodeCommentsEv
	.section	.text._ZNSt8__detail9_Map_baseIN2v88internal6HandleINS2_10HeapObjectEEESt4pairIKS5_mESaIS8_ENS_10_Select1stENS5_8equal_toENS5_4hashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS7_,"axG",@progbits,_ZNSt8__detail9_Map_baseIN2v88internal6HandleINS2_10HeapObjectEEESt4pairIKS5_mESaIS8_ENS_10_Select1stENS5_8equal_toENS5_4hashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIN2v88internal6HandleINS2_10HeapObjectEEESt4pairIKS5_mESaIS8_ENS_10_Select1stENS5_8equal_toENS5_4hashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS7_
	.type	_ZNSt8__detail9_Map_baseIN2v88internal6HandleINS2_10HeapObjectEEESt4pairIKS5_mESaIS8_ENS_10_Select1stENS5_8equal_toENS5_4hashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS7_, @function
_ZNSt8__detail9_Map_baseIN2v88internal6HandleINS2_10HeapObjectEEESt4pairIKS5_mESaIS8_ENS_10_Select1stENS5_8equal_toENS5_4hashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS7_:
.LFB25464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	call	_ZN2v84base10hash_valueEm@PLT
	movq	8(%r12), %rsi
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%rsi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r14
	testq	%rax, %rax
	je	.L158
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movq	24(%rcx), %rdi
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L156:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L158
	movq	24(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L158
.L159:
	cmpq	%rdi, %r13
	jne	.L156
	movq	(%r15), %rax
	cmpq	%rax, 8(%rcx)
	jne	.L156
	addq	$24, %rsp
	leaq	16(%rcx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movl	$32, %edi
	call	_Znwm@PLT
	movq	24(%r12), %rdx
	movq	8(%r12), %rsi
	leaq	32(%r12), %rdi
	movq	$0, (%rax)
	movq	%rax, %rbx
	movq	(%r15), %rax
	movl	$1, %ecx
	movq	$0, 16(%rbx)
	movq	%rax, 8(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r15
	testb	%al, %al
	jne	.L187
	movq	(%r12), %r8
	movq	%r13, 24(%rbx)
	leaq	(%r8,%r14), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L169
.L190:
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	(%rax), %rax
	movq	%rbx, (%rax)
.L170:
	addq	$1, 24(%r12)
	addq	$24, %rsp
	leaq	16(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L188
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L189
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r12), %r10
	movq	%rax, %r8
.L162:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L164
	xorl	%edi, %edi
	leaq	16(%r12), %r9
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L166:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L167:
	testq	%rsi, %rsi
	je	.L164
.L165:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	24(%rcx), %rax
	divq	%r15
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L166
	movq	16(%r12), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L172
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L165
	.p2align 4,,10
	.p2align 3
.L164:
	movq	(%r12), %rdi
	cmpq	%r10, %rdi
	je	.L168
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L168:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r15, 8(%r12)
	divq	%r15
	movq	%r8, (%r12)
	movq	%r13, 24(%rbx)
	leaq	0(,%rdx,8), %r14
	leaq	(%r8,%r14), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L190
.L169:
	movq	16(%r12), %rdx
	movq	%rbx, 16(%r12)
	movq	%rdx, (%rbx)
	testq	%rdx, %rdx
	je	.L171
	movq	24(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	%rbx, (%r8,%rdx,8)
	movq	(%r12), %rax
	addq	%r14, %rax
.L171:
	leaq	16(%r12), %rdx
	movq	%rdx, (%rax)
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L172:
	movq	%rdx, %rdi
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L188:
	movq	$0, 48(%r12)
	leaq	48(%r12), %r8
	movq	%r8, %r10
	jmp	.L162
.L189:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE25464:
	.size	_ZNSt8__detail9_Map_baseIN2v88internal6HandleINS2_10HeapObjectEEESt4pairIKS5_mESaIS8_ENS_10_Select1stENS5_8equal_toENS5_4hashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS7_, .-_ZNSt8__detail9_Map_baseIN2v88internal6HandleINS2_10HeapObjectEEESt4pairIKS5_mESaIS8_ENS_10_Select1stENS5_8equal_toENS5_4hashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS7_
	.section	.text._ZN2v88internal13AssemblerBase17AddEmbeddedObjectENS0_6HandleINS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13AssemblerBase17AddEmbeddedObjectENS0_6HandleINS0_10HeapObjectEEE
	.type	_ZN2v88internal13AssemblerBase17AddEmbeddedObjectENS0_6HandleINS0_10HeapObjectEEE, @function
_ZN2v88internal13AssemblerBase17AddEmbeddedObjectENS0_6HandleINS0_10HeapObjectEEE:
.LFB21610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	104(%rdi), %r13
	movq	%rsi, -56(%rbp)
	movq	%r13, %r12
	subq	96(%rdi), %r12
	sarq	$3, %r12
	movq	%r12, %r14
	testq	%rsi, %rsi
	je	.L192
	movq	%rsi, %rdi
	leaq	120(%rbx), %r13
	call	_ZN2v84base10hash_valueEm@PLT
	movq	128(%rbx), %r8
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%r8
	movq	120(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L193
	movq	(%rax), %rcx
	movq	-56(%rbp), %r10
	movq	24(%rcx), %rsi
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L194:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L193
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L193
.L196:
	cmpq	%rsi, %rdi
	jne	.L194
	cmpq	%r10, 8(%rcx)
	jne	.L194
	movq	16(%rcx), %r14
.L191:
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	movq	%r13, %rdi
	leaq	-56(%rbp), %rsi
	call	_ZNSt8__detail9_Map_baseIN2v88internal6HandleINS2_10HeapObjectEEESt4pairIKS5_mESaIS8_ENS_10_Select1stENS5_8equal_toENS5_4hashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS7_
	movq	%r12, (%rax)
	movq	104(%rbx), %r13
.L192:
	cmpq	%r13, 112(%rbx)
	je	.L197
	movq	-56(%rbp), %rax
	movq	%rax, 0(%r13)
	addq	$8, 104(%rbx)
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L197:
	movq	96(%rbx), %r8
	movq	%r13, %rsi
	movabsq	$1152921504606846975, %r15
	subq	%r8, %rsi
	movq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%r15, %rax
	je	.L236
	testq	%rax, %rax
	je	.L237
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	ja	.L238
	testq	%rdx, %rdx
	jne	.L239
	movq	$8, -64(%rbp)
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
.L201:
	movq	-56(%rbp), %rax
	movq	%rax, (%r12,%rsi)
	cmpq	%r13, %r8
	je	.L202
	movq	%r13, %rax
	subq	%r8, %rax
	leaq	-8(%rax), %rsi
	leaq	15(%r12), %rax
	movq	%rsi, %rdi
	subq	%r8, %rax
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L211
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L211
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L204:
	movdqu	(%r8,%rdx), %xmm1
	movups	%xmm1, (%r12,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rax
	jne	.L204
	movq	%rdi, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rdx
	leaq	(%r8,%rdx), %rax
	addq	%r12, %rdx
	cmpq	%r9, %rdi
	je	.L206
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L206:
	leaq	16(%r12,%rsi), %rax
	movq	%rax, -64(%rbp)
.L202:
	testq	%r8, %r8
	je	.L207
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L207:
	movq	%r12, %xmm0
	movq	%r15, 112(%rbx)
	movhps	-64(%rbp), %xmm0
	movups	%xmm0, 96(%rbx)
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L237:
	movl	$8, %r15d
.L199:
	movq	%r15, %rdi
	movq	%rsi, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	%rax, %r12
	addq	%rax, %r15
	leaq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L238:
	movabsq	$9223372036854775800, %r15
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L211:
	movq	%r12, %rdx
	movq	%r8, %rax
	.p2align 4,,10
	.p2align 3
.L203:
	movq	(%rax), %rdi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rdi, -8(%rdx)
	cmpq	%r13, %rax
	jne	.L203
	jmp	.L206
.L239:
	cmpq	%r15, %rdx
	cmova	%r15, %rdx
	leaq	0(,%rdx,8), %r15
	jmp	.L199
.L236:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21610:
	.size	_ZN2v88internal13AssemblerBase17AddEmbeddedObjectENS0_6HandleINS0_10HeapObjectEEE, .-_ZN2v88internal13AssemblerBase17AddEmbeddedObjectENS0_6HandleINS0_10HeapObjectEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal16AssemblerOptions7DefaultEPNS0_7IsolateEb,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal16AssemblerOptions7DefaultEPNS0_7IsolateEb, @function
_GLOBAL__sub_I__ZN2v88internal16AssemblerOptions7DefaultEPNS0_7IsolateEb:
.LFB27847:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27847:
	.size	_GLOBAL__sub_I__ZN2v88internal16AssemblerOptions7DefaultEPNS0_7IsolateEb, .-_GLOBAL__sub_I__ZN2v88internal16AssemblerOptions7DefaultEPNS0_7IsolateEb
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal16AssemblerOptions7DefaultEPNS0_7IsolateEb
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.data.rel.ro.local._ZTVN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferE,"aw"
	.align 8
	.type	_ZTVN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferE, @object
	.size	_ZTVN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferE, 56
_ZTVN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferD1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBufferD0Ev
	.quad	_ZNK2v88internal12_GLOBAL__N_122DefaultAssemblerBuffer5startEv
	.quad	_ZNK2v88internal12_GLOBAL__N_122DefaultAssemblerBuffer4sizeEv
	.quad	_ZN2v88internal12_GLOBAL__N_122DefaultAssemblerBuffer4GrowEi
	.section	.data.rel.ro.local._ZTVN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplE,"aw"
	.align 8
	.type	_ZTVN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplE, @object
	.size	_ZTVN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplE, 56
_ZTVN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplD1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImplD0Ev
	.quad	_ZNK2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl5startEv
	.quad	_ZNK2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl4sizeEv
	.quad	_ZN2v88internal12_GLOBAL__N_127ExternalAssemblerBufferImpl4GrowEi
	.weak	_ZTVN2v88internal13AssemblerBaseE
	.section	.data.rel.ro.local._ZTVN2v88internal13AssemblerBaseE,"awG",@progbits,_ZTVN2v88internal13AssemblerBaseE,comdat
	.align 8
	.type	_ZTVN2v88internal13AssemblerBaseE, @object
	.size	_ZTVN2v88internal13AssemblerBaseE, 40
_ZTVN2v88internal13AssemblerBaseE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal13AssemblerBaseD1Ev
	.quad	_ZN2v88internal13AssemblerBaseD0Ev
	.quad	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.globl	_ZN2v88internal11CpuFeatures17dcache_line_size_E
	.section	.bss._ZN2v88internal11CpuFeatures17dcache_line_size_E,"aw",@nobits
	.align 4
	.type	_ZN2v88internal11CpuFeatures17dcache_line_size_E, @object
	.size	_ZN2v88internal11CpuFeatures17dcache_line_size_E, 4
_ZN2v88internal11CpuFeatures17dcache_line_size_E:
	.zero	4
	.globl	_ZN2v88internal11CpuFeatures17icache_line_size_E
	.section	.bss._ZN2v88internal11CpuFeatures17icache_line_size_E,"aw",@nobits
	.align 4
	.type	_ZN2v88internal11CpuFeatures17icache_line_size_E, @object
	.size	_ZN2v88internal11CpuFeatures17icache_line_size_E, 4
_ZN2v88internal11CpuFeatures17icache_line_size_E:
	.zero	4
	.globl	_ZN2v88internal11CpuFeatures10supported_E
	.section	.bss._ZN2v88internal11CpuFeatures10supported_E,"aw",@nobits
	.align 4
	.type	_ZN2v88internal11CpuFeatures10supported_E, @object
	.size	_ZN2v88internal11CpuFeatures10supported_E, 4
_ZN2v88internal11CpuFeatures10supported_E:
	.zero	4
	.globl	_ZN2v88internal11CpuFeatures12initialized_E
	.section	.bss._ZN2v88internal11CpuFeatures12initialized_E,"aw",@nobits
	.type	_ZN2v88internal11CpuFeatures12initialized_E, @object
	.size	_ZN2v88internal11CpuFeatures12initialized_E, 1
_ZN2v88internal11CpuFeatures12initialized_E:
	.zero	1
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
