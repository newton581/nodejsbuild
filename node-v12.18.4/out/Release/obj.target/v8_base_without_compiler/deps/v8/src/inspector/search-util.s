	.file	"search-util.cc"
	.text
	.section	.text._ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger11SearchMatchD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev, @function
_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev:
.LFB5700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	40(%r12), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	24(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L2
	call	_ZdlPv@PLT
.L2:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5700:
	.size	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev, .-_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger11SearchMatchD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev, @function
_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev:
.LFB5698:
	.cfi_startproc
	endbr64
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	movq	24(%rdi), %r8
	addq	$40, %rdi
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -40(%rdi)
	cmpq	%rdi, %r8
	je	.L5
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	ret
	.cfi_endproc
.LFE5698:
	.size	_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev, .-_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger11SearchMatchD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger11SearchMatchD1Ev,_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev
	.type	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev, @function
_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev:
.LFB12722:
	.cfi_startproc
	endbr64
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	movq	16(%rdi), %r8
	addq	$32, %rdi
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -40(%rdi)
	cmpq	%rdi, %r8
	je	.L7
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	ret
	.cfi_endproc
.LFE12722:
	.size	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev, .-_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger11SearchMatchD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev
	.type	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev, @function
_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev:
.LFB12723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	addq	$32, %rdi
	subq	$8, %rsp
	movq	-16(%rdi), %r8
	movups	%xmm0, -40(%rdi)
	cmpq	%rdi, %r8
	je	.L10
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L10:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12723:
	.size	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev, .-_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev
	.section	.text._ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE4findEPKtmm,"axG",@progbits,_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE4findEPKtmm,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE4findEPKtmm
	.type	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE4findEPKtmm, @function
_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE4findEPKtmm:
.LFB8947:
	.cfi_startproc
	endbr64
	movq	%rdi, %r9
	movq	%rdx, %r11
	movq	8(%rdi), %rdi
	testq	%rcx, %rcx
	je	.L30
	movq	$-1, %rax
	cmpq	%rdi, %rdx
	jnb	.L12
	movq	(%r9), %r10
	movzwl	(%rsi), %r8d
	leaq	(%r10,%rdi,2), %r9
	subq	%r11, %rdi
	leaq	(%r10,%rdx,2), %rdx
	cmpq	%rdi, %rcx
	ja	.L12
	movl	$1, %r11d
	subq	%rcx, %r11
	.p2align 4,,10
	.p2align 3
.L21:
	addq	%r11, %rdi
	je	.L28
	xorl	%eax, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L31:
	addq	$1, %rax
	addq	$2, %rdx
	cmpq	%rax, %rdi
	je	.L28
.L17:
	cmpw	(%rdx), %r8w
	jne	.L31
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L16:
	addq	$1, %rax
	cmpq	%rax, %rcx
	je	.L32
	movzwl	(%rsi,%rax,2), %edi
	cmpw	%di, (%rdx,%rax,2)
	je	.L16
	addq	$2, %rdx
	movq	%r9, %rdi
	subq	%rdx, %rdi
	sarq	%rdi
	cmpq	%rdi, %rcx
	jbe	.L21
.L28:
	movq	$-1, %rax
	ret
.L12:
	ret
.L30:
	cmpq	%rdi, %rdx
	movq	$-1, %rax
	cmovbe	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	subq	%r10, %rdx
	movq	%rdx, %rax
	sarq	%rax
	ret
	.cfi_endproc
.LFE8947:
	.size	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE4findEPKtmm, .-_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE4findEPKtmm
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_116findMagicCommentERKNS_8String16ES3_b.str1.1,"aMS",@progbits,1
.LC0:
	.string	"*/"
.LC1:
	.string	"basic_string::substr"
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_116findMagicCommentERKNS_8String16ES3_b.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"%s: __pos (which is %zu) > this->size() (which is %zu)"
	.align 8
.LC3:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_116findMagicCommentERKNS_8String16ES3_b.str1.1
.LC4:
	.string	"basic_string::_M_create"
.LC5:
	.string	"\n"
.LC6:
	.string	""
	.section	.text._ZN12v8_inspector12_GLOBAL__N_116findMagicCommentERKNS_8String16ES3_b,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_116findMagicCommentERKNS_8String16ES3_b, @function
_ZN12v8_inspector12_GLOBAL__N_116findMagicCommentERKNS_8String16ES3_b:
.LFB7307:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %r11
	movq	8(%rdx), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r8, %r11
	jb	.L175
	movq	%r11, %r9
	movq	(%rdx), %r10
	movq	(%rsi), %rdi
	movq	%rsi, %rbx
	movl	%ecx, %r13d
	subq	%r8, %r9
	movq	%r11, %rax
	.p2align 4,,10
	.p2align 3
.L35:
	cmpq	%r9, %rax
	cmova	%r9, %rax
	leaq	(%rax,%rax), %rsi
	testq	%r8, %r8
	je	.L36
	.p2align 4,,10
	.p2align 3
.L178:
	xorl	%edx, %edx
	leaq	(%rdi,%rsi), %r14
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L177:
	addq	$1, %rdx
	cmpq	%rdx, %r8
	je	.L36
.L38:
	movzwl	(%r10,%rdx,2), %r15d
	cmpw	%r15w, (%r14,%rdx,2)
	je	.L177
	leaq	-1(%rax), %rdx
	subq	$2, %rsi
	testq	%rax, %rax
	je	.L175
	movq	%rdx, %rax
	testq	%r8, %r8
	jne	.L178
	.p2align 4,,10
	.p2align 3
.L36:
	cmpq	$-1, %rax
	je	.L175
	cmpq	$3, %rax
	jbe	.L175
	subq	$4, %rax
	cmpw	$47, (%rdi,%rax,2)
	leaq	(%rax,%rax), %rdx
	jne	.L35
	movzwl	2(%rdi,%rdx), %esi
	cmpw	$47, %si
	je	.L179
	cmpw	$42, %si
	jne	.L35
	testb	%r13b, %r13b
	je	.L35
.L43:
	movzwl	4(%rdi,%rdx), %esi
	cmpw	$35, %si
	je	.L107
	cmpw	$64, %si
	jne	.L35
.L107:
	movzwl	6(%rdi,%rdx), %edx
	cmpw	$32, %dx
	je	.L108
	cmpw	$9, %dx
	jne	.L35
.L108:
	leaq	(%rax,%r8), %rsi
	leaq	4(%rsi), %rdx
	cmpq	%r11, %rdx
	jnb	.L46
	cmpw	$61, (%rdi,%rdx,2)
	jne	.L35
.L46:
	leaq	5(%rsi), %r15
	testb	%cl, %cl
	jne	.L180
	cmpq	%r15, %r11
	jb	.L181
	movl	$4294967295, %eax
	leaq	(%rdi,%r15,2), %r14
	leaq	-80(%rbp), %r13
	subq	%r15, %r11
	cmpq	%rax, %r11
	movq	%r13, -96(%rbp)
	cmova	%rax, %r11
	movq	%r14, %rax
	leaq	(%r11,%r11), %rbx
	addq	%rbx, %rax
	je	.L59
	testq	%r14, %r14
	je	.L57
.L59:
	movq	%rbx, %r15
	sarq	%r15
	cmpq	$14, %rbx
	ja	.L182
	cmpq	$2, %rbx
	jne	.L61
	movzwl	(%r14), %eax
	movw	%ax, -80(%rbp)
	movq	-96(%rbp), %rax
.L62:
	xorl	%r10d, %r10d
	movq	%r15, -88(%rbp)
	leaq	-96(%rbp), %r14
	leaq	-144(%rbp), %r15
	movw	%r10w, (%rax,%rbx)
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L56
.L173:
	call	_ZdlPv@PLT
.L56:
	leaq	.LC5(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-96(%rbp), %rbx
	movq	-88(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE4findEPKtmm
	cmpq	%r13, %rbx
	je	.L64
	movq	%rbx, %rdi
	movq	%rax, -216(%rbp)
	call	_ZdlPv@PLT
	movq	-216(%rbp), %rax
.L64:
	leaq	-128(%rbp), %rbx
	cmpq	$-1, %rax
	je	.L65
	movq	-136(%rbp), %rbx
	movq	-144(%rbp), %rsi
	leaq	-160(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	cmpq	%rbx, %rax
	cmovbe	%rax, %rbx
	movq	%rsi, %rax
	leaq	(%rbx,%rbx), %rdx
	addq	%rdx, %rax
	je	.L109
	testq	%rsi, %rsi
	je	.L57
.L109:
	movq	%rdx, %r8
	movq	%rcx, %rdi
	sarq	%r8
	cmpq	$14, %rdx
	ja	.L183
.L67:
	cmpq	$2, %rdx
	je	.L184
	testq	%rdx, %rdx
	je	.L70
	movq	%rcx, -224(%rbp)
	movq	%r8, -216(%rbp)
	call	memmove@PLT
	movq	-176(%rbp), %rdi
	movq	-224(%rbp), %rcx
	movq	-216(%rbp), %r8
.L70:
	xorl	%r9d, %r9d
	movq	%r8, -168(%rbp)
	leaq	-176(%rbp), %rsi
	movw	%r9w, (%rdi,%rbx,2)
	movq	%r14, %rdi
	movq	%rcx, -216(%rbp)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-176(%rbp), %rdi
	movq	-216(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L71
	call	_ZdlPv@PLT
.L71:
	movq	-96(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movq	-88(%rbp), %rax
	cmpq	%r13, %rdx
	je	.L185
	leaq	-128(%rbp), %rbx
	movq	-80(%rbp), %rcx
	cmpq	%rbx, %rdi
	je	.L186
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	-128(%rbp), %rsi
	movq	%rdx, -144(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -136(%rbp)
	testq	%rdi, %rdi
	je	.L77
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L75:
	xorl	%esi, %esi
	movq	$0, -88(%rbp)
	movw	%si, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -112(%rbp)
	cmpq	%r13, %rdi
	je	.L65
	call	_ZdlPv@PLT
.L65:
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8String1615stripWhiteSpaceEv@PLT
	movq	-96(%rbp), %rdx
	movq	-144(%rbp), %rdi
	cmpq	%r13, %rdx
	je	.L187
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rax
	cmpq	%rbx, %rdi
	je	.L188
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	-128(%rbp), %rsi
	movq	%rdx, -144(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -136(%rbp)
	testq	%rdi, %rdi
	je	.L84
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L82:
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movw	%ax, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -112(%rbp)
	cmpq	%r13, %rdi
	je	.L85
	call	_ZdlPv@PLT
.L85:
	movq	-136(%rbp), %rcx
	movq	-144(%rbp), %rsi
	testq	%rcx, %rcx
	je	.L86
	movabsq	$571230650880, %rdi
	xorl	%eax, %eax
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L87:
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L86
.L89:
	movzwl	(%rsi,%rax,2), %edx
	cmpw	$39, %dx
	ja	.L87
	btq	%rdx, %rdi
	jnc	.L87
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-144(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L33
	call	_ZdlPv@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L175:
	leaq	16(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 32(%r12)
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movups	%xmm0, 16(%r12)
.L33:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L189
	addq	$200, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	testb	%r13b, %r13b
	jne	.L35
	jmp	.L43
.L182:
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r15, -80(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
.L60:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	memmove@PLT
	movq	-96(%rbp), %rax
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L180:
	leaq	-96(%rbp), %r14
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	leaq	-80(%rbp), %r13
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %rcx
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%r8, %rsi
	movq	%r8, -216(%rbp)
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE4findEPKtmm
	movq	-216(%rbp), %r8
	cmpq	%r13, %r8
	je	.L48
	movq	%r8, %rdi
	movq	%rax, -216(%rbp)
	call	_ZdlPv@PLT
	movq	-216(%rbp), %rax
.L48:
	cmpq	$-1, %rax
	je	.L175
	movq	8(%rbx), %rcx
	subq	%r15, %rax
	cmpq	%r15, %rcx
	jb	.L176
	subq	%r15, %rcx
	movq	(%rbx), %rdx
	leaq	-192(%rbp), %r8
	cmpq	%rax, %rcx
	movq	%r8, -208(%rbp)
	cmovbe	%rcx, %rax
	leaq	(%rdx,%r15,2), %rsi
	movq	%rax, %rbx
	leaq	(%rax,%rax), %r15
	movq	%rsi, %rax
	addq	%r15, %rax
	je	.L50
	testq	%rsi, %rsi
	je	.L57
.L50:
	movq	%r15, %rcx
	movq	%r8, %rdi
	sarq	%rcx
	cmpq	$14, %r15
	ja	.L190
.L51:
	cmpq	$2, %r15
	je	.L191
	testq	%r15, %r15
	je	.L54
	movq	%r15, %rdx
	movq	%r8, -224(%rbp)
	movq	%rcx, -216(%rbp)
	call	memmove@PLT
	movq	-208(%rbp), %rdi
	movq	-224(%rbp), %r8
	movq	-216(%rbp), %rcx
.L54:
	xorl	%r11d, %r11d
	movq	%rcx, -200(%rbp)
	leaq	-144(%rbp), %r15
	leaq	-208(%rbp), %rsi
	movw	%r11w, (%rdi,%rbx,2)
	movq	%r15, %rdi
	movq	%r8, -216(%rbp)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-208(%rbp), %rdi
	movq	-216(%rbp), %r8
	cmpq	%r8, %rdi
	jne	.L173
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	16(%r12), %rax
	movq	%rax, (%r12)
	cmpq	%rbx, %rsi
	je	.L192
	movq	-128(%rbp), %rax
	movq	%rsi, (%r12)
	movq	%rax, 16(%r12)
.L91:
	movq	-112(%rbp), %rax
	movq	%rcx, 8(%r12)
	movq	%rax, 32(%r12)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L187:
	movq	-88(%rbp), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L80
	cmpq	$1, %rax
	je	.L193
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L80
	movq	%r13, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	-144(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L80:
	xorl	%ecx, %ecx
	movq	%rax, -136(%rbp)
	movw	%cx, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L82
.L188:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdx, -144(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -136(%rbp)
.L84:
	movq	%r13, -96(%rbp)
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	jmp	.L82
.L191:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-208(%rbp), %rdi
	jmp	.L54
.L184:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-176(%rbp), %rdi
	jmp	.L70
.L61:
	movq	%r13, %rax
	testq	%rbx, %rbx
	je	.L62
	movq	%r13, %rdi
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L190:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %rcx
	ja	.L68
	leaq	2(%r15), %rdi
	movq	%r8, -232(%rbp)
	movq	%rcx, -224(%rbp)
	movq	%rsi, -216(%rbp)
	call	_Znwm@PLT
	movq	-224(%rbp), %rcx
	movq	-232(%rbp), %r8
	movq	%rax, -208(%rbp)
	movq	-216(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rcx, -192(%rbp)
	jmp	.L51
.L183:
	movabsq	$2305843009213693951, %rax
	movq	%rcx, -240(%rbp)
	cmpq	%rax, %r8
	movq	%rsi, -232(%rbp)
	movq	%r8, -224(%rbp)
	ja	.L68
	leaq	2(%rdx), %rdi
	movq	%rdx, -216(%rbp)
	call	_Znwm@PLT
	movq	-224(%rbp), %r8
	movq	-240(%rbp), %rcx
	movq	%rax, -176(%rbp)
	movq	-232(%rbp), %rsi
	movq	%rax, %rdi
	movq	%r8, -160(%rbp)
	movq	-216(%rbp), %rdx
	jmp	.L67
.L185:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L73
	cmpq	$1, %rax
	je	.L194
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L73
	movq	%r13, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	-144(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
.L73:
	xorl	%r8d, %r8d
	movq	%rax, -136(%rbp)
	leaq	-128(%rbp), %rbx
	movw	%r8w, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L75
.L193:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	-144(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L80
.L192:
	movdqa	-128(%rbp), %xmm5
	movups	%xmm5, 16(%r12)
	jmp	.L91
.L186:
	movq	%rax, %xmm0
	movq	%rcx, %xmm4
	movq	%rdx, -144(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, -136(%rbp)
.L77:
	movq	%r13, -96(%rbp)
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	jmp	.L75
.L194:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	-144(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L73
.L189:
	call	__stack_chk_fail@PLT
.L57:
	leaq	.LC3(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L181:
	movq	%r11, %rcx
.L176:
	movq	%r15, %rdx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L68:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7307:
	.size	_ZN12v8_inspector12_GLOBAL__N_116findMagicCommentERKNS_8String16ES3_b, .-_ZN12v8_inspector12_GLOBAL__N_116findMagicCommentERKNS_8String16ES3_b
	.section	.rodata._ZN12v8_inspector13findSourceURLERKNS_8String16Eb.str1.1,"aMS",@progbits,1
.LC7:
	.string	"sourceURL"
	.section	.text._ZN12v8_inspector13findSourceURLERKNS_8String16Eb,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector13findSourceURLERKNS_8String16Eb
	.type	_ZN12v8_inspector13findSourceURLERKNS_8String16Eb, @function
_ZN12v8_inspector13findSourceURLERKNS_8String16Eb:
.LFB7375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r14
	movq	%rsi, %r13
	leaq	.LC7(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movzbl	%bl, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_116findMagicCommentERKNS_8String16ES3_b
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L195
	call	_ZdlPv@PLT
.L195:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L199
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L199:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7375:
	.size	_ZN12v8_inspector13findSourceURLERKNS_8String16Eb, .-_ZN12v8_inspector13findSourceURLERKNS_8String16Eb
	.section	.rodata._ZN12v8_inspector16findSourceMapURLERKNS_8String16Eb.str1.1,"aMS",@progbits,1
.LC8:
	.string	"sourceMappingURL"
	.section	.text._ZN12v8_inspector16findSourceMapURLERKNS_8String16Eb,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector16findSourceMapURLERKNS_8String16Eb
	.type	_ZN12v8_inspector16findSourceMapURLERKNS_8String16Eb, @function
_ZN12v8_inspector16findSourceMapURLERKNS_8String16Eb:
.LFB7376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r14
	movq	%rsi, %r13
	leaq	.LC8(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movzbl	%bl, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_116findMagicCommentERKNS_8String16ES3_b
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L200
	call	_ZdlPv@PLT
.L200:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L204
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L204:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7376:
	.size	_ZN12v8_inspector16findSourceMapURLERKNS_8String16Eb, .-_ZN12v8_inspector16findSourceMapURLERKNS_8String16Eb
	.section	.rodata._ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC9:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.type	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, @function
_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_:
.LFB11325:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L219
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L215
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L220
.L207:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L214:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L221
	testq	%r13, %r13
	jg	.L210
	testq	%r9, %r9
	jne	.L213
.L211:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L210
.L213:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L220:
	testq	%rsi, %rsi
	jne	.L208
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L210:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L211
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L215:
	movl	$8, %r14d
	jmp	.L207
.L219:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L208:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L207
	.cfi_endproc
.LFE11325:
	.size	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, .-_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.section	.text._ZNSt6vectorISt4pairIiN12v8_inspector8String16EESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIiN12v8_inspector8String16EESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIiN12v8_inspector8String16EESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorISt4pairIiN12v8_inspector8String16EESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorISt4pairIiN12v8_inspector8String16EESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB11351:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %r14
	movq	%rdi, -80(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rbx, %rax
	subq	%r14, %rax
	sarq	$4, %rax
	imulq	%rcx, %rax
	movabsq	$192153584101141162, %rcx
	cmpq	%rcx, %rax
	je	.L249
	movq	%rsi, %r8
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L242
	movabsq	$9223372036854775776, %r12
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L250
.L224:
	movq	%r12, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rdx
	movq	%rax, -64(%rbp)
	addq	%rax, %r12
	movq	%r12, -72(%rbp)
	leaq	48(%rax), %r12
.L241:
	movq	-64(%rbp), %rax
	movl	(%rdx), %ecx
	movq	8(%rdx), %rdi
	addq	%r8, %rax
	movl	%ecx, (%rax)
	leaq	24(%rax), %rcx
	movq	%rcx, 8(%rax)
	leaq	24(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L251
	movq	%rdi, 8(%rax)
	movq	24(%rdx), %rdi
	movq	%rdi, 24(%rax)
.L227:
	movq	%rcx, 8(%rdx)
	xorl	%ecx, %ecx
	movq	16(%rdx), %rdi
	movw	%cx, 24(%rdx)
	movq	$0, 16(%rdx)
	movq	40(%rdx), %rdx
	movq	%rdi, 16(%rax)
	movq	%rdx, 40(%rax)
	movq	-56(%rbp), %rax
	cmpq	%r14, %rax
	je	.L228
	leaq	-48(%rax), %rdx
	movq	-64(%rbp), %r15
	leaq	24(%r14), %r13
	movabsq	$768614336404564651, %rcx
	subq	%r14, %rdx
	shrq	$4, %rdx
	imulq	%rcx, %rdx
	movabsq	$1152921504606846975, %rcx
	andq	%rcx, %rdx
	movq	%rdx, -88(%rbp)
	leaq	(%rdx,%rdx,2), %rdx
	salq	$4, %rdx
	leaq	72(%r14,%rdx), %r12
	.p2align 4,,10
	.p2align 3
.L234:
	movl	-24(%r13), %ecx
	movl	%ecx, (%r15)
	leaq	24(%r15), %rcx
	movq	%rcx, 8(%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	je	.L252
	movq	%rcx, 8(%r15)
	movq	0(%r13), %rcx
	movq	%rcx, 24(%r15)
.L230:
	movq	-8(%r13), %rcx
	xorl	%eax, %eax
	movq	%rcx, 16(%r15)
	movq	16(%r13), %rcx
	movq	%r13, -16(%r13)
	movq	$0, -8(%r13)
	movw	%ax, 0(%r13)
	movq	%rcx, 40(%r15)
	movq	-16(%r13), %rdi
	cmpq	%r13, %rdi
	je	.L231
	call	_ZdlPv@PLT
	addq	$48, %r13
	addq	$48, %r15
	cmpq	%r13, %r12
	jne	.L234
.L232:
	movq	-88(%rbp), %rax
	leaq	6(%rax,%rax,2), %r12
	salq	$4, %r12
	addq	-64(%rbp), %r12
.L228:
	movq	-56(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L235
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L239:
	movl	(%rax), %ecx
	leaq	24(%rax), %rdi
	movl	%ecx, (%rdx)
	leaq	24(%rdx), %rcx
	movq	%rcx, 8(%rdx)
	movq	8(%rax), %rcx
	cmpq	%rdi, %rcx
	je	.L253
	movq	%rcx, 8(%rdx)
	movq	24(%rax), %rcx
	addq	$48, %rax
	addq	$48, %rdx
	movq	%rcx, -24(%rdx)
	movq	-32(%rax), %rcx
	movq	%rcx, -32(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rbx, %rax
	jne	.L239
.L237:
	movabsq	$768614336404564651, %rdx
	movq	%rbx, %rsi
	subq	-56(%rbp), %rsi
	leaq	-48(%rsi), %rax
	shrq	$4, %rax
	imulq	%rdx, %rax
	movabsq	$1152921504606846975, %rdx
	andq	%rdx, %rax
	leaq	3(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	%rax, %r12
.L235:
	testq	%r14, %r14
	je	.L240
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L240:
	movq	-64(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%r12, %xmm3
	movq	-72(%rbp), %rsi
	punpcklqdq	%xmm3, %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	addq	$48, %r13
	addq	$48, %r15
	cmpq	%r12, %r13
	jne	.L234
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L253:
	movq	16(%rax), %rcx
	movdqu	24(%rax), %xmm2
	addq	$48, %rax
	addq	$48, %rdx
	movq	%rcx, -32(%rdx)
	movq	-8(%rax), %rcx
	movups	%xmm2, -24(%rdx)
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L239
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L252:
	movdqu	0(%r13), %xmm1
	movups	%xmm1, 24(%r15)
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L250:
	testq	%rdi, %rdi
	jne	.L225
	movq	$0, -72(%rbp)
	movl	$48, %r12d
	movq	$0, -64(%rbp)
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L242:
	movl	$48, %r12d
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L251:
	movdqu	24(%rdx), %xmm4
	movups	%xmm4, 24(%rax)
	jmp	.L227
.L225:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	imulq	$48, %rcx, %r12
	jmp	.L224
.L249:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11351:
	.size	_ZNSt6vectorISt4pairIiN12v8_inspector8String16EESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorISt4pairIiN12v8_inspector8String16EESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger11SearchMatchESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger11SearchMatchESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger11SearchMatchESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger11SearchMatchESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger11SearchMatchESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB11404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movabsq	$1152921504606846975, %rsi
	subq	$72, %rsp
	movq	8(%rdi), %rax
	movq	(%rdi), %r13
	movq	%rdi, -104(%rbp)
	movq	%rax, -96(%rbp)
	subq	%r13, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L282
	movq	%r12, %r8
	subq	%r13, %r8
	testq	%rax, %rax
	je	.L269
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L283
.L256:
	movq	%r15, %rdi
	movq	%rdx, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rdx
	movq	%rax, -80(%rbp)
	addq	%rax, %r15
	movq	%r15, -88(%rbp)
	leaq	8(%rax), %r15
.L268:
	movq	(%rdx), %rax
	movq	-80(%rbp), %rcx
	movq	$0, (%rdx)
	movq	%rax, (%rcx,%r8)
	cmpq	%r13, %r12
	je	.L258
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rsi
	movq	%rcx, %r15
	movq	%r13, %r14
	leaq	-64(%rsi), %rdi
	movq	%rsi, %xmm5
	movq	%rdi, %xmm3
	punpcklqdq	%xmm5, %xmm3
	movaps	%xmm3, -64(%rbp)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L285:
	movdqa	-64(%rbp), %xmm2
	movq	24(%r8), %rdi
	leaq	40(%r8), %rsi
	movups	%xmm2, (%r8)
	cmpq	%rsi, %rdi
	je	.L261
	movq	%r8, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r8
.L261:
	movl	$64, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L259:
	addq	$8, %r14
	addq	$8, %r15
	cmpq	%r14, %r12
	je	.L284
.L262:
	movq	(%r14), %rsi
	movq	$0, (%r14)
	movq	%rsi, (%r15)
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L259
	movq	(%r8), %rsi
	leaq	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev(%rip), %rax
	movq	24(%rsi), %rsi
	cmpq	%rax, %rsi
	je	.L285
	addq	$8, %r14
	movq	%r8, %rdi
	addq	$8, %r15
	call	*%rsi
	cmpq	%r14, %r12
	jne	.L262
	.p2align 4,,10
	.p2align 3
.L284:
	movq	-80(%rbp), %rcx
	movq	%r12, %rax
	subq	%r13, %rax
	leaq	8(%rcx,%rax), %r15
.L258:
	movq	-96(%rbp), %rax
	cmpq	%rax, %r12
	je	.L263
	subq	%r12, %rax
	leaq	-8(%rax), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	addq	$1, %rsi
	testq	%rdi, %rdi
	je	.L271
	movq	%rsi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L265:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%r15,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L265
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rbx
	leaq	(%r15,%rbx), %rdx
	addq	%r12, %rbx
	cmpq	%rax, %rsi
	je	.L266
.L264:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L266:
	leaq	8(%r15,%rdi), %r15
.L263:
	testq	%r13, %r13
	je	.L267
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L267:
	movq	-80(%rbp), %xmm0
	movq	-104(%rbp), %rax
	movq	%r15, %xmm4
	movq	-88(%rbp), %rcx
	punpcklqdq	%xmm4, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L257
	movq	$0, -88(%rbp)
	movl	$8, %r15d
	movq	$0, -80(%rbp)
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L269:
	movl	$8, %r15d
	jmp	.L256
.L271:
	movq	%r15, %rdx
	jmp	.L264
.L257:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	leaq	0(,%rsi,8), %r15
	jmp	.L256
.L282:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11404:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger11SearchMatchESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger11SearchMatchESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.rodata._ZN12v8_inspector23searchInTextByLinesImplEPNS_18V8InspectorSessionERKNS_8String16ES4_bb.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN12v8_inspector23searchInTextByLinesImplEPNS_18V8InspectorSessionERKNS_8String16ES4_bb,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector23searchInTextByLinesImplEPNS_18V8InspectorSessionERKNS_8String16ES4_bb
	.type	_ZN12v8_inspector23searchInTextByLinesImplEPNS_18V8InspectorSessionERKNS_8String16ES4_bb, @function
_ZN12v8_inspector23searchInTextByLinesImplEPNS_18V8InspectorSessionERKNS_8String16ES4_bb:
.LFB7359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	%r8b, %eax
	movl	%eax, -336(%rbp)
	movq	24(%rsi), %rax
	movq	%rax, -344(%rbp)
	testb	%r9b, %r9b
	je	.L287
	movq	(%rcx), %rsi
	movq	8(%rcx), %r14
	leaq	-144(%rbp), %rbx
	movq	%rbx, -160(%rbp)
	leaq	(%r14,%r14), %rdx
	movq	%rsi, %rax
	addq	%rdx, %rax
	je	.L288
	testq	%rsi, %rsi
	je	.L320
.L288:
	movq	%rdx, %r8
	movq	%rbx, %rdi
	sarq	%r8
	cmpq	$14, %rdx
	ja	.L460
.L289:
	cmpq	$2, %rdx
	je	.L461
	testq	%rdx, %rdx
	je	.L292
	movq	%r8, -352(%rbp)
	call	memmove@PLT
	movq	-160(%rbp), %rdi
	movq	-352(%rbp), %r8
.L292:
	xorl	%eax, %eax
	movq	%r8, -152(%rbp)
	movw	%ax, (%rdi,%r14,2)
	movq	32(%r12), %rax
	leaq	-160(%rbp), %r12
	movq	%rax, -128(%rbp)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L287:
	leaq	-304(%rbp), %r14
	xorl	%ebx, %ebx
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	cmpq	$0, 8(%r12)
	je	.L299
	movq	%r13, -352(%rbp)
	.p2align 4,,10
	.p2align 3
.L294:
	movq	(%r12), %rax
	movzwl	(%rax,%rbx,2), %r13d
	leal	-123(%r13), %edx
	cmpw	$2, %dx
	jbe	.L297
	leal	-36(%r13), %edx
	cmpw	$58, %dx
	ja	.L298
	movabsq	$540431955418679281, %rax
	btq	%rdx, %rax
	jnc	.L298
	.p2align 4,,10
	.p2align 3
.L297:
	movl	$92, %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
.L298:
	movzwl	%r13w, %esi
	movq	%r14, %rdi
	addq	$1, %rbx
	call	_ZN12v8_inspector15String16Builder6appendEt@PLT
	cmpq	%rbx, 8(%r12)
	ja	.L294
	movq	-352(%rbp), %r13
.L299:
	leaq	-160(%rbp), %r12
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L459
	call	_ZdlPv@PLT
.L459:
	leaq	-144(%rbp), %rbx
.L293:
	movl	$56, %edi
	call	_Znwm@PLT
	movl	-336(%rbp), %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	-344(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN12v8_inspector7V8RegexC1EPNS_15V8InspectorImplERKNS_8String16Ebb@PLT
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L300
	call	_ZdlPv@PLT
.L300:
	pxor	%xmm0, %xmm0
	cmpq	$0, 8(%r13)
	movq	$0, -288(%rbp)
	movaps	%xmm0, -304(%rbp)
	jne	.L301
	movq	$0, 16(%r15)
	movups	%xmm0, (%r15)
.L302:
	movq	-352(%rbp), %rax
	movq	16(%rax), %rdi
	addq	$32, %rax
	cmpq	%rax, %rdi
	je	.L372
	call	_ZdlPv@PLT
.L372:
	movq	-352(%rbp), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L370
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L370:
	movq	-352(%rbp), %rdi
	movl	$56, %esi
	call	_ZdlPvm@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L462
	addq	$392, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	.LC5(%rip), %rsi
	movq	$0, 16(%rax)
	movq	%rax, %r14
	movups	%xmm0, (%rax)
	leaq	-112(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -384(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	8(%r13), %r9
	movq	8(%r14), %r12
	movq	16(%r14), %rbx
	testq	%r9, %r9
	je	.L304
	movq	%r13, %rax
	movq	%r15, -368(%rbp)
	movq	%r9, %r13
	movq	%r14, %r15
	xorl	%edx, %edx
	movq	%r12, %r14
	movq	%rax, %r9
	movq	%rbx, %r12
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L463:
	movq	%rax, (%r14)
	movq	8(%r15), %rax
	leaq	1(%rbx), %rdx
	movq	8(%r9), %r13
	movq	16(%r15), %r12
	leaq	8(%rax), %r14
	movq	%r14, 8(%r15)
	cmpq	%rdx, %r13
	jbe	.L456
.L313:
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %rsi
	movq	%r9, %rdi
	movq	%r9, -336(%rbp)
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE4findEPKtmm
	movq	-336(%rbp), %r9
	cmpq	$-1, %rax
	movq	%rax, %rbx
	je	.L456
	cmpq	%r14, %r12
	jne	.L463
	movabsq	$1152921504606846975, %rcx
	movq	(%r15), %r10
	movq	%r12, %rdx
	subq	%r10, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L464
	testq	%rax, %rax
	je	.L376
	movabsq	$9223372036854775800, %r12
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L465
.L308:
	movq	%r12, %rdi
	movq	%r9, -360(%rbp)
	movq	%rdx, -344(%rbp)
	movq	%r10, -336(%rbp)
	call	_Znwm@PLT
	movq	-336(%rbp), %r10
	movq	-344(%rbp), %rdx
	movq	-360(%rbp), %r9
	movq	%rax, %r13
	addq	%rax, %r12
.L309:
	movq	%rbx, 0(%r13,%rdx)
	leaq	8(%r13,%rdx), %r14
	testq	%rdx, %rdx
	jg	.L466
	testq	%r10, %r10
	jne	.L311
.L312:
	movq	%r13, %xmm0
	movq	8(%r9), %r13
	leaq	1(%rbx), %rdx
	movq	%r12, 16(%r15)
	movq	%r14, %xmm6
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, (%r15)
	cmpq	%rdx, %r13
	ja	.L313
	.p2align 4,,10
	.p2align 3
.L456:
	movq	%r9, %rax
	movq	%r12, %rbx
	movq	%r14, %r12
	movq	%r15, %r14
	movq	-368(%rbp), %r15
	movq	%r13, %r9
	movq	%rax, %r13
.L304:
	movq	%r9, -312(%rbp)
	cmpq	%rbx, %r12
	je	.L314
	movq	%r9, (%r12)
	addq	$8, 8(%r14)
.L315:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	movq	%rax, -400(%rbp)
	cmpq	%rax, %rdi
	je	.L316
	call	_ZdlPv@PLT
.L316:
	movq	8(%r14), %rdx
	movq	(%r14), %rdi
	movq	%rdx, %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	movq	%rax, -360(%rbp)
	je	.L317
	leaq	-96(%rbp), %rax
	movq	%r14, -336(%rbp)
	xorl	%r8d, %r8d
	xorl	%r12d, %r12d
	movq	%rax, -416(%rbp)
	leaq	-256(%rbp), %rbx
	movq	%r15, -408(%rbp)
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L475:
	movq	%r8, -344(%rbp)
	addq	$1, %r12
	call	_ZdlPv@PLT
	movq	-336(%rbp), %rax
	cmpq	%r12, -360(%rbp)
	movq	-344(%rbp), %r8
	movq	(%rax), %rdi
	je	.L457
.L350:
	movq	-336(%rbp), %rax
	movq	8(%rax), %rdx
.L351:
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%r12, %rdx
	jbe	.L467
	movq	(%rdi,%r12,8), %r15
	movq	8(%r13), %rcx
	movq	%r15, %rax
	subq	%r8, %rax
	cmpq	%r8, %rcx
	jb	.L468
	subq	%r8, %rcx
	movq	0(%r13), %rdx
	movq	%rbx, -272(%rbp)
	cmpq	%rax, %rcx
	cmovbe	%rcx, %rax
	leaq	(%rdx,%r8,2), %rsi
	movq	%rax, %r14
	leaq	(%rax,%rax), %rdx
	movq	%rsi, %rax
	addq	%rdx, %rax
	je	.L384
	testq	%rsi, %rsi
	je	.L320
.L384:
	movq	%rdx, %r8
	movq	%rbx, %rdi
	sarq	%r8
	cmpq	$14, %rdx
	ja	.L469
.L322:
	cmpq	$2, %rdx
	je	.L470
	testq	%rdx, %rdx
	je	.L325
	movq	%r8, -344(%rbp)
	call	memmove@PLT
	movq	-272(%rbp), %rdi
	movq	-344(%rbp), %r8
.L325:
	xorl	%r11d, %r11d
	movq	%r8, -264(%rbp)
	leaq	-272(%rbp), %rsi
	movw	%r11w, (%rdi,%r14,2)
	leaq	-208(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-272(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L326
	call	_ZdlPv@PLT
.L326:
	movq	-200(%rbp), %rax
	testq	%rax, %rax
	je	.L327
	movq	-208(%rbp), %rsi
	leaq	-1(%rax), %rcx
	cmpw	$13, -2(%rsi,%rax,2)
	je	.L471
.L327:
	movq	-352(%rbp), %rdi
	xorl	%edx, %edx
	leaq	-312(%rbp), %rcx
	movq	%r14, %rsi
	call	_ZNK12v8_inspector7V8Regex5matchERKNS_8String16EiPi@PLT
	cmpl	$-1, %eax
	je	.L340
	movq	-200(%rbp), %rax
	movq	-208(%rbp), %rsi
	leaq	-88(%rbp), %rcx
	movl	%r12d, -112(%rbp)
	movq	%rcx, -104(%rbp)
	leaq	(%rax,%rax), %rdx
	movq	%rsi, %rax
	addq	%rdx, %rax
	je	.L386
	testq	%rsi, %rsi
	je	.L320
.L386:
	movq	%rdx, %r8
	movq	%rcx, %rdi
	sarq	%r8
	cmpq	$14, %rdx
	ja	.L472
.L342:
	cmpq	$2, %rdx
	je	.L473
	testq	%rdx, %rdx
	je	.L344
	movq	%rcx, -376(%rbp)
	movq	%r8, -368(%rbp)
	movq	%rdx, -344(%rbp)
	call	memmove@PLT
	movq	-104(%rbp), %rdi
	movq	-376(%rbp), %rcx
	movq	-368(%rbp), %r8
	movq	-344(%rbp), %rdx
.L344:
	xorl	%esi, %esi
	movq	%r8, -96(%rbp)
	movw	%si, (%rdi,%rdx)
	movq	-176(%rbp), %rax
	movq	-296(%rbp), %rsi
	movq	%rax, -72(%rbp)
	cmpq	-288(%rbp), %rsi
	je	.L345
	movl	-112(%rbp), %eax
	movl	%eax, (%rsi)
	leaq	24(%rsi), %rax
	movq	%rax, 8(%rsi)
	movq	-104(%rbp), %rax
	cmpq	%rcx, %rax
	je	.L474
	movq	%rax, 8(%rsi)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%rsi)
.L347:
	movq	-96(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	-72(%rbp), %rax
	movq	%rax, 40(%rsi)
	addq	$48, -296(%rbp)
.L340:
	movq	-208(%rbp), %rdi
	leaq	-192(%rbp), %rax
	leaq	1(%r15), %r8
	cmpq	%rax, %rdi
	jne	.L475
	movq	-336(%rbp), %rax
	addq	$1, %r12
	movq	(%rax), %rdi
	cmpq	%r12, -360(%rbp)
	jne	.L350
.L457:
	movq	-408(%rbp), %r15
	movq	%rax, %r14
.L317:
	testq	%rdi, %rdi
	je	.L352
	call	_ZdlPv@PLT
.L352:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	movq	-304(%rbp), %r8
	movq	-296(%rbp), %r12
	pxor	%xmm0, %xmm0
	movq	$0, 16(%r15)
	movups	%xmm0, (%r15)
	cmpq	%r12, %r8
	je	.L353
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rdx
	movq	%r12, -344(%rbp)
	leaq	8(%r8), %r14
	leaq	64(%rdx), %rax
	movq	%rdx, %xmm3
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm3
	movaps	%xmm3, -336(%rbp)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L355:
	leaq	(%r12,%r12), %r13
	testq	%r12, %r12
	jne	.L476
.L358:
	xorl	%eax, %eax
	movq	%r12, 32(%rbx)
	movw	%ax, (%rdx,%r13)
.L354:
	movq	32(%r14), %rax
	movq	8(%r15), %rsi
	movq	%rbx, -312(%rbp)
	movq	%rax, 56(%rbx)
	cmpq	16(%r15), %rsi
	je	.L360
	movq	$0, -312(%rbp)
	movq	%rbx, (%rsi)
	addq	$8, 8(%r15)
.L361:
	movq	-312(%rbp), %r13
	testq	%r13, %r13
	je	.L362
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L363
	movdqa	-336(%rbp), %xmm2
	movq	24(%r13), %rdi
	leaq	40(%r13), %rax
	movups	%xmm2, 0(%r13)
	cmpq	%rax, %rdi
	je	.L364
	call	_ZdlPv@PLT
.L364:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L362:
	leaq	48(%r14), %rax
	addq	$40, %r14
	cmpq	%r14, -344(%rbp)
	je	.L477
	movq	%rax, %r14
.L365:
	movl	-8(%r14), %r13d
	movl	$64, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movdqa	-336(%rbp), %xmm1
	xorl	%ecx, %ecx
	cvtsi2sdl	%r13d, %xmm0
	movq	%rax, %rbx
	leaq	24(%rax), %rax
	leaq	40(%rbx), %rdx
	movups	%xmm1, -24(%rax)
	movq	%rdx, 24(%rbx)
	movq	$0, 32(%rbx)
	movw	%cx, 40(%rbx)
	movq	$0, 56(%rbx)
	movsd	%xmm0, 16(%rbx)
	cmpq	%rax, %r14
	je	.L354
	movq	8(%r14), %r12
	cmpq	$7, %r12
	jbe	.L355
	movabsq	$2305843009213693951, %rax
	movq	%rdx, -360(%rbp)
	cmpq	%rax, %r12
	ja	.L323
	cmpq	$14, %r12
	movl	$14, %r13d
	cmovnb	%r12, %r13
	leaq	2(%r13,%r13), %rdi
	call	_Znwm@PLT
	movq	24(%rbx), %rdi
	movq	-360(%rbp), %rdx
	cmpq	%rdx, %rdi
	je	.L356
	movq	%rax, -360(%rbp)
	call	_ZdlPv@PLT
	movq	-360(%rbp), %rax
.L356:
	movq	%r13, 40(%rbx)
	leaq	(%r12,%r12), %r13
	movq	%rax, 24(%rbx)
	movq	(%r14), %rsi
.L357:
	movq	%r13, %rdx
	movq	%rax, %rdi
	call	memmove@PLT
	movq	24(%rbx), %rdx
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L470:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-272(%rbp), %rdi
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L469:
	movabsq	$2305843009213693951, %rax
	movq	%rsi, -376(%rbp)
	cmpq	%rax, %r8
	movq	%r8, -368(%rbp)
	ja	.L323
	leaq	2(%rdx), %rdi
	movq	%rdx, -344(%rbp)
	call	_Znwm@PLT
	movq	-368(%rbp), %r8
	movq	-376(%rbp), %rsi
	movq	%rax, -272(%rbp)
	movq	-344(%rbp), %rdx
	movq	%rax, %rdi
	movq	%r8, -256(%rbp)
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L345:
	movq	-384(%rbp), %rdx
	leaq	-304(%rbp), %rdi
	movq	%rcx, -344(%rbp)
	call	_ZNSt6vectorISt4pairIiN12v8_inspector8String16EESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	-104(%rbp), %rdi
	movq	-344(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L340
	call	_ZdlPv@PLT
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L360:
	leaq	-312(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger11SearchMatchESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L363:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L476:
	movq	(%r14), %rsi
	cmpq	$1, %r12
	jne	.L359
	movzwl	(%rsi), %eax
	movl	$2, %r13d
	movw	%ax, 40(%rbx)
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L477:
	movq	-296(%rbp), %rbx
	movq	-304(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L353
	.p2align 4,,10
	.p2align 3
.L369:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L366
	call	_ZdlPv@PLT
	addq	$48, %r12
	cmpq	%rbx, %r12
	jne	.L369
	movq	-304(%rbp), %r12
.L353:
	testq	%r12, %r12
	je	.L302
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L473:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-104(%rbp), %rdi
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L472:
	movabsq	$2305843009213693951, %rax
	movq	%rcx, -392(%rbp)
	cmpq	%rax, %r8
	movq	%rsi, -376(%rbp)
	movq	%r8, -368(%rbp)
	ja	.L323
	leaq	2(%rdx), %rdi
	movq	%rdx, -344(%rbp)
	call	_Znwm@PLT
	movq	-368(%rbp), %r8
	movq	-392(%rbp), %rcx
	movq	%rax, -104(%rbp)
	movq	-376(%rbp), %rsi
	movq	%rax, %rdi
	movq	%r8, -88(%rbp)
	movq	-344(%rbp), %rdx
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L366:
	addq	$48, %r12
	cmpq	%r12, %rbx
	jne	.L369
	movq	-304(%rbp), %r12
	jmp	.L353
.L359:
	movq	%rdx, %rax
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L474:
	movdqu	-88(%rbp), %xmm5
	movups	%xmm5, 24(%rsi)
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L471:
	cmpq	%rcx, %rax
	leaq	-224(%rbp), %r9
	cmovbe	%rax, %rcx
	movq	%rsi, %rax
	movq	%r9, -240(%rbp)
	leaq	(%rcx,%rcx), %rdx
	addq	%rdx, %rax
	je	.L385
	testq	%rsi, %rsi
	je	.L320
.L385:
	movq	%rdx, %r10
	movq	%r9, %rdi
	sarq	%r10
	cmpq	$14, %rdx
	ja	.L478
.L329:
	cmpq	$2, %rdx
	je	.L479
	testq	%rdx, %rdx
	je	.L331
	movq	%r9, -376(%rbp)
	movq	%r10, -368(%rbp)
	movq	%rcx, -344(%rbp)
	call	memmove@PLT
	movq	-240(%rbp), %rdi
	movq	-376(%rbp), %r9
	movq	-368(%rbp), %r10
	movq	-344(%rbp), %rcx
.L331:
	movq	%r10, -232(%rbp)
	xorl	%r10d, %r10d
	leaq	-240(%rbp), %rsi
	movw	%r10w, (%rdi,%rcx,2)
	movq	-384(%rbp), %rdi
	movq	%r9, -344(%rbp)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-240(%rbp), %rdi
	movq	-344(%rbp), %r9
	cmpq	%r9, %rdi
	je	.L332
	call	_ZdlPv@PLT
.L332:
	movq	-208(%rbp), %rdi
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rax
	cmpq	-400(%rbp), %rdx
	je	.L480
	leaq	-192(%rbp), %rcx
	movq	-96(%rbp), %rsi
	cmpq	%rcx, %rdi
	je	.L481
	movq	%rax, %xmm0
	movq	%rsi, %xmm7
	movq	-192(%rbp), %rcx
	movq	%rdx, -208(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, -200(%rbp)
	testq	%rdi, %rdi
	je	.L338
	movq	%rdi, -112(%rbp)
	movq	%rcx, -96(%rbp)
.L336:
	xorl	%r8d, %r8d
	movq	$0, -104(%rbp)
	movw	%r8w, (%rdi)
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movq	%rax, -176(%rbp)
	cmpq	-400(%rbp), %rdi
	je	.L327
	call	_ZdlPv@PLT
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L466:
	movq	%r10, %rsi
	movq	%r13, %rdi
	movq	%r9, -344(%rbp)
	movq	%r10, -336(%rbp)
	call	memmove@PLT
	movq	-336(%rbp), %r10
	movq	-344(%rbp), %r9
.L311:
	movq	%r10, %rdi
	movq	%r9, -336(%rbp)
	call	_ZdlPv@PLT
	movq	-336(%rbp), %r9
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L465:
	testq	%rcx, %rcx
	jne	.L482
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L479:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-240(%rbp), %rdi
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L478:
	movabsq	$2305843009213693951, %rax
	movq	%r9, -424(%rbp)
	cmpq	%rax, %r10
	movq	%rcx, -392(%rbp)
	movq	%rsi, -376(%rbp)
	movq	%r10, -368(%rbp)
	ja	.L323
	leaq	2(%rdx), %rdi
	movq	%rdx, -344(%rbp)
	call	_Znwm@PLT
	movq	-368(%rbp), %r10
	movq	-424(%rbp), %r9
	movq	%rax, -240(%rbp)
	movq	-392(%rbp), %rcx
	movq	%rax, %rdi
	movq	%r10, -224(%rbp)
	movq	-376(%rbp), %rsi
	movq	-344(%rbp), %rdx
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L461:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-160(%rbp), %rdi
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L480:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L334
	cmpq	$1, %rax
	je	.L483
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L334
	movq	-400(%rbp), %rsi
	call	memmove@PLT
	movq	-104(%rbp), %rax
	movq	-208(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L334:
	xorl	%r9d, %r9d
	movq	%rax, -200(%rbp)
	movw	%r9w, (%rdi,%rdx)
	movq	-112(%rbp), %rdi
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L460:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r8
	ja	.L323
	leaq	2(%rdx), %rdi
	movq	%r8, -368(%rbp)
	movq	%rsi, -360(%rbp)
	movq	%rdx, -352(%rbp)
	call	_Znwm@PLT
	movq	-368(%rbp), %r8
	movq	-360(%rbp), %rsi
	movq	%rax, -160(%rbp)
	movq	-352(%rbp), %rdx
	movq	%rax, %rdi
	movq	%r8, -144(%rbp)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L376:
	movl	$8, %r12d
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L481:
	movq	%rax, %xmm0
	movq	%rsi, %xmm7
	movq	%rdx, -208(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, -200(%rbp)
.L338:
	movq	-400(%rbp), %rax
	movq	-416(%rbp), %rdi
	movq	%rax, -112(%rbp)
	movq	%rdi, -400(%rbp)
	jmp	.L336
.L314:
	leaq	-312(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L315
.L483:
	movzwl	-96(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-104(%rbp), %rax
	movq	-208(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L334
.L467:
	movq	%r12, %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L468:
	movq	%r8, %rdx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L323:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L320:
	leaq	.LC3(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L482:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	movq	%rax, %r12
	cmovbe	%rcx, %r12
	salq	$3, %r12
	jmp	.L308
.L462:
	call	__stack_chk_fail@PLT
.L464:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7359:
	.size	_ZN12v8_inspector23searchInTextByLinesImplEPNS_18V8InspectorSessionERKNS_8String16ES4_bb, .-_ZN12v8_inspector23searchInTextByLinesImplEPNS_18V8InspectorSessionERKNS_8String16ES4_bb
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
