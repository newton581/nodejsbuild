	.file	"debug-evaluate.cc"
	.text
	.section	.text._ZN2v88internal13DebugEvaluate6GlobalEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13DebugEvaluate6GlobalEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb
	.type	_ZN2v88internal13DebugEvaluate6GlobalEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb, @function
_ZN2v88internal13DebugEvaluate6GlobalEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb:
.LFB20252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -104(%rbp)
	movq	41472(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	12(%r12), %eax
	movb	%dl, 12(%r12)
	movb	%al, -105(%rbp)
	movq	12464(%rdi), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L3:
	subq	$8, %rsp
	leaq	-96(%rbp), %r14
	movq	%r13, %rsi
	xorl	%r9d, %r9d
	pushq	$0
	leaq	128(%r15), %rax
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	pushq	$0
	movl	$2, %ecx
	movq	%r15, %rdi
	pushq	$0
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZN2v88internal8Compiler30GetSharedFunctionInfoForScriptEPNS0_7IsolateENS0_6HandleINS0_6StringEEERKNS1_13ScriptDetailsENS_19ScriptOriginOptionsEPNS_9ExtensionEPNS0_10ScriptDataENS_14ScriptCompiler14CompileOptionsENSF_13NoCacheReasonENS0_11NativesFlagE@PLT
	addq	$32, %rsp
	movq	%rax, %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L6
	movl	$1, %ecx
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE@PLT
	cmpb	$0, -104(%rbp)
	movq	%rax, %r13
	jne	.L15
.L7:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal7Context12global_proxyEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L8
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L9:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	cmpb	$0, -104(%rbp)
	je	.L6
	movq	41472(%r15), %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal5Debug23StopSideEffectCheckModeEv@PLT
	movq	-104(%rbp), %rax
.L6:
	movzbl	-105(%rbp), %ecx
	movb	%cl, 12(%r12)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L16
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	41088(%r15), %rdx
	cmpq	41096(%r15), %rdx
	je	.L17
.L10:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdx)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L2:
	movq	41088(%r15), %rbx
	cmpq	41096(%r15), %rbx
	je	.L18
.L4:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L15:
	movq	41472(%r15), %rdi
	call	_ZN2v88internal5Debug24StartSideEffectCheckModeEv@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%r15, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%r15, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L10
.L16:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20252:
	.size	_ZN2v88internal13DebugEvaluate6GlobalEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb, .-_ZN2v88internal13DebugEvaluate6GlobalEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb
	.section	.rodata._ZN2v88internal13DebugEvaluate20WithTopmostArgumentsEPNS0_7IsolateENS0_6HandleINS0_6StringEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"(location_) != nullptr"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal13DebugEvaluate20WithTopmostArgumentsEPNS0_7IsolateENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13DebugEvaluate20WithTopmostArgumentsEPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal13DebugEvaluate20WithTopmostArgumentsEPNS0_7IsolateENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal13DebugEvaluate20WithTopmostArgumentsEPNS0_7IsolateENS0_6HandleINS0_6StringEEE:
.LFB20260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-1504(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1512, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -1536(%rbp)
	movq	41472(%rdi), %r14
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	12(%r14), %eax
	movb	$1, 12(%r14)
	movb	%al, -1521(%rbp)
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L20
	movq	%r13, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
.L20:
	movq	(%rdi), %rax
	call	*96(%rax)
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L21
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L22:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	-88(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal9Accessors20FunctionGetArgumentsEPNS0_15JavaScriptFrameEi@PLT
	xorl	%ecx, %ecx
	leaq	2040(%r12), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	je	.L29
	movq	-88(%rbp), %rdi
	movq	(%rdi), %rax
	call	*80(%rax)
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L25
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L26:
	cmpq	%rsi, 96(%r12)
	je	.L28
	xorl	%ecx, %ecx
	leaq	3424(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	je	.L29
.L28:
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-1512(%rbp), %r15
	call	_ZN2v88internal9ScopeInfo18CreateForWithScopeEPNS0_7IsolateENS0_11MaybeHandleIS1_EE@PLT
	movq	%r15, %rdi
	movq	%rax, %rdx
	movq	(%rax), %rax
	movq	%rdx, -1544(%rbp)
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal9ScopeInfo23SetIsDebugEvaluateScopeEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movq	-1544(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory23NewDebugEvaluateContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_10JSReceiverEEES4_NS2_INS0_9StringSetEEE@PLT
	movq	%rax, -1544(%rbp)
	movq	(%rbx), %rax
	movq	343(%rax), %rax
	movq	23(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L30
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L31:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal7Context12global_proxyEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L33
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L34:
	pushq	$-1
	movq	-1544(%rbp), %rdx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	pushq	$-1
	movq	-1536(%rbp), %rdi
	movl	$-1, %r9d
	movq	%r13, %rsi
	call	_ZN2v88internal8Compiler19GetFunctionFromEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeENS0_16ParseRestrictionEiii@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L37
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
.L37:
	movzbl	-1521(%rbp), %ecx
	movb	%cl, 12(%r14)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L47
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L48
.L27:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L21:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L49
.L23:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L33:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L50
.L35:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L30:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L51
.L32:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%r12, %rdi
	movq	%rsi, -1544(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1544(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%r12, %rdi
	movq	%rax, -1544(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1544(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%r12, %rdi
	movq	%rsi, -1552(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1552(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%r12, %rdi
	movq	%rax, -1552(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1552(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L35
.L47:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20260:
	.size	_ZN2v88internal13DebugEvaluate20WithTopmostArgumentsEPNS0_7IsolateENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal13DebugEvaluate20WithTopmostArgumentsEPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal13DebugEvaluate8EvaluateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_6StringEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13DebugEvaluate8EvaluateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_6StringEEEb
	.type	_ZN2v88internal13DebugEvaluate8EvaluateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_6StringEEEb, @function
_ZN2v88internal13DebugEvaluate8EvaluateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_6StringEEEb:
.LFB20264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	xorl	%ecx, %ecx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%r8, %rdi
	xorl	%r8d, %r8d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%r9d, %ebx
	movl	$-1, %r9d
	pushq	$-1
	pushq	$-1
	call	_ZN2v88internal8Compiler19GetFunctionFromEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeENS0_16ParseRestrictionEiii@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L59
	movq	%rax, %r12
	testb	%bl, %bl
	jne	.L68
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L59
	movq	%rbx, %rax
.L70:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L69:
	.cfi_restore_state
	movq	41472(%r13), %rdi
	call	_ZN2v88internal5Debug23StopSideEffectCheckModeEv@PLT
.L59:
	leaq	-32(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	41472(%r13), %rdi
	call	_ZN2v88internal5Debug24StartSideEffectCheckModeEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L69
	movq	41472(%r13), %rdi
	call	_ZN2v88internal5Debug23StopSideEffectCheckModeEv@PLT
	movq	%rbx, %rax
	jmp	.L70
	.cfi_endproc
.LFE20264:
	.size	_ZN2v88internal13DebugEvaluate8EvaluateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_6StringEEEb, .-_ZN2v88internal13DebugEvaluate8EvaluateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_6StringEEEb
	.section	.text._ZNK2v88internal13DebugEvaluate14ContextBuilder10outer_infoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13DebugEvaluate14ContextBuilder10outer_infoEv
	.type	_ZNK2v88internal13DebugEvaluate14ContextBuilder10outer_infoEv, @function
_ZNK2v88internal13DebugEvaluate14ContextBuilder10outer_infoEv:
.LFB20265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	96(%rdi), %rax
	movq	32(%rdi), %rbx
	movq	(%rax), %rax
	movq	23(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L72
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L76
.L74:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L74
	.cfi_endproc
.LFE20265:
	.size	_ZNK2v88internal13DebugEvaluate14ContextBuilder10outer_infoEv, .-_ZNK2v88internal13DebugEvaluate14ContextBuilder10outer_infoEv
	.section	.text._ZN2v88internal13DebugEvaluate14ContextBuilder12UpdateValuesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13DebugEvaluate14ContextBuilder12UpdateValuesEv
	.type	_ZN2v88internal13DebugEvaluate14ContextBuilder12UpdateValuesEv, @function
_ZN2v88internal13DebugEvaluate14ContextBuilder12UpdateValuesEv:
.LFB20281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	120(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%rdi, -160(%rbp)
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal13ScopeIterator7RestartEv@PLT
	movq	16(%rbx), %rax
	movq	8(%rbx), %r12
	movq	%rax, -176(%rbp)
	cmpq	%r12, %rax
	jne	.L89
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%r15, %rdi
	addq	$24, %r12
	call	_ZN2v88internal13ScopeIterator4NextEv@PLT
	cmpq	%r12, -176(%rbp)
	je	.L77
.L89:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L79
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$18, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L100
	movq	(%rax), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L79
	xorl	%ebx, %ebx
	leaq	-144(%rbp), %r14
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L102:
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rdx
.L87:
	movq	%r13, %rsi
	movq	%r15, %rdi
	addq	$1, %rbx
	call	_ZN2v88internal13ScopeIterator16SetVariableValueENS0_6HandleINS0_6StringEEENS2_INS0_6ObjectEEE@PLT
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
	cmpl	%ebx, 11(%rax)
	jle	.L79
.L88:
	movq	-160(%rbp), %rsi
	movq	32(%rsi), %rdx
	movq	15(%rax,%rbx,8), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L81
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L82:
	movq	8(%r12), %rdx
	movq	0(%r13), %rcx
	movq	(%rdx), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rcx), %r8
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%r8)
	jne	.L84
	xorl	%eax, %eax
	testb	$1, 11(%rcx)
	sete	%al
	addl	%eax, %eax
.L84:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	%rdi, -120(%rbp)
	movq	0(%r13), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %ecx
	movq	%r13, %rax
	andl	$-32, %ecx
	cmpl	$32, %ecx
	je	.L101
.L85:
	movq	%r14, %rdi
	movq	%rax, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	je	.L102
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	%rax, %rdx
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L81:
	movq	41088(%rdx), %r13
	cmpq	41096(%rdx), %r13
	je	.L103
.L83:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, 0(%r13)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L101:
	movq	%r13, %rsi
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-168(%rbp), %rdx
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%rdx, %rdi
	movq	%rsi, -184(%rbp)
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	movq	-168(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L77:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L104
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20281:
	.size	_ZN2v88internal13DebugEvaluate14ContextBuilder12UpdateValuesEv, .-_ZN2v88internal13DebugEvaluate14ContextBuilder12UpdateValuesEv
	.section	.rodata._ZN2v88internal13DebugEvaluate26FunctionGetSideEffectStateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"[debug-evaluate] Checking function %s for side effect.\n"
	.align 8
.LC3:
	.string	"[debug-evaluate] intrinsic %s may cause side effect.\n"
	.align 8
.LC4:
	.string	"[debug-evaluate] bytecode %s may cause side effect.\n"
	.align 8
.LC5:
	.string	"[debug-evaluate] built-in %s may cause side effect.\n"
	.section	.text._ZN2v88internal13DebugEvaluate26FunctionGetSideEffectStateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13DebugEvaluate26FunctionGetSideEffectStateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal13DebugEvaluate26FunctionGetSideEffectStateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal13DebugEvaluate26FunctionGetSideEffectStateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB20286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal42FLAG_trace_side_effect_free_debug_evaluateE(%rip)
	movq	(%rsi), %rax
	jne	.L182
.L106:
	leaq	7(%rax), %rdx
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L183
.L108:
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L184
	movq	(%rbx), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L185
.L113:
	movq	(%rbx), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	je	.L186
.L181:
	movl	$1, %eax
.L105:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L187
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L108
.L112:
	movq	(%rbx), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L188
.L109:
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L189
.L116:
	movq	7(%rax), %rax
	movq	7(%rax), %rsi
.L115:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L117
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	cmpb	$0, _ZN2v88internal42FLAG_trace_side_effect_free_debug_evaluateE(%rip)
	leaq	-80(%rbp), %r15
	movq	%rax, %r13
	jne	.L190
.L120:
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	.L128(%rip), %r14
	xorl	%ebx, %ebx
	call	_ZN2v88internal11interpreter21BytecodeArrayIteratorC1ENS0_6HandleINS0_13BytecodeArrayEEE@PLT
	leaq	.L140(%rip), %r13
	leaq	.L141(%rip), %r12
.L144:
	movq	%r15, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv@PLT
	testb	%al, %al
	jne	.L121
	movq	%r15, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	leal	-97(%rax), %edx
	cmpb	$1, %dl
	jbe	.L162
	cmpb	$100, %al
	je	.L178
	cmpb	$39, %al
	jbe	.L131
	leal	-112(%rax), %edx
	cmpb	$3, %dl
	ja	.L132
.L127:
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv@PLT
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L182:
	leaq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r15
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	leaq	-88(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-80(%rbp), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L107
	call	_ZdaPv@PLT
.L107:
	movq	(%rbx), %rax
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L184:
	movq	-1(%rax), %rax
	cmpw	$91, 11(%rax)
	je	.L112
	movq	(%rbx), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	je	.L113
.L185:
	movq	-1(%rax), %rax
	cmpw	$88, 11(%rax)
	jne	.L113
	movq	(%rbx), %rax
	leaq	-80(%rbp), %r12
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo7GetCodeEv@PLT
	cmpl	$-1, 59(%rax)
	je	.L181
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo7GetCodeEv@PLT
	cmpl	$77, 59(%rax)
	sete	%al
	movzbl	%al, %eax
	leal	1(%rax,%rax), %eax
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L131:
	movabsq	$962337437696, %rdx
	btq	%rax, %rdx
	jc	.L127
.L132:
	leal	118(%rax), %edx
	cmpb	$22, %dl
	ja	.L133
	leal	107(%rax), %edx
	cmpb	$3, %dl
	ja	.L127
	.p2align 4,,10
	.p2align 3
.L134:
	leal	-96(%rax), %edx
	cmpb	$56, %dl
	jbe	.L135
	cmpb	$3, %al
	jbe	.L127
	cmpb	$82, %al
	ja	.L137
	cmpb	$18, %al
	ja	.L191
.L139:
	cmpb	$29, %al
	je	.L158
.L143:
	leal	-45(%rax), %edx
	cmpb	$5, %dl
	ja	.L192
.L158:
	movq	%r15, %rdi
	movl	$1, %ebx
	call	_ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv@PLT
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L162:
	cmpb	$100, %al
	je	.L178
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetRuntimeIdOperandEi@PLT
.L125:
	cmpl	$490, %eax
	ja	.L126
	movl	%eax, %edx
	movslq	(%r14,%rdx,4), %rdx
	addq	%r14, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal13DebugEvaluate26FunctionGetSideEffectStateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE,"a",@progbits
	.align 4
	.align 4
.L128:
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L126-.L128
	.long	.L127-.L128
	.section	.text._ZN2v88internal13DebugEvaluate26FunctionGetSideEffectStateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.p2align 4,,10
	.p2align 3
.L126:
	cmpb	$0, _ZN2v88internal42FLAG_trace_side_effect_free_debug_evaluateE(%rip)
	jne	.L193
.L130:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L181
	movq	(%rdi), %rax
	call	*72(%rax)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L117:
	movq	41088(%r12), %r13
	cmpq	%r13, 41096(%r12)
	je	.L194
.L119:
	leaq	8(%r13), %rax
	leaq	-80(%rbp), %r15
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	cmpb	$0, _ZN2v88internal42FLAG_trace_side_effect_free_debug_evaluateE(%rip)
	je	.L120
.L190:
	movq	0(%r13), %rax
	movq	stdout(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L186:
	movq	(%rbx), %rax
	movq	7(%rax), %rdi
	sarq	$32, %rdi
	cmpl	$1552, %edi
	ja	.L181
	leal	-55(%rdi), %eax
	cmpl	$943, %eax
	ja	.L153
	leaq	.L154(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal13DebugEvaluate26FunctionGetSideEffectStateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.align 4
	.align 4
.L154:
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L155-.L154
	.long	.L153-.L154
	.long	.L155-.L154
	.long	.L153-.L154
	.long	.L155-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L155-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L155-.L154
	.long	.L155-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L155-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L155-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L155-.L154
	.long	.L155-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L155-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L155-.L154
	.long	.L155-.L154
	.long	.L155-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L155-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L155-.L154
	.long	.L155-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L155-.L154
	.long	.L155-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L155-.L154
	.long	.L153-.L154
	.long	.L155-.L154
	.long	.L155-.L154
	.long	.L155-.L154
	.long	.L155-.L154
	.long	.L155-.L154
	.long	.L155-.L154
	.long	.L155-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L155-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L153-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.long	.L161-.L154
	.section	.text._ZN2v88internal13DebugEvaluate26FunctionGetSideEffectStateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.p2align 4,,10
	.p2align 3
.L193:
	movl	%eax, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	leaq	.LC3(%rip), %rdi
	movq	8(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L178:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor21GetIntrinsicIdOperandEi@PLT
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L188:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L109
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L109
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L109
	movq	31(%rdx), %rsi
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L189:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L116
	movq	7(%rax), %rsi
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%r12, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L153:
	cmpb	$0, _ZN2v88internal42FLAG_trace_side_effect_free_debug_evaluateE(%rip)
	je	.L181
	call	_ZN2v88internal8Builtins4nameEi@PLT
	leaq	.LC5(%rip), %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L181
.L161:
	movl	$3, %eax
	jmp	.L105
.L155:
	movl	$2, %eax
	jmp	.L105
.L191:
	leal	-19(%rax), %edx
	cmpb	$63, %dl
	ja	.L139
	movzbl	%dl, %edx
	movslq	(%r12,%rdx,4), %rdx
	addq	%r12, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal13DebugEvaluate26FunctionGetSideEffectStateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.align 4
	.align 4
.L141:
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L127-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L127-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L139-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.long	.L127-.L141
	.section	.text._ZN2v88internal13DebugEvaluate26FunctionGetSideEffectStateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.p2align 4,,10
	.p2align 3
.L192:
	cmpb	$0, _ZN2v88internal42FLAG_trace_side_effect_free_debug_evaluateE(%rip)
	je	.L130
	movl	%eax, %edi
	call	_ZN2v88internal11interpreter9Bytecodes8ToStringENS1_8BytecodeE@PLT
	leaq	.LC4(%rip), %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L135:
	movabsq	$135107988821114953, %rcx
	btq	%rdx, %rcx
	jc	.L127
.L137:
	leal	-99(%rax), %edx
	cmpb	$83, %dl
	ja	.L143
	movzbl	%dl, %edx
	movslq	0(%r13,%rdx,4), %rdx
	addq	%r13, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal13DebugEvaluate26FunctionGetSideEffectStateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.align 4
	.align 4
.L140:
	.long	.L127-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L139-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L127-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L127-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L139-.L140
	.long	.L127-.L140
	.section	.text._ZN2v88internal13DebugEvaluate26FunctionGetSideEffectStateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.p2align 4,,10
	.p2align 3
.L121:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L145
	movq	(%rdi), %rax
	call	*72(%rax)
.L145:
	xorl	$1, %ebx
	movzbl	%bl, %eax
	addl	$2, %eax
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L133:
	cmpb	$-95, %al
	je	.L127
	cmpb	$-81, %al
	je	.L127
	leal	-86(%rax), %edx
	cmpb	$9, %dl
	jbe	.L127
	cmpb	$101, %al
	jne	.L134
	jmp	.L127
.L187:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20286:
	.size	_ZN2v88internal13DebugEvaluate26FunctionGetSideEffectStateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal13DebugEvaluate26FunctionGetSideEffectStateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal13DebugEvaluate21ApplySideEffectChecksENS0_6HandleINS0_13BytecodeArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13DebugEvaluate21ApplySideEffectChecksENS0_6HandleINS0_13BytecodeArrayEEE
	.type	_ZN2v88internal13DebugEvaluate21ApplySideEffectChecksENS0_6HandleINS0_13BytecodeArrayEEE, @function
_ZN2v88internal13DebugEvaluate21ApplySideEffectChecksENS0_6HandleINS0_13BytecodeArrayEEE:
.LFB20295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	-48(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter21BytecodeArrayIteratorC1ENS0_6HandleINS0_13BytecodeArrayEEE@PLT
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L209:
	movq	%rbx, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	cmpb	$29, %al
	je	.L197
	subl	$45, %eax
	cmpb	$5, %al
	ja	.L198
.L197:
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter21BytecodeArrayAccessor15ApplyDebugBreakEv@PLT
.L198:
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv@PLT
.L199:
	movq	%rbx, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv@PLT
	testb	%al, %al
	je	.L209
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L195
	movq	(%rdi), %rax
	call	*72(%rax)
.L195:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L210
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L210:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20295:
	.size	_ZN2v88internal13DebugEvaluate21ApplySideEffectChecksENS0_6HandleINS0_13BytecodeArrayEEE, .-_ZN2v88internal13DebugEvaluate21ApplySideEffectChecksENS0_6HandleINS0_13BytecodeArrayEEE
	.section	.rodata._ZNSt6vectorIN2v88internal13DebugEvaluate14ContextBuilder19ContextChainElementESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_.str1.1,"aMS",@progbits,1
.LC6:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal13DebugEvaluate14ContextBuilder19ContextChainElementESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal13DebugEvaluate14ContextBuilder19ContextChainElementESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal13DebugEvaluate14ContextBuilder19ContextChainElementESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal13DebugEvaluate14ContextBuilder19ContextChainElementESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal13DebugEvaluate14ContextBuilder19ContextChainElementESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB23607:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r14
	movq	%rsi, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movabsq	$384307168202282325, %rcx
	cmpq	%rcx, %rax
	je	.L228
	movq	%r12, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L220
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L229
.L213:
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r8
	movq	%rax, %r13
	leaq	(%rax,%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	24(%r13), %rbx
.L219:
	movdqu	(%rdx), %xmm2
	movq	16(%rdx), %rax
	movups	%xmm2, 0(%r13,%r8)
	movq	%rax, 16(%r13,%r8)
	cmpq	%r14, %r12
	je	.L215
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L216:
	movdqu	(%rax), %xmm1
	addq	$24, %rax
	addq	$24, %rdx
	movups	%xmm1, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L216
	leaq	-24(%r12), %rax
	subq	%r14, %rax
	shrq	$3, %rax
	leaq	48(%r13,%rax,8), %rbx
.L215:
	cmpq	%rsi, %r12
	je	.L217
	subq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-24(%rsi), %rax
	movq	%r12, %rsi
	shrq	$3, %rax
	leaq	24(,%rax,8), %rdx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, %rbx
.L217:
	testq	%r14, %r14
	je	.L218
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L218:
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rbx, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L214
	movq	$0, -56(%rbp)
	movl	$24, %ebx
	xorl	%r13d, %r13d
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L220:
	movl	$24, %ebx
	jmp	.L213
.L214:
	cmpq	%rcx, %rdi
	movq	%rcx, %rbx
	cmovbe	%rdi, %rbx
	imulq	$24, %rbx, %rbx
	jmp	.L213
.L228:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23607:
	.size	_ZNSt6vectorIN2v88internal13DebugEvaluate14ContextBuilder19ContextChainElementESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal13DebugEvaluate14ContextBuilder19ContextChainElementESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZN2v88internal13DebugEvaluate14ContextBuilderC2EPNS0_7IsolateEPNS0_15JavaScriptFrameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13DebugEvaluate14ContextBuilderC2EPNS0_7IsolateEPNS0_15JavaScriptFrameEi
	.type	_ZN2v88internal13DebugEvaluate14ContextBuilderC2EPNS0_7IsolateEPNS0_15JavaScriptFrameEi, @function
_ZN2v88internal13DebugEvaluate14ContextBuilderC2EPNS0_7IsolateEPNS0_15JavaScriptFrameEi:
.LFB20279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	movl	%ecx, %edx
	pushq	%r13
	movq	%r14, %rcx
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	40(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	120(%rbx), %r13
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, 32(%rdi)
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal14FrameInspectorC1EPNS0_13StandardFrameEiPNS0_7IsolateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	$2, %ecx
	movq	%r12, %rdx
	call	_ZN2v88internal13ScopeIteratorC1EPNS0_7IsolateEPNS0_14FrameInspectorENS1_6OptionE@PLT
	movq	96(%rbx), %rax
	movq	41112(%r14), %rdi
	movq	(%rax), %rax
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L231
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L232:
	cmpq	$0, 160(%rbx)
	movq	%rax, (%rbx)
	je	.L230
	cmpq	$0, 152(%rbx)
	je	.L235
	.p2align 4,,10
	.p2align 3
.L245:
	movq	%r13, %rdi
	call	_ZNK2v88internal13ScopeIterator4TypeEv@PLT
	movl	%eax, %r12d
	cmpl	$6, %eax
	je	.L236
	pxor	%xmm1, %xmm1
	movq	$0, -64(%rbp)
	xorl	%esi, %esi
	movq	%r13, %rdi
	movaps	%xmm1, -80(%rbp)
	cmpl	$1, %eax
	je	.L237
	call	_ZNK2v88internal13ScopeIterator14DeclaresLocalsENS1_4ModeE@PLT
	testb	%al, %al
	je	.L261
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal13ScopeIterator11ScopeObjectENS1_4ModeE@PLT
	movq	%rax, -72(%rbp)
.L261:
	movq	%r13, %rdi
	call	_ZNK2v88internal13ScopeIterator10HasContextEv@PLT
	testb	%al, %al
	je	.L241
.L251:
	movq	160(%rbx), %rax
	movq	%rax, -80(%rbp)
	cmpl	$1, %r12d
	je	.L240
.L241:
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L242
.L264:
	movdqa	-80(%rbp), %xmm3
	movq	%r13, %rdi
	movups	%xmm3, (%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 16(%rbx)
	call	_ZN2v88internal13ScopeIterator4NextEv@PLT
	cmpq	$0, 152(%rbx)
	jne	.L245
.L236:
	movq	(%rbx), %rax
.L235:
	movq	(%rax), %rax
	xorl	%r12d, %r12d
	movq	-1(%rax), %rax
	cmpw	$145, 11(%rax)
	jne	.L262
.L246:
	movq	16(%rbx), %r13
	cmpq	%r13, 8(%rbx)
	je	.L230
	leaq	-88(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L250:
	movdqu	-24(%r13), %xmm2
	movq	%r12, %rsi
	movq	%r14, %rdi
	subq	$24, %r13
	movaps	%xmm2, -80(%rbp)
	movq	16(%r13), %rax
	movq	%rax, -64(%rbp)
	movq	8(%r13), %rcx
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal9ScopeInfo18CreateForWithScopeEPNS0_7IsolateENS0_11MaybeHandleIS1_EE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal9ScopeInfo23SetIsDebugEvaluateScopeEv@PLT
	movq	-64(%rbp), %r9
	movq	-80(%rbp), %r8
	movq	%r12, %rdx
	movq	-104(%rbp), %rcx
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory23NewDebugEvaluateContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_10JSReceiverEEES4_NS2_INS0_9StringSetEEE@PLT
	movq	%rax, (%rbx)
	cmpq	%r13, 8(%rbx)
	jne	.L250
.L230:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L263
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	call	_ZN2v88internal13ScopeIterator11ScopeObjectENS1_4ModeE@PLT
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	call	_ZNK2v88internal13ScopeIterator10HasContextEv@PLT
	testb	%al, %al
	jne	.L251
.L240:
	movq	%r13, %rdi
	call	_ZN2v88internal13ScopeIterator12GetNonLocalsEv@PLT
	movq	16(%rbx), %rsi
	movq	%rax, -64(%rbp)
	cmpq	24(%rbx), %rsi
	jne	.L264
.L242:
	leaq	-80(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIN2v88internal13DebugEvaluate14ContextBuilder19ContextChainElementESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	%r13, %rdi
	call	_ZN2v88internal13ScopeIterator4NextEv@PLT
	cmpq	$0, 152(%rbx)
	jne	.L245
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L262:
	movq	(%rbx), %rax
	leaq	-80(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Context10scope_infoEv@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L247
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L231:
	movq	41088(%r14), %rax
	cmpq	%rax, 41096(%r14)
	je	.L265
.L233:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L247:
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L266
.L248:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r12)
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L265:
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	jmp	.L233
.L266:
	movq	%r14, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L248
.L263:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20279:
	.size	_ZN2v88internal13DebugEvaluate14ContextBuilderC2EPNS0_7IsolateEPNS0_15JavaScriptFrameEi, .-_ZN2v88internal13DebugEvaluate14ContextBuilderC2EPNS0_7IsolateEPNS0_15JavaScriptFrameEi
	.globl	_ZN2v88internal13DebugEvaluate14ContextBuilderC1EPNS0_7IsolateEPNS0_15JavaScriptFrameEi
	.set	_ZN2v88internal13DebugEvaluate14ContextBuilderC1EPNS0_7IsolateEPNS0_15JavaScriptFrameEi,_ZN2v88internal13DebugEvaluate14ContextBuilderC2EPNS0_7IsolateEPNS0_15JavaScriptFrameEi
	.section	.text._ZN2v88internal13DebugEvaluate5LocalEPNS0_7IsolateENS0_12StackFrameIdEiNS0_6HandleINS0_6StringEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13DebugEvaluate5LocalEPNS0_7IsolateENS0_12StackFrameIdEiNS0_6HandleINS0_6StringEEEb
	.type	_ZN2v88internal13DebugEvaluate5LocalEPNS0_7IsolateENS0_12StackFrameIdEiNS0_6HandleINS0_6StringEEEb, @function
_ZN2v88internal13DebugEvaluate5LocalEPNS0_7IsolateENS0_12StackFrameIdEiNS0_6HandleINS0_6StringEEEb:
.LFB20253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	movl	%esi, %edx
	movq	%r14, %rsi
	pushq	%rbx
	subq	$1752, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -1752(%rbp)
	movq	41472(%rdi), %rbx
	leaq	-1504(%rbp), %rdi
	movzbl	12(%rbx), %r15d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$1, 12(%rbx)
	call	_ZN2v88internal23StackTraceFrameIteratorC1EPNS0_7IsolateENS0_12StackFrameIdE@PLT
	movq	-88(%rbp), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$20, %eax
	ja	.L268
	movl	$1150992, %edx
	btq	%rax, %rdx
	jnc	.L268
	leaq	-1728(%rbp), %rax
	movq	-88(%rbp), %rdx
	movl	%r12d, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -1760(%rbp)
	call	_ZN2v88internal13DebugEvaluate14ContextBuilderC1EPNS0_7IsolateEPNS0_15JavaScriptFrameEi
	movq	12480(%r14), %rax
	cmpq	%rax, 96(%r14)
	je	.L271
	xorl	%r12d, %r12d
.L272:
	leaq	-1608(%rbp), %rdi
	call	_ZN2v88internal13ScopeIteratorD1Ev@PLT
	leaq	-1688(%rbp), %rdi
	call	_ZN2v88internal14FrameInspectorD1Ev@PLT
	movq	-1720(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L270
	call	_ZdlPv@PLT
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L268:
	leaq	88(%r14), %r12
.L270:
	movb	%r15b, 12(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L289
	addq	$1752, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	movq	-1728(%rbp), %r12
	leaq	-1736(%rbp), %rdi
	movq	(%r12), %rax
	movq	%rax, -1736(%rbp)
	call	_ZN2v88internal7Context12global_proxyEv@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L273
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L274:
	movq	-1632(%rbp), %rax
	movzbl	%r13b, %r9d
	movq	-1696(%rbp), %r13
	movq	(%rax), %rax
	movq	23(%rax), %r8
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L276
	movq	%r8, %rsi
	movq	%rcx, -1776(%rbp)
	movl	%r9d, -1768(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-1768(%rbp), %r9d
	movq	-1776(%rbp), %rcx
	movq	%rax, %rsi
.L277:
	movq	-1752(%rbp), %r8
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal13DebugEvaluate8EvaluateEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_6StringEEEb
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L272
	movq	-1760(%rbp), %rdi
	call	_ZN2v88internal13DebugEvaluate14ContextBuilder12UpdateValuesEv
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L273:
	movq	41088(%r14), %rcx
	cmpq	41096(%r14), %rcx
	je	.L290
.L275:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rcx)
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L276:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L291
.L278:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r8, (%rsi)
	jmp	.L277
.L290:
	movq	%r14, %rdi
	movq	%rax, -1768(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1768(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L275
.L291:
	movq	%r13, %rdi
	movq	%r8, -1784(%rbp)
	movq	%rcx, -1776(%rbp)
	movl	%r9d, -1768(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1784(%rbp), %r8
	movq	-1776(%rbp), %rcx
	movl	-1768(%rbp), %r9d
	movq	%rax, %rsi
	jmp	.L278
.L289:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20253:
	.size	_ZN2v88internal13DebugEvaluate5LocalEPNS0_7IsolateENS0_12StackFrameIdEiNS0_6HandleINS0_6StringEEEb, .-_ZN2v88internal13DebugEvaluate5LocalEPNS0_7IsolateENS0_12StackFrameIdEiNS0_6HandleINS0_6StringEEEb
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal13DebugEvaluate6GlobalEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal13DebugEvaluate6GlobalEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb, @function
_GLOBAL__sub_I__ZN2v88internal13DebugEvaluate6GlobalEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb:
.LFB25000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25000:
	.size	_GLOBAL__sub_I__ZN2v88internal13DebugEvaluate6GlobalEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb, .-_GLOBAL__sub_I__ZN2v88internal13DebugEvaluate6GlobalEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal13DebugEvaluate6GlobalEPNS0_7IsolateENS0_6HandleINS0_6StringEEEb
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
