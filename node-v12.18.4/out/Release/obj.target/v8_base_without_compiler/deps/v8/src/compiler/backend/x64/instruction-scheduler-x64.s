	.file	"instruction-scheduler-x64.cc"
	.text
	.section	.text._ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv
	.type	_ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv, @function
_ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv:
.LFB10749:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE10749:
	.size	_ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv, .-_ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv
	.section	.rodata._ZNK2v88internal8compiler20InstructionScheduler25GetTargetInstructionFlagsEPKNS1_11InstructionE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal8compiler20InstructionScheduler25GetTargetInstructionFlagsEPKNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler20InstructionScheduler25GetTargetInstructionFlagsEPKNS1_11InstructionE
	.type	_ZNK2v88internal8compiler20InstructionScheduler25GetTargetInstructionFlagsEPKNS1_11InstructionE, @function
_ZNK2v88internal8compiler20InstructionScheduler25GetTargetInstructionFlagsEPKNS1_11InstructionE:
.LFB10750:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movl	%edx, %eax
	andl	$511, %eax
	cmpl	$237, %eax
	ja	.L4
	cmpl	$94, %eax
	jbe	.L5
	subl	$117, %eax
	cmpl	$120, %eax
	ja	.L6
	leaq	.L8(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler20InstructionScheduler25GetTargetInstructionFlagsEPKNS1_11InstructionE,"a",@progbits
	.align 4
	.align 4
.L8:
	.long	.L13-.L8
	.long	.L13-.L8
	.long	.L13-.L8
	.long	.L13-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L9-.L8
	.long	.L9-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L11-.L8
	.long	.L11-.L8
	.long	.L11-.L8
	.long	.L11-.L8
	.long	.L9-.L8
	.long	.L11-.L8
	.long	.L11-.L8
	.long	.L11-.L8
	.long	.L11-.L8
	.long	.L9-.L8
	.long	.L12-.L8
	.long	.L11-.L8
	.long	.L10-.L8
	.long	.L10-.L8
	.long	.L10-.L8
	.long	.L10-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L10-.L8
	.long	.L10-.L8
	.long	.L10-.L8
	.long	.L10-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L6-.L8
	.long	.L9-.L8
	.long	.L9-.L8
	.long	.L15-.L8
	.section	.text._ZNK2v88internal8compiler20InstructionScheduler25GetTargetInstructionFlagsEPKNS1_11InstructionE
	.p2align 4,,10
	.p2align 3
.L14:
	subl	$421, %eax
	cmpl	$31, %eax
	ja	.L5
.L9:
	movl	$1, %r8d
.L3:
	movl	%r8d, %eax
	ret
.L15:
	movl	$2, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	cmpl	$420, %eax
	ja	.L14
	movl	$2, %r8d
	cmpl	$416, %eax
	ja	.L3
.L6:
	shrl	$9, %edx
	andl	$31, %edx
	cmpl	$1, %edx
	sbbl	%r8d, %r8d
	notl	%r8d
	andl	$3, %r8d
	movl	%r8d, %eax
	ret
.L10:
	xorl	%r8d, %r8d
	cmpb	$0, 4(%rsi)
	setne	%r8b
	addl	$1, %r8d
	movl	%r8d, %eax
	ret
.L13:
	shrl	$9, %edx
	andl	$31, %edx
	cmpl	$1, %edx
	sbbl	%r8d, %r8d
	andl	$-3, %r8d
	addl	$7, %r8d
	movl	%r8d, %eax
	ret
.L11:
	movzbl	4(%rsi), %eax
	movl	$2, %r8d
	movq	40(%rsi,%rax,8), %rax
	testb	$4, %al
	je	.L3
.L34:
	testb	$24, %al
	jne	.L3
	shrq	$5, %rax
	xorl	%r8d, %r8d
	cmpb	$11, %al
	seta	%r8b
	addl	%r8d, %r8d
	jmp	.L3
.L12:
	movzbl	4(%rsi), %eax
	movl	$1, %r8d
	testl	%eax, %eax
	je	.L3
	movq	40(%rsi,%rax,8), %rax
	movl	$2, %r8d
	testb	$4, %al
	je	.L3
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L5:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10750:
	.size	_ZNK2v88internal8compiler20InstructionScheduler25GetTargetInstructionFlagsEPKNS1_11InstructionE, .-_ZNK2v88internal8compiler20InstructionScheduler25GetTargetInstructionFlagsEPKNS1_11InstructionE
	.section	.text._ZN2v88internal8compiler20InstructionScheduler21GetInstructionLatencyEPKNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20InstructionScheduler21GetInstructionLatencyEPKNS1_11InstructionE
	.type	_ZN2v88internal8compiler20InstructionScheduler21GetInstructionLatencyEPKNS1_11InstructionE, @function
_ZN2v88internal8compiler20InstructionScheduler21GetInstructionLatencyEPKNS1_11InstructionE:
.LFB10751:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	movl	$1, %r8d
	andl	$511, %eax
	subl	$26, %eax
	cmpl	$149, %eax
	ja	.L35
	leaq	CSWTCH.36(%rip), %rdx
	movsbl	(%rdx,%rax), %r8d
.L35:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE10751:
	.size	_ZN2v88internal8compiler20InstructionScheduler21GetInstructionLatencyEPKNS1_11InstructionE, .-_ZN2v88internal8compiler20InstructionScheduler21GetInstructionLatencyEPKNS1_11InstructionE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv, @function
_GLOBAL__sub_I__ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv:
.LFB12970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12970:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv, .-_GLOBAL__sub_I__ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv
	.section	.rodata.CSWTCH.36,"a"
	.align 32
	.type	CSWTCH.36, @object
	.size	CSWTCH.36, 150
CSWTCH.36:
	.byte	6
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	49
	.byte	35
	.byte	38
	.byte	26
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	13
	.byte	3
	.byte	3
	.byte	13
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	5
	.byte	13
	.byte	50
	.byte	3
	.byte	3
	.byte	13
	.byte	4
	.byte	1
	.byte	3
	.byte	1
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
