	.file	"literal-objects.cc"
	.text
	.section	.rodata._ZN2v88internal12_GLOBAL__N_123AddToDictionaryTemplateINS0_14NameDictionaryENS0_6HandleINS0_4NameEEEEEvPNS0_7IsolateENS4_IT_EET0_iNS0_16ClassBoilerplate9ValueKindENS0_6ObjectE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"*dict == *dictionary"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal12_GLOBAL__N_123AddToDictionaryTemplateINS0_14NameDictionaryENS0_6HandleINS0_4NameEEEEEvPNS0_7IsolateENS4_IT_EET0_iNS0_16ClassBoilerplate9ValueKindENS0_6ObjectE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_123AddToDictionaryTemplateINS0_14NameDictionaryENS0_6HandleINS0_4NameEEEEEvPNS0_7IsolateENS4_IT_EET0_iNS0_16ClassBoilerplate9ValueKindENS0_6ObjectE, @function
_ZN2v88internal12_GLOBAL__N_123AddToDictionaryTemplateINS0_14NameDictionaryENS0_6HandleINS0_4NameEEEEEvPNS0_7IsolateENS4_IT_EET0_iNS0_16ClassBoilerplate9ValueKindENS0_6ObjectE:
.LFB21183:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	%r8d, %esi
	subq	$56, %rsp
	movq	0(%r13), %rcx
	movq	(%rbx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	7(%rcx), %eax
	testb	$1, %al
	jne	.L2
	shrl	$2, %eax
.L3:
	movslq	35(%rdx), %rcx
	movq	88(%r12), %r10
	subq	$1, %rdx
	movl	$1, %edi
	subl	$1, %ecx
	andl	%ecx, %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L166:
	cmpq	0(%r13), %r8
	je	.L5
	addl	%edi, %eax
	addl	$1, %edi
	andl	%ecx, %eax
.L6:
	leal	(%rax,%rax,2), %r9d
	leal	56(,%r9,8), %r8d
	movslq	%r8d, %r8
	movq	(%r8,%rdx), %r8
	cmpq	%r8, %r10
	jne	.L166
	movl	$-1, -64(%rbp)
.L72:
	addl	$6, %r14d
	sall	$8, %r14d
	testl	%esi, %esi
	je	.L167
	movq	%r12, %rdi
	movl	%esi, -72(%rbp)
	orb	$-47, %r14b
	call	_ZN2v88internal7Factory15NewAccessorPairEv@PLT
	movl	-72(%rbp), %esi
	movq	(%rax), %rdi
	movq	%rax, %rcx
	movq	%r15, %rax
	notq	%rax
	andl	$1, %eax
	cmpl	$1, %esi
	jne	.L168
	movq	%r15, 7(%rdi)
	leaq	7(%rdi), %rsi
	testb	%al, %al
	jne	.L10
.L157:
	movq	%r15, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -72(%rbp)
	testl	$262144, %eax
	je	.L16
	movq	%r15, %rdx
	movq	%rcx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %r8
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	8(%r8), %rax
.L16:
	testb	$24, %al
	je	.L10
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L10
	movq	%r15, %rdx
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L167:
	movq	41112(%r12), %rdi
	orb	$-48, %r14b
	testq	%rdi, %rdi
	je	.L169
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L10:
	movq	%rbx, %rsi
	leaq	-64(%rbp), %r9
	movl	%r14d, %r8d
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE31AddNoUpdateNextEnumerationIndexEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_4NameEEENS7_INS0_6ObjectEEENS0_15PropertyDetailsEPi@PLT
	movq	(%rbx), %rbx
	cmpq	%rbx, (%rax)
	jne	.L170
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L171
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movl	%eax, -64(%rbp)
	cmpl	$-1, %eax
	je	.L72
	movq	(%rbx), %rdx
	leal	72(,%r9,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	shrq	$40, %rax
	andl	$8388607, %eax
	movl	%eax, %r13d
	leal	64(,%r9,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rdi
	testl	%esi, %esi
	jne	.L20
	testb	$1, %dil
	jne	.L172
	sarq	$32, %rdi
	cmpl	%edi, %r14d
	jle	.L1
.L22:
	movl	-64(%rbp), %eax
	leal	9(%rax,%rax,2), %ecx
	movl	%r13d, %eax
	sall	$9, %eax
	sall	$3, %ecx
	sarl	%eax
	movslq	%ecx, %rcx
	orb	$-48, %al
	salq	$32, %rax
	movq	%rax, -1(%rcx,%rdx)
	movl	-64(%rbp), %eax
	movq	(%rbx), %r12
	leal	8(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	leaq	-1(%r12,%rax), %r13
	movq	%r15, 0(%r13)
	testb	$1, %r15b
	je	.L1
	movq	%r15, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L39
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L39:
	testb	$24, %al
	je	.L1
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	-64(%rbp), %rdi
	movq	%rdx, -72(%rbp)
	movl	%r8d, -80(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movl	-80(%rbp), %esi
	movq	-72(%rbp), %rdx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L20:
	testb	$1, %dil
	jne	.L173
.L41:
	movq	%r12, %rdi
	movl	%esi, -72(%rbp)
	call	_ZN2v88internal7Factory15NewAccessorPairEv@PLT
	movl	-72(%rbp), %esi
	movq	(%rax), %r14
	movq	%rax, %r12
	movq	%r15, %rax
	notq	%rax
	andl	$1, %eax
	cmpl	$1, %esi
	je	.L174
	movq	%r15, 15(%r14)
	leaq	15(%r14), %rsi
	testb	%al, %al
	jne	.L62
.L163:
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	je	.L60
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	8(%rcx), %rax
.L60:
	testb	$24, %al
	je	.L62
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L175
	.p2align 4,,10
	.p2align 3
.L62:
	movl	-64(%rbp), %eax
	sall	$9, %r13d
	sarl	%r13d
	leal	9(%rax,%rax,2), %edx
	orb	$-47, %r13b
	movq	(%rbx), %rax
	sall	$3, %edx
	salq	$32, %r13
	movslq	%edx, %rdx
	movq	%r13, -1(%rdx,%rax)
	movl	-64(%rbp), %eax
	movq	(%rbx), %r13
	movq	(%r12), %r12
	leal	8(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	leaq	-1(%r13,%rax), %r14
	movq	%r12, (%r14)
	testb	$1, %r12b
	je	.L1
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L63
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L63:
	testb	$24, %al
	je	.L1
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L169:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L176
.L11:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rcx)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L174:
	movq	%r15, 7(%r14)
	leaq	7(%r14), %rsi
	testb	%al, %al
	je	.L163
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L173:
	movq	-1(%rdi), %rax
	cmpw	$79, 11(%rax)
	jne	.L41
	cmpl	$1, %esi
	jne	.L42
	movq	7(%rdi), %rax
.L43:
	testb	$1, %al
	jne	.L76
	shrq	$32, %rax
.L44:
	cmpl	%eax, %r14d
	jle	.L1
	movq	%r15, %rax
	notq	%rax
	andl	$1, %eax
	cmpl	$1, %esi
	jne	.L45
	movq	%r15, 7(%rdi)
	leaq	7(%rdi), %r12
	testb	%al, %al
	jne	.L1
.L161:
	movq	%r15, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L50
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-72(%rbp), %rdi
.L50:
	testb	$24, %al
	je	.L1
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L172:
	movq	-1(%rdi), %rax
	cmpw	$79, 11(%rax)
	jne	.L22
	movq	7(%rdi), %rax
	leaq	7(%rdi), %rsi
	testb	$1, %al
	jne	.L74
	shrq	$32, %rax
.L23:
	movq	15(%rdi), %rcx
	leaq	15(%rdi), %r8
	testb	$1, %cl
	jne	.L75
	shrq	$32, %rcx
.L24:
	cmpl	%eax, %ecx
	movl	%eax, %r9d
	cmovge	%ecx, %r9d
	cmpl	%r9d, %r14d
	jg	.L22
	cmpl	%eax, %r14d
	jg	.L177
	cmpl	%ecx, %r14d
	jle	.L1
	movq	104(%r12), %r12
	movq	%r12, 15(%rdi)
	testb	$1, %r12b
	je	.L1
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L35
	movq	%r8, %rsi
	movq	%r12, %rdx
	movq	%r8, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rdi
.L35:
	testb	$24, %al
	je	.L1
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1
	movq	%r12, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%r15, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	%al, %al
	je	.L157
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L170:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L175:
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L176:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L42:
	movq	15(%rdi), %rax
	jmp	.L43
.L177:
	movq	104(%r12), %r12
	movq	%r12, 7(%rdi)
	testb	$1, %r12b
	je	.L1
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L31
	movq	%r12, %rdx
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
.L31:
	testb	$24, %al
	je	.L1
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1
.L45:
	movq	%r15, 15(%rdi)
	leaq	15(%rdi), %r12
	testb	%al, %al
	je	.L161
	jmp	.L1
.L75:
	orl	$-1, %ecx
	jmp	.L24
.L76:
	orl	$-1, %eax
	jmp	.L44
.L74:
	orl	$-1, %eax
	jmp	.L23
.L171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21183:
	.size	_ZN2v88internal12_GLOBAL__N_123AddToDictionaryTemplateINS0_14NameDictionaryENS0_6HandleINS0_4NameEEEEEvPNS0_7IsolateENS4_IT_EET0_iNS0_16ClassBoilerplate9ValueKindENS0_6ObjectE, .-_ZN2v88internal12_GLOBAL__N_123AddToDictionaryTemplateINS0_14NameDictionaryENS0_6HandleINS0_4NameEEEEEvPNS0_7IsolateENS4_IT_EET0_iNS0_16ClassBoilerplate9ValueKindENS0_6ObjectE
	.section	.text._ZN2v88internal12_GLOBAL__N_123AddToDictionaryTemplateINS0_16NumberDictionaryEjEEvPNS0_7IsolateENS0_6HandleIT_EET0_iNS0_16ClassBoilerplate9ValueKindENS0_6ObjectE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_123AddToDictionaryTemplateINS0_16NumberDictionaryEjEEvPNS0_7IsolateENS0_6HandleIT_EET0_iNS0_16ClassBoilerplate9ValueKindENS0_6ObjectE, @function
_ZN2v88internal12_GLOBAL__N_123AddToDictionaryTemplateINS0_16NumberDictionaryEjEEvPNS0_7IsolateENS0_6HandleIT_EET0_iNS0_16ClassBoilerplate9ValueKindENS0_6ObjectE:
.LFB21184:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movl	%ecx, -96(%rbp)
	movq	(%rsi), %r13
	movq	%r9, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	1176(%rdi), %rax
	movl	%edx, %edi
	movq	15(%rax), %rsi
	call	_Z11halfsiphashjm@PLT
	movslq	35(%r13), %rsi
	movq	88(%r14), %r11
	leaq	-1(%r13), %rcx
	movq	96(%r14), %r10
	movq	-88(%rbp), %r9
	movl	$1, %edi
	subl	$1, %esi
	andl	%esi, %eax
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L345:
	sarq	$32, %rdx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	cvttsd2siq	%xmm0, %rdx
	cmpl	%edx, %r15d
	je	.L183
.L180:
	addl	%edi, %eax
	addl	$1, %edi
	andl	%esi, %eax
.L184:
	leal	(%rax,%rax,2), %r8d
	leal	48(,%r8,8), %edx
	movslq	%edx, %rdx
	movq	(%rdx,%rcx), %rdx
	cmpq	%rdx, %r11
	je	.L179
	cmpq	%rdx, %r10
	je	.L180
	testb	$1, %dl
	je	.L345
	movsd	7(%rdx), %xmm0
	cvttsd2siq	%xmm0, %rdx
	cmpl	%edx, %r15d
	jne	.L180
.L183:
	movl	%eax, -68(%rbp)
	cmpl	$-1, %eax
	je	.L251
	movq	(%rbx), %rcx
	leal	64(,%r8,8), %edx
	leal	7(%rax,%rax,2), %eax
	movslq	%edx, %rdx
	sall	$3, %eax
	movq	-1(%rdx,%rcx), %rdx
	cltq
	movq	-1(%rax,%rcx), %rdi
	shrq	$40, %rdx
	andl	$8388607, %edx
	movl	%edx, %r15d
	testl	%r12d, %r12d
	jne	.L199
	testb	$1, %dil
	jne	.L346
	sarq	$32, %rdi
	cmpl	%edi, -96(%rbp)
	jle	.L178
.L201:
	movl	-68(%rbp), %eax
	movl	%r15d, %edx
	sall	$9, %edx
	leal	8(%rax,%rax,2), %eax
	sarl	%edx
	sall	$3, %eax
	orb	$-48, %dl
	cltq
	salq	$32, %rdx
	movq	%rdx, -1(%rax,%rcx)
.L344:
	movl	-68(%rbp), %eax
	movq	(%rbx), %r12
	leal	7(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	leaq	-1(%r12,%rax), %r13
	movq	%r9, 0(%r13)
	testb	$1, %r9b
	je	.L178
	movq	%r9, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L218
	movq	%r9, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-88(%rbp), %r9
.L218:
	testb	$24, %al
	je	.L178
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L178
	movq	%r9, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L179:
	movl	$-1, -68(%rbp)
.L251:
	testl	%r12d, %r12d
	je	.L347
	movq	%r14, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal7Factory15NewAccessorPairEv@PLT
	movq	-88(%rbp), %r9
	movq	(%rax), %r13
	movq	%rax, %rcx
	movq	%r9, %rax
	notq	%rax
	andl	$1, %eax
	cmpl	$1, %r12d
	jne	.L348
	movq	%r9, 7(%r13)
	leaq	7(%r13), %rsi
	testb	%al, %al
	jne	.L196
.L336:
	movq	%r9, %r12
	andq	$-262144, %r12
	movq	8(%r12), %rax
	testl	$262144, %eax
	je	.L194
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%rcx, -104(%rbp)
	movq	%r9, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r12), %rax
	movq	-104(%rbp), %rcx
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rsi
.L194:
	testb	$24, %al
	je	.L196
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L349
	.p2align 4,,10
	.p2align 3
.L196:
	movl	$209, %r8d
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L347:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L350
	movq	%r9, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	$208, %r8d
	movq	%rax, %rcx
.L188:
	leaq	-68(%rbp), %r9
	movl	%r15d, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal10DictionaryINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE3AddEPNS0_7IsolateENS0_6HandleIS2_EEjNS7_INS0_6ObjectEEENS0_15PropertyDetailsEPi@PLT
	movq	%rax, %r8
	movq	(%rbx), %rax
	cmpq	%rax, (%r8)
	jne	.L351
	xorl	%edx, %edx
	leaq	-64(%rbp), %rdi
	movl	%r15d, %esi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal16NumberDictionary18UpdateMaxNumberKeyEjNS0_6HandleINS0_8JSObjectEEE@PLT
	movq	(%rbx), %rax
	movabsq	$4294967296, %rdx
	movq	%rdx, 39(%rax)
.L178:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L352
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	testb	$1, %dil
	jne	.L353
.L220:
	movq	%r14, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal7Factory15NewAccessorPairEv@PLT
	movq	-88(%rbp), %r9
	movq	(%rax), %r14
	movq	%rax, %r13
	movq	%r9, %rax
	notq	%rax
	andl	$1, %eax
	cmpl	$1, %r12d
	jne	.L232
	movq	%r9, 7(%r14)
	leaq	7(%r14), %rsi
	testb	%al, %al
	jne	.L241
.L342:
	movq	%r9, %r12
	andq	$-262144, %r12
	movq	8(%r12), %rax
	testl	$262144, %eax
	je	.L239
	movq	%r9, %rdx
	movq	%r14, %rdi
	movq	%r9, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r12), %rax
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rsi
.L239:
	testb	$24, %al
	je	.L241
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L354
	.p2align 4,,10
	.p2align 3
.L241:
	movl	-68(%rbp), %eax
	movl	%r15d, %edx
	sall	$9, %edx
	leal	8(%rax,%rax,2), %ecx
	sarl	%edx
	movq	(%rbx), %rax
	sall	$3, %ecx
	orb	$-47, %dl
	movslq	%ecx, %rcx
	salq	$32, %rdx
	movq	%rdx, -1(%rcx,%rax)
	movl	-68(%rbp), %eax
	movq	(%rbx), %r14
	movq	0(%r13), %r12
	leal	7(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	leaq	-1(%r14,%rax), %r13
	movq	%r12, 0(%r13)
	testb	$1, %r12b
	je	.L178
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L242
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L242:
	testb	$24, %al
	je	.L178
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L178
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L350:
	movq	41088(%r14), %rcx
	cmpq	41096(%r14), %rcx
	je	.L355
.L189:
	leaq	8(%rcx), %rax
	movl	$208, %r8d
	movq	%rax, 41088(%r14)
	movq	%r9, (%rcx)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L232:
	movq	%r9, 15(%r14)
	leaq	15(%r14), %rsi
	testb	%al, %al
	je	.L342
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L353:
	movq	-1(%rdi), %rax
	cmpw	$79, 11(%rax)
	jne	.L220
	cmpl	$1, %r12d
	jne	.L221
	movq	7(%rdi), %rax
.L222:
	testb	$1, %al
	jne	.L255
	shrq	$32, %rax
.L223:
	cmpl	%eax, -96(%rbp)
	jle	.L178
	movq	%r9, %rax
	notq	%rax
	andl	$1, %eax
	cmpl	$1, %r12d
	jne	.L224
	movq	%r9, 7(%rdi)
	leaq	7(%rdi), %r12
	testb	%al, %al
	jne	.L178
.L340:
	movq	%r9, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L229
	movq	%r9, %rdx
	movq	%r12, %rsi
	movq	%r9, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdi
.L229:
	testb	$24, %al
	je	.L178
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L178
	movq	%r9, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L346:
	movq	-1(%rdi), %rax
	cmpw	$79, 11(%rax)
	jne	.L201
	movq	7(%rdi), %rax
	leaq	7(%rdi), %r12
	testb	$1, %al
	jne	.L253
	shrq	$32, %rax
.L202:
	movq	15(%rdi), %rdx
	leaq	15(%rdi), %rsi
	testb	$1, %dl
	jne	.L254
	shrq	$32, %rdx
.L203:
	cmpl	%eax, %edx
	movl	%eax, %r8d
	cmovge	%edx, %r8d
	cmpl	%r8d, -96(%rbp)
	jg	.L356
	cmpl	%eax, -96(%rbp)
	jg	.L357
	cmpl	%edx, -96(%rbp)
	jle	.L178
	movq	104(%r14), %r12
	movq	%r12, 15(%rdi)
	testb	$1, %r12b
	je	.L178
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L215
	movq	%r12, %rdx
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
.L215:
	testb	$24, %al
	je	.L178
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L178
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L348:
	movq	%r9, 15(%r13)
	leaq	15(%r13), %rsi
	testb	%al, %al
	je	.L336
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L351:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L354:
	movq	%r9, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L349:
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L355:
	movq	%r14, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L221:
	movq	15(%rdi), %rax
	jmp	.L222
.L356:
	movl	-68(%rbp), %eax
	leal	8(%rax,%rax,2), %edx
	movl	%r15d, %eax
	sall	$9, %eax
	sall	$3, %edx
	sarl	%eax
	movslq	%edx, %rdx
	orb	$-48, %al
	salq	$32, %rax
	movq	%rax, -1(%rdx,%rcx)
	jmp	.L344
.L357:
	movq	104(%r14), %r13
	movq	%r13, 7(%rdi)
	testb	$1, %r13b
	je	.L178
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L211
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-88(%rbp), %rdi
.L211:
	testb	$24, %al
	je	.L178
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L178
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L178
.L224:
	movq	%r9, 15(%rdi)
	leaq	15(%rdi), %r12
	testb	%al, %al
	je	.L340
	jmp	.L178
.L253:
	orl	$-1, %eax
	jmp	.L202
.L255:
	orl	$-1, %eax
	jmp	.L223
.L254:
	orl	$-1, %edx
	jmp	.L203
.L352:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21184:
	.size	_ZN2v88internal12_GLOBAL__N_123AddToDictionaryTemplateINS0_16NumberDictionaryEjEEvPNS0_7IsolateENS0_6HandleIT_EET0_iNS0_16ClassBoilerplate9ValueKindENS0_6ObjectE, .-_ZN2v88internal12_GLOBAL__N_123AddToDictionaryTemplateINS0_16NumberDictionaryEjEEvPNS0_7IsolateENS0_6HandleIT_EET0_iNS0_16ClassBoilerplate9ValueKindENS0_6ObjectE
	.section	.rodata._ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE,"axG",@progbits,_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	.type	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE, @function
_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE:
.LFB13048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movswq	9(%rax), %rdx
	movl	%edx, -76(%rbp)
	movq	%rdx, %r12
	addl	$1, %edx
	movw	%dx, 9(%rax)
	movq	(%rsi), %rax
	movq	(%rax), %rdx
	movq	16(%rsi), %rax
	movl	8(%rsi), %esi
	testl	%esi, %esi
	jne	.L359
	testq	%rax, %rax
	je	.L362
	movq	(%rax), %rax
	orq	$2, %rax
	movq	%rax, %r13
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L359:
	testq	%rax, %rax
	je	.L362
	movq	(%rax), %r13
.L361:
	movswl	%r12w, %eax
	movq	(%rcx), %rsi
	movl	24(%rbx), %r14d
	leal	3(%rax,%rax,2), %r8d
	sall	$3, %r8d
	movslq	%r8d, %r15
	movq	%rdx, -1(%r15,%rsi)
	movq	(%rcx), %rdi
	testb	$1, %dl
	je	.L379
	movq	%rdx, %r9
	leaq	-1(%r15,%rdi), %r10
	andq	$-262144, %r9
	movq	8(%r9), %rsi
	movq	%r9, -72(%rbp)
	testl	$262144, %esi
	je	.L364
	movq	%r10, %rsi
	movl	%r8d, -80(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rcx
	movq	-72(%rbp), %r9
	movl	-80(%rbp), %r8d
	movq	-88(%rbp), %rdx
	movq	(%rcx), %rdi
	movq	8(%r9), %rsi
	leaq	-1(%r15,%rdi), %r10
.L364:
	andl	$24, %esi
	je	.L379
	movq	%rdi, %rsi
	andq	$-262144, %rsi
	testb	$24, 8(%rsi)
	je	.L397
	.p2align 4,,10
	.p2align 3
.L379:
	addl	%r14d, %r14d
	sarl	%r14d
	salq	$32, %r14
	movq	%r14, 7(%r15,%rdi)
	movq	(%rcx), %rdx
	movq	%r13, 15(%r15,%rdx)
	testb	$1, %r13b
	je	.L378
	cmpl	$3, %r13d
	je	.L378
	movq	%r13, %r14
	movq	(%rcx), %rdi
	addl	$16, %r8d
	movq	%r13, %rdx
	andq	$-262144, %r14
	movslq	%r8d, %r8
	andq	$-3, %rdx
	movq	8(%r14), %rax
	movq	%r8, -72(%rbp)
	leaq	-1(%rdi,%r8), %rsi
	testl	$262144, %eax
	jne	.L398
	testb	$24, %al
	je	.L378
.L403:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L378
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L378:
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movl	7(%rax), %ebx
	testb	$1, %bl
	jne	.L369
	shrl	$2, %ebx
.L370:
	movswl	%r12w, %eax
	movq	(%rcx), %rsi
	testl	%eax, %eax
	jle	.L371
	leal	(%rax,%rax,2), %edx
	subl	$1, %eax
	leaq	-64(%rbp), %r14
	subq	%rax, %r12
	sall	$3, %edx
	leaq	(%r12,%r12,2), %r12
	movslq	%edx, %r13
	salq	$3, %r12
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L400:
	shrl	$2, %eax
	leaq	24(%r13), %r15
	leaq	7(%rsi), %rdi
	cmpl	%eax, %ebx
	jnb	.L373
.L401:
	movq	7(%r13,%rsi), %rdi
	movq	7(%r15,%rsi), %rax
	sarq	$32, %rdi
	sarq	$32, %rax
	andl	$-523777, %eax
	andl	$523776, %edi
	orl	%edi, %eax
	addl	%eax, %eax
	sarl	%eax
	salq	$32, %rax
	movq	%rax, 7(%r15,%rsi)
	movq	(%rcx), %rsi
	cmpq	%r12, %r13
	je	.L399
	subq	$24, %r13
.L372:
	movq	7(%r13,%rsi), %rax
	shrq	$41, %rax
	andl	$1023, %eax
	leal	3(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rax,%rsi), %rdi
	movl	7(%rdi), %eax
	testb	$1, %al
	je	.L400
	movq	%rdi, -64(%rbp)
	movq	%r14, %rdi
	leaq	24(%r13), %r15
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-72(%rbp), %rcx
	movq	(%rcx), %rsi
	leaq	7(%rsi), %rdi
	cmpl	%eax, %ebx
	jb	.L401
.L373:
	movq	(%r15,%rdi), %rax
	movl	-76(%rbp), %r13d
	sarq	$32, %rax
	sall	$9, %r13d
	andl	$-523777, %eax
	orl	%r13d, %eax
	addl	%eax, %eax
	sarl	%eax
	salq	$32, %rax
	movq	%rax, 7(%r15,%rsi)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L402
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	.cfi_restore_state
	leaq	-64(%rbp), %rdi
	movq	%rcx, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-72(%rbp), %rcx
	movl	%eax, %ebx
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L399:
	leaq	7(%rsi), %rdi
	movq	%r13, %r15
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L398:
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	8(%r14), %rax
	movq	-88(%rbp), %rdx
	movq	(%rcx), %rdi
	leaq	-1(%rdi,%r8), %rsi
	testb	$24, %al
	jne	.L403
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L397:
	movq	%r10, %rsi
	movl	%r8d, -88(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movl	-88(%rbp), %r8d
	movq	(%rcx), %rdi
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L362:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L371:
	leaq	7(%rsi), %rdi
	jmp	.L373
.L402:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13048:
	.size	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE, .-_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	.section	.text._ZN2v88internal16ClassBoilerplate23AddToPropertiesTemplateEPNS0_7IsolateENS0_6HandleINS0_14NameDictionaryEEENS4_INS0_4NameEEEiNS1_9ValueKindENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ClassBoilerplate23AddToPropertiesTemplateEPNS0_7IsolateENS0_6HandleINS0_14NameDictionaryEEENS4_INS0_4NameEEEiNS1_9ValueKindENS0_6ObjectE
	.type	_ZN2v88internal16ClassBoilerplate23AddToPropertiesTemplateEPNS0_7IsolateENS0_6HandleINS0_14NameDictionaryEEENS4_INS0_4NameEEEiNS1_9ValueKindENS0_6ObjectE, @function
_ZN2v88internal16ClassBoilerplate23AddToPropertiesTemplateEPNS0_7IsolateENS0_6HandleINS0_14NameDictionaryEEENS4_INS0_4NameEEEiNS1_9ValueKindENS0_6ObjectE:
.LFB19143:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal12_GLOBAL__N_123AddToDictionaryTemplateINS0_14NameDictionaryENS0_6HandleINS0_4NameEEEEEvPNS0_7IsolateENS4_IT_EET0_iNS0_16ClassBoilerplate9ValueKindENS0_6ObjectE
	.cfi_endproc
.LFE19143:
	.size	_ZN2v88internal16ClassBoilerplate23AddToPropertiesTemplateEPNS0_7IsolateENS0_6HandleINS0_14NameDictionaryEEENS4_INS0_4NameEEEiNS1_9ValueKindENS0_6ObjectE, .-_ZN2v88internal16ClassBoilerplate23AddToPropertiesTemplateEPNS0_7IsolateENS0_6HandleINS0_14NameDictionaryEEENS4_INS0_4NameEEEiNS1_9ValueKindENS0_6ObjectE
	.section	.text._ZN2v88internal16ClassBoilerplate21AddToElementsTemplateEPNS0_7IsolateENS0_6HandleINS0_16NumberDictionaryEEEjiNS1_9ValueKindENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ClassBoilerplate21AddToElementsTemplateEPNS0_7IsolateENS0_6HandleINS0_16NumberDictionaryEEEjiNS1_9ValueKindENS0_6ObjectE
	.type	_ZN2v88internal16ClassBoilerplate21AddToElementsTemplateEPNS0_7IsolateENS0_6HandleINS0_16NumberDictionaryEEEjiNS1_9ValueKindENS0_6ObjectE, @function
_ZN2v88internal16ClassBoilerplate21AddToElementsTemplateEPNS0_7IsolateENS0_6HandleINS0_16NumberDictionaryEEEjiNS1_9ValueKindENS0_6ObjectE:
.LFB19144:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal12_GLOBAL__N_123AddToDictionaryTemplateINS0_16NumberDictionaryEjEEvPNS0_7IsolateENS0_6HandleIT_EET0_iNS0_16ClassBoilerplate9ValueKindENS0_6ObjectE
	.cfi_endproc
.LFE19144:
	.size	_ZN2v88internal16ClassBoilerplate21AddToElementsTemplateEPNS0_7IsolateENS0_6HandleINS0_16NumberDictionaryEEEjiNS1_9ValueKindENS0_6ObjectE, .-_ZN2v88internal16ClassBoilerplate21AddToElementsTemplateEPNS0_7IsolateENS0_6HandleINS0_16NumberDictionaryEEEjiNS1_9ValueKindENS0_6ObjectE
	.section	.text._ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi,"axG",@progbits,_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi,comdat
	.p2align 4
	.weak	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi
	.type	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi, @function
_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi:
.LFB21662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rbx
	movswl	9(%rbx), %r10d
	movl	7(%rsi), %r9d
	subl	$1, %r10d
	je	.L417
	movl	%r10d, %r12d
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L408:
	movl	%r12d, %ecx
	subl	%r8d, %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	addl	%r8d, %eax
	leal	1(%rax), %r11d
	leal	(%r11,%r11,2), %ecx
	sall	$3, %ecx
	movslq	%ecx, %rcx
	movq	7(%rcx,%rbx), %rcx
	shrq	$41, %rcx
	andl	$1023, %ecx
	leal	3(%rcx,%rcx,2), %ecx
	sall	$3, %ecx
	movslq	%ecx, %rcx
	movq	-1(%rcx,%rbx), %rcx
	cmpl	7(%rcx), %r9d
	ja	.L422
	cmpl	%r8d, %eax
	je	.L407
	movl	%eax, %r12d
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L422:
	cmpl	%r12d, %r11d
	je	.L418
	movl	%r11d, %r8d
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L417:
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L407:
	leal	3(%r8,%r8,2), %r11d
	sall	$3, %r11d
	movslq	%r11d, %r11
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L413:
	movq	(%rdi), %rbx
	addl	$1, %r8d
	movq	7(%r11,%rbx), %rax
	shrq	$41, %rax
	andl	$1023, %eax
	leal	3(%rax,%rax,2), %ecx
	sall	$3, %ecx
	movslq	%ecx, %rcx
	movq	-1(%rcx,%rbx), %rcx
	cmpl	7(%rcx), %r9d
	jne	.L415
	addq	$24, %r11
	cmpq	%rcx, %rsi
	je	.L423
.L416:
	cmpl	%r10d, %r8d
	jle	.L413
.L415:
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	.cfi_restore_state
	movl	%r12d, %r8d
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L423:
	cmpl	%eax, %edx
	jle	.L415
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21662:
	.size	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi, .-_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi
	.section	.text._ZN2v88internal16ClassBoilerplate21BuildClassBoilerplateEPNS0_7IsolateEPNS0_12ClassLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ClassBoilerplate21BuildClassBoilerplateEPNS0_7IsolateEPNS0_12ClassLiteralE
	.type	_ZN2v88internal16ClassBoilerplate21BuildClassBoilerplateEPNS0_7IsolateEPNS0_12ClassLiteralE, @function
_ZN2v88internal16ClassBoilerplate21BuildClassBoilerplateEPNS0_7IsolateEPNS0_12ClassLiteralE:
.LFB19145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-192(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$344, %rsp
	movq	%rsi, -280(%rbp)
	movdqa	.LC3(%rip), %xmm0
	movdqa	.LC4(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rdi), %rax
	movaps	%xmm1, -128(%rbp)
	addl	$1, 41104(%rdi)
	movq	48(%rsi), %rdx
	movq	%rax, -320(%rbp)
	movq	41096(%rdi), %rax
	movl	12(%rdx), %edi
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -328(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -72(%rbp)
	movups	%xmm0, -168(%rbp)
	movups	%xmm0, -152(%rbp)
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	testl	%edi, %edi
	jg	.L425
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L794:
	addl	$1, 16(%r15)
.L429:
	addq	$1, %r13
	cmpl	%r13d, 12(%rdx)
	jle	.L793
.L425:
	movq	(%rdx), %rax
	movq	%r12, %r15
	movq	(%rax,%r13,8), %rax
	cmpb	$0, 17(%rax)
	jne	.L427
	leaq	-128(%rbp), %r15
.L427:
	movq	(%rax), %rdi
	movq	%rdi, %rcx
	andl	$3, %ecx
	jne	.L794
	andq	$-4, %rdi
	movzbl	4(%rdi), %eax
	andl	$63, %eax
	cmpb	$41, %al
	cmovne	%rcx, %rdi
	call	_ZNK2v88internal7Literal14IsPropertyNameEv@PLT
	testb	%al, %al
	je	.L431
	movq	48(%r14), %rdx
	addl	$1, 4(%r15)
	addq	$1, %r13
	cmpl	%r13d, 12(%rdx)
	jg	.L425
.L793:
	movl	-176(%rbp), %r12d
	leaq	296(%rbx), %rdx
	leaq	1056(%rbx), %rax
	movl	-180(%rbp), %r13d
	movq	%rdx, %xmm2
	movq	%rax, %xmm4
	movl	-188(%rbp), %ecx
	punpcklqdq	%xmm4, %xmm2
	movl	%r12d, %r14d
	orl	%r13d, %r14d
	movaps	%xmm2, -304(%rbp)
	movups	%xmm2, -168(%rbp)
	testl	%r12d, %r12d
	jle	.L795
.L432:
	leal	6(%rcx,%r12), %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	%rax, -160(%rbp)
.L433:
	testl	%r14d, %r14d
	jne	.L796
	leaq	1016(%rbx), %rax
	movq	%rax, -152(%rbp)
.L436:
	leaq	288(%rbx), %rax
	movq	%rax, -144(%rbp)
.L437:
	movq	41112(%rbx), %rdi
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rsi
	testq	%rdi, %rdi
	je	.L438
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L439:
	movq	%rax, -136(%rbp)
	movq	4432(%rbx), %rax
	leaq	4432(%rbx), %r11
	leaq	2768(%rbx), %r10
	testb	$1, %al
	jne	.L797
.L441:
	testl	%r12d, %r12d
	jg	.L656
	movl	-192(%rbp), %eax
	addl	-188(%rbp), %eax
	cmpl	$1020, %eax
	jg	.L656
	leaq	-224(%rbp), %r12
	movl	$3, %ecx
	movq	%r11, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Descriptor12DataConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
.L447:
	movq	-168(%rbp), %rax
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	movq	(%rax), %rax
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L431:
	addl	$1, 12(%r15)
	movq	48(%r14), %rdx
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L656:
	xorl	%r8d, %r8d
.L623:
	movl	-184(%rbp), %eax
	xorl	%r9d, %r9d
	movq	%r11, %rcx
	movq	%rbx, %rdi
	movq	-160(%rbp), %rsi
	leal	1(%rax), %edx
	sall	$8, %eax
	orl	%eax, %r8d
	movl	%edx, -184(%rbp)
	movq	%r10, %rdx
	orb	$-40, %r8b
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE31AddNoUpdateNextEnumerationIndexEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_4NameEEENS7_INS0_6ObjectEEENS0_15PropertyDetailsEPi@PLT
	movq	%rax, -160(%rbp)
.L446:
	movq	4440(%rbx), %rax
	leaq	4440(%rbx), %r11
	leaq	3104(%rbx), %r10
	testb	$1, %al
	jne	.L798
.L448:
	movl	-176(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L654
	movl	-192(%rbp), %eax
	addl	-188(%rbp), %eax
	cmpl	$1020, %eax
	jg	.L654
	leaq	-224(%rbp), %r12
	movl	$7, %ecx
	movq	%r11, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Descriptor12DataConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
.L454:
	movq	-168(%rbp), %rax
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	movq	(%rax), %rax
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L654:
	xorl	%r8d, %r8d
.L620:
	movl	-184(%rbp), %eax
	xorl	%r9d, %r9d
	movq	%r11, %rcx
	movq	%rbx, %rdi
	movq	-160(%rbp), %rsi
	leal	1(%rax), %edx
	sall	$8, %eax
	orl	%eax, %r8d
	movl	%edx, -184(%rbp)
	movq	%r10, %rdx
	orb	$-8, %r8b
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE31AddNoUpdateNextEnumerationIndexEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_4NameEEENS7_INS0_6ObjectEEENS0_15PropertyDetailsEPi@PLT
	movq	%rax, -160(%rbp)
.L453:
	movq	-280(%rbp), %rax
	movq	40(%rax), %rdi
	call	_ZN2v88internal15FunctionLiteral15NeedsHomeObjectEPNS0_10ExpressionE@PLT
	testb	%al, %al
	je	.L455
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L456
	movabsq	$8589934592, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	leaq	3720(%rbx), %r10
	movq	%rax, %r11
	movq	(%rax), %rax
	testb	$1, %al
	je	.L457
	movq	-1(%rax), %rax
	cmpw	$78, 11(%rax)
	jne	.L457
	movl	-176(%rbp), %esi
	testl	%esi, %esi
	jle	.L799
.L653:
	movl	$1, %r8d
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L456:
	movq	41088(%rbx), %r11
	cmpq	%r11, 41096(%rbx)
	jne	.L459
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r11
	.p2align 4,,10
	.p2align 3
.L459:
	leaq	8(%r11), %rax
	leaq	3720(%rbx), %r10
	movq	%rax, 41088(%rbx)
	movabsq	$8589934592, %rax
	movq	%rax, (%r11)
.L457:
	movl	-176(%rbp), %edi
	testl	%edi, %edi
	jg	.L652
	movl	-192(%rbp), %eax
	addl	-188(%rbp), %eax
	cmpl	$1020, %eax
	jg	.L652
	leaq	-224(%rbp), %r12
	movl	$7, %ecx
	movq	%r11, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Descriptor12DataConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
.L464:
	movq	-168(%rbp), %rax
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	movq	(%rax), %rax
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	.p2align 4,,10
	.p2align 3
.L455:
	movq	-280(%rbp), %rax
	movq	%rbx, %rdi
	movl	8(%rax), %edx
	movl	(%rax), %esi
	call	_ZN2v88internal7Factory17NewClassPositionsEii@PLT
	leaq	3656(%rbx), %r10
	movq	%rax, %r11
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L800
.L465:
	movl	-176(%rbp), %r8d
	testl	%r8d, %r8d
	jg	.L651
	movl	-188(%rbp), %eax
	addl	-192(%rbp), %eax
	cmpl	$1020, %eax
	jg	.L651
	leaq	-224(%rbp), %r12
	movl	$2, %ecx
	movq	%r11, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Descriptor12DataConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
.L471:
	movq	-168(%rbp), %rax
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	movq	(%rax), %rax
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L651:
	xorl	%r8d, %r8d
.L614:
	movl	-184(%rbp), %edx
	xorl	%r9d, %r9d
	movq	%r11, %rcx
	movq	%rbx, %rdi
	movq	-160(%rbp), %rsi
	leal	1(%rdx), %eax
	sall	$8, %edx
	orl	%edx, %r8d
	movq	%r10, %rdx
	movl	%eax, -184(%rbp)
	orb	$-48, %r8b
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE31AddNoUpdateNextEnumerationIndexEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_4NameEEENS7_INS0_6ObjectEEENS0_15PropertyDetailsEPi@PLT
	movq	%rax, -160(%rbp)
.L470:
	movl	-124(%rbp), %eax
	movl	-112(%rbp), %r12d
	movdqa	-304(%rbp), %xmm3
	movl	-128(%rbp), %esi
	movl	%eax, %ecx
	movl	-116(%rbp), %r13d
	orl	%r12d, %ecx
	movups	%xmm3, -104(%rbp)
	jne	.L472
	testl	%esi, %esi
	je	.L801
.L473:
	leal	(%rax,%rsi), %edx
	cmpl	$1020, %edx
	jg	.L475
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal15DescriptorArray8AllocateEPNS0_7IsolateEiiNS0_14AllocationTypeE@PLT
	movq	%rax, -104(%rbp)
	movl	%r12d, %eax
	orl	%r13d, %eax
	jne	.L802
	.p2align 4,,10
	.p2align 3
.L477:
	leaq	1016(%rbx), %rax
	movq	%rax, -88(%rbp)
.L479:
	leaq	288(%rbx), %rax
	movq	%rax, -80(%rbp)
.L480:
	movq	41112(%rbx), %rdi
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rsi
	testq	%rdi, %rdi
	je	.L481
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, -72(%rbp)
	testq	%rdi, %rdi
	je	.L484
.L835:
	movabsq	$4294967296, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	leaq	2304(%rbx), %r10
	movq	%rax, %r11
	movq	(%rax), %rax
	testb	$1, %al
	je	.L485
	movq	-1(%rax), %rax
	cmpw	$78, 11(%rax)
	je	.L488
	movl	-112(%rbp), %r12d
.L485:
	testl	%r12d, %r12d
	jg	.L648
.L772:
	movl	-128(%rbp), %eax
	addl	-124(%rbp), %eax
	cmpl	$1020, %eax
	jg	.L648
	leaq	-224(%rbp), %r12
	movl	$2, %ecx
	movq	%r11, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Descriptor12DataConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
.L493:
	movq	-104(%rbp), %rax
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	movq	(%rax), %rax
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L472:
	testl	%r12d, %r12d
	jle	.L473
.L475:
	addl	%r12d, %eax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	addl	%eax, %esi
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	%rax, -96(%rbp)
	movl	%r12d, %eax
	orl	%r13d, %eax
	je	.L477
.L802:
	leal	(%r12,%r13), %esi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal9HashTableINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	%rax, -88(%rbp)
	testl	%r12d, %r12d
	je	.L479
	leal	(%r12,%r12), %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, -80(%rbp)
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L648:
	xorl	%r8d, %r8d
.L611:
	movl	-120(%rbp), %edx
	movq	-96(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	%r11, %rcx
	movq	%rbx, %rdi
	leal	1(%rdx), %eax
	sall	$8, %edx
	orl	%edx, %r8d
	movq	%r10, %rdx
	movl	%eax, -120(%rbp)
	orb	$-48, %r8b
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE31AddNoUpdateNextEnumerationIndexEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_4NameEEENS7_INS0_6ObjectEEENS0_15PropertyDetailsEPi@PLT
	movq	%rax, -96(%rbp)
.L492:
	movq	-280(%rbp), %rax
	movq	48(%rax), %rsi
	movl	12(%rsi), %edi
	testl	%edi, %edi
	jle	.L636
	xorl	%r12d, %r12d
	leaq	-192(%rbp), %rax
	movl	-284(%rbp), %r14d
	movl	$3, %r13d
	movq	%rax, -312(%rbp)
	movq	%r12, %r15
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L805:
	xorl	%r14d, %r14d
	testb	%r8b, %r8b
	setne	%r14b
.L496:
	cmpb	$0, 17(%rcx)
	movq	-312(%rbp), %r12
	jne	.L498
	leaq	-128(%rbp), %r12
.L498:
	testq	%rdx, %rdx
	jne	.L803
	andq	$-4, %rax
	leal	1(%r13), %esi
	movzbl	4(%rax), %ecx
	movl	%esi, -284(%rbp)
	leaq	-260(%rbp), %rsi
	andl	$63, %ecx
	cmpb	$41, %cl
	cmove	%rax, %rdx
	movq	%rdx, %rdi
	movq	%rdx, -304(%rbp)
	call	_ZNK2v88internal7Literal12AsArrayIndexEPj@PLT
	movq	%r13, %r9
	movq	-304(%rbp), %rdx
	salq	$32, %r9
	testb	%al, %al
	je	.L501
	movq	40(%r12), %rsi
	movl	%r14d, %r8d
	movl	%r13d, %ecx
	movq	%rbx, %rdi
	movl	-260(%rbp), %edx
	call	_ZN2v88internal12_GLOBAL__N_123AddToDictionaryTemplateINS0_16NumberDictionaryEjEEvPNS0_7IsolateENS0_6HandleIT_EET0_iNS0_16ClassBoilerplate9ValueKindENS0_6ObjectE
.L502:
	movq	-280(%rbp), %rax
	movl	-284(%rbp), %r13d
	movq	48(%rax), %rsi
	movl	12(%rsi), %edi
.L554:
	addq	$1, %r15
	cmpl	%r15d, %edi
	jle	.L804
.L555:
	movq	(%rsi), %rax
	movq	(%rax,%r15,8), %rcx
	cmpb	$0, 18(%rcx)
	jne	.L554
	movq	(%rcx), %rax
	movzbl	16(%rcx), %r8d
	movq	%rax, %rdx
	andl	$3, %edx
	cmpb	$2, %r8b
	je	.L637
	jbe	.L805
	cmpb	$3, %r8b
	jne	.L496
	cmpq	$1, %rdx
	sbbl	$-1, %r13d
	addq	$1, %r15
	cmpl	%r15d, %edi
	jg	.L555
	.p2align 4,,10
	.p2align 3
.L804:
	addl	%r13d, %r13d
.L494:
	movq	-280(%rbp), %rax
	movl	-176(%rbp), %r14d
	movl	4(%rax), %eax
	andl	$128, %eax
	jne	.L556
	movl	$1, %r12d
	testl	%r14d, %r14d
	jg	.L557
	movl	-188(%rbp), %edx
	movl	-192(%rbp), %eax
	leal	(%rdx,%rax), %ecx
	cmpl	$1020, %ecx
	jle	.L806
	.p2align 4,,10
	.p2align 3
.L557:
	movq	-160(%rbp), %rax
	movq	%rbx, %rdi
	movq	(%rax), %rdx
	movslq	-184(%rbp), %rax
	salq	$32, %rax
	movq	%rax, 39(%rdx)
	movl	-172(%rbp), %edx
	movq	-144(%rbp), %rsi
	call	_ZN2v88internal10FixedArray13ShrinkOrEmptyEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	%rax, -144(%rbp)
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	jle	.L807
.L568:
	movq	-96(%rbp), %rax
	movq	%rbx, %rdi
	movq	(%rax), %rdx
	movslq	-120(%rbp), %rax
	salq	$32, %rax
	movq	%rax, 39(%rdx)
	movl	-108(%rbp), %edx
	movq	-80(%rbp), %rsi
	call	_ZN2v88internal10FixedArray13ShrinkOrEmptyEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	%rax, -80(%rbp)
.L569:
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	$0, 15(%rax)
	movq	(%r14), %rdx
	movslq	19(%rdx), %rax
	andl	$-2, %eax
	orl	%eax, %r12d
	salq	$32, %r12
	movq	%r12, 15(%rdx)
	movq	(%r14), %rdx
	movslq	19(%rdx), %rax
	andl	$-2147483647, %eax
	orl	%eax, %r13d
	salq	$32, %r13
	movq	%r13, 15(%rdx)
	movl	-176(%rbp), %eax
	movq	(%r14), %r13
	testl	%eax, %eax
	jle	.L808
.L570:
	movq	-160(%rbp), %rax
.L571:
	movq	(%rax), %r12
	leaq	23(%r13), %rsi
	movq	%r12, 23(%r13)
	testb	$1, %r12b
	je	.L609
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L809
	testb	$24, %al
	je	.L609
.L828:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L810
	.p2align 4,,10
	.p2align 3
.L609:
	movq	-152(%rbp), %rax
	movq	(%r14), %r13
	movq	(%rax), %r12
	leaq	31(%r13), %rsi
	movq	%r12, 31(%r13)
	testb	$1, %r12b
	je	.L608
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L811
	testb	$24, %al
	je	.L608
.L829:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L812
	.p2align 4,,10
	.p2align 3
.L608:
	movq	-144(%rbp), %rax
	movq	(%r14), %r13
	movq	(%rax), %r12
	leaq	39(%r13), %rsi
	movq	%r12, 39(%r13)
	testb	$1, %r12b
	je	.L607
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L813
	testb	$24, %al
	je	.L607
.L831:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L814
	.p2align 4,,10
	.p2align 3
.L607:
	movl	-112(%rbp), %r15d
	movq	(%r14), %r13
	testl	%r15d, %r15d
	jle	.L815
.L581:
	movq	-96(%rbp), %rax
.L582:
	movq	(%rax), %r12
	leaq	47(%r13), %rsi
	movq	%r12, 47(%r13)
	testb	$1, %r12b
	je	.L606
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L816
	testb	$24, %al
	je	.L606
.L827:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L817
	.p2align 4,,10
	.p2align 3
.L606:
	movq	-88(%rbp), %rax
	movq	(%r14), %r13
	movq	(%rax), %r12
	leaq	55(%r13), %rsi
	movq	%r12, 55(%r13)
	testb	$1, %r12b
	je	.L605
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L818
	testb	$24, %al
	je	.L605
.L830:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L819
	.p2align 4,,10
	.p2align 3
.L605:
	movq	-80(%rbp), %rax
	movq	(%r14), %r13
	movq	(%rax), %r12
	leaq	63(%r13), %rsi
	movq	%r12, 63(%r13)
	testb	$1, %r12b
	je	.L604
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L820
	testb	$24, %al
	je	.L604
.L832:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L821
	.p2align 4,,10
	.p2align 3
.L604:
	movq	-320(%rbp), %rax
	movq	(%r14), %r12
	subl	$1, 41104(%rbx)
	movq	%rax, 41088(%rbx)
	movq	-328(%rbp), %rax
	cmpq	41096(%rbx), %rax
	je	.L592
	movq	%rax, 41096(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L592:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L593
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L594:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L822
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	.cfi_restore_state
	movq	8(%rdx), %rax
	movl	16(%r12), %esi
	movq	(%rax), %r11
	testl	%esi, %esi
	jle	.L823
.L503:
	leal	6(%r13), %eax
	movq	32(%r12), %rsi
	movl	%r14d, %r8d
	movl	%r13d, %ecx
	movl	%eax, 8(%r12)
	movq	%r11, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_123AddToDictionaryTemplateINS0_14NameDictionaryENS0_6HandleINS0_4NameEEEEEvPNS0_7IsolateENS4_IT_EET0_iNS0_16ClassBoilerplate9ValueKindENS0_6ObjectE
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L637:
	movl	$2, %r14d
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L806:
	movq	4424(%rbx), %rcx
	leaq	4424(%rbx), %r11
	leaq	2872(%rbx), %r10
	testb	$1, %cl
	jne	.L824
.L561:
	addl	%edx, %eax
	cmpl	$1020, %eax
	jle	.L564
	movl	-184(%rbp), %r8d
	xorl	%r9d, %r9d
	movq	%r11, %rcx
	movq	%r10, %rdx
	movq	-160(%rbp), %rsi
	movq	%rbx, %rdi
	leal	1(%r8), %eax
	sall	$8, %r8d
	orb	$-40, %r8b
	movl	%eax, -184(%rbp)
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE31AddNoUpdateNextEnumerationIndexEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_4NameEEENS7_INS0_6ObjectEEENS0_15PropertyDetailsEPi@PLT
	movq	%rax, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L556:
	xorl	%r12d, %r12d
	testl	%r14d, %r14d
	jg	.L557
.L566:
	movl	-192(%rbp), %eax
	addl	-188(%rbp), %eax
	xorl	%r12d, %r12d
	cmpl	$1020, %eax
	jg	.L557
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	jg	.L568
.L807:
	movl	-128(%rbp), %eax
	addl	-124(%rbp), %eax
	cmpl	$1020, %eax
	jle	.L569
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L803:
	leal	7(%r13), %eax
	movl	%eax, 8(%r12)
	movq	48(%r12), %rax
	movq	(%rax), %rcx
	movl	20(%r12), %eax
	leal	1(%rax), %edx
	movl	%edx, 20(%r12)
	leal	16(,%rax,8), %edx
	leal	0(,%r13,4), %eax
	addl	$2, %r13d
	orl	%r14d, %eax
	movslq	%edx, %rdx
	salq	$32, %rax
	movq	%rax, -1(%rcx,%rdx)
	movq	-280(%rbp), %rax
	movq	48(%rax), %rsi
	movl	12(%rsi), %edi
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L823:
	movl	(%r12), %eax
	addl	4(%r12), %eax
	cmpl	$1020, %eax
	jg	.L503
	movq	56(%r12), %r10
	movq	24(%r12), %r12
	movq	%r9, (%r10)
	movq	(%r12), %rax
	movq	%rax, -224(%rbp)
	movswl	9(%rax), %r8d
	movq	(%r11), %rsi
	testl	%r8d, %r8d
	je	.L505
	cmpl	$8, %r8d
	jle	.L825
	leaq	-224(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	%r8d, %edx
	movq	%r10, -336(%rbp)
	movq	%r11, -304(%rbp)
	call	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi
	movq	-336(%rbp), %r10
	movq	-304(%rbp), %r11
.L509:
	cmpl	$-1, %eax
	je	.L505
	leal	3(%rax,%rax,2), %eax
	sall	$3, %eax
	movl	%eax, -304(%rbp)
	movslq	%eax, %r8
	movq	(%r12), %rax
	movq	7(%r8,%rax), %r13
	shrq	$41, %r13
	andl	$1023, %r13d
	testl	%r14d, %r14d
	jne	.L522
	movl	$2, %ecx
	movq	%r10, %rdx
	movq	%r11, %rsi
	sall	$9, %r13d
	leaq	-256(%rbp), %rdi
	movq	%r8, -336(%rbp)
	call	_ZN2v88internal10Descriptor12DataConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movl	-232(%rbp), %eax
	movq	(%r12), %rdi
	movl	-248(%rbp), %ecx
	movq	-336(%rbp), %r8
	andl	$-523777, %eax
	orl	%eax, %r13d
	movq	-256(%rbp), %rax
	testl	%ecx, %ecx
	movq	(%rax), %rdx
	movq	-240(%rbp), %rax
	jne	.L523
	testq	%rax, %rax
	je	.L526
	movq	(%rax), %r12
	orq	$2, %r12
.L525:
	leaq	-1(%rdi), %rax
	addq	%rax, %r8
	movq	%rdx, (%r8)
	testb	$1, %dl
	je	.L599
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rsi
	movq	%rcx, -336(%rbp)
	testl	$262144, %esi
	jne	.L826
.L528:
	andl	$24, %esi
	je	.L599
	movq	%rdi, %rcx
	andq	$-262144, %rcx
	testb	$24, 8(%rcx)
	jne	.L599
	movq	%r8, %rsi
	movq	%rax, -352(%rbp)
	movq	%r8, -344(%rbp)
	movq	%rdi, -336(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-352(%rbp), %rax
	movq	-344(%rbp), %r8
	movq	-336(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L599:
	addl	%r13d, %r13d
	sarl	%r13d
	salq	$32, %r13
	movq	%r13, 8(%r8)
	movq	%r12, 16(%r8)
	testb	$1, %r12b
	je	.L502
	cmpl	$3, %r12d
	je	.L502
	movl	-304(%rbp), %edx
	addl	$16, %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rax), %r13
	movq	%r12, %rdx
	andq	$-262144, %r12
	movq	8(%r12), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	je	.L531
	movq	%r13, %rsi
	movq	%rdx, -336(%rbp)
	movq	%rdi, -304(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r12), %rax
	movq	-336(%rbp), %rdx
	movq	-304(%rbp), %rdi
.L531:
	testb	$24, %al
	je	.L502
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L502
	jmp	.L788
	.p2align 4,,10
	.p2align 3
.L796:
	leal	0(%r13,%r12), %esi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal9HashTableINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	%rax, -152(%rbp)
	testl	%r12d, %r12d
	je	.L436
	leal	(%r12,%r12), %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, -144(%rbp)
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L816:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-280(%rbp), %rsi
	testb	$24, %al
	jne	.L827
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L809:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-280(%rbp), %rsi
	testb	$24, %al
	jne	.L828
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L811:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-280(%rbp), %rsi
	testb	$24, %al
	jne	.L829
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L818:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-280(%rbp), %rsi
	testb	$24, %al
	jne	.L830
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L813:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-280(%rbp), %rsi
	testb	$24, %al
	jne	.L831
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L820:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-280(%rbp), %rsi
	testb	$24, %al
	jne	.L832
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L652:
	xorl	%r8d, %r8d
.L617:
	movl	-184(%rbp), %edx
	xorl	%r9d, %r9d
	movq	%r11, %rcx
	movq	%rbx, %rdi
	movq	-160(%rbp), %rsi
	leal	1(%rdx), %eax
	sall	$8, %edx
	orl	%edx, %r8d
	movq	%r10, %rdx
	movl	%eax, -184(%rbp)
	orb	$-8, %r8b
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE31AddNoUpdateNextEnumerationIndexEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_4NameEEENS7_INS0_6ObjectEEENS0_15PropertyDetailsEPi@PLT
	movq	%rax, -160(%rbp)
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L438:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L833
.L440:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L481:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L834
.L483:
	leaq	8(%rax), %rdx
	movq	%rax, -72(%rbp)
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L835
.L484:
	movq	41088(%rbx), %r11
	cmpq	%r11, 41096(%rbx)
	je	.L836
.L487:
	leaq	8(%r11), %rax
	leaq	2304(%rbx), %r10
	movq	%rax, 41088(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, (%r11)
	testl	%r12d, %r12d
	jle	.L772
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L593:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L837
.L595:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r12, (%rax)
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L815:
	movl	-128(%rbp), %eax
	addl	-124(%rbp), %eax
	cmpl	$1020, %eax
	jg	.L581
	movq	-104(%rbp), %rax
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L808:
	movl	-192(%rbp), %eax
	addl	-188(%rbp), %eax
	cmpl	$1020, %eax
	jg	.L570
	movq	-168(%rbp), %rax
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L797:
	movq	-1(%rax), %rax
	cmpw	$78, 11(%rax)
	je	.L442
	movl	-176(%rbp), %r12d
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L800:
	movq	-1(%rax), %rax
	cmpw	$78, 11(%rax)
	jne	.L465
	movl	-176(%rbp), %r9d
	testl	%r9d, %r9d
	jle	.L838
.L650:
	movl	$1, %r8d
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L798:
	movq	-1(%rax), %rax
	cmpw	$78, 11(%rax)
	jne	.L448
	movl	-176(%rbp), %edx
	testl	%edx, %edx
	jle	.L839
.L655:
	movl	$1, %r8d
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L795:
	leal	6(%rcx), %edx
	cmpl	$1020, %edx
	jg	.L432
.L426:
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal15DescriptorArray8AllocateEPNS0_7IsolateEiiNS0_14AllocationTypeE@PLT
	movq	%rax, -168(%rbp)
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L825:
	movl	$24, %edx
	xorl	%eax, %eax
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L840:
	movq	-224(%rbp), %rcx
	leal	1(%rax), %edi
	addq	$24, %rdx
	movq	-25(%rdx,%rcx), %rcx
	cmpq	%rcx, %rsi
	je	.L509
	movl	%edi, %eax
.L508:
	cmpl	%eax, %r8d
	jg	.L840
.L505:
	leaq	-256(%rbp), %r13
	movq	%r10, -336(%rbp)
	movq	%r13, %rdi
	movq	%r11, -304(%rbp)
	call	_ZN2v88internal10DescriptorC1Ev@PLT
	testl	%r14d, %r14d
	movq	-304(%rbp), %r11
	movq	-336(%rbp), %r10
	jne	.L511
	leaq	-224(%rbp), %rdi
	movl	$2, %ecx
	movq	%r10, %rdx
	movq	%r11, %rsi
	movq	%rdi, -304(%rbp)
	call	_ZN2v88internal10Descriptor12DataConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
.L785:
	movq	-208(%rbp), %rax
	movdqa	-224(%rbp), %xmm7
	movq	%r13, %rsi
	movq	-304(%rbp), %rdi
	movq	%rax, -240(%rbp)
	movl	-200(%rbp), %eax
	movaps	%xmm7, -256(%rbp)
	movl	%eax, -232(%rbp)
	movq	(%r12), %rax
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L821:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L812:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L814:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L817:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L819:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L810:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L801:
	testl	%r13d, %r13d
	je	.L477
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9HashTableINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	%rax, -88(%rbp)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L522:
	movq	15(%r8,%rax), %rdi
	testb	$1, %dil
	jne	.L533
.L537:
	movq	%rbx, %rdi
	movq	%r8, -360(%rbp)
	sall	$9, %r13d
	movq	%r10, -352(%rbp)
	movq	%r11, -344(%rbp)
	call	_ZN2v88internal7Factory15NewAccessorPairEv@PLT
	movq	-344(%rbp), %r11
	movl	$2, %ecx
	leaq	-224(%rbp), %rdi
	movq	%rax, %rdx
	movq	%rax, -336(%rbp)
	movq	%r11, %rsi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movl	-200(%rbp), %r11d
	movl	-216(%rbp), %edx
	movq	-224(%rbp), %rax
	movq	(%r12), %rdi
	andl	$-523777, %r11d
	movq	-336(%rbp), %r9
	movq	-352(%rbp), %r10
	orl	%r13d, %r11d
	testl	%edx, %edx
	movq	(%rax), %r13
	movq	-360(%rbp), %r8
	movq	-208(%rbp), %rax
	jne	.L841
	testq	%rax, %rax
	je	.L526
	movq	(%rax), %r12
	orq	$2, %r12
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L442:
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	jle	.L842
.L657:
	movl	$1, %r8d
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L488:
	movl	-112(%rbp), %r12d
	testl	%r12d, %r12d
	jle	.L843
.L649:
	movl	$1, %r8d
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L636:
	movl	$6, %r13d
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L792:
	leaq	296(%rbx), %rdx
	leaq	1056(%rbx), %rax
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	movq	%rdx, %xmm5
	movq	%rax, %xmm6
	movl	$6, %edx
	punpcklqdq	%xmm6, %xmm5
	movaps	%xmm5, -304(%rbp)
	movups	%xmm5, -168(%rbp)
	jmp	.L426
.L833:
	movq	%rbx, %rdi
	movq	%rsi, -312(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %rsi
	jmp	.L440
.L836:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r11
	jmp	.L487
.L834:
	movq	%rbx, %rdi
	movq	%rsi, -304(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-304(%rbp), %rsi
	jmp	.L483
.L837:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L595
.L511:
	movq	%rbx, %rdi
	movq	%r10, -336(%rbp)
	movq	%r11, -304(%rbp)
	call	_ZN2v88internal7Factory15NewAccessorPairEv@PLT
	movq	-336(%rbp), %r10
	cmpl	$1, %r14d
	movq	-304(%rbp), %r11
	movq	(%rax), %rdi
	movq	%rax, %r8
	movq	(%r10), %rdx
	je	.L844
	movq	%rdx, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %dl
	je	.L517
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rcx
	movq	%rax, -304(%rbp)
	testl	$262144, %ecx
	je	.L519
	movq	%r8, -368(%rbp)
	movq	%r11, -360(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rsi, -344(%rbp)
	movq	%rdi, -336(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-304(%rbp), %rax
	movq	-368(%rbp), %r8
	movq	-360(%rbp), %r11
	movq	-352(%rbp), %rdx
	movq	8(%rax), %rcx
	movq	-344(%rbp), %rsi
	movq	-336(%rbp), %rdi
.L519:
	andl	$24, %ecx
	je	.L517
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L517
	movq	%r8, -336(%rbp)
	movq	%r11, -304(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-336(%rbp), %r8
	movq	-304(%rbp), %r11
.L517:
	leaq	-224(%rbp), %rdi
	movl	$2, %ecx
	movq	%r8, %rdx
	movq	%r11, %rsi
	movq	%rdi, -304(%rbp)
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	jmp	.L785
.L523:
	testq	%rax, %rax
	je	.L526
	movq	(%rax), %r12
	jmp	.L525
.L841:
	testq	%rax, %rax
	je	.L526
	movq	(%rax), %r12
.L538:
	leaq	-1(%rdi), %rax
	addq	%rax, %r8
	movq	%r13, (%r8)
	testb	$1, %r13b
	je	.L603
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -336(%rbp)
	testl	$262144, %edx
	je	.L540
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%rax, -384(%rbp)
	movl	%r11d, -376(%rbp)
	movq	%r9, -368(%rbp)
	movq	%r10, -360(%rbp)
	movq	%r8, -352(%rbp)
	movq	%rdi, -344(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-336(%rbp), %rcx
	movq	-384(%rbp), %rax
	movl	-376(%rbp), %r11d
	movq	-368(%rbp), %r9
	movq	8(%rcx), %rdx
	movq	-360(%rbp), %r10
	movq	-352(%rbp), %r8
	movq	-344(%rbp), %rdi
.L540:
	andl	$24, %edx
	je	.L603
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L603
	movq	%r8, %rsi
	movq	%r13, %rdx
	movq	%rax, -376(%rbp)
	movl	%r11d, -368(%rbp)
	movq	%r9, -360(%rbp)
	movq	%r10, -352(%rbp)
	movq	%r8, -344(%rbp)
	movq	%rdi, -336(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-376(%rbp), %rax
	movl	-368(%rbp), %r11d
	movq	-360(%rbp), %r9
	movq	-352(%rbp), %r10
	movq	-344(%rbp), %r8
	movq	-336(%rbp), %rdi
.L603:
	addl	%r11d, %r11d
	sarl	%r11d
	salq	$32, %r11
	movq	%r11, 8(%r8)
	movq	%r12, 16(%r8)
	testb	$1, %r12b
	je	.L602
	cmpl	$3, %r12d
	je	.L602
	movl	-304(%rbp), %r13d
	movq	%r12, %rdx
	andq	$-262144, %r12
	andq	$-3, %rdx
	addl	$16, %r13d
	movslq	%r13d, %r13
	addq	%rax, %r13
	movq	8(%r12), %rax
	testl	$262144, %eax
	je	.L543
	movq	%r13, %rsi
	movq	%r9, -352(%rbp)
	movq	%r10, -344(%rbp)
	movq	%rdx, -336(%rbp)
	movq	%rdi, -304(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r12), %rax
	movq	-352(%rbp), %r9
	movq	-344(%rbp), %r10
	movq	-336(%rbp), %rdx
	movq	-304(%rbp), %rdi
.L543:
	testb	$24, %al
	je	.L602
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L602
	movq	%r13, %rsi
	movq	%r9, -336(%rbp)
	movq	%r10, -304(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-336(%rbp), %r9
	movq	-304(%rbp), %r10
	.p2align 4,,10
	.p2align 3
.L602:
	movq	(%r9), %rdi
.L536:
	movq	(%r10), %r12
	movq	%r12, %rax
	notq	%rax
	andl	$1, %eax
	cmpl	$1, %r14d
	jne	.L545
	movq	%r12, 7(%rdi)
	leaq	7(%rdi), %r13
	testb	%al, %al
	jne	.L502
.L790:
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -304(%rbp)
	testl	$262144, %eax
	je	.L551
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rdi, -336(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-304(%rbp), %rcx
	movq	-336(%rbp), %rdi
	movq	8(%rcx), %rax
.L551:
	testb	$24, %al
	je	.L502
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L502
	movq	%r12, %rdx
.L788:
	movq	%r13, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L502
.L826:
	movq	%r8, %rsi
	movq	%rax, -368(%rbp)
	movq	%rdx, -360(%rbp)
	movq	%r8, -352(%rbp)
	movq	%rdi, -344(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-336(%rbp), %rcx
	movq	-368(%rbp), %rax
	movq	-360(%rbp), %rdx
	movq	-352(%rbp), %r8
	movq	8(%rcx), %rsi
	movq	-344(%rbp), %rdi
	jmp	.L528
.L844:
	movq	%rdx, 7(%rdi)
	leaq	7(%rdi), %rsi
	testb	$1, %dl
	je	.L517
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rcx
	movq	%rax, -304(%rbp)
	testl	$262144, %ecx
	je	.L515
	movq	%r8, -368(%rbp)
	movq	%r11, -360(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rsi, -344(%rbp)
	movq	%rdi, -336(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-304(%rbp), %rax
	movq	-368(%rbp), %r8
	movq	-360(%rbp), %r11
	movq	-352(%rbp), %rdx
	movq	8(%rax), %rcx
	movq	-344(%rbp), %rsi
	movq	-336(%rbp), %rdi
.L515:
	andl	$24, %ecx
	je	.L517
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L517
	movq	%r8, -336(%rbp)
	movq	%r11, -304(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-304(%rbp), %r11
	movq	-336(%rbp), %r8
	jmp	.L517
.L564:
	leaq	-224(%rbp), %r12
	movl	$3, %ecx
	movq	%r11, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Descriptor12DataConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
.L565:
	movq	-168(%rbp), %rax
	leaq	-256(%rbp), %rdi
	movq	%r12, %rsi
	movq	(%rax), %rax
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	jmp	.L566
.L526:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L545:
	movq	%r12, 15(%rdi)
	leaq	15(%rdi), %r13
	testb	%al, %al
	je	.L790
	jmp	.L502
.L533:
	movq	-1(%rdi), %rax
	cmpw	$79, 11(%rax)
	jne	.L537
	jmp	.L536
.L824:
	movq	-1(%rcx), %rcx
	cmpw	$78, 11(%rcx)
	jne	.L561
	leaq	-224(%rbp), %r12
	movl	$3, %ecx
	movq	%r11, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	jmp	.L565
.L843:
	movl	-128(%rbp), %eax
	addl	-124(%rbp), %eax
	cmpl	$1020, %eax
	jg	.L649
	leaq	-224(%rbp), %r12
	movl	$2, %ecx
	movq	%r11, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	jmp	.L493
.L842:
	movl	-192(%rbp), %eax
	addl	-188(%rbp), %eax
	cmpl	$1020, %eax
	jg	.L657
	leaq	-224(%rbp), %r12
	movl	$3, %ecx
	movq	%r11, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	jmp	.L447
.L839:
	movl	-188(%rbp), %eax
	addl	-192(%rbp), %eax
	cmpl	$1020, %eax
	jg	.L655
	leaq	-224(%rbp), %r12
	movl	$7, %ecx
	movq	%r11, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	jmp	.L454
.L838:
	movl	-188(%rbp), %eax
	addl	-192(%rbp), %eax
	cmpl	$1020, %eax
	jg	.L650
	leaq	-224(%rbp), %r12
	movl	$2, %ecx
	movq	%r11, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	jmp	.L471
.L799:
	movl	-188(%rbp), %eax
	addl	-192(%rbp), %eax
	cmpl	$1020, %eax
	jg	.L653
	leaq	-224(%rbp), %r12
	movl	$7, %ecx
	movq	%r11, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	jmp	.L464
.L822:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19145:
	.size	_ZN2v88internal16ClassBoilerplate21BuildClassBoilerplateEPNS0_7IsolateEPNS0_12ClassLiteralE, .-_ZN2v88internal16ClassBoilerplate21BuildClassBoilerplateEPNS0_7IsolateEPNS0_12ClassLiteralE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal16ClassBoilerplate23AddToPropertiesTemplateEPNS0_7IsolateENS0_6HandleINS0_14NameDictionaryEEENS4_INS0_4NameEEEiNS1_9ValueKindENS0_6ObjectE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal16ClassBoilerplate23AddToPropertiesTemplateEPNS0_7IsolateENS0_6HandleINS0_14NameDictionaryEEENS4_INS0_4NameEEEiNS1_9ValueKindENS0_6ObjectE, @function
_GLOBAL__sub_I__ZN2v88internal16ClassBoilerplate23AddToPropertiesTemplateEPNS0_7IsolateENS0_6HandleINS0_14NameDictionaryEEENS4_INS0_4NameEEEiNS1_9ValueKindENS0_6ObjectE:
.LFB23435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23435:
	.size	_GLOBAL__sub_I__ZN2v88internal16ClassBoilerplate23AddToPropertiesTemplateEPNS0_7IsolateENS0_6HandleINS0_14NameDictionaryEEENS4_INS0_4NameEEEiNS1_9ValueKindENS0_6ObjectE, .-_GLOBAL__sub_I__ZN2v88internal16ClassBoilerplate23AddToPropertiesTemplateEPNS0_7IsolateENS0_6HandleINS0_14NameDictionaryEEENS4_INS0_4NameEEEiNS1_9ValueKindENS0_6ObjectE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal16ClassBoilerplate23AddToPropertiesTemplateEPNS0_7IsolateENS0_6HandleINS0_14NameDictionaryEEENS4_INS0_4NameEEEiNS1_9ValueKindENS0_6ObjectE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	6
	.long	0
	.long	1
	.long	0
	.align 16
.LC4:
	.long	1
	.long	0
	.long	1
	.long	0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
