	.file	"slot-set.cc"
	.text
	.section	.text._ZN2v88internal10TypedSlotsD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10TypedSlotsD2Ev
	.type	_ZN2v88internal10TypedSlotsD2Ev, @function
_ZN2v88internal10TypedSlotsD2Ev:
.LFB5663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal10TypedSlotsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	jne	.L5
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rbx, %r12
.L5:
	movq	8(%r12), %rdi
	movq	(%r12), %rbx
	testq	%rdi, %rdi
	je	.L3
	call	_ZdaPv@PLT
.L3:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	testq	%rbx, %rbx
	jne	.L17
.L1:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5663:
	.size	_ZN2v88internal10TypedSlotsD2Ev, .-_ZN2v88internal10TypedSlotsD2Ev
	.globl	_ZN2v88internal10TypedSlotsD1Ev
	.set	_ZN2v88internal10TypedSlotsD1Ev,_ZN2v88internal10TypedSlotsD2Ev
	.section	.text._ZN2v88internal10TypedSlotsD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10TypedSlotsD0Ev
	.type	_ZN2v88internal10TypedSlotsD0Ev, @function
_ZN2v88internal10TypedSlotsD0Ev:
.LFB5665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal10TypedSlotsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	jne	.L22
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%rbx, %r12
.L22:
	movq	8(%r12), %rdi
	movq	(%r12), %rbx
	testq	%rdi, %rdi
	je	.L20
	call	_ZdaPv@PLT
.L20:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	testq	%rbx, %rbx
	jne	.L33
.L19:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5665:
	.size	_ZN2v88internal10TypedSlotsD0Ev, .-_ZN2v88internal10TypedSlotsD0Ev
	.section	.text._ZN2v88internal10TypedSlots6InsertENS0_8SlotTypeEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10TypedSlots6InsertENS0_8SlotTypeEj
	.type	_ZN2v88internal10TypedSlots6InsertENS0_8SlotTypeEj, @function
_ZN2v88internal10TypedSlots6InsertENS0_8SlotTypeEj:
.LFB5666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$29, %esi
	orl	%edx, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L41
	movslq	20(%rbx), %rax
	cmpl	16(%rbx), %eax
	je	.L37
	movq	8(%rbx), %rdx
	leaq	(%rdx,%rax,4), %rax
.L36:
	movl	%r12d, (%rax)
	addl	$1, 20(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	leal	(%rax,%rax), %r13d
	movl	$16384, %eax
	movl	$24, %edi
	cmpl	$16384, %r13d
	cmovg	%eax, %r13d
	call	_Znwm@PLT
	movabsq	$2305843009213693950, %rdx
	movq	%rax, %r15
	movq	%rbx, (%rax)
	movslq	%r13d, %rax
	cmpq	%rdx, %rax
	leaq	0(,%rax,4), %rdi
	movq	%r15, %rbx
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_Znam@PLT
	movl	%r13d, 16(%r15)
	movq	%rax, 8(%r15)
	movl	$0, 20(%r15)
	movq	%r15, 8(%r14)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$400, %edi
	movq	$0, (%rax)
	movq	%rax, %rbx
	call	_Znam@PLT
	movq	%rbx, %xmm0
	movq	$100, 16(%rbx)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 8(%rbx)
	movups	%xmm0, 8(%r14)
	jmp	.L36
	.cfi_endproc
.LFE5666:
	.size	_ZN2v88internal10TypedSlots6InsertENS0_8SlotTypeEj, .-_ZN2v88internal10TypedSlots6InsertENS0_8SlotTypeEj
	.section	.text._ZN2v88internal10TypedSlots5MergeEPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10TypedSlots5MergeEPS1_
	.type	_ZN2v88internal10TypedSlots5MergeEPS1_, @function
_ZN2v88internal10TypedSlots5MergeEPS1_:
.LFB5667:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.L42
	cmpq	$0, 8(%rdi)
	je	.L49
	movq	16(%rdi), %rdx
	movq	%rax, (%rdx)
	movq	16(%rsi), %rax
	movq	%rax, 16(%rdi)
.L45:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%rsi)
.L42:
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%rax, 8(%rdi)
	movq	16(%rsi), %rax
	movq	%rax, 16(%rdi)
	jmp	.L45
	.cfi_endproc
.LFE5667:
	.size	_ZN2v88internal10TypedSlots5MergeEPS1_, .-_ZN2v88internal10TypedSlots5MergeEPS1_
	.section	.text._ZN2v88internal10TypedSlots11EnsureChunkEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10TypedSlots11EnsureChunkEv
	.type	_ZN2v88internal10TypedSlots11EnsureChunkEv, @function
_ZN2v88internal10TypedSlots11EnsureChunkEv:
.LFB5668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L56
	movl	16(%r12), %eax
	cmpl	%eax, 20(%r12)
	je	.L57
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	leal	(%rax,%rax), %r13d
	movl	$16384, %eax
	movl	$24, %edi
	cmpl	$16384, %r13d
	cmovg	%eax, %r13d
	call	_Znwm@PLT
	movabsq	$2305843009213693950, %rdx
	movq	%rax, %r14
	movq	%r12, (%rax)
	movslq	%r13d, %rax
	cmpq	%rdx, %rax
	leaq	0(,%rax,4), %rdi
	movq	%r14, %r12
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_Znam@PLT
	movl	%r13d, 16(%r14)
	movq	%rax, 8(%r14)
	movq	%r12, %rax
	movq	%r14, 8(%rbx)
	popq	%rbx
	movl	$0, 20(%r14)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$400, %edi
	movq	$0, (%rax)
	movq	%rax, %r12
	call	_Znam@PLT
	movq	%r12, %xmm0
	movq	$100, 16(%r12)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 8(%r12)
	movq	%r12, %rax
	movups	%xmm0, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5668:
	.size	_ZN2v88internal10TypedSlots11EnsureChunkEv, .-_ZN2v88internal10TypedSlots11EnsureChunkEv
	.section	.text._ZN2v88internal10TypedSlots8NewChunkEPNS1_5ChunkEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10TypedSlots8NewChunkEPNS1_5ChunkEi
	.type	_ZN2v88internal10TypedSlots8NewChunkEPNS1_5ChunkEi, @function
_ZN2v88internal10TypedSlots8NewChunkEPNS1_5ChunkEi:
.LFB5669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$8, %rsp
	call	_Znwm@PLT
	movabsq	$2305843009213693950, %rdx
	movq	%rax, %r12
	movq	%r13, (%rax)
	movslq	%ebx, %rax
	cmpq	%rdx, %rax
	leaq	0(,%rax,4), %rdi
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_Znam@PLT
	movl	%ebx, 16(%r12)
	movq	%rax, 8(%r12)
	movq	%r12, %rax
	movl	$0, 20(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5669:
	.size	_ZN2v88internal10TypedSlots8NewChunkEPNS1_5ChunkEi, .-_ZN2v88internal10TypedSlots8NewChunkEPNS1_5ChunkEi
	.section	.text._ZN2v88internal12TypedSlotSet19FreeToBeFreedChunksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12TypedSlotSet19FreeToBeFreedChunksEv
	.type	_ZN2v88internal12TypedSlotSet19FreeToBeFreedChunksEv, @function
_ZN2v88internal12TypedSlotSet19FreeToBeFreedChunksEv:
.LFB5674:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rax, %rdi
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rax, -104(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	$64, %edi
	call	_Znwm@PLT
	movl	$512, %edi
	leaq	24(%rax), %r8
	movq	%rax, -56(%rbp)
	movq	%r8, -64(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %r8
	movq	$8, 80(%r12)
	movq	112(%r12), %r14
	movq	%rax, %xmm0
	movq	104(%r12), %rcx
	leaq	512(%rax), %rdi
	movq	144(%r12), %rdx
	movq	120(%r12), %r13
	movq	%rax, 24(%rsi)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 120(%r12)
	movq	128(%r12), %r15
	movq	%rax, 128(%r12)
	movq	72(%r12), %rax
	movq	88(%r12), %rbx
	movq	%rdi, 104(%r12)
	movq	%r8, 112(%r12)
	movq	%rdi, 136(%r12)
	movq	%r8, 144(%r12)
	movq	%rsi, 72(%r12)
	movups	%xmm0, 88(%r12)
	leaq	8(%r14), %r12
	movq	%rcx, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rax, -72(%rbp)
	cmpq	%r12, %rdx
	jbe	.L63
	movq	%r12, %r8
	.p2align 4,,10
	.p2align 3
.L68:
	movq	(%r8), %rax
	leaq	512(%rax), %r9
	.p2align 4,,10
	.p2align 3
.L67:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L64
	movl	$24, %esi
	movq	%r9, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZdlPvm@PLT
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r8
	addq	$8, %rax
	cmpq	%r9, %rax
	jne	.L67
	addq	$8, %r8
	cmpq	%r8, -64(%rbp)
	ja	.L68
.L63:
	cmpq	-64(%rbp), %r14
	je	.L69
	cmpq	-56(%rbp), %rbx
	je	.L92
.L70:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L73
.L97:
	movl	$24, %esi
	addq	$8, %rbx
	call	_ZdlPvm@PLT
	cmpq	%rbx, -56(%rbp)
	je	.L92
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L97
.L73:
	addq	$8, %rbx
	cmpq	%rbx, -56(%rbp)
	jne	.L70
	cmpq	%r15, %r13
	je	.L71
	.p2align 4,,10
	.p2align 3
.L72:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L94
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L94:
	addq	$8, %r15
.L92:
	cmpq	%r15, %r13
	jne	.L72
.L71:
	cmpq	$0, -72(%rbp)
	je	.L81
	movq	-64(%rbp), %rbx
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jb	.L83
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L98:
	addq	$8, %r12
.L83:
	movq	(%r14), %rdi
	movq	%r12, %r14
	call	_ZdlPv@PLT
	cmpq	%r12, %rbx
	ja	.L98
.L82:
	movq	-72(%rbp), %rdi
	call	_ZdlPv@PLT
.L81:
	movq	-104(%rbp), %rdi
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	addq	$8, %rax
	cmpq	%r9, %rax
	jne	.L67
	addq	$8, %r8
	cmpq	%r8, -64(%rbp)
	ja	.L68
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L69:
	cmpq	%r13, %rbx
	je	.L71
.L80:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L78
.L99:
	movl	$24, %esi
	addq	$8, %rbx
	call	_ZdlPvm@PLT
	cmpq	%rbx, %r13
	je	.L71
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L99
.L78:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L80
	jmp	.L71
	.cfi_endproc
.LFE5674:
	.size	_ZN2v88internal12TypedSlotSet19FreeToBeFreedChunksEv, .-_ZN2v88internal12TypedSlotSet19FreeToBeFreedChunksEv
	.section	.text._ZN2v88internal12TypedSlotSetD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12TypedSlotSetD2Ev
	.type	_ZN2v88internal12TypedSlotSetD2Ev, @function
_ZN2v88internal12TypedSlotSetD2Ev:
.LFB5671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12TypedSlotSetE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal12TypedSlotSet19FreeToBeFreedChunksEv
	movq	112(%rbx), %r8
	movq	104(%rbx), %rax
	movq	144(%rbx), %rcx
	movq	120(%rbx), %r14
	leaq	8(%r8), %rdx
	movq	%rax, -56(%rbp)
	movq	128(%rbx), %r15
	movq	88(%rbx), %r12
	cmpq	%rdx, %rcx
	jbe	.L108
	.p2align 4,,10
	.p2align 3
.L101:
	movq	(%rdx), %r13
	leaq	512(%r13), %r9
	.p2align 4,,10
	.p2align 3
.L107:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L104
	movl	$24, %esi
	movq	%rdx, -88(%rbp)
	addq	$8, %r13
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZdlPvm@PLT
	movq	-80(%rbp), %r9
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	-88(%rbp), %rdx
	cmpq	%r9, %r13
	jne	.L107
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L101
.L108:
	cmpq	%r8, %rcx
	je	.L141
	cmpq	-56(%rbp), %r12
	je	.L114
	.p2align 4,,10
	.p2align 3
.L109:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L113
	movl	$24, %esi
	addq	$8, %r12
	call	_ZdlPvm@PLT
	cmpq	%r12, -56(%rbp)
	jne	.L109
.L114:
	cmpq	%r15, %r14
	je	.L117
	.p2align 4,,10
	.p2align 3
.L110:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L116
	movl	$24, %esi
	addq	$8, %r15
	call	_ZdlPvm@PLT
	cmpq	%r15, %r14
	jne	.L110
.L117:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L112
	movq	144(%rbx), %rax
	movq	112(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L122
	.p2align 4,,10
	.p2align 3
.L123:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L123
	movq	72(%rbx), %rdi
.L122:
	call	_ZdlPv@PLT
.L112:
	leaq	32(%rbx), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	8(%rbx), %r12
	leaq	16+_ZTVN2v88internal10TypedSlotsE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	jne	.L124
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L144:
	movq	%rbx, %r12
.L124:
	movq	8(%r12), %rdi
	movq	(%r12), %rbx
	testq	%rdi, %rdi
	je	.L125
	call	_ZdaPv@PLT
.L125:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	testq	%rbx, %rbx
	jne	.L144
.L100:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L143
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L143:
	addq	$8, %r12
.L141:
	cmpq	%r12, %r14
	jne	.L121
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L113:
	addq	$8, %r12
	cmpq	%r12, -56(%rbp)
	jne	.L109
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L104:
	addq	$8, %r13
	cmpq	%r13, %r9
	jne	.L107
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L101
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L116:
	addq	$8, %r15
	cmpq	%r15, %r14
	jne	.L110
	jmp	.L117
	.cfi_endproc
.LFE5671:
	.size	_ZN2v88internal12TypedSlotSetD2Ev, .-_ZN2v88internal12TypedSlotSetD2Ev
	.globl	_ZN2v88internal12TypedSlotSetD1Ev
	.set	_ZN2v88internal12TypedSlotSetD1Ev,_ZN2v88internal12TypedSlotSetD2Ev
	.section	.text._ZN2v88internal12TypedSlotSetD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12TypedSlotSetD0Ev
	.type	_ZN2v88internal12TypedSlotSetD0Ev, @function
_ZN2v88internal12TypedSlotSetD0Ev:
.LFB5673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12TypedSlotSetE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	call	_ZN2v88internal12TypedSlotSet19FreeToBeFreedChunksEv
	movq	112(%r12), %r8
	movq	104(%r12), %rax
	movq	144(%r12), %rcx
	movq	120(%r12), %r14
	leaq	8(%r8), %rdx
	movq	%rax, -56(%rbp)
	movq	88(%r12), %rbx
	movq	128(%r12), %r15
	cmpq	%rdx, %rcx
	jbe	.L153
	.p2align 4,,10
	.p2align 3
.L146:
	movq	(%rdx), %r13
	leaq	512(%r13), %r9
	.p2align 4,,10
	.p2align 3
.L152:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L149
	movl	$24, %esi
	movq	%rdx, -88(%rbp)
	addq	$8, %r13
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZdlPvm@PLT
	movq	-80(%rbp), %r9
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	-88(%rbp), %rdx
	cmpq	%r9, %r13
	jne	.L152
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L146
.L153:
	cmpq	%r8, %rcx
	je	.L186
	cmpq	-56(%rbp), %rbx
	je	.L159
	.p2align 4,,10
	.p2align 3
.L154:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L158
	movl	$24, %esi
	addq	$8, %rbx
	call	_ZdlPvm@PLT
	cmpq	%rbx, -56(%rbp)
	jne	.L154
.L159:
	cmpq	%r15, %r14
	je	.L162
	.p2align 4,,10
	.p2align 3
.L155:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L161
	movl	$24, %esi
	addq	$8, %r15
	call	_ZdlPvm@PLT
	cmpq	%r15, %r14
	jne	.L155
.L162:
	movq	72(%r12), %rdi
	testq	%rdi, %rdi
	je	.L157
	movq	144(%r12), %rax
	movq	112(%r12), %rbx
	leaq	8(%rax), %r13
	cmpq	%rbx, %r13
	jbe	.L167
	.p2align 4,,10
	.p2align 3
.L168:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r13
	ja	.L168
	movq	72(%r12), %rdi
.L167:
	call	_ZdlPv@PLT
.L157:
	leaq	32(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	8(%r12), %r13
	leaq	16+_ZTVN2v88internal10TypedSlotsE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L169
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%rbx, %r13
.L169:
	movq	8(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L170
	call	_ZdaPv@PLT
.L170:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	testq	%rbx, %rbx
	jne	.L190
.L171:
	addq	$56, %rsp
	movq	%r12, %rdi
	movl	$152, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L188
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L188:
	addq	$8, %rbx
.L186:
	cmpq	%rbx, %r14
	jne	.L166
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L158:
	addq	$8, %rbx
	cmpq	%rbx, -56(%rbp)
	jne	.L154
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L149:
	addq	$8, %r13
	cmpq	%r13, %r9
	jne	.L152
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L146
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L161:
	addq	$8, %r15
	cmpq	%r15, %r14
	jne	.L155
	jmp	.L162
	.cfi_endproc
.LFE5673:
	.size	_ZN2v88internal12TypedSlotSetD0Ev, .-_ZN2v88internal12TypedSlotSetD0Ev
	.section	.text._ZN2v88internal12TypedSlotSet17ClearInvalidSlotsERKSt3mapIjjSt4lessIjESaISt4pairIKjjEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12TypedSlotSet17ClearInvalidSlotsERKSt3mapIjjSt4lessIjESaISt4pairIKjjEEE
	.type	_ZN2v88internal12TypedSlotSet17ClearInvalidSlotsERKSt3mapIjjSt4lessIjESaISt4pairIKjjEEE, @function
_ZN2v88internal12TypedSlotSet17ClearInvalidSlotsERKSt3mapIjjSt4lessIjESaISt4pairIKjjEEE:
.LFB5678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L191
	leaq	8(%rsi), %rax
	movq	%rsi, %r14
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L200:
	movl	20(%r13), %eax
	movq	8(%r13), %r15
	testl	%eax, %eax
	jle	.L193
	subl	$1, %eax
	leaq	4(%r15,%rax,4), %r12
	.p2align 4,,10
	.p2align 3
.L199:
	movl	(%r15), %ebx
	movl	%ebx, %eax
	shrl	$29, %eax
	cmpl	$5, %eax
	je	.L194
	movq	16(%r14), %rax
	movq	-56(%rbp), %rdi
	andl	$536870911, %ebx
	testq	%rax, %rax
	jne	.L196
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L216:
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L195
.L196:
	cmpl	32(%rax), %ebx
	jb	.L216
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L196
.L195:
	cmpq	%rdi, 24(%r14)
	je	.L194
	call	_ZSt18_Rb_tree_decrementPKSt18_Rb_tree_node_base@PLT
	cmpl	36(%rax), %ebx
	jb	.L217
.L194:
	addq	$4, %r15
	cmpq	%r15, %r12
	jne	.L199
.L193:
	movq	0(%r13), %r13
	testq	%r13, %r13
	jne	.L200
.L191:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	movl	$-1610612736, (%r15)
	addq	$4, %r15
	cmpq	%r15, %r12
	jne	.L199
	jmp	.L193
	.cfi_endproc
.LFE5678:
	.size	_ZN2v88internal12TypedSlotSet17ClearInvalidSlotsERKSt3mapIjjSt4lessIjESaISt4pairIKjjEEE, .-_ZN2v88internal12TypedSlotSet17ClearInvalidSlotsERKSt3mapIjjSt4lessIjESaISt4pairIKjjEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10TypedSlotsD2Ev,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10TypedSlotsD2Ev, @function
_GLOBAL__sub_I__ZN2v88internal10TypedSlotsD2Ev:
.LFB6751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE6751:
	.size	_GLOBAL__sub_I__ZN2v88internal10TypedSlotsD2Ev, .-_GLOBAL__sub_I__ZN2v88internal10TypedSlotsD2Ev
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10TypedSlotsD2Ev
	.weak	_ZTVN2v88internal10TypedSlotsE
	.section	.data.rel.ro.local._ZTVN2v88internal10TypedSlotsE,"awG",@progbits,_ZTVN2v88internal10TypedSlotsE,comdat
	.align 8
	.type	_ZTVN2v88internal10TypedSlotsE, @object
	.size	_ZTVN2v88internal10TypedSlotsE, 32
_ZTVN2v88internal10TypedSlotsE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal10TypedSlotsD1Ev
	.quad	_ZN2v88internal10TypedSlotsD0Ev
	.weak	_ZTVN2v88internal12TypedSlotSetE
	.section	.data.rel.ro.local._ZTVN2v88internal12TypedSlotSetE,"awG",@progbits,_ZTVN2v88internal12TypedSlotSetE,comdat
	.align 8
	.type	_ZTVN2v88internal12TypedSlotSetE, @object
	.size	_ZTVN2v88internal12TypedSlotSetE, 32
_ZTVN2v88internal12TypedSlotSetE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12TypedSlotSetD1Ev
	.quad	_ZN2v88internal12TypedSlotSetD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
