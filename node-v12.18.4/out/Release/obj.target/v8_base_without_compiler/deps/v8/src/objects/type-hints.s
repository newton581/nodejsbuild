	.file	"type-hints.cc"
	.text
	.section	.rodata._ZN2v88internallsERSoNS0_19BinaryOperationHintE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"None"
.LC1:
	.string	"SignedSmall"
.LC2:
	.string	"SignedSmallInputs"
.LC3:
	.string	"Signed32"
.LC4:
	.string	"Number"
.LC5:
	.string	"NumberOrOddball"
.LC6:
	.string	"String"
.LC7:
	.string	"BigInt"
.LC8:
	.string	"Any"
.LC9:
	.string	"unreachable code"
	.section	.text._ZN2v88internallsERSoNS0_19BinaryOperationHintE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoNS0_19BinaryOperationHintE
	.type	_ZN2v88internallsERSoNS0_19BinaryOperationHintE, @function
_ZN2v88internallsERSoNS0_19BinaryOperationHintE:
.LFB4998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpb	$8, %sil
	ja	.L2
	leaq	.L4(%rip), %rcx
	movzbl	%sil, %edx
	movq	%rdi, %r12
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internallsERSoNS0_19BinaryOperationHintE,"a",@progbits
	.align 4
	.align 4
.L4:
	.long	.L12-.L4
	.long	.L11-.L4
	.long	.L10-.L4
	.long	.L9-.L4
	.long	.L8-.L4
	.long	.L7-.L4
	.long	.L6-.L4
	.long	.L5-.L4
	.long	.L3-.L4
	.section	.text._ZN2v88internallsERSoNS0_19BinaryOperationHintE
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$6, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movl	$3, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movl	$4, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	$11, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	$17, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movl	$8, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movl	$6, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	$15, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	$6, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2:
	.cfi_restore_state
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4998:
	.size	_ZN2v88internallsERSoNS0_19BinaryOperationHintE, .-_ZN2v88internallsERSoNS0_19BinaryOperationHintE
	.section	.rodata._ZN2v88internallsERSoNS0_20CompareOperationHintE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"InternalizedString"
.LC11:
	.string	"Symbol"
.LC12:
	.string	"Receiver"
.LC13:
	.string	"ReceiverOrNullOrUndefined"
	.section	.text._ZN2v88internallsERSoNS0_20CompareOperationHintE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoNS0_20CompareOperationHintE
	.type	_ZN2v88internallsERSoNS0_20CompareOperationHintE, @function
_ZN2v88internallsERSoNS0_20CompareOperationHintE:
.LFB4999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpb	$10, %sil
	ja	.L17
	leaq	.L19(%rip), %rcx
	movzbl	%sil, %edx
	movq	%rdi, %r12
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internallsERSoNS0_20CompareOperationHintE,"a",@progbits
	.align 4
	.align 4
.L19:
	.long	.L29-.L19
	.long	.L28-.L19
	.long	.L27-.L19
	.long	.L26-.L19
	.long	.L25-.L19
	.long	.L24-.L19
	.long	.L23-.L19
	.long	.L22-.L19
	.long	.L21-.L19
	.long	.L20-.L19
	.long	.L18-.L19
	.section	.text._ZN2v88internallsERSoNS0_20CompareOperationHintE
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$25, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L30:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movl	$3, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$4, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$11, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$6, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$15, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$18, %edx
	leaq	.LC10(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$6, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$6, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$6, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$8, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L30
.L17:
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4999:
	.size	_ZN2v88internallsERSoNS0_20CompareOperationHintE, .-_ZN2v88internallsERSoNS0_20CompareOperationHintE
	.section	.rodata._ZN2v88internallsERSoNS0_9ForInHintE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"EnumCacheKeys"
.LC15:
	.string	"EnumCacheKeysAndIndices"
	.section	.text._ZN2v88internallsERSoNS0_9ForInHintE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoNS0_9ForInHintE
	.type	_ZN2v88internallsERSoNS0_9ForInHintE, @function
_ZN2v88internallsERSoNS0_9ForInHintE:
.LFB5000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$2, %sil
	je	.L33
	ja	.L34
	testb	%sil, %sil
	je	.L43
	movl	$23, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	cmpb	$3, %sil
	jne	.L44
	movl	$3, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movl	$13, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movl	$4, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L44:
	.cfi_restore_state
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5000:
	.size	_ZN2v88internallsERSoNS0_9ForInHintE, .-_ZN2v88internallsERSoNS0_9ForInHintE
	.section	.rodata._ZN2v88internallsERSoRKNS0_14StringAddFlagsE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"CheckNone"
.LC17:
	.string	"ConvertLeft"
.LC18:
	.string	"ConvertRight"
	.section	.text._ZN2v88internallsERSoRKNS0_14StringAddFlagsE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoRKNS0_14StringAddFlagsE
	.type	_ZN2v88internallsERSoRKNS0_14StringAddFlagsE, @function
_ZN2v88internallsERSoRKNS0_14StringAddFlagsE:
.LFB5001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	(%rsi), %eax
	cmpl	$1, %eax
	je	.L46
	cmpl	$2, %eax
	je	.L47
	testl	%eax, %eax
	je	.L51
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$11, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movl	$9, %edx
	leaq	.LC16(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movl	$12, %edx
	leaq	.LC18(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5001:
	.size	_ZN2v88internallsERSoRKNS0_14StringAddFlagsE, .-_ZN2v88internallsERSoRKNS0_14StringAddFlagsE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internallsERSoNS0_19BinaryOperationHintE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internallsERSoNS0_19BinaryOperationHintE, @function
_GLOBAL__sub_I__ZN2v88internallsERSoNS0_19BinaryOperationHintE:
.LFB5767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5767:
	.size	_GLOBAL__sub_I__ZN2v88internallsERSoNS0_19BinaryOperationHintE, .-_GLOBAL__sub_I__ZN2v88internallsERSoNS0_19BinaryOperationHintE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internallsERSoNS0_19BinaryOperationHintE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
