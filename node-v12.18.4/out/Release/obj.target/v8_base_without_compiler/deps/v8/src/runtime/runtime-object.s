	.file	"runtime-object.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5053:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5053:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5054:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5054:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5056:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5056:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.rodata._ZN2v88internalL22Stats_Runtime_ToObjectEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internalL22Stats_Runtime_ToObjectEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC1:
	.string	"V8.Runtime_Runtime_ToObject"
.LC2:
	.string	"unreachable code"
	.section	.text._ZN2v88internalL22Stats_Runtime_ToObjectEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL22Stats_Runtime_ToObjectEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL22Stats_Runtime_ToObjectEiPmPNS0_7IsolateE.isra.0:
.LFB27236:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$88, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L26
.L6:
	movq	_ZZN2v88internalL22Stats_Runtime_ToObjectEiPmPNS0_7IsolateEE29trace_event_unique_atomic1111(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L27
.L8:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L28
.L9:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L27:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL22Stats_Runtime_ToObjectEiPmPNS0_7IsolateEE29trace_event_unique_atomic1111(%rip)
	movq	%rax, %r12
	jmp	.L8
.L28:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	movq	-40(%rbp), %rdi
	addq	$64, %rsp
	testq	%rdi, %rdi
	je	.L10
	movq	(%rdi), %rax
	call	*8(%rax)
.L10:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L9
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L9
.L26:
	movq	40960(%rdi), %rdi
	leaq	-88(%rbp), %rsi
	movl	$462, %edx
	addq	$23240, %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L6
	.cfi_endproc
.LFE27236:
	.size	_ZN2v88internalL22Stats_Runtime_ToObjectEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL22Stats_Runtime_ToObjectEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internalL15IsValidAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL15IsValidAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internalL15IsValidAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB21503:
	.cfi_startproc
	movq	(%rsi), %rax
	movl	$1, %r8d
	cmpq	%rax, 104(%rdi)
	jne	.L35
.L29:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	cmpq	%rax, 88(%rdi)
	je	.L29
	testb	$1, %al
	jne	.L36
.L31:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movq	-1(%rax), %rax
	testb	$2, 13(%rax)
	jne	.L29
	jmp	.L31
	.cfi_endproc
.LFE21503:
	.size	_ZN2v88internalL15IsValidAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internalL15IsValidAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB10657:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L43
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L46
.L37:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L37
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE10657:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL35Stats_Runtime_HasFastPackedElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"V8.Runtime_Runtime_HasFastPackedElements"
	.section	.rodata._ZN2v88internalL35Stats_Runtime_HasFastPackedElementsEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC4:
	.string	"args[0].IsHeapObject()"
.LC5:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL35Stats_Runtime_HasFastPackedElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_HasFastPackedElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_HasFastPackedElementsEiPmPNS0_7IsolateE.isra.0:
.LFB27234:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L85
.L48:
	movq	_ZZN2v88internalL35Stats_Runtime_HasFastPackedElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic968(%rip), %rbx
	testq	%rbx, %rbx
	je	.L86
.L50:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L87
	movq	0(%r13), %rax
	testb	$1, %al
	je	.L88
.L56:
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpb	$4, %al
	ja	.L57
	testb	$1, %al
	je	.L89
.L57:
	movq	120(%r12), %r12
.L58:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L90
.L47:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L91
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movq	112(%r12), %r12
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L87:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L92
.L53:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L54
	movq	(%rdi), %rax
	call	*8(%rax)
.L54:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L55
	movq	(%rdi), %rax
	call	*8(%rax)
.L55:
	leaq	.LC3(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	0(%r13), %rax
	movq	%r14, -120(%rbp)
	testb	$1, %al
	jne	.L56
.L88:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L86:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L93
.L51:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_HasFastPackedElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic968(%rip)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L85:
	movq	40960(%rsi), %rax
	movl	$427, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L93:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L92:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC3(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L53
.L91:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27234:
	.size	_ZN2v88internalL35Stats_Runtime_HasFastPackedElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_HasFastPackedElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL26Stats_Runtime_IsJSReceiverEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"V8.Runtime_Runtime_IsJSReceiver"
	.section	.text._ZN2v88internalL26Stats_Runtime_IsJSReceiverEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL26Stats_Runtime_IsJSReceiverEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL26Stats_Runtime_IsJSReceiverEiPmPNS0_7IsolateE.isra.0:
.LFB27235:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L126
.L95:
	movq	_ZZN2v88internalL26Stats_Runtime_IsJSReceiverEiPmPNS0_7IsolateEE28trace_event_unique_atomic976(%rip), %rbx
	testq	%rbx, %rbx
	je	.L127
.L97:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L128
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L103
.L105:
	movq	120(%r12), %r12
.L104:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L129
.L94:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L130
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L131
.L98:
	movq	%rbx, _ZZN2v88internalL26Stats_Runtime_IsJSReceiverEiPmPNS0_7IsolateEE28trace_event_unique_atomic976(%rip)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L128:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L132
.L100:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L101
	movq	(%rdi), %rax
	call	*8(%rax)
.L101:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L102
	movq	(%rdi), %rax
	call	*8(%rax)
.L102:
	leaq	.LC6(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	0(%r13), %rax
	movq	%r14, -120(%rbp)
	testb	$1, %al
	je	.L105
.L103:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L105
	movq	112(%r12), %r12
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L126:
	movq	40960(%rsi), %rax
	movl	$431, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L129:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L132:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L131:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L98
.L130:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27235:
	.size	_ZN2v88internalL26Stats_Runtime_IsJSReceiverEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL26Stats_Runtime_IsJSReceiverEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL29Stats_Runtime_GetFunctionNameEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"V8.Runtime_Runtime_GetFunctionName"
	.section	.rodata._ZN2v88internalL29Stats_Runtime_GetFunctionNameEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"args[0].IsJSFunction()"
	.section	.text._ZN2v88internalL29Stats_Runtime_GetFunctionNameEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_GetFunctionNameEiPmPNS0_7IsolateE, @function
_ZN2v88internalL29Stats_Runtime_GetFunctionNameEiPmPNS0_7IsolateE:
.LFB21526:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L165
.L134:
	movq	_ZZN2v88internalL29Stats_Runtime_GetFunctionNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic991(%rip), %rbx
	testq	%rbx, %rbx
	je	.L166
.L136:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L167
.L138:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L142
.L143:
	leaq	.LC8(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L166:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L168
.L137:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_GetFunctionNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic991(%rip)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L142:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L143
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSFunction7GetNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L144
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L144:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L169
.L133:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L170
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L171
.L139:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L140
	movq	(%rdi), %rax
	call	*8(%rax)
.L140:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L141
	movq	(%rdi), %rax
	call	*8(%rax)
.L141:
	leaq	.LC7(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L165:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$423, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L169:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L168:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L171:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L139
.L170:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21526:
	.size	_ZN2v88internalL29Stats_Runtime_GetFunctionNameEiPmPNS0_7IsolateE, .-_ZN2v88internalL29Stats_Runtime_GetFunctionNameEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL30Stats_Runtime_ToFastPropertiesEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"V8.Runtime_Runtime_ToFastProperties"
	.section	.rodata._ZN2v88internalL30Stats_Runtime_ToFastPropertiesEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC10:
	.string	"RuntimeToFastProperties"
	.section	.text._ZN2v88internalL30Stats_Runtime_ToFastPropertiesEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_ToFastPropertiesEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL30Stats_Runtime_ToFastPropertiesEiPmPNS0_7IsolateE.isra.0:
.LFB27241:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L210
.L173:
	movq	_ZZN2v88internalL30Stats_Runtime_ToFastPropertiesEiPmPNS0_7IsolateEE28trace_event_unique_atomic795(%rip), %rbx
	testq	%rbx, %rbx
	je	.L211
.L175:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L212
.L177:
	movl	41104(%r12), %eax
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	0(%r13), %r14
	testb	$1, %r14b
	jne	.L181
.L209:
	movq	%rbx, %rdx
.L186:
	movq	%r15, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L189
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L189:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L213
.L172:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L214
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L215
.L178:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L179
	movq	(%rdi), %rax
	call	*8(%rax)
.L179:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L180
	movq	(%rdi), %rax
	call	*8(%rax)
.L180:
	leaq	.LC9(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L211:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L216
.L176:
	movq	%rbx, _ZZN2v88internalL30Stats_Runtime_ToFastPropertiesEiPmPNS0_7IsolateEE28trace_event_unique_atomic795(%rip)
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L181:
	movq	-1(%r14), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L209
	movq	-1(%r14), %rcx
	movq	%rbx, %rdx
	cmpw	$1025, 11(%rcx)
	je	.L186
	leaq	.LC10(%rip), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc@PLT
	movl	41104(%r12), %eax
	movq	0(%r13), %r14
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L210:
	movq	40960(%rsi), %rax
	movl	$457, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L213:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L216:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L215:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC9(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L178
.L214:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27241:
	.size	_ZN2v88internalL30Stats_Runtime_ToFastPropertiesEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL30Stats_Runtime_ToFastPropertiesEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL40Stats_Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"V8.Runtime_Runtime_StoreDataPropertyInLiteral"
	.section	.rodata._ZN2v88internalL40Stats_Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"args[0].IsJSReceiver()"
	.section	.text._ZN2v88internalL40Stats_Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateE, @function
_ZN2v88internalL40Stats_Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateE:
.LFB21469:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$232, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -208(%rbp)
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L252
.L218:
	movq	_ZZN2v88internalL40Stats_Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic695(%rip), %r13
	testq	%r13, %r13
	je	.L253
.L220:
	movq	$0, -240(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L254
.L222:
	movq	41088(%r12), %rax
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	%rax, -264(%rbp)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L226
.L227:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L253:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L255
.L221:
	movq	%r13, _ZZN2v88internalL40Stats_Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic695(%rip)
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L226:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L227
	leaq	-8(%rbx), %rcx
	movl	$1, %r9d
	movq	%rbx, %rdx
	movq	%r12, %rsi
	leaq	-160(%rbp), %r13
	leaq	-241(%rbp), %r8
	movq	%r13, %rdi
	leaq	-16(%rbx), %r15
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r15, %rsi
	movabsq	$4294967297, %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE@PLT
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L228
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r13
.L229:
	subl	$1, 41104(%r12)
	movq	-264(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L232
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L232:
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L256
.L217:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L257
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	-16(%rbx), %r13
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L254:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L258
.L223:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L224
	movq	(%rdi), %rax
	call	*8(%rax)
.L224:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L225
	movq	(%rdi), %rax
	call	*8(%rax)
.L225:
	leaq	.LC11(%rip), %rax
	movq	%r13, -232(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%r14, -216(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L252:
	movq	40960(%rdx), %rax
	leaq	-200(%rbp), %rsi
	movl	$455, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L256:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L255:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L258:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L223
.L257:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21469:
	.size	_ZN2v88internalL40Stats_Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateE, .-_ZN2v88internalL40Stats_Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL38Stats_Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"V8.Runtime_Runtime_ShrinkPropertyDictionary"
	.section	.text._ZN2v88internalL38Stats_Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Stats_Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateE, @function
_ZN2v88internalL38Stats_Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateE:
.LFB21476:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L299
.L260:
	movq	_ZZN2v88internalL38Stats_Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateEE28trace_event_unique_atomic742(%rip), %rbx
	testq	%rbx, %rbx
	je	.L300
.L262:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L301
.L264:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L268
.L269:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L300:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L302
.L263:
	movq	%rbx, _ZZN2v88internalL38Stats_Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateEE28trace_event_unique_atomic742(%rip)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L268:
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L269
	movq	7(%rax), %r15
	testb	$1, %r15b
	jne	.L271
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %r15
.L271:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L272
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L273:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9HashTableINS0_14NameDictionaryENS0_19NameDictionaryShapeEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EEi@PLT
	movq	0(%r13), %rdx
	leaq	-168(%rbp), %rdi
	movq	%rdx, -168(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L275
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L275:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L303
.L276:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L304
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L305
.L274:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L301:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L306
.L265:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L266
	movq	(%rdi), %rax
	call	*8(%rax)
.L266:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L267
	movq	(%rdi), %rax
	call	*8(%rax)
.L267:
	leaq	.LC13(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L299:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$456, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L303:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L305:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L306:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L302:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L263
.L304:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21476:
	.size	_ZN2v88internalL38Stats_Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateE, .-_ZN2v88internalL38Stats_Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Stats_Runtime_AllocateHeapNumberEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"V8.Runtime_Runtime_AllocateHeapNumber"
	.section	.text._ZN2v88internalL32Stats_Runtime_AllocateHeapNumberEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_AllocateHeapNumberEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_AllocateHeapNumberEiPmPNS0_7IsolateE.isra.0:
.LFB27248:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L337
.L308:
	movq	_ZZN2v88internalL32Stats_Runtime_AllocateHeapNumberEiPmPNS0_7IsolateEE28trace_event_unique_atomic806(%rip), %rbx
	testq	%rbx, %rbx
	je	.L338
.L310:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L339
.L312:
	addl	$1, 41104(%r12)
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	$0, 7(%rdx)
	movq	(%rax), %r14
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L316
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L316:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L340
.L307:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L341
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L342
.L311:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_AllocateHeapNumberEiPmPNS0_7IsolateEE28trace_event_unique_atomic806(%rip)
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L339:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L343
.L313:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L314
	movq	(%rdi), %rax
	call	*8(%rax)
.L314:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L315
	movq	(%rdi), %rax
	call	*8(%rax)
.L315:
	leaq	.LC14(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L340:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L337:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$408, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L343:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L342:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L311
.L341:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27248:
	.size	_ZN2v88internalL32Stats_Runtime_AllocateHeapNumberEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_AllocateHeapNumberEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL36Stats_Runtime_CreateIterResultObjectEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"V8.Runtime_Runtime_CreateIterResultObject"
	.section	.text._ZN2v88internalL36Stats_Runtime_CreateIterResultObjectEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_CreateIterResultObjectEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL36Stats_Runtime_CreateIterResultObjectEiPmPNS0_7IsolateE.isra.0:
.LFB27250:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L374
.L345:
	movq	_ZZN2v88internalL36Stats_Runtime_CreateIterResultObjectEiPmPNS0_7IsolateEE29trace_event_unique_atomic1165(%rip), %rbx
	testq	%rbx, %rbx
	je	.L375
.L347:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L376
.L349:
	addl	$1, 41104(%r12)
	movq	-8(%r13), %rax
	leaq	-152(%rbp), %rdi
	movq	%r12, %rsi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movzbl	%al, %edx
	call	_ZN2v88internal7Factory19NewJSIteratorResultENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L353
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L353:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L377
.L344:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L378
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L379
.L348:
	movq	%rbx, _ZZN2v88internalL36Stats_Runtime_CreateIterResultObjectEiPmPNS0_7IsolateEE29trace_event_unique_atomic1165(%rip)
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L376:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L380
.L350:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L351
	movq	(%rdi), %rax
	call	*8(%rax)
.L351:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L352
	movq	(%rdi), %rax
	call	*8(%rax)
.L352:
	leaq	.LC15(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L377:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L374:
	movq	40960(%rsi), %rax
	movl	$415, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L380:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC15(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L379:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L348
.L378:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27250:
	.size	_ZN2v88internalL36Stats_Runtime_CreateIterResultObjectEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL36Stats_Runtime_CreateIterResultObjectEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_LoadPrivateSetterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"V8.Runtime_Runtime_LoadPrivateSetter"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_LoadPrivateSetterEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"args[0].IsAccessorPair()"
	.section	.text._ZN2v88internalL31Stats_Runtime_LoadPrivateSetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_LoadPrivateSetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Stats_Runtime_LoadPrivateSetterEiPmPNS0_7IsolateE:
.LFB21574:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L413
.L382:
	movq	_ZZN2v88internalL31Stats_Runtime_LoadPrivateSetterEiPmPNS0_7IsolateEE29trace_event_unique_atomic1205(%rip), %r12
	testq	%r12, %r12
	je	.L414
.L384:
	movq	$0, -144(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L415
.L386:
	addl	$1, 41104(%rbx)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L390
.L391:
	leaq	.LC17(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L414:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L416
.L385:
	movq	%r12, _ZZN2v88internalL31Stats_Runtime_LoadPrivateSetterEiPmPNS0_7IsolateEE29trace_event_unique_atomic1205(%rip)
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L390:
	movq	-1(%rax), %rdx
	cmpw	$79, 11(%rdx)
	jne	.L391
	movq	15(%rax), %r12
	subl	$1, 41104(%rbx)
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L417
.L381:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L418
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L419
.L387:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L388
	movq	(%rdi), %rax
	call	*8(%rax)
.L388:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L389
	movq	(%rdi), %rax
	call	*8(%rax)
.L389:
	leaq	.LC16(%rip), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L413:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$438, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L417:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L416:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L419:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC16(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L387
.L418:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21574:
	.size	_ZN2v88internalL31Stats_Runtime_LoadPrivateSetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Stats_Runtime_LoadPrivateSetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Stats_Runtime_LoadPrivateGetterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"V8.Runtime_Runtime_LoadPrivateGetter"
	.section	.text._ZN2v88internalL31Stats_Runtime_LoadPrivateGetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_LoadPrivateGetterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Stats_Runtime_LoadPrivateGetterEiPmPNS0_7IsolateE:
.LFB21577:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L452
.L421:
	movq	_ZZN2v88internalL31Stats_Runtime_LoadPrivateGetterEiPmPNS0_7IsolateEE29trace_event_unique_atomic1213(%rip), %r12
	testq	%r12, %r12
	je	.L453
.L423:
	movq	$0, -144(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L454
.L425:
	addl	$1, 41104(%rbx)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L429
.L430:
	leaq	.LC17(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L453:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L455
.L424:
	movq	%r12, _ZZN2v88internalL31Stats_Runtime_LoadPrivateGetterEiPmPNS0_7IsolateEE29trace_event_unique_atomic1213(%rip)
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L429:
	movq	-1(%rax), %rdx
	cmpw	$79, 11(%rdx)
	jne	.L430
	movq	7(%rax), %r12
	subl	$1, 41104(%rbx)
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L456
.L420:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L457
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L458
.L426:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L427
	movq	(%rdi), %rax
	call	*8(%rax)
.L427:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L428
	movq	(%rdi), %rax
	call	*8(%rax)
.L428:
	leaq	.LC18(%rip), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L452:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$437, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L456:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L455:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L458:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC18(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L426
.L457:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21577:
	.size	_ZN2v88internalL31Stats_Runtime_LoadPrivateGetterEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Stats_Runtime_LoadPrivateGetterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Stats_Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"V8.Runtime_Runtime_CreatePrivateAccessors"
	.section	.text._ZN2v88internalL36Stats_Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL36Stats_Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateE.isra.0:
.LFB27251:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L518
.L460:
	movq	_ZZN2v88internalL36Stats_Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1221(%rip), %rbx
	testq	%rbx, %rbx
	je	.L519
.L462:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L520
.L464:
	addl	$1, 41104(%r12)
	movq	%r12, %rdi
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory15NewAccessorPairEv@PLT
	movq	-8(%r14), %r8
	movq	(%r14), %r14
	movq	(%rax), %r13
	testb	$1, %r14b
	jne	.L521
	movq	%r14, 7(%r13)
.L469:
	testb	$1, %r8b
	jne	.L522
	movq	%r8, 15(%r13)
.L473:
	movq	(%rax), %r13
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L476
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L476:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L523
.L459:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L524
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L519:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L525
.L463:
	movq	%rbx, _ZZN2v88internalL36Stats_Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1221(%rip)
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L522:
	movq	%r8, %r14
	andq	$-262144, %r14
	movq	24(%r14), %rdx
	cmpq	-37488(%rdx), %r8
	je	.L473
	movq	%r8, 15(%r13)
	leaq	15(%r13), %rsi
	movq	8(%r14), %rdx
	testl	$262144, %edx
	je	.L481
	movq	%r8, %rdx
	movq	%r13, %rdi
	movq	%rax, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rdx
	movq	-184(%rbp), %rax
	movq	-176(%rbp), %r8
	movq	-168(%rbp), %rsi
.L481:
	andl	$24, %edx
	je	.L473
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L473
	movq	%r8, %rdx
	movq	%r13, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-168(%rbp), %rax
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L521:
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rdx
	cmpq	-37488(%rdx), %r14
	je	.L469
	movq	%r14, 7(%r13)
	leaq	7(%r13), %rsi
	movq	8(%rcx), %rdx
	testl	$262144, %edx
	je	.L479
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rcx, -192(%rbp)
	movq	%r8, -184(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-192(%rbp), %rcx
	movq	-184(%rbp), %r8
	movq	-176(%rbp), %rax
	movq	-168(%rbp), %rsi
	movq	8(%rcx), %rdx
.L479:
	andl	$24, %edx
	je	.L469
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L469
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%r8, -176(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-176(%rbp), %r8
	movq	-168(%rbp), %rax
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L520:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L526
.L465:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L466
	movq	(%rdi), %rax
	call	*8(%rax)
.L466:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L467
	movq	(%rdi), %rax
	call	*8(%rax)
.L467:
	leaq	.LC19(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L518:
	movq	40960(%rsi), %rax
	movl	$416, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L523:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L526:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC19(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L525:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L463
.L524:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27251:
	.size	_ZN2v88internalL36Stats_Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL36Stats_Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL29Stats_Runtime_AddPrivateBrandEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"V8.Runtime_Runtime_AddPrivateBrand"
	.section	.rodata._ZN2v88internalL29Stats_Runtime_AddPrivateBrandEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC21:
	.string	"args[1].IsSymbol()"
	.section	.rodata._ZN2v88internalL29Stats_Runtime_AddPrivateBrandEiPmPNS0_7IsolateE.str1.8
	.align 8
.LC22:
	.string	"Object::AddDataProperty(&it, brand, attributes, Just(kDontThrow), StoreOrigin::kMaybeKeyed) .FromJust()"
	.section	.text._ZN2v88internalL29Stats_Runtime_AddPrivateBrandEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_AddPrivateBrandEiPmPNS0_7IsolateE, @function
_ZN2v88internalL29Stats_Runtime_AddPrivateBrandEiPmPNS0_7IsolateE:
.LFB21583:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$312, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -288(%rbp)
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L590
.L528:
	movq	_ZZN2v88internalL29Stats_Runtime_AddPrivateBrandEiPmPNS0_7IsolateEE29trace_event_unique_atomic1231(%rip), %r13
	testq	%r13, %r13
	je	.L591
.L530:
	movq	$0, -320(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L592
.L532:
	movq	41088(%r12), %r15
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	(%rbx), %rdx
	testb	$1, %dl
	jne	.L536
.L537:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L591:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L593
.L531:
	movq	%r13, _ZZN2v88internalL29Stats_Runtime_AddPrivateBrandEiPmPNS0_7IsolateEE29trace_event_unique_atomic1231(%rip)
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L536:
	movq	-1(%rdx), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L537
	movq	-8(%rbx), %rax
	leaq	-8(%rbx), %r13
	testb	$1, %al
	jne	.L594
.L538:
	leaq	.LC21(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L592:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L595
.L533:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L534
	movq	(%rdi), %rax
	call	*8(%rax)
.L534:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L535
	movq	(%rdi), %rax
	call	*8(%rax)
.L535:
	leaq	.LC20(%rip), %rax
	movq	%r13, -312(%rbp)
	movq	%rax, -304(%rbp)
	leaq	-312(%rbp), %rax
	movq	%r14, -296(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L594:
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L538
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	jbe	.L596
.L541:
	testb	$1, %dl
	jne	.L548
.L550:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rcx
.L549:
	movq	-8(%rbx), %rdx
	movl	$1, %eax
	movq	-1(%rdx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L551
	movl	11(%rdx), %eax
	notl	%eax
	andl	$1, %eax
.L551:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	-8(%rbx), %rax
	movq	%r12, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r13, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L597
.L552:
	leaq	-240(%rbp), %rdi
	movq	%rax, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%rbx, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%rcx, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L547:
	cmpl	$4, -236(%rbp)
	je	.L553
	movq	%r13, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$175, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L554:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L559
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L559:
	leaq	-320(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L598
.L527:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L599
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L553:
	.cfi_restore_state
	xorl	%r8d, %r8d
	leaq	-240(%rbp), %rdi
	movl	$7, %edx
	movq	%r13, %rsi
	movabsq	$4294967297, %rcx
	call	_ZN2v88internal6Object15AddDataPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS0_11StoreOriginE@PLT
	testb	%al, %al
	je	.L600
.L555:
	shrw	$8, %ax
	je	.L601
	movq	(%rbx), %r13
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L596:
	movq	%rax, -160(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L542
	testb	$2, %al
	jne	.L541
.L542:
	leaq	-160(%rbp), %r8
	leaq	-324(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -344(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-344(%rbp), %r8
	testb	%al, %al
	je	.L602
	movq	(%rbx), %rax
	movl	-324(%rbp), %edx
	testb	$1, %al
	jne	.L544
.L546:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r8, -352(%rbp)
	movl	%edx, -344(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movl	-344(%rbp), %edx
	movq	-352(%rbp), %r8
.L545:
	movabsq	$824633720832, %rcx
	movq	%r8, %rdi
	movl	$1, -160(%rbp)
	movq	%rcx, -148(%rbp)
	movq	%r12, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%rbx, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -96(%rbp)
	movl	%edx, -88(%rbp)
	movl	$-1, -84(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r13, -128(%rbp)
	movdqa	-112(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm5
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm1, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm5, -176(%rbp)
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L602:
	movq	(%rbx), %rdx
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L544:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L546
	movq	%rbx, %rax
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L548:
	movq	-1(%rdx), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L550
	movq	%rbx, %rcx
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L590:
	movq	40960(%rdx), %rax
	leaq	-280(%rbp), %rsi
	movl	$407, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L597:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rcx, -344(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-344(%rbp), %rcx
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L598:
	leaq	-280(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L595:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC20(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L593:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L601:
	leaq	.LC22(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L600:
	movl	%eax, -344(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-344(%rbp), %eax
	jmp	.L555
.L599:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21583:
	.size	_ZN2v88internalL29Stats_Runtime_AddPrivateBrandEiPmPNS0_7IsolateE, .-_ZN2v88internalL29Stats_Runtime_AddPrivateBrandEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL21Stats_Runtime_ClassOfEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC23:
	.string	"V8.Runtime_Runtime_ClassOf"
	.section	.text._ZN2v88internalL21Stats_Runtime_ClassOfEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL21Stats_Runtime_ClassOfEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL21Stats_Runtime_ClassOfEiPmPNS0_7IsolateE.isra.0:
.LFB27254:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L635
.L604:
	movq	_ZZN2v88internalL21Stats_Runtime_ClassOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic983(%rip), %rbx
	testq	%rbx, %rbx
	je	.L636
.L606:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L637
.L608:
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L612
.L614:
	movq	104(%r13), %r12
.L613:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L638
.L603:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L639
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L636:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L640
.L607:
	movq	%rbx, _ZZN2v88internalL21Stats_Runtime_ClassOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic983(%rip)
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L612:
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L614
	leaq	-152(%rbp), %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal10JSReceiver10class_nameEv@PLT
	movq	%rax, %r12
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L637:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L641
.L609:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L610
	movq	(%rdi), %rax
	call	*8(%rax)
.L610:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L611
	movq	(%rdi), %rax
	call	*8(%rax)
.L611:
	leaq	.LC23(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L638:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L635:
	movq	40960(%rsi), %rax
	movl	$409, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L641:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC23(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L640:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L607
.L639:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27254:
	.size	_ZN2v88internalL21Stats_Runtime_ClassOfEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL21Stats_Runtime_ClassOfEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL49Stats_Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"V8.Runtime_Runtime_CompleteInobjectSlackTrackingForMap"
	.section	.rodata._ZN2v88internalL49Stats_Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC25:
	.string	"args[0].IsMap()"
	.section	.text._ZN2v88internalL49Stats_Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL49Stats_Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL49Stats_Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateE.isra.0:
.LFB27255:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L674
.L643:
	movq	_ZZN2v88internalL49Stats_Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateEE28trace_event_unique_atomic831(%rip), %rbx
	testq	%rbx, %rbx
	je	.L675
.L645:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L676
.L647:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L651
.L652:
	leaq	.LC25(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L675:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L677
.L646:
	movq	%rbx, _ZZN2v88internalL49Stats_Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateEE28trace_event_unique_atomic831(%rip)
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L651:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L652
	leaq	-152(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal3Map29CompleteInobjectSlackTrackingEPNS0_7IsolateE@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L653
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L653:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L678
.L642:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L679
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L676:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L680
.L648:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L649
	movq	(%rdi), %rax
	call	*8(%rax)
.L649:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L650
	movq	(%rdi), %rax
	call	*8(%rax)
.L650:
	leaq	.LC24(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L674:
	movq	40960(%rsi), %rax
	movl	$411, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L678:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L677:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L680:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC24(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L648
.L679:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27255:
	.size	_ZN2v88internalL49Stats_Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL49Stats_Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL29Stats_Runtime_AddPrivateFieldEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"V8.Runtime_Runtime_AddPrivateField"
	.align 8
.LC27:
	.string	"Object::AddDataProperty(&it, value, NONE, Just(kDontThrow), StoreOrigin::kMaybeKeyed) .FromJust()"
	.section	.text._ZN2v88internalL29Stats_Runtime_AddPrivateFieldEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_AddPrivateFieldEiPmPNS0_7IsolateE, @function
_ZN2v88internalL29Stats_Runtime_AddPrivateFieldEiPmPNS0_7IsolateE:
.LFB21586:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -288(%rbp)
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L744
.L682:
	movq	_ZZN2v88internalL29Stats_Runtime_AddPrivateFieldEiPmPNS0_7IsolateEE29trace_event_unique_atomic1256(%rip), %rbx
	testq	%rbx, %rbx
	je	.L745
.L684:
	movq	$0, -320(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L746
.L686:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	(%r15), %rdx
	testb	$1, %dl
	jne	.L690
.L691:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L745:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L747
.L685:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_AddPrivateFieldEiPmPNS0_7IsolateEE29trace_event_unique_atomic1256(%rip)
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L690:
	movq	-1(%rdx), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L691
	movq	-8(%r15), %rax
	leaq	-8(%r15), %r13
	testb	$1, %al
	jne	.L748
.L692:
	leaq	.LC21(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L746:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L749
.L687:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L688
	movq	(%rdi), %rax
	call	*8(%rax)
.L688:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L689
	movq	(%rdi), %rax
	call	*8(%rax)
.L689:
	leaq	.LC26(%rip), %rax
	movq	%rbx, -312(%rbp)
	movq	%rax, -304(%rbp)
	leaq	-312(%rbp), %rax
	movq	%r13, -296(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L748:
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L692
	leaq	-16(%r15), %rcx
	movq	%rcx, -344(%rbp)
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	jbe	.L750
.L695:
	testb	$1, %dl
	jne	.L702
.L704:
	movl	$-1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rcx
.L703:
	movq	-8(%r15), %rdx
	movl	$1, %eax
	movq	-1(%rdx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L705
	movl	11(%rdx), %eax
	notl	%eax
	andl	$1, %eax
.L705:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	-8(%r15), %rax
	movq	%r12, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r13, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L751
.L706:
	leaq	-240(%rbp), %rdi
	movq	%rax, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r15, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%rcx, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L701:
	cmpl	$4, -236(%rbp)
	je	.L707
	movq	%r13, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$175, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L708:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L713
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L713:
	leaq	-320(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L752
.L681:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L753
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L707:
	.cfi_restore_state
	movq	-344(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	leaq	-240(%rbp), %rdi
	movabsq	$4294967297, %rcx
	call	_ZN2v88internal6Object15AddDataPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS0_11StoreOriginE@PLT
	testb	%al, %al
	je	.L754
.L709:
	shrw	$8, %ax
	je	.L755
	movq	88(%r12), %r13
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L750:
	movq	%rax, -160(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L696
	testb	$2, %al
	jne	.L695
.L696:
	leaq	-160(%rbp), %r8
	leaq	-324(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -352(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-352(%rbp), %r8
	testb	%al, %al
	je	.L756
	movq	(%r15), %rax
	movl	-324(%rbp), %edx
	testb	$1, %al
	jne	.L698
.L700:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, -360(%rbp)
	movl	%edx, -352(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movl	-352(%rbp), %edx
	movq	-360(%rbp), %r8
.L699:
	movabsq	$824633720832, %rcx
	movq	%r8, %rdi
	movl	$1, -160(%rbp)
	movq	%rcx, -148(%rbp)
	movq	%r12, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%r15, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -96(%rbp)
	movl	%edx, -88(%rbp)
	movl	$-1, -84(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r13, -128(%rbp)
	movdqa	-112(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm5
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm1, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm5, -176(%rbp)
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L756:
	movq	(%r15), %rdx
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L698:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L700
	movq	%r15, %rax
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L702:
	movq	-1(%rdx), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L704
	movq	%r15, %rcx
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L744:
	movq	40960(%rdx), %rax
	leaq	-280(%rbp), %rsi
	movl	$406, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L751:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rcx, -352(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-352(%rbp), %rcx
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L752:
	leaq	-280(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L749:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC26(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L747:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L755:
	leaq	.LC27(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L754:
	movl	%eax, -344(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-344(%rbp), %eax
	jmp	.L709
.L753:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21586:
	.size	_ZN2v88internalL29Stats_Runtime_AddPrivateFieldEiPmPNS0_7IsolateE, .-_ZN2v88internalL29Stats_Runtime_AddPrivateFieldEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL34Stats_Runtime_InternalSetPrototypeEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC28:
	.string	"V8.Runtime_Runtime_InternalSetPrototype"
	.section	.text._ZN2v88internalL34Stats_Runtime_InternalSetPrototypeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_InternalSetPrototypeEiPmPNS0_7IsolateE, @function
_ZN2v88internalL34Stats_Runtime_InternalSetPrototypeEiPmPNS0_7IsolateE:
.LFB21424:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L792
.L758:
	movq	_ZZN2v88internalL34Stats_Runtime_InternalSetPrototypeEiPmPNS0_7IsolateEE28trace_event_unique_atomic425(%rip), %r13
	testq	%r13, %r13
	je	.L793
.L760:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L794
.L762:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L766
.L767:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L793:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L795
.L761:
	movq	%r13, _ZZN2v88internalL34Stats_Runtime_InternalSetPrototypeEiPmPNS0_7IsolateEE28trace_event_unique_atomic425(%rip)
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L766:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L767
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-8(%rbx), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal10JSReceiver12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE@PLT
	testb	%al, %al
	jne	.L768
	movq	312(%r12), %r15
.L769:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L772
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L772:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L796
.L757:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L797
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L768:
	.cfi_restore_state
	movq	(%rbx), %r15
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L794:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L798
.L763:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L764
	movq	(%rdi), %rax
	call	*8(%rax)
.L764:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L765
	movq	(%rdi), %rax
	call	*8(%rax)
.L765:
	leaq	.LC28(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L792:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$430, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L796:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L795:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L798:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC28(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L763
.L797:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21424:
	.size	_ZN2v88internalL34Stats_Runtime_InternalSetPrototypeEiPmPNS0_7IsolateE, .-_ZN2v88internalL34Stats_Runtime_InternalSetPrototypeEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL26Stats_Runtime_ObjectValuesEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"V8.Runtime_Runtime_ObjectValues"
	.section	.text._ZN2v88internalL26Stats_Runtime_ObjectValuesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL26Stats_Runtime_ObjectValuesEiPmPNS0_7IsolateE, @function
_ZN2v88internalL26Stats_Runtime_ObjectValuesEiPmPNS0_7IsolateE:
.LFB21430:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L834
.L800:
	movq	_ZZN2v88internalL26Stats_Runtime_ObjectValuesEiPmPNS0_7IsolateEE28trace_event_unique_atomic449(%rip), %rbx
	testq	%rbx, %rbx
	je	.L835
.L802:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L836
.L804:
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L808
.L809:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L835:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L837
.L803:
	movq	%rbx, _ZZN2v88internalL26Stats_Runtime_ObjectValuesEiPmPNS0_7IsolateEE28trace_event_unique_atomic449(%rip)
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L808:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L809
	movl	$18, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver12GetOwnValuesENS0_6HandleIS1_EENS0_14PropertyFilterEb@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L838
	movq	(%rax), %rax
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
.L811:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L814
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L814:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L839
.L799:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L840
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L836:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L841
.L805:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L806
	movq	(%rdi), %rax
	call	*8(%rax)
.L806:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L807
	movq	(%rdi), %rax
	call	*8(%rax)
.L807:
	leaq	.LC29(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L838:
	movq	312(%r12), %r14
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L834:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$448, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L839:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L837:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L841:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC29(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L805
.L840:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21430:
	.size	_ZN2v88internalL26Stats_Runtime_ObjectValuesEiPmPNS0_7IsolateE, .-_ZN2v88internalL26Stats_Runtime_ObjectValuesEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL38Stats_Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC30:
	.string	"V8.Runtime_Runtime_ObjectValuesSkipFastPath"
	.section	.text._ZN2v88internalL38Stats_Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Stats_Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateE, @function
_ZN2v88internalL38Stats_Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateE:
.LFB21433:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L877
.L843:
	movq	_ZZN2v88internalL38Stats_Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic463(%rip), %rbx
	testq	%rbx, %rbx
	je	.L878
.L845:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L879
.L847:
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L851
.L852:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L878:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L880
.L846:
	movq	%rbx, _ZZN2v88internalL38Stats_Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic463(%rip)
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L851:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L852
	movl	$18, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver12GetOwnValuesENS0_6HandleIS1_EENS0_14PropertyFilterEb@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L881
	movq	(%rax), %rax
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
.L854:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L857
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L857:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L882
.L842:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L883
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L879:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L884
.L848:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L849
	movq	(%rdi), %rax
	call	*8(%rax)
.L849:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L850
	movq	(%rdi), %rax
	call	*8(%rax)
.L850:
	leaq	.LC30(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L881:
	movq	312(%r12), %r14
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L877:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$449, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L882:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L880:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L884:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC30(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L848
.L883:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21433:
	.size	_ZN2v88internalL38Stats_Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateE, .-_ZN2v88internalL38Stats_Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL27Stats_Runtime_ObjectEntriesEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC31:
	.string	"V8.Runtime_Runtime_ObjectEntries"
	.section	.text._ZN2v88internalL27Stats_Runtime_ObjectEntriesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_ObjectEntriesEiPmPNS0_7IsolateE, @function
_ZN2v88internalL27Stats_Runtime_ObjectEntriesEiPmPNS0_7IsolateE:
.LFB21436:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L920
.L886:
	movq	_ZZN2v88internalL27Stats_Runtime_ObjectEntriesEiPmPNS0_7IsolateEE28trace_event_unique_atomic477(%rip), %rbx
	testq	%rbx, %rbx
	je	.L921
.L888:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L922
.L890:
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L894
.L895:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L921:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L923
.L889:
	movq	%rbx, _ZZN2v88internalL27Stats_Runtime_ObjectEntriesEiPmPNS0_7IsolateEE28trace_event_unique_atomic477(%rip)
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L894:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L895
	movl	$18, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver13GetOwnEntriesENS0_6HandleIS1_EENS0_14PropertyFilterEb@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L924
	movq	(%rax), %rax
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
.L897:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L900
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L900:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L925
.L885:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L926
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L922:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L927
.L891:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L892
	movq	(%rdi), %rax
	call	*8(%rax)
.L892:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L893
	movq	(%rdi), %rax
	call	*8(%rax)
.L893:
	leaq	.LC31(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L924:
	movq	312(%r12), %r14
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L920:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$441, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L925:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L923:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L927:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC31(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L891
.L926:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21436:
	.size	_ZN2v88internalL27Stats_Runtime_ObjectEntriesEiPmPNS0_7IsolateE, .-_ZN2v88internalL27Stats_Runtime_ObjectEntriesEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Stats_Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC32:
	.string	"V8.Runtime_Runtime_ObjectEntriesSkipFastPath"
	.section	.text._ZN2v88internalL39Stats_Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Stats_Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Stats_Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateE:
.LFB21439:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L963
.L929:
	movq	_ZZN2v88internalL39Stats_Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic491(%rip), %rbx
	testq	%rbx, %rbx
	je	.L964
.L931:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L965
.L933:
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L937
.L938:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L964:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L966
.L932:
	movq	%rbx, _ZZN2v88internalL39Stats_Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic491(%rip)
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L937:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L938
	movl	$18, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver13GetOwnEntriesENS0_6HandleIS1_EENS0_14PropertyFilterEb@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L967
	movq	(%rax), %rax
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
.L940:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L943
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L943:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L968
.L928:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L969
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L965:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L970
.L934:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L935
	movq	(%rdi), %rax
	call	*8(%rax)
.L935:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L936
	movq	(%rdi), %rax
	call	*8(%rax)
.L936:
	leaq	.LC32(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L967:
	movq	312(%r12), %r14
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L963:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$442, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L968:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L966:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L970:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC32(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L934
.L969:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21439:
	.size	_ZN2v88internalL39Stats_Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Stats_Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Stats_Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC33:
	.string	"V8.Runtime_Runtime_ObjectIsExtensible"
	.section	.text._ZN2v88internalL32Stats_Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateE.isra.0:
.LFB27256:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1014
.L972:
	movq	_ZZN2v88internalL32Stats_Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateEE28trace_event_unique_atomic505(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1015
.L974:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1016
.L976:
	movl	41104(%r12), %eax
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L980
.L1013:
	movq	%rbx, %rdx
.L985:
	movq	120(%r12), %r13
.L986:
	movq	%r14, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L990
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L990:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1017
.L971:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1018
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L980:
	.cfi_restore_state
	movq	-1(%rdx), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L1013
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver12IsExtensibleENS0_6HandleIS1_EE@PLT
	movzbl	%ah, %ecx
	testb	%al, %al
	jne	.L983
	movl	41104(%r12), %eax
	movq	312(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L1016:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1019
.L977:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L978
	movq	(%rdi), %rax
	call	*8(%rax)
.L978:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L979
	movq	(%rdi), %rax
	call	*8(%rax)
.L979:
	leaq	.LC33(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L1015:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1020
.L975:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateEE28trace_event_unique_atomic505(%rip)
	jmp	.L974
.L983:
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	subl	$1, %eax
	testb	%cl, %cl
	je	.L985
	movq	112(%r12), %r13
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L1014:
	movq	40960(%rsi), %rax
	movl	$446, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L1017:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L1020:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L1019:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC33(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L977
.L1018:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27256:
	.size	_ZN2v88internalL32Stats_Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL46Stats_Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC34:
	.string	"V8.Runtime_Runtime_JSReceiverPreventExtensionsThrow"
	.section	.text._ZN2v88internalL46Stats_Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Stats_Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateE, @function
_ZN2v88internalL46Stats_Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateE:
.LFB21445:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1056
.L1022:
	movq	_ZZN2v88internalL46Stats_Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic518(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1057
.L1024:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1058
.L1026:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1030
.L1031:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1057:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1059
.L1025:
	movq	%rbx, _ZZN2v88internalL46Stats_Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic518(%rip)
	jmp	.L1024
	.p2align 4,,10
	.p2align 3
.L1030:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1031
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE@PLT
	testb	%al, %al
	jne	.L1032
	movq	312(%r12), %r13
.L1033:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1036
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1036:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1060
.L1021:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1061
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1032:
	.cfi_restore_state
	movq	0(%r13), %r13
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1058:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1062
.L1027:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1028
	movq	(%rdi), %rax
	call	*8(%rax)
.L1028:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1029
	movq	(%rdi), %rax
	call	*8(%rax)
.L1029:
	leaq	.LC34(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1056:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$433, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1060:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1059:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1062:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC34(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1027
.L1061:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21445:
	.size	_ZN2v88internalL46Stats_Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateE, .-_ZN2v88internalL46Stats_Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL50Stats_Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC35:
	.string	"V8.Runtime_Runtime_JSReceiverPreventExtensionsDontThrow"
	.section	.text._ZN2v88internalL50Stats_Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL50Stats_Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateE, @function
_ZN2v88internalL50Stats_Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateE:
.LFB21448:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1098
.L1064:
	movq	_ZZN2v88internalL50Stats_Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic529(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1099
.L1066:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1100
.L1068:
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L1072
.L1073:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1099:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1101
.L1067:
	movq	%rbx, _ZZN2v88internalL50Stats_Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic529(%rip)
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L1072:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1073
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE@PLT
	testb	%al, %al
	je	.L1102
	movzbl	%ah, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r14
.L1075:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1078
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1078:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1103
.L1063:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1104
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1100:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1105
.L1069:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1070
	movq	(%rdi), %rax
	call	*8(%rax)
.L1070:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1071
	movq	(%rdi), %rax
	call	*8(%rax)
.L1071:
	leaq	.LC35(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1068
	.p2align 4,,10
	.p2align 3
.L1102:
	movq	312(%r12), %r14
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1098:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$432, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1103:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1101:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1105:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC35(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1069
.L1104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21448:
	.size	_ZN2v88internalL50Stats_Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateE, .-_ZN2v88internalL50Stats_Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Stats_Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC36:
	.string	"V8.Runtime_Runtime_JSReceiverSetPrototypeOfThrow"
	.section	.text._ZN2v88internalL43Stats_Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Stats_Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateE, @function
_ZN2v88internalL43Stats_Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateE:
.LFB21454:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1141
.L1107:
	movq	_ZZN2v88internalL43Stats_Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic549(%rip), %r13
	testq	%r13, %r13
	je	.L1142
.L1109:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L1143
.L1111:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L1115
.L1116:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1142:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1144
.L1110:
	movq	%r13, _ZZN2v88internalL43Stats_Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic549(%rip)
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1115:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1116
	xorl	%ecx, %ecx
	leaq	-8(%rbx), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal10JSReceiver12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE@PLT
	testb	%al, %al
	jne	.L1117
	movq	312(%r12), %r15
.L1118:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1121
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1121:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1145
.L1106:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1146
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1117:
	.cfi_restore_state
	movq	(%rbx), %r15
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1143:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1147
.L1112:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1113
	movq	(%rdi), %rax
	call	*8(%rax)
.L1113:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1114
	movq	(%rdi), %rax
	call	*8(%rax)
.L1114:
	leaq	.LC36(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1141:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$436, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1145:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1144:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1147:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC36(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1112
.L1146:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21454:
	.size	_ZN2v88internalL43Stats_Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateE, .-_ZN2v88internalL43Stats_Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL47Stats_Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"V8.Runtime_Runtime_JSReceiverSetPrototypeOfDontThrow"
	.section	.text._ZN2v88internalL47Stats_Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Stats_Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateE, @function
_ZN2v88internalL47Stats_Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateE:
.LFB21457:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1183
.L1149:
	movq	_ZZN2v88internalL47Stats_Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic562(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1184
.L1151:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1185
.L1153:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1157
.L1158:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1184:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1186
.L1152:
	movq	%rbx, _ZZN2v88internalL47Stats_Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic562(%rip)
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1157:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1158
	leaq	-8(%r13), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE@PLT
	testb	%al, %al
	je	.L1187
	movzbl	%ah, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r13
.L1160:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1163
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1163:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1188
.L1148:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1189
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1185:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1190
.L1154:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1155
	movq	(%rdi), %rax
	call	*8(%rax)
.L1155:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1156
	movq	(%rdi), %rax
	call	*8(%rax)
.L1156:
	leaq	.LC37(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1187:
	movq	312(%r12), %r13
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1183:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$435, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1149
	.p2align 4,,10
	.p2align 3
.L1188:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1186:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1190:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC37(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1154
.L1189:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21457:
	.size	_ZN2v88internalL47Stats_Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateE, .-_ZN2v88internalL47Stats_Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Stats_Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC38:
	.string	"V8.Runtime_Runtime_GetOwnPropertyKeys"
	.section	.rodata._ZN2v88internalL32Stats_Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC39:
	.string	"args[1].IsSmi()"
	.section	.text._ZN2v88internalL32Stats_Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateE, @function
_ZN2v88internalL32Stats_Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateE:
.LFB21482:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1227
.L1192:
	movq	_ZZN2v88internalL32Stats_Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateEE28trace_event_unique_atomic779(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1228
.L1194:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1229
.L1196:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1200
.L1201:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1228:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1230
.L1195:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateEE28trace_event_unique_atomic779(%rip)
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1200:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1201
	movq	-8(%r13), %rdx
	testb	$1, %dl
	jne	.L1231
	xorl	%esi, %esi
	sarq	$32, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1232
	movq	(%rax), %rax
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
.L1204:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1207
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1207:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1233
.L1191:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1234
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1229:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1235
.L1197:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1198
	movq	(%rdi), %rax
	call	*8(%rax)
.L1198:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1199
	movq	(%rdi), %rax
	call	*8(%rax)
.L1199:
	leaq	.LC38(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1232:
	movq	312(%r12), %r13
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1227:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$425, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1192
	.p2align 4,,10
	.p2align 3
.L1231:
	leaq	.LC39(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1233:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1191
	.p2align 4,,10
	.p2align 3
.L1235:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC38(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1230:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1195
.L1234:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21482:
	.size	_ZN2v88internalL32Stats_Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateE, .-_ZN2v88internalL32Stats_Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Stats_Runtime_SetDataPropertiesEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC40:
	.string	"V8.Runtime_Runtime_SetDataProperties"
	.section	.text._ZN2v88internalL31Stats_Runtime_SetDataPropertiesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_SetDataPropertiesEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Stats_Runtime_SetDataPropertiesEiPmPNS0_7IsolateE:
.LFB21532:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1273
.L1237:
	movq	_ZZN2v88internalL31Stats_Runtime_SetDataPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1021(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1274
.L1239:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1275
.L1241:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1245
.L1246:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1274:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1276
.L1240:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_SetDataPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1021(%rip)
	jmp	.L1239
	.p2align 4,,10
	.p2align 3
.L1245:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1246
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	cmpq	%rax, 88(%r12)
	je	.L1250
	cmpq	%rax, 104(%r12)
	jne	.L1248
.L1250:
	movq	88(%r12), %r13
.L1249:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1253
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1253:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1277
.L1236:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1278
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1275:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1279
.L1242:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1243
	movq	(%rdi), %rax
	call	*8(%rax)
.L1243:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1244
	movq	(%rdi), %rax
	call	*8(%rax)
.L1244:
	leaq	.LC40(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1248:
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver23SetOrCopyDataPropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPKNS0_12ScopedVectorIS7_EEb@PLT
	testb	%al, %al
	jne	.L1250
	movq	312(%r12), %r13
	jmp	.L1249
	.p2align 4,,10
	.p2align 3
.L1273:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$452, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1277:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1276:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1279:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC40(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1242
.L1278:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21532:
	.size	_ZN2v88internalL31Stats_Runtime_SetDataPropertiesEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Stats_Runtime_SetDataPropertiesEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Stats_Runtime_CopyDataPropertiesEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC41:
	.string	"V8.Runtime_Runtime_CopyDataProperties"
	.section	.rodata._ZN2v88internalL32Stats_Runtime_CopyDataPropertiesEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC42:
	.string	"args[0].IsJSObject()"
	.section	.text._ZN2v88internalL32Stats_Runtime_CopyDataPropertiesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_CopyDataPropertiesEiPmPNS0_7IsolateE, @function
_ZN2v88internalL32Stats_Runtime_CopyDataPropertiesEiPmPNS0_7IsolateE:
.LFB21535:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1317
.L1281:
	movq	_ZZN2v88internalL32Stats_Runtime_CopyDataPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1037(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1318
.L1283:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1319
.L1285:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1289
.L1290:
	leaq	.LC42(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1318:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1320
.L1284:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_CopyDataPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1037(%rip)
	jmp	.L1283
	.p2align 4,,10
	.p2align 3
.L1289:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L1290
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	cmpq	%rax, 88(%r12)
	je	.L1294
	cmpq	%rax, 104(%r12)
	jne	.L1292
.L1294:
	movq	88(%r12), %r13
.L1293:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1297
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1297:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1321
.L1280:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1322
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1319:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1323
.L1286:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1287
	movq	(%rdi), %rax
	call	*8(%rax)
.L1287:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1288
	movq	(%rdi), %rax
	call	*8(%rax)
.L1288:
	leaq	.LC41(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1285
	.p2align 4,,10
	.p2align 3
.L1292:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver23SetOrCopyDataPropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPKNS0_12ScopedVectorIS7_EEb@PLT
	testb	%al, %al
	jne	.L1294
	movq	312(%r12), %r13
	jmp	.L1293
	.p2align 4,,10
	.p2align 3
.L1317:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$412, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1321:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1280
	.p2align 4,,10
	.p2align 3
.L1320:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1323:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC41(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1286
.L1322:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21535:
	.size	_ZN2v88internalL32Stats_Runtime_CopyDataPropertiesEiPmPNS0_7IsolateE, .-_ZN2v88internalL32Stats_Runtime_CopyDataPropertiesEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL22Stats_Runtime_ToNumberEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC43:
	.string	"V8.Runtime_Runtime_ToNumber"
	.section	.text._ZN2v88internalL22Stats_Runtime_ToNumberEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL22Stats_Runtime_ToNumberEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL22Stats_Runtime_ToNumberEiPmPNS0_7IsolateE.isra.0:
.LFB27259:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1362
.L1325:
	movq	_ZZN2v88internalL22Stats_Runtime_ToNumberEiPmPNS0_7IsolateEE29trace_event_unique_atomic1117(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1363
.L1327:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1364
.L1329:
	movl	41104(%r12), %eax
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	0(%r13), %r14
	testb	$1, %r14b
	jne	.L1333
.L1361:
	movq	%rbx, %rdx
.L1338:
	movq	%r15, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L1341
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1341:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1365
.L1324:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1366
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1333:
	.cfi_restore_state
	movq	-1(%r14), %rdx
	cmpw	$65, 11(%rdx)
	je	.L1361
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	jne	.L1337
	movl	41104(%r12), %eax
	movq	312(%r12), %r14
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L1338
	.p2align 4,,10
	.p2align 3
.L1364:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1367
.L1330:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1331
	movq	(%rdi), %rax
	call	*8(%rax)
.L1331:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1332
	movq	(%rdi), %rax
	call	*8(%rax)
.L1332:
	leaq	.LC43(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1363:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1368
.L1328:
	movq	%rbx, _ZZN2v88internalL22Stats_Runtime_ToNumberEiPmPNS0_7IsolateEE29trace_event_unique_atomic1117(%rip)
	jmp	.L1327
	.p2align 4,,10
	.p2align 3
.L1365:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1362:
	movq	40960(%rsi), %rax
	movl	$460, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1368:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1328
	.p2align 4,,10
	.p2align 3
.L1367:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC43(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1330
.L1366:
	call	__stack_chk_fail@PLT
.L1337:
	movq	(%rax), %r14
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L1338
	.cfi_endproc
.LFE27259:
	.size	_ZN2v88internalL22Stats_Runtime_ToNumberEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL22Stats_Runtime_ToNumberEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL23Stats_Runtime_ToNumericEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC44:
	.string	"V8.Runtime_Runtime_ToNumeric"
	.section	.text._ZN2v88internalL23Stats_Runtime_ToNumericEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL23Stats_Runtime_ToNumericEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL23Stats_Runtime_ToNumericEiPmPNS0_7IsolateE.isra.0:
.LFB27260:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1410
.L1370:
	movq	_ZZN2v88internalL23Stats_Runtime_ToNumericEiPmPNS0_7IsolateEE29trace_event_unique_atomic1124(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1411
.L1372:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1412
.L1374:
	movl	41104(%r12), %eax
	movq	41096(%r12), %rbx
	movq	41088(%r12), %r15
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	0(%r13), %r14
	movq	%rbx, %rdx
	testb	$1, %r14b
	jne	.L1413
.L1386:
	movq	%r15, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L1389
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1389:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1414
.L1369:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1415
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1413:
	.cfi_restore_state
	movq	-1(%r14), %rcx
	cmpw	$65, 11(%rcx)
	je	.L1386
	movq	-1(%r14), %rcx
	cmpw	$66, 11(%rcx)
	je	.L1386
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	jne	.L1408
	movl	41104(%r12), %eax
	movq	312(%r12), %r14
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L1386
	.p2align 4,,10
	.p2align 3
.L1412:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1416
.L1375:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1376
	movq	(%rdi), %rax
	call	*8(%rax)
.L1376:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1377
	movq	(%rdi), %rax
	call	*8(%rax)
.L1377:
	leaq	.LC44(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1411:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1417
.L1373:
	movq	%rbx, _ZZN2v88internalL23Stats_Runtime_ToNumericEiPmPNS0_7IsolateEE29trace_event_unique_atomic1124(%rip)
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1414:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1369
	.p2align 4,,10
	.p2align 3
.L1410:
	movq	40960(%rsi), %rax
	movl	$461, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1370
	.p2align 4,,10
	.p2align 3
.L1417:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1373
	.p2align 4,,10
	.p2align 3
.L1416:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC44(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1375
.L1415:
	call	__stack_chk_fail@PLT
.L1408:
	movq	(%rax), %r14
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L1386
	.cfi_endproc
.LFE27260:
	.size	_ZN2v88internalL23Stats_Runtime_ToNumericEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL23Stats_Runtime_ToNumericEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL22Stats_Runtime_ToLengthEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC45:
	.string	"V8.Runtime_Runtime_ToLength"
	.section	.text._ZN2v88internalL22Stats_Runtime_ToLengthEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL22Stats_Runtime_ToLengthEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL22Stats_Runtime_ToLengthEiPmPNS0_7IsolateE.isra.0:
.LFB27261:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1456
.L1419:
	movq	_ZZN2v88internalL22Stats_Runtime_ToLengthEiPmPNS0_7IsolateEE29trace_event_unique_atomic1131(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1457
.L1421:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1458
.L1423:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rsi
	testb	$1, %sil
	jne	.L1427
	sarq	$32, %rsi
	movl	$0, %eax
	movq	41112(%r12), %rdi
	cmovs	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L1428
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	testq	%rax, %rax
	je	.L1459
.L1431:
	movq	(%rax), %r13
.L1432:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1435
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1435:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1460
.L1418:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1461
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1428:
	.cfi_restore_state
	movq	%r14, %rax
	cmpq	%rbx, %r14
	je	.L1462
.L1430:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1431
	.p2align 4,,10
	.p2align 3
.L1427:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	jne	.L1431
.L1459:
	movq	312(%r12), %r13
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1458:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1463
.L1424:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1425
	movq	(%rdi), %rax
	call	*8(%rax)
.L1425:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1426
	movq	(%rdi), %rax
	call	*8(%rax)
.L1426:
	leaq	.LC45(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1423
	.p2align 4,,10
	.p2align 3
.L1457:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1464
.L1422:
	movq	%rbx, _ZZN2v88internalL22Stats_Runtime_ToLengthEiPmPNS0_7IsolateEE29trace_event_unique_atomic1131(%rip)
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1456:
	movq	40960(%rsi), %rax
	movl	$458, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1419
	.p2align 4,,10
	.p2align 3
.L1460:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1418
	.p2align 4,,10
	.p2align 3
.L1464:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1422
	.p2align 4,,10
	.p2align 3
.L1463:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC45(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L1462:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	jmp	.L1430
.L1461:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27261:
	.size	_ZN2v88internalL22Stats_Runtime_ToLengthEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL22Stats_Runtime_ToLengthEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL24Stats_Runtime_ToStringRTEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC46:
	.string	"V8.Runtime_Runtime_ToStringRT"
	.section	.text._ZN2v88internalL24Stats_Runtime_ToStringRTEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Stats_Runtime_ToStringRTEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL24Stats_Runtime_ToStringRTEiPmPNS0_7IsolateE.isra.0:
.LFB27262:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1501
.L1466:
	movq	_ZZN2v88internalL24Stats_Runtime_ToStringRTEiPmPNS0_7IsolateEE29trace_event_unique_atomic1138(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1502
.L1468:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1503
.L1470:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1474
.L1477:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1504
.L1476:
	movq	0(%r13), %r13
.L1478:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1481
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1481:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1505
.L1465:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1506
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1474:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1477
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1503:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1507
.L1471:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1472
	movq	(%rdi), %rax
	call	*8(%rax)
.L1472:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1473
	movq	(%rdi), %rax
	call	*8(%rax)
.L1473:
	leaq	.LC46(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1470
	.p2align 4,,10
	.p2align 3
.L1502:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1508
.L1469:
	movq	%rbx, _ZZN2v88internalL24Stats_Runtime_ToStringRTEiPmPNS0_7IsolateEE29trace_event_unique_atomic1138(%rip)
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1504:
	movq	312(%r12), %r13
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1505:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1501:
	movq	40960(%rsi), %rax
	movl	$463, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1508:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1507:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC46(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1471
.L1506:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27262:
	.size	_ZN2v88internalL24Stats_Runtime_ToStringRTEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL24Stats_Runtime_ToStringRTEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL32Stats_Runtime_CreateDataPropertyEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC47:
	.string	"V8.Runtime_Runtime_CreateDataProperty"
	.section	.text._ZN2v88internalL32Stats_Runtime_CreateDataPropertyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_CreateDataPropertyEiPmPNS0_7IsolateE, @function
_ZN2v88internalL32Stats_Runtime_CreateDataPropertyEiPmPNS0_7IsolateE:
.LFB21568:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -208(%rbp)
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1546
.L1510:
	movq	_ZZN2v88internalL32Stats_Runtime_CreateDataPropertyEiPmPNS0_7IsolateEE29trace_event_unique_atomic1174(%rip), %r13
	testq	%r13, %r13
	je	.L1547
.L1512:
	movq	$0, -240(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L1548
.L1514:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L1518
.L1519:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1547:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1549
.L1513:
	movq	%r13, _ZZN2v88internalL32Stats_Runtime_CreateDataPropertyEiPmPNS0_7IsolateEE29trace_event_unique_atomic1174(%rip)
	jmp	.L1512
	.p2align 4,,10
	.p2align 3
.L1518:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1519
	leaq	-8(%rbx), %rcx
	movl	$1, %r9d
	movq	%rbx, %rdx
	movq	%r12, %rsi
	leaq	-160(%rbp), %r15
	leaq	-241(%rbp), %r8
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	cmpb	$0, -241(%rbp)
	je	.L1545
	leaq	-16(%rbx), %rsi
	movl	$1, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L1545
	movq	-16(%rbx), %r15
.L1521:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1525
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1525:
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1550
.L1509:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1551
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1545:
	.cfi_restore_state
	movq	312(%r12), %r15
	jmp	.L1521
	.p2align 4,,10
	.p2align 3
.L1548:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1552
.L1515:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1516
	movq	(%rdi), %rax
	call	*8(%rax)
.L1516:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1517
	movq	(%rdi), %rax
	call	*8(%rax)
.L1517:
	leaq	.LC47(%rip), %rax
	movq	%r13, -232(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%r14, -216(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L1514
	.p2align 4,,10
	.p2align 3
.L1546:
	movq	40960(%rdx), %rax
	leaq	-200(%rbp), %rsi
	movl	$414, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1510
	.p2align 4,,10
	.p2align 3
.L1550:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1549:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1513
	.p2align 4,,10
	.p2align 3
.L1552:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC47(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1515
.L1551:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21568:
	.size	_ZN2v88internalL32Stats_Runtime_CreateDataPropertyEiPmPNS0_7IsolateE, .-_ZN2v88internalL32Stats_Runtime_CreateDataPropertyEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL38Stats_Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC48:
	.string	"V8.Runtime_Runtime_GetOwnPropertyDescriptor"
	.section	.rodata._ZN2v88internalL38Stats_Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC49:
	.string	"args[1].IsName()"
	.section	.text._ZN2v88internalL38Stats_Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Stats_Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL38Stats_Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateE:
.LFB21571:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -176(%rbp)
	movq	$0, -144(%rbp)
	movaps	%xmm0, -160(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1591
.L1554:
	movq	_ZZN2v88internalL38Stats_Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1189(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1592
.L1556:
	movq	$0, -208(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1593
.L1558:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1562
.L1563:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1592:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1594
.L1557:
	movq	%rbx, _ZZN2v88internalL38Stats_Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1189(%rip)
	jmp	.L1556
	.p2align 4,,10
	.p2align 3
.L1562:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1563
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	testb	$1, %al
	jne	.L1595
.L1564:
	leaq	.LC49(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1593:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1596
.L1559:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1560
	movq	(%rdi), %rax
	call	*8(%rax)
.L1560:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1561
	movq	(%rdi), %rax
	call	*8(%rax)
.L1561:
	leaq	.LC48(%rip), %rax
	movq	%rbx, -200(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-200(%rbp), %rax
	movq	%r14, -184(%rbp)
	movq	%rax, -208(%rbp)
	jmp	.L1558
	.p2align 4,,10
	.p2align 3
.L1595:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L1564
	leaq	-128(%rbp), %r15
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r15, %rcx
	andb	$-64, -128(%rbp)
	movups	%xmm0, -120(%rbp)
	movups	%xmm0, -104(%rbp)
	call	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorE@PLT
	testb	%al, %al
	je	.L1597
	shrw	$8, %ax
	jne	.L1568
	movq	88(%r12), %r13
.L1567:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1571
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1571:
	leaq	-208(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1598
.L1553:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1599
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1568:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18PropertyDescriptor26ToPropertyDescriptorObjectEPNS0_7IsolateE@PLT
	movq	(%rax), %r13
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1597:
	movq	312(%r12), %r13
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1591:
	movq	40960(%rdx), %rax
	leaq	-168(%rbp), %rsi
	movl	$424, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -176(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1598:
	leaq	-168(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1553
	.p2align 4,,10
	.p2align 3
.L1596:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC48(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1594:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1557
.L1599:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21571:
	.size	_ZN2v88internalL38Stats_Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateE, .-_ZN2v88internalL38Stats_Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Stats_Runtime_HasInPrototypeChainEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC50:
	.string	"V8.Runtime_Runtime_HasInPrototypeChain"
	.section	.text._ZN2v88internalL33Stats_Runtime_HasInPrototypeChainEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_HasInPrototypeChainEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_HasInPrototypeChainEiPmPNS0_7IsolateE.isra.0:
.LFB27263:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1640
.L1601:
	movq	_ZZN2v88internalL33Stats_Runtime_HasInPrototypeChainEiPmPNS0_7IsolateEE29trace_event_unique_atomic1152(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1641
.L1603:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1642
.L1605:
	movl	41104(%r12), %edx
	movq	41088(%r12), %r14
	leaq	-8(%r13), %r8
	movq	41096(%r12), %rbx
	leal	1(%rdx), %eax
	movl	%eax, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1609
.L1639:
	movq	%rbx, %rcx
.L1615:
	movq	120(%r12), %r13
.L1614:
	movq	%r14, 41088(%r12)
	movl	%edx, 41104(%r12)
	cmpq	%rcx, %rbx
	je	.L1618
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1618:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1643
.L1600:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1644
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1609:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1639
	movq	%r8, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver19HasInPrototypeChainEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L1612
	movl	41104(%r12), %eax
	movq	312(%r12), %r13
	movq	41096(%r12), %rcx
	leal	-1(%rax), %edx
	jmp	.L1614
	.p2align 4,,10
	.p2align 3
.L1642:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1645
.L1606:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1607
	movq	(%rdi), %rax
	call	*8(%rax)
.L1607:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1608
	movq	(%rdi), %rax
	call	*8(%rax)
.L1608:
	leaq	.LC50(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1605
	.p2align 4,,10
	.p2align 3
.L1641:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1646
.L1604:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_HasInPrototypeChainEiPmPNS0_7IsolateEE29trace_event_unique_atomic1152(%rip)
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1643:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1600
	.p2align 4,,10
	.p2align 3
.L1640:
	movq	40960(%rsi), %rax
	movl	$428, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1612:
	movl	41104(%r12), %ecx
	shrw	$8, %ax
	leal	-1(%rcx), %edx
	movq	41096(%r12), %rcx
	je	.L1615
	movq	112(%r12), %r13
	jmp	.L1614
	.p2align 4,,10
	.p2align 3
.L1646:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1645:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC50(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1606
.L1644:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27263:
	.size	_ZN2v88internalL33Stats_Runtime_HasInPrototypeChainEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_HasInPrototypeChainEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL23Stats_Runtime_NewObjectEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC51:
	.string	"V8.Runtime_Runtime_NewObject"
.LC52:
	.string	"args[1].IsJSReceiver()"
	.section	.text._ZN2v88internalL23Stats_Runtime_NewObjectEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL23Stats_Runtime_NewObjectEiPmPNS0_7IsolateE, @function
_ZN2v88internalL23Stats_Runtime_NewObjectEiPmPNS0_7IsolateE:
.LFB21491:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1684
.L1648:
	movq	_ZZN2v88internalL23Stats_Runtime_NewObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic812(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1685
.L1650:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1686
.L1652:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1656
.L1657:
	leaq	.LC8(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1685:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1687
.L1651:
	movq	%rbx, _ZZN2v88internalL23Stats_Runtime_NewObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic812(%rip)
	jmp	.L1650
	.p2align 4,,10
	.p2align 3
.L1656:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L1657
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rsi
	testb	$1, %al
	jne	.L1688
.L1658:
	leaq	.LC52(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1686:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1689
.L1653:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1654
	movq	(%rdi), %rax
	call	*8(%rax)
.L1654:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1655
	movq	(%rdi), %rax
	call	*8(%rax)
.L1655:
	leaq	.LC51(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1652
	.p2align 4,,10
	.p2align 3
.L1688:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1658
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEENS2_INS0_14AllocationSiteEEE@PLT
	testq	%rax, %rax
	je	.L1690
	movq	(%rax), %r13
.L1661:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1664
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1664:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1691
.L1647:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1692
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1690:
	.cfi_restore_state
	movq	312(%r12), %r13
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1684:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$439, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1648
	.p2align 4,,10
	.p2align 3
.L1691:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1647
	.p2align 4,,10
	.p2align 3
.L1689:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC51(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1687:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1651
.L1692:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21491:
	.size	_ZN2v88internalL23Stats_Runtime_NewObjectEiPmPNS0_7IsolateE, .-_ZN2v88internalL23Stats_Runtime_NewObjectEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL24Stats_Runtime_ObjectKeysEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC53:
	.string	"V8.Runtime_Runtime_ObjectKeys"
	.section	.text._ZN2v88internalL24Stats_Runtime_ObjectKeysEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Stats_Runtime_ObjectKeysEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL24Stats_Runtime_ObjectKeysEiPmPNS0_7IsolateE.isra.0:
.LFB27290:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1731
.L1694:
	movq	_ZZN2v88internalL24Stats_Runtime_ObjectKeysEiPmPNS0_7IsolateEE28trace_event_unique_atomic188(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1732
.L1696:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1733
.L1698:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1702
.L1705:
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1730
.L1704:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	$18, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	testq	%rax, %rax
	je	.L1730
	movq	(%rax), %r13
.L1706:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1710
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1710:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1734
.L1693:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1735
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1702:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1705
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1733:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1736
.L1699:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1700
	movq	(%rdi), %rax
	call	*8(%rax)
.L1700:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1701
	movq	(%rdi), %rax
	call	*8(%rax)
.L1701:
	leaq	.LC53(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1698
	.p2align 4,,10
	.p2align 3
.L1732:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1737
.L1697:
	movq	%rbx, _ZZN2v88internalL24Stats_Runtime_ObjectKeysEiPmPNS0_7IsolateEE28trace_event_unique_atomic188(%rip)
	jmp	.L1696
	.p2align 4,,10
	.p2align 3
.L1730:
	movq	312(%r12), %r13
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1731:
	movq	40960(%rsi), %rax
	movl	$447, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1694
	.p2align 4,,10
	.p2align 3
.L1734:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1693
	.p2align 4,,10
	.p2align 3
.L1737:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1697
	.p2align 4,,10
	.p2align 3
.L1736:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC53(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1699
.L1735:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27290:
	.size	_ZN2v88internalL24Stats_Runtime_ObjectKeysEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL24Stats_Runtime_ObjectKeysEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL39Stats_Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC54:
	.string	"V8.Runtime_Runtime_ObjectGetOwnPropertyNames"
	.section	.text._ZN2v88internalL39Stats_Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Stats_Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL39Stats_Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateE.isra.0:
.LFB27291:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1776
.L1739:
	movq	_ZZN2v88internalL39Stats_Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateEE28trace_event_unique_atomic208(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1777
.L1741:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1778
.L1743:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1747
.L1750:
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1775
.L1749:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	$16, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	testq	%rax, %rax
	je	.L1775
	movq	(%rax), %r13
.L1751:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1755
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1755:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1779
.L1738:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1780
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1747:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1750
	jmp	.L1749
	.p2align 4,,10
	.p2align 3
.L1778:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1781
.L1744:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1745
	movq	(%rdi), %rax
	call	*8(%rax)
.L1745:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1746
	movq	(%rdi), %rax
	call	*8(%rax)
.L1746:
	leaq	.LC54(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1743
	.p2align 4,,10
	.p2align 3
.L1777:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1782
.L1742:
	movq	%rbx, _ZZN2v88internalL39Stats_Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateEE28trace_event_unique_atomic208(%rip)
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1775:
	movq	312(%r12), %r13
	jmp	.L1751
	.p2align 4,,10
	.p2align 3
.L1776:
	movq	40960(%rsi), %rax
	movl	$443, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1739
	.p2align 4,,10
	.p2align 3
.L1779:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1738
	.p2align 4,,10
	.p2align 3
.L1782:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1742
	.p2align 4,,10
	.p2align 3
.L1781:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC54(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1744
.L1780:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27291:
	.size	_ZN2v88internalL39Stats_Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL39Stats_Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL46Stats_Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC55:
	.string	"V8.Runtime_Runtime_ObjectGetOwnPropertyNamesTryFast"
	.section	.text._ZN2v88internalL46Stats_Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Stats_Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL46Stats_Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateE.isra.0:
.LFB27292:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1830
.L1784:
	movq	_ZZN2v88internalL46Stats_Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateEE28trace_event_unique_atomic229(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1831
.L1786:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1832
.L1788:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1792
.L1795:
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1801
.L1794:
	movq	0(%r13), %rax
	movq	-1(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1797
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1798:
	movl	15(%rsi), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	movl	%edx, %r15d
	je	.L1803
	movq	(%rax), %rax
	leaq	-168(%rbp), %rdi
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal3Map28NumberOfEnumerablePropertiesEv@PLT
	cmpl	%eax, %r15d
	je	.L1833
.L1803:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$16, %edx
.L1829:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	testq	%rax, %rax
	je	.L1801
	movq	(%rax), %r13
.L1796:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1806
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1806:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1834
.L1783:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1835
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1797:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1836
.L1799:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L1792:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1795
	jmp	.L1794
	.p2align 4,,10
	.p2align 3
.L1832:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1837
.L1789:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1790
	movq	(%rdi), %rax
	call	*8(%rax)
.L1790:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1791
	movq	(%rdi), %rax
	call	*8(%rax)
.L1791:
	leaq	.LC55(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1788
	.p2align 4,,10
	.p2align 3
.L1831:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1838
.L1787:
	movq	%rbx, _ZZN2v88internalL46Stats_Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateEE28trace_event_unique_atomic229(%rip)
	jmp	.L1786
	.p2align 4,,10
	.p2align 3
.L1801:
	movq	312(%r12), %r13
	jmp	.L1796
	.p2align 4,,10
	.p2align 3
.L1834:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1783
	.p2align 4,,10
	.p2align 3
.L1830:
	movq	40960(%rsi), %rax
	movl	$444, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1784
	.p2align 4,,10
	.p2align 3
.L1833:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$18, %edx
	jmp	.L1829
	.p2align 4,,10
	.p2align 3
.L1836:
	movq	%r12, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	jmp	.L1799
	.p2align 4,,10
	.p2align 3
.L1838:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1787
	.p2align 4,,10
	.p2align 3
.L1837:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC55(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1789
.L1835:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27292:
	.size	_ZN2v88internalL46Stats_Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL46Stats_Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL25Stats_Runtime_HasPropertyEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC56:
	.string	"V8.Runtime_Runtime_HasProperty"
	.section	.text._ZN2v88internalL25Stats_Runtime_HasPropertyEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Stats_Runtime_HasPropertyEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL25Stats_Runtime_HasPropertyEiPmPNS0_7IsolateE.isra.0:
.LFB27293:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -288(%rbp)
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1898
.L1840:
	movq	_ZZN2v88internalL25Stats_Runtime_HasPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic754(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1899
.L1842:
	movq	$0, -320(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1900
.L1844:
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	leaq	-8(%r13), %rdx
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	testb	$1, %al
	jne	.L1848
.L1850:
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$65, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L1849:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1866
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1866:
	leaq	-320(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1901
.L1839:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1902
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1848:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$1023, 11(%rcx)
	jbe	.L1850
	movq	-8(%r13), %rcx
	testb	$1, %cl
	jne	.L1851
.L1854:
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object13ConvertToNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1897
	movq	0(%r13), %rax
.L1853:
	andq	$-262144, %rax
	movq	24(%rax), %r15
	movq	(%rdx), %rax
	movq	-1(%rax), %rcx
	subq	$37592, %r15
	cmpw	$63, 11(%rcx)
	jbe	.L1903
.L1856:
	movq	(%rdx), %rcx
	movl	$3, %eax
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	je	.L1904
.L1860:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	%r15, -216(%rbp)
	movq	(%rdx), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L1905
.L1861:
	movq	%r13, -192(%rbp)
	movq	%r13, -176(%rbp)
	leaq	-240(%rbp), %r13
	movq	%r13, %rdi
	movq	%rdx, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -184(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L1859:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	je	.L1897
	shrw	$8, %ax
	je	.L1863
	movq	112(%r12), %r13
	jmp	.L1849
	.p2align 4,,10
	.p2align 3
.L1900:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1906
.L1845:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1846
	movq	(%rdi), %rax
	call	*8(%rax)
.L1846:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1847
	movq	(%rdi), %rax
	call	*8(%rax)
.L1847:
	leaq	.LC56(%rip), %rax
	movq	%rbx, -312(%rbp)
	movq	%rax, -304(%rbp)
	leaq	-312(%rbp), %rax
	movq	%r14, -296(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L1899:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1907
.L1843:
	movq	%rbx, _ZZN2v88internalL25Stats_Runtime_HasPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic754(%rip)
	jmp	.L1842
	.p2align 4,,10
	.p2align 3
.L1901:
	leaq	-280(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1839
	.p2align 4,,10
	.p2align 3
.L1898:
	movq	40960(%rsi), %rax
	movl	$429, %edx
	leaq	-280(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1840
	.p2align 4,,10
	.p2align 3
.L1897:
	movq	312(%r12), %r13
	jmp	.L1849
	.p2align 4,,10
	.p2align 3
.L1907:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1843
	.p2align 4,,10
	.p2align 3
.L1906:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC56(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L1851:
	movq	-1(%rcx), %rcx
	cmpw	$64, 11(%rcx)
	ja	.L1854
	jmp	.L1853
	.p2align 4,,10
	.p2align 3
.L1863:
	movq	120(%r12), %r13
	jmp	.L1849
	.p2align 4,,10
	.p2align 3
.L1904:
	movl	11(%rcx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L1903:
	movq	%rax, -160(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L1857
	testb	$2, %al
	jne	.L1856
.L1857:
	leaq	-160(%rbp), %rdi
	leaq	-324(%rbp), %rsi
	movq	%rdx, -344(%rbp)
	movq	%rdi, -352(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-344(%rbp), %rdx
	testb	%al, %al
	je	.L1856
	movabsq	$824633720832, %rax
	movq	-352(%rbp), %rdi
	movq	%r13, -112(%rbp)
	movq	%rax, -148(%rbp)
	movl	-324(%rbp), %eax
	movq	%r13, -96(%rbp)
	leaq	-240(%rbp), %r13
	movl	$3, -160(%rbp)
	movq	%r15, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -104(%rbp)
	movl	%eax, -88(%rbp)
	movl	$-1, -84(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	movq	-344(%rbp), %rdx
	movdqa	-160(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm2
	movaps	%xmm4, -192(%rbp)
	movq	%rdx, -128(%rbp)
	movdqa	-128(%rbp), %xmm3
	movaps	%xmm1, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm5, -176(%rbp)
	jmp	.L1859
	.p2align 4,,10
	.p2align 3
.L1905:
	movq	%rdx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rdx
	jmp	.L1861
.L1902:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27293:
	.size	_ZN2v88internalL25Stats_Runtime_HasPropertyEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL25Stats_Runtime_HasPropertyEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL20Stats_Runtime_ToNameEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC57:
	.string	"V8.Runtime_Runtime_ToName"
	.section	.text._ZN2v88internalL20Stats_Runtime_ToNameEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL20Stats_Runtime_ToNameEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL20Stats_Runtime_ToNameEiPmPNS0_7IsolateE.isra.0:
.LFB27294:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1944
.L1909:
	movq	_ZZN2v88internalL20Stats_Runtime_ToNameEiPmPNS0_7IsolateEE29trace_event_unique_atomic1145(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1945
.L1911:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1946
.L1913:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1917
.L1920:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object13ConvertToNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1947
.L1919:
	movq	0(%r13), %r13
.L1921:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1924
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1924:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1948
.L1908:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1949
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1917:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L1920
	jmp	.L1919
	.p2align 4,,10
	.p2align 3
.L1946:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1950
.L1914:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1915
	movq	(%rdi), %rax
	call	*8(%rax)
.L1915:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1916
	movq	(%rdi), %rax
	call	*8(%rax)
.L1916:
	leaq	.LC57(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1913
	.p2align 4,,10
	.p2align 3
.L1945:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1951
.L1912:
	movq	%rbx, _ZZN2v88internalL20Stats_Runtime_ToNameEiPmPNS0_7IsolateEE29trace_event_unique_atomic1145(%rip)
	jmp	.L1911
	.p2align 4,,10
	.p2align 3
.L1947:
	movq	312(%r12), %r13
	jmp	.L1921
	.p2align 4,,10
	.p2align 3
.L1948:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1908
	.p2align 4,,10
	.p2align 3
.L1944:
	movq	40960(%rsi), %rax
	movl	$459, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1909
	.p2align 4,,10
	.p2align 3
.L1951:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1912
	.p2align 4,,10
	.p2align 3
.L1950:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC57(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1914
.L1949:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27294:
	.size	_ZN2v88internalL20Stats_Runtime_ToNameEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL20Stats_Runtime_ToNameEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL32Stats_Runtime_CollectTypeProfileEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC58:
	.string	"V8.Runtime_Runtime_CollectTypeProfile"
	.section	.rodata._ZN2v88internalL32Stats_Runtime_CollectTypeProfileEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC59:
	.string	"args[0].IsSmi()"
.LC60:
	.string	"args[2].IsFeedbackVector()"
	.section	.text._ZN2v88internalL32Stats_Runtime_CollectTypeProfileEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_CollectTypeProfileEiPmPNS0_7IsolateE, @function
_ZN2v88internalL32Stats_Runtime_CollectTypeProfileEiPmPNS0_7IsolateE:
.LFB21514:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1993
.L1953:
	movq	_ZZN2v88internalL32Stats_Runtime_CollectTypeProfileEiPmPNS0_7IsolateEE28trace_event_unique_atomic944(%rip), %r13
	testq	%r13, %r13
	je	.L1994
.L1955:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L1995
.L1957:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	testb	$1, (%rbx)
	jne	.L1996
	movq	-16(%rbx), %rax
	leaq	-8(%rbx), %r9
	leaq	-16(%rbx), %r15
	testb	$1, %al
	jne	.L1997
.L1962:
	leaq	.LC60(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1994:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1998
.L1956:
	movq	%r13, _ZZN2v88internalL32Stats_Runtime_CollectTypeProfileEiPmPNS0_7IsolateEE28trace_event_unique_atomic944(%rip)
	jmp	.L1955
	.p2align 4,,10
	.p2align 3
.L1995:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1999
.L1958:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1959
	movq	(%rdi), %rax
	call	*8(%rax)
.L1959:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1960
	movq	(%rdi), %rax
	call	*8(%rax)
.L1960:
	leaq	.LC58(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1957
	.p2align 4,,10
	.p2align 3
.L1997:
	movq	-1(%rax), %rax
	cmpw	$155, 11(%rax)
	jne	.L1962
	movq	%r9, %rsi
	movq	%r12, %rdi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal6Object6TypeOfEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-216(%rbp), %r9
	movq	%rax, %r8
	movq	-8(%rbx), %rax
	testb	$1, %al
	jne	.L2000
.L1965:
	cmpq	104(%r12), %rax
	je	.L2001
.L1967:
	movq	-16(%rbx), %rax
	leaq	-208(%rbp), %rdi
	movq	%r8, -216(%rbp)
	movq	%rax, -208(%rbp)
	call	_ZNK2v88internal14FeedbackVector18GetTypeProfileSlotEv@PLT
	leaq	-200(%rbp), %rdi
	movq	%r15, -192(%rbp)
	movl	%eax, %esi
	movl	%eax, -176(%rbp)
	movq	-16(%rbx), %rax
	movq	$0, -184(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdx
	movq	-216(%rbp), %r8
	leaq	-192(%rbp), %rdi
	movl	%eax, -172(%rbp)
	sarq	$32, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal13FeedbackNexus7CollectENS0_6HandleINS0_6StringEEEi@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r15
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L1971
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1971:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2002
.L1952:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2003
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2000:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L1965
	movq	%r9, %rdi
	call	_ZN2v88internal10JSReceiver18GetConstructorNameENS0_6HandleIS1_EE@PLT
	movq	%rax, %r8
	jmp	.L1967
	.p2align 4,,10
	.p2align 3
.L2001:
	movq	41112(%r12), %rdi
	movq	2960(%r12), %rsi
	testq	%rdi, %rdi
	je	.L1968
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
	jmp	.L1967
	.p2align 4,,10
	.p2align 3
.L1968:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L2004
.L1970:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L1967
	.p2align 4,,10
	.p2align 3
.L1993:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$410, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1953
	.p2align 4,,10
	.p2align 3
.L1996:
	leaq	.LC59(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2002:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1952
	.p2align 4,,10
	.p2align 3
.L1999:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC58(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1958
	.p2align 4,,10
	.p2align 3
.L1998:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1956
	.p2align 4,,10
	.p2align 3
.L2004:
	movq	%r12, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1970
.L2003:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21514:
	.size	_ZN2v88internalL32Stats_Runtime_CollectTypeProfileEiPmPNS0_7IsolateE, .-_ZN2v88internalL32Stats_Runtime_CollectTypeProfileEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL27Stats_Runtime_GetDerivedMapEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC61:
	.string	"V8.Runtime_Runtime_GetDerivedMap"
	.section	.text._ZN2v88internalL27Stats_Runtime_GetDerivedMapEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_GetDerivedMapEiPmPNS0_7IsolateE, @function
_ZN2v88internalL27Stats_Runtime_GetDerivedMapEiPmPNS0_7IsolateE:
.LFB21494:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2044
.L2006:
	movq	_ZZN2v88internalL27Stats_Runtime_GetDerivedMapEiPmPNS0_7IsolateEE28trace_event_unique_atomic822(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2045
.L2008:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2046
.L2010:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2047
.L2014:
	leaq	.LC8(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2045:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2048
.L2009:
	movq	%rbx, _ZZN2v88internalL27Stats_Runtime_GetDerivedMapEiPmPNS0_7IsolateEE28trace_event_unique_atomic822(%rip)
	jmp	.L2008
	.p2align 4,,10
	.p2align 3
.L2047:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L2014
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	testb	$1, %al
	jne	.L2049
.L2015:
	leaq	.LC52(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2046:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2050
.L2011:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2012
	movq	(%rdi), %rax
	call	*8(%rax)
.L2012:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2013
	movq	(%rdi), %rax
	call	*8(%rax)
.L2013:
	leaq	.LC61(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2049:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2015
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	testq	%rax, %rax
	je	.L2051
	movq	(%rax), %r13
.L2019:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2022
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2022:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2052
.L2005:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2053
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2051:
	.cfi_restore_state
	movq	312(%r12), %r13
	jmp	.L2019
	.p2align 4,,10
	.p2align 3
.L2044:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$422, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2006
	.p2align 4,,10
	.p2align 3
.L2052:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2005
	.p2align 4,,10
	.p2align 3
.L2050:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC61(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2011
	.p2align 4,,10
	.p2align 3
.L2048:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2009
.L2053:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21494:
	.size	_ZN2v88internalL27Stats_Runtime_GetDerivedMapEiPmPNS0_7IsolateE, .-_ZN2v88internalL27Stats_Runtime_GetDerivedMapEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Stats_Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC62:
	.string	"V8.Runtime_Runtime_DefineGetterPropertyUnchecked"
	.section	.rodata._ZN2v88internalL43Stats_Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC63:
	.string	"args[2].IsJSFunction()"
.LC64:
	.string	"args[3].IsSmi()"
	.section	.rodata._ZN2v88internalL43Stats_Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateE.isra.0.str1.8
	.align 8
.LC65:
	.string	"args.smi_at(3) & ~(READ_ONLY | DONT_ENUM | DONT_DELETE) == 0"
	.section	.rodata._ZN2v88internalL43Stats_Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateE.isra.0.str1.1
.LC66:
	.string	"*getter_map == getter->map()"
	.section	.text._ZN2v88internalL43Stats_Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Stats_Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL43Stats_Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateE.isra.0:
.LFB27303:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2110
.L2055:
	movq	_ZZN2v88internalL43Stats_Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateEE28trace_event_unique_atomic998(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2111
.L2057:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2112
.L2059:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2113
.L2063:
	leaq	.LC42(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2111:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2114
.L2058:
	movq	%rbx, _ZZN2v88internalL43Stats_Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateEE28trace_event_unique_atomic998(%rip)
	jmp	.L2057
	.p2align 4,,10
	.p2align 3
.L2113:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L2063
	movq	-8(%r13), %rax
	leaq	-8(%r13), %r9
	testb	$1, %al
	jne	.L2115
.L2064:
	leaq	.LC49(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2112:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2116
.L2060:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2061
	movq	(%rdi), %rax
	call	*8(%rax)
.L2061:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2062
	movq	(%rdi), %rax
	call	*8(%rax)
.L2062:
	leaq	.LC62(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2059
	.p2align 4,,10
	.p2align 3
.L2115:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L2064
	movq	-16(%r13), %rax
	leaq	-16(%r13), %r10
	testb	$1, %al
	jne	.L2117
.L2066:
	leaq	.LC63(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2117:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L2066
	movq	-24(%r13), %r8
	testb	$1, %r8b
	jne	.L2118
	sarq	$32, %r8
	testl	$-8, %r8d
	jne	.L2119
	movq	23(%rax), %r15
	movq	15(%r15), %rax
	testb	$1, %al
	jne	.L2120
.L2071:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L2072:
	testb	%al, %al
	je	.L2076
	movq	15(%r15), %rdx
	testb	$1, %dl
	jne	.L2121
.L2074:
	movl	11(%rdx), %eax
	testl	%eax, %eax
	jne	.L2077
	movq	-16(%r13), %rax
	movq	-1(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2078
	movq	%r8, -200(%rbp)
	movq	%r10, -192(%rbp)
	movq	%r9, -184(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %r10
	movq	-200(%rbp), %r8
	movq	%rax, %r15
.L2079:
	movq	%r9, %rsi
	movq	%r10, %rdi
	leaq	2608(%r12), %rdx
	movq	%r8, -200(%rbp)
	movq	%r9, -192(%rbp)
	movq	%r10, -184(%rbp)
	call	_ZN2v88internal10JSFunction7SetNameENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6StringEEE@PLT
	movq	-184(%rbp), %r10
	movq	-192(%rbp), %r9
	testb	%al, %al
	movq	-200(%rbp), %r8
	je	.L2109
	movq	-16(%r13), %rax
	movq	-1(%rax), %rax
	cmpq	(%r15), %rax
	jne	.L2122
.L2077:
	leaq	104(%r12), %rcx
	movq	%r10, %rdx
	movq	%r9, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject14DefineAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEES7_NS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	je	.L2109
	movq	88(%r12), %r13
.L2082:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2086
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2086:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2123
.L2054:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2124
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2109:
	.cfi_restore_state
	movq	312(%r12), %r13
	jmp	.L2082
	.p2align 4,,10
	.p2align 3
.L2076:
	andq	$-262144, %r15
	movq	24(%r15), %rax
	movq	-37464(%rax), %rdx
	jmp	.L2074
	.p2align 4,,10
	.p2align 3
.L2078:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L2125
.L2080:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L2079
	.p2align 4,,10
	.p2align 3
.L2120:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L2071
	leaq	-168(%rbp), %rdi
	movq	%r8, -200(%rbp)
	movq	%r10, -192(%rbp)
	movq	%r9, -184(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %r10
	movq	-200(%rbp), %r8
	jmp	.L2072
	.p2align 4,,10
	.p2align 3
.L2121:
	movq	-1(%rdx), %rax
	cmpw	$136, 11(%rax)
	jne	.L2074
	leaq	-168(%rbp), %rdi
	movq	%r8, -200(%rbp)
	movq	%r10, -192(%rbp)
	movq	%r9, -184(%rbp)
	movq	%rdx, -168(%rbp)
	movq	%rdx, -216(%rbp)
	movq	%rdi, -208(%rbp)
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %r10
	testb	%al, %al
	movq	-200(%rbp), %r8
	je	.L2076
	movq	-216(%rbp), %rdx
	movq	-208(%rbp), %rdi
	movq	%rdx, -168(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %r10
	movq	-200(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L2074
	.p2align 4,,10
	.p2align 3
.L2110:
	movq	40960(%rsi), %rax
	movl	$419, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2055
	.p2align 4,,10
	.p2align 3
.L2118:
	leaq	.LC64(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2116:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC62(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2060
	.p2align 4,,10
	.p2align 3
.L2114:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2058
	.p2align 4,,10
	.p2align 3
.L2119:
	leaq	.LC65(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2123:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2054
	.p2align 4,,10
	.p2align 3
.L2122:
	leaq	.LC66(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2125:
	movq	%r12, %rdi
	movq	%rsi, -208(%rbp)
	movq	%r8, -200(%rbp)
	movq	%r10, -192(%rbp)
	movq	%r9, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %rsi
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r10
	movq	-184(%rbp), %r9
	movq	%rax, %r15
	jmp	.L2080
.L2124:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27303:
	.size	_ZN2v88internalL43Stats_Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL43Stats_Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL43Stats_Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC67:
	.string	"V8.Runtime_Runtime_DefineSetterPropertyUnchecked"
	.section	.rodata._ZN2v88internalL43Stats_Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC68:
	.string	"*setter_map == setter->map()"
	.section	.text._ZN2v88internalL43Stats_Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Stats_Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL43Stats_Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateE.isra.0:
.LFB27304:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2182
.L2127:
	movq	_ZZN2v88internalL43Stats_Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateEE29trace_event_unique_atomic1088(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2183
.L2129:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2184
.L2131:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2185
.L2135:
	leaq	.LC42(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2183:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2186
.L2130:
	movq	%rbx, _ZZN2v88internalL43Stats_Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateEE29trace_event_unique_atomic1088(%rip)
	jmp	.L2129
	.p2align 4,,10
	.p2align 3
.L2185:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L2135
	movq	-8(%r13), %rax
	leaq	-8(%r13), %r9
	testb	$1, %al
	jne	.L2187
.L2136:
	leaq	.LC49(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2184:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2188
.L2132:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2133
	movq	(%rdi), %rax
	call	*8(%rax)
.L2133:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2134
	movq	(%rdi), %rax
	call	*8(%rax)
.L2134:
	leaq	.LC67(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2131
	.p2align 4,,10
	.p2align 3
.L2187:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L2136
	movq	-16(%r13), %rax
	leaq	-16(%r13), %rcx
	testb	$1, %al
	jne	.L2189
.L2138:
	leaq	.LC63(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2189:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L2138
	movq	-24(%r13), %r8
	testb	$1, %r8b
	jne	.L2190
	sarq	$32, %r8
	testl	$-8, %r8d
	jne	.L2191
	movq	23(%rax), %r15
	movq	15(%r15), %rax
	testb	$1, %al
	jne	.L2192
.L2143:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L2144:
	testb	%al, %al
	je	.L2148
	movq	15(%r15), %rdx
	testb	$1, %dl
	jne	.L2193
.L2146:
	movl	11(%rdx), %eax
	testl	%eax, %eax
	jne	.L2149
	movq	-16(%r13), %rax
	movq	-1(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2150
	movq	%r8, -200(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%r9, -184(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %rcx
	movq	-200(%rbp), %r8
	movq	%rax, %r15
.L2151:
	movq	%r9, %rsi
	movq	%rcx, %rdi
	leaq	3272(%r12), %rdx
	movq	%r8, -200(%rbp)
	movq	%r9, -192(%rbp)
	movq	%rcx, -184(%rbp)
	call	_ZN2v88internal10JSFunction7SetNameENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6StringEEE@PLT
	movq	-184(%rbp), %rcx
	movq	-192(%rbp), %r9
	testb	%al, %al
	movq	-200(%rbp), %r8
	je	.L2181
	movq	-16(%r13), %rax
	movq	-1(%rax), %rax
	cmpq	(%r15), %rax
	jne	.L2194
.L2149:
	leaq	104(%r12), %rdx
	movq	%r9, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject14DefineAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEES7_NS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	je	.L2181
	movq	88(%r12), %r13
.L2154:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2158
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2158:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2195
.L2126:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2196
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2181:
	.cfi_restore_state
	movq	312(%r12), %r13
	jmp	.L2154
	.p2align 4,,10
	.p2align 3
.L2148:
	andq	$-262144, %r15
	movq	24(%r15), %rax
	movq	-37464(%rax), %rdx
	jmp	.L2146
	.p2align 4,,10
	.p2align 3
.L2150:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L2197
.L2152:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L2151
	.p2align 4,,10
	.p2align 3
.L2192:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L2143
	leaq	-168(%rbp), %rdi
	movq	%r8, -200(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%r9, -184(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %rcx
	movq	-200(%rbp), %r8
	jmp	.L2144
	.p2align 4,,10
	.p2align 3
.L2193:
	movq	-1(%rdx), %rax
	cmpw	$136, 11(%rax)
	jne	.L2146
	leaq	-168(%rbp), %rdi
	movq	%r8, -200(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%r9, -184(%rbp)
	movq	%rdx, -168(%rbp)
	movq	%rdx, -216(%rbp)
	movq	%rdi, -208(%rbp)
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %rcx
	testb	%al, %al
	movq	-200(%rbp), %r8
	je	.L2148
	movq	-216(%rbp), %rdx
	movq	-208(%rbp), %rdi
	movq	%rdx, -168(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %rcx
	movq	-200(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L2146
	.p2align 4,,10
	.p2align 3
.L2182:
	movq	40960(%rsi), %rax
	movl	$420, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2127
	.p2align 4,,10
	.p2align 3
.L2190:
	leaq	.LC64(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2188:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC67(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2132
	.p2align 4,,10
	.p2align 3
.L2186:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2130
	.p2align 4,,10
	.p2align 3
.L2191:
	leaq	.LC65(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2195:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2126
	.p2align 4,,10
	.p2align 3
.L2194:
	leaq	.LC68(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2197:
	movq	%r12, %rdi
	movq	%rsi, -208(%rbp)
	movq	%r8, -200(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%r9, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %rsi
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %rcx
	movq	-184(%rbp), %r9
	movq	%rax, %r15
	jmp	.L2152
.L2196:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27304:
	.size	_ZN2v88internalL43Stats_Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL43Stats_Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL34Stats_Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC69:
	.string	"V8.Runtime_Runtime_ObjectHasOwnProperty"
	.section	.text._ZN2v88internalL34Stats_Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL34Stats_Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateE.isra.0:
.LFB27332:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -208(%rbp)
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2333
.L2199:
	movq	_ZZN2v88internalL34Stats_Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic260(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2334
.L2201:
	movq	$0, -240(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2335
.L2203:
	addl	$1, 41104(%r12)
	movq	-8(%r13), %rax
	leaq	-8(%r13), %r14
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	testb	$1, %al
	jne	.L2207
	sarq	$32, %rax
	js	.L2221
	movl	%eax, -244(%rbp)
.L2209:
	xorl	%r14d, %r14d
	movl	$1, %ecx
.L2217:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2336
.L2270:
	cmpq	%rax, 104(%r12)
	jne	.L2337
.L2282:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$173, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L2222:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2285
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2285:
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2338
.L2198:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2339
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2207:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L2326
	movsd	7(%rax), %xmm0
	movsd	.LC70(%rip), %xmm2
	addsd	%xmm0, %xmm2
	movq	%xmm2, %rcx
	movq	%xmm2, %rdx
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	jne	.L2326
	movl	%edx, %ecx
	pxor	%xmm1, %xmm1
	movd	%xmm2, -244(%rbp)
	cvtsi2sdq	%rcx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L2293
	jne	.L2293
	cmpl	$-1, %edx
	jne	.L2209
.L2326:
	xorl	%edx, %edx
.L2211:
	testb	%dl, %dl
	jne	.L2221
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	jbe	.L2220
	.p2align 4,,10
	.p2align 3
.L2221:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object13ConvertToNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2329
.L2220:
	movq	(%r14), %rax
	xorl	%ecx, %ecx
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2217
	movq	%rax, -160(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L2224
	testb	$2, %al
	jne	.L2217
.L2224:
	leaq	-244(%rbp), %rsi
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movl	%eax, %ecx
	movq	0(%r13), %rax
	testb	$1, %al
	je	.L2270
	.p2align 4,,10
	.p2align 3
.L2336:
	movq	-1(%rax), %rdx
	cmpw	$1027, 11(%rdx)
	je	.L2227
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L2340
	testb	%cl, %cl
	je	.L2321
	movabsq	$824633720832, %rax
	movb	%cl, -265(%rbp)
	leaq	-160(%rbp), %r8
	movq	%rax, -148(%rbp)
	movl	-244(%rbp), %eax
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movl	$0, -160(%rbp)
	movq	%r12, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%r13, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movl	%eax, -88(%rbp)
	movl	$-1, -84(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	-264(%rbp), %r8
	movzbl	-265(%rbp), %ecx
.L2240:
	movq	%r8, %rdi
	movb	%cl, -265(%rbp)
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	movq	-264(%rbp), %r8
	movzbl	-265(%rbp), %ecx
	testb	%al, %al
	je	.L2329
	shrw	$8, %ax
	jne	.L2327
	movq	0(%r13), %rax
	movq	-1(%rax), %rax
	cmpw	$1026, 11(%rax)
	je	.L2245
	movzbl	13(%rax), %eax
	testb	%cl, %cl
	je	.L2248
	testb	$8, %al
	je	.L2328
.L2249:
	movabsq	$824633720832, %rax
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	%rax, -148(%rbp)
	movl	-244(%rbp), %eax
	movl	$1, -160(%rbp)
	movq	%r12, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%r13, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movl	%eax, -88(%rbp)
	movl	$-1, -84(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	-264(%rbp), %r8
.L2252:
	movq	%r8, %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	jne	.L2266
	.p2align 4,,10
	.p2align 3
.L2329:
	movq	312(%r12), %r13
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2335:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2341
.L2204:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2205
	movq	(%rdi), %rax
	call	*8(%rax)
.L2205:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2206
	movq	(%rdi), %rax
	call	*8(%rax)
.L2206:
	leaq	.LC69(%rip), %rax
	movq	%rbx, -232(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%r14, -216(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L2203
	.p2align 4,,10
	.p2align 3
.L2334:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2342
.L2202:
	movq	%rbx, _ZZN2v88internalL34Stats_Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic260(%rip)
	jmp	.L2201
	.p2align 4,,10
	.p2align 3
.L2340:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L2343
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2270
	leaq	37592(%r12), %r13
	testb	%cl, %cl
	jne	.L2344
	movq	(%r14), %rax
	movq	2768(%r12), %rsi
	cmpq	%rsi, %rax
	je	.L2345
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$65504, %edx
	jne	.L2279
	movq	-1(%rsi), %rdx
	movzwl	11(%rdx), %edx
	andl	$65504, %edx
	je	.L2277
.L2279:
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	je	.L2277
	movq	-1(%rsi), %rdx
	cmpw	$64, 11(%rdx)
	je	.L2277
	leaq	-160(%rbp), %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal6String10SlowEqualsES1_@PLT
	movzbl	%al, %edx
	.p2align 4,,10
	.p2align 3
.L2273:
	leaq	-37592(%r13), %rax
	testl	%edx, %edx
	je	.L2280
.L2275:
	movq	112(%rax), %r13
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2337:
	cmpq	%rax, 88(%r12)
	je	.L2282
.L2328:
	movq	120(%r12), %r13
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2227:
	testq	%r14, %r14
	je	.L2328
.L2261:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L2329
.L2266:
	shrw	$8, %ax
	je	.L2328
.L2327:
	movq	112(%r12), %r13
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2343:
	testq	%r14, %r14
	jne	.L2261
	movl	-244(%rbp), %r8d
	testl	%r8d, %r8d
	js	.L2262
	movq	%r8, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	movl	%r8d, -264(%rbp)
	salq	$32, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb@PLT
	movl	-264(%rbp), %r8d
	movq	%rax, %r14
.L2263:
	movq	(%r14), %rax
	movl	11(%rax), %esi
	cmpl	$10, %esi
	jg	.L2261
	cmpl	$3, 7(%rax)
	jne	.L2261
	movl	%r8d, %edi
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movq	(%r14), %rdx
	movl	%eax, 7(%rdx)
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2338:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L2333:
	movq	40960(%rsi), %rax
	movl	$445, %edx
	leaq	-200(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2199
	.p2align 4,,10
	.p2align 3
.L2342:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2202
	.p2align 4,,10
	.p2align 3
.L2341:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC69(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2204
	.p2align 4,,10
	.p2align 3
.L2321:
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	(%r14), %rax
	subq	$37592, %rdi
	movq	-1(%rax), %rax
	movabsq	$824633720832, %rax
	movl	$0, -160(%rbp)
	movq	%rax, -148(%rbp)
	movq	%rdi, -136(%rbp)
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r14, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L2346
.L2241:
	leaq	-160(%rbp), %r8
	movb	%cl, -265(%rbp)
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%r13, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movzbl	-265(%rbp), %ecx
	movq	-264(%rbp), %r8
	jmp	.L2240
	.p2align 4,,10
	.p2align 3
.L2344:
	xorl	%edx, %edx
	movl	-244(%rbp), %ecx
	cmpl	%ecx, 11(%rax)
	seta	%dl
	jmp	.L2273
	.p2align 4,,10
	.p2align 3
.L2277:
	leaq	-37592(%r13), %rax
.L2280:
	movq	120(%rax), %r13
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2245:
	testb	%cl, %cl
	jne	.L2249
.L2251:
	movq	0(%r13), %rax
	movq	(%r14), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$1, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L2253
	movl	11(%rdx), %eax
	notl	%eax
	andl	$1, %eax
.L2253:
	movl	%eax, -160(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -148(%rbp)
	movq	%rdi, -136(%rbp)
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L2347
.L2254:
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	movq	%r14, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%r13, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	-264(%rbp), %r8
	jmp	.L2252
	.p2align 4,,10
	.p2align 3
.L2345:
	leaq	-37592(%r13), %rax
	jmp	.L2275
	.p2align 4,,10
	.p2align 3
.L2346:
	movq	%r14, %rsi
	movb	%cl, -264(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movzbl	-264(%rbp), %ecx
	jmp	.L2241
	.p2align 4,,10
	.p2align 3
.L2248:
	testb	$4, %al
	jne	.L2251
	jmp	.L2328
.L2262:
	movl	%r8d, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	%r8d, -264(%rbp)
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movl	-264(%rbp), %r8d
	movq	%rax, %r14
	jmp	.L2263
.L2293:
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L2211
.L2347:
	movq	%r14, %rsi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-264(%rbp), %r8
	movq	%rax, %r14
	jmp	.L2254
.L2339:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27332:
	.size	_ZN2v88internalL34Stats_Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL34Stats_Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL35Stats_Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC71:
	.string	"V8.Runtime_Runtime_AddDictionaryProperty"
	.section	.text._ZN2v88internalL35Stats_Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateE.isra.0:
.LFB27333:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2388
.L2349:
	movq	_ZZN2v88internalL35Stats_Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic348(%rip), %r13
	testq	%r13, %r13
	je	.L2389
.L2351:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L2390
.L2353:
	movq	41088(%r12), %r13
	movq	41096(%r12), %r14
	leaq	-8(%rbx), %r15
	leaq	-16(%rbx), %rcx
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	movq	7(%rax), %r8
	testb	$1, %r8b
	jne	.L2358
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %r8
.L2358:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2359
	movq	%r8, %rsi
	movq	%rcx, -184(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-184(%rbp), %rcx
	movq	%rax, %rsi
.L2360:
	movq	%r15, %rdx
	xorl	%r9d, %r9d
	movl	$192, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE3AddEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_4NameEEENS7_INS0_6ObjectEEENS0_15PropertyDetailsEPi@PLT
	movq	(%rbx), %rdx
	leaq	-168(%rbp), %rdi
	movq	%rdx, -168(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE@PLT
	movq	-16(%rbx), %r15
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L2364
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2364:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2391
.L2348:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2392
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2359:
	.cfi_restore_state
	movq	%r13, %rsi
	cmpq	%r14, %r13
	je	.L2393
.L2361:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L2360
	.p2align 4,,10
	.p2align 3
.L2390:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2394
.L2354:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2355
	movq	(%rdi), %rax
	call	*8(%rax)
.L2355:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2356
	movq	(%rdi), %rax
	call	*8(%rax)
.L2356:
	leaq	.LC71(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2353
	.p2align 4,,10
	.p2align 3
.L2389:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2395
.L2352:
	movq	%r13, _ZZN2v88internalL35Stats_Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic348(%rip)
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2388:
	movq	40960(%rsi), %rax
	movl	$405, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2349
	.p2align 4,,10
	.p2align 3
.L2391:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2348
	.p2align 4,,10
	.p2align 3
.L2393:
	movq	%r12, %rdi
	movq	%r8, -192(%rbp)
	movq	%rcx, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %r8
	movq	-184(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L2361
	.p2align 4,,10
	.p2align 3
.L2394:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC71(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2354
	.p2align 4,,10
	.p2align 3
.L2395:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L2352
.L2392:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27333:
	.size	_ZN2v88internalL35Stats_Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL32Stats_Runtime_TryMigrateInstanceEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC72:
	.string	"V8.Runtime_Runtime_TryMigrateInstance"
	.section	.text._ZN2v88internalL32Stats_Runtime_TryMigrateInstanceEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_TryMigrateInstanceEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_TryMigrateInstanceEiPmPNS0_7IsolateE.isra.0:
.LFB27334:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2432
.L2397:
	movq	_ZZN2v88internalL32Stats_Runtime_TryMigrateInstanceEiPmPNS0_7IsolateEE28trace_event_unique_atomic842(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2433
.L2399:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2434
.L2401:
	movl	41104(%r12), %eax
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L2405
	movl	%eax, 41104(%r12)
	xorl	%r13d, %r13d
.L2406:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2435
.L2396:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2436
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2433:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2437
.L2400:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_TryMigrateInstanceEiPmPNS0_7IsolateEE28trace_event_unique_atomic842(%rip)
	jmp	.L2399
	.p2align 4,,10
	.p2align 3
.L2405:
	movq	-1(%rdx), %rcx
	cmpw	$1024, 11(%rcx)
	ja	.L2407
.L2409:
	movq	%rbx, %rdx
	xorl	%r13d, %r13d
.L2408:
	movq	%r14, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L2406
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L2406
	.p2align 4,,10
	.p2align 3
.L2434:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2438
.L2402:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2403
	movq	(%rdi), %rax
	call	*8(%rax)
.L2403:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2404
	movq	(%rdi), %rax
	call	*8(%rax)
.L2404:
	leaq	.LC72(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2401
	.p2align 4,,10
	.p2align 3
.L2407:
	movq	-1(%rdx), %rdx
	movl	15(%rdx), %edx
	andl	$16777216, %edx
	je	.L2409
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject18TryMigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testb	%al, %al
	jne	.L2410
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	xorl	%r13d, %r13d
	subl	$1, %eax
	jmp	.L2408
	.p2align 4,,10
	.p2align 3
.L2435:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2396
	.p2align 4,,10
	.p2align 3
.L2432:
	movq	40960(%rsi), %rax
	movl	$464, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2397
	.p2align 4,,10
	.p2align 3
.L2438:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC72(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2402
	.p2align 4,,10
	.p2align 3
.L2437:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2400
	.p2align 4,,10
	.p2align 3
.L2410:
	movl	41104(%r12), %eax
	movq	0(%r13), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L2408
.L2436:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27334:
	.size	_ZN2v88internalL32Stats_Runtime_TryMigrateInstanceEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_TryMigrateInstanceEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL45Stats_Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC73:
	.string	"V8.Runtime_Runtime_DefineAccessorPropertyUnchecked"
	.section	.rodata._ZN2v88internalL45Stats_Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC74:
	.string	"!obj->IsNull(isolate)"
	.section	.rodata._ZN2v88internalL45Stats_Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateE.isra.0.str1.8
	.align 8
.LC75:
	.string	"IsValidAccessor(isolate, getter)"
	.align 8
.LC76:
	.string	"IsValidAccessor(isolate, setter)"
	.section	.rodata._ZN2v88internalL45Stats_Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateE.isra.0.str1.1
.LC77:
	.string	"args[4].IsSmi()"
	.section	.rodata._ZN2v88internalL45Stats_Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateE.isra.0.str1.8
	.align 8
.LC78:
	.string	"args.smi_at(4) & ~(READ_ONLY | DONT_ENUM | DONT_DELETE) == 0"
	.section	.text._ZN2v88internalL45Stats_Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Stats_Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL45Stats_Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateE.isra.0:
.LFB27335:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2483
.L2440:
	movq	_ZZN2v88internalL45Stats_Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateEE28trace_event_unique_atomic868(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2484
.L2442:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2485
.L2444:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2486
.L2448:
	leaq	.LC42(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2484:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2487
.L2443:
	movq	%rbx, _ZZN2v88internalL45Stats_Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateEE28trace_event_unique_atomic868(%rip)
	jmp	.L2442
	.p2align 4,,10
	.p2align 3
.L2486:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L2448
	cmpq	104(%r12), %rax
	je	.L2488
	movq	-8(%r13), %rax
	leaq	-8(%r13), %r9
	testb	$1, %al
	jne	.L2489
.L2451:
	leaq	.LC49(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2485:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2490
.L2445:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2446
	movq	(%rdi), %rax
	call	*8(%rax)
.L2446:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2447
	movq	(%rdi), %rax
	call	*8(%rax)
.L2447:
	leaq	.LC73(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2444
	.p2align 4,,10
	.p2align 3
.L2489:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L2451
	leaq	-16(%r13), %rdx
	movq	%r12, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL15IsValidAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L2491
	leaq	-24(%r13), %rcx
	movq	%rcx, %rsi
	call	_ZN2v88internalL15IsValidAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L2492
	movq	-32(%r13), %r8
	testb	$1, %r8b
	jne	.L2493
	sarq	$32, %r8
	testl	$-8, %r8d
	jne	.L2494
	movq	%r9, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject14DefineAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEES7_NS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	jne	.L2457
	movq	312(%r12), %r13
.L2458:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2461
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2461:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2495
.L2439:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2496
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2457:
	.cfi_restore_state
	movq	88(%r12), %r13
	jmp	.L2458
	.p2align 4,,10
	.p2align 3
.L2483:
	movq	40960(%rsi), %rax
	movl	$417, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2440
	.p2align 4,,10
	.p2align 3
.L2488:
	leaq	.LC74(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2491:
	leaq	.LC75(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2490:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC73(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2445
	.p2align 4,,10
	.p2align 3
.L2487:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2443
	.p2align 4,,10
	.p2align 3
.L2492:
	leaq	.LC76(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2493:
	leaq	.LC77(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2494:
	leaq	.LC78(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2495:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2439
.L2496:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27335:
	.size	_ZN2v88internalL45Stats_Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL45Stats_Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL26Stats_Runtime_ObjectCreateEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC79:
	.string	"V8.Runtime_Runtime_ObjectCreate"
	.section	.text._ZN2v88internalL26Stats_Runtime_ObjectCreateEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL26Stats_Runtime_ObjectCreateEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL26Stats_Runtime_ObjectCreateEiPmPNS0_7IsolateE.isra.0:
.LFB27336:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2536
.L2498:
	movq	_ZZN2v88internalL26Stats_Runtime_ObjectCreateEiPmPNS0_7IsolateEE28trace_event_unique_atomic367(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2537
.L2500:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2538
.L2502:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	cmpq	104(%r12), %rax
	je	.L2506
	testb	$1, %al
	jne	.L2507
.L2509:
	movq	%r13, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$116, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L2508:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2515
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2515:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2539
.L2497:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2540
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2507:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2509
	.p2align 4,,10
	.p2align 3
.L2506:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject12ObjectCreateEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2535
	movq	88(%r12), %rax
	leaq	-8(%r13), %rdx
	cmpq	%rax, -8(%r13)
	je	.L2511
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver16DefinePropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_@PLT
	testq	%rax, %rax
	je	.L2535
	movq	(%rax), %r13
	jmp	.L2508
	.p2align 4,,10
	.p2align 3
.L2538:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2541
.L2503:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2504
	movq	(%rdi), %rax
	call	*8(%rax)
.L2504:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2505
	movq	(%rdi), %rax
	call	*8(%rax)
.L2505:
	leaq	.LC79(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2502
	.p2align 4,,10
	.p2align 3
.L2537:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2542
.L2501:
	movq	%rbx, _ZZN2v88internalL26Stats_Runtime_ObjectCreateEiPmPNS0_7IsolateEE28trace_event_unique_atomic367(%rip)
	jmp	.L2500
	.p2align 4,,10
	.p2align 3
.L2511:
	movq	(%rsi), %r13
	jmp	.L2508
	.p2align 4,,10
	.p2align 3
.L2535:
	movq	312(%r12), %r13
	jmp	.L2508
	.p2align 4,,10
	.p2align 3
.L2536:
	movq	40960(%rsi), %rax
	movl	$440, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2498
	.p2align 4,,10
	.p2align 3
.L2539:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2497
	.p2align 4,,10
	.p2align 3
.L2542:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2501
	.p2align 4,,10
	.p2align 3
.L2541:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC79(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2503
.L2540:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27336:
	.size	_ZN2v88internalL26Stats_Runtime_ObjectCreateEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL26Stats_Runtime_ObjectCreateEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL55Stats_Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC80:
	.string	"V8.Runtime_Runtime_OptimizeObjectForAddingMultipleProperties"
	.section	.rodata._ZN2v88internalL55Stats_Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC81:
	.string	"OptimizeForAdding"
	.section	.text._ZN2v88internalL55Stats_Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL55Stats_Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL55Stats_Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateE.isra.0:
.LFB27337:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2582
.L2544:
	movq	_ZZN2v88internalL55Stats_Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateEE28trace_event_unique_atomic435(%rip), %r13
	testq	%r13, %r13
	je	.L2583
.L2546:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L2584
.L2548:
	movq	41088(%r12), %r15
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L2585
.L2552:
	leaq	.LC42(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2583:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2586
.L2547:
	movq	%r13, _ZZN2v88internalL55Stats_Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateEE28trace_event_unique_atomic435(%rip)
	jmp	.L2546
	.p2align 4,,10
	.p2align 3
.L2585:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L2552
	movq	-8(%rbx), %rcx
	testb	$1, %cl
	jne	.L2587
	sarq	$32, %rcx
	cmpq	$100000, %rcx
	jle	.L2555
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate21ThrowIllegalOperationEv@PLT
	movq	%rax, %r13
.L2556:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L2561
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2561:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2588
.L2543:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2589
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2555:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	je	.L2590
.L2558:
	movq	%rax, %r13
	jmp	.L2556
	.p2align 4,,10
	.p2align 3
.L2584:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2591
.L2549:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2550
	movq	(%rdi), %rax
	call	*8(%rax)
.L2550:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2551
	movq	(%rdi), %rax
	call	*8(%rax)
.L2551:
	leaq	.LC80(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2548
	.p2align 4,,10
	.p2align 3
.L2590:
	movq	-1(%rax), %rdx
	cmpw	$1026, 11(%rdx)
	je	.L2558
	leaq	.LC81(%rip), %r8
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject19NormalizePropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeEiPKc@PLT
	movq	(%rbx), %rax
	jmp	.L2558
	.p2align 4,,10
	.p2align 3
.L2582:
	movq	40960(%rsi), %rax
	movl	$450, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2544
	.p2align 4,,10
	.p2align 3
.L2587:
	leaq	.LC39(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2588:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2591:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC80(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2549
	.p2align 4,,10
	.p2align 3
.L2586:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L2547
.L2589:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27337:
	.size	_ZN2v88internalL55Stats_Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL55Stats_Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL41Stats_Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC82:
	.string	"V8.Runtime_Runtime_DefineDataPropertyInLiteral"
	.section	.rodata._ZN2v88internalL41Stats_Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC83:
	.string	"args[4].IsHeapObject()"
.LC84:
	.string	"args[5].IsSmi()"
	.section	.rodata._ZN2v88internalL41Stats_Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateE.isra.0.str1.8
	.align 8
.LC85:
	.string	"!IsClassConstructor(function->shared().kind()) implies *function_map == function->map()"
	.align 8
.LC86:
	.string	"JSObject::DefineOwnPropertyIgnoreAttributes(&it, value, attrs, Just(kDontThrow)) .IsJust()"
	.section	.text._ZN2v88internalL41Stats_Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Stats_Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL41Stats_Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateE.isra.0:
.LFB27339:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$344, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -288(%rbp)
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2672
.L2593:
	movq	_ZZN2v88internalL41Stats_Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic885(%rip), %r12
	testq	%r12, %r12
	je	.L2673
.L2595:
	movq	$0, -320(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L2674
.L2597:
	movq	41088(%r14), %rax
	addl	$1, 41104(%r14)
	movq	%rax, -352(%rbp)
	movq	41096(%r14), %rax
	movq	%rax, -344(%rbp)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L2675
.L2601:
	leaq	.LC42(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2673:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2676
.L2596:
	movq	%r12, _ZZN2v88internalL41Stats_Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic885(%rip)
	jmp	.L2595
	.p2align 4,,10
	.p2align 3
.L2675:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L2601
	movq	-8(%rbx), %rax
	leaq	-8(%rbx), %r12
	testb	$1, %al
	jne	.L2677
.L2602:
	leaq	.LC49(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2674:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2678
.L2598:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2599
	movq	(%rdi), %rax
	call	*8(%rax)
.L2599:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2600
	movq	(%rdi), %rax
	call	*8(%rax)
.L2600:
	leaq	.LC82(%rip), %rax
	movq	%r12, -312(%rbp)
	movq	%rax, -304(%rbp)
	leaq	-312(%rbp), %rax
	movq	%r13, -296(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L2597
	.p2align 4,,10
	.p2align 3
.L2677:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L2602
	movq	-24(%rbx), %r8
	testb	$1, %r8b
	jne	.L2679
	movq	-32(%rbx), %rax
	sarq	$32, %r8
	leaq	-32(%rbx), %rcx
	testb	$1, %al
	je	.L2680
	movq	-40(%rbx), %rsi
	testb	$1, %sil
	jne	.L2681
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rax
	jne	.L2682
.L2608:
	movl	%r8d, %r13d
	leaq	-16(%rbx), %r15
	andl	$1, %r13d
	addl	%r13d, %r13d
	andl	$2, %r8d
	je	.L2618
	movq	-16(%rbx), %rax
	movq	-1(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2619
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2620:
	leaq	128(%r14), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rcx, -360(%rbp)
	call	_ZN2v88internal10JSFunction7SetNameENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6StringEEE@PLT
	movq	-360(%rbp), %rcx
	testb	%al, %al
	jne	.L2622
	movq	312(%r14), %r12
	jmp	.L2623
	.p2align 4,,10
	.p2align 3
.L2622:
	movq	-16(%rbx), %rdx
	movq	23(%rdx), %rax
	movl	47(%rax), %eax
	andl	$31, %eax
	subl	$2, %eax
	cmpb	$3, %al
	ja	.L2683
.L2618:
	movq	-8(%rbx), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L2684
.L2627:
	movq	-8(%rbx), %rcx
	movl	$1, %edx
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	je	.L2685
.L2631:
	movabsq	$824633720832, %rcx
	movl	%edx, -240(%rbp)
	movq	%rcx, -228(%rbp)
	movq	%r14, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L2686
.L2632:
	leaq	-240(%rbp), %r11
	movq	%r12, -208(%rbp)
	movq	%r11, %rdi
	movq	%r11, -360(%rbp)
	movq	$0, -200(%rbp)
	movq	%rbx, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	-360(%rbp), %r11
.L2630:
	movl	$1, %r8d
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%r11, %rdi
	movabsq	$4294967297, %rcx
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE@PLT
	testb	%al, %al
	je	.L2687
	movq	(%rbx), %r12
.L2623:
	movq	-352(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-344(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L2636
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2636:
	leaq	-320(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2688
.L2592:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2689
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2619:
	.cfi_restore_state
	movq	41088(%r14), %rcx
	cmpq	41096(%r14), %rcx
	je	.L2690
.L2621:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rcx)
	jmp	.L2620
	.p2align 4,,10
	.p2align 3
.L2685:
	movl	11(%rcx), %edx
	notl	%edx
	andl	$1, %edx
	jmp	.L2631
	.p2align 4,,10
	.p2align 3
.L2682:
	leaq	-240(%rbp), %r11
	sarq	$32, %rsi
	leaq	-160(%rbp), %r13
	movq	%r8, -368(%rbp)
	movq	%r11, %rdi
	movq	%r11, -360(%rbp)
	movq	%rcx, -160(%rbp)
	movl	%esi, -144(%rbp)
	movq	$0, -152(%rbp)
	movq	%rax, -240(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movq	%r13, %rdi
	movl	%eax, -140(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movq	-360(%rbp), %r11
	movq	-368(%rbp), %r8
	cmpl	$1, %eax
	je	.L2691
	movq	%r13, %rdi
	movq	%r8, -360(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movq	-360(%rbp), %r8
	cmpl	$3, %eax
	jne	.L2608
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movq	-1(%rax), %r15
	call	_ZNK2v88internal13FeedbackNexus11GetFirstMapEv@PLT
	movq	-360(%rbp), %r8
	cmpq	%rax, %r15
	jne	.L2616
	movq	-8(%rbx), %r15
	movq	%r13, %rdi
	movq	%r8, -360(%rbp)
	call	_ZNK2v88internal13FeedbackNexus7GetNameEv@PLT
	movq	-360(%rbp), %r8
	cmpq	%rax, %r15
	je	.L2608
	.p2align 4,,10
	.p2align 3
.L2616:
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%r8, -360(%rbp)
	call	_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicENS0_11IcCheckTypeE@PLT
	movq	-360(%rbp), %r8
	jmp	.L2608
	.p2align 4,,10
	.p2align 3
.L2684:
	movq	%rax, -160(%rbp)
	movl	7(%rax), %edx
	testb	$1, %dl
	jne	.L2628
	andl	$2, %edx
	jne	.L2627
.L2628:
	leaq	-160(%rbp), %rdi
	leaq	-324(%rbp), %rsi
	movq	%rdi, -360(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-360(%rbp), %rdi
	testb	%al, %al
	je	.L2692
	movabsq	$824633720832, %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -148(%rbp)
	movl	-324(%rbp), %eax
	movl	$1, -160(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%rbx, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movl	%eax, -88(%rbp)
	movl	$-1, -84(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r12, -128(%rbp)
	movdqa	-112(%rbp), %xmm4
	leaq	-240(%rbp), %r11
	movdqa	-160(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm5
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm1, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm5, -176(%rbp)
	jmp	.L2630
	.p2align 4,,10
	.p2align 3
.L2672:
	movq	40960(%rsi), %rax
	movl	$418, %edx
	leaq	-280(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2593
	.p2align 4,,10
	.p2align 3
.L2679:
	leaq	.LC64(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2680:
	leaq	.LC83(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2691:
	movq	-8(%rbx), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L2616
	movl	$1, -240(%rbp)
	movq	(%rbx), %rax
	movq	$0, -232(%rbp)
	movq	-1(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2611
	movq	%r11, -368(%rbp)
	movq	%r8, -360(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-360(%rbp), %r8
	movq	-368(%rbp), %r11
	movq	%rax, %rdx
.L2612:
	movq	%r11, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, -360(%rbp)
	call	_ZN2v88internal13FeedbackNexus20ConfigureMonomorphicENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE@PLT
	movq	-360(%rbp), %r8
	jmp	.L2608
	.p2align 4,,10
	.p2align 3
.L2678:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC82(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L2598
	.p2align 4,,10
	.p2align 3
.L2676:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L2596
	.p2align 4,,10
	.p2align 3
.L2681:
	leaq	.LC84(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2688:
	leaq	-280(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2592
	.p2align 4,,10
	.p2align 3
.L2686:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r12
	jmp	.L2632
	.p2align 4,,10
	.p2align 3
.L2683:
	movq	-1(%rdx), %rax
	cmpq	(%rcx), %rax
	je	.L2618
	leaq	.LC85(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2687:
	leaq	.LC86(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2692:
	movq	-8(%rbx), %rax
	jmp	.L2627
	.p2align 4,,10
	.p2align 3
.L2690:
	movq	%r14, %rdi
	movq	%rsi, -360(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-360(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L2621
	.p2align 4,,10
	.p2align 3
.L2611:
	movq	41088(%r14), %rdx
	cmpq	41096(%r14), %rdx
	je	.L2693
.L2613:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L2612
.L2693:
	movq	%r14, %rdi
	movq	%r11, -376(%rbp)
	movq	%r8, -368(%rbp)
	movq	%rsi, -360(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-376(%rbp), %r11
	movq	-368(%rbp), %r8
	movq	-360(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L2613
.L2689:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27339:
	.size	_ZN2v88internalL41Stats_Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL41Stats_Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internal10JSReceiver12GetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EE.str1.1,"aMS",@progbits,1
.LC87:
	.string	"!handle_.is_null()"
	.section	.text._ZN2v88internal10JSReceiver12GetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EE,"axG",@progbits,_ZN2v88internal10JSReceiver12GetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JSReceiver12GetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal10JSReceiver12GetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal10JSReceiver12GetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB14139:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-64(%rbp), %rbx
	subq	$40, %rsp
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testq	%rsi, %rsi
	je	.L2728
	.p2align 4,,10
	.p2align 3
.L2695:
	movq	(%r12), %r14
	movq	-1(%r14), %rax
	cmpw	$1026, 11(%rax)
	je	.L2729
	movq	-1(%r14), %rax
	movzbl	13(%rax), %eax
	shrb	$5, %al
	andl	$1, %eax
.L2699:
	testb	%al, %al
	je	.L2700
	movq	41112(%r13), %rdi
	movq	12464(%r13), %r14
	testq	%rdi, %rdi
	je	.L2701
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2702:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	jne	.L2700
	leaq	104(%r13), %r12
.L2708:
	movq	%r12, %rax
.L2710:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2730
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2700:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jne	.L2714
	movq	%r12, %rdi
	call	_ZN2v88internal7JSProxy12GetPrototypeENS0_6HandleIS1_EE@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	jne	.L2708
	jmp	.L2710
	.p2align 4,,10
	.p2align 3
.L2729:
	movq	%r14, %r15
	movq	%rbx, %rdi
	andq	$-262144, %r15
	movq	24(%r15), %rax
	movq	-25128(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	24(%r15), %rdx
	testb	$1, %r14b
	jne	.L2731
.L2697:
	movq	-1(%r14), %rdx
	movq	23(%rdx), %rdx
.L2698:
	cmpq	%rax, %rdx
	setne	%al
	jmp	.L2699
	.p2align 4,,10
	.p2align 3
.L2714:
	movq	-1(%rax), %rax
	movl	$1, %r14d
	movq	23(%rax), %rsi
	cmpq	104(%r13), %rsi
	je	.L2705
	cmpw	$1026, 11(%rax)
	setne	%r14b
.L2705:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2732
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L2706:
	testb	%r14b, %r14b
	je	.L2695
	jmp	.L2708
	.p2align 4,,10
	.p2align 3
.L2701:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L2733
.L2703:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%rsi)
	jmp	.L2702
	.p2align 4,,10
	.p2align 3
.L2732:
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L2734
.L2707:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L2706
	.p2align 4,,10
	.p2align 3
.L2733:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2703
	.p2align 4,,10
	.p2align 3
.L2731:
	movq	-1(%r14), %rcx
	cmpw	$1024, 11(%rcx)
	jne	.L2697
	movq	-37488(%rdx), %rdx
	jmp	.L2698
	.p2align 4,,10
	.p2align 3
.L2734:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L2707
	.p2align 4,,10
	.p2align 3
.L2728:
	leaq	.LC87(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2730:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14139:
	.size	_ZN2v88internal10JSReceiver12GetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal10JSReceiver12GetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internalL38Stats_Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC88:
	.string	"V8.Runtime_Runtime_JSReceiverGetPrototypeOf"
	.section	.text._ZN2v88internalL38Stats_Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Stats_Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL38Stats_Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateE.isra.0:
.LFB27302:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2771
.L2736:
	movq	_ZZN2v88internalL38Stats_Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic540(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2772
.L2738:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2773
.L2740:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2774
.L2744:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2772:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2775
.L2739:
	movq	%rbx, _ZZN2v88internalL38Stats_Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic540(%rip)
	jmp	.L2738
	.p2align 4,,10
	.p2align 3
.L2774:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2744
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver12GetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EE
	testq	%rax, %rax
	je	.L2776
	movq	(%rax), %r13
.L2747:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L2750
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2750:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2777
.L2735:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2778
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2773:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2779
.L2741:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2742
	movq	(%rdi), %rax
	call	*8(%rax)
.L2742:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2743
	movq	(%rdi), %rax
	call	*8(%rax)
.L2743:
	leaq	.LC88(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2740
	.p2align 4,,10
	.p2align 3
.L2776:
	movq	312(%r12), %r13
	jmp	.L2747
	.p2align 4,,10
	.p2align 3
.L2771:
	movq	40960(%rsi), %rax
	movl	$434, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2736
	.p2align 4,,10
	.p2align 3
.L2777:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2735
	.p2align 4,,10
	.p2align 3
.L2775:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2739
	.p2align 4,,10
	.p2align 3
.L2779:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC88(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2741
.L2778:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27302:
	.size	_ZN2v88internalL38Stats_Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL38Stats_Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal8JSObject20RawFastPropertyAtPutENS0_10FieldIndexENS0_6ObjectENS0_16WriteBarrierModeE,"axG",@progbits,_ZN2v88internal8JSObject20RawFastPropertyAtPutENS0_10FieldIndexENS0_6ObjectENS0_16WriteBarrierModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8JSObject20RawFastPropertyAtPutENS0_10FieldIndexENS0_6ObjectENS0_16WriteBarrierModeE
	.type	_ZN2v88internal8JSObject20RawFastPropertyAtPutENS0_10FieldIndexENS0_6ObjectENS0_16WriteBarrierModeE, @function
_ZN2v88internal8JSObject20RawFastPropertyAtPutENS0_10FieldIndexENS0_6ObjectENS0_16WriteBarrierModeE:
.LFB14174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	testl	$16384, %esi
	je	.L2781
	andl	$16383, %esi
	movq	%rdx, -1(%rax,%rsi)
	leaq	-1(%rsi), %r13
	testl	%ecx, %ecx
	je	.L2780
	movq	%rdi, %rbx
	movq	%rdx, %rax
	movq	(%rdi), %rdi
	notq	%rax
	leaq	0(%r13,%rdi), %rsi
	andl	$1, %eax
	cmpl	$4, %ecx
	je	.L2812
	testb	%al, %al
	jne	.L2780
.L2804:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2780
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2811
.L2780:
	addq	$24, %rsp
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2781:
	.cfi_restore_state
	movq	7(%rax), %r13
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %r13b
	jne	.L2788
.L2790:
	movq	968(%rax), %r13
.L2789:
	movq	%rsi, %rax
	sarl	$3, %esi
	shrq	$30, %rax
	andl	$2047, %esi
	andl	$15, %eax
	subl	%eax, %esi
	leal	16(,%rsi,8), %eax
	cltq
	leaq	-1(%r13,%rax), %r14
	movq	%rdx, (%r14)
	testb	$1, %dl
	je	.L2780
	movq	%rdx, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L2813
.L2792:
	testb	$24, %al
	je	.L2780
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2780
	movq	%r14, %rsi
	movq	%r13, %rdi
.L2811:
	addq	$24, %rsp
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2788:
	.cfi_restore_state
	cmpq	288(%rax), %r13
	jne	.L2789
	jmp	.L2790
	.p2align 4,,10
	.p2align 3
.L2813:
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-40(%rbp), %rdx
	jmp	.L2792
	.p2align 4,,10
	.p2align 3
.L2812:
	testb	%al, %al
	jne	.L2780
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2804
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	-40(%rbp), %rdx
	leaq	0(%r13,%rdi), %rsi
	jmp	.L2804
	.cfi_endproc
.LFE14174:
	.size	_ZN2v88internal8JSObject20RawFastPropertyAtPutENS0_10FieldIndexENS0_6ObjectENS0_16WriteBarrierModeE, .-_ZN2v88internal8JSObject20RawFastPropertyAtPutENS0_10FieldIndexENS0_6ObjectENS0_16WriteBarrierModeE
	.section	.rodata._ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE.str1.8,"aMS",@progbits,1
	.align 8
.LC89:
	.string	"(!IsSmi() && (*layout_word_index < length())) || (IsSmi() && (*layout_word_index < 1))"
	.section	.text._ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE,"axG",@progbits,_ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE
	.type	_ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE, @function
_ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE:
.LFB17654:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testb	$64, %dh
	jne	.L2834
.L2814:
	ret
	.p2align 4,,10
	.p2align 3
.L2834:
	movq	(%rdi), %rcx
	movq	47(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L2814
	movq	%rsi, %rcx
	movl	$32, %r8d
	notq	%rcx
	movl	%ecx, %edi
	andl	$1, %edi
	je	.L2835
.L2817:
	movq	%rdx, %rax
	sarl	$3, %edx
	shrq	$30, %rax
	andl	$2047, %edx
	andl	$15, %eax
	subl	%eax, %edx
	xorl	%eax, %eax
	cmpl	%r8d, %edx
	jnb	.L2814
	testl	%edx, %edx
	leal	31(%rdx), %eax
	cmovns	%edx, %eax
	sarl	$5, %eax
	andl	$1, %ecx
	jne	.L2818
	cmpl	%eax, 11(%rsi)
	jle	.L2818
	movl	%edx, %r8d
	sarl	$31, %r8d
	shrl	$27, %r8d
	leal	(%rdx,%r8), %ecx
	movl	$1, %edx
	andl	$31, %ecx
	subl	%r8d, %ecx
	sall	%cl, %edx
	testb	%dil, %dil
	je	.L2836
.L2822:
	sarq	$32, %rsi
	testl	%esi, %edx
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L2835:
	movslq	11(%rsi), %r8
	sall	$3, %r8d
	jmp	.L2817
	.p2align 4,,10
	.p2align 3
.L2836:
	sall	$2, %eax
	cltq
	movl	15(%rsi,%rax), %eax
	testl	%eax, %edx
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L2818:
	testb	%dil, %dil
	je	.L2820
	cmpl	$31, %edx
	jg	.L2820
	movl	%edx, %ecx
	movl	$1, %eax
	sall	%cl, %eax
	movl	%eax, %edx
	jmp	.L2822
.L2820:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC89(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17654:
	.size	_ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE, .-_ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE
	.section	.text._ZN2v88internal12_GLOBAL__N_124DeleteObjectPropertyFastEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_124DeleteObjectPropertyFastEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal12_GLOBAL__N_124DeleteObjectPropertyFastEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEE:
.LFB21403:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	-1(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2838
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L2839:
	cmpw	$1040, 11(%rsi)
	jbe	.L2845
	movq	(%r15), %rax
	movq	%rax, %r13
	notq	%r13
	andl	$1, %r13d
	je	.L2843
.L2845:
	xorl	%r13d, %r13d
.L2837:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2890
	addq	$88, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2843:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$65504, %edx
	jne	.L2891
.L2844:
	movq	(%r14), %rcx
	movl	15(%rcx), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	movl	%eax, %edx
	je	.L2845
	movq	41112(%r12), %rdi
	movq	39(%rcx), %rsi
	testq	%rdi, %rdi
	je	.L2847
	movl	%eax, -88(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-88(%rbp), %edx
	movq	(%rax), %rsi
	movq	%rax, %r8
.L2848:
	leal	(%rdx,%rdx,2), %ecx
	sall	$3, %ecx
	movslq	%ecx, %rcx
	movq	-1(%rcx,%rsi), %rax
	cmpq	%rax, (%r15)
	jne	.L2845
	movq	(%r8), %rax
	movq	7(%rcx,%rax), %rax
	movq	%rax, %r10
	sarq	$32, %r10
	btq	$37, %rax
	jc	.L2845
	movq	(%r14), %rax
	movq	31(%rax), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %sil
	je	.L2850
	movq	-1(%rsi), %rdi
	cmpq	%rdi, 136(%rax)
	je	.L2851
.L2850:
	movq	88(%rax), %rsi
.L2851:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2852
	movq	%r10, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movl	%edx, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-88(%rbp), %r8
	movl	-96(%rbp), %edx
	movq	(%rax), %rsi
	movq	-104(%rbp), %rcx
	movq	%rax, %r15
	movq	-112(%rbp), %r10
.L2853:
	testb	$1, %sil
	je	.L2837
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L2837
	movq	(%r15), %rax
	leal	-1(%rdx), %r13d
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%eax, %r13d
	jne	.L2845
	movl	%r10d, %eax
	shrl	%eax
	andl	$1, %eax
	testb	$4, %r10b
	je	.L2857
	testl	%eax, %eax
	je	.L2892
.L2858:
	movq	(%r14), %rax
	movl	15(%rax), %edx
	andl	$33554432, %edx
	je	.L2893
.L2869:
	movq	(%rbx), %rdi
	movq	(%r15), %rdx
	movq	%rdx, -1(%rdi)
	testq	%rdx, %rdx
	jne	.L2894
.L2870:
	movl	$1, %r13d
	jmp	.L2837
	.p2align 4,,10
	.p2align 3
.L2838:
	movq	41088(%r12), %r14
	cmpq	%r14, 41096(%r12)
	je	.L2895
.L2840:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L2839
	.p2align 4,,10
	.p2align 3
.L2891:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	jne	.L2845
	jmp	.L2844
	.p2align 4,,10
	.p2align 3
.L2895:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L2840
	.p2align 4,,10
	.p2align 3
.L2847:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L2896
.L2849:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L2848
.L2896:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	movl	%eax, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
	movq	%rax, %r8
	jmp	.L2849
.L2852:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L2897
.L2854:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L2853
.L2897:
	movq	%r12, %rdi
	movq	%r10, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movl	%edx, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %r10
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rcx
	movl	-96(%rbp), %edx
	movq	%rax, %r15
	movq	-88(%rbp), %r8
	jmp	.L2854
.L2857:
	testl	%eax, %eax
	jne	.L2858
.L2862:
	movq	(%r14), %rax
	movq	%r10, -96(%rbp)
	leaq	37592(%r12), %r8
	leaq	-65(%rbp), %rcx
	movq	%r8, %rdi
	movzbl	7(%rax), %edx
	movq	(%rbx), %rsi
	movq	%r8, -88(%rbp)
	sall	$3, %edx
	call	_ZN2v88internal4Heap24NotifyObjectLayoutChangeENS0_10HeapObjectEiRKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	-96(%rbp), %r10
	movq	(%r14), %rsi
	movl	%r10d, %eax
	movzbl	7(%rsi), %ecx
	movzbl	8(%rsi), %edx
	shrl	$19, %eax
	movq	-88(%rbp), %r8
	andl	$1023, %eax
	subl	%edx, %ecx
	cmpl	%ecx, %eax
	setl	%dil
	jl	.L2898
	subl	%ecx, %eax
	movl	$16, %edx
	leal	16(,%rax,8), %esi
.L2864:
	movzbl	%dil, %eax
	movslq	%ecx, %rcx
	movslq	%esi, %rsi
	movslq	%edx, %r13
	salq	$17, %rcx
	salq	$14, %rax
	orq	%rcx, %rax
	salq	$27, %r13
	movq	(%rbx), %rcx
	orq	%rsi, %rax
	orq	%rax, %r13
	testb	$64, %ah
	jne	.L2865
	movq	%r13, %rax
	movl	$8, %esi
	shrq	$27, %rax
	andl	$127, %eax
	cltd
	idivl	%esi
	movl	%r13d, %edx
	sarl	$3, %edx
	andl	$2047, %edx
	cmpl	%eax, %edx
	je	.L2899
	movq	64(%r12), %rdx
	movq	%rcx, -64(%rbp)
	leaq	-64(%rbp), %rdi
	movq	%r13, %rsi
	movl	$4, %ecx
	call	_ZN2v88internal8JSObject20RawFastPropertyAtPutENS0_10FieldIndexENS0_6ObjectENS0_16WriteBarrierModeE
	jmp	.L2858
.L2890:
	call	__stack_chk_fail@PLT
.L2893:
	movl	15(%rax), %edx
	leaq	-64(%rbp), %rdi
	movq	%r12, %rsi
	orl	$33554432, %edx
	movl	%edx, 15(%rax)
	movl	$1, %edx
	movq	55(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE@PLT
	jmp	.L2869
.L2894:
	testb	$1, %dl
	je	.L2870
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2870
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2870
.L2892:
	movq	(%r8), %rax
	movq	%r10, -88(%rbp)
	movq	15(%rcx,%rax), %rdi
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	movq	41112(%r12), %rdi
	movq	-88(%rbp), %r10
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2859
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-88(%rbp), %r10
	movq	%rax, %r9
.L2860:
	movl	%r10d, %r8d
	xorl	%ecx, %ecx
	movl	%r13d, %edx
	movq	%r14, %rsi
	shrl	$6, %r8d
	movq	%r12, %rdi
	movq	%r10, -88(%rbp)
	andl	$7, %r8d
	call	_ZN2v88internal3Map15GeneralizeFieldEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS0_14RepresentationENS4_INS0_9FieldTypeEEE@PLT
	movq	-88(%rbp), %r10
	jmp	.L2862
.L2899:
	movq	288(%r12), %rsi
	leaq	-64(%rbp), %rdi
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE@PLT
	jmp	.L2858
.L2859:
	movq	41088(%r12), %r9
	cmpq	41096(%r12), %r9
	je	.L2900
.L2861:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r9)
	jmp	.L2860
.L2865:
	movq	64(%r12), %rdx
	leaq	-64(%rbp), %rdi
	movq	%rcx, -64(%rbp)
	movq	%r13, %rsi
	movl	$4, %ecx
	movq	%r8, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal8JSObject20RawFastPropertyAtPutENS0_10FieldIndexENS0_6ObjectENS0_16WriteBarrierModeE
	movq	(%r14), %rax
	movq	-88(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE
	movq	-96(%rbp), %r8
	testb	%al, %al
	jne	.L2858
	movq	(%rbx), %rsi
	andl	$16376, %r13d
	movq	%r8, %rdi
	leaq	-1(%rsi,%r13), %rdx
	call	_ZN2v88internal4Heap17ClearRecordedSlotENS0_10HeapObjectENS0_14FullObjectSlotE@PLT
	jmp	.L2858
.L2898:
	movzbl	8(%rsi), %edx
	movzbl	8(%rsi), %esi
	addl	%eax, %esi
	sall	$3, %edx
	sall	$3, %esi
	jmp	.L2864
.L2900:
	movq	%r12, %rdi
	movq	%r10, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %r10
	movq	-88(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L2861
	.cfi_endproc
.LFE21403:
	.size	_ZN2v88internal12_GLOBAL__N_124DeleteObjectPropertyFastEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal12_GLOBAL__N_124DeleteObjectPropertyFastEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_114DeletePropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_12LanguageModeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_114DeletePropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_12LanguageModeE, @function
_ZN2v88internal12_GLOBAL__N_114DeletePropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_12LanguageModeE:
.LFB21472:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L2902
.L2905:
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2910
.L2904:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_124DeleteObjectPropertyFastEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEE
	testb	%al, %al
	je	.L2916
.L2908:
	movq	112(%rbx), %rax
.L2906:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2917
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2916:
	.cfi_restore_state
	leaq	-144(%rbp), %r15
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movl	$1, %r9d
	leaq	-145(%rbp), %r8
	movq	%r15, %rdi
	movb	$0, -145(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	cmpb	$0, -145(%rbp)
	je	.L2910
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal10JSReceiver14DeletePropertyEPNS0_14LookupIteratorENS0_12LanguageModeE@PLT
	movzbl	%ah, %edx
	testb	%al, %al
	jne	.L2911
.L2910:
	movq	312(%rbx), %rax
	jmp	.L2906
	.p2align 4,,10
	.p2align 3
.L2902:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2905
	movq	%rsi, %r12
	jmp	.L2904
.L2917:
	call	__stack_chk_fail@PLT
.L2911:
	testb	%dl, %dl
	jne	.L2908
	movq	120(%rbx), %rax
	jmp	.L2906
	.cfi_endproc
.LFE21472:
	.size	_ZN2v88internal12_GLOBAL__N_114DeletePropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_12LanguageModeE, .-_ZN2v88internal12_GLOBAL__N_114DeletePropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_12LanguageModeE
	.section	.rodata._ZN2v88internalL28Stats_Runtime_DeletePropertyEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC90:
	.string	"V8.Runtime_Runtime_DeleteProperty"
	.section	.rodata._ZN2v88internalL28Stats_Runtime_DeletePropertyEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC91:
	.string	"args[2].IsSmi()"
	.section	.text._ZN2v88internalL28Stats_Runtime_DeletePropertyEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_DeletePropertyEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL28Stats_Runtime_DeletePropertyEiPmPNS0_7IsolateE.isra.0:
.LFB27233:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2949
.L2919:
	movq	_ZZN2v88internalL28Stats_Runtime_DeletePropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic732(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2950
.L2921:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2951
.L2923:
	addl	$1, 41104(%r12)
	movq	-16(%r13), %rcx
	leaq	-8(%r13), %rdx
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	testb	$1, %cl
	jne	.L2952
	sarq	$32, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	andl	$1, %ecx
	call	_ZN2v88internal12_GLOBAL__N_114DeletePropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_12LanguageModeE
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L2928
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2928:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2953
.L2918:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2954
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2951:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2955
.L2924:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2925
	movq	(%rdi), %rax
	call	*8(%rax)
.L2925:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2926
	movq	(%rdi), %rax
	call	*8(%rax)
.L2926:
	leaq	.LC90(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2923
	.p2align 4,,10
	.p2align 3
.L2950:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2956
.L2922:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_DeletePropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic732(%rip)
	jmp	.L2921
	.p2align 4,,10
	.p2align 3
.L2949:
	movq	40960(%rsi), %rax
	movl	$421, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2919
	.p2align 4,,10
	.p2align 3
.L2952:
	leaq	.LC91(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2953:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2918
	.p2align 4,,10
	.p2align 3
.L2956:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2922
	.p2align 4,,10
	.p2align 3
.L2955:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC90(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2924
.L2954:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27233:
	.size	_ZN2v88internalL28Stats_Runtime_DeletePropertyEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL28Stats_Runtime_DeletePropertyEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb
	.type	_ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb, @function
_ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb:
.LFB21401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpq	%rax, 104(%rdi)
	jne	.L2976
.L2958:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE@PLT
	xorl	%eax, %eax
.L2961:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2977
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2976:
	.cfi_restore_state
	cmpq	%rax, 88(%rdi)
	je	.L2958
	leaq	-144(%rbp), %r15
	movq	%rcx, %rbx
	movl	$3, %r9d
	movq	%rdx, %rcx
	leaq	-145(%rbp), %r8
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r15, %rdi
	movb	$0, -145(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	cmpb	$0, -145(%rbp)
	jne	.L2978
	xorl	%eax, %eax
	jmp	.L2961
	.p2align 4,,10
	.p2align 3
.L2978:
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movl	-140(%rbp), %edx
	testq	%rbx, %rbx
	je	.L2963
	cmpl	$4, %edx
	setne	(%rbx)
.L2963:
	cmpl	$4, %edx
	jne	.L2961
	movq	(%r14), %rdx
	testb	$1, %dl
	je	.L2961
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L2961
	testb	$16, 11(%rdx)
	je	.L2961
	movq	41112(%r12), %rdi
	movq	15(%rdx), %r14
	testq	%rdi, %rdi
	je	.L2966
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L2967:
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$259, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	jmp	.L2961
.L2966:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L2979
.L2968:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rdx)
	jmp	.L2967
.L2977:
	call	__stack_chk_fail@PLT
.L2979:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L2968
	.cfi_endproc
.LFE21401:
	.size	_ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb, .-_ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb
	.section	.rodata._ZN2v88internalL25Stats_Runtime_GetPropertyEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC92:
	.string	"V8.Runtime_Runtime_GetProperty"
	.section	.text._ZN2v88internalL25Stats_Runtime_GetPropertyEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Stats_Runtime_GetPropertyEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL25Stats_Runtime_GetPropertyEiPmPNS0_7IsolateE.isra.0:
.LFB27338:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3098
.L2981:
	movq	_ZZN2v88internalL25Stats_Runtime_GetPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic575(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3099
.L2983:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3100
.L2985:
	addl	$1, 41104(%r12)
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	testb	$1, %al
	jne	.L3101
.L2990:
	movq	0(%r13), %r15
	testb	$1, %r15b
	jne	.L3102
.L3012:
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb
	testq	%rax, %rax
	je	.L3103
	movq	(%rax), %r15
.L3021:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3060
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3060:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3104
.L2980:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3105
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3099:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3106
.L2984:
	movq	%rbx, _ZZN2v88internalL25Stats_Runtime_GetPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic575(%rip)
	jmp	.L2983
	.p2align 4,,10
	.p2align 3
.L3102:
	movq	-1(%r15), %rax
	cmpw	$1024, 11(%rax)
	movq	-1(%r15), %rax
	jbe	.L3107
	cmpw	$1026, 11(%rax)
	je	.L2999
	movq	-1(%r15), %rax
	cmpw	$1026, 11(%rax)
	je	.L3108
	movq	-1(%r15), %rax
	movzbl	13(%rax), %ecx
	shrb	$5, %cl
	andl	$1, %ecx
.L3004:
	movq	(%rdx), %rsi
	movq	%rsi, %rax
	notq	%rax
	andl	$1, %eax
	testb	%cl, %cl
	je	.L3109
.L3005:
	testb	%al, %al
	je	.L3012
	.p2align 4,,10
	.p2align 3
.L3011:
	movq	0(%r13), %rdi
	movq	-1(%rdi), %rax
	movzbl	14(%rax), %ecx
	shrl	$3, %ecx
	leal	-4(%rcx), %eax
	cmpb	$1, %al
	ja	.L3012
	movq	%rsi, %rax
	movq	15(%rdi), %rsi
	sarq	$32, %rax
	cmpl	%eax, 11(%rsi)
	jg	.L3012
	cmpb	$4, %cl
	movq	%r13, %rdi
	movq	%rdx, -184(%rbp)
	setne	%sil
	addl	$2, %esi
	movzbl	%sil, %esi
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	-184(%rbp), %rdx
	jmp	.L3012
	.p2align 4,,10
	.p2align 3
.L3107:
	cmpw	$63, 11(%rax)
	ja	.L3012
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L3012
	sarq	$32, %rax
	movq	%rax, %rcx
	js	.L3012
	cmpl	11(%r15), %eax
	jge	.L3012
	movq	-1(%r15), %rax
	cmpw	$63, 11(%rax)
	ja	.L3036
	movq	-1(%r15), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	je	.L3110
.L3036:
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L3043
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L3043
	movq	41112(%r12), %rdi
	movq	15(%rax), %r15
	testq	%rdi, %rdi
	je	.L3046
	movq	%r15, %rsi
	movq	%rcx, -184(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-184(%rbp), %rcx
	movq	%rax, %r13
.L3043:
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L3049
	leaq	.L3051(%rip), %rsi
	movzwl	%ax, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internalL25Stats_Runtime_GetPropertyEiPmPNS0_7IsolateE.isra.0,"a",@progbits
	.align 4
	.align 4
.L3051:
	.long	.L3057-.L3051
	.long	.L3054-.L3051
	.long	.L3056-.L3051
	.long	.L3052-.L3051
	.long	.L3049-.L3051
	.long	.L3050-.L3051
	.long	.L3049-.L3051
	.long	.L3049-.L3051
	.long	.L3055-.L3051
	.long	.L3054-.L3051
	.long	.L3053-.L3051
	.long	.L3052-.L3051
	.long	.L3049-.L3051
	.long	.L3050-.L3051
	.section	.text._ZN2v88internalL25Stats_Runtime_GetPropertyEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L3101:
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L2990
	movq	%rax, -168(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L2992
	testb	$2, %al
	jne	.L2990
.L2992:
	leaq	-172(%rbp), %rsi
	leaq	-168(%rbp), %rdi
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-184(%rbp), %rdx
	testb	%al, %al
	je	.L2990
	movl	-172(%rbp), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	jmp	.L2990
	.p2align 4,,10
	.p2align 3
.L3100:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3111
.L2986:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2987
	movq	(%rdi), %rax
	call	*8(%rax)
.L2987:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2988
	movq	(%rdi), %rax
	call	*8(%rax)
.L2988:
	leaq	.LC92(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2985
	.p2align 4,,10
	.p2align 3
.L3103:
	movq	312(%r12), %r15
	jmp	.L3021
	.p2align 4,,10
	.p2align 3
.L3104:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2980
	.p2align 4,,10
	.p2align 3
.L3098:
	movq	40960(%rsi), %rax
	movl	$426, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2981
	.p2align 4,,10
	.p2align 3
.L3106:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2984
	.p2align 4,,10
	.p2align 3
.L3111:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC92(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2986
	.p2align 4,,10
	.p2align 3
.L3108:
	movq	%r15, %rcx
	leaq	-168(%rbp), %rdi
	movq	%rdx, -192(%rbp)
	andq	$-262144, %rcx
	movq	24(%rcx), %rax
	movq	%rcx, -184(%rbp)
	movq	-25128(%rax), %rax
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	-184(%rbp), %rcx
	movq	24(%rcx), %rsi
	movq	-1(%r15), %rcx
	movq	-192(%rbp), %rdx
	cmpw	$1024, 11(%rcx)
	je	.L3112
	movq	-1(%r15), %rcx
	movq	23(%rcx), %rcx
.L3003:
	cmpq	%rcx, %rax
	setne	%cl
	jmp	.L3004
.L3050:
	leaq	-168(%rbp), %rdi
	movl	%ecx, %esi
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal10ThinString3GetEi@PLT
.L3058:
	movzwl	%ax, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt@PLT
	movq	(%rax), %r15
	jmp	.L3021
.L3054:
	leaq	-168(%rbp), %rdi
	movl	%ecx, %esi
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal10ConsString3GetEi@PLT
	jmp	.L3058
.L3052:
	leaq	-168(%rbp), %rdi
	movl	%ecx, %esi
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	jmp	.L3058
.L3049:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3053:
	movq	15(%rdx), %rdi
	movq	%rcx, -184(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	-184(%rbp), %rcx
	movzbl	(%rax,%rcx), %eax
	jmp	.L3058
.L3055:
	leal	16(%rcx), %eax
	cltq
	movzbl	-1(%rdx,%rax), %eax
	jmp	.L3058
.L3057:
	leal	16(%rcx,%rcx), %eax
	cltq
	movzwl	-1(%rdx,%rax), %eax
	jmp	.L3058
.L3056:
	movq	15(%rdx), %rdi
	movq	%rcx, -184(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	-184(%rbp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	jmp	.L3058
	.p2align 4,,10
	.p2align 3
.L3109:
	testb	%al, %al
	jne	.L3011
	movq	-1(%rsi), %rax
	cmpw	$64, 11(%rax)
	jbe	.L3007
	movl	%ecx, %eax
	jmp	.L3005
	.p2align 4,,10
	.p2align 3
.L3007:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L3113
.L3008:
	movq	0(%r13), %rax
	movq	-1(%rax), %rcx
	cmpw	$1025, 11(%rcx)
	je	.L3114
	movq	-1(%rax), %rcx
	movl	15(%rcx), %ecx
	andl	$2097152, %ecx
	je	.L3012
	movq	7(%rax), %r15
	testb	$1, %r15b
	jne	.L3023
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %r15
.L3023:
	movq	(%rdx), %rcx
	movl	7(%rcx), %eax
	testb	$1, %al
	jne	.L3024
	shrl	$2, %eax
.L3025:
	movslq	35(%r15), %r8
	movq	88(%r12), %r9
	leaq	-1(%r15), %rdi
	movl	$1, %esi
	subl	$1, %r8d
	andl	%r8d, %eax
	movl	%eax, %r10d
	jmp	.L3028
	.p2align 4,,10
	.p2align 3
.L3115:
	leal	(%r10,%rsi), %eax
	addl	$1, %esi
	andl	%r8d, %eax
	movl	%eax, %r10d
.L3028:
	leal	(%r10,%r10,2), %ecx
	leal	56(,%rcx,8), %eax
	cltq
	movq	(%rax,%rdi), %rax
	cmpq	%rax, %r9
	je	.L3012
	cmpq	(%rdx), %rax
	jne	.L3115
	cmpl	$-1, %r10d
	je	.L3012
	leal	72(,%rcx,8), %eax
	movslq	%eax, %rcx
	movq	(%rcx,%rdi), %rcx
	btq	$32, %rcx
	jc	.L3012
	subl	$8, %eax
	cltq
	movq	(%rax,%rdi), %r15
	jmp	.L3021
	.p2align 4,,10
	.p2align 3
.L2999:
	movq	(%rdx), %rsi
	movq	%rsi, %rax
	notq	%rax
	andl	$1, %eax
	jmp	.L3005
.L3114:
	movq	7(%rax), %r15
	movq	(%rdx), %rax
	movl	7(%rax), %ecx
	testb	$1, %cl
	jne	.L3013
	shrl	$2, %ecx
.L3014:
	movslq	35(%r15), %r9
	movq	88(%r12), %r10
	leaq	-1(%r15), %r8
	movl	$1, %edi
	subl	$1, %r9d
	andl	%r9d, %ecx
	jmp	.L3017
	.p2align 4,,10
	.p2align 3
.L3116:
	addl	%edi, %ecx
	addl	$1, %edi
	andl	%r9d, %ecx
.L3017:
	leal	56(,%rcx,8), %eax
	cltq
	addq	%r8, %rax
	movq	(%rax), %rsi
	cmpq	%rsi, %r10
	je	.L3012
	movq	(%rdx), %r11
	cmpq	%r11, 7(%rsi)
	jne	.L3116
	cmpl	$-1, %ecx
	je	.L3012
	movq	(%rax), %rax
	testb	$1, 19(%rax)
	jne	.L3012
	movq	23(%rax), %r15
	cmpq	%r15, 96(%r12)
	je	.L3012
	jmp	.L3021
.L3113:
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rdx
	jmp	.L3008
.L3013:
	leaq	-168(%rbp), %rdi
	movq	%rdx, -184(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-184(%rbp), %rdx
	movl	%eax, %ecx
	jmp	.L3014
.L3024:
	leaq	-168(%rbp), %rdi
	movq	%rdx, -184(%rbp)
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-184(%rbp), %rdx
	jmp	.L3025
.L3110:
	movq	-1(%r15), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L3038
	movq	23(%r15), %rax
	cmpl	$0, 11(%rax)
	je	.L3038
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rcx, -184(%rbp)
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	-184(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L3043
.L3046:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3117
.L3048:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, 0(%r13)
	jmp	.L3043
.L3038:
	movq	41112(%r12), %rdi
	movq	15(%r15), %r15
	testq	%rdi, %rdi
	je	.L3040
	movq	%r15, %rsi
	movq	%rcx, -184(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-184(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L3036
.L3112:
	movq	-37488(%rsi), %rcx
	jmp	.L3003
.L3105:
	call	__stack_chk_fail@PLT
.L3040:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3118
.L3042:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, 0(%r13)
	jmp	.L3036
.L3117:
	movq	%r12, %rdi
	movq	%rcx, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L3048
.L3118:
	movq	%r12, %rdi
	movq	%rcx, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L3042
	.cfi_endproc
.LFE27338:
	.size	_ZN2v88internalL25Stats_Runtime_GetPropertyEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL25Stats_Runtime_GetPropertyEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal7Runtime11HasPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Runtime11HasPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_
	.type	_ZN2v88internal7Runtime11HasPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_, @function
_ZN2v88internal7Runtime11HasPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_:
.LFB21402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$208, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L3120
.L3122:
	xorl	%r8d, %r8d
	movl	$65, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
.L3121:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3155
	addq	$208, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3120:
	.cfi_restore_state
	movq	-1(%rax), %rsi
	cmpw	$1023, 11(%rsi)
	jbe	.L3122
	movq	(%rdx), %rsi
	movq	%rdx, %r13
	testb	$1, %sil
	jne	.L3123
.L3126:
	movq	%rdx, %rsi
	movq	%r12, %rdi
	movq	%rcx, -232(%rbp)
	call	_ZN2v88internal6Object13ConvertToNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3124
	movq	-232(%rbp), %rcx
	movq	(%rcx), %rax
.L3125:
	andq	$-262144, %rax
	movq	24(%rax), %r14
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	subq	$37592, %r14
	cmpw	$63, 11(%rdx)
	jbe	.L3156
.L3128:
	movq	0(%r13), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rsi
	cmpw	$64, 11(%rsi)
	je	.L3157
.L3132:
	movl	%eax, -208(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -196(%rbp)
	movq	%r14, -184(%rbp)
	movq	0(%r13), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L3158
.L3133:
	movq	%r13, -176(%rbp)
	leaq	-208(%rbp), %r13
	movq	%r13, %rdi
	movq	$0, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movq	$-1, -136(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L3131:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	je	.L3124
	leaq	112(%r12), %rdx
	addq	$120, %r12
	shrw	$8, %ax
	movq	%rdx, %rax
	cmove	%r12, %rax
	jmp	.L3121
	.p2align 4,,10
	.p2align 3
.L3124:
	xorl	%eax, %eax
	jmp	.L3121
	.p2align 4,,10
	.p2align 3
.L3123:
	movq	-1(%rsi), %rsi
	cmpw	$64, 11(%rsi)
	ja	.L3126
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3157:
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
	jmp	.L3132
	.p2align 4,,10
	.p2align 3
.L3156:
	movq	%rax, -128(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L3129
	testb	$2, %al
	jne	.L3128
.L3129:
	leaq	-128(%rbp), %r15
	leaq	-212(%rbp), %rsi
	movq	%rcx, -232(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-232(%rbp), %rcx
	testb	%al, %al
	je	.L3128
	movabsq	$824633720832, %rax
	movq	%r15, %rdi
	movq	%r14, -104(%rbp)
	movq	%rax, -116(%rbp)
	movl	-212(%rbp), %eax
	movl	$3, -128(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movl	%eax, -56(%rbp)
	movl	$-1, -52(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r13, -96(%rbp)
	movdqa	-128(%rbp), %xmm0
	leaq	-208(%rbp), %r13
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movdqa	-64(%rbp), %xmm4
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	jmp	.L3131
	.p2align 4,,10
	.p2align 3
.L3158:
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -232(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-232(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L3133
.L3155:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21402:
	.size	_ZN2v88internal7Runtime11HasPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_, .-_ZN2v88internal7Runtime11HasPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_
	.section	.text._ZN2v88internal7Runtime20DeleteObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEENS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Runtime20DeleteObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEENS0_12LanguageModeE
	.type	_ZN2v88internal7Runtime20DeleteObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEENS0_12LanguageModeE, @function
_ZN2v88internal7Runtime20DeleteObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEENS0_12LanguageModeE:
.LFB21404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12_GLOBAL__N_124DeleteObjectPropertyFastEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEE
	testb	%al, %al
	je	.L3160
	movl	$257, %eax
.L3161:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L3165
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3160:
	.cfi_restore_state
	leaq	-144(%rbp), %r15
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	$1, %r9d
	leaq	-145(%rbp), %r8
	movq	%r15, %rdi
	movb	$0, -145(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	cmpb	$0, -145(%rbp)
	jne	.L3162
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L3161
	.p2align 4,,10
	.p2align 3
.L3162:
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal10JSReceiver14DeletePropertyEPNS0_14LookupIteratorENS0_12LanguageModeE@PLT
	jmp	.L3161
.L3165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21404:
	.size	_ZN2v88internal7Runtime20DeleteObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEENS0_12LanguageModeE, .-_ZN2v88internal7Runtime20DeleteObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEENS0_12LanguageModeE
	.section	.text._ZN2v88internal18Runtime_ObjectKeysEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Runtime_ObjectKeysEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Runtime_ObjectKeysEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Runtime_ObjectKeysEiPmPNS0_7IsolateE:
.LFB21406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3178
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3168
.L3171:
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3177
.L3170:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	$18, %edx
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	testq	%rax, %rax
	je	.L3177
	movq	(%rax), %r14
.L3172:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3166
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3166:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3168:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3171
	jmp	.L3170
	.p2align 4,,10
	.p2align 3
.L3177:
	movq	312(%r12), %r14
	jmp	.L3172
	.p2align 4,,10
	.p2align 3
.L3178:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL24Stats_Runtime_ObjectKeysEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21406:
	.size	_ZN2v88internal18Runtime_ObjectKeysEiPmPNS0_7IsolateE, .-_ZN2v88internal18Runtime_ObjectKeysEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateE:
.LFB21409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3191
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3181
.L3184:
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3190
.L3183:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	$16, %edx
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	testq	%rax, %rax
	je	.L3190
	movq	(%rax), %r14
.L3185:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3179
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3179:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3181:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3184
	jmp	.L3183
	.p2align 4,,10
	.p2align 3
.L3190:
	movq	312(%r12), %r14
	jmp	.L3185
	.p2align 4,,10
	.p2align 3
.L3191:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Stats_Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21409:
	.size	_ZN2v88internal33Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateE, .-_ZN2v88internal33Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal40Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateE
	.type	_ZN2v88internal40Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateE, @function
_ZN2v88internal40Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateE:
.LFB21412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3215
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3195
.L3198:
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3204
.L3197:
	movq	0(%r13), %rax
	movq	-1(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3200
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3201:
	movl	15(%rsi), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	movl	%edx, %r15d
	je	.L3206
	movq	(%rax), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal3Map28NumberOfEnumerablePropertiesEv@PLT
	cmpl	%r15d, %eax
	je	.L3216
.L3206:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$16, %edx
.L3214:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	testq	%rax, %rax
	je	.L3204
	movq	(%rax), %r13
.L3199:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3192
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3192:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3217
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3200:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L3218
.L3202:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3201
	.p2align 4,,10
	.p2align 3
.L3195:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3198
	jmp	.L3197
	.p2align 4,,10
	.p2align 3
.L3204:
	movq	312(%r12), %r13
	jmp	.L3199
	.p2align 4,,10
	.p2align 3
.L3215:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL46Stats_Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L3192
	.p2align 4,,10
	.p2align 3
.L3216:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$18, %edx
	jmp	.L3214
	.p2align 4,,10
	.p2align 3
.L3218:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L3202
.L3217:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21412:
	.size	_ZN2v88internal40Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateE, .-_ZN2v88internal40Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateE:
.LFB21415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3329
	addl	$1, 41104(%rdx)
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r14
	movq	41088(%rdx), %r15
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3222
	sarq	$32, %rax
	js	.L3236
	movl	%eax, -148(%rbp)
.L3224:
	movq	0(%r13), %rax
	xorl	%r14d, %r14d
	movl	$1, %edx
	testb	$1, %al
	je	.L3285
.L3332:
	movq	-1(%rax), %rcx
	cmpw	$1027, 11(%rcx)
	je	.L3242
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L3330
	testb	%dl, %dl
	je	.L3318
	movabsq	$824633720832, %rax
	movb	%dl, -169(%rbp)
	leaq	-144(%rbp), %r8
	movq	%rax, -132(%rbp)
	movl	-148(%rbp), %eax
	movq	%r8, %rdi
	movq	%r8, -168(%rbp)
	movl	$0, -144(%rbp)
	movq	%r12, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	-168(%rbp), %r8
	movzbl	-169(%rbp), %edx
.L3255:
	movq	%r8, %rdi
	movb	%dl, -169(%rbp)
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	movq	-168(%rbp), %r8
	movzbl	-169(%rbp), %edx
	testb	%al, %al
	je	.L3325
	shrw	$8, %ax
	jne	.L3323
	movq	0(%r13), %rax
	movq	-1(%rax), %rax
	cmpw	$1026, 11(%rax)
	je	.L3260
	movzbl	13(%rax), %eax
	testb	%dl, %dl
	je	.L3263
	testb	$8, %al
	je	.L3324
.L3264:
	movabsq	$824633720832, %rax
	movq	%r8, %rdi
	movq	%r8, -168(%rbp)
	movq	%rax, -132(%rbp)
	movl	-148(%rbp), %eax
	movl	$1, -144(%rbp)
	movq	%r12, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	-168(%rbp), %r8
.L3267:
	movq	%r8, %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	jne	.L3281
	.p2align 4,,10
	.p2align 3
.L3325:
	movq	312(%r12), %r13
	jmp	.L3237
	.p2align 4,,10
	.p2align 3
.L3222:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L3322
	movsd	7(%rax), %xmm0
	movsd	.LC70(%rip), %xmm2
	addsd	%xmm0, %xmm2
	movq	%xmm2, %rcx
	movq	%xmm2, %rdx
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	jne	.L3322
	movl	%edx, %ecx
	pxor	%xmm1, %xmm1
	movd	%xmm2, -148(%rbp)
	cvtsi2sdq	%rcx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L3305
	jne	.L3305
	cmpl	$-1, %edx
	jne	.L3224
.L3322:
	xorl	%edx, %edx
.L3226:
	testb	%dl, %dl
	jne	.L3236
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	jbe	.L3235
	.p2align 4,,10
	.p2align 3
.L3236:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object13ConvertToNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L3325
.L3235:
	movq	(%r14), %rax
	xorl	%edx, %edx
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	jbe	.L3331
.L3232:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L3332
.L3285:
	cmpq	%rax, 104(%r12)
	jne	.L3333
.L3297:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$173, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L3237:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3219
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3219:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3334
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3330:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	je	.L3335
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L3285
	leaq	37592(%r12), %r13
	testb	%dl, %dl
	jne	.L3336
	movq	(%r14), %rax
	movq	2768(%r12), %rsi
	cmpq	%rsi, %rax
	je	.L3337
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$65504, %edx
	jne	.L3294
	movq	-1(%rsi), %rdx
	movzwl	11(%rdx), %edx
	andl	$65504, %edx
	je	.L3292
.L3294:
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	je	.L3292
	movq	-1(%rsi), %rdx
	cmpw	$64, 11(%rdx)
	je	.L3292
	leaq	-144(%rbp), %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal6String10SlowEqualsES1_@PLT
	movzbl	%al, %eax
	.p2align 4,,10
	.p2align 3
.L3288:
	subq	$37592, %r13
	testl	%eax, %eax
	je	.L3295
.L3290:
	movq	112(%r13), %r13
	jmp	.L3237
	.p2align 4,,10
	.p2align 3
.L3242:
	testq	%r14, %r14
	je	.L3324
.L3276:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE@PLT
	testb	%al, %al
	je	.L3325
.L3281:
	shrw	$8, %ax
	je	.L3324
.L3323:
	movq	112(%r12), %r13
	jmp	.L3237
	.p2align 4,,10
	.p2align 3
.L3333:
	cmpq	%rax, 88(%r12)
	je	.L3297
.L3324:
	movq	120(%r12), %r13
	jmp	.L3237
	.p2align 4,,10
	.p2align 3
.L3331:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L3239
	testb	$2, %al
	jne	.L3232
.L3239:
	leaq	-148(%rbp), %rsi
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movl	%eax, %edx
	jmp	.L3232
	.p2align 4,,10
	.p2align 3
.L3329:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL34Stats_Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L3219
	.p2align 4,,10
	.p2align 3
.L3335:
	testq	%r14, %r14
	jne	.L3276
	movl	-148(%rbp), %r8d
	testl	%r8d, %r8d
	js	.L3277
	movq	%r8, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	movl	%r8d, -168(%rbp)
	salq	$32, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb@PLT
	movl	-168(%rbp), %r8d
	movq	%rax, %r14
.L3278:
	movq	(%r14), %rax
	movl	11(%rax), %esi
	cmpl	$10, %esi
	jg	.L3276
	cmpl	$3, 7(%rax)
	jne	.L3276
	movl	%r8d, %edi
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movq	(%r14), %rdx
	movl	%eax, 7(%rdx)
	jmp	.L3276
	.p2align 4,,10
	.p2align 3
.L3318:
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	(%r14), %rax
	subq	$37592, %rdi
	movq	-1(%rax), %rax
	movabsq	$824633720832, %rax
	movl	$0, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	%rdi, -120(%rbp)
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %ecx
	movq	%r14, %rax
	andl	$-32, %ecx
	cmpl	$32, %ecx
	je	.L3338
.L3256:
	leaq	-144(%rbp), %r8
	movb	%dl, -169(%rbp)
	movq	%r8, %rdi
	movq	%r8, -168(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movzbl	-169(%rbp), %edx
	movq	-168(%rbp), %r8
	jmp	.L3255
	.p2align 4,,10
	.p2align 3
.L3336:
	movl	-148(%rbp), %edi
	cmpl	%edi, 11(%rax)
	seta	%al
	movzbl	%al, %eax
	jmp	.L3288
	.p2align 4,,10
	.p2align 3
.L3338:
	movq	%r14, %rsi
	movb	%dl, -168(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movzbl	-168(%rbp), %edx
	jmp	.L3256
	.p2align 4,,10
	.p2align 3
.L3292:
	subq	$37592, %r13
.L3295:
	movq	120(%r13), %r13
	jmp	.L3237
	.p2align 4,,10
	.p2align 3
.L3260:
	testb	%dl, %dl
	jne	.L3264
.L3266:
	movq	0(%r13), %rax
	movq	(%r14), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$1, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L3268
	movl	11(%rdx), %eax
	notl	%eax
	andl	$1, %eax
.L3268:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	%rdi, -120(%rbp)
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L3339
.L3269:
	movq	%r8, %rdi
	movq	%r8, -168(%rbp)
	movq	%r14, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	-168(%rbp), %r8
	jmp	.L3267
	.p2align 4,,10
	.p2align 3
.L3337:
	subq	$37592, %r13
	jmp	.L3290
.L3263:
	testb	$4, %al
	jne	.L3266
	jmp	.L3324
.L3277:
	movl	%r8d, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	%r8d, -168(%rbp)
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movl	-168(%rbp), %r8d
	movq	%rax, %r14
	jmp	.L3278
.L3305:
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L3226
.L3339:
	movq	%r14, %rsi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-168(%rbp), %r8
	movq	%rax, %r14
	jmp	.L3269
.L3334:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21415:
	.size	_ZN2v88internal28Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateE:
.LFB21418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3356
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	leaq	-8(%rsi), %r15
	leaq	-16(%rsi), %rcx
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	movq	7(%rax), %r8
	testb	$1, %r8b
	jne	.L3344
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %r8
.L3344:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3345
	movq	%r8, %rsi
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %rsi
.L3346:
	xorl	%r9d, %r9d
	movl	$192, %r8d
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE3AddEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_4NameEEENS7_INS0_6ObjectEEENS0_15PropertyDetailsEPi@PLT
	movq	0(%r13), %rdx
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE@PLT
	movq	-16(%r13), %r13
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L3340
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3340:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3357
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3345:
	.cfi_restore_state
	movq	%rbx, %rsi
	cmpq	%r14, %rbx
	je	.L3358
.L3347:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L3346
	.p2align 4,,10
	.p2align 3
.L3356:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL35Stats_Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L3340
	.p2align 4,,10
	.p2align 3
.L3358:
	movq	%r12, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L3347
.L3357:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21418:
	.size	_ZN2v88internal29Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Runtime_ObjectCreateEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Runtime_ObjectCreateEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Runtime_ObjectCreateEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Runtime_ObjectCreateEiPmPNS0_7IsolateE:
.LFB21421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3372
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rax
	cmpq	104(%rdx), %rax
	je	.L3361
	testb	$1, %al
	jne	.L3362
.L3364:
	movq	%r13, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$116, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L3363:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3359
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3359:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3362:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3364
	.p2align 4,,10
	.p2align 3
.L3361:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject12ObjectCreateEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3371
	movq	88(%r12), %rax
	leaq	-8(%r13), %rdx
	cmpq	%rax, -8(%r13)
	je	.L3366
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver16DefinePropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_@PLT
	testq	%rax, %rax
	je	.L3371
	movq	(%rax), %r13
	jmp	.L3363
	.p2align 4,,10
	.p2align 3
.L3366:
	movq	(%rsi), %r13
	jmp	.L3363
	.p2align 4,,10
	.p2align 3
.L3371:
	movq	312(%r12), %r13
	jmp	.L3363
	.p2align 4,,10
	.p2align 3
.L3372:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movq	%rdx, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL26Stats_Runtime_ObjectCreateEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21421:
	.size	_ZN2v88internal20Runtime_ObjectCreateEiPmPNS0_7IsolateE, .-_ZN2v88internal20Runtime_ObjectCreateEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE
	.type	_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE, @function
_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE:
.LFB21423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpq	%rax, 104(%rdi)
	jne	.L3391
.L3374:
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r14, %rdx
	movl	$86, %esi
.L3389:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L3390:
	xorl	%eax, %eax
.L3377:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L3392
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3391:
	.cfi_restore_state
	movl	%r8d, -164(%rbp)
	cmpq	%rax, 88(%rdi)
	je	.L3374
	leaq	-144(%rbp), %rdi
	movq	%rcx, %rbx
	movq	%r9, %r15
	movq	%rdx, %rcx
	movl	$3, %r9d
	movq	%rsi, %rdx
	leaq	-145(%rbp), %r8
	movq	%r12, %rsi
	movq	%rdi, -176(%rbp)
	movb	$0, -145(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	cmpb	$0, -145(%rbp)
	movq	-176(%rbp), %rdi
	movl	-164(%rbp), %r10d
	je	.L3390
	cmpl	$4, -140(%rbp)
	jne	.L3380
	movq	(%r14), %rax
	testb	$1, %al
	je	.L3380
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	jne	.L3380
	testb	$16, 11(%rax)
	je	.L3380
	movq	41112(%r12), %rdi
	movq	15(%rax), %r14
	testq	%rdi, %rdi
	je	.L3381
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L3382:
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$260, %esi
	jmp	.L3389
	.p2align 4,,10
	.p2align 3
.L3380:
	movq	%r15, %rcx
	movl	%r10d, %edx
	movq	%rbx, %rsi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movl	%eax, %r8d
	movq	%rbx, %rax
	testb	%r8b, %r8b
	jne	.L3377
	jmp	.L3390
.L3381:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L3393
.L3383:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rdx)
	jmp	.L3382
.L3392:
	call	__stack_chk_fail@PLT
.L3393:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L3383
	.cfi_endproc
.LFE21423:
	.size	_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE, .-_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE
	.section	.rodata._ZN2v88internalL30Stats_Runtime_SetKeyedPropertyEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC93:
	.string	"V8.Runtime_Runtime_SetKeyedProperty"
	.section	.text._ZN2v88internalL30Stats_Runtime_SetKeyedPropertyEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_SetKeyedPropertyEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL30Stats_Runtime_SetKeyedPropertyEiPmPNS0_7IsolateE.isra.0:
.LFB27257:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3427
.L3395:
	movq	_ZZN2v88internalL30Stats_Runtime_SetKeyedPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic665(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3428
.L3397:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3429
.L3399:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	-8(%r13), %rdx
	movq	%r13, %rsi
	addl	$1, 41104(%r12)
	leaq	-16(%r13), %rcx
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	call	_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE
	testq	%rax, %rax
	je	.L3430
	movq	(%rax), %r13
.L3404:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L3407
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3407:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3431
.L3394:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3432
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3429:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3433
.L3400:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3401
	movq	(%rdi), %rax
	call	*8(%rax)
.L3401:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3402
	movq	(%rdi), %rax
	call	*8(%rax)
.L3402:
	leaq	.LC93(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L3399
	.p2align 4,,10
	.p2align 3
.L3428:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3434
.L3398:
	movq	%rbx, _ZZN2v88internalL30Stats_Runtime_SetKeyedPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic665(%rip)
	jmp	.L3397
	.p2align 4,,10
	.p2align 3
.L3430:
	movq	312(%r12), %r13
	jmp	.L3404
	.p2align 4,,10
	.p2align 3
.L3431:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3394
	.p2align 4,,10
	.p2align 3
.L3427:
	movq	40960(%rsi), %rax
	movl	$453, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3395
	.p2align 4,,10
	.p2align 3
.L3434:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3398
	.p2align 4,,10
	.p2align 3
.L3433:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC93(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3400
.L3432:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27257:
	.size	_ZN2v88internalL30Stats_Runtime_SetKeyedPropertyEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL30Stats_Runtime_SetKeyedPropertyEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL30Stats_Runtime_SetNamedPropertyEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC94:
	.string	"V8.Runtime_Runtime_SetNamedProperty"
	.section	.text._ZN2v88internalL30Stats_Runtime_SetNamedPropertyEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_SetNamedPropertyEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL30Stats_Runtime_SetNamedPropertyEiPmPNS0_7IsolateE.isra.0:
.LFB27258:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3468
.L3436:
	movq	_ZZN2v88internalL30Stats_Runtime_SetNamedPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic678(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3469
.L3438:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3470
.L3440:
	xorl	%r9d, %r9d
	leaq	-8(%r13), %rdx
	leaq	-16(%r13), %rcx
	movq	%r13, %rsi
	addl	$1, 41104(%r12)
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	call	_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE
	testq	%rax, %rax
	je	.L3471
	movq	(%rax), %r13
.L3445:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L3448
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3448:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3472
.L3435:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3473
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3470:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3474
.L3441:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3442
	movq	(%rdi), %rax
	call	*8(%rax)
.L3442:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3443
	movq	(%rdi), %rax
	call	*8(%rax)
.L3443:
	leaq	.LC94(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L3440
	.p2align 4,,10
	.p2align 3
.L3469:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3475
.L3439:
	movq	%rbx, _ZZN2v88internalL30Stats_Runtime_SetNamedPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic678(%rip)
	jmp	.L3438
	.p2align 4,,10
	.p2align 3
.L3471:
	movq	312(%r12), %r13
	jmp	.L3445
	.p2align 4,,10
	.p2align 3
.L3472:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3435
	.p2align 4,,10
	.p2align 3
.L3468:
	movq	40960(%rsi), %rax
	movl	$454, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3436
	.p2align 4,,10
	.p2align 3
.L3475:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3439
	.p2align 4,,10
	.p2align 3
.L3474:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC94(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3441
.L3473:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27258:
	.size	_ZN2v88internalL30Stats_Runtime_SetNamedPropertyEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL30Stats_Runtime_SetNamedPropertyEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal28Runtime_InternalSetPrototypeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_InternalSetPrototypeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_InternalSetPrototypeEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_InternalSetPrototypeEiPmPNS0_7IsolateE:
.LFB21425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3485
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3478
.L3479:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3478:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3479
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-8(%rsi), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE@PLT
	testb	%al, %al
	jne	.L3480
	movq	312(%r12), %r13
.L3481:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3476
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3476:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3480:
	.cfi_restore_state
	movq	0(%r13), %r13
	jmp	.L3481
	.p2align 4,,10
	.p2align 3
.L3485:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL34Stats_Runtime_InternalSetPrototypeEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21425:
	.size	_ZN2v88internal28Runtime_InternalSetPrototypeEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_InternalSetPrototypeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal49Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal49Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal49Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateE, @function
_ZN2v88internal49Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateE:
.LFB21428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3499
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3500
.L3488:
	leaq	.LC42(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3500:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L3488
	movq	-8(%rsi), %rcx
	testb	$1, %cl
	jne	.L3501
	sarq	$32, %rcx
	cmpq	$100000, %rcx
	jle	.L3491
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate21ThrowIllegalOperationEv@PLT
	movq	%rax, %r13
.L3492:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3486
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3486:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3491:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	je	.L3502
.L3494:
	movq	%rax, %r13
	jmp	.L3492
	.p2align 4,,10
	.p2align 3
.L3502:
	movq	-1(%rax), %rdx
	cmpw	$1026, 11(%rdx)
	je	.L3494
	leaq	.LC81(%rip), %r8
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject19NormalizePropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeEiPKc@PLT
	movq	0(%r13), %rax
	jmp	.L3494
	.p2align 4,,10
	.p2align 3
.L3499:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movq	%rdx, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL55Stats_Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L3501:
	.cfi_restore_state
	leaq	.LC39(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21428:
	.size	_ZN2v88internal49Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateE, .-_ZN2v88internal49Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Runtime_ObjectValuesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Runtime_ObjectValuesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Runtime_ObjectValuesEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Runtime_ObjectValuesEiPmPNS0_7IsolateE:
.LFB21431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %r8d
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3512
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3505
.L3506:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3505:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3506
	movl	$18, %esi
	movl	$1, %edx
	call	_ZN2v88internal10JSReceiver12GetOwnValuesENS0_6HandleIS1_EENS0_14PropertyFilterEb@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3513
	movq	(%rax), %rax
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
.L3508:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3503
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3503:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3513:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L3508
	.p2align 4,,10
	.p2align 3
.L3512:
	popq	%rbx
	movl	%r8d, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL26Stats_Runtime_ObjectValuesEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21431:
	.size	_ZN2v88internal20Runtime_ObjectValuesEiPmPNS0_7IsolateE, .-_ZN2v88internal20Runtime_ObjectValuesEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateE:
.LFB21434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %r8d
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3523
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3516
.L3517:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3516:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3517
	movl	$18, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal10JSReceiver12GetOwnValuesENS0_6HandleIS1_EENS0_14PropertyFilterEb@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3524
	movq	(%rax), %rax
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
.L3519:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3514
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3514:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3524:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L3519
	.p2align 4,,10
	.p2align 3
.L3523:
	popq	%rbx
	movl	%r8d, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL38Stats_Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21434:
	.size	_ZN2v88internal32Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateE, .-_ZN2v88internal32Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_ObjectEntriesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_ObjectEntriesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_ObjectEntriesEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_ObjectEntriesEiPmPNS0_7IsolateE:
.LFB21437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %r8d
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3534
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3527
.L3528:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3527:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3528
	movl	$18, %esi
	movl	$1, %edx
	call	_ZN2v88internal10JSReceiver13GetOwnEntriesENS0_6HandleIS1_EENS0_14PropertyFilterEb@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3535
	movq	(%rax), %rax
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
.L3530:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3525
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3525:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3535:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L3530
	.p2align 4,,10
	.p2align 3
.L3534:
	popq	%rbx
	movl	%r8d, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL27Stats_Runtime_ObjectEntriesEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21437:
	.size	_ZN2v88internal21Runtime_ObjectEntriesEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_ObjectEntriesEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateE:
.LFB21440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %r8d
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3545
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3538
.L3539:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3538:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3539
	movl	$18, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal10JSReceiver13GetOwnEntriesENS0_6HandleIS1_EENS0_14PropertyFilterEb@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3546
	movq	(%rax), %rax
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
.L3541:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3536
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3536:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3546:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L3541
	.p2align 4,,10
	.p2align 3
.L3545:
	popq	%rbx
	movl	%r8d, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Stats_Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21440:
	.size	_ZN2v88internal33Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateE, .-_ZN2v88internal33Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateE:
.LFB21443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3564
	movl	41104(%rdx), %eax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L3549
.L3563:
	movq	%rbx, %rdx
.L3554:
	movq	120(%r12), %r13
.L3555:
	movq	%r14, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L3547
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3547:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3549:
	.cfi_restore_state
	movq	-1(%rdx), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L3563
	call	_ZN2v88internal10JSReceiver12IsExtensibleENS0_6HandleIS1_EE@PLT
	movzbl	%ah, %ecx
	testb	%al, %al
	jne	.L3552
	movl	41104(%r12), %eax
	movq	312(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L3555
.L3552:
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	subl	$1, %eax
	testb	%cl, %cl
	je	.L3554
	movq	112(%r12), %r13
	jmp	.L3555
	.p2align 4,,10
	.p2align 3
.L3564:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Stats_Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21443:
	.size	_ZN2v88internal26Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal40Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal40Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateE, @function
_ZN2v88internal40Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateE:
.LFB21446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3574
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3567
.L3568:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3567:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3568
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE@PLT
	testb	%al, %al
	jne	.L3569
	movq	312(%r12), %r13
.L3570:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3565
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3565:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3569:
	.cfi_restore_state
	movq	0(%r13), %r13
	jmp	.L3570
	.p2align 4,,10
	.p2align 3
.L3574:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL46Stats_Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21446:
	.size	_ZN2v88internal40Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateE, .-_ZN2v88internal40Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal44Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal44Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal44Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateE, @function
_ZN2v88internal44Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateE:
.LFB21449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %r8d
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3584
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3577
.L3578:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3577:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3578
	movl	$1, %esi
	call	_ZN2v88internal10JSReceiver17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE@PLT
	testb	%al, %al
	je	.L3585
	movzbl	%ah, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r14
.L3580:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3575
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3575:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3585:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L3580
	.p2align 4,,10
	.p2align 3
.L3584:
	popq	%rbx
	movl	%r8d, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL50Stats_Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21449:
	.size	_ZN2v88internal44Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateE, .-_ZN2v88internal44Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateE:
.LFB21452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3596
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	testb	$1, %al
	jne	.L3597
.L3588:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3597:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3588
	movq	%rdx, %rdi
	call	_ZN2v88internal10JSReceiver12GetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EE
	testq	%rax, %rax
	je	.L3598
	movq	(%rax), %r14
.L3591:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L3586
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3586:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3598:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L3591
	.p2align 4,,10
	.p2align 3
.L3596:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL38Stats_Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21452:
	.size	_ZN2v88internal32Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateE, .-_ZN2v88internal32Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal37Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal37Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal37Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateE, @function
_ZN2v88internal37Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateE:
.LFB21455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3608
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3601
.L3602:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3601:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3602
	xorl	%ecx, %ecx
	leaq	-8(%rsi), %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE@PLT
	testb	%al, %al
	jne	.L3603
	movq	312(%r12), %r13
.L3604:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3599
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3599:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3603:
	.cfi_restore_state
	movq	0(%r13), %r13
	jmp	.L3604
	.p2align 4,,10
	.p2align 3
.L3608:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL43Stats_Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21455:
	.size	_ZN2v88internal37Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateE, .-_ZN2v88internal37Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal41Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal41Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal41Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateE, @function
_ZN2v88internal41Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateE:
.LFB21458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %r8d
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3618
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3611
.L3612:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3611:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3612
	leaq	-8(%rsi), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	call	_ZN2v88internal10JSReceiver12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE@PLT
	testb	%al, %al
	je	.L3619
	movzbl	%ah, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r14
.L3614:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3609
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3609:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3619:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L3614
	.p2align 4,,10
	.p2align 3
.L3618:
	popq	%rbx
	movl	%r8d, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL47Stats_Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21458:
	.size	_ZN2v88internal41Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateE, .-_ZN2v88internal41Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Runtime_GetPropertyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Runtime_GetPropertyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Runtime_GetPropertyEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Runtime_GetPropertyEiPmPNS0_7IsolateE:
.LFB21461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3714
	addl	$1, 41104(%rdx)
	movq	-8(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L3715
.L3624:
	movq	0(%r13), %r15
	testb	$1, %r15b
	jne	.L3716
.L3646:
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb
	testq	%rax, %rax
	je	.L3717
	movq	(%rax), %r15
.L3655:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3620
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3620:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3718
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3716:
	.cfi_restore_state
	movq	-1(%r15), %rax
	cmpw	$1024, 11(%rax)
	movq	-1(%r15), %rax
	jbe	.L3719
	cmpw	$1026, 11(%rax)
	je	.L3633
	movq	-1(%r15), %rax
	cmpw	$1026, 11(%rax)
	je	.L3720
	movq	-1(%r15), %rax
	movzbl	13(%rax), %ecx
	shrb	$5, %cl
	andl	$1, %ecx
.L3638:
	movq	(%rdx), %rsi
	movq	%rsi, %rax
	notq	%rax
	andl	$1, %eax
	testb	%cl, %cl
	je	.L3721
.L3639:
	testb	%al, %al
	je	.L3646
	.p2align 4,,10
	.p2align 3
.L3645:
	movq	0(%r13), %rax
	movq	-1(%rax), %rcx
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	leal	-4(%rcx), %edi
	cmpb	$1, %dil
	ja	.L3646
	movq	15(%rax), %rdi
	movq	%rsi, %rax
	sarq	$32, %rax
	cmpl	%eax, 11(%rdi)
	jg	.L3646
	cmpb	$4, %cl
	movq	%r13, %rdi
	movq	%rdx, -88(%rbp)
	setne	%sil
	addl	$2, %esi
	movzbl	%sil, %esi
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	-88(%rbp), %rdx
	jmp	.L3646
	.p2align 4,,10
	.p2align 3
.L3719:
	cmpw	$63, 11(%rax)
	ja	.L3646
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L3646
	sarq	$32, %rax
	movq	%rax, %rcx
	js	.L3646
	cmpl	11(%r15), %eax
	jge	.L3646
	movq	-1(%r15), %rax
	cmpw	$63, 11(%rax)
	ja	.L3670
	movq	-1(%r15), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	je	.L3722
.L3670:
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L3677
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L3677
	movq	41112(%r12), %rdi
	movq	15(%rax), %r15
	testq	%rdi, %rdi
	je	.L3680
	movq	%r15, %rsi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-88(%rbp), %rcx
	movq	%rax, %r13
.L3677:
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L3683
	leaq	.L3685(%rip), %rsi
	movzwl	%ax, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal19Runtime_GetPropertyEiPmPNS0_7IsolateE,"a",@progbits
	.align 4
	.align 4
.L3685:
	.long	.L3691-.L3685
	.long	.L3688-.L3685
	.long	.L3690-.L3685
	.long	.L3686-.L3685
	.long	.L3683-.L3685
	.long	.L3684-.L3685
	.long	.L3683-.L3685
	.long	.L3683-.L3685
	.long	.L3689-.L3685
	.long	.L3688-.L3685
	.long	.L3687-.L3685
	.long	.L3686-.L3685
	.long	.L3683-.L3685
	.long	.L3684-.L3685
	.section	.text._ZN2v88internal19Runtime_GetPropertyEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L3715:
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L3624
	movq	%rax, -64(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L3626
	testb	$2, %al
	jne	.L3624
.L3626:
	leaq	-68(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-88(%rbp), %rdx
	testb	%al, %al
	je	.L3624
	movl	-68(%rbp), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	jmp	.L3624
	.p2align 4,,10
	.p2align 3
.L3717:
	movq	312(%r12), %r15
	jmp	.L3655
	.p2align 4,,10
	.p2align 3
.L3714:
	movq	%rdx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internalL25Stats_Runtime_GetPropertyEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r15
	jmp	.L3620
	.p2align 4,,10
	.p2align 3
.L3720:
	movq	%r15, %rcx
	leaq	-64(%rbp), %rdi
	movq	%rdx, -96(%rbp)
	andq	$-262144, %rcx
	movq	24(%rcx), %rax
	movq	%rcx, -88(%rbp)
	movq	-25128(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	-88(%rbp), %rcx
	movq	24(%rcx), %rsi
	movq	-1(%r15), %rcx
	movq	-96(%rbp), %rdx
	cmpw	$1024, 11(%rcx)
	je	.L3723
	movq	-1(%r15), %rcx
	movq	23(%rcx), %rcx
.L3637:
	cmpq	%rcx, %rax
	setne	%cl
	jmp	.L3638
.L3684:
	leaq	-64(%rbp), %rdi
	movl	%ecx, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10ThinString3GetEi@PLT
.L3692:
	movzwl	%ax, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt@PLT
	movq	(%rax), %r15
	jmp	.L3655
.L3688:
	leaq	-64(%rbp), %rdi
	movl	%ecx, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10ConsString3GetEi@PLT
	jmp	.L3692
.L3686:
	leaq	-64(%rbp), %rdi
	movl	%ecx, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	jmp	.L3692
.L3683:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3687:
	movq	15(%rdx), %rdi
	movq	%rcx, -88(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	-88(%rbp), %rcx
	movzbl	(%rax,%rcx), %eax
	jmp	.L3692
.L3689:
	leal	16(%rcx), %eax
	cltq
	movzbl	-1(%rdx,%rax), %eax
	jmp	.L3692
.L3691:
	leal	16(%rcx,%rcx), %eax
	cltq
	movzwl	-1(%rdx,%rax), %eax
	jmp	.L3692
.L3690:
	movq	15(%rdx), %rdi
	movq	%rcx, -88(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	-88(%rbp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	jmp	.L3692
	.p2align 4,,10
	.p2align 3
.L3721:
	testb	%al, %al
	jne	.L3645
	movq	-1(%rsi), %rax
	cmpw	$64, 11(%rax)
	jbe	.L3641
	movl	%ecx, %eax
	jmp	.L3639
	.p2align 4,,10
	.p2align 3
.L3641:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L3724
.L3642:
	movq	0(%r13), %rax
	movq	-1(%rax), %rcx
	cmpw	$1025, 11(%rcx)
	je	.L3725
	movq	-1(%rax), %rcx
	movl	15(%rcx), %ecx
	andl	$2097152, %ecx
	je	.L3646
	movq	7(%rax), %r15
	testb	$1, %r15b
	jne	.L3657
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %r15
.L3657:
	movq	(%rdx), %rcx
	movl	7(%rcx), %eax
	testb	$1, %al
	jne	.L3658
	shrl	$2, %eax
.L3659:
	movslq	35(%r15), %r8
	movq	88(%r12), %r9
	leaq	-1(%r15), %rdi
	movl	$1, %esi
	subl	$1, %r8d
	andl	%r8d, %eax
	movl	%eax, %r10d
	jmp	.L3662
	.p2align 4,,10
	.p2align 3
.L3726:
	leal	(%r10,%rsi), %eax
	addl	$1, %esi
	andl	%r8d, %eax
	movl	%eax, %r10d
.L3662:
	leal	(%r10,%r10,2), %ecx
	leal	56(,%rcx,8), %eax
	cltq
	movq	(%rax,%rdi), %rax
	cmpq	%rax, %r9
	je	.L3646
	cmpq	(%rdx), %rax
	jne	.L3726
	cmpl	$-1, %r10d
	je	.L3646
	leal	72(,%rcx,8), %eax
	movslq	%eax, %rcx
	movq	(%rcx,%rdi), %rcx
	btq	$32, %rcx
	jc	.L3646
	subl	$8, %eax
	cltq
	movq	(%rax,%rdi), %r15
	jmp	.L3655
	.p2align 4,,10
	.p2align 3
.L3633:
	movq	(%rdx), %rsi
	movq	%rsi, %rax
	notq	%rax
	andl	$1, %eax
	jmp	.L3639
.L3725:
	movq	7(%rax), %r15
	movq	(%rdx), %rax
	movl	7(%rax), %ecx
	testb	$1, %cl
	jne	.L3647
	shrl	$2, %ecx
.L3648:
	movslq	35(%r15), %r9
	movq	88(%r12), %r10
	leaq	-1(%r15), %r8
	movl	$1, %edi
	subl	$1, %r9d
	andl	%r9d, %ecx
	jmp	.L3651
	.p2align 4,,10
	.p2align 3
.L3727:
	addl	%edi, %ecx
	addl	$1, %edi
	andl	%r9d, %ecx
.L3651:
	leal	56(,%rcx,8), %eax
	cltq
	addq	%r8, %rax
	movq	(%rax), %rsi
	cmpq	%rsi, %r10
	je	.L3646
	movq	(%rdx), %r11
	cmpq	%r11, 7(%rsi)
	jne	.L3727
	cmpl	$-1, %ecx
	je	.L3646
	movq	(%rax), %rax
	testb	$1, 19(%rax)
	jne	.L3646
	movq	23(%rax), %r15
	cmpq	%r15, 96(%r12)
	je	.L3646
	jmp	.L3655
.L3724:
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rdx
	jmp	.L3642
.L3647:
	leaq	-64(%rbp), %rdi
	movq	%rdx, -88(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-88(%rbp), %rdx
	movl	%eax, %ecx
	jmp	.L3648
.L3658:
	leaq	-64(%rbp), %rdi
	movq	%rdx, -88(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-88(%rbp), %rdx
	jmp	.L3659
.L3722:
	movq	-1(%r15), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	subw	$1, %ax
	jne	.L3672
	movq	23(%r15), %rax
	cmpl	$0, 11(%rax)
	je	.L3672
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	-88(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L3677
.L3680:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3728
.L3682:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, 0(%r13)
	jmp	.L3677
.L3672:
	movq	41112(%r12), %rdi
	movq	15(%r15), %r15
	testq	%rdi, %rdi
	je	.L3674
	movq	%r15, %rsi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-88(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L3670
.L3723:
	movq	-37488(%rsi), %rcx
	jmp	.L3637
.L3718:
	call	__stack_chk_fail@PLT
.L3674:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3729
.L3676:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, 0(%r13)
	jmp	.L3670
.L3728:
	movq	%r12, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L3682
.L3729:
	movq	%r12, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L3676
	.cfi_endproc
.LFE21461:
	.size	_ZN2v88internal19Runtime_GetPropertyEiPmPNS0_7IsolateE, .-_ZN2v88internal19Runtime_GetPropertyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_SetKeyedPropertyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_SetKeyedPropertyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_SetKeyedPropertyEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_SetKeyedPropertyEiPmPNS0_7IsolateE:
.LFB21464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3754
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	leaq	-16(%rsi), %r15
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rax
	leaq	-8(%rsi), %rdx
	cmpq	104(%r12), %rax
	jne	.L3755
.L3733:
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$86, %esi
.L3753:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L3747:
	movq	312(%r12), %r13
	.p2align 4,,10
	.p2align 3
.L3744:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3730
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3730:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3756
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3755:
	.cfi_restore_state
	cmpq	88(%r12), %rax
	je	.L3733
	leaq	-144(%rbp), %rdi
	movq	%rdx, %rcx
	movl	$3, %r9d
	movq	%rsi, %rdx
	leaq	-145(%rbp), %r8
	movq	%r12, %rsi
	movq	%rdi, -168(%rbp)
	movb	$0, -145(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	cmpb	$0, -145(%rbp)
	movq	-168(%rbp), %rdi
	je	.L3747
	cmpl	$4, -140(%rbp)
	jne	.L3739
	movq	-8(%r13), %rax
	testb	$1, %al
	je	.L3739
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	jne	.L3739
	testb	$16, 11(%rax)
	je	.L3739
	movq	41112(%r12), %rdi
	movq	15(%rax), %r15
	testq	%rdi, %rdi
	je	.L3740
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L3741:
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$260, %esi
	jmp	.L3753
	.p2align 4,,10
	.p2align 3
.L3739:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L3747
	cmpq	$16, %r13
	je	.L3747
	movq	-16(%r13), %r13
	jmp	.L3744
	.p2align 4,,10
	.p2align 3
.L3754:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL30Stats_Runtime_SetKeyedPropertyEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L3730
.L3740:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L3757
.L3742:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rdx)
	jmp	.L3741
.L3756:
	call	__stack_chk_fail@PLT
.L3757:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L3742
	.cfi_endproc
.LFE21464:
	.size	_ZN2v88internal24Runtime_SetKeyedPropertyEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_SetKeyedPropertyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_SetNamedPropertyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_SetNamedPropertyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_SetNamedPropertyEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_SetNamedPropertyEiPmPNS0_7IsolateE:
.LFB21467:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3782
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	leaq	-16(%rsi), %r15
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rax
	leaq	-8(%rsi), %rdx
	cmpq	104(%r12), %rax
	jne	.L3783
.L3761:
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$86, %esi
.L3781:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L3775:
	movq	312(%r12), %r13
	.p2align 4,,10
	.p2align 3
.L3772:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3758
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3758:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3784
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3783:
	.cfi_restore_state
	cmpq	88(%r12), %rax
	je	.L3761
	leaq	-144(%rbp), %rdi
	movq	%rdx, %rcx
	movl	$3, %r9d
	movq	%rsi, %rdx
	leaq	-145(%rbp), %r8
	movq	%r12, %rsi
	movq	%rdi, -168(%rbp)
	movb	$0, -145(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	cmpb	$0, -145(%rbp)
	movq	-168(%rbp), %rdi
	je	.L3775
	cmpl	$4, -140(%rbp)
	jne	.L3767
	movq	-8(%r13), %rax
	testb	$1, %al
	je	.L3767
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	jne	.L3767
	testb	$16, 11(%rax)
	je	.L3767
	movq	41112(%r12), %rdi
	movq	15(%rax), %r15
	testq	%rdi, %rdi
	je	.L3768
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L3769:
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$260, %esi
	jmp	.L3781
	.p2align 4,,10
	.p2align 3
.L3767:
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L3775
	cmpq	$16, %r13
	je	.L3775
	movq	-16(%r13), %r13
	jmp	.L3772
	.p2align 4,,10
	.p2align 3
.L3782:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL30Stats_Runtime_SetNamedPropertyEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L3758
.L3768:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L3785
.L3770:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rdx)
	jmp	.L3769
.L3784:
	call	__stack_chk_fail@PLT
.L3785:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L3770
	.cfi_endproc
.LFE21467:
	.size	_ZN2v88internal24Runtime_SetNamedPropertyEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_SetNamedPropertyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateE:
.LFB21470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3797
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41096(%rdx), %rbx
	movq	%rax, -168(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L3789
.L3790:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3789:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3790
	leaq	-144(%rbp), %r14
	leaq	-8(%rsi), %rcx
	movq	%rsi, %rdx
	movl	$1, %r9d
	leaq	-16(%rsi), %r15
	leaq	-145(%rbp), %r8
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r15, %rsi
	movabsq	$4294967297, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE@PLT
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L3791
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r13
.L3792:
	subl	$1, 41104(%r12)
	movq	-168(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3786
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3786:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3798
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3791:
	.cfi_restore_state
	movq	-16(%r13), %r13
	jmp	.L3792
	.p2align 4,,10
	.p2align 3
.L3797:
	call	_ZN2v88internalL40Stats_Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L3786
.L3798:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21470:
	.size	_ZN2v88internal34Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_DeletePropertyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_DeletePropertyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_DeletePropertyEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_DeletePropertyEiPmPNS0_7IsolateE:
.LFB21474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3804
	addl	$1, 41104(%rdx)
	movq	-16(%rsi), %rcx
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	leaq	-8(%rsi), %rdx
	testb	$1, %cl
	jne	.L3805
	sarq	$32, %rcx
	movq	%r12, %rdi
	andl	$1, %ecx
	call	_ZN2v88internal12_GLOBAL__N_114DeletePropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS0_12LanguageModeE
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L3802
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3802:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3804:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL28Stats_Runtime_DeletePropertyEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L3805:
	.cfi_restore_state
	leaq	.LC91(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21474:
	.size	_ZN2v88internal22Runtime_DeletePropertyEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_DeletePropertyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateE:
.LFB21477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3822
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3809
.L3810:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3809:
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L3810
	movq	7(%rax), %r15
	testb	$1, %r15b
	jne	.L3812
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %r15
.L3812:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3813
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3814:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9HashTableINS0_14NameDictionaryENS0_19NameDictionaryShapeEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EEi@PLT
	movq	0(%r13), %rdx
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3816
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3816:
	xorl	%eax, %eax
.L3806:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3823
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3813:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L3824
.L3815:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L3814
	.p2align 4,,10
	.p2align 3
.L3822:
	call	_ZN2v88internalL38Stats_Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateE
	jmp	.L3806
	.p2align 4,,10
	.p2align 3
.L3824:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L3815
.L3823:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21477:
	.size	_ZN2v88internal32Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateE, .-_ZN2v88internal32Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Runtime_HasPropertyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Runtime_HasPropertyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Runtime_HasPropertyEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Runtime_HasPropertyEiPmPNS0_7IsolateE:
.LFB21480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %r13d
	testl	%r13d, %r13d
	jne	.L3860
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L3828
.L3830:
	xorl	%r8d, %r8d
	movl	$65, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L3829:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3825
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3825:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3861
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3828:
	.cfi_restore_state
	movq	-1(%rax), %rsi
	cmpw	$1023, 11(%rsi)
	jbe	.L3830
	movq	-8(%rcx), %rsi
	testb	$1, %sil
	jne	.L3831
.L3834:
	movq	%rdx, %rsi
	movq	%r12, %rdi
	movq	%rcx, -248(%rbp)
	call	_ZN2v88internal6Object13ConvertToNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3859
	movq	-248(%rbp), %rcx
	movq	(%rcx), %rax
.L3833:
	andq	$-262144, %rax
	movq	24(%rax), %r15
	movq	(%rdx), %rax
	movq	-1(%rax), %rsi
	subq	$37592, %r15
	cmpw	$63, 11(%rsi)
	jbe	.L3862
.L3836:
	movq	(%rdx), %rax
	movq	-1(%rax), %rsi
	cmpw	$64, 11(%rsi)
	je	.L3863
	movl	$3, %r13d
.L3840:
	movabsq	$824633720832, %rax
	movl	%r13d, -224(%rbp)
	movq	%rax, -212(%rbp)
	movq	%r15, -200(%rbp)
	movq	(%rdx), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L3864
.L3841:
	leaq	-224(%rbp), %r13
	movq	%rdx, -192(%rbp)
	movq	%r13, %rdi
	movq	$0, -184(%rbp)
	movq	%rcx, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L3839:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	je	.L3859
	shrw	$8, %ax
	je	.L3843
	movq	112(%r12), %r13
	jmp	.L3829
	.p2align 4,,10
	.p2align 3
.L3860:
	movq	%rdx, %rsi
	movq	%rcx, %rdi
	call	_ZN2v88internalL25Stats_Runtime_HasPropertyEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L3825
	.p2align 4,,10
	.p2align 3
.L3859:
	movq	312(%r12), %r13
	jmp	.L3829
	.p2align 4,,10
	.p2align 3
.L3863:
	testb	$1, 11(%rax)
	movl	$3, %eax
	cmove	%eax, %r13d
	jmp	.L3840
	.p2align 4,,10
	.p2align 3
.L3831:
	movq	-1(%rsi), %rsi
	cmpw	$64, 11(%rsi)
	ja	.L3834
	jmp	.L3833
	.p2align 4,,10
	.p2align 3
.L3862:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L3837
	testb	$2, %al
	jne	.L3836
.L3837:
	leaq	-144(%rbp), %rdi
	leaq	-228(%rbp), %rsi
	movq	%rcx, -264(%rbp)
	movq	%rdx, -248(%rbp)
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-248(%rbp), %rdx
	movq	-264(%rbp), %rcx
	testb	%al, %al
	je	.L3836
	movabsq	$824633720832, %rax
	movq	-256(%rbp), %rdi
	movq	%r15, -120(%rbp)
	leaq	-224(%rbp), %r13
	movq	%rax, -132(%rbp)
	movl	-228(%rbp), %eax
	movl	$3, -144(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movdqa	-128(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm3
	movq	-248(%rbp), %rdx
	movdqa	-144(%rbp), %xmm0
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movq	%rdx, -112(%rbp)
	movdqa	-112(%rbp), %xmm2
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L3839
	.p2align 4,,10
	.p2align 3
.L3843:
	movq	120(%r12), %r13
	jmp	.L3829
	.p2align 4,,10
	.p2align 3
.L3864:
	movq	%rdx, %rsi
	movq	%r15, %rdi
	movq	%rcx, -248(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-248(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L3841
.L3861:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21480:
	.size	_ZN2v88internal19Runtime_HasPropertyEiPmPNS0_7IsolateE, .-_ZN2v88internal19Runtime_HasPropertyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateE:
.LFB21483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %r8d
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3875
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3867
.L3868:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3867:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3868
	movq	-8(%rsi), %rdx
	testb	$1, %dl
	jne	.L3876
	xorl	%esi, %esi
	sarq	$32, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3877
	movq	(%rax), %rax
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
.L3871:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3865
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3865:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3877:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L3871
	.p2align 4,,10
	.p2align 3
.L3875:
	popq	%rbx
	movl	%r8d, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Stats_Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L3876:
	.cfi_restore_state
	leaq	.LC39(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21483:
	.size	_ZN2v88internal26Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_ToFastPropertiesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_ToFastPropertiesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_ToFastPropertiesEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_ToFastPropertiesEiPmPNS0_7IsolateE:
.LFB21486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3890
	movl	41104(%rdx), %eax
	movq	41088(%rdx), %r15
	movq	41096(%rdx), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	(%rsi), %r14
	testb	$1, %r14b
	jne	.L3880
.L3889:
	movq	%rbx, %rdx
.L3885:
	movq	%r15, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L3878
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3878:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3880:
	.cfi_restore_state
	movq	-1(%r14), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L3889
	movq	-1(%r14), %rcx
	movq	%rbx, %rdx
	cmpw	$1025, 11(%rcx)
	je	.L3885
	leaq	.LC10(%rip), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc@PLT
	movl	41104(%r12), %eax
	movq	0(%r13), %r14
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L3885
	.p2align 4,,10
	.p2align 3
.L3890:
	addq	$8, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL30Stats_Runtime_ToFastPropertiesEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21486:
	.size	_ZN2v88internal24Runtime_ToFastPropertiesEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_ToFastPropertiesEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_AllocateHeapNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_AllocateHeapNumberEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_AllocateHeapNumberEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_AllocateHeapNumberEiPmPNS0_7IsolateE:
.LFB21489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3895
	addl	$1, 41104(%rdx)
	movq	%rdx, %rdi
	xorl	%esi, %esi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	$0, 7(%rdx)
	movq	(%rax), %r13
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3891
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3891:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3895:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Stats_Runtime_AllocateHeapNumberEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21489:
	.size	_ZN2v88internal26Runtime_AllocateHeapNumberEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_AllocateHeapNumberEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal17Runtime_NewObjectEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17Runtime_NewObjectEiPmPNS0_7IsolateE
	.type	_ZN2v88internal17Runtime_NewObjectEiPmPNS0_7IsolateE, @function
_ZN2v88internal17Runtime_NewObjectEiPmPNS0_7IsolateE:
.LFB21492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %r8d
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3907
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3898
.L3899:
	leaq	.LC8(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3898:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L3899
	movq	-8(%rdi), %rax
	leaq	-8(%rsi), %rsi
	testb	$1, %al
	jne	.L3908
.L3900:
	leaq	.LC52(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3908:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3900
	xorl	%edx, %edx
	call	_ZN2v88internal8JSObject3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEENS2_INS0_14AllocationSiteEEE@PLT
	testq	%rax, %rax
	je	.L3909
	movq	(%rax), %r14
.L3903:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3896
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3896:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3909:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L3903
	.p2align 4,,10
	.p2align 3
.L3907:
	popq	%rbx
	movl	%r8d, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL23Stats_Runtime_NewObjectEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21492:
	.size	_ZN2v88internal17Runtime_NewObjectEiPmPNS0_7IsolateE, .-_ZN2v88internal17Runtime_NewObjectEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_GetDerivedMapEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_GetDerivedMapEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_GetDerivedMapEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_GetDerivedMapEiPmPNS0_7IsolateE:
.LFB21495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3923
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3924
.L3912:
	leaq	.LC8(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3924:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L3912
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L3925
.L3913:
	leaq	.LC52(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3925:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3913
	movq	%r12, %rdi
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	testq	%rax, %rax
	je	.L3926
	movq	(%rax), %r14
.L3917:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3910
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3910:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3926:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L3917
	.p2align 4,,10
	.p2align 3
.L3923:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL27Stats_Runtime_GetDerivedMapEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21495:
	.size	_ZN2v88internal21Runtime_GetDerivedMapEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_GetDerivedMapEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal43Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal43Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateE
	.type	_ZN2v88internal43Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateE, @function
_ZN2v88internal43Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateE:
.LFB21498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3935
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3930
.L3931:
	leaq	.LC25(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3930:
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L3931
	leaq	-48(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal3Map29CompleteInobjectSlackTrackingEPNS0_7IsolateE@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3927
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3927:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3936
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3935:
	.cfi_restore_state
	movq	%rdx, %rsi
	call	_ZN2v88internalL49Stats_Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L3927
.L3936:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21498:
	.size	_ZN2v88internal43Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateE, .-_ZN2v88internal43Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_TryMigrateInstanceEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_TryMigrateInstanceEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_TryMigrateInstanceEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_TryMigrateInstanceEiPmPNS0_7IsolateE:
.LFB21501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3947
	movl	41104(%rdx), %eax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%rdi)
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L3939
	movl	%eax, 41104(%rdi)
	xorl	%r12d, %r12d
.L3937:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3939:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	cmpw	$1024, 11(%rcx)
	ja	.L3941
.L3943:
	movq	%rbx, %rdx
	xorl	%r12d, %r12d
.L3942:
	movq	%r13, 41088(%rdi)
	movl	%eax, 41104(%rdi)
	cmpq	%rdx, %rbx
	je	.L3937
	movq	%rbx, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3941:
	.cfi_restore_state
	movq	-1(%rdx), %rdx
	movl	15(%rdx), %edx
	andl	$16777216, %edx
	je	.L3943
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal8JSObject18TryMigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-40(%rbp), %rdi
	testb	%al, %al
	jne	.L3944
	movl	41104(%rdi), %eax
	movq	41096(%rdi), %rdx
	xorl	%r12d, %r12d
	subl	$1, %eax
	jmp	.L3942
	.p2align 4,,10
	.p2align 3
.L3947:
	addq	$24, %rsp
	movq	%r12, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Stats_Runtime_TryMigrateInstanceEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L3944:
	.cfi_restore_state
	movl	41104(%rdi), %eax
	movq	(%r12), %r12
	movq	41096(%rdi), %rdx
	subl	$1, %eax
	jmp	.L3942
	.cfi_endproc
.LFE21501:
	.size	_ZN2v88internal26Runtime_TryMigrateInstanceEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_TryMigrateInstanceEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal39Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal39Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateE
	.type	_ZN2v88internal39Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateE, @function
_ZN2v88internal39Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateE:
.LFB21505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3966
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L3967
.L3950:
	leaq	.LC42(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3967:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L3950
	cmpq	104(%r12), %rax
	je	.L3968
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r10
	testb	$1, %al
	jne	.L3969
.L3953:
	leaq	.LC49(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3969:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L3953
	leaq	-16(%rsi), %rdx
	movq	%r12, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL15IsValidAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L3970
	leaq	-24(%r9), %rcx
	movq	%rcx, %rsi
	call	_ZN2v88internalL15IsValidAccessorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L3971
	movq	-32(%r9), %r8
	testb	$1, %r8b
	jne	.L3972
	sarq	$32, %r8
	testl	$-8, %r8d
	jne	.L3973
	movq	%r10, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8JSObject14DefineAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEES7_NS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	jne	.L3959
	movq	312(%r12), %r14
.L3960:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3948
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3948:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3959:
	.cfi_restore_state
	movq	88(%r12), %r14
	jmp	.L3960
	.p2align 4,,10
	.p2align 3
.L3966:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r9, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL45Stats_Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L3968:
	.cfi_restore_state
	leaq	.LC74(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3970:
	leaq	.LC75(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3971:
	leaq	.LC76(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3972:
	leaq	.LC77(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3973:
	leaq	.LC78(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21505:
	.size	_ZN2v88internal39Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateE, .-_ZN2v88internal39Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal35Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateE:
.LFB21508:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %ebx
	testl	%ebx, %ebx
	jne	.L4030
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%rax, -256(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -248(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L4031
.L3977:
	leaq	.LC42(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4031:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L3977
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r13
	testb	$1, %al
	jne	.L4032
.L3978:
	leaq	.LC49(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4032:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L3978
	movq	-24(%rsi), %r8
	testb	$1, %r8b
	jne	.L4033
	movq	-32(%rsi), %rax
	sarq	$32, %r8
	leaq	-32(%rsi), %rcx
	testb	$1, %al
	je	.L4034
	movq	-40(%rsi), %rsi
	testb	$1, %sil
	jne	.L4035
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	jne	.L4036
.L3984:
	testb	$1, %r8b
	movl	$2, %eax
	leaq	-16(%r12), %r15
	cmovne	%eax, %ebx
	andl	$2, %r8d
	je	.L3994
	movq	-16(%r12), %rax
	movq	-1(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3995
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L3996:
	leaq	128(%r14), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rcx, -264(%rbp)
	call	_ZN2v88internal10JSFunction7SetNameENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6StringEEE@PLT
	movq	-264(%rbp), %rcx
	testb	%al, %al
	jne	.L3998
	movq	312(%r14), %r12
.L3999:
	movq	-256(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-248(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L3974
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3974:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4037
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3998:
	.cfi_restore_state
	movq	-16(%r12), %rdx
	movq	23(%rdx), %rax
	movl	47(%rax), %eax
	andl	$31, %eax
	subl	$2, %eax
	cmpb	$3, %al
	ja	.L4038
.L3994:
	movq	-8(%r12), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L4039
.L4003:
	movq	-8(%r12), %rcx
	movl	$1, %edx
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	je	.L4040
.L4007:
	movabsq	$824633720832, %rcx
	movl	%edx, -224(%rbp)
	movq	%rcx, -212(%rbp)
	movq	%r14, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L4041
.L4008:
	leaq	-224(%rbp), %r11
	movq	%r13, -192(%rbp)
	movq	%r11, %rdi
	movq	%r11, -264(%rbp)
	movq	$0, -184(%rbp)
	movq	%r12, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%r12, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	-264(%rbp), %r11
.L4006:
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r15, %rsi
	movq	%r11, %rdi
	movabsq	$4294967297, %rcx
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE@PLT
	testb	%al, %al
	je	.L4042
	movq	(%r12), %r12
	jmp	.L3999
	.p2align 4,,10
	.p2align 3
.L3995:
	movq	41088(%r14), %rcx
	cmpq	41096(%r14), %rcx
	je	.L4043
.L3997:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rcx)
	jmp	.L3996
	.p2align 4,,10
	.p2align 3
.L4040:
	movl	11(%rcx), %edx
	notl	%edx
	andl	$1, %edx
	jmp	.L4007
	.p2align 4,,10
	.p2align 3
.L4036:
	leaq	-224(%rbp), %r11
	sarq	$32, %rsi
	leaq	-144(%rbp), %r15
	movq	%r8, -272(%rbp)
	movq	%r11, %rdi
	movq	%r11, -264(%rbp)
	movq	%rcx, -144(%rbp)
	movl	%esi, -128(%rbp)
	movq	$0, -136(%rbp)
	movq	%rax, -224(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movq	%r15, %rdi
	movl	%eax, -124(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movq	-264(%rbp), %r11
	movq	-272(%rbp), %r8
	cmpl	$1, %eax
	je	.L4044
	movq	%r15, %rdi
	movq	%r8, -264(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movq	-264(%rbp), %r8
	cmpl	$3, %eax
	jne	.L3984
	movq	(%r12), %rax
	movq	%r8, -272(%rbp)
	movq	%r15, %rdi
	movq	-1(%rax), %rax
	movq	%rax, -264(%rbp)
	call	_ZNK2v88internal13FeedbackNexus11GetFirstMapEv@PLT
	cmpq	-264(%rbp), %rax
	movq	-272(%rbp), %r8
	jne	.L3992
	movq	-8(%r12), %rax
	movq	%r15, %rdi
	movq	%r8, -272(%rbp)
	movq	%rax, -264(%rbp)
	call	_ZNK2v88internal13FeedbackNexus7GetNameEv@PLT
	cmpq	%rax, -264(%rbp)
	movq	-272(%rbp), %r8
	je	.L3984
	.p2align 4,,10
	.p2align 3
.L3992:
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicENS0_11IcCheckTypeE@PLT
	movq	-264(%rbp), %r8
	jmp	.L3984
	.p2align 4,,10
	.p2align 3
.L4039:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %edx
	testb	$1, %dl
	jne	.L4004
	andl	$2, %edx
	jne	.L4003
.L4004:
	leaq	-144(%rbp), %rdi
	leaq	-228(%rbp), %rsi
	movq	%rdi, -264(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-264(%rbp), %rdi
	testb	%al, %al
	je	.L4045
	movabsq	$824633720832, %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -132(%rbp)
	movl	-228(%rbp), %eax
	movl	$1, -144(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r12, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r13, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	leaq	-224(%rbp), %r11
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L4006
	.p2align 4,,10
	.p2align 3
.L4030:
	movq	%r12, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL41Stats_Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r12
	jmp	.L3974
	.p2align 4,,10
	.p2align 3
.L4033:
	leaq	.LC64(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4034:
	leaq	.LC83(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4044:
	movq	-8(%r12), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L3992
	movl	$1, -224(%rbp)
	movq	(%r12), %rax
	movq	$0, -216(%rbp)
	movq	-1(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3987
	movq	%r11, -272(%rbp)
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-264(%rbp), %r8
	movq	-272(%rbp), %r11
	movq	%rax, %rdx
.L3988:
	movq	%r11, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal13FeedbackNexus20ConfigureMonomorphicENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE@PLT
	movq	-264(%rbp), %r8
	jmp	.L3984
	.p2align 4,,10
	.p2align 3
.L4035:
	leaq	.LC84(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4038:
	movq	-1(%rdx), %rax
	cmpq	%rax, (%rcx)
	je	.L3994
	leaq	.LC85(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4041:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r13
	jmp	.L4008
	.p2align 4,,10
	.p2align 3
.L4042:
	leaq	.LC86(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4045:
	movq	-8(%r12), %rax
	jmp	.L4003
	.p2align 4,,10
	.p2align 3
.L4043:
	movq	%r14, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L3997
	.p2align 4,,10
	.p2align 3
.L3987:
	movq	41088(%r14), %rdx
	cmpq	41096(%r14), %rdx
	je	.L4046
.L3989:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L3988
.L4046:
	movq	%r14, %rdi
	movq	%r11, -280(%rbp)
	movq	%r8, -272(%rbp)
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %r11
	movq	-272(%rbp), %r8
	movq	-264(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L3989
.L4037:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21508:
	.size	_ZN2v88internal35Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateE, .-_ZN2v88internal35Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_CollectTypeProfileEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_CollectTypeProfileEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_CollectTypeProfileEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_CollectTypeProfileEiPmPNS0_7IsolateE:
.LFB21515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4064
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, (%rsi)
	jne	.L4065
	movq	-16(%rsi), %rax
	leaq	-8(%rsi), %r9
	leaq	-16(%rsi), %r15
	testb	$1, %al
	jne	.L4066
.L4051:
	leaq	.LC60(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4066:
	movq	-1(%rax), %rax
	cmpw	$155, 11(%rax)
	jne	.L4051
	movq	%r9, %rsi
	movq	%rdx, %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal6Object6TypeOfEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-104(%rbp), %r9
	movq	%rax, %r8
	movq	-8(%r13), %rax
	testb	$1, %al
	jne	.L4067
.L4054:
	cmpq	104(%r12), %rax
	je	.L4068
.L4056:
	movq	-16(%r13), %rax
	leaq	-96(%rbp), %rdi
	movq	%r8, -104(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal14FeedbackVector18GetTypeProfileSlotEv@PLT
	leaq	-88(%rbp), %rdi
	movq	%r15, -80(%rbp)
	movl	%eax, %esi
	movl	%eax, -64(%rbp)
	movq	-16(%r13), %rax
	movq	$0, -72(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movq	0(%r13), %rdx
	movq	-104(%rbp), %r8
	leaq	-80(%rbp), %rdi
	movl	%eax, -60(%rbp)
	sarq	$32, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal13FeedbackNexus7CollectENS0_6HandleINS0_6StringEEEi@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4047
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4047:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4069
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4067:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L4054
	movq	%r9, %rdi
	call	_ZN2v88internal10JSReceiver18GetConstructorNameENS0_6HandleIS1_EE@PLT
	movq	%rax, %r8
	jmp	.L4056
	.p2align 4,,10
	.p2align 3
.L4068:
	movq	41112(%r12), %rdi
	movq	2960(%r12), %rsi
	testq	%rdi, %rdi
	je	.L4057
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
	jmp	.L4056
	.p2align 4,,10
	.p2align 3
.L4057:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L4070
.L4059:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L4056
	.p2align 4,,10
	.p2align 3
.L4064:
	call	_ZN2v88internalL32Stats_Runtime_CollectTypeProfileEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L4047
	.p2align 4,,10
	.p2align 3
.L4065:
	leaq	.LC59(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4070:
	movq	%r12, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L4059
.L4069:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21515:
	.size	_ZN2v88internal26Runtime_CollectTypeProfileEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_CollectTypeProfileEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_HasFastPackedElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_HasFastPackedElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_HasFastPackedElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_HasFastPackedElementsEiPmPNS0_7IsolateE:
.LFB21518:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L4085
	movq	(%rsi), %rax
	testb	$1, %al
	je	.L4086
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpb	$4, %al
	ja	.L4074
	testb	$1, %al
	je	.L4087
.L4074:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4087:
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4085:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL35Stats_Runtime_HasFastPackedElementsEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L4086:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21518:
	.size	_ZN2v88internal29Runtime_HasFastPackedElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_HasFastPackedElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Runtime_IsJSReceiverEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Runtime_IsJSReceiverEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Runtime_IsJSReceiverEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Runtime_IsJSReceiverEiPmPNS0_7IsolateE:
.LFB21521:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L4093
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L4090
.L4092:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4090:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L4092
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4093:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL26Stats_Runtime_IsJSReceiverEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21521:
	.size	_ZN2v88internal20Runtime_IsJSReceiverEiPmPNS0_7IsolateE, .-_ZN2v88internal20Runtime_IsJSReceiverEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal15Runtime_ClassOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal15Runtime_ClassOfEiPmPNS0_7IsolateE
	.type	_ZN2v88internal15Runtime_ClassOfEiPmPNS0_7IsolateE, @function
_ZN2v88internal15Runtime_ClassOfEiPmPNS0_7IsolateE:
.LFB21524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4101
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L4097
.L4098:
	movq	104(%rdx), %rax
.L4094:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4102
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4097:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$1023, 11(%rcx)
	jbe	.L4098
	leaq	-16(%rbp), %rdi
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal10JSReceiver10class_nameEv@PLT
	jmp	.L4094
	.p2align 4,,10
	.p2align 3
.L4101:
	movq	%rdx, %rsi
	call	_ZN2v88internalL21Stats_Runtime_ClassOfEiPmPNS0_7IsolateE.isra.0
	jmp	.L4094
.L4102:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21524:
	.size	_ZN2v88internal15Runtime_ClassOfEiPmPNS0_7IsolateE, .-_ZN2v88internal15Runtime_ClassOfEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_GetFunctionNameEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_GetFunctionNameEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_GetFunctionNameEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_GetFunctionNameEiPmPNS0_7IsolateE:
.LFB21527:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4109
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4105
.L4106:
	leaq	.LC8(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4105:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L4106
	movq	%rdx, %rdi
	call	_ZN2v88internal10JSFunction7GetNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4103
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4103:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4109:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL29Stats_Runtime_GetFunctionNameEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21527:
	.size	_ZN2v88internal23Runtime_GetFunctionNameEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_GetFunctionNameEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal37Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal37Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateE
	.type	_ZN2v88internal37Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateE, @function
_ZN2v88internal37Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateE:
.LFB21530:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4142
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4143
.L4113:
	leaq	.LC42(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4143:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L4113
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r9
	testb	$1, %al
	jne	.L4144
.L4114:
	leaq	.LC49(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4144:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L4114
	movq	-16(%rsi), %rax
	leaq	-16(%rsi), %r10
	testb	$1, %al
	jne	.L4145
.L4116:
	leaq	.LC63(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4145:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L4116
	movq	-24(%rsi), %r8
	testb	$1, %r8b
	jne	.L4146
	sarq	$32, %r8
	testl	$-8, %r8d
	jne	.L4147
	movq	23(%rax), %r15
	movq	15(%r15), %rax
	testb	$1, %al
	jne	.L4148
.L4121:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L4122:
	testb	%al, %al
	je	.L4126
	movq	15(%r15), %rdx
	testb	$1, %dl
	jne	.L4149
.L4124:
	movl	11(%rdx), %eax
	testl	%eax, %eax
	jne	.L4127
	movq	-16(%r13), %rax
	movq	-1(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4128
	movq	%r8, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %r8
	movq	%rax, %r15
.L4129:
	movq	%r9, %rsi
	movq	%r10, %rdi
	leaq	2608(%r12), %rdx
	movq	%r8, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal10JSFunction7SetNameENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6StringEEE@PLT
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %r9
	testb	%al, %al
	movq	-88(%rbp), %r8
	je	.L4141
	movq	-16(%r13), %rax
	movq	-1(%rax), %rax
	cmpq	%rax, (%r15)
	jne	.L4150
.L4127:
	leaq	104(%r12), %rcx
	movq	%r10, %rdx
	movq	%r9, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject14DefineAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEES7_NS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	jne	.L4133
.L4141:
	movq	312(%r12), %r13
.L4132:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4110
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4110:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4151
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4133:
	.cfi_restore_state
	movq	88(%r12), %r13
	jmp	.L4132
	.p2align 4,,10
	.p2align 3
.L4128:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L4152
.L4130:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L4129
	.p2align 4,,10
	.p2align 3
.L4126:
	andq	$-262144, %r15
	movq	24(%r15), %rax
	movq	-37464(%rax), %rdx
	jmp	.L4124
	.p2align 4,,10
	.p2align 3
.L4148:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L4121
	leaq	-64(%rbp), %rdi
	movq	%r8, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %r8
	jmp	.L4122
	.p2align 4,,10
	.p2align 3
.L4149:
	movq	-1(%rdx), %rax
	cmpw	$136, 11(%rax)
	jne	.L4124
	leaq	-64(%rbp), %rdi
	movq	%r8, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r10
	testb	%al, %al
	movq	-88(%rbp), %r8
	je	.L4126
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L4124
	.p2align 4,,10
	.p2align 3
.L4142:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL43Stats_Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L4110
	.p2align 4,,10
	.p2align 3
.L4146:
	leaq	.LC64(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4147:
	leaq	.LC65(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4150:
	leaq	.LC66(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4152:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r9
	movq	%rax, %r15
	jmp	.L4130
.L4151:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21530:
	.size	_ZN2v88internal37Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateE, .-_ZN2v88internal37Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_SetDataPropertiesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_SetDataPropertiesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_SetDataPropertiesEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_SetDataPropertiesEiPmPNS0_7IsolateE:
.LFB21533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4164
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4155
.L4156:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4155:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L4156
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rdx
	cmpq	%rax, 88(%r12)
	je	.L4160
	cmpq	%rax, 104(%r12)
	jne	.L4158
.L4160:
	movq	88(%r12), %r13
.L4159:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4153
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4153:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4158:
	.cfi_restore_state
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver23SetOrCopyDataPropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPKNS0_12ScopedVectorIS7_EEb@PLT
	testb	%al, %al
	jne	.L4160
	movq	312(%r12), %r13
	jmp	.L4159
	.p2align 4,,10
	.p2align 3
.L4164:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_SetDataPropertiesEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21533:
	.size	_ZN2v88internal25Runtime_SetDataPropertiesEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_SetDataPropertiesEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_CopyDataPropertiesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_CopyDataPropertiesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_CopyDataPropertiesEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_CopyDataPropertiesEiPmPNS0_7IsolateE:
.LFB21536:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4176
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4167
.L4168:
	leaq	.LC42(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4167:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L4168
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rdx
	cmpq	%rax, 88(%r12)
	je	.L4172
	cmpq	%rax, 104(%r12)
	jne	.L4170
.L4172:
	movq	88(%r12), %r13
.L4171:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4165
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4165:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4170:
	.cfi_restore_state
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver23SetOrCopyDataPropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPKNS0_12ScopedVectorIS7_EEb@PLT
	testb	%al, %al
	jne	.L4172
	movq	312(%r12), %r13
	jmp	.L4171
	.p2align 4,,10
	.p2align 3
.L4176:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Stats_Runtime_CopyDataPropertiesEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21536:
	.size	_ZN2v88internal26Runtime_CopyDataPropertiesEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_CopyDataPropertiesEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal37Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal37Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateE
	.type	_ZN2v88internal37Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateE, @function
_ZN2v88internal37Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateE:
.LFB21542:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4209
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4210
.L4180:
	leaq	.LC42(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4210:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L4180
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r9
	testb	$1, %al
	jne	.L4211
.L4181:
	leaq	.LC49(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4211:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L4181
	movq	-16(%rsi), %rax
	leaq	-16(%rsi), %rcx
	testb	$1, %al
	jne	.L4212
.L4183:
	leaq	.LC63(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4212:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L4183
	movq	-24(%rsi), %r8
	testb	$1, %r8b
	jne	.L4213
	sarq	$32, %r8
	testl	$-8, %r8d
	jne	.L4214
	movq	23(%rax), %r15
	movq	15(%r15), %rax
	testb	$1, %al
	jne	.L4215
.L4188:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L4189:
	testb	%al, %al
	je	.L4193
	movq	15(%r15), %rdx
	testb	$1, %dl
	jne	.L4216
.L4191:
	movl	11(%rdx), %eax
	testl	%eax, %eax
	jne	.L4194
	movq	-16(%r13), %rax
	movq	-1(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4195
	movq	%r8, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %r8
	movq	%rax, %r15
.L4196:
	movq	%r9, %rsi
	movq	%rcx, %rdi
	leaq	3272(%r12), %rdx
	movq	%r8, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal10JSFunction7SetNameENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6StringEEE@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r9
	testb	%al, %al
	movq	-88(%rbp), %r8
	je	.L4208
	movq	-16(%r13), %rax
	movq	-1(%rax), %rax
	cmpq	%rax, (%r15)
	jne	.L4217
.L4194:
	leaq	104(%r12), %rdx
	movq	%r9, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject14DefineAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEES7_NS0_18PropertyAttributesE@PLT
	testq	%rax, %rax
	jne	.L4200
.L4208:
	movq	312(%r12), %r13
.L4199:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4177
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4177:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4218
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4200:
	.cfi_restore_state
	movq	88(%r12), %r13
	jmp	.L4199
	.p2align 4,,10
	.p2align 3
.L4195:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L4219
.L4197:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L4196
	.p2align 4,,10
	.p2align 3
.L4193:
	andq	$-262144, %r15
	movq	24(%r15), %rax
	movq	-37464(%rax), %rdx
	jmp	.L4191
	.p2align 4,,10
	.p2align 3
.L4215:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L4188
	leaq	-64(%rbp), %rdi
	movq	%r8, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %r8
	jmp	.L4189
	.p2align 4,,10
	.p2align 3
.L4216:
	movq	-1(%rdx), %rax
	cmpw	$136, 11(%rax)
	jne	.L4191
	leaq	-64(%rbp), %rdi
	movq	%r8, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %rcx
	testb	%al, %al
	movq	-88(%rbp), %r8
	je	.L4193
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L4191
	.p2align 4,,10
	.p2align 3
.L4209:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL43Stats_Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L4177
	.p2align 4,,10
	.p2align 3
.L4213:
	leaq	.LC64(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4214:
	leaq	.LC65(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4217:
	leaq	.LC68(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4219:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %r9
	movq	%rax, %r15
	jmp	.L4197
.L4218:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21542:
	.size	_ZN2v88internal37Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateE, .-_ZN2v88internal37Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal16Runtime_ToObjectEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal16Runtime_ToObjectEiPmPNS0_7IsolateE
	.type	_ZN2v88internal16Runtime_ToObjectEiPmPNS0_7IsolateE, @function
_ZN2v88internal16Runtime_ToObjectEiPmPNS0_7IsolateE:
.LFB21545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	jne	.L4223
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4223:
	movq	%rdx, %rdi
	call	_ZN2v88internalL22Stats_Runtime_ToObjectEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21545:
	.size	_ZN2v88internal16Runtime_ToObjectEiPmPNS0_7IsolateE, .-_ZN2v88internal16Runtime_ToObjectEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal16Runtime_ToNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal16Runtime_ToNumberEiPmPNS0_7IsolateE
	.type	_ZN2v88internal16Runtime_ToNumberEiPmPNS0_7IsolateE, @function
_ZN2v88internal16Runtime_ToNumberEiPmPNS0_7IsolateE:
.LFB21548:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4236
	movl	41104(%rdx), %eax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	(%rsi), %r13
	testb	$1, %r13b
	jne	.L4226
.L4235:
	movq	%rbx, %rdx
.L4231:
	movq	%r14, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L4224
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4224:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4226:
	.cfi_restore_state
	movq	-1(%r13), %rdx
	cmpw	$65, 11(%rdx)
	je	.L4235
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	jne	.L4230
	movl	41104(%r12), %eax
	movq	312(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L4231
	.p2align 4,,10
	.p2align 3
.L4236:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL22Stats_Runtime_ToNumberEiPmPNS0_7IsolateE.isra.0
.L4230:
	.cfi_restore_state
	movq	(%rax), %r13
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L4231
	.cfi_endproc
.LFE21548:
	.size	_ZN2v88internal16Runtime_ToNumberEiPmPNS0_7IsolateE, .-_ZN2v88internal16Runtime_ToNumberEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal17Runtime_ToNumericEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17Runtime_ToNumericEiPmPNS0_7IsolateE
	.type	_ZN2v88internal17Runtime_ToNumericEiPmPNS0_7IsolateE, @function
_ZN2v88internal17Runtime_ToNumericEiPmPNS0_7IsolateE:
.LFB21551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4252
	movl	41104(%rdx), %eax
	movq	41096(%rdx), %rbx
	movq	41088(%rdx), %r14
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	(%rsi), %r13
	movq	%rbx, %rdx
	testb	$1, %r13b
	jne	.L4253
.L4247:
	movq	%r14, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L4237
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4237:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4253:
	.cfi_restore_state
	movq	-1(%r13), %rcx
	cmpw	$65, 11(%rcx)
	je	.L4247
	movq	-1(%r13), %rcx
	cmpw	$66, 11(%rcx)
	je	.L4247
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	jne	.L4250
	movl	41104(%r12), %eax
	movq	312(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L4247
	.p2align 4,,10
	.p2align 3
.L4252:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL23Stats_Runtime_ToNumericEiPmPNS0_7IsolateE.isra.0
.L4250:
	.cfi_restore_state
	movq	(%rax), %r13
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L4247
	.cfi_endproc
.LFE21551:
	.size	_ZN2v88internal17Runtime_ToNumericEiPmPNS0_7IsolateE, .-_ZN2v88internal17Runtime_ToNumericEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal16Runtime_ToLengthEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal16Runtime_ToLengthEiPmPNS0_7IsolateE
	.type	_ZN2v88internal16Runtime_ToLengthEiPmPNS0_7IsolateE, @function
_ZN2v88internal16Runtime_ToLengthEiPmPNS0_7IsolateE:
.LFB21554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4266
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rsi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %sil
	jne	.L4256
	sarq	$32, %rsi
	movl	$0, %eax
	movq	41112(%rdx), %rdi
	cmovs	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L4257
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	testq	%rax, %rax
	je	.L4267
.L4260:
	movq	(%rax), %r14
.L4261:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4254
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4254:
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4257:
	.cfi_restore_state
	movq	%r13, %rax
	cmpq	%rbx, %r13
	je	.L4268
.L4259:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L4260
	.p2align 4,,10
	.p2align 3
.L4256:
	movq	%r8, %rsi
	movq	%rdx, %rdi
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	jne	.L4260
.L4267:
	movq	312(%r12), %r14
	jmp	.L4261
	.p2align 4,,10
	.p2align 3
.L4266:
	addq	$16, %rsp
	movq	%rdx, %rsi
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL22Stats_Runtime_ToLengthEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L4268:
	.cfi_restore_state
	movq	%rdx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L4259
	.cfi_endproc
.LFE21554:
	.size	_ZN2v88internal16Runtime_ToLengthEiPmPNS0_7IsolateE, .-_ZN2v88internal16Runtime_ToLengthEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal18Runtime_ToStringRTEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Runtime_ToStringRTEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Runtime_ToStringRTEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Runtime_ToStringRTEiPmPNS0_7IsolateE:
.LFB21557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4279
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4271
.L4274:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L4280
.L4273:
	movq	(%r8), %r13
.L4275:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4269
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4269:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4271:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L4274
	jmp	.L4273
	.p2align 4,,10
	.p2align 3
.L4280:
	movq	312(%r12), %r13
	jmp	.L4275
	.p2align 4,,10
	.p2align 3
.L4279:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL24Stats_Runtime_ToStringRTEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21557:
	.size	_ZN2v88internal18Runtime_ToStringRTEiPmPNS0_7IsolateE, .-_ZN2v88internal18Runtime_ToStringRTEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal14Runtime_ToNameEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal14Runtime_ToNameEiPmPNS0_7IsolateE
	.type	_ZN2v88internal14Runtime_ToNameEiPmPNS0_7IsolateE, @function
_ZN2v88internal14Runtime_ToNameEiPmPNS0_7IsolateE:
.LFB21560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4291
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4283
.L4286:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object13ConvertToNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L4292
.L4285:
	movq	(%r8), %r13
.L4287:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4281
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4281:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4283:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L4286
	jmp	.L4285
	.p2align 4,,10
	.p2align 3
.L4292:
	movq	312(%r12), %r13
	jmp	.L4287
	.p2align 4,,10
	.p2align 3
.L4291:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL20Stats_Runtime_ToNameEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21560:
	.size	_ZN2v88internal14Runtime_ToNameEiPmPNS0_7IsolateE, .-_ZN2v88internal14Runtime_ToNameEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_HasInPrototypeChainEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_HasInPrototypeChainEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_HasInPrototypeChainEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_HasInPrototypeChainEiPmPNS0_7IsolateE:
.LFB21563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4307
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	leaq	-8(%rsi), %r9
	movl	41104(%rdx), %edx
	leal	1(%rdx), %eax
	movl	%eax, 41104(%r12)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L4295
.L4306:
	movq	%rbx, %rcx
.L4301:
	movq	120(%r12), %r13
.L4300:
	movq	%r14, 41088(%r12)
	movl	%edx, 41104(%r12)
	cmpq	%rcx, %rbx
	je	.L4293
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4293:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4295:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L4306
	movq	%r9, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver19HasInPrototypeChainEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L4298
	movl	41104(%r12), %eax
	movq	312(%r12), %r13
	movq	41096(%r12), %rcx
	leal	-1(%rax), %edx
	jmp	.L4300
	.p2align 4,,10
	.p2align 3
.L4307:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_HasInPrototypeChainEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L4298:
	.cfi_restore_state
	movl	41104(%r12), %ecx
	shrw	$8, %ax
	leal	-1(%rcx), %edx
	movq	41096(%r12), %rcx
	je	.L4301
	movq	112(%r12), %r13
	jmp	.L4300
	.cfi_endproc
.LFE21563:
	.size	_ZN2v88internal27Runtime_HasInPrototypeChainEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_HasInPrototypeChainEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_CreateIterResultObjectEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_CreateIterResultObjectEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_CreateIterResultObjectEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_CreateIterResultObjectEiPmPNS0_7IsolateE:
.LFB21566:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4314
	addl	$1, 41104(%rdx)
	movq	-8(%rsi), %rax
	leaq	-48(%rbp), %rdi
	movq	%rdx, %rsi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movzbl	%al, %edx
	call	_ZN2v88internal7Factory19NewJSIteratorResultENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4308
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4308:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4315
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4314:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL36Stats_Runtime_CreateIterResultObjectEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L4308
.L4315:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21566:
	.size	_ZN2v88internal30Runtime_CreateIterResultObjectEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_CreateIterResultObjectEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_CreateDataPropertyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_CreateDataPropertyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_CreateDataPropertyEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_CreateDataPropertyEiPmPNS0_7IsolateE:
.LFB21569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4329
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4319
.L4320:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4319:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L4320
	leaq	-144(%rbp), %r15
	leaq	-8(%rsi), %rcx
	movq	%rsi, %rdx
	movl	$1, %r9d
	leaq	-145(%rbp), %r8
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	cmpb	$0, -145(%rbp)
	je	.L4328
	leaq	-16(%r13), %rsi
	movl	$1, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L4328
	movq	-16(%r13), %r13
.L4322:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4316
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4316:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4330
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4328:
	.cfi_restore_state
	movq	312(%r12), %r13
	jmp	.L4322
	.p2align 4,,10
	.p2align 3
.L4329:
	call	_ZN2v88internalL32Stats_Runtime_CreateDataPropertyEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L4316
.L4330:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21569:
	.size	_ZN2v88internal26Runtime_CreateDataPropertyEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_CreateDataPropertyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateE:
.LFB21572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4345
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4334
.L4335:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4334:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L4335
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L4346
.L4336:
	leaq	.LC49(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4346:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L4336
	leaq	-80(%rbp), %r14
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	andb	$-64, -80(%rbp)
	movq	%r14, %rcx
	movups	%xmm0, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	call	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorE@PLT
	testb	%al, %al
	je	.L4347
	shrw	$8, %ax
	jne	.L4340
	movq	88(%r12), %r14
.L4339:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4331
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4331:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4348
	addq	$48, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4340:
	.cfi_restore_state
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal18PropertyDescriptor26ToPropertyDescriptorObjectEPNS0_7IsolateE@PLT
	movq	(%rax), %r14
	jmp	.L4339
	.p2align 4,,10
	.p2align 3
.L4347:
	movq	312(%r12), %r14
	jmp	.L4339
	.p2align 4,,10
	.p2align 3
.L4345:
	call	_ZN2v88internalL38Stats_Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateE
	movq	%rax, %r14
	jmp	.L4331
.L4348:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21572:
	.size	_ZN2v88internal32Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateE, .-_ZN2v88internal32Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_LoadPrivateSetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_LoadPrivateSetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_LoadPrivateSetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_LoadPrivateSetterEiPmPNS0_7IsolateE:
.LFB21575:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4356
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L4351
.L4352:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC17(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4351:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$79, 11(%rcx)
	jne	.L4352
	movq	15(%rax), %rax
	subl	$1, 41104(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L4356:
	jmp	_ZN2v88internalL31Stats_Runtime_LoadPrivateSetterEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21575:
	.size	_ZN2v88internal25Runtime_LoadPrivateSetterEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_LoadPrivateSetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_LoadPrivateGetterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_LoadPrivateGetterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_LoadPrivateGetterEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_LoadPrivateGetterEiPmPNS0_7IsolateE:
.LFB21578:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4364
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L4359
.L4360:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC17(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4359:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$79, 11(%rcx)
	jne	.L4360
	movq	7(%rax), %rax
	subl	$1, 41104(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L4364:
	jmp	_ZN2v88internalL31Stats_Runtime_LoadPrivateGetterEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE21578:
	.size	_ZN2v88internal25Runtime_LoadPrivateGetterEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_LoadPrivateGetterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateE:
.LFB21581:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4398
	addl	$1, 41104(%rdx)
	movq	%rdx, %rdi
	movq	41088(%rdx), %r15
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal7Factory15NewAccessorPairEv@PLT
	movq	-8(%r13), %r8
	movq	0(%r13), %r13
	movq	(%rax), %r14
	testb	$1, %r13b
	jne	.L4399
	movq	%r13, 7(%r14)
.L4368:
	testb	$1, %r8b
	jne	.L4400
	movq	%r8, 15(%r14)
.L4372:
	movq	(%rax), %r13
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4365
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4365:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4400:
	.cfi_restore_state
	movq	%r8, %r13
	andq	$-262144, %r13
	movq	24(%r13), %rdx
	cmpq	-37488(%rdx), %r8
	je	.L4372
	movq	%r8, 15(%r14)
	leaq	15(%r14), %rsi
	movq	8(%r13), %rdx
	testl	$262144, %edx
	je	.L4379
	movq	%r8, %rdx
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r13), %rdx
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rsi
.L4379:
	andl	$24, %edx
	je	.L4372
	movq	%r14, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L4372
	movq	%r8, %rdx
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rax
	jmp	.L4372
	.p2align 4,,10
	.p2align 3
.L4399:
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rdx
	cmpq	-37488(%rdx), %r13
	je	.L4368
	movq	%r13, 7(%r14)
	leaq	7(%r14), %rsi
	movq	8(%rcx), %rdx
	testl	$262144, %edx
	je	.L4376
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rsi
	movq	8(%rcx), %rdx
.L4376:
	andl	$24, %edx
	je	.L4368
	movq	%r14, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L4368
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%r8, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rax
	jmp	.L4368
	.p2align 4,,10
	.p2align 3
.L4398:
	addq	$40, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL36Stats_Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE21581:
	.size	_ZN2v88internal30Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_AddPrivateBrandEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_AddPrivateBrandEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_AddPrivateBrandEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_AddPrivateBrandEiPmPNS0_7IsolateE:
.LFB21584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4440
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r15
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L4404
.L4405:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4404:
	movq	-1(%rdx), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L4405
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r14
	testb	$1, %al
	jne	.L4441
.L4406:
	leaq	.LC21(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4441:
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L4406
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	jbe	.L4442
.L4409:
	testb	$1, %dl
	jne	.L4416
.L4418:
	movl	$-1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rcx
.L4417:
	movq	-8(%r13), %rdx
	movl	$1, %eax
	movq	-1(%rdx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L4419
	movl	11(%rdx), %eax
	notl	%eax
	andl	$1, %eax
.L4419:
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	-8(%r13), %rax
	movq	%r12, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r14, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L4443
.L4420:
	leaq	-224(%rbp), %rdi
	movq	%rax, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L4415:
	cmpl	$4, -220(%rbp)
	je	.L4421
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$175, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L4422:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4401
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4401:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4444
	addq	$216, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4421:
	.cfi_restore_state
	xorl	%r8d, %r8d
	leaq	-224(%rbp), %rdi
	movl	$7, %edx
	movq	%r14, %rsi
	movabsq	$4294967297, %rcx
	call	_ZN2v88internal6Object15AddDataPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS0_11StoreOriginE@PLT
	testb	%al, %al
	je	.L4445
.L4423:
	shrw	$8, %ax
	je	.L4446
	movq	0(%r13), %r13
	jmp	.L4422
	.p2align 4,,10
	.p2align 3
.L4442:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L4410
	testb	$2, %al
	jne	.L4409
.L4410:
	leaq	-144(%rbp), %r8
	leaq	-228(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-248(%rbp), %r8
	testb	%al, %al
	je	.L4447
	movq	0(%r13), %rax
	movl	-228(%rbp), %edx
	testb	$1, %al
	jne	.L4412
.L4414:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, -256(%rbp)
	movl	%edx, -248(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movl	-248(%rbp), %edx
	movq	-256(%rbp), %r8
.L4413:
	movabsq	$824633720832, %rcx
	movq	%r8, %rdi
	movq	%r12, -120(%rbp)
	movl	$1, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%edx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r14, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L4415
	.p2align 4,,10
	.p2align 3
.L4416:
	movq	-1(%rdx), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L4418
	movq	%r13, %rcx
	jmp	.L4417
	.p2align 4,,10
	.p2align 3
.L4412:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L4414
	movq	%r13, %rax
	jmp	.L4413
	.p2align 4,,10
	.p2align 3
.L4440:
	call	_ZN2v88internalL29Stats_Runtime_AddPrivateBrandEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L4401
	.p2align 4,,10
	.p2align 3
.L4443:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rcx, -248(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-248(%rbp), %rcx
	jmp	.L4420
	.p2align 4,,10
	.p2align 3
.L4447:
	movq	0(%r13), %rdx
	jmp	.L4409
	.p2align 4,,10
	.p2align 3
.L4446:
	leaq	.LC22(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4445:
	movl	%eax, -248(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-248(%rbp), %eax
	jmp	.L4423
.L4444:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21584:
	.size	_ZN2v88internal23Runtime_AddPrivateBrandEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_AddPrivateBrandEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_AddPrivateFieldEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_AddPrivateFieldEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_AddPrivateFieldEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_AddPrivateFieldEiPmPNS0_7IsolateE:
.LFB21587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4487
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L4451
.L4452:
	leaq	.LC12(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4451:
	movq	-1(%rdx), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L4452
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r13
	testb	$1, %al
	jne	.L4488
.L4453:
	leaq	.LC21(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4488:
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L4453
	leaq	-16(%rsi), %rcx
	movq	%rcx, -248(%rbp)
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	jbe	.L4489
.L4456:
	testb	$1, %dl
	jne	.L4463
.L4465:
	movl	$-1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rcx
.L4464:
	movq	-8(%r15), %rdx
	movl	$1, %eax
	movq	-1(%rdx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L4466
	movl	11(%rdx), %eax
	notl	%eax
	andl	$1, %eax
.L4466:
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	-8(%r15), %rax
	movq	%r12, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r13, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L4490
.L4467:
	leaq	-224(%rbp), %rdi
	movq	%rax, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r15, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L4462:
	cmpl	$4, -220(%rbp)
	je	.L4468
	movq	%r13, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$175, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L4469:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4448
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4448:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4491
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4468:
	.cfi_restore_state
	movq	-248(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	leaq	-224(%rbp), %rdi
	movabsq	$4294967297, %rcx
	call	_ZN2v88internal6Object15AddDataPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS0_11StoreOriginE@PLT
	testb	%al, %al
	je	.L4492
.L4470:
	shrw	$8, %ax
	je	.L4493
	movq	88(%r12), %r13
	jmp	.L4469
	.p2align 4,,10
	.p2align 3
.L4489:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L4457
	testb	$2, %al
	jne	.L4456
.L4457:
	leaq	-144(%rbp), %r9
	leaq	-228(%rbp), %rsi
	movq	%r9, %rdi
	movq	%r9, -256(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-256(%rbp), %r9
	testb	%al, %al
	je	.L4494
	movq	(%r15), %rax
	movl	-228(%rbp), %edx
	testb	$1, %al
	jne	.L4459
.L4461:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r9, -264(%rbp)
	movl	%edx, -256(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movl	-256(%rbp), %edx
	movq	-264(%rbp), %r9
.L4460:
	movabsq	$824633720832, %rcx
	movq	%r9, %rdi
	movq	%r12, -120(%rbp)
	movl	$1, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r15, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%edx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r13, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L4462
	.p2align 4,,10
	.p2align 3
.L4463:
	movq	-1(%rdx), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L4465
	movq	%r15, %rcx
	jmp	.L4464
	.p2align 4,,10
	.p2align 3
.L4459:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L4461
	movq	%r15, %rax
	jmp	.L4460
	.p2align 4,,10
	.p2align 3
.L4487:
	call	_ZN2v88internalL29Stats_Runtime_AddPrivateFieldEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L4448
	.p2align 4,,10
	.p2align 3
.L4490:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rcx, -256(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-256(%rbp), %rcx
	jmp	.L4467
	.p2align 4,,10
	.p2align 3
.L4494:
	movq	(%r15), %rdx
	jmp	.L4456
	.p2align 4,,10
	.p2align 3
.L4493:
	leaq	.LC27(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4492:
	movl	%eax, -248(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-248(%rbp), %eax
	jmp	.L4470
.L4491:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21587:
	.size	_ZN2v88internal23Runtime_AddPrivateFieldEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_AddPrivateFieldEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.str1.1,"aMS",@progbits,1
.LC95:
	.string	"NewArray"
	.section	.text._ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m,"axG",@progbits,_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m,comdat
	.p2align 4
	.weak	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	.type	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m, @function
_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m:
.LFB25301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZSt7nothrow(%rip), %rsi
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdi
	movq	$-1, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	0(,%rdi,8), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmova	%rax, %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L4497
	movq	%rbx, %rsi
	subq	$1, %rsi
	js	.L4495
	leaq	-2(%rbx), %rdx
	movl	$1, %edi
	cmpq	$-1, %rdx
	cmovge	%rbx, %rdi
	cmpq	$1, %rbx
	je	.L4510
	cmpq	$-1, %rdx
	jl	.L4510
	movq	%rdi, %rsi
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	shrq	%rsi
	.p2align 4,,10
	.p2align 3
.L4501:
	movq	%rdx, %rcx
	addq	$1, %rdx
	salq	$4, %rcx
	movups	%xmm0, (%rax,%rcx)
	cmpq	%rdx, %rsi
	jne	.L4501
	movq	%rdi, %rcx
	andq	$-2, %rcx
	leaq	(%rax,%rcx,8), %rdx
	cmpq	%rcx, %rdi
	je	.L4495
.L4499:
	movq	$0, (%rdx)
.L4495:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4510:
	.cfi_restore_state
	movq	%rax, %rdx
	jmp	.L4499
.L4497:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L4514
	movq	%rbx, %rdi
	subq	$1, %rdi
	js	.L4495
	leaq	-2(%rbx), %rcx
	movl	$1, %edx
	addq	$1, %rcx
	cmovge	%rbx, %rdx
	jl	.L4511
	subq	$1, %rbx
	je	.L4511
	movq	%rdx, %rsi
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	shrq	%rsi
.L4505:
	movq	%rcx, %rdi
	addq	$1, %rcx
	salq	$4, %rdi
	movups	%xmm0, (%rax,%rdi)
	cmpq	%rcx, %rsi
	jne	.L4505
	movq	%rdx, %rsi
	andq	$-2, %rsi
	leaq	(%rax,%rsi,8), %rcx
	cmpq	%rsi, %rdx
	je	.L4495
.L4503:
	movq	$0, (%rcx)
	jmp	.L4495
.L4514:
	leaq	.LC95(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L4511:
	movq	%rax, %rcx
	jmp	.L4503
	.cfi_endproc
.LFE25301:
	.size	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m, .-_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	.section	.rodata._ZN2v88internalL54Stats_Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC96:
	.string	"V8.Runtime_Runtime_CopyDataPropertiesWithExcludedProperties"
	.section	.text._ZN2v88internalL54Stats_Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL54Stats_Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateE, @function
_ZN2v88internalL54Stats_Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateE:
.LFB21538:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4571
.L4516:
	movq	_ZZN2v88internalL54Stats_Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1054(%rip), %rbx
	testq	%rbx, %rbx
	je	.L4572
.L4518:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L4573
.L4520:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	cmpq	%rax, 104(%r12)
	jne	.L4574
.L4524:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	%rax, %r13
.L4527:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4541
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4541:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4575
.L4515:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4576
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4574:
	.cfi_restore_state
	cmpq	%rax, 88(%r12)
	je	.L4524
	leal	-1(%r15), %edx
	movslq	%edx, %rdx
	movq	%rdx, %rdi
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	movq	-200(%rbp), %rdx
	movq	%rax, -176(%rbp)
	movq	%rdx, -168(%rbp)
	cmpl	$1, %r15d
	jle	.L4526
	leal	-2(%r15), %eax
	leaq	-16(%r13), %r9
	salq	$3, %rax
	leaq	-8(%r13), %r8
	subq	%rax, %r9
	leaq	-188(%rbp), %rax
	movq	%r8, %r15
	movq	%rax, -224(%rbp)
	jmp	.L4532
	.p2align 4,,10
	.p2align 3
.L4529:
	movq	-176(%rbp), %rsi
	movq	%r8, %rax
	subq	%r15, %rax
	subq	$8, %r15
	movq	%rdx, (%rsi,%rax)
	cmpq	%r15, %r9
	je	.L4526
.L4532:
	movq	(%r15), %rax
	movq	%r15, %rdx
	testb	$1, %al
	je	.L4529
	movq	-1(%rax), %rsi
	cmpw	$63, 11(%rsi)
	ja	.L4529
	movq	%rax, -184(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L4531
	testb	$2, %al
	jne	.L4529
.L4531:
	movq	-224(%rbp), %rsi
	leaq	-184(%rbp), %rdi
	movq	%r8, -216(%rbp)
	movq	%r9, -208(%rbp)
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-200(%rbp), %rdx
	movq	-208(%rbp), %r9
	testb	%al, %al
	movq	-216(%rbp), %r8
	je	.L4529
	movl	-188(%rbp), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, -208(%rbp)
	movq	%r9, -200(%rbp)
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	-208(%rbp), %r8
	movq	-200(%rbp), %r9
	movq	%rax, %rdx
	jmp	.L4529
	.p2align 4,,10
	.p2align 3
.L4573:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4577
.L4521:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4522
	movq	(%rdi), %rax
	call	*8(%rax)
.L4522:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4523
	movq	(%rdi), %rax
	call	*8(%rax)
.L4523:
	leaq	.LC96(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L4520
	.p2align 4,,10
	.p2align 3
.L4572:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4578
.L4519:
	movq	%rbx, _ZZN2v88internalL54Stats_Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1054(%rip)
	jmp	.L4518
	.p2align 4,,10
	.p2align 3
.L4526:
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r15
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4533
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L4534:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-176(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal10JSReceiver23SetOrCopyDataPropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPKNS0_12ScopedVectorIS7_EEb@PLT
	testb	%al, %al
	jne	.L4536
	movq	312(%r12), %r13
.L4537:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4527
	call	_ZdaPv@PLT
	jmp	.L4527
	.p2align 4,,10
	.p2align 3
.L4575:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L4515
	.p2align 4,,10
	.p2align 3
.L4571:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$413, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4516
	.p2align 4,,10
	.p2align 3
.L4536:
	movq	(%r15), %r13
	jmp	.L4537
	.p2align 4,,10
	.p2align 3
.L4533:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L4579
.L4535:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L4534
	.p2align 4,,10
	.p2align 3
.L4578:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L4519
	.p2align 4,,10
	.p2align 3
.L4577:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC96(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L4521
.L4579:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4535
.L4576:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21538:
	.size	_ZN2v88internalL54Stats_Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateE, .-_ZN2v88internalL54Stats_Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal48Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal48Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal48Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateE, @function
_ZN2v88internal48Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateE:
.LFB21539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4612
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %r13
	movq	(%rsi), %rax
	cmpq	%rax, 104(%rdx)
	jne	.L4613
.L4583:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	%rax, %r15
.L4586:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L4580
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4580:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4614
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4613:
	.cfi_restore_state
	cmpq	%rax, 88(%rdx)
	je	.L4583
	leal	-1(%rdi), %ebx
	movq	%rsi, -104(%rbp)
	movslq	%ebx, %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	cmpl	$1, %r15d
	movq	%rbx, -72(%rbp)
	movq	-104(%rbp), %r9
	movq	%rax, -80(%rbp)
	jle	.L4585
	leal	-2(%r15), %eax
	leaq	-16(%r9), %rcx
	salq	$3, %rax
	leaq	-8(%r9), %r8
	subq	%rax, %rcx
	leaq	-92(%rbp), %rax
	movq	%r8, %rbx
	movq	%rax, -128(%rbp)
	movq	%rcx, %r15
	jmp	.L4591
	.p2align 4,,10
	.p2align 3
.L4588:
	movq	-80(%rbp), %rsi
	movq	%r8, %rax
	subq	%rbx, %rax
	subq	$8, %rbx
	movq	%rdx, (%rsi,%rax)
	cmpq	%r15, %rbx
	je	.L4585
.L4591:
	movq	(%rbx), %rax
	movq	%rbx, %rdx
	testb	$1, %al
	je	.L4588
	movq	-1(%rax), %rsi
	cmpw	$63, 11(%rsi)
	ja	.L4588
	movq	%rax, -88(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L4590
	testb	$2, %al
	jne	.L4588
.L4590:
	movq	-128(%rbp), %rsi
	leaq	-88(%rbp), %rdi
	movq	%r9, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r8
	testb	%al, %al
	movq	-120(%rbp), %r9
	je	.L4588
	movl	-92(%rbp), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L4588
	.p2align 4,,10
	.p2align 3
.L4585:
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r15
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4592
	movq	%r15, %rsi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-104(%rbp), %r9
	movq	%rax, %rsi
.L4593:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	-104(%rbp), %r9
	xorl	%r8d, %r8d
	leaq	-80(%rbp), %rcx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%r9, %rdx
	call	_ZN2v88internal10JSReceiver23SetOrCopyDataPropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPKNS0_12ScopedVectorIS7_EEb@PLT
	testb	%al, %al
	jne	.L4595
	movq	312(%r12), %r15
.L4596:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4586
	call	_ZdaPv@PLT
	jmp	.L4586
	.p2align 4,,10
	.p2align 3
.L4612:
	call	_ZN2v88internalL54Stats_Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateE
	movq	%rax, %r15
	jmp	.L4580
	.p2align 4,,10
	.p2align 3
.L4595:
	movq	(%rbx), %r15
	jmp	.L4596
	.p2align 4,,10
	.p2align 3
.L4592:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L4615
.L4594:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L4593
.L4615:
	movq	%r12, %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L4594
.L4614:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21539:
	.size	_ZN2v88internal48Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateE, .-_ZN2v88internal48Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb, @function
_GLOBAL__sub_I__ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb:
.LFB27198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27198:
	.size	_GLOBAL__sub_I__ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb, .-_GLOBAL__sub_I__ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb
	.section	.bss._ZZN2v88internalL29Stats_Runtime_AddPrivateFieldEiPmPNS0_7IsolateEE29trace_event_unique_atomic1256,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_AddPrivateFieldEiPmPNS0_7IsolateEE29trace_event_unique_atomic1256, @object
	.size	_ZZN2v88internalL29Stats_Runtime_AddPrivateFieldEiPmPNS0_7IsolateEE29trace_event_unique_atomic1256, 8
_ZZN2v88internalL29Stats_Runtime_AddPrivateFieldEiPmPNS0_7IsolateEE29trace_event_unique_atomic1256:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_AddPrivateBrandEiPmPNS0_7IsolateEE29trace_event_unique_atomic1231,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_AddPrivateBrandEiPmPNS0_7IsolateEE29trace_event_unique_atomic1231, @object
	.size	_ZZN2v88internalL29Stats_Runtime_AddPrivateBrandEiPmPNS0_7IsolateEE29trace_event_unique_atomic1231, 8
_ZZN2v88internalL29Stats_Runtime_AddPrivateBrandEiPmPNS0_7IsolateEE29trace_event_unique_atomic1231:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1221,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1221, @object
	.size	_ZZN2v88internalL36Stats_Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1221, 8
_ZZN2v88internalL36Stats_Runtime_CreatePrivateAccessorsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1221:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_LoadPrivateGetterEiPmPNS0_7IsolateEE29trace_event_unique_atomic1213,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_LoadPrivateGetterEiPmPNS0_7IsolateEE29trace_event_unique_atomic1213, @object
	.size	_ZZN2v88internalL31Stats_Runtime_LoadPrivateGetterEiPmPNS0_7IsolateEE29trace_event_unique_atomic1213, 8
_ZZN2v88internalL31Stats_Runtime_LoadPrivateGetterEiPmPNS0_7IsolateEE29trace_event_unique_atomic1213:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_LoadPrivateSetterEiPmPNS0_7IsolateEE29trace_event_unique_atomic1205,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_LoadPrivateSetterEiPmPNS0_7IsolateEE29trace_event_unique_atomic1205, @object
	.size	_ZZN2v88internalL31Stats_Runtime_LoadPrivateSetterEiPmPNS0_7IsolateEE29trace_event_unique_atomic1205, 8
_ZZN2v88internalL31Stats_Runtime_LoadPrivateSetterEiPmPNS0_7IsolateEE29trace_event_unique_atomic1205:
	.zero	8
	.section	.bss._ZZN2v88internalL38Stats_Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1189,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Stats_Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1189, @object
	.size	_ZZN2v88internalL38Stats_Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1189, 8
_ZZN2v88internalL38Stats_Runtime_GetOwnPropertyDescriptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1189:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_CreateDataPropertyEiPmPNS0_7IsolateEE29trace_event_unique_atomic1174,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_CreateDataPropertyEiPmPNS0_7IsolateEE29trace_event_unique_atomic1174, @object
	.size	_ZZN2v88internalL32Stats_Runtime_CreateDataPropertyEiPmPNS0_7IsolateEE29trace_event_unique_atomic1174, 8
_ZZN2v88internalL32Stats_Runtime_CreateDataPropertyEiPmPNS0_7IsolateEE29trace_event_unique_atomic1174:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_CreateIterResultObjectEiPmPNS0_7IsolateEE29trace_event_unique_atomic1165,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_CreateIterResultObjectEiPmPNS0_7IsolateEE29trace_event_unique_atomic1165, @object
	.size	_ZZN2v88internalL36Stats_Runtime_CreateIterResultObjectEiPmPNS0_7IsolateEE29trace_event_unique_atomic1165, 8
_ZZN2v88internalL36Stats_Runtime_CreateIterResultObjectEiPmPNS0_7IsolateEE29trace_event_unique_atomic1165:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_HasInPrototypeChainEiPmPNS0_7IsolateEE29trace_event_unique_atomic1152,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_HasInPrototypeChainEiPmPNS0_7IsolateEE29trace_event_unique_atomic1152, @object
	.size	_ZZN2v88internalL33Stats_Runtime_HasInPrototypeChainEiPmPNS0_7IsolateEE29trace_event_unique_atomic1152, 8
_ZZN2v88internalL33Stats_Runtime_HasInPrototypeChainEiPmPNS0_7IsolateEE29trace_event_unique_atomic1152:
	.zero	8
	.section	.bss._ZZN2v88internalL20Stats_Runtime_ToNameEiPmPNS0_7IsolateEE29trace_event_unique_atomic1145,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL20Stats_Runtime_ToNameEiPmPNS0_7IsolateEE29trace_event_unique_atomic1145, @object
	.size	_ZZN2v88internalL20Stats_Runtime_ToNameEiPmPNS0_7IsolateEE29trace_event_unique_atomic1145, 8
_ZZN2v88internalL20Stats_Runtime_ToNameEiPmPNS0_7IsolateEE29trace_event_unique_atomic1145:
	.zero	8
	.section	.bss._ZZN2v88internalL24Stats_Runtime_ToStringRTEiPmPNS0_7IsolateEE29trace_event_unique_atomic1138,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL24Stats_Runtime_ToStringRTEiPmPNS0_7IsolateEE29trace_event_unique_atomic1138, @object
	.size	_ZZN2v88internalL24Stats_Runtime_ToStringRTEiPmPNS0_7IsolateEE29trace_event_unique_atomic1138, 8
_ZZN2v88internalL24Stats_Runtime_ToStringRTEiPmPNS0_7IsolateEE29trace_event_unique_atomic1138:
	.zero	8
	.section	.bss._ZZN2v88internalL22Stats_Runtime_ToLengthEiPmPNS0_7IsolateEE29trace_event_unique_atomic1131,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL22Stats_Runtime_ToLengthEiPmPNS0_7IsolateEE29trace_event_unique_atomic1131, @object
	.size	_ZZN2v88internalL22Stats_Runtime_ToLengthEiPmPNS0_7IsolateEE29trace_event_unique_atomic1131, 8
_ZZN2v88internalL22Stats_Runtime_ToLengthEiPmPNS0_7IsolateEE29trace_event_unique_atomic1131:
	.zero	8
	.section	.bss._ZZN2v88internalL23Stats_Runtime_ToNumericEiPmPNS0_7IsolateEE29trace_event_unique_atomic1124,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL23Stats_Runtime_ToNumericEiPmPNS0_7IsolateEE29trace_event_unique_atomic1124, @object
	.size	_ZZN2v88internalL23Stats_Runtime_ToNumericEiPmPNS0_7IsolateEE29trace_event_unique_atomic1124, 8
_ZZN2v88internalL23Stats_Runtime_ToNumericEiPmPNS0_7IsolateEE29trace_event_unique_atomic1124:
	.zero	8
	.section	.bss._ZZN2v88internalL22Stats_Runtime_ToNumberEiPmPNS0_7IsolateEE29trace_event_unique_atomic1117,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL22Stats_Runtime_ToNumberEiPmPNS0_7IsolateEE29trace_event_unique_atomic1117, @object
	.size	_ZZN2v88internalL22Stats_Runtime_ToNumberEiPmPNS0_7IsolateEE29trace_event_unique_atomic1117, 8
_ZZN2v88internalL22Stats_Runtime_ToNumberEiPmPNS0_7IsolateEE29trace_event_unique_atomic1117:
	.zero	8
	.section	.bss._ZZN2v88internalL22Stats_Runtime_ToObjectEiPmPNS0_7IsolateEE29trace_event_unique_atomic1111,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL22Stats_Runtime_ToObjectEiPmPNS0_7IsolateEE29trace_event_unique_atomic1111, @object
	.size	_ZZN2v88internalL22Stats_Runtime_ToObjectEiPmPNS0_7IsolateEE29trace_event_unique_atomic1111, 8
_ZZN2v88internalL22Stats_Runtime_ToObjectEiPmPNS0_7IsolateEE29trace_event_unique_atomic1111:
	.zero	8
	.section	.bss._ZZN2v88internalL43Stats_Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateEE29trace_event_unique_atomic1088,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Stats_Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateEE29trace_event_unique_atomic1088, @object
	.size	_ZZN2v88internalL43Stats_Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateEE29trace_event_unique_atomic1088, 8
_ZZN2v88internalL43Stats_Runtime_DefineSetterPropertyUncheckedEiPmPNS0_7IsolateEE29trace_event_unique_atomic1088:
	.zero	8
	.section	.bss._ZZN2v88internalL54Stats_Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1054,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL54Stats_Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1054, @object
	.size	_ZZN2v88internalL54Stats_Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1054, 8
_ZZN2v88internalL54Stats_Runtime_CopyDataPropertiesWithExcludedPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1054:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_CopyDataPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1037,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_CopyDataPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1037, @object
	.size	_ZZN2v88internalL32Stats_Runtime_CopyDataPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1037, 8
_ZZN2v88internalL32Stats_Runtime_CopyDataPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1037:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_SetDataPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1021,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_SetDataPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1021, @object
	.size	_ZZN2v88internalL31Stats_Runtime_SetDataPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1021, 8
_ZZN2v88internalL31Stats_Runtime_SetDataPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1021:
	.zero	8
	.section	.bss._ZZN2v88internalL43Stats_Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateEE28trace_event_unique_atomic998,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Stats_Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateEE28trace_event_unique_atomic998, @object
	.size	_ZZN2v88internalL43Stats_Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateEE28trace_event_unique_atomic998, 8
_ZZN2v88internalL43Stats_Runtime_DefineGetterPropertyUncheckedEiPmPNS0_7IsolateEE28trace_event_unique_atomic998:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_GetFunctionNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic991,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_GetFunctionNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic991, @object
	.size	_ZZN2v88internalL29Stats_Runtime_GetFunctionNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic991, 8
_ZZN2v88internalL29Stats_Runtime_GetFunctionNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic991:
	.zero	8
	.section	.bss._ZZN2v88internalL21Stats_Runtime_ClassOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic983,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL21Stats_Runtime_ClassOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic983, @object
	.size	_ZZN2v88internalL21Stats_Runtime_ClassOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic983, 8
_ZZN2v88internalL21Stats_Runtime_ClassOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic983:
	.zero	8
	.section	.bss._ZZN2v88internalL26Stats_Runtime_IsJSReceiverEiPmPNS0_7IsolateEE28trace_event_unique_atomic976,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL26Stats_Runtime_IsJSReceiverEiPmPNS0_7IsolateEE28trace_event_unique_atomic976, @object
	.size	_ZZN2v88internalL26Stats_Runtime_IsJSReceiverEiPmPNS0_7IsolateEE28trace_event_unique_atomic976, 8
_ZZN2v88internalL26Stats_Runtime_IsJSReceiverEiPmPNS0_7IsolateEE28trace_event_unique_atomic976:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_HasFastPackedElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic968,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_HasFastPackedElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic968, @object
	.size	_ZZN2v88internalL35Stats_Runtime_HasFastPackedElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic968, 8
_ZZN2v88internalL35Stats_Runtime_HasFastPackedElementsEiPmPNS0_7IsolateEE28trace_event_unique_atomic968:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_CollectTypeProfileEiPmPNS0_7IsolateEE28trace_event_unique_atomic944,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_CollectTypeProfileEiPmPNS0_7IsolateEE28trace_event_unique_atomic944, @object
	.size	_ZZN2v88internalL32Stats_Runtime_CollectTypeProfileEiPmPNS0_7IsolateEE28trace_event_unique_atomic944, 8
_ZZN2v88internalL32Stats_Runtime_CollectTypeProfileEiPmPNS0_7IsolateEE28trace_event_unique_atomic944:
	.zero	8
	.section	.bss._ZZN2v88internalL41Stats_Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic885,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Stats_Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic885, @object
	.size	_ZZN2v88internalL41Stats_Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic885, 8
_ZZN2v88internalL41Stats_Runtime_DefineDataPropertyInLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic885:
	.zero	8
	.section	.bss._ZZN2v88internalL45Stats_Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateEE28trace_event_unique_atomic868,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL45Stats_Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateEE28trace_event_unique_atomic868, @object
	.size	_ZZN2v88internalL45Stats_Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateEE28trace_event_unique_atomic868, 8
_ZZN2v88internalL45Stats_Runtime_DefineAccessorPropertyUncheckedEiPmPNS0_7IsolateEE28trace_event_unique_atomic868:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_TryMigrateInstanceEiPmPNS0_7IsolateEE28trace_event_unique_atomic842,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_TryMigrateInstanceEiPmPNS0_7IsolateEE28trace_event_unique_atomic842, @object
	.size	_ZZN2v88internalL32Stats_Runtime_TryMigrateInstanceEiPmPNS0_7IsolateEE28trace_event_unique_atomic842, 8
_ZZN2v88internalL32Stats_Runtime_TryMigrateInstanceEiPmPNS0_7IsolateEE28trace_event_unique_atomic842:
	.zero	8
	.section	.bss._ZZN2v88internalL49Stats_Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateEE28trace_event_unique_atomic831,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL49Stats_Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateEE28trace_event_unique_atomic831, @object
	.size	_ZZN2v88internalL49Stats_Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateEE28trace_event_unique_atomic831, 8
_ZZN2v88internalL49Stats_Runtime_CompleteInobjectSlackTrackingForMapEiPmPNS0_7IsolateEE28trace_event_unique_atomic831:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_GetDerivedMapEiPmPNS0_7IsolateEE28trace_event_unique_atomic822,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_GetDerivedMapEiPmPNS0_7IsolateEE28trace_event_unique_atomic822, @object
	.size	_ZZN2v88internalL27Stats_Runtime_GetDerivedMapEiPmPNS0_7IsolateEE28trace_event_unique_atomic822, 8
_ZZN2v88internalL27Stats_Runtime_GetDerivedMapEiPmPNS0_7IsolateEE28trace_event_unique_atomic822:
	.zero	8
	.section	.bss._ZZN2v88internalL23Stats_Runtime_NewObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic812,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL23Stats_Runtime_NewObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic812, @object
	.size	_ZZN2v88internalL23Stats_Runtime_NewObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic812, 8
_ZZN2v88internalL23Stats_Runtime_NewObjectEiPmPNS0_7IsolateEE28trace_event_unique_atomic812:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_AllocateHeapNumberEiPmPNS0_7IsolateEE28trace_event_unique_atomic806,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_AllocateHeapNumberEiPmPNS0_7IsolateEE28trace_event_unique_atomic806, @object
	.size	_ZZN2v88internalL32Stats_Runtime_AllocateHeapNumberEiPmPNS0_7IsolateEE28trace_event_unique_atomic806, 8
_ZZN2v88internalL32Stats_Runtime_AllocateHeapNumberEiPmPNS0_7IsolateEE28trace_event_unique_atomic806:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_ToFastPropertiesEiPmPNS0_7IsolateEE28trace_event_unique_atomic795,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_ToFastPropertiesEiPmPNS0_7IsolateEE28trace_event_unique_atomic795, @object
	.size	_ZZN2v88internalL30Stats_Runtime_ToFastPropertiesEiPmPNS0_7IsolateEE28trace_event_unique_atomic795, 8
_ZZN2v88internalL30Stats_Runtime_ToFastPropertiesEiPmPNS0_7IsolateEE28trace_event_unique_atomic795:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateEE28trace_event_unique_atomic779,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateEE28trace_event_unique_atomic779, @object
	.size	_ZZN2v88internalL32Stats_Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateEE28trace_event_unique_atomic779, 8
_ZZN2v88internalL32Stats_Runtime_GetOwnPropertyKeysEiPmPNS0_7IsolateEE28trace_event_unique_atomic779:
	.zero	8
	.section	.bss._ZZN2v88internalL25Stats_Runtime_HasPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic754,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL25Stats_Runtime_HasPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic754, @object
	.size	_ZZN2v88internalL25Stats_Runtime_HasPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic754, 8
_ZZN2v88internalL25Stats_Runtime_HasPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic754:
	.zero	8
	.section	.bss._ZZN2v88internalL38Stats_Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateEE28trace_event_unique_atomic742,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Stats_Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateEE28trace_event_unique_atomic742, @object
	.size	_ZZN2v88internalL38Stats_Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateEE28trace_event_unique_atomic742, 8
_ZZN2v88internalL38Stats_Runtime_ShrinkPropertyDictionaryEiPmPNS0_7IsolateEE28trace_event_unique_atomic742:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_DeletePropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic732,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_DeletePropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic732, @object
	.size	_ZZN2v88internalL28Stats_Runtime_DeletePropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic732, 8
_ZZN2v88internalL28Stats_Runtime_DeletePropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic732:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic695,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic695, @object
	.size	_ZZN2v88internalL40Stats_Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic695, 8
_ZZN2v88internalL40Stats_Runtime_StoreDataPropertyInLiteralEiPmPNS0_7IsolateEE28trace_event_unique_atomic695:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_SetNamedPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic678,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_SetNamedPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic678, @object
	.size	_ZZN2v88internalL30Stats_Runtime_SetNamedPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic678, 8
_ZZN2v88internalL30Stats_Runtime_SetNamedPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic678:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_SetKeyedPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic665,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_SetKeyedPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic665, @object
	.size	_ZZN2v88internalL30Stats_Runtime_SetKeyedPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic665, 8
_ZZN2v88internalL30Stats_Runtime_SetKeyedPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic665:
	.zero	8
	.section	.bss._ZZN2v88internalL25Stats_Runtime_GetPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic575,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL25Stats_Runtime_GetPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic575, @object
	.size	_ZZN2v88internalL25Stats_Runtime_GetPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic575, 8
_ZZN2v88internalL25Stats_Runtime_GetPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic575:
	.zero	8
	.section	.bss._ZZN2v88internalL47Stats_Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic562,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Stats_Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic562, @object
	.size	_ZZN2v88internalL47Stats_Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic562, 8
_ZZN2v88internalL47Stats_Runtime_JSReceiverSetPrototypeOfDontThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic562:
	.zero	8
	.section	.bss._ZZN2v88internalL43Stats_Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic549,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Stats_Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic549, @object
	.size	_ZZN2v88internalL43Stats_Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic549, 8
_ZZN2v88internalL43Stats_Runtime_JSReceiverSetPrototypeOfThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic549:
	.zero	8
	.section	.bss._ZZN2v88internalL38Stats_Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic540,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Stats_Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic540, @object
	.size	_ZZN2v88internalL38Stats_Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic540, 8
_ZZN2v88internalL38Stats_Runtime_JSReceiverGetPrototypeOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic540:
	.zero	8
	.section	.bss._ZZN2v88internalL50Stats_Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic529,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL50Stats_Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic529, @object
	.size	_ZZN2v88internalL50Stats_Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic529, 8
_ZZN2v88internalL50Stats_Runtime_JSReceiverPreventExtensionsDontThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic529:
	.zero	8
	.section	.bss._ZZN2v88internalL46Stats_Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic518,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL46Stats_Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic518, @object
	.size	_ZZN2v88internalL46Stats_Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic518, 8
_ZZN2v88internalL46Stats_Runtime_JSReceiverPreventExtensionsThrowEiPmPNS0_7IsolateEE28trace_event_unique_atomic518:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateEE28trace_event_unique_atomic505,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateEE28trace_event_unique_atomic505, @object
	.size	_ZZN2v88internalL32Stats_Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateEE28trace_event_unique_atomic505, 8
_ZZN2v88internalL32Stats_Runtime_ObjectIsExtensibleEiPmPNS0_7IsolateEE28trace_event_unique_atomic505:
	.zero	8
	.section	.bss._ZZN2v88internalL39Stats_Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic491,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Stats_Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic491, @object
	.size	_ZZN2v88internalL39Stats_Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic491, 8
_ZZN2v88internalL39Stats_Runtime_ObjectEntriesSkipFastPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic491:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_ObjectEntriesEiPmPNS0_7IsolateEE28trace_event_unique_atomic477,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_ObjectEntriesEiPmPNS0_7IsolateEE28trace_event_unique_atomic477, @object
	.size	_ZZN2v88internalL27Stats_Runtime_ObjectEntriesEiPmPNS0_7IsolateEE28trace_event_unique_atomic477, 8
_ZZN2v88internalL27Stats_Runtime_ObjectEntriesEiPmPNS0_7IsolateEE28trace_event_unique_atomic477:
	.zero	8
	.section	.bss._ZZN2v88internalL38Stats_Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic463,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Stats_Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic463, @object
	.size	_ZZN2v88internalL38Stats_Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic463, 8
_ZZN2v88internalL38Stats_Runtime_ObjectValuesSkipFastPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic463:
	.zero	8
	.section	.bss._ZZN2v88internalL26Stats_Runtime_ObjectValuesEiPmPNS0_7IsolateEE28trace_event_unique_atomic449,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL26Stats_Runtime_ObjectValuesEiPmPNS0_7IsolateEE28trace_event_unique_atomic449, @object
	.size	_ZZN2v88internalL26Stats_Runtime_ObjectValuesEiPmPNS0_7IsolateEE28trace_event_unique_atomic449, 8
_ZZN2v88internalL26Stats_Runtime_ObjectValuesEiPmPNS0_7IsolateEE28trace_event_unique_atomic449:
	.zero	8
	.section	.bss._ZZN2v88internalL55Stats_Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateEE28trace_event_unique_atomic435,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL55Stats_Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateEE28trace_event_unique_atomic435, @object
	.size	_ZZN2v88internalL55Stats_Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateEE28trace_event_unique_atomic435, 8
_ZZN2v88internalL55Stats_Runtime_OptimizeObjectForAddingMultiplePropertiesEiPmPNS0_7IsolateEE28trace_event_unique_atomic435:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_InternalSetPrototypeEiPmPNS0_7IsolateEE28trace_event_unique_atomic425,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_InternalSetPrototypeEiPmPNS0_7IsolateEE28trace_event_unique_atomic425, @object
	.size	_ZZN2v88internalL34Stats_Runtime_InternalSetPrototypeEiPmPNS0_7IsolateEE28trace_event_unique_atomic425, 8
_ZZN2v88internalL34Stats_Runtime_InternalSetPrototypeEiPmPNS0_7IsolateEE28trace_event_unique_atomic425:
	.zero	8
	.section	.bss._ZZN2v88internalL26Stats_Runtime_ObjectCreateEiPmPNS0_7IsolateEE28trace_event_unique_atomic367,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL26Stats_Runtime_ObjectCreateEiPmPNS0_7IsolateEE28trace_event_unique_atomic367, @object
	.size	_ZZN2v88internalL26Stats_Runtime_ObjectCreateEiPmPNS0_7IsolateEE28trace_event_unique_atomic367, 8
_ZZN2v88internalL26Stats_Runtime_ObjectCreateEiPmPNS0_7IsolateEE28trace_event_unique_atomic367:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic348,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic348, @object
	.size	_ZZN2v88internalL35Stats_Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic348, 8
_ZZN2v88internalL35Stats_Runtime_AddDictionaryPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic348:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic260,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic260, @object
	.size	_ZZN2v88internalL34Stats_Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic260, 8
_ZZN2v88internalL34Stats_Runtime_ObjectHasOwnPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic260:
	.zero	8
	.section	.bss._ZZN2v88internalL46Stats_Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateEE28trace_event_unique_atomic229,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL46Stats_Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateEE28trace_event_unique_atomic229, @object
	.size	_ZZN2v88internalL46Stats_Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateEE28trace_event_unique_atomic229, 8
_ZZN2v88internalL46Stats_Runtime_ObjectGetOwnPropertyNamesTryFastEiPmPNS0_7IsolateEE28trace_event_unique_atomic229:
	.zero	8
	.section	.bss._ZZN2v88internalL39Stats_Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateEE28trace_event_unique_atomic208,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Stats_Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateEE28trace_event_unique_atomic208, @object
	.size	_ZZN2v88internalL39Stats_Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateEE28trace_event_unique_atomic208, 8
_ZZN2v88internalL39Stats_Runtime_ObjectGetOwnPropertyNamesEiPmPNS0_7IsolateEE28trace_event_unique_atomic208:
	.zero	8
	.section	.bss._ZZN2v88internalL24Stats_Runtime_ObjectKeysEiPmPNS0_7IsolateEE28trace_event_unique_atomic188,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL24Stats_Runtime_ObjectKeysEiPmPNS0_7IsolateEE28trace_event_unique_atomic188, @object
	.size	_ZZN2v88internalL24Stats_Runtime_ObjectKeysEiPmPNS0_7IsolateEE28trace_event_unique_atomic188, 8
_ZZN2v88internalL24Stats_Runtime_ObjectKeysEiPmPNS0_7IsolateEE28trace_event_unique_atomic188:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC70:
	.long	0
	.long	1127219200
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
