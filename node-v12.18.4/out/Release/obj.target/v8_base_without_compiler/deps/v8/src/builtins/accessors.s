	.file	"accessors.cc"
	.text
	.section	.text._ZN2v88internal12_GLOBAL__N_117GetFrameArgumentsEPNS0_7IsolateEPNS0_23JavaScriptFrameIteratorEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117GetFrameArgumentsEPNS0_7IsolateEPNS0_23JavaScriptFrameIteratorEi, @function
_ZN2v88internal12_GLOBAL__N_117GetFrameArgumentsEPNS0_7IsolateEPNS0_23JavaScriptFrameIteratorEi:
.LFB19362:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	1416(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L87
	movq	32(%r12), %rax
	movq	%rdi, %rbx
	movq	(%rax), %rax
	cmpq	$38, -8(%rax)
	je	.L88
.L23:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*120(%rax)
	movq	%r12, %rdi
	movl	%eax, %r15d
	movq	(%r12), %rax
	call	*152(%rax)
	movq	41112(%rbx), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L24
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L25:
	movl	%r15d, %edx
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal7Factory18NewArgumentsObjectENS0_6HandleINS0_10JSFunctionEEEi@PLT
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	leal	-1(%r15), %eax
	movq	%rax, -280(%rbp)
	testl	%r15d, %r15d
	jg	.L35
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%rax, %r14
.L35:
	movq	(%r12), %rax
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	*112(%rax)
	movq	%rax, %r15
	cmpq	96(%rbx), %rax
	jne	.L30
	movq	88(%rbx), %r15
.L30:
	movq	0(%r13), %rdi
	leaq	15(%rdi,%r14,8), %rsi
	movq	%r15, (%rsi)
	testb	$1, %r15b
	je	.L41
	movq	%r15, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -288(%rbp)
	testl	$262144, %eax
	je	.L32
	movq	%r15, %rdx
	movq	%rsi, -304(%rbp)
	movq	%rdi, -296(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-288(%rbp), %r8
	movq	-304(%rbp), %rsi
	movq	-296(%rbp), %rdi
	movq	8(%r8), %rax
.L32:
	testb	$24, %al
	je	.L41
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L41
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	leaq	1(%r14), %rax
	cmpq	%r14, -280(%rbp)
	jne	.L89
.L34:
	movq	-312(%rbp), %rax
	movq	0(%r13), %r12
	movq	(%rax), %r14
	movq	%r12, 15(%r14)
	leaq	15(%r14), %r13
	testb	$1, %r12b
	je	.L40
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L90
.L36:
	testb	$24, %al
	je	.L40
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L91
	.p2align 4,,10
	.p2align 3
.L40:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	-312(%rbp), %rax
	jne	.L92
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L93
.L26:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r13, (%rsi)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	-208(%rbp), %rax
	movq	%r12, %rsi
	movl	%edx, %r13d
	movq	16(%r12), %r14
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	%rax, -320(%rbp)
	leaq	-256(%rbp), %r15
	call	_ZN2v88internal15TranslatedStateC1EPKNS0_15JavaScriptFrameE@PLT
	movq	32(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal15TranslatedState7PrepareEm@PLT
	movl	%r13d, %esi
	leaq	-260(%rbp), %rdx
	movq	%rbx, %rdi
	movl	$0, -260(%rbp)
	call	_ZN2v88internal15TranslatedState32GetArgumentsInfoFromJSFrameIndexEiPi@PLT
	movl	$0, -224(%rbp)
	movdqu	72(%rax), %xmm0
	movdqu	56(%rax), %xmm1
	movq	56(%rax), %rdi
	movaps	%xmm1, -256(%rbp)
	movaps	%xmm0, -240(%rbp)
	call	_ZNK2v88internal15TranslatedValue20IsMaterializedObjectEv@PLT
	movq	-256(%rbp), %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal15TranslatedValue8GetValueEv@PLT
	movq	%r15, %rdi
	addl	$1, -224(%rbp)
	movq	%rax, %rbx
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E@PLT
	movq	%r15, %rdi
	addl	$1, -224(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E@PLT
	movl	-260(%rbp), %eax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	leal	-1(%rax), %edx
	movl	%edx, -260(%rbp)
	call	_ZN2v88internal7Factory18NewArgumentsObjectENS0_6HandleINS0_10JSFunctionEEEi@PLT
	movl	-260(%rbp), %esi
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	movl	-260(%rbp), %eax
	testl	%eax, %eax
	jle	.L3
	xorl	%ebx, %ebx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L4:
	call	_ZN2v88internal15TranslatedValue8GetValueEv@PLT
	movq	(%r14), %rdi
	movq	(%rax), %rdx
	leaq	15(%rdi,%rbx,8), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L39
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -280(%rbp)
	testl	$262144, %eax
	je	.L6
	movq	%rdx, -304(%rbp)
	movq	%rsi, -296(%rbp)
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-280(%rbp), %rcx
	movq	-304(%rbp), %rdx
	movq	-296(%rbp), %rsi
	movq	-288(%rbp), %rdi
	movq	8(%rcx), %rax
.L6:
	testb	$24, %al
	je	.L39
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L39
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%r15, %rdi
	addl	$1, -224(%rbp)
	addq	$1, %rbx
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E@PLT
	cmpl	%ebx, -260(%rbp)
	jle	.L3
.L8:
	movq	-256(%rbp), %rdi
	testb	%r13b, %r13b
	jne	.L4
	call	_ZNK2v88internal15TranslatedValue20IsMaterializedObjectEv@PLT
	movq	-256(%rbp), %rdi
	movl	%eax, %r13d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L3:
	movq	-312(%rbp), %rax
	movq	(%r14), %r14
	movq	(%rax), %r15
	movq	%r14, 15(%r15)
	leaq	15(%r15), %rsi
	testb	$1, %r14b
	je	.L38
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L94
	testb	$24, %al
	je	.L38
.L96:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L38
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	testb	%r13b, %r13b
	jne	.L95
.L12:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L13
	movq	-88(%rbp), %rax
	movq	-120(%rbp), %rbx
	leaq	8(%rax), %r12
	cmpq	%rbx, %r12
	jbe	.L14
	.p2align 4,,10
	.p2align 3
.L15:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	ja	.L15
	movq	-160(%rbp), %rdi
.L14:
	call	_ZdlPv@PLT
.L13:
	movq	-200(%rbp), %r14
	movq	-208(%rbp), %r12
	cmpq	%r12, %r14
	je	.L16
	.p2align 4,,10
	.p2align 3
.L20:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L17
	movq	112(%r12), %rax
	movq	80(%r12), %rbx
	leaq	8(%rax), %r13
	cmpq	%rbx, %r13
	jbe	.L18
	.p2align 4,,10
	.p2align 3
.L19:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r13
	ja	.L19
	movq	40(%r12), %rdi
.L18:
	call	_ZdlPv@PLT
.L17:
	addq	$120, %r12
	cmpq	%r12, %r14
	jne	.L20
	movq	-208(%rbp), %r12
.L16:
	testq	%r12, %r12
	je	.L40
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L94:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-280(%rbp), %rsi
	testb	$24, %al
	jne	.L96
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%rsi, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-280(%rbp), %rsi
	movq	1416(%rsi), %r12
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L95:
	movq	-320(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal15TranslatedState31StoreMaterializedValuesAndDeoptEPNS0_15JavaScriptFrameE@PLT
	jmp	.L12
.L93:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L26
.L92:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19362:
	.size	_ZN2v88internal12_GLOBAL__N_117GetFrameArgumentsEPNS0_7IsolateEPNS0_23JavaScriptFrameIteratorEi, .-_ZN2v88internal12_GLOBAL__N_117GetFrameArgumentsEPNS0_7IsolateEPNS0_23JavaScriptFrameIteratorEi
	.section	.text._ZN2v88internal9Accessors18FunctionNameGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors18FunctionNameGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal9Accessors18FunctionNameGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal9Accessors18FunctionNameGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE:
.LFB19346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rsi, %rbx
	movq	16(%rax), %r12
	addl	$1, 41104(%r12)
	movq	(%rsi), %rax
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %r14
	leaq	8(%rax), %rsi
	call	_ZN2v88internal10JSFunction7GetNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rbx), %rdx
	testq	%rax, %rax
	je	.L103
	movq	(%rax), %rax
.L99:
	movq	%rax, 32(%rdx)
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r14
	je	.L104
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	24(%rdx), %rax
	jmp	.L99
	.cfi_endproc
.LFE19346:
	.size	_ZN2v88internal9Accessors18FunctionNameGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, .-_ZN2v88internal9Accessors18FunctionNameGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.section	.text._ZN2v88internal9Accessors23FunctionPrototypeSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors23FunctionPrototypeSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE
	.type	_ZN2v88internal9Accessors23FunctionPrototypeSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE, @function
_ZN2v88internal9Accessors23FunctionPrototypeSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE:
.LFB19342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movaps	%xmm0, -96(%rbp)
	movq	16(%rax), %r12
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L117
.L106:
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	movq	%r13, %rsi
	movq	41088(%r12), %r15
	movq	41096(%r12), %r14
	leaq	8(%rax), %rdi
	call	_ZN2v88internal10JSFunction12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEE@PLT
	movq	(%rbx), %rax
	movq	16(%rax), %rdx
	movq	112(%rdx), %rdx
	movq	%rdx, 32(%rax)
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r14
	je	.L107
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L107:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L118
.L105:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L119
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	movq	40960(%r12), %rax
	leaq	-88(%rbp), %rsi
	movl	$146, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L118:
	leaq	-88(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L105
.L119:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19342:
	.size	_ZN2v88internal9Accessors23FunctionPrototypeSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE, .-_ZN2v88internal9Accessors23FunctionPrototypeSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE
	.section	.text._ZN2v88internal9Accessors26ModuleNamespaceEntryGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors26ModuleNamespaceEntryGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal9Accessors26ModuleNamespaceEntryGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal9Accessors26ModuleNamespaceEntryGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE:
.LFB19335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	leaq	-48(%rbp), %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	16(%rax), %r12
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rsi), %rax
	movq	%r12, %rsi
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal17JSModuleNamespace9GetExportEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	je	.L127
	movq	(%rbx), %rdx
	movq	(%rax), %rax
	movq	%rax, 32(%rdx)
.L122:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L120
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L120:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L128
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb@PLT
	jmp	.L122
.L128:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19335:
	.size	_ZN2v88internal9Accessors26ModuleNamespaceEntryGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, .-_ZN2v88internal9Accessors26ModuleNamespaceEntryGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.section	.text._ZN2v88internal9Accessors23FunctionPrototypeGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors23FunctionPrototypeGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal9Accessors23FunctionPrototypeGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal9Accessors23FunctionPrototypeGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE:
.LFB19341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movaps	%xmm0, -96(%rbp)
	movq	16(%rax), %r12
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L160
.L130:
	movq	41088(%r12), %r13
	movq	41096(%r12), %r15
	addl	$1, 41104(%r12)
	movq	(%rbx), %r14
	movq	8(%r14), %rax
	leaq	8(%r14), %r8
	movq	-1(%rax), %rdx
	movzbl	13(%rdx), %edx
	andl	$1, %edx
	je	.L161
.L133:
	movq	-1(%rax), %rdx
	movzbl	13(%rdx), %edx
	andl	$1, %edx
	je	.L134
	movq	-1(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L136
.L137:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L140
.L165:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rbx), %rdx
	testq	%rax, %rax
	je	.L162
	movq	(%rax), %rsi
.L144:
	movq	%rsi, 32(%rdx)
.L145:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L148
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L148:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L163
.L129:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L164
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L137
	movq	41112(%r12), %rdi
	movq	23(%rsi), %rsi
	testq	%rdi, %rdi
	jne	.L165
.L140:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L166
.L143:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	movq	(%rbx), %rdx
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L161:
	movq	55(%rax), %rdx
	movq	-1(%rdx), %rcx
	cmpw	$68, 11(%rcx)
	je	.L133
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	-37496(%rcx), %rdx
	jne	.L133
	movq	%r8, %rsi
	movq	%r12, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal7Factory20NewFunctionPrototypeENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	-104(%rbp), %r8
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal10JSFunction12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEE@PLT
	movq	8(%r14), %rax
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L136:
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L137
	movq	31(%rsi), %rsi
	testb	$1, %sil
	je	.L137
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L162:
	movq	24(%rdx), %rax
	movq	%rax, 32(%rdx)
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L160:
	movq	40960(%r12), %rax
	leaq	-88(%rbp), %rsi
	movl	$145, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L163:
	leaq	-88(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L166:
	movq	%r12, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	jmp	.L143
.L164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19341:
	.size	_ZN2v88internal9Accessors23FunctionPrototypeGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, .-_ZN2v88internal9Accessors23FunctionPrototypeGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.section	.text._ZN2v88internal9Accessors23ArgumentsIteratorGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors23ArgumentsIteratorGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal9Accessors23ArgumentsIteratorGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal9Accessors23ArgumentsIteratorGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE:
.LFB19330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	(%rsi), %rax
	movq	16(%rax), %r12
	movq	12464(%r12), %rax
	movq	41088(%r12), %r14
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r13
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L168
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L169:
	movq	1599(%rsi), %rsi
	movq	41112(%r12), %rdi
	movq	(%rbx), %rbx
	testq	%rdi, %rdi
	je	.L171
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	testq	%rax, %rax
	je	.L181
	movq	(%rax), %rsi
.L176:
	movq	%rsi, 32(%rbx)
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L182
	movq	%r13, 41096(%r12)
	addq	$16, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L183
.L174:
	movq	%rsi, (%rax)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%r14, %rax
	cmpq	41096(%r12), %r14
	je	.L184
.L170:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L182:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movq	24(%rbx), %rsi
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L184:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L183:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L174
	.cfi_endproc
.LFE19330:
	.size	_ZN2v88internal9Accessors23ArgumentsIteratorGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, .-_ZN2v88internal9Accessors23ArgumentsIteratorGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.section	.text._ZN2v88internal9Accessors17ArrayLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors17ArrayLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal9Accessors17ArrayLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal9Accessors17ArrayLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE:
.LFB19332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movaps	%xmm0, -80(%rbp)
	movq	16(%rax), %r12
	movq	$0, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L206
.L186:
	movq	41112(%r12), %rdi
	movq	41088(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rbx
	movq	41096(%r12), %r14
	movq	8(%rbx), %rax
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L187
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	testq	%rax, %rax
	je	.L207
	movq	(%rax), %rsi
.L192:
	movq	%rsi, 32(%rbx)
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r14
	je	.L195
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L195:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L208
.L185:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L209
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	%r13, %rax
	cmpq	%r14, %r13
	je	.L210
.L190:
	movq	%rsi, (%rax)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L207:
	movq	24(%rbx), %rsi
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L208:
	leaq	-72(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L206:
	movq	40960(%r12), %rax
	leaq	-72(%rbp), %rsi
	movl	$111, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L210:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L190
.L209:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19332:
	.size	_ZN2v88internal9Accessors17ArrayLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, .-_ZN2v88internal9Accessors17ArrayLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.section	.text._ZN2v88internal9Accessors23FunctionArgumentsGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors23FunctionArgumentsGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal9Accessors23FunctionArgumentsGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal9Accessors23FunctionArgumentsGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE:
.LFB19367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1544, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -1576(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	16(%rax), %r13
	movq	41088(%r13), %rax
	addl	$1, 41104(%r13)
	movq	(%rsi), %rbx
	movq	%rax, -1568(%rbp)
	movq	41096(%r13), %rax
	movq	%rax, -1552(%rbp)
	leaq	104(%r13), %rax
	movq	%rax, -1560(%rbp)
	movq	8(%rbx), %rax
	movq	23(%rax), %rax
	movl	47(%rax), %edx
	movq	%rbx, %rax
	andl	$32, %edx
	je	.L238
.L221:
	movq	-1560(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, 32(%rax)
.L223:
	movq	-1568(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-1552(%rbp), %rax
	cmpq	41096(%r13), %rax
	je	.L211
	movq	%rax, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L211:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L239
	addq	$1544, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_restore_state
	leaq	-1504(%rbp), %r14
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -88(%rbp)
	je	.L213
	movq	%r14, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L213
	movq	%rbx, -1544(%rbp)
	leaq	-1536(%rbp), %r15
	movq	%r13, -1584(%rbp)
	.p2align 4,,10
	.p2align 3
.L222:
	pxor	%xmm0, %xmm0
	movq	%r15, %rsi
	movq	$0, -1520(%rbp)
	movaps	%xmm0, -1536(%rbp)
	movq	(%rdi), %rax
	call	*136(%rax)
	movq	-1528(%rbp), %r13
	movq	-1536(%rbp), %rbx
	movabsq	$7905747460161236407, %rsi
	movq	%r13, %rax
	movq	%r13, %rcx
	subq	%rbx, %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L241:
	movq	-32(%rcx), %rdx
	movq	-1544(%rbp), %rdi
	leaq	-1(%rax), %rsi
	subq	$56, %rcx
	movq	(%rdx), %rdx
	cmpq	%rdx, 8(%rdi)
	je	.L240
	movq	%rsi, %rax
.L215:
	testq	%rax, %rax
	jne	.L241
	movl	$-1, %r12d
.L214:
	cmpq	%r13, %rbx
	je	.L216
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%rbx, %rdi
	addq	$56, %rbx
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	cmpq	%rbx, %r13
	jne	.L217
	movq	-1536(%rbp), %r13
.L216:
	testq	%r13, %r13
	je	.L218
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L218:
	testl	%r12d, %r12d
	jns	.L242
	movq	%r14, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L222
	movq	-1584(%rbp), %r13
.L213:
	movq	-1576(%rbp), %rax
	movq	(%rax), %rax
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L240:
	leal	-1(%rax), %r12d
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L242:
	movq	-1584(%rbp), %r13
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_117GetFrameArgumentsEPNS0_7IsolateEPNS0_23JavaScriptFrameIteratorEi
	movq	%rax, -1560(%rbp)
	movq	%rax, %rdx
	movq	-1576(%rbp), %rax
	movq	(%rax), %rax
	testq	%rdx, %rdx
	jne	.L221
	movq	24(%rax), %rdx
	movq	%rdx, 32(%rax)
	jmp	.L223
.L239:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19367:
	.size	_ZN2v88internal9Accessors23FunctionArgumentsGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, .-_ZN2v88internal9Accessors23FunctionArgumentsGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.section	.text._ZN2v88internal9Accessors20FunctionLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors20FunctionLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal9Accessors20FunctionLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal9Accessors20FunctionLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE:
.LFB19344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movaps	%xmm0, -80(%rbp)
	movq	16(%rax), %r12
	movq	$0, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L263
.L244:
	movq	41088(%r12), %r13
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	movq	23(%rax), %rax
	movq	41112(%r12), %rdi
	movzwl	39(%rax), %esi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L245
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rbx), %rdx
	testq	%rax, %rax
	je	.L264
	movq	(%rax), %rsi
.L249:
	movq	%rsi, 32(%rdx)
.L250:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L253
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L253:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L265
.L243:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L266
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	movq	%r13, %rax
	cmpq	41096(%r12), %r13
	je	.L267
.L248:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	movq	(%rbx), %rdx
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L264:
	movq	24(%rdx), %rax
	movq	%rax, 32(%rdx)
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L265:
	leaq	-72(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L263:
	movq	40960(%r12), %rax
	leaq	-72(%rbp), %rsi
	movl	$144, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L267:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L248
.L266:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19344:
	.size	_ZN2v88internal9Accessors20FunctionLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, .-_ZN2v88internal9Accessors20FunctionLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.section	.text._ZN2v88internal9Accessors26ModuleNamespaceEntrySetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors26ModuleNamespaceEntrySetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE
	.type	_ZN2v88internal9Accessors26ModuleNamespaceEntrySetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE, @function
_ZN2v88internal9Accessors26ModuleNamespaceEntrySetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE:
.LFB19336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	(%rdx), %rax
	movq	16(%rax), %r12
	movq	41088(%r12), %r9
	movq	41096(%r12), %r15
	addl	$1, 41104(%r12)
	movq	(%rdx), %r13
	movabsq	$8589934592, %rdx
	movq	0(%r13), %rax
	cmpq	%rdx, %rax
	je	.L269
	movabsq	$4294967296, %rdx
	cmpq	%rdx, %rax
	setne	%al
.L270:
	testb	%al, %al
	jne	.L276
	movq	(%rbx), %rax
	movq	16(%rax), %rdx
	movq	120(%rdx), %rdx
	movq	%rdx, 32(%rax)
.L272:
	subl	$1, 41104(%r12)
	movq	%r9, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L277
	movq	%r15, 41096(%r12)
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	addq	$8, %r13
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	movq	%r13, %rsi
	call	_ZN2v88internal6Object6TypeOfEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%r13, %r8
	movq	%r14, %rdx
	movl	$165, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb@PLT
	movq	-56(%rbp), %r9
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L269:
	movq	16(%r13), %rdi
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal18ShouldThrowOnErrorEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r9
	jmp	.L270
	.cfi_endproc
.LFE19336:
	.size	_ZN2v88internal9Accessors26ModuleNamespaceEntrySetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE, .-_ZN2v88internal9Accessors26ModuleNamespaceEntrySetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE
	.section	.text._ZN2v88internal9Accessors18StringLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors18StringLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal9Accessors18StringLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal9Accessors18StringLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE:
.LFB19338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movaps	%xmm0, -80(%rbp)
	movq	16(%rax), %r12
	movq	$0, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L301
.L279:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rbx
	movq	48(%rbx), %rax
	testb	$1, %al
	jne	.L302
.L280:
	movq	8(%rbx), %rax
	movq	23(%rax), %rax
.L282:
	movslq	11(%rax), %rsi
	movq	41112(%r12), %rdi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L283
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	testq	%rax, %rax
	je	.L303
	movq	(%rax), %rsi
.L288:
	movq	%rsi, 32(%rbx)
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L291
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L291:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L304
.L278:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L305
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L306
.L286:
	movq	%rsi, (%rax)
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L302:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L282
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L303:
	movq	24(%rbx), %rsi
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L304:
	leaq	-72(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L301:
	movq	40960(%r12), %rax
	leaq	-72(%rbp), %rsi
	movl	$198, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L306:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L286
.L305:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19338:
	.size	_ZN2v88internal9Accessors18StringLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, .-_ZN2v88internal9Accessors18StringLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.section	.text._ZN2v88internal9Accessors16ErrorStackGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors16ErrorStackGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal9Accessors16ErrorStackGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal9Accessors16ErrorStackGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE:
.LFB19392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	16(%rax), %r12
	movq	41088(%r12), %rax
	movq	3808(%r12), %rdx
	leaq	3808(%r12), %r14
	addl	$1, 41104(%r12)
	movq	41096(%r12), %rbx
	movq	%rax, -160(%rbp)
	movq	(%rsi), %rax
	movq	-1(%rdx), %rcx
	leaq	8(%rax), %r13
	movl	$3, %eax
	cmpw	$64, 11(%rcx)
	jne	.L308
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L308:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	3808(%r12), %rax
	movq	%r12, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r14, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L337
.L309:
	leaq	-144(%rbp), %rdi
	movq	%rax, -112(%rbp)
	movq	%rdi, -152(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	movq	-152(%rbp), %rdi
	jne	.L310
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rdx
.L311:
	movq	(%rdx), %rax
	cmpq	%rax, 88(%r12)
	je	.L312
	testb	$1, %al
	jne	.L338
.L317:
	movq	(%r15), %rdx
	movq	%rax, 32(%rdx)
.L315:
	subl	$1, 41104(%r12)
	movq	-160(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L307
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L307:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L339
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	subl	$123, %ecx
	cmpw	$14, %cx
	ja	.L317
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10ErrorUtils16FormatStackTraceEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L336
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movq	-152(%rbp), %rcx
	testq	%rax, %rax
	je	.L336
	movq	(%r15), %rax
	movq	(%rcx), %rdx
	movq	%rdx, 32(%rax)
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L310:
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L311
.L312:
	movq	(%r15), %rax
	movq	88(%r12), %rdx
	movq	%rdx, 32(%rax)
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L337:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L336:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb@PLT
	jmp	.L315
.L339:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19392:
	.size	_ZN2v88internal9Accessors16ErrorStackGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, .-_ZN2v88internal9Accessors16ErrorStackGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.section	.text._ZN2v88internal9Accessors16ErrorStackSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors16ErrorStackSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE
	.type	_ZN2v88internal9Accessors16ErrorStackSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE, @function
_ZN2v88internal9Accessors16ErrorStackSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE:
.LFB19393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdx), %rax
	movq	16(%rax), %r12
	addl	$1, 41104(%r12)
	movq	(%rdx), %rsi
	movq	%r12, %rdi
	leaq	3808(%r12), %rdx
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addq	$48, %rsi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L347
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L340
.L346:
	movq	%rbx, 41096(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	jne	.L346
.L340:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19393:
	.size	_ZN2v88internal9Accessors16ErrorStackSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE, .-_ZN2v88internal9Accessors16ErrorStackSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE
	.section	.text._ZN2v88internal9Accessors23BoundFunctionNameGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors23BoundFunctionNameGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal9Accessors23BoundFunctionNameGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal9Accessors23BoundFunctionNameGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE:
.LFB19390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movaps	%xmm0, -80(%rbp)
	movq	16(%rax), %r12
	movq	$0, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L364
.L349:
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %r14
	leaq	8(%rax), %rsi
	call	_ZN2v88internal15JSBoundFunction7GetNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L365
	movq	(%rbx), %rdx
	movq	(%rax), %rax
	movq	%rax, 32(%rdx)
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r14
	je	.L354
.L363:
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L354:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L366
.L348:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L367
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r14
	jne	.L363
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L366:
	leaq	-72(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L364:
	movq	40960(%r12), %rax
	leaq	-72(%rbp), %rsi
	movl	$114, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L349
.L367:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19390:
	.size	_ZN2v88internal9Accessors23BoundFunctionNameGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, .-_ZN2v88internal9Accessors23BoundFunctionNameGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.section	.text._ZN2v88internal9Accessors25BoundFunctionLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors25BoundFunctionLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal9Accessors25BoundFunctionLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal9Accessors25BoundFunctionLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE:
.LFB19388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movaps	%xmm0, -80(%rbp)
	movq	16(%rax), %r12
	movq	$0, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L398
.L369:
	addl	$1, 41104(%r12)
	movq	(%r14), %rax
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	leaq	8(%rax), %rsi
	call	_ZN2v88internal15JSBoundFunction9GetLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testb	%al, %al
	je	.L370
	sarq	$32, %rax
	movq	41112(%r12), %rdi
	salq	$32, %rax
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L399
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%r14), %rdx
	testq	%rax, %rax
	je	.L400
	movq	(%rax), %rsi
.L378:
	movq	%rsi, 32(%rdx)
.L397:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L382
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L382:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L401
.L368:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L402
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L399:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L403
.L377:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	movq	(%r14), %rdx
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L370:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb@PLT
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L401:
	leaq	-72(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L398:
	movq	40960(%r12), %rax
	leaq	-72(%rbp), %rsi
	movl	$113, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L400:
	movq	24(%rdx), %rax
	movq	%rax, 32(%rdx)
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L403:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L377
.L402:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19388:
	.size	_ZN2v88internal9Accessors25BoundFunctionLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, .-_ZN2v88internal9Accessors25BoundFunctionLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.section	.rodata._ZN2v88internal9Accessors17ArrayLengthSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"array->length().ToArrayLength(&actual_new_len)"
	.section	.rodata._ZN2v88internal9Accessors17ArrayLengthSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal9Accessors17ArrayLengthSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors17ArrayLengthSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE
	.type	_ZN2v88internal9Accessors17ArrayLengthSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE, @function
_ZN2v88internal9Accessors17ArrayLengthSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE:
.LFB19333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%rdi, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movaps	%xmm0, -96(%rbp)
	movq	16(%rax), %r12
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L455
.L405:
	addl	$1, 41104(%r12)
	movq	41088(%r12), %rax
	movq	(%r15), %r13
	movq	%rax, -136(%rbp)
	movq	41096(%r12), %rax
	leaq	8(%r13), %r14
	movq	%r14, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal7JSArray17HasReadOnlyLengthENS0_6HandleIS1_EE@PLT
	leaq	-100(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movb	%al, -121(%rbp)
	movl	$0, -100(%rbp)
	call	_ZN2v88internal7JSArray21AnythingToArrayLengthEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPj@PLT
	testb	%al, %al
	je	.L451
	cmpb	$0, -121(%rbp)
	je	.L456
.L408:
	movl	-100(%rbp), %esi
.L410:
	movq	%r14, %rdi
	call	_ZN2v88internal7JSArray9SetLengthENS0_6HandleIS1_EEj@PLT
	movq	8(%r13), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L418
	sarq	$32, %rax
	movl	%eax, %ebx
	js	.L420
.L421:
	movq	(%r15), %rax
	cmpl	%ebx, -100(%rbp)
	je	.L429
	movabsq	$8589934592, %rsi
	movq	(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L430
	movabsq	$4294967296, %rax
	cmpq	%rax, %rdx
	setne	%al
.L431:
	testb	%al, %al
	jne	.L457
.L432:
	movq	(%r15), %rax
	movq	16(%rax), %rdx
	movq	120(%rdx), %rdx
	movq	%rdx, 32(%rax)
.L433:
	subl	$1, 41104(%r12)
	movq	-136(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-120(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L438
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L438:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L458
.L404:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L459
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v88internal7JSArray17HasReadOnlyLengthENS0_6HandleIS1_EE@PLT
	testb	%al, %al
	je	.L408
	movl	-100(%rbp), %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	movq	%rax, %rsi
	movq	8(%r13), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L413
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L414:
	ucomisd	%xmm0, %xmm1
	jp	.L442
	je	.L410
.L442:
	movabsq	$4294967296, %rdx
	movq	(%r15), %rsi
	movabsq	$8589934592, %rdi
	movq	(%rsi), %rax
	cmpq	%rdx, %rax
	setne	%dl
	cmpq	%rdi, %rax
	je	.L460
.L427:
	testb	%dl, %dl
	je	.L432
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object6TypeOfEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-144(%rbp), %rdx
	movq	%r14, %r8
	movl	$165, %esi
	movq	%rax, %rcx
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L429:
	movq	16(%rax), %rdx
	movq	112(%rdx), %rdx
	movq	%rdx, 32(%rax)
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L457:
	leal	-1(%rbx), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$163, %esi
	movq	%rax, %rdx
.L454:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L451:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb@PLT
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L418:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L461
.L420:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L461:
	movsd	7(%rax), %xmm1
	movsd	.LC0(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L420
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %ebx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L420
	je	.L421
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L455:
	movq	40960(%r12), %rax
	leaq	-88(%rbp), %rsi
	movl	$112, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L430:
	movq	16(%rax), %rdi
	call	_ZN2v88internal18ShouldThrowOnErrorEPNS0_7IsolateE@PLT
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L458:
	leaq	-88(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L413:
	movsd	7(%rax), %xmm0
	jmp	.L414
.L460:
	movq	16(%rsi), %rdi
	call	_ZN2v88internal18ShouldThrowOnErrorEPNS0_7IsolateE@PLT
	movl	%eax, %edx
	jmp	.L427
.L459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19333:
	.size	_ZN2v88internal9Accessors17ArrayLengthSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE, .-_ZN2v88internal9Accessors17ArrayLengthSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE
	.section	.text._ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE
	.type	_ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE, @function
_ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE:
.LFB19325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Factory15NewAccessorInfoEv@PLT
	movq	(%rax), %rdx
	movq	%rax, %r12
	movslq	19(%rdx), %rax
	andl	$-2, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-3, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	(%r12), %rdx
	movslq	19(%rdx), %rax
	orl	$4, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-9, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	(%r12), %rax
	movslq	19(%rax), %rdx
	andl	$-17, %edx
	salq	$32, %rdx
	movq	%rdx, 15(%rax)
	movq	(%r12), %rax
	movslq	19(%rax), %rdx
	andl	$-97, %edx
	salq	$32, %rdx
	movq	%rdx, 15(%rax)
	movq	(%r12), %rax
	movslq	19(%rax), %rdx
	andl	$-385, %edx
	salq	$32, %rdx
	movq	%rdx, 15(%rax)
	movq	(%rbx), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L518
.L463:
	movq	(%r12), %rdi
	movq	(%rbx), %rbx
	movq	%rbx, 7(%rdi)
	leaq	7(%rdi), %rsi
	testb	$1, %bl
	je	.L488
	movq	%rbx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L519
	testb	$24, %al
	je	.L488
.L526:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L520
.L488:
	testq	%r15, %r15
	je	.L521
.L467:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
.L471:
	testq	%r14, %r14
	leaq	_ZN2v88internal9Accessors25ReconfigureToDataPropertyENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE(%rip), %rax
	movq	%r13, %rdi
	cmove	%rax, %r14
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	(%r12), %rdi
	movq	(%rbx), %r14
	movq	%rax, %r15
	movq	%r14, 39(%rdi)
	leaq	39(%rdi), %rsi
	testb	$1, %r14b
	je	.L485
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L474
	movq	%r14, %rdx
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
.L474:
	testb	$24, %al
	je	.L485
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L522
	.p2align 4,,10
	.p2align 3
.L485:
	movq	(%r12), %rbx
	movq	(%r15), %r14
	movq	%r14, 31(%rbx)
	leaq	31(%rbx), %rsi
	testb	$1, %r14b
	je	.L484
	movq	%r14, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	je	.L477
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-72(%rbp), %rsi
.L477:
	testb	$24, %al
	je	.L484
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L523
	.p2align 4,,10
	.p2align 3
.L484:
	movq	(%r12), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal12AccessorInfo17redirected_getterEv@PLT
	testq	%rax, %rax
	jne	.L524
.L479:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L525
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L521:
	.cfi_restore_state
	movq	41112(%r13), %rdi
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rsi
	testq	%rdi, %rdi
	je	.L468
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L519:
	movq	%rbx, %rdx
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L526
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L524:
	movq	%r13, %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	(%r12), %r14
	movq	(%rax), %r13
	leaq	47(%r14), %r15
	movq	%r13, 47(%r14)
	testb	$1, %r13b
	je	.L479
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L481
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L481:
	testb	$24, %al
	je	.L479
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L479
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L523:
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L520:
	movq	%rbx, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	testq	%r15, %r15
	jne	.L467
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L522:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L468:
	movq	41088(%r13), %rbx
	cmpq	41096(%r13), %rbx
	je	.L527
.L470:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rbx)
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L518:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rbx
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L527:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L470
.L525:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19325:
	.size	_ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE, .-_ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE
	.section	.text._ZN2v88internal9Accessors23IsJSObjectFieldAccessorEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_4NameEEEPNS0_10FieldIndexE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors23IsJSObjectFieldAccessorEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_4NameEEEPNS0_10FieldIndexE
	.type	_ZN2v88internal9Accessors23IsJSObjectFieldAccessorEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_4NameEEEPNS0_10FieldIndexE, @function
_ZN2v88internal9Accessors23IsJSObjectFieldAccessorEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_4NameEEEPNS0_10FieldIndexE:
.LFB19327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rsi), %rax
	movzwl	11(%rax), %edx
	cmpw	$1061, %dx
	je	.L556
	xorl	%eax, %eax
	cmpw	$63, %dx
	jbe	.L557
.L528:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L557:
	.cfi_restore_state
	leaq	2768(%rdi), %rdx
	cmpq	%r8, %rdx
	je	.L544
	movq	(%r8), %rax
	cmpq	2768(%rdi), %rax
	je	.L544
	movq	-1(%rax), %rsi
	movzwl	11(%rsi), %esi
	andl	$65504, %esi
	jne	.L543
	movq	2768(%rdi), %rsi
	movq	-1(%rsi), %rsi
	movzwl	11(%rsi), %esi
	andl	$65504, %esi
	je	.L541
.L543:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	je	.L541
	movq	2768(%rdi), %rax
	movq	%rcx, -8(%rbp)
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	je	.L541
	movq	%r8, %rsi
	call	_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	movq	-8(%rbp), %rcx
	testb	%al, %al
	je	.L528
.L544:
	movq	$81932, (%rcx)
	movl	$1, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L556:
	.cfi_restore_state
	leaq	2768(%rdi), %rdx
	cmpq	%r8, %rdx
	je	.L531
	movq	(%r8), %rax
	cmpq	%rax, 2768(%rdi)
	je	.L531
	movq	-1(%rax), %rsi
	movzwl	11(%rsi), %esi
	andl	$65504, %esi
	jne	.L536
	movq	2768(%rdi), %rsi
	movq	-1(%rsi), %rsi
	movzwl	11(%rsi), %esi
	andl	$65504, %esi
	je	.L541
.L536:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	je	.L541
	movq	2768(%rdi), %rax
	movq	%rcx, -8(%rbp)
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	je	.L541
	movq	%r8, %rsi
	call	_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	movq	-8(%rbp), %rcx
	testb	%al, %al
	je	.L528
.L531:
	movq	$16408, (%rcx)
	movl	$1, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L541:
	.cfi_restore_state
	leave
	.cfi_def_cfa 7, 8
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE19327:
	.size	_ZN2v88internal9Accessors23IsJSObjectFieldAccessorEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_4NameEEEPNS0_10FieldIndexE, .-_ZN2v88internal9Accessors23IsJSObjectFieldAccessorEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_4NameEEEPNS0_10FieldIndexE
	.section	.rodata._ZN2v88internal9Accessors31ReplaceAccessorWithDataPropertyENS0_6HandleINS0_6ObjectEEENS2_INS0_8JSObjectEEENS2_INS0_4NameEEES4_.str1.1,"aMS",@progbits,1
.LC3:
	.string	"it.HasAccess()"
	.section	.rodata._ZN2v88internal9Accessors31ReplaceAccessorWithDataPropertyENS0_6HandleINS0_6ObjectEEENS2_INS0_8JSObjectEEENS2_INS0_4NameEEES4_.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"LookupIterator::ACCESSOR == it.state()"
	.section	.text._ZN2v88internal9Accessors31ReplaceAccessorWithDataPropertyENS0_6HandleINS0_6ObjectEEENS2_INS0_8JSObjectEEENS2_INS0_4NameEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors31ReplaceAccessorWithDataPropertyENS0_6HandleINS0_6ObjectEEENS2_INS0_8JSObjectEEENS2_INS0_4NameEEES4_
	.type	_ZN2v88internal9Accessors31ReplaceAccessorWithDataPropertyENS0_6HandleINS0_6ObjectEEENS2_INS0_8JSObjectEEENS2_INS0_4NameEEES4_, @function
_ZN2v88internal9Accessors31ReplaceAccessorWithDataPropertyENS0_6HandleINS0_6ObjectEEENS2_INS0_8JSObjectEEENS2_INS0_4NameEEES4_:
.LFB19328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rcx, %r12
	movabsq	$824633720832, %rcx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	(%rdx), %rax
	subq	$37592, %rdi
	movq	-1(%rax), %rdx
	movl	$0, -128(%rbp)
	movq	%rcx, -116(%rbp)
	movq	%rdi, -104(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L565
.L559:
	movq	%r13, -80(%rbp)
	leaq	-128(%rbp), %r13
	movq	%r13, %rdi
	movq	%rsi, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -72(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movl	-124(%rbp), %eax
	testl	%eax, %eax
	je	.L566
.L560:
	cmpl	$5, %eax
	jne	.L567
	movl	-112(%rbp), %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	shrl	$3, %edx
	andl	$7, %edx
	call	_ZN2v88internal14LookupIterator23ReconfigureDataPropertyENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L568
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L566:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNK2v88internal14LookupIterator9HasAccessEv@PLT
	testb	%al, %al
	je	.L569
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	-124(%rbp), %eax
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L565:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L567:
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L569:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L568:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19328:
	.size	_ZN2v88internal9Accessors31ReplaceAccessorWithDataPropertyENS0_6HandleINS0_6ObjectEEENS2_INS0_8JSObjectEEENS2_INS0_4NameEEES4_, .-_ZN2v88internal9Accessors31ReplaceAccessorWithDataPropertyENS0_6HandleINS0_6ObjectEEENS2_INS0_8JSObjectEEENS2_INS0_4NameEEES4_
	.section	.text._ZN2v88internal9Accessors25ReconfigureToDataPropertyENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors25ReconfigureToDataPropertyENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE
	.type	_ZN2v88internal9Accessors25ReconfigureToDataPropertyENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE, @function
_ZN2v88internal9Accessors25ReconfigureToDataPropertyENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE:
.LFB19329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movaps	%xmm0, -96(%rbp)
	movq	16(%rax), %r12
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L585
.L571:
	addl	$1, 41104(%r12)
	movq	(%rbx), %rsi
	movq	%r15, %rdx
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	leaq	48(%rsi), %rdi
	addq	$8, %rsi
	call	_ZN2v88internal9Accessors31ReplaceAccessorWithDataPropertyENS0_6HandleINS0_6ObjectEEENS2_INS0_8JSObjectEEENS2_INS0_4NameEEES4_
	testq	%rax, %rax
	je	.L586
	movq	(%rbx), %rax
	movq	16(%rax), %rdx
	movq	112(%rdx), %rdx
	movq	%rdx, 32(%rax)
.L573:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L576
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L576:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L587
.L570:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L588
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L586:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb@PLT
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L587:
	leaq	-88(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L585:
	movq	40960(%r12), %rax
	movq	%rsi, -104(%rbp)
	movl	$197, %edx
	leaq	-88(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	-104(%rbp), %rcx
	jmp	.L571
.L588:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19329:
	.size	_ZN2v88internal9Accessors25ReconfigureToDataPropertyENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE, .-_ZN2v88internal9Accessors25ReconfigureToDataPropertyENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE
	.section	.text._ZN2v88internal9Accessors25MakeArgumentsIteratorInfoEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors25MakeArgumentsIteratorInfoEPNS0_7IsolateE
	.type	_ZN2v88internal9Accessors25MakeArgumentsIteratorInfoEPNS0_7IsolateE, @function
_ZN2v88internal9Accessors25MakeArgumentsIteratorInfoEPNS0_7IsolateE:
.LFB19331:
	.cfi_startproc
	endbr64
	leaq	3856(%rdi), %rsi
	xorl	%ecx, %ecx
	leaq	_ZN2v88internal9Accessors23ArgumentsIteratorGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE(%rip), %rdx
	jmp	_ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE
	.cfi_endproc
.LFE19331:
	.size	_ZN2v88internal9Accessors25MakeArgumentsIteratorInfoEPNS0_7IsolateE, .-_ZN2v88internal9Accessors25MakeArgumentsIteratorInfoEPNS0_7IsolateE
	.section	.text._ZN2v88internal9Accessors19MakeArrayLengthInfoEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors19MakeArrayLengthInfoEPNS0_7IsolateE
	.type	_ZN2v88internal9Accessors19MakeArrayLengthInfoEPNS0_7IsolateE, @function
_ZN2v88internal9Accessors19MakeArrayLengthInfoEPNS0_7IsolateE:
.LFB19334:
	.cfi_startproc
	endbr64
	leaq	2768(%rdi), %rsi
	leaq	_ZN2v88internal9Accessors17ArrayLengthSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE(%rip), %rcx
	leaq	_ZN2v88internal9Accessors17ArrayLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE(%rip), %rdx
	jmp	_ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE
	.cfi_endproc
.LFE19334:
	.size	_ZN2v88internal9Accessors19MakeArrayLengthInfoEPNS0_7IsolateE, .-_ZN2v88internal9Accessors19MakeArrayLengthInfoEPNS0_7IsolateE
	.section	.text._ZN2v88internal9Accessors28MakeModuleNamespaceEntryInfoEPNS0_7IsolateENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors28MakeModuleNamespaceEntryInfoEPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal9Accessors28MakeModuleNamespaceEntryInfoEPNS0_7IsolateENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal9Accessors28MakeModuleNamespaceEntryInfoEPNS0_7IsolateENS0_6HandleINS0_6StringEEE:
.LFB19337:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internal9Accessors26ModuleNamespaceEntrySetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE(%rip), %rcx
	leaq	_ZN2v88internal9Accessors26ModuleNamespaceEntryGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE(%rip), %rdx
	jmp	_ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE
	.cfi_endproc
.LFE19337:
	.size	_ZN2v88internal9Accessors28MakeModuleNamespaceEntryInfoEPNS0_7IsolateENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal9Accessors28MakeModuleNamespaceEntryInfoEPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal9Accessors20MakeStringLengthInfoEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors20MakeStringLengthInfoEPNS0_7IsolateE
	.type	_ZN2v88internal9Accessors20MakeStringLengthInfoEPNS0_7IsolateE, @function
_ZN2v88internal9Accessors20MakeStringLengthInfoEPNS0_7IsolateE:
.LFB19339:
	.cfi_startproc
	endbr64
	leaq	2768(%rdi), %rsi
	xorl	%ecx, %ecx
	leaq	_ZN2v88internal9Accessors18StringLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE(%rip), %rdx
	jmp	_ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE
	.cfi_endproc
.LFE19339:
	.size	_ZN2v88internal9Accessors20MakeStringLengthInfoEPNS0_7IsolateE, .-_ZN2v88internal9Accessors20MakeStringLengthInfoEPNS0_7IsolateE
	.section	.text._ZN2v88internal9Accessors25MakeFunctionPrototypeInfoEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors25MakeFunctionPrototypeInfoEPNS0_7IsolateE
	.type	_ZN2v88internal9Accessors25MakeFunctionPrototypeInfoEPNS0_7IsolateE, @function
_ZN2v88internal9Accessors25MakeFunctionPrototypeInfoEPNS0_7IsolateE:
.LFB19343:
	.cfi_startproc
	endbr64
	leaq	3104(%rdi), %rsi
	leaq	_ZN2v88internal9Accessors23FunctionPrototypeSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE(%rip), %rcx
	leaq	_ZN2v88internal9Accessors23FunctionPrototypeGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE(%rip), %rdx
	jmp	_ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE
	.cfi_endproc
.LFE19343:
	.size	_ZN2v88internal9Accessors25MakeFunctionPrototypeInfoEPNS0_7IsolateE, .-_ZN2v88internal9Accessors25MakeFunctionPrototypeInfoEPNS0_7IsolateE
	.section	.text._ZN2v88internal9Accessors22MakeFunctionLengthInfoEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors22MakeFunctionLengthInfoEPNS0_7IsolateE
	.type	_ZN2v88internal9Accessors22MakeFunctionLengthInfoEPNS0_7IsolateE, @function
_ZN2v88internal9Accessors22MakeFunctionLengthInfoEPNS0_7IsolateE:
.LFB19345:
	.cfi_startproc
	endbr64
	leaq	2768(%rdi), %rsi
	leaq	_ZN2v88internal9Accessors25ReconfigureToDataPropertyENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE(%rip), %rcx
	leaq	_ZN2v88internal9Accessors20FunctionLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE(%rip), %rdx
	jmp	_ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE
	.cfi_endproc
.LFE19345:
	.size	_ZN2v88internal9Accessors22MakeFunctionLengthInfoEPNS0_7IsolateE, .-_ZN2v88internal9Accessors22MakeFunctionLengthInfoEPNS0_7IsolateE
	.section	.text._ZN2v88internal9Accessors20MakeFunctionNameInfoEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors20MakeFunctionNameInfoEPNS0_7IsolateE
	.type	_ZN2v88internal9Accessors20MakeFunctionNameInfoEPNS0_7IsolateE, @function
_ZN2v88internal9Accessors20MakeFunctionNameInfoEPNS0_7IsolateE:
.LFB19347:
	.cfi_startproc
	endbr64
	leaq	2872(%rdi), %rsi
	leaq	_ZN2v88internal9Accessors25ReconfigureToDataPropertyENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE(%rip), %rcx
	leaq	_ZN2v88internal9Accessors18FunctionNameGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE(%rip), %rdx
	jmp	_ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE
	.cfi_endproc
.LFE19347:
	.size	_ZN2v88internal9Accessors20MakeFunctionNameInfoEPNS0_7IsolateE, .-_ZN2v88internal9Accessors20MakeFunctionNameInfoEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal9Accessors20FunctionGetArgumentsEPNS0_15JavaScriptFrameEi.str1.1,"aMS",@progbits,1
.LC5:
	.string	"unreachable code"
	.section	.text._ZN2v88internal9Accessors20FunctionGetArgumentsEPNS0_15JavaScriptFrameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors20FunctionGetArgumentsEPNS0_15JavaScriptFrameEi
	.type	_ZN2v88internal9Accessors20FunctionGetArgumentsEPNS0_15JavaScriptFrameEi, @function
_ZN2v88internal9Accessors20FunctionGetArgumentsEPNS0_15JavaScriptFrameEi:
.LFB19363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-1488(%rbp), %r12
	pushq	%rbx
	subq	$1456, %rsp
	.cfi_offset 3, -48
	movq	16(%rdi), %r14
	movq	32(%rdi), %rbx
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r14, %rsi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -72(%rbp)
	je	.L597
.L607:
	movq	%r12, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L597
	cmpq	32(%rax), %rbx
	jne	.L607
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_117GetFrameArgumentsEPNS0_7IsolateEPNS0_23JavaScriptFrameIteratorEi
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L608
	addq	$1456, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L597:
	.cfi_restore_state
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L608:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19363:
	.size	_ZN2v88internal9Accessors20FunctionGetArgumentsEPNS0_15JavaScriptFrameEi, .-_ZN2v88internal9Accessors20FunctionGetArgumentsEPNS0_15JavaScriptFrameEi
	.section	.text._ZN2v88internal9Accessors25MakeFunctionArgumentsInfoEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors25MakeFunctionArgumentsInfoEPNS0_7IsolateE
	.type	_ZN2v88internal9Accessors25MakeFunctionArgumentsInfoEPNS0_7IsolateE, @function
_ZN2v88internal9Accessors25MakeFunctionArgumentsInfoEPNS0_7IsolateE:
.LFB19368:
	.cfi_startproc
	endbr64
	leaq	2040(%rdi), %rsi
	xorl	%ecx, %ecx
	leaq	_ZN2v88internal9Accessors23FunctionArgumentsGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE(%rip), %rdx
	jmp	_ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE
	.cfi_endproc
.LFE19368:
	.size	_ZN2v88internal9Accessors25MakeFunctionArgumentsInfoEPNS0_7IsolateE, .-_ZN2v88internal9Accessors25MakeFunctionArgumentsInfoEPNS0_7IsolateE
	.section	.text._ZN2v88internal10FindCallerEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal10FindCallerEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal10FindCallerEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal10FindCallerEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE:
.LFB19382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1536(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	subq	$1688, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1552(%rbp)
	movq	%r14, %rdi
	movq	$0, -1544(%rbp)
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -120(%rbp)
	je	.L611
	movq	%r14, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-120(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	movaps	%xmm0, -96(%rbp)
	testq	%rdi, %rdi
	je	.L612
	movq	(%rdi), %rax
	leaq	-96(%rbp), %rsi
	call	*136(%rax)
	movq	-88(%rbp), %rax
	subq	-96(%rbp), %rax
	movabsq	$7905747460161236407, %rdx
	sarq	$3, %rax
	imulq	%rdx, %rax
	movl	%eax, -72(%rbp)
.L612:
	movq	(%rbx), %rax
	movq	23(%rax), %rax
	movl	47(%rax), %eax
	testb	$32, %al
	je	.L622
.L707:
	movq	-88(%rbp), %r12
	xorl	%r14d, %r14d
.L614:
	movq	-96(%rbp), %r13
	cmpq	%r13, %r12
	je	.L661
	.p2align 4,,10
	.p2align 3
.L662:
	movq	%r13, %rdi
	addq	$56, %r13
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	cmpq	%r13, %r12
	jne	.L662
	movq	-96(%rbp), %r13
.L661:
	testq	%r13, %r13
	je	.L663
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L663:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L709
	addq	$1688, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L615:
	.cfi_restore_state
	movq	-96(%rbp), %rcx
.L621:
	subl	$1, %eax
	movl	%eax, -72(%rbp)
	cltq
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	24(%rcx,%rdx,8), %rax
	movq	-1552(%rbp), %rdx
	movq	(%rax), %rcx
	movq	12464(%rdx), %rsi
	movq	31(%rcx), %rcx
	movq	39(%rcx), %rcx
	movq	1151(%rcx), %rcx
	movq	39(%rsi), %rsi
	movq	1151(%rsi), %rsi
	cmpq	%rcx, %rsi
	jne	.L622
	movq	%rax, -1544(%rbp)
	cmpq	%rax, %rbx
	je	.L631
	movq	(%rbx), %rdi
	cmpq	%rdi, (%rax)
	je	.L631
.L622:
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	jg	.L615
	cmpq	$0, -120(%rbp)
	je	.L710
	movq	%r14, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %r12
	movq	%rax, -1720(%rbp)
	cmpq	%rax, %r12
	je	.L618
	movq	%r12, %r15
	.p2align 4,,10
	.p2align 3
.L619:
	movq	%r15, %rdi
	addq	$56, %r15
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	cmpq	%r15, -1720(%rbp)
	jne	.L619
	movq	%r12, -88(%rbp)
.L618:
	movq	-120(%rbp), %rdi
	movl	$-1, -72(%rbp)
	testq	%rdi, %rdi
	je	.L642
	movq	(%rdi), %rax
	leaq	-96(%rbp), %rsi
	call	*136(%rax)
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movabsq	$7905747460161236407, %rax
	subq	%rcx, %rdx
	sarq	$3, %rdx
	imulq	%rax, %rdx
	movl	%edx, -72(%rbp)
	movl	%edx, %eax
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L624:
	movq	-96(%rbp), %rsi
.L630:
	subl	$1, %eax
	movl	%eax, -72(%rbp)
	cltq
	leaq	0(,%rax,8), %rcx
	subq	%rax, %rcx
	movq	24(%rsi,%rcx,8), %rax
	movq	12464(%rdx), %rsi
	movq	(%rax), %rcx
	movq	31(%rcx), %rcx
	movq	39(%rcx), %rcx
	movq	1151(%rcx), %rcx
	movq	39(%rsi), %rsi
	movq	1151(%rsi), %rsi
	cmpq	%rcx, %rsi
	jne	.L631
	movq	%rax, -1544(%rbp)
	movq	(%rax), %rcx
	movq	23(%rcx), %rcx
	movl	47(%rcx), %ecx
	andl	$268435456, %ecx
	je	.L645
.L631:
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	jg	.L624
	cmpq	$0, -120(%rbp)
	je	.L711
	movq	%r14, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-96(%rbp), %r12
	movq	-88(%rbp), %r15
	cmpq	%r15, %r12
	je	.L627
	movq	%r12, %rbx
	.p2align 4,,10
	.p2align 3
.L628:
	movq	%rbx, %rdi
	addq	$56, %rbx
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	cmpq	%rbx, %r15
	jne	.L628
	movq	%r12, -88(%rbp)
.L627:
	movq	-120(%rbp), %rdi
	movl	$-1, -72(%rbp)
	testq	%rdi, %rdi
	je	.L642
	movq	(%rdi), %rax
	leaq	-96(%rbp), %rsi
	call	*136(%rax)
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdx
	movabsq	$7905747460161236407, %rax
	subq	%rsi, %rdx
	sarq	$3, %rdx
	imulq	%rax, %rdx
	movl	%edx, -72(%rbp)
	movl	%edx, %eax
.L626:
	cmpl	$-1, %eax
	je	.L708
	movq	-1552(%rbp), %rdx
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L710:
	movq	-96(%rbp), %rcx
.L617:
	cmpl	$-1, %eax
	jne	.L621
.L708:
	movq	-88(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L642:
	movq	$0, -1544(%rbp)
	xorl	%r14d, %r14d
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L714:
	movq	%r12, %rbx
	.p2align 4,,10
	.p2align 3
.L641:
	movq	%rbx, %rdi
	addq	$56, %rbx
	call	_ZN2v88internal12FrameSummaryD1Ev@PLT
	cmpq	%rbx, %r15
	jne	.L641
	movq	%r12, -88(%rbp)
.L640:
	movq	-120(%rbp), %rdi
	movl	$-1, -72(%rbp)
	testq	%rdi, %rdi
	je	.L642
	movq	(%rdi), %rax
	leaq	-96(%rbp), %rsi
	call	*136(%rax)
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movabsq	$7905747460161236407, %rax
	subq	%rcx, %rdx
	sarq	$3, %rdx
	imulq	%rax, %rdx
	movl	%edx, -72(%rbp)
	movl	%edx, %eax
.L639:
	cmpl	$-1, %eax
	je	.L708
.L643:
	subl	$1, %eax
	movl	%eax, -72(%rbp)
	cltq
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	24(%rcx,%rdx,8), %rax
	movq	-1552(%rbp), %rdx
	movq	12464(%rdx), %rcx
	movq	(%rax), %rdx
	movq	31(%rdx), %rdx
	movq	39(%rdx), %rdx
	movq	1151(%rdx), %rdx
	movq	39(%rcx), %rcx
	movq	1151(%rcx), %rcx
	cmpq	%rdx, %rcx
	jne	.L644
	movq	%rax, -1544(%rbp)
.L645:
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movl	47(%rax), %eax
	testb	$32, %al
	jne	.L633
	movq	-1544(%rbp), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L712
.L634:
	leaq	-1712(%rbp), %rdi
	movq	%rax, -1712(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L633
.L644:
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	jg	.L637
	cmpq	$0, -120(%rbp)
	je	.L713
	movq	%r14, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-96(%rbp), %r12
	movq	-88(%rbp), %r15
	cmpq	%r15, %r12
	jne	.L714
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L711:
	movq	-96(%rbp), %rsi
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L611:
	pxor	%xmm0, %xmm0
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	movaps	%xmm0, -96(%rbp)
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L637:
	movq	-96(%rbp), %rcx
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L713:
	movq	-96(%rbp), %rcx
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L712:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L715
.L635:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	je	.L644
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L633:
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	jne	.L646
	movq	-1544(%rbp), %rax
	movq	%rax, -1720(%rbp)
.L647:
	movq	-1720(%rbp), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andl	$64, %edx
	jne	.L707
	movq	31(%rax), %rax
	movq	12464(%r13), %rdx
	movq	39(%rax), %rax
	movq	1151(%rax), %rax
	movq	39(%rdx), %rdx
	movq	1151(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L707
	movq	-1720(%rbp), %r14
	movq	-88(%rbp), %r12
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L715:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L635
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L646:
	movq	-120(%rbp), %r15
	leaq	-1712(%rbp), %r12
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal15TranslatedStateC1EPKNS0_15JavaScriptFrameE@PLT
	movq	32(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15TranslatedState7PrepareEm@PLT
	movl	-72(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal15TranslatedState24GetFrameFromJSFrameIndexEi@PLT
	movq	56(%rax), %r14
	movq	%r14, %rdi
	call	_ZNK2v88internal15TranslatedValue20IsMaterializedObjectEv@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal15TranslatedValue8GetValueEv@PLT
	movq	%rax, %r14
	testb	%bl, %bl
	jne	.L716
.L648:
	cmpq	$0, -1664(%rbp)
	movq	%r14, -1720(%rbp)
	je	.L649
	movq	-1592(%rbp), %rax
	movq	-1624(%rbp), %rbx
	leaq	8(%rax), %r12
	cmpq	%rbx, %r12
	jbe	.L651
	.p2align 4,,10
	.p2align 3
.L650:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	ja	.L650
.L651:
	movq	-1664(%rbp), %rdi
	call	_ZdlPv@PLT
.L649:
	movq	-1704(%rbp), %r15
	movq	-1712(%rbp), %r12
	cmpq	%r12, %r15
	je	.L658
	.p2align 4,,10
	.p2align 3
.L652:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L655
	movq	112(%r12), %rax
	movq	80(%r12), %r14
	leaq	8(%rax), %rbx
	cmpq	%r14, %rbx
	jbe	.L656
	.p2align 4,,10
	.p2align 3
.L657:
	movq	(%r14), %rdi
	addq	$8, %r14
	call	_ZdlPv@PLT
	cmpq	%r14, %rbx
	ja	.L657
	movq	40(%r12), %rdi
.L656:
	call	_ZdlPv@PLT
.L655:
	addq	$120, %r12
	cmpq	%r12, %r15
	jne	.L652
.L658:
	movq	-1712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L647
	call	_ZdlPv@PLT
	jmp	.L647
.L716:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15TranslatedState31StoreMaterializedValuesAndDeoptEPNS0_15JavaScriptFrameE@PLT
	jmp	.L648
.L709:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19382:
	.size	_ZN2v88internal10FindCallerEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal10FindCallerEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZN2v88internal9Accessors20FunctionCallerGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors20FunctionCallerGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal9Accessors20FunctionCallerGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal9Accessors20FunctionCallerGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE:
.LFB19386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rsi, %rbx
	movq	16(%rax), %r12
	addl	$1, 41104(%r12)
	movq	(%rsi), %rax
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	leaq	8(%rax), %rsi
	call	_ZN2v88internal10FindCallerEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	leaq	104(%r12), %rdx
	testq	%rax, %rax
	cmove	%rdx, %rax
	movq	(%rbx), %rdx
	movq	(%rax), %rax
	movq	%rax, 32(%rdx)
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L721
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L721:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19386:
	.size	_ZN2v88internal9Accessors20FunctionCallerGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE, .-_ZN2v88internal9Accessors20FunctionCallerGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE
	.section	.text._ZN2v88internal9Accessors22MakeFunctionCallerInfoEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors22MakeFunctionCallerInfoEPNS0_7IsolateE
	.type	_ZN2v88internal9Accessors22MakeFunctionCallerInfoEPNS0_7IsolateE, @function
_ZN2v88internal9Accessors22MakeFunctionCallerInfoEPNS0_7IsolateE:
.LFB19387:
	.cfi_startproc
	endbr64
	leaq	2232(%rdi), %rsi
	xorl	%ecx, %ecx
	leaq	_ZN2v88internal9Accessors20FunctionCallerGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE(%rip), %rdx
	jmp	_ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE
	.cfi_endproc
.LFE19387:
	.size	_ZN2v88internal9Accessors22MakeFunctionCallerInfoEPNS0_7IsolateE, .-_ZN2v88internal9Accessors22MakeFunctionCallerInfoEPNS0_7IsolateE
	.section	.text._ZN2v88internal9Accessors27MakeBoundFunctionLengthInfoEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors27MakeBoundFunctionLengthInfoEPNS0_7IsolateE
	.type	_ZN2v88internal9Accessors27MakeBoundFunctionLengthInfoEPNS0_7IsolateE, @function
_ZN2v88internal9Accessors27MakeBoundFunctionLengthInfoEPNS0_7IsolateE:
.LFB19389:
	.cfi_startproc
	endbr64
	leaq	2768(%rdi), %rsi
	leaq	_ZN2v88internal9Accessors25ReconfigureToDataPropertyENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE(%rip), %rcx
	leaq	_ZN2v88internal9Accessors25BoundFunctionLengthGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE(%rip), %rdx
	jmp	_ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE
	.cfi_endproc
.LFE19389:
	.size	_ZN2v88internal9Accessors27MakeBoundFunctionLengthInfoEPNS0_7IsolateE, .-_ZN2v88internal9Accessors27MakeBoundFunctionLengthInfoEPNS0_7IsolateE
	.section	.text._ZN2v88internal9Accessors25MakeBoundFunctionNameInfoEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors25MakeBoundFunctionNameInfoEPNS0_7IsolateE
	.type	_ZN2v88internal9Accessors25MakeBoundFunctionNameInfoEPNS0_7IsolateE, @function
_ZN2v88internal9Accessors25MakeBoundFunctionNameInfoEPNS0_7IsolateE:
.LFB19391:
	.cfi_startproc
	endbr64
	leaq	2872(%rdi), %rsi
	leaq	_ZN2v88internal9Accessors25ReconfigureToDataPropertyENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE(%rip), %rcx
	leaq	_ZN2v88internal9Accessors23BoundFunctionNameGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE(%rip), %rdx
	jmp	_ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE
	.cfi_endproc
.LFE19391:
	.size	_ZN2v88internal9Accessors25MakeBoundFunctionNameInfoEPNS0_7IsolateE, .-_ZN2v88internal9Accessors25MakeBoundFunctionNameInfoEPNS0_7IsolateE
	.section	.text._ZN2v88internal9Accessors18MakeErrorStackInfoEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Accessors18MakeErrorStackInfoEPNS0_7IsolateE
	.type	_ZN2v88internal9Accessors18MakeErrorStackInfoEPNS0_7IsolateE, @function
_ZN2v88internal9Accessors18MakeErrorStackInfoEPNS0_7IsolateE:
.LFB19394:
	.cfi_startproc
	endbr64
	leaq	3320(%rdi), %rsi
	leaq	_ZN2v88internal9Accessors16ErrorStackSetterENS_5LocalINS_4NameEEENS2_INS_5ValueEEERKNS_20PropertyCallbackInfoINS_7BooleanEEE(%rip), %rcx
	leaq	_ZN2v88internal9Accessors16ErrorStackGetterENS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEE(%rip), %rdx
	jmp	_ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE
	.cfi_endproc
.LFE19394:
	.size	_ZN2v88internal9Accessors18MakeErrorStackInfoEPNS0_7IsolateE, .-_ZN2v88internal9Accessors18MakeErrorStackInfoEPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE, @function
_GLOBAL__sub_I__ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE:
.LFB24078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24078:
	.size	_GLOBAL__sub_I__ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE, .-_GLOBAL__sub_I__ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal9Accessors12MakeAccessorEPNS0_7IsolateENS0_6HandleINS0_4NameEEEPFvNS_5LocalINS_4NameEEERKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS9_NS7_ISB_EERKNSA_INS_7BooleanEEEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1127219200
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
