	.file	"js-objects.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4551:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4551:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4552:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4552:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD1Ev
	.type	_ZN2v88internal8OFStreamD1Ev, @function
_ZN2v88internal8OFStreamD1Ev:
.LFB21368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE21368:
	.size	_ZN2v88internal8OFStreamD1Ev, .-_ZN2v88internal8OFStreamD1Ev
	.section	.rodata._ZNK2v88internal14Representation8MnemonicEv.isra.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"h"
.LC1:
	.string	"v"
.LC2:
	.string	"s"
.LC3:
	.string	"d"
.LC4:
	.string	"t"
.LC5:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal14Representation8MnemonicEv.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal14Representation8MnemonicEv.isra.0, @function
_ZNK2v88internal14Representation8MnemonicEv.isra.0:
.LFB28042:
	.cfi_startproc
	cmpb	$4, %dil
	ja	.L7
	leaq	.L9(%rip), %rdx
	movzbl	%dil, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal14Representation8MnemonicEv.isra.0,"a",@progbits
	.align 4
	.align 4
.L9:
	.long	.L13-.L9
	.long	.L14-.L9
	.long	.L11-.L9
	.long	.L10-.L9
	.long	.L8-.L9
	.section	.text._ZNK2v88internal14Representation8MnemonicEv.isra.0
	.p2align 4,,10
	.p2align 3
.L14:
	leaq	.LC2(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC4(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	leaq	.LC1(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	leaq	.LC3(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	.LC0(%rip), %rax
	ret
.L7:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE28042:
	.size	_ZNK2v88internal14Representation8MnemonicEv.isra.0, .-_ZNK2v88internal14Representation8MnemonicEv.isra.0
	.section	.text._ZNK2v88internal8JSObject21HasTypedArrayElementsEPNS0_7IsolateE.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal8JSObject21HasTypedArrayElementsEPNS0_7IsolateE.isra.0, @function
_ZNK2v88internal8JSObject21HasTypedArrayElementsEPNS0_7IsolateE.isra.0:
.LFB28196:
	.cfi_startproc
	movq	(%rdi), %rax
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$17, %eax
	cmpb	$10, %al
	setbe	%al
	ret
	.cfi_endproc
.LFE28196:
	.size	_ZNK2v88internal8JSObject21HasTypedArrayElementsEPNS0_7IsolateE.isra.0, .-_ZNK2v88internal8JSObject21HasTypedArrayElementsEPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal8OFStreamD0Ev,"axG",@progbits,_ZN2v88internal8OFStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD0Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD0Ev:
.LFB28381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28381:
	.size	_ZTv0_n24_N2v88internal8OFStreamD0Ev, .-_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD0Ev
	.type	_ZN2v88internal8OFStreamD0Ev, @function
_ZN2v88internal8OFStreamD0Ev:
.LFB21369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21369:
	.size	_ZN2v88internal8OFStreamD0Ev, .-_ZN2v88internal8OFStreamD0Ev
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD1Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD1Ev:
.LFB28382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE28382:
	.size	_ZTv0_n24_N2v88internal8OFStreamD1Ev, .-_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.section	.rodata._ZN2v88internal10FieldIndex13ForDescriptorEPNS0_7IsolateENS0_3MapEi.isra.0.str1.1,"aMS",@progbits,1
.LC6:
	.string	"%s\n"
	.section	.text._ZN2v88internal10FieldIndex13ForDescriptorEPNS0_7IsolateENS0_3MapEi.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal10FieldIndex13ForDescriptorEPNS0_7IsolateENS0_3MapEi.isra.0, @function
_ZN2v88internal10FieldIndex13ForDescriptorEPNS0_7IsolateENS0_3MapEi.isra.0:
.LFB28329:
	.cfi_startproc
	leal	3(%rsi,%rsi,2), %eax
	movq	39(%rdi), %rdx
	movq	%rdi, %r9
	sall	$3, %eax
	cltq
	movq	7(%rax,%rdx), %rdi
	movzbl	7(%r9), %ecx
	movzbl	8(%r9), %edx
	movq	%rdi, %rax
	shrq	$38, %rdi
	shrq	$51, %rax
	subl	%edx, %ecx
	andl	$7, %edi
	movq	%rax, %rsi
	andl	$1023, %esi
	cmpl	%ecx, %esi
	setl	%dl
	jl	.L34
	subl	%ecx, %esi
	movl	$16, %r8d
	leal	16(,%rsi,8), %esi
.L26:
	cmpl	$2, %edi
	je	.L30
	cmpb	$2, %dil
	jg	.L28
	je	.L29
.L31:
	xorl	%edi, %edi
.L27:
	movzbl	%dl, %eax
	movslq	%ecx, %rcx
	movslq	%esi, %rsi
	movslq	%r8d, %r8
	salq	$14, %rax
	salq	$17, %rcx
	orq	%rcx, %rax
	salq	$27, %r8
	orq	%rsi, %rax
	orq	%r8, %rax
	orq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	leal	-3(%rdi), %eax
	cmpb	$1, %al
	jbe	.L31
.L29:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal14Representation8MnemonicEv.isra.0
	leaq	.LC6(%rip), %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$32768, %edi
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L34:
	movzbl	8(%r9), %r8d
	movzbl	8(%r9), %eax
	addl	%eax, %esi
	sall	$3, %r8d
	sall	$3, %esi
	jmp	.L26
	.cfi_endproc
.LFE28329:
	.size	_ZN2v88internal10FieldIndex13ForDescriptorEPNS0_7IsolateENS0_3MapEi.isra.0, .-_ZN2v88internal10FieldIndex13ForDescriptorEPNS0_7IsolateENS0_3MapEi.isra.0
	.section	.text._ZNK2v88internal10JSReceiver19property_dictionaryEPNS0_7IsolateE.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal10JSReceiver19property_dictionaryEPNS0_7IsolateE.isra.0, @function
_ZNK2v88internal10JSReceiver19property_dictionaryEPNS0_7IsolateE.isra.0:
.LFB28229:
	.cfi_startproc
	movq	(%rdi), %rax
	movq	7(%rax), %rdx
	movq	%rdx, %r8
	andl	$1, %edx
	jne	.L37
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %r8
.L37:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE28229:
	.size	_ZNK2v88internal10JSReceiver19property_dictionaryEPNS0_7IsolateE.isra.0, .-_ZNK2v88internal10JSReceiver19property_dictionaryEPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal12_GLOBAL__N_126SetHashAndUpdatePropertiesENS0_10HeapObjectEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_126SetHashAndUpdatePropertiesENS0_10HeapObjectEi, @function
_ZN2v88internal12_GLOBAL__N_126SetHashAndUpdatePropertiesENS0_10HeapObjectEi:
.LFB21423:
	.cfi_startproc
	movq	%rdi, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	%rdi, -37304(%rax)
	je	.L40
	cmpq	%rdi, -36624(%rax)
	je	.L40
	cmpq	%rdi, -36536(%rax)
	je	.L40
	movq	-1(%rdi), %rax
	cmpw	$158, 11(%rax)
	je	.L45
	movq	-1(%rdi), %rax
	salq	$32, %rsi
	cmpw	$131, 11(%rax)
	movq	%rdi, %rax
	movq	%rsi, 47(%rdi)
	je	.L46
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%rsi, %rax
	salq	$32, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movslq	11(%rdi), %rax
	sall	$10, %esi
	andl	$-2147482625, %eax
	orl	%eax, %esi
	movq	%rdi, %rax
	salq	$32, %rsi
	movq	%rsi, 7(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	ret
	.cfi_endproc
.LFE21423:
	.size	_ZN2v88internal12_GLOBAL__N_126SetHashAndUpdatePropertiesENS0_10HeapObjectEi, .-_ZN2v88internal12_GLOBAL__N_126SetHashAndUpdatePropertiesENS0_10HeapObjectEi
	.section	.text._ZN2v88internal12_GLOBAL__N_128TestPropertiesIntegrityLevelENS0_8JSObjectENS0_18PropertyAttributesE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_128TestPropertiesIntegrityLevelENS0_8JSObjectENS0_18PropertyAttributesE, @function
_ZN2v88internal12_GLOBAL__N_128TestPropertiesIntegrityLevelENS0_8JSObjectENS0_18PropertyAttributesE:
.LFB21511:
	.cfi_startproc
	movq	-1(%rdi), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	jne	.L48
	movq	-1(%rdi), %rax
	movq	39(%rax), %rdi
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	movl	%eax, %edx
	je	.L72
	cmpl	$5, %esi
	je	.L93
	subl	$1, %edx
	leaq	23(%rdi), %rax
	leaq	(%rdx,%rdx,2), %rdx
	leaq	47(%rdi,%rdx,8), %rsi
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L52:
	movq	8(%rax), %rdx
	btq	$37, %rdx
	jnc	.L74
	addq	$24, %rax
	cmpq	%rax, %rsi
	je	.L72
.L56:
	movq	(%rax), %rdx
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L52
	testb	$1, 11(%rdx)
	je	.L52
	addq	$24, %rax
	cmpq	%rax, %rsi
	jne	.L56
.L72:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%rdi, %rax
	movq	7(%rdi), %rdi
	andq	$-262144, %rax
	movq	24(%rax), %r8
	subq	$37592, %r8
	testb	$1, %dil
	jne	.L64
	movq	1056(%r8), %rdi
.L64:
	movslq	35(%rdi), %rax
	xorl	%ecx, %ecx
	subq	$1, %rdi
	movl	%eax, %r9d
	testl	%eax, %eax
	jne	.L73
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L96:
	movq	-1(%rax), %r10
	cmpw	$64, 11(%r10)
	je	.L94
.L70:
	leal	72(,%rdx,8), %eax
	cltq
	movq	(%rax,%rdi), %rdx
	movq	%rdx, %r10
	shrq	$35, %rdx
	movl	%edx, %eax
	sarq	$32, %r10
	andl	$7, %eax
	andl	$4, %edx
	je	.L74
	cmpl	$5, %esi
	je	.L95
	.p2align 4,,10
	.p2align 3
.L71:
	addl	$1, %ecx
	cmpl	%ecx, %r9d
	je	.L72
.L73:
	leal	(%rcx,%rcx,2), %edx
	leal	56(,%rdx,8), %eax
	cltq
	movq	(%rax,%rdi), %rax
	cmpq	96(%r8), %rax
	je	.L71
	cmpq	88(%r8), %rax
	je	.L71
	testb	$1, %al
	je	.L70
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L95:
	andl	$1, %r10d
	jne	.L71
	andl	$1, %eax
	jne	.L71
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	testb	$1, 11(%rax)
	je	.L70
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L74:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	leal	-1(%rax), %eax
	leaq	23(%rdi), %rcx
	leaq	(%rax,%rax,2), %rax
	leaq	47(%rdi,%rax,8), %rdi
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L60:
	movq	8(%rcx), %rdx
	movq	%rdx, %rsi
	shrq	$35, %rdx
	movl	%edx, %eax
	sarq	$32, %rsi
	andl	$7, %eax
	andl	$4, %edx
	je	.L74
	andl	$1, %esi
	jne	.L61
	andl	$1, %eax
	je	.L97
.L61:
	addq	$24, %rcx
	cmpq	%rcx, %rdi
	je	.L72
.L51:
	movq	(%rcx), %rax
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	jne	.L60
	testb	$1, 11(%rax)
	je	.L60
	jmp	.L61
.L97:
	ret
	.cfi_endproc
.LFE21511:
	.size	_ZN2v88internal12_GLOBAL__N_128TestPropertiesIntegrityLevelENS0_8JSObjectENS0_18PropertyAttributesE, .-_ZN2v88internal12_GLOBAL__N_128TestPropertiesIntegrityLevelENS0_8JSObjectENS0_18PropertyAttributesE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_130NativeCodeFunctionSourceStringENS0_6HandleINS0_18SharedFunctionInfoEEE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"function "
.LC8:
	.string	"() { [native code] }"
.LC9:
	.string	"(location_) != nullptr"
.LC10:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal12_GLOBAL__N_130NativeCodeFunctionSourceStringENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_130NativeCodeFunctionSourceStringENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal12_GLOBAL__N_130NativeCodeFunctionSourceStringENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB21583:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	subq	$37592, %rbx
	movq	%rbx, %rsi
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movl	-88(%rbp), %edx
	testl	%edx, %edx
	je	.L124
	movl	-76(%rbp), %eax
	movl	$102, %edx
	leaq	.LC7(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L100:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %r14
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movw	%dx, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L149
	movzbl	(%r14), %edx
	testb	%dl, %dl
	jne	.L100
.L101:
	movq	(%r12), %r12
	movq	15(%r12), %rax
	testb	$1, %al
	jne	.L150
.L106:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L107:
	testb	%al, %al
	je	.L111
	movq	15(%r12), %r14
	testb	$1, %r14b
	jne	.L151
.L109:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L112
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L113:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movl	-88(%rbp), %eax
	testl	%eax, %eax
	je	.L125
	movl	-76(%rbp), %eax
	movl	$40, %edx
	leaq	.LC8(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L116:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	leal	16(%rax,%rax), %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movw	%dx, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L152
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L116
.L117:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	testq	%rax, %rax
	je	.L153
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L154
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L117
	movl	-76(%rbp), %eax
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L149:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r14), %edx
	testb	%dl, %dl
	je	.L101
	movl	-76(%rbp), %eax
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L125:
	movl	$40, %edx
	leaq	.LC8(%rip), %rbx
.L148:
	movl	-76(%rbp), %eax
.L115:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movb	%dl, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L155
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L115
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L148
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$102, %edx
	leaq	.LC7(%rip), %r14
.L147:
	movl	-76(%rbp), %eax
.L99:
	movq	-64(%rbp), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %r14
	cltq
	movq	(%rcx), %rcx
	movl	%esi, -76(%rbp)
	movb	%dl, -1(%rcx,%rax)
	movl	-76(%rbp), %eax
	cmpl	-80(%rbp), %eax
	je	.L156
	movzbl	(%r14), %edx
	testb	%dl, %dl
	jne	.L99
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r14), %edx
	testb	%dl, %dl
	jne	.L147
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L151:
	movq	-1(%r14), %rax
	cmpw	$136, 11(%rax)
	jne	.L109
	leaq	-104(%rbp), %r15
	movq	%r14, -104(%rbp)
	movq	%r15, %rdi
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	testb	%al, %al
	je	.L111
	movq	%r15, %rdi
	movq	%r14, -104(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	%rax, %r14
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L112:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L157
.L114:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r14, (%rsi)
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L111:
	andq	$-262144, %r12
	movq	24(%r12), %rax
	movq	-37464(%rax), %r14
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L150:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L106
	leaq	-104(%rbp), %rdi
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L153:
	leaq	.LC9(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L157:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L114
.L154:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21583:
	.size	_ZN2v88internal12_GLOBAL__N_130NativeCodeFunctionSourceStringENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal12_GLOBAL__N_130NativeCodeFunctionSourceStringENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_,"axG",@progbits,_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_,comdat
	.p2align 4
	.weak	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	.type	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_, @function
_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_:
.LFB10744:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L158
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L163
.L158:
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE10744:
	.size	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_, .-_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	.section	.text._ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE,"axG",@progbits,_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE,comdat
	.p2align 4
	.weak	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	.type	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE, @function
_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE:
.LFB10755:
	.cfi_startproc
	endbr64
	testb	$1, %dl
	je	.L164
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L170
.L164:
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	jmp	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE10755:
	.size	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE, .-_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_133InvalidatePrototypeChainsInternalENS0_3MapE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"Invalidating prototype map %p 's cell\n"
	.section	.text._ZN2v88internal12_GLOBAL__N_133InvalidatePrototypeChainsInternalENS0_3MapE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_133InvalidatePrototypeChainsInternalENS0_3MapE, @function
_ZN2v88internal12_GLOBAL__N_133InvalidatePrototypeChainsInternalENS0_3MapE:
.LFB21534:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, _ZN2v88internal26FLAG_trace_prototype_usersE(%rip)
	jne	.L184
	movq	63(%rbx), %rdi
	testb	$1, %dil
	jne	.L185
.L174:
	movq	71(%rbx), %rax
	testb	$1, %al
	jne	.L186
.L171:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movq	%rdi, %rsi
	xorl	%eax, %eax
	leaq	.LC11(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	63(%rbx), %rdi
	testb	$1, %dil
	je	.L174
.L185:
	movq	-1(%rdi), %rax
	cmpw	$151, 11(%rax)
	jne	.L174
	movabsq	$4294967296, %rdx
	leaq	7(%rdi), %rsi
	movq	%rdx, 7(%rdi)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	71(%rbx), %rax
	testb	$1, %al
	je	.L171
	.p2align 4,,10
	.p2align 3
.L186:
	movq	-1(%rax), %rdx
	cmpw	$95, 11(%rdx)
	jne	.L171
	movq	15(%rax), %rax
	testb	$1, %al
	je	.L171
	movq	-1(%rax), %rdx
	cmpw	$167, 11(%rdx)
	jne	.L171
	cmpl	$1, 19(%rax)
	leaq	15(%rax), %r13
	jle	.L171
	leaq	31(%rax), %rbx
	movl	$1, %r12d
.L181:
	movq	(%rbx), %rdi
	movq	%rdi, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	je	.L187
.L179:
	addl	$1, %r12d
	addq	$8, %rbx
	cmpl	%r12d, 4(%r13)
	jg	.L181
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L187:
	cmpl	$3, %edi
	je	.L179
	andq	$-3, %rdi
	movq	-1(%rdi), %rax
	cmpw	$68, 11(%rax)
	jne	.L179
	call	_ZN2v88internal12_GLOBAL__N_133InvalidatePrototypeChainsInternalENS0_3MapE
	jmp	.L179
	.cfi_endproc
.LFE21534:
	.size	_ZN2v88internal12_GLOBAL__N_133InvalidatePrototypeChainsInternalENS0_3MapE, .-_ZN2v88internal12_GLOBAL__N_133InvalidatePrototypeChainsInternalENS0_3MapE
	.section	.text._ZN2v88internal10FixedArray3setEiNS0_6ObjectE,"axG",@progbits,_ZN2v88internal10FixedArray3setEiNS0_6ObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10FixedArray3setEiNS0_6ObjectE
	.type	_ZN2v88internal10FixedArray3setEiNS0_6ObjectE, @function
_ZN2v88internal10FixedArray3setEiNS0_6ObjectE:
.LFB11471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leal	16(,%rsi,8), %ebx
	movq	(%rdi), %rax
	movslq	%ebx, %rbx
	movq	%rdx, -1(%rbx,%rax)
	testb	$1, %dl
	je	.L188
	movq	%rdx, %r14
	movq	%rdi, %r13
	movq	(%rdi), %rdi
	movq	%rdx, %r12
	andq	$-262144, %r14
	movq	8(%r14), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testl	$262144, %eax
	jne	.L200
	testb	$24, %al
	je	.L188
.L202:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L201
.L188:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rdi
	movq	8(%r14), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testb	$24, %al
	jne	.L202
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L201:
	popq	%rbx
	movq	%r12, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE11471:
	.size	_ZN2v88internal10FixedArray3setEiNS0_6ObjectE, .-_ZN2v88internal10FixedArray3setEiNS0_6ObjectE
	.section	.text._ZN2v88internal13PropertyArray3setEiNS0_6ObjectE,"axG",@progbits,_ZN2v88internal13PropertyArray3setEiNS0_6ObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13PropertyArray3setEiNS0_6ObjectE
	.type	_ZN2v88internal13PropertyArray3setEiNS0_6ObjectE, @function
_ZN2v88internal13PropertyArray3setEiNS0_6ObjectE:
.LFB12327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leal	16(,%rsi,8), %ebx
	movq	(%rdi), %rax
	movslq	%ebx, %rbx
	movq	%rdx, -1(%rbx,%rax)
	testb	$1, %dl
	je	.L203
	movq	%rdx, %r14
	movq	%rdi, %r13
	movq	(%rdi), %rdi
	movq	%rdx, %r12
	andq	$-262144, %r14
	movq	8(%r14), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testl	$262144, %eax
	jne	.L215
	testb	$24, %al
	je	.L203
.L217:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L216
.L203:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rdi
	movq	8(%r14), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testb	$24, %al
	jne	.L217
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L216:
	popq	%rbx
	movq	%r12, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE12327:
	.size	_ZN2v88internal13PropertyArray3setEiNS0_6ObjectE, .-_ZN2v88internal13PropertyArray3setEiNS0_6ObjectE
	.section	.text._ZN2v88internal17PrototypeIterator7AdvanceEv,"axG",@progbits,_ZN2v88internal17PrototypeIterator7AdvanceEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17PrototypeIterator7AdvanceEv
	.type	_ZN2v88internal17PrototypeIterator7AdvanceEv, @function
_ZN2v88internal17PrototypeIterator7AdvanceEv:
.LFB12353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L236
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L223
	subq	$1, %rax
.L225:
	movq	(%rax), %rdx
	movq	(%rbx), %r12
	movl	$1, %eax
	movq	23(%rdx), %rsi
	cmpq	104(%r12), %rsi
	je	.L226
	xorl	%eax, %eax
	cmpl	$1, 24(%rbx)
	je	.L237
.L226:
	cmpq	$0, 16(%rbx)
	movb	%al, 28(%rbx)
	je	.L238
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L228
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L235:
	movq	%rax, 16(%rbx)
.L218:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L239
.L230:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L236:
	movq	8(%rdi), %rdx
	leaq	-1(%rdx), %rax
	testb	$1, %dl
	je	.L225
	movq	-1(%rdx), %rdx
	cmpw	$1024, 11(%rdx)
	jne	.L225
	movq	(%rdi), %rax
	movb	$1, 28(%rdi)
	movq	104(%rax), %rax
	movq	%rax, 8(%rdi)
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L238:
	movq	%rsi, 8(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	cmpw	$1026, 11(%rdx)
	setne	%al
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L223:
	movq	(%rdi), %rax
	movb	$1, 28(%rdi)
	addq	$104, %rax
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L239:
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L230
	.cfi_endproc
.LFE12353:
	.size	_ZN2v88internal17PrototypeIterator7AdvanceEv, .-_ZN2v88internal17PrototypeIterator7AdvanceEv
	.section	.text._ZN2v88internal10JSReceiver11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE,"axG",@progbits,_ZN2v88internal10JSReceiver11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JSReceiver11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE
	.type	_ZN2v88internal10JSReceiver11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE, @function
_ZN2v88internal10JSReceiver11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE:
.LFB12401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$96, %rsp
	movq	(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	movl	$3, %edx
	movq	-1(%rcx), %r8
	cmpw	$64, 11(%r8)
	jne	.L241
	movl	11(%rcx), %edx
	andl	$1, %edx
	cmpb	$1, %dl
	sbbl	%edx, %edx
	andl	$3, %edx
.L241:
	movabsq	$824633720832, %rcx
	movl	%edx, -112(%rbp)
	movq	(%rsi), %rdx
	movq	%rcx, -100(%rbp)
	movq	%rdi, -88(%rbp)
	movq	-1(%rdx), %rdx
	movzwl	11(%rdx), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L250
.L242:
	leaq	-112(%rbp), %r12
	movq	%rax, -80(%rbp)
	movq	%r12, %rdi
	movq	$0, -72(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	%rbx, -48(%rbp)
	movq	$-1, -40(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -108(%rbp)
	jne	.L243
	movq	-88(%rbp), %rax
	addq	$88, %rax
.L244:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L251
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L250:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L242
.L251:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12401:
	.size	_ZN2v88internal10JSReceiver11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE, .-_ZN2v88internal10JSReceiver11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE
	.section	.text._ZN2v88internal8JSObject20RawFastPropertyAtPutENS0_10FieldIndexENS0_6ObjectENS0_16WriteBarrierModeE,"axG",@progbits,_ZN2v88internal8JSObject20RawFastPropertyAtPutENS0_10FieldIndexENS0_6ObjectENS0_16WriteBarrierModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8JSObject20RawFastPropertyAtPutENS0_10FieldIndexENS0_6ObjectENS0_16WriteBarrierModeE
	.type	_ZN2v88internal8JSObject20RawFastPropertyAtPutENS0_10FieldIndexENS0_6ObjectENS0_16WriteBarrierModeE, @function
_ZN2v88internal8JSObject20RawFastPropertyAtPutENS0_10FieldIndexENS0_6ObjectENS0_16WriteBarrierModeE:
.LFB12439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	testl	$16384, %esi
	je	.L253
	andl	$16383, %esi
	movq	%rdx, -1(%rax,%rsi)
	leaq	-1(%rsi), %r13
	testl	%ecx, %ecx
	je	.L252
	movq	%rdi, %rbx
	movq	%rdx, %rax
	movq	(%rdi), %rdi
	notq	%rax
	leaq	0(%r13,%rdi), %rsi
	andl	$1, %eax
	cmpl	$4, %ecx
	je	.L284
	testb	%al, %al
	jne	.L252
.L276:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L252
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L283
.L252:
	addq	$24, %rsp
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	movq	7(%rax), %r13
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %r13b
	jne	.L260
.L262:
	movq	968(%rax), %r13
.L261:
	movq	%rsi, %rax
	sarl	$3, %esi
	shrq	$30, %rax
	andl	$2047, %esi
	andl	$15, %eax
	subl	%eax, %esi
	leal	16(,%rsi,8), %eax
	cltq
	leaq	-1(%r13,%rax), %r14
	movq	%rdx, (%r14)
	testb	$1, %dl
	je	.L252
	movq	%rdx, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L285
.L264:
	testb	$24, %al
	je	.L252
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L252
	movq	%r14, %rsi
	movq	%r13, %rdi
.L283:
	addq	$24, %rsp
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	cmpq	288(%rax), %r13
	jne	.L261
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L285:
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-40(%rbp), %rdx
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L284:
	testb	%al, %al
	jne	.L252
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L276
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	-40(%rbp), %rdx
	leaq	0(%r13,%rdi), %rsi
	jmp	.L276
	.cfi_endproc
.LFE12439:
	.size	_ZN2v88internal8JSObject20RawFastPropertyAtPutENS0_10FieldIndexENS0_6ObjectENS0_16WriteBarrierModeE, .-_ZN2v88internal8JSObject20RawFastPropertyAtPutENS0_10FieldIndexENS0_6ObjectENS0_16WriteBarrierModeE
	.section	.text._ZNK2v88internal18SharedFunctionInfo4NameEv,"axG",@progbits,_ZNK2v88internal18SharedFunctionInfo4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal18SharedFunctionInfo4NameEv
	.type	_ZNK2v88internal18SharedFunctionInfo4NameEv, @function
_ZNK2v88internal18SharedFunctionInfo4NameEv:
.LFB15529:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	15(%rdx), %rax
	testb	$1, %al
	jne	.L296
.L287:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L289:
	testb	%al, %al
	je	.L297
	movq	15(%rdx), %rbx
	testb	$1, %bl
	jne	.L298
.L292:
	movq	%rbx, %rax
.L291:
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L299
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	movq	-1(%rbx), %rax
	cmpw	$136, 11(%rax)
	jne	.L292
	leaq	-48(%rbp), %r13
	movq	%rbx, -48(%rbp)
	movq	%r13, %rdi
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	testb	%al, %al
	je	.L293
	movq	%r13, %rdi
	movq	%rbx, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L297:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	movq	-37464(%rax), %rax
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L296:
	movq	-1(%rax), %rcx
	cmpw	$136, 11(%rcx)
	jne	.L287
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	movq	(%r12), %rdx
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L293:
	movq	(%r12), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37464(%rax), %rax
	jmp	.L291
.L299:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15529:
	.size	_ZNK2v88internal18SharedFunctionInfo4NameEv, .-_ZNK2v88internal18SharedFunctionInfo4NameEv
	.section	.text._ZN2v88internal19TransitionsAccessor10InitializeEv,"axG",@progbits,_ZN2v88internal19TransitionsAccessor10InitializeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19TransitionsAccessor10InitializeEv
	.type	_ZN2v88internal19TransitionsAccessor10InitializeEv, @function
_ZN2v88internal19TransitionsAccessor10InitializeEv:
.LFB15840:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	71(%rax), %rax
	movq	%rax, 24(%rdi)
	testb	$1, %al
	je	.L301
	cmpl	$3, %eax
	je	.L301
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L312
	cmpq	$1, %rdx
	je	.L313
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$1, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	movl	$3, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L314
	movl	$4, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, 32(%rdi)
	ret
	.cfi_endproc
.LFE15840:
	.size	_ZN2v88internal19TransitionsAccessor10InitializeEv, .-_ZN2v88internal19TransitionsAccessor10InitializeEv
	.section	.rodata._ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"(!IsSmi() && (*layout_word_index < length())) || (IsSmi() && (*layout_word_index < 1))"
	.section	.text._ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE,"axG",@progbits,_ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE
	.type	_ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE, @function
_ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE:
.LFB15921:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testb	$64, %dh
	jne	.L334
.L315:
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	movq	(%rdi), %rcx
	movq	47(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L315
	movq	%rsi, %rcx
	movl	$32, %r8d
	notq	%rcx
	movl	%ecx, %edi
	andl	$1, %edi
	je	.L335
.L318:
	movq	%rdx, %rax
	sarl	$3, %edx
	shrq	$30, %rax
	andl	$2047, %edx
	andl	$15, %eax
	subl	%eax, %edx
	cmpl	%r8d, %edx
	jnb	.L328
	testl	%edx, %edx
	leal	31(%rdx), %eax
	cmovns	%edx, %eax
	sarl	$5, %eax
	andl	$1, %ecx
	jne	.L319
	cmpl	%eax, 11(%rsi)
	jle	.L319
	movl	%edx, %r8d
	sarl	$31, %r8d
	shrl	$27, %r8d
	leal	(%rdx,%r8), %ecx
	movl	$1, %edx
	andl	$31, %ecx
	subl	%r8d, %ecx
	sall	%cl, %edx
	testb	%dil, %dil
	je	.L336
.L323:
	sarq	$32, %rsi
	testl	%esi, %edx
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	movslq	11(%rsi), %r8
	sall	$3, %r8d
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L328:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	sall	$2, %eax
	cltq
	movl	15(%rsi,%rax), %eax
	testl	%eax, %edx
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	testb	%dil, %dil
	je	.L321
	cmpl	$31, %edx
	jg	.L321
	movl	%edx, %ecx
	movl	$1, %eax
	sall	%cl, %eax
	movl	%eax, %edx
	jmp	.L323
.L321:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC12(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE15921:
	.size	_ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE, .-_ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE
	.section	.rodata._ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"!constructor_or_backpointer().IsMap()"
	.section	.text._ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE,"axG",@progbits,_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE
	.type	_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE, @function
_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE:
.LFB16140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	leaq	31(%rax), %rcx
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L355
.L338:
	movq	%r12, (%rcx)
	testl	%edx, %edx
	je	.L337
	movq	(%rbx), %rdi
	movq	%r12, %rax
	notq	%rax
	leaq	31(%rdi), %rsi
	andl	$1, %eax
	cmpl	$4, %edx
	je	.L356
	testb	%al, %al
	jne	.L337
.L350:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L337
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L337
	popq	%rbx
	movq	%r12, %rdx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L355:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L338
	leaq	.LC13(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L356:
	testb	%al, %al
	jne	.L337
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L350
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	leaq	31(%rdi), %rsi
	jmp	.L350
	.cfi_endproc
.LFE16140:
	.size	_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE, .-_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE
	.section	.text._ZN2v88internal7Factory21NewHeapNumberFromBitsEmNS0_14AllocationTypeE,"axG",@progbits,_ZN2v88internal7Factory21NewHeapNumberFromBitsEmNS0_14AllocationTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Factory21NewHeapNumberFromBitsEmNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory21NewHeapNumberFromBitsEmNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory21NewHeapNumberFromBitsEmNS0_14AllocationTypeE:
.LFB17026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	movl	%edx, %esi
	subq	$8, %rsp
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	%rbx, 7(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17026:
	.size	_ZN2v88internal7Factory21NewHeapNumberFromBitsEmNS0_14AllocationTypeE, .-_ZN2v88internal7Factory21NewHeapNumberFromBitsEmNS0_14AllocationTypeE
	.section	.text._ZN2v88internal14LookupIteratorC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_10JSReceiverEEENS1_13ConfigurationE,"axG",@progbits,_ZN2v88internal14LookupIteratorC5EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_10JSReceiverEEENS1_13ConfigurationE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LookupIteratorC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_10JSReceiverEEENS1_13ConfigurationE
	.type	_ZN2v88internal14LookupIteratorC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_10JSReceiverEEENS1_13ConfigurationE, @function
_ZN2v88internal14LookupIteratorC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_10JSReceiverEEENS1_13ConfigurationE:
.LFB17039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movq	%rcx, %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%r8, %rbx
	subq	$8, %rsp
	movq	(%rcx), %rdx
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L360
	testb	$1, 11(%rdx)
	movl	$0, %ecx
	cmovne	%ecx, %r9d
.L360:
	movabsq	$824633720832, %rcx
	movl	%r9d, (%r12)
	movq	%rcx, 12(%r12)
	movq	%rdi, 24(%r12)
	movq	-1(%rdx), %rdx
	movzwl	11(%rdx), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L365
.L361:
	movq	%r13, 48(%r12)
	movq	%r12, %rdi
	movq	%rbx, 64(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 56(%r12)
	movq	$-1, 72(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_restore_state
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L361
	.cfi_endproc
.LFE17039:
	.size	_ZN2v88internal14LookupIteratorC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_10JSReceiverEEENS1_13ConfigurationE, .-_ZN2v88internal14LookupIteratorC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_10JSReceiverEEENS1_13ConfigurationE
	.weak	_ZN2v88internal14LookupIteratorC1EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_10JSReceiverEEENS1_13ConfigurationE
	.set	_ZN2v88internal14LookupIteratorC1EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_10JSReceiverEEENS1_13ConfigurationE,_ZN2v88internal14LookupIteratorC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_10JSReceiverEEENS1_13ConfigurationE
	.section	.text._ZN2v88internal14LookupIterator7GetNameEv,"axG",@progbits,_ZN2v88internal14LookupIterator7GetNameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LookupIterator7GetNameEv
	.type	_ZN2v88internal14LookupIterator7GetNameEv, @function
_ZN2v88internal14LookupIterator7GetNameEv:
.LFB17046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 32(%rdi)
	je	.L374
	movq	32(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	movl	72(%rdi), %r13d
	movq	24(%rdi), %r12
	testl	%r13d, %r13d
	js	.L368
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	salq	$32, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb@PLT
	movq	%rax, %r12
.L369:
	movq	(%r12), %rax
	movl	11(%rax), %esi
	cmpl	$10, %esi
	jle	.L375
.L372:
	movq	%r12, 32(%rbx)
	movq	32(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	%r12, %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rax, %r12
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L375:
	cmpl	$3, 7(%rax)
	jne	.L372
	movl	%r13d, %edi
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movq	(%r12), %rdx
	movl	%eax, 7(%rdx)
	jmp	.L372
	.cfi_endproc
.LFE17046:
	.size	_ZN2v88internal14LookupIterator7GetNameEv, .-_ZN2v88internal14LookupIterator7GetNameEv
	.section	.text._ZNK2v88internal14LookupIterator14GetInterceptorEv,"axG",@progbits,_ZNK2v88internal14LookupIterator14GetInterceptorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal14LookupIterator14GetInterceptorEv
	.type	_ZNK2v88internal14LookupIterator14GetInterceptorEv, @function
_ZNK2v88internal14LookupIterator14GetInterceptorEv:
.LFB17060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	56(%rdi), %rax
	cmpl	$-1, 72(%rdi)
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	movq	31(%rax), %rax
	je	.L377
	testb	$1, %al
	jne	.L380
.L381:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rsi
	cmpq	%rsi, %rdx
	je	.L383
	movq	39(%rdx), %rsi
.L383:
	movq	24(%rdi), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L389
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L381
	.p2align 4,,10
	.p2align 3
.L380:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L396
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L381
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L389:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L397
.L391:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	testb	$1, %al
	jne	.L386
.L387:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rsi
	cmpq	%rsi, %rdx
	je	.L383
	movq	31(%rdx), %rsi
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L398:
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L387
	.p2align 4,,10
	.p2align 3
.L386:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L398
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L387
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L397:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L391
	.cfi_endproc
.LFE17060:
	.size	_ZNK2v88internal14LookupIterator14GetInterceptorEv, .-_ZNK2v88internal14LookupIterator14GetInterceptorEv
	.section	.text._ZN2v88internal10HeapObject20synchronized_set_mapENS0_3MapE,"axG",@progbits,_ZN2v88internal10HeapObject20synchronized_set_mapENS0_3MapE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10HeapObject20synchronized_set_mapENS0_3MapE
	.type	_ZN2v88internal10HeapObject20synchronized_set_mapENS0_3MapE, @function
_ZN2v88internal10HeapObject20synchronized_set_mapENS0_3MapE:
.LFB17843:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rsi, -1(%rax)
	testq	%rsi, %rsi
	jne	.L409
.L399:
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	testb	$1, %sil
	je	.L399
	movq	%rsi, %rax
	movq	(%rdi), %rdi
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L399
	movq	%rsi, %rdx
	xorl	%esi, %esi
	jmp	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE17843:
	.size	_ZN2v88internal10HeapObject20synchronized_set_mapENS0_3MapE, .-_ZN2v88internal10HeapObject20synchronized_set_mapENS0_3MapE
	.section	.rodata._ZN2v88internal21ExternalCallbackScopeC2EPNS0_7IsolateEm.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internal21ExternalCallbackScopeC2EPNS0_7IsolateEm.str1.1,"aMS",@progbits,1
.LC15:
	.string	"V8.ExternalCallback"
	.section	.text._ZN2v88internal21ExternalCallbackScopeC2EPNS0_7IsolateEm,"axG",@progbits,_ZN2v88internal21ExternalCallbackScopeC5EPNS0_7IsolateEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21ExternalCallbackScopeC2EPNS0_7IsolateEm
	.type	_ZN2v88internal21ExternalCallbackScopeC2EPNS0_7IsolateEm, @function
_ZN2v88internal21ExternalCallbackScopeC2EPNS0_7IsolateEm:
.LFB18296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	12608(%rsi), %rax
	movq	%rsi, (%rdi)
	movq	%rdx, 8(%rdi)
	movq	%rax, 16(%rdi)
	movq	%rdi, 12608(%rsi)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L430
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L431
.L410:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L432
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L433
.L413:
	movq	%r12, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	movzbl	(%r12), %eax
	testb	$5, %al
	je	.L410
.L431:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L434
.L415:
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L416
	movq	(%rdi), %rax
	call	*8(%rax)
.L416:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L410
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L433:
	leaq	.LC14(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L434:
	subq	$8, %rsp
	leaq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC15(%rip), %rcx
	movl	$66, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L415
.L432:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18296:
	.size	_ZN2v88internal21ExternalCallbackScopeC2EPNS0_7IsolateEm, .-_ZN2v88internal21ExternalCallbackScopeC2EPNS0_7IsolateEm
	.weak	_ZN2v88internal21ExternalCallbackScopeC1EPNS0_7IsolateEm
	.set	_ZN2v88internal21ExternalCallbackScopeC1EPNS0_7IsolateEm,_ZN2v88internal21ExternalCallbackScopeC2EPNS0_7IsolateEm
	.section	.text._ZN2v88internal21ExternalCallbackScopeD2Ev,"axG",@progbits,_ZN2v88internal21ExternalCallbackScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21ExternalCallbackScopeD2Ev
	.type	_ZN2v88internal21ExternalCallbackScopeD2Ev, @function
_ZN2v88internal21ExternalCallbackScopeD2Ev:
.LFB18299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rdx, 12608(%rax)
	movq	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L455
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L456
.L435:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L457
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L458
.L438:
	movq	%r12, _ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip)
	movzbl	(%r12), %eax
	testb	$5, %al
	je	.L435
.L456:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L459
.L440:
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L441
	movq	(%rdi), %rax
	call	*8(%rax)
.L441:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L435
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L458:
	leaq	.LC14(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L459:
	subq	$8, %rsp
	leaq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC15(%rip), %rcx
	movl	$69, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L440
.L457:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18299:
	.size	_ZN2v88internal21ExternalCallbackScopeD2Ev, .-_ZN2v88internal21ExternalCallbackScopeD2Ev
	.weak	_ZN2v88internal21ExternalCallbackScopeD1Ev
	.set	_ZN2v88internal21ExternalCallbackScopeD1Ev,_ZN2v88internal21ExternalCallbackScopeD2Ev
	.section	.rodata._ZN2v88internal12_GLOBAL__N_134SetPropertyWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEENS_5MaybeINS0_11ShouldThrowEEENS4_INS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"interceptor-indexed-set"
.LC17:
	.string	"interceptor-named-set"
	.section	.text._ZN2v88internal12_GLOBAL__N_134SetPropertyWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEENS_5MaybeINS0_11ShouldThrowEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_134SetPropertyWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEENS_5MaybeINS0_11ShouldThrowEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal12_GLOBAL__N_134SetPropertyWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEENS_5MaybeINS0_11ShouldThrowEEENS4_INS0_6ObjectEEE:
.LFB21441:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	15(%rax), %rcx
	cmpq	%rcx, 88(%r15)
	je	.L508
	movq	%rsi, %r12
	movq	48(%rdi), %rsi
	movq	%rdx, %r14
	movq	56(%rdi), %r8
	movq	%rdi, %rbx
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L463
.L466:
	movq	%r15, %rdi
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal6Object15ConvertReceiverEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L464
	movq	(%r12), %rax
	movq	-248(%rbp), %r8
.L467:
	movq	(%rsi), %rcx
	movq	63(%rax), %rdx
	movq	%r15, %rsi
	movq	%r14, %r9
	movq	(%r8), %r8
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movl	72(%rbx), %r15d
	cmpl	$-1, %r15d
	je	.L468
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %r14
	movq	$0, -160(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L509
.L469:
	movq	(%r12), %rax
	movq	15(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L483
	movq	7(%rax), %r9
	movq	%r9, %r8
.L470:
	xorl	%edx, %edx
	cmpl	$32, 41828(%r14)
	movq	%r8, -248(%rbp)
	jne	.L510
.L477:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L511
.L474:
	movq	24(%rbx), %rdi
	movq	12552(%rdi), %rax
	cmpq	%rax, 96(%rdi)
	je	.L480
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
	movb	$0, %ah
.L481:
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rcx
	movq	%rcx, 41704(%rdx)
.L462:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L512
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	movq	-1(%rdx), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L466
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L508:
	movl	$1, %eax
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L480:
	movl	$1, %eax
	movb	%dl, %ah
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L510:
	movl	12616(%r14), %eax
	leaq	-224(%rbp), %r12
	movq	%r9, %rdx
	movq	%r14, %rsi
	movl	$6, 12616(%r14)
	movq	%r12, %rdi
	movl	%eax, -256(%rbp)
	call	_ZN2v88internal21ExternalCallbackScopeC1EPNS0_7IsolateEm
	leaq	-120(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	41016(%r14), %rdi
	movq	%rdi, -264(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-264(%rbp), %rdi
	movq	-248(%rbp), %r8
	testb	%al, %al
	jne	.L513
.L472:
	leaq	-232(%rbp), %rdx
	movq	%r13, %rsi
	movl	%r15d, %edi
	call	*%r8
	movq	-88(%rbp), %rax
	cmpq	%rax, 96(%r14)
	movq	%r12, %rdi
	setne	%dl
	movb	%dl, -248(%rbp)
	call	_ZN2v88internal21ExternalCallbackScopeD1Ev
	movl	-256(%rbp), %eax
	movzbl	-248(%rbp), %edx
	movl	%eax, 12616(%r14)
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L464:
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L468:
	movq	(%r12), %rax
	movq	32(%rbx), %r8
	movq	15(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L485
	movq	7(%rax), %r9
	movq	%r9, %r14
.L475:
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %r15
	movq	$0, -160(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L514
.L476:
	xorl	%edx, %edx
	cmpl	$32, 41828(%r15)
	movq	%r8, -248(%rbp)
	je	.L477
	movl	12616(%r15), %eax
	leaq	-224(%rbp), %r12
	movq	%r9, %rdx
	movq	%r15, %rsi
	movl	$6, 12616(%r15)
	movq	%r12, %rdi
	movl	%eax, -256(%rbp)
	call	_ZN2v88internal21ExternalCallbackScopeC1EPNS0_7IsolateEm
	leaq	-120(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	41016(%r15), %rdi
	movq	%rdi, -264(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-264(%rbp), %rdi
	movq	-248(%rbp), %r8
	testb	%al, %al
	jne	.L515
.L478:
	movq	%r8, %rdi
	leaq	-232(%rbp), %rdx
	movq	%r13, %rsi
	call	*%r14
	movq	-88(%rbp), %rax
	cmpq	%rax, 96(%r15)
	movq	%r12, %rdi
	setne	%dl
	movb	%dl, -248(%rbp)
	call	_ZN2v88internal21ExternalCallbackScopeD1Ev
	movl	-256(%rbp), %eax
	movzbl	-248(%rbp), %edx
	movl	%eax, 12616(%r15)
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L513:
	movq	-112(%rbp), %rdx
	movl	%r15d, %ecx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj@PLT
	movq	-248(%rbp), %r8
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L511:
	leaq	-184(%rbp), %rsi
	movb	%dl, -248(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movzbl	-248(%rbp), %edx
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L509:
	movq	40960(%r14), %rax
	leaq	-184(%rbp), %rsi
	movl	$160, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L483:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L515:
	movq	(%r8), %rcx
	movq	-112(%rbp), %rdx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE@PLT
	movq	-248(%rbp), %r8
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L514:
	movq	40960(%r15), %rax
	leaq	-184(%rbp), %rsi
	movl	$175, %edx
	movq	%r9, -256(%rbp)
	movq	%r8, -248(%rbp)
	leaq	23240(%rax), %rdi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	-256(%rbp), %r9
	movq	-248(%rbp), %r8
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L485:
	xorl	%r9d, %r9d
	xorl	%r14d, %r14d
	jmp	.L475
.L512:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21441:
	.size	_ZN2v88internal12_GLOBAL__N_134SetPropertyWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEENS_5MaybeINS0_11ShouldThrowEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal12_GLOBAL__N_134SetPropertyWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEENS_5MaybeINS0_11ShouldThrowEEENS4_INS0_6ObjectEEE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_144GetPropertyAttributesWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"interceptor-indexed-query"
.LC19:
	.string	"interceptor-named-query"
.LC20:
	.string	"result->ToInt32(&value)"
.LC21:
	.string	"interceptor-indexed-getter"
.LC22:
	.string	"interceptor-named-getter"
	.section	.text._ZN2v88internal12_GLOBAL__N_144GetPropertyAttributesWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_144GetPropertyAttributesWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEE, @function
_ZN2v88internal12_GLOBAL__N_144GetPropertyAttributesWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEE:
.LFB21440:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%r12), %rax
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	48(%rdi), %rsi
	movq	%rax, -248(%rbp)
	movq	56(%rdi), %r15
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L517
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertReceiverEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L628
.L521:
	movq	0(%r13), %rax
	movq	(%rsi), %rcx
	leaq	-144(%rbp), %rdi
	movabsq	$4294967297, %r9
	movq	(%r15), %r8
	movq	%r12, %rsi
	movq	63(%rax), %rdx
	call	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movq	0(%r13), %rax
	movq	88(%r12), %rdx
	cmpq	23(%rax), %rdx
	je	.L523
	movl	72(%rbx), %r9d
	movq	-104(%rbp), %r15
	cmpl	$-1, %r9d
	je	.L524
	movq	$0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %edx
	testl	%edx, %edx
	jne	.L629
.L525:
	movq	23(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L566
	movq	7(%rax), %r10
	movq	%r10, %rbx
.L526:
	cmpl	$32, 41828(%r15)
	je	.L630
.L527:
	movl	12616(%r15), %eax
	leaq	-224(%rbp), %r13
	movq	%r10, %rdx
	movq	%r15, %rsi
	movl	$6, 12616(%r15)
	movq	%r13, %rdi
	movl	%r9d, -272(%rbp)
	movl	%eax, -256(%rbp)
	call	_ZN2v88internal21ExternalCallbackScopeC1EPNS0_7IsolateEm
	leaq	-120(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	41016(%r15), %rdi
	movq	%rdi, -264(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-264(%rbp), %rdi
	movl	-272(%rbp), %r9d
	testb	%al, %al
	jne	.L631
.L529:
	leaq	-232(%rbp), %rsi
	movl	%r9d, %edi
	call	*%rbx
	movq	96(%r15), %rax
	xorl	%ebx, %ebx
	cmpq	%rax, -88(%rbp)
	je	.L530
	leaq	-88(%rbp), %rbx
.L530:
	movq	%r13, %rdi
	call	_ZN2v88internal21ExternalCallbackScopeD1Ev
	movl	-256(%rbp), %eax
	movl	%eax, 12616(%r15)
.L536:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L632
.L532:
	testq	%rbx, %rbx
	je	.L540
	movq	(%rbx), %rax
	leaq	-224(%rbp), %rsi
	leaq	-192(%rbp), %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L633
	movl	-224(%rbp), %r13d
	salq	$32, %r13
	orq	$1, %r13
.L542:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
.L522:
	subl	$1, 41104(%r12)
	movq	-248(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L564
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L564:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L634
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore_state
	cmpq	7(%rax), %rdx
	jne	.L635
.L540:
	movq	12552(%r12), %rax
	movabsq	$274877906945, %r13
	cmpq	%rax, 96(%r12)
	je	.L542
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L517:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	ja	.L521
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertReceiverEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L521
	.p2align 4,,10
	.p2align 3
.L628:
	xorl	%r13d, %r13d
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L635:
	movl	72(%rbx), %eax
	movl	%eax, %ecx
	movl	%eax, -256(%rbp)
	movq	-104(%rbp), %rax
	cmpl	$-1, %ecx
	je	.L543
	movq	$0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %edx
	testl	%edx, %edx
	jne	.L636
.L544:
	movq	41016(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L637
.L545:
	movq	0(%r13), %rax
	movq	7(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L570
	movq	7(%rax), %r9
	movq	%r9, %rbx
.L546:
	movq	-104(%rbp), %r8
	cmpl	$32, 41828(%r8)
	je	.L547
.L550:
	movl	12616(%r8), %r15d
	leaq	-224(%rbp), %r13
	movq	%r8, %rsi
	movq	%r9, %rdx
	movl	$6, 12616(%r8)
	movq	%r13, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal21ExternalCallbackScopeC1EPNS0_7IsolateEm
	leaq	-120(%rbp), %rax
	movl	-256(%rbp), %edi
	leaq	-232(%rbp), %rsi
	movq	%rax, -232(%rbp)
	call	*%rbx
.L626:
	movq	-264(%rbp), %r8
	xorl	%ebx, %ebx
	movq	96(%r8), %rax
	cmpq	%rax, -88(%rbp)
	je	.L558
	leaq	-88(%rbp), %rbx
.L558:
	movq	%r13, %rdi
	movq	%r8, -256(%rbp)
	call	_ZN2v88internal21ExternalCallbackScopeD1Ev
	movq	-256(%rbp), %r8
	movl	%r15d, 12616(%r8)
.L561:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L638
.L553:
	movabsq	$8589934593, %r13
	testq	%rbx, %rbx
	jne	.L542
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L524:
	pxor	%xmm0, %xmm0
	movq	32(%rbx), %rbx
	movq	$0, -160(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %edx
	testl	%edx, %edx
	jne	.L639
.L533:
	movq	23(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L568
	movq	7(%rax), %r9
	movq	%r9, -256(%rbp)
.L534:
	cmpl	$32, 41828(%r15)
	je	.L640
.L535:
	movl	12616(%r15), %eax
	leaq	-224(%rbp), %r13
	movq	%r9, %rdx
	movq	%r15, %rsi
	movl	$6, 12616(%r15)
	movq	%r13, %rdi
	movl	%eax, -264(%rbp)
	call	_ZN2v88internal21ExternalCallbackScopeC1EPNS0_7IsolateEm
	leaq	-120(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	41016(%r15), %rdi
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-272(%rbp), %rdi
	testb	%al, %al
	jne	.L641
.L537:
	movq	-256(%rbp), %rax
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	leaq	-232(%rbp), %rsi
	call	*%rax
	movq	96(%r15), %rax
	cmpq	%rax, -88(%rbp)
	je	.L538
	leaq	-88(%rbp), %rbx
.L538:
	movq	%r13, %rdi
	call	_ZN2v88internal21ExternalCallbackScopeD1Ev
	movl	-264(%rbp), %eax
	movl	%eax, 12616(%r15)
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L630:
	movq	41472(%r15), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r10, -264(%rbp)
	movl	%r9d, -256(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movl	-256(%rbp), %r9d
	movq	-264(%rbp), %r10
	testb	%al, %al
	jne	.L527
	xorl	%ebx, %ebx
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L631:
	movq	-112(%rbp), %rdx
	movl	%r9d, %ecx
	leaq	.LC18(%rip), %rsi
	movl	%r9d, -264(%rbp)
	call	_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj@PLT
	movl	-264(%rbp), %r9d
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L640:
	movq	41472(%r15), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r9, -264(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-264(%rbp), %r9
	testb	%al, %al
	jne	.L535
	xorl	%ebx, %ebx
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L543:
	pxor	%xmm0, %xmm0
	movq	32(%rbx), %rbx
	movq	$0, -160(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %edx
	testl	%edx, %edx
	jne	.L642
.L554:
	movq	41016(%rax), %rdi
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-256(%rbp), %rdi
	testb	%al, %al
	jne	.L643
.L555:
	movq	0(%r13), %rax
	movq	7(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L572
	movq	7(%rax), %r9
	movq	%r9, -256(%rbp)
.L556:
	movq	-104(%rbp), %r8
	cmpl	$32, 41828(%r8)
	je	.L557
.L560:
	movl	12616(%r8), %r15d
	leaq	-224(%rbp), %r13
	movq	%r8, %rsi
	movq	%r9, %rdx
	movl	$6, 12616(%r8)
	movq	%r13, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal21ExternalCallbackScopeC1EPNS0_7IsolateEm
	leaq	-120(%rbp), %rax
	leaq	-232(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, -232(%rbp)
	movq	-256(%rbp), %rax
	call	*%rax
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L632:
	leaq	-184(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L641:
	movq	(%rbx), %rcx
	movq	-112(%rbp), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE@PLT
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L557:
	movq	41472(%r8), %rdi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r9, -272(%rbp)
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-264(%rbp), %r8
	movq	-272(%rbp), %r9
	testb	%al, %al
	jne	.L560
	xorl	%ebx, %ebx
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L643:
	movq	(%rbx), %rcx
	movq	-112(%rbp), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE@PLT
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L547:
	movq	41472(%r8), %rdi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r9, -272(%rbp)
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-264(%rbp), %r8
	movq	-272(%rbp), %r9
	testb	%al, %al
	jne	.L550
	xorl	%ebx, %ebx
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L637:
	movl	-256(%rbp), %ecx
	movq	-112(%rbp), %rdx
	leaq	.LC21(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj@PLT
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L629:
	movq	40960(%r15), %rax
	leaq	-184(%rbp), %rsi
	movl	$159, %edx
	movl	%r9d, -256(%rbp)
	leaq	23240(%rax), %rdi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	0(%r13), %rax
	movl	-256(%rbp), %r9d
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L566:
	xorl	%r10d, %r10d
	xorl	%ebx, %ebx
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L633:
	leaq	.LC20(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L639:
	movq	40960(%r15), %rax
	leaq	-184(%rbp), %rsi
	movl	$174, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	0(%r13), %rax
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L638:
	leaq	-184(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L568:
	movq	$0, -256(%rbp)
	xorl	%r9d, %r9d
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L636:
	movq	40960(%rax), %rcx
	leaq	-184(%rbp), %rsi
	movl	$173, %edx
	movq	%rax, -264(%rbp)
	leaq	23240(%rcx), %rdi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	-264(%rbp), %rax
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L570:
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
	jmp	.L546
.L642:
	movq	40960(%rax), %rcx
	leaq	-184(%rbp), %rsi
	movl	$173, %edx
	movq	%rax, -256(%rbp)
	leaq	23240(%rcx), %rdi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	-256(%rbp), %rax
	jmp	.L554
.L572:
	movq	$0, -256(%rbp)
	xorl	%r9d, %r9d
	jmp	.L556
.L634:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21440:
	.size	_ZN2v88internal12_GLOBAL__N_144GetPropertyAttributesWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEE, .-_ZN2v88internal12_GLOBAL__N_144GetPropertyAttributesWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_134GetPropertyWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEEPb,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_134GetPropertyWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEEPb, @function
_ZN2v88internal12_GLOBAL__N_134GetPropertyWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEEPb:
.LFB21439:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, (%rdx)
	movq	(%rsi), %rax
	movq	24(%rdi), %r12
	movq	7(%rax), %rcx
	cmpq	%rcx, 88(%r12)
	je	.L722
	movq	%rsi, %r13
	movq	48(%rdi), %rsi
	movq	%rdx, %r14
	movq	56(%rdi), %r15
	movq	%rdi, %rbx
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L647
.L650:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertReceiverEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L648
	movq	0(%r13), %rax
.L651:
	movq	(%rsi), %rcx
	movq	63(%rax), %rdx
	movq	%r12, %rsi
	leaq	-144(%rbp), %rdi
	movabsq	$4294967297, %r9
	movq	(%r15), %r8
	call	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movl	72(%rbx), %eax
	movl	%eax, -264(%rbp)
	cmpl	$-1, %eax
	je	.L652
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %rbx
	movq	$0, -176(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L723
.L653:
	movq	41016(%rbx), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L724
.L654:
	movq	0(%r13), %rax
	movq	7(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L685
	movq	7(%rax), %r8
	movq	%r8, %rbx
.L655:
	movq	-104(%rbp), %r15
	cmpl	$32, 41828(%r15)
	je	.L656
.L659:
	movl	12616(%r15), %eax
	leaq	-240(%rbp), %r13
	movl	$6, 12616(%r15)
	movq	%r15, -240(%rbp)
	movq	%r8, -232(%rbp)
	movl	%eax, -272(%rbp)
	movq	12608(%r15), %rax
	movq	%rax, -224(%rbp)
	movq	%r13, 12608(%r15)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	testq	%rdx, %rdx
	je	.L657
.L658:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L725
.L662:
	leaq	-120(%rbp), %rax
	movl	-264(%rbp), %edi
	leaq	-248(%rbp), %rsi
	movq	%rax, -248(%rbp)
	call	*%rbx
	movq	96(%r15), %rax
	xorl	%ebx, %ebx
	cmpq	%rax, -88(%rbp)
	je	.L673
.L717:
	leaq	-88(%rbp), %rbx
.L673:
	movq	%r13, %rdi
	call	_ZN2v88internal21ExternalCallbackScopeD1Ev
	movl	-272(%rbp), %eax
	movl	%eax, 12616(%r15)
.L676:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L726
.L668:
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L678
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
.L679:
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rcx
	movq	%rcx, 41704(%rdx)
.L646:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L727
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L647:
	.cfi_restore_state
	movq	-1(%rdx), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L650
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L722:
	leaq	88(%r12), %rax
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L678:
	leaq	88(%r12), %rax
	testq	%rbx, %rbx
	je	.L679
	movb	$1, (%r14)
	movq	41112(%r12), %rdi
	movq	(%rbx), %rsi
	testq	%rdi, %rdi
	je	.L681
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L648:
	xorl	%eax, %eax
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L681:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L728
.L683:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L652:
	movq	32(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -176(%rbp)
	movq	-104(%rbp), %rbx
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -264(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L729
.L669:
	movq	41016(%rbx), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L730
.L670:
	movq	0(%r13), %rax
	movq	7(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L688
	movq	7(%rax), %r9
	movq	%r9, %rbx
.L671:
	movq	-104(%rbp), %r15
	cmpl	$32, 41828(%r15)
	je	.L672
.L675:
	movl	12616(%r15), %eax
	leaq	-240(%rbp), %r13
	movq	%r9, %rdx
	movq	%r15, %rsi
	movl	$6, 12616(%r15)
	movq	%r13, %rdi
	movl	%eax, -272(%rbp)
	call	_ZN2v88internal21ExternalCallbackScopeC1EPNS0_7IsolateEm
	leaq	-120(%rbp), %rax
	movq	-264(%rbp), %rdi
	leaq	-248(%rbp), %rsi
	movq	%rax, -248(%rbp)
	call	*%rbx
	movq	96(%r15), %rax
	xorl	%ebx, %ebx
	cmpq	%rax, -88(%rbp)
	jne	.L717
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L656:
	movq	41472(%r15), %rdi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-272(%rbp), %r8
	testb	%al, %al
	jne	.L659
	xorl	%ebx, %ebx
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L724:
	movl	-264(%rbp), %ecx
	movq	-112(%rbp), %rdx
	leaq	.LC21(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj@PLT
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L725:
	pxor	%xmm0, %xmm0
	movq	%rdx, -280(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	-280(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L731
.L663:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L664
	movq	(%rdi), %rax
	call	*8(%rax)
.L664:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L662
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L657:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L732
.L661:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L672:
	movq	41472(%r15), %rdi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r9, -272(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-272(%rbp), %r9
	testb	%al, %al
	jne	.L675
	xorl	%ebx, %ebx
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L730:
	movq	-264(%rbp), %rax
	movq	-112(%rbp), %rdx
	leaq	.LC22(%rip), %rsi
	movq	%r15, %rdi
	movq	(%rax), %rcx
	call	_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE@PLT
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L726:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L723:
	movq	40960(%rbx), %rax
	leaq	-200(%rbp), %rsi
	movl	$173, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L685:
	xorl	%r8d, %r8d
	xorl	%ebx, %ebx
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L731:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-160(%rbp), %rcx
	pushq	$0
	pushq	%rcx
	leaq	.LC15(%rip), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L732:
	leaq	.LC14(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L729:
	movq	40960(%rbx), %rax
	leaq	-200(%rbp), %rsi
	movl	$173, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L688:
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L728:
	movq	%r12, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	jmp	.L683
.L727:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21439:
	.size	_ZN2v88internal12_GLOBAL__N_134GetPropertyWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEEPb, .-_ZN2v88internal12_GLOBAL__N_134GetPropertyWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEEPb
	.section	.text._ZN2v88internal12StringStream3AddEPKc,"axG",@progbits,_ZN2v88internal12StringStream3AddEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringStream3AddEPKc
	.type	_ZN2v88internal12StringStream3AddEPKc, @function
_ZN2v88internal12StringStream3AddEPKc:
.LFB21356:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	.cfi_endproc
.LFE21356:
	.size	_ZN2v88internal12StringStream3AddEPKc, .-_ZN2v88internal12StringStream3AddEPKc
	.section	.text._ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE
	.type	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE, @function
_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE:
.LFB21396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	4(%rdi), %eax
	cmpl	$4, %eax
	je	.L752
	leaq	.L740(%rip), %rbx
.L736:
	cmpl	$7, %eax
	ja	.L738
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE,"a",@progbits
	.align 4
	.align 4
.L740:
	.long	.L745-.L740
	.long	.L752-.L740
	.long	.L739-.L740
	.long	.L751-.L740
	.long	.L738-.L740
	.long	.L742-.L740
	.long	.L741-.L740
	.long	.L739-.L740
	.section	.text._ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE
	.p2align 4,,10
	.p2align 3
.L742:
	movb	$0, 8(%r12)
	movl	$4, 4(%r12)
.L752:
	movq	24(%r12), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	$88, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L745:
	.cfi_restore_state
	movq	24(%r12), %rax
	cmpq	$0, 12464(%rax)
	je	.L746
	movq	%r12, %rdi
	call	_ZNK2v88internal14LookupIterator9HasAccessEv@PLT
	testb	%al, %al
	jne	.L738
.L751:
	movq	24(%r12), %rax
.L746:
	popq	%rbx
	movb	$0, 8(%r12)
	addq	$88, %rax
	movl	$4, 4(%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L741:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v88internal14LookupIterator12GetDataValueEv@PLT
	.p2align 4,,10
	.p2align 3
.L738:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	4(%r12), %eax
	cmpl	$4, %eax
	jne	.L736
	jmp	.L752
.L739:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21396:
	.size	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE, .-_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE
	.section	.rodata._ZN2v88internal10JSReceiver19HasInPrototypeChainEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC23:
	.string	"!handle_.is_null()"
	.section	.text._ZN2v88internal10JSReceiver19HasInPrototypeChainEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver19HasInPrototypeChainEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal10JSReceiver19HasInPrototypeChainEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE, @function
_ZN2v88internal10JSReceiver19HasInPrototypeChainEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE:
.LFB21397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L754
	movl	$0, -68(%rbp)
	movq	(%rsi), %rbx
	movq	%rdi, %r14
	movq	%rdx, %r13
	movq	%rsi, %r12
	leaq	-64(%rbp), %r15
.L780:
	movq	-1(%rbx), %rax
	leaq	-1(%rbx), %rdx
	cmpw	$1026, 11(%rax)
	je	.L786
	movq	-1(%rbx), %rax
	movzbl	13(%rax), %eax
	shrb	$5, %al
	andl	$1, %eax
.L759:
	testb	%al, %al
	je	.L760
	movq	41112(%r14), %rdi
	movq	12464(%r14), %r9
	testq	%rdi, %rdi
	je	.L761
	movq	%r9, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L762:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	jne	.L760
.L771:
	movl	$1, %eax
.L764:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L787
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L760:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jne	.L775
	addl	$1, -68(%rbp)
	movl	-68(%rbp), %eax
	cmpl	$102400, %eax
	jg	.L788
	movq	%r12, %rdi
	call	_ZN2v88internal7JSProxy12GetPrototypeENS0_6HandleIS1_EE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L769
	movq	(%rax), %rax
	cmpq	%rax, 104(%r14)
	sete	%al
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L786:
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	andq	$-262144, %rcx
	movq	24(%rcx), %rax
	movq	%rcx, -80(%rbp)
	movq	-25128(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	-80(%rbp), %rcx
	andl	$1, %ebx
	movq	-88(%rbp), %rdx
	movq	24(%rcx), %rcx
	jne	.L789
.L757:
	movq	(%rdx), %rdx
	movq	23(%rdx), %rdx
.L758:
	cmpq	%rax, %rdx
	setne	%al
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L775:
	movq	-1(%rax), %rax
	movq	41112(%r14), %rdi
	movq	104(%r14), %rdx
	movq	23(%rax), %rbx
	testq	%rdi, %rdi
	je	.L790
	movq	%rbx, %rsi
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-80(%rbp), %rdx
	movq	%rax, %r12
.L765:
	cmpq	%rdx, %rbx
	sete	%al
.L767:
	testb	%al, %al
	jne	.L771
	cmpq	%r12, %r13
	je	.L772
	movq	(%r12), %rbx
	testq	%r12, %r12
	je	.L780
	testq	%r13, %r13
	je	.L780
	cmpq	%rbx, 0(%r13)
	jne	.L780
	.p2align 4,,10
	.p2align 3
.L772:
	movl	$257, %eax
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L761:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L791
.L763:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r9, (%rsi)
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L788:
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
.L769:
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L790:
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L792
.L766:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rbx, (%r12)
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L791:
	movq	%r14, %rdi
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L789:
	movq	(%rdx), %rsi
	cmpw	$1024, 11(%rsi)
	jne	.L757
	movq	-37488(%rcx), %rdx
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L754:
	leaq	.LC23(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L792:
	movq	%r14, %rdi
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L766
.L787:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21397:
	.size	_ZN2v88internal10JSReceiver19HasInPrototypeChainEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE, .-_ZN2v88internal10JSReceiver19HasInPrototypeChainEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal10JSReceiver10class_nameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver10class_nameEv
	.type	_ZN2v88internal10JSReceiver10class_nameEv, @function
_ZN2v88internal10JSReceiver10class_nameEv:
.LFB21401:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-1(%rdx), %rcx
	subq	$37592, %rax
	cmpw	$1103, 11(%rcx)
	jbe	.L794
.L857:
	movq	2560(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L794:
	movq	-1(%rdx), %rcx
	cmpw	$1058, 11(%rcx)
	jne	.L796
	movq	2032(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L796:
	movq	-1(%rdx), %rcx
	cmpw	$1061, 11(%rcx)
	jne	.L797
	movq	2056(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L797:
	movq	-1(%rdx), %rcx
	cmpw	$1059, 11(%rcx)
	je	.L858
	movq	-1(%rdx), %rcx
	cmpw	$1060, 11(%rcx)
	jne	.L800
	movq	2080(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L800:
	movq	-1(%rdx), %rcx
	cmpw	$1066, 11(%rcx)
	jne	.L801
	movq	2312(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L858:
	movl	39(%rdx), %edx
	andl	$8, %edx
	je	.L799
	movq	3296(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L799:
	movq	2072(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L801:
	movq	-1(%rdx), %rcx
	cmpw	$1067, 11(%rcx)
	jne	.L802
	movq	2480(%rax), %rax
	ret
.L802:
	movq	-1(%rdx), %rcx
	cmpw	$1068, 11(%rcx)
	je	.L803
	movq	-1(%rdx), %rcx
	cmpw	$1063, 11(%rcx)
	je	.L803
	movq	-1(%rdx), %rcx
	cmpw	$1064, 11(%rcx)
	je	.L803
	movq	-1(%rdx), %rcx
	cmpw	$1069, 11(%rcx)
	jne	.L859
	movq	2808(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L803:
	movq	2592(%rax), %rax
	ret
.L859:
	movq	-1(%rdx), %rcx
	movzwl	11(%rcx), %ecx
	subw	$1070, %cx
	cmpw	$2, %cx
	ja	.L806
	movq	2816(%rax), %rax
	ret
.L806:
	movq	-1(%rdx), %rcx
	cmpw	$1024, 11(%rcx)
	je	.L860
	movq	-1(%rdx), %rcx
	cmpw	$1075, 11(%rcx)
	jne	.L809
	movq	3176(%rax), %rax
	ret
.L860:
	movq	-1(%rdx), %rdx
	testb	$2, 13(%rdx)
	jne	.L857
.L845:
	movq	3000(%rax), %rax
	ret
.L809:
	movq	-1(%rdx), %rcx
	cmpw	$1077, 11(%rcx)
	jne	.L810
	movq	3248(%rax), %rax
	ret
.L810:
	movq	-1(%rdx), %rcx
	movzwl	11(%rcx), %ecx
	subw	$1078, %cx
	cmpw	$1, %cx
	ja	.L811
	movq	3280(%rax), %rax
	ret
.L811:
	movq	-1(%rdx), %rcx
	cmpw	$1086, 11(%rcx)
	je	.L861
.L812:
	movq	-1(%rdx), %rcx
	cmpw	$1041, 11(%rcx)
	jne	.L823
	movq	23(%rdx), %rdx
	testb	$1, %dl
	jne	.L862
.L824:
	movq	2976(%rax), %rax
	ret
.L861:
	movq	-1(%rdx), %rcx
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	cmpl	$17, %ecx
	jne	.L813
	movq	3496(%rax), %rax
	ret
.L862:
	movq	-1(%rdx), %rcx
	cmpw	$67, 11(%rcx)
	je	.L825
.L828:
	movq	-1(%rdx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L863
	movq	3344(%rax), %rax
	ret
.L823:
	movq	-1(%rdx), %rcx
	cmpw	$1084, 11(%rcx)
	jne	.L834
	movq	3560(%rax), %rax
	ret
.L813:
	movq	-1(%rdx), %rcx
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	cmpl	$18, %ecx
	jne	.L814
	movq	2736(%rax), %rax
	ret
.L834:
	movq	-1(%rdx), %rcx
	cmpw	$1085, 11(%rcx)
	jne	.L835
	movq	3576(%rax), %rax
	ret
.L814:
	movq	-1(%rdx), %rcx
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	cmpl	$19, %ecx
	jne	.L815
	movq	3480(%rax), %rax
	ret
.L835:
	movq	-1(%rdx), %rcx
	cmpw	$1026, 11(%rcx)
	jne	.L836
	movq	2632(%rax), %rax
	ret
.L815:
	movq	-1(%rdx), %rcx
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	cmpl	$20, %ecx
	jne	.L816
	movq	2720(%rax), %rax
	ret
.L836:
	movq	-1(%rdx), %rdx
.L856:
	movq	31(%rdx), %rdx
	testb	$1, %dl
	je	.L845
	movq	-1(%rdx), %rsi
	leaq	-1(%rdx), %rcx
	cmpw	$68, 11(%rsi)
	je	.L856
	movq	(%rcx), %rcx
	cmpw	$1105, 11(%rcx)
	je	.L864
.L847:
	movq	-1(%rdx), %rcx
	cmpw	$88, 11(%rcx)
	jne	.L845
	movq	55(%rdx), %rdx
	testb	$1, %dl
	je	.L845
	movq	-1(%rdx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L845
	movq	%rdx, %rax
	ret
.L816:
	movq	-1(%rdx), %rcx
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	cmpl	$21, %ecx
	jne	.L817
	movq	3488(%rax), %rax
	ret
.L864:
	movq	23(%rdx), %rcx
	movq	7(%rcx), %rcx
	testb	$1, %cl
	je	.L847
	movq	-1(%rcx), %rcx
	cmpw	$88, 11(%rcx)
	jne	.L847
	movq	23(%rdx), %rdx
	movq	7(%rdx), %rdx
	testb	$1, %dl
	jne	.L847
	jmp	.L845
.L817:
	movq	-1(%rdx), %rcx
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	cmpl	$22, %ecx
	jne	.L818
	movq	2728(%rax), %rax
	ret
.L863:
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	je	.L824
	movq	-1(%rdx), %rcx
	cmpw	$66, 11(%rcx)
	je	.L829
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	je	.L865
	movq	-1(%rdx), %rdx
	cmpw	$96, 11(%rdx)
	je	.L866
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
.L825:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	testb	$-2, 43(%rdx)
	jne	.L828
	movq	2160(%rax), %rax
	ret
.L818:
	movq	-1(%rdx), %rcx
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	cmpl	$23, %ecx
	jne	.L819
	movq	2536(%rax), %rax
	ret
.L866:
	movq	3224(%rax), %rax
	ret
.L819:
	movq	-1(%rdx), %rcx
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	cmpl	$24, %ecx
	jne	.L820
	movq	2544(%rax), %rax
	ret
.L865:
	movq	3376(%rax), %rax
	ret
.L829:
	movq	2120(%rax), %rax
	ret
.L820:
	movq	-1(%rdx), %rcx
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	cmpl	$25, %ecx
	jne	.L821
	movq	3504(%rax), %rax
	ret
.L821:
	movq	-1(%rdx), %rcx
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	cmpl	$26, %ecx
	jne	.L822
	movq	2144(%rax), %rax
	ret
.L822:
	movq	-1(%rdx), %rcx
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	cmpl	$27, %ecx
	jne	.L812
	movq	2136(%rax), %rax
	ret
	.cfi_endproc
.LFE21401:
	.size	_ZN2v88internal10JSReceiver10class_nameEv, .-_ZN2v88internal10JSReceiver10class_nameEv
	.section	.text._ZN2v88internal12_GLOBAL__N_120GetConstructorHelperENS0_6HandleINS0_10JSReceiverEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120GetConstructorHelperENS0_6HandleINS0_10JSReceiverEEE, @function
_ZN2v88internal12_GLOBAL__N_120GetConstructorHelperENS0_6HandleINS0_10JSReceiverEEE:
.LFB21402:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-128(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$176, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	-1(%rax), %rdx
	subq	$37592, %rbx
	cmpw	$1024, 11(%rdx)
	jne	.L949
.L872:
	movq	(%r12), %rdx
	movq	3952(%rbx), %rax
	leaq	3952(%rbx), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	movq	-1(%rax), %rcx
	movl	$2, %edx
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L890
	xorl	%edx, %edx
	testb	$1, 11(%rax)
	sete	%dl
	addl	%edx, %edx
.L890:
	movabsq	$824633720832, %rax
	movl	%edx, -128(%rbp)
	movq	%rax, -116(%rbp)
	movq	%rdi, -104(%rbp)
	movq	3952(%rbx), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L950
.L891:
	movq	%r13, %rdi
	movq	%rsi, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r12, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%r12, -64(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -124(%rbp)
	jne	.L892
	movq	-104(%rbp), %rax
	movq	88(%rax), %rdx
	addq	$88, %rax
	testb	$1, %dl
	jne	.L951
.L894:
	leaq	-176(%rbp), %rdi
	movq	%rbx, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%r12, -160(%rbp)
	movl	$0, -152(%rbp)
	movb	$0, -148(%rbp)
	movl	$0, -144(%rbp)
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	cmpb	$0, -148(%rbp)
	je	.L952
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	%rax, -128(%rbp)
.L948:
	call	_ZN2v88internal10JSReceiver10class_nameEv
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L918
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L919:
	xorl	%r8d, %r8d
	movq	%rax, %rdx
.L885:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L953
	addq	$176, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L952:
	.cfi_restore_state
	movq	-160(%rbp), %r14
	movq	2304(%rbx), %rax
	leaq	2304(%rbx), %rsi
	movq	(%r14), %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	movq	-1(%rax), %rcx
	movl	$2, %edx
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L901
	xorl	%edx, %edx
	testb	$1, 11(%rax)
	sete	%dl
	addl	%edx, %edx
.L901:
	movabsq	$824633720832, %rax
	movl	%edx, -128(%rbp)
	movq	%rax, -116(%rbp)
	movq	%rdi, -104(%rbp)
	movq	2304(%rbx), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L954
.L902:
	movq	%r13, %rdi
	movq	%rsi, -96(%rbp)
	movq	%r14, -64(%rbp)
	leaq	-184(%rbp), %r14
	movq	$0, -88(%rbp)
	movq	%r12, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE
	movq	(%rax), %r13
	testb	$1, %r13b
	jne	.L955
.L905:
	movq	(%r12), %rax
	movq	%r14, %rdi
	movq	%rax, -184(%rbp)
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L949:
	movq	-1(%rax), %rdx
	testb	$1, 14(%rdx)
	je	.L872
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$1048576, %edx
	jne	.L872
	movq	-1(%rax), %rax
	movq	31(%rax), %r14
	testb	$1, %r14b
	je	.L872
.L871:
	movq	-1(%r14), %rdx
	leaq	-1(%r14), %rax
	cmpw	$68, 11(%rdx)
	je	.L956
	movq	(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L957
	movq	(%rax), %rax
	cmpw	$88, 11(%rax)
	je	.L921
.L943:
	leaq	-128(%rbp), %r13
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L892:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE
	movq	(%rax), %rdx
	testb	$1, %dl
	je	.L894
.L951:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L919
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L918:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L958
.L920:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L950:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L955:
	movq	-1(%r13), %rax
	cmpw	$1105, 11(%rax)
	jne	.L905
	movq	23(%r13), %rax
	movq	%r14, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	%rax, -192(%rbp)
	movl	11(%rax), %edx
	testl	%edx, %edx
	je	.L905
	movq	3000(%rbx), %rsi
	cmpq	%rax, %rsi
	je	.L905
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L909
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L910
.L909:
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal6String10SlowEqualsES1_@PLT
	testb	%al, %al
	jne	.L905
.L910:
	movq	41112(%rbx), %rdi
	movq	-192(%rbp), %rsi
	testq	%rdi, %rdi
	je	.L912
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L913:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L915
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L916:
	movq	%r12, %rdx
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L954:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L958:
	movq	%rbx, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L920
.L956:
	movq	31(%r14), %r14
	testb	$1, %r14b
	je	.L943
	jmp	.L871
.L957:
	movq	23(%r14), %rax
	leaq	-128(%rbp), %r13
	movq	%r13, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	%rax, -176(%rbp)
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	je	.L872
	movq	3000(%rbx), %rsi
	cmpq	%rax, %rsi
	je	.L872
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L876
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L877
.L876:
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal6String10SlowEqualsES1_@PLT
	testb	%al, %al
	jne	.L872
.L877:
	movq	41112(%rbx), %rdi
	movq	-176(%rbp), %r13
	testq	%rdi, %rdi
	je	.L879
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L880:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L882
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L921:
	movq	55(%r14), %r14
	leaq	-128(%rbp), %r13
	testb	$1, %r14b
	je	.L872
	movq	-1(%r14), %rax
	cmpw	$63, 11(%rax)
	ja	.L872
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L887
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L919
.L915:
	movq	41088(%rbx), %r8
	cmpq	41096(%rbx), %r8
	je	.L959
.L917:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%rbx)
	movq	%r13, (%r8)
	jmp	.L916
.L912:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L960
.L914:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L913
.L959:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r8
	jmp	.L917
.L960:
	movq	%rbx, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L914
.L882:
	movq	41088(%rbx), %r8
	cmpq	41096(%rbx), %r8
	je	.L961
.L884:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%rbx)
	movq	%r14, (%r8)
	jmp	.L916
.L879:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L962
.L881:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%r13, (%r12)
	jmp	.L880
.L953:
	call	__stack_chk_fail@PLT
.L887:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L963
.L889:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r14, (%rax)
	jmp	.L919
.L961:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r8
	jmp	.L884
.L962:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r12
	jmp	.L881
.L963:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L889
	.cfi_endproc
.LFE21402:
	.size	_ZN2v88internal12_GLOBAL__N_120GetConstructorHelperENS0_6HandleINS0_10JSReceiverEEE, .-_ZN2v88internal12_GLOBAL__N_120GetConstructorHelperENS0_6HandleINS0_10JSReceiverEEE
	.section	.text._ZN2v88internal10JSReceiver14GetConstructorENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver14GetConstructorENS0_6HandleIS1_EE
	.type	_ZN2v88internal10JSReceiver14GetConstructorENS0_6HandleIS1_EE, @function
_ZN2v88internal10JSReceiver14GetConstructorENS0_6HandleIS1_EE:
.LFB21414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal12_GLOBAL__N_120GetConstructorHelperENS0_6HandleINS0_10JSReceiverEEE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21414:
	.size	_ZN2v88internal10JSReceiver14GetConstructorENS0_6HandleIS1_EE, .-_ZN2v88internal10JSReceiver14GetConstructorENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal10JSReceiver18GetConstructorNameENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver18GetConstructorNameENS0_6HandleIS1_EE
	.type	_ZN2v88internal10JSReceiver18GetConstructorNameENS0_6HandleIS1_EE, @function
_ZN2v88internal10JSReceiver18GetConstructorNameENS0_6HandleIS1_EE:
.LFB21415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal12_GLOBAL__N_120GetConstructorHelperENS0_6HandleINS0_10JSReceiverEEE
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rdx, %rax
	ret
	.cfi_endproc
.LFE21415:
	.size	_ZN2v88internal10JSReceiver18GetConstructorNameENS0_6HandleIS1_EE, .-_ZN2v88internal10JSReceiver18GetConstructorNameENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal10JSReceiver18GetCreationContextEv.str1.1,"aMS",@progbits,1
.LC24:
	.string	"receiver.IsJSFunction()"
	.section	.text._ZN2v88internal10JSReceiver18GetCreationContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver18GetCreationContextEv
	.type	_ZN2v88internal10JSReceiver18GetCreationContextEv, @function
_ZN2v88internal10JSReceiver18GetCreationContextEv:
.LFB21416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-1(%rax), %rdx
.L986:
	movq	31(%rdx), %rdx
	testb	$1, %dl
	jne	.L987
.L980:
	movq	-1(%rax), %rdx
	cmpw	$1068, 11(%rdx)
	jne	.L988
.L973:
	movq	23(%rax), %rdx
.L975:
	movq	31(%rdx), %rcx
	movq	-1(%rcx), %rdx
	movzwl	11(%rdx), %edx
	subw	$138, %dx
	cmpw	$9, %dx
	jbe	.L989
.L976:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L988:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1063, 11(%rdx)
	je	.L973
	movq	-1(%rax), %rdx
	cmpw	$1064, 11(%rdx)
	je	.L973
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L974
	movq	%rax, %rdx
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L989:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	39(%rcx), %rsi
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L977
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L987:
	.cfi_restore_state
	movq	-1(%rdx), %rsi
	leaq	-1(%rdx), %rcx
	cmpw	$68, 11(%rsi)
	je	.L986
	movq	(%rcx), %rsi
	cmpw	$1105, 11(%rsi)
	je	.L975
	movq	(%rcx), %rdx
	cmpw	$88, 11(%rdx)
	jne	.L980
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L977:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L990
.L979:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L990:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L974:
	leaq	.LC24(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21416:
	.size	_ZN2v88internal10JSReceiver18GetCreationContextEv, .-_ZN2v88internal10JSReceiver18GetCreationContextEv
	.section	.text._ZN2v88internal10JSReceiver22GetContextForMicrotaskENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver22GetContextForMicrotaskENS0_6HandleIS1_EE
	.type	_ZN2v88internal10JSReceiver22GetContextForMicrotaskENS0_6HandleIS1_EE, @function
_ZN2v88internal10JSReceiver22GetContextForMicrotaskENS0_6HandleIS1_EE:
.LFB21418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	subq	$37592, %rbx
.L992:
	movq	-1(%rsi), %rdx
	movq	(%rax), %rax
	cmpw	$1104, 11(%rdx)
	je	.L996
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jne	.L1015
.L996:
	movq	-1(%rax), %rdx
	cmpw	$1104, 11(%rdx)
	je	.L1016
	movq	41112(%rbx), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1003
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L1017
.L997:
	xorl	%eax, %eax
.L1008:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1016:
	.cfi_restore_state
	movq	41112(%rbx), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L999
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L1003:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1018
.L1005:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	testb	$1, %sil
	je	.L997
.L1017:
	movq	-1(%rsi), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L997
	movq	(%rax), %rsi
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L999:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1019
.L1001:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1019:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1015:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L997
	movq	31(%rax), %rax
	movq	39(%rax), %r12
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1009
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L1008
.L1009:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1020
.L1011:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r12, (%rax)
	jmp	.L1008
.L1020:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1011
	.cfi_endproc
.LFE21418:
	.size	_ZN2v88internal10JSReceiver22GetContextForMicrotaskENS0_6HandleIS1_EE, .-_ZN2v88internal10JSReceiver22GetContextForMicrotaskENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal10JSReceiver15SetIdentityHashEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver15SetIdentityHashEi
	.type	_ZN2v88internal10JSReceiver15SetIdentityHashEi, @function
_ZN2v88internal10JSReceiver15SetIdentityHashEi:
.LFB21425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	7(%rax), %r12
	leaq	7(%rax), %rdx
	movq	%r12, %r13
	andq	$-262144, %r13
	movq	24(%r13), %rax
	cmpq	-37304(%rax), %r12
	je	.L1022
	cmpq	-36624(%rax), %r12
	je	.L1022
	cmpq	-36536(%rax), %r12
	je	.L1022
	movq	-1(%r12), %rax
	cmpw	$158, 11(%rax)
	je	.L1038
	salq	$32, %rsi
	movq	-1(%r12), %rax
	movq	%rsi, 47(%r12)
	movq	(%rdi), %rax
	leaq	7(%rax), %rdx
	jmp	.L1024
	.p2align 4,,10
	.p2align 3
.L1022:
	movq	%rsi, %r12
	salq	$32, %r12
	movq	%r12, %r13
.L1024:
	movq	%r12, (%rdx)
	testb	$1, %r12b
	je	.L1021
	movq	(%rbx), %rdi
	movq	8(%r13), %rax
	leaq	7(%rdi), %rsi
	testl	$262144, %eax
	jne	.L1039
	testb	$24, %al
	je	.L1021
.L1041:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1040
.L1021:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1039:
	.cfi_restore_state
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r13), %rax
	leaq	7(%rdi), %rsi
	testb	$24, %al
	jne	.L1041
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1040:
	addq	$8, %rsp
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1038:
	.cfi_restore_state
	movslq	11(%r12), %rax
	sall	$10, %esi
	andl	$-2147482625, %eax
	orl	%eax, %esi
	salq	$32, %rsi
	movq	%rsi, 7(%r12)
	movq	(%rdi), %rax
	leaq	7(%rax), %rdx
	jmp	.L1024
	.cfi_endproc
.LFE21425:
	.size	_ZN2v88internal10JSReceiver15SetIdentityHashEi, .-_ZN2v88internal10JSReceiver15SetIdentityHashEi
	.section	.text._ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE
	.type	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE, @function
_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE:
.LFB21426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	7(%rax), %rsi
	testb	$1, %sil
	jne	.L1043
.L1063:
	shrq	$32, %rsi
.L1044:
	testl	%esi, %esi
	je	.L1049
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_126SetHashAndUpdatePropertiesENS0_10HeapObjectEi
	movq	%rax, %r12
	movq	(%rbx), %rax
.L1049:
	movq	%r12, 7(%rax)
	testb	$1, %r12b
	je	.L1042
	movq	%r12, %r13
	movq	(%rbx), %rdi
	andq	$-262144, %r13
	movq	8(%r13), %rax
	leaq	7(%rdi), %rsi
	testl	$262144, %eax
	jne	.L1064
	testb	$24, %al
	je	.L1042
.L1066:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1065
.L1042:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1064:
	.cfi_restore_state
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r13), %rax
	leaq	7(%rdi), %rsi
	testb	$24, %al
	jne	.L1066
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1043:
	movq	-1(%rsi), %rdx
	cmpw	$158, 11(%rdx)
	je	.L1045
	movq	-1(%rsi), %rdx
	cmpw	$130, 11(%rdx)
	je	.L1048
	movq	-1(%rsi), %rdx
	cmpw	$131, 11(%rdx)
	jne	.L1049
.L1048:
	movq	47(%rsi), %rsi
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1065:
	addq	$8, %rsp
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1045:
	.cfi_restore_state
	movslq	11(%rsi), %rsi
	shrl	$10, %esi
	andl	$2097151, %esi
	jmp	.L1044
	.cfi_endproc
.LFE21426:
	.size	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE, .-_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE
	.section	.text._ZN2v88internal10JSReceiver15GetIdentityHashEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver15GetIdentityHashEv
	.type	_ZN2v88internal10JSReceiver15GetIdentityHashEv, @function
_ZN2v88internal10JSReceiver15GetIdentityHashEv:
.LFB21427:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L1068
.L1080:
	shrq	$32, %rax
.L1069:
	testl	%eax, %eax
	je	.L1075
	salq	$32, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1068:
	movq	-1(%rax), %rdx
	cmpw	$158, 11(%rdx)
	je	.L1070
	movq	-1(%rax), %rdx
	cmpw	$130, 11(%rdx)
	je	.L1073
	movq	-1(%rax), %rdx
	cmpw	$131, 11(%rdx)
	je	.L1073
.L1075:
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1073:
	movq	47(%rax), %rax
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1070:
	movslq	11(%rax), %rax
	shrl	$10, %eax
	andl	$2097151, %eax
	jmp	.L1069
	.cfi_endproc
.LFE21427:
	.size	_ZN2v88internal10JSReceiver15GetIdentityHashEv, .-_ZN2v88internal10JSReceiver15GetIdentityHashEv
	.section	.text._ZN2v88internal10JSReceiver18CreateIdentityHashEPNS0_7IsolateES1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver18CreateIdentityHashEPNS0_7IsolateES1_
	.type	_ZN2v88internal10JSReceiver18CreateIdentityHashEPNS0_7IsolateES1_, @function
_ZN2v88internal10JSReceiver18CreateIdentityHashEPNS0_7IsolateES1_:
.LFB21428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$2097151, %esi
	pushq	%rbx
	leaq	7(%r12), %r15
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal7Isolate20GenerateIdentityHashEj@PLT
	movq	7(%r12), %rdx
	movq	%rax, %r13
	movq	%rdx, %rbx
	salq	$32, %r13
	andq	$-262144, %rbx
	movq	24(%rbx), %rcx
	cmpq	-37304(%rcx), %rdx
	je	.L1083
	movq	%rax, %r13
	leaq	-37592(%rcx), %rsi
	salq	$32, %r13
	cmpq	968(%rsi), %rdx
	je	.L1083
	cmpq	1056(%rsi), %rdx
	je	.L1083
	movq	-1(%rdx), %rcx
	cmpw	$158, 11(%rcx)
	je	.L1099
	movq	-1(%rdx), %rax
	movq	%r13, %r14
	movq	%r13, 47(%rdx)
	movq	%rdx, %r13
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1083:
	movq	%r13, %rbx
	movq	%r13, %r14
.L1085:
	movq	%r13, 7(%r12)
	testb	$1, %r13b
	je	.L1091
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1100
	testb	$24, %al
	je	.L1091
.L1102:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1101
.L1091:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1100:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1102
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L1101:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L1099:
	movslq	11(%rdx), %rcx
	sall	$10, %eax
	movq	%r13, %r14
	movq	%rdx, %r13
	andl	$-2147482625, %ecx
	orl	%ecx, %eax
	salq	$32, %rax
	movq	%rax, 7(%rdx)
	jmp	.L1085
	.cfi_endproc
.LFE21428:
	.size	_ZN2v88internal10JSReceiver18CreateIdentityHashEPNS0_7IsolateES1_, .-_ZN2v88internal10JSReceiver18CreateIdentityHashEPNS0_7IsolateES1_
	.section	.text._ZN2v88internal10JSReceiver23GetOrCreateIdentityHashEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver23GetOrCreateIdentityHashEPNS0_7IsolateE
	.type	_ZN2v88internal10JSReceiver23GetOrCreateIdentityHashEPNS0_7IsolateE, @function
_ZN2v88internal10JSReceiver23GetOrCreateIdentityHashEPNS0_7IsolateE:
.LFB21429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%r8), %r12
	movq	7(%r12), %rax
	testb	$1, %al
	jne	.L1104
.L1127:
	shrq	$32, %rax
.L1105:
	testl	%eax, %eax
	je	.L1110
.L1128:
	addq	$8, %rsp
	salq	$32, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1104:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$158, 11(%rdx)
	je	.L1106
	movq	-1(%rax), %rdx
	cmpw	$130, 11(%rdx)
	je	.L1109
	movq	-1(%rax), %rdx
	cmpw	$131, 11(%rdx)
	je	.L1109
.L1110:
	movl	$2097151, %esi
	leaq	7(%r12), %r14
	call	_ZN2v88internal7Isolate20GenerateIdentityHashEj@PLT
	movq	7(%r12), %rdi
	movl	%eax, %esi
	movl	%eax, %ebx
	call	_ZN2v88internal12_GLOBAL__N_126SetHashAndUpdatePropertiesENS0_10HeapObjectEi
	movq	%rax, 7(%r12)
	movq	%rax, %r13
	testb	$1, %al
	je	.L1115
	movq	%rax, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L1129
	testb	$24, %al
	je	.L1115
.L1130:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1115
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1115:
	movq	%rbx, %rax
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1129:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	testb	$24, %al
	jne	.L1130
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1109:
	movq	47(%rax), %rax
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1106:
	movslq	11(%rax), %rax
	shrl	$10, %eax
	andl	$2097151, %eax
	jmp	.L1105
	.cfi_endproc
.LFE21429:
	.size	_ZN2v88internal10JSReceiver23GetOrCreateIdentityHashEPNS0_7IsolateE, .-_ZN2v88internal10JSReceiver23GetOrCreateIdentityHashEPNS0_7IsolateE
	.section	.text._ZN2v88internal10JSReceiver24DeleteNormalizedPropertyENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver24DeleteNormalizedPropertyENS0_6HandleIS1_EEi
	.type	_ZN2v88internal10JSReceiver24DeleteNormalizedPropertyENS0_6HandleIS1_EEi, @function
_ZN2v88internal10JSReceiver24DeleteNormalizedPropertyENS0_6HandleIS1_EEi:
.LFB21430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	-1(%rax), %rcx
	leaq	-37592(%rdx), %r12
	movq	7(%rax), %r14
	cmpw	$1025, 11(%rcx)
	je	.L1156
	testb	$1, %r14b
	jne	.L1140
	movq	-36536(%rdx), %r14
.L1140:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1141
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1142:
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal10DictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE11DeleteEntryEPNS0_7IsolateENS0_6HandleIS2_EEi@PLT
	movq	(%rbx), %rdx
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE
.L1138:
	movq	(%rbx), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$1048576, %edx
	jne	.L1157
.L1131:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1158
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1141:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1159
.L1143:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1156:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1133
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1134:
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal12PropertyCell15InvalidateEntryEPNS0_7IsolateENS0_6HandleINS0_16GlobalDictionaryEEEi@PLT
	movq	96(%r12), %r12
	movq	(%rax), %r14
	movq	%rax, %r13
	movq	%r12, %rdx
	movq	%r12, 23(%r14)
	leaq	23(%r14), %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	je	.L1136
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1136
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1136
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1136:
	movq	0(%r13), %rdi
	xorl	%edx, %edx
	movq	$0, 15(%rdi)
	leaq	15(%rdi), %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1157:
	movq	-1(%rax), %rdi
	call	_ZN2v88internal12_GLOBAL__N_133InvalidatePrototypeChainsInternalENS0_3MapE
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1133:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1160
.L1135:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1159:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1160:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1135
.L1158:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21430:
	.size	_ZN2v88internal10JSReceiver24DeleteNormalizedPropertyENS0_6HandleIS1_EEi, .-_ZN2v88internal10JSReceiver24DeleteNormalizedPropertyENS0_6HandleIS1_EEi
	.section	.text._ZN2v88internal10JSReceiver19OrdinaryToPrimitiveENS0_6HandleIS1_EENS0_23OrdinaryToPrimitiveHintE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver19OrdinaryToPrimitiveENS0_6HandleIS1_EENS0_23OrdinaryToPrimitiveHintE
	.type	_ZN2v88internal10JSReceiver19OrdinaryToPrimitiveENS0_6HandleIS1_EENS0_23OrdinaryToPrimitiveHintE, @function
_ZN2v88internal10JSReceiver19OrdinaryToPrimitiveENS0_6HandleIS1_EENS0_23OrdinaryToPrimitiveHintE:
.LFB21459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movaps	%xmm0, -80(%rbp)
	andq	$-262144, %rax
	movq	24(%rax), %rax
	leaq	-37592(%rax), %r14
	testl	%esi, %esi
	je	.L1162
	cmpl	$1, %esi
	jne	.L1164
	leaq	-34136(%rax), %rcx
	subq	$34040, %rax
	movq	%rcx, %xmm0
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
.L1164:
	leaq	-64(%rbp), %rax
	leaq	-80(%rbp), %rbx
	movabsq	$824633720832, %r15
	movq	%rax, -168(%rbp)
	leaq	-160(%rbp), %r13
.L1176:
	movq	(%rbx), %rsi
	movl	$3, %eax
	movq	(%rsi), %rdx
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L1165
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L1165:
	movl	%eax, -160(%rbp)
	movq	%r15, -148(%rbp)
	movq	%r14, -136(%rbp)
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L1184
.L1166:
	movq	%r13, %rdi
	movq	%rsi, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%r12, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -156(%rbp)
	jne	.L1167
	movq	-136(%rbp), %rax
	leaq	88(%rax), %rsi
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1185
.L1172:
	addq	$8, %rbx
	cmpq	-168(%rbp), %rbx
	jne	.L1176
	xorl	%edx, %edx
	movl	$30, %esi
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	.p2align 4,,10
	.p2align 3
.L1182:
	xorl	%eax, %eax
.L1169:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1186
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1167:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1182
	movq	(%rsi), %rax
	testb	$1, %al
	je	.L1172
.L1185:
	movq	-1(%rax), %rax
	testb	$2, 13(%rax)
	je	.L1172
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	testq	%rax, %rax
	je	.L1169
	movq	(%rax), %rdx
	testb	$1, %dl
	je	.L1169
	movq	-1(%rdx), %rdx
	cmpw	$67, 11(%rdx)
	ja	.L1172
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1184:
	movq	%r14, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L1166
	.p2align 4,,10
	.p2align 3
.L1162:
	leaq	-34040(%rax), %rcx
	subq	$34136, %rax
	movq	%rcx, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	jmp	.L1164
.L1186:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21459:
	.size	_ZN2v88internal10JSReceiver19OrdinaryToPrimitiveENS0_6HandleIS1_EENS0_23OrdinaryToPrimitiveHintE, .-_ZN2v88internal10JSReceiver19OrdinaryToPrimitiveENS0_6HandleIS1_EENS0_23OrdinaryToPrimitiveHintE
	.section	.text._ZN2v88internal10JSReceiver11ToPrimitiveENS0_6HandleIS1_EENS0_15ToPrimitiveHintE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver11ToPrimitiveENS0_6HandleIS1_EENS0_15ToPrimitiveHintE
	.type	_ZN2v88internal10JSReceiver11ToPrimitiveENS0_6HandleIS1_EENS0_15ToPrimitiveHintE, @function
_ZN2v88internal10JSReceiver11ToPrimitiveENS0_6HandleIS1_EENS0_15ToPrimitiveHintE:
.LFB21458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rsi
	leaq	-37592(%rsi), %r14
	subq	$33672, %rsi
	call	_ZN2v88internal6Object9GetMethodENS0_6HandleINS0_10JSReceiverEEENS2_INS0_4NameEEE@PLT
	testq	%rax, %rax
	je	.L1197
	movq	%rax, %r13
	movq	(%rax), %rax
	cmpq	%rax, 88(%r14)
	jne	.L1198
	xorl	%esi, %esi
	cmpl	$2, %r15d
	movq	%r12, %rdi
	sete	%sil
	call	_ZN2v88internal10JSReceiver19OrdinaryToPrimitiveENS0_6HandleIS1_EENS0_23OrdinaryToPrimitiveHintE
.L1189:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1199
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1198:
	.cfi_restore_state
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory21ToPrimitiveHintStringENS0_15ToPrimitiveHintE@PLT
	leaq	-48(%rbp), %r8
	movq	%r12, %rdx
	movq	%r13, %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	testq	%rax, %rax
	je	.L1189
	movq	(%rax), %rdx
	testb	$1, %dl
	je	.L1189
	movq	-1(%rdx), %rdx
	cmpw	$67, 11(%rdx)
	jbe	.L1189
	xorl	%edx, %edx
	movl	$30, %esi
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L1197:
	xorl	%eax, %eax
	jmp	.L1189
.L1199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21458:
	.size	_ZN2v88internal10JSReceiver11ToPrimitiveENS0_6HandleIS1_EENS0_15ToPrimitiveHintE, .-_ZN2v88internal10JSReceiver11ToPrimitiveENS0_6HandleIS1_EENS0_15ToPrimitiveHintE
	.section	.text._ZN2v88internal10JSReceiver19HasProxyInPrototypeEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver19HasProxyInPrototypeEPNS0_7IsolateE
	.type	_ZN2v88internal10JSReceiver19HasProxyInPrototypeEPNS0_7IsolateE, @function
_ZN2v88internal10JSReceiver19HasProxyInPrototypeEPNS0_7IsolateE:
.LFB21465:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1201:
	movq	-1(%rax), %rax
	movq	23(%rax), %rax
	cmpq	104(%rsi), %rax
	je	.L1205
.L1204:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jne	.L1201
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1205:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE21465:
	.size	_ZN2v88internal10JSReceiver19HasProxyInPrototypeEPNS0_7IsolateE, .-_ZN2v88internal10JSReceiver19HasProxyInPrototypeEPNS0_7IsolateE
	.section	.text._ZN2v88internal8JSObject12ObjectCreateEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject12ObjectCreateEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal8JSObject12ObjectCreateEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal8JSObject12ObjectCreateEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB21470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal3Map18GetObjectCreateMapEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %rsi
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L1207
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$2, %edx
	call	_ZN2v88internal7Factory22NewSlowJSObjectFromMapENS0_6HandleINS0_3MapEEEiNS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1207:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21470:
	.size	_ZN2v88internal8JSObject12ObjectCreateEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal8JSObject12ObjectCreateEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal8JSObject26EnsureWritableFastElementsENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject26EnsureWritableFastElementsENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSObject26EnsureWritableFastElementsENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSObject26EnsureWritableFastElementsENS0_6HandleIS1_EE:
.LFB21471:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	15(%rax), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-1(%rsi), %rdx
	cmpq	%rdx, -37432(%rax)
	jne	.L1232
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-37592(%rax), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1212
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L1213:
	leaq	152(%r12), %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory21CopyFixedArrayWithMapENS0_6HandleINS0_10FixedArrayEEENS2_INS0_3MapEEE@PLT
	movq	(%rbx), %r14
	movq	(%rax), %r13
	leaq	15(%r14), %r15
	movq	%r13, 15(%r14)
	testb	$1, %r13b
	je	.L1221
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1235
.L1216:
	testb	$24, %al
	je	.L1221
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1236
	.p2align 4,,10
	.p2align 3
.L1221:
	movq	40960(%r12), %rbx
	cmpb	$0, 6912(%rbx)
	je	.L1218
	movq	6904(%rbx), %rax
.L1219:
	testq	%rax, %rax
	je	.L1210
	addl	$1, (%rax)
.L1210:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1212:
	.cfi_restore_state
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L1237
.L1214:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1232:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1218:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movb	$1, 6912(%rbx)
	leaq	6888(%rbx), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6904(%rbx)
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1235:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1236:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1237:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1214
	.cfi_endproc
.LFE21471:
	.size	_ZN2v88internal8JSObject26EnsureWritableFastElementsENS0_6HandleIS1_EE, .-_ZN2v88internal8JSObject26EnsureWritableFastElementsENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb
	.type	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb, @function
_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb:
.LFB21472:
	.cfi_startproc
	endbr64
	cmpw	$168, %di
	je	.L1259
	subw	$1025, %di
	cmpw	$80, %di
	ja	.L1240
	leaq	.L1242(%rip), %rdx
	movzwl	%di, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb,"a",@progbits
	.align 4
	.align 4
.L1242:
	.long	.L1248-.L1242
	.long	.L1249-.L1242
	.long	.L1249-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1256-.L1242
	.long	.L1249-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1240-.L1242
	.long	.L1256-.L1242
	.long	.L1256-.L1242
	.long	.L1256-.L1242
	.long	.L1246-.L1242
	.long	.L1246-.L1242
	.long	.L1249-.L1242
	.long	.L1248-.L1242
	.long	.L1260-.L1242
	.long	.L1252-.L1242
	.long	.L1256-.L1242
	.long	.L1252-.L1242
	.long	.L1256-.L1242
	.long	.L1255-.L1242
	.long	.L1249-.L1242
	.long	.L1248-.L1242
	.long	.L1248-.L1242
	.long	.L1248-.L1242
	.long	.L1252-.L1242
	.long	.L1248-.L1242
	.long	.L1246-.L1242
	.long	.L1246-.L1242
	.long	.L1249-.L1242
	.long	.L1248-.L1242
	.long	.L1248-.L1242
	.long	.L1248-.L1242
	.long	.L1249-.L1242
	.long	.L1249-.L1242
	.long	.L1255-.L1242
	.long	.L1249-.L1242
	.long	.L1249-.L1242
	.long	.L1259-.L1242
	.long	.L1244-.L1242
	.long	.L1252-.L1242
	.long	.L1248-.L1242
	.long	.L1250-.L1242
	.long	.L1246-.L1242
	.long	.L1249-.L1242
	.long	.L1244-.L1242
	.long	.L1244-.L1242
	.long	.L1246-.L1242
	.long	.L1246-.L1242
	.long	.L1246-.L1242
	.long	.L1248-.L1242
	.long	.L1244-.L1242
	.long	.L1247-.L1242
	.long	.L1246-.L1242
	.long	.L1259-.L1242
	.long	.L1244-.L1242
	.long	.L1246-.L1242
	.long	.L1241-.L1242
	.section	.text._ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb
.L1255:
	movl	$80, %eax
	ret
.L1260:
	movl	$88, %eax
	ret
.L1259:
	movl	$72, %eax
	ret
.L1249:
	movl	$32, %eax
	ret
.L1246:
	movl	$48, %eax
	ret
.L1248:
	movl	$40, %eax
	ret
.L1244:
	movl	$56, %eax
	ret
.L1252:
	movl	$96, %eax
	ret
.L1250:
	movl	$64, %eax
	ret
.L1241:
	cmpb	$1, %sil
	sbbl	%eax, %eax
	andl	$-8, %eax
	addl	$64, %eax
	ret
.L1247:
	movl	$280, %eax
	ret
.L1256:
	movl	$24, %eax
	ret
.L1240:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21472:
	.size	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb, .-_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb
	.section	.text._ZN2v88internal8JSObject10AllCanReadEPNS0_14LookupIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject10AllCanReadEPNS0_14LookupIteratorE
	.type	_ZN2v88internal8JSObject10AllCanReadEPNS0_14LookupIteratorE, @function
_ZN2v88internal8JSObject10AllCanReadEPNS0_14LookupIteratorE:
.LFB21473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	4(%rbx), %eax
	cmpl	$4, %eax
	jne	.L1265
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1267:
	cmpl	$2, %eax
	je	.L1299
	cmpl	$3, %eax
	je	.L1289
.L1269:
	movq	%rbx, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	4(%rbx), %eax
	cmpl	$4, %eax
	je	.L1289
.L1265:
	cmpl	$5, %eax
	jne	.L1267
	movq	%rbx, %rdi
	call	_ZNK2v88internal14LookupIterator12GetAccessorsEv@PLT
	movq	(%rax), %rax
	testb	$1, %al
	je	.L1269
	movq	-1(%rax), %rdx
	cmpw	$78, 11(%rdx)
	jne	.L1269
	testb	$1, 19(%rax)
	je	.L1269
.L1271:
	movl	$1, %eax
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1299:
	movq	56(%rbx), %rax
	cmpl	$-1, 72(%rbx)
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	movq	31(%rax), %rax
	je	.L1273
	testb	$1, %al
	jne	.L1276
.L1277:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rsi
	cmpq	%rsi, %rdx
	je	.L1279
	movq	39(%rdx), %rsi
.L1279:
	movq	24(%rbx), %r12
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1285
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1286:
	testb	$2, 75(%rsi)
	jne	.L1271
	movq	%rbx, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	4(%rbx), %eax
	cmpl	$4, %eax
	jne	.L1265
	.p2align 4,,10
	.p2align 3
.L1289:
	xorl	%eax, %eax
.L1264:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1300:
	.cfi_restore_state
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L1277
	.p2align 4,,10
	.p2align 3
.L1276:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L1300
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1277
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1285:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1301
.L1287:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1273:
	testb	$1, %al
	jne	.L1282
.L1283:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rsi
	cmpq	%rsi, %rdx
	je	.L1279
	movq	31(%rdx), %rsi
	jmp	.L1279
.L1302:
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L1283
	.p2align 4,,10
	.p2align 3
.L1282:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L1302
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1283
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L1283
	.p2align 4,,10
	.p2align 3
.L1301:
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1287
	.cfi_endproc
.LFE21473:
	.size	_ZN2v88internal8JSObject10AllCanReadEPNS0_14LookupIteratorE, .-_ZN2v88internal8JSObject10AllCanReadEPNS0_14LookupIteratorE
	.section	.text._ZN2v88internal8JSObject32GetPropertyWithFailedAccessCheckEPNS0_14LookupIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject32GetPropertyWithFailedAccessCheckEPNS0_14LookupIteratorE
	.type	_ZN2v88internal8JSObject32GetPropertyWithFailedAccessCheckEPNS0_14LookupIteratorE, @function
_ZN2v88internal8JSObject32GetPropertyWithFailedAccessCheckEPNS0_14LookupIteratorE:
.LFB21474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %r13
	movq	56(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal14LookupIterator34GetInterceptorForFailedAccessCheckEv@PLT
	testq	%rax, %rax
	je	.L1349
	movq	%rax, %rsi
	leaq	-57(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_134GetPropertyWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEEPb
	testq	%rax, %rax
	je	.L1326
	cmpb	$0, -57(%rbp)
	jne	.L1326
.L1305:
	movq	32(%r12), %rbx
	testq	%rbx, %rbx
	je	.L1350
.L1330:
	movq	(%rbx), %rax
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	jne	.L1336
	testb	$2, 11(%rax)
	je	.L1336
.L1346:
	movq	24(%r12), %rax
	addq	$88, %rax
	.p2align 4,,10
	.p2align 3
.L1326:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1351
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1336:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	12552(%r13), %rax
	cmpq	%rax, 96(%r13)
	je	.L1346
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
	jmp	.L1326
	.p2align 4,,10
	.p2align 3
.L1349:
	leaq	-57(%rbp), %rbx
.L1325:
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject10AllCanReadEPNS0_14LookupIteratorE
	testb	%al, %al
	je	.L1305
	cmpl	$5, 4(%r12)
	je	.L1352
	movq	56(%r12), %rax
	cmpl	$-1, 72(%r12)
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	movq	31(%rax), %rax
	je	.L1308
	testb	$1, %al
	jne	.L1311
.L1312:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %r15
	cmpq	%r15, %rdx
	je	.L1314
	movq	39(%rdx), %r15
.L1314:
	movq	24(%r12), %rdx
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1320
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1321:
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_134GetPropertyWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEEPb
	testq	%rax, %rax
	je	.L1326
	cmpb	$0, -57(%rbp)
	je	.L1325
	jmp	.L1326
	.p2align 4,,10
	.p2align 3
.L1353:
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L1312
	.p2align 4,,10
	.p2align 3
.L1311:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L1353
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1312
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L1312
	.p2align 4,,10
	.p2align 3
.L1320:
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L1354
.L1322:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r15, (%rsi)
	jmp	.L1321
	.p2align 4,,10
	.p2align 3
.L1308:
	testb	$1, %al
	jne	.L1317
.L1318:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %r15
	cmpq	%r15, %rdx
	je	.L1314
	movq	31(%rdx), %r15
	jmp	.L1314
	.p2align 4,,10
	.p2align 3
.L1355:
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L1318
	.p2align 4,,10
	.p2align 3
.L1317:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L1355
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1318
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L1318
	.p2align 4,,10
	.p2align 3
.L1354:
	movq	%rdx, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L1322
	.p2align 4,,10
	.p2align 3
.L1352:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object23GetPropertyWithAccessorEPNS0_14LookupIteratorE@PLT
	jmp	.L1326
	.p2align 4,,10
	.p2align 3
.L1350:
	movl	72(%r12), %r15d
	movq	24(%r12), %rdi
	testl	%r15d, %r15d
	js	.L1331
	movq	%r15, %rsi
	movl	$1, %edx
	salq	$32, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb@PLT
	movq	%rax, %rbx
.L1332:
	movq	(%rbx), %rax
	movl	11(%rax), %esi
	cmpl	$10, %esi
	jle	.L1356
.L1335:
	movq	%rbx, 32(%r12)
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1331:
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	-72(%rbp), %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rax, %rbx
	jmp	.L1332
.L1356:
	cmpl	$3, 7(%rax)
	jne	.L1335
	movl	%r15d, %edi
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movq	(%rbx), %rdx
	movl	%eax, 7(%rdx)
	jmp	.L1335
.L1351:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21474:
	.size	_ZN2v88internal8JSObject32GetPropertyWithFailedAccessCheckEPNS0_14LookupIteratorE, .-_ZN2v88internal8JSObject32GetPropertyWithFailedAccessCheckEPNS0_14LookupIteratorE
	.section	.text._ZN2v88internal8JSObject42GetPropertyAttributesWithFailedAccessCheckEPNS0_14LookupIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject42GetPropertyAttributesWithFailedAccessCheckEPNS0_14LookupIteratorE
	.type	_ZN2v88internal8JSObject42GetPropertyAttributesWithFailedAccessCheckEPNS0_14LookupIteratorE, @function
_ZN2v88internal8JSObject42GetPropertyAttributesWithFailedAccessCheckEPNS0_14LookupIteratorE:
.LFB21475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %r13
	movq	56(%rdi), %r14
	call	_ZNK2v88internal14LookupIterator34GetInterceptorForFailedAccessCheckEv@PLT
	testq	%rax, %rax
	je	.L1377
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_144GetPropertyAttributesWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEE
	movq	12480(%r13), %rcx
	cmpq	%rcx, 96(%r13)
	je	.L1379
.L1396:
	xorl	%eax, %eax
.L1394:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1399:
	.cfi_restore_state
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1375:
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_144GetPropertyAttributesWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEE
	movq	12552(%r13), %rcx
	cmpq	%rcx, 96(%r13)
	jne	.L1382
	testb	%al, %al
	jne	.L1397
.L1377:
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject10AllCanReadEPNS0_14LookupIteratorE
	testb	%al, %al
	je	.L1382
	cmpl	$5, 4(%r12)
	je	.L1398
	movq	56(%r12), %rax
	cmpl	$-1, 72(%r12)
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	movq	31(%rax), %rax
	je	.L1362
	testb	$1, %al
	jne	.L1365
.L1366:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %r15
	cmpq	%r15, %rdx
	je	.L1368
	movq	39(%rdx), %r15
.L1368:
	movq	24(%r12), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L1399
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L1400
.L1376:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r15, (%rsi)
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1401:
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L1366
	.p2align 4,,10
	.p2align 3
.L1365:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L1401
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1366
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1362:
	testb	$1, %al
	jne	.L1371
.L1372:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %r15
	cmpq	%r15, %rdx
	je	.L1368
	movq	31(%rdx), %r15
	jmp	.L1368
	.p2align 4,,10
	.p2align 3
.L1402:
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L1372
	.p2align 4,,10
	.p2align 3
.L1371:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L1402
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1372
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1400:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1376
	.p2align 4,,10
	.p2align 3
.L1379:
	movq	%rax, %rdx
	shrq	$32, %rdx
	cmpq	$64, %rdx
	je	.L1382
	testb	%al, %al
	jne	.L1394
	.p2align 4,,10
	.p2align 3
.L1382:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	12552(%r13), %rcx
	movabsq	$274877906945, %rax
	cmpq	%rcx, 96(%r13)
	je	.L1394
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	jmp	.L1396
	.p2align 4,,10
	.p2align 3
.L1398:
	movl	16(%r12), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	shrl	$3, %eax
	popq	%r13
	popq	%r14
	andl	$7, %eax
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	salq	$32, %rax
	orq	$1, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1397:
	.cfi_restore_state
	movq	%rax, %rdx
	shrq	$32, %rdx
	movl	%edx, %ecx
	cmpl	$64, %edx
	je	.L1377
	salq	$32, %rcx
	movzbl	%al, %eax
	orq	%rcx, %rax
	jmp	.L1394
	.cfi_endproc
.LFE21475:
	.size	_ZN2v88internal8JSObject42GetPropertyAttributesWithFailedAccessCheckEPNS0_14LookupIteratorE, .-_ZN2v88internal8JSObject42GetPropertyAttributesWithFailedAccessCheckEPNS0_14LookupIteratorE
	.section	.text._ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE
	.type	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE, @function
_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE:
.LFB21394:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	cmpl	$4, %eax
	je	.L1428
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	.L1408(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
.L1404:
	cmpl	$7, %eax
	ja	.L1406
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE,"a",@progbits
	.align 4
	.align 4
.L1408:
	.long	.L1413-.L1408
	.long	.L1412-.L1408
	.long	.L1411-.L1408
	.long	.L1410-.L1408
	.long	.L1406-.L1408
	.long	.L1409-.L1408
	.long	.L1409-.L1408
	.long	.L1407-.L1408
	.section	.text._ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE
	.p2align 4,,10
	.p2align 3
.L1411:
	movq	%r12, %rdi
	call	_ZNK2v88internal14LookupIterator14GetInterceptorEv
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal12_GLOBAL__N_144GetPropertyAttributesWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEE
	movq	%rax, %rdx
	shrq	$32, %rdx
	testb	%al, %al
	je	.L1431
	cmpl	$64, %edx
	jne	.L1409
.L1406:
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	4(%r12), %eax
	cmpl	$4, %eax
	jne	.L1404
.L1412:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1409:
	.cfi_restore_state
	movl	$257, %eax
.L1405:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1413:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal14LookupIterator9HasAccessEv@PLT
	testb	%al, %al
	jne	.L1406
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject42GetPropertyAttributesWithFailedAccessCheckEPNS0_14LookupIteratorE
	xorl	%edx, %edx
	testb	%al, %al
	je	.L1422
	shrq	$32, %rax
	movl	$1, %edx
	cmpl	$64, %eax
	setne	%bl
.L1422:
	xorl	%eax, %eax
	movb	%bl, %ah
	movb	%dl, %al
	jmp	.L1405
	.p2align 4,,10
	.p2align 3
.L1410:
	movq	32(%r12), %rdx
	movq	24(%r12), %rdi
	testq	%rdx, %rdx
	je	.L1432
.L1414:
	movq	56(%r12), %rsi
	addq	$24, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7JSProxy11HasPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE@PLT
	.p2align 4,,10
	.p2align 3
.L1432:
	.cfi_restore_state
	movl	72(%r12), %r13d
	testl	%r13d, %r13d
	js	.L1415
	movq	%r13, %rsi
	movl	$1, %edx
	salq	$32, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb@PLT
	movq	%rax, %rdx
.L1416:
	movq	(%rdx), %rax
	movl	11(%rax), %esi
	cmpl	$10, %esi
	jle	.L1433
.L1419:
	movq	%rdx, 32(%r12)
	movq	24(%r12), %rdi
	jmp	.L1414
.L1407:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1415:
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	-40(%rbp), %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rax, %rdx
	jmp	.L1416
.L1433:
	cmpl	$3, 7(%rax)
	jne	.L1419
	movl	%r13d, %edi
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movq	-40(%rbp), %rdx
	movq	(%rdx), %rcx
	movl	%eax, 7(%rcx)
	jmp	.L1419
.L1431:
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L1405
.L1428:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE21394:
	.size	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE, .-_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE
	.section	.text._ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE
	.type	_ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE, @function
_ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE:
.LFB21422:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	cmpl	$4, %eax
	je	.L1457
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	.L1438(%rip), %rbx
.L1451:
	cmpl	$7, %eax
	ja	.L1436
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE,"a",@progbits
	.align 4
	.align 4
.L1438:
	.long	.L1444-.L1438
	.long	.L1461-.L1438
	.long	.L1442-.L1438
	.long	.L1441-.L1438
	.long	.L1436-.L1438
	.long	.L1440-.L1438
	.long	.L1439-.L1438
	.long	.L1437-.L1438
	.section	.text._ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE
	.p2align 4,,10
	.p2align 3
.L1442:
	movq	%r12, %rdi
	call	_ZNK2v88internal14LookupIterator14GetInterceptorEv
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal12_GLOBAL__N_144GetPropertyAttributesWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEE
	movq	%rax, %rdx
	shrq	$32, %rdx
	movl	%edx, %ecx
	testb	%al, %al
	je	.L1462
	cmpl	$64, %edx
	jne	.L1463
.L1436:
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	4(%r12), %eax
	cmpl	$4, %eax
	jne	.L1451
.L1461:
	movabsq	$274877906945, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1440:
	.cfi_restore_state
	movq	56(%r12), %rax
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L1449
.L1439:
	movl	16(%r12), %eax
	shrl	$3, %eax
	andl	$7, %eax
	salq	$32, %rax
	orq	$1, %rax
.L1446:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1441:
	.cfi_restore_state
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7JSProxy21GetPropertyAttributesEPNS0_14LookupIteratorE@PLT
	.p2align 4,,10
	.p2align 3
.L1444:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal14LookupIterator9HasAccessEv@PLT
	testb	%al, %al
	jne	.L1436
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8JSObject42GetPropertyAttributesWithFailedAccessCheckEPNS0_14LookupIteratorE
	.p2align 4,,10
	.p2align 3
.L1449:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1027, 11(%rax)
	jne	.L1439
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17JSModuleNamespace21GetPropertyAttributesEPNS0_14LookupIteratorE@PLT
.L1437:
	.cfi_restore_state
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1463:
	salq	$32, %rcx
	movzbl	%al, %eax
	orq	%rcx, %rax
	jmp	.L1446
.L1462:
	movq	%rcx, %rax
	salq	$32, %rax
	jmp	.L1446
.L1457:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movabsq	$274877906945, %rax
	ret
	.cfi_endproc
.LFE21422:
	.size	_ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE, .-_ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE
	.section	.rodata._ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorE.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"v8::IndexedPropertyDescriptorCallback"
	.align 8
.LC26:
	.string	"v8::NamedPropertyDescriptorCallback"
	.align 8
.LC27:
	.string	"interceptor-indexed-descriptor"
	.section	.rodata._ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorE.str1.1,"aMS",@progbits,1
.LC28:
	.string	"interceptor-named-descriptor"
.LC29:
	.string	"Invalid property descriptor."
	.section	.text._ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorE
	.type	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorE, @function
_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorE:
.LFB21452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	cmpl	$4, 4(%rdi)
	movq	%rax, -248(%rbp)
	je	.L1478
	movq	56(%rdi), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	je	.L1466
	movl	4(%rdi), %eax
	testl	%eax, %eax
	jne	.L1556
	call	_ZNK2v88internal14LookupIterator9HasAccessEv@PLT
	movq	%r14, %rdi
	testb	%al, %al
	jne	.L1557
	call	_ZNK2v88internal14LookupIterator34GetInterceptorForFailedAccessCheckEv@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1558
	cmpl	$2, 4(%r14)
	je	.L1471
.L1474:
	movq	24(%r14), %r13
	movq	(%r15), %rax
	movq	88(%r13), %rcx
	cmpq	%rcx, 31(%rax)
	je	.L1478
	movq	48(%r14), %rsi
	movq	56(%r14), %rbx
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L1559
.L1479:
	movq	%r13, %rdi
	call	_ZN2v88internal6Object15ConvertReceiverEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1551
	movq	(%r15), %rax
.L1483:
	movq	(%rsi), %rcx
	movq	63(%rax), %rdx
	movq	%r13, %rsi
	leaq	-144(%rbp), %rdi
	movabsq	$4294967297, %r9
	movq	(%rbx), %r8
	call	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movl	72(%r14), %eax
	movl	%eax, -256(%rbp)
	cmpl	$-1, %eax
	je	.L1484
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %rbx
	movq	$0, -160(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1560
.L1485:
	movq	41016(%rbx), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L1561
.L1486:
	movq	(%r15), %rax
	movq	31(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L1513
	movq	7(%rax), %r9
	movq	%r9, -264(%rbp)
.L1487:
	movq	-104(%rbp), %rbx
	cmpl	$32, 41828(%rbx)
	je	.L1488
.L1491:
	movl	12616(%rbx), %eax
	leaq	-224(%rbp), %r11
	movq	%rbx, %rsi
	movq	%r9, %rdx
	movl	$6, 12616(%rbx)
	movq	%r11, %rdi
	movl	%eax, -272(%rbp)
	movq	%r11, -280(%rbp)
	call	_ZN2v88internal21ExternalCallbackScopeC1EPNS0_7IsolateEm
	leaq	-120(%rbp), %rax
	movl	-256(%rbp), %edi
	leaq	-232(%rbp), %rsi
	movq	%rax, -232(%rbp)
	movq	-264(%rbp), %rax
	call	*%rax
.L1555:
	movq	96(%rbx), %rax
	xorl	%r15d, %r15d
	cmpq	%rax, -88(%rbp)
	movq	-280(%rbp), %r11
	je	.L1499
	leaq	-88(%rbp), %r15
.L1499:
	movq	%r11, %rdi
	call	_ZN2v88internal21ExternalCallbackScopeD1Ev
	movl	-272(%rbp), %eax
	movl	%eax, 12616(%rbx)
.L1502:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1562
.L1494:
	testq	%r15, %r15
	je	.L1504
	cmpl	$-1, 72(%r14)
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	.LC25(%rip), %rax
	leaq	.LC26(%rip), %r14
	cmovne	%rax, %r14
	call	_ZN2v88internal18PropertyDescriptor20ToPropertyDescriptorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPS1_@PLT
	testb	%al, %al
	je	.L1563
.L1506:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	movl	$257, %eax
.L1469:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1564
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1556:
	.cfi_restore_state
	cmpl	$2, %eax
	jne	.L1478
.L1471:
	movq	%r14, %rdi
	call	_ZNK2v88internal14LookupIterator14GetInterceptorEv
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L1474
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1557:
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	cmpl	$2, 4(%r14)
	je	.L1471
	.p2align 4,,10
	.p2align 3
.L1478:
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE
	testb	%al, %al
	je	.L1551
	shrq	$32, %rax
	movq	%rax, %rbx
	movl	$1, %eax
	cmpl	$64, %ebx
	je	.L1469
	cmpl	$5, 4(%r14)
	je	.L1565
.L1509:
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	je	.L1566
	movq	%rax, 8(%r12)
	movl	%ebx, %eax
	movzbl	(%r12), %ecx
	movl	%ebx, %edx
	notl	%eax
	andl	$1, %eax
	andl	$-49, %ecx
	sall	$4, %eax
	orl	$32, %eax
	orl	%ecx, %eax
	movb	%al, (%r12)
.L1510:
	movl	%edx, %eax
	shrl	%edx
	shrl	$2, %eax
	xorl	$1, %edx
	xorl	$1, %eax
	andl	$1, %edx
	andl	$1, %eax
	orl	$10, %edx
	sall	$2, %eax
	orl	%edx, %eax
	movzbl	(%r12), %edx
	andl	$-16, %edx
	orl	%edx, %eax
	movb	%al, (%r12)
	movl	$257, %eax
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1551:
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1466:
	call	_ZN2v88internal14LookupIterator7GetNameEv
	movq	56(%r14), %rsi
	movq	-248(%rbp), %rdi
	movq	%r12, %rcx
	movq	%rax, %rdx
	call	_ZN2v88internal7JSProxy24GetOwnPropertyDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEEPNS0_18PropertyDescriptorE@PLT
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1559:
	movq	-1(%rdx), %rdx
	cmpw	$1023, 11(%rdx)
	ja	.L1483
	jmp	.L1479
	.p2align 4,,10
	.p2align 3
.L1484:
	movq	32(%r14), %rax
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %rbx
	movq	$0, -160(%rbp)
	movaps	%xmm0, -192(%rbp)
	movq	%rax, -256(%rbp)
	movaps	%xmm0, -176(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1567
.L1495:
	movq	41016(%rbx), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L1568
.L1496:
	movq	(%r15), %rax
	movq	31(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L1515
	movq	7(%rax), %r9
	movq	%r9, -264(%rbp)
.L1497:
	movq	-104(%rbp), %rbx
	cmpl	$32, 41828(%rbx)
	je	.L1498
.L1501:
	movl	12616(%rbx), %eax
	leaq	-224(%rbp), %r11
	movq	%rbx, %rsi
	movq	%r9, %rdx
	movl	$6, 12616(%rbx)
	movq	%r11, %rdi
	movl	%eax, -272(%rbp)
	movq	%r11, -280(%rbp)
	call	_ZN2v88internal21ExternalCallbackScopeC1EPNS0_7IsolateEm
	leaq	-120(%rbp), %rax
	movq	-256(%rbp), %rdi
	leaq	-232(%rbp), %rsi
	movq	%rax, -232(%rbp)
	movq	-264(%rbp), %rax
	call	*%rax
	jmp	.L1555
	.p2align 4,,10
	.p2align 3
.L1563:
	leaq	.LC29(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v85Utils16ReportApiFailureEPKcS2_@PLT
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1488:
	movq	41472(%rbx), %rdi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%r9, -272(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-272(%rbp), %r9
	testb	%al, %al
	jne	.L1491
.L1553:
	xorl	%r15d, %r15d
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1561:
	movl	-256(%rbp), %ecx
	movq	-112(%rbp), %rdx
	leaq	.LC27(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj@PLT
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1504:
	movq	%r14, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1498:
	movq	41472(%rbx), %rdi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%r9, -272(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-272(%rbp), %r9
	testb	%al, %al
	jne	.L1501
	jmp	.L1553
	.p2align 4,,10
	.p2align 3
.L1568:
	movq	-256(%rbp), %rax
	movq	-112(%rbp), %rdx
	leaq	.LC28(%rip), %rsi
	movq	%rbx, %rdi
	movq	(%rax), %rcx
	call	_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE@PLT
	jmp	.L1496
	.p2align 4,,10
	.p2align 3
.L1562:
	leaq	-184(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1494
	.p2align 4,,10
	.p2align 3
.L1558:
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject10AllCanReadEPNS0_14LookupIteratorE
	testb	%al, %al
	je	.L1476
	cmpl	$2, 4(%r14)
	je	.L1471
.L1476:
	xorl	%esi, %esi
	cmpl	$-1, 72(%r14)
	movq	%r14, %rdi
	je	.L1477
	call	_ZN2v88internal14LookupIterator15RestartInternalILb1EEEvNS1_16InterceptorStateE@PLT
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1560:
	movq	40960(%rbx), %rax
	leaq	-184(%rbp), %rsi
	movl	$156, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1513:
	movq	$0, -264(%rbp)
	xorl	%r9d, %r9d
	jmp	.L1487
	.p2align 4,,10
	.p2align 3
.L1565:
	movq	%r14, %rdi
	call	_ZNK2v88internal14LookupIterator12GetAccessorsEv@PLT
	movq	(%rax), %rax
	testb	$1, %al
	je	.L1509
	movq	-1(%rax), %rax
	cmpw	$79, 11(%rax)
	jne	.L1509
	movq	%r14, %rdi
	call	_ZNK2v88internal14LookupIterator12GetAccessorsEv@PLT
	leaq	-192(%rbp), %rdi
	movq	%rax, %r15
	movq	56(%r14), %rax
	movq	(%rax), %rax
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal10JSReceiver18GetCreationContextEv
	movq	-248(%rbp), %r14
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%rax, %rsi
	movq	%rax, %r13
	movq	%r14, %rdi
	call	_ZN2v88internal12AccessorPair12GetComponentEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_IS1_EENS0_17AccessorComponentE@PLT
	movq	%r15, %rdx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%rax, 16(%r12)
	movq	%r14, %rdi
	call	_ZN2v88internal12AccessorPair12GetComponentEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_IS1_EENS0_17AccessorComponentE@PLT
	movl	%ebx, %edx
	movq	%rax, 24(%r12)
	jmp	.L1510
	.p2align 4,,10
	.p2align 3
.L1567:
	movq	40960(%rbx), %rax
	leaq	-184(%rbp), %rsi
	movl	$171, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1495
	.p2align 4,,10
	.p2align 3
.L1515:
	movq	$0, -264(%rbp)
	xorl	%r9d, %r9d
	jmp	.L1497
.L1566:
	movb	$0, %ah
	jmp	.L1469
.L1477:
	call	_ZN2v88internal14LookupIterator15RestartInternalILb0EEEvNS1_16InterceptorStateE@PLT
	jmp	.L1478
.L1564:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21452:
	.size	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorE, .-_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorE
	.section	.text._ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorE
	.type	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorE, @function
_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorE:
.LFB21450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-128(%rbp), %r14
	leaq	-129(%rbp), %r8
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r14, %rdi
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$0, -129(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorE
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1572
	addq	$120, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1572:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21450:
	.size	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorE, .-_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorE
	.section	.text._ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE
	.type	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE, @function
_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE:
.LFB21395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	-1(%rax), %rdx
	cmpw	$1027, 11(%rdx)
	je	.L1614
	movq	-1(%rax), %rdx
	andq	$-262144, %rax
	movq	%rsi, %rbx
	movq	24(%rax), %r14
	cmpw	$1024, 11(%rdx)
	jbe	.L1576
	movq	(%rsi), %rax
	subq	$37592, %r14
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1615
.L1578:
	movq	(%r12), %rcx
	movl	$1, %edx
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	je	.L1616
.L1582:
	movabsq	$824633720832, %rcx
	movl	%edx, -224(%rbp)
	movq	%rcx, -212(%rbp)
	movq	%r14, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L1617
.L1583:
	leaq	-224(%rbp), %r12
	movq	%rbx, -192(%rbp)
	movq	%r12, %rdi
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%r13, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L1581:
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE
	jmp	.L1575
	.p2align 4,,10
	.p2align 3
.L1576:
	movq	(%rsi), %rax
	subq	$37592, %r14
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1618
.L1585:
	movq	(%r12), %rcx
	movl	$1, %edx
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	je	.L1619
.L1589:
	movabsq	$824633720832, %rcx
	movl	%edx, -224(%rbp)
	movq	%rcx, -212(%rbp)
	movq	%r14, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L1620
.L1590:
	leaq	-224(%rbp), %r12
	movq	%rbx, -192(%rbp)
	movq	%r12, %rdi
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%r13, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L1588:
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE
	testb	%al, %al
	jne	.L1591
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L1575
	.p2align 4,,10
	.p2align 3
.L1614:
	pxor	%xmm0, %xmm0
	andq	$-262144, %rax
	movq	%rdi, %rdx
	movq	%r12, %rcx
	andb	$-64, -224(%rbp)
	leaq	-144(%rbp), %r14
	movl	$1, %r9d
	movups	%xmm0, -216(%rbp)
	movq	%r14, %rdi
	leaq	-228(%rbp), %r8
	movups	%xmm0, -200(%rbp)
	movq	24(%rax), %rsi
	movb	$0, -228(%rbp)
	subq	$37592, %rsi
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	leaq	-224(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorE
.L1575:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1621
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1619:
	.cfi_restore_state
	movl	11(%rcx), %edx
	notl	%edx
	andl	$1, %edx
	jmp	.L1589
	.p2align 4,,10
	.p2align 3
.L1616:
	movl	11(%rcx), %edx
	notl	%edx
	andl	$1, %edx
	jmp	.L1582
	.p2align 4,,10
	.p2align 3
.L1591:
	shrq	$32, %rax
	cmpl	$64, %eax
	movl	$1, %eax
	setne	%dl
	movb	%dl, %ah
	jmp	.L1575
	.p2align 4,,10
	.p2align 3
.L1615:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %edx
	testb	$1, %dl
	jne	.L1579
	andl	$2, %edx
	jne	.L1578
.L1579:
	leaq	-144(%rbp), %r15
	leaq	-228(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L1622
	movabsq	$824633720832, %rax
	movq	%r15, %rdi
	movq	%r14, -120(%rbp)
	movq	%rax, -132(%rbp)
	movl	-228(%rbp), %eax
	movl	$1, -144(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r12, -112(%rbp)
	movdqa	-128(%rbp), %xmm2
	leaq	-224(%rbp), %r12
	movdqa	-144(%rbp), %xmm1
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	jmp	.L1581
	.p2align 4,,10
	.p2align 3
.L1618:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %edx
	testb	$1, %dl
	jne	.L1586
	andl	$2, %edx
	jne	.L1585
.L1586:
	leaq	-144(%rbp), %r15
	leaq	-228(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L1623
	movabsq	$824633720832, %rax
	movq	%r15, %rdi
	movq	%r14, -120(%rbp)
	movq	%rax, -132(%rbp)
	movl	-228(%rbp), %eax
	movl	$1, -144(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r12, -112(%rbp)
	movdqa	-128(%rbp), %xmm7
	leaq	-224(%rbp), %r12
	movdqa	-144(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	jmp	.L1588
	.p2align 4,,10
	.p2align 3
.L1617:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rbx
	jmp	.L1583
	.p2align 4,,10
	.p2align 3
.L1620:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rbx
	jmp	.L1590
	.p2align 4,,10
	.p2align 3
.L1622:
	movq	(%r12), %rax
	jmp	.L1578
	.p2align 4,,10
	.p2align 3
.L1623:
	movq	(%r12), %rax
	jmp	.L1585
.L1621:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21395:
	.size	_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE, .-_ZN2v88internal10JSReceiver14HasOwnPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE
	.section	.text._ZN2v88internal8JSObject11AllCanWriteEPNS0_14LookupIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject11AllCanWriteEPNS0_14LookupIteratorE
	.type	_ZN2v88internal8JSObject11AllCanWriteEPNS0_14LookupIteratorE, @function
_ZN2v88internal8JSObject11AllCanWriteEPNS0_14LookupIteratorE:
.LFB21476:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	leal	-3(%rax), %edx
	cmpl	$1, %edx
	jbe	.L1636
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L1632
	.p2align 4,,10
	.p2align 3
.L1627:
	movq	%rbx, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	4(%rbx), %eax
	leal	-3(%rax), %edx
	cmpl	$1, %edx
	jbe	.L1639
.L1632:
	cmpl	$5, %eax
	jne	.L1627
	movq	%rbx, %rdi
	call	_ZNK2v88internal14LookupIterator12GetAccessorsEv@PLT
	movq	(%rax), %rax
	testb	$1, %al
	je	.L1627
	movq	-1(%rax), %rdx
	cmpw	$78, 11(%rdx)
	jne	.L1627
	testb	$2, 19(%rax)
	je	.L1627
	movl	$1, %eax
	jmp	.L1624
	.p2align 4,,10
	.p2align 3
.L1639:
	xorl	%eax, %eax
.L1624:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L1636:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE21476:
	.size	_ZN2v88internal8JSObject11AllCanWriteEPNS0_14LookupIteratorE, .-_ZN2v88internal8JSObject11AllCanWriteEPNS0_14LookupIteratorE
	.section	.text._ZN2v88internal8JSObject32SetPropertyWithFailedAccessCheckEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject32SetPropertyWithFailedAccessCheckEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE
	.type	_ZN2v88internal8JSObject32SetPropertyWithFailedAccessCheckEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE, @function
_ZN2v88internal8JSObject32SetPropertyWithFailedAccessCheckEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE:
.LFB21477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %r13
	movq	56(%rdi), %r14
	call	_ZNK2v88internal14LookupIterator34GetInterceptorForFailedAccessCheckEv@PLT
	testq	%rax, %rax
	je	.L1655
	movq	%rbx, %rcx
	movq	%rax, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_134SetPropertyWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEENS_5MaybeINS0_11ShouldThrowEEENS4_INS0_6ObjectEEE
	movq	12480(%r13), %rcx
	cmpq	%rcx, 96(%r13)
	je	.L1643
	xorl	%ebx, %ebx
	movb	$0, %bh
.L1646:
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1643:
	.cfi_restore_state
	movl	%eax, %ebx
	testb	%al, %al
	jne	.L1646
.L1642:
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	$257, %ebx
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	12552(%r13), %rax
	cmpq	%rax, 96(%r13)
	je	.L1646
	movq	%r13, %rdi
	xorb	%bl, %bl
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movb	$0, %bh
	jmp	.L1646
	.p2align 4,,10
	.p2align 3
.L1655:
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject11AllCanWriteEPNS0_14LookupIteratorE
	testb	%al, %al
	je	.L1642
	addq	$8, %rsp
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6Object23SetPropertyWithAccessorEPNS0_14LookupIteratorENS0_6HandleIS1_EENS_5MaybeINS0_11ShouldThrowEEE@PLT
	.cfi_endproc
.LFE21477:
	.size	_ZN2v88internal8JSObject32SetPropertyWithFailedAccessCheckEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE, .-_ZN2v88internal8JSObject32SetPropertyWithFailedAccessCheckEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE
	.section	.text._ZN2v88internal8JSObject21SetNormalizedPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_15PropertyDetailsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject21SetNormalizedPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_15PropertyDetailsE
	.type	_ZN2v88internal8JSObject21SetNormalizedPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_15PropertyDetailsE, @function
_ZN2v88internal8JSObject21SetNormalizedPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_15PropertyDetailsE:
.LFB21478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$72, %rsp
	movq	(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %r12
	movq	(%rsi), %rax
	movl	7(%rax), %edx
	subq	$37592, %r12
	testb	$1, %dl
	jne	.L1657
	movq	-1(%rcx), %rax
	shrl	$2, %edx
	cmpw	$1025, 11(%rax)
	je	.L1707
.L1659:
	movq	7(%rcx), %r8
	testb	$1, %r8b
	jne	.L1672
	andq	$-262144, %rcx
	movq	24(%rcx), %rax
	movq	-36536(%rax), %r8
.L1672:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1673
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%r15), %rdx
	movq	(%rax), %r8
	movq	%rax, %rsi
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L1676
.L1711:
	shrl	$2, %eax
.L1677:
	movslq	35(%r8), %rdi
	movq	88(%r12), %r11
	subq	$1, %r8
	movl	$1, %ecx
	subl	$1, %edi
	andl	%edi, %eax
	jmp	.L1680
	.p2align 4,,10
	.p2align 3
.L1708:
	cmpq	(%r15), %r9
	je	.L1679
	addl	%ecx, %eax
	addl	$1, %ecx
	andl	%edi, %eax
.L1680:
	leal	(%rax,%rax,2), %r10d
	leal	56(,%r10,8), %edx
	movslq	%edx, %rdx
	movq	(%rdx,%r8), %r9
	cmpq	%r9, %r11
	jne	.L1708
.L1692:
	movq	%r15, %rdx
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movl	%ebx, %r8d
	movq	%r14, %rcx
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE3AddEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_4NameEEENS7_INS0_6ObjectEEENS0_15PropertyDetailsEPi@PLT
	movq	0(%r13), %rdx
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE
.L1656:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1709
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1673:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1710
.L1675:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	movq	(%r15), %rdx
	movl	7(%rdx), %eax
	testb	$1, %al
	je	.L1711
.L1676:
	leaq	-64(%rbp), %rdi
	movq	%rsi, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %r8
	jmp	.L1677
	.p2align 4,,10
	.p2align 3
.L1657:
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	0(%r13), %rcx
	movl	%eax, %edx
	movq	-1(%rcx), %rax
	cmpw	$1025, 11(%rax)
	jne	.L1659
.L1707:
	movq	41112(%r12), %rdi
	movq	7(%rcx), %rsi
	testq	%rdi, %rdi
	je	.L1660
	movl	%edx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-72(%rbp), %edx
	movq	(%rax), %rsi
	movq	%rax, %r10
.L1661:
	movslq	35(%rsi), %r8
	movq	88(%r12), %r9
	subq	$1, %rsi
	movl	$1, %edi
	subl	$1, %r8d
	andl	%r8d, %edx
	jmp	.L1665
	.p2align 4,,10
	.p2align 3
.L1712:
	movq	(%r15), %rcx
	cmpq	%rcx, 7(%rax)
	je	.L1664
	addl	%edi, %edx
	addl	$1, %edi
	andl	%r8d, %edx
.L1665:
	leal	56(,%rdx,8), %ecx
	movslq	%ecx, %rcx
	movq	(%rcx,%rsi), %rax
	cmpq	%rax, %r9
	jne	.L1712
.L1688:
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r10, -88(%rbp)
	call	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE@PLT
	movq	(%r14), %rdx
	movq	(%rax), %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, 23(%rdi)
	leaq	23(%rdi), %rsi
	movq	%rdx, -72(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %r10
	testb	$1, %dl
	je	.L1667
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %r10
.L1667:
	movq	(%r14), %rax
	cmpq	%rax, 88(%r12)
	movq	%r12, %rdi
	movq	%r15, %rdx
	setne	%al
	andb	$63, %bl
	xorl	%r9d, %r9d
	movq	%r10, %rsi
	movzbl	%al, %eax
	movl	%ebx, %r8d
	sall	$6, %eax
	orl	%eax, %r8d
	call	_ZN2v88internal18BaseNameDictionaryINS0_16GlobalDictionaryENS0_21GlobalDictionaryShapeEE3AddEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_4NameEEENS7_INS0_6ObjectEEENS0_15PropertyDetailsEPi@PLT
	movq	0(%r13), %r12
	movq	(%rax), %r13
	leaq	7(%r12), %r14
	movq	%r13, 7(%r12)
.L1706:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	je	.L1656
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	jmp	.L1656
	.p2align 4,,10
	.p2align 3
.L1679:
	cmpl	$-1, %eax
	je	.L1692
	movq	(%rsi), %rdi
	leal	72(,%r10,8), %r12d
	andl	$-2147483393, %ebx
	movslq	%r12d, %r15
	subl	$8, %r12d
	movq	-1(%r15,%rdi), %rcx
	leaq	-1(%rdi), %r10
	movslq	%r12d, %r12
	movq	(%r14), %r14
	leaq	(%rdx,%r10), %r13
	addq	%r10, %r12
	sarq	$32, %rcx
	andl	$2147483392, %ecx
	orl	%ebx, %ecx
	movq	%rdi, %rbx
	andq	$-262144, %rbx
	movl	%ecx, -72(%rbp)
	movq	8(%rbx), %rcx
	testl	$262144, %ecx
	jne	.L1684
	andl	$24, %ecx
	jne	.L1713
.L1684:
	movq	%r10, -96(%rbp)
	movq	%r9, %rdx
	movq	%r13, %rsi
	movq	%r9, 0(%r13)
	movq	%r9, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %rdi
	movq	-96(%rbp), %r10
	testb	$1, %r9b
	jne	.L1714
.L1690:
	movq	%r10, -80(%rbp)
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r14, (%r12)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	movq	-80(%rbp), %r10
	je	.L1689
	movq	%r14, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L1689
	testb	$24, 8(%rbx)
	jne	.L1689
	movq	-88(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r10
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1713:
	movq	%r9, 0(%r13)
	movq	%r14, (%r12)
.L1689:
	movl	-72(%rbp), %ebx
	addl	%ebx, %ebx
	sarl	%ebx
	salq	$32, %rbx
	movq	%rbx, (%r10,%r15)
	jmp	.L1656
	.p2align 4,,10
	.p2align 3
.L1660:
	movq	41088(%r12), %r10
	cmpq	41096(%r12), %r10
	je	.L1715
.L1662:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r10)
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1714:
	movq	%r9, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L1690
	testb	$24, 8(%rbx)
	jne	.L1690
	movq	%r9, %rdx
	movq	%r13, %rsi
	movq	%r10, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r10
	movq	-80(%rbp), %rdi
	jmp	.L1690
	.p2align 4,,10
	.p2align 3
.L1664:
	cmpl	$-1, %edx
	je	.L1688
	movq	%r14, %rcx
	movq	%r12, %rdi
	movl	%ebx, %r8d
	movq	%r10, %rsi
	call	_ZN2v88internal12PropertyCell15PrepareForValueEPNS0_7IsolateENS0_6HandleINS0_16GlobalDictionaryEEEiNS4_INS0_6ObjectEEENS0_15PropertyDetailsE@PLT
	movq	(%r14), %r13
	movq	(%rax), %r12
	movq	%r13, 23(%r12)
	leaq	23(%r12), %r14
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1710:
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1675
	.p2align 4,,10
	.p2align 3
.L1715:
	movq	%r12, %rdi
	movq	%rsi, -80(%rbp)
	movl	%edx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movq	%rax, %r10
	jmp	.L1662
.L1709:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21478:
	.size	_ZN2v88internal8JSObject21SetNormalizedPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_15PropertyDetailsE, .-_ZN2v88internal8JSObject21SetNormalizedPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_15PropertyDetailsE
	.section	.rodata._ZN2v88internal8JSObject18JSObjectShortPrintEPNS0_12StringStreamE.str1.1,"aMS",@progbits,1
.LC30:
	.string	"<GlobalObject "
.LC31:
	.string	"<"
.LC32:
	.string	"deprecated-"
.LC33:
	.string	""
.LC34:
	.string	"Global "
.LC35:
	.string	"<JSArray[%u]>"
.LC36:
	.string	"<JSBoundFunction"
.LC37:
	.string	" (BoundTargetFunction %p)>"
.LC38:
	.string	"<JSWeakMap>"
.LC39:
	.string	"<JSWeakSet>"
.LC40:
	.string	"<JSRegExp"
.LC41:
	.string	" "
.LC42:
	.string	">"
.LC43:
	.string	"<JSFunction "
.LC44:
	.string	"<JSFunction"
.LC45:
	.string	" <"
.LC46:
	.string	" (sfi = %p)"
.LC47:
	.string	"<JSGenerator>"
.LC48:
	.string	"<JSAsyncFunctionObject>"
.LC49:
	.string	"<JS AsyncGenerator>"
.LC50:
	.string	"!!!INVALID CONSTRUCTOR!!!"
	.section	.rodata._ZN2v88internal8JSObject18JSObjectShortPrintEPNS0_12StringStreamE.str1.8,"aMS",@progbits,1
	.align 8
.LC51:
	.string	"!!!INVALID SHARED ON CONSTRUCTOR!!!"
	.section	.rodata._ZN2v88internal8JSObject18JSObjectShortPrintEPNS0_12StringStreamE.str1.1
.LC52:
	.string	" %smap = %p"
.LC53:
	.string	"<RemoteObject>"
.LC54:
	.string	"<JS%sObject"
.LC55:
	.string	" value = "
	.section	.text._ZN2v88internal8JSObject18JSObjectShortPrintEPNS0_12StringStreamE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject18JSObjectShortPrintEPNS0_12StringStreamE
	.type	_ZN2v88internal8JSObject18JSObjectShortPrintEPNS0_12StringStreamE, @function
_ZN2v88internal8JSObject18JSObjectShortPrintEPNS0_12StringStreamE:
.LFB21479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	subw	$1061, %ax
	cmpw	$44, %ax
	ja	.L1717
	leaq	.L1719(%rip), %rcx
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8JSObject18JSObjectShortPrintEPNS0_12StringStreamE,"a",@progbits
	.align 4
	.align 4
.L1719:
	.long	.L1727-.L1719
	.long	.L1717-.L1719
	.long	.L1726-.L1719
	.long	.L1725-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1724-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1723-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1722-.L1719
	.long	.L1721-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1717-.L1719
	.long	.L1720-.L1719
	.long	.L1718-.L1719
	.section	.text._ZN2v88internal8JSObject18JSObjectShortPrintEPNS0_12StringStreamE
	.p2align 4,,10
	.p2align 3
.L1717:
	movq	-1(%rdx), %r15
	movq	31(%r15), %r13
	testb	$1, %r13b
	jne	.L1764
.L1738:
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %edx
.L1743:
	cmpw	$1026, %dx
	leaq	.LC34(%rip), %rax
	leaq	-80(%rbp), %rcx
	movl	$1, %r8d
	leaq	.LC33(%rip), %rdx
	leaq	.LC54(%rip), %rsi
	movq	%r12, %rdi
	cmovne	%rdx, %rax
	movl	$11, %edx
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
.L1741:
	movq	(%rbx), %rax
	movq	-1(%rax), %rax
	cmpw	$1041, 11(%rax)
	je	.L1765
.L1748:
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3PutEc@PLT
	.p2align 4,,10
	.p2align 3
.L1716:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1766
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1727:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	23(%rax), %rax
	movq	%rax, %rbx
	shrq	$32, %rbx
	movq	%rbx, %rdx
	testb	$1, %al
	jne	.L1767
.L1754:
	movl	%edx, -80(%rbp)
	leaq	-80(%rbp), %rcx
	movl	$1, %r8d
	movq	%r12, %rdi
	leaq	.LC35(%rip), %rsi
	movl	$13, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1726:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC48(%rip), %rsi
	movq	%r12, %rdi
	movl	$23, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1725:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC49(%rip), %rsi
	movq	%r12, %rdi
	movl	$19, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1724:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC47(%rip), %rsi
	movq	%r12, %rdi
	movl	$13, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1723:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC40(%rip), %rsi
	movq	%r12, %rdi
	movl	$9, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	(%rbx), %rbx
	movq	31(%rbx), %rax
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1768
.L1731:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1722:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC38(%rip), %rsi
	movq	%r12, %rdi
	movl	$11, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1721:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	movl	$11, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1720:
	movq	(%rdi), %rbx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	.LC36(%rip), %rsi
	movl	$16, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	23(%rbx), %rax
	leaq	-80(%rbp), %rcx
	movq	%r12, %rdi
	movl	$1, %r8d
	leaq	.LC37(%rip), %rsi
	movl	$26, %edx
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1718:
	movq	(%rdi), %rbx
	leaq	-88(%rbp), %rdi
	movq	23(%rbx), %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	%rax, %r13
	testb	$1, %al
	jne	.L1769
.L1732:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC44(%rip), %rsi
	movq	%r12, %rdi
	movl	$11, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
.L1733:
	cmpb	$0, _ZN2v88internal21FLAG_trace_file_namesE(%rip)
	jne	.L1770
.L1735:
	movq	23(%rbx), %rax
	movq	%r12, %rdi
	leaq	-80(%rbp), %rcx
	movl	$11, %edx
	movl	$1, %r8d
	leaq	.LC46(%rip), %rsi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3PutEc@PLT
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1764:
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %r14
.L1740:
	leaq	-1(%r13), %rax
	movq	%rax, -104(%rbp)
	movq	-1(%r13), %rax
	cmpw	$68, 11(%rax)
	je	.L1771
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4Heap8ContainsENS0_10HeapObjectE@PLT
	testb	%al, %al
	je	.L1772
	movq	(%rbx), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	-1(%r13), %rax
	cmpw	$1105, 11(%rax)
	je	.L1773
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	cmpw	$88, 11(%rax)
	jne	.L1743
	leaq	.LC53(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3AddEPKc
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1770:
	movq	23(%rbx), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1774
.L1736:
	movq	15(%rax), %r13
	testb	$1, %r13b
	je	.L1735
	movq	-1(%r13), %rax
	cmpw	$63, 11(%rax)
	ja	.L1735
	movl	11(%r13), %edx
	testl	%edx, %edx
	jle	.L1735
	movq	%r12, %rdi
	leaq	.LC45(%rip), %rsi
	call	_ZN2v88internal12StringStream3AddEPKc
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3PutENS0_6StringE@PLT
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3AddEPKc
	jmp	.L1735
	.p2align 4,,10
	.p2align 3
.L1769:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1732
	movl	11(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L1732
	movq	%r12, %rdi
	leaq	.LC43(%rip), %rsi
	call	_ZN2v88internal12StringStream3AddEPKc
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3PutENS0_6StringE@PLT
	jmp	.L1733
	.p2align 4,,10
	.p2align 3
.L1768:
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$1, %edx
	leaq	.LC41(%rip), %rsi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	31(%rbx), %rax
	leaq	-88(%rbp), %rdi
	movq	%r12, %rsi
	movl	$1, %edx
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String16StringShortPrintEPNS0_12StringStreamEb@PLT
	jmp	.L1731
	.p2align 4,,10
	.p2align 3
.L1767:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	jne	.L1729
	xorl	%edx, %edx
	jmp	.L1754
	.p2align 4,,10
	.p2align 3
.L1729:
	cvttsd2siq	7(%rax), %rdx
	jmp	.L1754
	.p2align 4,,10
	.p2align 3
.L1765:
	movq	%r12, %rdi
	leaq	.LC55(%rip), %rsi
	call	_ZN2v88internal12StringStream3AddEPKc
	movq	(%rbx), %rax
	leaq	-88(%rbp), %rdi
	movq	%r12, %rsi
	movq	23(%rax), %rax
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEPNS0_12StringStreamE@PLT
	jmp	.L1748
	.p2align 4,,10
	.p2align 3
.L1772:
	leaq	.LC50(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3AddEPKc
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1771:
	movq	31(%r13), %r13
	testb	$1, %r13b
	je	.L1738
	jmp	.L1740
	.p2align 4,,10
	.p2align 3
.L1774:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L1736
	movq	23(%rax), %rax
	jmp	.L1736
.L1773:
	movl	%edx, -104(%rbp)
	movq	23(%r13), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4Heap8ContainsENS0_10HeapObjectE@PLT
	movl	-104(%rbp), %edx
	testb	%al, %al
	jne	.L1742
	leaq	.LC51(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3AddEPKc
	movl	-104(%rbp), %edx
	jmp	.L1743
.L1742:
	movl	%edx, -104(%rbp)
	movq	23(%r13), %rax
	leaq	-88(%rbp), %rdi
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo4NameEv
	movl	-104(%rbp), %edx
	movq	%rax, %r13
	movl	11(%rax), %eax
	testl	%eax, %eax
	jle	.L1743
	cmpw	$1026, %dx
	leaq	.LC31(%rip), %rax
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	cmovne	%rax, %rsi
	call	_ZN2v88internal12StringStream3AddEPKc
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3PutENS0_6StringE@PLT
	movl	15(%r15), %eax
	leaq	-80(%rbp), %rcx
	movq	%r12, %rdi
	leaq	.LC33(%rip), %rdx
	movl	$2, %r8d
	leaq	.LC52(%rip), %rsi
	movq	%r15, -72(%rbp)
	testl	$16777216, %eax
	leaq	.LC32(%rip), %rax
	cmove	%rdx, %rax
	movl	$11, %edx
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L1741
.L1766:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21479:
	.size	_ZN2v88internal8JSObject18JSObjectShortPrintEPNS0_12StringStreamE, .-_ZN2v88internal8JSObject18JSObjectShortPrintEPNS0_12StringStreamE
	.section	.rodata._ZN2v88internal8JSObject23PrintElementsTransitionEP8_IO_FILENS0_6HandleIS1_EENS0_12ElementsKindENS4_INS0_14FixedArrayBaseEEES6_S8_.str1.1,"aMS",@progbits,1
.LC56:
	.string	"elements transition ["
.LC57:
	.string	" -> "
.LC58:
	.string	"] in "
.LC59:
	.string	" for "
.LC60:
	.string	" from "
.LC61:
	.string	" to "
.LC62:
	.string	"\n"
	.section	.text._ZN2v88internal8JSObject23PrintElementsTransitionEP8_IO_FILENS0_6HandleIS1_EENS0_12ElementsKindENS4_INS0_14FixedArrayBaseEEES6_S8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject23PrintElementsTransitionEP8_IO_FILENS0_6HandleIS1_EENS0_12ElementsKindENS4_INS0_14FixedArrayBaseEEES6_S8_
	.type	_ZN2v88internal8JSObject23PrintElementsTransitionEP8_IO_FILENS0_6HandleIS1_EENS0_12ElementsKindENS4_INS0_14FixedArrayBaseEEES6_S8_, @function
_ZN2v88internal8JSObject23PrintElementsTransitionEP8_IO_FILENS0_6HandleIS1_EENS0_12ElementsKindENS4_INS0_14FixedArrayBaseEEES6_S8_:
.LFB21480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, -424(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	%r8b, %dl
	movl	%edx, -432(%rbp)
	jne	.L1783
.L1775:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1784
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1783:
	.cfi_restore_state
	leaq	-400(%rbp), %r15
	movq	%rdi, %r12
	movq	%rsi, %r13
	movq	%rdi, %rsi
	movq	%r15, %rdi
	movq	%rcx, %r14
	movl	%r8d, %ebx
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movq	%r15, %rdi
	movl	$21, %edx
	leaq	.LC56(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-432(%rbp), %r10d
	movzbl	%r10b, %edi
	call	_ZN2v88internal20ElementsKindToStringENS0_12ElementsKindE@PLT
	testq	%rax, %rax
	je	.L1785
	movq	%rax, %rdi
	movq	%rax, -432(%rbp)
	call	strlen@PLT
	movq	-432(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1778:
	movl	$4, %edx
	leaq	.LC57(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	%bl, %edi
	call	_ZN2v88internal20ElementsKindToStringENS0_12ElementsKindE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1786
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1780:
	movq	%r15, %rdi
	movl	$5, %edx
	leaq	.LC58(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal15JavaScriptFrame8PrintTopEPNS0_7IsolateEP8_IO_FILEbb@PLT
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC59(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	0(%r13), %rax
	leaq	-408(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -408(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC60(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	(%r14), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -408(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC61(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-424(%rbp), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -408(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC62(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-320(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1775
	.p2align 4,,10
	.p2align 3
.L1785:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1778
	.p2align 4,,10
	.p2align 3
.L1786:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1780
.L1784:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21480:
	.size	_ZN2v88internal8JSObject23PrintElementsTransitionEP8_IO_FILENS0_6HandleIS1_EENS0_12ElementsKindENS4_INS0_14FixedArrayBaseEEES6_S8_, .-_ZN2v88internal8JSObject23PrintElementsTransitionEP8_IO_FILENS0_6HandleIS1_EENS0_12ElementsKindENS4_INS0_14FixedArrayBaseEEES6_S8_
	.section	.rodata._ZN2v88internal8JSObject22PrintInstanceMigrationEP8_IO_FILENS0_3MapES4_.str1.1,"aMS",@progbits,1
.LC63:
	.string	"[migrating to slow]\n"
.LC64:
	.string	"[migrating]"
.LC65:
	.string	":%s->%s "
.LC66:
	.string	"{symbol %p}"
.LC67:
	.string	"elements_kind[%i->%i]"
	.section	.text._ZN2v88internal8JSObject22PrintInstanceMigrationEP8_IO_FILENS0_3MapES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject22PrintInstanceMigrationEP8_IO_FILENS0_3MapES4_
	.type	_ZN2v88internal8JSObject22PrintInstanceMigrationEP8_IO_FILENS0_3MapES4_, @function
_ZN2v88internal8JSObject22PrintInstanceMigrationEP8_IO_FILENS0_3MapES4_:
.LFB21481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -112(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movl	15(%rcx), %eax
	testl	$2097152, %eax
	jne	.L1820
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC64(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %rax
	leaq	.L1795(%rip), %r13
	leaq	.L1801(%rip), %r12
	movq	39(%rdi), %rdi
	movq	39(%rax), %rbx
	addq	$15, %rax
	movq	%rax, -88(%rbp)
	movq	%rdi, -96(%rbp)
	leaq	31(%rbx), %r15
	movq	%rbx, -104(%rbp)
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1790:
	movq	-88(%rbp), %rax
	movl	(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%eax, %ebx
	jge	.L1791
	movq	(%r15), %rax
	movq	-96(%rbp), %rcx
	addl	$1, %ebx
	subq	-104(%rbp), %rcx
	shrq	$38, %rax
	addq	%r15, %rcx
	andl	$7, %eax
	movb	%al, -74(%rbp)
	movq	(%rcx), %rdx
	movl	%eax, -72(%rbp)
	shrq	$38, %rdx
	andl	$7, %edx
	cmpb	%al, %dl
	movb	%dl, -73(%rbp)
	movl	%edx, -68(%rbp)
	je	.L1792
	movq	-8(%r15), %rcx
	leaq	-64(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal6String7PrintOnEP8_IO_FILE@PLT
	movl	-68(%rbp), %edx
	cmpb	$4, %dl
	ja	.L1793
	movzbl	-73(%rbp), %r10d
	movl	-72(%rbp), %eax
	movzbl	-74(%rbp), %r9d
	movslq	0(%r13,%r10,4), %rdx
	addq	%r13, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal8JSObject22PrintInstanceMigrationEP8_IO_FILENS0_3MapES4_,"a",@progbits
	.align 4
	.align 4
.L1795:
	.long	.L1799-.L1795
	.long	.L1813-.L1795
	.long	.L1797-.L1795
	.long	.L1796-.L1795
	.long	.L1794-.L1795
	.section	.text._ZN2v88internal8JSObject22PrintInstanceMigrationEP8_IO_FILENS0_3MapES4_
	.p2align 4,,10
	.p2align 3
.L1813:
	leaq	.LC2(%rip), %rcx
.L1798:
	cmpb	$4, %al
	ja	.L1793
	movslq	(%r12,%r9,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8JSObject22PrintInstanceMigrationEP8_IO_FILENS0_3MapES4_
	.align 4
	.align 4
.L1801:
	.long	.L1805-.L1801
	.long	.L1814-.L1801
	.long	.L1803-.L1801
	.long	.L1802-.L1801
	.long	.L1800-.L1801
	.section	.text._ZN2v88internal8JSObject22PrintInstanceMigrationEP8_IO_FILENS0_3MapES4_
	.p2align 4,,10
	.p2align 3
.L1794:
	leaq	.LC4(%rip), %rcx
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L1799:
	leaq	.LC1(%rip), %rcx
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L1796:
	leaq	.LC0(%rip), %rcx
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L1797:
	leaq	.LC3(%rip), %rcx
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L1814:
	leaq	.LC2(%rip), %rdx
.L1804:
	leaq	.LC65(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L1806:
	addq	$24, %r15
	jmp	.L1790
	.p2align 4,,10
	.p2align 3
.L1800:
	leaq	.LC4(%rip), %rdx
	jmp	.L1804
	.p2align 4,,10
	.p2align 3
.L1805:
	leaq	.LC1(%rip), %rdx
	jmp	.L1804
	.p2align 4,,10
	.p2align 3
.L1802:
	leaq	.LC0(%rip), %rdx
	jmp	.L1804
	.p2align 4,,10
	.p2align 3
.L1803:
	leaq	.LC3(%rip), %rdx
	jmp	.L1804
	.p2align 4,,10
	.p2align 3
.L1792:
	movq	(%r15), %rax
	btq	$33, %rax
	jnc	.L1806
	movq	(%rcx), %rax
	btq	$33, %rax
	jc	.L1806
	movq	-8(%r15), %rdx
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	ja	.L1818
	leaq	-64(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal6String7PrintOnEP8_IO_FILE@PLT
.L1810:
	leaq	.LC41(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1791:
	movq	-112(%rbp), %rax
	movzbl	14(%rax), %edx
	movq	-120(%rbp), %rax
	movzbl	14(%rax), %ecx
	shrl	$3, %edx
	shrl	$3, %ecx
	cmpb	%dl, %cl
	jne	.L1821
.L1811:
	leaq	.LC62(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L1787:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1822
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1821:
	.cfi_restore_state
	leaq	.LC67(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	jmp	.L1811
	.p2align 4,,10
	.p2align 3
.L1820:
	leaq	.LC63(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	jmp	.L1787
	.p2align 4,,10
	.p2align 3
.L1818:
	leaq	.LC66(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	jmp	.L1810
.L1793:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1822:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21481:
	.size	_ZN2v88internal8JSObject22PrintInstanceMigrationEP8_IO_FILENS0_3MapES4_, .-_ZN2v88internal8JSObject22PrintInstanceMigrationEP8_IO_FILENS0_3MapES4_
	.section	.text._ZN2v88internal8JSObject21IsUnmodifiedApiObjectENS0_14FullObjectSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject21IsUnmodifiedApiObjectENS0_14FullObjectSlotE
	.type	_ZN2v88internal8JSObject21IsUnmodifiedApiObjectENS0_14FullObjectSlotE, @function
_ZN2v88internal8JSObject21IsUnmodifiedApiObjectENS0_14FullObjectSlotE:
.LFB21482:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	je	.L1824
.L1826:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1824:
	movq	-1(%rdx), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L1826
	movq	-1(%rdx), %rcx
	movzwl	11(%rcx), %ecx
	subw	$1040, %cx
	testw	$-17, %cx
	jne	.L1826
	movq	-1(%rdx), %rcx
.L1844:
	movq	31(%rcx), %rcx
	testb	$1, %cl
	jne	.L1846
	ret
	.p2align 4,,10
	.p2align 3
.L1846:
	movq	-1(%rcx), %rsi
	leaq	-1(%rcx), %rdi
	cmpw	$68, 11(%rsi)
	je	.L1844
	movq	(%rdi), %rax
	cmpw	$1105, 11(%rax)
	jne	.L1826
	movq	15(%rdx), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	jne	.L1826
	movq	7(%rdx), %rax
	testb	$1, %al
	jne	.L1830
.L1845:
	shrq	$32, %rax
.L1831:
	testl	%eax, %eax
	je	.L1837
	salq	$32, %rax
.L1836:
	testb	$1, %al
	je	.L1826
	movq	%rax, %rsi
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	cmpq	%rax, -37504(%rsi)
	jne	.L1826
	movq	-1(%rdx), %rax
	cmpq	%rax, 55(%rcx)
	sete	%al
	ret
.L1830:
	movq	-1(%rax), %rsi
	cmpw	$158, 11(%rsi)
	je	.L1832
	movq	-1(%rax), %rsi
	cmpw	$130, 11(%rsi)
	je	.L1835
	movq	-1(%rax), %rsi
	cmpw	$131, 11(%rsi)
	je	.L1835
.L1837:
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	jmp	.L1836
.L1835:
	movq	47(%rax), %rax
	jmp	.L1845
.L1832:
	movslq	11(%rax), %rax
	shrl	$10, %eax
	andl	$2097151, %eax
	jmp	.L1831
	.cfi_endproc
.LFE21482:
	.size	_ZN2v88internal8JSObject21IsUnmodifiedApiObjectENS0_14FullObjectSlotE, .-_ZN2v88internal8JSObject21IsUnmodifiedApiObjectENS0_14FullObjectSlotE
	.section	.text._ZN2v88internal8JSObject26SetPropertyWithInterceptorEPNS0_14LookupIteratorENS_5MaybeINS0_11ShouldThrowEEENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject26SetPropertyWithInterceptorEPNS0_14LookupIteratorENS_5MaybeINS0_11ShouldThrowEEENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal8JSObject26SetPropertyWithInterceptorEPNS0_14LookupIteratorENS_5MaybeINS0_11ShouldThrowEEENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal8JSObject26SetPropertyWithInterceptorEPNS0_14LookupIteratorENS_5MaybeINS0_11ShouldThrowEEENS0_6HandleINS0_6ObjectEEE:
.LFB21489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZNK2v88internal14LookupIterator14GetInterceptorEv
	addq	$8, %rsp
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12_GLOBAL__N_134SetPropertyWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEENS_5MaybeINS0_11ShouldThrowEEENS4_INS0_6ObjectEEE
	.cfi_endproc
.LFE21489:
	.size	_ZN2v88internal8JSObject26SetPropertyWithInterceptorEPNS0_14LookupIteratorENS_5MaybeINS0_11ShouldThrowEEENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal8JSObject26SetPropertyWithInterceptorEPNS0_14LookupIteratorENS_5MaybeINS0_11ShouldThrowEEENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal8JSObject24GetElementsTransitionMapENS0_6HandleIS1_EENS0_12ElementsKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject24GetElementsTransitionMapENS0_6HandleIS1_EENS0_12ElementsKindE
	.type	_ZN2v88internal8JSObject24GetElementsTransitionMapENS0_6HandleIS1_EENS0_12ElementsKindE, @function
_ZN2v88internal8JSObject24GetElementsTransitionMapENS0_6HandleIS1_EENS0_12ElementsKindE:
.LFB21490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movl	%esi, %ebx
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %r12
	movq	-1(%rax), %r14
	movq	3520(%r12), %rdi
	subq	$37592, %r12
	testq	%rdi, %rdi
	je	.L1850
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1851:
	movq	0(%r13), %rax
	movzbl	%bl, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	andq	$-262144, %rax
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	jmp	_ZN2v88internal3Map20TransitionElementsToEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	.p2align 4,,10
	.p2align 3
.L1850:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1854
.L1852:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L1851
	.p2align 4,,10
	.p2align 3
.L1854:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1852
	.cfi_endproc
.LFE21490:
	.size	_ZN2v88internal8JSObject24GetElementsTransitionMapENS0_6HandleIS1_EENS0_12ElementsKindE, .-_ZN2v88internal8JSObject24GetElementsTransitionMapENS0_6HandleIS1_EENS0_12ElementsKindE
	.section	.text._ZN2v88internal8JSObject16GetFunctionRealmENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject16GetFunctionRealmENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSObject16GetFunctionRealmENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSObject16GetFunctionRealmENS0_6HandleIS1_EE:
.LFB21491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-16(%rbp), %rdi
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal10JSReceiver18GetCreationContextEv
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1858
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1858:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21491:
	.size	_ZN2v88internal8JSObject16GetFunctionRealmENS0_6HandleIS1_EE, .-_ZN2v88internal8JSObject16GetFunctionRealmENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE.str1.8,"aMS",@progbits,1
	.align 8
.LC68:
	.string	"LookupIterator::ACCESS_CHECK != it.state()"
	.align 8
.LC69:
	.string	"Object::AddDataProperty(&it, value, attributes, Just(ShouldThrow::kThrowOnError), StoreOrigin::kNamed) .IsJust()"
	.section	.text._ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE
	.type	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE, @function
_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE:
.LFB21495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	movabsq	$824633720832, %rcx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	-1(%rax), %rdx
	movl	$0, -128(%rbp)
	movq	%rcx, -116(%rbp)
	movq	%rdi, -104(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L1865
.L1860:
	leaq	-128(%rbp), %r14
	movq	%rsi, -96(%rbp)
	movq	%r14, %rdi
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movl	-124(%rbp), %eax
	testl	%eax, %eax
	je	.L1866
	movl	$1, %r8d
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal6Object15AddDataPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS0_11StoreOriginE@PLT
	testb	%al, %al
	je	.L1867
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1868
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1865:
	.cfi_restore_state
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L1866:
	leaq	.LC68(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1867:
	leaq	.LC69(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1868:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21495:
	.size	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE, .-_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE
	.type	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE, @function
_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE:
.LFB21496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rdx, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	movq	%r15, -144(%rbp)
	movq	%r14, %rdi
	leaq	-144(%rbp), %r15
	movq	%r15, %rsi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%rax, %rsi
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	movq	%r14, -120(%rbp)
	movabsq	$824633720832, %rax
	movl	$0, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L1875
.L1870:
	movq	%r15, %rdi
	movq	%rsi, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movl	-140(%rbp), %eax
	testl	%eax, %eax
	je	.L1876
	movl	$1, %r8d
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal6Object15AddDataPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS0_11StoreOriginE@PLT
	testb	%al, %al
	je	.L1877
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1878
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1875:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L1870
	.p2align 4,,10
	.p2align 3
.L1876:
	leaq	.LC68(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1877:
	leaq	.LC69(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1878:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21496:
	.size	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE, .-_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE
	.type	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE, @function
_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE:
.LFB21498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$24, %rsp
	cmpl	$-1, 72(%rdi)
	movq	%rcx, -56(%rbp)
	jne	.L1880
	movq	32(%rdi), %rdx
	movq	24(%rdi), %rax
	movq	(%rdx), %rdx
	cmpq	%rdx, 3944(%rax)
	je	.L1881
	cmpq	2304(%rax), %rdx
	je	.L1881
	cmpq	2912(%rax), %rdx
	je	.L1881
	cmpq	3904(%rax), %rdx
	je	.L1881
	cmpq	3856(%rax), %rdx
	je	.L1881
	cmpq	3192(%rax), %rdx
	je	.L1881
	cmpq	3408(%rax), %rdx
	je	.L1881
	.p2align 4,,10
	.p2align 3
.L1880:
	movq	48(%r15), %rax
	movq	%rax, -64(%rbp)
	movl	4(%r15), %eax
	cmpl	$4, %eax
	je	.L1916
	leaq	.L1886(%rip), %r14
	leaq	.L1896(%rip), %r8
	cmpl	$1, %ebx
	je	.L1883
.L1893:
	cmpl	$7, %eax
	ja	.L1884
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE,"a",@progbits
	.align 4
	.align 4
.L1886:
	.long	.L1891-.L1886
	.long	.L1890-.L1886
	.long	.L1884-.L1886
	.long	.L1885-.L1886
	.long	.L1884-.L1886
	.long	.L1888-.L1886
	.long	.L1887-.L1886
	.long	.L1885-.L1886
	.section	.text._ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE
	.p2align 4,,10
	.p2align 3
.L1891:
	movq	%r15, %rdi
	call	_ZNK2v88internal14LookupIterator9HasAccessEv@PLT
	testb	%al, %al
	je	.L1892
.L1884:
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	4(%r15), %eax
	cmpl	$4, %eax
	jne	.L1893
.L1916:
	movq	-56(%rbp), %rcx
	addq	$24, %rsp
	movl	%r13d, %edx
	movq	%r12, %rsi
	popq	%rbx
	movq	%r15, %rdi
	popq	%r12
	movl	$1, %r8d
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6Object15AddDataPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS0_11StoreOriginE@PLT
	.p2align 4,,10
	.p2align 3
.L1887:
	.cfi_restore_state
	movl	16(%r15), %eax
	shrl	$3, %eax
	andl	$7, %eax
	cmpl	%eax, %r13d
	je	.L1937
	cmpl	$-1, 72(%r15)
	je	.L1915
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$17, %eax
	cmpb	$10, %al
	ja	.L1915
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator7GetNameEv
	movq	24(%r15), %rdi
	movq	-56(%rbp), %rcx
	movq	%r12, %rdx
	movq	%rax, %rsi
	jmp	.L1936
	.p2align 4,,10
	.p2align 3
.L1888:
	movq	%r15, %rdi
	call	_ZNK2v88internal14LookupIterator12GetAccessorsEv@PLT
	movq	(%rax), %rdx
	testb	$1, %dl
	jne	.L1903
.L1915:
	movq	%r12, %rsi
	movl	%r13d, %edx
	movq	%r15, %rdi
	movl	$257, %r12d
	call	_ZN2v88internal14LookupIterator23ReconfigureDataPropertyENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesE@PLT
.L1900:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1890:
	.cfi_restore_state
	movq	32(%r15), %r13
	movq	24(%r15), %rdi
	testq	%r13, %r13
	je	.L1938
.L1907:
	movq	-56(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
.L1936:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6Object28RedefineIncompatiblePropertyEPNS0_7IsolateENS0_6HandleIS1_EES5_NS_5MaybeINS0_11ShouldThrowEEE@PLT
	.p2align 4,,10
	.p2align 3
.L1898:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZNK2v88internal14LookupIterator9HasAccessEv@PLT
	testb	%al, %al
	je	.L1892
.L1895:
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	4(%r15), %eax
	leaq	.L1896(%rip), %r8
	cmpl	$4, %eax
	je	.L1916
.L1883:
	cmpl	$7, %eax
	ja	.L1895
	movslq	(%r8,%rax,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE
	.align 4
	.align 4
.L1896:
	.long	.L1898-.L1896
	.long	.L1890-.L1896
	.long	.L1897-.L1896
	.long	.L1885-.L1896
	.long	.L1895-.L1896
	.long	.L1888-.L1896
	.long	.L1887-.L1896
	.long	.L1885-.L1896
	.section	.text._ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE
	.p2align 4,,10
	.p2align 3
.L1881:
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator23InternalUpdateProtectorEv@PLT
	jmp	.L1880
	.p2align 4,,10
	.p2align 3
.L1903:
	movq	-1(%rdx), %rdx
	cmpw	$78, 11(%rdx)
	jne	.L1915
	cmpl	$1, %ebx
	jne	.L1915
	movl	16(%r15), %edx
	shrl	$3, %edx
	andl	$7, %edx
	cmpl	%edx, %r13d
	je	.L1906
	movl	%r13d, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator24TransitionToAccessorPairENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesE@PLT
.L1906:
	movq	-56(%rbp), %rdx
	addq	$24, %rsp
	movq	%r12, %rsi
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6Object23SetPropertyWithAccessorEPNS0_14LookupIteratorENS0_6HandleIS1_EENS_5MaybeINS0_11ShouldThrowEEE@PLT
	.p2align 4,,10
	.p2align 3
.L1937:
	.cfi_restore_state
	addq	$24, %rsp
	movq	%r12, %rsi
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6Object15SetDataPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EE@PLT
	.p2align 4,,10
	.p2align 3
.L1938:
	.cfi_restore_state
	movl	72(%r15), %r14d
	testl	%r14d, %r14d
	js	.L1908
	movq	%r14, %rsi
	movl	$1, %edx
	salq	$32, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb@PLT
	movq	%rax, %r13
.L1909:
	movq	0(%r13), %rax
	movl	11(%rax), %esi
	cmpl	$10, %esi
	jle	.L1939
.L1912:
	movq	%r13, 32(%r15)
	movq	24(%r15), %rdi
	jmp	.L1907
.L1885:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1897:
	movq	%r15, %rdi
	call	_ZNK2v88internal14LookupIterator14GetInterceptorEv
	movq	-56(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal12_GLOBAL__N_134SetPropertyWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEENS_5MaybeINS0_11ShouldThrowEEENS4_INS0_6ObjectEEE
	movl	%eax, %edx
	movzbl	%ah, %eax
	testb	%dl, %dl
	je	.L1901
	testb	%al, %al
	je	.L1895
.L1901:
	xorl	%r12d, %r12d
	movb	%dl, %r12b
	movl	%r12d, %ebx
	movb	%al, %bh
	movl	%ebx, %r12d
	jmp	.L1900
	.p2align 4,,10
	.p2align 3
.L1908:
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rax, %r13
	jmp	.L1909
.L1892:
	movq	24(%r15), %rdi
	movq	56(%r15), %rsi
	movl	$257, %r12d
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	24(%r15), %rdi
	movq	12552(%rdi), %rax
	cmpq	%rax, 96(%rdi)
	je	.L1900
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorb	%r12b, %r12b
	movl	%r12d, %eax
	movb	$0, %ah
	movl	%eax, %r12d
	jmp	.L1900
.L1939:
	cmpl	$3, 7(%rax)
	jne	.L1912
	movl	%r14d, %edi
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movq	0(%r13), %rdx
	movl	%eax, 7(%rdx)
	jmp	.L1912
	.cfi_endproc
.LFE21498:
	.size	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE, .-_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE
	.section	.text._ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS1_20AccessorInfoHandlingE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS1_20AccessorInfoHandlingE
	.type	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS1_20AccessorInfoHandlingE, @function
_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS1_20AccessorInfoHandlingE:
.LFB21497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE
	testb	%al, %al
	movl	$0, %eax
	cmovne	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21497:
	.size	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS1_20AccessorInfoHandlingE, .-_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS1_20AccessorInfoHandlingE
	.section	.text._ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE
	.type	_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE, @function
_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE:
.LFB21499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$104, %rsp
	movq	(%rdi), %rdx
	movq	(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	movq	-1(%rcx), %r8
	movl	$1, %edx
	subq	$37592, %rdi
	cmpw	$64, 11(%r8)
	jne	.L1945
	movl	11(%rcx), %edx
	notl	%edx
	andl	$1, %edx
.L1945:
	movabsq	$824633720832, %rcx
	movl	%edx, -128(%rbp)
	movq	(%rsi), %rdx
	movq	%rcx, -116(%rbp)
	movq	%rdi, -104(%rbp)
	movq	-1(%rdx), %rdx
	movzwl	11(%rdx), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L1953
.L1946:
	movq	%r12, -80(%rbp)
	movq	%r12, -64(%rbp)
	leaq	-128(%rbp), %r12
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -72(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	%rbx, %rsi
	movl	$1, %ecx
	movl	%r13d, %edx
	movl	$1, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE
	testb	%al, %al
	movl	$0, %eax
	cmovne	%rbx, %rax
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1954
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1953:
	.cfi_restore_state
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L1946
.L1954:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21499:
	.size	_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE, .-_ZN2v88internal8JSObject30SetOwnPropertyIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal8JSObject29SetOwnElementIgnoreAttributesENS0_6HandleIS1_EEjNS2_INS0_6ObjectEEENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject29SetOwnElementIgnoreAttributesENS0_6HandleIS1_EEjNS2_INS0_6ObjectEEENS0_18PropertyAttributesE
	.type	_ZN2v88internal8JSObject29SetOwnElementIgnoreAttributesENS0_6HandleIS1_EEjNS2_INS0_6ObjectEEENS0_18PropertyAttributesE, @function
_ZN2v88internal8JSObject29SetOwnElementIgnoreAttributesENS0_6HandleIS1_EEjNS2_INS0_6ObjectEEENS0_18PropertyAttributesE:
.LFB21500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-128(%rbp), %r13
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	movabsq	$824633720832, %rdx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	%rdi, -80(%rbp)
	movq	%rdi, -64(%rbp)
	movq	%r13, %rdi
	subq	$37592, %rax
	movq	%rdx, -116(%rbp)
	movq	%rax, -104(%rbp)
	movl	%esi, -56(%rbp)
	movl	$1, -128(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -72(%rbp)
	movl	$-1, -52(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%rbx, %rsi
	movl	$1, %ecx
	movl	%r12d, %edx
	movl	$1, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE
	testb	%al, %al
	movl	$0, %eax
	cmovne	%rbx, %rax
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1960
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1960:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21500:
	.size	_ZN2v88internal8JSObject29SetOwnElementIgnoreAttributesENS0_6HandleIS1_EEjNS2_INS0_6ObjectEEENS0_18PropertyAttributesE, .-_ZN2v88internal8JSObject29SetOwnElementIgnoreAttributesENS0_6HandleIS1_EEjNS2_INS0_6ObjectEEENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal8JSObject39DefinePropertyOrElementIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject39DefinePropertyOrElementIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE
	.type	_ZN2v88internal8JSObject39DefinePropertyOrElementIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE, @function
_ZN2v88internal8JSObject39DefinePropertyOrElementIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE:
.LFB21501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	movq	%rsi, %rcx
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r14
	movq	(%rsi), %rax
	subq	$37592, %r14
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1983
.L1963:
	movq	(%r12), %rsi
	movl	$1, %edx
	movq	-1(%rsi), %rdi
	cmpw	$64, 11(%rdi)
	je	.L1984
.L1967:
	movabsq	$824633720832, %rsi
	movl	%edx, -224(%rbp)
	movq	%rsi, -212(%rbp)
	movq	%r14, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L1985
.L1968:
	leaq	-224(%rbp), %r12
	movq	%rcx, -192(%rbp)
	movq	%r12, %rdi
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%r13, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L1966:
	movq	%rbx, %rsi
	movl	$1, %r8d
	movl	%r15d, %edx
	movq	%r12, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE
	testb	%al, %al
	movl	$0, %eax
	cmovne	%rbx, %rax
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1986
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1983:
	.cfi_restore_state
	movq	%rax, -144(%rbp)
	movl	7(%rax), %edx
	testb	$1, %dl
	jne	.L1964
	andl	$2, %edx
	jne	.L1963
.L1964:
	leaq	-144(%rbp), %rdi
	leaq	-228(%rbp), %rsi
	movq	%rcx, -256(%rbp)
	movq	%rdi, -248(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-248(%rbp), %rdi
	movq	-256(%rbp), %rcx
	testb	%al, %al
	je	.L1987
	movabsq	$824633720832, %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -132(%rbp)
	movl	-228(%rbp), %eax
	movl	$1, -144(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r12, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	leaq	-224(%rbp), %r12
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L1966
	.p2align 4,,10
	.p2align 3
.L1984:
	movl	11(%rsi), %edx
	notl	%edx
	andl	$1, %edx
	jmp	.L1967
	.p2align 4,,10
	.p2align 3
.L1985:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rcx
	jmp	.L1968
	.p2align 4,,10
	.p2align 3
.L1987:
	movq	(%r12), %rax
	jmp	.L1963
.L1986:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21501:
	.size	_ZN2v88internal8JSObject39DefinePropertyOrElementIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE, .-_ZN2v88internal8JSObject39DefinePropertyOrElementIgnoreAttributesENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal8JSObject36GetPropertyAttributesWithInterceptorEPNS0_14LookupIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject36GetPropertyAttributesWithInterceptorEPNS0_14LookupIteratorE
	.type	_ZN2v88internal8JSObject36GetPropertyAttributesWithInterceptorEPNS0_14LookupIteratorE, @function
_ZN2v88internal8JSObject36GetPropertyAttributesWithInterceptorEPNS0_14LookupIteratorE:
.LFB21502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	56(%rdi), %rax
	cmpl	$-1, 72(%rdi)
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	movq	31(%rax), %rax
	je	.L1989
	testb	$1, %al
	jne	.L1992
.L1993:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %r13
	cmpq	%r13, %rdx
	je	.L1995
	movq	39(%rdx), %r13
.L1995:
	movq	24(%r12), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2001
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2002:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12_GLOBAL__N_144GetPropertyAttributesWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEE
	.p2align 4,,10
	.p2align 3
.L2007:
	.cfi_restore_state
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L1993
	.p2align 4,,10
	.p2align 3
.L1992:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L2007
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1993
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L1993
	.p2align 4,,10
	.p2align 3
.L2001:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L2008
.L2003:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r13, (%rsi)
	jmp	.L2002
	.p2align 4,,10
	.p2align 3
.L1989:
	testb	$1, %al
	jne	.L1998
.L1999:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %r13
	cmpq	%r13, %rdx
	je	.L1995
	movq	31(%rdx), %r13
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L2009:
	movq	31(%rax), %rax
	testb	$1, %al
	je	.L1999
	.p2align 4,,10
	.p2align 3
.L1998:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L2009
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1999
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L1999
	.p2align 4,,10
	.p2align 3
.L2008:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2003
	.cfi_endproc
.LFE21502:
	.size	_ZN2v88internal8JSObject36GetPropertyAttributesWithInterceptorEPNS0_14LookupIteratorE, .-_ZN2v88internal8JSObject36GetPropertyAttributesWithInterceptorEPNS0_14LookupIteratorE
	.section	.text._ZN2v88internal8JSObject19RequireSlowElementsENS0_16NumberDictionaryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject19RequireSlowElementsENS0_16NumberDictionaryE
	.type	_ZN2v88internal8JSObject19RequireSlowElementsENS0_16NumberDictionaryE, @function
_ZN2v88internal8JSObject19RequireSlowElementsENS0_16NumberDictionaryE:
.LFB21505:
	.cfi_startproc
	endbr64
	movq	39(%rsi), %rax
	testb	$1, %al
	jne	.L2011
	btq	$32, %rax
	jc	.L2010
.L2011:
	movabsq	$4294967296, %rax
	movq	%rax, 39(%rsi)
	movq	(%rdi), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$1048576, %edx
	jne	.L2023
.L2010:
	ret
	.p2align 4,,10
	.p2align 3
.L2023:
	movq	-1(%rax), %rdi
	jmp	_ZN2v88internal12_GLOBAL__N_133InvalidatePrototypeChainsInternalENS0_3MapE
	.cfi_endproc
.LFE21505:
	.size	_ZN2v88internal8JSObject19RequireSlowElementsENS0_16NumberDictionaryE, .-_ZN2v88internal8JSObject19RequireSlowElementsENS0_16NumberDictionaryE
	.section	.rodata._ZN2v88internal8JSObject29DeletePropertyWithInterceptorEPNS0_14LookupIteratorENS0_11ShouldThrowE.str1.1,"aMS",@progbits,1
.LC70:
	.string	"interceptor-indexed-deleter"
.LC71:
	.string	"interceptor-named-deleter"
	.section	.text._ZN2v88internal8JSObject29DeletePropertyWithInterceptorEPNS0_14LookupIteratorENS0_11ShouldThrowE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject29DeletePropertyWithInterceptorEPNS0_14LookupIteratorENS0_11ShouldThrowE
	.type	_ZN2v88internal8JSObject29DeletePropertyWithInterceptorEPNS0_14LookupIteratorENS0_11ShouldThrowE, @function
_ZN2v88internal8JSObject29DeletePropertyWithInterceptorEPNS0_14LookupIteratorENS0_11ShouldThrowE:
.LFB21507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movq	24(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal14LookupIterator14GetInterceptorEv
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	39(%rax), %rcx
	cmpq	%rcx, 88(%r13)
	je	.L2028
	movq	48(%rbx), %rsi
	movq	56(%rbx), %r15
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L2027
.L2030:
	movq	%r13, %rdi
	call	_ZN2v88internal6Object15ConvertReceiverEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2028
	movq	(%r12), %rax
.L2031:
	movq	%r14, %r9
	movq	(%rsi), %rcx
	movq	63(%rax), %rdx
	movq	%r13, %rsi
	movq	(%r15), %r8
	salq	$32, %r9
	leaq	-144(%rbp), %rdi
	orq	$1, %r9
	call	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movl	72(%rbx), %r14d
	cmpl	$-1, %r14d
	je	.L2032
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %r15
	movq	$0, -176(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2096
.L2033:
	movq	(%r12), %rax
	movq	39(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L2059
	movq	7(%rax), %r12
	movq	%r12, %rbx
.L2034:
	cmpl	$32, 41828(%r15)
	je	.L2097
.L2035:
	movl	12616(%r15), %eax
	movl	$6, 12616(%r15)
	movq	%r12, -232(%rbp)
	leaq	-240(%rbp), %r12
	movq	%r15, -240(%rbp)
	movl	%eax, -264(%rbp)
	movq	12608(%r15), %rax
	movq	%rax, -224(%rbp)
	movq	%r12, 12608(%r15)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	testq	%rdx, %rdx
	je	.L2098
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L2099
.L2040:
	leaq	-120(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	41016(%r15), %rdi
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-272(%rbp), %rdi
	testb	%al, %al
	jne	.L2100
.L2044:
	leaq	-248(%rbp), %rsi
	movl	%r14d, %edi
	call	*%rbx
	movq	96(%r15), %rax
	xorl	%ebx, %ebx
	cmpq	%rax, -88(%rbp)
	je	.L2053
.L2095:
	leaq	-88(%rbp), %rbx
.L2053:
	movq	%r12, %rdi
	call	_ZN2v88internal21ExternalCallbackScopeD1Ev
	movl	-264(%rbp), %eax
	movl	%eax, 12616(%r15)
.L2051:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2101
.L2047:
	movq	12552(%r13), %rax
	cmpq	%rax, 96(%r13)
	je	.L2055
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
	movb	$0, %ah
.L2056:
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rcx
	movq	%rcx, 41704(%rdx)
.L2026:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2102
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2027:
	.cfi_restore_state
	movq	-1(%rdx), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L2030
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2028:
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L2055:
	xorl	%eax, %eax
	movb	$0, %ah
	testq	%rbx, %rbx
	je	.L2056
	movq	(%rbx), %rax
	cmpq	%rax, 112(%r13)
	movl	$1, %eax
	sete	%dl
	movb	%dl, %ah
	jmp	.L2056
	.p2align 4,,10
	.p2align 3
.L2032:
	pxor	%xmm0, %xmm0
	movq	32(%rbx), %r14
	movq	-104(%rbp), %r15
	movq	$0, -176(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2103
.L2048:
	movq	(%r12), %rax
	movq	39(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L2062
	movq	7(%rax), %r9
	movq	%r9, %rbx
.L2049:
	cmpl	$32, 41828(%r15)
	je	.L2104
.L2050:
	movl	12616(%r15), %eax
	leaq	-240(%rbp), %r12
	movq	%r9, %rdx
	movq	%r15, %rsi
	movl	$6, 12616(%r15)
	movq	%r12, %rdi
	movl	%eax, -264(%rbp)
	call	_ZN2v88internal21ExternalCallbackScopeC1EPNS0_7IsolateEm
	leaq	-120(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	41016(%r15), %rdi
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-272(%rbp), %rdi
	testb	%al, %al
	jne	.L2105
.L2052:
	leaq	-248(%rbp), %rsi
	movq	%r14, %rdi
	call	*%rbx
	movq	96(%r15), %rax
	xorl	%ebx, %ebx
	cmpq	%rax, -88(%rbp)
	jne	.L2095
	jmp	.L2053
	.p2align 4,,10
	.p2align 3
.L2097:
	movq	41472(%r15), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	testb	%al, %al
	jne	.L2035
	xorl	%ebx, %ebx
	jmp	.L2051
	.p2align 4,,10
	.p2align 3
.L2098:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2106
.L2039:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	movzbl	(%rdx), %eax
	testb	$5, %al
	je	.L2040
.L2099:
	pxor	%xmm0, %xmm0
	movq	%rdx, -272(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-272(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L2107
.L2041:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2042
	movq	(%rdi), %rax
	call	*8(%rax)
.L2042:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2040
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2040
	.p2align 4,,10
	.p2align 3
.L2100:
	movq	-112(%rbp), %rdx
	movl	%r14d, %ecx
	leaq	.LC70(%rip), %rsi
	call	_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj@PLT
	jmp	.L2044
	.p2align 4,,10
	.p2align 3
.L2104:
	movq	41472(%r15), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r9, -264(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-264(%rbp), %r9
	testb	%al, %al
	jne	.L2050
	xorl	%ebx, %ebx
	jmp	.L2051
	.p2align 4,,10
	.p2align 3
.L2101:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2047
	.p2align 4,,10
	.p2align 3
.L2105:
	movq	(%r14), %rcx
	movq	-112(%rbp), %rdx
	leaq	.LC71(%rip), %rsi
	call	_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE@PLT
	jmp	.L2052
	.p2align 4,,10
	.p2align 3
.L2096:
	movq	40960(%r15), %rax
	leaq	-200(%rbp), %rsi
	movl	$155, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2033
	.p2align 4,,10
	.p2align 3
.L2059:
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	jmp	.L2034
	.p2align 4,,10
	.p2align 3
.L2107:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC15(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	addq	$64, %rsp
	jmp	.L2041
	.p2align 4,,10
	.p2align 3
.L2106:
	leaq	.LC14(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L2039
	.p2align 4,,10
	.p2align 3
.L2103:
	movq	40960(%r15), %rax
	leaq	-200(%rbp), %rsi
	movl	$170, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2048
	.p2align 4,,10
	.p2align 3
.L2062:
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
	jmp	.L2049
.L2102:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21507:
	.size	_ZN2v88internal8JSObject29DeletePropertyWithInterceptorEPNS0_14LookupIteratorENS0_11ShouldThrowE, .-_ZN2v88internal8JSObject29DeletePropertyWithInterceptorEPNS0_14LookupIteratorENS0_11ShouldThrowE
	.section	.text._ZN2v88internal10JSReceiver14DeletePropertyEPNS0_14LookupIteratorENS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver14DeletePropertyEPNS0_14LookupIteratorENS0_12LanguageModeE
	.type	_ZN2v88internal10JSReceiver14DeletePropertyEPNS0_14LookupIteratorENS0_12LanguageModeE, @function
_ZN2v88internal10JSReceiver14DeletePropertyEPNS0_14LookupIteratorENS0_12LanguageModeE:
.LFB21431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	cmpl	$-1, 72(%rdi)
	movq	24(%rdi), %r15
	jne	.L2109
	movq	32(%rdi), %r14
	movq	(%r14), %rax
	cmpq	%rax, 3944(%r15)
	je	.L2110
	cmpq	2304(%r15), %rax
	je	.L2110
	cmpq	2912(%r15), %rax
	je	.L2110
	cmpq	3904(%r15), %rax
	je	.L2110
	cmpq	3856(%r15), %rax
	je	.L2110
	cmpq	3192(%r15), %rax
	je	.L2110
	cmpq	3408(%r15), %rax
	je	.L2110
	cmpl	$3, 4(%rdi)
	jne	.L2112
	jmp	.L2113
	.p2align 4,,10
	.p2align 3
.L2110:
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator23InternalUpdateProtectorEv@PLT
	movq	24(%r12), %r15
.L2109:
	cmpl	$3, 4(%r12)
	je	.L2150
.L2112:
	movq	48(%r12), %rax
	movq	%rax, -56(%rbp)
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L2151
.L2120:
	movl	4(%r12), %eax
	cmpl	$4, %eax
	je	.L2148
	xorl	$1, %r13d
	leaq	.L2128(%rip), %r14
	movzbl	%r13b, %r13d
.L2138:
	cmpl	$7, %eax
	ja	.L2126
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10JSReceiver14DeletePropertyEPNS0_14LookupIteratorENS0_12LanguageModeE,"a",@progbits
	.align 4
	.align 4
.L2128:
	.long	.L2132-.L2128
	.long	.L2148-.L2128
	.long	.L2130-.L2128
	.long	.L2127-.L2128
	.long	.L2126-.L2128
	.long	.L2129-.L2128
	.long	.L2129-.L2128
	.long	.L2127-.L2128
	.section	.text._ZN2v88internal10JSReceiver14DeletePropertyEPNS0_14LookupIteratorENS0_12LanguageModeE
	.p2align 4,,10
	.p2align 3
.L2151:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jne	.L2120
	cmpl	$4, 4(%r12)
	je	.L2148
.L2137:
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator6DeleteEv@PLT
.L2148:
	movl	$257, %eax
.L2146:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2130:
	.cfi_restore_state
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject29DeletePropertyWithInterceptorEPNS0_14LookupIteratorENS0_11ShouldThrowE
	movq	12480(%r15), %rdx
	cmpq	%rdx, 96(%r15)
	jne	.L2149
	testb	%al, %al
	jne	.L2146
.L2126:
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	4(%r12), %eax
	cmpl	$4, %eax
	jne	.L2138
	jmp	.L2148
	.p2align 4,,10
	.p2align 3
.L2129:
	testb	$32, 16(%r12)
	je	.L2137
	testb	%bl, %bl
	jne	.L2152
.L2133:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2132:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal14LookupIterator9HasAccessEv@PLT
	testb	%al, %al
	jne	.L2126
	movq	56(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	12552(%r15), %rax
	cmpq	%rax, 96(%r15)
	je	.L2133
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L2146
	.p2align 4,,10
	.p2align 3
.L2152:
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator7GetNameEv
	movq	-56(%rbp), %rcx
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rdx
	movl	$163, %esi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L2149:
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L2146
	.p2align 4,,10
	.p2align 3
.L2150:
	movq	32(%r12), %r14
	testq	%r14, %r14
	je	.L2153
.L2113:
	movq	56(%r12), %rdi
	addq	$24, %rsp
	movl	%ebx, %edx
	movq	%r14, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7JSProxy23DeletePropertyOrElementENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE@PLT
.L2127:
	.cfi_restore_state
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2153:
	movl	72(%r12), %r13d
	testl	%r13d, %r13d
	js	.L2114
	movq	%r13, %rsi
	movl	$1, %edx
	movq	%r15, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb@PLT
	movq	%rax, %r14
.L2115:
	movq	(%r14), %rax
	movl	11(%rax), %esi
	cmpl	$10, %esi
	jle	.L2154
.L2118:
	movq	%r14, 32(%r12)
	jmp	.L2113
.L2114:
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rax, %r14
	jmp	.L2115
.L2154:
	cmpl	$3, 7(%rax)
	jne	.L2118
	movl	%r13d, %edi
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movq	(%r14), %rdx
	movl	%eax, 7(%rdx)
	jmp	.L2118
	.cfi_endproc
.LFE21431:
	.size	_ZN2v88internal10JSReceiver14DeletePropertyEPNS0_14LookupIteratorENS0_12LanguageModeE, .-_ZN2v88internal10JSReceiver14DeletePropertyEPNS0_14LookupIteratorENS0_12LanguageModeE
	.section	.text._ZN2v88internal10JSReceiver13DeleteElementENS0_6HandleIS1_EEjNS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver13DeleteElementENS0_6HandleIS1_EEjNS0_12LanguageModeE
	.type	_ZN2v88internal10JSReceiver13DeleteElementENS0_6HandleIS1_EEjNS0_12LanguageModeE, @function
_ZN2v88internal10JSReceiver13DeleteElementENS0_6HandleIS1_EEjNS0_12LanguageModeE:
.LFB21432:
	.cfi_startproc
	endbr64
	movabsq	$824633720832, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-112(%rbp), %r12
	movl	%edx, %ebx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	%rdi, -64(%rbp)
	movq	%rdi, -48(%rbp)
	movq	%r12, %rdi
	subq	$37592, %rax
	movq	%rcx, -100(%rbp)
	movq	%rax, -88(%rbp)
	movl	%esi, -40(%rbp)
	movl	$1, -112(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -56(%rbp)
	movl	$-1, -36(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver14DeletePropertyEPNS0_14LookupIteratorENS0_12LanguageModeE
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2158
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2158:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21432:
	.size	_ZN2v88internal10JSReceiver13DeleteElementENS0_6HandleIS1_EEjNS0_12LanguageModeE, .-_ZN2v88internal10JSReceiver13DeleteElementENS0_6HandleIS1_EEjNS0_12LanguageModeE
	.section	.text._ZN2v88internal10JSReceiver14DeletePropertyENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver14DeletePropertyENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE
	.type	_ZN2v88internal10JSReceiver14DeletePropertyENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE, @function
_ZN2v88internal10JSReceiver14DeletePropertyENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE:
.LFB21433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rdi), %rdx
	movq	(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	movq	-1(%rcx), %r8
	movl	$1, %edx
	subq	$37592, %rdi
	cmpw	$64, 11(%r8)
	jne	.L2160
	movl	11(%rcx), %edx
	notl	%edx
	andl	$1, %edx
.L2160:
	movabsq	$824633720832, %rcx
	movl	%edx, -128(%rbp)
	movq	(%rsi), %rdx
	movq	%rcx, -116(%rbp)
	movq	%rdi, -104(%rbp)
	movq	-1(%rdx), %rdx
	movzwl	11(%rdx), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L2166
.L2161:
	leaq	-128(%rbp), %r13
	movq	%rax, -96(%rbp)
	movq	%r13, %rdi
	movq	%rbx, -80(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -72(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver14DeletePropertyEPNS0_14LookupIteratorENS0_12LanguageModeE
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2167
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2166:
	.cfi_restore_state
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L2161
.L2167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21433:
	.size	_ZN2v88internal10JSReceiver14DeletePropertyENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE, .-_ZN2v88internal10JSReceiver14DeletePropertyENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE
	.section	.text._ZN2v88internal10JSReceiver23DeletePropertyOrElementENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver23DeletePropertyOrElementENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE
	.type	_ZN2v88internal10JSReceiver23DeletePropertyOrElementENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE, @function
_ZN2v88internal10JSReceiver23DeletePropertyOrElementENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE:
.LFB21434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r13
	movq	(%rsi), %rax
	subq	$37592, %r13
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L2188
.L2170:
	movq	(%r12), %rcx
	movl	$1, %edx
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	je	.L2189
.L2174:
	movabsq	$824633720832, %rcx
	movl	%edx, -224(%rbp)
	movq	%rcx, -212(%rbp)
	movq	%r13, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L2190
.L2175:
	leaq	-224(%rbp), %r12
	movq	%r15, -192(%rbp)
	movq	%r12, %rdi
	movq	$0, -184(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rbx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L2173:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver14DeletePropertyEPNS0_14LookupIteratorENS0_12LanguageModeE
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2191
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2188:
	.cfi_restore_state
	movq	%rax, -144(%rbp)
	movl	7(%rax), %edx
	testb	$1, %dl
	jne	.L2171
	andl	$2, %edx
	jne	.L2170
.L2171:
	leaq	-144(%rbp), %rdi
	leaq	-228(%rbp), %rsi
	movq	%rdi, -248(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-248(%rbp), %rdi
	testb	%al, %al
	je	.L2192
	movabsq	$824633720832, %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -132(%rbp)
	movl	-228(%rbp), %eax
	movl	$1, -144(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r12, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	leaq	-224(%rbp), %r12
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L2173
	.p2align 4,,10
	.p2align 3
.L2189:
	movl	11(%rcx), %edx
	notl	%edx
	andl	$1, %edx
	jmp	.L2174
	.p2align 4,,10
	.p2align 3
.L2190:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r15
	jmp	.L2175
	.p2align 4,,10
	.p2align 3
.L2192:
	movq	(%r12), %rax
	jmp	.L2170
.L2191:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21434:
	.size	_ZN2v88internal10JSReceiver23DeletePropertyOrElementENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE, .-_ZN2v88internal10JSReceiver23DeletePropertyOrElementENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE
	.section	.text._ZN2v88internal8JSObject12IsExtensibleENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject12IsExtensibleENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSObject12IsExtensibleENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSObject12IsExtensibleENS0_6HandleIS1_EE:
.LFB21516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	24(%r14), %rax
	movq	-1(%r12), %rdx
	cmpw	$1026, 11(%rdx)
	leaq	-37592(%rax), %r13
	je	.L2209
	movq	-1(%r12), %rax
	movzbl	13(%rax), %r12d
	shrb	$5, %r12b
	andl	$1, %r12d
.L2195:
	testb	%r12b, %r12b
	je	.L2196
	movq	41112(%r13), %rdi
	movq	12464(%r13), %r14
	testq	%rdi, %rdi
	je	.L2197
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2198:
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	je	.L2193
.L2196:
	movq	(%rbx), %rax
	movq	-1(%rax), %rdx
	cmpw	$1026, 11(%rdx)
	je	.L2210
	movq	-1(%rax), %rax
	movl	15(%rax), %r12d
	shrl	$27, %r12d
	andl	$1, %r12d
.L2193:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2211
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2197:
	.cfi_restore_state
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L2212
.L2199:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%rsi)
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L2209:
	movq	-25128(%rax), %rax
	leaq	-96(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	-104(%rbp), %rdi
	movq	%rax, %r15
	movq	24(%r14), %rax
	movq	%r12, -88(%rbp)
	movq	$0, -80(%rbp)
	subq	$37592, %rax
	movl	$0, -72(%rbp)
	movq	%rax, -96(%rbp)
	movb	$0, -68(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	cmpq	%r15, -88(%rbp)
	setne	%r12b
	jmp	.L2195
	.p2align 4,,10
	.p2align 3
.L2210:
	leaq	-96(%rbp), %rdi
	movq	%r13, -96(%rbp)
	xorl	%r12d, %r12d
	movq	%rax, -88(%rbp)
	movq	$0, -80(%rbp)
	movl	$0, -72(%rbp)
	movb	$0, -68(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	cmpb	$0, -68(%rbp)
	jne	.L2193
	movq	-88(%rbp), %rax
	movq	-1(%rax), %rax
	movl	15(%rax), %r12d
	shrl	$27, %r12d
	andl	$1, %r12d
	jmp	.L2193
	.p2align 4,,10
	.p2align 3
.L2212:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2199
.L2211:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21516:
	.size	_ZN2v88internal8JSObject12IsExtensibleENS0_6HandleIS1_EE, .-_ZN2v88internal8JSObject12IsExtensibleENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal10JSReceiver12IsExtensibleENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver12IsExtensibleENS0_6HandleIS1_EE
	.type	_ZN2v88internal10JSReceiver12IsExtensibleENS0_6HandleIS1_EE, @function
_ZN2v88internal10JSReceiver12IsExtensibleENS0_6HandleIS1_EE:
.LFB21457:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	je	.L2217
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8JSObject12IsExtensibleENS0_6HandleIS1_EE
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, %edx
	movl	$1, %eax
	movb	%dl, %ah
	ret
	.p2align 4,,10
	.p2align 3
.L2217:
	.cfi_restore 6
	jmp	_ZN2v88internal7JSProxy12IsExtensibleENS0_6HandleIS1_EE@PLT
	.cfi_endproc
.LFE21457:
	.size	_ZN2v88internal10JSReceiver12IsExtensibleENS0_6HandleIS1_EE, .-_ZN2v88internal10JSReceiver12IsExtensibleENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal8JSObject18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE
	.type	_ZN2v88internal8JSObject18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE, @function
_ZN2v88internal8JSObject18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE:
.LFB21508:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	_ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE
	testb	%al, %al
	je	.L2237
	movq	48(%r12), %rdi
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r14
	subq	$37592, %r14
	cmpl	$4, 4(%r12)
	je	.L2221
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE
	testb	%al, %al
	jne	.L2222
	.p2align 4,,10
	.p2align 3
.L2237:
	xorl	%eax, %eax
	movb	$0, %ah
.L2220:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2221:
	.cfi_restore_state
	call	_ZN2v88internal8JSObject12IsExtensibleENS0_6HandleIS1_EE
	testb	%al, %al
	je	.L2239
.L2226:
	xorl	%edx, %edx
	movl	$1, %r8d
	movl	$1, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE
	testb	%al, %al
	je	.L2237
	testq	%rbx, %rbx
	je	.L2237
	popq	%rbx
	movl	$257, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2222:
	.cfi_restore_state
	btq	$34, %rax
	jnc	.L2226
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14GetShouldThrowEPNS0_7IsolateENS_5MaybeINS0_11ShouldThrowEEE@PLT
	cmpl	$1, %eax
	je	.L2220
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator7GetNameEv
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$151, %esi
	movq	%rax, %rdx
	jmp	.L2238
	.p2align 4,,10
	.p2align 3
.L2239:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14GetShouldThrowEPNS0_7IsolateENS_5MaybeINS0_11ShouldThrowEEE@PLT
	cmpl	$1, %eax
	je	.L2220
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator7GetNameEv
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$49, %esi
	movq	%rax, %rdx
.L2238:
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L2237
	.cfi_endproc
.LFE21508:
	.size	_ZN2v88internal8JSObject18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE, .-_ZN2v88internal8JSObject18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_125GenericTestIntegrityLevelENS0_6HandleINS0_10JSReceiverEEENS0_18PropertyAttributesE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_125GenericTestIntegrityLevelENS0_6HandleINS0_10JSReceiverEEENS0_18PropertyAttributesE, @function
_ZN2v88internal12_GLOBAL__N_125GenericTestIntegrityLevelENS0_6HandleINS0_10JSReceiverEEENS0_18PropertyAttributesE:
.LFB21454:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movl	%esi, -228(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	je	.L2271
	call	_ZN2v88internal8JSObject12IsExtensibleENS0_6HandleIS1_EE
.L2244:
	testb	%al, %al
	je	.L2246
.L2257:
	movl	$1, %eax
.L2245:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2272
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2271:
	.cfi_restore_state
	call	_ZN2v88internal7JSProxy12IsExtensibleENS0_6HandleIS1_EE@PLT
	testb	%al, %al
	jne	.L2273
.L2270:
	xorl	%eax, %eax
.L2269:
	movb	$0, %ah
	jmp	.L2245
	.p2align 4,,10
	.p2align 3
.L2246:
	movq	(%rbx), %rax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	andq	$-262144, %rax
	movq	24(%rax), %r11
	leaq	-37592(%r11), %r14
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, -216(%rbp)
	testq	%rax, %rax
	je	.L2269
	movq	(%rax), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L2248
	leaq	-192(%rbp), %rdx
	xorl	%r13d, %r13d
	leaq	-144(%rbp), %r12
	movq	%rdx, -224(%rbp)
	leaq	-193(%rbp), %r15
	jmp	.L2258
	.p2align 4,,10
	.p2align 3
.L2275:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L2250:
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movq	%r14, %rsi
	movq	%r12, %rdi
	movups	%xmm0, -184(%rbp)
	movups	%xmm0, -168(%rbp)
	andb	$-64, -192(%rbp)
	movb	$0, -193(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorE
	movzbl	%ah, %edx
	testb	%al, %al
	je	.L2270
	testb	%dl, %dl
	je	.L2254
	movzbl	-192(%rbp), %eax
	testb	$4, %al
	jne	.L2257
	cmpl	$5, -228(%rbp)
	je	.L2274
.L2254:
	movq	-216(%rbp), %rax
	addq	$1, %r13
	movq	(%rax), %rax
	cmpl	%r13d, 11(%rax)
	jle	.L2248
.L2258:
	movq	15(%rax,%r13,8), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L2275
	movq	41088(%r14), %rcx
	cmpq	41096(%r14), %rcx
	je	.L2276
.L2251:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rcx)
	jmp	.L2250
	.p2align 4,,10
	.p2align 3
.L2274:
	cmpq	$0, -184(%rbp)
	je	.L2277
.L2256:
	testb	$16, %al
	je	.L2254
	jmp	.L2257
	.p2align 4,,10
	.p2align 3
.L2276:
	movq	%r14, %rdi
	movq	%rsi, -240(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-240(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L2251
	.p2align 4,,10
	.p2align 3
.L2277:
	testb	$32, %al
	je	.L2254
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2248:
	movl	$257, %eax
	jmp	.L2245
.L2272:
	call	__stack_chk_fail@PLT
.L2273:
	movzbl	%ah, %eax
	jmp	.L2244
	.cfi_endproc
.LFE21454:
	.size	_ZN2v88internal12_GLOBAL__N_125GenericTestIntegrityLevelENS0_6HandleINS0_10JSReceiverEEENS0_18PropertyAttributesE, .-_ZN2v88internal12_GLOBAL__N_125GenericTestIntegrityLevelENS0_6HandleINS0_10JSReceiverEEENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal8JSObject18TestIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject18TestIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesE
	.type	_ZN2v88internal8JSObject18TestIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesE, @function
_ZN2v88internal8JSObject18TestIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesE:
.LFB21514:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	-1(%r13), %rax
	cmpw	$1041, 11(%rax)
	jbe	.L2279
	movq	-1(%r13), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$13, %eax
	cmpb	$1, %al
	ja	.L2323
.L2279:
	addq	$40, %rsp
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12_GLOBAL__N_125GenericTestIntegrityLevelENS0_6HandleINS0_10JSReceiverEEENS0_18PropertyAttributesE
	.p2align 4,,10
	.p2align 3
.L2323:
	.cfi_restore_state
	movq	-1(%r13), %rax
	movl	15(%rax), %edx
	andl	$134217728, %edx
	movl	%edx, %r15d
	je	.L2280
.L2322:
	xorl	%edx, %edx
.L2281:
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	movb	%dl, %ah
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2280:
	.cfi_restore_state
	movq	-1(%r13), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$12, %eax
	je	.L2324
	leal	-17(%rax), %edx
	cmpb	$10, %dl
	ja	.L2293
	cmpl	$5, %esi
	jne	.L2294
	cmpq	$0, 39(%r13)
	jne	.L2322
.L2294:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_128TestPropertiesIntegrityLevelENS0_8JSObjectENS0_18PropertyAttributesE
.L2295:
	testb	%al, %al
	je	.L2322
	.p2align 4,,10
	.p2align 3
.L2284:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_128TestPropertiesIntegrityLevelENS0_8JSObjectENS0_18PropertyAttributesE
	movl	%eax, %edx
	jmp	.L2281
	.p2align 4,,10
	.p2align 3
.L2324:
	movq	%r13, %rax
	movq	15(%r13), %rbx
	andq	$-262144, %rax
	movq	24(%rax), %r14
	subq	$1, %rbx
	leaq	-37536(%r14), %rax
	movq	%rax, -64(%rbp)
	movslq	36(%rbx), %rax
	movq	%rbx, -56(%rbp)
	movl	%eax, -68(%rbp)
	testl	%eax, %eax
	jne	.L2292
	jmp	.L2284
	.p2align 4,,10
	.p2align 3
.L2289:
	leal	64(,%rbx,8), %eax
	movq	-56(%rbp), %rbx
	cltq
	movq	(%rax,%rbx), %rax
	movq	%rax, %rcx
	shrq	$35, %rax
	movl	%eax, %esi
	sarq	$32, %rcx
	andl	$7, %esi
	testb	$4, %al
	je	.L2322
	cmpl	$5, %r12d
	je	.L2325
.L2286:
	addl	$1, %r15d
	cmpl	%r15d, -68(%rbp)
	je	.L2284
.L2292:
	leal	(%r15,%r15,2), %ebx
	movq	-56(%rbp), %rcx
	leal	48(,%rbx,8), %eax
	cltq
	movq	(%rax,%rcx), %r14
	movq	-64(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9HashTableINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE5IsKeyENS0_13ReadOnlyRootsENS0_6ObjectE@PLT
	testb	%al, %al
	je	.L2286
	testb	$1, %r14b
	je	.L2289
	movq	-1(%r14), %rax
	cmpw	$64, 11(%rax)
	jne	.L2289
	testb	$1, 11(%r14)
	je	.L2289
	jmp	.L2286
	.p2align 4,,10
	.p2align 3
.L2293:
	leal	-10(%rax), %edx
	cmpb	$1, %dl
	jbe	.L2284
	leal	-8(%rax), %edx
	cmpb	$1, %dl
	ja	.L2302
	cmpl	$5, %esi
	jne	.L2284
.L2302:
	leal	-6(%rax), %edx
	cmpb	$1, %dl
	ja	.L2303
	testl	%r12d, %r12d
	je	.L2284
.L2303:
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movq	%r13, %rsi
	movq	(%rdx,%rax,8), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	testl	%eax, %eax
	sete	%al
	jmp	.L2295
	.p2align 4,,10
	.p2align 3
.L2325:
	andl	$1, %ecx
	jne	.L2286
	andl	$1, %esi
	jne	.L2286
	jmp	.L2322
	.cfi_endproc
.LFE21514:
	.size	_ZN2v88internal8JSObject18TestIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesE, .-_ZN2v88internal8JSObject18TestIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal10JSReceiver18TestIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver18TestIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesE
	.type	_ZN2v88internal10JSReceiver18TestIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesE, @function
_ZN2v88internal10JSReceiver18TestIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesE:
.LFB21455:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-1(%rax), %rax
	cmpw	$1041, 11(%rax)
	jbe	.L2327
	jmp	_ZN2v88internal8JSObject18TestIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesE
	.p2align 4,,10
	.p2align 3
.L2327:
	jmp	_ZN2v88internal12_GLOBAL__N_125GenericTestIntegrityLevelENS0_6HandleINS0_10JSReceiverEEENS0_18PropertyAttributesE
	.cfi_endproc
.LFE21455:
	.size	_ZN2v88internal10JSReceiver18TestIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesE, .-_ZN2v88internal10JSReceiver18TestIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_16NumberDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE,"axG",@progbits,_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_16NumberDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_16NumberDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE
	.type	_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_16NumberDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE, @function
_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_16NumberDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE:
.LFB24286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	(%rdx), %rax
	movl	%ecx, -52(%rbp)
	movslq	35(%rax), %rdx
	testq	%rdx, %rdx
	jle	.L2328
	movl	%ecx, %edi
	subl	$1, %edx
	movq	%rsi, %r12
	movl	$48, %r15d
	andl	$1, %edi
	leaq	(%rdx,%rdx,2), %rdx
	movl	%edi, -56(%rbp)
	leaq	72(,%rdx,8), %r13
	jmp	.L2340
	.p2align 4,,10
	.p2align 3
.L2334:
	movq	(%rbx), %r9
	leal	16(%r15), %esi
	movl	%r15d, %ecx
	movslq	%esi, %rsi
	movq	-1(%rsi,%r9), %r8
	movl	-56(%rbp), %edx
	leaq	-1(%r9), %rdi
	movl	-52(%rbp), %eax
	sarq	$32, %r8
	testl	%edx, %edx
	je	.L2336
	testb	$1, %r8b
	jne	.L2345
.L2336:
	movl	%r8d, %ecx
	andl	$-57, %r8d
	shrl	$3, %ecx
	andl	$7, %ecx
	orl	%eax, %ecx
	leal	0(,%rcx,8), %eax
	orl	%r8d, %eax
	addl	%eax, %eax
	sarl	%eax
	salq	$32, %rax
	movq	%rax, (%rdi,%rsi)
.L2339:
	addq	$24, %r15
	cmpq	%r15, %r13
	je	.L2328
.L2346:
	movq	(%rbx), %rax
.L2340:
	movq	-1(%rax,%r15), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9HashTableINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE5IsKeyENS0_13ReadOnlyRootsENS0_6ObjectE@PLT
	testb	%al, %al
	je	.L2339
	testb	$1, %r14b
	je	.L2334
	movq	-1(%r14), %rax
	cmpw	$64, 11(%rax)
	jne	.L2334
	testb	$1, 11(%r14)
	je	.L2334
	addq	$24, %r15
	cmpq	%r15, %r13
	jne	.L2346
.L2328:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2345:
	.cfi_restore_state
	addl	$8, %ecx
	movslq	%ecx, %rcx
	movq	-1(%r9,%rcx), %rax
	testb	$1, %al
	jne	.L2337
	movl	-52(%rbp), %eax
	jmp	.L2336
	.p2align 4,,10
	.p2align 3
.L2337:
	movq	-1(%rax), %rcx
	movl	-52(%rbp), %edx
	movl	%edx, %eax
	andl	$-2, %eax
	cmpw	$79, 11(%rcx)
	cmovne	%edx, %eax
	jmp	.L2336
	.cfi_endproc
.LFE24286:
	.size	_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_16NumberDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE, .-_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_16NumberDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal23CreateElementDictionaryEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23CreateElementDictionaryEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
	.type	_ZN2v88internal23CreateElementDictionaryEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE, @function
_ZN2v88internal23CreateElementDictionaryEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE:
.LFB21518:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rcx
	movq	-1(%rcx), %rdx
	movzbl	14(%rdx), %edx
	shrl	$3, %edx
	subl	$17, %edx
	cmpb	$10, %dl
	ja	.L2365
.L2348:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2365:
	movq	-1(%rcx), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$12, %eax
	je	.L2348
	movq	-1(%rcx), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$16, %eax
	je	.L2348
	movq	-1(%rcx), %rax
	cmpw	$1061, 11(%rax)
	je	.L2366
	movq	15(%rcx), %rax
	movl	11(%rax), %eax
.L2352:
	testl	%eax, %eax
	jne	.L2353
	leaq	1016(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2366:
	movl	27(%rcx), %eax
	jmp	.L2352
	.p2align 4,,10
	.p2align 3
.L2353:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	-1(%rcx), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movzbl	14(%rax), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	(%rdi), %rax
	call	*176(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21518:
	.size	_ZN2v88internal23CreateElementDictionaryEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE, .-_ZN2v88internal23CreateElementDictionaryEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
	.section	.text._ZN2v88internal8JSObject14FastPropertyAtENS0_6HandleIS1_EENS0_14RepresentationENS0_10FieldIndexE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject14FastPropertyAtENS0_6HandleIS1_EENS0_14RepresentationENS0_10FieldIndexE
	.type	_ZN2v88internal8JSObject14FastPropertyAtENS0_6HandleIS1_EENS0_14RepresentationENS0_10FieldIndexE, @function
_ZN2v88internal8JSObject14FastPropertyAtENS0_6HandleIS1_EENS0_14RepresentationENS0_10FieldIndexE:
.LFB21520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	sarl	$3, %ecx
	andl	$2047, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rsi
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	24(%rax), %r12
	movq	%rdx, %rax
	movq	-1(%rsi), %r8
	shrq	$30, %rax
	andl	$15, %eax
	subq	$37592, %r12
	subl	%eax, %ecx
	testb	$64, %dh
	jne	.L2368
	movq	7(%rsi), %rax
	testb	$1, %al
	jne	.L2369
.L2382:
	movq	968(%r12), %rax
.L2370:
	leal	16(,%rcx,8), %edx
	movq	41112(%r12), %rdi
	movslq	%edx, %rdx
	movq	-1(%rax,%rdx), %r14
	testq	%rdi, %rdi
	je	.L2383
.L2400:
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2384:
	popq	%rbx
	movl	%r13d, %edx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6Object11WrapForReadEPNS0_7IsolateENS0_6HandleIS1_EENS0_14RepresentationE@PLT
	.p2align 4,,10
	.p2align 3
.L2368:
	.cfi_restore_state
	movq	47(%r8), %r8
	testq	%r8, %r8
	je	.L2397
	movq	%r8, %rsi
	movl	$32, %eax
	notq	%rsi
	movl	%esi, %r9d
	andl	$1, %r9d
	je	.L2398
.L2372:
	cmpl	%eax, %ecx
	jnb	.L2397
	testl	%ecx, %ecx
	leal	31(%rcx), %eax
	cmovns	%ecx, %eax
	sarl	$5, %eax
	andl	$1, %esi
	jne	.L2373
	cmpl	%eax, 11(%r8)
	jle	.L2373
	movl	%ecx, %esi
	sarl	$31, %esi
	shrl	$27, %esi
	addl	%esi, %ecx
	andl	$31, %ecx
	subl	%esi, %ecx
	movl	$1, %esi
	sall	%cl, %esi
	movl	%esi, %ecx
	testb	%r9b, %r9b
	je	.L2399
.L2377:
	sarq	$32, %r8
	testl	%r8d, %ecx
	sete	%sil
.L2379:
	movq	(%rdi), %rax
	movq	%rdx, %rcx
	andl	$16383, %ecx
	movq	-1(%rcx,%rax), %rbx
	testb	%sil, %sil
	jne	.L2380
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	%rbx, 7(%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2397:
	.cfi_restore_state
	movq	(%rdi), %rax
.L2380:
	movq	41112(%r12), %rdi
	andl	$16383, %edx
	movq	-1(%rdx,%rax), %r14
	testq	%rdi, %rdi
	jne	.L2400
.L2383:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2401
.L2385:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L2384
	.p2align 4,,10
	.p2align 3
.L2369:
	cmpq	288(%r12), %rax
	jne	.L2370
	jmp	.L2382
	.p2align 4,,10
	.p2align 3
.L2398:
	movslq	11(%r8), %rax
	sall	$3, %eax
	jmp	.L2372
	.p2align 4,,10
	.p2align 3
.L2399:
	sall	$2, %eax
	cltq
	movl	15(%r8,%rax), %eax
	testl	%eax, %esi
	sete	%sil
	jmp	.L2379
	.p2align 4,,10
	.p2align 3
.L2401:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2385
	.p2align 4,,10
	.p2align 3
.L2373:
	cmpl	$31, %ecx
	jg	.L2375
	movl	$1, %eax
	sall	%cl, %eax
	movl	%eax, %ecx
	testb	%r9b, %r9b
	jne	.L2377
.L2375:
	leaq	.LC12(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21520:
	.size	_ZN2v88internal8JSObject14FastPropertyAtENS0_6HandleIS1_EENS0_14RepresentationENS0_10FieldIndexE, .-_ZN2v88internal8JSObject14FastPropertyAtENS0_6HandleIS1_EENS0_14RepresentationENS0_10FieldIndexE
	.section	.text._ZN2v88internal25FastGetOwnValuesOrEntriesEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEbPNS3_INS0_10FixedArrayEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25FastGetOwnValuesOrEntriesEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEbPNS3_INS0_10FixedArrayEEE
	.type	_ZN2v88internal25FastGetOwnValuesOrEntriesEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEbPNS3_INS0_10FixedArrayEEE, @function
_ZN2v88internal25FastGetOwnValuesOrEntriesEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEbPNS3_INS0_10FixedArrayEEE:
.LFB21460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -192(%rbp)
	movb	%dl, -177(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	-1(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2403
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
	cmpw	$1024, 11(%rsi)
	ja	.L2406
.L2408:
	movl	$1, %eax
.L2407:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2459
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2403:
	.cfi_restore_state
	movq	41088(%r15), %rbx
	cmpq	%rbx, 41096(%r15)
	je	.L2460
.L2405:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	cmpw	$1024, 11(%rsi)
	jbe	.L2408
.L2406:
	leaq	-144(%rbp), %rax
	movq	%rsi, -144(%rbp)
	movq	%rax, %rdi
	movq	%rax, -200(%rbp)
	call	_ZNK2v88internal3Map23OnlyHasSimplePropertiesEv@PLT
	testb	%al, %al
	je	.L2408
	movq	41112(%r15), %rdi
	movq	0(%r13), %rsi
	testq	%rdi, %rdi
	je	.L2409
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -168(%rbp)
.L2410:
	movq	(%rbx), %rax
	movq	41112(%r15), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2412
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -176(%rbp)
.L2413:
	movq	(%rbx), %rax
	movl	15(%rax), %r13d
	movq	-168(%rbp), %rax
	movq	(%rax), %rsi
	shrl	$10, %r13d
	andl	$1023, %r13d
	movq	-1(%rsi), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	15(%rsi), %rdx
	movq	(%rdi), %rax
	call	*184(%rax)
	movl	$134217726, %edx
	subl	%r13d, %edx
	cmpl	%eax, %edx
	jge	.L2415
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$189, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L2407
	.p2align 4,,10
	.p2align 3
.L2415:
	xorl	%edx, %edx
	leal	(%rax,%r13), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-168(%rbp), %r14
	movl	$0, -148(%rbp)
	movq	%rax, -224(%rbp)
	movq	%rax, %rcx
	movq	(%r14), %rax
	movq	15(%rax), %rdx
	cmpq	%rdx, 288(%r15)
	je	.L2416
	movq	-1(%rax), %rax
	subq	$8, %rsp
	movzbl	%r12b, %r8d
	movq	%r15, %rsi
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	leaq	-148(%rbp), %r9
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	%r14, %rdx
	movq	(%rdi), %rax
	movq	80(%rax), %rax
	pushq	$18
	call	*%rax
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	jne	.L2461
.L2458:
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L2407
	.p2align 4,,10
	.p2align 3
.L2460:
	movq	%r15, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L2405
	.p2align 4,,10
	.p2align 3
.L2412:
	movq	41088(%r15), %rax
	movq	%rax, -176(%rbp)
	cmpq	41096(%r15), %rax
	je	.L2462
.L2414:
	movq	-176(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L2413
	.p2align 4,,10
	.p2align 3
.L2409:
	movq	41088(%r15), %rax
	movq	%rax, -168(%rbp)
	cmpq	41096(%r15), %rax
	je	.L2463
.L2411:
	movq	-168(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2461:
	movq	(%r14), %rax
.L2416:
	movq	-1(%rax), %rax
	cmpq	%rax, (%rbx)
	sete	-178(%rbp)
	testl	%r13d, %r13d
	je	.L2418
	leal	-1(%r13), %eax
	movq	%rbx, -232(%rbp)
	xorl	%r12d, %r12d
	movq	%rax, -208(%rbp)
	.p2align 4,,10
	.p2align 3
.L2419:
	movq	-176(%rbp), %rax
	leaq	1(%r12), %rbx
	movl	%r12d, -216(%rbp)
	leaq	(%rbx,%rbx,2), %r13
	movq	(%rax), %rax
	salq	$3, %r13
	movq	-1(%r13,%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2420
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L2421:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L2423
	cmpb	$0, -178(%rbp)
	je	.L2424
	movq	-176(%rbp), %rax
	movq	(%rax), %rsi
	movq	7(%r13,%rsi), %rax
	movq	%rax, %rdx
	sarq	$32, %rdx
	btq	$36, %rax
	jc	.L2423
	testb	$1, %dl
	jne	.L2425
	testb	$2, %dl
	je	.L2426
	movq	15(%r13,%rsi), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2427
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	jmp	.L2428
	.p2align 4,,10
	.p2align 3
.L2424:
	movq	-168(%rbp), %rax
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L2434
.L2436:
	movq	-168(%rbp), %rsi
	movl	$-1, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %r13
.L2435:
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	movq	%r15, -120(%rbp)
	movabsq	$824633720832, %rax
	movl	$0, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r14, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L2464
.L2437:
	movq	%r13, -80(%rbp)
	movq	-200(%rbp), %r13
	movq	%rax, -112(%rbp)
	movq	-168(%rbp), %rax
	movq	%r13, %rdi
	movq	$0, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	je	.L2423
	testb	$16, -128(%rbp)
	jne	.L2423
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2458
.L2428:
	cmpb	$0, -177(%rbp)
	jne	.L2465
.L2440:
	movq	-224(%rbp), %rax
	movl	-148(%rbp), %esi
	movq	-200(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -144(%rbp)
	movq	0(%r13), %rdx
	call	_ZN2v88internal10FixedArray3setEiNS0_6ObjectE
	addl	$1, -148(%rbp)
.L2423:
	cmpq	-208(%rbp), %r12
	je	.L2418
	movq	%rbx, %r12
	jmp	.L2419
	.p2align 4,,10
	.p2align 3
.L2420:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L2466
.L2422:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L2421
	.p2align 4,,10
	.p2align 3
.L2434:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2436
	movq	-168(%rbp), %r13
	jmp	.L2435
	.p2align 4,,10
	.p2align 3
.L2465:
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%r14), %rdx
	xorl	%r8d, %r8d
	movl	$2, %ecx
	movq	%rax, %rsi
	movq	(%rax), %rax
	movq	%r15, %rdi
	movq	%rdx, 15(%rax)
	movq	0(%r13), %rdx
	movq	(%rsi), %rax
	movq	%rdx, 23(%rax)
	movl	$2, %edx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	jmp	.L2440
	.p2align 4,,10
	.p2align 3
.L2466:
	movq	%r15, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L2422
	.p2align 4,,10
	.p2align 3
.L2426:
	movq	-232(%rbp), %rax
	movl	%edx, %r13d
	movl	%r12d, %esi
	shrl	$6, %r13d
	movq	(%rax), %rdi
	andl	$7, %r13d
	call	_ZN2v88internal10FieldIndex13ForDescriptorEPNS0_7IsolateENS0_3MapEi.isra.0
	movq	-168(%rbp), %rdi
	movl	%r13d, %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8JSObject14FastPropertyAtENS0_6HandleIS1_EENS0_14RepresentationENS0_10FieldIndexE
	movq	%rax, %r13
	jmp	.L2428
	.p2align 4,,10
	.p2align 3
.L2425:
	movq	(%r14), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rsi
	cmpw	$64, 11(%rsi)
	je	.L2467
.L2430:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	%r15, -120(%rbp)
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r14, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L2468
.L2431:
	movq	%rax, -112(%rbp)
	movq	-200(%rbp), %rdi
	movq	-168(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L2432
	movq	-120(%rbp), %rax
	leaq	88(%rax), %r13
.L2433:
	movq	-168(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	movq	-232(%rbp), %rcx
	cmpq	%rax, (%rcx)
	sete	-178(%rbp)
	jmp	.L2428
	.p2align 4,,10
	.p2align 3
.L2464:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L2437
	.p2align 4,,10
	.p2align 3
.L2432:
	movq	-200(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L2433
	jmp	.L2458
	.p2align 4,,10
	.p2align 3
.L2427:
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L2469
.L2429:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L2428
	.p2align 4,,10
	.p2align 3
.L2467:
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
	jmp	.L2430
	.p2align 4,,10
	.p2align 3
.L2468:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L2431
	.p2align 4,,10
	.p2align 3
.L2418:
	movl	-148(%rbp), %edx
	movq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal10FixedArray13ShrinkOrEmptyEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	-192(%rbp), %rcx
	movq	%rax, (%rcx)
	movl	$257, %eax
	jmp	.L2407
	.p2align 4,,10
	.p2align 3
.L2463:
	movq	%r15, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, -168(%rbp)
	jmp	.L2411
	.p2align 4,,10
	.p2align 3
.L2462:
	movq	%r15, %rdi
	movq	%rsi, -208(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %rsi
	movq	%rax, -176(%rbp)
	jmp	.L2414
.L2469:
	movq	%r15, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L2429
.L2459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21460:
	.size	_ZN2v88internal25FastGetOwnValuesOrEntriesEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEbPNS3_INS0_10FixedArrayEEE, .-_ZN2v88internal25FastGetOwnValuesOrEntriesEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEbPNS3_INS0_10FixedArrayEEE
	.section	.text._ZN2v88internal21GetOwnValuesOrEntriesEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS0_14PropertyFilterEbb,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21GetOwnValuesOrEntriesEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS0_14PropertyFilterEbb
	.type	_ZN2v88internal21GetOwnValuesOrEntriesEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS0_14PropertyFilterEbb, @function
_ZN2v88internal21GetOwnValuesOrEntriesEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS0_14PropertyFilterEbb:
.LFB21461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$280, %rsp
	movb	%r8b, -265(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -232(%rbp)
	cmpl	$18, %edx
	jne	.L2471
	testb	%cl, %cl
	je	.L2471
	leaq	-232(%rbp), %rcx
	movzbl	%r8b, %edx
	call	_ZN2v88internal25FastGetOwnValuesOrEntriesEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEbPNS3_INS0_10FixedArrayEEE
	testb	%al, %al
	jne	.L2472
.L2566:
	xorl	%eax, %eax
.L2475:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2567
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2472:
	.cfi_restore_state
	shrw	$8, %ax
	jne	.L2568
	.p2align 4,,10
	.p2align 3
.L2471:
	movl	%r13d, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	andl	$-3, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, -256(%rbp)
	testq	%rax, %rax
	je	.L2475
	movq	%rax, %r15
	movq	(%rax), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	movslq	11(%rax), %rsi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, -232(%rbp)
	movq	(%r15), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L2514
	andl	$2, %r13d
	leaq	-224(%rbp), %r15
	movl	$0, -244(%rbp)
	movl	%r13d, -248(%rbp)
	xorl	%r13d, %r13d
	jmp	.L2509
	.p2align 4,,10
	.p2align 3
.L2572:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L2479:
	movl	-248(%rbp), %eax
	testl	%eax, %eax
	je	.L2481
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdx
	movl	$1, %r9d
	movq	%r14, %rcx
	leaq	-144(%rbp), %rdi
	leaq	-236(%rbp), %r8
	movq	%r12, %rsi
	movups	%xmm0, -216(%rbp)
	movq	%rdi, -264(%rbp)
	movups	%xmm0, -200(%rbp)
	andb	$-64, -224(%rbp)
	movb	$0, -236(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	movq	-264(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorE
	movzbl	%ah, %edx
	testb	%al, %al
	je	.L2566
	testb	%dl, %dl
	je	.L2484
	testb	$1, -224(%rbp)
	je	.L2484
.L2481:
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L2569
.L2486:
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L2493
.L2495:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %r8
.L2494:
	movq	(%r14), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L2496
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L2496:
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	%r12, -200(%rbp)
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r14, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L2570
.L2497:
	movq	%r15, %rdi
	movq	%rax, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%r8, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L2492:
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L2566
	cmpb	$0, -265(%rbp)
	jne	.L2571
.L2499:
	movq	-232(%rbp), %rax
	movq	(%r8), %rdx
	movq	(%rax), %rdi
	movl	-244(%rbp), %eax
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L2510
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -264(%rbp)
	testl	$262144, %eax
	je	.L2507
	movq	%rdx, -296(%rbp)
	movq	%rsi, -288(%rbp)
	movq	%rdi, -280(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-264(%rbp), %rcx
	movq	-296(%rbp), %rdx
	movq	-288(%rbp), %rsi
	movq	-280(%rbp), %rdi
	movq	8(%rcx), %rax
.L2507:
	testb	$24, %al
	je	.L2510
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2510
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2510:
	addl	$1, -244(%rbp)
.L2484:
	movq	-256(%rbp), %rax
	addq	$1, %r13
	movq	(%rax), %rax
	cmpl	%r13d, 11(%rax)
	jle	.L2477
.L2509:
	movq	15(%rax,%r13,8), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L2572
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L2573
.L2480:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L2479
	.p2align 4,,10
	.p2align 3
.L2569:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L2487
	testb	$2, %al
	jne	.L2486
.L2487:
	leaq	-144(%rbp), %r8
	leaq	-236(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L2486
	movq	(%rbx), %rax
	movl	-236(%rbp), %edx
	movq	-264(%rbp), %r8
	testb	$1, %al
	jne	.L2489
.L2491:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r8, -280(%rbp)
	movl	%edx, -264(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movl	-264(%rbp), %edx
	movq	-280(%rbp), %r8
.L2490:
	movabsq	$824633720832, %rcx
	movq	%r8, %rdi
	movq	%r12, -120(%rbp)
	movl	$3, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%edx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r14, -112(%rbp)
	movdqa	-128(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm1
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	jmp	.L2492
	.p2align 4,,10
	.p2align 3
.L2571:
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%r14), %rdx
	movq	(%rax), %rdi
	movq	%rax, %r9
	testb	$1, %dl
	movq	%rdx, 15(%rdi)
	leaq	15(%rdi), %rsi
	movq	-264(%rbp), %r8
	je	.L2512
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -264(%rbp)
	testl	$262144, %eax
	jne	.L2574
.L2501:
	testb	$24, %al
	je	.L2512
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2575
	.p2align 4,,10
	.p2align 3
.L2512:
	movq	(%r9), %rdi
	movq	(%r8), %rdx
	movq	%rdx, 23(%rdi)
	leaq	23(%rdi), %rsi
	testb	$1, %dl
	je	.L2511
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -264(%rbp)
	testl	$262144, %eax
	jne	.L2576
.L2504:
	testb	$24, %al
	je	.L2511
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2577
	.p2align 4,,10
	.p2align 3
.L2511:
	xorl	%r8d, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r8
	jmp	.L2499
	.p2align 4,,10
	.p2align 3
.L2493:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2495
	movq	%rbx, %r8
	jmp	.L2494
	.p2align 4,,10
	.p2align 3
.L2489:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2491
	movq	%rbx, %rax
	jmp	.L2490
	.p2align 4,,10
	.p2align 3
.L2573:
	movq	%r12, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L2480
	.p2align 4,,10
	.p2align 3
.L2570:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-264(%rbp), %r8
	jmp	.L2497
	.p2align 4,,10
	.p2align 3
.L2576:
	movq	%r9, -304(%rbp)
	movq	%rdx, -296(%rbp)
	movq	%rsi, -288(%rbp)
	movq	%rdi, -280(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-264(%rbp), %rcx
	movq	-304(%rbp), %r9
	movq	-296(%rbp), %rdx
	movq	-288(%rbp), %rsi
	movq	8(%rcx), %rax
	movq	-280(%rbp), %rdi
	jmp	.L2504
	.p2align 4,,10
	.p2align 3
.L2574:
	movq	%r8, -312(%rbp)
	movq	%r9, -304(%rbp)
	movq	%rdx, -296(%rbp)
	movq	%rsi, -288(%rbp)
	movq	%rdi, -280(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-264(%rbp), %rcx
	movq	-312(%rbp), %r8
	movq	-304(%rbp), %r9
	movq	-296(%rbp), %rdx
	movq	8(%rcx), %rax
	movq	-288(%rbp), %rsi
	movq	-280(%rbp), %rdi
	jmp	.L2501
	.p2align 4,,10
	.p2align 3
.L2568:
	movq	-232(%rbp), %rax
	jmp	.L2475
	.p2align 4,,10
	.p2align 3
.L2577:
	movq	%r9, -264(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-264(%rbp), %r9
	jmp	.L2511
	.p2align 4,,10
	.p2align 3
.L2575:
	movq	%r8, -280(%rbp)
	movq	%r9, -264(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-280(%rbp), %r8
	movq	-264(%rbp), %r9
	jmp	.L2512
.L2514:
	movl	$0, -244(%rbp)
	.p2align 4,,10
	.p2align 3
.L2477:
	movl	-244(%rbp), %edx
	movq	-232(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10FixedArray13ShrinkOrEmptyEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	jmp	.L2475
.L2567:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21461:
	.size	_ZN2v88internal21GetOwnValuesOrEntriesEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS0_14PropertyFilterEbb, .-_ZN2v88internal21GetOwnValuesOrEntriesEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS0_14PropertyFilterEbb
	.section	.text._ZN2v88internal10JSReceiver12GetOwnValuesENS0_6HandleIS1_EENS0_14PropertyFilterEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver12GetOwnValuesENS0_6HandleIS1_EENS0_14PropertyFilterEb
	.type	_ZN2v88internal10JSReceiver12GetOwnValuesENS0_6HandleIS1_EENS0_14PropertyFilterEb, @function
_ZN2v88internal10JSReceiver12GetOwnValuesENS0_6HandleIS1_EENS0_14PropertyFilterEb:
.LFB21462:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rdi, %r9
	movzbl	%dl, %ecx
	xorl	%r8d, %r8d
	movl	%esi, %edx
	movq	%r9, %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	jmp	_ZN2v88internal21GetOwnValuesOrEntriesEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS0_14PropertyFilterEbb
	.cfi_endproc
.LFE21462:
	.size	_ZN2v88internal10JSReceiver12GetOwnValuesENS0_6HandleIS1_EENS0_14PropertyFilterEb, .-_ZN2v88internal10JSReceiver12GetOwnValuesENS0_6HandleIS1_EENS0_14PropertyFilterEb
	.section	.text._ZN2v88internal10JSReceiver13GetOwnEntriesENS0_6HandleIS1_EENS0_14PropertyFilterEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver13GetOwnEntriesENS0_6HandleIS1_EENS0_14PropertyFilterEb
	.type	_ZN2v88internal10JSReceiver13GetOwnEntriesENS0_6HandleIS1_EENS0_14PropertyFilterEb, @function
_ZN2v88internal10JSReceiver13GetOwnEntriesENS0_6HandleIS1_EENS0_14PropertyFilterEb:
.LFB21463:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rdi, %r9
	movzbl	%dl, %ecx
	movl	$1, %r8d
	movl	%esi, %edx
	movq	%r9, %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	jmp	_ZN2v88internal21GetOwnValuesOrEntriesEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS0_14PropertyFilterEbb
	.cfi_endproc
.LFE21463:
	.size	_ZN2v88internal10JSReceiver13GetOwnEntriesENS0_6HandleIS1_EENS0_14PropertyFilterEb, .-_ZN2v88internal10JSReceiver13GetOwnEntriesENS0_6HandleIS1_EENS0_14PropertyFilterEb
	.section	.text._ZN2v88internal8JSObject21HasEnumerableElementsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject21HasEnumerableElementsEv
	.type	_ZN2v88internal8JSObject21HasEnumerableElementsEv, @function
_ZN2v88internal8JSObject21HasEnumerableElementsEv:
.LFB21521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-1(%rcx), %rax
	movzbl	14(%rax), %eax
	movl	%eax, %edx
	shrl	$3, %edx
	cmpl	$231, %eax
	ja	.L2581
	leaq	.L2583(%rip), %rsi
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8JSObject21HasEnumerableElementsEv,"a",@progbits
	.align 4
	.align 4
.L2583:
	.long	.L2589-.L2583
	.long	.L2588-.L2583
	.long	.L2589-.L2583
	.long	.L2588-.L2583
	.long	.L2589-.L2583
	.long	.L2590-.L2583
	.long	.L2589-.L2583
	.long	.L2588-.L2583
	.long	.L2589-.L2583
	.long	.L2588-.L2583
	.long	.L2589-.L2583
	.long	.L2588-.L2583
	.long	.L2587-.L2583
	.long	.L2604-.L2583
	.long	.L2604-.L2583
	.long	.L2585-.L2583
	.long	.L2585-.L2583
	.long	.L2584-.L2583
	.long	.L2584-.L2583
	.long	.L2584-.L2583
	.long	.L2584-.L2583
	.long	.L2584-.L2583
	.long	.L2584-.L2583
	.long	.L2584-.L2583
	.long	.L2584-.L2583
	.long	.L2584-.L2583
	.long	.L2584-.L2583
	.long	.L2584-.L2583
	.long	.L2597-.L2583
	.section	.text._ZN2v88internal8JSObject21HasEnumerableElementsEv
	.p2align 4,,10
	.p2align 3
.L2590:
	movq	-1(%rcx), %rax
	cmpw	$1061, 11(%rax)
	je	.L2610
	movq	15(%rcx), %rax
	movl	11(%rax), %edx
.L2600:
	testl	%edx, %edx
	je	.L2597
	movq	15(%rcx), %rcx
	jle	.L2597
	subl	$1, %edx
	leaq	15(%rcx), %rax
	leaq	23(%rcx,%rdx,8), %rcx
	movabsq	$-2251799814209537, %rdx
	jmp	.L2602
	.p2align 4,,10
	.p2align 3
.L2611:
	addq	$8, %rax
	cmpq	%rcx, %rax
	je	.L2597
.L2602:
	cmpq	%rdx, (%rax)
	je	.L2611
	.p2align 4,,10
	.p2align 3
.L2604:
	movl	$1, %eax
.L2580:
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L2612
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2584:
	.cfi_restore_state
	cmpq	$0, 47(%rcx)
	setne	%al
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L2589:
	movq	-1(%rcx), %rax
	cmpw	$1061, 11(%rax)
	je	.L2613
	movq	15(%rcx), %rax
	movl	11(%rax), %eax
.L2609:
	testl	%eax, %eax
	setg	%al
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L2588:
	movq	15(%rcx), %rdx
	movq	-1(%rcx), %rax
	cmpw	$1061, 11(%rax)
	je	.L2614
	movl	11(%rdx), %esi
.L2594:
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rcx
	subq	$37592, %rcx
	testl	%esi, %esi
	jle	.L2597
	subl	$1, %esi
	leaq	15(%rdx), %rax
	leaq	23(%rdx,%rsi,8), %rsi
	jmp	.L2598
	.p2align 4,,10
	.p2align 3
.L2596:
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L2597
.L2598:
	movq	(%rax), %rdx
	cmpq	96(%rcx), %rdx
	je	.L2596
	jmp	.L2604
	.p2align 4,,10
	.p2align 3
.L2585:
	movq	23(%rcx), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jg	.L2604
	movq	15(%rcx), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	setg	%al
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L2597:
	xorl	%eax, %eax
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L2587:
	movq	15(%rcx), %rax
	leaq	-16(%rbp), %rdi
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal10DictionaryINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE28NumberOfEnumerablePropertiesEv@PLT
	jmp	.L2609
	.p2align 4,,10
	.p2align 3
.L2613:
	movl	27(%rcx), %eax
	jmp	.L2609
	.p2align 4,,10
	.p2align 3
.L2610:
	movl	27(%rcx), %edx
	jmp	.L2600
	.p2align 4,,10
	.p2align 3
.L2614:
	movl	27(%rcx), %esi
	jmp	.L2594
.L2581:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2612:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21521:
	.size	_ZN2v88internal8JSObject21HasEnumerableElementsEv, .-_ZN2v88internal8JSObject21HasEnumerableElementsEv
	.section	.text._ZN2v88internal8JSObject14DefineAccessorEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEES6_NS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject14DefineAccessorEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEES6_NS0_18PropertyAttributesE
	.type	_ZN2v88internal8JSObject14DefineAccessorEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEES6_NS0_18PropertyAttributesE, @function
_ZN2v88internal8JSObject14DefineAccessorEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEES6_NS0_18PropertyAttributesE:
.LFB21523:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	cmpl	$-1, 72(%rdi)
	movq	24(%rdi), %r13
	jne	.L2616
	movq	32(%rdi), %rax
	movq	(%rax), %rax
	cmpq	%rax, 3944(%r13)
	je	.L2617
	cmpq	2304(%r13), %rax
	je	.L2617
	cmpq	2912(%r13), %rax
	je	.L2617
	cmpq	3904(%r13), %rax
	je	.L2617
	cmpq	3856(%r13), %rax
	je	.L2617
	cmpq	3192(%r13), %rax
	je	.L2617
	cmpq	3408(%r13), %rax
	je	.L2617
	movl	4(%rdi), %eax
	testl	%eax, %eax
	je	.L2625
	.p2align 4,,10
	.p2align 3
.L2623:
	movl	%r15d, %ecx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator28TransitionToAccessorPropertyENS0_6HandleINS0_6ObjectEEES4_NS0_18PropertyAttributesE@PLT
	leaq	88(%r13), %rax
.L2627:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2616:
	.cfi_restore_state
	movl	4(%rdi), %edx
	testl	%edx, %edx
	je	.L2625
.L2630:
	movq	48(%r12), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$17, %eax
	cmpb	$10, %al
	ja	.L2623
	movq	24(%r12), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	addq	$88, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2625:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal14LookupIterator9HasAccessEv@PLT
	testb	%al, %al
	jne	.L2620
	movq	56(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	12552(%r13), %rbx
	leaq	88(%r13), %rax
	cmpq	%rbx, 96(%r13)
	je	.L2627
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
	jmp	.L2627
	.p2align 4,,10
	.p2align 3
.L2617:
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator23InternalUpdateProtectorEv@PLT
	movl	4(%r12), %ecx
	testl	%ecx, %ecx
	je	.L2625
.L2619:
	cmpl	$-1, 72(%r12)
	je	.L2623
	jmp	.L2630
	.p2align 4,,10
	.p2align 3
.L2620:
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	jmp	.L2619
	.cfi_endproc
.LFE21523:
	.size	_ZN2v88internal8JSObject14DefineAccessorEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEES6_NS0_18PropertyAttributesE, .-_ZN2v88internal8JSObject14DefineAccessorEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEES6_NS0_18PropertyAttributesE
	.section	.text._ZN2v88internal8JSObject14DefineAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEES7_NS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject14DefineAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEES7_NS0_18PropertyAttributesE
	.type	_ZN2v88internal8JSObject14DefineAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEES7_NS0_18PropertyAttributesE, @function
_ZN2v88internal8JSObject14DefineAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEES7_NS0_18PropertyAttributesE:
.LFB21522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	movq	%rsi, %rcx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %r8
	movq	(%rsi), %rdx
	movq	-1(%rdx), %rsi
	subq	$37592, %r8
	cmpw	$63, 11(%rsi)
	jbe	.L2655
.L2633:
	testb	$1, %al
	jne	.L2640
.L2642:
	movl	$-1, %edx
	movq	%r8, %rdi
	movq	%rbx, %rsi
	movq	%rcx, -256(%rbp)
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-248(%rbp), %r8
	movq	-256(%rbp), %rcx
	movq	%rax, %rdx
.L2641:
	movabsq	$824633720832, %rdi
	movq	(%r12), %rax
	movq	-1(%rax), %rsi
	movq	%rdi, -212(%rbp)
	movl	$0, -224(%rbp)
	movq	%r8, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L2656
.L2643:
	leaq	-224(%rbp), %r12
	movq	%rcx, -192(%rbp)
	movq	%r12, %rdi
	movq	$0, -184(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L2639:
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject14DefineAccessorEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEES6_NS0_18PropertyAttributesE
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2657
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2655:
	.cfi_restore_state
	movq	%rdx, -144(%rbp)
	movl	7(%rdx), %edx
	testb	$1, %dl
	jne	.L2634
	andl	$2, %edx
	jne	.L2633
.L2634:
	leaq	-144(%rbp), %r9
	leaq	-228(%rbp), %rsi
	movq	%rcx, -264(%rbp)
	movq	%r9, %rdi
	movq	%r8, -256(%rbp)
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-248(%rbp), %r9
	movq	-256(%rbp), %r8
	testb	%al, %al
	movq	-264(%rbp), %rcx
	je	.L2658
	movq	(%rbx), %rax
	movl	-228(%rbp), %edx
	testb	$1, %al
	jne	.L2636
.L2638:
	movq	%r8, %rdi
	movq	%rbx, %rsi
	movq	%r9, -264(%rbp)
	movl	%edx, -256(%rbp)
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-248(%rbp), %r8
	movl	-256(%rbp), %edx
	movq	-264(%rbp), %r9
.L2637:
	movabsq	$824633720832, %rdi
	movq	%r8, -120(%rbp)
	movq	%rdi, -132(%rbp)
	movq	%r9, %rdi
	movl	$0, -144(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%edx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r12, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	leaq	-224(%rbp), %r12
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L2639
	.p2align 4,,10
	.p2align 3
.L2640:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2642
	movq	%rbx, %rdx
	jmp	.L2641
	.p2align 4,,10
	.p2align 3
.L2658:
	movq	(%rbx), %rax
	jmp	.L2633
	.p2align 4,,10
	.p2align 3
.L2636:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2638
	movq	%rbx, %rax
	jmp	.L2637
	.p2align 4,,10
	.p2align 3
.L2656:
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%rdx, -248(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-248(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L2643
.L2657:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21522:
	.size	_ZN2v88internal8JSObject14DefineAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEES7_NS0_18PropertyAttributesE, .-_ZN2v88internal8JSObject14DefineAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEES7_NS0_18PropertyAttributesE
	.section	.text._ZN2v88internal10JSReceiver34ValidateAndApplyPropertyDescriptorEPNS0_7IsolateEPNS0_14LookupIteratorEbPNS0_18PropertyDescriptorES7_NS_5MaybeINS0_11ShouldThrowEEENS0_6HandleINS0_4NameEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver34ValidateAndApplyPropertyDescriptorEPNS0_7IsolateEPNS0_14LookupIteratorEbPNS0_18PropertyDescriptorES7_NS_5MaybeINS0_11ShouldThrowEEENS0_6HandleINS0_4NameEEE
	.type	_ZN2v88internal10JSReceiver34ValidateAndApplyPropertyDescriptorEPNS0_7IsolateEPNS0_14LookupIteratorEbPNS0_18PropertyDescriptorES7_NS_5MaybeINS0_11ShouldThrowEEENS0_6HandleINS0_4NameEEE, @function
_ZN2v88internal10JSReceiver34ValidateAndApplyPropertyDescriptorEPNS0_7IsolateEPNS0_14LookupIteratorEbPNS0_18PropertyDescriptorES7_NS_5MaybeINS0_11ShouldThrowEEENS0_6HandleINS0_4NameEEE:
.LFB21447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$56, %rsp
	movq	8(%rcx), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L2869
	movq	16(%rcx), %r10
	movl	$1, %r8d
	testq	%r10, %r10
	je	.L2870
.L2754:
	movl	$1, %eax
	xorl	%r9d, %r9d
.L2662:
	movzbl	(%r14), %ecx
	testb	$2, %cl
	jne	.L2663
	testb	$8, %cl
	jne	.L2663
	testb	$32, %cl
	jne	.L2663
	cmpq	$0, 8(%r14)
	je	.L2871
	.p2align 4,,10
	.p2align 3
.L2663:
	movzbl	(%rbx), %eax
	testb	$2, %al
	jne	.L2872
.L2688:
	testb	$8, %al
	jne	.L2873
.L2690:
	testq	%rsi, %rsi
	je	.L2692
	movq	8(%r14), %rax
	testq	%rax, %rax
	je	.L2846
	movq	(%rax), %rax
	leaq	-80(%rbp), %rdi
	movb	%r8b, -82(%rbp)
	movb	%r9b, -81(%rbp)
	movq	%rax, -80(%rbp)
	movq	(%rsi), %rsi
	call	_ZN2v88internal6Object9SameValueES1_@PLT
	movzbl	-81(%rbp), %r9d
	movzbl	-82(%rbp), %r8d
	testb	%al, %al
	je	.L2845
	movzbl	(%rbx), %eax
.L2692:
	testb	$32, %al
	je	.L2694
	movzbl	(%r14), %ecx
	testb	$32, %cl
	jne	.L2695
	.p2align 4,,10
	.p2align 3
.L2846:
	movl	%ecx, %edx
	shrb	$2, %dl
	andl	$1, %edx
.L2691:
	testb	%dl, %dl
	jne	.L2700
	movzbl	(%rbx), %eax
	testb	$8, %al
	je	.L2701
	testb	$4, %al
	jne	.L2866
.L2701:
	testb	$2, %al
	jne	.L2703
.L2700:
	movq	8(%r14), %rax
	movl	$1, %r10d
	testq	%rax, %rax
	je	.L2874
.L2709:
	testb	%r9b, %r9b
	je	.L2875
.L2710:
	testq	%r13, %r13
	je	.L2698
	movzbl	(%rbx), %eax
	movl	%eax, %edx
	testb	$2, %al
	jne	.L2847
	movzbl	(%r14), %edx
.L2847:
	andl	$1, %edx
	cmpb	$1, %dl
	sbbl	%r11d, %r11d
	andl	$2, %r11d
	movl	%r11d, %ecx
	testb	$8, %al
	jne	.L2876
	testb	$4, (%r14)
	jne	.L2737
.L2848:
	orl	$4, %ecx
	movl	%ecx, %r11d
.L2737:
	testb	%r8b, %r8b
	jne	.L2739
	testb	%r9b, %r9b
	je	.L2740
	testb	%r10b, %r10b
	je	.L2740
.L2739:
	testb	$32, %al
	je	.L2741
	shrb	$4, %al
	andl	$1, %eax
	xorl	$1, %eax
	movzbl	%al, %edx
	orl	%ecx, %edx
.L2742:
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L2877
.L2744:
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE
	jmp	.L2666
	.p2align 4,,10
	.p2align 3
.L2872:
	movl	%ecx, %edi
	movl	%eax, %edx
	andl	$1, %edi
	andl	$1, %edx
	cmpb	%dl, %dil
	jne	.L2846
	jmp	.L2688
	.p2align 4,,10
	.p2align 3
.L2870:
	cmpq	$0, 24(%rbx)
	setne	%al
	xorl	%r9d, %r9d
	jmp	.L2662
	.p2align 4,,10
	.p2align 3
.L2669:
	testq	%r13, %r13
	je	.L2698
	movzbl	(%rbx), %eax
	testb	$2, %al
	jne	.L2680
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, (%rbx)
.L2680:
	testb	$8, %al
	jne	.L2681
	andl	$-13, %eax
	orl	$8, %eax
	movb	%al, (%rbx)
.L2681:
	movq	24(%rbx), %rdx
	addq	$104, %r12
	testq	%r10, %r10
	movl	%eax, %esi
	cmove	%r12, %r10
	testq	%rdx, %rdx
	cmove	%r12, %rdx
	andl	$1, %esi
	cmpb	$1, %sil
	sbbl	%ecx, %ecx
	andl	$2, %ecx
	addl	$4, %ecx
	cmpb	$1, %sil
	sbbl	%esi, %esi
	andl	$2, %esi
	testb	$4, %al
	cmovne	%esi, %ecx
	testb	$32, %al
	je	.L2686
	shrb	$4, %al
	andl	$1, %eax
	xorl	$1, %eax
	movzbl	%al, %eax
	orl	%eax, %ecx
.L2686:
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject14DefineAccessorEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEES6_NS0_18PropertyAttributesE
	testq	%rax, %rax
	je	.L2851
.L2698:
	movl	$257, %eax
.L2666:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2878
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2869:
	.cfi_restore_state
	movzbl	(%rcx), %ecx
	movq	16(%rbx), %r10
	movl	%ecx, %r8d
	shrb	$5, %r8b
	andl	$1, %r8d
	testq	%r10, %r10
	jne	.L2754
	movq	24(%rbx), %rdi
	movl	$0, %r9d
	testq	%rdi, %rdi
	setne	%al
	jne	.L2662
	shrb	$5, %cl
	xorl	%eax, %eax
	movl	%ecx, %r9d
	andl	$1, %r9d
	xorl	$1, %r9d
	jmp	.L2662
	.p2align 4,,10
	.p2align 3
.L2875:
	cmpb	%r8b, %r10b
	je	.L2711
	testb	%dl, %dl
	jne	.L2710
.L2866:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14GetShouldThrowEPNS0_7IsolateENS_5MaybeINS0_11ShouldThrowEEE@PLT
	cmpl	$1, %eax
	je	.L2704
	movq	16(%rbp), %rdx
	testq	%r13, %r13
	je	.L2717
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator7GetNameEv
	movq	%rax, %rdx
.L2717:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$151, %esi
.L2849:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L2850:
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L2666
	.p2align 4,,10
	.p2align 3
.L2873:
	movl	%ecx, %edx
	movl	%eax, %edi
	shrb	$2, %dl
	shrb	$2, %dil
	andl	$1, %edx
	andl	$1, %edi
	cmpb	%dil, %dl
	jne	.L2691
	jmp	.L2690
	.p2align 4,,10
	.p2align 3
.L2876:
	testb	$4, %al
	jne	.L2737
	jmp	.L2848
	.p2align 4,,10
	.p2align 3
.L2703:
	movl	%ecx, %esi
	andl	$1, %eax
	andl	$1, %esi
	cmpb	%al, %sil
	je	.L2700
	jmp	.L2866
	.p2align 4,,10
	.p2align 3
.L2845:
	movzbl	(%r14), %ecx
	jmp	.L2846
	.p2align 4,,10
	.p2align 3
.L2874:
	movl	%ecx, %r10d
	shrb	$5, %r10b
	andl	$1, %r10d
	jmp	.L2709
	.p2align 4,,10
	.p2align 3
.L2704:
	movl	$1, %eax
	jmp	.L2666
	.p2align 4,,10
	.p2align 3
.L2741:
	movzbl	(%r14), %edx
	shrb	$4, %dl
	andl	$1, %edx
	xorl	$1, %edx
	movzbl	%dl, %edx
	orl	%ecx, %edx
	jmp	.L2742
	.p2align 4,,10
	.p2align 3
.L2711:
	testb	%r10b, %r10b
	je	.L2714
	testb	%dl, %dl
	jne	.L2710
	andl	$16, %ecx
	jne	.L2710
	movzbl	(%rbx), %edx
	testb	$32, %dl
	je	.L2715
	andl	$16, %edx
	jne	.L2866
.L2715:
	movq	8(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L2710
	movq	(%rdx), %rdx
	movb	%r8b, -83(%rbp)
	movb	%r10b, -82(%rbp)
	movb	%r9b, -81(%rbp)
	movq	%rdx, -64(%rbp)
	jmp	.L2868
	.p2align 4,,10
	.p2align 3
.L2695:
	movl	%ecx, %edx
	shrb	$4, %al
	shrb	$4, %dl
	andl	$1, %eax
	andl	$1, %edx
	cmpb	%dl, %al
	jne	.L2846
	.p2align 4,,10
	.p2align 3
.L2694:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L2697
	movq	16(%r14), %rdx
	testq	%rdx, %rdx
	je	.L2845
	movq	(%rdx), %rdx
	leaq	-72(%rbp), %rdi
	movb	%r8b, -82(%rbp)
	movb	%r9b, -81(%rbp)
	movq	%rdx, -72(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal6Object9SameValueES1_@PLT
	movzbl	-81(%rbp), %r9d
	movzbl	-82(%rbp), %r8d
	testb	%al, %al
	je	.L2845
.L2697:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L2698
	movq	24(%r14), %rdx
	testq	%rdx, %rdx
	je	.L2845
	movq	(%rdx), %rdx
	leaq	-64(%rbp), %rdi
	movb	%r8b, -82(%rbp)
	movb	%r9b, -81(%rbp)
	movq	%rdx, -64(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal6Object9SameValueES1_@PLT
	testb	%al, %al
	jne	.L2698
	movzbl	(%r14), %ecx
	movzbl	-81(%rbp), %r9d
	movzbl	-82(%rbp), %r8d
	movl	%ecx, %edx
	shrb	$2, %dl
	andl	$1, %edx
	jmp	.L2691
	.p2align 4,,10
	.p2align 3
.L2740:
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L2879
.L2747:
	movq	24(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L2880
.L2750:
	movl	%r11d, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject14DefineAccessorEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEES6_NS0_18PropertyAttributesE
	testq	%rax, %rax
	jne	.L2698
.L2851:
	movb	$0, %ah
	jmp	.L2666
	.p2align 4,,10
	.p2align 3
.L2877:
	movq	8(%r14), %rax
	leaq	88(%r12), %rsi
	testq	%rax, %rax
	cmovne	%rax, %rsi
	jmp	.L2744
	.p2align 4,,10
	.p2align 3
.L2871:
	cmpq	$0, 16(%r14)
	jne	.L2663
	cmpq	$0, 24(%r14)
	jne	.L2663
	testb	%dl, %dl
	je	.L2881
	testb	%al, %al
	jne	.L2669
	testq	%r13, %r13
	je	.L2698
	movzbl	(%rbx), %eax
	testb	$32, %al
	jne	.L2672
	andl	$-49, %eax
	orl	$32, %eax
	movb	%al, (%rbx)
.L2672:
	testb	$2, %al
	jne	.L2673
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, (%rbx)
.L2673:
	testb	$8, %al
	jne	.L2674
	andl	$-13, %eax
	orl	$8, %eax
	movb	%al, (%rbx)
.L2674:
	addq	$88, %r12
	movl	%eax, %edx
	testq	%rsi, %rsi
	movl	$1, %r8d
	cmove	%r12, %rsi
	andl	$1, %edx
	movq	%r13, %rdi
	cmpb	$1, %dl
	sbbl	%ecx, %ecx
	andl	$2, %ecx
	addl	$4, %ecx
	cmpb	$1, %dl
	sbbl	%edx, %edx
	andl	$2, %edx
	testb	$4, %al
	cmovne	%edx, %ecx
	shrb	$4, %al
	andl	$1, %eax
	xorl	$1, %eax
	movzbl	%al, %edx
	orl	%ecx, %edx
	movl	$1, %ecx
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE
	testb	%al, %al
	jne	.L2698
	jmp	.L2850
	.p2align 4,,10
	.p2align 3
.L2714:
	testb	%dl, %dl
	jne	.L2710
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L2725
	movq	(%rax), %rax
	leaq	-64(%rbp), %rdi
	movb	%r8b, -83(%rbp)
	movb	%r10b, -82(%rbp)
	movq	%rax, -64(%rbp)
	movq	24(%r14), %rax
	movb	%r9b, -81(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal6Object9SameValueES1_@PLT
	movzbl	-81(%rbp), %r9d
	movzbl	-82(%rbp), %r10d
	testb	%al, %al
	movzbl	-83(%rbp), %r8d
	je	.L2866
.L2725:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L2710
	movq	(%rax), %rax
	movb	%r8b, -83(%rbp)
	movb	%r10b, -82(%rbp)
	movb	%r9b, -81(%rbp)
	movq	%rax, -64(%rbp)
	movq	16(%r14), %rax
.L2868:
	movq	(%rax), %rsi
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal6Object9SameValueES1_@PLT
	movzbl	-81(%rbp), %r9d
	movzbl	-82(%rbp), %r10d
	testb	%al, %al
	movzbl	-83(%rbp), %r8d
	jne	.L2710
	jmp	.L2866
	.p2align 4,,10
	.p2align 3
.L2880:
	movq	24(%r14), %rax
	leaq	104(%r12), %rdx
	testq	%rax, %rax
	cmovne	%rax, %rdx
	jmp	.L2750
	.p2align 4,,10
	.p2align 3
.L2879:
	movq	16(%r14), %rax
	leaq	104(%r12), %rsi
	testq	%rax, %rax
	cmovne	%rax, %rsi
	jmp	.L2747
	.p2align 4,,10
	.p2align 3
.L2881:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14GetShouldThrowEPNS0_7IsolateENS_5MaybeINS0_11ShouldThrowEEE@PLT
	cmpl	$1, %eax
	je	.L2704
	movq	16(%rbp), %rdx
	testq	%r13, %r13
	je	.L2668
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator7GetNameEv
	movq	%rax, %rdx
.L2668:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$49, %esi
	jmp	.L2849
.L2878:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21447:
	.size	_ZN2v88internal10JSReceiver34ValidateAndApplyPropertyDescriptorEPNS0_7IsolateEPNS0_14LookupIteratorEbPNS0_18PropertyDescriptorES7_NS_5MaybeINS0_11ShouldThrowEEENS0_6HandleINS0_4NameEEE, .-_ZN2v88internal10JSReceiver34ValidateAndApplyPropertyDescriptorEPNS0_7IsolateEPNS0_14LookupIteratorEbPNS0_18PropertyDescriptorES7_NS_5MaybeINS0_11ShouldThrowEEENS0_6HandleINS0_4NameEEE
	.section	.text._ZN2v88internal10JSReceiver30IsCompatiblePropertyDescriptorEPNS0_7IsolateEbPNS0_18PropertyDescriptorES5_NS0_6HandleINS0_4NameEEENS_5MaybeINS0_11ShouldThrowEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver30IsCompatiblePropertyDescriptorEPNS0_7IsolateEbPNS0_18PropertyDescriptorES5_NS0_6HandleINS0_4NameEEENS_5MaybeINS0_11ShouldThrowEEE
	.type	_ZN2v88internal10JSReceiver30IsCompatiblePropertyDescriptorEPNS0_7IsolateEbPNS0_18PropertyDescriptorES5_NS0_6HandleINS0_4NameEEENS_5MaybeINS0_11ShouldThrowEEE, @function
_ZN2v88internal10JSReceiver30IsCompatiblePropertyDescriptorEPNS0_7IsolateEbPNS0_18PropertyDescriptorES5_NS0_6HandleINS0_4NameEEENS_5MaybeINS0_11ShouldThrowEEE:
.LFB21446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	movzbl	%sil, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%r8
	movq	%rcx, %r8
	movq	%r10, %rcx
	call	_ZN2v88internal10JSReceiver34ValidateAndApplyPropertyDescriptorEPNS0_7IsolateEPNS0_14LookupIteratorEbPNS0_18PropertyDescriptorES7_NS_5MaybeINS0_11ShouldThrowEEENS0_6HandleINS0_4NameEEE
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21446:
	.size	_ZN2v88internal10JSReceiver30IsCompatiblePropertyDescriptorEPNS0_7IsolateEbPNS0_18PropertyDescriptorES5_NS0_6HandleINS0_4NameEEENS_5MaybeINS0_11ShouldThrowEEE, .-_ZN2v88internal10JSReceiver30IsCompatiblePropertyDescriptorEPNS0_7IsolateEbPNS0_18PropertyDescriptorES5_NS0_6HandleINS0_4NameEEENS_5MaybeINS0_11ShouldThrowEEE
	.section	.rodata._ZN2v88internal10JSReceiver25OrdinaryDefineOwnPropertyEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE.str1.1,"aMS",@progbits,1
.LC72:
	.string	"interceptor-indexed-define"
.LC73:
	.string	"interceptor-named-define"
	.section	.text._ZN2v88internal10JSReceiver25OrdinaryDefineOwnPropertyEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver25OrdinaryDefineOwnPropertyEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE
	.type	_ZN2v88internal10JSReceiver25OrdinaryDefineOwnPropertyEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE, @function
_ZN2v88internal10JSReceiver25OrdinaryDefineOwnPropertyEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE:
.LFB21445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-240(%rbp), %rbx
	subq	$312, %rsp
	movq	%rsi, -296(%rbp)
	movq	%rbx, %rsi
	movq	%rdx, -304(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	andb	$-64, -240(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movups	%xmm0, -216(%rbp)
	call	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorE
	testb	%al, %al
	jne	.L2885
	xorl	%eax, %eax
	movb	$0, %ah
.L2886:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2970
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2885:
	.cfi_restore_state
	xorl	%esi, %esi
	cmpl	$-1, 72(%r12)
	movq	%r12, %rdi
	je	.L2887
	call	_ZN2v88internal14LookupIterator15RestartInternalILb1EEEvNS1_16InterceptorStateE@PLT
.L2888:
	movl	4(%r12), %eax
	leaq	-144(%rbp), %rcx
	movq	%rcx, -328(%rbp)
	cmpl	$4, %eax
	je	.L2922
	movq	%rbx, -336(%rbp)
	jmp	.L2889
	.p2align 4,,10
	.p2align 3
.L2892:
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	4(%r12), %eax
	cmpl	$4, %eax
	je	.L2971
.L2889:
	cmpl	$2, %eax
	jne	.L2892
	movq	%r12, %rdi
	call	_ZNK2v88internal14LookupIterator33HolderIsReceiverOrHiddenPrototypeEv@PLT
	testb	%al, %al
	je	.L2892
	movq	%r12, %rdi
	call	_ZNK2v88internal14LookupIterator14GetInterceptorEv
	movq	24(%r12), %r13
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	88(%r13), %rcx
	cmpq	%rcx, 55(%rax)
	je	.L2892
	movq	48(%r12), %rsi
	movq	56(%r12), %rbx
	movq	(%rsi), %rcx
	testb	$1, %cl
	jne	.L2894
.L2897:
	movq	%r13, %rdi
	call	_ZN2v88internal6Object15ConvertReceiverEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2967
	movq	(%r14), %rax
.L2898:
	movq	(%rsi), %rcx
	movq	63(%rax), %rdx
	movq	%r13, %rsi
	movq	(%rbx), %r8
	movq	-304(%rbp), %r9
	movq	-328(%rbp), %rdi
	call	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movl	$8, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v818PropertyDescriptorC1Ev@PLT
	movq	-296(%rbp), %rax
	movq	16(%rax), %rbx
	movq	24(%rax), %rdx
	testq	%rbx, %rbx
	je	.L2972
.L2901:
	movl	$8, %edi
	movq	%rdx, -320(%rbp)
	call	_Znwm@PLT
	movq	-320(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v818PropertyDescriptorC1ENS_5LocalINS_5ValueEEES3_@PLT
.L2965:
	movq	%r13, %rdi
	call	_ZN2v818PropertyDescriptorD1Ev@PLT
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	-296(%rbp), %rax
	movzbl	(%rax), %esi
.L2904:
	testb	$2, %sil
	jne	.L2973
.L2907:
	testb	$8, %sil
	jne	.L2974
.L2908:
	movl	72(%r12), %r11d
	movq	-104(%rbp), %rbx
	cmpl	$-1, %r11d
	je	.L2909
	movq	$0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2975
.L2910:
	movq	(%r14), %rax
	movq	55(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L2925
	movq	7(%rax), %rdx
	movq	%rdx, %r10
.L2911:
	xorl	%r13d, %r13d
	cmpl	$32, 41828(%rbx)
	movq	%r10, -352(%rbp)
	movl	%r11d, -320(%rbp)
	jne	.L2976
.L2918:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2977
.L2915:
	movq	24(%r12), %rdi
	movq	12552(%rdi), %rax
	cmpq	%rax, 96(%rdi)
	je	.L2921
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%r15, %rdi
	call	_ZN2v818PropertyDescriptorD1Ev@PLT
	movl	$8, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
.L2967:
	xorl	%edx, %edx
.L2899:
	movzwl	-338(%rbp), %ecx
	xorl	%eax, %eax
	movb	%dl, %al
	movb	%cl, %ah
	jmp	.L2886
	.p2align 4,,10
	.p2align 3
.L2921:
	movq	%r15, %rdi
	movb	%r13b, -338(%rbp)
	call	_ZN2v818PropertyDescriptorD1Ev@PLT
	movl	$8, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	testb	%r13b, %r13b
	je	.L2892
	movl	$1, %edx
	jmp	.L2899
	.p2align 4,,10
	.p2align 3
.L2971:
	movq	-336(%rbp), %rbx
.L2922:
	cmpl	$-1, 72(%r12)
	je	.L2978
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator15RestartInternalILb1EEEvNS1_16InterceptorStateE@PLT
.L2923:
	movq	48(%r12), %rdi
	call	_ZN2v88internal8JSObject12IsExtensibleENS0_6HandleIS1_EE
	subq	$8, %rsp
	movq	%rbx, %r8
	movq	%r12, %rsi
	pushq	$0
	movq	-296(%rbp), %rcx
	movzbl	%al, %edx
	movq	-304(%rbp), %r9
	movq	-312(%rbp), %rdi
	call	_ZN2v88internal10JSReceiver34ValidateAndApplyPropertyDescriptorEPNS0_7IsolateEPNS0_14LookupIteratorEbPNS0_18PropertyDescriptorES7_NS_5MaybeINS0_11ShouldThrowEEENS0_6HandleINS0_4NameEEE
	popq	%rdx
	popq	%rcx
	jmp	.L2886
	.p2align 4,,10
	.p2align 3
.L2976:
	movl	12616(%rbx), %eax
	leaq	-272(%rbp), %r14
	movq	%rbx, %rsi
	movl	$6, 12616(%rbx)
	movq	%r14, %rdi
	movl	%eax, -344(%rbp)
	call	_ZN2v88internal21ExternalCallbackScopeC1EPNS0_7IsolateEm
	leaq	-120(%rbp), %rax
	movq	%rax, -280(%rbp)
	movq	41016(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movl	-320(%rbp), %r11d
	movq	-352(%rbp), %r10
	testb	%al, %al
	jne	.L2979
.L2913:
	leaq	-280(%rbp), %rdx
	movq	%r15, %rsi
	movl	%r11d, %edi
	call	*%r10
.L2969:
	movq	-88(%rbp), %rax
	movq	%r14, %rdi
	cmpq	%rax, 96(%rbx)
	setne	%r13b
	call	_ZN2v88internal21ExternalCallbackScopeD1Ev
	movl	-344(%rbp), %eax
	movl	%eax, 12616(%rbx)
	jmp	.L2918
	.p2align 4,,10
	.p2align 3
.L2978:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator15RestartInternalILb0EEEvNS1_16InterceptorStateE@PLT
	jmp	.L2923
	.p2align 4,,10
	.p2align 3
.L2894:
	movq	-1(%rcx), %rcx
	cmpw	$1023, 11(%rcx)
	jbe	.L2897
	jmp	.L2898
	.p2align 4,,10
	.p2align 3
.L2909:
	pxor	%xmm0, %xmm0
	movq	32(%r12), %r10
	movq	$0, -160(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2980
.L2916:
	movq	(%r14), %rax
	movq	55(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L2927
	movq	7(%rax), %rdx
	movq	%rdx, %r11
.L2917:
	xorl	%r13d, %r13d
	cmpl	$32, 41828(%rbx)
	movq	%r11, -352(%rbp)
	movq	%r10, -320(%rbp)
	je	.L2918
	movl	12616(%rbx), %eax
	leaq	-272(%rbp), %r14
	movq	%rbx, %rsi
	movl	$6, 12616(%rbx)
	movq	%r14, %rdi
	movl	%eax, -344(%rbp)
	call	_ZN2v88internal21ExternalCallbackScopeC1EPNS0_7IsolateEm
	leaq	-120(%rbp), %rax
	movq	%rax, -280(%rbp)
	movq	41016(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-320(%rbp), %r10
	movq	-352(%rbp), %r11
	testb	%al, %al
	jne	.L2981
.L2919:
	leaq	-280(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r10, %rdi
	call	*%r11
	jmp	.L2969
	.p2align 4,,10
	.p2align 3
.L2974:
	shrb	$2, %sil
	movq	%r15, %rdi
	andl	$1, %esi
	call	_ZN2v818PropertyDescriptor16set_configurableEb@PLT
	jmp	.L2908
	.p2align 4,,10
	.p2align 3
.L2973:
	andl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v818PropertyDescriptor14set_enumerableEb@PLT
	movq	-296(%rbp), %rax
	movzbl	(%rax), %esi
	jmp	.L2907
	.p2align 4,,10
	.p2align 3
.L2972:
	testq	%rdx, %rdx
	jne	.L2901
	movzbl	(%rax), %esi
	movq	8(%rax), %rbx
	movl	%esi, %eax
	shrb	$5, %al
	andl	$1, %eax
	testq	%rbx, %rbx
	je	.L2982
	testb	%al, %al
	jne	.L2905
	movl	$8, %edi
	call	_Znwm@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v818PropertyDescriptorC1ENS_5LocalINS_5ValueEEE@PLT
	jmp	.L2965
	.p2align 4,,10
	.p2align 3
.L2887:
	call	_ZN2v88internal14LookupIterator15RestartInternalILb0EEEvNS1_16InterceptorStateE@PLT
	jmp	.L2888
.L2979:
	movq	-112(%rbp), %rdx
	movl	%r11d, %ecx
	leaq	.LC72(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj@PLT
	movq	-352(%rbp), %r10
	movl	-320(%rbp), %r11d
	jmp	.L2913
.L2977:
	leaq	-184(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2915
.L2975:
	movq	40960(%rbx), %rax
	leaq	-184(%rbp), %rsi
	movl	$154, %edx
	movl	%r11d, -320(%rbp)
	leaq	23240(%rax), %rdi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movl	-320(%rbp), %r11d
	jmp	.L2910
.L2925:
	xorl	%edx, %edx
	xorl	%r10d, %r10d
	jmp	.L2911
.L2981:
	movq	(%r10), %rcx
	movq	-112(%rbp), %rdx
	leaq	.LC73(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE@PLT
	movq	-352(%rbp), %r11
	movq	-320(%rbp), %r10
	jmp	.L2919
.L2982:
	movq	%r13, %r15
	testb	%al, %al
	je	.L2904
.L2905:
	shrb	$4, %sil
	movl	$8, %edi
	movl	%esi, %edx
	andl	$1, %edx
	movl	%edx, -320(%rbp)
	call	_Znwm@PLT
	movl	-320(%rbp), %edx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v818PropertyDescriptorC1ENS_5LocalINS_5ValueEEEb@PLT
	jmp	.L2965
.L2980:
	movq	40960(%rbx), %rax
	leaq	-184(%rbp), %rsi
	movl	$169, %edx
	movq	%r10, -320(%rbp)
	leaq	23240(%rax), %rdi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	-320(%rbp), %r10
	jmp	.L2916
.L2927:
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	jmp	.L2917
.L2970:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21445:
	.size	_ZN2v88internal10JSReceiver25OrdinaryDefineOwnPropertyEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE, .-_ZN2v88internal10JSReceiver25OrdinaryDefineOwnPropertyEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE
	.section	.text._ZN2v88internal10JSReceiver25OrdinaryDefineOwnPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver25OrdinaryDefineOwnPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE
	.type	_ZN2v88internal10JSReceiver25OrdinaryDefineOwnPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE, @function
_ZN2v88internal10JSReceiver25OrdinaryDefineOwnPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE:
.LFB21438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	movq	%r8, %r14
	leaq	-129(%rbp), %r8
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	pushq	%r12
	movq	%rdi, %rsi
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$0, -129(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	movl	-124(%rbp), %eax
	testl	%eax, %eax
	jne	.L2984
	movq	%r15, %rdi
	call	_ZNK2v88internal14LookupIterator9HasAccessEv@PLT
	testb	%al, %al
	jne	.L2985
	movq	-72(%rbp), %rsi
	movq	%r12, %rdi
	movl	$257, %r13d
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L2987
	xorb	%r13b, %r13b
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movl	%r13d, %eax
	movb	$0, %ah
	movl	%eax, %r13d
	jmp	.L2987
	.p2align 4,,10
	.p2align 3
.L2985:
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
.L2984:
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal10JSReceiver25OrdinaryDefineOwnPropertyEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE
	movl	%eax, %r13d
.L2987:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2990
	addq	$112, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2990:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21438:
	.size	_ZN2v88internal10JSReceiver25OrdinaryDefineOwnPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE, .-_ZN2v88internal10JSReceiver25OrdinaryDefineOwnPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE
	.section	.text._ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE
	.type	_ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE, @function
_ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE:
.LFB21437:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	-1(%rax), %r9
	cmpw	$1061, 11(%r9)
	je	.L2995
	movq	-1(%rax), %r9
	cmpw	$1024, 11(%r9)
	je	.L2996
	movq	-1(%rax), %rax
	cmpw	$1086, 11(%rax)
	je	.L2997
	jmp	_ZN2v88internal10JSReceiver25OrdinaryDefineOwnPropertyEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE
	.p2align 4,,10
	.p2align 3
.L2996:
	jmp	_ZN2v88internal7JSProxy17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE@PLT
	.p2align 4,,10
	.p2align 3
.L2995:
	jmp	_ZN2v88internal7JSArray17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE@PLT
	.p2align 4,,10
	.p2align 3
.L2997:
	jmp	_ZN2v88internal12JSTypedArray17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE@PLT
	.cfi_endproc
.LFE21437:
	.size	_ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE, .-_ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE
	.section	.rodata._ZN2v88internal10JSReceiver14DefinePropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_.str1.1,"aMS",@progbits,1
.LC74:
	.string	"Object.defineProperty"
.LC75:
	.string	"success.FromJust()"
	.section	.text._ZN2v88internal10JSReceiver14DefinePropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver14DefinePropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_
	.type	_ZN2v88internal10JSReceiver14DefinePropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_, @function
_ZN2v88internal10JSReceiver14DefinePropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_:
.LFB21435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L2999
.L3001:
	leaq	.LC74(%rip), %rax
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	$21, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movl	$26, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rdx
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L3000:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L3012
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2999:
	.cfi_restore_state
	movq	-1(%rax), %rax
	movq	%rdx, %r13
	cmpw	$1023, 11(%rax)
	jbe	.L3001
	movq	(%rdx), %rax
	movq	%rsi, %rbx
	testb	$1, %al
	jne	.L3013
.L3003:
	leaq	-80(%rbp), %r14
	pxor	%xmm0, %xmm0
	movq	%rcx, %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	andb	$-64, -80(%rbp)
	movups	%xmm0, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	call	_ZN2v88internal18PropertyDescriptor20ToPropertyDescriptorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPS1_@PLT
	testb	%al, %al
	jne	.L3005
.L3011:
	movq	312(%r12), %rax
	jmp	.L3000
	.p2align 4,,10
	.p2align 3
.L3013:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	jbe	.L3003
	movq	%rdx, %rsi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal6Object20ConvertToPropertyKeyEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-88(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L3011
	jmp	.L3003
	.p2align 4,,10
	.p2align 3
.L3005:
	movl	$1, %r8d
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE
	testb	%al, %al
	je	.L3011
	shrw	$8, %ax
	je	.L3014
	movq	(%rbx), %rax
	jmp	.L3000
.L3014:
	leaq	.LC75(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3012:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21435:
	.size	_ZN2v88internal10JSReceiver14DefinePropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_, .-_ZN2v88internal10JSReceiver14DefinePropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_
	.section	.text._ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE
	.type	_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE, @function
_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE:
.LFB21449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	48(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	0(%r13), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L3016
	movq	%r8, %rdx
	call	_ZN2v88internal8JSObject18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE
.L3017:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L3026
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3016:
	.cfi_restore_state
	movq	32(%rdi), %rdx
	orb	$63, -96(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	%rsi, -88(%rbp)
	testq	%rdx, %rdx
	je	.L3027
.L3018:
	leaq	-96(%rbp), %rcx
	leaq	-37592(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE
	jmp	.L3017
	.p2align 4,,10
	.p2align 3
.L3027:
	movl	72(%rdi), %r14d
	movq	24(%rdi), %r15
	movq	%r8, -104(%rbp)
	testl	%r14d, %r14d
	js	.L3019
	movq	%r14, %rsi
	movl	$1, %edx
	movq	%r15, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb@PLT
	movq	-104(%rbp), %r8
	movq	%rax, %rdx
.L3020:
	movq	(%rdx), %rax
	movl	11(%rax), %esi
	cmpl	$10, %esi
	jle	.L3028
.L3023:
	movq	%rdx, 32(%r12)
	jmp	.L3018
	.p2align 4,,10
	.p2align 3
.L3019:
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	-104(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L3020
	.p2align 4,,10
	.p2align 3
.L3028:
	cmpl	$3, 7(%rax)
	jne	.L3023
	movl	%r14d, %edi
	movq	%r8, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r8
	movq	(%rdx), %rcx
	movl	%eax, 7(%rcx)
	jmp	.L3023
.L3026:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21449:
	.size	_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE, .-_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE
	.section	.rodata._ZN2v88internal10JSReceiver16DefinePropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_.str1.1,"aMS",@progbits,1
.LC76:
	.string	"Object.defineProperties"
	.section	.rodata._ZN2v88internal10JSReceiver16DefinePropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_.str1.8,"aMS",@progbits,1
	.align 8
.LC77:
	.string	"cannot create std::vector larger than max_size()"
	.section	.rodata._ZN2v88internal10JSReceiver16DefinePropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_.str1.1
.LC78:
	.string	"status.FromJust()"
	.section	.text._ZN2v88internal10JSReceiver16DefinePropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver16DefinePropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_
	.type	_ZN2v88internal10JSReceiver16DefinePropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_, @function
_ZN2v88internal10JSReceiver16DefinePropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_:
.LFB21436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L3030
.L3032:
	leaq	.LC76(%rip), %rax
	movq	%r15, %rdi
	leaq	-144(%rbp), %rsi
	movq	$23, -136(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$26, %esi
	movq	%rax, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
.L3031:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3074
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3030:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3032
	movq	(%rdx), %rax
	movq	%rdx, -168(%rbp)
	movq	%rdx, %rsi
	testb	$1, %al
	jne	.L3033
.L3036:
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L3075
.L3035:
	movq	-168(%rbp), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	$1, %ecx
	xorl	%esi, %esi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L3031
	movabsq	$230584300921369395, %rdx
	movq	(%rax), %rax
	movslq	11(%rax), %rax
	cmpq	%rdx, %rax
	ja	.L3076
	testq	%rax, %rax
	je	.L3039
	leaq	(%rax,%rax,4), %rdx
	leaq	0(,%rdx,8), %r12
	movq	%r12, %rdi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -200(%rbp)
	leaq	(%rax,%r12), %rdx
	.p2align 4,,10
	.p2align 3
.L3040:
	andb	$-64, (%rax)
	addq	$40, %rax
	movups	%xmm0, -32(%rax)
	movups	%xmm0, -16(%rax)
	cmpq	%rdx, %rax
	jne	.L3040
	movq	-176(%rbp), %rax
	movq	(%rax), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L3043
	leaq	-145(%rbp), %rcx
	movq	%rbx, -208(%rbp)
	xorl	%r12d, %r12d
	movq	%r15, %rbx
	movq	$0, -184(%rbp)
	leaq	-144(%rbp), %r13
	movq	%rcx, %r15
	jmp	.L3042
	.p2align 4,,10
	.p2align 3
.L3078:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L3046:
	movq	-168(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r15, %r8
	movq	%r14, %rcx
	movl	$1, %r9d
	movq	%rbx, %rsi
	movb	$0, -145(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE
	testb	%al, %al
	je	.L3073
	shrq	$32, %rax
	cmpl	$64, %eax
	je	.L3053
	testb	$2, %al
	jne	.L3053
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3073
	movq	-184(%rbp), %rax
	movq	-200(%rbp), %rcx
	movq	%rbx, %rdi
	leaq	(%rax,%rax,4), %rax
	leaq	(%rcx,%rax,8), %rdx
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal18PropertyDescriptor20ToPropertyDescriptorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPS1_@PLT
	movb	%al, -145(%rbp)
	testb	%al, %al
	je	.L3073
	movq	-192(%rbp), %rdx
	addq	$1, -184(%rbp)
	movq	%r14, 32(%rdx)
.L3053:
	movq	-176(%rbp), %rax
	addq	$1, %r12
	movq	(%rax), %rax
	cmpl	%r12d, 11(%rax)
	jle	.L3077
.L3042:
	movq	15(%rax,%r12,8), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L3078
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L3079
.L3047:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L3046
	.p2align 4,,10
	.p2align 3
.L3073:
	xorl	%eax, %eax
.L3041:
	movq	-200(%rbp), %rdi
	movq	%rax, -168(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %rax
	jmp	.L3031
	.p2align 4,,10
	.p2align 3
.L3079:
	movq	%rbx, %rdi
	movq	%rsi, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L3047
	.p2align 4,,10
	.p2align 3
.L3033:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3036
	jmp	.L3035
	.p2align 4,,10
	.p2align 3
.L3039:
	movq	%rbx, %rax
	jmp	.L3031
	.p2align 4,,10
	.p2align 3
.L3075:
	xorl	%eax, %eax
	jmp	.L3031
	.p2align 4,,10
	.p2align 3
.L3077:
	cmpq	$0, -184(%rbp)
	movq	%rbx, %r15
	movq	-208(%rbp), %rbx
	je	.L3043
	xorl	%r12d, %r12d
	movq	-200(%rbp), %r13
	movq	%r12, %r14
	movq	-216(%rbp), %r12
	jmp	.L3057
	.p2align 4,,10
	.p2align 3
.L3056:
	addq	$1, %r14
	addq	$40, %r13
	cmpq	%r14, -184(%rbp)
	je	.L3043
.L3057:
	movb	$1, %r12b
	movq	32(%r13), %rdx
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movl	%r12d, %r12d
	movq	%r15, %rdi
	movq	%r12, %r8
	call	_ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE
	testb	%al, %al
	je	.L3073
	shrw	$8, %ax
	jne	.L3056
	leaq	.LC78(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3043:
	movq	%rbx, %rax
	jmp	.L3041
.L3074:
	call	__stack_chk_fail@PLT
.L3076:
	leaq	.LC77(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21436:
	.size	_ZN2v88internal10JSReceiver16DefinePropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_, .-_ZN2v88internal10JSReceiver16DefinePropertiesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_
	.section	.text._ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE
	.type	_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE, @function
_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE:
.LFB21448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	movq	%rdx, %rcx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L3114
.L3082:
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L3089
.L3091:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rcx, -248(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-248(%rbp), %rcx
	movq	%rax, %rdx
.L3090:
	movq	(%r12), %rsi
	movl	$1, %eax
	movq	-1(%rsi), %rdi
	cmpw	$64, 11(%rdi)
	jne	.L3092
	movl	11(%rsi), %eax
	notl	%eax
	andl	$1, %eax
.L3092:
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	(%r12), %rax
	movq	%r13, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L3115
.L3093:
	leaq	-224(%rbp), %rdi
	movq	%rcx, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L3088:
	movq	-176(%rbp), %r12
	movq	(%r12), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L3094
	leaq	-224(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal8JSObject18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE
.L3095:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L3116
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3094:
	.cfi_restore_state
	movq	-192(%rbp), %rdx
	orb	$63, -144(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	%r15, -136(%rbp)
	testq	%rdx, %rdx
	je	.L3117
.L3096:
	leaq	-144(%rbp), %rcx
	leaq	-37592(%rbx), %rdi
	movq	%r14, %r8
	movq	%r12, %rsi
	call	_ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE
	jmp	.L3095
	.p2align 4,,10
	.p2align 3
.L3114:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L3083
	testb	$2, %al
	jne	.L3082
.L3083:
	leaq	-144(%rbp), %r8
	leaq	-228(%rbp), %rsi
	movq	%rcx, -256(%rbp)
	movq	%r8, %rdi
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-256(%rbp), %rcx
	testb	%al, %al
	je	.L3082
	movq	(%rbx), %rax
	movl	-228(%rbp), %edx
	movq	-248(%rbp), %r8
	testb	$1, %al
	jne	.L3085
.L3087:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -256(%rbp)
	movl	%edx, -248(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movl	-248(%rbp), %edx
	movq	-256(%rbp), %r8
.L3086:
	movabsq	$824633720832, %rsi
	movq	%r8, %rdi
	movq	%r13, -120(%rbp)
	movl	$1, -144(%rbp)
	movq	%rsi, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%edx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r12, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L3088
	.p2align 4,,10
	.p2align 3
.L3089:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3091
	movq	%rbx, %rdx
	jmp	.L3090
	.p2align 4,,10
	.p2align 3
.L3085:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3087
	movq	%rbx, %rax
	jmp	.L3086
	.p2align 4,,10
	.p2align 3
.L3115:
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rdx, -248(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-248(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L3093
	.p2align 4,,10
	.p2align 3
.L3117:
	movl	-152(%rbp), %r13d
	movq	-200(%rbp), %r15
	testl	%r13d, %r13d
	js	.L3097
	movq	%r13, %rsi
	movl	$1, %edx
	movq	%r15, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb@PLT
	movq	%rax, %rdx
.L3098:
	movq	(%rdx), %rax
	movl	11(%rax), %esi
	cmpl	$10, %esi
	jle	.L3118
.L3101:
	movq	%rdx, -192(%rbp)
	jmp	.L3096
	.p2align 4,,10
	.p2align 3
.L3097:
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rax, %rdx
	jmp	.L3098
	.p2align 4,,10
	.p2align 3
.L3118:
	cmpl	$3, 7(%rax)
	jne	.L3101
	movl	%r13d, %edi
	movq	%rdx, -248(%rbp)
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movq	-248(%rbp), %rdx
	movq	(%rdx), %rcx
	movl	%eax, 7(%rcx)
	jmp	.L3101
.L3116:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21448:
	.size	_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE, .-_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE
	.section	.rodata._ZN2v88internal8JSObject11SetAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_12AccessorInfoEEENS0_18PropertyAttributesE.str1.8,"aMS",@progbits,1
	.align 8
.LC79:
	.string	"GetPropertyAttributes(&it).IsJust()"
	.section	.text._ZN2v88internal8JSObject11SetAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_12AccessorInfoEEENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject11SetAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_12AccessorInfoEEENS0_18PropertyAttributesE
	.type	_ZN2v88internal8JSObject11SetAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_12AccessorInfoEEENS0_18PropertyAttributesE, @function
_ZN2v88internal8JSObject11SetAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_12AccessorInfoEEENS0_18PropertyAttributesE:
.LFB21524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	movq	%rsi, %rcx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %r14
	movq	(%rsi), %rdx
	movq	-1(%rdx), %rsi
	subq	$37592, %r14
	cmpw	$63, 11(%rsi)
	jbe	.L3151
.L3121:
	testb	$1, %al
	jne	.L3128
.L3130:
	movl	$-1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rcx, -248(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-248(%rbp), %rcx
	movq	%rax, %rdx
.L3129:
	movq	0(%r13), %rax
	movq	-1(%rax), %rsi
	movq	%r14, -200(%rbp)
	movabsq	$824633720832, %rsi
	movl	$0, -224(%rbp)
	movq	%rsi, -212(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L3152
.L3131:
	leaq	-224(%rbp), %rdi
	movq	%rcx, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r12, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movl	-220(%rbp), %eax
	testl	%eax, %eax
	je	.L3153
.L3132:
	cmpl	$-1, -152(%rbp)
	je	.L3136
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$17, %eax
	cmpb	$10, %al
	jbe	.L3139
.L3136:
	leaq	-224(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE
	testb	%al, %al
	je	.L3154
	cmpl	$4, -220(%rbp)
	je	.L3138
	testb	$32, -208(%rbp)
	jne	.L3139
.L3138:
	movl	%ebx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator24TransitionToAccessorPairENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	%r12, %rax
.L3135:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L3155
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3139:
	.cfi_restore_state
	movq	-200(%rbp), %rax
	addq	$88, %rax
	jmp	.L3135
	.p2align 4,,10
	.p2align 3
.L3151:
	movq	%rdx, -144(%rbp)
	movl	7(%rdx), %edx
	testb	$1, %dl
	jne	.L3122
	andl	$2, %edx
	jne	.L3121
.L3122:
	leaq	-144(%rbp), %r8
	leaq	-228(%rbp), %rsi
	movq	%rcx, -256(%rbp)
	movq	%r8, %rdi
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-248(%rbp), %r8
	movq	-256(%rbp), %rcx
	testb	%al, %al
	je	.L3156
	movq	(%r12), %rax
	movl	-228(%rbp), %edx
	testb	$1, %al
	jne	.L3124
.L3126:
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r8, -256(%rbp)
	movl	%edx, -248(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movl	-248(%rbp), %edx
	movq	-256(%rbp), %r8
.L3125:
	movabsq	$824633720832, %rcx
	movq	%r8, %rdi
	movq	%rax, -80(%rbp)
	movl	$0, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	%r14, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	movl	%edx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r13, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	-220(%rbp), %eax
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	testl	%eax, %eax
	jne	.L3132
.L3153:
	leaq	-224(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal14LookupIterator9HasAccessEv@PLT
	testb	%al, %al
	jne	.L3133
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	12552(%r14), %rax
	cmpq	%rax, 96(%r14)
	je	.L3139
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
	jmp	.L3135
	.p2align 4,,10
	.p2align 3
.L3133:
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	jmp	.L3132
	.p2align 4,,10
	.p2align 3
.L3128:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3130
	movq	%r12, %rdx
	jmp	.L3129
	.p2align 4,,10
	.p2align 3
.L3156:
	movq	(%r12), %rax
	jmp	.L3121
	.p2align 4,,10
	.p2align 3
.L3124:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3126
	movq	%r12, %rax
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3152:
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rdx, -248(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-248(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L3131
	.p2align 4,,10
	.p2align 3
.L3154:
	leaq	.LC79(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L3155:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21524:
	.size	_ZN2v88internal8JSObject11SetAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_12AccessorInfoEEENS0_18PropertyAttributesE, .-_ZN2v88internal8JSObject11SetAccessorENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_12AccessorInfoEEENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal8JSObject17SlowReverseLookupENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject17SlowReverseLookupENS0_6ObjectE
	.type	_ZN2v88internal8JSObject17SlowReverseLookupENS0_6ObjectE, @function
_ZN2v88internal8JSObject17SlowReverseLookupENS0_6ObjectE:
.LFB21525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	movq	-1(%rax), %rdx
	jne	.L3158
	movl	15(%rdx), %ecx
	movq	%rsi, %r14
	movq	-1(%rax), %rdx
	notq	%r14
	shrl	$10, %ecx
	andl	$1, %r14d
	movq	39(%rdx), %r8
	andl	$1023, %ecx
	movl	%r14d, %r13d
	testb	%r14b, %r14b
	je	.L3230
	testl	%ecx, %ecx
	je	.L3160
.L3239:
	movq	%r12, %rax
	movl	$8, %ebx
	leaq	23(%r8), %rdx
	shrq	$32, %rax
	subq	%r8, %rbx
	movq	%rax, -72(%rbp)
	leal	-1(%rcx), %eax
	leaq	(%rax,%rax,2), %rax
	leaq	47(%r8,%rax,8), %rsi
	jmp	.L3161
	.p2align 4,,10
	.p2align 3
.L3162:
	testb	$1, %al
	je	.L3231
.L3181:
	addq	$24, %rdx
	cmpq	%rsi, %rdx
	je	.L3232
.L3161:
	movq	8(%rdx), %rax
	sarq	$32, %rax
	testb	$2, %al
	jne	.L3162
	movq	(%rdi), %r15
	leaq	(%rdx,%rbx), %rax
	movq	-1(%r15), %r11
	addq	39(%r11), %rax
	movq	(%rax), %r8
	movzbl	7(%r11), %ecx
	movq	%r8, %rax
	shrq	$38, %r8
	shrq	$51, %rax
	andl	$7, %r8d
	movq	%rax, %r9
	movzbl	8(%r11), %eax
	andl	$1023, %r9d
	subl	%eax, %ecx
	cmpl	%ecx, %r9d
	setl	%al
	jl	.L3233
	subl	%ecx, %r9d
	movl	$16, %r10d
	leal	16(,%r9,8), %r9d
.L3164:
	cmpl	$2, %r8d
	jne	.L3234
	movl	$32768, %r8d
.L3165:
	movzbl	%al, %eax
	movslq	%ecx, %rcx
	movslq	%r9d, %r9
	movslq	%r10d, %r10
	salq	$14, %rax
	salq	$17, %rcx
	orq	%rax, %rcx
	salq	$27, %r10
	movq	-1(%r15), %rax
	orq	%rcx, %r9
	orq	%r9, %r10
	orq	%r10, %r8
	shrq	$30, %r10
	movl	%r8d, %ecx
	andl	$15, %r10d
	sarl	$3, %ecx
	andl	$2047, %ecx
	subl	%r10d, %ecx
	testl	$16384, %r8d
	jne	.L3168
	movq	7(%r15), %r9
	andq	$-262144, %r15
	movq	24(%r15), %rax
	subq	$37592, %rax
	testb	$1, %r9b
	je	.L3186
	cmpq	288(%rax), %r9
	je	.L3186
	.p2align 4,,10
	.p2align 3
.L3170:
	leal	16(,%rcx,8), %eax
	shrq	$15, %r8
	cltq
	andl	$3, %r8d
	movq	-1(%r9,%rax), %rax
	cmpl	$1, %r8d
	je	.L3235
.L3187:
	cmpq	%r12, %rax
	jne	.L3181
.L3225:
	movq	(%rdx), %rax
	jmp	.L3193
	.p2align 4,,10
	.p2align 3
.L3158:
	cmpw	$1025, 11(%rdx)
	je	.L3236
	movq	7(%rax), %rdx
	testb	$1, %dl
	je	.L3237
.L3196:
	leaq	-64(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10DictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE17SlowReverseLookupENS0_6ObjectE@PLT
.L3193:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L3238
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3230:
	.cfi_restore_state
	movq	-1(%rsi), %rdx
	cmpw	$65, 11(%rdx)
	sete	%r13b
	testl	%ecx, %ecx
	jne	.L3239
	jmp	.L3160
	.p2align 4,,10
	.p2align 3
.L3186:
	movq	968(%rax), %r9
	jmp	.L3170
	.p2align 4,,10
	.p2align 3
.L3232:
	movq	(%rdi), %rax
.L3160:
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	jmp	.L3193
	.p2align 4,,10
	.p2align 3
.L3234:
	cmpb	$2, %r8b
	jg	.L3166
	je	.L3167
.L3201:
	xorl	%r8d, %r8d
	jmp	.L3165
	.p2align 4,,10
	.p2align 3
.L3166:
	subl	$3, %r8d
	cmpb	$1, %r8b
	jbe	.L3201
.L3167:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3231:
	movq	16(%rdx), %rax
	cmpq	%rax, %r12
	jne	.L3181
	jmp	.L3225
	.p2align 4,,10
	.p2align 3
.L3168:
	movq	47(%rax), %rax
	testq	%rax, %rax
	je	.L3180
	movq	%rax, %r11
	movl	$32, %r9d
	notq	%r11
	movl	%r11d, %r10d
	andl	$1, %r10d
	jne	.L3172
	movslq	11(%rax), %r9
	sall	$3, %r9d
.L3172:
	cmpl	%r9d, %ecx
	jnb	.L3180
	testl	%ecx, %ecx
	leal	31(%rcx), %r9d
	cmovns	%ecx, %r9d
	sarl	$5, %r9d
	andl	$1, %r11d
	jne	.L3173
	cmpl	%r9d, 11(%rax)
	jle	.L3173
	movl	%ecx, %r11d
	movl	$1, %r15d
	sarl	$31, %r11d
	shrl	$27, %r11d
	addl	%r11d, %ecx
	andl	$31, %ecx
	subl	%r11d, %ecx
	sall	%cl, %r15d
	movl	%r15d, %ecx
	testb	%r10b, %r10b
	je	.L3240
.L3177:
	sarq	$32, %rax
	testl	%eax, %ecx
	sete	%al
.L3179:
	testb	%al, %al
	jne	.L3180
	testb	%r13b, %r13b
	je	.L3181
	movq	(%rdi), %rax
	andl	$16376, %r8d
	movsd	-1(%r8,%rax), %xmm0
	testb	%r14b, %r14b
	je	.L3190
.L3229:
	pxor	%xmm1, %xmm1
	cvtsi2sdl	-72(%rbp), %xmm1
.L3191:
	ucomisd	%xmm0, %xmm1
	jp	.L3181
	je	.L3225
	jmp	.L3181
	.p2align 4,,10
	.p2align 3
.L3233:
	movzbl	8(%r11), %r10d
	movzbl	8(%r11), %r11d
	addl	%r11d, %r9d
	sall	$3, %r10d
	sall	$3, %r9d
	jmp	.L3164
	.p2align 4,,10
	.p2align 3
.L3180:
	movq	%r8, %rcx
	movq	(%rdi), %rax
	shrq	$15, %r8
	andl	$16376, %ecx
	andl	$3, %r8d
	movq	-1(%rcx,%rax), %rax
	cmpl	$1, %r8d
	jne	.L3187
.L3235:
	testb	%r13b, %r13b
	je	.L3181
	testb	$1, %al
	je	.L3241
	movsd	7(%rax), %xmm0
.L3189:
	testb	%r14b, %r14b
	jne	.L3229
	.p2align 4,,10
	.p2align 3
.L3190:
	movsd	7(%r12), %xmm1
	jmp	.L3191
	.p2align 4,,10
	.p2align 3
.L3240:
	sall	$2, %r9d
	movslq	%r9d, %r9
	movl	15(%rax,%r9), %eax
	testl	%eax, %r15d
	sete	%al
	jmp	.L3179
	.p2align 4,,10
	.p2align 3
.L3236:
	movq	7(%rax), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal10DictionaryINS0_16GlobalDictionaryENS0_21GlobalDictionaryShapeEE17SlowReverseLookupENS0_6ObjectE@PLT
	jmp	.L3193
	.p2align 4,,10
	.p2align 3
.L3237:
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %rdx
	jmp	.L3196
.L3241:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	jmp	.L3189
	.p2align 4,,10
	.p2align 3
.L3173:
	cmpl	$31, %ecx
	jg	.L3175
	testb	%r10b, %r10b
	je	.L3175
	movl	$1, %r15d
	sall	%cl, %r15d
	movl	%r15d, %ecx
	jmp	.L3177
.L3175:
	leaq	.LC12(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3238:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21525:
	.size	_ZN2v88internal8JSObject17SlowReverseLookupENS0_6ObjectE, .-_ZN2v88internal8JSObject17SlowReverseLookupENS0_6ObjectE
	.section	.text._ZN2v88internal8JSObject35PrototypeRegistryCompactionCallbackENS0_10HeapObjectEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject35PrototypeRegistryCompactionCallbackENS0_10HeapObjectEii
	.type	_ZN2v88internal8JSObject35PrototypeRegistryCompactionCallbackENS0_10HeapObjectEii, @function
_ZN2v88internal8JSObject35PrototypeRegistryCompactionCallbackENS0_10HeapObjectEii:
.LFB21526:
	.cfi_startproc
	endbr64
	movq	71(%rdi), %rax
	salq	$32, %rdx
	movq	%rdx, 23(%rax)
	ret
	.cfi_endproc
.LFE21526:
	.size	_ZN2v88internal8JSObject35PrototypeRegistryCompactionCallbackENS0_10HeapObjectEii, .-_ZN2v88internal8JSObject35PrototypeRegistryCompactionCallbackENS0_10HeapObjectEii
	.section	.rodata._ZN2v88internal8JSObject25LazyRegisterPrototypeUserENS0_6HandleINS0_3MapEEEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC80:
	.string	"Registering %p as a user of prototype %p (map=%p).\n"
	.section	.text._ZN2v88internal8JSObject25LazyRegisterPrototypeUserENS0_6HandleINS0_3MapEEEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject25LazyRegisterPrototypeUserENS0_6HandleINS0_3MapEEEPNS0_7IsolateE
	.type	_ZN2v88internal8JSObject25LazyRegisterPrototypeUserENS0_6HandleINS0_3MapEEEPNS0_7IsolateE, @function
_ZN2v88internal8JSObject25LazyRegisterPrototypeUserENS0_6HandleINS0_3MapEEEPNS0_7IsolateE:
.LFB21531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleIS1_EEPNS0_7IsolateE@PLT
	movq	%r15, %rsi
	movq	%rax, %r14
	movq	(%r12), %rax
	movq	%rax, -64(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal3Map24GetPrototypeChainRootMapEPNS0_7IsolateE@PLT
	movq	41112(%r15), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3244
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L3245:
	movq	104(%r15), %rax
	cmpq	%rax, (%rbx)
	je	.L3243
	movq	(%r14), %rax
	cmpl	$-1, 27(%rax)
	jne	.L3243
	.p2align 4,,10
	.p2align 3
.L3294:
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L3250
.L3253:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleINS0_8JSObjectEEEPNS0_7IsolateE@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	15(%rax), %r10
	testq	%rdi, %rdi
	je	.L3290
	movq	%r10, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r10
	movq	%rax, %r9
.L3255:
	andl	$1, %r10d
	movq	%r9, %rsi
	jne	.L3261
	movq	41112(%r15), %rdi
	movq	1080(%r15), %r10
	testq	%rdi, %rdi
	je	.L3258
	movq	%r10, %rsi
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %r9
	movq	%rax, %rsi
.L3261:
	movq	-80(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%r9, -72(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal14PrototypeUsers3AddEPNS0_7IsolateENS0_6HandleINS0_13WeakArrayListEEENS4_INS0_3MapEEEPi@PLT
	movslq	-64(%rbp), %rdx
	movq	(%r14), %rcx
	salq	$32, %rdx
	movq	%rdx, 23(%rcx)
	movq	-72(%rbp), %r9
	cmpq	%rax, %r9
	je	.L3262
	movq	(%rax), %r14
	cmpq	%r14, (%r9)
	je	.L3262
	movq	0(%r13), %rdi
	movq	%r14, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %r14b
	je	.L3262
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	je	.L3264
	movq	%r14, %rdx
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	8(%rcx), %rax
.L3264:
	testb	$24, %al
	je	.L3262
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3291
	.p2align 4,,10
	.p2align 3
.L3262:
	movq	(%rbx), %rdx
	cmpb	$0, _ZN2v88internal26FLAG_trace_prototype_usersE(%rip)
	leaq	-1(%rdx), %rax
	jne	.L3292
.L3266:
	movq	(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3267
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L3268:
	movq	(%rbx), %rax
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L3243
	movq	-1(%rax), %rax
	movq	41112(%r15), %rdi
	movq	104(%r15), %rdx
	movq	23(%rax), %r14
	testq	%rdi, %rdi
	je	.L3293
	movq	%r14, %rsi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rbx
.L3270:
	cmpq	%rdx, %r14
	je	.L3243
	movq	%r13, %r14
	movq	(%r14), %rax
	cmpl	$-1, 27(%rax)
	je	.L3294
	.p2align 4,,10
	.p2align 3
.L3243:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3295
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3290:
	.cfi_restore_state
	movq	41088(%r15), %r9
	cmpq	41096(%r15), %r9
	je	.L3296
.L3256:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r15)
	movq	%r10, (%r9)
	jmp	.L3255
	.p2align 4,,10
	.p2align 3
.L3267:
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L3297
.L3269:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r12)
	jmp	.L3268
	.p2align 4,,10
	.p2align 3
.L3250:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jne	.L3253
	jmp	.L3243
	.p2align 4,,10
	.p2align 3
.L3292:
	movq	-1(%rdx), %rcx
	movq	(%r12), %rsi
	xorl	%eax, %eax
	leaq	.LC80(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%rbx), %rax
	subq	$1, %rax
	jmp	.L3266
	.p2align 4,,10
	.p2align 3
.L3293:
	movq	41088(%r15), %rbx
	cmpq	41096(%r15), %rbx
	je	.L3298
.L3271:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%r14, (%rbx)
	jmp	.L3270
	.p2align 4,,10
	.p2align 3
.L3258:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L3299
.L3260:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r10, (%rsi)
	jmp	.L3261
	.p2align 4,,10
	.p2align 3
.L3291:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3262
	.p2align 4,,10
	.p2align 3
.L3244:
	movq	41088(%r15), %rbx
	cmpq	41096(%r15), %rbx
	je	.L3300
.L3246:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	jmp	.L3245
	.p2align 4,,10
	.p2align 3
.L3297:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L3269
	.p2align 4,,10
	.p2align 3
.L3296:
	movq	%r15, %rdi
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r10
	movq	%rax, %r9
	jmp	.L3256
	.p2align 4,,10
	.p2align 3
.L3298:
	movq	%r15, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L3271
	.p2align 4,,10
	.p2align 3
.L3299:
	movq	%r15, %rdi
	movq	%r10, -88(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r10
	movq	-72(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L3260
	.p2align 4,,10
	.p2align 3
.L3300:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L3246
.L3295:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21531:
	.size	_ZN2v88internal8JSObject25LazyRegisterPrototypeUserENS0_6HandleINS0_3MapEEEPNS0_7IsolateE, .-_ZN2v88internal8JSObject25LazyRegisterPrototypeUserENS0_6HandleINS0_3MapEEEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal8JSObject23UnregisterPrototypeUserENS0_6HandleINS0_3MapEEEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC81:
	.string	"Unregistering %p as a user of prototype %p.\n"
	.section	.text._ZN2v88internal8JSObject23UnregisterPrototypeUserENS0_6HandleINS0_3MapEEEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject23UnregisterPrototypeUserENS0_6HandleINS0_3MapEEEPNS0_7IsolateE
	.type	_ZN2v88internal8JSObject23UnregisterPrototypeUserENS0_6HandleINS0_3MapEEEPNS0_7IsolateE, @function
_ZN2v88internal8JSObject23UnregisterPrototypeUserENS0_6HandleINS0_3MapEEEPNS0_7IsolateE:
.LFB21532:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdx
	movq	71(%rdx), %rax
	testb	$1, %al
	jne	.L3302
.L3304:
	xorl	%r12d, %r12d
.L3301:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3302:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$95, 11(%rcx)
	jne	.L3304
	movq	%rsi, %r12
	movq	23(%rdx), %rsi
	movq	-1(%rsi), %rdx
	cmpw	$1024, 11(%rdx)
	ja	.L3305
	movq	15(%rax), %rax
	testb	$1, %al
	je	.L3304
	movq	-1(%rax), %rax
	cmpw	$167, 11(%rax)
	sete	%r12b
	jmp	.L3301
	.p2align 4,,10
	.p2align 3
.L3305:
	movq	%rdi, %rbx
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3306
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L3307:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal3Map24GetOrCreatePrototypeInfoENS0_6HandleIS1_EEPNS0_7IsolateE@PLT
	movq	(%rax), %rax
	movslq	27(%rax), %r14
	cmpq	$-1, %r14
	je	.L3304
	movq	0(%r13), %rax
	movq	-1(%rax), %rax
	movq	41112(%r12), %rdi
	movq	71(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3309
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3310:
	movq	41112(%r12), %rdi
	movq	15(%rsi), %r15
	testq	%rdi, %rdi
	je	.L3312
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r15
.L3313:
	leal	24(,%r14,8), %edx
	movq	23(%r15), %rax
	leaq	23(%r15), %rcx
	movslq	%edx, %rdx
	leaq	-1(%r15,%rdx), %r12
	movq	%rax, (%r12)
	testb	$1, %al
	je	.L3317
	cmpl	$3, %eax
	je	.L3317
	movq	%rax, %rdx
	andq	$-262144, %rax
	andq	$-3, %rdx
	testb	$4, 10(%rax)
	je	.L3316
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rdx
.L3316:
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	movq	-56(%rbp), %rcx
.L3317:
	salq	$32, %r14
	movq	%r14, (%rcx)
	movzbl	_ZN2v88internal26FLAG_trace_prototype_usersE(%rip), %r12d
	testb	%r12b, %r12b
	jne	.L3326
	movl	$1, %r12d
	jmp	.L3301
	.p2align 4,,10
	.p2align 3
.L3306:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3327
.L3308:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L3307
	.p2align 4,,10
	.p2align 3
.L3312:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L3328
.L3314:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%r15, (%rax)
	jmp	.L3313
	.p2align 4,,10
	.p2align 3
.L3309:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L3329
.L3311:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3310
	.p2align 4,,10
	.p2align 3
.L3326:
	movq	0(%r13), %rdx
	movq	(%rbx), %rsi
	leaq	.LC81(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3301
.L3327:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3308
.L3328:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L3314
.L3329:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L3311
	.cfi_endproc
.LFE21532:
	.size	_ZN2v88internal8JSObject23UnregisterPrototypeUserENS0_6HandleINS0_3MapEEEPNS0_7IsolateE, .-_ZN2v88internal8JSObject23UnregisterPrototypeUserENS0_6HandleINS0_3MapEEEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal8JSObject31UpdatePrototypeUserRegistrationENS0_6HandleINS0_3MapEEES4_PNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC82:
	.string	"Moving prototype_info %p from map %p to map %p.\n"
	.section	.text._ZN2v88internal8JSObject31UpdatePrototypeUserRegistrationENS0_6HandleINS0_3MapEEES4_PNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject31UpdatePrototypeUserRegistrationENS0_6HandleINS0_3MapEEES4_PNS0_7IsolateE
	.type	_ZN2v88internal8JSObject31UpdatePrototypeUserRegistrationENS0_6HandleINS0_3MapEEES4_PNS0_7IsolateE, @function
_ZN2v88internal8JSObject31UpdatePrototypeUserRegistrationENS0_6HandleINS0_3MapEEES4_PNS0_7IsolateE:
.LFB21483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	call	_ZN2v88internal8JSObject23UnregisterPrototypeUserENS0_6HandleINS0_3MapEEEPNS0_7IsolateE
	movq	(%r12), %rdi
	movl	%eax, %r15d
	movq	(%rbx), %rax
	movq	71(%rax), %r14
	movq	%r14, 71(%rdi)
	testb	$1, %r14b
	je	.L3343
	movq	%r14, %rcx
	leaq	71(%rdi), %rsi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L3357
	testb	$24, %al
	je	.L3343
.L3364:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3358
	.p2align 4,,10
	.p2align 3
.L3343:
	movq	(%rbx), %rdi
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r14
	movq	%r14, 71(%rdi)
	leaq	71(%rdi), %rsi
	testb	$1, %r14b
	je	.L3342
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L3359
	testb	$24, %al
	je	.L3342
.L3363:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3360
	.p2align 4,,10
	.p2align 3
.L3342:
	cmpb	$0, _ZN2v88internal26FLAG_trace_prototype_usersE(%rip)
	jne	.L3361
.L3337:
	testb	%r15b, %r15b
	je	.L3330
	movq	(%r12), %rax
	movq	71(%rax), %rax
	testb	$1, %al
	jne	.L3362
.L3340:
	addq	$40, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8JSObject25LazyRegisterPrototypeUserENS0_6HandleINS0_3MapEEEPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L3359:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3363
	jmp	.L3342
	.p2align 4,,10
	.p2align 3
.L3357:
	movq	%r14, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3364
	jmp	.L3343
	.p2align 4,,10
	.p2align 3
.L3330:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3361:
	.cfi_restore_state
	movq	(%r12), %rcx
	movq	(%rbx), %rdx
	leaq	.LC82(%rip), %rdi
	xorl	%eax, %eax
	movq	71(%rcx), %rsi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3337
	.p2align 4,,10
	.p2align 3
.L3362:
	movq	-1(%rax), %rdx
	cmpw	$95, 11(%rdx)
	jne	.L3340
	movabsq	$-4294967296, %rdx
	movq	%rdx, 23(%rax)
	jmp	.L3340
	.p2align 4,,10
	.p2align 3
.L3358:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3343
	.p2align 4,,10
	.p2align 3
.L3360:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3342
	.cfi_endproc
.LFE21483:
	.size	_ZN2v88internal8JSObject31UpdatePrototypeUserRegistrationENS0_6HandleINS0_3MapEEES4_PNS0_7IsolateE, .-_ZN2v88internal8JSObject31UpdatePrototypeUserRegistrationENS0_6HandleINS0_3MapEEES4_PNS0_7IsolateE
	.section	.text._ZN2v88internal8JSObject15NotifyMapChangeENS0_6HandleINS0_3MapEEES4_PNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject15NotifyMapChangeENS0_6HandleINS0_3MapEEES4_PNS0_7IsolateE
	.type	_ZN2v88internal8JSObject15NotifyMapChangeENS0_6HandleINS0_3MapEEES4_PNS0_7IsolateE, @function
_ZN2v88internal8JSObject15NotifyMapChangeENS0_6HandleINS0_3MapEEES4_PNS0_7IsolateE:
.LFB21484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movl	15(%rdi), %eax
	testl	$1048576, %eax
	jne	.L3368
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3368:
	.cfi_restore_state
	movq	%rsi, %r13
	movq	%rdx, %r14
	call	_ZN2v88internal12_GLOBAL__N_133InvalidatePrototypeChainsInternalENS0_3MapE
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8JSObject31UpdatePrototypeUserRegistrationENS0_6HandleINS0_3MapEEES4_PNS0_7IsolateE
	.cfi_endproc
.LFE21484:
	.size	_ZN2v88internal8JSObject15NotifyMapChangeENS0_6HandleINS0_3MapEEES4_PNS0_7IsolateE, .-_ZN2v88internal8JSObject15NotifyMapChangeENS0_6HandleINS0_3MapEEES4_PNS0_7IsolateE
	.section	.rodata._ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi.str1.1,"aMS",@progbits,1
.LC83:
	.string	"new_map->is_dictionary_map()"
	.section	.text._ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
	.type	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi, @function
_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi:
.LFB21487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rdx, %rdi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	-1(%rax), %rdx
	cmpq	%rdx, (%rdi)
	je	.L3369
	movq	%rsi, %r12
	movq	-1(%rax), %rsi
	movq	41112(%r15), %rdi
	movl	%ecx, %ebx
	testq	%rdi, %rdi
	je	.L3372
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L3373:
	movl	15(%rsi), %eax
	testl	$1048576, %eax
	je	.L3375
	movq	0(%r13), %rdi
	call	_ZN2v88internal12_GLOBAL__N_133InvalidatePrototypeChainsInternalENS0_3MapE
	movq	-88(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject31UpdatePrototypeUserRegistrationENS0_6HandleINS0_3MapEEES4_PNS0_7IsolateE
.L3375:
	movq	0(%r13), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	movq	-88(%rbp), %rax
	je	.L3376
	movq	(%rax), %rdx
	movl	15(%rdx), %eax
	testl	$2097152, %eax
	je	.L3743
	movq	(%r12), %rdi
	movq	%rdx, -1(%rdi)
	testq	%rdx, %rdx
	jne	.L3744
.L3369:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3745
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3372:
	.cfi_restore_state
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L3746
.L3374:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L3373
	.p2align 4,,10
	.p2align 3
.L3376:
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L3747
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	%rax, -136(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -144(%rbp)
	movq	(%r12), %rax
	movq	-1(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3510
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -96(%rbp)
	movq	(%rax), %rsi
.L3511:
	movl	15(%rsi), %eax
	movq	%r15, %rdi
	shrl	$10, %eax
	andl	$1023, %eax
	testl	%ebx, %ebx
	movl	%eax, %ecx
	movl	%eax, -128(%rbp)
	leal	(%rbx,%rax), %eax
	leal	2(%rcx), %esi
	cmovg	%eax, %esi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	41112(%r15), %rdi
	movq	%rax, -120(%rbp)
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3515
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L3516:
	movl	-128(%rbp), %eax
	testl	%eax, %eax
	je	.L3518
	subl	$1, %eax
	movl	$31, %r13d
	movq	%r12, -112(%rbp)
	movq	%r15, %r12
	leaq	(%rax,%rax,2), %rax
	movq	%r13, %r15
	movq	-120(%rbp), %r13
	leaq	55(,%rax,8), %rax
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L3556:
	movq	(%r14), %rax
	movq	(%rax,%r15), %rbx
	movq	-8(%r15,%rax), %rsi
	movq	41112(%r12), %rdi
	sarq	$32, %rbx
	testq	%rdi, %rdi
	je	.L3519
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L3520:
	testb	$2, %bl
	jne	.L3522
	movq	-96(%rbp), %rax
	movq	(%rax), %r9
	movq	39(%r9), %rax
	movq	(%rax,%r15), %rcx
	movq	%rcx, %rax
	shrq	$38, %rcx
	shrq	$51, %rax
	andl	$7, %ecx
	movq	%rax, %rdi
	movzbl	7(%r9), %eax
	movzbl	8(%r9), %esi
	andl	$1023, %edi
	subl	%esi, %eax
	cmpl	%eax, %edi
	setl	%r8b
	jl	.L3748
	subl	%eax, %edi
	movl	$16, %esi
	leal	16(,%rdi,8), %edi
.L3524:
	cmpl	$2, %ecx
	jne	.L3749
	movl	$32768, %ecx
.L3525:
	movzbl	%r8b, %r8d
	cltq
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	salq	$14, %r8
	salq	$17, %rax
	orq	%r8, %rax
	salq	$27, %rsi
	orq	%rdi, %rax
	orq	%rsi, %rax
	orq	%rax, %rcx
	andl	$16384, %eax
	movq	%rcx, %rsi
	movq	-112(%rbp), %rcx
	movq	(%rcx), %rdi
	testb	$1, %bl
	jne	.L3528
	movq	%rsi, %r9
	movl	%esi, %ecx
	movq	-1(%rdi), %r8
	shrq	$30, %r9
	sarl	$3, %ecx
	andl	$15, %r9d
	andl	$2047, %ecx
	subl	%r9d, %ecx
	testq	%rax, %rax
	jne	.L3529
	movq	%rdi, %rax
	movq	7(%rdi), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %sil
	je	.L3541
	cmpq	288(%rax), %rsi
	je	.L3541
	.p2align 4,,10
	.p2align 3
.L3531:
	leal	16(,%rcx,8), %eax
	cltq
	movq	-1(%rsi,%rax), %rsi
.L3590:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3542
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %rdx
	movq	%rax, %rcx
.L3543:
	movl	%ebx, %eax
	shrl	$6, %eax
	andl	$7, %eax
	cmpl	$2, %eax
	je	.L3750
	.p2align 4,,10
	.p2align 3
.L3553:
	movl	%ebx, %r8d
	movq	%r13, %rsi
	xorl	%r9d, %r9d
	movq	%r12, %rdi
	andl	$57, %r8d
	addq	$24, %r15
	orb	$-64, %r8b
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE3AddEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_4NameEEENS7_INS0_6ObjectEEENS0_15PropertyDetailsEPi@PLT
	movq	%rax, %r13
	cmpq	%r15, -104(%rbp)
	jne	.L3556
	movq	%rax, -120(%rbp)
	movq	%r12, %r15
	movq	-112(%rbp), %r12
.L3518:
	movq	-120(%rbp), %rax
	leaq	37592(%r15), %r14
	leaq	-68(%rbp), %rcx
	movq	%r14, %rdi
	movq	(%rax), %rdx
	movl	-128(%rbp), %eax
	addl	$1, %eax
	salq	$32, %rax
	movq	%rax, 39(%rdx)
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movzbl	7(%rax), %eax
	movq	(%r12), %rsi
	leal	0(,%rax,8), %r13d
	movl	%r13d, %edx
	call	_ZN2v88internal4Heap24NotifyObjectLayoutChangeENS0_10HeapObjectEiRKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	-88(%rbp), %rax
	movl	%r13d, %edx
	movq	(%rax), %rsi
	movzbl	7(%rsi), %ebx
	sall	$3, %ebx
	subl	%ebx, %edx
	testl	%edx, %edx
	jle	.L3557
	movq	(%r12), %rcx
	movslq	%ebx, %rax
	movl	$1, %r8d
	movq	%r14, %rdi
	leaq	-1(%rcx,%rax), %rsi
	xorl	%ecx, %ecx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	-88(%rbp), %rax
	movq	(%rax), %rsi
.L3557:
	movq	(%r12), %rdi
	movq	%rsi, -1(%rdi)
	testq	%rsi, %rsi
	jne	.L3751
.L3558:
	movq	(%r12), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	movq	-120(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	movzbl	7(%rax), %r13d
	movzbl	8(%rax), %eax
	subl	%eax, %r13d
	jne	.L3561
.L3564:
	movq	40960(%r15), %rbx
	cmpb	$0, 6080(%rbx)
	je	.L3752
	movq	6072(%rbx), %rax
.L3578:
	testq	%rax, %rax
	je	.L3579
	addl	$1, (%rax)
.L3579:
	movq	-136(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-144(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L3369
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L3369
	.p2align 4,,10
	.p2align 3
.L3747:
	movq	(%r12), %rax
	movq	-1(%rax), %r14
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3382
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -104(%rbp)
	movq	(%rax), %r14
.L3383:
	movq	-88(%rbp), %rax
	movq	(%rax), %rsi
	movq	%rsi, %rdx
	movq	31(%rsi), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	subq	$37592, %rdx
	testb	$1, %al
	je	.L3385
	movq	-1(%rax), %rcx
	cmpq	%rcx, 136(%rdx)
	je	.L3386
.L3385:
	movq	88(%rdx), %rax
.L3386:
	cmpq	%r14, %rax
	je	.L3753
	leaq	-64(%rbp), %rax
	movq	%rsi, -64(%rbp)
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNK2v88internal3Map14NumberOfFieldsEv@PLT
	movl	%eax, -152(%rbp)
	movq	-88(%rbp), %rax
	movq	(%rax), %rsi
	movzbl	7(%rsi), %edx
	movzbl	8(%rsi), %eax
	subl	%eax, %edx
	movl	%edx, -96(%rbp)
	movzbl	9(%rsi), %ebx
	cmpl	$2, %ebx
	jg	.L3754
.L3422:
	movq	-104(%rbp), %rax
	movl	-96(%rbp), %ecx
	leaq	-68(%rbp), %r9
	movl	%ebx, %r8d
	movl	-152(%rbp), %edx
	movq	-136(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal3Map22InstancesNeedRewritingES1_iiiPi@PLT
	testb	%al, %al
	je	.L3755
	movl	-152(%rbp), %eax
	xorl	%edx, %edx
	movq	%r15, %rdi
	leal	(%rax,%rbx), %esi
	movl	-96(%rbp), %ebx
	subl	%ebx, %esi
	call	_ZN2v88internal7Factory16NewPropertyArrayEiNS0_14AllocationTypeE@PLT
	movl	%ebx, %esi
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	41112(%r15), %rdi
	movq	%rax, -128(%rbp)
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3427
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -144(%rbp)
.L3428:
	movq	-88(%rbp), %rax
	movq	41112(%r15), %rdi
	movq	(%rax), %rax
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3430
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
.L3431:
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	movq	-88(%rbp), %rbx
	movq	(%rbx), %rdx
	shrl	$10, %eax
	movl	15(%rdx), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	andl	$1023, %eax
	movl	%edx, -184(%rbp)
	movl	%eax, -176(%rbp)
	je	.L3433
	subl	$1, %eax
	movq	%r13, -192(%rbp)
	movq	%r10, %r13
	movl	$31, %r14d
	leaq	(%rax,%rax,2), %rax
	movq	%r12, -160(%rbp)
	movq	%r15, %r12
	leaq	55(,%rax,8), %rax
	movq	%rax, -112(%rbp)
	jmp	.L3434
	.p2align 4,,10
	.p2align 3
.L3757:
	movq	-128(%rbp), %rbx
	movq	(%rax), %r15
	leal	16(,%rdx,8), %eax
	cltq
	movq	(%rbx), %rbx
	leaq	-1(%rbx,%rax), %rsi
	movq	%r15, (%rsi)
	testb	$1, %r15b
	je	.L3437
	movq	%r15, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rdx
	movq	%rax, -168(%rbp)
	testl	$262144, %edx
	je	.L3462
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-168(%rbp), %rax
	movq	-200(%rbp), %rsi
	movq	8(%rax), %rdx
.L3462:
	andl	$24, %edx
	je	.L3437
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3437
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L3437:
	addq	$24, %r14
	cmpq	%r14, -112(%rbp)
	je	.L3713
.L3434:
	movq	0(%r13), %rdx
	movq	(%rdx,%r14), %rbx
	sarq	$32, %rbx
	testb	$2, %bl
	jne	.L3437
	movq	-144(%rbp), %rax
	shrl	$6, %ebx
	andl	$7, %ebx
	movq	(%rax), %rax
	movq	(%rax,%r14), %r15
	sarq	$32, %r15
	testb	$2, %r15b
	je	.L3438
	andl	$1, %r15d
	je	.L3439
	leaq	80(%r12), %rax
	cmpl	$2, %ebx
	je	.L3756
.L3441:
	movq	(%rdx,%r14), %rdx
	shrq	$51, %rdx
	andl	$1023, %edx
	cmpl	-96(%rbp), %edx
	jl	.L3757
	movq	-120(%rbp), %rbx
	subl	-96(%rbp), %edx
	movq	(%rbx), %r15
	movq	(%rax), %rbx
	leal	16(,%rdx,8), %eax
	cltq
	leaq	-1(%r15,%rax), %rsi
	movq	%rbx, (%rsi)
	testb	$1, %bl
	je	.L3437
	movq	%rbx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rdx
	movq	%rax, -168(%rbp)
	testl	$262144, %edx
	je	.L3465
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-168(%rbp), %rax
	movq	-200(%rbp), %rsi
	movq	8(%rax), %rdx
.L3465:
	andl	$24, %edx
	je	.L3437
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3437
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3437
	.p2align 4,,10
	.p2align 3
.L3744:
	testb	$1, %dl
	je	.L3369
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L3369
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3369
	.p2align 4,,10
	.p2align 3
.L3541:
	movq	968(%rax), %rsi
	jmp	.L3531
	.p2align 4,,10
	.p2align 3
.L3749:
	cmpb	$2, %cl
	jg	.L3526
	je	.L3734
.L3598:
	xorl	%ecx, %ecx
	jmp	.L3525
	.p2align 4,,10
	.p2align 3
.L3526:
	subl	$3, %ecx
	cmpb	$1, %cl
	jbe	.L3598
.L3734:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3522:
	movq	(%r14), %rax
	movq	8(%r15,%rax), %rsi
.L3741:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3554
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-120(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L3553
	.p2align 4,,10
	.p2align 3
.L3519:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L3758
.L3521:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L3520
	.p2align 4,,10
	.p2align 3
.L3554:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L3759
.L3555:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L3553
	.p2align 4,,10
	.p2align 3
.L3528:
	testq	%rax, %rax
	je	.L3545
	movq	%rsi, %rcx
	andl	$16376, %ecx
	movq	-1(%rdi,%rcx), %rsi
	jmp	.L3741
	.p2align 4,,10
	.p2align 3
.L3748:
	movzbl	8(%r9), %esi
	movzbl	8(%r9), %r9d
	addl	%r9d, %edi
	sall	$3, %esi
	sall	$3, %edi
	jmp	.L3524
	.p2align 4,,10
	.p2align 3
.L3529:
	movq	47(%r8), %rax
	testq	%rax, %rax
	je	.L3732
	movq	%rax, %r9
	movl	$32, %edi
	notq	%r9
	movl	%r9d, %r8d
	andl	$1, %r8d
	jne	.L3533
	movslq	11(%rax), %rdi
	sall	$3, %edi
.L3533:
	cmpl	%ecx, %edi
	jbe	.L3732
	testl	%ecx, %ecx
	leal	31(%rcx), %edi
	cmovns	%ecx, %edi
	sarl	$5, %edi
	andl	$1, %r9d
	jne	.L3534
	cmpl	%edi, 11(%rax)
	jle	.L3534
	movl	%ecx, %r9d
	sarl	$31, %r9d
	shrl	$27, %r9d
	addl	%r9d, %ecx
	andl	$31, %ecx
	subl	%r9d, %ecx
	movl	$1, %r9d
	sall	%cl, %r9d
	testb	%r8b, %r8b
	je	.L3760
.L3537:
	sarq	$32, %rax
	testl	%eax, %r9d
	sete	%dil
.L3538:
	movq	-112(%rbp), %rax
	movq	%rsi, %rcx
	andl	$16376, %ecx
	movq	(%rax), %rax
	movq	-1(%rcx,%rax), %rcx
	movq	%rcx, -152(%rbp)
	testb	%dil, %dil
	jne	.L3539
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movq	-152(%rbp), %rdi
	movq	-120(%rbp), %rdx
	movq	%rax, %rcx
	movq	(%rax), %rax
	movq	%rdi, 7(%rax)
	jmp	.L3553
	.p2align 4,,10
	.p2align 3
.L3545:
	movq	%rdi, %rax
	movq	7(%rdi), %r8
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %r8b
	je	.L3549
	cmpq	288(%rax), %r8
	je	.L3549
.L3548:
	movq	%rsi, %rax
	movl	%esi, %ecx
	shrq	$30, %rax
	sarl	$3, %ecx
	andl	$15, %eax
	andl	$2047, %ecx
	subl	%eax, %ecx
	leal	16(,%rcx,8), %eax
	cltq
	movq	-1(%r8,%rax), %rsi
	jmp	.L3741
	.p2align 4,,10
	.p2align 3
.L3549:
	movq	968(%rax), %r8
	jmp	.L3548
	.p2align 4,,10
	.p2align 3
.L3758:
	movq	%r12, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L3521
	.p2align 4,,10
	.p2align 3
.L3732:
	movq	-112(%rbp), %rax
	movq	(%rax), %rax
.L3539:
	movq	%rsi, %rcx
	andl	$16376, %ecx
	movq	-1(%rcx,%rax), %rsi
	jmp	.L3590
	.p2align 4,,10
	.p2align 3
.L3759:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L3555
	.p2align 4,,10
	.p2align 3
.L3752:
	movb	$1, 6080(%rbx)
	leaq	6056(%rbx), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6072(%rbx)
	jmp	.L3578
	.p2align 4,,10
	.p2align 3
.L3515:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L3761
.L3517:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L3516
	.p2align 4,,10
	.p2align 3
.L3510:
	movq	-136(%rbp), %rax
	movq	%rax, -96(%rbp)
	cmpq	41096(%r15), %rax
	je	.L3762
.L3512:
	movq	-96(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L3511
	.p2align 4,,10
	.p2align 3
.L3382:
	movq	41088(%r15), %rax
	movq	%rax, -104(%rbp)
	cmpq	41096(%r15), %rax
	je	.L3763
.L3384:
	movq	-104(%rbp), %rbx
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%r14, (%rbx)
	jmp	.L3383
	.p2align 4,,10
	.p2align 3
.L3542:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L3764
.L3544:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L3543
	.p2align 4,,10
	.p2align 3
.L3753:
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movl	15(%rax), %edx
	movl	15(%rsi), %eax
	shrl	$10, %edx
	shrl	$10, %eax
	andl	$1023, %edx
	andl	$1023, %eax
	cmpl	%eax, %edx
	je	.L3765
	movq	39(%rsi), %rdx
	movl	15(%rsi), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	7(%rdx,%rax), %rax
	movq	%rax, %rbx
	sarq	$32, %rbx
	btq	$33, %rax
	jc	.L3479
	movl	15(%rsi), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	7(%rax,%rdx), %rdi
	movzbl	7(%rsi), %edx
	movq	%rdi, %rax
	shrq	$38, %rdi
	shrq	$51, %rax
	andl	$7, %edi
	movq	%rax, %rcx
	movzbl	8(%rsi), %eax
	andl	$1023, %ecx
	subl	%eax, %edx
	cmpl	%edx, %ecx
	setl	%al
	jl	.L3766
	subl	%edx, %ecx
	movl	$16, %r8d
	leal	16(,%rcx,8), %r9d
.L3395:
	cmpl	$2, %edi
	jne	.L3767
	movl	$32768, %ecx
.L3396:
	movzbl	%al, %eax
	movslq	%edx, %rdx
	movslq	%r9d, %r9
	movslq	%r8d, %r8
	salq	$17, %rdx
	salq	$14, %rax
	orq	%rdx, %rax
	salq	$27, %r8
	movq	%rsi, %rdx
	orq	%r9, %rax
	orq	%r8, %rax
	orq	%rax, %rcx
	movq	%rcx, %r14
	testb	$64, %ah
	jne	.L3405
	movq	%rcx, %rax
	sarl	$3, %ecx
	shrq	$30, %rax
	andl	$2047, %ecx
	andl	$15, %eax
	subl	%eax, %ecx
	movq	(%r12), %rax
	movl	%ecx, %r9d
	movq	%rax, %rdi
	movq	7(%rax), %rcx
	andq	$-262144, %rdi
	movq	24(%rdi), %rdi
	subq	$37592, %rdi
	testb	$1, %cl
	je	.L3404
	cmpq	288(%rdi), %rcx
	je	.L3404
.L3403:
	movslq	11(%rcx), %rcx
	andl	$1023, %ecx
	cmpl	%ecx, %r9d
	jge	.L3768
.L3405:
	movq	%r14, %rax
	shrq	$15, %rax
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L3769
.L3413:
	movq	(%r12), %rdi
	movq	%rdx, -1(%rdi)
	testq	%rdx, %rdx
	jne	.L3729
	.p2align 4,,10
	.p2align 3
.L3508:
	movq	0(%r13), %rax
	movl	15(%rax), %edx
	andl	$1048576, %edx
	je	.L3369
	movl	15(%rax), %edx
	andl	$-4194305, %edx
	movl	%edx, 15(%rax)
	jmp	.L3369
	.p2align 4,,10
	.p2align 3
.L3746:
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3374
	.p2align 4,,10
	.p2align 3
.L3561:
	movq	-96(%rbp), %rcx
	movq	(%r12), %rax
	movq	%r14, %rdi
	movq	(%rcx), %rdx
	subq	$1, %rax
	movzbl	8(%rdx), %esi
	movslq	%ebx, %rdx
	addq	%rax, %rdx
	salq	$3, %rsi
	andl	$2040, %esi
	addq	%rax, %rsi
	call	_ZN2v88internal4Heap22ClearRecordedSlotRangeEmm@PLT
	testl	%r13d, %r13d
	jle	.L3564
	xorl	%r14d, %r14d
	movq	%r15, -128(%rbp)
	leaq	_ZN2v88internal3Smi5kZeroE(%rip), %rbx
	movl	%r13d, -96(%rbp)
	movl	%r14d, %r15d
	jmp	.L3577
	.p2align 4,,10
	.p2align 3
.L3772:
	andl	$16376, %eax
	movb	%r8b, -104(%rbp)
	movq	%r13, %rdx
	movq	%r9, %rdi
	leaq	-1(%r9,%rax), %r14
	movq	%r13, (%r14)
	movq	%r14, %rsi
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movzbl	-104(%rbp), %r8d
	testb	%r8b, %r8b
	jne	.L3570
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3570
	movq	-112(%rbp), %r9
	movq	%r9, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3570
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L3570:
	addl	$1, %r15d
	cmpl	-96(%rbp), %r15d
	je	.L3770
.L3577:
	movq	-88(%rbp), %rax
	movq	(%rax), %rdi
	movzbl	7(%rdi), %edx
	movzbl	8(%rdi), %eax
	subl	%eax, %edx
	cmpl	%edx, %r15d
	setl	%sil
	jl	.L3771
	movl	%r15d, %eax
	movl	$2147483648, %r8d
	subl	%edx, %eax
	leal	16(,%rax,8), %edi
.L3566:
	movzbl	%sil, %eax
	movslq	%edx, %rdx
	movq	(%rbx), %r13
	movslq	%edi, %rdi
	salq	$14, %rax
	salq	$17, %rdx
	movq	(%r12), %r9
	orq	%rdx, %rax
	orq	%r8, %rax
	movq	%r13, %r8
	notq	%r8
	orq	%rdi, %rax
	andl	$1, %r8d
	testb	$64, %ah
	jne	.L3772
	movq	7(%r9), %rdi
	andq	$-262144, %r9
	movq	24(%r9), %rsi
	subq	$37592, %rsi
	testb	$1, %dil
	je	.L3573
	cmpq	288(%rsi), %rdi
	je	.L3573
.L3572:
	movq	%rax, %rsi
	sarl	$3, %eax
	shrq	$30, %rsi
	andl	$2047, %eax
	andl	$15, %esi
	subl	%esi, %eax
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%r13, (%rsi)
	testb	%r8b, %r8b
	jne	.L3570
	movq	%r13, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -104(%rbp)
	testl	$262144, %eax
	je	.L3575
	movq	%r13, %rdx
	movq	%rsi, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %r8
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	8(%r8), %rax
.L3575:
	testb	$24, %al
	je	.L3570
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3570
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3570
	.p2align 4,,10
	.p2align 3
.L3751:
	testb	$1, %sil
	je	.L3558
	movq	%rsi, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L3558
	movq	%rsi, %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3558
	.p2align 4,,10
	.p2align 3
.L3427:
	movq	41088(%r15), %rax
	movq	%rax, -144(%rbp)
	cmpq	41096(%r15), %rax
	je	.L3773
.L3429:
	movq	-144(%rbp), %rbx
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	jmp	.L3428
	.p2align 4,,10
	.p2align 3
.L3760:
	sall	$2, %edi
	movslq	%edi, %rdi
	movl	15(%rax,%rdi), %eax
	testl	%eax, %r9d
	sete	%dil
	jmp	.L3538
	.p2align 4,,10
	.p2align 3
.L3765:
	movq	(%r12), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal10HeapObject20synchronized_set_mapENS0_3MapE
	jmp	.L3508
	.p2align 4,,10
	.p2align 3
.L3573:
	movq	968(%rsi), %rdi
	jmp	.L3572
	.p2align 4,,10
	.p2align 3
.L3771:
	movzbl	8(%rdi), %eax
	movzbl	8(%rdi), %edi
	salq	$3, %rax
	addl	%r15d, %edi
	andl	$2040, %eax
	sall	$3, %edi
	salq	$27, %rax
	movq	%rax, %r8
	jmp	.L3566
	.p2align 4,,10
	.p2align 3
.L3438:
	movq	-104(%rbp), %rax
	movq	(%rax), %r9
	movq	39(%r9), %rax
	movq	(%rax,%r14), %rdx
	movq	%rdx, %rax
	shrq	$38, %rdx
	shrq	$51, %rax
	andl	$7, %edx
	movq	%rax, %rcx
	movzbl	7(%r9), %eax
	movzbl	8(%r9), %esi
	movl	%edx, %r11d
	andl	$1023, %ecx
	subl	%esi, %eax
	cmpl	%eax, %ecx
	setl	%dil
	jl	.L3774
	subl	%eax, %ecx
	movl	$16, %esi
	leal	16(,%rcx,8), %ecx
.L3445:
	cmpl	$2, %edx
	jne	.L3775
	movl	$32768, %edx
.L3446:
	movzbl	%dil, %edi
	cltq
	movslq	%ecx, %rcx
	movslq	%esi, %rsi
	salq	$14, %rdi
	salq	$17, %rax
	orq	%rdi, %rax
	salq	$27, %rsi
	orq	%rcx, %rax
	orq	%rsi, %rax
	movq	%r12, %rsi
	orq	%rax, %rdx
	movq	-160(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	movq	-136(%rbp), %rdi
	movq	%rdx, -168(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE
	movq	-168(%rbp), %rdx
	testb	%al, %al
	je	.L3449
	movq	-160(%rbp), %rax
	andl	$16376, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	(%rax), %r9
	movq	-1(%rdx,%r9), %rbx
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	%rbx, 7(%rdx)
	movq	0(%r13), %rdx
	jmp	.L3441
	.p2align 4,,10
	.p2align 3
.L3775:
	cmpb	$2, %dl
	jg	.L3447
	je	.L3448
.L3595:
	xorl	%edx, %edx
	jmp	.L3446
	.p2align 4,,10
	.p2align 3
.L3447:
	subl	$3, %edx
	cmpb	$1, %dl
	jbe	.L3595
.L3448:
	movl	%r11d, %edi
.L3733:
	call	_ZNK2v88internal14Representation8MnemonicEv.isra.0
	leaq	.LC6(%rip), %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3734
	.p2align 4,,10
	.p2align 3
.L3716:
	movq	-152(%rbp), %r13
	movq	%rbx, %r12
.L3503:
	movq	(%r12), %rax
	movq	-136(%rbp), %rdi
	movq	%rax, -64(%rbp)
	movq	-120(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE
	movq	-88(%rbp), %rax
	movq	(%rax), %rsi
	movzbl	7(%rsi), %eax
	movl	-144(%rbp), %edx
	sall	$3, %eax
	subl	%eax, %edx
	testl	%edx, %edx
	jle	.L3479
	movq	(%r12), %rcx
	cltq
	movq	-160(%rbp), %rdi
	movl	$1, %r8d
	leaq	-1(%rcx,%rax), %rsi
	xorl	%ecx, %ecx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	-88(%rbp), %rax
	movq	(%rax), %rsi
.L3479:
	movq	(%r12), %rdi
	movq	%rsi, -1(%rdi)
	testq	%rsi, %rsi
	je	.L3508
	testb	$1, %sil
	je	.L3508
	movq	%rsi, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L3508
	movq	%rsi, %rdx
.L3727:
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3508
	.p2align 4,,10
	.p2align 3
.L3713:
	movq	%r13, %r10
	movq	%r12, %r15
	movq	-192(%rbp), %r13
	movq	-160(%rbp), %r12
.L3433:
	movl	-176(%rbp), %ebx
	movl	-184(%rbp), %eax
	cmpl	%eax, %ebx
	jge	.L3435
	subl	$1, %eax
	movslq	%ebx, %rdx
	leal	3(%rbx,%rbx,2), %r14d
	movq	%r13, -144(%rbp)
	subl	%ebx, %eax
	sall	$3, %r14d
	movq	%r12, -160(%rbp)
	movq	%r10, %r12
	addq	%rdx, %rax
	movslq	%r14d, %r14
	leaq	(%rax,%rax,2), %rax
	addq	$7, %r14
	leaq	55(,%rax,8), %rbx
	movq	%rbx, %r13
	movl	-96(%rbp), %ebx
	jmp	.L3436
	.p2align 4,,10
	.p2align 3
.L3777:
	movq	-128(%rbp), %rax
	movq	(%rdx), %rdx
	movq	(%rax), %rdi
.L3736:
	leal	16(,%rsi,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L3467
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r9
	movq	%rax, -112(%rbp)
	testl	$262144, %r9d
	je	.L3475
	movq	%rdx, -184(%rbp)
	movq	%rsi, -176(%rbp)
	movq	%rdi, -168(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %rax
	movq	-184(%rbp), %rdx
	movq	-176(%rbp), %rsi
	movq	-168(%rbp), %rdi
	movq	8(%rax), %r9
.L3475:
	andl	$24, %r9d
	je	.L3467
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3467
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L3467:
	addq	$24, %r14
	cmpq	%r13, %r14
	je	.L3714
.L3436:
	movq	(%r12), %rsi
	movq	(%rsi,%r14), %rax
	sarq	$32, %rax
	testb	$2, %al
	jne	.L3467
	shrl	$6, %eax
	leaq	80(%r15), %rdx
	andl	$7, %eax
	cmpl	$2, %eax
	je	.L3776
.L3469:
	movq	(%rsi,%r14), %rsi
	shrq	$51, %rsi
	andl	$1023, %esi
	cmpl	%ebx, %esi
	jl	.L3777
	movq	-120(%rbp), %rax
	movq	(%rdx), %rdx
	subl	%ebx, %esi
	movq	(%rax), %rdi
	jmp	.L3736
	.p2align 4,,10
	.p2align 3
.L3776:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movabsq	$-2251799814209537, %rcx
	movq	%rax, %rdx
	movq	(%rax), %rax
	movq	%rcx, 7(%rax)
	movq	(%r12), %rsi
	jmp	.L3469
	.p2align 4,,10
	.p2align 3
.L3714:
	movq	-144(%rbp), %r13
	movq	-160(%rbp), %r12
.L3435:
	movq	-104(%rbp), %rax
	leaq	-69(%rbp), %rcx
	leaq	37592(%r15), %rdi
	movl	$2147483648, %r14d
	movq	%rdi, -160(%rbp)
	movq	(%rax), %rax
	movzbl	7(%rax), %edx
	movq	(%r12), %rsi
	leal	0(,%rdx,8), %eax
	movl	%eax, %edx
	movl	%eax, -144(%rbp)
	call	_ZN2v88internal4Heap24NotifyObjectLayoutChangeENS0_10HeapObjectEiRKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movl	-152(%rbp), %ebx
	movl	-96(%rbp), %eax
	cmpl	%eax, %ebx
	cmovle	%ebx, %eax
	xorl	%r15d, %r15d
	leal	-1(%rax), %ebx
	testl	%eax, %eax
	jle	.L3503
	movq	%r13, -152(%rbp)
	movq	-128(%rbp), %r13
	movq	%rbx, -96(%rbp)
	movq	%r12, %rbx
	jmp	.L3504
	.p2align 4,,10
	.p2align 3
.L3779:
	movq	(%rbx), %rax
	movq	7(%rax), %rdi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %dil
	je	.L3499
	cmpq	288(%rax), %rdi
	je	.L3499
.L3484:
	leal	16(,%rcx,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%r12, (%rsi)
	testb	$1, %r12b
	je	.L3496
	movq	%r12, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rdx
	movq	%rax, -112(%rbp)
	testl	$262144, %edx
	je	.L3501
	movq	%r12, %rdx
	movq	%rsi, -168(%rbp)
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %rax
	movq	-168(%rbp), %rsi
	movq	-128(%rbp), %rdi
	movq	8(%rax), %rdx
.L3501:
	andl	$24, %edx
	je	.L3496
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3496
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L3496:
	leaq	1(%r15), %rax
	cmpq	%r15, -96(%rbp)
	je	.L3716
	movq	%rax, %r15
.L3504:
	movq	-88(%rbp), %rax
	movl	%r15d, %edi
	movq	(%rax), %rsi
	movzbl	7(%rsi), %eax
	movzbl	8(%rsi), %edx
	subl	%edx, %eax
	cmpl	%eax, %r15d
	setl	%cl
	jl	.L3778
	movl	%r15d, %edx
	movq	%r14, %r11
	subl	%eax, %edx
	leal	16(,%rdx,8), %r10d
.L3481:
	movzbl	%cl, %ecx
	cltq
	movslq	%r10d, %r10
	salq	$17, %rax
	salq	$14, %rcx
	orq	%rax, %rcx
	movq	0(%r13), %rax
	movq	%rcx, %rdx
	orq	%r11, %rdx
	movq	15(%rax,%r15,8), %r12
	orq	%r10, %rdx
	movq	%rdx, %rax
	movl	%edx, %ecx
	shrq	$30, %rax
	sarl	$3, %ecx
	andl	$15, %eax
	andl	$2047, %ecx
	subl	%eax, %ecx
	testb	$64, %dh
	je	.L3779
	movq	47(%rsi), %rax
	testq	%rax, %rax
	je	.L3731
	movq	%rax, %r11
	movl	$32, %esi
	notq	%r11
	movl	%r11d, %r10d
	andl	$1, %r10d
	jne	.L3486
	movslq	11(%rax), %rsi
	sall	$3, %esi
.L3486:
	cmpl	%esi, %ecx
	jnb	.L3731
	testl	%ecx, %ecx
	leal	31(%rcx), %esi
	cmovns	%ecx, %esi
	sarl	$5, %esi
	andl	$1, %r11d
	jne	.L3487
	cmpl	%esi, 11(%rax)
	jle	.L3487
	movl	%ecx, %r11d
	sarl	$31, %r11d
	shrl	$27, %r11d
	addl	%r11d, %ecx
	andl	$31, %ecx
	subl	%r11d, %ecx
	movl	$1, %r11d
	sall	%cl, %r11d
	testb	%r10b, %r10b
	je	.L3780
.L3491:
	sarq	$32, %rax
	testl	%eax, %r11d
	sete	%sil
.L3493:
	movq	(%rbx), %r10
	movq	%rdx, %rcx
	andl	$16376, %ecx
	leaq	-1(%rcx,%r10), %rax
	testb	%sil, %sil
	jne	.L3494
	movq	7(%r12), %rsi
	movq	%rsi, (%rax)
	cmpl	%edi, -68(%rbp)
	jle	.L3496
	movq	-104(%rbp), %rax
	movq	-136(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rcx, -112(%rbp)
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE
	movq	-112(%rbp), %rcx
	testb	%al, %al
	jne	.L3496
	movq	(%rbx), %rsi
	movq	-160(%rbp), %rdi
	leaq	-1(%rsi,%rcx), %rdx
	call	_ZN2v88internal4Heap17ClearRecordedSlotENS0_10HeapObjectENS0_14FullObjectSlotE@PLT
	jmp	.L3496
	.p2align 4,,10
	.p2align 3
.L3778:
	movzbl	8(%rsi), %r11d
	movzbl	8(%rsi), %r10d
	salq	$3, %r11
	addl	%r15d, %r10d
	andl	$2040, %r11d
	sall	$3, %r10d
	salq	$27, %r11
	jmp	.L3481
	.p2align 4,,10
	.p2align 3
.L3499:
	movq	968(%rax), %rdi
	jmp	.L3484
	.p2align 4,,10
	.p2align 3
.L3731:
	movq	(%rbx), %r10
.L3494:
	andl	$16376, %edx
	movq	%r10, %rdi
	leaq	-1(%rdx,%r10), %rsi
	movq	%r12, %rdx
	movq	%r12, (%rsi)
	movq	%rsi, -128(%rbp)
	movq	%r10, -112(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	je	.L3496
	movq	-112(%rbp), %r10
	movq	-128(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r10, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	jmp	.L3496
	.p2align 4,,10
	.p2align 3
.L3439:
	movq	8(%r14,%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3442
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	0(%r13), %rdx
	jmp	.L3441
	.p2align 4,,10
	.p2align 3
.L3449:
	movq	-160(%rbp), %rax
	movq	(%rax), %rcx
	testb	$64, %dh
	je	.L3451
	andl	$16376, %edx
	movq	-1(%rcx,%rdx), %rsi
.L3452:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3456
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L3457:
	shrl	$6, %r15d
	andl	$7, %r15d
	cmpl	$2, %r15d
	je	.L3459
	cmpl	$2, %ebx
	je	.L3702
.L3719:
	movq	0(%r13), %rdx
	jmp	.L3441
	.p2align 4,,10
	.p2align 3
.L3770:
	movq	-128(%rbp), %r15
	jmp	.L3564
	.p2align 4,,10
	.p2align 3
.L3780:
	sall	$2, %esi
	movslq	%esi, %rsi
	movl	15(%rax,%rsi), %eax
	testl	%eax, %r11d
	sete	%sil
	jmp	.L3493
	.p2align 4,,10
	.p2align 3
.L3755:
	movq	-88(%rbp), %rax
	movq	(%r12), %rdi
	movq	(%rax), %rdx
	movq	%rdx, -1(%rdi)
	testq	%rdx, %rdx
	je	.L3508
.L3729:
	testb	$1, %dl
	je	.L3508
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L3508
	jmp	.L3727
	.p2align 4,,10
	.p2align 3
.L3754:
	movzbl	7(%rsi), %eax
	subl	%ebx, %eax
	movl	%eax, %ebx
	jmp	.L3422
	.p2align 4,,10
	.p2align 3
.L3430:
	movq	41088(%r15), %r10
	cmpq	41096(%r15), %r10
	je	.L3781
.L3432:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r10)
	jmp	.L3431
	.p2align 4,,10
	.p2align 3
.L3743:
	leaq	.LC83(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3750:
	movq	(%rcx), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rdx, -152(%rbp)
	movq	7(%rax), %r8
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movq	-120(%rbp), %r8
	movq	-152(%rbp), %rdx
	movq	%rax, %rcx
	movq	(%rax), %rax
	movq	%r8, 7(%rax)
	jmp	.L3553
	.p2align 4,,10
	.p2align 3
.L3774:
	movzbl	8(%r9), %esi
	movzbl	8(%r9), %r9d
	addl	%r9d, %ecx
	sall	$3, %esi
	sall	$3, %ecx
	jmp	.L3445
	.p2align 4,,10
	.p2align 3
.L3442:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L3782
.L3443:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	movq	0(%r13), %rdx
	jmp	.L3441
	.p2align 4,,10
	.p2align 3
.L3767:
	cmpb	$2, %dil
	jg	.L3397
	je	.L3733
	xorl	%ecx, %ecx
	jmp	.L3396
	.p2align 4,,10
	.p2align 3
.L3397:
	leal	-3(%rdi), %ecx
	cmpb	$1, %cl
	ja	.L3733
	xorl	%ecx, %ecx
	jmp	.L3396
	.p2align 4,,10
	.p2align 3
.L3756:
	movabsq	$-2251799814209537, %rbx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	%rbx, 7(%rdx)
	movq	0(%r13), %rdx
	jmp	.L3441
	.p2align 4,,10
	.p2align 3
.L3456:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L3783
.L3458:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3457
	.p2align 4,,10
	.p2align 3
.L3487:
	cmpl	$31, %ecx
	jg	.L3489
	testb	%r10b, %r10b
	je	.L3489
	movl	$1, %r11d
	sall	%cl, %r11d
	jmp	.L3491
	.p2align 4,,10
	.p2align 3
.L3451:
	movq	7(%rcx), %rax
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	subq	$37592, %rcx
	testb	$1, %al
	je	.L3455
	cmpq	288(%rcx), %rax
	je	.L3455
.L3454:
	movq	%rdx, %rcx
	sarl	$3, %edx
	shrq	$30, %rcx
	andl	$2047, %edx
	andl	$15, %ecx
	subl	%ecx, %edx
	leal	16(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	-1(%rax,%rdx), %rsi
	jmp	.L3452
	.p2align 4,,10
	.p2align 3
.L3459:
	cmpl	$2, %ebx
	je	.L3719
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11WrapForReadEPNS0_7IsolateENS0_6HandleIS1_EENS0_14RepresentationE@PLT
	jmp	.L3719
	.p2align 4,,10
	.p2align 3
.L3763:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, -104(%rbp)
	jmp	.L3384
	.p2align 4,,10
	.p2align 3
.L3762:
	movq	%r15, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, -96(%rbp)
	jmp	.L3512
	.p2align 4,,10
	.p2align 3
.L3761:
	movq	%r15, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L3517
	.p2align 4,,10
	.p2align 3
.L3534:
	cmpl	$31, %ecx
	jg	.L3489
	testb	%r8b, %r8b
	je	.L3489
	movl	$1, %r9d
	sall	%cl, %r9d
	jmp	.L3537
	.p2align 4,,10
	.p2align 3
.L3764:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L3544
	.p2align 4,,10
	.p2align 3
.L3455:
	movq	968(%rcx), %rax
	jmp	.L3454
.L3766:
	movzbl	8(%rsi), %r8d
	movzbl	8(%rsi), %r9d
	addl	%r9d, %ecx
	sall	$3, %r8d
	leal	0(,%rcx,8), %r9d
	jmp	.L3395
.L3404:
	movq	968(%rdi), %rcx
	jmp	.L3403
.L3489:
	leaq	.LC12(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3702:
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object13NewStorageForEPNS0_7IsolateENS0_6HandleIS1_EENS0_14RepresentationE@PLT
	movq	0(%r13), %rdx
	jmp	.L3441
.L3773:
	movq	%r15, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, -144(%rbp)
	jmp	.L3429
.L3781:
	movq	%r15, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L3432
.L3782:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	jmp	.L3443
.L3769:
	leaq	-64(%rbp), %rbx
	movq	%rsi, -64(%rbp)
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal3Map20IsUnboxedDoubleFieldEPNS0_7IsolateENS0_10FieldIndexE
	testb	%al, %al
	je	.L3412
.L3718:
	movq	-88(%rbp), %rax
	movq	(%rax), %rdx
	jmp	.L3413
.L3768:
	movzbl	9(%rsi), %edx
	cmpl	$2, %edx
	jg	.L3784
.L3406:
	movq	7(%rax), %r8
	andq	$-262144, %rax
	leal	1(%rdx), %r14d
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %r8b
	je	.L3409
	cmpq	288(%rax), %r8
	je	.L3409
.L3408:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3785
	movq	%r8, %rsi
	movl	%r9d, -96(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-96(%rbp), %r9d
	movq	%rax, %rsi
.L3417:
	shrl	$6, %ebx
	xorl	%ecx, %ecx
	movl	%r14d, %edx
	movq	%r15, %rdi
	andl	$7, %ebx
	movl	%r9d, -96(%rbp)
	call	_ZN2v88internal7Factory24CopyPropertyArrayAndGrowENS0_6HandleINS0_13PropertyArrayEEEiNS0_14AllocationTypeE@PLT
	cmpl	$2, %ebx
	movl	-96(%rbp), %r9d
	movq	%rax, %r14
	leaq	80(%r15), %rax
	je	.L3786
.L3420:
	movq	(%r14), %rdx
	leaq	-64(%rbp), %r15
	movl	%r9d, %esi
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	movq	(%rax), %rdx
	call	_ZN2v88internal13PropertyArray3setEiNS0_6ObjectE
	movq	(%r12), %rax
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	(%r14), %rsi
	call	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE
	movq	(%r12), %rax
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	-88(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal10HeapObject20synchronized_set_mapENS0_3MapE
	jmp	.L3508
.L3783:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	jmp	.L3458
.L3409:
	movq	968(%rax), %r8
	jmp	.L3408
.L3785:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L3787
.L3418:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r8, (%rsi)
	jmp	.L3417
.L3412:
	movabsq	$-2251799814209537, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory21NewHeapNumberFromBitsEmNS0_14AllocationTypeE
	movq	(%r12), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	$4, %ecx
	movq	%rdx, -64(%rbp)
	movq	(%rax), %rdx
	call	_ZN2v88internal8JSObject20RawFastPropertyAtPutENS0_10FieldIndexENS0_6ObjectENS0_16WriteBarrierModeE
	jmp	.L3718
.L3784:
	movzbl	7(%rsi), %ecx
	subl	%edx, %ecx
	movl	%ecx, %edx
	jmp	.L3406
.L3786:
	movabsq	$-2251799814209537, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory21NewHeapNumberFromBitsEmNS0_14AllocationTypeE
	movl	-96(%rbp), %r9d
	jmp	.L3420
.L3745:
	call	__stack_chk_fail@PLT
.L3787:
	movq	%r15, %rdi
	movq	%r8, -104(%rbp)
	movl	%r9d, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %r8
	movl	-96(%rbp), %r9d
	movq	%rax, %rsi
	jmp	.L3418
	.cfi_endproc
.LFE21487:
	.size	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi, .-_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
	.section	.rodata._ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE.str1.1,"aMS",@progbits,1
.LC84:
	.string	"ForceSetPrototype"
	.section	.text._ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE
	.type	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE, @function
_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE:
.LFB21488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %r12
	movq	-1(%rax), %r14
	movq	3520(%r12), %rdi
	subq	$37592, %r12
	testq	%rdi, %rdi
	je	.L3789
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3790:
	movq	%r12, %rdi
	leaq	.LC84(%rip), %rdx
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$1, %ecx
	movq	%rax, %r14
	movq	%rax, %rsi
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
	.p2align 4,,10
	.p2align 3
.L3789:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L3793
.L3791:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L3790
	.p2align 4,,10
	.p2align 3
.L3793:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L3791
	.cfi_endproc
.LFE21488:
	.size	_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE, .-_ZN2v88internal8JSObject17ForceSetPrototypeENS0_6HandleIS1_EENS2_INS0_10HeapObjectEEE
	.section	.text._ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB21493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	-1(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3795
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L3796:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Map6UpdateEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	movq	%rax, %rbx
	movl	15(%rdx), %eax
	orl	$67108864, %eax
	movl	%eax, 15(%rdx)
	movq	%rbx, %rdx
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
	cmpb	$0, _ZN2v88internal20FLAG_trace_migrationE(%rip)
	jne	.L3801
.L3794:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3802
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3795:
	.cfi_restore_state
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L3803
.L3797:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L3796
	.p2align 4,,10
	.p2align 3
.L3801:
	movq	0(%r13), %rax
	movq	stdout(%rip), %rsi
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	movq	(%rbx), %rcx
	movq	(%r14), %rdx
	call	_ZN2v88internal8JSObject22PrintInstanceMigrationEP8_IO_FILENS0_3MapES4_
	jmp	.L3794
	.p2align 4,,10
	.p2align 3
.L3803:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L3797
.L3802:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21493:
	.size	_ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal10JSReceiver23SetOrCopyDataPropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPKNS0_12ScopedVectorIS7_EEb.str1.1,"aMS",@progbits,1
.LC85:
	.string	"success"
	.section	.rodata._ZN2v88internal10JSReceiver23SetOrCopyDataPropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPKNS0_12ScopedVectorIS7_EEb.str1.8,"aMS",@progbits,1
	.align 8
.LC86:
	.string	"JSObject::CreateDataProperty(&it, prop_value, Just(kThrowOnError)) .FromJust()"
	.section	.text._ZN2v88internal10JSReceiver23SetOrCopyDataPropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPKNS0_12ScopedVectorIS7_EEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver23SetOrCopyDataPropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPKNS0_12ScopedVectorIS7_EEb
	.type	_ZN2v88internal10JSReceiver23SetOrCopyDataPropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPKNS0_12ScopedVectorIS7_EEb, @function
_ZN2v88internal10JSReceiver23SetOrCopyDataPropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPKNS0_12ScopedVectorIS7_EEb:
.LFB21400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -320(%rbp)
	movq	%rcx, -336(%rbp)
	movb	%r8b, -321(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L3914
.L3806:
	movl	$257, %eax
.L3849:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3915
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3914:
	.cfi_restore_state
	movq	%rdx, %r12
	movq	%rdx, %r13
	movq	-1(%rax), %rdx
	movq	%rdi, %rbx
	cmpw	$1023, 11(%rdx)
	jbe	.L3916
	movq	(%rsi), %rdx
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %r14
	movq	-1(%rdx), %rdx
	movl	15(%rdx), %edx
	subq	$37592, %r14
	andl	$16777216, %edx
	jne	.L3917
.L3810:
	movq	-1(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3813
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -296(%rbp)
	movq	(%rax), %rsi
.L3814:
	cmpw	$1024, 11(%rsi)
	ja	.L3906
	movq	(%r12), %rax
.L3812:
	testb	$1, %al
	jne	.L3850
.L3853:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3918
.L3852:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, -304(%rbp)
	testq	%rax, %rax
	je	.L3913
	movq	(%rax), %rax
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L3806
	leaq	-224(%rbp), %rcx
	xorl	%r12d, %r12d
	leaq	-144(%rbp), %r15
	movq	%rcx, -296(%rbp)
	leaq	-272(%rbp), %rcx
	movq	%rcx, -312(%rbp)
	jmp	.L3855
	.p2align 4,,10
	.p2align 3
.L3920:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L3857:
	movq	-296(%rbp), %r8
	movq	%r13, %rdx
	movq	%r14, %rcx
	movq	%rbx, %rsi
	pxor	%xmm0, %xmm0
	movl	$1, %r9d
	movq	%r15, %rdi
	andb	$-64, -272(%rbp)
	movups	%xmm0, -264(%rbp)
	movups	%xmm0, -248(%rbp)
	movb	$0, -224(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	movq	-312(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorE
	movzbl	%ah, %edx
	testb	%al, %al
	je	.L3911
	testb	%dl, %dl
	je	.L3874
	testb	$1, -272(%rbp)
	jne	.L3919
.L3874:
	movq	-304(%rbp), %rax
	addq	$1, %r12
	movq	(%rax), %rax
	cmpl	%r12d, 11(%rax)
	jle	.L3806
.L3855:
	movq	15(%rax,%r12,8), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L3920
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L3921
.L3858:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L3857
	.p2align 4,,10
	.p2align 3
.L3916:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L3806
	movl	11(%rax), %edi
	testl	%edi, %edi
	jne	.L3812
	jmp	.L3806
	.p2align 4,,10
	.p2align 3
.L3825:
	movq	-376(%rbp), %rdx
	xorl	%r9d, %r9d
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	(%rdx), %rax
	movq	%rdx, %r8
	movq	%rax, -368(%rbp)
	andq	$-262144, %rax
	movq	24(%rax), %rsi
	subq	$37592, %rsi
	call	_ZN2v88internal14LookupIteratorC1EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_10JSReceiverEEENS1_13ConfigurationE
	cmpl	$4, -140(%rbp)
	je	.L3848
	testb	$16, -128(%rbp)
	jne	.L3848
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L3832
	.p2align 4,,10
	.p2align 3
.L3911:
	xorl	%eax, %eax
.L3913:
	movb	$0, %ah
	jmp	.L3849
	.p2align 4,,10
	.p2align 3
.L3919:
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb@PLT
	movq	%rax, -344(%rbp)
	testq	%rax, %rax
	je	.L3913
	cmpb	$0, -321(%rbp)
	jne	.L3922
	movq	-336(%rbp), %rax
	testq	%rax, %rax
	je	.L3867
	movl	8(%rax), %edx
	testl	%edx, %edx
	jle	.L3867
	xorl	%edx, %edx
	movq	%r12, -368(%rbp)
	movq	%rax, %r12
	movq	%rbx, -376(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	jmp	.L3870
	.p2align 4,,10
	.p2align 3
.L3924:
	addq	$1, %r14
	cmpl	%r14d, 8(%r12)
	jle	.L3923
.L3870:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	%rax, -144(%rbp)
	movq	(%r12), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal6Object9SameValueES1_@PLT
	testb	%al, %al
	je	.L3924
	movq	-368(%rbp), %r12
	movq	-376(%rbp), %rbx
	jmp	.L3874
	.p2align 4,,10
	.p2align 3
.L3921:
	movq	%rbx, %rdi
	movq	%rsi, -344(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-344(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L3858
	.p2align 4,,10
	.p2align 3
.L3923:
	movq	%rbx, %r14
	movq	-368(%rbp), %r12
	movq	-376(%rbp), %rbx
.L3867:
	movq	%r14, %rcx
	movl	$1, %r9d
	movq	-296(%rbp), %r14
	movq	-320(%rbp), %rdx
	leaq	-274(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	cmpb	$0, -274(%rbp)
	je	.L3871
	movb	$1, -352(%rbp)
	movl	-352(%rbp), %eax
	movq	%r14, %rdi
	movq	-344(%rbp), %rsi
	movq	%rax, %rdx
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal8JSObject18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE
	testb	%al, %al
	je	.L3925
.L3872:
	shrw	$8, %ax
	jne	.L3874
.L3873:
	leaq	.LC86(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3813:
	movq	41088(%r14), %rax
	movq	%rax, -296(%rbp)
	cmpq	41096(%r14), %rax
	je	.L3926
.L3815:
	movq	-296(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rcx)
	jmp	.L3814
	.p2align 4,,10
	.p2align 3
.L3922:
	movb	$1, -360(%rbp)
	movq	%rax, %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movl	-360(%rbp), %eax
	movq	-320(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %r9
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	jne	.L3874
	movb	$0, %ah
	jmp	.L3849
	.p2align 4,,10
	.p2align 3
.L3906:
	leaq	-144(%rbp), %r15
	movq	%rsi, -144(%rbp)
	movq	%r15, %rdi
	call	_ZNK2v88internal3Map23OnlyHasSimplePropertiesEv@PLT
	testb	%al, %al
	movb	%al, -344(%rbp)
	movq	(%r12), %rax
	je	.L3812
	movq	15(%rax), %rcx
	cmpq	%rcx, 288(%r14)
	jne	.L3812
	movq	-296(%rbp), %rax
	movq	41112(%r14), %rdi
	movq	(%rax), %rax
	movq	39(%rax), %r13
	testq	%rdi, %rdi
	je	.L3817
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -352(%rbp)
.L3818:
	movq	-296(%rbp), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	je	.L3806
	subl	$1, %eax
	movq	%r14, -312(%rbp)
	movq	-336(%rbp), %rbx
	movq	%rax, -360(%rbp)
	leaq	-273(%rbp), %rax
	movq	%r12, -376(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -400(%rbp)
	movq	-304(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L3821:
	movl	%eax, %r14d
	addq	$1, %rax
	movq	%rax, -336(%rbp)
	leaq	(%rax,%rax,2), %r13
	movq	-352(%rbp), %rax
	salq	$3, %r13
	movq	(%rax), %rax
	movq	-1(%r13,%rax), %rsi
	movq	-312(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3822
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L3823:
	cmpb	$0, -344(%rbp)
	je	.L3825
	movq	-352(%rbp), %rax
	movq	(%rax), %rsi
	movq	7(%r13,%rsi), %rax
	movq	%rax, %rdx
	sarq	$32, %rdx
	btq	$36, %rax
	jc	.L3848
	testb	$1, %dl
	jne	.L3827
	testb	$2, %dl
	je	.L3828
	movq	15(%r13,%rsi), %r14
	movq	-312(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3829
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L3832:
	cmpb	$0, -321(%rbp)
	jne	.L3927
	testq	%rbx, %rbx
	je	.L3841
	movl	8(%rbx), %esi
	testl	%esi, %esi
	jle	.L3841
	xorl	%r14d, %r14d
	jmp	.L3844
	.p2align 4,,10
	.p2align 3
.L3928:
	addq	$1, %r14
	cmpl	%r14d, 8(%rbx)
	jle	.L3841
.L3844:
	movq	(%r12), %rax
	movq	%r15, %rdi
	movq	%rax, -144(%rbp)
	movq	(%rbx), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal6Object9SameValueES1_@PLT
	testb	%al, %al
	je	.L3928
	.p2align 4,,10
	.p2align 3
.L3848:
	movq	-304(%rbp), %rcx
	cmpq	%rcx, -360(%rbp)
	je	.L3806
	movq	-336(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	.L3821
	.p2align 4,,10
	.p2align 3
.L3917:
	movq	-320(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE
	movq	(%r12), %rax
	jmp	.L3810
	.p2align 4,,10
	.p2align 3
.L3850:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3853
	jmp	.L3852
	.p2align 4,,10
	.p2align 3
.L3822:
	movq	41088(%rax), %r12
	cmpq	41096(%rax), %r12
	je	.L3929
.L3824:
	movq	-312(%rbp), %rcx
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%r12)
	jmp	.L3823
	.p2align 4,,10
	.p2align 3
.L3841:
	movq	-400(%rbp), %r8
	movq	-320(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-312(%rbp), %rsi
	movl	$1, %r9d
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	cmpb	$0, -273(%rbp)
	je	.L3871
	movb	$1, -384(%rbp)
	movl	-384(%rbp), %eax
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal8JSObject18CreateDataPropertyEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE
	testb	%al, %al
	je	.L3930
.L3846:
	shrw	$8, %ax
	jne	.L3848
	jmp	.L3873
	.p2align 4,,10
	.p2align 3
.L3918:
	leaq	.LC9(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3926:
	movq	%r14, %rdi
	movq	%rsi, -304(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-304(%rbp), %rsi
	movq	%rax, -296(%rbp)
	jmp	.L3815
.L3927:
	movq	-320(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movl	$3, %r9d
	movq	(%rdx), %rax
	movq	%rdx, %r8
	movq	%rax, -368(%rbp)
	andq	$-262144, %rax
	movq	24(%rax), %rsi
	subq	$37592, %rsi
	call	_ZN2v88internal14LookupIteratorC1EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_10JSReceiverEEENS1_13ConfigurationE
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movb	$1, -392(%rbp)
	movl	-392(%rbp), %eax
	movq	%rax, %rcx
	movq	%rax, -392(%rbp)
	call	_ZN2v88internal6Object11SetPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L3911
	cmpb	$0, -344(%rbp)
	je	.L3848
	movq	-376(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	movq	-296(%rbp), %rcx
	cmpq	%rax, (%rcx)
	sete	-344(%rbp)
	jmp	.L3848
.L3871:
	leaq	.LC85(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3925:
	movl	%eax, -344(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-344(%rbp), %eax
	jmp	.L3872
.L3828:
	movq	-296(%rbp), %rax
	movl	%edx, %r13d
	movl	%r14d, %esi
	shrl	$6, %r13d
	movq	(%rax), %rdi
	andl	$7, %r13d
	call	_ZN2v88internal10FieldIndex13ForDescriptorEPNS0_7IsolateENS0_3MapEi.isra.0
	movq	-376(%rbp), %rdi
	movl	%r13d, %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8JSObject14FastPropertyAtENS0_6HandleIS1_EENS0_14RepresentationENS0_10FieldIndexE
	movq	%rax, %r13
	jmp	.L3832
.L3827:
	movq	-376(%rbp), %r14
	movq	-312(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal10JSReceiver11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3911
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	movq	-296(%rbp), %rcx
	cmpq	%rax, (%rcx)
	sete	-344(%rbp)
	jmp	.L3832
.L3929:
	movq	%rax, %rdi
	movq	%rsi, -368(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-368(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L3824
.L3817:
	movq	41088(%r14), %rax
	movq	%rax, -352(%rbp)
	cmpq	41096(%r14), %rax
	je	.L3931
.L3819:
	movq	-352(%rbp), %rbx
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r14)
	movq	%r13, (%rbx)
	jmp	.L3818
.L3829:
	movq	41088(%rax), %r13
	cmpq	41096(%rax), %r13
	je	.L3932
.L3831:
	movq	-312(%rbp), %rcx
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rcx)
	movq	%r14, 0(%r13)
	jmp	.L3832
.L3930:
	movl	%eax, -368(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-368(%rbp), %eax
	jmp	.L3846
.L3931:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, -352(%rbp)
	jmp	.L3819
.L3932:
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r13
	jmp	.L3831
.L3915:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21400:
	.size	_ZN2v88internal10JSReceiver23SetOrCopyDataPropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPKNS0_12ScopedVectorIS7_EEb, .-_ZN2v88internal10JSReceiver23SetOrCopyDataPropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPKNS0_12ScopedVectorIS7_EEb
	.section	.text._ZN2v88internal8JSObject18TryMigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject18TryMigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSObject18TryMigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSObject18TryMigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB21494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	-1(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3934
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L3935:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Map9TryUpdateEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L3933
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
	movzbl	_ZN2v88internal20FLAG_trace_migrationE(%rip), %eax
	testb	%al, %al
	jne	.L3939
.L3940:
	movl	$1, %eax
.L3933:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3943
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3934:
	.cfi_restore_state
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L3944
.L3936:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L3935
	.p2align 4,,10
	.p2align 3
.L3939:
	movq	0(%r13), %rdx
	movb	%al, -56(%rbp)
	movq	-1(%rdx), %rcx
	cmpq	%rcx, (%rbx)
	je	.L3940
	movq	%rdx, -48(%rbp)
	movq	-1(%rdx), %rcx
	leaq	-48(%rbp), %rdi
	movq	(%rbx), %rdx
	movq	stdout(%rip), %rsi
	call	_ZN2v88internal8JSObject22PrintInstanceMigrationEP8_IO_FILENS0_3MapES4_
	movzbl	-56(%rbp), %eax
	jmp	.L3933
	.p2align 4,,10
	.p2align 3
.L3944:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L3936
.L3943:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21494:
	.size	_ZN2v88internal8JSObject18TryMigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal8JSObject18TryMigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal8JSObject19NormalizePropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeEiPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject19NormalizePropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeEiPKc
	.type	_ZN2v88internal8JSObject19NormalizePropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeEiPKc, @function
_ZN2v88internal8JSObject19NormalizePropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeEiPKc:
.LFB21503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	(%rsi), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	je	.L3953
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3953:
	.cfi_restore_state
	movq	-1(%rax), %r9
	movq	%rdi, %r12
	movq	%rsi, %r13
	movl	%ecx, %r14d
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3948
	movq	%r9, %rsi
	movq	%r8, -40(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-40(%rbp), %r8
	movq	(%rax), %r9
	movq	%rax, %rsi
.L3949:
	movzbl	14(%r9), %edx
	movl	%r15d, %ecx
	movq	%r12, %rdi
	shrl	$3, %edx
	call	_ZN2v88internal3Map9NormalizeEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindENS0_25PropertyNormalizationModeEPKc@PLT
	addq	$16, %rsp
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
	.p2align 4,,10
	.p2align 3
.L3948:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L3954
.L3950:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r9, (%rsi)
	jmp	.L3949
	.p2align 4,,10
	.p2align 3
.L3954:
	movq	%r12, %rdi
	movq	%r8, -48(%rbp)
	movq	%r9, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-48(%rbp), %r8
	movq	-40(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L3950
	.cfi_endproc
.LFE21503:
	.size	_ZN2v88internal8JSObject19NormalizePropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeEiPKc, .-_ZN2v88internal8JSObject19NormalizePropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeEiPKc
	.section	.text._ZN2v88internal8JSObject17NormalizeElementsENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject17NormalizeElementsENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSObject17NormalizeElementsENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSObject17NormalizeElementsENS0_6HandleIS1_EE:
.LFB21506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %r14
	movq	-1(%rax), %rdx
	movq	15(%rax), %rsi
	movzbl	14(%rdx), %ebx
	subq	$37592, %r14
	shrl	$3, %ebx
	subl	$13, %ebx
	cmpb	$1, %bl
	ja	.L3956
	movq	23(%rsi), %rsi
.L3956:
	movq	-1(%rsi), %rdx
	cmpw	$132, 11(%rdx)
	jne	.L3957
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3958
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3958:
	.cfi_restore_state
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L3993
.L3960:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3957:
	.cfi_restore_state
	movq	-1(%rax), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movq	%r13, %rsi
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	(%rdi), %rax
	call	*176(%rax)
	movq	%rax, %r12
	cmpb	$1, %bl
	ja	.L3994
	movq	%r13, %rdi
	movl	$14, %esi
	call	_ZN2v88internal8JSObject24GetElementsTransitionMapENS0_6HandleIS1_EENS0_12ElementsKindE
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
	movq	0(%r13), %rax
	movq	(%r12), %r13
	movq	15(%rax), %r15
	movq	%r13, 23(%r15)
	leaq	23(%r15), %rsi
	testb	$1, %r13b
	je	.L3966
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L3964
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rsi
.L3964:
	testb	$24, %al
	je	.L3966
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3995
	.p2align 4,,10
	.p2align 3
.L3966:
	movq	40960(%r14), %rbx
	cmpb	$0, 6112(%rbx)
	je	.L3968
	movq	6104(%rbx), %rax
.L3969:
	testq	%rax, %rax
	je	.L3970
	addl	$1, (%rax)
.L3970:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3968:
	.cfi_restore_state
	movb	$1, 6112(%rbx)
	leaq	6088(%rbx), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6104(%rbx)
	jmp	.L3969
	.p2align 4,,10
	.p2align 3
.L3994:
	movq	0(%r13), %rax
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$15, %eax
	sete	%sil
	leal	12(,%rsi,4), %esi
	call	_ZN2v88internal8JSObject24GetElementsTransitionMapENS0_6HandleIS1_EENS0_12ElementsKindE
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
	movq	0(%r13), %r15
	movq	(%r12), %r13
	movq	%r13, 15(%r15)
	leaq	15(%r15), %rbx
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	je	.L3966
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3966
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3966
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3966
	.p2align 4,,10
	.p2align 3
.L3993:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L3960
	.p2align 4,,10
	.p2align 3
.L3995:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3966
	.cfi_endproc
.LFE21506:
	.size	_ZN2v88internal8JSObject17NormalizeElementsENS0_6HandleIS1_EE, .-_ZN2v88internal8JSObject17NormalizeElementsENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc.str1.1,"aMS",@progbits,1
.LC87:
	.string	"0 == value"
.LC88:
	.string	"0 <= value"
	.section	.rodata._ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC89:
	.string	"static_cast<unsigned>(value) <= 255"
	.section	.rodata._ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc.str1.1
.LC90:
	.string	"SlowToFast"
.LC91:
	.string	"k.IsUniqueName()"
	.section	.rodata._ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc.str1.8
	.align 8
.LC92:
	.string	"static_cast<unsigned>(id) < 256"
	.align 8
.LC93:
	.string	"static_cast<unsigned>(value) < JSObject::kFieldsAdded"
	.section	.text._ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc
	.type	_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc, @function
_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc:
.LFB21504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -148(%rbp)
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	jne	.L4120
.L3996:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4121
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4120:
	.cfi_restore_state
	movq	%rax, %rdx
	movq	7(%rax), %rsi
	movq	%rdi, %r12
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	subq	$37592, %rbx
	testb	$1, %sil
	jne	.L3999
	movq	1056(%rbx), %rsi
.L3999:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4000
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L4001:
	cmpl	$1020, 19(%rsi)
	jg	.L3996
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE16IterationIndicesEPNS0_7IsolateENS0_6HandleIS2_EE@PLT
	movq	(%rax), %rcx
	movq	%rax, -192(%rbp)
	movslq	11(%rcx), %r14
	testq	%r14, %r14
	jle	.L4072
	leal	-1(%r14), %eax
	movq	0(%r13), %rdi
	movl	$16, %edx
	xorl	%r15d, %r15d
	leaq	24(,%rax,8), %r8
	movabsq	$4294967296, %rsi
	.p2align 4,,10
	.p2align 3
.L4005:
	movq	-1(%rcx,%rdx), %rax
	sarq	$32, %rax
	leal	9(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rax,%rdi), %rax
	andq	%rsi, %rax
	cmpq	$1, %rax
	adcl	$0, %r15d
	addq	$8, %rdx
	cmpq	%rdx, %r8
	jne	.L4005
.L4003:
	movq	(%r12), %rax
	movq	-1(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4006
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -176(%rbp)
	movq	(%rax), %rsi
.L4007:
	movzbl	7(%rsi), %edx
	movzbl	8(%rsi), %eax
	movq	%rbx, %rdi
	movq	-176(%rbp), %rsi
	subl	%eax, %edx
	movl	%edx, -152(%rbp)
	call	_ZN2v88internal3Map19CopyDropDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rcx
	movq	%rax, %rdi
	testb	$36, 13(%rcx)
	movl	15(%rcx), %eax
	setne	%dl
	movzbl	%dl, %edx
	andl	$-268435457, %eax
	sall	$28, %edx
	orl	%edx, %eax
	movl	%eax, 15(%rcx)
	movq	%rdi, -200(%rbp)
	movq	(%rdi), %rdx
	movl	15(%rdx), %eax
	andl	$-35651585, %eax
	movl	%eax, 15(%rdx)
	movq	-176(%rbp), %rax
	movq	(%rax), %rdi
	movl	15(%rdi), %eax
	testl	$1048576, %eax
	je	.L4009
	call	_ZN2v88internal12_GLOBAL__N_133InvalidatePrototypeChainsInternalENS0_3MapE
	movq	-200(%rbp), %rsi
	movq	-176(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZN2v88internal8JSObject31UpdatePrototypeUserRegistrationENS0_6HandleINS0_3MapEEES4_PNS0_7IsolateE
.L4009:
	testq	%r14, %r14
	je	.L4122
	movl	$1, %ecx
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal15DescriptorArray8AllocateEPNS0_7IsolateEiiNS0_14AllocationTypeE@PLT
	movl	-152(%rbp), %ecx
	movq	%rax, -208(%rbp)
	movl	-148(%rbp), %eax
	addl	%r15d, %eax
	subl	%ecx, %eax
	movl	%eax, -248(%rbp)
	jns	.L4020
	movl	$0, -248(%rbp)
	subl	%r15d, %ecx
	movl	%ecx, -148(%rbp)
.L4020:
	movl	-248(%rbp), %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory16NewPropertyArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, -296(%rbp)
	movq	-176(%rbp), %rax
	movq	(%rax), %rax
	movzbl	14(%rax), %edx
	shrl	$3, %edx
	cmpb	$3, %dl
	setne	%al
	cmpl	$5, %edx
	setbe	%dl
	andl	%edx, %eax
	testl	%r14d, %r14d
	jle	.L4123
	leal	-1(%r14), %edx
	xorl	$1, %eax
	movl	$0, -160(%rbp)
	movl	$16, %r14d
	leaq	24(,%rdx,8), %rsi
	movzbl	%al, %eax
	movq	%r12, -304(%rbp)
	movq	%rsi, -232(%rbp)
	leaq	-128(%rbp), %rsi
	movq	%rsi, -216(%rbp)
	leaq	-96(%rbp), %rsi
	movq	%rsi, -224(%rbp)
	movl	%eax, -244(%rbp)
.L4054:
	movq	-192(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%r14,%rax), %rax
	sarq	$32, %rax
	leal	(%rax,%rax,2), %edx
	movq	0(%r13), %rax
	leal	56(,%rdx,8), %r12d
	movslq	%r12d, %rcx
	movq	-1(%rcx,%rax), %rsi
	movq	-1(%rsi), %rax
	leaq	-1(%rsi), %rcx
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L4124
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4025
	movl	%edx, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-168(%rbp), %edx
	movq	(%rax), %rsi
	movq	%rax, %r15
	movq	-1(%rsi), %rax
	leaq	-1(%rsi), %rcx
	cmpw	$64, 11(%rax)
	je	.L4125
.L4029:
	movq	0(%r13), %rcx
	leal	72(,%rdx,8), %eax
	addl	$8, %r12d
	movslq	%r12d, %r12
	cltq
	movq	-1(%r12,%rcx), %r12
	movq	-1(%rax,%rcx), %rax
	movq	-216(%rbp), %rdi
	sarq	$32, %rax
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal10DescriptorC1Ev@PLT
	movq	-168(%rbp), %rax
	movl	%eax, %ecx
	shrl	$3, %ecx
	andl	$7, %ecx
	testb	$1, %al
	jne	.L4031
	movq	%rbx, %rdi
	movl	%ecx, -240(%rbp)
	call	_ZN2v88internal9FieldType3AnyEPNS0_7IsolateE@PLT
	subq	$8, %rsp
	movq	%r15, %rsi
	movq	-224(%rbp), %rdi
	movq	%rax, -136(%rbp)
	leaq	-144(%rbp), %rax
	movl	-244(%rbp), %r8d
	movl	$4, %r9d
	movl	-240(%rbp), %ecx
	movl	-160(%rbp), %edx
	pushq	%rax
	movq	%rdi, -168(%rbp)
	movl	$1, -144(%rbp)
	call	_ZN2v88internal10Descriptor9DataFieldENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_17PropertyConstnessENS0_14RepresentationERKNS0_17MaybeObjectHandleE@PLT
	movq	-80(%rbp), %rax
	movdqa	-96(%rbp), %xmm0
	popq	%rsi
	popq	%rdi
	movq	%rax, -112(%rbp)
	movl	-72(%rbp), %eax
	movaps	%xmm0, -128(%rbp)
	movl	%eax, -104(%rbp)
.L4032:
	testb	$2, -104(%rbp)
	jne	.L4036
	movq	%r12, %rcx
	movl	-160(%rbp), %esi
	notq	%rcx
	andl	$1, %ecx
	cmpl	%esi, -152(%rbp)
	jle	.L4037
	movq	-304(%rbp), %rax
	movb	%cl, -240(%rbp)
	movq	%r12, %rdx
	movq	(%rax), %rdi
	movq	-1(%rdi), %rax
	movzbl	8(%rax), %eax
	addl	%esi, %eax
	sall	$3, %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%r12, (%rsi)
	movq	%rsi, -264(%rbp)
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movzbl	-240(%rbp), %ecx
	testb	%cl, %cl
	jne	.L4039
	movq	-264(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L4039:
	addl	$1, -160(%rbp)
.L4036:
	movq	-208(%rbp), %rax
	movl	-120(%rbp), %ecx
	movq	(%rax), %rdi
	movq	-128(%rbp), %rax
	movq	(%rax), %rdx
	movq	-112(%rbp), %rax
	testl	%ecx, %ecx
	jne	.L4043
	testq	%rax, %rax
	je	.L4046
	movq	(%rax), %rcx
	orq	$2, %rcx
.L4045:
	leaq	-1(%rdi), %r8
	leaq	-24(%r14,%r14,2), %rax
	movl	-104(%rbp), %r12d
	addq	%r8, %rax
	movq	%rdx, (%rax)
	testb	$1, %dl
	je	.L4069
	movq	%rdx, %r9
	andq	$-262144, %r9
	movq	8(%r9), %rsi
	movq	%r9, -240(%rbp)
	testl	$262144, %esi
	je	.L4048
	movq	%rax, %rsi
	movq	%r8, -288(%rbp)
	movq	%rcx, -280(%rbp)
	movq	%rdx, -272(%rbp)
	movq	%rax, -264(%rbp)
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-240(%rbp), %r9
	movq	-288(%rbp), %r8
	movq	-280(%rbp), %rcx
	movq	-272(%rbp), %rdx
	movq	8(%r9), %rsi
	movq	-264(%rbp), %rax
	movq	-256(%rbp), %rdi
.L4048:
	andl	$24, %esi
	je	.L4069
	movq	%rdi, %rsi
	andq	$-262144, %rsi
	testb	$24, 8(%rsi)
	jne	.L4069
	movq	%rax, %rsi
	movq	%r8, -272(%rbp)
	movq	%rcx, -264(%rbp)
	movq	%rax, -256(%rbp)
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-272(%rbp), %r8
	movq	-264(%rbp), %rcx
	movq	-256(%rbp), %rax
	movq	-240(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L4069:
	addl	%r12d, %r12d
	sarl	%r12d
	salq	$32, %r12
	movq	%r12, 8(%rax)
	movq	%rcx, 16(%rax)
	testb	$1, %cl
	je	.L4068
	cmpl	$3, %ecx
	je	.L4068
	movq	%rcx, %rdx
	andq	$-262144, %rcx
	leal	-8(%r14,%r14,2), %esi
	movq	8(%rcx), %rax
	movslq	%esi, %rsi
	andq	$-3, %rdx
	movq	%rcx, %r12
	addq	%r8, %rsi
	testl	$262144, %eax
	je	.L4051
	movq	%rdx, -264(%rbp)
	movq	%rsi, -256(%rbp)
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r12), %rax
	movq	-264(%rbp), %rdx
	movq	-256(%rbp), %rsi
	movq	-240(%rbp), %rdi
.L4051:
	testb	$24, %al
	je	.L4068
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4068
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L4068:
	addq	$8, %r14
	cmpq	-232(%rbp), %r14
	jne	.L4054
	movq	-304(%rbp), %r12
.L4053:
	movq	-208(%rbp), %r14
	movq	-168(%rbp), %rdi
	movq	(%r14), %rax
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal15DescriptorArray4SortEv@PLT
	movq	(%r14), %rax
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movswl	9(%rax), %ecx
	movq	-200(%rbp), %r15
	movq	%r15, %rsi
	call	_ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi@PLT
	movq	(%r15), %rdx
	movq	%rbx, %rsi
	movq	%rdx, -96(%rbp)
	movq	(%r14), %rdx
	movq	(%rax), %r13
	movswl	9(%rdx), %ecx
	movq	-168(%rbp), %rdi
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi@PLT
	movq	-96(%rbp), %rax
	movq	%r13, %rdx
	movq	%r13, 47(%rax)
	movq	-96(%rbp), %rdi
	leaq	47(%rdi), %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-96(%rbp), %rdi
	testb	$1, %r13b
	je	.L4022
	leaq	47(%rdi), %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L4022:
	movq	-96(%rbp), %rdi
	call	_ZN2v88internal3Map12GetVisitorIdES1_@PLT
	cmpl	$255, %eax
	ja	.L4126
	movq	-96(%rbp), %rdx
	movb	%al, 10(%rdx)
	movl	-248(%rbp), %edx
	testl	%edx, %edx
	jne	.L4056
	movq	-200(%rbp), %rax
	movq	(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	ja	.L4057
	movl	-148(%rbp), %eax
	testl	%eax, %eax
	jne	.L4058
	movb	$0, 9(%rdx)
.L4062:
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	jne	.L4127
.L4065:
	movq	(%r12), %rax
	movq	-168(%rbp), %rbx
	movq	%rax, -96(%rbp)
	movq	-200(%rbp), %rax
	movq	%rbx, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal10HeapObject20synchronized_set_mapENS0_3MapE
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	%rax, -96(%rbp)
	movq	-296(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE
	jmp	.L3996
	.p2align 4,,10
	.p2align 3
.L4000:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L4128
.L4002:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L4001
	.p2align 4,,10
	.p2align 3
.L4006:
	movq	41088(%rbx), %rax
	movq	%rax, -176(%rbp)
	cmpq	41096(%rbx), %rax
	je	.L4129
.L4008:
	movq	-176(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L4007
	.p2align 4,,10
	.p2align 3
.L4043:
	testq	%rax, %rax
	je	.L4046
	movq	(%rax), %rcx
	jmp	.L4045
	.p2align 4,,10
	.p2align 3
.L4031:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4033
	movq	%r12, %rsi
	movl	%ecx, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-168(%rbp), %ecx
	movq	%rax, %rdx
.L4034:
	movq	-224(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rdi, -168(%rbp)
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-80(%rbp), %rax
	movdqa	-96(%rbp), %xmm1
	movq	%rax, -112(%rbp)
	movl	-72(%rbp), %eax
	movaps	%xmm1, -128(%rbp)
	movl	%eax, -104(%rbp)
	jmp	.L4032
	.p2align 4,,10
	.p2align 3
.L4025:
	movq	41088(%rbx), %r15
	cmpq	41096(%rbx), %r15
	je	.L4130
.L4027:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	movq	(%rcx), %rax
	cmpw	$64, 11(%rax)
	jne	.L4029
.L4125:
	testb	$8, 11(%rsi)
	je	.L4029
	movq	-200(%rbp), %rax
	movq	(%rax), %rcx
	movl	15(%rcx), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rcx)
	jmp	.L4029
	.p2align 4,,10
	.p2align 3
.L4037:
	movq	-296(%rbp), %rax
	movq	(%rax), %rdi
	movl	-160(%rbp), %eax
	subl	-152(%rbp), %eax
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%r12, (%rsi)
	testb	%cl, %cl
	jne	.L4039
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -240(%rbp)
	testl	$262144, %eax
	jne	.L4131
.L4041:
	testb	$24, %al
	je	.L4039
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4039
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4039
	.p2align 4,,10
	.p2align 3
.L4033:
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L4132
.L4035:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, (%rdx)
	jmp	.L4034
	.p2align 4,,10
	.p2align 3
.L4122:
	movq	-200(%rbp), %rax
	movq	(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L4133
	movl	-152(%rbp), %r8d
	testl	%r8d, %r8d
	js	.L4060
	movzbl	7(%rdx), %eax
	movzbl	8(%rdx), %esi
	movzbl	8(%rdx), %ecx
	subl	%esi, %eax
	subl	-152(%rbp), %eax
	addl	%ecx, %eax
	cmpl	$255, %eax
	ja	.L4061
	movb	%al, 9(%rdx)
.L4013:
	movq	-200(%rbp), %rax
	movq	(%r12), %rdi
	movq	(%rax), %rdx
	movq	%rdx, -1(%rdi)
	testq	%rdx, %rdx
	jne	.L4134
.L4016:
	movq	(%r12), %rax
	leaq	-96(%rbp), %rdi
	movq	%rax, -96(%rbp)
	movq	288(%rbx), %rsi
	call	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	je	.L3996
	movq	41016(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L3996
	movq	-200(%rbp), %rax
	xorl	%r9d, %r9d
	movq	%r12, %rdi
	movq	-184(%rbp), %r8
	leaq	.LC90(%rip), %rsi
	movq	(%rax), %rcx
	movq	-176(%rbp), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal6Logger8MapEventEPKcNS0_3MapES4_S3_NS0_10HeapObjectE@PLT
	jmp	.L3996
	.p2align 4,,10
	.p2align 3
.L4124:
	leaq	.LC91(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4046:
	leaq	.LC9(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4131:
	movq	%r12, %rdx
	movq	%rsi, -264(%rbp)
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-240(%rbp), %rcx
	movq	-264(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movq	8(%rcx), %rax
	jmp	.L4041
	.p2align 4,,10
	.p2align 3
.L4133:
	movl	-152(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L4058
	movb	$0, 9(%rdx)
	jmp	.L4013
	.p2align 4,,10
	.p2align 3
.L4128:
	movq	%rbx, %rdi
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L4002
	.p2align 4,,10
	.p2align 3
.L4130:
	movq	%rbx, %rdi
	movl	%edx, -256(%rbp)
	movq	%rcx, -240(%rbp)
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-256(%rbp), %edx
	movq	-240(%rbp), %rcx
	movq	-168(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L4027
	.p2align 4,,10
	.p2align 3
.L4072:
	xorl	%r15d, %r15d
	jmp	.L4003
	.p2align 4,,10
	.p2align 3
.L4132:
	movq	%rbx, %rdi
	movl	%ecx, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-168(%rbp), %ecx
	movq	%rax, %rdx
	jmp	.L4035
.L4134:
	testb	$1, %dl
	je	.L4016
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L4016
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4016
.L4129:
	movq	%rbx, %rdi
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	movq	%rax, -176(%rbp)
	jmp	.L4008
.L4056:
	cmpl	$2, -148(%rbp)
	ja	.L4135
	movq	-200(%rbp), %rax
	movzbl	-148(%rbp), %esi
	movq	(%rax), %rax
	movb	%sil, 9(%rax)
	jmp	.L4062
.L4127:
	movq	41016(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L4065
	movq	-200(%rbp), %rax
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	movq	-184(%rbp), %r8
	leaq	.LC90(%rip), %rsi
	movq	(%rax), %rcx
	movq	-176(%rbp), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal6Logger8MapEventEPKcNS0_3MapES4_S3_NS0_10HeapObjectE@PLT
	jmp	.L4065
.L4123:
	leaq	-96(%rbp), %rax
	movq	%rax, -168(%rbp)
	jmp	.L4053
.L4057:
	movl	-148(%rbp), %edi
	testl	%edi, %edi
	js	.L4060
	movzbl	7(%rdx), %eax
	movzbl	8(%rdx), %esi
	movzbl	8(%rdx), %ecx
	subl	%esi, %eax
	subl	%edi, %eax
	addl	%ecx, %eax
	cmpl	$255, %eax
	ja	.L4061
	movb	%al, 9(%rdx)
	jmp	.L4062
.L4060:
	leaq	.LC88(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4058:
	leaq	.LC87(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4061:
	leaq	.LC89(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4126:
	leaq	.LC92(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4135:
	leaq	.LC93(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4121:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21504:
	.size	_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc, .-_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc
	.section	.rodata._ZN2v88internal8JSObject19OptimizeAsPrototypeENS0_6HandleIS1_EEb.str1.1,"aMS",@progbits,1
.LC94:
	.string	"NormalizeAsPrototype"
.LC95:
	.string	"OptimizeAsPrototype"
.LC96:
	.string	"CopyAsPrototype"
	.section	.text._ZN2v88internal8JSObject19OptimizeAsPrototypeENS0_6HandleIS1_EEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject19OptimizeAsPrototypeENS0_6HandleIS1_EEb
	.type	_ZN2v88internal8JSObject19OptimizeAsPrototypeENS0_6HandleIS1_EEb, @function
_ZN2v88internal8JSObject19OptimizeAsPrototypeENS0_6HandleIS1_EEb:
.LFB21529:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	-1(%rax), %rcx
	cmpw	$1025, 11(%rcx)
	je	.L4136
	movq	%rdi, %r12
	leaq	-37592(%rdx), %r13
	leaq	-1(%rax), %rcx
	testb	%sil, %sil
	jne	.L4172
.L4138:
	movq	(%rcx), %rdx
	movl	15(%rdx), %edx
	andl	$1048576, %edx
	jne	.L4173
	movq	-1(%rax), %r14
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L4149
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L4150:
	leaq	.LC96(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %eax
	orl	$1048576, %eax
	movl	%eax, 15(%rdx)
	movq	(%r12), %rax
	movq	-1(%rax), %rax
.L4171:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L4174
.L4136:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4175
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4173:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movq	71(%rdx), %rdx
	testb	$1, %dl
	je	.L4136
	movq	-1(%rdx), %rcx
	cmpw	$95, 11(%rcx)
	jne	.L4136
	testb	$1, 51(%rdx)
	je	.L4136
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L4136
	leaq	.LC95(%rip), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject17MigrateSlowToFastENS0_6HandleIS1_EEiPKc
	jmp	.L4136
	.p2align 4,,10
	.p2align 3
.L4172:
	movq	-1(%rax), %rsi
	movl	15(%rsi), %esi
	andl	$2097152, %esi
	jne	.L4138
	movq	-1(%rax), %rsi
	cmpw	$1026, 11(%rsi)
	je	.L4138
	movq	3344(%rdx), %rdx
	movl	8(%rdx), %edx
	testl	%edx, %edx
	jne	.L4138
	movq	(%rcx), %rdx
	movl	15(%rdx), %edx
	andl	$1048576, %edx
	jne	.L4176
.L4142:
	xorl	%ecx, %ecx
	leaq	.LC94(%rip), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$1, %edx
	call	_ZN2v88internal8JSObject19NormalizePropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeEiPKc
	movq	(%r12), %rax
	leaq	-1(%rax), %rcx
	jmp	.L4138
	.p2align 4,,10
	.p2align 3
.L4174:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L4171
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L4136
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	testb	$1, %dl
	jne	.L4177
.L4155:
	movq	31(%rax), %rax
	leaq	-48(%rbp), %rdi
	movl	$4, %edx
	movq	39(%rax), %rax
	movq	879(%rax), %rsi
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal3Map14SetConstructorENS0_6ObjectENS0_16WriteBarrierModeE
	jmp	.L4136
	.p2align 4,,10
	.p2align 3
.L4149:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L4178
.L4151:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%rsi)
	jmp	.L4150
	.p2align 4,,10
	.p2align 3
.L4178:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4151
	.p2align 4,,10
	.p2align 3
.L4176:
	movq	-1(%rax), %rdx
	movq	71(%rdx), %rdx
	testb	$1, %dl
	je	.L4142
	movq	-1(%rdx), %rsi
	cmpw	$95, 11(%rsi)
	jne	.L4142
	testb	$1, 51(%rdx)
	jne	.L4138
	jmp	.L4142
	.p2align 4,,10
	.p2align 3
.L4177:
	movq	-1(%rdx), %rdx
	cmpw	$88, 11(%rdx)
	jne	.L4155
	jmp	.L4136
.L4175:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21529:
	.size	_ZN2v88internal8JSObject19OptimizeAsPrototypeENS0_6HandleIS1_EEb, .-_ZN2v88internal8JSObject19OptimizeAsPrototypeENS0_6HandleIS1_EEb
	.section	.text._ZN2v88internal8JSObject18MakePrototypesFastENS0_6HandleINS0_6ObjectEEENS0_12WhereToStartEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject18MakePrototypesFastENS0_6HandleINS0_6ObjectEEENS0_12WhereToStartEPNS0_7IsolateE
	.type	_ZN2v88internal8JSObject18MakePrototypesFastENS0_6HandleINS0_6ObjectEEENS0_12WhereToStartEPNS0_7IsolateE, @function
_ZN2v88internal8JSObject18MakePrototypesFastENS0_6HandleINS0_6ObjectEEENS0_12WhereToStartEPNS0_7IsolateE:
.LFB21527:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	testb	$1, %al
	jne	.L4203
.L4179:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4204
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4203:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L4179
	movq	%rdx, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	%rdi, -48(%rbp)
	movl	$0, -40(%rbp)
	movb	$0, -36(%rbp)
	movl	$0, -32(%rbp)
	cmpl	$1, %esi
	je	.L4205
.L4184:
	leaq	-64(%rbp), %rbx
.L4194:
	movq	-48(%rbp), %r12
	movq	(%r12), %rax
	testb	$1, %al
	je	.L4179
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L4179
	movq	-1(%rax), %rsi
	movl	15(%rsi), %eax
	testl	$1048576, %eax
	je	.L4189
	movq	71(%rsi), %rax
	testb	$1, %al
	jne	.L4206
.L4190:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L4191
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rdi
.L4192:
	movl	$1, %esi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal3Map27SetShouldBeFastPrototypeMapENS0_6HandleIS1_EEbPNS0_7IsolateE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject19OptimizeAsPrototypeENS0_6HandleIS1_EEb
	movq	-72(%rbp), %rdx
.L4189:
	movq	%rbx, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	cmpb	$0, -36(%rbp)
	movq	-72(%rbp), %rdx
	je	.L4194
	jmp	.L4179
	.p2align 4,,10
	.p2align 3
.L4205:
	leaq	-64(%rbp), %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	cmpb	$0, -36(%rbp)
	movq	-72(%rbp), %rdx
	je	.L4184
	jmp	.L4179
.L4191:
	movq	41088(%rdx), %rdi
	cmpq	41096(%rdx), %rdi
	je	.L4207
.L4193:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rdi)
	jmp	.L4192
.L4206:
	movq	-1(%rax), %rcx
	cmpw	$95, 11(%rcx)
	jne	.L4190
	testb	$1, 51(%rax)
	je	.L4190
	jmp	.L4179
.L4207:
	movq	%rdx, %rdi
	movq	%rsi, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L4193
.L4204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21527:
	.size	_ZN2v88internal8JSObject18MakePrototypesFastENS0_6HandleINS0_6ObjectEEENS0_12WhereToStartEPNS0_7IsolateE, .-_ZN2v88internal8JSObject18MakePrototypesFastENS0_6HandleINS0_6ObjectEEENS0_12WhereToStartEPNS0_7IsolateE
	.section	.text._ZN2v88internal8JSObject21ReoptimizeIfPrototypeENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject21ReoptimizeIfPrototypeENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSObject21ReoptimizeIfPrototypeENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSObject21ReoptimizeIfPrototypeENS0_6HandleIS1_EE:
.LFB21530:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$1048576, %edx
	jne	.L4217
.L4208:
	ret
	.p2align 4,,10
	.p2align 3
.L4217:
	movq	-1(%rax), %rax
	movq	71(%rax), %rax
	testb	$1, %al
	je	.L4208
	movq	-1(%rax), %rdx
	cmpw	$95, 11(%rdx)
	jne	.L4208
	testb	$1, 51(%rax)
	je	.L4208
	movl	$1, %esi
	jmp	_ZN2v88internal8JSObject19OptimizeAsPrototypeENS0_6HandleIS1_EEb
	.cfi_endproc
.LFE21530:
	.size	_ZN2v88internal8JSObject21ReoptimizeIfPrototypeENS0_6HandleIS1_EE, .-_ZN2v88internal8JSObject21ReoptimizeIfPrototypeENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal8JSObject25InvalidatePrototypeChainsENS0_3MapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject25InvalidatePrototypeChainsENS0_3MapE
	.type	_ZN2v88internal8JSObject25InvalidatePrototypeChainsENS0_3MapE, @function
_ZN2v88internal8JSObject25InvalidatePrototypeChainsENS0_3MapE:
.LFB21535:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal12_GLOBAL__N_133InvalidatePrototypeChainsInternalENS0_3MapE
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21535:
	.size	_ZN2v88internal8JSObject25InvalidatePrototypeChainsENS0_3MapE, .-_ZN2v88internal8JSObject25InvalidatePrototypeChainsENS0_3MapE
	.section	.text._ZN2v88internal8JSObject31InvalidatePrototypeValidityCellENS0_14JSGlobalObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject31InvalidatePrototypeValidityCellENS0_14JSGlobalObjectE
	.type	_ZN2v88internal8JSObject31InvalidatePrototypeValidityCellENS0_14JSGlobalObjectE, @function
_ZN2v88internal8JSObject31InvalidatePrototypeValidityCellENS0_14JSGlobalObjectE:
.LFB21536:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	-1(%rdi), %rbx
	cmpb	$0, _ZN2v88internal26FLAG_trace_prototype_usersE(%rip)
	jne	.L4226
	movq	63(%rbx), %rax
	testb	$1, %al
	jne	.L4227
.L4220:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4226:
	.cfi_restore_state
	xorl	%eax, %eax
	movq	%rbx, %rsi
	leaq	.LC11(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	63(%rbx), %rax
	testb	$1, %al
	je	.L4220
.L4227:
	movq	-1(%rax), %rdx
	cmpw	$151, 11(%rdx)
	jne	.L4220
	movabsq	$4294967296, %rdx
	movq	%rdx, 7(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21536:
	.size	_ZN2v88internal8JSObject31InvalidatePrototypeValidityCellENS0_14JSGlobalObjectE, .-_ZN2v88internal8JSObject31InvalidatePrototypeValidityCellENS0_14JSGlobalObjectE
	.section	.text._ZN2v88internal8JSObject12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE
	.type	_ZN2v88internal8JSObject12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE, @function
_ZN2v88internal8JSObject12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE:
.LFB21537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$88, %rsp
	movq	(%rdi), %r15
	movl	%ecx, -116(%rbp)
	movq	%r15, %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	leaq	-37592(%rax), %r14
	testb	%bl, %bl
	jne	.L4281
.L4229:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L4282
.L4240:
	cmpq	%rax, 104(%r14)
	je	.L4244
.L4280:
	movl	$257, %eax
.L4238:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4283
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4282:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L4240
.L4244:
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	movl	15(%rax), %r15d
	shrl	$27, %r15d
	movl	%r15d, %eax
	andl	$1, %eax
	movb	%al, -112(%rbp)
	testb	%bl, %bl
	jne	.L4242
.L4265:
	movq	%r12, %r15
.L4243:
	movq	(%r15), %rax
	movq	-1(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4249
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L4250:
	movq	0(%r13), %rdx
	cmpq	23(%rsi), %rdx
	je	.L4280
	testb	$2, 14(%rsi)
	je	.L4253
	cmpl	$1, -116(%rbp)
	je	.L4279
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movl	$57, %esi
.L4278:
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L4238
	.p2align 4,,10
	.p2align 3
.L4281:
	movq	-1(%r15), %rcx
	cmpw	$1026, 11(%rcx)
	je	.L4284
	movq	-1(%r15), %rax
	movzbl	13(%rax), %eax
	shrb	$5, %al
	andl	$1, %eax
.L4231:
	testb	%al, %al
	je	.L4229
	movq	41112(%r14), %rdi
	movq	12464(%r14), %r15
	testq	%rdi, %rdi
	je	.L4233
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L4234:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	jne	.L4229
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	12552(%r14), %rax
	cmpq	%rax, 96(%r14)
	jne	.L4285
	cmpl	$1, -116(%rbp)
	je	.L4279
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$79, %esi
	jmp	.L4278
	.p2align 4,,10
	.p2align 3
.L4253:
	cmpb	$0, -112(%rbp)
	jne	.L4255
	cmpl	$1, -116(%rbp)
	jne	.L4286
.L4279:
	movl	$1, %eax
	jmp	.L4238
	.p2align 4,,10
	.p2align 3
.L4249:
	movq	41088(%r14), %rbx
	cmpq	41096(%r14), %rbx
	je	.L4287
.L4251:
	leaq	8(%rbx), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rbx)
	jmp	.L4250
	.p2align 4,,10
	.p2align 3
.L4242:
	leaq	-96(%rbp), %rbx
	movq	%r14, -96(%rbp)
	movq	%rbx, %rdi
	movq	$0, -88(%rbp)
	movq	%r12, -80(%rbp)
	movl	$1, -72(%rbp)
	movb	$0, -68(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	cmpb	$0, -68(%rbp)
	jne	.L4265
.L4248:
	movq	%rbx, %rdi
	movq	-80(%rbp), %r15
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	cmpb	$0, -112(%rbp)
	je	.L4288
	movq	(%r15), %rax
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	shrl	$27, %eax
	andl	$1, %eax
.L4247:
	cmpb	$0, -68(%rbp)
	movb	%al, -112(%rbp)
	je	.L4248
	jmp	.L4243
	.p2align 4,,10
	.p2align 3
.L4288:
	movzbl	-68(%rbp), %eax
	testb	%al, %al
	movb	%al, -104(%rbp)
	jne	.L4243
	movq	%rbx, %rdi
	movq	-80(%rbp), %r15
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	movzbl	-104(%rbp), %eax
	jmp	.L4247
	.p2align 4,,10
	.p2align 3
.L4284:
	movq	-25128(%rax), %rax
	leaq	-96(%rbp), %rdi
	movq	%rdx, -128(%rbp)
	movq	%rdi, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	-128(%rbp), %rdx
	movq	-112(%rbp), %rdi
	movq	%rax, -104(%rbp)
	movq	24(%rdx), %rdx
	movq	%r15, -88(%rbp)
	movq	$0, -80(%rbp)
	subq	$37592, %rdx
	movl	$0, -72(%rbp)
	movq	%rdx, -96(%rbp)
	movb	$0, -68(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	movq	-104(%rbp), %rax
	cmpq	%rax, -88(%rbp)
	setne	%al
	jmp	.L4231
	.p2align 4,,10
	.p2align 3
.L4233:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L4289
.L4235:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r15, (%rsi)
	jmp	.L4234
	.p2align 4,,10
	.p2align 3
.L4255:
	testb	$1, %dl
	jne	.L4290
.L4258:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate37UpdateNoElementsProtectorOnSetElementENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Map21TransitionToPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEE@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
	jmp	.L4280
	.p2align 4,,10
	.p2align 3
.L4286:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movl	$83, %esi
	jmp	.L4278
	.p2align 4,,10
	.p2align 3
.L4285:
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L4238
	.p2align 4,,10
	.p2align 3
.L4290:
	movq	-1(%rdx), %rcx
	cmpw	$1023, 11(%rcx)
	jbe	.L4258
	leaq	-96(%rbp), %rax
	movq	%r14, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	$0, -80(%rbp)
	movl	$0, -72(%rbp)
	movb	$0, -68(%rbp)
	movl	$0, -64(%rbp)
	movq	%rax, -104(%rbp)
	jmp	.L4259
	.p2align 4,,10
	.p2align 3
.L4260:
	movq	-104(%rbp), %rdi
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	cmpb	$0, -68(%rbp)
	jne	.L4258
	movq	-88(%rbp), %rdx
.L4259:
	cmpq	%rdx, (%r12)
	jne	.L4260
	cmpl	$1, -116(%rbp)
	je	.L4279
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	jmp	.L4278
	.p2align 4,,10
	.p2align 3
.L4287:
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L4251
	.p2align 4,,10
	.p2align 3
.L4289:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4235
.L4283:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21537:
	.size	_ZN2v88internal8JSObject12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE, .-_ZN2v88internal8JSObject12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE
	.section	.text._ZN2v88internal10JSReceiver12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE
	.type	_ZN2v88internal10JSReceiver12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE, @function
_ZN2v88internal10JSReceiver12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE:
.LFB21464:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movzbl	%dl, %edx
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	je	.L4293
	jmp	_ZN2v88internal8JSObject12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE
	.p2align 4,,10
	.p2align 3
.L4293:
	jmp	_ZN2v88internal7JSProxy12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE@PLT
	.cfi_endproc
.LFE21464:
	.size	_ZN2v88internal10JSReceiver12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE, .-_ZN2v88internal10JSReceiver12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEEbNS0_11ShouldThrowE
	.section	.text._ZN2v88internal8JSObject17SetImmutableProtoENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject17SetImmutableProtoENS0_6HandleIS1_EE
	.type	_ZN2v88internal8JSObject17SetImmutableProtoENS0_6HandleIS1_EE, @function
_ZN2v88internal8JSObject17SetImmutableProtoENS0_6HandleIS1_EE:
.LFB21538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	-1(%rax), %r13
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L4295
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	movq	%rax, %rsi
.L4296:
	testb	$2, 14(%r13)
	je	.L4310
.L4294:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4310:
	.cfi_restore_state
	movq	(%r12), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal3Map26TransitionToImmutableProtoEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%r12), %rdi
	movq	(%rax), %rdx
	movq	%rdx, -1(%rdi)
	testq	%rdx, %rdx
	je	.L4294
	testb	$1, %dl
	je	.L4294
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L4294
	addq	$8, %rsp
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L4295:
	.cfi_restore_state
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L4311
.L4297:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r13, (%rsi)
	jmp	.L4296
	.p2align 4,,10
	.p2align 3
.L4311:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4297
	.cfi_endproc
.LFE21538:
	.size	_ZN2v88internal8JSObject17SetImmutableProtoENS0_6HandleIS1_EE, .-_ZN2v88internal8JSObject17SetImmutableProtoENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal8JSObject16ValidateElementsES1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject16ValidateElementsES1_
	.type	_ZN2v88internal8JSObject16ValidateElementsES1_, @function
_ZN2v88internal8JSObject16ValidateElementsES1_:
.LFB21540:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21540:
	.size	_ZN2v88internal8JSObject16ValidateElementsES1_, .-_ZN2v88internal8JSObject16ValidateElementsES1_
	.section	.text.unlikely._ZN2v88internal8JSObject20GetFastElementsUsageEv,"ax",@progbits
	.align 2
.LCOLDB97:
	.section	.text._ZN2v88internal8JSObject20GetFastElementsUsageEv,"ax",@progbits
.LHOTB97:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject20GetFastElementsUsageEv
	.type	_ZN2v88internal8JSObject20GetFastElementsUsageEv, @function
_ZN2v88internal8JSObject20GetFastElementsUsageEv:
.LFB21549:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	movq	15(%rcx), %r8
	movq	-1(%rcx), %rax
	movzbl	14(%rax), %eax
	movl	%eax, %edx
	shrl	$3, %edx
	cmpl	$231, %eax
	ja	.L4332
	leaq	.L4316(%rip), %rsi
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8JSObject20GetFastElementsUsageEv,"a",@progbits
	.align 4
	.align 4
.L4316:
	.long	.L4319-.L4316
	.long	.L4317-.L4316
	.long	.L4319-.L4316
	.long	.L4317-.L4316
	.long	.L4319-.L4316
	.long	.L4320-.L4316
	.long	.L4319-.L4316
	.long	.L4317-.L4316
	.long	.L4319-.L4316
	.long	.L4317-.L4316
	.long	.L4319-.L4316
	.long	.L4317-.L4316
	.long	.L4315-.L4316
	.long	.L4318-.L4316
	.long	.L4315-.L4316
	.long	.L4317-.L4316
	.long	.L4315-.L4316
	.long	.L4315-.L4316
	.long	.L4315-.L4316
	.long	.L4315-.L4316
	.long	.L4315-.L4316
	.long	.L4315-.L4316
	.long	.L4315-.L4316
	.long	.L4315-.L4316
	.long	.L4315-.L4316
	.long	.L4315-.L4316
	.long	.L4315-.L4316
	.long	.L4315-.L4316
	.long	.L4315-.L4316
	.section	.text._ZN2v88internal8JSObject20GetFastElementsUsageEv
	.p2align 4,,10
	.p2align 3
.L4318:
	movq	23(%r8), %r8
.L4317:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rcx
	movq	-1(%rax), %rdx
	subq	$37592, %rcx
	cmpw	$1061, 11(%rdx)
	je	.L4341
	movl	11(%r8), %eax
.L4323:
	testl	%eax, %eax
	jle	.L4335
	subl	$1, %eax
	leaq	15(%r8), %rdx
	leaq	23(%r8,%rax,8), %rdi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L4326:
	movq	(%rdx), %rsi
	cmpq	96(%rcx), %rsi
	setne	%sil
	addq	$8, %rdx
	movzbl	%sil, %esi
	addl	%esi, %eax
	cmpq	%rdx, %rdi
	jne	.L4326
	ret
	.p2align 4,,10
	.p2align 3
.L4319:
	movq	(%rdi), %rax
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	je	.L4342
	movl	11(%r8), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4320:
	movq	15(%rcx), %rdx
	xorl	%eax, %eax
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L4313
	movq	-1(%rcx), %rax
	cmpw	$1061, 11(%rax)
	je	.L4343
	movl	11(%r8), %eax
.L4328:
	testl	%eax, %eax
	jle	.L4335
	subl	$1, %eax
	leaq	15(%r8), %rdx
	movabsq	$-2251799814209537, %rsi
	leaq	23(%r8,%rax,8), %rdi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L4331:
	xorl	%ecx, %ecx
	cmpq	%rsi, (%rdx)
	setne	%cl
	addq	$8, %rdx
	addl	%ecx, %eax
	cmpq	%rdi, %rdx
	jne	.L4331
	ret
	.p2align 4,,10
	.p2align 3
.L4313:
	ret
	.p2align 4,,10
	.p2align 3
.L4341:
	movl	27(%rax), %eax
	jmp	.L4323
	.p2align 4,,10
	.p2align 3
.L4342:
	movl	27(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4335:
	xorl	%eax, %eax
	ret
.L4315:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4343:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	27(%rcx), %eax
	jmp	.L4328
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal8JSObject20GetFastElementsUsageEv
	.cfi_startproc
	.type	_ZN2v88internal8JSObject20GetFastElementsUsageEv.cold, @function
_ZN2v88internal8JSObject20GetFastElementsUsageEv.cold:
.LFSB21549:
.L4332:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE21549:
	.section	.text._ZN2v88internal8JSObject20GetFastElementsUsageEv
	.size	_ZN2v88internal8JSObject20GetFastElementsUsageEv, .-_ZN2v88internal8JSObject20GetFastElementsUsageEv
	.section	.text.unlikely._ZN2v88internal8JSObject20GetFastElementsUsageEv
	.size	_ZN2v88internal8JSObject20GetFastElementsUsageEv.cold, .-_ZN2v88internal8JSObject20GetFastElementsUsageEv.cold
.LCOLDE97:
	.section	.text._ZN2v88internal8JSObject20GetFastElementsUsageEv
.LHOTE97:
	.section	.rodata._ZN2v88internal8JSObject14AddDataElementENS0_6HandleIS1_EEjNS2_INS0_6ObjectEEENS0_18PropertyAttributesE.str1.8,"aMS",@progbits,1
	.align 8
.LC99:
	.string	"JSArray::cast(*object).length().ToArrayLength(&old_length)"
	.section	.text._ZN2v88internal8JSObject14AddDataElementENS0_6HandleIS1_EEjNS2_INS0_6ObjectEEENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject14AddDataElementENS0_6HandleIS1_EEjNS2_INS0_6ObjectEEENS0_18PropertyAttributesE
	.type	_ZN2v88internal8JSObject14AddDataElementENS0_6HandleIS1_EEjNS2_INS0_6ObjectEEENS0_18PropertyAttributesE, @function
_ZN2v88internal8JSObject14AddDataElementENS0_6HandleIS1_EEjNS2_INS0_6ObjectEEENS0_18PropertyAttributesE:
.LFB21545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	xorl	%edx, %edx
	movq	%rbx, -88(%rbp)
	movq	-1(%rax), %rcx
	movb	$1, -73(%rbp)
	cmpw	$1061, 11(%rcx)
	je	.L4453
.L4349:
	movq	-1(%rax), %rcx
	movq	15(%rax), %rsi
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	leal	-13(%rcx), %edi
	cmpb	$1, %dil
	jbe	.L4454
	leal	-15(%rcx), %edi
	cmpb	$2, %dil
	sbbl	%ebx, %ebx
	xorl	%r9d, %r9d
	andl	$4, %ebx
	addl	$12, %ebx
	testl	%r8d, %r8d
	je	.L4455
.L4357:
	movq	(%r14), %rcx
	testb	$1, %cl
	je	.L4384
	movq	-1(%rcx), %rcx
	cmpw	$65, 11(%rcx)
	je	.L4382
.L4444:
	movq	-1(%rax), %rax
	cmpw	$1061, 11(%rax)
	jne	.L4402
	cmpl	%edx, %r13d
	jbe	.L4456
.L4402:
	movl	$3, %esi
	movl	$3, %r15d
	testb	%bl, %bl
	jne	.L4383
.L4412:
	movl	$1, %edi
	movl	$1, %ebx
	jmp	.L4387
.L4462:
	movq	(%r12), %rax
	.p2align 4,,10
	.p2align 3
.L4384:
	movq	-1(%rax), %rax
	movl	$1, %esi
	movl	$1, %r15d
	cmpw	$1061, 11(%rax)
	je	.L4457
.L4386:
	testb	%bl, %bl
	je	.L4412
.L4383:
	cmpb	$4, %bl
	je	.L4413
	cmpb	$2, %bl
	je	.L4414
	cmpb	$6, %bl
	je	.L4415
.L4451:
	movzbl	%bl, %edi
.L4387:
	movl	%r8d, -72(%rbp)
	movl	%r9d, -68(%rbp)
	call	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
	movl	-68(%rbp), %r9d
	movl	-72(%rbp), %r8d
	movq	%r14, %rcx
	testb	%al, %al
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rax
	movl	%r13d, %edx
	movq	%r12, %rsi
	cmovne	%r15d, %ebx
	movzbl	%bl, %ebx
	movq	(%rax,%rbx,8), %rdi
	movq	(%rdi), %rax
	call	*136(%rax)
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	cmpw	$1061, 11(%rax)
	jne	.L4344
	cmpb	$0, -73(%rbp)
	jne	.L4458
.L4344:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4459
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4454:
	.cfi_restore_state
	movq	23(%rsi), %rsi
	movl	$14, %ebx
	xorl	%r9d, %r9d
	testl	%r8d, %r8d
	jne	.L4357
.L4455:
	movq	-1(%rsi), %rdi
	cmpw	$132, 11(%rdi)
	je	.L4460
	movslq	11(%rsi), %rsi
	movq	%rax, -64(%rbp)
	movl	%esi, %r9d
	cmpl	%r13d, %esi
	ja	.L4443
	movl	%r13d, %edi
	subl	%esi, %edi
	cmpl	$1023, %edi
	ja	.L4452
	leal	1(%r13), %esi
	shrl	%esi
	leal	17(%r13,%rsi), %r9d
	cmpl	$500, %r9d
	jbe	.L4443
	cmpl	$5000, %r9d
	ja	.L4377
	testb	$1, %al
	je	.L4377
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4443
.L4377:
	leaq	-64(%rbp), %rdi
	movl	%r8d, -92(%rbp)
	movl	%edx, -80(%rbp)
	movl	%r9d, -72(%rbp)
	movl	%ecx, -68(%rbp)
	call	_ZN2v88internal8JSObject20GetFastElementsUsageEv
	movl	%eax, %esi
	sarl	%esi
	leal	(%rsi,%rax), %edi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo32Ej@PLT
	movl	$4, %esi
	movl	-68(%rbp), %ecx
	movl	-80(%rbp), %edx
	cmpl	$4, %eax
	movl	-72(%rbp), %r9d
	movl	-92(%rbp), %r8d
	cmovl	%esi, %eax
	leal	(%rax,%rax,8), %eax
	cmpl	%eax, %r9d
	jb	.L4443
	movq	(%r12), %rax
	jmp	.L4357
	.p2align 4,,10
	.p2align 3
.L4453:
	movq	23(%rax), %rdx
	testb	$1, %dl
	jne	.L4346
	sarq	$32, %rdx
	js	.L4348
	cmpl	%edx, %esi
	setnb	-73(%rbp)
	jmp	.L4349
	.p2align 4,,10
	.p2align 3
.L4443:
	cmpb	$5, %cl
	movl	%ecx, %ebx
	setbe	%sil
	andl	%ecx, %esi
.L4373:
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L4461
	testb	%sil, %sil
	je	.L4462
	movl	$1, %esi
	movl	$1, %r15d
	jmp	.L4383
	.p2align 4,,10
	.p2align 3
.L4457:
	cmpl	%edx, %r13d
	ja	.L4386
	xorl	%esi, %esi
	xorl	%r15d, %r15d
	jmp	.L4451
	.p2align 4,,10
	.p2align 3
.L4460:
	movq	39(%rsi), %rcx
	leaq	39(%rsi), %rdi
	testb	$1, %cl
	jne	.L4359
	btq	$32, %rcx
	jc	.L4452
.L4359:
	cmpl	$2147483646, %r13d
	jbe	.L4463
.L4452:
	xorl	%r9d, %r9d
	jmp	.L4357
.L4466:
	movq	(%r12), %rax
	.p2align 4,,10
	.p2align 3
.L4382:
	movq	-1(%rax), %rax
	cmpw	$1061, 11(%rax)
	jne	.L4403
	cmpl	%edx, %r13d
	ja	.L4403
	movl	$4, %esi
	movl	$4, %r15d
	jmp	.L4451
	.p2align 4,,10
	.p2align 3
.L4346:
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	je	.L4464
.L4348:
	leaq	.LC99(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4458:
	movq	-88(%rbp), %rdi
	leal	1(%r13), %esi
	xorl	%edx, %edx
	subq	$37592, %rdi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	(%r12), %r12
	movq	(%rax), %r13
	leaq	23(%r12), %r14
	movq	%r12, %rdi
	movq	%r13, 23(%r12)
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	je	.L4344
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	jmp	.L4344
	.p2align 4,,10
	.p2align 3
.L4413:
	movl	$5, %edi
	movl	$5, %ebx
	jmp	.L4387
	.p2align 4,,10
	.p2align 3
.L4414:
	movl	$3, %edi
	movl	$3, %ebx
	jmp	.L4387
	.p2align 4,,10
	.p2align 3
.L4415:
	movl	$7, %edi
	movl	$7, %ebx
	jmp	.L4387
	.p2align 4,,10
	.p2align 3
.L4463:
	movq	-1(%rax), %rcx
	cmpw	$1061, 11(%rcx)
	jne	.L4361
	movq	23(%rax), %r9
	testb	$1, %r9b
	jne	.L4452
	shrq	$32, %r9
.L4362:
	leal	1(%r13), %ecx
	cmpl	%r9d, %ecx
	cmovnb	%ecx, %r9d
	movslq	35(%rsi), %rcx
	leal	(%rcx,%rcx,2), %ecx
	addl	%ecx, %ecx
	cmpl	%ecx, %r9d
	ja	.L4357
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	subw	$1041, %cx
	cmpw	$20, %cx
	ja	.L4364
	movl	$1179649, %esi
	shrq	%cl, %rsi
	notq	%rsi
	andl	$1, %esi
	jne	.L4364
	movq	-1(%rax), %rcx
	movl	$13, %ebx
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	subl	$13, %ecx
	cmpb	$1, %cl
	jbe	.L4373
	movq	-1(%rax), %rcx
	movl	$15, %ebx
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	subl	$15, %ecx
	cmpb	$1, %cl
	jbe	.L4373
	movq	15(%rax), %rax
	movl	35(%rax), %ecx
	leaq	31(%rax), %rdi
	testl	%ecx, %ecx
	jle	.L4410
	leaq	-1(%rax), %r11
	movl	$56, %ecx
	addq	$47, %rax
	xorl	%esi, %esi
	subl	%eax, %ecx
	movl	$1, %ebx
	movl	%ecx, %r10d
	.p2align 4,,10
	.p2align 3
.L4372:
	movq	(%rax), %rcx
	testb	$1, %cl
	je	.L4370
	movq	-1(%rcx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L4368
.L4370:
	leal	(%r10,%rax), %ecx
	movslq	%ecx, %rcx
	movq	(%rcx,%r11), %rcx
	testb	$1, %cl
	je	.L4368
	movq	-1(%rcx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L4364
	cmpb	$0, _ZN2v88internal24FLAG_unbox_double_arraysE(%rip)
	je	.L4364
	movl	$5, %ebx
.L4368:
	addl	$1, %esi
	addq	$24, %rax
	cmpl	%esi, 4(%rdi)
	jg	.L4372
	movl	%ebx, %esi
	andl	$1, %esi
	jmp	.L4373
	.p2align 4,,10
	.p2align 3
.L4403:
	movl	$5, %esi
	movl	$5, %r15d
	jmp	.L4386
	.p2align 4,,10
	.p2align 3
.L4464:
	movsd	7(%rdx), %xmm0
	movsd	.LC98(%rip), %xmm2
	addsd	%xmm0, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rcx
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L4348
	movl	%ecx, %esi
	pxor	%xmm1, %xmm1
	movd	%xmm2, %edx
	cvtsi2sdq	%rsi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L4348
	jne	.L4348
	cmpl	%ecx, %r13d
	setnb	-73(%rbp)
	jmp	.L4349
	.p2align 4,,10
	.p2align 3
.L4461:
	movq	-1(%rax), %rax
	cmpw	$65, 11(%rax)
	jne	.L4465
	testb	%sil, %sil
	je	.L4466
	movl	$5, %esi
	movl	$5, %r15d
	jmp	.L4383
.L4465:
	testb	%sil, %sil
	jne	.L4380
	movq	(%r12), %rax
	jmp	.L4444
	.p2align 4,,10
	.p2align 3
.L4380:
	movl	$3, %esi
	movl	$3, %r15d
	jmp	.L4383
	.p2align 4,,10
	.p2align 3
.L4361:
	movq	-1(%rax), %rcx
	cmpw	$1058, 11(%rcx)
	je	.L4452
	movq	(%rdi), %rcx
	movl	$1, %r9d
	testb	$1, %cl
	jne	.L4362
	shrq	$33, %rcx
	leal	1(%rcx), %r9d
	jmp	.L4362
	.p2align 4,,10
	.p2align 3
.L4364:
	movl	$1, %esi
	movl	$3, %ebx
	jmp	.L4373
.L4410:
	movl	$1, %esi
	movl	$1, %ebx
	jmp	.L4373
.L4459:
	call	__stack_chk_fail@PLT
.L4456:
	movl	$2, %esi
	movl	$2, %r15d
	jmp	.L4451
	.cfi_endproc
.LFE21545:
	.size	_ZN2v88internal8JSObject14AddDataElementENS0_6HandleIS1_EEjNS2_INS0_6ObjectEEENS0_18PropertyAttributesE, .-_ZN2v88internal8JSObject14AddDataElementENS0_6HandleIS1_EEjNS2_INS0_6ObjectEEENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal8JSObject26WouldConvertToSlowElementsEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject26WouldConvertToSlowElementsEj
	.type	_ZN2v88internal8JSObject26WouldConvertToSlowElementsEj, @function
_ZN2v88internal8JSObject26WouldConvertToSlowElementsEj:
.LFB21541:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-1(%rdx), %rcx
	cmpb	$47, 14(%rcx)
	jbe	.L4477
.L4467:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L4478
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4477:
	.cfi_restore_state
	movq	15(%rdx), %rcx
	movslq	11(%rcx), %rcx
	movq	%rdx, -32(%rbp)
	cmpl	%esi, %ecx
	ja	.L4467
	movl	%esi, %eax
	subl	%ecx, %eax
	movl	%eax, %ecx
	movl	$1, %eax
	cmpl	$1023, %ecx
	ja	.L4467
	leal	1(%rsi), %eax
	shrl	%eax
	leal	17(%rsi,%rax), %ebx
	xorl	%eax, %eax
	cmpl	$500, %ebx
	jbe	.L4467
	cmpl	$5000, %ebx
	ja	.L4470
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	jne	.L4470
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L4467
.L4470:
	leaq	-32(%rbp), %rdi
	call	_ZN2v88internal8JSObject20GetFastElementsUsageEv
	movl	%eax, %edx
	sarl	%edx
	leal	(%rdx,%rax), %edi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo32Ej@PLT
	movl	$4, %edx
	cmpl	$4, %eax
	cmovl	%edx, %eax
	leal	(%rax,%rax,8), %eax
	cmpl	%eax, %ebx
	setnb	%al
	jmp	.L4467
.L4478:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21541:
	.size	_ZN2v88internal8JSObject26WouldConvertToSlowElementsEj, .-_ZN2v88internal8JSObject26WouldConvertToSlowElementsEj
	.section	.text._ZN2v88internal8JSObject26GetPropertyWithInterceptorEPNS0_14LookupIteratorEPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject26GetPropertyWithInterceptorEPNS0_14LookupIteratorEPb
	.type	_ZN2v88internal8JSObject26GetPropertyWithInterceptorEPNS0_14LookupIteratorEPb, @function
_ZN2v88internal8JSObject26GetPropertyWithInterceptorEPNS0_14LookupIteratorEPb:
.LFB21550:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZNK2v88internal14LookupIterator14GetInterceptorEv
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12_GLOBAL__N_134GetPropertyWithInterceptorInternalEPNS0_14LookupIteratorENS0_6HandleINS0_15InterceptorInfoEEEPb
	.cfi_endproc
.LFE21550:
	.size	_ZN2v88internal8JSObject26GetPropertyWithInterceptorEPNS0_14LookupIteratorEPb, .-_ZN2v88internal8JSObject26GetPropertyWithInterceptorEPNS0_14LookupIteratorEPb
	.section	.text._ZN2v88internal8JSObject20HasRealNamedPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject20HasRealNamedPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE
	.type	_ZN2v88internal8JSObject20HasRealNamedPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE, @function
_ZN2v88internal8JSObject20HasRealNamedPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE:
.LFB21551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %r13
	movq	(%rsi), %rdx
	movq	-1(%rdx), %rcx
	subq	$37592, %r13
	cmpw	$63, 11(%rcx)
	jbe	.L4505
.L4483:
	testb	$1, %al
	jne	.L4490
.L4492:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %r14
.L4491:
	movabsq	$824633720832, %rcx
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	movq	%rcx, -212(%rbp)
	movl	$0, -224(%rbp)
	movq	%r13, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L4506
.L4493:
	leaq	-224(%rbp), %r12
	movq	%r15, -192(%rbp)
	movq	%r12, %rdi
	movq	$0, -184(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%r14, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L4489:
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4507
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4505:
	.cfi_restore_state
	movq	%rdx, -144(%rbp)
	movl	7(%rdx), %edx
	testb	$1, %dl
	jne	.L4484
	andl	$2, %edx
	jne	.L4483
.L4484:
	leaq	-144(%rbp), %r8
	leaq	-228(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-248(%rbp), %r8
	testb	%al, %al
	je	.L4508
	movq	(%rbx), %rax
	movl	-228(%rbp), %r14d
	testb	$1, %al
	jne	.L4486
.L4488:
	movl	%r14d, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -248(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-248(%rbp), %r8
.L4487:
	movabsq	$824633720832, %rcx
	movq	%r8, %rdi
	movq	%r13, -120(%rbp)
	movl	$0, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%r14d, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r12, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	leaq	-224(%rbp), %r12
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L4489
	.p2align 4,,10
	.p2align 3
.L4490:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L4492
	movq	%rbx, %r14
	jmp	.L4491
	.p2align 4,,10
	.p2align 3
.L4508:
	movq	(%rbx), %rax
	jmp	.L4483
	.p2align 4,,10
	.p2align 3
.L4486:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L4488
	movq	%rbx, %rax
	jmp	.L4487
	.p2align 4,,10
	.p2align 3
.L4506:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r15
	jmp	.L4493
.L4507:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21551:
	.size	_ZN2v88internal8JSObject20HasRealNamedPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE, .-_ZN2v88internal8JSObject20HasRealNamedPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE
	.section	.text._ZN2v88internal8JSObject22HasRealElementPropertyENS0_6HandleIS1_EEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject22HasRealElementPropertyENS0_6HandleIS1_EEj
	.type	_ZN2v88internal8JSObject22HasRealElementPropertyENS0_6HandleIS1_EEj, @function
_ZN2v88internal8JSObject22HasRealElementPropertyENS0_6HandleIS1_EEj:
.LFB21552:
	.cfi_startproc
	endbr64
	movabsq	$824633720832, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-112(%rbp), %r12
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	%rdi, -64(%rbp)
	movq	%rdi, -48(%rbp)
	movq	%r12, %rdi
	subq	$37592, %rax
	movq	%rdx, -100(%rbp)
	movq	%rax, -88(%rbp)
	movl	%esi, -40(%rbp)
	movl	$0, -112(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -56(%rbp)
	movl	$-1, -36(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4512
	addq	$104, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4512:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21552:
	.size	_ZN2v88internal8JSObject22HasRealElementPropertyENS0_6HandleIS1_EEj, .-_ZN2v88internal8JSObject22HasRealElementPropertyENS0_6HandleIS1_EEj
	.section	.text._ZN2v88internal8JSObject28HasRealNamedCallbackPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject28HasRealNamedCallbackPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE
	.type	_ZN2v88internal8JSObject28HasRealNamedCallbackPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE, @function
_ZN2v88internal8JSObject28HasRealNamedCallbackPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE:
.LFB21553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %r13
	movq	(%rsi), %rdx
	movq	-1(%rdx), %rcx
	subq	$37592, %r13
	cmpw	$63, 11(%rcx)
	jbe	.L4539
.L4515:
	testb	$1, %al
	jne	.L4522
.L4524:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rdx
.L4523:
	movq	(%r12), %rax
	movq	-1(%rax), %rcx
	movq	%r13, -200(%rbp)
	movabsq	$824633720832, %rcx
	movl	$0, -224(%rbp)
	movq	%rcx, -212(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L4540
.L4525:
	leaq	-224(%rbp), %r12
	movq	%r15, -192(%rbp)
	movq	%r12, %rdi
	movq	$0, -184(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L4521:
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE
	xorl	%edx, %edx
	testb	%al, %al
	je	.L4526
	cmpl	$5, -220(%rbp)
	movl	$1, %edx
	sete	-242(%rbp)
.L4526:
	xorl	%eax, %eax
	movzwl	-242(%rbp), %ebx
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	movb	%bl, %ah
	movb	%dl, %al
	jne	.L4541
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4539:
	.cfi_restore_state
	movq	%rdx, -144(%rbp)
	movl	7(%rdx), %edx
	testb	$1, %dl
	jne	.L4516
	andl	$2, %edx
	jne	.L4515
.L4516:
	leaq	-144(%rbp), %r14
	leaq	-228(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L4542
	movq	(%rbx), %rax
	movl	-228(%rbp), %r15d
	testb	$1, %al
	jne	.L4518
.L4520:
	movl	%r15d, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
.L4519:
	movabsq	$824633720832, %rcx
	movq	%r14, %rdi
	movq	%r13, -120(%rbp)
	movl	$0, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%r15d, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r12, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	leaq	-224(%rbp), %r12
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L4521
	.p2align 4,,10
	.p2align 3
.L4522:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L4524
	movq	%rbx, %rdx
	jmp	.L4523
	.p2align 4,,10
	.p2align 3
.L4542:
	movq	(%rbx), %rax
	jmp	.L4515
	.p2align 4,,10
	.p2align 3
.L4518:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L4520
	movq	%rbx, %rax
	jmp	.L4519
	.p2align 4,,10
	.p2align 3
.L4540:
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rdx, -256(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-256(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L4525
.L4541:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21553:
	.size	_ZN2v88internal8JSObject28HasRealNamedCallbackPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE, .-_ZN2v88internal8JSObject28HasRealNamedCallbackPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE
	.section	.text._ZN2v88internal8JSObject12IsApiWrapperEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject12IsApiWrapperEv
	.type	_ZN2v88internal8JSObject12IsApiWrapperEv, @function
_ZN2v88internal8JSObject12IsApiWrapperEv:
.LFB21554:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %ecx
	xorl	%eax, %eax
	leal	-1025(%rcx), %edx
	cmpw	$62, %dx
	ja	.L4543
	movabsq	$4611686037754740737, %rsi
	movl	$1, %eax
	btq	%rdx, %rsi
	jc	.L4543
	subw	$1026, %cx
	cmpw	$60, %cx
	ja	.L4548
	movabsq	$1152921504606863361, %rax
	shrq	%cl, %rax
	andl	$1, %eax
	ret
.L4548:
	xorl	%eax, %eax
.L4543:
	ret
	.cfi_endproc
.LFE21554:
	.size	_ZN2v88internal8JSObject12IsApiWrapperEv, .-_ZN2v88internal8JSObject12IsApiWrapperEv
	.section	.text._ZN2v88internal8JSObject21IsDroppableApiWrapperEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject21IsDroppableApiWrapperEv
	.type	_ZN2v88internal8JSObject21IsDroppableApiWrapperEv, @function
_ZN2v88internal8JSObject21IsDroppableApiWrapperEv:
.LFB21555:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subw	$1040, %ax
	testw	$-17, %ax
	sete	%al
	ret
	.cfi_endproc
.LFE21555:
	.size	_ZN2v88internal8JSObject21IsDroppableApiWrapperEv, .-_ZN2v88internal8JSObject21IsDroppableApiWrapperEv
	.section	.text._ZN2v88internal15JSBoundFunction9GetLengthEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JSBoundFunction9GetLengthEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal15JSBoundFunction9GetLengthEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal15JSBoundFunction9GetLengthEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB21561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$2147483647, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	(%rsi), %rsi
	movq	39(%rsi), %rdx
	movl	11(%rdx), %ebx
	jmp	.L4552
	.p2align 4,,10
	.p2align 3
.L4563:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L4555:
	movq	39(%rsi), %rdx
	movl	%r13d, %ecx
	subl	%ebx, %ecx
	movslq	11(%rdx), %rdx
	addl	%edx, %ebx
	cmpl	%edx, %ecx
	cmovle	%r13d, %ebx
.L4552:
	movq	23(%rsi), %rdx
	movq	-1(%rdx), %rdx
	movq	(%rax), %rax
	movq	41112(%r12), %rdi
	cmpw	$1104, 11(%rdx)
	movq	23(%rax), %rsi
	jne	.L4553
	testq	%rdi, %rdi
	jne	.L4563
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L4564
.L4556:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L4555
	.p2align 4,,10
	.p2align 3
.L4553:
	testq	%rdi, %rdi
	je	.L4558
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L4559:
	movq	23(%rsi), %rax
	movl	$0, %edx
	movzwl	39(%rax), %eax
	subl	%ebx, %eax
	cmovs	%edx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	salq	$32, %rax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	orq	$1, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4564:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L4556
	.p2align 4,,10
	.p2align 3
.L4558:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L4565
.L4560:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L4559
.L4565:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L4560
	.cfi_endproc
.LFE21561:
	.size	_ZN2v88internal15JSBoundFunction9GetLengthEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal15JSBoundFunction9GetLengthEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15JSBoundFunction8ToStringENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JSBoundFunction8ToStringENS0_6HandleIS1_EE
	.type	_ZN2v88internal15JSBoundFunction8ToStringENS0_6HandleIS1_EE, @function
_ZN2v88internal15JSBoundFunction8ToStringENS0_6HandleIS1_EE:
.LFB21562:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$35024, %rax
	ret
	.cfi_endproc
.LFE21562:
	.size	_ZN2v88internal15JSBoundFunction8ToStringENS0_6HandleIS1_EE, .-_ZN2v88internal15JSBoundFunction8ToStringENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal10JSFunction7GetNameEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction7GetNameEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal10JSFunction7GetNameEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal10JSFunction7GetNameEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB21563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andl	$67108864, %edx
	je	.L4568
	leaq	2016(%rdi), %rax
.L4569:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4581
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4568:
	.cfi_restore_state
	movq	23(%rax), %r12
	movq	15(%r12), %rax
	testb	$1, %al
	jne	.L4582
.L4570:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L4571:
	testb	%al, %al
	je	.L4575
	movq	15(%r12), %r13
	testb	$1, %r13b
	jne	.L4583
.L4573:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4576
.L4585:
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L4569
	.p2align 4,,10
	.p2align 3
.L4576:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L4584
.L4578:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r13, (%rax)
	jmp	.L4569
	.p2align 4,,10
	.p2align 3
.L4583:
	movq	-1(%r13), %rax
	cmpw	$136, 11(%rax)
	jne	.L4573
	leaq	-48(%rbp), %r14
	movq	%r13, -48(%rbp)
	movq	%r14, %rdi
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	testb	%al, %al
	je	.L4575
	movq	%r14, %rdi
	movq	%r13, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	%rax, %r13
	jmp	.L4573
	.p2align 4,,10
	.p2align 3
.L4575:
	andq	$-262144, %r12
	movq	41112(%rbx), %rdi
	movq	24(%r12), %rax
	movq	-37464(%rax), %r13
	testq	%rdi, %rdi
	jne	.L4585
	jmp	.L4576
	.p2align 4,,10
	.p2align 3
.L4582:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L4570
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	jmp	.L4571
	.p2align 4,,10
	.p2align 3
.L4584:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L4578
.L4581:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21563:
	.size	_ZN2v88internal10JSFunction7GetNameEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal10JSFunction7GetNameEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15JSBoundFunction7GetNameEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JSBoundFunction7GetNameEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal15JSBoundFunction7GetNameEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal15JSBoundFunction7GetNameEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB21557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	2184(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%r13, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	(%rsi), %rsi
.L4593:
	movq	23(%rsi), %rax
	movq	-1(%rax), %rax
	cmpw	$1104, 11(%rax)
	jne	.L4587
.L4603:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4602
	movq	(%rbx), %rax
	movq	41112(%r14), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L4590
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
	movq	23(%rsi), %rax
	movq	-1(%rax), %rax
	cmpw	$1104, 11(%rax)
	je	.L4603
.L4587:
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	movq	%r12, %rax
	movq	-1(%rsi), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L4589
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4595
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L4596:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal10JSFunction7GetNameEPNS0_7IsolateENS0_6HandleIS1_EE
	movq	%rax, %rdx
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L4604
.L4598:
	movq	%r12, %rax
	jmp	.L4589
	.p2align 4,,10
	.p2align 3
.L4590:
	movq	41088(%r14), %rbx
	cmpq	41096(%r14), %rbx
	je	.L4605
.L4592:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rbx)
	jmp	.L4593
	.p2align 4,,10
	.p2align 3
.L4602:
	xorl	%eax, %eax
.L4589:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4605:
	.cfi_restore_state
	movq	%r14, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L4592
.L4595:
	movq	41088(%r14), %r8
	cmpq	41096(%r14), %r8
	je	.L4606
.L4597:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r8)
	jmp	.L4596
.L4604:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L4598
	addq	$16, %rsp
	movq	%r12, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_@PLT
.L4606:
	.cfi_restore_state
	movq	%r14, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L4597
	.cfi_endproc
.LFE21557:
	.size	_ZN2v88internal15JSBoundFunction7GetNameEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal15JSBoundFunction7GetNameEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal10JSFunction16GetFunctionRealmENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction16GetFunctionRealmENS0_6HandleIS1_EE
	.type	_ZN2v88internal10JSFunction16GetFunctionRealmENS0_6HandleIS1_EE, @function
_ZN2v88internal10JSFunction16GetFunctionRealmENS0_6HandleIS1_EE:
.LFB21564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	%rax, %rdx
	movq	31(%rax), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	39(%rax), %rsi
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L4608
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4608:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L4612
.L4610:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4612:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L4610
	.cfi_endproc
.LFE21564:
	.size	_ZN2v88internal10JSFunction16GetFunctionRealmENS0_6HandleIS1_EE, .-_ZN2v88internal10JSFunction16GetFunctionRealmENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal10JSReceiver16GetFunctionRealmENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver16GetFunctionRealmENS0_6HandleIS1_EE
	.type	_ZN2v88internal10JSReceiver16GetFunctionRealmENS0_6HandleIS1_EE, @function
_ZN2v88internal10JSReceiver16GetFunctionRealmENS0_6HandleIS1_EE:
.LFB21417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L4623
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L4624
	movq	-1(%rax), %rdx
	cmpw	$1104, 11(%rdx)
	je	.L4625
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal10JSReceiver18GetCreationContextEv
.L4615:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4626
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4624:
	.cfi_restore_state
	call	_ZN2v88internal10JSFunction16GetFunctionRealmENS0_6HandleIS1_EE
	jmp	.L4615
	.p2align 4,,10
	.p2align 3
.L4623:
	call	_ZN2v88internal7JSProxy16GetFunctionRealmENS0_6HandleIS1_EE@PLT
	jmp	.L4615
	.p2align 4,,10
	.p2align 3
.L4625:
	movq	%rax, %rdx
	movq	23(%rax), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L4618
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L4619:
	call	_ZN2v88internal10JSReceiver16GetFunctionRealmENS0_6HandleIS1_EE
	jmp	.L4615
	.p2align 4,,10
	.p2align 3
.L4618:
	movq	41088(%rbx), %rdi
	cmpq	41096(%rbx), %rdi
	je	.L4627
.L4620:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L4619
	.p2align 4,,10
	.p2align 3
.L4627:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L4620
.L4626:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21417:
	.size	_ZN2v88internal10JSReceiver16GetFunctionRealmENS0_6HandleIS1_EE, .-_ZN2v88internal10JSReceiver16GetFunctionRealmENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15JSBoundFunction16GetFunctionRealmENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JSBoundFunction16GetFunctionRealmENS0_6HandleIS1_EE
	.type	_ZN2v88internal15JSBoundFunction16GetFunctionRealmENS0_6HandleIS1_EE, @function
_ZN2v88internal15JSBoundFunction16GetFunctionRealmENS0_6HandleIS1_EE:
.LFB21556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	%rax, %rdx
	movq	23(%rax), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L4629
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	movq	%rax, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10JSReceiver16GetFunctionRealmENS0_6HandleIS1_EE
	.p2align 4,,10
	.p2align 3
.L4629:
	.cfi_restore_state
	movq	41088(%rbx), %rdi
	cmpq	41096(%rbx), %rdi
	je	.L4633
.L4631:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10JSReceiver16GetFunctionRealmENS0_6HandleIS1_EE
	.p2align 4,,10
	.p2align 3
.L4633:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L4631
	.cfi_endproc
.LFE21556:
	.size	_ZN2v88internal15JSBoundFunction16GetFunctionRealmENS0_6HandleIS1_EE, .-_ZN2v88internal15JSBoundFunction16GetFunctionRealmENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal10JSFunction19MarkForOptimizationENS0_15ConcurrencyModeE.str1.1,"aMS",@progbits,1
.LC100:
	.string	"  ** Not marking "
	.section	.rodata._ZN2v88internal10JSFunction19MarkForOptimizationENS0_15ConcurrencyModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC101:
	.string	" -- already in optimization queue.\n"
	.section	.rodata._ZN2v88internal10JSFunction19MarkForOptimizationENS0_15ConcurrencyModeE.str1.1
.LC102:
	.string	"  ** Marking "
	.section	.rodata._ZN2v88internal10JSFunction19MarkForOptimizationENS0_15ConcurrencyModeE.str1.8
	.align 8
.LC103:
	.string	" for concurrent recompilation.\n"
	.section	.text._ZN2v88internal10JSFunction19MarkForOptimizationENS0_15ConcurrencyModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction19MarkForOptimizationENS0_15ConcurrencyModeE
	.type	_ZN2v88internal10JSFunction19MarkForOptimizationENS0_15ConcurrencyModeE, @function
_ZN2v88internal10JSFunction19MarkForOptimizationENS0_15ConcurrencyModeE:
.LFB21565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	$0, 7824(%rdx)
	je	.L4648
	movq	3344(%rdx), %rdx
	movl	8(%rdx), %edx
	testl	%edx, %edx
	jne	.L4648
	cmpl	$1, %esi
	je	.L4661
.L4648:
	movl	$2, %esi
.L4635:
	movq	39(%rax), %rax
	leaq	-32(%rbp), %rdi
	movq	7(%rax), %rax
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal14FeedbackVector21SetOptimizationMarkerENS0_18OptimizationMarkerE@PLT
.L4634:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4662
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4661:
	.cfi_restore_state
	movq	23(%rax), %rax
	movq	%rdi, %r12
	movabsq	$287762808832, %rdx
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L4641
	testb	$1, %al
	jne	.L4638
.L4642:
	movq	(%r12), %rax
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	jne	.L4641
	movq	15(%rax), %rdx
	movzbl	_ZN2v88internal35FLAG_trace_concurrent_recompilationE(%rip), %eax
	testb	$1, %dl
	jne	.L4637
	sarq	$32, %rdx
	cmpq	$4, %rdx
	jne	.L4637
	testb	%al, %al
	je	.L4634
	xorl	%eax, %eax
	leaq	.LC100(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	stdout(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC101(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L4634
	.p2align 4,,10
	.p2align 3
.L4638:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L4641
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L4642
	.p2align 4,,10
	.p2align 3
.L4641:
	movzbl	_ZN2v88internal35FLAG_trace_concurrent_recompilationE(%rip), %eax
.L4637:
	testb	%al, %al
	jne	.L4645
.L4660:
	movq	(%r12), %rax
	movl	$3, %esi
	jmp	.L4635
	.p2align 4,,10
	.p2align 3
.L4645:
	xorl	%eax, %eax
	leaq	.LC102(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	stdout(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC103(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L4660
.L4662:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21565:
	.size	_ZN2v88internal10JSFunction19MarkForOptimizationENS0_15ConcurrencyModeE, .-_ZN2v88internal10JSFunction19MarkForOptimizationENS0_15ConcurrencyModeE
	.section	.text._ZN2v88internal10JSFunction30EnsureClosureFeedbackCellArrayENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction30EnsureClosureFeedbackCellArrayENS0_6HandleIS1_EE
	.type	_ZN2v88internal10JSFunction30EnsureClosureFeedbackCellArrayENS0_6HandleIS1_EE, @function
_ZN2v88internal10JSFunction30EnsureClosureFeedbackCellArrayENS0_6HandleIS1_EE:
.LFB21566:
	.cfi_startproc
	endbr64
	movabsq	$287762808832, %rsi
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rcx
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rsi, %rdx
	je	.L4669
	testb	$1, %dl
	jne	.L4667
.L4671:
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$125, 11(%rax)
	je	.L4663
.L4669:
	movabsq	$287762808832, %rsi
	movq	(%rbx), %rdx
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	cmpq	%rsi, %rax
	je	.L4675
	testb	$1, %al
	jne	.L4673
.L4676:
	movq	39(%rdx), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$155, 11(%rax)
	je	.L4663
.L4675:
	movq	(%rbx), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L4691
.L4677:
	movq	(%rbx), %rax
	leaq	-37592(%rcx), %r12
	movq	23(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4688
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L4683:
	movq	%r12, %rdi
	call	_ZN2v88internal24ClosureFeedbackCellArray3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movq	(%rbx), %rdx
	movq	39(%rdx), %r13
	cmpq	%r13, 4480(%r12)
	je	.L4692
	movq	(%rax), %r12
	leaq	7(%r13), %r14
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%r12, 7(%r13)
	movq	%r12, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	jne	.L4693
.L4663:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4667:
	.cfi_restore_state
	movq	-1(%rdx), %rsi
	cmpw	$165, 11(%rsi)
	je	.L4669
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L4671
	jmp	.L4669
	.p2align 4,,10
	.p2align 3
.L4673:
	movq	-1(%rax), %rsi
	cmpw	$165, 11(%rsi)
	je	.L4675
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L4676
	jmp	.L4675
	.p2align 4,,10
	.p2align 3
.L4688:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L4694
.L4684:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L4683
	.p2align 4,,10
	.p2align 3
.L4691:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L4677
	jmp	.L4663
	.p2align 4,,10
	.p2align 3
.L4693:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
.L4690:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L4694:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4684
	.p2align 4,,10
	.p2align 3
.L4692:
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory17NewOneClosureCellENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	(%rbx), %r12
	movq	(%rax), %r13
	leaq	39(%r12), %r14
	movq	%r12, %rdi
	movq	%r13, 39(%r12)
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	je	.L4663
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	jmp	.L4690
	.cfi_endproc
.LFE21566:
	.size	_ZN2v88internal10JSFunction30EnsureClosureFeedbackCellArrayENS0_6HandleIS1_EE, .-_ZN2v88internal10JSFunction30EnsureClosureFeedbackCellArrayENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal10JSFunction20EnsureFeedbackVectorENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction20EnsureFeedbackVectorENS0_6HandleIS1_EE
	.type	_ZN2v88internal10JSFunction20EnsureFeedbackVectorENS0_6HandleIS1_EE, @function
_ZN2v88internal10JSFunction20EnsureFeedbackVectorENS0_6HandleIS1_EE:
.LFB21567:
	.cfi_startproc
	endbr64
	movabsq	$287762808832, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %r12
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L4701
	testb	$1, %dl
	jne	.L4699
.L4703:
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$155, 11(%rax)
	je	.L4695
.L4701:
	movq	(%rbx), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L4722
.L4697:
	movq	(%rbx), %rax
	subq	$37592, %r12
	movq	23(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4720
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L4708:
	movq	%rbx, %rdi
	call	_ZN2v88internal10JSFunction30EnsureClosureFeedbackCellArrayENS0_6HandleIS1_EE
	movq	(%rbx), %rax
	movq	41112(%r12), %rdi
	movq	39(%rax), %rax
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L4710
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L4711:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14FeedbackVector3NewEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_24ClosureFeedbackCellArrayEEE@PLT
	movq	(%rbx), %rdx
	movq	(%rax), %r12
	movq	39(%rdx), %r13
	movq	%r12, %rdx
	movq	%r12, 7(%r13)
	leaq	7(%r13), %r14
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	je	.L4695
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4695
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4723
.L4695:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4699:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L4701
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L4703
	jmp	.L4701
	.p2align 4,,10
	.p2align 3
.L4720:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L4724
.L4709:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L4708
	.p2align 4,,10
	.p2align 3
.L4722:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L4697
	jmp	.L4695
	.p2align 4,,10
	.p2align 3
.L4710:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L4725
.L4712:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L4711
	.p2align 4,,10
	.p2align 3
.L4723:
	addq	$16, %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L4725:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L4712
	.p2align 4,,10
	.p2align 3
.L4724:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L4709
	.cfi_endproc
.LFE21567:
	.size	_ZN2v88internal10JSFunction20EnsureFeedbackVectorENS0_6HandleIS1_EE, .-_ZN2v88internal10JSFunction20EnsureFeedbackVectorENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal10JSFunction22InitializeFeedbackCellENS0_6HandleIS1_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC104:
	.string	"function->feedback_vector().length() == function->feedback_vector().metadata().slot_count()"
	.section	.text._ZN2v88internal10JSFunction22InitializeFeedbackCellENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction22InitializeFeedbackCellENS0_6HandleIS1_EE
	.type	_ZN2v88internal10JSFunction22InitializeFeedbackCellENS0_6HandleIS1_EE, @function
_ZN2v88internal10JSFunction22InitializeFeedbackCellENS0_6HandleIS1_EE:
.LFB21568:
	.cfi_startproc
	endbr64
	movabsq	$287762808832, %rsi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rcx
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rsi, %rdx
	je	.L4732
	testb	$1, %dl
	jne	.L4730
.L4734:
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$155, 11(%rax)
	je	.L4745
.L4732:
	movzbl	_ZN2v88internal24FLAG_log_function_eventsE(%rip), %eax
	movzbl	_ZN2v88internal29FLAG_lazy_feedback_allocationE(%rip), %edx
	xorl	$1, %edx
	testb	%al, %al
	cmove	%edx, %eax
	leaq	-37592(%rcx), %rdx
	movl	4240(%rcx), %ecx
	testl	%ecx, %ecx
	je	.L4746
.L4737:
	jmp	_ZN2v88internal10JSFunction20EnsureFeedbackVectorENS0_6HandleIS1_EE
	.p2align 4,,10
	.p2align 3
.L4745:
	movq	(%rdi), %rax
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	7(%rax), %rdx
	movl	31(%rax), %eax
	movq	23(%rdx), %rdx
	cmpl	%eax, 7(%rdx)
	jne	.L4747
	ret
	.p2align 4,,10
	.p2align 3
.L4746:
	cmpl	$1, 41836(%rdx)
	je	.L4737
	cmpb	$0, _ZN2v88internal15FLAG_always_optE(%rip)
	jne	.L4737
	testb	%al, %al
	jne	.L4737
	jmp	_ZN2v88internal10JSFunction30EnsureClosureFeedbackCellArrayENS0_6HandleIS1_EE
	.p2align 4,,10
	.p2align 3
.L4730:
	movq	-1(%rdx), %rsi
	cmpw	$165, 11(%rsi)
	je	.L4732
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L4734
	jmp	.L4732
	.p2align 4,,10
	.p2align 3
.L4747:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC104(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21568:
	.size	_ZN2v88internal10JSFunction22InitializeFeedbackCellENS0_6HandleIS1_EE, .-_ZN2v88internal10JSFunction22InitializeFeedbackCellENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal10JSFunction13SetInitialMapENS0_6HandleIS1_EENS2_INS0_3MapEEENS2_INS0_10HeapObjectEEE.str1.1,"aMS",@progbits,1
.LC105:
	.string	"InitialMap"
	.section	.text._ZN2v88internal10JSFunction13SetInitialMapENS0_6HandleIS1_EENS2_INS0_3MapEEENS2_INS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction13SetInitialMapENS0_6HandleIS1_EENS2_INS0_3MapEEENS2_INS0_10HeapObjectEEE
	.type	_ZN2v88internal10JSFunction13SetInitialMapENS0_6HandleIS1_EENS2_INS0_3MapEEENS2_INS0_10HeapObjectEEE, @function
_ZN2v88internal10JSFunction13SetInitialMapENS0_6HandleIS1_EENS2_INS0_3MapEEENS2_INS0_10HeapObjectEEE:
.LFB21571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %r14
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	23(%r14), %rax
	cmpq	%rax, (%rdx)
	je	.L4749
	andq	$-262144, %r13
	movl	$1, %ecx
	movq	24(%r13), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	(%rbx), %r14
	movq	(%r12), %r13
.L4749:
	movq	%r14, 55(%r13)
	leaq	55(%r13), %rsi
	testb	$1, %r14b
	je	.L4761
	movq	%r14, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L4779
	testb	$24, %al
	je	.L4761
.L4786:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4780
	.p2align 4,,10
	.p2align 3
.L4761:
	movq	(%rbx), %r13
	movq	(%r12), %r14
	movq	31(%r13), %rax
	leaq	31(%r13), %rsi
	testb	$1, %al
	jne	.L4781
.L4753:
	movq	%r14, 31(%r13)
	testb	$1, %r14b
	je	.L4760
	movq	%r14, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L4782
	testb	$24, %al
	je	.L4760
.L4787:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4783
	.p2align 4,,10
	.p2align 3
.L4760:
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	jne	.L4784
.L4748:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4785
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4779:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-72(%rbp), %rsi
	testb	$24, %al
	jne	.L4786
	jmp	.L4761
	.p2align 4,,10
	.p2align 3
.L4782:
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-72(%rbp), %rsi
	testb	$24, %al
	jne	.L4787
	jmp	.L4760
	.p2align 4,,10
	.p2align 3
.L4781:
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L4753
	leaq	.LC13(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4784:
	movq	(%r12), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	3424(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L4748
	movq	(%r12), %rax
	leaq	-64(%rbp), %rdi
	movq	23(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	(%rbx), %rcx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %r9
	leaq	.LC33(%rip), %r8
	leaq	.LC105(%rip), %rsi
	call	_ZN2v88internal6Logger8MapEventEPKcNS0_3MapES4_S3_NS0_10HeapObjectE@PLT
	jmp	.L4748
	.p2align 4,,10
	.p2align 3
.L4780:
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4761
	.p2align 4,,10
	.p2align 3
.L4783:
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4760
.L4785:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21571:
	.size	_ZN2v88internal10JSFunction13SetInitialMapENS0_6HandleIS1_EENS2_INS0_3MapEEENS2_INS0_10HeapObjectEEE, .-_ZN2v88internal10JSFunction13SetInitialMapENS0_6HandleIS1_EENS2_INS0_3MapEEENS2_INS0_10HeapObjectEEE
	.section	.rodata._ZN2v88internal10JSFunction12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC106:
	.string	"SetPrototype"
.LC107:
	.string	"SetInstancePrototype"
	.section	.text._ZN2v88internal10JSFunction12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal10JSFunction12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEE, @function
_ZN2v88internal10JSFunction12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEE:
.LFB21570:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %r12
	movq	(%rsi), %rdx
	subq	$37592, %r12
	testb	$1, %dl
	jne	.L4851
.L4790:
	movq	-1(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4793
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L4794:
	leaq	.LC106(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rbx, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, %rdx
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
	movq	0(%r13), %r15
	movq	(%r14), %r14
	movq	31(%r15), %rax
	leaq	31(%r15), %rsi
	testb	$1, %al
	jne	.L4852
.L4796:
	movq	%r14, 31(%r15)
	testb	$1, %r14b
	je	.L4832
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	je	.L4798
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	8(%rcx), %rax
.L4798:
	testb	$24, %al
	je	.L4832
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4853
	.p2align 4,,10
	.p2align 3
.L4832:
	movq	0(%r13), %rdx
	movzbl	13(%rdx), %eax
	orl	$1, %eax
	movb	%al, 13(%rdx)
	movq	(%rbx), %rdx
	movq	23(%rdx), %rax
	movl	47(%rax), %eax
	movl	%eax, %r13d
	movq	31(%rdx), %rax
	andl	$31, %r13d
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4800
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	leal	-12(%r13), %eax
	cmpb	$3, %al
	ja	.L4803
.L4860:
	leal	-9(%r13), %eax
	cmpb	$4, %al
	ja	.L4804
	movq	487(%rsi), %rsi
.L4805:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4806
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L4792:
	movq	(%rbx), %r14
	movq	55(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L4809
	movq	-1(%r14), %rdx
	movzbl	13(%rdx), %edx
	testb	%dl, %dl
	js	.L4810
	leaq	-64(%rbp), %rax
	movq	%rax, -72(%rbp)
.L4811:
	movq	41112(%r12), %rdi
	movq	55(%r14), %rsi
	testq	%rdi, %rdi
	je	.L4813
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L4814:
	movq	40936(%r12), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L4816
	movq	(%r15), %rax
	cmpw	$1057, 11(%rax)
	je	.L4854
.L4816:
	leaq	.LC107(%rip), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rbx, %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal10JSFunction13SetInitialMapENS0_6HandleIS1_EENS2_INS0_3MapEEENS2_INS0_10HeapObjectEEE
	movq	(%rbx), %rax
	movq	31(%rax), %rax
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4820
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L4821:
	movq	103(%rsi), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4823
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L4855
.L4819:
	movq	(%r15), %rax
	movq	-72(%rbp), %rdi
	movl	$4, %edx
	movq	%r12, %rsi
	movq	55(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE@PLT
.L4788:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4856
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4793:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L4857
.L4795:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L4794
	.p2align 4,,10
	.p2align 3
.L4809:
	movq	0(%r13), %r12
	leaq	55(%r14), %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, 55(%r14)
	movq	%r12, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	je	.L4830
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4830
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4830
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L4830:
	movq	0(%r13), %rax
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L4788
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject19OptimizeAsPrototypeENS0_6HandleIS1_EEb
	jmp	.L4788
	.p2align 4,,10
	.p2align 3
.L4806:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L4858
.L4808:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L4792
	.p2align 4,,10
	.p2align 3
.L4800:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L4859
.L4802:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	leal	-12(%r13), %eax
	cmpb	$3, %al
	jbe	.L4860
.L4803:
	movq	519(%rsi), %rsi
	jmp	.L4805
	.p2align 4,,10
	.p2align 3
.L4851:
	movq	-1(%rdx), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L4790
	movq	-1(%rax), %rdx
	movq	%rsi, %r13
	movzbl	13(%rdx), %eax
	andl	$-2, %eax
	movb	%al, 13(%rdx)
	jmp	.L4792
	.p2align 4,,10
	.p2align 3
.L4852:
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L4796
	leaq	.LC13(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4804:
	movq	479(%rsi), %rsi
	jmp	.L4805
	.p2align 4,,10
	.p2align 3
.L4810:
	movq	-1(%rax), %rdx
	leaq	-64(%rbp), %rcx
	movq	%rcx, -72(%rbp)
	cmpw	$68, 11(%rdx)
	jne	.L4811
	movl	15(%rax), %edx
	shrl	$29, %edx
	je	.L4811
	andq	$-262144, %r14
	movq	%rax, -64(%rbp)
	movq	%rcx, %rdi
	movq	24(%r14), %rsi
	subq	$37592, %rsi
	call	_ZN2v88internal3Map29CompleteInobjectSlackTrackingEPNS0_7IsolateE@PLT
	movq	(%rbx), %r14
	jmp	.L4811
	.p2align 4,,10
	.p2align 3
.L4813:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L4861
.L4815:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L4814
	.p2align 4,,10
	.p2align 3
.L4853:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4832
	.p2align 4,,10
	.p2align 3
.L4823:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L4862
.L4825:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	testb	$1, %sil
	je	.L4819
.L4855:
	movq	-1(%rsi), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L4819
	movq	(%rbx), %rcx
	cmpq	%rcx, (%rax)
	jne	.L4819
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23CacheInitialJSArrayMapsEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS3_INS0_3MapEEE@PLT
	jmp	.L4819
	.p2align 4,,10
	.p2align 3
.L4820:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L4863
.L4822:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L4821
	.p2align 4,,10
	.p2align 3
.L4857:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4795
	.p2align 4,,10
	.p2align 3
.L4854:
	movq	(%rbx), %r14
	movq	0(%r13), %r13
	movq	%r13, 55(%r14)
	leaq	55(%r14), %rbx
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	je	.L4819
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4819
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4819
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4819
	.p2align 4,,10
	.p2align 3
.L4859:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L4802
	.p2align 4,,10
	.p2align 3
.L4858:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L4808
	.p2align 4,,10
	.p2align 3
.L4861:
	movq	%r12, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L4815
	.p2align 4,,10
	.p2align 3
.L4863:
	movq	%r12, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L4822
	.p2align 4,,10
	.p2align 3
.L4862:
	movq	%r12, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	jmp	.L4825
.L4856:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21570:
	.size	_ZN2v88internal10JSFunction12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEE, .-_ZN2v88internal10JSFunction12SetPrototypeENS0_6HandleIS1_EENS2_INS0_6ObjectEEE
	.section	.rodata._ZN2v88internal10JSFunction31ComputeInstanceSizeWithMinSlackEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC108:
	.string	"has_initial_map()"
	.section	.text._ZN2v88internal10JSFunction31ComputeInstanceSizeWithMinSlackEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction31ComputeInstanceSizeWithMinSlackEPNS0_7IsolateE
	.type	_ZN2v88internal10JSFunction31ComputeInstanceSizeWithMinSlackEPNS0_7IsolateE, @function
_ZN2v88internal10JSFunction31ComputeInstanceSizeWithMinSlackEPNS0_7IsolateE:
.LFB21578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	55(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L4870
	movl	15(%rax), %edx
	shrl	$29, %edx
	jne	.L4871
	movzbl	7(%rax), %eax
	sall	$3, %eax
.L4864:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4872
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4871:
	.cfi_restore_state
	movq	%rdi, %rbx
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal3Map21ComputeMinObjectSlackEPNS0_7IsolateE@PLT
	movl	%eax, %r8d
	movq	(%rbx), %rax
	movq	55(%rax), %rax
	movzbl	7(%rax), %eax
	subl	%r8d, %eax
	sall	$3, %eax
	jmp	.L4864
	.p2align 4,,10
	.p2align 3
.L4870:
	leaq	.LC108(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4872:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21578:
	.size	_ZN2v88internal10JSFunction31ComputeInstanceSizeWithMinSlackEPNS0_7IsolateE, .-_ZN2v88internal10JSFunction31ComputeInstanceSizeWithMinSlackEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal10JSFunction9PrintNameEP8_IO_FILE.str1.1,"aMS",@progbits,1
.LC109:
	.string	"%s"
	.section	.text._ZN2v88internal10JSFunction9PrintNameEP8_IO_FILE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction9PrintNameEP8_IO_FILE
	.type	_ZN2v88internal10JSFunction9PrintNameEP8_IO_FILE, @function
_ZN2v88internal10JSFunction9PrintNameEP8_IO_FILE:
.LFB21579:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-40(%rbp), %rdi
	movq	23(%rax), %rax
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	leaq	-48(%rbp), %rdi
	leaq	-32(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-48(%rbp), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC109(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4873
	call	_ZdaPv@PLT
.L4873:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4880
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4880:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21579:
	.size	_ZN2v88internal10JSFunction9PrintNameEP8_IO_FILE, .-_ZN2v88internal10JSFunction9PrintNameEP8_IO_FILE
	.section	.text._ZN2v88internal10JSFunction7GetNameENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction7GetNameENS0_6HandleIS1_EE
	.type	_ZN2v88internal10JSFunction7GetNameENS0_6HandleIS1_EE, @function
_ZN2v88internal10JSFunction7GetNameENS0_6HandleIS1_EE:
.LFB21580:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rsi
	movl	$2, %eax
	movq	-34720(%rsi), %rdx
	leaq	-37592(%rsi), %r12
	subq	$34720, %rsi
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L4882
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L4882:
	movl	%eax, -128(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -116(%rbp)
	movq	%r12, -104(%rbp)
	movq	2872(%r12), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L4899
.L4883:
	leaq	-128(%rbp), %r13
	movq	%rsi, -96(%rbp)
	movq	%r13, %rdi
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -124(%rbp)
	jne	.L4884
	movq	-104(%rbp), %rax
	movq	88(%rax), %rdx
	addq	$88, %rax
	testb	$1, %dl
	jne	.L4886
.L4889:
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movq	23(%rax), %rax
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L4900
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L4890:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4901
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4900:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L4902
.L4892:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L4890
	.p2align 4,,10
	.p2align 3
.L4884:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE
	movq	(%rax), %rdx
	testb	$1, %dl
	je	.L4889
.L4886:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L4889
	jmp	.L4890
	.p2align 4,,10
	.p2align 3
.L4899:
	movq	%r12, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L4883
	.p2align 4,,10
	.p2align 3
.L4902:
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L4892
.L4901:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21580:
	.size	_ZN2v88internal10JSFunction7GetNameENS0_6HandleIS1_EE, .-_ZN2v88internal10JSFunction7GetNameENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal10JSFunction12GetDebugNameENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction12GetDebugNameENS0_6HandleIS1_EE
	.type	_ZN2v88internal10JSFunction12GetDebugNameENS0_6HandleIS1_EE, @function
_ZN2v88internal10JSFunction12GetDebugNameENS0_6HandleIS1_EE:
.LFB21581:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rsi
	movl	$2, %eax
	movq	-35232(%rsi), %rdx
	leaq	-37592(%rsi), %rdi
	subq	$35232, %rsi
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L4904
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L4904:
	movl	%eax, -112(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -100(%rbp)
	movq	2360(%rdi), %rax
	movq	%rdi, -88(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L4916
.L4905:
	leaq	-112(%rbp), %r13
	movq	%rsi, -80(%rbp)
	movq	%r13, %rdi
	movq	$0, -72(%rbp)
	movq	%r12, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	%r12, -48(%rbp)
	movq	$-1, -40(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -108(%rbp)
	jne	.L4906
	movq	-88(%rbp), %rax
	movq	88(%rax), %rdx
	addq	$88, %rax
	testb	$1, %dl
	jne	.L4908
.L4910:
	movq	%r12, %rdi
	call	_ZN2v88internal10JSFunction7GetNameENS0_6HandleIS1_EE
.L4909:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4917
	addq	$96, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4906:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE
	movq	(%rax), %rdx
	testb	$1, %dl
	je	.L4910
.L4908:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L4910
	jmp	.L4909
	.p2align 4,,10
	.p2align 3
.L4916:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L4905
.L4917:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21581:
	.size	_ZN2v88internal10JSFunction12GetDebugNameENS0_6HandleIS1_EE, .-_ZN2v88internal10JSFunction12GetDebugNameENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal10JSFunction7SetNameENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction7SetNameENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6StringEEE
	.type	_ZN2v88internal10JSFunction7SetNameENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6StringEEE, @function
_ZN2v88internal10JSFunction7SetNameENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6StringEEE:
.LFB21582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	subq	$37592, %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal4Name14ToFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L4947
	movq	%rax, %r12
	movq	(%r14), %rax
	movl	11(%rax), %edi
	testl	%edi, %edi
	jle	.L4921
	leaq	-144(%rbp), %r15
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	-112(%rbp), %rax
	movl	-136(%rbp), %esi
	movq	(%rax), %rdx
	movl	-124(%rbp), %eax
	testl	%esi, %esi
	leal	1(%rax), %ecx
	movl	%ecx, -124(%rbp)
	jne	.L4922
	addl	$16, %eax
	cltq
	movb	$32, -1(%rdx,%rax)
	movl	-128(%rbp), %eax
	cmpl	%eax, -124(%rbp)
	je	.L4923
.L4924:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4947
.L4921:
	movq	0(%r13), %rax
	leaq	2872(%rbx), %r15
	andq	$-262144, %rax
	movq	24(%rax), %r14
	movq	2872(%rbx), %rax
	movq	-1(%rax), %rdx
	subq	$37592, %r14
	cmpw	$63, 11(%rdx)
	jbe	.L4948
.L4928:
	movq	2872(%rbx), %rcx
	movl	$1, %edx
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L4932
	movl	11(%rcx), %edx
	notl	%edx
	andl	$1, %edx
.L4932:
	movabsq	$824633720832, %rcx
	movl	%edx, -224(%rbp)
	movq	%rcx, -212(%rbp)
	movq	%r14, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L4949
.L4933:
	movq	%r13, -176(%rbp)
	movq	%r13, -160(%rbp)
	leaq	-224(%rbp), %r13
	movq	%r13, %rdi
	movq	%r15, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	$0, -168(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L4931:
	movl	$1, %ecx
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE
.L4918:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4950
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4948:
	.cfi_restore_state
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L4929
	testb	$2, %al
	jne	.L4946
.L4929:
	leaq	-144(%rbp), %rdi
	leaq	-228(%rbp), %rsi
	movq	%rdi, -248(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-248(%rbp), %rdi
	testb	%al, %al
	je	.L4946
	movabsq	$824633720832, %rax
	movq	%r13, -96(%rbp)
	movq	%rax, -132(%rbp)
	movl	-228(%rbp), %eax
	movq	%r13, -80(%rbp)
	leaq	-224(%rbp), %r13
	movl	$1, -144(%rbp)
	movq	%r14, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	movl	%eax, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r15, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L4931
	.p2align 4,,10
	.p2align 3
.L4947:
	xorl	%eax, %eax
	jmp	.L4918
	.p2align 4,,10
	.p2align 3
.L4922:
	leal	16(%rax,%rax), %eax
	movl	$32, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	-128(%rbp), %eax
	cmpl	%eax, -124(%rbp)
	jne	.L4924
.L4923:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L4924
	.p2align 4,,10
	.p2align 3
.L4946:
	movq	2872(%rbx), %rax
	jmp	.L4928
	.p2align 4,,10
	.p2align 3
.L4949:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r15
	jmp	.L4933
.L4950:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21582:
	.size	_ZN2v88internal10JSFunction7SetNameENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6StringEEE, .-_ZN2v88internal10JSFunction7SetNameENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6StringEEE
	.section	.text._ZN2v88internal10JSFunction8ToStringENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction8ToStringENS0_6HandleIS1_EE
	.type	_ZN2v88internal10JSFunction8ToStringENS0_6HandleIS1_EE, @function
_ZN2v88internal10JSFunction8ToStringENS0_6HandleIS1_EE:
.LFB21584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %r12
	subq	$37592, %r12
	movq	23(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4952
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	movq	31(%rsi), %rax
	testb	$1, %al
	jne	.L4983
.L4955:
	leaq	-128(%rbp), %r14
	movq	%rax, -128(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	je	.L4982
	movq	(%rbx), %rdx
	movq	3656(%r12), %rax
	leaq	3656(%r12), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	movq	-1(%rax), %rcx
	movl	$2, %edx
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	je	.L4984
.L4960:
	movabsq	$824633720832, %rax
	movl	%edx, -128(%rbp)
	movq	%rax, -116(%rbp)
	movq	%rdi, -104(%rbp)
	movq	3656(%r12), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L4985
.L4961:
	movq	%r14, %rdi
	movq	%rsi, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -124(%rbp)
	jne	.L4962
	movq	-104(%rbp), %rax
	addq	$88, %rax
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L4964
.L4967:
	movq	0(%r13), %rax
	movq	%r14, %rdi
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13HasSourceCodeEv@PLT
	testb	%al, %al
	je	.L4982
	movq	0(%r13), %rax
	movq	%rax, -128(%rbp)
	movzwl	45(%rax), %ebx
	cmpl	$65535, %ebx
	je	.L4975
	movq	%r14, %rdi
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	subl	%ebx, %eax
	cmpl	$-1, %eax
	jne	.L4976
.L4975:
	movl	$49, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	.p2align 4,,10
	.p2align 3
.L4982:
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_130NativeCodeFunctionSourceStringENS0_6HandleINS0_18SharedFunctionInfoEEE
.L4959:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4986
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4962:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE
	movq	(%rax), %rax
	testb	$1, %al
	je	.L4967
.L4964:
	movq	-1(%rax), %rdx
	cmpw	$85, 11(%rdx)
	jne	.L4967
	movslq	11(%rax), %rbx
	movslq	19(%rax), %r14
	movq	0(%r13), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L4987
.L4968:
	movq	41112(%r12), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L4969
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L4970:
	testq	%rbx, %rbx
	jne	.L4972
	movq	(%rax), %rdx
	cmpl	11(%rdx), %r14d
	je	.L4959
.L4972:
	movl	%r14d, %ecx
	movl	%ebx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	jmp	.L4959
	.p2align 4,,10
	.p2align 3
.L4952:
	movq	41088(%r12), %r13
	cmpq	%r13, 41096(%r12)
	je	.L4988
.L4954:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	movq	31(%rsi), %rax
	testb	$1, %al
	je	.L4955
.L4983:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L4989
.L4956:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	je	.L4982
	jmp	.L4955
	.p2align 4,,10
	.p2align 3
.L4984:
	xorl	%edx, %edx
	testb	$1, 11(%rax)
	sete	%dl
	addl	%edx, %edx
	jmp	.L4960
	.p2align 4,,10
	.p2align 3
.L4976:
	movq	%r13, %rdi
	call	_ZN2v88internal18SharedFunctionInfo20GetSourceCodeHarmonyENS0_6HandleIS1_EE@PLT
	jmp	.L4959
	.p2align 4,,10
	.p2align 3
.L4985:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L4961
	.p2align 4,,10
	.p2align 3
.L4989:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L4956
	jmp	.L4955
	.p2align 4,,10
	.p2align 3
.L4988:
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L4954
	.p2align 4,,10
	.p2align 3
.L4969:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L4990
.L4971:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L4970
	.p2align 4,,10
	.p2align 3
.L4987:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L4968
	movq	23(%rax), %rax
	jmp	.L4968
.L4990:
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L4971
.L4986:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21584:
	.size	_ZN2v88internal10JSFunction8ToStringENS0_6HandleIS1_EE, .-_ZN2v88internal10JSFunction8ToStringENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal10JSFunction30CalculateExpectedNofPropertiesEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction30CalculateExpectedNofPropertiesEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal10JSFunction30CalculateExpectedNofPropertiesEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal10JSFunction30CalculateExpectedNofPropertiesEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB21585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L5040
	movq	%rdi, %rbx
	movq	%rsi, %r15
	xorl	%r13d, %r13d
	movabsq	$287762808832, %r14
	.p2align 4,,10
	.p2align 3
.L4992:
	movq	(%r15), %rax
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L5018
	movq	23(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4994
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L4995:
	movq	%rsi, %rax
	leaq	7(%rsi), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	7(%rsi), %rcx
	testb	$1, %cl
	jne	.L4997
.L5000:
	movq	(%rdx), %rcx
	testb	$1, %cl
	jne	.L5041
.L4998:
	xorl	%eax, %eax
.L5009:
	movq	(%rdx), %rdx
	cmpq	%r14, %rdx
	je	.L5010
.L5048:
	testb	$1, %dl
	jne	.L5042
	movq	%rax, -80(%rbp)
.L5039:
	movb	$1, -72(%rbp)
.L5015:
	movq	(%r12), %rax
	movzwl	43(%rax), %edx
	movl	$252, %eax
	subl	%edx, %eax
	cmpl	%eax, %r13d
	jg	.L5043
	movq	(%r15), %rax
	addl	%edx, %r13d
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L5018
	movq	-1(%rax), %rax
	movq	41112(%rbx), %rdi
	movq	104(%rbx), %rdx
	movq	23(%rax), %r12
	testq	%rdi, %rdi
	je	.L5044
	movq	%r12, %rsi
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-88(%rbp), %rdx
	movq	%rax, %r15
.L5016:
	cmpq	%rdx, %r12
	jne	.L4992
	.p2align 4,,10
	.p2align 3
.L5018:
	testl	%r13d, %r13d
	je	.L4991
	addl	$8, %r13d
	movl	$252, %eax
	cmpl	$252, %r13d
	cmovg	%eax, %r13d
.L4991:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5045
	addq	$72, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5041:
	.cfi_restore_state
	movq	-1(%rcx), %rcx
	cmpw	$91, 11(%rcx)
	jne	.L4998
	movq	31(%rsi), %rcx
	testb	$1, %cl
	jne	.L5046
.L5001:
	movq	(%rdx), %rcx
	testb	$1, %cl
	jne	.L5047
.L5005:
	movq	(%rdx), %rcx
	movq	7(%rcx), %rsi
.L5004:
	movq	3520(%rax), %rdi
	leaq	-37592(%rax), %rcx
	testq	%rdi, %rdi
	je	.L5006
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-88(%rbp), %rdx
	movq	(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L5048
	.p2align 4,,10
	.p2align 3
.L5010:
	movq	%rax, -80(%rbp)
.L5038:
	leaq	-80(%rbp), %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	movb	$0, -72(%rbp)
	call	_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_10JSFunctionEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE@PLT
	testb	%al, %al
	jne	.L5015
	jmp	.L5018
	.p2align 4,,10
	.p2align 3
.L4994:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L5049
.L4996:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L4995
	.p2align 4,,10
	.p2align 3
.L4997:
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L5000
	movq	31(%rsi), %rcx
	testb	$1, %cl
	je	.L5001
.L5046:
	movq	-1(%rcx), %rsi
	cmpw	$86, 11(%rsi)
	jne	.L5001
	movq	39(%rcx), %rsi
	testb	$1, %sil
	je	.L5001
	movq	-1(%rsi), %rsi
	cmpw	$72, 11(%rsi)
	jne	.L5001
	movq	31(%rcx), %rsi
	jmp	.L5004
	.p2align 4,,10
	.p2align 3
.L5044:
	movq	41088(%rbx), %r15
	cmpq	41096(%rbx), %r15
	je	.L5050
.L5017:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, (%r15)
	jmp	.L5016
	.p2align 4,,10
	.p2align 3
.L5042:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L5010
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	movq	%rax, -80(%rbp)
	jne	.L5039
	jmp	.L5038
	.p2align 4,,10
	.p2align 3
.L5006:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L5051
.L5008:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L5009
	.p2align 4,,10
	.p2align 3
.L5047:
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L5005
	movq	(%rdx), %rsi
	jmp	.L5004
	.p2align 4,,10
	.p2align 3
.L5049:
	movq	%rbx, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L4996
	.p2align 4,,10
	.p2align 3
.L5050:
	movq	%rbx, %rdi
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L5017
	.p2align 4,,10
	.p2align 3
.L5043:
	movl	$252, %r13d
	jmp	.L4991
	.p2align 4,,10
	.p2align 3
.L5051:
	movq	%rcx, %rdi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rcx
	jmp	.L5008
.L5040:
	leaq	.LC23(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L5045:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21585:
	.size	_ZN2v88internal10JSFunction30CalculateExpectedNofPropertiesEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal10JSFunction30CalculateExpectedNofPropertiesEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal10JSFunction19EnsureHasInitialMapENS0_6HandleIS1_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC110:
	.string	"max_nof_fields <= JSObject::kMaxInObjectProperties"
	.align 8
.LC111:
	.string	"*in_object_properties == ((*instance_size - header_size) >> kTaggedSizeLog2) - requested_embedder_fields"
	.align 8
.LC112:
	.string	"static_cast<unsigned>(*instance_size) <= static_cast<unsigned>(JSObject::kMaxInstanceSize)"
	.section	.text._ZN2v88internal10JSFunction19EnsureHasInitialMapENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction19EnsureHasInitialMapENS0_6HandleIS1_EE
	.type	_ZN2v88internal10JSFunction19EnsureHasInitialMapENS0_6HandleIS1_EE, @function
_ZN2v88internal10JSFunction19EnsureHasInitialMapENS0_6HandleIS1_EE:
.LFB21572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	55(%rax), %rdx
	movq	-1(%rdx), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L5076
.L5052:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5077
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5076:
	.cfi_restore_state
	movq	%rax, %rdx
	movq	%rdi, %r12
	andq	$-262144, %rdx
	movq	24(%rdx), %r14
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	subq	$37592, %r14
	andl	$31, %edx
	leal	-9(%rdx), %ecx
	cmpb	$6, %cl
	jbe	.L5072
	movl	$1057, %r13d
	cmpb	$1, %dl
	jne	.L5055
.L5072:
	movq	23(%rax), %rax
	movl	47(%rax), %eax
	andl	$31, %eax
	subl	$12, %eax
	cmpb	$2, %al
	sbbl	%r13d, %r13d
	andl	$-4, %r13d
	addl	$1068, %r13d
.L5055:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal10JSFunction30CalculateExpectedNofPropertiesEPNS0_7IsolateENS0_6HandleIS1_EE
	xorl	%esi, %esi
	movl	%r13d, %edi
	movl	%eax, %ebx
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb
	movl	$2040, %edx
	subl	%eax, %edx
	movl	%edx, %r8d
	sarl	$3, %r8d
	cmpl	$2023, %edx
	jg	.L5078
	cmpl	%r8d, %ebx
	cmovle	%ebx, %r8d
	leal	0(,%r8,8), %ecx
	leal	(%rax,%rcx), %edx
	sarl	$3, %ecx
	cmpl	%ecx, %r8d
	jne	.L5079
	cmpl	$2040, %edx
	ja	.L5080
	movl	$3, %ecx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi@PLT
	movq	%rax, %rbx
	movq	(%r12), %rax
	movq	55(%rax), %rdx
	movq	-1(%rdx), %rcx
	cmpw	$68, 11(%rcx)
	jne	.L5060
.L5063:
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	je	.L5061
.L5065:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L5066
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L5064:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal10JSFunction13SetInitialMapENS0_6HandleIS1_EENS2_INS0_3MapEEENS2_INS0_10HeapObjectEEE
	movq	(%rbx), %rax
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal3Map26StartInobjectSlackTrackingEv@PLT
	jmp	.L5052
	.p2align 4,,10
	.p2align 3
.L5061:
	movq	23(%rsi), %rsi
	jmp	.L5065
	.p2align 4,,10
	.p2align 3
.L5060:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	-37496(%rcx), %rdx
	jne	.L5063
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory20NewFunctionPrototypeENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	%rax, %rdx
	jmp	.L5064
	.p2align 4,,10
	.p2align 3
.L5066:
	movq	41088(%r14), %rdx
	cmpq	41096(%r14), %rdx
	je	.L5081
.L5068:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L5064
	.p2align 4,,10
	.p2align 3
.L5078:
	leaq	.LC110(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5079:
	leaq	.LC111(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5080:
	leaq	.LC112(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5081:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L5068
.L5077:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21572:
	.size	_ZN2v88internal10JSFunction19EnsureHasInitialMapENS0_6HandleIS1_EE, .-_ZN2v88internal10JSFunction19EnsureHasInitialMapENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal10JSFunction27CalculateInstanceSizeHelperENS0_12InstanceTypeEbiiPiS3_.str1.8,"aMS",@progbits,1
	.align 8
.LC113:
	.string	"static_cast<unsigned>(requested_embedder_fields) <= static_cast<unsigned>(max_nof_fields)"
	.section	.text._ZN2v88internal10JSFunction27CalculateInstanceSizeHelperENS0_12InstanceTypeEbiiPiS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction27CalculateInstanceSizeHelperENS0_12InstanceTypeEbiiPiS3_
	.type	_ZN2v88internal10JSFunction27CalculateInstanceSizeHelperENS0_12InstanceTypeEbiiPiS3_, @function
_ZN2v88internal10JSFunction27CalculateInstanceSizeHelperENS0_12InstanceTypeEbiiPiS3_:
.LFB21586:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%sil, %esi
	movzwl	%di, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%r8, %rbx
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb
	testl	%r12d, %r12d
	je	.L5083
	addl	$7, %eax
	movl	$2040, %edx
	andl	$-8, %eax
	subl	%eax, %edx
	movl	%edx, %ecx
	sarl	$3, %ecx
	cmpl	$2023, %edx
	jg	.L5088
	cmpl	%r12d, %ecx
	jb	.L5090
.L5085:
	subl	%r12d, %ecx
	cmpl	%r14d, %ecx
	cmovg	%r14d, %ecx
	movl	%ecx, 0(%r13)
	addl	%r12d, %ecx
	sall	$3, %ecx
	addl	%ecx, %eax
	sarl	$3, %ecx
	movl	%eax, (%rbx)
	subl	%r12d, %ecx
	cmpl	0(%r13), %ecx
	jne	.L5091
	cmpl	$2040, %eax
	ja	.L5092
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5083:
	.cfi_restore_state
	movl	$2040, %edx
	subl	%eax, %edx
	movl	%edx, %ecx
	sarl	$3, %ecx
	cmpl	$2023, %edx
	jle	.L5085
.L5088:
	leaq	.LC110(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5090:
	leaq	.LC113(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5091:
	leaq	.LC111(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5092:
	leaq	.LC112(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21586:
	.size	_ZN2v88internal10JSFunction27CalculateInstanceSizeHelperENS0_12InstanceTypeEbiiPiS3_, .-_ZN2v88internal10JSFunction27CalculateInstanceSizeHelperENS0_12InstanceTypeEbiiPiS3_
	.section	.rodata._ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC114:
	.string	"constructor_initial_map->UsedInstanceSize() <= instance_size"
	.section	.rodata._ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE.str1.1,"aMS",@progbits,1
.LC115:
	.string	"prototype->IsJSReceiver()"
	.section	.text._ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE
	.type	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE, @function
_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE:
.LFB21574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal10JSFunction19EnsureHasInitialMapENS0_6HandleIS1_EE
	movq	(%rbx), %rax
	movq	41112(%r12), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L5094
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L5095:
	movq	(%r14), %rax
	cmpq	%rax, (%rbx)
	jne	.L5097
.L5171:
	movq	%r13, %rax
.L5098:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5204
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5097:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L5205
.L5099:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L5206
	movq	3104(%r12), %rax
	leaq	3104(%r12), %rsi
	movl	$3, %edx
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	je	.L5207
.L5137:
	movabsq	$824633720832, %rax
	movl	%edx, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	3104(%r12), %rax
	movq	%r12, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L5208
.L5138:
	leaq	-144(%rbp), %r13
	movq	%rsi, -112(%rbp)
	movq	%r13, %rdi
	movq	$0, -104(%rbp)
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r14, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L5139
	movq	-120(%rbp), %rax
	leaq	88(%rax), %r15
.L5140:
	movq	%rbx, %rdi
	call	_ZN2v88internal10JSFunction19EnsureHasInitialMapENS0_6HandleIS1_EE
	movq	(%rbx), %rax
	movq	41112(%r12), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L5141
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%r15), %rsi
	movq	%rax, %r13
.L5136:
	testb	$1, %sil
	jne	.L5209
.L5143:
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver16GetFunctionRealmENS0_6HandleIS1_EE
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L5203
	movq	(%rbx), %rdx
	movq	3744(%r12), %rax
	leaq	3744(%r12), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	movq	-1(%rax), %rcx
	movl	$2, %edx
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L5148
	xorl	%edx, %edx
	testb	$1, 11(%rax)
	sete	%dl
	addl	%edx, %edx
.L5148:
	movabsq	$824633720832, %rax
	movl	%edx, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	3744(%r12), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L5210
.L5149:
	leaq	-144(%rbp), %r15
	movq	%rsi, -112(%rbp)
	movq	%r15, %rdi
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L5150
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L5151:
	movq	(%rax), %rdx
	movl	$880, %eax
	testb	$1, %dl
	jne	.L5152
	sarq	$32, %rdx
	leal	16(,%rdx,8), %eax
	cltq
.L5152:
	movq	(%r14), %rdx
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5153
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L5154:
	movq	-1(%rsi), %rax
	movzbl	13(%rax), %eax
	testb	$1, %al
	je	.L5156
	movq	-1(%rsi), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L5158
.L5159:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5162
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L5147:
	movq	0(%r13), %rax
	movzbl	9(%rax), %r8d
	cmpl	$2, %r8d
	jg	.L5211
.L5165:
	movzbl	7(%rax), %ecx
	movzbl	8(%rax), %esi
	movq	%r12, %rdi
	movzbl	7(%rax), %edx
	movzbl	%sil, %eax
	movq	%r13, %rsi
	subl	%eax, %ecx
	sall	$3, %edx
	call	_ZN2v88internal3Map14CopyInitialMapEPNS0_7IsolateENS0_6HandleIS1_EEiii@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	andb	$-2, 14(%rax)
	movq	(%r15), %rax
	testb	$1, %al
	jne	.L5212
.L5166:
	leaq	.LC115(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5094:
	movq	41088(%r12), %r13
	cmpq	%r13, 41096(%r12)
	je	.L5213
.L5096:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L5095
	.p2align 4,,10
	.p2align 3
.L5139:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L5140
.L5203:
	xorl	%eax, %eax
	jmp	.L5098
	.p2align 4,,10
	.p2align 3
.L5207:
	movl	11(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%edx, %edx
	andl	$3, %edx
	jmp	.L5137
	.p2align 4,,10
	.p2align 3
.L5205:
	movq	-1(%rax), %rdx
	movzbl	13(%rdx), %edx
	testb	%dl, %dl
	jns	.L5099
	movq	55(%rax), %rdx
	movq	-1(%rdx), %rcx
	cmpw	$68, 11(%rcx)
	jne	.L5105
	movq	31(%rdx), %rdx
	movq	(%rbx), %rsi
	movq	%rax, %rcx
	testb	$1, %dl
	jne	.L5104
.L5102:
	cmpq	%rdx, %rsi
	jne	.L5105
.L5106:
	movq	41112(%r12), %rdi
	movq	55(%rcx), %rsi
	testq	%rdi, %rdi
	je	.L5122
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L5098
	.p2align 4,,10
	.p2align 3
.L5105:
	movq	0(%r13), %r15
	movzwl	11(%r15), %ecx
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andl	$31, %edx
	subl	$4, %edx
	cmpb	$1, %dl
	ja	.L5099
	movzbl	7(%r15), %edx
	movzwl	%cx, %r10d
	sall	$3, %edx
	je	.L5107
	cmpw	$1057, %cx
	je	.L5175
	movl	%edx, -176(%rbp)
	movsbl	13(%r15), %esi
	movl	%r10d, %edi
	movl	%r10d, -168(%rbp)
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb
	movl	-168(%rbp), %r10d
	movl	-176(%rbp), %edx
.L5108:
	movzbl	7(%r15), %ecx
	movzbl	8(%r15), %esi
	subl	%eax, %edx
	sarl	$3, %edx
	subl	%esi, %ecx
	subl	%ecx, %edx
.L5107:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%r10d, -176(%rbp)
	movl	%edx, -168(%rbp)
	call	_ZN2v88internal10JSFunction30CalculateExpectedNofPropertiesEPNS0_7IsolateENS0_6HandleIS1_EE
	movl	-168(%rbp), %edx
	movl	-176(%rbp), %r10d
	leaq	-148(%rbp), %r9
	movl	%eax, %ecx
	leaq	-152(%rbp), %r8
	movl	$1, %esi
	movl	%r10d, %edi
	call	_ZN2v88internal10JSFunction27CalculateInstanceSizeHelperENS0_12InstanceTypeEbiiPiS3_
	movq	0(%r13), %rcx
	movzbl	7(%rcx), %eax
	movzbl	8(%rcx), %edx
	subl	%edx, %eax
	movzbl	9(%rcx), %edx
	cmpl	$2, %edx
	jg	.L5214
.L5109:
	movl	-152(%rbp), %r9d
	subl	%edx, %eax
	movzbl	9(%rcx), %edx
	leal	0(,%rdx,8), %esi
	cmpl	$2, %edx
	jle	.L5215
.L5111:
	cmpl	%esi, %r9d
	jl	.L5216
	movl	-148(%rbp), %ecx
	movq	%r13, %rsi
	movl	%r9d, %edx
	movq	%r12, %rdi
	movl	%ecx, %r8d
	subl	%eax, %r8d
	call	_ZN2v88internal3Map14CopyInitialMapEPNS0_7IsolateENS0_6HandleIS1_EEiii@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	andb	$-2, 14(%rax)
	movq	(%r14), %rax
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L5114
	movq	23(%rsi), %rsi
.L5114:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5115
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L5116:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal10JSFunction13SetInitialMapENS0_6HandleIS1_EENS2_INS0_3MapEEENS2_INS0_10HeapObjectEEE
	movq	0(%r13), %r15
	movq	(%rbx), %rbx
	movq	31(%r15), %rax
	leaq	31(%r15), %rsi
	testb	$1, %al
	jne	.L5217
.L5118:
	movq	%rbx, 31(%r15)
	testb	$1, %bl
	je	.L5173
	movq	%rbx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -168(%rbp)
	testl	$262144, %eax
	je	.L5120
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %rsi
	movq	8(%rcx), %rax
.L5120:
	testb	$24, %al
	je	.L5173
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L5173
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L5173:
	movq	0(%r13), %rdx
	leaq	-144(%rbp), %rdi
	movl	15(%rdx), %eax
	andl	$536870911, %eax
	movl	%eax, 15(%rdx)
	movq	0(%r13), %rax
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal3Map26StartInobjectSlackTrackingEv@PLT
	movq	(%r14), %rcx
	jmp	.L5106
	.p2align 4,,10
	.p2align 3
.L5206:
	movq	-1(%rax), %rax
	movzbl	13(%rax), %eax
	testb	%al, %al
	js	.L5218
	movq	88(%r12), %rsi
	leaq	88(%r12), %r15
	jmp	.L5136
	.p2align 4,,10
	.p2align 3
.L5156:
	movq	55(%rsi), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L5159
	movq	23(%rsi), %rsi
	jmp	.L5159
	.p2align 4,,10
	.p2align 3
.L5150:
	movq	%r15, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE
	jmp	.L5151
	.p2align 4,,10
	.p2align 3
.L5209:
	movq	-1(%rsi), %rax
	cmpw	$1023, 11(%rax)
	ja	.L5147
	jmp	.L5143
	.p2align 4,,10
	.p2align 3
.L5153:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L5219
.L5155:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L5154
	.p2align 4,,10
	.p2align 3
.L5162:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L5220
.L5164:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	movq	0(%r13), %rax
	movzbl	9(%rax), %r8d
	cmpl	$2, %r8d
	jle	.L5165
.L5211:
	movzbl	7(%rax), %edx
	subl	%r8d, %edx
	movl	%edx, %r8d
	jmp	.L5165
	.p2align 4,,10
	.p2align 3
.L5141:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L5221
.L5142:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	movq	(%r15), %rsi
	jmp	.L5136
	.p2align 4,,10
	.p2align 3
.L5212:
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L5166
	movq	0(%r13), %r14
	cmpq	23(%r14), %rax
	je	.L5168
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	0(%r13), %r14
.L5168:
	movq	31(%r14), %rax
	movq	(%rbx), %r12
	leaq	31(%r14), %r15
	testb	$1, %al
	jne	.L5222
.L5169:
	movq	%r12, (%r15)
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	je	.L5171
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L5171
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L5171
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L5171
	.p2align 4,,10
	.p2align 3
.L5222:
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L5169
.L5170:
	leaq	.LC13(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5213:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L5096
	.p2align 4,,10
	.p2align 3
.L5208:
	movq	%r12, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L5138
	.p2align 4,,10
	.p2align 3
.L5158:
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L5159
	movq	31(%rsi), %rsi
	testb	$1, %sil
	je	.L5159
	jmp	.L5158
.L5103:
	movq	31(%rdx), %rdx
	testb	$1, %dl
	je	.L5202
	.p2align 4,,10
	.p2align 3
.L5104:
	movq	-1(%rdx), %rcx
	cmpw	$68, 11(%rcx)
	je	.L5103
.L5202:
	movq	%rax, %rcx
	jmp	.L5102
	.p2align 4,,10
	.p2align 3
.L5210:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L5149
	.p2align 4,,10
	.p2align 3
.L5218:
	movq	%r14, %rdi
	call	_ZN2v88internal10JSFunction19EnsureHasInitialMapENS0_6HandleIS1_EE
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	movzbl	13(%rdx), %edx
	andl	$1, %edx
	je	.L5127
	movq	-1(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L5129
.L5130:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5133
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
	jmp	.L5136
.L5129:
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L5130
	movq	31(%rsi), %rsi
	testb	$1, %sil
	je	.L5130
	jmp	.L5129
	.p2align 4,,10
	.p2align 3
.L5122:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L5223
.L5124:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L5098
	.p2align 4,,10
	.p2align 3
.L5127:
	movq	55(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L5130
	movq	23(%rsi), %rsi
	jmp	.L5130
	.p2align 4,,10
	.p2align 3
.L5133:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L5224
.L5135:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L5136
	.p2align 4,,10
	.p2align 3
.L5220:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L5164
	.p2align 4,,10
	.p2align 3
.L5219:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	jmp	.L5155
	.p2align 4,,10
	.p2align 3
.L5221:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L5142
	.p2align 4,,10
	.p2align 3
.L5115:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L5225
.L5117:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L5116
.L5215:
	movzbl	7(%rcx), %esi
	sall	$3, %esi
	jmp	.L5111
.L5214:
	movzbl	7(%rcx), %esi
	subl	%edx, %esi
	movl	%esi, %edx
	jmp	.L5109
.L5217:
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L5118
	jmp	.L5170
.L5223:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	jmp	.L5124
.L5175:
	movl	$24, %eax
	movl	$1057, %r10d
	jmp	.L5108
.L5224:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L5135
.L5216:
	leaq	.LC114(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L5225:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L5117
.L5204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21574:
	.size	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE, .-_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE
	.section	.text._ZN2v88internal8JSObject3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEENS2_INS0_14AllocationSiteEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEENS2_INS0_14AllocationSiteEEE
	.type	_ZN2v88internal8JSObject3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEENS2_INS0_14AllocationSiteEEE, @function
_ZN2v88internal8JSObject3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEENS2_INS0_14AllocationSiteEEE:
.LFB21466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	subq	$37592, %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE
	testq	%rax, %rax
	je	.L5244
	movq	%rax, %rsi
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	jne	.L5245
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r12
.L5230:
	movq	40960(%rbx), %r13
	cmpb	$0, 7840(%r13)
	je	.L5231
	movq	7832(%r13), %rax
.L5232:
	testq	%rax, %rax
	je	.L5233
	addl	$1, (%rax)
.L5233:
	movq	40960(%rbx), %rbx
	cmpb	$0, 6944(%rbx)
	je	.L5234
	movq	6936(%rbx), %rax
.L5235:
	testq	%rax, %rax
	je	.L5236
	addl	$1, (%rax)
.L5236:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5245:
	.cfi_restore_state
	movq	%r12, %r8
	xorl	%ecx, %ecx
	movl	$2, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory22NewSlowJSObjectFromMapENS0_6HandleINS0_3MapEEEiNS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r12
	jmp	.L5230
	.p2align 4,,10
	.p2align 3
.L5234:
	movb	$1, 6944(%rbx)
	leaq	6920(%rbx), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6936(%rbx)
	jmp	.L5235
	.p2align 4,,10
	.p2align 3
.L5231:
	movb	$1, 7840(%r13)
	leaq	7816(%r13), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7832(%r13)
	jmp	.L5232
	.p2align 4,,10
	.p2align 3
.L5244:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21466:
	.size	_ZN2v88internal8JSObject3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEENS2_INS0_14AllocationSiteEEE, .-_ZN2v88internal8JSObject3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEENS2_INS0_14AllocationSiteEEE
	.section	.rodata._ZN2v88internal10JSFunction21ClearTypeFeedbackInfoEv.str1.1,"aMS",@progbits,1
.LC116:
	.string	"ClearTypeFeedbackInfo"
	.section	.text._ZN2v88internal10JSFunction21ClearTypeFeedbackInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSFunction21ClearTypeFeedbackInfoEv
	.type	_ZN2v88internal10JSFunction21ClearTypeFeedbackInfoEv, @function
_ZN2v88internal10JSFunction21ClearTypeFeedbackInfoEv:
.LFB21587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal19FLAG_flush_bytecodeE(%rip)
	leaq	23(%rdx), %rax
	jne	.L5284
.L5247:
	movabsq	$287762808832, %rdx
	movq	(%rax), %rax
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5246
	testb	$1, %al
	jne	.L5265
.L5268:
	movq	(%rbx), %rax
	movq	39(%rax), %rdx
	movq	7(%rdx), %rdx
	movq	-1(%rdx), %rcx
	cmpw	$155, 11(%rcx)
	je	.L5285
.L5246:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5286
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5265:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L5246
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L5268
	jmp	.L5246
	.p2align 4,,10
	.p2align 3
.L5285:
	andq	$-262144, %rax
	movq	%rdx, -64(%rbp)
	leaq	-64(%rbp), %rdi
	movq	24(%rax), %r12
	subq	$37592, %r12
	movq	%r12, %rsi
	call	_ZN2v88internal14FeedbackVector10ClearSlotsEPNS0_7IsolateE@PLT
	testb	%al, %al
	je	.L5246
	movq	-64(%rbp), %rsi
	leaq	.LC116(%rip), %rcx
	movl	$-1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc@PLT
	jmp	.L5246
	.p2align 4,,10
	.p2align 3
.L5284:
	movq	23(%rdx), %rdx
	movq	(%rdi), %rax
	movq	47(%rax), %rcx
	testb	$1, %dl
	jne	.L5248
.L5282:
	addq	$23, %rax
	jmp	.L5247
	.p2align 4,,10
	.p2align 3
.L5248:
	movq	-1(%rdx), %rsi
	addq	$23, %rax
	cmpw	$160, 11(%rsi)
	jne	.L5247
	testb	$1, %cl
	je	.L5247
	movq	-1(%rcx), %rsi
	cmpw	$69, 11(%rsi)
	jne	.L5247
	movq	7(%rdx), %rax
	movabsq	$287762808832, %rdx
	cmpq	%rdx, %rax
	je	.L5256
	testb	$1, %al
	jne	.L5287
	.p2align 4,,10
	.p2align 3
.L5283:
	movq	(%rbx), %rax
	jmp	.L5282
.L5287:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L5256
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L5283
.L5256:
	cmpl	$67, 59(%rcx)
	movq	(%rbx), %rax
	je	.L5282
	andq	$-262144, %rax
	movl	$67, %esi
	movq	24(%rax), %rdi
	addq	$3592, %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%rax, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 47(%rax)
	movq	(%rbx), %rdi
	testb	$1, %dl
	je	.L5257
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L5257
	leaq	47(%rdi), %rsi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
.L5257:
	movq	39(%rdi), %r12
	movq	%r12, %r14
	leaq	7(%r12), %r15
	movq	%r12, %rdi
	andq	$-262144, %r14
	movq	%r15, %rsi
	movq	24(%r14), %rax
	movq	-37504(%rax), %r13
	movq	%r13, 7(%r12)
	movq	%r13, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	je	.L5260
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L5260
	testb	$24, 8(%r14)
	jne	.L5260
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L5260:
	cmpb	$0, _ZN2v88internal29FLAG_lazy_feedback_allocationE(%rip)
	movl	_ZN2v88internal21FLAG_interrupt_budgetE(%rip), %eax
	je	.L5263
	movl	_ZN2v88internal42FLAG_budget_for_feedback_vector_allocationE(%rip), %eax
.L5263:
	movl	%eax, 15(%r12)
	jmp	.L5283
.L5286:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21587:
	.size	_ZN2v88internal10JSFunction21ClearTypeFeedbackInfoEv, .-_ZN2v88internal10JSFunction21ClearTypeFeedbackInfoEv
	.section	.text._ZN2v88internal14JSGlobalObject22InvalidatePropertyCellENS0_6HandleIS1_EENS2_INS0_4NameEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14JSGlobalObject22InvalidatePropertyCellENS0_6HandleIS1_EENS2_INS0_4NameEEE
	.type	_ZN2v88internal14JSGlobalObject22InvalidatePropertyCellENS0_6HandleIS1_EENS2_INS0_4NameEEE, @function
_ZN2v88internal14JSGlobalObject22InvalidatePropertyCellENS0_6HandleIS1_EENS2_INS0_4NameEEE:
.LFB21588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	-1(%rax), %r12
	cmpb	$0, _ZN2v88internal26FLAG_trace_prototype_usersE(%rip)
	jne	.L5307
.L5289:
	movq	63(%r12), %rdi
	testb	$1, %dil
	jne	.L5308
.L5291:
	movq	0(%r13), %rax
	movq	%rax, %rdx
	movq	7(%rax), %r12
	andq	$-262144, %rdx
	movq	24(%rdx), %r14
	movq	3520(%r14), %rdi
	subq	$37592, %r14
	testq	%rdi, %rdi
	je	.L5293
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	movq	%rax, %r15
.L5294:
	movq	0(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r14
	movq	(%rbx), %rax
	movl	7(%rax), %edx
	subq	$37592, %r14
	testb	$1, %dl
	jne	.L5296
	shrl	$2, %edx
.L5297:
	movslq	35(%r12), %rcx
	movq	88(%r14), %rdi
	subq	$1, %r12
	movl	$1, %esi
	subl	$1, %ecx
	andl	%ecx, %edx
	jmp	.L5300
	.p2align 4,,10
	.p2align 3
.L5309:
	movq	(%rbx), %r8
	cmpq	%r8, 7(%rax)
	je	.L5299
	addl	%esi, %edx
	addl	$1, %esi
	andl	%ecx, %edx
.L5300:
	leal	56(,%rdx,8), %eax
	cltq
	movq	(%rax,%r12), %rax
	cmpq	%rax, %rdi
	jne	.L5309
.L5288:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5310
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5299:
	.cfi_restore_state
	cmpl	$-1, %edx
	je	.L5288
	movq	0(%r13), %rax
	movq	%r15, %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal12PropertyCell15InvalidateEntryEPNS0_7IsolateENS0_6HandleINS0_16GlobalDictionaryEEEi@PLT
	jmp	.L5288
	.p2align 4,,10
	.p2align 3
.L5296:
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movl	%eax, %edx
	jmp	.L5297
	.p2align 4,,10
	.p2align 3
.L5293:
	movq	41088(%r14), %r15
	cmpq	41096(%r14), %r15
	je	.L5311
.L5295:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r14)
	movq	%r12, (%r15)
	jmp	.L5294
	.p2align 4,,10
	.p2align 3
.L5308:
	movq	-1(%rdi), %rax
	cmpw	$151, 11(%rax)
	jne	.L5291
	movabsq	$4294967296, %rdx
	leaq	7(%rdi), %rsi
	movq	%rdx, 7(%rdi)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	jmp	.L5291
	.p2align 4,,10
	.p2align 3
.L5307:
	movq	%r12, %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5289
	.p2align 4,,10
	.p2align 3
.L5311:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r15
	jmp	.L5295
.L5310:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21588:
	.size	_ZN2v88internal14JSGlobalObject22InvalidatePropertyCellENS0_6HandleIS1_EENS2_INS0_4NameEEE, .-_ZN2v88internal14JSGlobalObject22InvalidatePropertyCellENS0_6HandleIS1_EENS2_INS0_4NameEEE
	.section	.text._ZN2v88internal14JSGlobalObject23EnsureEmptyPropertyCellENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_16PropertyCellTypeEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14JSGlobalObject23EnsureEmptyPropertyCellENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_16PropertyCellTypeEPi
	.type	_ZN2v88internal14JSGlobalObject23EnsureEmptyPropertyCellENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_16PropertyCellTypeEPi, @function
_ZN2v88internal14JSGlobalObject23EnsureEmptyPropertyCellENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_16PropertyCellTypeEPi:
.LFB21589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	%edx, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %rdx
	movq	7(%rax), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %r13
	movq	3520(%r13), %rdi
	subq	$37592, %r13
	testq	%rdi, %rdi
	je	.L5313
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-80(%rbp), %r9
	movq	(%rax), %rsi
	movq	%rax, %r14
.L5314:
	movq	(%r15), %rax
	movl	7(%rax), %r12d
	testb	$1, %r12b
	jne	.L5316
	shrl	$2, %r12d
.L5317:
	movslq	35(%rsi), %rcx
	movq	88(%r13), %r8
	subq	$1, %rsi
	movl	$1, %edi
	subl	$1, %ecx
	andl	%ecx, %r12d
	jmp	.L5320
	.p2align 4,,10
	.p2align 3
.L5336:
	movq	(%r15), %r10
	cmpq	%r10, 7(%rdx)
	je	.L5319
	addl	%edi, %r12d
	addl	$1, %edi
	andl	%ecx, %r12d
.L5320:
	leal	56(,%r12,8), %eax
	cltq
	movq	(%rax,%rsi), %rdx
	cmpq	%rdx, %r8
	jne	.L5336
.L5321:
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE@PLT
	movl	-68(%rbp), %r8d
	movq	-80(%rbp), %r9
	movq	%r15, %rdx
	movq	%rax, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	sall	$6, %r8d
	call	_ZN2v88internal18BaseNameDictionaryINS0_16GlobalDictionaryENS0_21GlobalDictionaryShapeEE3AddEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_4NameEEENS7_INS0_6ObjectEEENS0_15PropertyDetailsEPi@PLT
	movq	(%rbx), %rdx
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE
	movq	%r12, %rax
.L5327:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L5337
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5319:
	.cfi_restore_state
	cmpl	$-1, %r12d
	je	.L5321
	testq	%r9, %r9
	je	.L5322
	movl	%r12d, (%r9)
.L5322:
	movq	(%r14), %rdx
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L5323
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L5324:
	movslq	19(%rsi), %rdx
	leaq	15(%rsi), %rcx
	shrl	$6, %edx
	andl	$3, %edx
	cmpl	$1, %edx
	je	.L5338
.L5326:
	movl	-68(%rbp), %ebx
	sall	$7, %ebx
	sarl	%ebx
	salq	$32, %rbx
	movq	%rbx, (%rcx)
	jmp	.L5327
	.p2align 4,,10
	.p2align 3
.L5323:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L5339
.L5325:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L5324
	.p2align 4,,10
	.p2align 3
.L5316:
	leaq	-64(%rbp), %rdi
	movq	%r9, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %rsi
	movl	%eax, %r12d
	jmp	.L5317
	.p2align 4,,10
	.p2align 3
.L5313:
	movq	41088(%r13), %r14
	cmpq	%r14, 41096(%r13)
	je	.L5340
.L5315:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L5314
	.p2align 4,,10
	.p2align 3
.L5338:
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12PropertyCell15InvalidateEntryEPNS0_7IsolateENS0_6HandleINS0_16GlobalDictionaryEEEi@PLT
	movq	(%rax), %rcx
	addq	$15, %rcx
	jmp	.L5326
	.p2align 4,,10
	.p2align 3
.L5340:
	movq	%r13, %rdi
	movq	%rsi, -80(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L5315
	.p2align 4,,10
	.p2align 3
.L5339:
	movq	%r13, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	jmp	.L5325
.L5337:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21589:
	.size	_ZN2v88internal14JSGlobalObject23EnsureEmptyPropertyCellENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_16PropertyCellTypeEPi, .-_ZN2v88internal14JSGlobalObject23EnsureEmptyPropertyCellENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_16PropertyCellTypeEPi
	.section	.text._ZN2v88internal6JSDate16CurrentTimeValueEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6JSDate16CurrentTimeValueEPNS0_7IsolateE
	.type	_ZN2v88internal6JSDate16CurrentTimeValueEPNS0_7IsolateE, @function
_ZN2v88internal6JSDate16CurrentTimeValueEPNS0_7IsolateE:
.LFB21594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpb	$0, _ZN2v88internal30FLAG_log_internal_timer_eventsE(%rip)
	jne	.L5350
.L5343:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*128(%rax)
	movsd	.LC117(%rip), %xmm3
	movsd	.LC98(%rip), %xmm4
	movapd	%xmm0, %xmm2
	movapd	%xmm0, %xmm1
	andpd	%xmm3, %xmm2
	ucomisd	%xmm2, %xmm4
	jbe	.L5345
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm2, %xmm2
	movsd	.LC118(%rip), %xmm4
	andnpd	%xmm1, %xmm3
	cvtsi2sdq	%rax, %xmm2
	movapd	%xmm2, %xmm5
	cmpnlesd	%xmm0, %xmm5
	movapd	%xmm5, %xmm0
	andpd	%xmm4, %xmm0
	subsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm0
	orpd	%xmm3, %xmm0
.L5345:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5350:
	.cfi_restore_state
	movq	41016(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L5343
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger16CurrentTimeEventEv@PLT
	jmp	.L5343
	.cfi_endproc
.LFE21594:
	.size	_ZN2v88internal6JSDate16CurrentTimeValueEPNS0_7IsolateE, .-_ZN2v88internal6JSDate16CurrentTimeValueEPNS0_7IsolateE
	.section	.text._ZN2v88internal6JSDate11GetUTCFieldENS1_10FieldIndexEdPNS0_9DateCacheE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6JSDate11GetUTCFieldENS1_10FieldIndexEdPNS0_9DateCacheE
	.type	_ZN2v88internal6JSDate11GetUTCFieldENS1_10FieldIndexEdPNS0_9DateCacheE, @function
_ZN2v88internal6JSDate11GetUTCFieldENS1_10FieldIndexEdPNS0_9DateCacheE:
.LFB21597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	ucomisd	%xmm0, %xmm0
	jp	.L5375
	cvttsd2siq	%xmm0, %r12
	movl	%esi, %ebx
	movq	%rdx, %r13
	cmpl	$21, %esi
	je	.L5376
	testq	%r12, %r12
	leaq	-86399999(%r12), %rcx
	movabsq	$7164004856975580295, %rdx
	cmovns	%r12, %rcx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	movq	%rdx, %rax
	sarq	$25, %rax
	subq	%rcx, %rax
	movq	%rax, %rsi
	cmpl	$14, %ebx
	je	.L5377
	cmpl	$13, %ebx
	jle	.L5378
	imull	$-86400000, %eax, %eax
	subl	$15, %ebx
	addl	%r12d, %eax
	cmpl	$5, %ebx
	ja	.L5362
	leaq	.L5364(%rip), %rcx
	movslq	(%rcx,%rbx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal6JSDate11GetUTCFieldENS1_10FieldIndexEdPNS0_9DateCacheE,"a",@progbits
	.align 4
	.align 4
.L5364:
	.long	.L5369-.L5364
	.long	.L5368-.L5364
	.long	.L5367-.L5364
	.long	.L5366-.L5364
	.long	.L5365-.L5364
	.long	.L5374-.L5364
	.section	.text._ZN2v88internal6JSDate11GetUTCFieldENS1_10FieldIndexEdPNS0_9DateCacheE
	.p2align 4,,10
	.p2align 3
.L5377:
	addl	$4, %esi
	movslq	%esi, %rax
	movl	%esi, %edx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %edx
	shrq	$32, %rax
	addl	%esi, %eax
	sarl	$2, %eax
	subl	%edx, %eax
	leal	0(,%rax,8), %edx
	subl	%eax, %edx
	subl	%edx, %esi
	movl	%esi, %eax
	leal	7(%rsi), %edx
	cmovs	%edx, %eax
	salq	$32, %rax
.L5353:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L5379
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5367:
	.cfi_restore_state
	movslq	%eax, %rdx
	imulq	$274877907, %rdx, %rdx
	sarq	$38, %rdx
.L5373:
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movslq	%edx, %rdx
	imulq	$-2004318071, %rdx, %rdx
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrq	$32, %rdx
	addl	%eax, %edx
	sarl	$5, %edx
	subl	%ecx, %edx
	imull	$60, %edx, %edx
	subl	%edx, %eax
.L5374:
	salq	$32, %rax
	jmp	.L5353
	.p2align 4,,10
	.p2align 3
.L5365:
	movq	%rsi, %rax
	salq	$32, %rax
	jmp	.L5353
	.p2align 4,,10
	.p2align 3
.L5366:
	movslq	%eax, %rdx
	movl	%eax, %ecx
	imulq	$274877907, %rdx, %rdx
	sarl	$31, %ecx
	sarq	$38, %rdx
	subl	%ecx, %edx
	imull	$1000, %edx, %edx
	subl	%edx, %eax
	salq	$32, %rax
	jmp	.L5353
	.p2align 4,,10
	.p2align 3
.L5368:
	movslq	%eax, %rdx
	imulq	$1172812403, %rdx, %rdx
	sarq	$46, %rdx
	jmp	.L5373
	.p2align 4,,10
	.p2align 3
.L5369:
	movslq	%eax, %rdx
	sarl	$31, %eax
	imulq	$1250999897, %rdx, %rdx
	sarq	$52, %rdx
	subl	%eax, %edx
	movl	%edx, %eax
	salq	$32, %rax
	jmp	.L5353
	.p2align 4,,10
	.p2align 3
.L5376:
	movq	(%rdi), %rax
	movl	$74, %esi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	0(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$1, %edx
	call	*24(%rax)
	movabsq	$5037190915060954895, %rdx
	movslq	%eax, %rcx
	negq	%rcx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	movq	%rdx, %rax
	sarq	$14, %rax
	subq	%rcx, %rax
	salq	$32, %rax
	jmp	.L5353
	.p2align 4,,10
	.p2align 3
.L5378:
	leaq	-48(%rbp), %rcx
	leaq	-52(%rbp), %rdx
	movq	%r13, %rdi
	leaq	-44(%rbp), %r8
	call	_ZN2v88internal9DateCache20YearMonthDayFromDaysEiPiS2_S2_@PLT
	cmpl	$11, %ebx
	je	.L5380
	cmpl	$12, %ebx
	je	.L5381
	movslq	-44(%rbp), %rax
	salq	$32, %rax
	jmp	.L5353
	.p2align 4,,10
	.p2align 3
.L5380:
	movslq	-52(%rbp), %rax
	salq	$32, %rax
	jmp	.L5353
	.p2align 4,,10
	.p2align 3
.L5375:
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36504(%rax), %rax
	jmp	.L5353
	.p2align 4,,10
	.p2align 3
.L5381:
	movslq	-48(%rbp), %rax
	salq	$32, %rax
	jmp	.L5353
.L5379:
	call	__stack_chk_fail@PLT
.L5362:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21597:
	.size	_ZN2v88internal6JSDate11GetUTCFieldENS1_10FieldIndexEdPNS0_9DateCacheE, .-_ZN2v88internal6JSDate11GetUTCFieldENS1_10FieldIndexEdPNS0_9DateCacheE
	.section	.text._ZN2v88internal6JSDate8GetFieldEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6JSDate8GetFieldEmm
	.type	_ZN2v88internal6JSDate8GetFieldEmm, @function
_ZN2v88internal6JSDate8GetFieldEmm:
.LFB21595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	sarq	$32, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %rax
	movq	%rdi, -64(%rbp)
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	subq	$37592, %rdx
	movq	41240(%rdx), %r13
	cmpq	$7, %rbx
	jg	.L5383
	movq	87(%rdi), %rax
	cmpq	8(%r13), %rax
	je	.L5384
	testb	$1, %al
	jne	.L5384
	movq	23(%rdi), %rax
	testb	$1, %al
	jne	.L5385
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L5386:
	cvttsd2siq	%xmm0, %r12
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	call	*24(%rax)
	movq	%r13, %rdi
	leaq	-68(%rbp), %r8
	movabsq	$7164004856975580295, %rdx
	movslq	%eax, %r14
	leaq	(%r14,%r12), %rcx
	testq	%rcx, %rcx
	leaq	-86399999(%rcx), %rsi
	cmovns	%rcx, %rsi
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%rdx
	sarq	$25, %rdx
	subq	%rsi, %rdx
	imull	$-86400000, %edx, %r14d
	movq	%rdx, %r12
	leaq	-76(%rbp), %rdx
	movl	%r12d, %esi
	addl	%ecx, %r14d
	leaq	-72(%rbp), %rcx
	call	_ZN2v88internal9DateCache20YearMonthDayFromDaysEiPiS2_S2_@PLT
	leal	4(%r12), %eax
	movq	8(%r13), %r13
	movslq	%eax, %r12
	cltd
	imulq	$-1840700269, %r12, %r12
	shrq	$32, %r12
	addl	%eax, %r12d
	sarl	$2, %r12d
	subl	%edx, %r12d
	leal	0(,%r12,8), %edx
	subl	%r12d, %edx
	subl	%edx, %eax
	movl	%eax, %r12d
	leal	7(%rax), %eax
	cmovs	%eax, %r12d
	movslq	%r14d, %rax
	sarl	$31, %r14d
	imulq	$1172812403, %rax, %rdx
	imulq	$1250999897, %rax, %rcx
	imulq	$274877907, %rax, %rax
	sarq	$46, %rdx
	subl	%r14d, %edx
	sarq	$52, %rcx
	movslq	%edx, %r15
	sarq	$38, %rax
	movl	%edx, %esi
	subl	%r14d, %ecx
	imulq	$-2004318071, %r15, %r15
	subl	%r14d, %eax
	sarl	$31, %esi
	movslq	%eax, %r14
	imulq	$-2004318071, %r14, %r14
	shrq	$32, %r15
	addl	%edx, %r15d
	sarl	$5, %r15d
	shrq	$32, %r14
	subl	%esi, %r15d
	addl	%eax, %r14d
	imull	$60, %r15d, %r15d
	sarl	$5, %r14d
	subl	%r15d, %edx
	movl	%edx, %r15d
	cltd
	subl	%edx, %r14d
	imull	$60, %r14d, %r14d
	subl	%r14d, %eax
	movl	%eax, %r14d
	movq	-64(%rbp), %rax
	movq	%r13, 87(%rax)
	movq	-64(%rbp), %rdi
	testb	$1, %r13b
	je	.L5411
	movq	%r13, %r8
	leaq	87(%rdi), %rsi
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -88(%rbp)
	testl	$262144, %eax
	je	.L5390
	movq	%r13, %rdx
	movl	%ecx, -92(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	-64(%rbp), %rdi
	movl	-92(%rbp), %ecx
	movq	8(%r8), %rax
	leaq	87(%rdi), %rsi
.L5390:
	testb	$24, %al
	je	.L5411
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L5426
	.p2align 4,,10
	.p2align 3
.L5411:
	movslq	-76(%rbp), %rax
	salq	$32, %r12
	salq	$32, %rcx
	salq	$32, %r15
	salq	$32, %r14
	salq	$32, %rax
	movq	%rax, 31(%rdi)
	movslq	-72(%rbp), %rax
	movq	-64(%rbp), %rdx
	salq	$32, %rax
	movq	%rax, 39(%rdx)
	movslq	-68(%rbp), %rax
	movq	-64(%rbp), %rdx
	salq	$32, %rax
	movq	%rax, 47(%rdx)
	movq	-64(%rbp), %rax
	movq	%r12, 55(%rax)
	movq	-64(%rbp), %rax
	movq	%rcx, 63(%rax)
	movq	-64(%rbp), %rax
	movq	%r15, 71(%rax)
	movq	-64(%rbp), %rax
	movq	%r14, 79(%rax)
.L5384:
	cmpl	$7, %ebx
	ja	.L5392
	leaq	.L5394(%rip), %rdx
	movl	%ebx, %ebx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6JSDate8GetFieldEmm,"a",@progbits
	.align 4
	.align 4
.L5394:
	.long	.L5392-.L5394
	.long	.L5400-.L5394
	.long	.L5399-.L5394
	.long	.L5398-.L5394
	.long	.L5397-.L5394
	.long	.L5396-.L5394
	.long	.L5395-.L5394
	.long	.L5393-.L5394
	.section	.text._ZN2v88internal6JSDate8GetFieldEmm
	.p2align 4,,10
	.p2align 3
.L5383:
	movq	23(%rdi), %rax
	movq	%rax, %rcx
	notq	%rcx
	andl	$1, %ecx
	cmpq	$10, %rbx
	jle	.L5402
	testb	%cl, %cl
	jne	.L5427
	movsd	7(%rax), %xmm0
.L5404:
	leaq	-64(%rbp), %rdi
	movq	%r13, %rdx
	movl	%ebx, %esi
	call	_ZN2v88internal6JSDate11GetUTCFieldENS1_10FieldIndexEdPNS0_9DateCacheE
.L5382:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L5428
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5402:
	.cfi_restore_state
	testb	%cl, %cl
	jne	.L5429
	movsd	7(%rax), %xmm0
.L5406:
	ucomisd	%xmm0, %xmm0
	jp	.L5430
	cvttsd2siq	%xmm0, %r12
	movq	0(%r13), %rax
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	*24(%rax)
	movslq	%eax, %rdx
	addq	%rdx, %r12
	movabsq	$7164004856975580295, %rdx
	leaq	-86399999(%r12), %rcx
	cmovns	%r12, %rcx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	sarq	$25, %rdx
	subq	%rcx, %rdx
	cmpl	$9, %ebx
	je	.L5425
	imull	$-86400000, %edx, %edx
	addl	%r12d, %edx
	movq	%rdx, %rax
	movslq	%edx, %rcx
	salq	$32, %rax
	cmpl	$8, %ebx
	jne	.L5382
	imulq	$274877907, %rcx, %rax
	movl	%edx, %ecx
	sarl	$31, %ecx
	sarq	$38, %rax
	subl	%ecx, %eax
	imull	$1000, %eax, %eax
	subl	%eax, %edx
.L5425:
	movq	%rdx, %rax
	salq	$32, %rax
	jmp	.L5382
	.p2align 4,,10
	.p2align 3
.L5427:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	jmp	.L5404
	.p2align 4,,10
	.p2align 3
.L5429:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	jmp	.L5406
	.p2align 4,,10
	.p2align 3
.L5400:
	movq	-64(%rbp), %rax
	movq	31(%rax), %rax
	jmp	.L5382
	.p2align 4,,10
	.p2align 3
.L5399:
	movq	-64(%rbp), %rax
	movq	39(%rax), %rax
	jmp	.L5382
	.p2align 4,,10
	.p2align 3
.L5398:
	movq	-64(%rbp), %rax
	movq	47(%rax), %rax
	jmp	.L5382
	.p2align 4,,10
	.p2align 3
.L5397:
	movq	-64(%rbp), %rax
	movq	55(%rax), %rax
	jmp	.L5382
	.p2align 4,,10
	.p2align 3
.L5396:
	movq	-64(%rbp), %rax
	movq	63(%rax), %rax
	jmp	.L5382
	.p2align 4,,10
	.p2align 3
.L5395:
	movq	-64(%rbp), %rax
	movq	71(%rax), %rax
	jmp	.L5382
	.p2align 4,,10
	.p2align 3
.L5393:
	movq	-64(%rbp), %rax
	movq	79(%rax), %rax
	jmp	.L5382
	.p2align 4,,10
	.p2align 3
.L5385:
	movsd	7(%rax), %xmm0
	jmp	.L5386
	.p2align 4,,10
	.p2align 3
.L5426:
	movq	%r13, %rdx
	movl	%ecx, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdi
	movl	-88(%rbp), %ecx
	jmp	.L5411
	.p2align 4,,10
	.p2align 3
.L5430:
	movq	1088(%rdx), %rax
	jmp	.L5382
.L5428:
	call	__stack_chk_fail@PLT
.L5392:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21595:
	.size	_ZN2v88internal6JSDate8GetFieldEmm, .-_ZN2v88internal6JSDate8GetFieldEmm
	.section	.text._ZN2v88internal6JSDate8SetValueENS0_6ObjectEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6JSDate8SetValueENS0_6ObjectEb
	.type	_ZN2v88internal6JSDate8SetValueENS0_6ObjectEb, @function
_ZN2v88internal6JSDate8SetValueENS0_6ObjectEb:
.LFB21599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rsi, 23(%rax)
	movq	(%rdi), %rdi
	testb	$1, %sil
	je	.L5437
	movq	%rsi, %r12
	leaq	23(%rdi), %rsi
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L5445
	testb	$24, %al
	je	.L5437
.L5447:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L5446
	.p2align 4,,10
	.p2align 3
.L5437:
	testb	%r13b, %r13b
	je	.L5435
	movq	%rdi, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36504(%rax), %rax
	movq	%rax, 87(%rdi)
	movq	(%rbx), %rdx
	movq	%rax, 31(%rdx)
	movq	(%rbx), %rdx
	movq	%rax, 39(%rdx)
	movq	(%rbx), %rdx
	movq	%rax, 47(%rdx)
	movq	(%rbx), %rdx
	movq	%rax, 63(%rdx)
	movq	(%rbx), %rdx
	movq	%rax, 71(%rdx)
	movq	(%rbx), %rdx
	movq	%rax, 79(%rdx)
	movq	(%rbx), %rdx
	movq	%rax, 55(%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5435:
	.cfi_restore_state
	movabsq	$-4294967296, %rax
	movq	%rax, 87(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5445:
	.cfi_restore_state
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r14), %rax
	leaq	23(%rdi), %rsi
	testb	$24, %al
	jne	.L5447
	jmp	.L5437
	.p2align 4,,10
	.p2align 3
.L5446:
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	jmp	.L5437
	.cfi_endproc
.LFE21599:
	.size	_ZN2v88internal6JSDate8SetValueENS0_6ObjectEb, .-_ZN2v88internal6JSDate8SetValueENS0_6ObjectEb
	.section	.text._ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd
	.type	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd, @function
_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd:
.LFB21598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movsd	%xmm0, -40(%rbp)
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movsd	-40(%rbp), %xmm0
	xorl	%edx, %edx
	leaq	-32(%rbp), %rdi
	movq	%rax, %r12
	movq	(%rbx), %rax
	ucomisd	%xmm0, %xmm0
	movq	%rax, -32(%rbp)
	movq	(%r12), %rsi
	setp	%dl
	call	_ZN2v88internal6JSDate8SetValueENS0_6ObjectEb
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5451
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5451:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21598:
	.size	_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd, .-_ZN2v88internal6JSDate8SetValueENS0_6HandleIS1_EEd
	.section	.text._ZN2v88internal6JSDate3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6JSDate3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEEd
	.type	_ZN2v88internal6JSDate3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEEd, @function
_ZN2v88internal6JSDate3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEEd:
.LFB21590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movsd	%xmm0, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	leaq	-37592(%rax), %r12
	call	_ZN2v88internal8JSObject3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEENS2_INS0_14AllocationSiteEEE
	movsd	-56(%rbp), %xmm0
	testq	%rax, %rax
	je	.L5481
	comisd	.LC121(%rip), %xmm0
	movq	%rax, %rbx
	jnb	.L5482
.L5477:
	movsd	.LC120(%rip), %xmm0
	movl	$1, %r13d
.L5455:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdx
	leaq	-48(%rbp), %rdi
	movq	%rdx, -48(%rbp)
	movq	(%rax), %rsi
	movl	%r13d, %edx
	call	_ZN2v88internal6JSDate8SetValueENS0_6ObjectEb
	movq	%rbx, %rax
.L5454:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5483
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5482:
	.cfi_restore_state
	movsd	.LC122(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L5477
	ucomisd	%xmm0, %xmm0
	jp	.L5470
	movq	.LC117(%rip), %xmm2
	movsd	.LC123(%rip), %xmm3
	movapd	%xmm0, %xmm1
	andpd	%xmm2, %xmm1
	ucomisd	%xmm1, %xmm3
	pxor	%xmm1, %xmm1
	jb	.L5461
	ucomisd	%xmm1, %xmm0
	jnp	.L5484
.L5473:
	comisd	%xmm1, %xmm0
	movapd	%xmm0, %xmm4
	movsd	.LC98(%rip), %xmm5
	movapd	%xmm0, %xmm3
	andpd	%xmm2, %xmm4
	jb	.L5479
	ucomisd	%xmm4, %xmm5
	ja	.L5485
.L5466:
	addsd	%xmm1, %xmm3
	xorl	%r13d, %r13d
	ucomisd	%xmm3, %xmm3
	movapd	%xmm3, %xmm0
	setp	%r13b
	jmp	.L5455
	.p2align 4,,10
	.p2align 3
.L5481:
	xorl	%eax, %eax
	jmp	.L5454
	.p2align 4,,10
	.p2align 3
.L5484:
	jne	.L5473
.L5461:
	addsd	%xmm1, %xmm0
	xorl	%r13d, %r13d
	ucomisd	%xmm0, %xmm0
	setp	%r13b
	jmp	.L5455
	.p2align 4,,10
	.p2align 3
.L5485:
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm4, %xmm4
	movsd	.LC118(%rip), %xmm5
	andnpd	%xmm0, %xmm2
	cvtsi2sdq	%rax, %xmm4
	movapd	%xmm4, %xmm6
	cmpnlesd	%xmm0, %xmm6
	movapd	%xmm6, %xmm3
	andpd	%xmm5, %xmm3
	subsd	%xmm3, %xmm4
	movapd	%xmm4, %xmm3
	orpd	%xmm2, %xmm3
	jmp	.L5466
	.p2align 4,,10
	.p2align 3
.L5470:
	xorl	%r13d, %r13d
	pxor	%xmm0, %xmm0
	jmp	.L5455
	.p2align 4,,10
	.p2align 3
.L5479:
	ucomisd	%xmm4, %xmm5
	jbe	.L5466
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm4, %xmm4
	movsd	.LC118(%rip), %xmm5
	andnpd	%xmm0, %xmm2
	cvtsi2sdq	%rax, %xmm4
	cmpnlesd	%xmm4, %xmm3
	andpd	%xmm5, %xmm3
	addsd	%xmm4, %xmm3
	orpd	%xmm2, %xmm3
	jmp	.L5466
.L5483:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21590:
	.size	_ZN2v88internal6JSDate3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEEd, .-_ZN2v88internal6JSDate3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEEd
	.section	.text._ZN2v88internal15JSMessageObject30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JSMessageObject30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal15JSMessageObject30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal15JSMessageObject30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB21601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	55(%rax), %rsi
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37504(%rax), %rsi
	je	.L5486
	movq	%rdi, %r12
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5488
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L5489:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	0(%r13), %rax
	movq	%rax, -56(%rbp)
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L5491
.L5494:
	movq	-56(%rbp), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L5517
.L5492:
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal18SharedFunctionInfo7GetCodeEv@PLT
.L5502:
	movq	%rax, -48(%rbp)
	movq	(%rbx), %rax
	leaq	-48(%rbp), %rdi
	movslq	67(%rax), %rsi
	call	_ZN2v88internal12AbstractCode14SourcePositionEi@PLT
	movq	(%rbx), %rdx
	movq	%rax, %rcx
	addl	$1, %eax
	salq	$32, %rcx
	salq	$32, %rax
	movq	%rcx, 71(%rdx)
	movq	(%rbx), %rdx
	movq	%rax, 79(%rdx)
	movq	(%rbx), %r13
	movq	88(%r12), %r12
	movq	%r12, 55(%r13)
	leaq	55(%r13), %r14
	testb	$1, %r12b
	je	.L5486
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L5504
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L5504:
	testb	$24, %al
	je	.L5486
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L5518
	.p2align 4,,10
	.p2align 3
.L5486:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5519
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5488:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L5520
.L5490:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L5489
	.p2align 4,,10
	.p2align 3
.L5491:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L5494
.L5497:
	movq	-56(%rbp), %rdx
	movq	31(%rdx), %rax
	testb	$1, %al
	jne	.L5521
.L5495:
	movq	7(%rdx), %rax
	testb	$1, %al
	jne	.L5522
.L5501:
	movq	-56(%rbp), %rax
	movq	7(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L5502
	.p2align 4,,10
	.p2align 3
.L5518:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L5486
	.p2align 4,,10
	.p2align 3
.L5522:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L5501
	movq	-56(%rbp), %rax
	movq	7(%rax), %rax
	jmp	.L5502
	.p2align 4,,10
	.p2align 3
.L5517:
	movq	-1(%rax), %rax
	cmpw	$91, 11(%rax)
	jne	.L5492
	jmp	.L5497
	.p2align 4,,10
	.p2align 3
.L5521:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L5495
	movq	39(%rax), %rcx
	testb	$1, %cl
	je	.L5495
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L5495
	movq	31(%rax), %rax
	jmp	.L5502
	.p2align 4,,10
	.p2align 3
.L5520:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L5490
.L5519:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21601:
	.size	_ZN2v88internal15JSMessageObject30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal15JSMessageObject30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZNK2v88internal15JSMessageObject13GetLineNumberEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JSMessageObject13GetLineNumberEv
	.type	_ZNK2v88internal15JSMessageObject13GetLineNumberEv, @function
_ZNK2v88internal15JSMessageObject13GetLineNumberEv:
.LFB21602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	cmpl	$-1, 75(%rax)
	je	.L5529
	movq	%rax, %rdx
	movq	%rdi, %rbx
	movq	39(%rax), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %r12
	movq	3520(%r12), %rdi
	subq	$37592, %r12
	testq	%rdi, %rdi
	je	.L5526
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L5527:
	movq	(%rbx), %rax
	pcmpeqd	%xmm0, %xmm0
	leaq	-48(%rbp), %rdx
	movl	$1, %ecx
	movaps	%xmm0, -48(%rbp)
	movslq	75(%rax), %rsi
	call	_ZN2v88internal6Script15GetPositionInfoENS0_6HandleIS1_EEiPNS1_12PositionInfoENS1_10OffsetFlagE@PLT
	testb	%al, %al
	je	.L5529
	movl	-48(%rbp), %eax
	addl	$1, %eax
.L5523:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5535
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5526:
	.cfi_restore_state
	movq	41088(%r12), %rdi
	cmpq	41096(%r12), %rdi
	je	.L5536
.L5528:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L5527
	.p2align 4,,10
	.p2align 3
.L5529:
	xorl	%eax, %eax
	jmp	.L5523
	.p2align 4,,10
	.p2align 3
.L5536:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L5528
.L5535:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21602:
	.size	_ZNK2v88internal15JSMessageObject13GetLineNumberEv, .-_ZNK2v88internal15JSMessageObject13GetLineNumberEv
	.section	.text._ZNK2v88internal15JSMessageObject15GetColumnNumberEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JSMessageObject15GetColumnNumberEv
	.type	_ZNK2v88internal15JSMessageObject15GetColumnNumberEv, @function
_ZNK2v88internal15JSMessageObject15GetColumnNumberEv:
.LFB21603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	cmpl	$-1, 75(%rax)
	je	.L5543
	movq	%rax, %rdx
	movq	%rdi, %r12
	movq	39(%rax), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L5540
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L5541:
	movq	(%r12), %rax
	pcmpeqd	%xmm0, %xmm0
	leaq	-48(%rbp), %rdx
	movl	$1, %ecx
	movaps	%xmm0, -48(%rbp)
	movslq	75(%rax), %rsi
	call	_ZN2v88internal6Script15GetPositionInfoENS0_6HandleIS1_EEiPNS1_12PositionInfoENS1_10OffsetFlagE@PLT
	testb	%al, %al
	je	.L5543
	movl	-44(%rbp), %eax
.L5537:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5549
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5540:
	.cfi_restore_state
	movq	41088(%rbx), %rdi
	cmpq	41096(%rbx), %rdi
	je	.L5550
.L5542:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L5541
	.p2align 4,,10
	.p2align 3
.L5550:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L5542
	.p2align 4,,10
	.p2align 3
.L5543:
	movl	$-1, %eax
	jmp	.L5537
.L5549:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21603:
	.size	_ZNK2v88internal15JSMessageObject15GetColumnNumberEv, .-_ZNK2v88internal15JSMessageObject15GetColumnNumberEv
	.section	.text._ZNK2v88internal15JSMessageObject13GetSourceLineEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JSMessageObject13GetSourceLineEv
	.type	_ZNK2v88internal15JSMessageObject13GetSourceLineEv, @function
_ZNK2v88internal15JSMessageObject13GetSourceLineEv:
.LFB21604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	movq	39(%rdx), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %r12
	movq	3520(%r12), %rdi
	subq	$37592, %r12
	testq	%rdi, %rdi
	je	.L5552
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L5553:
	cmpl	$3, 51(%rsi)
	jne	.L5555
.L5565:
	leaq	128(%r12), %rax
.L5556:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5566
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5555:
	.cfi_restore_state
	movq	0(%r13), %rax
	pcmpeqd	%xmm0, %xmm0
	leaq	-64(%rbp), %rdx
	movq	%rbx, %rdi
	movaps	%xmm0, -64(%rbp)
	movl	$1, %ecx
	movslq	75(%rax), %rsi
	call	_ZN2v88internal6Script15GetPositionInfoENS0_6HandleIS1_EEiPNS1_12PositionInfoENS1_10OffsetFlagE@PLT
	testb	%al, %al
	je	.L5565
	movq	(%rbx), %rax
	movq	41112(%r12), %rdi
	movq	7(%rax), %r13
	testq	%rdi, %rdi
	je	.L5558
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L5559:
	movl	-56(%rbp), %edx
	movl	-52(%rbp), %ecx
	testl	%edx, %edx
	jne	.L5561
	movq	(%rsi), %rax
	cmpl	11(%rax), %ecx
	je	.L5562
.L5561:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	%rax, %rsi
.L5562:
	movq	%rsi, %rax
	jmp	.L5556
	.p2align 4,,10
	.p2align 3
.L5552:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L5567
.L5554:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L5553
	.p2align 4,,10
	.p2align 3
.L5558:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L5568
.L5560:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L5559
	.p2align 4,,10
	.p2align 3
.L5567:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L5554
	.p2align 4,,10
	.p2align 3
.L5568:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L5560
.L5566:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21604:
	.size	_ZNK2v88internal15JSMessageObject13GetSourceLineEv, .-_ZNK2v88internal15JSMessageObject13GetSourceLineEv
	.section	.text._ZN2v88internal23TorqueGeneratedJSObjectINS0_8JSObjectENS0_10JSReceiverEE12set_elementsENS0_14FixedArrayBaseENS0_16WriteBarrierModeE,"axG",@progbits,_ZN2v88internal23TorqueGeneratedJSObjectINS0_8JSObjectENS0_10JSReceiverEE12set_elementsENS0_14FixedArrayBaseENS0_16WriteBarrierModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23TorqueGeneratedJSObjectINS0_8JSObjectENS0_10JSReceiverEE12set_elementsENS0_14FixedArrayBaseENS0_16WriteBarrierModeE
	.type	_ZN2v88internal23TorqueGeneratedJSObjectINS0_8JSObjectENS0_10JSReceiverEE12set_elementsENS0_14FixedArrayBaseENS0_16WriteBarrierModeE, @function
_ZN2v88internal23TorqueGeneratedJSObjectINS0_8JSObjectENS0_10JSReceiverEE12set_elementsENS0_14FixedArrayBaseENS0_16WriteBarrierModeE:
.LFB22696:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rsi, 15(%rax)
	testl	%edx, %edx
	je	.L5585
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%r12, %rax
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	notq	%rax
	leaq	15(%rdi), %rsi
	andl	$1, %eax
	cmpl	$4, %edx
	je	.L5588
	testb	%al, %al
	jne	.L5569
.L5580:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L5569
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L5569
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r12, %rdx
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L5569:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5585:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L5588:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	testb	%al, %al
	jne	.L5569
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L5580
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	leaq	15(%rdi), %rsi
	jmp	.L5580
	.cfi_endproc
.LFE22696:
	.size	_ZN2v88internal23TorqueGeneratedJSObjectINS0_8JSObjectENS0_10JSReceiverEE12set_elementsENS0_14FixedArrayBaseENS0_16WriteBarrierModeE, .-_ZN2v88internal23TorqueGeneratedJSObjectINS0_8JSObjectENS0_10JSReceiverEE12set_elementsENS0_14FixedArrayBaseENS0_16WriteBarrierModeE
	.section	.rodata._ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE0EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE.str1.1,"aMS",@progbits,1
.LC124:
	.string	"CopyForPreventExtensions"
.LC125:
	.string	"SlowPreventExtensions"
.LC126:
	.string	"SlowCopyForPreventExtensions"
	.section	.text._ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE0EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE,"axG",@progbits,_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE0EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE,comdat
	.p2align 4
	.weak	_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE0EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE
	.type	_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE0EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE, @function
_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE0EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE:
.LFB24283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, %r15
	andq	$-262144, %r15
	movq	24(%r15), %rax
	movq	-1(%rbx), %rdx
	cmpw	$1026, 11(%rdx)
	leaq	-37592(%rax), %r13
	je	.L5656
	movq	-1(%rbx), %rax
	movzbl	13(%rax), %eax
	shrb	$5, %al
	andl	$1, %eax
.L5591:
	testb	%al, %al
	je	.L5592
	movq	41112(%r13), %rdi
	movq	12464(%r13), %r15
	testq	%rdi, %rdi
	je	.L5593
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L5594:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	jne	.L5592
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	12552(%r13), %rax
	cmpq	%rax, 96(%r13)
	jne	.L5657
	cmpl	$1, %r14d
	je	.L5608
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$79, %esi
.L5652:
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L5600
	.p2align 4,,10
	.p2align 3
.L5592:
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$134217728, %edx
	jne	.L5658
.L5651:
	movl	$257, %eax
.L5600:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5659
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5593:
	.cfi_restore_state
	movq	41088(%r13), %rsi
	cmpq	%rsi, 41096(%r13)
	je	.L5660
.L5595:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rsi)
	jmp	.L5594
	.p2align 4,,10
	.p2align 3
.L5656:
	movq	-25128(%rax), %rax
	leaq	-96(%rbp), %rdi
	movq	%rdi, -128(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	24(%r15), %rdx
	movq	-128(%rbp), %rdi
	movq	%rbx, -88(%rbp)
	movq	%rax, -120(%rbp)
	subq	$37592, %rdx
	movq	$0, -80(%rbp)
	movq	%rdx, -96(%rbp)
	movl	$0, -72(%rbp)
	movb	$0, -68(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	movq	-120(%rbp), %rax
	cmpq	%rax, -88(%rbp)
	setne	%al
	jmp	.L5591
	.p2align 4,,10
	.p2align 3
.L5658:
	movq	-1(%rax), %rdx
	movzbl	14(%rdx), %edx
	shrl	$3, %edx
	subl	$8, %edx
	cmpb	$3, %dl
	jbe	.L5651
	movq	-1(%rax), %rdx
	cmpw	$1026, 11(%rdx)
	je	.L5661
	movq	-1(%rax), %rdx
	testb	$4, 13(%rdx)
	je	.L5662
.L5605:
	cmpl	$1, %r14d
	je	.L5608
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$31, %esi
	jmp	.L5652
	.p2align 4,,10
	.p2align 3
.L5657:
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L5600
	.p2align 4,,10
	.p2align 3
.L5608:
	movl	$1, %eax
	jmp	.L5600
	.p2align 4,,10
	.p2align 3
.L5660:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L5595
	.p2align 4,,10
	.p2align 3
.L5661:
	leaq	-96(%rbp), %rdi
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r12, -80(%rbp)
	movl	$0, -72(%rbp)
	movb	$0, -68(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	cmpb	$0, -68(%rbp)
	jne	.L5651
	movq	-80(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE0EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE
	jmp	.L5600
	.p2align 4,,10
	.p2align 3
.L5662:
	movq	-1(%rax), %rdx
	testb	$8, 13(%rdx)
	jne	.L5605
	movq	-1(%rax), %r14
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L5663
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L5609:
	movq	%r13, %rdi
	leaq	-96(%rbp), %rbx
	call	_ZN2v88internal3Map6UpdateEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%r13, -96(%rbp)
	movq	%rbx, %rdi
	movq	%rax, -88(%rbp)
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal19TransitionsAccessor10InitializeEv
	movq	3752(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L5611
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L5612
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r15
	movq	%rax, %r14
.L5613:
	movzbl	14(%r15), %eax
.L5655:
	shrl	$3, %eax
	xorl	%ebx, %ebx
	subl	$6, %eax
	cmpb	$5, %al
	ja	.L5664
.L5618:
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
.L5616:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$6, %eax
	cmpb	$5, %al
	jbe	.L5651
	leaq	-104(%rbp), %r14
	movq	%rdx, -104(%rbp)
	movq	%r14, %rdi
	call	_ZNK2v88internal8JSObject21HasTypedArrayElementsEPNS0_7IsolateE.isra.0
	testb	%al, %al
	jne	.L5651
	testq	%rbx, %rbx
	je	.L5624
	movq	(%r12), %rax
	movl	$4, %edx
	movq	%rax, -104(%rbp)
	movq	(%rbx), %rsi
	call	_ZN2v88internal23TorqueGeneratedJSObjectINS0_8JSObjectENS0_10JSReceiverEE12set_elementsENS0_14FixedArrayBaseENS0_16WriteBarrierModeE
.L5624:
	movq	(%r12), %rax
	movq	15(%rax), %r15
	cmpq	%r15, 1016(%r13)
	je	.L5651
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L5625
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L5626:
	movq	(%r12), %rdx
	movq	%r14, %rdi
	movq	%rdx, -104(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal8JSObject19RequireSlowElementsENS0_16NumberDictionaryE
	jmp	.L5651
.L5663:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L5665
.L5610:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%rsi)
	jmp	.L5609
.L5611:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TransitionsAccessor22CanHaveMoreTransitionsEv@PLT
	testb	%al, %al
	je	.L5617
	movq	%r14, %rsi
	leaq	3752(%r13), %rcx
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	leaq	.LC124(%rip), %r8
	movq	%r13, %rdi
	call	_ZN2v88internal3Map24CopyForPreventExtensionsEPNS0_7IsolateENS0_6HandleIS1_EENS0_18PropertyAttributesENS4_INS0_6SymbolEEEPKcb@PLT
	movq	%rax, %r14
	movq	(%rax), %rax
	movzbl	14(%rax), %eax
	jmp	.L5655
.L5612:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L5666
.L5614:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%r14)
	jmp	.L5613
.L5664:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23CreateElementDictionaryEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
	movq	%rax, %rbx
	jmp	.L5618
.L5617:
	movq	%r13, %rdi
	leaq	.LC125(%rip), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8JSObject19NormalizePropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeEiPKc
	movq	(%r12), %rax
	movq	-1(%rax), %r15
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L5619
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L5620:
	leaq	.LC126(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	(%rax), %rdx
	movq	%rax, %r15
	movl	15(%rdx), %eax
	andl	$-134217729, %eax
	movl	%eax, 15(%rdx)
	call	_ZN2v88internal23CreateElementDictionaryEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L5622
	movq	(%r14), %rax
	movq	(%r15), %rcx
	movzbl	14(%rax), %eax
	movzbl	14(%rcx), %edx
	shrl	$3, %eax
	subl	$15, %eax
	cmpb	$2, %al
	sbbl	%eax, %eax
	andb	$7, %dl
	andl	$4, %eax
	addl	$12, %eax
	movzbl	%al, %eax
	sall	$3, %eax
	orl	%edx, %eax
	movb	%al, 14(%rcx)
.L5622:
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
	jmp	.L5616
.L5665:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L5610
.L5659:
	call	__stack_chk_fail@PLT
.L5619:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L5667
.L5621:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rsi)
	jmp	.L5620
.L5625:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L5668
.L5627:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r15, (%rax)
	jmp	.L5626
.L5666:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r14
	jmp	.L5614
.L5667:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L5621
.L5668:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L5627
	.cfi_endproc
.LFE24283:
	.size	_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE0EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE, .-_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE0EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE
	.section	.rodata._ZN2v88internal8JSObject17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE.str1.1,"aMS",@progbits,1
.LC127:
	.string	"PreventExtensions"
	.section	.text._ZN2v88internal8JSObject17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE
	.type	_ZN2v88internal8JSObject17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE, @function
_ZN2v88internal8JSObject17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE:
.LFB21515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, %r15
	andq	$-262144, %r15
	movq	24(%r15), %r13
	movq	-1(%rbx), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$13, %eax
	cmpb	$1, %al
	jbe	.L5670
	call	_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE0EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE
	movl	%eax, %r15d
.L5671:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5705
	addq	$72, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5670:
	.cfi_restore_state
	movq	-1(%rbx), %rax
	cmpw	$1026, 11(%rax)
	je	.L5706
	movq	-1(%rbx), %rax
	movzbl	13(%rax), %eax
	shrb	$5, %al
	andl	$1, %eax
.L5673:
	subq	$37592, %r13
	testb	%al, %al
	je	.L5674
	movq	41112(%r13), %rdi
	movq	12464(%r13), %r15
	testq	%rdi, %rdi
	je	.L5675
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L5676:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	jne	.L5674
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	12552(%r13), %rax
	cmpq	%rax, 96(%r13)
	je	.L5701
	xorl	%r15d, %r15d
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movl	%r15d, %eax
	movb	$0, %ah
	movl	%eax, %r15d
	jmp	.L5671
	.p2align 4,,10
	.p2align 3
.L5674:
	movq	(%r12), %rax
	movl	$257, %r15d
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$134217728, %edx
	je	.L5671
	movq	-1(%rax), %rdx
	cmpw	$1026, 11(%rdx)
	je	.L5707
	movq	-1(%rax), %rdx
	testb	$4, 13(%rdx)
	je	.L5708
.L5686:
	cmpl	$1, %r14d
	jne	.L5709
.L5689:
	movl	$1, %r15d
	jmp	.L5671
	.p2align 4,,10
	.p2align 3
.L5706:
	movq	-25128(%r13), %rax
	leaq	-96(%rbp), %rdi
	movq	%rdi, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	24(%r15), %rdx
	movq	-112(%rbp), %rdi
	movq	%rbx, -88(%rbp)
	movq	%rax, -104(%rbp)
	subq	$37592, %rdx
	movq	$0, -80(%rbp)
	movq	%rdx, -96(%rbp)
	movl	$0, -72(%rbp)
	movb	$0, -68(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	movq	-104(%rbp), %rax
	cmpq	%rax, -88(%rbp)
	setne	%al
	jmp	.L5673
	.p2align 4,,10
	.p2align 3
.L5675:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L5710
.L5677:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rsi)
	jmp	.L5676
	.p2align 4,,10
	.p2align 3
.L5701:
	cmpl	$1, %r14d
	je	.L5689
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$79, %esi
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movl	%r15d, %eax
	movb	$0, %ah
	movl	%eax, %r15d
	jmp	.L5671
	.p2align 4,,10
	.p2align 3
.L5709:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$31, %esi
	movq	%r13, %rdi
	xorb	%r15b, %r15b
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movl	%r15d, %eax
	movb	$0, %ah
	movl	%eax, %r15d
	jmp	.L5671
	.p2align 4,,10
	.p2align 3
.L5707:
	leaq	-96(%rbp), %rdi
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r12, -80(%rbp)
	movl	$0, -72(%rbp)
	movb	$0, -68(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	cmpb	$0, -68(%rbp)
	jne	.L5671
	movq	-80(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal8JSObject17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE
	movl	%eax, %r15d
	jmp	.L5671
	.p2align 4,,10
	.p2align 3
.L5708:
	movq	-1(%rax), %rdx
	testb	$8, 13(%rdx)
	jne	.L5686
	movq	-1(%rax), %rdx
	movzbl	14(%rdx), %edx
	shrl	$3, %edx
	subl	$17, %edx
	cmpb	$10, %dl
	ja	.L5711
.L5687:
	movq	-1(%rax), %r14
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L5690
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L5691:
	leaq	.LC127(%rip), %rdx
	movq	%r13, %rdi
	movl	$257, %r15d
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	(%rax), %rcx
	movq	%rax, %rdx
	movl	15(%rcx), %eax
	andl	$-134217729, %eax
	movl	%eax, 15(%rcx)
	xorl	%ecx, %ecx
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
	jmp	.L5671
	.p2align 4,,10
	.p2align 3
.L5710:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L5677
.L5690:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L5712
.L5692:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%rsi)
	jmp	.L5691
.L5711:
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject17NormalizeElementsENS0_6HandleIS1_EE
	movq	(%r12), %rdx
	leaq	-96(%rbp), %rdi
	movq	%rdx, -96(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal8JSObject19RequireSlowElementsENS0_16NumberDictionaryE
	movq	(%r12), %rax
	jmp	.L5687
.L5712:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L5692
.L5705:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21515:
	.size	_ZN2v88internal8JSObject17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE, .-_ZN2v88internal8JSObject17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE
	.section	.text._ZN2v88internal10JSReceiver17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE
	.type	_ZN2v88internal10JSReceiver17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE, @function
_ZN2v88internal10JSReceiver17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE:
.LFB21456:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	je	.L5715
	jmp	_ZN2v88internal8JSObject17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE
	.p2align 4,,10
	.p2align 3
.L5715:
	jmp	_ZN2v88internal7JSProxy17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE@PLT
	.cfi_endproc
.LFE21456:
	.size	_ZN2v88internal10JSReceiver17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE, .-_ZN2v88internal10JSReceiver17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE
	.section	.text._ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_16GlobalDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE,"axG",@progbits,_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_16GlobalDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_16GlobalDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE
	.type	_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_16GlobalDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE, @function
_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_16GlobalDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE:
.LFB25900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	35(%rdx), %rax
	testq	%rax, %rax
	jle	.L5716
	movl	%ecx, %r9d
	subl	$1, %eax
	movq	%rdi, %r8
	movq	%rsi, %r13
	andl	$1, %r9d
	leaq	64(,%rax,8), %r15
	movl	$56, %ebx
	leaq	-64(%rbp), %rdi
	jmp	.L5730
	.p2align 4,,10
	.p2align 3
.L5723:
	movq	-1(%rbx,%rdx), %rax
	movl	%ecx, %esi
	leaq	-1(%rbx,%rdx), %rdx
	movslq	19(%rax), %rax
	testl	%r9d, %r9d
	je	.L5725
	testb	$1, %al
	jne	.L5736
.L5725:
	movl	%eax, %r12d
	andl	$-57, %eax
	shrl	$3, %r12d
	andl	$7, %r12d
	orl	%esi, %r12d
	sall	$3, %r12d
	orl	%eax, %r12d
	movq	(%rdx), %rax
	movl	%r12d, %esi
	movslq	19(%rax), %rdx
	shrl	$3, %esi
	andl	$1, %esi
	shrl	$3, %edx
	andl	$1, %edx
	cmpb	%sil, %dl
	je	.L5728
	movq	31(%rax), %rdx
	movq	%r8, %rsi
	movl	%ecx, -96(%rbp)
	movl	%r9d, -92(%rbp)
	movq	%rdx, -64(%rbp)
	movl	$2, %edx
	movq	%rax, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE@PLT
	movl	-96(%rbp), %ecx
	movl	-92(%rbp), %r9d
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rdi
.L5728:
	addl	%r12d, %r12d
	sarl	%r12d
	salq	$32, %r12
	movq	%r12, 15(%rax)
.L5729:
	addq	$8, %rbx
	cmpq	%r15, %rbx
	je	.L5716
.L5737:
	movq	(%r14), %rdx
.L5730:
	movq	-1(%rbx,%rdx), %rax
	cmpq	32(%r13), %rax
	je	.L5729
	movq	40(%r13), %rsi
	cmpq	%rsi, 23(%rax)
	je	.L5729
	movq	7(%rax), %rax
	testb	$1, %al
	je	.L5723
	movq	-1(%rax), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L5723
	testb	$1, 11(%rax)
	je	.L5723
	addq	$8, %rbx
	cmpq	%r15, %rbx
	jne	.L5737
	.p2align 4,,10
	.p2align 3
.L5716:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5738
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5736:
	.cfi_restore_state
	movq	(%rdx), %rsi
	movq	23(%rsi), %rsi
	testb	$1, %sil
	jne	.L5726
	movl	%ecx, %esi
	jmp	.L5725
	.p2align 4,,10
	.p2align 3
.L5726:
	movq	-1(%rsi), %r10
	movl	%ecx, %esi
	andl	$-2, %esi
	cmpw	$79, 11(%r10)
	cmovne	%ecx, %esi
	jmp	.L5725
.L5738:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25900:
	.size	_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_16GlobalDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE, .-_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_16GlobalDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_14NameDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE,"axG",@progbits,_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_14NameDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_14NameDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE
	.type	_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_14NameDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE, @function
_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_14NameDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE:
.LFB25901:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rdx), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movslq	35(%r12), %rax
	testq	%rax, %rax
	jle	.L5739
	subl	$1, %eax
	movl	%ecx, %ebx
	leaq	(%rax,%rax,2), %rax
	andl	$1, %ebx
	leaq	80(,%rax,8), %r9
	movl	$56, %eax
	jmp	.L5752
	.p2align 4,,10
	.p2align 3
.L5746:
	leal	16(%rax), %r10d
	leaq	-1(%r12), %r11
	movl	%ecx, %edi
	movq	-1(%r10,%r12), %r13
	sarq	$32, %r13
	testl	%ebx, %ebx
	je	.L5748
	testb	$1, %r13b
	jne	.L5757
.L5748:
	movl	%r13d, %r8d
	andl	$-57, %r13d
	shrl	$3, %r8d
	andl	$7, %r8d
	orl	%edi, %r8d
	leal	0(,%r8,8), %edi
	orl	%r13d, %edi
	addl	%edi, %edi
	sarl	%edi
	salq	$32, %rdi
	movq	%rdi, (%r11,%r10)
.L5751:
	addq	$24, %rax
	cmpq	%rax, %r9
	je	.L5739
.L5758:
	movq	(%rdx), %r12
.L5752:
	movq	-1(%r12,%rax), %rdi
	cmpq	40(%rsi), %rdi
	je	.L5751
	cmpq	32(%rsi), %rdi
	je	.L5751
	testb	$1, %dil
	je	.L5746
	movq	-1(%rdi), %r8
	cmpw	$64, 11(%r8)
	jne	.L5746
	testb	$1, 11(%rdi)
	je	.L5746
	addq	$24, %rax
	cmpq	%rax, %r9
	jne	.L5758
.L5739:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5757:
	.cfi_restore_state
	leal	8(%rax), %edi
	movq	-1(%rdi,%r12), %rdi
	testb	$1, %dil
	jne	.L5749
	movl	%ecx, %edi
	jmp	.L5748
	.p2align 4,,10
	.p2align 3
.L5749:
	movq	-1(%rdi), %r8
	movl	%ecx, %edi
	andl	$-2, %edi
	cmpw	$79, 11(%r8)
	cmovne	%ecx, %edi
	jmp	.L5748
	.cfi_endproc
.LFE25901:
	.size	_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_14NameDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE, .-_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_14NameDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE4EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE,"axG",@progbits,_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE4EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE,comdat
	.p2align 4
	.weak	_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE4EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE
	.type	_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE4EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE, @function
_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE4EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE:
.LFB24222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, %r15
	andq	$-262144, %r15
	movq	24(%r15), %rax
	movq	-1(%rbx), %rdx
	cmpw	$1026, 11(%rdx)
	leaq	-37592(%rax), %r13
	je	.L5831
	movq	-1(%rbx), %rax
	movzbl	13(%rax), %eax
	shrb	$5, %al
	andl	$1, %eax
.L5761:
	testb	%al, %al
	je	.L5762
	movq	41112(%r13), %rdi
	movq	12464(%r13), %r15
	testq	%rdi, %rdi
	je	.L5763
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L5764:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	jne	.L5762
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	12552(%r13), %rax
	cmpq	%rax, 96(%r13)
	jne	.L5832
	cmpl	$1, %r14d
	je	.L5778
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$79, %esi
	jmp	.L5827
	.p2align 4,,10
	.p2align 3
.L5762:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$8, %eax
	cmpb	$3, %al
	ja	.L5833
.L5826:
	movl	$257, %eax
.L5770:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5834
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5763:
	.cfi_restore_state
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L5835
.L5765:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rsi)
	jmp	.L5764
	.p2align 4,,10
	.p2align 3
.L5831:
	movq	-25128(%rax), %rax
	leaq	-96(%rbp), %rdi
	movq	%rdi, -128(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	24(%r15), %rdx
	movq	-128(%rbp), %rdi
	movq	%rbx, -88(%rbp)
	movq	%rax, -120(%rbp)
	subq	$37592, %rdx
	movq	$0, -80(%rbp)
	movq	%rdx, -96(%rbp)
	movl	$0, -72(%rbp)
	movb	$0, -68(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	movq	-120(%rbp), %rax
	cmpq	%rax, -88(%rbp)
	setne	%al
	jmp	.L5761
	.p2align 4,,10
	.p2align 3
.L5833:
	movq	-1(%rdx), %rax
	cmpw	$1026, 11(%rax)
	je	.L5836
	movq	-1(%rdx), %rax
	testb	$4, 13(%rax)
	je	.L5837
.L5775:
	cmpl	$1, %r14d
	je	.L5778
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$34, %esi
.L5827:
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L5770
	.p2align 4,,10
	.p2align 3
.L5832:
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L5770
	.p2align 4,,10
	.p2align 3
.L5778:
	movl	$1, %eax
	jmp	.L5770
	.p2align 4,,10
	.p2align 3
.L5836:
	leaq	-96(%rbp), %rdi
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r12, -80(%rbp)
	movl	$0, -72(%rbp)
	movb	$0, -68(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	cmpb	$0, -68(%rbp)
	jne	.L5826
	movq	-80(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE4EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE
	jmp	.L5770
	.p2align 4,,10
	.p2align 3
.L5835:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L5765
	.p2align 4,,10
	.p2align 3
.L5837:
	movq	-1(%rdx), %rax
	testb	$8, 13(%rax)
	jne	.L5775
	movq	-1(%rdx), %r14
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L5838
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L5779:
	movq	%r13, %rdi
	leaq	-96(%rbp), %r15
	call	_ZN2v88internal3Map6UpdateEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%r13, -96(%rbp)
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal19TransitionsAccessor10InitializeEv
	movq	3800(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L5781
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L5782
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L5783:
	movzbl	14(%rsi), %eax
.L5830:
	shrl	$3, %eax
	xorl	%ebx, %ebx
	subl	$6, %eax
	cmpb	$5, %al
	ja	.L5839
.L5788:
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
.L5786:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$6, %eax
	cmpb	$5, %al
	jbe	.L5826
	leaq	-104(%rbp), %r15
	movq	%rdx, -104(%rbp)
	movq	%r15, %rdi
	call	_ZNK2v88internal8JSObject21HasTypedArrayElementsEPNS0_7IsolateE.isra.0
	testb	%al, %al
	jne	.L5826
	movq	(%r12), %rax
	testq	%rbx, %rbx
	je	.L5802
	movq	%rax, -104(%rbp)
	movq	(%rbx), %rsi
	movl	$4, %edx
	call	_ZN2v88internal23TorqueGeneratedJSObjectINS0_8JSObjectENS0_10JSReceiverEE12set_elementsENS0_14FixedArrayBaseENS0_16WriteBarrierModeE
	movq	(%r12), %rax
.L5802:
	movq	15(%rax), %rbx
	cmpq	%rbx, 1016(%r13)
	je	.L5826
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L5803
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L5804:
	movq	(%r12), %rax
	movq	%r15, %rdi
	movq	%rax, -104(%rbp)
	movq	(%r14), %rsi
	call	_ZN2v88internal8JSObject19RequireSlowElementsENS0_16NumberDictionaryE
	leaq	56(%r13), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movl	$4, %ecx
	call	_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_16NumberDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE
	jmp	.L5826
.L5838:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L5840
.L5780:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%rsi)
	jmp	.L5779
.L5781:
	movq	%r15, %rdi
	call	_ZN2v88internal19TransitionsAccessor22CanHaveMoreTransitionsEv@PLT
	testb	%al, %al
	je	.L5787
	movq	%r14, %rsi
	leaq	3800(%r13), %rcx
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	leaq	.LC124(%rip), %r8
	movl	$4, %edx
	call	_ZN2v88internal3Map24CopyForPreventExtensionsEPNS0_7IsolateENS0_6HandleIS1_EENS0_18PropertyAttributesENS4_INS0_6SymbolEEEPKcb@PLT
	movq	%rax, %r14
	movq	(%rax), %rax
	movzbl	14(%rax), %eax
	jmp	.L5830
.L5782:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L5841
.L5784:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L5783
.L5839:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23CreateElementDictionaryEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
	movq	%rax, %rbx
	jmp	.L5788
.L5787:
	movq	%r13, %rdi
	leaq	.LC125(%rip), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8JSObject19NormalizePropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeEiPKc
	movq	(%r12), %rax
	movq	-1(%rax), %r15
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L5789
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L5790:
	leaq	.LC126(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	(%rax), %rdx
	movq	%rax, %r15
	movl	15(%rdx), %eax
	andl	$-134217729, %eax
	movl	%eax, 15(%rdx)
	call	_ZN2v88internal23CreateElementDictionaryEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L5792
	movq	(%r14), %rax
	movq	(%r15), %rcx
	movzbl	14(%rax), %eax
	movzbl	14(%rcx), %edx
	shrl	$3, %eax
	subl	$15, %eax
	cmpb	$2, %al
	sbbl	%eax, %eax
	andb	$7, %dl
	andl	$4, %eax
	addl	$12, %eax
	movzbl	%al, %eax
	sall	$3, %eax
	orl	%edx, %eax
	movb	%al, 14(%rcx)
.L5792:
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
	movq	(%r12), %rax
	leaq	56(%r13), %r14
	movq	-1(%rax), %rdx
	cmpw	$1025, 11(%rdx)
	je	.L5842
	leaq	-104(%rbp), %rdi
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal10JSReceiver19property_dictionaryEPNS0_7IsolateE.isra.0
	movq	41112(%r13), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L5799
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L5800:
	movl	$4, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_14NameDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE
	jmp	.L5786
.L5789:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L5843
.L5791:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rsi)
	jmp	.L5790
.L5840:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L5780
.L5842:
	movq	41112(%r13), %rdi
	movq	7(%rax), %r15
	testq	%rdi, %rdi
	je	.L5795
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L5796:
	movl	$4, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_16GlobalDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE
	jmp	.L5786
.L5803:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L5844
.L5805:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rbx, (%r14)
	jmp	.L5804
.L5841:
	movq	%r13, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L5784
.L5799:
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L5845
.L5801:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rdx)
	jmp	.L5800
.L5834:
	call	__stack_chk_fail@PLT
.L5795:
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L5846
.L5797:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rdx)
	jmp	.L5796
.L5843:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L5791
.L5845:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L5801
.L5844:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r14
	jmp	.L5805
.L5846:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L5797
	.cfi_endproc
.LFE24222:
	.size	_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE4EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE, .-_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE4EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE
	.section	.text._ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE5EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE,"axG",@progbits,_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE5EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE,comdat
	.p2align 4
	.weak	_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE5EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE
	.type	_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE5EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE, @function
_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE5EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE:
.LFB24223:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, %r15
	andq	$-262144, %r15
	movq	24(%r15), %rax
	movq	-1(%rbx), %rdx
	cmpw	$1026, 11(%rdx)
	leaq	-37592(%rax), %r13
	je	.L5920
	movq	-1(%rbx), %rax
	movzbl	13(%rax), %eax
	shrb	$5, %al
	andl	$1, %eax
.L5849:
	testb	%al, %al
	je	.L5850
	movq	41112(%r13), %rdi
	movq	12464(%r13), %r15
	testq	%rdi, %rdi
	je	.L5851
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L5852:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	jne	.L5850
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	12552(%r13), %rax
	cmpq	%rax, 96(%r13)
	jne	.L5921
	cmpl	$1, %r14d
	je	.L5866
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$79, %esi
	jmp	.L5916
	.p2align 4,,10
	.p2align 3
.L5850:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$10, %eax
	cmpb	$1, %al
	ja	.L5922
.L5915:
	movl	$257, %eax
.L5858:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5923
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5922:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	cmpw	$1026, 11(%rax)
	je	.L5924
	movq	-1(%rdx), %rax
	testb	$4, 13(%rax)
	je	.L5925
.L5863:
	cmpl	$1, %r14d
	je	.L5866
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$32, %esi
.L5916:
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L5858
	.p2align 4,,10
	.p2align 3
.L5851:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L5926
.L5853:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rsi)
	jmp	.L5852
	.p2align 4,,10
	.p2align 3
.L5920:
	movq	-25128(%rax), %rax
	leaq	-96(%rbp), %rdi
	movq	%rdi, -128(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	24(%r15), %rdx
	movq	-128(%rbp), %rdi
	movq	%rbx, -88(%rbp)
	movq	%rax, -120(%rbp)
	subq	$37592, %rdx
	movq	$0, -80(%rbp)
	movq	%rdx, -96(%rbp)
	movl	$0, -72(%rbp)
	movb	$0, -68(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	movq	-120(%rbp), %rax
	cmpq	%rax, -88(%rbp)
	setne	%al
	jmp	.L5849
	.p2align 4,,10
	.p2align 3
.L5921:
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L5858
	.p2align 4,,10
	.p2align 3
.L5925:
	movq	-1(%rdx), %rax
	testb	$8, 13(%rax)
	jne	.L5863
	movq	-1(%rdx), %r14
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L5927
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L5867:
	movq	%r13, %rdi
	leaq	-96(%rbp), %r15
	call	_ZN2v88internal3Map6UpdateEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%r13, -96(%rbp)
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal19TransitionsAccessor10InitializeEv
	movq	3704(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19TransitionsAccessor13SearchSpecialENS0_6SymbolE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L5869
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L5870
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L5871:
	movzbl	14(%rsi), %eax
.L5919:
	shrl	$3, %eax
	xorl	%ebx, %ebx
	subl	$6, %eax
	cmpb	$5, %al
	ja	.L5928
.L5876:
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
.L5874:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$6, %eax
	cmpb	$5, %al
	jbe	.L5915
	movq	-1(%rdx), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$17, %eax
	cmpb	$10, %al
	ja	.L5890
	cmpq	$0, 39(%rdx)
	je	.L5915
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$33, %esi
	jmp	.L5916
	.p2align 4,,10
	.p2align 3
.L5866:
	movl	$1, %eax
	jmp	.L5858
	.p2align 4,,10
	.p2align 3
.L5924:
	leaq	-96(%rbp), %rdi
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r12, -80(%rbp)
	movl	$0, -72(%rbp)
	movb	$0, -68(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	cmpb	$0, -68(%rbp)
	jne	.L5915
	movq	-80(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE5EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE
	jmp	.L5858
	.p2align 4,,10
	.p2align 3
.L5926:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L5853
	.p2align 4,,10
	.p2align 3
.L5927:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L5929
.L5868:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%rsi)
	jmp	.L5867
.L5869:
	movq	%r15, %rdi
	call	_ZN2v88internal19TransitionsAccessor22CanHaveMoreTransitionsEv@PLT
	testb	%al, %al
	je	.L5875
	movq	%r14, %rsi
	leaq	3704(%r13), %rcx
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	leaq	.LC124(%rip), %r8
	movl	$5, %edx
	call	_ZN2v88internal3Map24CopyForPreventExtensionsEPNS0_7IsolateENS0_6HandleIS1_EENS0_18PropertyAttributesENS4_INS0_6SymbolEEEPKcb@PLT
	movq	%rax, %r14
	movq	(%rax), %rax
	movzbl	14(%rax), %eax
	jmp	.L5919
.L5870:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L5930
.L5872:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L5871
.L5928:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23CreateElementDictionaryEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
	movq	%rax, %rbx
	jmp	.L5876
.L5875:
	movq	%r13, %rdi
	leaq	.LC125(%rip), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8JSObject19NormalizePropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeEiPKc
	movq	(%r12), %rax
	movq	-1(%rax), %r15
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L5877
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L5878:
	leaq	.LC126(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	(%rax), %rdx
	movq	%rax, %r15
	movl	15(%rdx), %eax
	andl	$-134217729, %eax
	movl	%eax, 15(%rdx)
	call	_ZN2v88internal23CreateElementDictionaryEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L5880
	movq	(%r14), %rax
	movq	(%r15), %rcx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$15, %eax
	cmpb	$2, %al
	movzbl	14(%rcx), %eax
	sbbl	%edx, %edx
	andl	$32, %edx
	andb	$7, %al
	addl	$96, %edx
	orl	%edx, %eax
	movb	%al, 14(%rcx)
.L5880:
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
	movq	(%r12), %rax
	leaq	56(%r13), %r14
	movq	-1(%rax), %rdx
	cmpw	$1025, 11(%rdx)
	je	.L5931
	leaq	-104(%rbp), %rdi
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal10JSReceiver19property_dictionaryEPNS0_7IsolateE.isra.0
	movq	41112(%r13), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L5887
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L5888:
	movl	$5, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_14NameDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE
	jmp	.L5874
.L5890:
	testq	%rbx, %rbx
	je	.L5891
	movq	%rdx, -104(%rbp)
	movq	(%rbx), %rsi
	movl	$4, %edx
	leaq	-104(%rbp), %rdi
	call	_ZN2v88internal23TorqueGeneratedJSObjectINS0_8JSObjectENS0_10JSReceiverEE12set_elementsENS0_14FixedArrayBaseENS0_16WriteBarrierModeE
	movq	(%r12), %rdx
.L5891:
	movq	15(%rdx), %r15
	cmpq	%r15, 1016(%r13)
	je	.L5915
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L5892
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L5893:
	movq	(%r12), %rax
	leaq	-104(%rbp), %rdi
	movq	%rax, -104(%rbp)
	movq	(%r14), %rsi
	call	_ZN2v88internal8JSObject19RequireSlowElementsENS0_16NumberDictionaryE
	leaq	56(%r13), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movl	$5, %ecx
	call	_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_16NumberDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE
	jmp	.L5915
.L5929:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L5868
.L5877:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L5932
.L5879:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rsi)
	jmp	.L5878
.L5930:
	movq	%r13, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L5872
.L5931:
	movq	41112(%r13), %rdi
	movq	7(%rax), %r15
	testq	%rdi, %rdi
	je	.L5883
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L5884:
	movl	$5, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject27ApplyAttributesToDictionaryINS0_16GlobalDictionaryEEEvPNS0_7IsolateENS0_13ReadOnlyRootsENS0_6HandleIT_EENS0_18PropertyAttributesE
	jmp	.L5874
.L5892:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L5933
.L5894:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%r14)
	jmp	.L5893
.L5887:
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L5934
.L5889:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rdx)
	jmp	.L5888
.L5883:
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L5935
.L5885:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rdx)
	jmp	.L5884
.L5923:
	call	__stack_chk_fail@PLT
.L5932:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L5879
.L5933:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r14
	jmp	.L5894
.L5934:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L5889
.L5935:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L5885
	.cfi_endproc
.LFE24223:
	.size	_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE5EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE, .-_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE5EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE
	.section	.text._ZN2v88internal10JSReceiver17SetIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesENS0_11ShouldThrowE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10JSReceiver17SetIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesENS0_11ShouldThrowE
	.type	_ZN2v88internal10JSReceiver17SetIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesENS0_11ShouldThrowE, @function
_ZN2v88internal10JSReceiver17SetIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesENS0_11ShouldThrowE:
.LFB21453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5937
	movq	-1(%rax), %rdx
	movzbl	14(%rdx), %ecx
	shrl	$3, %ecx
	subl	$13, %ecx
	cmpb	$1, %cl
	ja	.L5976
.L5937:
	movq	%rax, %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	andq	$-262144, %rdx
	movq	24(%rdx), %rcx
	movq	%rcx, -312(%rbp)
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	je	.L5977
	call	_ZN2v88internal8JSObject17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE
.L5945:
	testb	%al, %al
	jne	.L5946
.L5974:
	xorl	%eax, %eax
.L5975:
	movb	$0, %ah
.L5941:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L5978
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5946:
	.cfi_restore_state
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, -320(%rbp)
	testq	%rax, %rax
	je	.L5975
	movzbl	-288(%rbp), %eax
	movq	-312(%rbp), %r14
	pxor	%xmm0, %xmm0
	movups	%xmm0, -280(%rbp)
	andl	$-56, %eax
	movups	%xmm0, -264(%rbp)
	subq	$37592, %r14
	orl	$8, %eax
	movups	%xmm0, -232(%rbp)
	movb	%al, -288(%rbp)
	movzbl	-240(%rbp), %eax
	movups	%xmm0, -216(%rbp)
	andl	$-64, %eax
	orl	$40, %eax
	movb	%al, -240(%rbp)
	movq	-320(%rbp), %rax
	movq	(%rax), %rax
	cmpl	$4, %r13d
	je	.L5979
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L5949
	leaq	-289(%rbp), %rcx
	xorl	%ebx, %ebx
	leaq	-144(%rbp), %r13
	movq	%rcx, -336(%rbp)
	leaq	-192(%rbp), %rcx
	movq	%rcx, -328(%rbp)
	jmp	.L5963
	.p2align 4,,10
	.p2align 3
.L5981:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
.L5956:
	movq	-336(%rbp), %r8
	movq	%r10, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	pxor	%xmm1, %xmm1
	movl	$1, %r9d
	movq	%r13, %rdi
	movq	%r10, -312(%rbp)
	andb	$-64, -192(%rbp)
	movups	%xmm1, -184(%rbp)
	movups	%xmm1, -168(%rbp)
	movb	$0, -289(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	movq	-328(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver24GetOwnPropertyDescriptorEPNS0_14LookupIteratorEPNS0_18PropertyDescriptorE
	movq	-312(%rbp), %r10
	testb	%al, %al
	movzbl	%ah, %edx
	je	.L5974
	testb	%dl, %dl
	je	.L5960
	cmpq	$0, -176(%rbp)
	leaq	-288(%rbp), %rax
	je	.L5980
.L5961:
	movb	$1, %r15b
	movdqa	(%rax), %xmm2
	movdqa	16(%rax), %xmm3
	movq	%r13, %rcx
	movq	32(%rax), %rax
	movl	%r15d, %r15d
	movq	%r10, %rdx
	movq	%r12, %rsi
	movq	%r15, %r8
	movq	%r14, %rdi
	movaps	%xmm2, -144(%rbp)
	movq	%rax, -112(%rbp)
	movaps	%xmm3, -128(%rbp)
	call	_ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE
	testb	%al, %al
	je	.L5974
.L5960:
	movq	-320(%rbp), %rax
	addq	$1, %rbx
	movq	(%rax), %rax
	cmpl	%ebx, 11(%rax)
	jle	.L5949
.L5963:
	movq	15(%rax,%rbx,8), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L5981
	movq	41088(%r14), %r10
	cmpq	41096(%r14), %r10
	je	.L5982
.L5957:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r10)
	jmp	.L5956
	.p2align 4,,10
	.p2align 3
.L5977:
	call	_ZN2v88internal7JSProxy17PreventExtensionsENS0_6HandleIS1_EENS0_11ShouldThrowE@PLT
	jmp	.L5945
	.p2align 4,,10
	.p2align 3
.L5976:
	movq	-1(%rax), %rdx
	cmpw	$1027, 11(%rdx)
	je	.L5937
	call	_ZN2v88internal8JSObject18TestIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesE
	movl	%eax, %edx
	testb	%al, %al
	je	.L5974
	movzbl	%ah, %ecx
	testb	%cl, %cl
	je	.L5942
	xorl	%eax, %eax
	movb	%dl, %al
	movb	%cl, %ah
	jmp	.L5941
	.p2align 4,,10
	.p2align 3
.L5979:
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L5949
	xorl	%r13d, %r13d
	leaq	-288(%rbp), %r15
	jmp	.L5954
	.p2align 4,,10
	.p2align 3
.L5983:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L5951:
	movb	$1, %bl
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	%ebx, %ebx
	movq	%rbx, %r8
	call	_ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE
	testb	%al, %al
	je	.L5974
	movq	-320(%rbp), %rax
	addq	$1, %r13
	movq	(%rax), %rax
	cmpl	%r13d, 11(%rax)
	jle	.L5949
.L5954:
	movq	15(%rax,%r13,8), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L5983
	movq	41088(%r14), %rdx
	cmpq	41096(%r14), %rdx
	je	.L5984
.L5952:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L5951
	.p2align 4,,10
	.p2align 3
.L5980:
	cmpq	$0, -168(%rbp)
	jne	.L5961
	leaq	-240(%rbp), %rax
	jmp	.L5961
	.p2align 4,,10
	.p2align 3
.L5982:
	movq	%r14, %rdi
	movq	%rsi, -312(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L5957
	.p2align 4,,10
	.p2align 3
.L5949:
	movl	$257, %eax
	jmp	.L5941
	.p2align 4,,10
	.p2align 3
.L5984:
	movq	%r14, %rdi
	movq	%rsi, -312(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L5952
	.p2align 4,,10
	.p2align 3
.L5942:
	movl	%r14d, %esi
	movq	%r12, %rdi
	cmpl	$4, %r13d
	je	.L5985
	call	_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE5EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE
	jmp	.L5941
.L5985:
	call	_ZN2v88internal8JSObject31PreventExtensionsWithTransitionILNS0_18PropertyAttributesE4EEENS_5MaybeIbEENS0_6HandleIS1_EENS0_11ShouldThrowE
	jmp	.L5941
.L5978:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21453:
	.size	_ZN2v88internal10JSReceiver17SetIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesENS0_11ShouldThrowE, .-_ZN2v88internal10JSReceiver17SetIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesENS0_11ShouldThrowE
	.section	.rodata._ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE1EEEbNS0_6HandleIS1_EENS0_12ElementsKindE.str1.8,"aMS",@progbits,1
	.align 8
.LC128:
	.string	"boilerplate->length().ToArrayLength(&length)"
	.section	.text._ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE1EEEbNS0_6HandleIS1_EENS0_12ElementsKindE,"axG",@progbits,_ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE1EEEbNS0_6HandleIS1_EENS0_12ElementsKindE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE1EEEbNS0_6HandleIS1_EENS0_12ElementsKindE
	.type	_ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE1EEEbNS0_6HandleIS1_EENS0_12ElementsKindE, @function
_ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE1EEEbNS0_6HandleIS1_EENS0_12ElementsKindE:
.LFB25946:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	7(%rax), %rbx
	testb	$1, %bl
	je	.L5987
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	movq	-1(%rbx), %rax
	cmpw	$1061, 11(%rax)
	je	.L6028
.L5987:
	sarq	$32, %rbx
	movl	%ebx, %eax
	movl	%ebx, %edi
	andl	$31, %eax
	andl	$31, %edi
	cmpb	$5, %al
	ja	.L6010
	andl	$1, %ebx
	jne	.L5991
.L6010:
	movzbl	%r12b, %esi
.L5993:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
	.p2align 4,,10
	.p2align 3
.L5991:
	.cfi_restore_state
	movl	$1, %esi
	testb	%r12b, %r12b
	je	.L5993
	cmpb	$4, %r12b
	je	.L6015
	cmpb	$2, %r12b
	je	.L6016
	movl	$7, %esi
	cmpb	$6, %r12b
	jne	.L6010
	jmp	.L5993
	.p2align 4,,10
	.p2align 3
.L6028:
	movq	3520(%rdx), %rdi
	leaq	-37592(%rdx), %r13
	movq	%rbx, %rsi
	testq	%rdi, %rdi
	je	.L6029
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L5994:
	movq	-1(%rsi), %rax
	movzbl	14(%rax), %edi
	shrl	$3, %edi
	cmpb	$5, %dil
	ja	.L5999
	testb	$1, %dil
	jne	.L5996
.L5999:
	movzbl	%r12b, %esi
.L5998:
	call	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
	testb	%al, %al
	jne	.L6030
.L5986:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5996:
	.cfi_restore_state
	movl	$1, %esi
	testb	%r12b, %r12b
	je	.L5998
	cmpb	$4, %r12b
	je	.L6012
	cmpb	$2, %r12b
	je	.L6013
	movl	$7, %esi
	cmpb	$6, %r12b
	jne	.L5999
	jmp	.L5998
	.p2align 4,,10
	.p2align 3
.L6029:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L6031
.L5995:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rbx, (%r14)
	jmp	.L5994
	.p2align 4,,10
	.p2align 3
.L6030:
	movq	(%r14), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L6001
	sarq	$32, %rax
	movl	%eax, %edx
	js	.L6003
.L6004:
	cmpl	$8192, %edx
	setbe	%al
	jmp	.L5986
	.p2align 4,,10
	.p2align 3
.L6015:
	movl	$5, %esi
	jmp	.L5993
	.p2align 4,,10
	.p2align 3
.L6016:
	movl	$3, %esi
	jmp	.L5993
	.p2align 4,,10
	.p2align 3
.L6001:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L6032
.L6003:
	leaq	.LC128(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L6031:
	movq	%r13, %rdi
	movq	%rbx, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L5995
.L6032:
	movsd	7(%rax), %xmm1
	movsd	.LC98(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L6003
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %edx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L6003
	je	.L6004
	jmp	.L6003
	.p2align 4,,10
	.p2align 3
.L6012:
	movl	$5, %esi
	jmp	.L5998
.L6013:
	movl	$3, %esi
	jmp	.L5998
	.cfi_endproc
.LFE25946:
	.size	_ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE1EEEbNS0_6HandleIS1_EENS0_12ElementsKindE, .-_ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE1EEEbNS0_6HandleIS1_EENS0_12ElementsKindE
	.section	.text._ZN2v88internal8JSObject20UpdateAllocationSiteILNS0_24AllocationSiteUpdateModeE1EEEbNS0_6HandleIS1_EENS0_12ElementsKindE,"axG",@progbits,_ZN2v88internal8JSObject20UpdateAllocationSiteILNS0_24AllocationSiteUpdateModeE1EEEbNS0_6HandleIS1_EENS0_12ElementsKindE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8JSObject20UpdateAllocationSiteILNS0_24AllocationSiteUpdateModeE1EEEbNS0_6HandleIS1_EENS0_12ElementsKindE
	.type	_ZN2v88internal8JSObject20UpdateAllocationSiteILNS0_24AllocationSiteUpdateModeE1EEEbNS0_6HandleIS1_EENS0_12ElementsKindE, @function
_ZN2v88internal8JSObject20UpdateAllocationSiteILNS0_24AllocationSiteUpdateModeE1EEEbNS0_6HandleIS1_EENS0_12ElementsKindE:
.LFB24310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-1(%rdi), %rax
	cmpw	$1061, 11(%rax)
	jne	.L6036
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L6059
.L6036:
	xorl	%r12d, %r12d
.L6033:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6060
	addq	$32, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6059:
	.cfi_restore_state
	movl	%esi, %r13d
	call	_ZN2v88internal4Heap13IsLargeObjectENS0_10HeapObjectE@PLT
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L6036
	movq	(%rbx), %rax
	leaq	-48(%rbp), %rdi
	movq	%rax, %rdx
	leaq	-1(%rax), %r14
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	-1(%rax), %rsi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	%r14, %rcx
	cltq
	andq	$-262144, %rcx
	addq	%r14, %rax
	leaq	8(%rax), %rdx
	andq	$-262144, %rdx
	cmpq	%rdx, %rcx
	jne	.L6033
	movq	(%rax), %rdx
	cmpq	%rdx, -33600(%rbx)
	jne	.L6033
	testb	$8, 10(%rcx)
	jne	.L6038
.L6040:
	cmpq	$-1, %rax
	je	.L6033
	movq	248(%rbx), %rdx
	cmpq	104(%rdx), %rax
	je	.L6033
	movq	8(%rax), %rsi
	testb	$1, %sil
	je	.L6033
	movq	-1(%rsi), %rax
	cmpw	$121, 11(%rax)
	jne	.L6033
	movl	31(%rsi), %eax
	shrl	$26, %eax
	andl	$7, %eax
	cmpl	$4, %eax
	je	.L6033
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L6057
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L6044:
	movzbl	%r13b, %esi
	call	_ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE1EEEbNS0_6HandleIS1_EENS0_12ElementsKindE
	movl	%eax, %r12d
	jmp	.L6033
	.p2align 4,,10
	.p2align 3
.L6057:
	movq	41088(%rbx), %rdi
	cmpq	41096(%rbx), %rdi
	je	.L6061
.L6045:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L6044
.L6061:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L6045
.L6038:
	movq	80(%rcx), %rdx
	movq	128(%rdx), %rdx
	cmpq	40(%rcx), %rdx
	jb	.L6033
	cmpq	48(%rcx), %rdx
	jnb	.L6033
	cmpq	%rdx, %r14
	jb	.L6033
	jmp	.L6040
.L6060:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24310:
	.size	_ZN2v88internal8JSObject20UpdateAllocationSiteILNS0_24AllocationSiteUpdateModeE1EEEbNS0_6HandleIS1_EENS0_12ElementsKindE, .-_ZN2v88internal8JSObject20UpdateAllocationSiteILNS0_24AllocationSiteUpdateModeE1EEEbNS0_6HandleIS1_EENS0_12ElementsKindE
	.section	.rodata._ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE0EEEbNS0_6HandleIS1_EENS0_12ElementsKindE.str1.1,"aMS",@progbits,1
.LC129:
	.string	"(nested)"
	.section	.rodata._ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE0EEEbNS0_6HandleIS1_EENS0_12ElementsKindE.str1.8,"aMS",@progbits,1
	.align 8
.LC130:
	.string	"AllocationSite: JSArray %p boilerplate %supdated %s->%s\n"
	.align 8
.LC131:
	.string	"AllocationSite: JSArray %p site updated %s->%s\n"
	.section	.text._ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE0EEEbNS0_6HandleIS1_EENS0_12ElementsKindE,"axG",@progbits,_ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE0EEEbNS0_6HandleIS1_EENS0_12ElementsKindE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE0EEEbNS0_6HandleIS1_EENS0_12ElementsKindE
	.type	_ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE0EEEbNS0_6HandleIS1_EENS0_12ElementsKindE, @function
_ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE0EEEbNS0_6HandleIS1_EENS0_12ElementsKindE:
.LFB25947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$56, %rsp
	movb	%sil, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %rdx
	movq	7(%rax), %r12
	andq	$-262144, %rdx
	movq	24(%rdx), %r13
	subq	$37592, %r13
	testb	$1, %r12b
	jne	.L6112
.L6063:
	sarq	$32, %r12
	movl	%r12d, %eax
	movl	%r12d, %r14d
	andl	$31, %eax
	andl	$31, %r14d
	cmpb	$5, %al
	ja	.L6090
	andl	$1, %r12d
	jne	.L6067
.L6090:
	movzbl	%bl, %r12d
.L6069:
	movl	%r12d, %esi
	movl	%r14d, %edi
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
	movl	%eax, %r15d
	testb	%al, %al
	jne	.L6113
.L6087:
	xorl	%r15d, %r15d
.L6062:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6114
	addq	$56, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6113:
	.cfi_restore_state
	cmpb	$0, _ZN2v88internal33FLAG_trace_track_allocation_sitesE(%rip)
	movq	-80(%rbp), %r9
	jne	.L6115
.L6091:
	movq	(%r9), %rdx
	movzbl	-72(%rbp), %r14d
	leaq	-64(%rbp), %rdi
	movq	%r13, %rsi
	movslq	11(%rdx), %rax
	andl	$-32, %eax
	orl	%eax, %r14d
	salq	$32, %r14
	movq	%r14, 7(%rdx)
	movl	$6, %edx
	movq	(%r9), %rax
	movq	23(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE@PLT
	jmp	.L6062
	.p2align 4,,10
	.p2align 3
.L6067:
	testb	%bl, %bl
	je	.L6097
	cmpb	$4, %bl
	je	.L6098
	cmpb	$2, %bl
	je	.L6099
	cmpb	$6, %bl
	jne	.L6090
	movb	$7, -72(%rbp)
	movl	$7, %r12d
	jmp	.L6069
	.p2align 4,,10
	.p2align 3
.L6112:
	movq	-1(%r12), %rax
	cmpw	$1061, 11(%rax)
	jne	.L6063
	movq	41112(%r13), %rdi
	movq	%r12, %rsi
	testq	%rdi, %rdi
	je	.L6116
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %r9
	movq	(%rax), %rsi
	movq	%rax, %r14
.L6070:
	movq	-1(%rsi), %rax
	movzbl	14(%rax), %r12d
	shrl	$3, %r12d
	cmpb	$5, %r12b
	ja	.L6075
	testb	$1, %r12b
	jne	.L6072
.L6075:
	movzbl	%bl, %r10d
.L6074:
	movl	%r10d, %esi
	movl	%r12d, %edi
	movq	%r9, -80(%rbp)
	movl	%r10d, -72(%rbp)
	call	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
	movl	-72(%rbp), %r10d
	movq	-80(%rbp), %r9
	testb	%al, %al
	movl	%eax, %r15d
	je	.L6087
	movq	(%r14), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L6078
	sarq	$32, %rax
	movl	%eax, %edx
	js	.L6080
.L6081:
	cmpl	$8192, %edx
	ja	.L6087
	cmpb	$0, _ZN2v88internal33FLAG_trace_track_allocation_sitesE(%rip)
	leaq	-64(%rbp), %rbx
	jne	.L6117
.L6088:
	movl	%r10d, %esi
	movq	%r14, %rdi
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE
	movq	-72(%rbp), %r9
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	$6, %edx
	movq	(%r9), %rax
	movq	23(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE@PLT
	jmp	.L6062
	.p2align 4,,10
	.p2align 3
.L6097:
	movb	$1, -72(%rbp)
	movl	$1, %r12d
	jmp	.L6069
	.p2align 4,,10
	.p2align 3
.L6115:
	movl	%r12d, %edi
	call	_ZN2v88internal20ElementsKindToStringENS0_12ElementsKindE@PLT
	movl	%r14d, %edi
	movq	%rax, %r12
	call	_ZN2v88internal20ElementsKindToStringENS0_12ElementsKindE@PLT
	movq	-80(%rbp), %r9
	movq	%r12, %rcx
	leaq	.LC131(%rip), %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	movq	(%r9), %rsi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-80(%rbp), %r9
	jmp	.L6091
	.p2align 4,,10
	.p2align 3
.L6116:
	movq	41088(%r13), %r14
	cmpq	%r14, 41096(%r13)
	je	.L6118
.L6071:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%r12, (%r14)
	jmp	.L6070
	.p2align 4,,10
	.p2align 3
.L6072:
	movl	$1, %r10d
	testb	%bl, %bl
	je	.L6074
	cmpb	$4, %bl
	je	.L6094
	cmpb	$2, %bl
	je	.L6095
	movl	$7, %r10d
	cmpb	$6, %bl
	jne	.L6075
	jmp	.L6074
.L6118:
	movq	%r13, %rdi
	movq	%r9, -80(%rbp)
	movq	%r12, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L6071
	.p2align 4,,10
	.p2align 3
.L6098:
	movb	$5, -72(%rbp)
	movl	$5, %r12d
	jmp	.L6069
	.p2align 4,,10
	.p2align 3
.L6099:
	movb	$3, -72(%rbp)
	movl	$3, %r12d
	jmp	.L6069
	.p2align 4,,10
	.p2align 3
.L6078:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L6119
.L6080:
	leaq	.LC128(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L6117:
	movq	(%r9), %rax
	movq	%rbx, %rdi
	movq	%r9, -96(%rbp)
	movl	%r10d, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal14AllocationSite8IsNestedEv@PLT
	movl	-72(%rbp), %r10d
	movb	%al, -81(%rbp)
	movl	%r10d, %edi
	movl	%r10d, -80(%rbp)
	call	_ZN2v88internal20ElementsKindToStringENS0_12ElementsKindE@PLT
	movl	%r12d, %edi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal20ElementsKindToStringENS0_12ElementsKindE@PLT
	movzbl	-81(%rbp), %edx
	movq	-96(%rbp), %r9
	leaq	.LC130(%rip), %rdi
	movq	%rax, %rcx
	leaq	.LC41(%rip), %rax
	movq	-72(%rbp), %r8
	testb	%dl, %dl
	leaq	.LC129(%rip), %rdx
	movq	(%r9), %rsi
	movq	%r9, -72(%rbp)
	cmove	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	-80(%rbp), %r10d
	movq	-72(%rbp), %r9
	jmp	.L6088
.L6119:
	movsd	7(%rax), %xmm1
	movsd	.LC98(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L6080
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movd	%xmm2, %edx
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L6080
	je	.L6081
	jmp	.L6080
	.p2align 4,,10
	.p2align 3
.L6094:
	movl	$5, %r10d
	jmp	.L6074
.L6095:
	movl	$3, %r10d
	jmp	.L6074
.L6114:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25947:
	.size	_ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE0EEEbNS0_6HandleIS1_EENS0_12ElementsKindE, .-_ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE0EEEbNS0_6HandleIS1_EENS0_12ElementsKindE
	.section	.text._ZN2v88internal8JSObject20UpdateAllocationSiteILNS0_24AllocationSiteUpdateModeE0EEEbNS0_6HandleIS1_EENS0_12ElementsKindE,"axG",@progbits,_ZN2v88internal8JSObject20UpdateAllocationSiteILNS0_24AllocationSiteUpdateModeE0EEEbNS0_6HandleIS1_EENS0_12ElementsKindE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8JSObject20UpdateAllocationSiteILNS0_24AllocationSiteUpdateModeE0EEEbNS0_6HandleIS1_EENS0_12ElementsKindE
	.type	_ZN2v88internal8JSObject20UpdateAllocationSiteILNS0_24AllocationSiteUpdateModeE0EEEbNS0_6HandleIS1_EENS0_12ElementsKindE, @function
_ZN2v88internal8JSObject20UpdateAllocationSiteILNS0_24AllocationSiteUpdateModeE0EEEbNS0_6HandleIS1_EENS0_12ElementsKindE:
.LFB24311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-1(%rdi), %rax
	cmpw	$1061, 11(%rax)
	jne	.L6123
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L6146
.L6123:
	xorl	%r12d, %r12d
.L6120:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6147
	addq	$32, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6146:
	.cfi_restore_state
	movl	%esi, %r13d
	call	_ZN2v88internal4Heap13IsLargeObjectENS0_10HeapObjectE@PLT
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L6123
	movq	(%rbx), %rax
	leaq	-48(%rbp), %rdi
	movq	%rax, %rdx
	leaq	-1(%rax), %r14
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	-1(%rax), %rsi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	movq	%r14, %rcx
	cltq
	andq	$-262144, %rcx
	addq	%r14, %rax
	leaq	8(%rax), %rdx
	andq	$-262144, %rdx
	cmpq	%rdx, %rcx
	jne	.L6120
	movq	(%rax), %rdx
	cmpq	%rdx, -33600(%rbx)
	jne	.L6120
	testb	$8, 10(%rcx)
	jne	.L6125
.L6127:
	cmpq	$-1, %rax
	je	.L6120
	movq	248(%rbx), %rdx
	cmpq	104(%rdx), %rax
	je	.L6120
	movq	8(%rax), %rsi
	testb	$1, %sil
	je	.L6120
	movq	-1(%rsi), %rax
	cmpw	$121, 11(%rax)
	jne	.L6120
	movl	31(%rsi), %eax
	shrl	$26, %eax
	andl	$7, %eax
	cmpl	$4, %eax
	je	.L6120
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L6144
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L6131:
	movzbl	%r13b, %esi
	call	_ZN2v88internal14AllocationSite24DigestTransitionFeedbackILNS0_24AllocationSiteUpdateModeE0EEEbNS0_6HandleIS1_EENS0_12ElementsKindE
	movl	%eax, %r12d
	jmp	.L6120
	.p2align 4,,10
	.p2align 3
.L6144:
	movq	41088(%rbx), %rdi
	cmpq	41096(%rbx), %rdi
	je	.L6148
.L6132:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L6131
.L6148:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L6132
.L6125:
	movq	80(%rcx), %rdx
	movq	128(%rdx), %rdx
	cmpq	40(%rcx), %rdx
	jb	.L6120
	cmpq	48(%rcx), %rdx
	jnb	.L6120
	cmpq	%rdx, %r14
	jb	.L6120
	jmp	.L6127
.L6147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24311:
	.size	_ZN2v88internal8JSObject20UpdateAllocationSiteILNS0_24AllocationSiteUpdateModeE0EEEbNS0_6HandleIS1_EENS0_12ElementsKindE, .-_ZN2v88internal8JSObject20UpdateAllocationSiteILNS0_24AllocationSiteUpdateModeE0EEEbNS0_6HandleIS1_EENS0_12ElementsKindE
	.section	.text._ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE
	.type	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE, @function
_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE:
.LFB21547:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movzbl	%sil, %ebx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	-1(%rax), %rax
	movzbl	14(%rax), %r13d
	shrl	$3, %r13d
	cmpb	$5, %r13b
	ja	.L6150
	testb	$1, %r13b
	jne	.L6169
.L6150:
	cmpb	%r13b, %bl
	je	.L6149
.L6151:
	movzbl	%bl, %r14d
	movq	%r12, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal8JSObject20UpdateAllocationSiteILNS0_24AllocationSiteUpdateModeE0EEEbNS0_6HandleIS1_EENS0_12ElementsKindE
	movq	(%r12), %rdx
	movq	%rdx, %rax
	movq	15(%rdx), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	leaq	-37592(%rax), %r15
	cmpq	%rdx, -37304(%rax)
	je	.L6153
	leal	-4(%rbx), %eax
	cmpb	$1, %al
	leal	-4(%r13), %eax
	setbe	%cl
	cmpb	$1, %al
	setbe	%al
	cmpb	%al, %cl
	je	.L6153
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rax
	movslq	11(%rdx), %rdx
	movq	%r12, %rsi
	movq	(%rax,%rbx,8), %rdi
	movq	(%rdi), %rax
	movq	112(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L6153:
	.cfi_restore_state
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject24GetElementsTransitionMapENS0_6HandleIS1_EENS0_12ElementsKindE
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi
	cmpb	$0, _ZN2v88internal31FLAG_trace_elements_transitionsE(%rip)
	jne	.L6170
.L6149:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6169:
	.cfi_restore_state
	testb	%sil, %sil
	je	.L6158
	cmpb	$4, %sil
	je	.L6159
	cmpb	$2, %sil
	je	.L6160
	cmpb	$6, %sil
	jne	.L6150
	movl	$7, %ebx
	jmp	.L6151
	.p2align 4,,10
	.p2align 3
.L6170:
	movq	(%r12), %rax
	movq	41112(%r15), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L6155
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L6156:
	movq	stdout(%rip), %rdi
	movl	%r14d, %r8d
	movl	%r13d, %edx
	movq	%r12, %rsi
	addq	$24, %rsp
	movq	%rcx, %r9
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8JSObject23PrintElementsTransitionEP8_IO_FILENS0_6HandleIS1_EENS0_12ElementsKindENS4_INS0_14FixedArrayBaseEEES6_S8_
	.p2align 4,,10
	.p2align 3
.L6158:
	.cfi_restore_state
	movl	$1, %ebx
	jmp	.L6150
	.p2align 4,,10
	.p2align 3
.L6155:
	movq	41088(%r15), %rcx
	cmpq	41096(%r15), %rcx
	je	.L6171
.L6157:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L6156
	.p2align 4,,10
	.p2align 3
.L6159:
	movl	$5, %ebx
	jmp	.L6150
	.p2align 4,,10
	.p2align 3
.L6160:
	movl	$3, %ebx
	jmp	.L6150
	.p2align 4,,10
	.p2align 3
.L6171:
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L6157
	.cfi_endproc
.LFE21547:
	.size	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE, .-_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE
	.section	.text._ZN2v88internal8JSObject21AllocateStorageForMapENS0_6HandleIS1_EENS2_INS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject21AllocateStorageForMapENS0_6HandleIS1_EENS2_INS0_3MapEEE
	.type	_ZN2v88internal8JSObject21AllocateStorageForMapENS0_6HandleIS1_EENS2_INS0_3MapEEE, @function
_ZN2v88internal8JSObject21AllocateStorageForMapENS0_6HandleIS1_EENS2_INS0_3MapEEE:
.LFB21492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	-1(%rax), %rax
	movzbl	14(%rax), %r14d
	movq	(%rsi), %rax
	movzbl	14(%rax), %edx
	shrl	$3, %r14d
	shrl	$3, %edx
	cmpb	%dl, %r14b
	jne	.L6240
.L6173:
	movq	%rax, -64(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal3Map14NumberOfFieldsEv@PLT
	movq	(%r12), %rdx
	movzbl	7(%rdx), %ecx
	movzbl	8(%rdx), %esi
	movzbl	9(%rdx), %r14d
	subl	%esi, %ecx
	cmpl	$2, %r14d
	jg	.L6241
.L6178:
	addl	%eax, %r14d
	movq	(%rbx), %rdi
	movq	%rdx, %r8
	subl	%ecx, %r14d
	testl	%r14d, %r14d
	jle	.L6179
	andq	$-262144, %rdi
	movq	39(%rdx), %rsi
	movq	24(%rdi), %r9
	movq	3520(%r9), %rdi
	leaq	-37592(%r9), %r13
	testq	%rdi, %rdi
	je	.L6180
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L6181:
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal7Factory16NewPropertyArrayEiNS0_14AllocationTypeE@PLT
	movq	-80(%rbp), %r8
	movq	%rbx, -96(%rbp)
	xorl	%r15d, %r15d
	movq	%r13, %rbx
	movq	%rax, -72(%rbp)
	movq	(%r12), %rdx
	movl	$24, %r14d
	movq	%r8, %r13
	jmp	.L6183
	.p2align 4,,10
	.p2align 3
.L6206:
	addq	$24, %r14
.L6183:
	movl	15(%rdx), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%eax, %r15d
	jge	.L6184
	movq	0(%r13), %rax
	addl	$1, %r15d
	movq	7(%r14,%rax), %rax
	shrq	$38, %rax
	andl	$7, %eax
	cmpl	$2, %eax
	jne	.L6206
	movq	39(%rdx), %rax
	movq	7(%r14,%rax), %rsi
	movzbl	7(%rdx), %r11d
	movzbl	8(%rdx), %ecx
	movq	%rsi, %rax
	shrq	$38, %rsi
	shrq	$51, %rax
	subl	%ecx, %r11d
	andl	$7, %esi
	movq	%rax, %r10
	movl	%esi, %r9d
	andl	$1023, %r10d
	cmpl	%r11d, %r10d
	setl	%cl
	jl	.L6242
	subl	%r11d, %r10d
	movl	$16, %edi
	leal	16(,%r10,8), %r10d
.L6187:
	cmpl	$2, %esi
	jne	.L6243
	movl	$32768, %esi
.L6188:
	movzbl	%cl, %eax
	movslq	%r11d, %r11
	movslq	%r10d, %r10
	movslq	%edi, %rdi
	salq	$14, %rax
	salq	$17, %r11
	orq	%r11, %rax
	salq	$27, %rdi
	orq	%r10, %rax
	orq	%rdi, %rax
	orq	%rax, %rsi
	shrq	$30, %rax
	movl	%esi, %ecx
	andl	$15, %eax
	sarl	$3, %ecx
	andl	$2047, %ecx
	subl	%eax, %ecx
	testl	$16384, %esi
	jne	.L6191
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	%ecx, -80(%rbp)
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movl	-80(%rbp), %ecx
	movabsq	$-2251799814209537, %rdi
	movq	(%rax), %rdx
	movq	%rdi, 7(%rdx)
	movq	-72(%rbp), %rdi
	movq	(%rax), %rdx
	leal	16(,%rcx,8), %eax
	movq	(%rdi), %rdi
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L6239
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	jne	.L6244
	testb	$24, %al
	je	.L6239
.L6234:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L6239
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L6239:
	movq	(%r12), %rdx
	jmp	.L6206
	.p2align 4,,10
	.p2align 3
.L6184:
	movq	-96(%rbp), %rbx
	movq	-88(%rbp), %rdi
	movq	(%rbx), %rax
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE
	movq	(%r12), %r8
	movq	(%rbx), %rdi
.L6179:
	movq	%r8, -1(%rdi)
	testq	%r8, %r8
	jne	.L6245
.L6172:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6246
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6180:
	.cfi_restore_state
	movq	41088(%r13), %r8
	cmpq	%r8, 41096(%r13)
	je	.L6247
.L6182:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r8)
	jmp	.L6181
	.p2align 4,,10
	.p2align 3
.L6243:
	cmpb	$2, %sil
	jg	.L6189
	je	.L6190
.L6213:
	xorl	%esi, %esi
	jmp	.L6188
	.p2align 4,,10
	.p2align 3
.L6189:
	subl	$3, %esi
	cmpb	$1, %sil
	jbe	.L6213
.L6190:
	movl	%r9d, %edi
	call	_ZNK2v88internal14Representation8MnemonicEv.isra.0
	leaq	.LC6(%rip), %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L6191:
	movq	47(%rdx), %rax
	testq	%rax, %rax
	je	.L6203
	movq	%rax, %rsi
	movl	$32, %edx
	notq	%rsi
	movl	%esi, %edi
	andl	$1, %edi
	jne	.L6195
	movslq	11(%rax), %rdx
	sall	$3, %edx
.L6195:
	cmpl	%edx, %ecx
	jnb	.L6203
	testl	%ecx, %ecx
	leal	31(%rcx), %edx
	cmovns	%ecx, %edx
	sarl	$5, %edx
	andl	$1, %esi
	jne	.L6196
	cmpl	%edx, 11(%rax)
	jle	.L6196
	movl	%ecx, %esi
	sarl	$31, %esi
	shrl	$27, %esi
	addl	%esi, %ecx
	andl	$31, %ecx
	subl	%esi, %ecx
	movl	$1, %esi
	sall	%cl, %esi
	movl	%esi, %ecx
	testb	%dil, %dil
	je	.L6248
.L6200:
	sarq	$32, %rax
	testl	%eax, %ecx
	sete	%al
.L6202:
	testb	%al, %al
	je	.L6239
	.p2align 4,,10
	.p2align 3
.L6203:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	movabsq	$-2251799814209537, %rbx
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movq	(%rax), %rax
	movq	%rbx, 7(%rax)
	movq	0, %rax
	ud2
	.p2align 4,,10
	.p2align 3
.L6242:
	movzbl	8(%rdx), %edi
	movzbl	8(%rdx), %eax
	addl	%eax, %r10d
	sall	$3, %edi
	sall	$3, %r10d
	jmp	.L6187
	.p2align 4,,10
	.p2align 3
.L6240:
	movq	%rsi, %r13
	movl	%edx, %edi
	movl	%r14d, %esi
	movl	%edx, %r12d
	call	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
	testb	%al, %al
	jne	.L6174
	cmpb	$12, %r14b
	je	.L6175
	cmpb	$12, %r12b
	je	.L6175
.L6176:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE
.L6177:
	movq	(%rbx), %rax
	movl	%r12d, %edx
	movq	%r13, %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal3Map23ReconfigureElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	jmp	.L6173
	.p2align 4,,10
	.p2align 3
.L6241:
	movzbl	7(%rdx), %esi
	subl	%r14d, %esi
	movl	%esi, %r14d
	jmp	.L6178
	.p2align 4,,10
	.p2align 3
.L6245:
	testb	$1, %r8b
	je	.L6172
	movq	%r8, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L6172
	movq	%r8, %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L6172
	.p2align 4,,10
	.p2align 3
.L6244:
	movq	%rdx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L6234
	jmp	.L6239
	.p2align 4,,10
	.p2align 3
.L6248:
	sall	$2, %edx
	movslq	%edx, %rdx
	movl	15(%rax,%rdx), %eax
	testl	%eax, %esi
	sete	%al
	jmp	.L6202
	.p2align 4,,10
	.p2align 3
.L6174:
	movl	%r14d, %r12d
	cmpb	$12, %r14b
	jne	.L6176
.L6175:
	movq	%rbx, %rdi
	movl	$12, %r12d
	call	_ZN2v88internal8JSObject17NormalizeElementsENS0_6HandleIS1_EE
	jmp	.L6177
	.p2align 4,,10
	.p2align 3
.L6196:
	cmpl	$31, %ecx
	jg	.L6198
	testb	%dil, %dil
	je	.L6198
	movl	$1, %edx
	sall	%cl, %edx
	movl	%edx, %ecx
	jmp	.L6200
.L6247:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L6182
.L6198:
	leaq	.LC12(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L6246:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21492:
	.size	_ZN2v88internal8JSObject21AllocateStorageForMapENS0_6HandleIS1_EENS2_INS0_3MapEEE, .-_ZN2v88internal8JSObject21AllocateStorageForMapENS0_6HandleIS1_EENS2_INS0_3MapEEE
	.section	.text._ZN2v88internal8JSObject24EnsureCanContainElementsENS0_6HandleIS1_EEPNS0_9ArgumentsEjjNS0_18EnsureElementsModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8JSObject24EnsureCanContainElementsENS0_6HandleIS1_EEPNS0_9ArgumentsEjjNS0_18EnsureElementsModeE
	.type	_ZN2v88internal8JSObject24EnsureCanContainElementsENS0_6HandleIS1_EEPNS0_9ArgumentsEjjNS0_18EnsureElementsModeE, @function
_ZN2v88internal8JSObject24EnsureCanContainElementsENS0_6HandleIS1_EEPNS0_9ArgumentsEjjNS0_18EnsureElementsModeE:
.LFB21539:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rax
	movq	(%rdi), %rsi
	movq	-1(%rsi), %r9
	movzbl	14(%r9), %r9d
	shrl	$3, %r9d
	cmpl	$3, %r9d
	je	.L6249
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	movq	-37496(%rsi), %r10
	testl	%ecx, %ecx
	je	.L6249
	subl	$1, %ecx
	movzbl	%r9b, %esi
	addl	%ecx, %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	subq	%rdx, %rax
	cmpb	$5, %r9b
	setbe	%dl
	andl	%r9d, %edx
	cmpl	$2, %r8d
	leaq	8(%rax,%rcx,8), %r8
	jne	.L6258
	.p2align 4,,10
	.p2align 3
.L6265:
	movq	(%rax), %rcx
	cmpq	%rcx, %r10
	je	.L6280
	testb	$1, %cl
	je	.L6260
	movq	-1(%rcx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L6263
	cmpb	$1, %sil
	jbe	.L6281
	.p2align 4,,10
	.p2align 3
.L6260:
	addq	$8, %rax
	cmpq	%rax, %r8
	jne	.L6265
	.p2align 4,,10
	.p2align 3
.L6257:
	cmpb	%sil, %r9b
	jne	.L6256
.L6249:
	ret
	.p2align 4,,10
	.p2align 3
.L6282:
	andl	$1, %ecx
	je	.L6255
	testb	%dl, %dl
	jne	.L6274
	movl	$2, %esi
.L6255:
	addq	$8, %rax
	cmpq	%rax, %r8
	je	.L6257
.L6258:
	movq	(%rax), %rcx
	cmpq	%rcx, %r10
	jne	.L6282
	movl	$1, %edx
	testb	%sil, %sil
	je	.L6267
	cmpb	$4, %sil
	je	.L6268
	cmpb	$2, %sil
	je	.L6269
	cmpb	$6, %sil
	movl	$7, %ecx
	cmove	%ecx, %esi
	jmp	.L6255
	.p2align 4,,10
	.p2align 3
.L6263:
	testb	%dl, %dl
	jne	.L6274
	addq	$8, %rax
	movl	$2, %esi
	cmpq	%rax, %r8
	jne	.L6265
	jmp	.L6257
	.p2align 4,,10
	.p2align 3
.L6267:
	movl	$1, %esi
	jmp	.L6255
	.p2align 4,,10
	.p2align 3
.L6280:
	testb	%sil, %sil
	je	.L6271
	cmpb	$4, %sil
	je	.L6264
	movl	$1, %edx
	cmpb	$2, %sil
	je	.L6272
	cmpb	$6, %sil
	movl	$7, %ecx
	cmove	%ecx, %esi
	addq	$8, %rax
	cmpq	%rax, %r8
	jne	.L6265
	jmp	.L6257
	.p2align 4,,10
	.p2align 3
.L6271:
	addq	$8, %rax
	movl	$1, %edx
	movl	$1, %esi
	cmpq	%rax, %r8
	jne	.L6265
	jmp	.L6257
	.p2align 4,,10
	.p2align 3
.L6274:
	movl	$3, %esi
.L6256:
	jmp	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE
	.p2align 4,,10
	.p2align 3
.L6268:
	movl	$5, %esi
	jmp	.L6255
	.p2align 4,,10
	.p2align 3
.L6269:
	movl	$3, %esi
	jmp	.L6255
	.p2align 4,,10
	.p2align 3
.L6281:
	movl	$4, %esi
	testb	%dl, %dl
	je	.L6260
	.p2align 4,,10
	.p2align 3
.L6264:
	movl	$1, %edx
	movl	$5, %esi
	jmp	.L6260
	.p2align 4,,10
	.p2align 3
.L6272:
	movl	$3, %esi
	jmp	.L6260
	.cfi_endproc
.LFE21539:
	.size	_ZN2v88internal8JSObject24EnsureCanContainElementsENS0_6HandleIS1_EEPNS0_9ArgumentsEjjNS0_18EnsureElementsModeE, .-_ZN2v88internal8JSObject24EnsureCanContainElementsENS0_6HandleIS1_EEPNS0_9ArgumentsEjjNS0_18EnsureElementsModeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE, @function
_GLOBAL__sub_I__ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE:
.LFB28035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28035:
	.size	_GLOBAL__sub_I__ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE, .-_GLOBAL__sub_I__ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE
	.hidden	_ZTCN2v88internal8OFStreamE0_So
	.weak	_ZTCN2v88internal8OFStreamE0_So
	.section	.rodata._ZTCN2v88internal8OFStreamE0_So,"aG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal8OFStreamE0_So, @object
	.size	_ZTCN2v88internal8OFStreamE0_So, 80
_ZTCN2v88internal8OFStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal8OFStreamE, @object
	.size	_ZTTN2v88internal8OFStreamE, 32
_ZTTN2v88internal8OFStreamE:
	.quad	_ZTVN2v88internal8OFStreamE+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+64
	.quad	_ZTVN2v88internal8OFStreamE+64
	.weak	_ZTVN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal8OFStreamE, @object
	.size	_ZTVN2v88internal8OFStreamE, 80
_ZTVN2v88internal8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8OFStreamD1Ev
	.quad	_ZN2v88internal8OFStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.weak	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68
	.section	.bss._ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68,"awG",@nobits,_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68,comdat
	.align 8
	.type	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68, @gnu_unique_object
	.size	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68, 8
_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68:
	.zero	8
	.weak	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62
	.section	.bss._ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62,"awG",@nobits,_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62,comdat
	.align 8
	.type	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62, @gnu_unique_object
	.size	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62, 8
_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC98:
	.long	0
	.long	1127219200
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC117:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC118:
	.long	0
	.long	1072693248
	.align 8
.LC120:
	.long	0
	.long	2146959360
	.align 8
.LC121:
	.long	3269197824
	.long	-1019301368
	.align 8
.LC122:
	.long	3269197824
	.long	1128182280
	.align 8
.LC123:
	.long	4294967295
	.long	2146435071
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
