	.file	"scope-info.cc"
	.text
	.section	.text._ZNK2v88internal29NativesExternalStringResource4dataEv,"axG",@progbits,_ZNK2v88internal29NativesExternalStringResource4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal29NativesExternalStringResource4dataEv
	.type	_ZNK2v88internal29NativesExternalStringResource4dataEv, @function
_ZNK2v88internal29NativesExternalStringResource4dataEv:
.LFB10432:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE10432:
	.size	_ZNK2v88internal29NativesExternalStringResource4dataEv, .-_ZNK2v88internal29NativesExternalStringResource4dataEv
	.section	.text._ZNK2v88internal9ScopeInfo13ContextLengthEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal9ScopeInfo13ContextLengthEv.part.0, @function
_ZNK2v88internal9ScopeInfo13ContextLengthEv.part.0:
.LFB23879:
	.cfi_startproc
	movq	(%rdi), %rdx
	movl	11(%rdx), %r9d
	testl	%r9d, %r9d
	jle	.L25
	movq	31(%rdx), %rax
	movq	15(%rdx), %rcx
	shrq	$43, %rcx
	sarq	$32, %rax
	movl	%ecx, %esi
	movq	15(%rdx), %rcx
	movl	%eax, %edi
	andl	$3, %esi
	testl	%eax, %eax
	setg	%r8b
	shrq	$55, %rcx
	andl	$1, %ecx
	orb	%cl, %r8b
	je	.L40
	xorl	%edx, %edx
	cmpl	$2, %esi
	sete	%dl
	leal	4(%rdx,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	addl	$5, %eax
	cmpl	$2, %esi
	jne	.L4
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	xorl	%edi, %edi
.L4:
	movl	11(%rdx), %r8d
	testl	%r8d, %r8d
	jle	.L7
	movq	15(%rdx), %rax
	sarq	$32, %rax
	andl	$15, %eax
	cmpb	$7, %al
	jne	.L41
.L7:
	leal	4(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	movq	15(%rdx), %rax
	sarq	$32, %rax
	testb	$15, %al
	je	.L7
	movq	15(%rdx), %rax
	sarq	$32, %rax
	andl	$15, %eax
	cmpb	$6, %al
	jne	.L14
	movq	15(%rdx), %rax
	btq	$36, %rax
	jc	.L42
.L14:
	movl	11(%rdx), %esi
	testl	%esi, %esi
	jle	.L21
	movq	15(%rdx), %rax
	sarq	$32, %rax
	andl	$15, %eax
	cmpb	$2, %al
	jne	.L18
	movq	15(%rdx), %rax
	btq	$36, %rax
	jc	.L7
.L18:
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L21
	movq	15(%rdx), %rax
	sarq	$32, %rax
	andl	$15, %eax
	cmpb	$2, %al
	jne	.L22
	movq	15(%rdx), %rax
	btq	$46, %rax
	jc	.L7
.L22:
	movl	11(%rdx), %eax
	testl	%eax, %eax
	jle	.L21
	movq	15(%rdx), %rax
	sarq	$32, %rax
	andl	$15, %eax
	cmpb	$3, %al
	je	.L7
.L21:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	movq	15(%rdx), %rax
	btq	$38, %rax
	jc	.L7
	jmp	.L14
	.cfi_endproc
.LFE23879:
	.size	_ZNK2v88internal9ScopeInfo13ContextLengthEv.part.0, .-_ZNK2v88internal9ScopeInfo13ContextLengthEv.part.0
	.section	.text._ZN2v88internal9ScopeInfo18CreateForWithScopeEPNS0_7IsolateENS0_11MaybeHandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ScopeInfo18CreateForWithScopeEPNS0_7IsolateENS0_11MaybeHandleIS1_EE
	.type	_ZN2v88internal9ScopeInfo18CreateForWithScopeEPNS0_7IsolateENS0_11MaybeHandleIS1_EE, @function
_ZN2v88internal9ScopeInfo18CreateForWithScopeEPNS0_7IsolateENS0_11MaybeHandleIS1_EE:
.LFB19285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	setne	%sil
	setne	%bl
	movzbl	%sil, %esi
	movzbl	%bl, %ebx
	addl	$3, %esi
	sall	$21, %ebx
	call	_ZN2v88internal7Factory12NewScopeInfoEiNS0_14AllocationTypeE@PLT
	orl	$32775, %ebx
	movq	%rax, %r12
	movq	(%rax), %rax
	salq	$32, %rbx
	movq	%rbx, 15(%rax)
	movq	(%r12), %rax
	movq	$0, 23(%rax)
	movq	(%r12), %rax
	movq	$0, 31(%rax)
	testq	%r13, %r13
	je	.L45
	movq	(%r12), %r14
	movq	0(%r13), %r13
	movq	%r13, 39(%r14)
	leaq	39(%r14), %r15
	testb	$1, %r13b
	je	.L45
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L47
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L47:
	testb	$24, %al
	je	.L45
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L61
.L45:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L45
	.cfi_endproc
.LFE19285:
	.size	_ZN2v88internal9ScopeInfo18CreateForWithScopeEPNS0_7IsolateENS0_11MaybeHandleIS1_EE, .-_ZN2v88internal9ScopeInfo18CreateForWithScopeEPNS0_7IsolateENS0_11MaybeHandleIS1_EE
	.section	.text._ZN2v88internal9ScopeInfo23CreateGlobalThisBindingEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ScopeInfo23CreateGlobalThisBindingEPNS0_7IsolateE
	.type	_ZN2v88internal9ScopeInfo23CreateGlobalThisBindingEPNS0_7IsolateE, @function
_ZN2v88internal9ScopeInfo23CreateGlobalThisBindingEPNS0_7IsolateE:
.LFB19294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %edx
	movl	$8, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal7Factory12NewScopeInfoEiNS0_14AllocationTypeE@PLT
	movabsq	$142129057759232, %rdx
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	%rdx, 15(%rax)
	movabsq	$4294967296, %rdx
	movq	(%r12), %rax
	movq	$0, 23(%rax)
	movq	(%r12), %rax
	movq	%rdx, 31(%rax)
	movq	(%r12), %r14
	movq	3424(%rbx), %r13
	movq	%r13, 39(%r14)
	testb	$1, %r13b
	je	.L74
	movq	%r13, %rbx
	leaq	39(%r14), %r15
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L94
	testb	$24, %al
	je	.L74
.L100:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L95
	.p2align 4,,10
	.p2align 3
.L74:
	movabsq	$18014196646019072, %rdx
	movq	(%r12), %rax
	movq	%rdx, 47(%rax)
	movabsq	$17179869184, %rdx
	movq	(%r12), %rax
	movq	%rdx, 55(%rax)
	movq	(%r12), %r14
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	movq	%r13, 63(%r14)
	leaq	63(%r14), %r15
	testb	$1, %r13b
	je	.L73
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L96
	testb	$24, %al
	je	.L73
.L102:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L97
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%r12), %r14
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	movq	%r13, 71(%r14)
	leaq	71(%r14), %r15
	testb	$1, %r13b
	je	.L72
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L98
	testb	$24, %al
	je	.L72
.L101:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L99
.L72:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L100
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L101
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L96:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L102
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L95:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L97:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L99:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L72
	.cfi_endproc
.LFE19294:
	.size	_ZN2v88internal9ScopeInfo23CreateGlobalThisBindingEPNS0_7IsolateE, .-_ZN2v88internal9ScopeInfo23CreateGlobalThisBindingEPNS0_7IsolateE
	.section	.text._ZN2v88internal9ScopeInfo22CreateForEmptyFunctionEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ScopeInfo22CreateForEmptyFunctionEPNS0_7IsolateE
	.type	_ZN2v88internal9ScopeInfo22CreateForEmptyFunctionEPNS0_7IsolateE, @function
_ZN2v88internal9ScopeInfo22CreateForEmptyFunctionEPNS0_7IsolateE:
.LFB19295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %edx
	movl	$8, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal7Factory12NewScopeInfoEiNS0_14AllocationTypeE@PLT
	movabsq	$204242874793984, %rdx
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	%rdx, 15(%rax)
	movq	(%r12), %rax
	movq	$0, 23(%rax)
	movq	(%r12), %rax
	movq	$0, 31(%rax)
	movq	(%r12), %r14
	movq	128(%rbx), %r13
	movq	%r13, 39(%r14)
	testb	$1, %r13b
	je	.L123
	movq	%r13, %r15
	leaq	39(%r14), %rsi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L155
	testb	$24, %al
	je	.L123
.L165:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L156
	.p2align 4,,10
	.p2align 3
.L123:
	movq	(%r12), %r14
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	movq	%r13, 47(%r14)
	leaq	47(%r14), %rsi
	testb	$1, %r13b
	je	.L122
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L157
	testb	$24, %al
	je	.L122
.L169:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L158
	.p2align 4,,10
	.p2align 3
.L122:
	movq	(%r12), %r14
	movq	128(%rbx), %r13
	movq	%r13, 55(%r14)
	leaq	55(%r14), %r15
	testb	$1, %r13b
	je	.L121
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L159
	testb	$24, %al
	je	.L121
.L168:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L160
	.p2align 4,,10
	.p2align 3
.L121:
	movq	(%r12), %r14
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	movq	%r13, 63(%r14)
	leaq	63(%r14), %r15
	testb	$1, %r13b
	je	.L120
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L161
	testb	$24, %al
	je	.L120
.L167:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L162
	.p2align 4,,10
	.p2align 3
.L120:
	movq	(%r12), %r14
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	movq	%r13, 71(%r14)
	leaq	71(%r14), %r15
	testb	$1, %r13b
	je	.L119
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L163
	testb	$24, %al
	je	.L119
.L166:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L164
.L119:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L165
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L163:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L166
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L167
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L168
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L157:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L169
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L158:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L160:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L162:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L164:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L119
	.cfi_endproc
.LFE19295:
	.size	_ZN2v88internal9ScopeInfo22CreateForEmptyFunctionEPNS0_7IsolateE, .-_ZN2v88internal9ScopeInfo22CreateForEmptyFunctionEPNS0_7IsolateE
	.section	.text._ZN2v88internal9ScopeInfo22CreateForBootstrappingEPNS0_7IsolateENS0_9ScopeTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ScopeInfo22CreateForBootstrappingEPNS0_7IsolateENS0_9ScopeTypeE
	.type	_ZN2v88internal9ScopeInfo22CreateForBootstrappingEPNS0_7IsolateENS0_9ScopeTypeE, @function
_ZN2v88internal9ScopeInfo22CreateForBootstrappingEPNS0_7IsolateENS0_9ScopeTypeE:
.LFB19296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$56, %rsp
	cmpb	$2, %sil
	sete	%r13b
	je	.L243
	movl	$4, %edx
	movl	$8, %esi
	call	_ZN2v88internal7Factory12NewScopeInfoEiNS0_14AllocationTypeE@PLT
	movl	$1, %edx
	movabsq	$4294967296, %rcx
	movq	%rax, %r12
	movzbl	%bl, %eax
	orb	$1, %ah
	movl	%eax, %edi
	xorl	%eax, %eax
.L201:
	movzbl	%r13b, %r13d
	orl	%edi, %eax
	movq	(%r12), %rsi
	sall	$13, %r13d
	orl	%r13d, %eax
	orl	$32832, %eax
	salq	$32, %rax
	movq	%rax, 15(%rsi)
	movq	(%r12), %rax
	movq	$0, 23(%rax)
	movq	(%r12), %rax
	movq	%rcx, 31(%rax)
	testl	%edx, %edx
	jne	.L244
	movl	$64, %ecx
	movl	$56, %eax
	movl	$40, %edx
	movl	$72, %r8d
	movl	$48, %r15d
.L173:
	movq	(%r12), %r13
	leaq	-1(%r13,%rdx), %rsi
	cmpb	$2, %bl
	jne	.L245
	movq	128(%r14), %rbx
	movq	%rbx, (%rsi)
	testb	$1, %bl
	je	.L197
	movq	%rbx, %r9
	andq	$-262144, %r9
	movq	8(%r9), %rdx
	movq	%r9, -56(%rbp)
	testl	$262144, %edx
	je	.L179
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%r8, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r9
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rax
	movq	8(%r9), %rdx
	movq	-64(%rbp), %rsi
.L179:
	andl	$24, %edx
	je	.L197
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L246
	.p2align 4,,10
	.p2align 3
.L197:
	movq	(%r12), %rbx
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	leaq	-1(%rbx,%r15), %r15
	movq	%r13, (%r15)
	testb	$1, %r13b
	je	.L196
	movq	%r13, %r9
	andq	$-262144, %r9
	movq	8(%r9), %rdx
	movq	%r9, -56(%rbp)
	testl	$262144, %edx
	je	.L182
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r9
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %rax
	movq	8(%r9), %rdx
.L182:
	andl	$24, %edx
	je	.L196
	movq	%rbx, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L247
	.p2align 4,,10
	.p2align 3
.L196:
	movq	(%r12), %r15
	movq	128(%r14), %r13
	leaq	-1(%r15,%rax), %r14
	movq	%r13, (%r14)
	testb	$1, %r13b
	je	.L195
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L185
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
.L185:
	testb	$24, %al
	je	.L195
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L248
	.p2align 4,,10
	.p2align 3
.L195:
	movq	%rcx, %r15
	movq	%r8, %rax
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L245:
	movabsq	$17179869184, %rdx
	movq	%rdx, (%rsi)
.L178:
	movq	(%r12), %r14
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	leaq	-1(%r14,%r15), %r15
	movq	%r13, (%r15)
	testb	$1, %r13b
	je	.L194
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rdx
	testl	$262144, %edx
	jne	.L249
	andl	$24, %edx
	je	.L194
.L254:
	movq	%r14, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L250
	.p2align 4,,10
	.p2align 3
.L194:
	movq	(%r12), %r14
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	leaq	-1(%r14,%rax), %r15
	movq	%r13, (%r15)
	testb	$1, %r13b
	je	.L215
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L251
	testb	$24, %al
	je	.L215
.L253:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L252
.L215:
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L253
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L249:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rdx
	movq	-56(%rbp), %rax
	andl	$24, %edx
	jne	.L254
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L243:
	movl	$4, %edx
	movl	$8, %esi
	call	_ZN2v88internal7Factory12NewScopeInfoEiNS0_14AllocationTypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$386, %edi
	movq	%rax, %r12
	movl	$6144, %eax
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L244:
	movq	(%r12), %r15
	movq	3424(%r14), %r13
	movq	%r13, 39(%r15)
	leaq	39(%r15), %rsi
	testb	$1, %r13b
	je	.L200
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L255
	testb	$24, %al
	je	.L200
.L256:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L200
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L200:
	movq	(%r12), %rax
	movl	$80, %ecx
	movabsq	$18014196646019072, %rdx
	movl	$88, %r8d
	movl	$64, %r15d
	movq	%rdx, 47(%rax)
	movl	$72, %eax
	movl	$56, %edx
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L250:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rax
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L252:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L255:
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L256
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L246:
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rax
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rax
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L248:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
	jmp	.L195
	.cfi_endproc
.LFE19296:
	.size	_ZN2v88internal9ScopeInfo22CreateForBootstrappingEPNS0_7IsolateENS0_9ScopeTypeE, .-_ZN2v88internal9ScopeInfo22CreateForBootstrappingEPNS0_7IsolateENS0_9ScopeTypeE
	.section	.text._ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE
	.type	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE, @function
_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE:
.LFB19300:
	.cfi_startproc
	endbr64
	movq	280(%rdi), %rax
	ret
	.cfi_endproc
.LFE19300:
	.size	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE, .-_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE
	.section	.text._ZNK2v88internal9ScopeInfo10scope_typeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo10scope_typeEv
	.type	_ZNK2v88internal9ScopeInfo10scope_typeEv, @function
_ZNK2v88internal9ScopeInfo10scope_typeEv:
.LFB19301:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L258
	movq	15(%rdx), %rax
	sarq	$32, %rax
	andl	$15, %eax
.L258:
	ret
	.cfi_endproc
.LFE19301:
	.size	_ZNK2v88internal9ScopeInfo10scope_typeEv, .-_ZNK2v88internal9ScopeInfo10scope_typeEv
	.section	.text._ZNK2v88internal9ScopeInfo23SloppyEvalCanExtendVarsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo23SloppyEvalCanExtendVarsEv
	.type	_ZNK2v88internal9ScopeInfo23SloppyEvalCanExtendVarsEv, @function
_ZNK2v88internal9ScopeInfo23SloppyEvalCanExtendVarsEv:
.LFB19302:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L264
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	movq	15(%rdx), %rax
	shrq	$36, %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE19302:
	.size	_ZNK2v88internal9ScopeInfo23SloppyEvalCanExtendVarsEv, .-_ZNK2v88internal9ScopeInfo23SloppyEvalCanExtendVarsEv
	.section	.text._ZNK2v88internal9ScopeInfo13language_modeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo13language_modeEv
	.type	_ZNK2v88internal9ScopeInfo13language_modeEv, @function
_ZNK2v88internal9ScopeInfo13language_modeEv:
.LFB19303:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L268
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	movq	15(%rdx), %rax
	shrq	$37, %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE19303:
	.size	_ZNK2v88internal9ScopeInfo13language_modeEv, .-_ZNK2v88internal9ScopeInfo13language_modeEv
	.section	.text._ZNK2v88internal9ScopeInfo20is_declaration_scopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo20is_declaration_scopeEv
	.type	_ZNK2v88internal9ScopeInfo20is_declaration_scopeEv, @function
_ZNK2v88internal9ScopeInfo20is_declaration_scopeEv:
.LFB19304:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L269
	movq	15(%rdx), %rax
	shrq	$38, %rax
	andl	$1, %eax
.L269:
	ret
	.cfi_endproc
.LFE19304:
	.size	_ZNK2v88internal9ScopeInfo20is_declaration_scopeEv, .-_ZNK2v88internal9ScopeInfo20is_declaration_scopeEv
	.section	.text._ZNK2v88internal9ScopeInfo13ContextLengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo13ContextLengthEv
	.type	_ZNK2v88internal9ScopeInfo13ContextLengthEv, @function
_ZNK2v88internal9ScopeInfo13ContextLengthEv:
.LFB19305:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	jg	.L274
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	jmp	_ZNK2v88internal9ScopeInfo13ContextLengthEv.part.0
	.cfi_endproc
.LFE19305:
	.size	_ZNK2v88internal9ScopeInfo13ContextLengthEv, .-_ZNK2v88internal9ScopeInfo13ContextLengthEv
	.section	.text._ZNK2v88internal9ScopeInfo11HasReceiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo11HasReceiverEv
	.type	_ZNK2v88internal9ScopeInfo11HasReceiverEv, @function
_ZNK2v88internal9ScopeInfo11HasReceiverEv:
.LFB19306:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	movslq	11(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.L281
.L275:
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	jle	.L275
	movq	15(%rdx), %rax
	shrq	$39, %rax
	testb	$3, %al
	setne	%al
	ret
	.cfi_endproc
.LFE19306:
	.size	_ZNK2v88internal9ScopeInfo11HasReceiverEv, .-_ZNK2v88internal9ScopeInfo11HasReceiverEv
	.section	.text._ZNK2v88internal9ScopeInfo20HasAllocatedReceiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo20HasAllocatedReceiverEv
	.type	_ZNK2v88internal9ScopeInfo20HasAllocatedReceiverEv, @function
_ZNK2v88internal9ScopeInfo20HasAllocatedReceiverEv:
.LFB19307:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	movslq	11(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.L288
.L282:
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	jle	.L282
	movq	15(%rdx), %rax
	shrq	$39, %rax
	andl	$3, %eax
	subl	$1, %eax
	cmpl	$1, %eax
	setbe	%al
	ret
	.cfi_endproc
.LFE19307:
	.size	_ZNK2v88internal9ScopeInfo20HasAllocatedReceiverEv, .-_ZNK2v88internal9ScopeInfo20HasAllocatedReceiverEv
	.section	.text._ZNK2v88internal9ScopeInfo13HasClassBrandEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo13HasClassBrandEv
	.type	_ZNK2v88internal9ScopeInfo13HasClassBrandEv, @function
_ZNK2v88internal9ScopeInfo13HasClassBrandEv:
.LFB19308:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L289
	movq	15(%rdx), %rax
	shrq	$41, %rax
	andl	$1, %eax
.L289:
	ret
	.cfi_endproc
.LFE19308:
	.size	_ZNK2v88internal9ScopeInfo13HasClassBrandEv, .-_ZNK2v88internal9ScopeInfo13HasClassBrandEv
	.section	.text._ZNK2v88internal9ScopeInfo12HasNewTargetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo12HasNewTargetEv
	.type	_ZNK2v88internal9ScopeInfo12HasNewTargetEv, @function
_ZNK2v88internal9ScopeInfo12HasNewTargetEv:
.LFB19309:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L292
	movq	15(%rdx), %rax
	shrq	$42, %rax
	andl	$1, %eax
.L292:
	ret
	.cfi_endproc
.LFE19309:
	.size	_ZNK2v88internal9ScopeInfo12HasNewTargetEv, .-_ZNK2v88internal9ScopeInfo12HasNewTargetEv
	.section	.text._ZNK2v88internal9ScopeInfo15HasFunctionNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv
	.type	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv, @function
_ZNK2v88internal9ScopeInfo15HasFunctionNameEv:
.LFB19310:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	movslq	11(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.L301
.L295:
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	jle	.L295
	movq	15(%rdx), %rax
	shrq	$43, %rax
	testb	$3, %al
	setne	%al
	ret
	.cfi_endproc
.LFE19310:
	.size	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv, .-_ZNK2v88internal9ScopeInfo15HasFunctionNameEv
	.section	.text._ZNK2v88internal9ScopeInfo23HasInferredFunctionNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo23HasInferredFunctionNameEv
	.type	_ZNK2v88internal9ScopeInfo23HasInferredFunctionNameEv, @function
_ZNK2v88internal9ScopeInfo23HasInferredFunctionNameEv:
.LFB19311:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	movslq	11(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.L308
.L302:
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	jle	.L302
	movq	15(%rdx), %rax
	shrq	$45, %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE19311:
	.size	_ZNK2v88internal9ScopeInfo23HasInferredFunctionNameEv, .-_ZNK2v88internal9ScopeInfo23HasInferredFunctionNameEv
	.section	.text._ZNK2v88internal9ScopeInfo15HasPositionInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo15HasPositionInfoEv
	.type	_ZNK2v88internal9ScopeInfo15HasPositionInfoEv, @function
_ZNK2v88internal9ScopeInfo15HasPositionInfoEv:
.LFB19312:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	movslq	11(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.L317
.L309:
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	jle	.L309
	movq	15(%rdx), %rdx
	movl	$1, %eax
	sarq	$32, %rdx
	movl	%edx, %ecx
	andl	$15, %ecx
	subl	$2, %ecx
	andl	$253, %ecx
	je	.L309
	andl	$13, %edx
	cmpb	$1, %dl
	sete	%al
	ret
	.cfi_endproc
.LFE19312:
	.size	_ZNK2v88internal9ScopeInfo15HasPositionInfoEv, .-_ZNK2v88internal9ScopeInfo15HasPositionInfoEv
	.section	.text._ZN2v88internal9ScopeInfo17NeedsPositionInfoENS0_9ScopeTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ScopeInfo17NeedsPositionInfoENS0_9ScopeTypeE
	.type	_ZN2v88internal9ScopeInfo17NeedsPositionInfoENS0_9ScopeTypeE, @function
_ZN2v88internal9ScopeInfo17NeedsPositionInfoENS0_9ScopeTypeE:
.LFB19313:
	.cfi_startproc
	endbr64
	leal	-2(%rdi), %edx
	movl	$1, %eax
	andl	$253, %edx
	je	.L318
	andl	$-3, %edi
	cmpb	$1, %dil
	sete	%al
.L318:
	ret
	.cfi_endproc
.LFE19313:
	.size	_ZN2v88internal9ScopeInfo17NeedsPositionInfoENS0_9ScopeTypeE, .-_ZN2v88internal9ScopeInfo17NeedsPositionInfoENS0_9ScopeTypeE
	.section	.text._ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv
	.type	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv, @function
_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv:
.LFB19314:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	$3, %eax
	movslq	11(%rdx), %rcx
	testq	%rcx, %rcx
	jle	.L323
	movq	31(%rdx), %rax
	sarq	$32, %rax
	addl	$3, %eax
	testq	%rcx, %rcx
	jle	.L323
	movq	31(%rdx), %rsi
	sarq	$32, %rsi
	addl	%esi, %eax
.L323:
	testq	%rcx, %rcx
	je	.L324
	testl	%ecx, %ecx
	jg	.L332
.L324:
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	movq	15(%rdx), %rcx
	movq	(%rdi), %rdx
	shrq	$39, %rcx
	andl	$3, %ecx
	subl	$1, %ecx
	cmpl	$1, %ecx
	setbe	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %eax
	jmp	.L324
	.cfi_endproc
.LFE19314:
	.size	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv, .-_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv
	.section	.text._ZN2v88internal9ScopeInfo15SetFunctionNameENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ScopeInfo15SetFunctionNameENS0_6ObjectE
	.type	_ZN2v88internal9ScopeInfo15SetFunctionNameENS0_6ObjectE, @function
_ZN2v88internal9ScopeInfo15SetFunctionNameENS0_6ObjectE:
.LFB19315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rdx
	movq	%rdi, %rbx
	movslq	11(%rdx), %rcx
	testq	%rcx, %rcx
	jle	.L334
	movq	31(%rdx), %rax
	sarq	$32, %rax
	addl	$3, %eax
	testq	%rcx, %rcx
	jle	.L334
	movq	31(%rdx), %rsi
	sarq	$32, %rsi
	addl	%esi, %eax
.L334:
	testq	%rcx, %rcx
	je	.L335
	testl	%ecx, %ecx
	jg	.L354
.L335:
	leal	16(,%rax,8), %r13d
	movslq	%r13d, %r13
	movq	%r12, -1(%r13,%rdx)
	testb	$1, %r12b
	je	.L333
	movq	%r12, %r14
	movq	(%rbx), %rdi
	andq	$-262144, %r14
	movq	8(%r14), %rax
	leaq	-1(%r13,%rdi), %rsi
	testl	$262144, %eax
	jne	.L355
	testb	$24, %al
	je	.L333
.L357:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L356
.L333:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L355:
	.cfi_restore_state
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r14), %rax
	leaq	-1(%r13,%rdi), %rsi
	testb	$24, %al
	jne	.L357
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L354:
	movq	15(%rdx), %rcx
	movq	(%rbx), %rdx
	shrq	$39, %rcx
	andl	$3, %ecx
	subl	$1, %ecx
	cmpl	$1, %ecx
	setbe	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %eax
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L356:
	popq	%rbx
	movq	%r12, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE19315:
	.size	_ZN2v88internal9ScopeInfo15SetFunctionNameENS0_6ObjectE, .-_ZN2v88internal9ScopeInfo15SetFunctionNameENS0_6ObjectE
	.section	.text._ZN2v88internal9ScopeInfo23SetInferredFunctionNameENS0_6StringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ScopeInfo23SetInferredFunctionNameENS0_6StringE
	.type	_ZN2v88internal9ScopeInfo23SetInferredFunctionNameENS0_6StringE, @function
_ZN2v88internal9ScopeInfo23SetInferredFunctionNameENS0_6StringE:
.LFB19316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rdx
	movslq	11(%rdx), %rcx
	testq	%rcx, %rcx
	jle	.L359
	movq	31(%rdx), %rax
	sarq	$32, %rax
	addl	$3, %eax
	testq	%rcx, %rcx
	jle	.L359
	movq	31(%rdx), %rsi
	sarq	$32, %rsi
	addl	%esi, %eax
.L359:
	testq	%rcx, %rcx
	je	.L360
	testl	%ecx, %ecx
	jg	.L388
.L360:
	addl	$2, %eax
.L361:
	sall	$3, %eax
	movslq	%eax, %rbx
	movq	%r12, -1(%rbx,%rdx)
	testb	$1, %r12b
	je	.L358
	movq	%r12, %r14
	movq	0(%r13), %rdi
	andq	$-262144, %r14
	movq	8(%r14), %rax
	leaq	-1(%rbx,%rdi), %rsi
	testl	$262144, %eax
	jne	.L389
	testb	$24, %al
	je	.L358
.L391:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L390
.L358:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_restore_state
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rdi
	movq	8(%r14), %rax
	leaq	-1(%rbx,%rdi), %rsi
	testb	$24, %al
	jne	.L391
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L388:
	movq	15(%rdx), %rcx
	movslq	11(%rdx), %rsi
	shrq	$39, %rcx
	andl	$3, %ecx
	subl	$1, %ecx
	cmpl	$1, %ecx
	setbe	%cl
	movzbl	%cl, %ecx
	addl	%eax, %ecx
	leal	2(%rcx), %eax
	testl	%esi, %esi
	jle	.L361
	testq	%rsi, %rsi
	je	.L361
	movq	15(%rdx), %rsi
	addl	$4, %ecx
	shrq	$43, %rsi
	andl	$3, %esi
	cmovne	%ecx, %eax
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L390:
	popq	%rbx
	movq	%r12, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE19316:
	.size	_ZN2v88internal9ScopeInfo23SetInferredFunctionNameENS0_6StringE, .-_ZN2v88internal9ScopeInfo23SetInferredFunctionNameENS0_6StringE
	.section	.text._ZNK2v88internal9ScopeInfo17HasOuterScopeInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo17HasOuterScopeInfoEv
	.type	_ZNK2v88internal9ScopeInfo17HasOuterScopeInfoEv, @function
_ZNK2v88internal9ScopeInfo17HasOuterScopeInfoEv:
.LFB19317:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	movslq	11(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.L398
.L392:
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	jle	.L392
	movq	15(%rdx), %rax
	shrq	$53, %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE19317:
	.size	_ZNK2v88internal9ScopeInfo17HasOuterScopeInfoEv, .-_ZNK2v88internal9ScopeInfo17HasOuterScopeInfoEv
	.section	.text._ZNK2v88internal9ScopeInfo20IsDebugEvaluateScopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo20IsDebugEvaluateScopeEv
	.type	_ZNK2v88internal9ScopeInfo20IsDebugEvaluateScopeEv, @function
_ZNK2v88internal9ScopeInfo20IsDebugEvaluateScopeEv:
.LFB19318:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	movslq	11(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.L405
.L399:
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	jle	.L399
	movq	15(%rdx), %rax
	shrq	$54, %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE19318:
	.size	_ZNK2v88internal9ScopeInfo20IsDebugEvaluateScopeEv, .-_ZNK2v88internal9ScopeInfo20IsDebugEvaluateScopeEv
	.section	.rodata._ZN2v88internal9ScopeInfo23SetIsDebugEvaluateScopeEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal9ScopeInfo23SetIsDebugEvaluateScopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ScopeInfo23SetIsDebugEvaluateScopeEv
	.type	_ZN2v88internal9ScopeInfo23SetIsDebugEvaluateScopeEv, @function
_ZN2v88internal9ScopeInfo23SetIsDebugEvaluateScopeEv:
.LFB19319:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	11(%rdx), %eax
	testl	%eax, %eax
	jle	.L407
	movq	15(%rdx), %rax
	sarq	$32, %rax
	orl	$4194304, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	ret
.L407:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19319:
	.size	_ZN2v88internal9ScopeInfo23SetIsDebugEvaluateScopeEv, .-_ZN2v88internal9ScopeInfo23SetIsDebugEvaluateScopeEv
	.section	.text._ZNK2v88internal9ScopeInfo10HasContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo10HasContextEv
	.type	_ZNK2v88internal9ScopeInfo10HasContextEv, @function
_ZNK2v88internal9ScopeInfo10HasContextEv:
.LFB19320:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	11(%rdx), %eax
	testl	%eax, %eax
	jg	.L419
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L419:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal9ScopeInfo13ContextLengthEv.part.0
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setg	%al
	ret
	.cfi_endproc
.LFE19320:
	.size	_ZNK2v88internal9ScopeInfo10HasContextEv, .-_ZNK2v88internal9ScopeInfo10HasContextEv
	.section	.text._ZNK2v88internal9ScopeInfo12FunctionNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo12FunctionNameEv
	.type	_ZNK2v88internal9ScopeInfo12FunctionNameEv, @function
_ZNK2v88internal9ScopeInfo12FunctionNameEv:
.LFB19321:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	$3, %eax
	movslq	11(%rdx), %rcx
	testq	%rcx, %rcx
	jle	.L421
	movq	31(%rdx), %rax
	sarq	$32, %rax
	addl	$3, %eax
	testq	%rcx, %rcx
	jle	.L421
	movq	31(%rdx), %rsi
	sarq	$32, %rsi
	addl	%esi, %eax
.L421:
	testq	%rcx, %rcx
	je	.L422
	testl	%ecx, %ecx
	jg	.L430
.L422:
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	movq	15(%rdx), %rcx
	movq	(%rdi), %rdx
	shrq	$39, %rcx
	andl	$3, %ecx
	subl	$1, %ecx
	cmpl	$1, %ecx
	setbe	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %eax
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	ret
	.cfi_endproc
.LFE19321:
	.size	_ZNK2v88internal9ScopeInfo12FunctionNameEv, .-_ZNK2v88internal9ScopeInfo12FunctionNameEv
	.section	.text._ZNK2v88internal9ScopeInfo16ContextLocalNameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo16ContextLocalNameEi
	.type	_ZNK2v88internal9ScopeInfo16ContextLocalNameEi, @function
_ZNK2v88internal9ScopeInfo16ContextLocalNameEi:
.LFB19329:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	leal	40(,%rsi,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	ret
	.cfi_endproc
.LFE19329:
	.size	_ZNK2v88internal9ScopeInfo16ContextLocalNameEi, .-_ZNK2v88internal9ScopeInfo16ContextLocalNameEi
	.section	.text._ZNK2v88internal9ScopeInfo16ContextLocalModeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo16ContextLocalModeEi
	.type	_ZNK2v88internal9ScopeInfo16ContextLocalModeEi, @function
_ZNK2v88internal9ScopeInfo16ContextLocalModeEi:
.LFB19330:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	$3, %eax
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L433
	movq	31(%rdx), %rax
	sarq	$32, %rax
	addl	$3, %eax
.L433:
	leal	2(%rax,%rsi), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	sarq	$32, %rax
	andl	$15, %eax
	ret
	.cfi_endproc
.LFE19330:
	.size	_ZNK2v88internal9ScopeInfo16ContextLocalModeEi, .-_ZNK2v88internal9ScopeInfo16ContextLocalModeEi
	.section	.text._ZNK2v88internal9ScopeInfo20ContextLocalInitFlagEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo20ContextLocalInitFlagEi
	.type	_ZNK2v88internal9ScopeInfo20ContextLocalInitFlagEi, @function
_ZNK2v88internal9ScopeInfo20ContextLocalInitFlagEi:
.LFB19331:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	$3, %eax
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L436
	movq	31(%rdx), %rax
	sarq	$32, %rax
	addl	$3, %eax
.L436:
	leal	2(%rax,%rsi), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	shrq	$36, %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE19331:
	.size	_ZNK2v88internal9ScopeInfo20ContextLocalInitFlagEi, .-_ZNK2v88internal9ScopeInfo20ContextLocalInitFlagEi
	.section	.text._ZNK2v88internal9ScopeInfo23ContextLocalIsParameterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo23ContextLocalIsParameterEi
	.type	_ZNK2v88internal9ScopeInfo23ContextLocalIsParameterEi, @function
_ZNK2v88internal9ScopeInfo23ContextLocalIsParameterEi:
.LFB19332:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	$3, %eax
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L439
	movq	31(%rdx), %rax
	sarq	$32, %rax
	addl	$3, %eax
.L439:
	leal	2(%rax,%rsi), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	shrq	$38, %rax
	movzwl	%ax, %eax
	cmpl	$65535, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE19332:
	.size	_ZNK2v88internal9ScopeInfo23ContextLocalIsParameterEi, .-_ZNK2v88internal9ScopeInfo23ContextLocalIsParameterEi
	.section	.text._ZNK2v88internal9ScopeInfo27ContextLocalParameterNumberEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo27ContextLocalParameterNumberEi
	.type	_ZNK2v88internal9ScopeInfo27ContextLocalParameterNumberEi, @function
_ZNK2v88internal9ScopeInfo27ContextLocalParameterNumberEi:
.LFB19333:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	$3, %eax
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L442
	movq	31(%rdx), %rax
	sarq	$32, %rax
	addl	$3, %eax
.L442:
	leal	2(%rax,%rsi), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	shrq	$38, %rax
	movzwl	%ax, %eax
	ret
	.cfi_endproc
.LFE19333:
	.size	_ZNK2v88internal9ScopeInfo27ContextLocalParameterNumberEi, .-_ZNK2v88internal9ScopeInfo27ContextLocalParameterNumberEi
	.section	.text._ZNK2v88internal9ScopeInfo29ContextLocalMaybeAssignedFlagEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo29ContextLocalMaybeAssignedFlagEi
	.type	_ZNK2v88internal9ScopeInfo29ContextLocalMaybeAssignedFlagEi, @function
_ZNK2v88internal9ScopeInfo29ContextLocalMaybeAssignedFlagEi:
.LFB19334:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	$3, %eax
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L445
	movq	31(%rdx), %rax
	sarq	$32, %rax
	addl	$3, %eax
.L445:
	leal	2(%rax,%rsi), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	shrq	$37, %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE19334:
	.size	_ZNK2v88internal9ScopeInfo29ContextLocalMaybeAssignedFlagEi, .-_ZNK2v88internal9ScopeInfo29ContextLocalMaybeAssignedFlagEi
	.section	.text._ZN2v88internal9ScopeInfo19VariableIsSyntheticENS0_6StringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ScopeInfo19VariableIsSyntheticENS0_6StringE
	.type	_ZN2v88internal9ScopeInfo19VariableIsSyntheticENS0_6StringE, @function
_ZN2v88internal9ScopeInfo19VariableIsSyntheticENS0_6StringE:
.LFB19335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movl	11(%rdi), %eax
	movq	%fs:40, %rsi
	movq	%rsi, -8(%rbp)
	xorl	%esi, %esi
	testl	%eax, %eax
	jne	.L448
.L484:
	movl	$1, %eax
.L447:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L485
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L448:
	.cfi_restore_state
	movq	-1(%rdi), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L450
	leaq	.L452(%rip), %rcx
	movzwl	%dx, %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal9ScopeInfo19VariableIsSyntheticENS0_6StringE,"a",@progbits
	.align 4
	.align 4
.L452:
	.long	.L458-.L452
	.long	.L455-.L452
	.long	.L457-.L452
	.long	.L453-.L452
	.long	.L450-.L452
	.long	.L451-.L452
	.long	.L450-.L452
	.long	.L450-.L452
	.long	.L456-.L452
	.long	.L455-.L452
	.long	.L454-.L452
	.long	.L453-.L452
	.long	.L450-.L452
	.long	.L451-.L452
	.section	.text._ZN2v88internal9ScopeInfo19VariableIsSyntheticENS0_6StringE
	.p2align 4,,10
	.p2align 3
.L451:
	movq	%rdi, -16(%rbp)
	xorl	%esi, %esi
	leaq	-16(%rbp), %rdi
	call	_ZN2v88internal10ThinString3GetEi@PLT
	.p2align 4,,10
	.p2align 3
.L459:
	cmpw	$46, %ax
	je	.L484
	movq	-24(%rbp), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L450
	leaq	.L464(%rip), %rcx
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal9ScopeInfo19VariableIsSyntheticENS0_6StringE
	.align 4
	.align 4
.L464:
	.long	.L470-.L464
	.long	.L467-.L464
	.long	.L469-.L464
	.long	.L465-.L464
	.long	.L450-.L464
	.long	.L463-.L464
	.long	.L450-.L464
	.long	.L450-.L464
	.long	.L468-.L464
	.long	.L467-.L464
	.long	.L466-.L464
	.long	.L465-.L464
	.long	.L450-.L464
	.long	.L463-.L464
	.section	.text._ZN2v88internal9ScopeInfo19VariableIsSyntheticENS0_6StringE
	.p2align 4,,10
	.p2align 3
.L455:
	movq	%rdi, -16(%rbp)
	xorl	%esi, %esi
	leaq	-16(%rbp), %rdi
	call	_ZN2v88internal10ConsString3GetEi@PLT
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L453:
	movq	%rdi, -16(%rbp)
	xorl	%esi, %esi
	leaq	-16(%rbp), %rdi
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	jmp	.L459
.L450:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L456:
	movzbl	15(%rdi), %eax
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L454:
	movq	15(%rdi), %rdi
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L460
	movq	8(%rdi), %rax
.L461:
	movzbl	(%rax), %eax
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L458:
	movzwl	15(%rdi), %eax
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L457:
	movq	15(%rdi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax), %eax
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L463:
	leaq	-16(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rdx, -16(%rbp)
	call	_ZN2v88internal10ThinString3GetEi@PLT
	.p2align 4,,10
	.p2align 3
.L471:
	cmpw	$35, %ax
	je	.L484
	movq	-24(%rbp), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	-34168(%rdx), %rsi
	cmpq	%rax, %rsi
	je	.L484
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L475
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L486
.L475:
	leaq	-24(%rbp), %rdi
	call	_ZN2v88internal6String10SlowEqualsES1_@PLT
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L465:
	leaq	-16(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rdx, -16(%rbp)
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L467:
	leaq	-16(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rdx, -16(%rbp)
	call	_ZN2v88internal10ConsString3GetEi@PLT
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L470:
	movzwl	15(%rdx), %eax
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L469:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax), %eax
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L466:
	movq	15(%rdx), %rdi
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L472
	movq	8(%rdi), %rax
.L473:
	movzbl	(%rax), %eax
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L468:
	movzbl	15(%rdx), %eax
	jmp	.L471
.L460:
	call	*%rax
	jmp	.L461
.L486:
	xorl	%eax, %eax
	jmp	.L447
.L472:
	call	*%rax
	jmp	.L473
.L485:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19335:
	.size	_ZN2v88internal9ScopeInfo19VariableIsSyntheticENS0_6StringE, .-_ZN2v88internal9ScopeInfo19VariableIsSyntheticENS0_6StringE
	.section	.text._ZN2v88internal9ScopeInfo16ContextSlotIndexES1_NS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ScopeInfo16ContextSlotIndexES1_NS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE
	.type	_ZN2v88internal9ScopeInfo16ContextSlotIndexES1_NS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE, @function
_ZN2v88internal9ScopeInfo16ContextSlotIndexES1_NS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE:
.LFB19337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	7(%rdi), %r12
	movq	%r12, %rax
	sarq	$32, %rax
	testq	%rax, %rax
	je	.L490
	jle	.L490
	movq	31(%rdi), %r9
	sarq	$32, %r9
	testl	%r9d, %r9d
	jle	.L490
	leal	3(%r9), %r10d
	leaq	-1(%rdi), %rbx
	movl	$3, %r9d
	leaq	39(%rdi), %rax
.L492:
	movq	(%rax), %r13
	movl	%r9d, %r14d
	movq	%rax, %r11
	addl	$1, %r9d
	cmpq	%rsi, %r13
	je	.L491
	addq	$8, %rax
	cmpl	%r9d, %r10d
	jne	.L492
.L490:
	movl	$-1, %r9d
	popq	%rbx
	popq	%r12
	movl	%r9d, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L491:
	.cfi_restore_state
	sarq	$32, %r12
	leal	-3(%r14), %esi
	movq	%rax, %r10
	testq	%r12, %r12
	jle	.L493
	movq	31(%rdi), %r10
	sarq	$32, %r10
	leal	5(%rsi,%r10), %r10d
	sall	$3, %r10d
	movslq	%r10d, %r10
	addq	%rbx, %r10
.L493:
	movq	(%r10), %r10
	sarq	$32, %r10
	andl	$15, %r10d
	movb	%r10b, (%rdx)
	movl	11(%rdi), %edx
	testl	%edx, %edx
	jle	.L494
	movq	31(%rdi), %rax
	sarq	$32, %rax
	leal	5(%rsi,%rax), %eax
	sall	$3, %eax
	cltq
	addq	%rbx, %rax
.L494:
	movq	(%rax), %rax
	shrq	$36, %rax
	andl	$1, %eax
	movb	%al, (%rcx)
	movl	11(%rdi), %eax
	testl	%eax, %eax
	jle	.L495
	movq	31(%rdi), %rax
	sarq	$32, %rax
	leal	5(%rsi,%rax), %r11d
	sall	$3, %r11d
	movslq	%r11d, %r11
	addq	%rbx, %r11
.L495:
	movq	(%r11), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	shrq	$37, %rax
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	andl	$1, %eax
	movb	%al, (%r8)
	movl	%r9d, %eax
	ret
	.cfi_endproc
.LFE19337:
	.size	_ZN2v88internal9ScopeInfo16ContextSlotIndexES1_NS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE, .-_ZN2v88internal9ScopeInfo16ContextSlotIndexES1_NS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE
	.section	.text._ZNK2v88internal9ScopeInfo24ReceiverContextSlotIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo24ReceiverContextSlotIndexEv
	.type	_ZNK2v88internal9ScopeInfo24ReceiverContextSlotIndexEv, @function
_ZNK2v88internal9ScopeInfo24ReceiverContextSlotIndexEv:
.LFB19338:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	11(%rdx), %eax
	testl	%eax, %eax
	jle	.L502
	movq	15(%rdx), %rax
	shrq	$39, %rax
	andl	$3, %eax
	cmpl	$2, %eax
	jne	.L502
	movq	31(%rdx), %rcx
	movq	31(%rdx), %rax
	sarq	$32, %rcx
	sarq	$32, %rax
	leal	5(%rcx,%rax), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	shrq	$32, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L502:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE19338:
	.size	_ZNK2v88internal9ScopeInfo24ReceiverContextSlotIndexEv, .-_ZNK2v88internal9ScopeInfo24ReceiverContextSlotIndexEv
	.section	.text._ZNK2v88internal9ScopeInfo13function_kindEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo13function_kindEv
	.type	_ZNK2v88internal9ScopeInfo13function_kindEv, @function
_ZNK2v88internal9ScopeInfo13function_kindEv:
.LFB19340:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L506
	movq	15(%rdx), %rax
	shrq	$48, %rax
	andl	$31, %eax
.L506:
	ret
	.cfi_endproc
.LFE19340:
	.size	_ZNK2v88internal9ScopeInfo13function_kindEv, .-_ZNK2v88internal9ScopeInfo13function_kindEv
	.section	.text._ZNK2v88internal9ScopeInfo22ContextLocalNamesIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo22ContextLocalNamesIndexEv
	.type	_ZNK2v88internal9ScopeInfo22ContextLocalNamesIndexEv, @function
_ZNK2v88internal9ScopeInfo22ContextLocalNamesIndexEv:
.LFB19341:
	.cfi_startproc
	endbr64
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE19341:
	.size	_ZNK2v88internal9ScopeInfo22ContextLocalNamesIndexEv, .-_ZNK2v88internal9ScopeInfo22ContextLocalNamesIndexEv
	.section	.text._ZNK2v88internal9ScopeInfo22ContextLocalInfosIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo22ContextLocalInfosIndexEv
	.type	_ZNK2v88internal9ScopeInfo22ContextLocalInfosIndexEv, @function
_ZNK2v88internal9ScopeInfo22ContextLocalInfosIndexEv:
.LFB19342:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	$3, %eax
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L510
	movq	31(%rdx), %rax
	sarq	$32, %rax
	addl	$3, %eax
.L510:
	ret
	.cfi_endproc
.LFE19342:
	.size	_ZNK2v88internal9ScopeInfo22ContextLocalInfosIndexEv, .-_ZNK2v88internal9ScopeInfo22ContextLocalInfosIndexEv
	.section	.text._ZNK2v88internal9ScopeInfo17ReceiverInfoIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo17ReceiverInfoIndexEv
	.type	_ZNK2v88internal9ScopeInfo17ReceiverInfoIndexEv, @function
_ZNK2v88internal9ScopeInfo17ReceiverInfoIndexEv:
.LFB19343:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	$3, %r8d
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L513
	movq	31(%rax), %rdx
	movq	31(%rax), %rax
	sarq	$32, %rdx
	sarq	$32, %rax
	leal	3(%rdx,%rax), %r8d
.L513:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE19343:
	.size	_ZNK2v88internal9ScopeInfo17ReceiverInfoIndexEv, .-_ZNK2v88internal9ScopeInfo17ReceiverInfoIndexEv
	.section	.text._ZNK2v88internal9ScopeInfo21FunctionNameInfoIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo21FunctionNameInfoIndexEv
	.type	_ZNK2v88internal9ScopeInfo21FunctionNameInfoIndexEv, @function
_ZNK2v88internal9ScopeInfo21FunctionNameInfoIndexEv:
.LFB19344:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	movl	$3, %eax
	movslq	11(%rcx), %rdx
	testq	%rdx, %rdx
	jle	.L517
	movq	31(%rcx), %rax
	sarq	$32, %rax
	addl	$3, %eax
	testq	%rdx, %rdx
	jle	.L517
	movq	31(%rcx), %rsi
	sarq	$32, %rsi
	addl	%esi, %eax
.L517:
	testq	%rdx, %rdx
	je	.L516
	testl	%edx, %edx
	jg	.L526
.L516:
	ret
	.p2align 4,,10
	.p2align 3
.L526:
	movq	15(%rcx), %rdx
	shrq	$39, %rdx
	andl	$3, %edx
	subl	$1, %edx
	cmpl	$1, %edx
	setbe	%dl
	movzbl	%dl, %edx
	addl	%edx, %eax
	ret
	.cfi_endproc
.LFE19344:
	.size	_ZNK2v88internal9ScopeInfo21FunctionNameInfoIndexEv, .-_ZNK2v88internal9ScopeInfo21FunctionNameInfoIndexEv
	.section	.text._ZNK2v88internal9ScopeInfo24FunctionContextSlotIndexENS0_6StringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo24FunctionContextSlotIndexENS0_6StringE
	.type	_ZNK2v88internal9ScopeInfo24FunctionContextSlotIndexENS0_6StringE, @function
_ZNK2v88internal9ScopeInfo24FunctionContextSlotIndexENS0_6StringE:
.LFB19339:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L532
	movq	15(%rax), %rax
	shrq	$43, %rax
	andl	$3, %eax
	cmpl	$2, %eax
	jne	.L532
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal9ScopeInfo21FunctionNameInfoIndexEv
	movq	(%rdi), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	cmpq	%rax, %r8
	jne	.L529
	call	_ZNK2v88internal9ScopeInfo21FunctionNameInfoIndexEv
	movq	(%rdi), %rdx
	leal	24(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	shrq	$32, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L529:
	.cfi_restore_state
	movl	$-1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L532:
	.cfi_restore 6
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE19339:
	.size	_ZNK2v88internal9ScopeInfo24FunctionContextSlotIndexENS0_6StringE, .-_ZNK2v88internal9ScopeInfo24FunctionContextSlotIndexENS0_6StringE
	.section	.text._ZNK2v88internal9ScopeInfo20InferredFunctionNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo20InferredFunctionNameEv
	.type	_ZNK2v88internal9ScopeInfo20InferredFunctionNameEv, @function
_ZNK2v88internal9ScopeInfo20InferredFunctionNameEv:
.LFB19322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal9ScopeInfo21FunctionNameInfoIndexEv
	movq	(%rdi), %rcx
	movslq	11(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L541
	testl	%edx, %edx
	jg	.L537
.L541:
	leal	2(%rax), %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	-1(%rcx,%rdx), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L537:
	.cfi_restore_state
	movq	15(%rcx), %rsi
	leal	2(%rax), %edx
	addl	$4, %eax
	shrq	$43, %rsi
	andl	$3, %esi
	cmovne	%eax, %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	-1(%rcx,%rdx), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19322:
	.size	_ZNK2v88internal9ScopeInfo20InferredFunctionNameEv, .-_ZNK2v88internal9ScopeInfo20InferredFunctionNameEv
	.section	.text._ZNK2v88internal9ScopeInfo25InferredFunctionNameIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo25InferredFunctionNameIndexEv
	.type	_ZNK2v88internal9ScopeInfo25InferredFunctionNameIndexEv, @function
_ZNK2v88internal9ScopeInfo25InferredFunctionNameIndexEv:
.LFB19345:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	movl	$3, %eax
	movslq	11(%rcx), %rdx
	testq	%rdx, %rdx
	jle	.L548
	movq	31(%rcx), %rax
	sarq	$32, %rax
	addl	$3, %eax
	testq	%rdx, %rdx
	jle	.L548
	movq	31(%rcx), %rsi
	sarq	$32, %rsi
	addl	%esi, %eax
.L548:
	testq	%rdx, %rdx
	je	.L547
	testl	%edx, %edx
	jg	.L565
.L547:
	ret
	.p2align 4,,10
	.p2align 3
.L565:
	movq	15(%rcx), %rdx
	shrq	$39, %rdx
	andl	$3, %edx
	subl	$1, %edx
	cmpl	$1, %edx
	setbe	%dl
	movzbl	%dl, %edx
	addl	%edx, %eax
	movslq	11(%rcx), %rdx
	testl	%edx, %edx
	jle	.L547
	testq	%rdx, %rdx
	je	.L547
	movq	15(%rcx), %rdx
	leal	2(%rax), %ecx
	shrq	$43, %rdx
	andl	$3, %edx
	cmovne	%ecx, %eax
	ret
	.cfi_endproc
.LFE19345:
	.size	_ZNK2v88internal9ScopeInfo25InferredFunctionNameIndexEv, .-_ZNK2v88internal9ScopeInfo25InferredFunctionNameIndexEv
	.section	.text._ZNK2v88internal9ScopeInfo17FunctionDebugNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo17FunctionDebugNameEv
	.type	_ZNK2v88internal9ScopeInfo17FunctionDebugNameEv, @function
_ZNK2v88internal9ScopeInfo17FunctionDebugNameEv:
.LFB19323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal9ScopeInfo21FunctionNameInfoIndexEv
	movq	(%rdi), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	testb	$1, %al
	jne	.L567
.L571:
	movq	(%rdi), %rax
	movslq	11(%rax), %rdx
	testl	%edx, %edx
	jle	.L570
	testq	%rdx, %rdx
	jne	.L582
.L570:
	andq	$-262144, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rax
	movq	-37464(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L582:
	.cfi_restore_state
	movq	15(%rax), %rdx
	btq	$45, %rdx
	jnc	.L570
	call	_ZNK2v88internal9ScopeInfo25InferredFunctionNameIndexEv
	movq	(%rdi), %rcx
	leal	16(,%rax,8), %edx
	movslq	%edx, %rdx
	movq	-1(%rdx,%rcx), %r8
	testb	$1, %r8b
	jne	.L574
.L580:
	movq	%rcx, %rax
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L567:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L571
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L571
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L574:
	.cfi_restore_state
	movq	-1(%r8), %rdx
	movq	%r8, %rax
	cmpw	$63, 11(%rdx)
	ja	.L580
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19323:
	.size	_ZNK2v88internal9ScopeInfo17FunctionDebugNameEv, .-_ZNK2v88internal9ScopeInfo17FunctionDebugNameEv
	.section	.text._ZNK2v88internal9ScopeInfo17PositionInfoIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo17PositionInfoIndexEv
	.type	_ZNK2v88internal9ScopeInfo17PositionInfoIndexEv, @function
_ZNK2v88internal9ScopeInfo17PositionInfoIndexEv:
.LFB19346:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	$3, %eax
	movslq	11(%rdx), %rcx
	testq	%rcx, %rcx
	jle	.L584
	movq	31(%rdx), %rax
	sarq	$32, %rax
	addl	$3, %eax
	testq	%rcx, %rcx
	jle	.L584
	movq	31(%rdx), %rsi
	sarq	$32, %rsi
	addl	%esi, %eax
.L584:
	testq	%rcx, %rcx
	je	.L583
	testl	%ecx, %ecx
	jg	.L609
.L583:
	ret
	.p2align 4,,10
	.p2align 3
.L609:
	movq	15(%rdx), %rcx
	shrq	$39, %rcx
	andl	$3, %ecx
	subl	$1, %ecx
	cmpl	$1, %ecx
	setbe	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %eax
	movslq	11(%rdx), %rcx
	testl	%ecx, %ecx
	jle	.L583
	testq	%rcx, %rcx
	je	.L583
	movq	15(%rdx), %rsi
	leal	2(%rax), %edi
	shrq	$43, %rsi
	andl	$3, %esi
	cmovne	%edi, %eax
	testl	%ecx, %ecx
	jle	.L583
	testq	%rcx, %rcx
	je	.L583
	movq	15(%rdx), %rdx
	shrq	$45, %rdx
	andl	$1, %edx
	addl	%edx, %eax
	ret
	.cfi_endproc
.LFE19346:
	.size	_ZNK2v88internal9ScopeInfo17PositionInfoIndexEv, .-_ZNK2v88internal9ScopeInfo17PositionInfoIndexEv
	.section	.text._ZNK2v88internal9ScopeInfo13StartPositionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo13StartPositionEv
	.type	_ZNK2v88internal9ScopeInfo13StartPositionEv, @function
_ZNK2v88internal9ScopeInfo13StartPositionEv:
.LFB19324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal9ScopeInfo17PositionInfoIndexEv
	movq	(%r8), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	sarq	$32, %rax
	ret
	.cfi_endproc
.LFE19324:
	.size	_ZNK2v88internal9ScopeInfo13StartPositionEv, .-_ZNK2v88internal9ScopeInfo13StartPositionEv
	.section	.text._ZNK2v88internal9ScopeInfo11EndPositionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo11EndPositionEv
	.type	_ZNK2v88internal9ScopeInfo11EndPositionEv, @function
_ZNK2v88internal9ScopeInfo11EndPositionEv:
.LFB19325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal9ScopeInfo17PositionInfoIndexEv
	movq	(%r8), %rdx
	leal	24(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	sarq	$32, %rax
	ret
	.cfi_endproc
.LFE19325:
	.size	_ZNK2v88internal9ScopeInfo11EndPositionEv, .-_ZNK2v88internal9ScopeInfo11EndPositionEv
	.section	.text._ZN2v88internal9ScopeInfo15SetPositionInfoEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ScopeInfo15SetPositionInfoEii
	.type	_ZN2v88internal9ScopeInfo15SetPositionInfoEii, @function
_ZN2v88internal9ScopeInfo15SetPositionInfoEii:
.LFB19326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movl	%edx, %r9d
	movl	%esi, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal9ScopeInfo17PositionInfoIndexEv
	movq	(%r8), %rdx
	leal	16(,%rax,8), %eax
	salq	$32, %r10
	movq	%r8, %rdi
	cltq
	movq	%r10, -1(%rax,%rdx)
	call	_ZNK2v88internal9ScopeInfo17PositionInfoIndexEv
	movq	(%r8), %rdx
	leal	24(,%rax,8), %eax
	salq	$32, %r9
	cltq
	movq	%r9, -1(%rax,%rdx)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19326:
	.size	_ZN2v88internal9ScopeInfo15SetPositionInfoEii, .-_ZN2v88internal9ScopeInfo15SetPositionInfoEii
	.section	.text._ZNK2v88internal9ScopeInfo14OuterScopeInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo14OuterScopeInfoEv
	.type	_ZNK2v88internal9ScopeInfo14OuterScopeInfoEv, @function
_ZNK2v88internal9ScopeInfo14OuterScopeInfoEv:
.LFB19327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal9ScopeInfo17PositionInfoIndexEv
	movq	(%r8), %rcx
	leal	2(%rax), %edx
	movslq	11(%rcx), %rsi
	testq	%rsi, %rsi
	jne	.L617
.L618:
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	-1(%rcx,%rdx), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L617:
	.cfi_restore_state
	jle	.L618
	movq	15(%rcx), %rsi
	sarq	$32, %rsi
	movl	%esi, %edi
	andl	$15, %edi
	subl	$2, %edi
	andl	$253, %edi
	jne	.L619
.L621:
	leal	4(%rax), %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	-1(%rcx,%rdx), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L619:
	.cfi_restore_state
	andl	$13, %esi
	cmpb	$1, %sil
	je	.L621
	jmp	.L618
	.cfi_endproc
.LFE19327:
	.size	_ZNK2v88internal9ScopeInfo14OuterScopeInfoEv, .-_ZNK2v88internal9ScopeInfo14OuterScopeInfoEv
	.section	.text._ZNK2v88internal9ScopeInfo20ModuleDescriptorInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo20ModuleDescriptorInfoEv
	.type	_ZNK2v88internal9ScopeInfo20ModuleDescriptorInfoEv, @function
_ZNK2v88internal9ScopeInfo20ModuleDescriptorInfoEv:
.LFB19328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal9ScopeInfo17PositionInfoIndexEv
	movq	(%r8), %rdx
	movslq	11(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.L637
.L623:
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L637:
	.cfi_restore_state
	jle	.L623
	movq	15(%rdx), %rsi
	sarq	$32, %rsi
	movl	%esi, %edi
	andl	$15, %edi
	subl	$2, %edi
	andl	$253, %edi
	movl	%ecx, %edi
	jne	.L624
.L636:
	addl	$2, %eax
.L625:
	testl	%edi, %edi
	jle	.L623
	testq	%rcx, %rcx
	je	.L623
	movq	15(%rdx), %rcx
	movq	(%r8), %rdx
	shrq	$53, %rcx
	andl	$1, %ecx
	addl	%ecx, %eax
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L624:
	.cfi_restore_state
	andl	$13, %esi
	cmpb	$1, %sil
	je	.L636
	jmp	.L625
	.cfi_endproc
.LFE19328:
	.size	_ZNK2v88internal9ScopeInfo20ModuleDescriptorInfoEv, .-_ZNK2v88internal9ScopeInfo20ModuleDescriptorInfoEv
	.section	.text._ZNK2v88internal9ScopeInfo19OuterScopeInfoIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo19OuterScopeInfoIndexEv
	.type	_ZNK2v88internal9ScopeInfo19OuterScopeInfoIndexEv, @function
_ZNK2v88internal9ScopeInfo19OuterScopeInfoIndexEv:
.LFB19347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal9ScopeInfo17PositionInfoIndexEv
	movq	(%r8), %rdx
	movslq	11(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.L647
.L638:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L647:
	.cfi_restore_state
	jle	.L638
	movq	15(%rdx), %rdx
	sarq	$32, %rdx
	movl	%edx, %ecx
	andl	$15, %ecx
	subl	$2, %ecx
	andl	$253, %ecx
	jne	.L640
.L645:
	addl	$2, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L640:
	.cfi_restore_state
	andl	$13, %edx
	cmpb	$1, %dl
	je	.L645
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19347:
	.size	_ZNK2v88internal9ScopeInfo19OuterScopeInfoIndexEv, .-_ZNK2v88internal9ScopeInfo19OuterScopeInfoIndexEv
	.section	.text._ZNK2v88internal9ScopeInfo15ModuleInfoIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo15ModuleInfoIndexEv
	.type	_ZNK2v88internal9ScopeInfo15ModuleInfoIndexEv, @function
_ZNK2v88internal9ScopeInfo15ModuleInfoIndexEv:
.LFB19348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal9ScopeInfo17PositionInfoIndexEv
	movq	(%r8), %rcx
	movslq	11(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L663
.L648:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L663:
	.cfi_restore_state
	jle	.L648
	movq	15(%rcx), %rsi
	sarq	$32, %rsi
	movl	%esi, %edi
	andl	$15, %edi
	subl	$2, %edi
	andl	$253, %edi
	movl	%edx, %edi
	jne	.L650
.L662:
	addl	$2, %eax
.L651:
	testl	%edi, %edi
	jle	.L648
	testq	%rdx, %rdx
	je	.L648
	movq	15(%rcx), %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	shrq	$53, %rdx
	andl	$1, %edx
	addl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L650:
	.cfi_restore_state
	andl	$13, %esi
	cmpb	$1, %sil
	je	.L662
	jmp	.L651
	.cfi_endproc
.LFE19348:
	.size	_ZNK2v88internal9ScopeInfo15ModuleInfoIndexEv, .-_ZNK2v88internal9ScopeInfo15ModuleInfoIndexEv
	.section	.text._ZNK2v88internal9ScopeInfo24ModuleVariableCountIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo24ModuleVariableCountIndexEv
	.type	_ZNK2v88internal9ScopeInfo24ModuleVariableCountIndexEv, @function
_ZNK2v88internal9ScopeInfo24ModuleVariableCountIndexEv:
.LFB19349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal9ScopeInfo17PositionInfoIndexEv
	movq	(%r8), %rcx
	movslq	11(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L679
.L665:
	addl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L679:
	.cfi_restore_state
	jle	.L665
	movq	15(%rcx), %rsi
	sarq	$32, %rsi
	movl	%esi, %edi
	andl	$15, %edi
	subl	$2, %edi
	andl	$253, %edi
	movl	%edx, %edi
	jne	.L666
.L678:
	addl	$2, %eax
.L667:
	testl	%edi, %edi
	jle	.L665
	testq	%rdx, %rdx
	je	.L665
	movq	15(%rcx), %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	shrq	$53, %rdx
	andl	$1, %edx
	addl	%edx, %eax
	addl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L666:
	.cfi_restore_state
	andl	$13, %esi
	cmpb	$1, %sil
	je	.L678
	jmp	.L667
	.cfi_endproc
.LFE19349:
	.size	_ZNK2v88internal9ScopeInfo24ModuleVariableCountIndexEv, .-_ZNK2v88internal9ScopeInfo24ModuleVariableCountIndexEv
	.section	.text._ZNK2v88internal9ScopeInfo20ModuleVariablesIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ScopeInfo20ModuleVariablesIndexEv
	.type	_ZNK2v88internal9ScopeInfo20ModuleVariablesIndexEv, @function
_ZNK2v88internal9ScopeInfo20ModuleVariablesIndexEv:
.LFB19350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal9ScopeInfo17PositionInfoIndexEv
	movq	(%r8), %rcx
	movslq	11(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L695
.L681:
	addl	$2, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L695:
	.cfi_restore_state
	jle	.L681
	movq	15(%rcx), %rsi
	sarq	$32, %rsi
	movl	%esi, %edi
	andl	$15, %edi
	subl	$2, %edi
	andl	$253, %edi
	movl	%edx, %edi
	jne	.L682
.L694:
	addl	$2, %eax
.L683:
	testl	%edi, %edi
	jle	.L681
	testq	%rdx, %rdx
	je	.L681
	movq	15(%rcx), %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	shrq	$53, %rdx
	andl	$1, %edx
	addl	%edx, %eax
	addl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L682:
	.cfi_restore_state
	andl	$13, %esi
	cmpb	$1, %sil
	je	.L694
	jmp	.L683
	.cfi_endproc
.LFE19350:
	.size	_ZNK2v88internal9ScopeInfo20ModuleVariablesIndexEv, .-_ZNK2v88internal9ScopeInfo20ModuleVariablesIndexEv
	.section	.text._ZN2v88internal9ScopeInfo14ModuleVariableEiPNS0_6StringEPiPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ScopeInfo14ModuleVariableEiPNS0_6StringEPiPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE
	.type	_ZN2v88internal9ScopeInfo14ModuleVariableEiPNS0_6StringEPiPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE, @function
_ZN2v88internal9ScopeInfo14ModuleVariableEiPNS0_6StringEPiPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE:
.LFB19351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	movq	16(%rbp), %r10
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	call	_ZNK2v88internal9ScopeInfo15ModuleInfoIndexEv
	leal	(%rbx,%rbx,2), %edx
	leal	6(%rdx,%rax), %esi
	movq	(%r11), %rdx
	sall	$3, %esi
	movslq	%esi, %rax
	movq	-1(%rax,%rdx), %rax
	sarq	$32, %rax
	testq	%r14, %r14
	je	.L697
	leal	-16(%rsi), %ecx
	movslq	%ecx, %rcx
	movq	-1(%rcx,%rdx), %rdx
	movq	%rdx, (%r14)
.L697:
	testq	%r13, %r13
	je	.L698
	movq	(%r11), %rdx
	subl	$8, %esi
	movslq	%esi, %rsi
	movq	-1(%rsi,%rdx), %rdx
	sarq	$32, %rdx
	movl	%edx, 0(%r13)
.L698:
	testq	%r12, %r12
	je	.L699
	movl	%eax, %edx
	andl	$15, %edx
	movb	%dl, (%r12)
.L699:
	testq	%r9, %r9
	je	.L700
	movl	%eax, %edx
	shrl	$4, %edx
	andl	$1, %edx
	movb	%dl, (%r9)
.L700:
	testq	%r10, %r10
	je	.L696
	shrl	$5, %eax
	andl	$1, %eax
	movb	%al, (%r10)
.L696:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19351:
	.size	_ZN2v88internal9ScopeInfo14ModuleVariableEiPNS0_6StringEPiPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE, .-_ZN2v88internal9ScopeInfo14ModuleVariableEiPNS0_6StringEPiPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE
	.section	.text._ZN2v88internal9ScopeInfo11ModuleIndexENS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ScopeInfo11ModuleIndexENS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE
	.type	_ZN2v88internal9ScopeInfo11ModuleIndexENS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE, @function
_ZN2v88internal9ScopeInfo11ModuleIndexENS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE:
.LFB19336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%r8, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal9ScopeInfo15ModuleInfoIndexEv
	movq	(%r15), %rdx
	movq	%r15, %rdi
	leal	24(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %r9
	sarq	$32, %r9
	call	_ZNK2v88internal9ScopeInfo15ModuleInfoIndexEv
	testq	%r9, %r9
	jle	.L719
	leal	32(,%rax,8), %edx
	movl	%r9d, %ebx
	leaq	-72(%rbp), %r12
	xorl	%r13d, %r13d
	movslq	%edx, %r14
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L732:
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L722
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L723
.L722:
	movq	%r12, %rdi
	call	_ZN2v88internal6String10SlowEqualsES1_@PLT
	testb	%al, %al
	jne	.L721
.L723:
	addl	$1, %r13d
	addq	$24, %r14
	cmpl	%ebx, %r13d
	je	.L719
.L725:
	movq	(%r15), %rax
	movq	-1(%r14,%rax), %rsi
	movq	-72(%rbp), %rax
	cmpq	%rax, %rsi
	jne	.L732
.L721:
	subq	$8, %rsp
	pushq	-96(%rbp)
	movq	-88(%rbp), %r9
	leaq	-60(%rbp), %rcx
	movq	-80(%rbp), %r8
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9ScopeInfo14ModuleVariableEiPNS0_6StringEPiPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE
	popq	%rdx
	movl	-60(%rbp), %eax
	popq	%rcx
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L719:
	xorl	%eax, %eax
.L718:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L733
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L733:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19336:
	.size	_ZN2v88internal9ScopeInfo11ModuleIndexENS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE, .-_ZN2v88internal9ScopeInfo11ModuleIndexENS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE
	.section	.rodata._ZN2v88internallsERSoNS0_9ScopeInfo22VariableAllocationInfoE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"NONE"
.LC2:
	.string	"STACK"
.LC3:
	.string	"CONTEXT"
.LC4:
	.string	"UNUSED"
	.section	.text._ZN2v88internallsERSoNS0_9ScopeInfo22VariableAllocationInfoE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoNS0_9ScopeInfo22VariableAllocationInfoE
	.type	_ZN2v88internallsERSoNS0_9ScopeInfo22VariableAllocationInfoE, @function
_ZN2v88internallsERSoNS0_9ScopeInfo22VariableAllocationInfoE:
.LFB19352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$2, %esi
	je	.L735
	ja	.L736
	testl	%esi, %esi
	je	.L745
	movl	$5, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L736:
	.cfi_restore_state
	cmpl	$3, %esi
	jne	.L746
	movl	$6, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L735:
	.cfi_restore_state
	movl	$7, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L745:
	.cfi_restore_state
	movl	$4, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L746:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19352:
	.size	_ZN2v88internallsERSoNS0_9ScopeInfo22VariableAllocationInfoE, .-_ZN2v88internallsERSoNS0_9ScopeInfo22VariableAllocationInfoE
	.section	.text._ZN2v88internal25SourceTextModuleInfoEntry3NewEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEES6_S6_iiii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25SourceTextModuleInfoEntry3NewEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEES6_S6_iiii
	.type	_ZN2v88internal25SourceTextModuleInfoEntry3NewEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEES6_S6_iiii, @function
_ZN2v88internal25SourceTextModuleInfoEntry3NewEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEES6_S6_iiii:
.LFB19353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	$1, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$56, %rsp
	movl	16(%rbp), %eax
	movq	%rsi, -72(%rbp)
	movl	$98, %esi
	movl	%eax, -56(%rbp)
	movl	24(%rbp), %eax
	movl	%eax, -64(%rbp)
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE@PLT
	movq	-72(%rbp), %r10
	movq	(%rax), %rdi
	movq	%rax, %r12
	movq	(%r10), %rdx
	movq	%rdx, 7(%rdi)
	testb	$1, %dl
	je	.L759
	movq	%rdx, %rcx
	leaq	7(%rdi), %rsi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L779
	testb	$24, %al
	je	.L759
.L785:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L780
	.p2align 4,,10
	.p2align 3
.L759:
	movq	(%r12), %rdi
	movq	(%r14), %r14
	movq	%r14, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %r14b
	je	.L758
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L781
	testb	$24, %al
	je	.L758
.L787:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L782
	.p2align 4,,10
	.p2align 3
.L758:
	movq	(%r12), %rdi
	movq	(%r15), %r14
	movq	%r14, 23(%rdi)
	leaq	23(%rdi), %rsi
	testb	$1, %r14b
	je	.L757
	movq	%r14, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L783
	testb	$24, %al
	je	.L757
.L786:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L784
.L757:
	movq	(%r12), %rax
	salq	$32, %r13
	salq	$32, %rbx
	movq	%r13, 31(%rax)
	movq	(%r12), %rax
	movq	%rbx, 39(%rax)
	movq	-56(%rbp), %rax
	movq	(%r12), %rdx
	salq	$32, %rax
	movq	%rax, 47(%rdx)
	movq	-64(%rbp), %rax
	movq	(%r12), %rdx
	salq	$32, %rax
	movq	%rax, 55(%rdx)
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L779:
	.cfi_restore_state
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L785
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L783:
	movq	%r14, %rdx
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
	testb	$24, %al
	jne	.L786
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L781:
	movq	%r14, %rdx
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L787
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L780:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L782:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L784:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L757
	.cfi_endproc
.LFE19353:
	.size	_ZN2v88internal25SourceTextModuleInfoEntry3NewEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEES6_S6_iiii, .-_ZN2v88internal25SourceTextModuleInfoEntry3NewEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEES6_S6_iiii
	.section	.text._ZN2v88internal20SourceTextModuleInfo3NewEPNS0_7IsolateEPNS0_4ZoneEPNS0_26SourceTextModuleDescriptorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20SourceTextModuleInfo3NewEPNS0_7IsolateEPNS0_4ZoneEPNS0_26SourceTextModuleDescriptorE
	.type	_ZN2v88internal20SourceTextModuleInfo3NewEPNS0_7IsolateEPNS0_4ZoneEPNS0_26SourceTextModuleDescriptorE, @function
_ZN2v88internal20SourceTextModuleInfo3NewEPNS0_7IsolateEPNS0_4ZoneEPNS0_26SourceTextModuleDescriptorE:
.LFB19354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	48(%rdx), %r13
	movq	%rsi, -112(%rbp)
	xorl	%edx, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movl	%r13d, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	leaq	16(%rbx), %r13
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	32(%rbx), %r15
	movq	%rax, %r14
	cmpq	%r15, %r13
	je	.L795
	movq	%rbx, -88(%rbp)
	movq	%r12, -80(%rbp)
	movq	%r15, %r12
	movq	%r13, %r15
	movq	-104(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L789:
	movq	32(%r12), %rax
	movq	0(%r13), %rbx
	movq	(%rax), %rax
	movq	(%rax), %rdx
	movl	40(%r12), %eax
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%rbx,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L830
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L793
	movq	%rbx, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L793:
	testb	$24, %al
	je	.L830
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L830
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L830:
	movl	40(%r12), %eax
	movslq	44(%r12), %rdx
	movq	%r12, %rdi
	movq	(%r14), %rcx
	leal	16(,%rax,8), %eax
	salq	$32, %rdx
	cltq
	movq	%rdx, -1(%rax,%rcx)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r15
	jne	.L789
	movq	-80(%rbp), %r12
	movq	-88(%rbp), %rbx
.L795:
	movq	72(%rbx), %rsi
	subq	64(%rbx), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	sarq	$3, %rsi
	movl	$16, %r15d
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	64(%rbx), %r13
	movq	%rax, -96(%rbp)
	movq	72(%rbx), %rax
	movq	%rax, %rdx
	subq	%r13, %rdx
	leaq	16(%rdx), %rcx
	cmpq	%rax, %r13
	je	.L791
	movq	%r14, -88(%rbp)
	movq	-96(%rbp), %r14
	movq	%rbx, -120(%rbp)
	movq	%r15, %rbx
	movq	%rcx, %r15
	.p2align 4,,10
	.p2align 3
.L801:
	movq	-16(%r13,%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal26SourceTextModuleDescriptor5Entry9SerializeEPNS0_7IsolateE@PLT
	movq	(%r14), %rdi
	movq	(%rax), %rdx
	leaq	-1(%rdi,%rbx), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L831
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -56(%rbp)
	testl	$262144, %eax
	je	.L799
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r8
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%r8), %rax
.L799:
	testb	$24, %al
	je	.L831
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L831
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L831:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	jne	.L801
	movq	-88(%rbp), %r14
	movq	-120(%rbp), %rbx
.L791:
	movq	104(%rbx), %rsi
	subq	96(%rbx), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	sarq	$3, %rsi
	movl	$16, %r15d
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	96(%rbx), %r13
	movq	%rax, -88(%rbp)
	movq	104(%rbx), %rax
	movq	%rax, %rdx
	subq	%r13, %rdx
	leaq	16(%rdx), %rcx
	cmpq	%rax, %r13
	je	.L797
	movq	%r14, -120(%rbp)
	movq	-88(%rbp), %r14
	movq	%rbx, -128(%rbp)
	movq	%r15, %rbx
	movq	%rcx, %r15
	.p2align 4,,10
	.p2align 3
.L807:
	movq	-16(%r13,%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal26SourceTextModuleDescriptor5Entry9SerializeEPNS0_7IsolateE@PLT
	movq	(%r14), %rdi
	movq	(%rax), %rdx
	leaq	-1(%rdi,%rbx), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L832
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -56(%rbp)
	testl	$262144, %eax
	je	.L805
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r8
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%r8), %rax
.L805:
	testb	$24, %al
	je	.L832
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L832
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L832:
	addq	$8, %rbx
	cmpq	%r15, %rbx
	jne	.L807
	movq	-120(%rbp), %r14
	movq	-128(%rbp), %rbx
.L797:
	movq	-112(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	$16, %r13d
	addq	$192, %rbx
	call	_ZNK2v88internal26SourceTextModuleDescriptor23SerializeRegularExportsEPNS0_7IsolateEPNS0_4ZoneE@PLT
	movl	32(%rbx), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	16(%rbx), %r15
	movq	%rax, -112(%rbp)
	cmpq	%r15, %rbx
	je	.L803
	movq	%r14, -128(%rbp)
	movq	%r15, %r14
	movq	-112(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L802:
	movq	40(%r14), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal26SourceTextModuleDescriptor5Entry9SerializeEPNS0_7IsolateE@PLT
	movq	(%r15), %rdi
	movq	(%rax), %rdx
	leaq	-1(%rdi,%r13), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L833
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L811
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
.L811:
	testb	$24, %al
	je	.L833
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L833
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L833:
	movq	%r14, %rdi
	addq	$8, %r13
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%rax, %rbx
	jne	.L802
	movq	-128(%rbp), %r14
.L803:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory23NewSourceTextModuleInfoEv@PLT
	movq	(%rax), %rbx
	movq	%rax, %r12
	movq	-104(%rbp), %rax
	movq	(%rax), %r13
	leaq	15(%rbx), %rsi
	movq	%r13, 15(%rbx)
	testb	$1, %r13b
	je	.L839
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L913
	testb	$24, %al
	je	.L839
.L926:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L914
	.p2align 4,,10
	.p2align 3
.L839:
	movq	-96(%rbp), %rax
	movq	(%r12), %rbx
	movq	(%rax), %r13
	leaq	23(%rbx), %rsi
	movq	%r13, 23(%rbx)
	testb	$1, %r13b
	je	.L838
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L915
	testb	$24, %al
	je	.L838
.L925:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L916
	.p2align 4,,10
	.p2align 3
.L838:
	movq	-120(%rbp), %rax
	movq	(%r12), %rbx
	movq	(%rax), %r13
	leaq	31(%rbx), %rsi
	movq	%r13, 31(%rbx)
	testb	$1, %r13b
	je	.L837
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L917
	testb	$24, %al
	je	.L837
.L928:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L918
	.p2align 4,,10
	.p2align 3
.L837:
	movq	-88(%rbp), %rax
	movq	(%r12), %rbx
	movq	(%rax), %r13
	leaq	39(%rbx), %rsi
	movq	%r13, 39(%rbx)
	testb	$1, %r13b
	je	.L836
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L919
	testb	$24, %al
	je	.L836
.L927:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L920
	.p2align 4,,10
	.p2align 3
.L836:
	movq	-112(%rbp), %rax
	movq	(%r12), %rbx
	movq	(%rax), %r13
	leaq	47(%rbx), %rsi
	movq	%r13, 47(%rbx)
	testb	$1, %r13b
	je	.L835
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L921
	testb	$24, %al
	je	.L835
.L930:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L922
	.p2align 4,,10
	.p2align 3
.L835:
	movq	(%r12), %r15
	movq	(%r14), %r13
	movq	%r13, 55(%r15)
	leaq	55(%r15), %r14
	testb	$1, %r13b
	je	.L834
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L923
	testb	$24, %al
	je	.L834
.L929:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L924
.L834:
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L915:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L925
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L913:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L926
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L919:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L927
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L917:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L928
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L923:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L929
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L921:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L930
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L914:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L916:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L918:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L920:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L922:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L924:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L834
	.cfi_endproc
.LFE19354:
	.size	_ZN2v88internal20SourceTextModuleInfo3NewEPNS0_7IsolateEPNS0_4ZoneEPNS0_26SourceTextModuleDescriptorE, .-_ZN2v88internal20SourceTextModuleInfo3NewEPNS0_7IsolateEPNS0_4ZoneEPNS0_26SourceTextModuleDescriptorE
	.section	.text._ZN2v88internal9ScopeInfo6CreateEPNS0_7IsolateEPNS0_4ZoneEPNS0_5ScopeENS0_11MaybeHandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ScopeInfo6CreateEPNS0_7IsolateEPNS0_4ZoneEPNS0_5ScopeENS0_11MaybeHandleIS1_EE
	.type	_ZN2v88internal9ScopeInfo6CreateEPNS0_7IsolateEPNS0_4ZoneEPNS0_5ScopeENS0_11MaybeHandleIS1_EE, @function
_ZN2v88internal9ScopeInfo6CreateEPNS0_7IsolateEPNS0_4ZoneEPNS0_5ScopeENS0_11MaybeHandleIS1_EE:
.LFB19282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	56(%rdx), %rbx
	subq	$152, %rsp
	movq	%rsi, -176(%rbp)
	movq	64(%rdx), %rsi
	movq	%rdi, -128(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rbx
	je	.L1015
	movq	%rbx, %rax
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L1097:
	cmpb	$5, %dl
	sete	%dl
	addq	$24, %rax
	movzbl	%dl, %edx
	addl	%edx, %edi
	cmpq	%rsi, %rax
	je	.L1096
.L936:
	movq	(%rax), %rax
	movzwl	40(%rax), %edx
	sarl	$7, %edx
	andl	$7, %edx
	cmpb	$3, %dl
	jne	.L1097
	addq	$24, %rax
	addl	$1, %ecx
	cmpq	%rsi, %rax
	jne	.L936
.L1096:
	movl	%edi, -168(%rbp)
.L932:
	testb	$1, 130(%r12)
	jne	.L1098
	movl	$0, -120(%rbp)
	xorl	%r14d, %r14d
	movl	$0, -144(%rbp)
	movl	$-1, -156(%rbp)
.L937:
	movzbl	128(%r12), %eax
	cmpb	$2, %al
	movb	%al, -90(%rbp)
	sete	%r15b
	je	.L1099
	movl	%eax, %edi
	leal	-3(%rdi), %eax
	cmpb	$1, %al
	jbe	.L1032
	movl	$0, -136(%rbp)
	movl	%edi, %eax
	movl	$0, -96(%rbp)
	cmpb	$1, %dil
	je	.L1032
.L941:
	movl	$0, -112(%rbp)
	testb	%al, %al
	je	.L1100
.L944:
	leal	-2(%rax), %edx
	movb	$1, -89(%rbp)
	andl	$253, %edx
	jne	.L1101
.L946:
	movq	$0, -80(%rbp)
	movl	$0, -116(%rbp)
	testb	$1, 130(%r12)
	jne	.L1102
.L947:
	cmpq	$0, -88(%rbp)
	leal	(%rcx,%rcx), %edi
	movl	-96(%rbp), %r8d
	setne	%r13b
	movl	%edi, -160(%rbp)
	addl	$3, %edi
	leal	(%rdi,%r14), %esi
	testl	%r8d, %r8d
	movzbl	%r15b, %r14d
	movzbl	%r13b, %r13d
	leal	2(%rsi), %edx
	movl	%edi, -68(%rbp)
	cmovne	%edx, %esi
	addl	%r14d, %esi
	cmpb	$0, -89(%rbp)
	leal	2(%rsi), %edx
	cmovne	%edx, %esi
	addl	%r13d, %esi
	cmpb	$3, %al
	jne	.L950
	movl	-168(%rbp), %eax
	leal	2(%rax,%rax,2), %eax
	addl	%eax, %esi
.L950:
	movq	-128(%rbp), %rdi
	movl	$1, %edx
	movl	%ecx, -152(%rbp)
	call	_ZN2v88internal7Factory12NewScopeInfoEiNS0_14AllocationTypeE@PLT
	movl	$4, -72(%rbp)
	movl	-152(%rbp), %ecx
	movq	%rax, -104(%rbp)
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testl	$262144, %eax
	jne	.L951
	testb	$24, %al
	sete	%al
	movzbl	%al, %eax
	sall	$2, %eax
	movl	%eax, -72(%rbp)
.L951:
	xorl	%esi, %esi
	xorl	%r9d, %r9d
	cmpb	$2, 128(%r12)
	je	.L1103
.L952:
	movzbl	130(%r12), %edx
	xorl	%edi, %edi
	xorl	%r15d, %r15d
	andl	$1, %edx
	jne	.L1104
.L953:
	movzbl	128(%r12), %r11d
	sall	$21, %r13d
	sall	$13, %r14d
	movzbl	%dl, %edx
	movzbl	129(%r12), %eax
	sall	$6, %edx
	movl	%r11d, %r8d
	orl	%r11d, %r13d
	movl	%eax, %r11d
	movl	%eax, %r10d
	shrb	$5, %al
	andl	$1, %r11d
	andl	$1, %r10d
	sall	$5, %r11d
	orl	%r11d, %r13d
	movl	%eax, %r11d
	movl	-144(%rbp), %eax
	andl	$1, %r11d
	sall	$22, %r11d
	orl	%r11d, %r13d
	orl	%r14d, %r13d
	orl	%r13d, %eax
	orl	-120(%rbp), %eax
	orl	-136(%rbp), %eax
	orl	-112(%rbp), %eax
	orl	%eax, %r9d
	leal	-2(%r8), %eax
	orl	%esi, %r9d
	orl	%r9d, %r15d
	orl	%r15d, %edi
	orl	%edx, %edi
	testb	$-3, %al
	je	.L954
	movq	8(%r12), %rax
	movzbl	129(%rax), %eax
	andl	$1, %eax
	cmpb	%al, %r10b
	seta	%al
	movzbl	%al, %eax
	sall	$23, %eax
	orl	%eax, %edi
.L954:
	movq	-64(%rbp), %rax
	salq	$32, %rdi
	movq	%rcx, %rdx
	salq	$32, %rdx
	movq	%rdi, 15(%rax)
	movq	-80(%rbp), %rdi
	movq	-64(%rbp), %rax
	movq	%rdi, 23(%rax)
	leaq	-64(%rbp), %rdi
	movq	-64(%rbp), %rax
	movq	%rdx, 31(%rax)
	leal	3(%rcx), %eax
	movl	%eax, -80(%rbp)
	call	_ZNK2v88internal9ScopeInfo15ModuleInfoIndexEv
	movq	64(%r12), %rcx
	leal	2(%rax), %r15d
	cmpq	%rcx, %rbx
	je	.L975
	movq	%r12, %r8
	movl	-72(%rbp), %r14d
	movl	%r15d, %r12d
	movq	%rcx, %r15
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L1106:
	cmpb	$5, %al
	jne	.L960
	movq	8(%r13), %rax
	movq	-64(%rbp), %rsi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	leal	16(,%r12,8), %eax
	movslq	%eax, %r9
	movq	%rdx, -1(%r9,%rsi)
	testl	%r14d, %r14d
	jne	.L968
	movq	-64(%rbp), %rcx
	leaq	-1(%rcx), %rdi
.L969:
	movslq	32(%r13), %rsi
	leal	8(%rax), %edx
	addl	$3, %r12d
	movslq	%edx, %rdx
	salq	$32, %rsi
	movq	%rsi, (%rdx,%rdi)
	leal	16(%rax), %edx
	movslq	%edx, %rdx
	movzwl	40(%r13), %esi
	movl	%esi, %eax
	movl	%esi, %edi
	shrl	$8, %esi
	shrl	$9, %eax
	andl	$16, %esi
	andl	$15, %edi
	andl	$32, %eax
	orl	%esi, %eax
	movq	-64(%rbp), %rsi
	orl	%edi, %eax
	orl	$4194240, %eax
	salq	$32, %rax
	movq	%rax, -1(%rdx,%rsi)
	movq	(%rbx), %r13
.L960:
	leaq	24(%r13), %rbx
	cmpq	%rbx, %r15
	je	.L1105
.L955:
	movq	(%rbx), %r13
	movzwl	40(%r13), %edx
	movl	%edx, %eax
	movl	%edx, %esi
	sarl	$7, %eax
	andl	$7, %eax
	cmpb	$3, %al
	jne	.L1106
	movl	%edx, %r9d
	shrl	$8, %edx
	andl	$15, %esi
	movl	32(%r13), %edi
	shrl	$9, %r9d
	andl	$16, %edx
	andl	$32, %r9d
	leal	-4(%rdi), %eax
	orl	%r9d, %edx
	orl	%edx, %esi
	movq	8(%r13), %rdx
	leal	8(,%rdi,8), %r13d
	orl	$4194240, %esi
	movslq	%r13d, %r13
	movq	(%rdx), %rdx
	movl	%esi, %r9d
	movq	(%rdx), %r10
	movq	-64(%rbp), %rdx
	movq	%r10, -1(%r13,%rdx)
	testl	%r14d, %r14d
	jne	.L961
	movq	-64(%rbp), %rcx
	leaq	-1(%rcx), %rdx
.L962:
	movl	-80(%rbp), %ecx
	salq	$32, %r9
	leal	2(%rcx,%rax), %eax
	sall	$3, %eax
	cltq
	movq	%r9, (%rax,%rdx)
	movq	(%rbx), %r13
	leaq	24(%r13), %rbx
	cmpq	%rbx, %r15
	jne	.L955
.L1105:
	movq	%r8, %r12
.L975:
	testb	$1, 130(%r12)
	je	.L976
	movl	-116(%rbp), %edi
	testl	%edi, %edi
	jle	.L980
	movl	-116(%rbp), %eax
	movl	-80(%rbp), %r14d
	xorl	%r13d, %r13d
	subl	$1, %eax
	leaq	8(,%rax,8), %rbx
	.p2align 4,,10
	.p2align 3
.L981:
	movq	%r12, %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	144(%rax), %rax
	movq	(%rax,%r13), %rdx
	movzwl	40(%rdx), %eax
	sarl	$7, %eax
	andl	$7, %eax
	cmpb	$3, %al
	jne	.L979
	movl	32(%rdx), %eax
	movq	-64(%rbp), %rcx
	leal	0(,%r13,8), %esi
	leal	-2(%r14,%rax), %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	-1(%rdx,%rcx), %rax
	sarq	$32, %rax
	andl	$-4194241, %eax
	orl	%esi, %eax
	salq	$32, %rax
	movq	%rax, -1(%rdx,%rcx)
.L979:
	addq	$8, %r13
	cmpq	%rbx, %r13
	jne	.L981
.L980:
	movq	%r12, %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	testb	$16, 132(%rax)
	jne	.L1107
.L976:
	cmpl	$1, -156(%rbp)
	jbe	.L1108
.L989:
	movl	-96(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L1109
.L990:
	xorl	%eax, %eax
	cmpb	$2, -90(%rbp)
	sete	%al
	addl	%eax, -68(%rbp)
	cmpb	$0, -89(%rbp)
	jne	.L1110
.L1000:
	cmpq	$0, -88(%rbp)
	je	.L1013
	movq	-88(%rbp), %rax
	movq	(%rax), %r13
	movl	-68(%rbp), %eax
	leal	16(,%rax,8), %ebx
	leal	1(%rax), %r14d
	movq	-64(%rbp), %rax
	movslq	%ebx, %rbx
	movq	%r13, -1(%rbx,%rax)
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	jne	.L1011
.L1004:
	movl	%r14d, -68(%rbp)
.L1013:
	cmpb	$3, 128(%r12)
	je	.L1111
.L1006:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1112
	movq	-104(%rbp), %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L968:
	.cfi_restore_state
	movq	-64(%rbp), %r10
	movq	%rdx, %r11
	notq	%r11
	leaq	-1(%r10), %rdi
	andl	$1, %r11d
	leaq	(%r9,%rdi), %rsi
	cmpl	$4, %r14d
	je	.L1113
	testb	%r11b, %r11b
	jne	.L969
.L1063:
	movq	%rdx, %r9
	andq	$-262144, %r9
	testb	$24, 8(%r9)
	je	.L969
	movq	%r10, %r9
	andq	$-262144, %r9
	testb	$24, 8(%r9)
	jne	.L969
	movq	%r10, %rdi
	movq	%r8, -136(%rbp)
	movl	%eax, -112(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rax
	movq	-136(%rbp), %r8
	leaq	-1(%rax), %rdi
	movl	-112(%rbp), %eax
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L961:
	movq	-64(%rbp), %rdi
	movq	%r10, %r11
	notq	%r11
	leaq	-1(%rdi), %rdx
	andl	$1, %r11d
	leaq	0(%r13,%rdx), %rsi
	cmpl	$4, %r14d
	je	.L1114
	testb	%r11b, %r11b
	jne	.L962
.L1062:
	movq	%r10, %r11
	andq	$-262144, %r11
	testb	$24, 8(%r11)
	je	.L962
	movq	%rdi, %r11
	andq	$-262144, %r11
	testb	$24, 8(%r11)
	jne	.L962
	movq	%r10, %rdx
	movq	%r8, -144(%rbp)
	movl	%r9d, -136(%rbp)
	movl	%eax, -112(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rax
	movq	-144(%rbp), %r8
	movl	-136(%rbp), %r9d
	leaq	-1(%rax), %rdx
	movl	-112(%rbp), %eax
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L1101:
	movl	%eax, %edx
	andl	$-3, %edx
	cmpb	$1, %dl
	sete	-89(%rbp)
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L1011:
	movq	%r13, %rax
	movq	-64(%rbp), %rdi
	notq	%rax
	andl	$1, %eax
	cmpl	$4, -72(%rbp)
	leaq	-1(%rbx,%rdi), %rsi
	je	.L1115
	testb	%al, %al
	jne	.L1004
.L1066:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1004
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1004
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1110:
	addl	$2, -68(%rbp)
	movl	-68(%rbp), %eax
	movslq	112(%r12), %rdx
	movq	-64(%rbp), %rcx
	sall	$3, %eax
	movslq	%eax, %rsi
	salq	$32, %rdx
	addl	$8, %eax
	movq	%rdx, -1(%rsi,%rcx)
	cltq
	movslq	116(%r12), %rdx
	movq	-64(%rbp), %rcx
	salq	$32, %rdx
	movq	%rdx, -1(%rax,%rcx)
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1109:
	movabsq	$-4294967296, %r13
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	184(%rax), %rax
	testq	%rax, %rax
	je	.L991
	movq	8(%rax), %rdx
	movslq	32(%rax), %r13
	movq	(%rdx), %rdx
	salq	$32, %r13
	movq	(%rdx), %r14
.L991:
	addl	$2, -68(%rbp)
	movl	-68(%rbp), %eax
	leal	0(,%rax,8), %ebx
	movq	-64(%rbp), %rax
	movslq	%ebx, %r15
	movq	%r14, -1(%r15,%rax)
	movl	-72(%rbp), %edx
	testl	%edx, %edx
	je	.L1095
	movq	-64(%rbp), %rdi
	movq	%r14, %rdx
	notq	%rdx
	leaq	-1(%rdi), %rax
	andl	$1, %edx
	cmpl	$4, -72(%rbp)
	leaq	(%r15,%rax), %rsi
	je	.L1116
	testb	%dl, %dl
	jne	.L993
.L1065:
	movq	%r14, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L993
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L993
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1095:
	movq	-64(%rbp), %rax
	subq	$1, %rax
.L993:
	addl	$8, %ebx
	movslq	%ebx, %rbx
	movq	%r13, (%rbx,%rax)
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L1100:
	movq	%r12, %rdi
	movl	%ecx, -68(%rbp)
	call	_ZN2v88internal5Scope12AsClassScopeEv@PLT
	movl	-68(%rbp), %ecx
	movq	136(%rax), %rax
	testq	%rax, %rax
	je	.L945
	cmpq	$0, 40(%rax)
	setne	%al
	movzbl	%al, %eax
	sall	$9, %eax
	movl	%eax, -112(%rbp)
.L945:
	movzbl	128(%r12), %eax
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L1104:
	movq	%r12, %rdi
	movl	%esi, -184(%rbp)
	movl	%r9d, -180(%rbp)
	movl	%ecx, -152(%rbp)
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	%r12, %rdi
	movzbl	133(%rax), %r15d
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movl	-184(%rbp), %esi
	movzbl	130(%r12), %edx
	movzbl	129(%rax), %edi
	movl	-180(%rbp), %r9d
	sall	$16, %r15d
	movl	-152(%rbp), %ecx
	andl	$1, %edx
	shrb	$2, %dil
	andl	$1, %edi
	sall	$4, %edi
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L1102:
	movq	%r12, %rdi
	movl	%ecx, -68(%rbp)
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movl	136(%rax), %eax
	movl	%eax, %ecx
	movl	%eax, -116(%rbp)
	movzbl	128(%r12), %eax
	salq	$32, %rcx
	movq	%rcx, -80(%rbp)
	movl	-68(%rbp), %ecx
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L1098:
	movq	%r12, %rdi
	movl	%ecx, -68(%rbp)
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movl	-68(%rbp), %ecx
	testb	$16, 132(%rax)
	jne	.L1117
	movl	$0, -120(%rbp)
	xorl	%r14d, %r14d
	movl	$-1, -156(%rbp)
.L938:
	testb	$1, 130(%r12)
	je	.L1020
	movq	%r12, %rdi
	movl	%ecx, -68(%rbp)
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movl	-68(%rbp), %ecx
	cmpq	$0, 192(%rax)
	setne	%al
	movzbl	%al, %eax
	sall	$10, %eax
	movl	%eax, -144(%rbp)
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L1113:
	movq	%r9, -112(%rbp)
	testb	%r11b, %r11b
	jne	.L969
	movq	%rdx, %r11
	andq	$-262144, %r11
	testb	$4, 10(%r11)
	je	.L1063
	movq	%r10, %rdi
	movq	%r8, -152(%rbp)
	movl	%eax, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %r10
	movq	-112(%rbp), %r9
	movq	-136(%rbp), %rdx
	movl	-144(%rbp), %eax
	leaq	-1(%r10), %rdi
	movq	-152(%rbp), %r8
	leaq	(%r9,%rdi), %rsi
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1114:
	testb	%r11b, %r11b
	jne	.L962
	movq	%r10, %r11
	andq	$-262144, %r11
	testb	$4, 10(%r11)
	je	.L1062
	movq	%r10, %rdx
	movq	%r8, -152(%rbp)
	movl	%r9d, -144(%rbp)
	movl	%eax, -136(%rbp)
	movq	%r10, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdi
	movq	-112(%rbp), %r10
	movl	-136(%rbp), %eax
	movl	-144(%rbp), %r9d
	leaq	-1(%rdi), %rdx
	movq	-152(%rbp), %r8
	leaq	0(%r13,%rdx), %rsi
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1108:
	movq	%r12, %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	%rax, %r9
	movl	-160(%rbp), %eax
	movq	176(%r9), %rdx
	leal	4(%rax), %ecx
	leal	40(,%rax,8), %eax
	movslq	32(%rdx), %rdx
	movl	%ecx, -68(%rbp)
	cltq
	movq	-64(%rbp), %rcx
	salq	$32, %rdx
	movq	%rdx, -1(%rax,%rcx)
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L1103:
	movq	%r12, %rdi
	movl	%ecx, -152(%rbp)
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movl	-152(%rbp), %ecx
	movzbl	131(%rax), %esi
	movl	%esi, %r9d
	shrb	%sil
	andl	$1, %r9d
	andl	$1, %esi
	sall	$15, %r9d
	sall	$14, %esi
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	%r12, %rdi
	movl	%ecx, -68(%rbp)
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movl	-68(%rbp), %ecx
	cmpq	$0, 184(%rax)
	je	.L1118
	movq	%r12, %rdi
	movl	%ecx, -68(%rbp)
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movl	-68(%rbp), %ecx
	movq	184(%rax), %rax
	movzwl	40(%rax), %edx
	movzbl	128(%r12), %eax
	testb	$8, %dh
	je	.L1093
	shrl	$7, %edx
	movl	$4096, %edi
	movl	$2048, %esi
	andl	$7, %edx
	cmpb	$3, %dl
	cmove	%edi, %esi
	sete	%dil
	movzbl	%dil, %edi
	addl	$1, %edi
	movl	%esi, -136(%rbp)
	movl	%edi, -96(%rbp)
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L1107:
	movq	%r12, %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movq	176(%rax), %rsi
	movzwl	40(%rsi), %edx
	movl	%edx, %eax
	movl	%edx, %ecx
	sarl	$7, %eax
	andl	$7, %eax
	cmpb	$3, %al
	jne	.L976
	movl	%edx, %r13d
	shrl	$8, %edx
	andl	$15, %ecx
	movl	32(%rsi), %eax
	shrl	$9, %r13d
	andl	$16, %edx
	andl	$32, %r13d
	leal	8(,%rax,8), %r14d
	leal	-4(%rax), %ebx
	movq	-64(%rbp), %rax
	orl	%r13d, %edx
	movslq	%r14d, %r14
	orl	%edx, %ecx
	movq	8(%rsi), %rdx
	orl	$4194240, %ecx
	movq	(%rdx), %rdx
	movl	%ecx, %r13d
	movq	(%rdx), %r15
	movq	%r15, -1(%r14,%rax)
	movl	-72(%rbp), %esi
	testl	%esi, %esi
	je	.L1094
	movq	-64(%rbp), %rdi
	movq	%r15, %rax
	notq	%rax
	leaq	-1(%rdi), %rdx
	andl	$1, %eax
	cmpl	$4, -72(%rbp)
	leaq	(%r14,%rdx), %rsi
	je	.L1119
	testb	%al, %al
	jne	.L983
.L1064:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L983
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L983
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L1094:
	movq	-64(%rbp), %rax
	leaq	-1(%rax), %rdx
.L983:
	movl	-80(%rbp), %eax
	salq	$32, %r13
	leal	2(%rax,%rbx), %eax
	sall	$3, %eax
	cltq
	movq	%r13, (%rax,%rdx)
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L1118:
	movzbl	128(%r12), %eax
	movl	$3, -96(%rbp)
	movl	$6144, -136(%rbp)
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L1111:
	movq	%r12, %rdi
	call	_ZN2v88internal5Scope13AsModuleScopeEv@PLT
	movq	-176(%rbp), %rsi
	movq	-128(%rbp), %rdi
	movq	224(%rax), %rdx
	call	_ZN2v88internal20SourceTextModuleInfo3NewEPNS0_7IsolateEPNS0_4ZoneEPNS0_26SourceTextModuleDescriptorE
	movq	-104(%rbp), %rcx
	movq	(%rax), %r13
	movl	-68(%rbp), %eax
	movq	(%rcx), %r14
	leal	16(,%rax,8), %ebx
	movslq	%ebx, %rax
	leaq	-1(%r14,%rax), %r15
	movq	%r13, (%r15)
	testb	$1, %r13b
	je	.L1010
	movq	%r13, %r12
	andq	$-262144, %r12
	movq	8(%r12), %rax
	testl	$262144, %eax
	je	.L1008
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r12), %rax
.L1008:
	testb	$24, %al
	je	.L1010
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1120
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	-104(%rbp), %rax
	movq	-168(%rbp), %r12
	addl	$8, %ebx
	movslq	%ebx, %rbx
	movq	(%rax), %rax
	salq	$32, %r12
	movq	%r12, -1(%rbx,%rax)
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1032:
	movzbl	-90(%rbp), %eax
.L1093:
	movl	$6144, -136(%rbp)
	movl	$3, -96(%rbp)
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L1015:
	movl	$0, -168(%rbp)
	xorl	%ecx, %ecx
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L1117:
	movq	%r12, %rdi
	call	_ZN2v88internal5Scope18AsDeclarationScopeEv@PLT
	movl	-68(%rbp), %ecx
	movq	176(%rax), %rax
	movzwl	40(%rax), %eax
	testb	$8, %ah
	je	.L1018
	shrl	$7, %eax
	andl	$7, %eax
	cmpb	$3, %al
	je	.L1121
	movl	$128, -120(%rbp)
	movl	$1, %r14d
	movl	$0, -156(%rbp)
	jmp	.L938
.L1115:
	testb	%al, %al
	jne	.L1004
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L1066
	movq	%r13, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdi
	leaq	-1(%rbx,%rdi), %rsi
	jmp	.L1066
.L1018:
	movl	$384, -120(%rbp)
	xorl	%r14d, %r14d
	movl	$2, -156(%rbp)
	jmp	.L938
.L1120:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1010
.L1121:
	movl	$256, -120(%rbp)
	addl	$1, %ecx
	movl	$1, %r14d
	movl	$1, -156(%rbp)
	jmp	.L938
.L1116:
	testb	%dl, %dl
	jne	.L993
	movq	%r14, %rdx
	andq	$-262144, %rdx
	testb	$4, 10(%rdx)
	je	.L1065
	movq	%r14, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdi
	leaq	-1(%rdi), %rax
	leaq	(%r15,%rax), %rsi
	jmp	.L1065
.L1119:
	testb	%al, %al
	jne	.L983
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L1064
	movq	%r15, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdi
	leaq	-1(%rdi), %rdx
	leaq	(%r14,%rdx), %rsi
	jmp	.L1064
.L1112:
	call	__stack_chk_fail@PLT
.L1020:
	movl	$0, -144(%rbp)
	jmp	.L937
	.cfi_endproc
.LFE19282:
	.size	_ZN2v88internal9ScopeInfo6CreateEPNS0_7IsolateEPNS0_4ZoneEPNS0_5ScopeENS0_11MaybeHandleIS1_EE, .-_ZN2v88internal9ScopeInfo6CreateEPNS0_7IsolateEPNS0_4ZoneEPNS0_5ScopeENS0_11MaybeHandleIS1_EE
	.section	.text._ZNK2v88internal20SourceTextModuleInfo18RegularExportCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal20SourceTextModuleInfo18RegularExportCountEv
	.type	_ZNK2v88internal20SourceTextModuleInfo18RegularExportCountEv, @function
_ZNK2v88internal20SourceTextModuleInfo18RegularExportCountEv:
.LFB19355:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	31(%rax), %rax
	movslq	11(%rax), %rdx
	imulq	$1431655766, %rdx, %rax
	sarl	$31, %edx
	shrq	$32, %rax
	subl	%edx, %eax
	ret
	.cfi_endproc
.LFE19355:
	.size	_ZNK2v88internal20SourceTextModuleInfo18RegularExportCountEv, .-_ZNK2v88internal20SourceTextModuleInfo18RegularExportCountEv
	.section	.text._ZNK2v88internal20SourceTextModuleInfo22RegularExportLocalNameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal20SourceTextModuleInfo22RegularExportLocalNameEi
	.type	_ZNK2v88internal20SourceTextModuleInfo22RegularExportLocalNameEi, @function
_ZNK2v88internal20SourceTextModuleInfo22RegularExportLocalNameEi:
.LFB19356:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	31(%rax), %rdx
	leal	2(%rsi,%rsi,2), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	ret
	.cfi_endproc
.LFE19356:
	.size	_ZNK2v88internal20SourceTextModuleInfo22RegularExportLocalNameEi, .-_ZNK2v88internal20SourceTextModuleInfo22RegularExportLocalNameEi
	.section	.text._ZNK2v88internal20SourceTextModuleInfo22RegularExportCellIndexEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal20SourceTextModuleInfo22RegularExportCellIndexEi
	.type	_ZNK2v88internal20SourceTextModuleInfo22RegularExportCellIndexEi, @function
_ZNK2v88internal20SourceTextModuleInfo22RegularExportCellIndexEi:
.LFB19357:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	31(%rax), %rdx
	leal	3(%rsi,%rsi,2), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	sarq	$32, %rax
	ret
	.cfi_endproc
.LFE19357:
	.size	_ZNK2v88internal20SourceTextModuleInfo22RegularExportCellIndexEi, .-_ZNK2v88internal20SourceTextModuleInfo22RegularExportCellIndexEi
	.section	.text._ZNK2v88internal20SourceTextModuleInfo24RegularExportExportNamesEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal20SourceTextModuleInfo24RegularExportExportNamesEi
	.type	_ZNK2v88internal20SourceTextModuleInfo24RegularExportExportNamesEi, @function
_ZNK2v88internal20SourceTextModuleInfo24RegularExportExportNamesEi:
.LFB19358:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	31(%rax), %rdx
	leal	4(%rsi,%rsi,2), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	ret
	.cfi_endproc
.LFE19358:
	.size	_ZNK2v88internal20SourceTextModuleInfo24RegularExportExportNamesEi, .-_ZNK2v88internal20SourceTextModuleInfo24RegularExportExportNamesEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal9ScopeInfo6CreateEPNS0_7IsolateEPNS0_4ZoneEPNS0_5ScopeENS0_11MaybeHandleIS1_EE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal9ScopeInfo6CreateEPNS0_7IsolateEPNS0_4ZoneEPNS0_5ScopeENS0_11MaybeHandleIS1_EE, @function
_GLOBAL__sub_I__ZN2v88internal9ScopeInfo6CreateEPNS0_7IsolateEPNS0_4ZoneEPNS0_5ScopeENS0_11MaybeHandleIS1_EE:
.LFB23803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23803:
	.size	_GLOBAL__sub_I__ZN2v88internal9ScopeInfo6CreateEPNS0_7IsolateEPNS0_4ZoneEPNS0_5ScopeENS0_11MaybeHandleIS1_EE, .-_GLOBAL__sub_I__ZN2v88internal9ScopeInfo6CreateEPNS0_7IsolateEPNS0_4ZoneEPNS0_5ScopeENS0_11MaybeHandleIS1_EE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal9ScopeInfo6CreateEPNS0_7IsolateEPNS0_4ZoneEPNS0_5ScopeENS0_11MaybeHandleIS1_EE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
