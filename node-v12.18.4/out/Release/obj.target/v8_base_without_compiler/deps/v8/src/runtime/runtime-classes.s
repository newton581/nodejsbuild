	.file	"runtime-classes.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4479:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4479:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4480:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4480:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4482:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4482:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal12_GLOBAL__N_114GetSuperHolderEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS1_9SuperModeENS0_11MaybeHandleINS0_4NameEEEj.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_114GetSuperHolderEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS1_9SuperModeENS0_11MaybeHandleINS0_4NameEEEj.isra.0, @function
_ZN2v88internal12_GLOBAL__N_114GetSuperHolderEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS1_9SuperModeENS0_11MaybeHandleINS0_4NameEEEj.isra.0:
.LFB22615:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%rdx), %rax
	cmpw	$1026, 11(%rax)
	je	.L40
	movq	-1(%rdx), %rax
	movzbl	13(%rax), %eax
	shrb	$5, %al
	andl	$1, %eax
.L9:
	testb	%al, %al
	je	.L15
	movq	41112(%r12), %rdi
	movq	12464(%r12), %r8
	testq	%rdi, %rdi
	je	.L11
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	je	.L41
	.p2align 4,,10
	.p2align 3
.L15:
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L42
	movq	-1(%rax), %rax
	movq	41112(%r12), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L43
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rcx
.L19:
	testb	$1, %sil
	jne	.L44
.L20:
	addl	$85, %r14d
	movq	%rbx, %rdx
	testq	%rbx, %rbx
	je	.L45
.L23:
	xorl	%r8d, %r8d
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
.L16:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L46
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L47
.L18:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	testb	$1, %sil
	je	.L20
.L44:
	movq	-1(%rsi), %rdx
	movq	%rcx, %rax
	cmpw	$1023, 11(%rdx)
	ja	.L16
	addl	$85, %r14d
	movq	%rbx, %rdx
	testq	%rbx, %rbx
	jne	.L23
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%rcx, -72(%rbp)
	testl	%r15d, %r15d
	js	.L24
	movq	%r15, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %rdx
.L25:
	movq	(%rdx), %rax
	movl	11(%rax), %esi
	cmpl	$10, %esi
	jg	.L23
	cmpl	$3, 7(%rax)
	jne	.L23
	movl	%r15d, %edi
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	(%rdx), %rsi
	movl	%eax, 7(%rsi)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L11:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L48
.L13:
	leaq	8(%rsi), %rax
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	jne	.L15
.L41:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate23ReportFailedAccessCheckENS0_6HandleINS0_8JSObjectEEE@PLT
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L15
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	xorl	%eax, %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%rdx, %rcx
	leaq	-64(%rbp), %rdi
	movq	%rdx, -80(%rbp)
	andq	$-262144, %rcx
	movq	24(%rcx), %rax
	movq	%rcx, -72(%rbp)
	movq	-25128(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	24(%rcx), %rcx
	testb	$1, %dl
	jne	.L49
.L7:
	movq	-1(%rdx), %rdx
	movq	23(%rdx), %rdx
.L8:
	cmpq	%rax, %rdx
	setne	%al
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L42:
	movq	104(%r12), %rsi
	leaq	104(%r12), %rcx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L24:
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L49:
	movq	-1(%rdx), %rsi
	cmpw	$1024, 11(%rsi)
	jne	.L7
	movq	-37488(%rcx), %rdx
	jmp	.L8
.L46:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22615:
	.size	_ZN2v88internal12_GLOBAL__N_114GetSuperHolderEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS1_9SuperModeENS0_11MaybeHandleINS0_4NameEEEj.isra.0, .-_ZN2v88internal12_GLOBAL__N_114GetSuperHolderEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS1_9SuperModeENS0_11MaybeHandleINS0_4NameEEEj.isra.0
	.section	.text._ZN2v88internal12_GLOBAL__N_113LoadFromSuperEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS4_INS0_4NameEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_113LoadFromSuperEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS4_INS0_4NameEEE, @function
_ZN2v88internal12_GLOBAL__N_113LoadFromSuperEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS4_INS0_4NameEEE:
.LFB18509:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movq	%rdx, %rsi
	xorl	%edx, %edx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12_GLOBAL__N_114GetSuperHolderEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS1_9SuperModeENS0_11MaybeHandleINS0_4NameEEEj.isra.0
	testq	%rax, %rax
	je	.L60
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	(%r12), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$3, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L53
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L53:
	movl	%eax, -128(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -116(%rbp)
	movq	(%r12), %rax
	movq	%rdi, -104(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L61
.L54:
	leaq	-128(%rbp), %r12
	movq	%r14, -96(%rbp)
	movq	%r12, %rdi
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
.L52:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L62
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%r12, %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r14
	jmp	.L54
.L62:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18509:
	.size	_ZN2v88internal12_GLOBAL__N_113LoadFromSuperEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS4_INS0_4NameEEE, .-_ZN2v88internal12_GLOBAL__N_113LoadFromSuperEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS4_INS0_4NameEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_112StoreToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEENS4_INS0_4NameEEES8_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_112StoreToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEENS4_INS0_4NameEEES8_, @function
_ZN2v88internal12_GLOBAL__N_112StoreToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEENS4_INS0_4NameEEES8_:
.LFB18520:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	$1, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	xorl	%r8d, %r8d
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12_GLOBAL__N_114GetSuperHolderEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS1_9SuperModeENS0_11MaybeHandleINS0_4NameEEEj.isra.0
	testq	%rax, %rax
	je	.L73
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	(%r12), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$3, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L66
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L66:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	(%r12), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L74
.L67:
	leaq	-144(%rbp), %r12
	movq	%r15, -112(%rbp)
	movq	%r12, %rdi
	movq	$0, -104(%rbp)
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object16SetSuperPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	movl	$0, %eax
	cmovne	%rbx, %rax
.L65:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L75
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L74:
	movq	%r12, %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r15
	jmp	.L67
.L75:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18520:
	.size	_ZN2v88internal12_GLOBAL__N_112StoreToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEENS4_INS0_4NameEEES8_, .-_ZN2v88internal12_GLOBAL__N_112StoreToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEENS4_INS0_4NameEEES8_
	.section	.text._ZN2v88internal12_GLOBAL__N_119StoreElementToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEjS8_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119StoreElementToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEjS8_, @function
_ZN2v88internal12_GLOBAL__N_119StoreElementToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEjS8_:
.LFB18521:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	movl	$1, %edx
	pushq	%r12
	.cfi_offset 12, -40
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%r8, %rbx
	movl	%ecx, %r8d
	xorl	%ecx, %ecx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12_GLOBAL__N_114GetSuperHolderEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS1_9SuperModeENS0_11MaybeHandleINS0_4NameEEEj.isra.0
	testq	%rax, %rax
	je	.L78
	movabsq	$824633720832, %rdx
	movl	%r12d, -56(%rbp)
	leaq	-128(%rbp), %r12
	movq	%r12, %rdi
	movq	%rdx, -116(%rbp)
	movq	%rax, -64(%rbp)
	movl	$3, -128(%rbp)
	movq	%r14, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	$-1, -52(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object16SetSuperPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	movl	$0, %eax
	cmovne	%rbx, %rax
.L78:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L82
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L82:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18521:
	.size	_ZN2v88internal12_GLOBAL__N_119StoreElementToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEjS8_, .-_ZN2v88internal12_GLOBAL__N_119StoreElementToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEjS8_
	.section	.text._ZN2v88internalL17StoreKeyedToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6ObjectEEES7_S7_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL17StoreKeyedToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6ObjectEEES7_S7_, @function
_ZN2v88internalL17StoreKeyedToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6ObjectEEES7_S7_:
.LFB18525:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rcx, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rcx), %rax
	movl	$0, -68(%rbp)
	testb	$1, %al
	jne	.L84
	sarq	$32, %rax
	js	.L98
	movl	%eax, -68(%rbp)
	movl	%eax, %ecx
.L86:
	movq	%rbx, %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119StoreElementToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEjS8_
.L94:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L117
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movq	%rcx, %r15
	cmpw	$65, 11(%rdx)
	jne	.L116
	movsd	7(%rax), %xmm0
	movsd	.LC0(%rip), %xmm2
	addsd	%xmm0, %xmm2
	movq	%xmm2, %rcx
	movq	%xmm2, %rdx
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	jne	.L116
	movl	%edx, %edi
	pxor	%xmm1, %xmm1
	movd	%xmm2, %ecx
	movd	%xmm2, -68(%rbp)
	cvtsi2sdq	%rdi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L104
	jne	.L104
	cmpl	$-1, %edx
	jne	.L86
.L116:
	xorl	%edx, %edx
.L88:
	testb	%dl, %dl
	jne	.L98
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	jbe	.L97
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object13ConvertToNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r15
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L94
.L97:
	movq	(%r15), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L118
.L100:
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_112StoreToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEENS4_INS0_4NameEEES8_
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%rax, -64(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L101
	testb	$2, %al
	jne	.L100
.L101:
	leaq	-68(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L100
	movl	-68(%rbp), %ecx
	movq	%rbx, %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119StoreElementToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEjS8_
	jmp	.L94
.L104:
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L88
.L117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18525:
	.size	_ZN2v88internalL17StoreKeyedToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6ObjectEEES7_S7_, .-_ZN2v88internalL17StoreKeyedToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6ObjectEEES7_S7_
	.section	.text._ZN2v88internal12_GLOBAL__N_129ShallowCopyDictionaryTemplateINS0_16NumberDictionaryEEENS0_6HandleIT_EEPNS0_7IsolateES6_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_129ShallowCopyDictionaryTemplateINS0_16NumberDictionaryEEENS0_6HandleIT_EEPNS0_7IsolateES6_, @function
_ZN2v88internal12_GLOBAL__N_129ShallowCopyDictionaryTemplateINS0_16NumberDictionaryEEENS0_6HandleIT_EEPNS0_7IsolateES6_:
.LFB20434:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	-1(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L120
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L121:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory21CopyFixedArrayWithMapENS0_6HandleINS0_10FixedArrayEEENS2_INS0_3MapEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movslq	35(%rax), %rdx
	testq	%rdx, %rdx
	jle	.L123
	subl	$1, %edx
	movl	$56, %ebx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	80(,%rdx,8), %r14
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L125:
	addq	$24, %rbx
	cmpq	%rbx, %r14
	je	.L123
.L142:
	movq	(%r12), %rax
.L133:
	movq	-1(%rbx,%rax), %rsi
	testb	$1, %sil
	je	.L125
	movq	-1(%rsi), %rax
	cmpw	$79, 11(%rax)
	jne	.L125
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L127
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L128:
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12AccessorPair4CopyEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%r12), %rdi
	movq	(%rax), %r15
	leaq	-1(%rdi,%rbx), %rsi
	movq	%r15, (%rsi)
	testb	$1, %r15b
	je	.L125
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L131
	movq	%r15, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
.L131:
	testb	$24, %al
	je	.L125
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L125
	movq	%r15, %rdx
	addq	$24, %rbx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	cmpq	%rbx, %r14
	jne	.L142
.L123:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movq	41088(%r13), %r8
	cmpq	41096(%r13), %r8
	je	.L143
.L129:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r8)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L120:
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L144
.L122:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdx)
	jmp	.L121
.L144:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L122
.L143:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L129
	.cfi_endproc
.LFE20434:
	.size	_ZN2v88internal12_GLOBAL__N_129ShallowCopyDictionaryTemplateINS0_16NumberDictionaryEEENS0_6HandleIT_EEPNS0_7IsolateES6_, .-_ZN2v88internal12_GLOBAL__N_129ShallowCopyDictionaryTemplateINS0_16NumberDictionaryEEENS0_6HandleIT_EEPNS0_7IsolateES6_
	.section	.text._ZN2v88internal12_GLOBAL__N_124ThrowNotSuperConstructorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_10JSFunctionEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_124ThrowNotSuperConstructorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_10JSFunctionEEE, @function
_ZN2v88internal12_GLOBAL__N_124ThrowNotSuperConstructorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_10JSFunctionEEE:
.LFB18482:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L176
.L147:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object21NoSideEffectsToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r15
	movq	%rax, %r13
.L159:
	movl	11(%r15), %edx
	testl	%edx, %edx
	jne	.L161
	leaq	2960(%r12), %r13
.L161:
	movq	(%r14), %rax
	movq	23(%rax), %rbx
	movq	15(%rbx), %rax
	testb	$1, %al
	jne	.L177
.L162:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L163:
	testb	%al, %al
	je	.L167
	movq	15(%rbx), %r14
	testb	$1, %r14b
	jne	.L178
.L165:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L168
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r14
	movq	%rax, %rcx
.L169:
	movl	11(%r14), %eax
	xorl	%r8d, %r8d
	testl	%eax, %eax
	je	.L179
	movq	%r13, %rdx
	movl	$101, %esi
.L175:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L180
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$102, %esi
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L178:
	movq	-1(%r14), %rax
	cmpw	$136, 11(%rax)
	jne	.L165
	leaq	-64(%rbp), %r15
	movq	%r14, -64(%rbp)
	movq	%r15, %rdi
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	testb	%al, %al
	je	.L167
	movq	%r15, %rdi
	movq	%r14, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	%rax, %r14
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L168:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L181
.L170:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rcx)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L167:
	andq	$-262144, %rbx
	movq	24(%rbx), %rax
	movq	-37464(%rax), %r14
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L177:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L162
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L176:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L148
	movq	-1(%rax), %rax
	cmpw	$67, 11(%rax)
	jne	.L147
	movq	2960(%rdi), %r15
	leaq	2960(%rdi), %r13
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L148:
	movq	23(%rax), %rbx
	movq	15(%rbx), %rax
	testb	$1, %al
	jne	.L182
.L150:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L151:
	testb	%al, %al
	je	.L155
	movq	15(%rbx), %r15
	testb	$1, %r15b
	jne	.L183
.L153:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L156
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r15
	movq	%rax, %r13
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L181:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L156:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L184
.L158:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, 0(%r13)
	jmp	.L159
.L184:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r13
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L155:
	andq	$-262144, %rbx
	movq	24(%rbx), %rax
	movq	-37464(%rax), %r15
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L182:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L150
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L183:
	movq	-1(%r15), %rax
	cmpw	$136, 11(%rax)
	jne	.L153
	leaq	-64(%rbp), %r13
	movq	%r15, -64(%rbp)
	movq	%r13, %rdi
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	testb	%al, %al
	je	.L155
	movq	%r13, %rdi
	movq	%r15, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	%rax, %r15
	jmp	.L153
.L180:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18482:
	.size	_ZN2v88internal12_GLOBAL__N_124ThrowNotSuperConstructorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_10JSFunctionEEE, .-_ZN2v88internal12_GLOBAL__N_124ThrowNotSuperConstructorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_10JSFunctionEEE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_113SetHomeObjectEPNS0_7IsolateENS0_10JSFunctionENS0_8JSObjectE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"method.map().instance_descriptors().GetKey(kPropertyIndex) == ReadOnlyRoots(isolate).home_object_symbol()"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_113SetHomeObjectEPNS0_7IsolateENS0_10JSFunctionENS0_8JSObjectE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Check failed: %s."
.LC3:
	.string	"unreachable code"
	.section	.text._ZN2v88internal12_GLOBAL__N_113SetHomeObjectEPNS0_7IsolateENS0_10JSFunctionENS0_8JSObjectE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_113SetHomeObjectEPNS0_7IsolateENS0_10JSFunctionENS0_8JSObjectE, @function
_ZN2v88internal12_GLOBAL__N_113SetHomeObjectEPNS0_7IsolateENS0_10JSFunctionENS0_8JSObjectE:
.LFB18491:
	.cfi_startproc
	movq	23(%rsi), %rax
	movl	47(%rax), %eax
	testb	$32, %ah
	jne	.L228
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	-1(%rsi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	-1(%rsi), %rax
	movq	39(%rax), %rax
	movq	71(%rax), %rax
	cmpq	%rax, 3720(%rdi)
	jne	.L229
	movq	-1(%rsi), %rsi
	movq	%rdx, %r13
	movq	39(%rsi), %rax
	movq	79(%rax), %rdx
	movzbl	7(%rsi), %edi
	movzbl	8(%rsi), %ecx
	movq	%rdx, %rax
	shrq	$38, %rdx
	shrq	$51, %rax
	subl	%ecx, %edi
	andl	$7, %edx
	movq	%rax, %r9
	andl	$1023, %r9d
	cmpl	%edi, %r9d
	setl	%cl
	jl	.L230
	subl	%edi, %r9d
	movl	$16, %r8d
	leal	16(,%r9,8), %r9d
.L189:
	cmpl	$2, %edx
	jne	.L231
	movl	$32768, %esi
.L190:
	movzbl	%cl, %eax
	movslq	%edi, %rdi
	movslq	%r9d, %r9
	movslq	%r8d, %r8
	salq	$14, %rax
	salq	$17, %rdi
	movq	%r13, %rdx
	orq	%rdi, %rax
	salq	$27, %r8
	notq	%rdx
	orq	%r9, %rax
	andl	$1, %edx
	orq	%r8, %rax
	orq	%rax, %rsi
	testb	$64, %ah
	je	.L193
	andl	$16376, %esi
	addq	%rsi, %r14
	movq	%r13, (%r14)
	testb	%dl, %dl
	jne	.L185
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L195
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L195:
	testb	$24, %al
	je	.L185
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L232
.L185:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movzbl	8(%rsi), %r8d
	movzbl	8(%rsi), %eax
	addl	%eax, %r9d
	sall	$3, %r8d
	sall	$3, %r9d
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L231:
	cmpb	$2, %dl
	jg	.L191
	je	.L192
.L207:
	xorl	%esi, %esi
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L191:
	subl	$3, %edx
	cmpb	$1, %dl
	jbe	.L207
.L192:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L193:
	movq	7(%r12), %r14
	andq	$-262144, %r12
	movq	24(%r12), %rax
	subq	$37592, %rax
	testb	$1, %r14b
	je	.L200
	cmpq	288(%rax), %r14
	je	.L200
.L199:
	movq	%rsi, %rax
	sarl	$3, %esi
	shrq	$30, %rax
	andl	$2047, %esi
	andl	$15, %eax
	subl	%eax, %esi
	leal	16(,%rsi,8), %eax
	cltq
	leaq	-1(%r14,%rax), %r12
	movq	%r13, (%r12)
	testb	%dl, %dl
	jne	.L185
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L202
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L202:
	testb	$24, %al
	je	.L185
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L185
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
.L227:
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movq	968(%rax), %r14
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L232:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L229:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18491:
	.size	_ZN2v88internal12_GLOBAL__N_113SetHomeObjectEPNS0_7IsolateENS0_10JSFunctionENS0_8JSObjectE, .-_ZN2v88internal12_GLOBAL__N_113SetHomeObjectEPNS0_7IsolateENS0_10JSFunctionENS0_8JSObjectE
	.section	.text._ZN2v88internal12_GLOBAL__N_132GetMethodAndSetHomeObjectAndNameINS0_14NameDictionaryEEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateERNS0_9ArgumentsENS0_3SmiENS0_6HandleINS0_8JSObjectEEENSC_INS0_6StringEEENSC_IS5_EE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_132GetMethodAndSetHomeObjectAndNameINS0_14NameDictionaryEEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateERNS0_9ArgumentsENS0_3SmiENS0_6HandleINS0_8JSObjectEEENSC_INS0_6StringEEENSC_IS5_EE, @function
_ZN2v88internal12_GLOBAL__N_132GetMethodAndSetHomeObjectAndNameINS0_14NameDictionaryEEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateERNS0_9ArgumentsENS0_3SmiENS0_6HandleINS0_8JSObjectEEENSC_INS0_6StringEEENSC_IS5_EE:
.LFB21536:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	sarq	$32, %rdx
	leal	0(,%rdx,8), %esi
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%r10), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	subq	%rsi, %rbx
	cmpq	$2, %rdx
	jg	.L234
.L238:
	movq	%rbx, %rax
.L235:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L242
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movq	(%rcx), %rdx
	movq	(%rbx), %rsi
	movq	%r8, %r12
	movq	%r9, %r13
	call	_ZN2v88internal12_GLOBAL__N_113SetHomeObjectEPNS0_7IsolateENS0_10JSFunctionENS0_8JSObjectE
	movq	(%rbx), %rax
	movq	23(%rax), %rax
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L243
.L236:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L237:
	testb	%al, %al
	jne	.L238
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal10JSFunction7SetNameENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6StringEEE@PLT
	testb	%al, %al
	jne	.L238
	xorl	%eax, %eax
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L243:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L236
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	jmp	.L237
.L242:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21536:
	.size	_ZN2v88internal12_GLOBAL__N_132GetMethodAndSetHomeObjectAndNameINS0_14NameDictionaryEEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateERNS0_9ArgumentsENS0_3SmiENS0_6HandleINS0_8JSObjectEEENSC_INS0_6StringEEENSC_IS5_EE, .-_ZN2v88internal12_GLOBAL__N_132GetMethodAndSetHomeObjectAndNameINS0_14NameDictionaryEEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateERNS0_9ArgumentsENS0_3SmiENS0_6HandleINS0_8JSObjectEEENSC_INS0_6StringEEENSC_IS5_EE
	.section	.text._ZN2v88internal12_GLOBAL__N_132GetMethodAndSetHomeObjectAndNameINS0_16NumberDictionaryEEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateERNS0_9ArgumentsENS0_3SmiENS0_6HandleINS0_8JSObjectEEENSC_INS0_6StringEEENSC_IS5_EE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_132GetMethodAndSetHomeObjectAndNameINS0_16NumberDictionaryEEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateERNS0_9ArgumentsENS0_3SmiENS0_6HandleINS0_8JSObjectEEENSC_INS0_6StringEEENSC_IS5_EE, @function
_ZN2v88internal12_GLOBAL__N_132GetMethodAndSetHomeObjectAndNameINS0_16NumberDictionaryEEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateERNS0_9ArgumentsENS0_3SmiENS0_6HandleINS0_8JSObjectEEENSC_INS0_6StringEEENSC_IS5_EE:
.LFB21525:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	sarq	$32, %rdx
	leal	0(,%rdx,8), %esi
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%r10), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	subq	%rsi, %rbx
	cmpq	$2, %rdx
	jg	.L245
.L249:
	movq	%rbx, %rax
.L246:
	movq	-40(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L253
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	movq	(%rcx), %rdx
	movq	(%rbx), %rsi
	movq	%rdi, %r12
	movq	%r8, %r13
	movq	%r9, %r14
	call	_ZN2v88internal12_GLOBAL__N_113SetHomeObjectEPNS0_7IsolateENS0_10JSFunctionENS0_8JSObjectE
	movq	(%rbx), %rax
	movq	23(%rax), %rax
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L254
.L247:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L248:
	testb	%al, %al
	jne	.L249
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal10JSFunction7SetNameENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6StringEEE@PLT
	testb	%al, %al
	jne	.L249
	xorl	%eax, %eax
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L254:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L247
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	jmp	.L248
.L253:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21525:
	.size	_ZN2v88internal12_GLOBAL__N_132GetMethodAndSetHomeObjectAndNameINS0_16NumberDictionaryEEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateERNS0_9ArgumentsENS0_3SmiENS0_6HandleINS0_8JSObjectEEENSC_INS0_6StringEEENSC_IS5_EE, .-_ZN2v88internal12_GLOBAL__N_132GetMethodAndSetHomeObjectAndNameINS0_16NumberDictionaryEEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateERNS0_9ArgumentsENS0_3SmiENS0_6HandleINS0_8JSObjectEEENSC_INS0_6StringEEENSC_IS5_EE
	.section	.text._ZN2v88internal12_GLOBAL__N_116SubstituteValuesINS0_16NumberDictionaryEEEbPNS0_7IsolateENS0_6HandleIT_EENS6_INS0_8JSObjectEEERNS0_9ArgumentsEPb.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_116SubstituteValuesINS0_16NumberDictionaryEEEbPNS0_7IsolateENS0_6HandleIT_EENS6_INS0_8JSObjectEEERNS0_9ArgumentsEPb.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_116SubstituteValuesINS0_16NumberDictionaryEEEbPNS0_7IsolateENS0_6HandleIT_EENS6_INS0_8JSObjectEEERNS0_9ArgumentsEPb.constprop.0:
.LFB22627:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	%rdx, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movslq	35(%rax), %rdx
	testq	%rdx, %rdx
	jle	.L256
	subl	$1, %edx
	movq	%rdi, %r12
	movq	%rsi, %r13
	movl	$48, %ebx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	56(%rdi), %r14
	leaq	72(,%rdx,8), %rcx
	movq	%rcx, -56(%rbp)
	leaq	128(%rdi), %rcx
	movq	%rcx, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L286:
	movq	-1(%rbx,%rax), %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal9HashTableINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE5IsKeyENS0_13ReadOnlyRootsENS0_6ObjectE@PLT
	testb	%al, %al
	je	.L268
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L259
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r9
.L260:
	movq	0(%r13), %rax
	leal	8(%rbx), %r11d
	movslq	%r11d, %r15
	movq	-1(%r15,%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L262
	movq	%r9, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-64(%rbp), %r9
	movq	(%rax), %rsi
	movq	%rax, %r10
	testb	$1, %sil
	jne	.L314
.L267:
	movq	%rsi, %rdx
	movq	-88(%rbp), %r8
	movq	-72(%rbp), %rcx
	movq	%r12, %rdi
	movq	-80(%rbp), %rsi
	call	_ZN2v88internal12_GLOBAL__N_132GetMethodAndSetHomeObjectAndNameINS0_16NumberDictionaryEEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateERNS0_9ArgumentsENS0_3SmiENS0_6HandleINS0_8JSObjectEEENSC_INS0_6StringEEENSC_IS5_EE
	testq	%rax, %rax
	je	.L272
	movq	0(%r13), %rdi
	movq	(%rax), %rdx
	leaq	-1(%r15,%rdi), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L268
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -64(%rbp)
	testl	$262144, %eax
	je	.L283
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
.L283:
	testb	$24, %al
	je	.L268
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L315
	.p2align 4,,10
	.p2align 3
.L268:
	addq	$24, %rbx
	cmpq	%rbx, -56(%rbp)
	je	.L256
.L311:
	movq	0(%r13), %rax
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L259:
	movq	41088(%r12), %r9
	cmpq	41096(%r12), %r9
	je	.L316
.L261:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%r9)
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L262:
	movq	41088(%r12), %r10
	cmpq	41096(%r12), %r10
	je	.L317
.L264:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r10)
	testb	$1, %sil
	je	.L267
.L314:
	movq	-1(%rsi), %rax
	cmpw	$79, 11(%rax)
	je	.L266
	movq	(%r10), %rsi
	testb	$1, %sil
	je	.L267
	addq	$24, %rbx
	cmpq	%rbx, -56(%rbp)
	jne	.L311
.L256:
	addq	$88, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	movq	(%r10), %rax
	movq	7(%rax), %rdx
	testb	$1, %dl
	je	.L318
.L270:
	movq	15(%rax), %rdx
	testb	$1, %dl
	jne	.L268
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r10, -64(%rbp)
	leaq	3272(%r12), %r8
	call	_ZN2v88internal12_GLOBAL__N_132GetMethodAndSetHomeObjectAndNameINS0_16NumberDictionaryEEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateERNS0_9ArgumentsENS0_3SmiENS0_6HandleINS0_8JSObjectEEENSC_INS0_6StringEEENSC_IS5_EE
	testq	%rax, %rax
	je	.L272
	movq	-64(%rbp), %r10
	movq	(%rax), %rdx
	movq	(%r10), %r15
	movq	%rdx, 15(%r15)
	leaq	15(%r15), %rsi
	testb	$1, %dl
	je	.L268
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -64(%rbp)
	testl	$262144, %eax
	je	.L279
	movq	%r15, %rdi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rax
.L279:
	testb	$24, %al
	je	.L268
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L268
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L316:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r9
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L317:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	-64(%rbp), %r9
	movq	%rax, %r10
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L272:
	addq	$88, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L318:
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r10, -96(%rbp)
	leaq	2608(%r12), %r8
	movq	%r9, -64(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_132GetMethodAndSetHomeObjectAndNameINS0_16NumberDictionaryEEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateERNS0_9ArgumentsENS0_3SmiENS0_6HandleINS0_8JSObjectEEENSC_INS0_6StringEEENSC_IS5_EE
	testq	%rax, %rax
	je	.L272
	movq	-96(%rbp), %r10
	movq	(%rax), %rdx
	movq	(%r10), %r15
	testb	$1, %dl
	movq	%rdx, 7(%r15)
	leaq	7(%r15), %rsi
	movq	-64(%rbp), %r9
	je	.L288
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -64(%rbp)
	testl	$262144, %eax
	je	.L274
	movq	%r15, %rdi
	movq	%r10, -120(%rbp)
	movq	%r9, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rcx
	movq	-120(%rbp), %r10
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %rdx
	movq	8(%rcx), %rax
	movq	-96(%rbp), %rsi
.L274:
	testb	$24, %al
	je	.L288
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L319
	.p2align 4,,10
	.p2align 3
.L288:
	movq	(%r10), %rax
	jmp	.L270
.L319:
	movq	%r15, %rdi
	movq	%r10, -96(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %r10
	movq	-64(%rbp), %r9
	jmp	.L288
	.cfi_endproc
.LFE22627:
	.size	_ZN2v88internal12_GLOBAL__N_116SubstituteValuesINS0_16NumberDictionaryEEEbPNS0_7IsolateENS0_6HandleIT_EENS6_INS0_8JSObjectEEERNS0_9ArgumentsEPb.constprop.0, .-_ZN2v88internal12_GLOBAL__N_116SubstituteValuesINS0_16NumberDictionaryEEEbPNS0_7IsolateENS0_6HandleIT_EENS6_INS0_8JSObjectEEERNS0_9ArgumentsEPb.constprop.0
	.section	.rodata._ZN2v88internal12_GLOBAL__N_124AddDescriptorsByTemplateEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_14NameDictionaryEEENS4_INS0_16NumberDictionaryEEENS4_INS0_10FixedArrayEEENS4_INS0_8JSObjectEEEbRNS0_9ArgumentsE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"*dict == *properties_dictionary"
	.section	.text._ZN2v88internal12_GLOBAL__N_124AddDescriptorsByTemplateEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_14NameDictionaryEEENS4_INS0_16NumberDictionaryEEENS4_INS0_10FixedArrayEEENS4_INS0_8JSObjectEEEbRNS0_9ArgumentsE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_124AddDescriptorsByTemplateEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_14NameDictionaryEEENS4_INS0_16NumberDictionaryEEENS4_INS0_10FixedArrayEEENS4_INS0_8JSObjectEEEbRNS0_9ArgumentsE, @function
_ZN2v88internal12_GLOBAL__N_124AddDescriptorsByTemplateEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_14NameDictionaryEEENS4_INS0_16NumberDictionaryEEENS4_INS0_10FixedArrayEEENS4_INS0_8JSObjectEEEbRNS0_9ArgumentsE:
.LFB18497:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	16(%rbp), %eax
	movq	%rcx, -104(%rbp)
	movq	%rsi, -160(%rbp)
	movq	24(%rbp), %r12
	movq	%r9, -120(%rbp)
	movb	%al, -81(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	movslq	11(%rax), %rax
	movq	%rax, -96(%rbp)
	movq	(%rdx), %rax
	movq	-1(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L321
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L322:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory21CopyFixedArrayWithMapENS0_6HandleINS0_10FixedArrayEEENS2_INS0_3MapEEE@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	movslq	35(%rax), %rdx
	testq	%rdx, %rdx
	jle	.L324
	subl	$1, %edx
	movq	%r12, -112(%rbp)
	movl	$64, %r15d
	movq	%r14, %r12
	leaq	(%rdx,%rdx,2), %rdx
	leaq	88(,%rdx,8), %r8
	movq	%r8, %r14
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L326:
	addq	$24, %r15
	cmpq	%r15, %r14
	je	.L460
.L465:
	movq	(%rbx), %rax
.L334:
	movq	-1(%rax,%r15), %rsi
	testb	$1, %sil
	je	.L326
	movq	-1(%rsi), %rax
	cmpw	$79, 11(%rax)
	jne	.L326
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L328
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L329:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12AccessorPair4CopyEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rbx), %rdi
	movq	(%rax), %rdx
	leaq	-1(%rdi,%r15), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L326
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -128(%rbp)
	testl	$262144, %eax
	je	.L332
	movq	%rdx, -152(%rbp)
	movq	%rsi, -144(%rbp)
	movq	%rdi, -136(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %r8
	movq	-152(%rbp), %rdx
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	8(%r8), %rax
.L332:
	testb	$24, %al
	je	.L326
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L326
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	addq	$24, %r15
	cmpq	%r15, %r14
	jne	.L465
	.p2align 4,,10
	.p2align 3
.L460:
	movq	%r12, %r14
	movq	-112(%rbp), %r12
.L324:
	movq	-104(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_129ShallowCopyDictionaryTemplateINS0_16NumberDictionaryEEENS0_6HandleIT_EEPNS0_7IsolateES6_
	cmpq	$0, -96(%rbp)
	movq	%rax, -112(%rbp)
	jle	.L344
	movq	-96(%rbp), %rax
	movl	$16, %r15d
	subl	$1, %eax
	leaq	24(,%rax,8), %rax
	movq	%rax, -96(%rbp)
	leaq	-68(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	%r13, %rax
	movq	%r15, %r13
	movq	%rax, %r15
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L339:
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L466
.L343:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	addq	$8, %r13
	call	_ZN2v88internal16ClassBoilerplate23AddToPropertiesTemplateEPNS0_7IsolateENS0_6HandleINS0_14NameDictionaryEEENS4_INS0_4NameEEEiNS1_9ValueKindENS0_6ObjectE@PLT
	cmpq	%r13, -96(%rbp)
	je	.L344
.L345:
	movq	(%r15), %rax
	movq	-1(%r13,%rax), %rcx
	movq	8(%r12), %rdx
	sarq	$32, %rcx
	movl	%ecx, %r8d
	shrl	$2, %ecx
	andl	$536870911, %ecx
	andl	$3, %r8d
	leal	0(,%rcx,8), %eax
	leal	1(%rcx), %r9d
	cltq
	salq	$32, %r9
	subq	%rax, %rdx
	movq	(%rdx), %rax
	movq	-1(%rax), %rsi
	cmpw	$63, 11(%rsi)
	ja	.L339
	movq	%rax, -64(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L340
	testb	$2, %al
	jne	.L463
.L340:
	movq	-152(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movq	%rdx, -128(%rbp)
	movq	%r9, -144(%rbp)
	movl	%r8d, -136(%rbp)
	movl	%ecx, -104(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movl	-104(%rbp), %ecx
	movq	-128(%rbp), %rdx
	testb	%al, %al
	movl	-136(%rbp), %r8d
	movq	-144(%rbp), %r9
	je	.L463
	movl	-68(%rbp), %edx
	movq	-112(%rbp), %rsi
	movq	%r14, %rdi
	addq	$8, %r13
	call	_ZN2v88internal16ClassBoilerplate21AddToElementsTemplateEPNS0_7IsolateENS0_6HandleINS0_16NumberDictionaryEEEjiNS1_9ValueKindENS0_6ObjectE@PLT
	cmpq	%r13, -96(%rbp)
	jne	.L345
.L344:
	movq	(%rbx), %rax
	movslq	35(%rax), %rdx
	testq	%rdx, %rdx
	jle	.L336
	subl	$1, %edx
	movq	%r12, -128(%rbp)
	movl	$56, %r13d
	movq	%rbx, %r12
	leaq	(%rdx,%rdx,2), %rdx
	leaq	80(,%rdx,8), %rdi
	movq	%rdi, -96(%rbp)
	leaq	128(%r14), %rdi
	movq	%rdi, -136(%rbp)
	.p2align 4,,10
	.p2align 3
.L337:
	movq	-1(%r13,%rax), %rsi
	cmpq	96(%r14), %rsi
	je	.L348
	cmpq	88(%r14), %rsi
	je	.L348
	cmpb	$0, -81(%rbp)
	je	.L349
	cmpq	%rsi, 2872(%r14)
	setne	-81(%rbp)
.L349:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L350
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r9
.L351:
	movq	(%r12), %rax
	leal	8(%r13), %ebx
	movq	-1(%rbx,%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L353
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-104(%rbp), %r9
	movq	(%rax), %rsi
	movq	%rax, %r15
	testb	$1, %sil
	jne	.L467
.L358:
	movq	%rsi, %rdx
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rsi
	movq	%r14, %rdi
	movq	-136(%rbp), %r8
	call	_ZN2v88internal12_GLOBAL__N_132GetMethodAndSetHomeObjectAndNameINS0_14NameDictionaryEEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateERNS0_9ArgumentsENS0_3SmiENS0_6HandleINS0_8JSObjectEEENSC_INS0_6StringEEENSC_IS5_EE
	testq	%rax, %rax
	je	.L363
	movq	(%r12), %rdi
	movq	(%rax), %r15
	leaq	-1(%rbx,%rdi), %rsi
	movq	%r15, (%rsi)
	testb	$1, %r15b
	je	.L348
	movq	%r15, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rdx
	movq	%rax, -104(%rbp)
	testl	$262144, %edx
	je	.L374
	movq	%r15, %rdx
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rax
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	8(%rax), %rdx
.L374:
	andl	$24, %edx
	je	.L348
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L468
	.p2align 4,,10
	.p2align 3
.L348:
	addq	$24, %r13
	cmpq	-96(%rbp), %r13
	je	.L461
.L470:
	movq	(%r12), %rax
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L463:
	movq	(%rdx), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	jne	.L343
.L466:
	movq	%rdx, %rsi
	movq	%r14, %rdi
	movq	%r9, -136(%rbp)
	movl	%r8d, -128(%rbp)
	movl	%ecx, -104(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-136(%rbp), %r9
	movl	-128(%rbp), %r8d
	movl	-104(%rbp), %ecx
	movq	%rax, %rdx
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L353:
	movq	41088(%r14), %r15
	cmpq	41096(%r14), %r15
	je	.L469
.L355:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r15)
	testb	$1, %sil
	je	.L358
.L467:
	movq	-1(%rsi), %rax
	cmpw	$79, 11(%rax)
	je	.L357
	movq	(%r15), %rsi
	testb	$1, %sil
	je	.L358
	addq	$24, %r13
	cmpq	-96(%rbp), %r13
	jne	.L470
.L461:
	movq	%r12, %rbx
	movq	-128(%rbp), %r12
.L336:
	cmpb	$0, -81(%rbp)
	jne	.L471
.L377:
	movq	-112(%rbp), %rsi
	movq	(%rsi), %rax
	movl	19(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L379
	movq	-120(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_116SubstituteValuesINS0_16NumberDictionaryEEEbPNS0_7IsolateENS0_6HandleIT_EENS6_INS0_8JSObjectEEERNS0_9ArgumentsEPb.constprop.0
	testb	%al, %al
	je	.L320
	movq	-160(%rbp), %rax
	movq	(%rax), %rdx
	movzbl	14(%rdx), %eax
	andb	$7, %al
	orl	$96, %eax
	movb	%al, 14(%rdx)
.L379:
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	movq	-160(%rbp), %rax
	movq	(%rax), %rdx
	movq	%rdx, -1(%rdi)
	testq	%rdx, %rdx
	jne	.L472
.L380:
	movq	-120(%rbp), %rax
	movq	(%rbx), %r12
	movq	(%rax), %r13
	movq	%r12, 7(%r13)
	leaq	7(%r13), %r14
	testb	$1, %r12b
	je	.L390
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L473
.L384:
	testb	$24, %al
	je	.L390
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L474
	.p2align 4,,10
	.p2align 3
.L390:
	movq	-112(%rbp), %rax
	movq	(%rax), %r12
	movl	$1, %eax
	movl	19(%r12), %edx
	testl	%edx, %edx
	jg	.L475
.L320:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L476
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	movq	41088(%r14), %r9
	cmpq	41096(%r14), %r9
	je	.L477
.L352:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r9)
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L357:
	movq	(%r15), %rax
	movq	7(%rax), %rdx
	testb	$1, %dl
	je	.L478
.L361:
	movq	15(%rax), %rdx
	testb	$1, %dl
	jne	.L348
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rsi
	leaq	3272(%r14), %r8
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_132GetMethodAndSetHomeObjectAndNameINS0_14NameDictionaryEEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateERNS0_9ArgumentsENS0_3SmiENS0_6HandleINS0_8JSObjectEEENSC_INS0_6StringEEENSC_IS5_EE
	testq	%rax, %rax
	je	.L363
	movq	(%r15), %rbx
	movq	(%rax), %r15
	movq	%r15, 15(%rbx)
	leaq	15(%rbx), %rsi
	testb	$1, %r15b
	je	.L348
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -104(%rbp)
	testl	$262144, %eax
	je	.L370
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rcx
	movq	-144(%rbp), %rsi
	movq	8(%rcx), %rax
.L370:
	testb	$24, %al
	je	.L348
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L348
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L469:
	movq	%r14, %rdi
	movq	%rsi, -144(%rbp)
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	-104(%rbp), %r9
	movq	%rax, %r15
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L477:
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L328:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L479
.L330:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L468:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L471:
	leaq	4424(%r14), %rcx
	xorl	%r9d, %r9d
	movq	%rbx, %rsi
	movq	%r14, %rdi
	leaq	2872(%r14), %rdx
	movl	$217, %r8d
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE3AddEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_4NameEEENS7_INS0_6ObjectEEENS0_15PropertyDetailsEPi@PLT
	movq	(%rax), %rax
	cmpq	%rax, (%rbx)
	je	.L377
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L475:
	movq	-120(%rbp), %rax
	movq	(%rax), %r13
	movq	%r12, 15(%r13)
	leaq	15(%r13), %r14
	testb	$1, %r12b
	je	.L389
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L387
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L387:
	testb	$24, %al
	je	.L389
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L480
	.p2align 4,,10
	.p2align 3
.L389:
	movl	$1, %eax
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L363:
	xorl	%eax, %eax
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L321:
	movq	41088(%r14), %rdx
	cmpq	41096(%r14), %rdx
	je	.L481
.L323:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L478:
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r9, -104(%rbp)
	leaq	2608(%r14), %r8
	call	_ZN2v88internal12_GLOBAL__N_132GetMethodAndSetHomeObjectAndNameINS0_14NameDictionaryEEENS0_11MaybeHandleINS0_6ObjectEEEPNS0_7IsolateERNS0_9ArgumentsENS0_3SmiENS0_6HandleINS0_8JSObjectEEENSC_INS0_6StringEEENSC_IS5_EE
	testq	%rax, %rax
	je	.L363
	movq	(%r15), %rbx
	movq	(%rax), %rdx
	movq	%rdx, 7(%rbx)
	testb	$1, %dl
	leaq	7(%rbx), %rsi
	movq	-104(%rbp), %r9
	je	.L392
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rcx
	movq	%rax, -104(%rbp)
	testl	$262144, %ecx
	je	.L365
	movq	%rbx, %rdi
	movq	%r9, -168(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rax
	movq	-168(%rbp), %r9
	movq	-152(%rbp), %rdx
	movq	-144(%rbp), %rsi
	movq	8(%rax), %rcx
.L365:
	andl	$24, %ecx
	je	.L392
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L482
	.p2align 4,,10
	.p2align 3
.L392:
	movq	(%r15), %rax
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L472:
	testb	$1, %dl
	je	.L380
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L380
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L473:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L384
.L474:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L390
.L481:
	movq	%r14, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L323
.L479:
	movq	%r12, %rdi
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L330
.L482:
	movq	%rbx, %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %r9
	jmp	.L392
.L480:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L389
.L476:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18497:
	.size	_ZN2v88internal12_GLOBAL__N_124AddDescriptorsByTemplateEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_14NameDictionaryEEENS4_INS0_16NumberDictionaryEEENS4_INS0_10FixedArrayEEENS4_INS0_8JSObjectEEEbRNS0_9ArgumentsE, .-_ZN2v88internal12_GLOBAL__N_124AddDescriptorsByTemplateEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_14NameDictionaryEEENS4_INS0_16NumberDictionaryEEENS4_INS0_10FixedArrayEEENS4_INS0_8JSObjectEEEbRNS0_9ArgumentsE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_124AddDescriptorsByTemplateEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEENS4_INS0_16NumberDictionaryEEENS4_INS0_8JSObjectEEERNS0_9ArgumentsE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"static_cast<unsigned>(id) < 256"
	.section	.text._ZN2v88internal12_GLOBAL__N_124AddDescriptorsByTemplateEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEENS4_INS0_16NumberDictionaryEEENS4_INS0_8JSObjectEEERNS0_9ArgumentsE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_124AddDescriptorsByTemplateEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEENS4_INS0_16NumberDictionaryEEENS4_INS0_8JSObjectEEERNS0_9ArgumentsE, @function
_ZN2v88internal12_GLOBAL__N_124AddDescriptorsByTemplateEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEENS4_INS0_16NumberDictionaryEEENS4_INS0_8JSObjectEEERNS0_9ArgumentsE:
.LFB18496:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	xorl	%ecx, %ecx
	subq	$136, %rsp
	movq	%rsi, -168(%rbp)
	movq	%rdi, -96(%rbp)
	movq	%r8, -136(%rbp)
	movq	%r9, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	xorl	%edx, %edx
	movswl	9(%rax), %r12d
	movl	%r12d, %esi
	call	_ZN2v88internal15DescriptorArray8AllocateEPNS0_7IsolateEiiNS0_14AllocationTypeE@PLT
	movq	%rbx, -160(%rbp)
	movq	%rax, -72(%rbp)
	movq	(%rbx), %rax
	cmpq	%rax, 1016(%r15)
	je	.L484
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_129ShallowCopyDictionaryTemplateINS0_16NumberDictionaryEEENS0_6HandleIT_EEPNS0_7IsolateES6_
	movq	%rax, -160(%rbp)
.L484:
	testl	%r12d, %r12d
	jle	.L485
	leal	-1(%r12), %eax
	movq	(%r14), %rcx
	movl	$24, %edx
	xorl	%esi, %esi
	leaq	(%rax,%rax,2), %rax
	leaq	48(,%rax,8), %rdi
	movq	%rdi, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L486:
	movq	7(%rdx,%rcx), %rax
	sarq	$32, %rax
	testb	$2, %al
	je	.L488
	andl	$1, %eax
	cmpl	$1, %eax
	adcl	$0, %esi
.L488:
	addq	$24, %rdx
	cmpq	%rdi, %rdx
	jne	.L486
	movq	-96(%rbp), %rdi
	xorl	%edx, %edx
	movl	$24, %r15d
	call	_ZN2v88internal7Factory16NewPropertyArrayEiNS0_14AllocationTypeE@PLT
	movl	$0, -128(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L631:
	testb	%al, %al
	jne	.L628
	movzbl	_ZN2v88internal17FLAG_track_fieldsE(%rip), %eax
	testb	%al, %al
	jne	.L550
.L627:
	movl	$256, %r11d
.L498:
	andl	$-449, %r10d
	movl	%r11d, %ebx
	orl	%r10d, %ebx
	andl	$2, %r10d
	jne	.L629
.L504:
	movq	-72(%rbp), %rcx
	movq	(%rcx), %rdi
	leaq	-1(%rdi), %r10
	leaq	(%r15,%r10), %rsi
	movq	%r13, (%rsi)
	testb	$1, %r13b
	je	.L549
	movq	%r13, %r11
	andq	$-262144, %r11
	movq	8(%r11), %rdx
	movq	%r11, -88(%rbp)
	testl	$262144, %edx
	je	.L518
	movq	%r13, %rdx
	movb	%al, -121(%rbp)
	movq	%r10, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r11
	movzbl	-121(%rbp), %eax
	movq	-120(%rbp), %r10
	movq	-112(%rbp), %rsi
	movq	8(%r11), %rdx
	movq	-104(%rbp), %rdi
.L518:
	andl	$24, %edx
	je	.L549
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L549
	movq	%r13, %rdx
	movb	%al, -120(%rbp)
	movq	%r10, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movzbl	-120(%rbp), %eax
	movq	-112(%rbp), %r10
	movq	-104(%rbp), %rsi
	movq	-88(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L549:
	addl	%ebx, %ebx
	sarl	%ebx
	salq	$32, %rbx
	movq	%rbx, 8(%rsi)
	movq	%r12, 16(%rsi)
	testb	%al, %al
	jne	.L523
	cmpl	$3, %r12d
	je	.L523
	movq	%r12, %rdx
	andq	$-262144, %r12
	leaq	16(%r15,%r10), %r13
	movq	8(%r12), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	je	.L521
	movq	%r13, %rsi
	movq	%rdx, -104(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r12), %rax
	movq	-104(%rbp), %rdx
	movq	-88(%rbp), %rdi
.L521:
	testb	$24, %al
	je	.L523
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L523
	movq	%r13, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L523:
	addq	$24, %r15
	cmpq	-80(%rbp), %r15
	je	.L552
.L533:
	movq	(%r14), %rax
	movq	15(%r15,%rax), %r12
	testb	$1, %r12b
	jne	.L630
.L489:
	movq	-1(%r15,%rax), %r13
	movq	7(%r15,%rax), %r10
	sarq	$32, %r10
	movl	%r10d, %ebx
	testb	$2, %r10b
	je	.L493
	movq	%r12, %rax
	movl	%r10d, %r11d
	notq	%rax
	andl	$1, %eax
	andl	$1, %r11d
	je	.L631
	testb	%al, %al
	jne	.L504
	movq	-1(%r12), %rdx
	cmpw	$79, 11(%rdx)
	jne	.L504
	movq	7(%r12), %rdx
	leaq	7(%r12), %r8
	testb	$1, %dl
	je	.L632
.L506:
	movq	15(%r12), %rdx
	leaq	15(%r12), %r8
	testb	$1, %dl
	jne	.L504
	movq	-136(%rbp), %rcx
	sarq	$32, %rdx
	leal	0(,%rdx,8), %edi
	movq	(%rcx), %r9
	movq	-152(%rbp), %rcx
	movslq	%edi, %rdi
	movq	8(%rcx), %rcx
	movq	%rcx, %r11
	movq	%rcx, -88(%rbp)
	subq	%rdi, %r11
	movq	(%r11), %r10
	cmpq	$2, %rdx
	jg	.L633
.L512:
	movq	%r10, (%r8)
	testb	$1, %r10b
	je	.L504
	movq	%r10, %r11
	andq	$-262144, %r11
	movq	8(%r11), %rdx
	movq	%r11, -88(%rbp)
	testl	$262144, %edx
	jne	.L634
	andl	$24, %edx
	je	.L504
.L646:
	movq	%r12, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L504
	movq	%r10, %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	movb	%al, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movzbl	-88(%rbp), %eax
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L630:
	movq	-1(%r12), %rdx
	cmpw	$79, 11(%rdx)
	jne	.L489
	movq	-96(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L490
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L491:
	movq	-96(%rbp), %rdi
	call	_ZN2v88internal12AccessorPair4CopyEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r12
	movq	(%r14), %rax
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L629:
	testb	$1, %bl
	jne	.L504
	movl	-128(%rbp), %ecx
	movq	-144(%rbp), %rdi
	andl	$524280, %ebx
	movl	%ecx, %edx
	movq	(%rdi), %rdi
	sall	$19, %edx
	orl	%edx, %ebx
	leal	16(,%rcx,8), %edx
	movslq	%edx, %rdx
	leaq	-1(%rdi,%rdx), %rsi
	movq	%r12, (%rsi)
	testb	%al, %al
	jne	.L547
	movq	%r12, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rdx
	movq	%rax, -88(%rbp)
	testl	$262144, %edx
	je	.L525
	movq	%r12, %rdx
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rax
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	8(%rax), %rdx
.L525:
	andl	$24, %edx
	je	.L547
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L547
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L547:
	movq	-72(%rbp), %rax
	addl	$1, -128(%rbp)
	movq	(%rax), %r12
	call	_ZN2v88internal9FieldType3AnyEv@PLT
	leaq	-1(%r12), %r8
	leaq	(%r15,%r8), %rsi
	movq	%r13, (%rsi)
	testb	$1, %r13b
	je	.L546
	movq	%r13, %r10
	andq	$-262144, %r10
	movq	8(%r10), %rdx
	movq	%r10, -88(%rbp)
	testl	$262144, %edx
	je	.L528
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r8, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r10
	movq	-120(%rbp), %r8
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %rsi
	movq	8(%r10), %rdx
.L528:
	andl	$24, %edx
	je	.L546
	movq	%r12, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L546
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r8, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rax
	movq	-88(%rbp), %rsi
	.p2align 4,,10
	.p2align 3
.L546:
	addl	%ebx, %ebx
	sarl	%ebx
	orl	$4, %ebx
	salq	$32, %rbx
	movq	%rbx, 8(%rsi)
	movq	%rax, 16(%rsi)
	testb	$1, %al
	je	.L523
	cmpl	$3, %eax
	je	.L523
	movq	%rax, %rdx
	andq	$-262144, %rax
	leaq	16(%r15,%r8), %r13
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	je	.L531
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-88(%rbp), %rdx
.L531:
	testb	$24, %al
	je	.L523
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L523
	movq	%r13, %rsi
	movq	%r12, %rdi
	addq	$24, %r15
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	cmpq	-80(%rbp), %r15
	jne	.L533
	.p2align 4,,10
	.p2align 3
.L552:
	movq	-168(%rbp), %rax
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rax
	movq	(%rax), %rdx
	movswl	9(%rdx), %ecx
	movq	-96(%rbp), %rsi
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi@PLT
	movq	-64(%rbp), %rax
	movq	$0, 47(%rax)
	movq	-64(%rbp), %rdi
	call	_ZN2v88internal3Map12GetVisitorIdES1_@PLT
	cmpl	$255, %eax
	ja	.L635
	movq	-64(%rbp), %rdx
	movb	%al, 10(%rdx)
	movq	-160(%rbp), %rsi
	movq	(%rsi), %rax
	movl	19(%rax), %edx
	testl	%edx, %edx
	jle	.L535
	movq	-152(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	-96(%rbp), %rdi
	call	_ZN2v88internal12_GLOBAL__N_116SubstituteValuesINS0_16NumberDictionaryEEEbPNS0_7IsolateENS0_6HandleIT_EENS6_INS0_8JSObjectEEERNS0_9ArgumentsEPb.constprop.0
	testb	%al, %al
	je	.L483
	movq	-168(%rbp), %rax
	movq	(%rax), %rdx
	movzbl	14(%rdx), %eax
	andb	$7, %al
	orl	$96, %eax
	movb	%al, 14(%rdx)
.L535:
	movq	-136(%rbp), %rax
	movq	(%rax), %rdi
	movq	-168(%rbp), %rax
	movq	(%rax), %rdx
	movq	%rdx, -1(%rdi)
	testq	%rdx, %rdx
	jne	.L636
.L537:
	movq	-160(%rbp), %rax
	movq	(%rax), %r13
	movl	19(%r13), %eax
	testl	%eax, %eax
	jg	.L637
.L540:
	movq	-144(%rbp), %rax
	movq	(%rax), %rdx
	movl	$1, %eax
	testw	$1023, 11(%rdx)
	jne	.L638
.L483:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L639
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L628:
	.cfi_restore_state
	movq	-152(%rbp), %rcx
	movq	-136(%rbp), %rax
	movq	%r12, %rdx
	sarq	$32, %rdx
	movq	8(%rcx), %rcx
	movq	(%rax), %r8
	leal	0(,%rdx,8), %eax
	cltq
	movq	%rcx, %rbx
	movq	%rcx, -88(%rbp)
	subq	%rax, %rbx
	movq	(%rbx), %r12
	cmpq	$2, %rdx
	jg	.L640
.L496:
	movq	%r12, %rax
	notq	%rax
	andl	$1, %eax
	cmpb	$0, _ZN2v88internal17FLAG_track_fieldsE(%rip)
	je	.L627
	testb	%al, %al
	je	.L550
	movl	$64, %r11d
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L550:
	cmpb	$0, _ZN2v88internal24FLAG_track_double_fieldsE(%rip)
	jne	.L499
.L502:
	cmpb	$0, _ZN2v88internal26FLAG_track_computed_fieldsE(%rip)
	jne	.L641
.L501:
	movzbl	_ZN2v88internal29FLAG_track_heap_object_fieldsE(%rip), %eax
	testb	%al, %al
	je	.L627
	xorl	%eax, %eax
	movl	$192, %r11d
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L640:
	movq	-96(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, %rdx
	movq	%r10, -104(%rbp)
	movl	%r11d, -88(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_113SetHomeObjectEPNS0_7IsolateENS0_10JSFunctionENS0_8JSObjectE
	movq	(%rbx), %r12
	movq	-104(%rbp), %r10
	movl	-88(%rbp), %r11d
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L490:
	movq	41088(%rax), %rsi
	cmpq	41096(%rax), %rsi
	je	.L642
.L492:
	movq	-96(%rbp), %rcx
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rcx)
	movq	%r12, (%rsi)
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L499:
	movq	-1(%r12), %rax
	cmpw	$65, 11(%rax)
	jne	.L502
	xorl	%eax, %eax
	movl	$128, %r11d
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L641:
	movq	%r12, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	%r12, -37512(%rax)
	jne	.L501
	xorl	%eax, %eax
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L637:
	movq	-136(%rbp), %rax
	movq	(%rax), %r14
	movq	%r13, 15(%r14)
	leaq	15(%r14), %r15
	testb	$1, %r13b
	je	.L540
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L542
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L542:
	testb	$24, %al
	je	.L540
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L540
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L638:
	movb	%al, -72(%rbp)
	movq	-136(%rbp), %rax
	movq	%r12, %rdi
	movq	(%rax), %rdx
	movq	-144(%rbp), %rax
	movq	%rdx, -64(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE@PLT
	movzbl	-72(%rbp), %eax
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L636:
	testb	$1, %dl
	je	.L537
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L537
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L632:
	movq	-136(%rbp), %rcx
	sarq	$32, %rdx
	leal	0(,%rdx,8), %edi
	movq	(%rcx), %r9
	movq	-152(%rbp), %rcx
	movslq	%edi, %rdi
	movq	8(%rcx), %rcx
	movq	%rcx, %r11
	movq	%rcx, -88(%rbp)
	subq	%rdi, %r11
	movq	(%r11), %r10
	cmpq	$2, %rdx
	jg	.L643
.L507:
	movq	%r10, (%r8)
	testb	$1, %r10b
	je	.L506
	movq	%r10, %r11
	andq	$-262144, %r11
	movq	8(%r11), %rdx
	movq	%r11, -88(%rbp)
	testl	$262144, %edx
	jne	.L644
	andl	$24, %edx
	je	.L506
.L645:
	movq	%r12, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L506
	movq	%r10, %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	movb	%al, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movzbl	-88(%rbp), %eax
	jmp	.L506
.L644:
	movq	%r10, %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	movb	%al, -120(%rbp)
	movq	%r10, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r11
	movzbl	-120(%rbp), %eax
	movq	-112(%rbp), %r10
	movq	-104(%rbp), %r8
	movq	8(%r11), %rdx
	andl	$24, %edx
	jne	.L645
	jmp	.L506
.L634:
	movq	%r10, %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	movb	%al, -120(%rbp)
	movq	%r10, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r11
	movzbl	-120(%rbp), %eax
	movq	-112(%rbp), %r10
	movq	-104(%rbp), %r8
	movq	8(%r11), %rdx
	andl	$24, %edx
	jne	.L646
	jmp	.L504
.L642:
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L492
.L633:
	movq	-96(%rbp), %rdi
	movq	%r10, %rsi
	movq	%r9, %rdx
	movb	%al, -112(%rbp)
	movq	%r11, -104(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_113SetHomeObjectEPNS0_7IsolateENS0_10JSFunctionENS0_8JSObjectE
	movq	-104(%rbp), %r11
	movzbl	-112(%rbp), %eax
	movq	-88(%rbp), %r8
	movq	(%r11), %r10
	jmp	.L512
.L643:
	movq	-96(%rbp), %rdi
	movq	%r10, %rsi
	movq	%r9, %rdx
	movb	%al, -112(%rbp)
	movq	%r11, -104(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_113SetHomeObjectEPNS0_7IsolateENS0_10JSFunctionENS0_8JSObjectE
	movq	-104(%rbp), %r11
	movzbl	-112(%rbp), %eax
	movq	-88(%rbp), %r8
	movq	(%r11), %r10
	jmp	.L507
.L635:
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L485:
	movq	-96(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory16NewPropertyArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, -144(%rbp)
	jmp	.L552
.L493:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L639:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18496:
	.size	_ZN2v88internal12_GLOBAL__N_124AddDescriptorsByTemplateEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEENS4_INS0_16NumberDictionaryEEENS4_INS0_8JSObjectEEERNS0_9ArgumentsE, .-_ZN2v88internal12_GLOBAL__N_124AddDescriptorsByTemplateEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEENS4_INS0_16NumberDictionaryEEENS4_INS0_8JSObjectEEERNS0_9ArgumentsE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_111DefineClassEPNS0_7IsolateENS0_6HandleINS0_16ClassBoilerplateEEENS4_INS0_6ObjectEEENS4_INS0_10JSFunctionEEERNS0_9ArgumentsE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"!constructor_or_backpointer().IsMap()"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_111DefineClassEPNS0_7IsolateENS0_6HandleINS0_16ClassBoilerplateEEENS4_INS0_6ObjectEEENS4_INS0_10JSFunctionEEERNS0_9ArgumentsE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"init class constructor"
.LC8:
	.string	"InitialMap"
.LC9:
	.string	"init class prototype"
	.section	.text._ZN2v88internal12_GLOBAL__N_111DefineClassEPNS0_7IsolateENS0_6HandleINS0_16ClassBoilerplateEEENS4_INS0_6ObjectEEENS4_INS0_10JSFunctionEEERNS0_9ArgumentsE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_111DefineClassEPNS0_7IsolateENS0_6HandleINS0_16ClassBoilerplateEEENS4_INS0_6ObjectEEENS4_INS0_10JSFunctionEEERNS0_9ArgumentsE, @function
_ZN2v88internal12_GLOBAL__N_111DefineClassEPNS0_7IsolateENS0_6HandleINS0_16ClassBoilerplateEEENS4_INS0_6ObjectEEENS4_INS0_10JSFunctionEEERNS0_9ArgumentsE:
.LFB18501:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%r8, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	cmpq	%rax, 96(%rdi)
	je	.L745
	cmpq	104(%rdi), %rax
	je	.L746
	movq	%rdx, %r12
	testb	$1, %al
	jne	.L653
.L655:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movl	$52, %esi
.L743:
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L744:
	xorl	%eax, %eax
.L654:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L747
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L746:
	.cfi_restore_state
	leaq	104(%rdi), %rax
	movq	$0, -80(%rbp)
	movq	%rax, -88(%rbp)
.L650:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal3Map6CreateEPNS0_7IsolateEi@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	(%rax), %rdx
	movq	%rax, %r12
	movq	-72(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rdx, -16(%rax)
	movq	0(%r13), %rax
	movq	-1(%rax), %r15
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L664
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L665:
	movq	%r14, %rdi
	call	_ZN2v88internal3Map19CopyDropDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r15
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L667
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
.L667:
	movq	(%rbx), %rax
	movq	41112(%r14), %rdi
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L668
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
.L669:
	movq	(%rbx), %rax
	movq	41112(%r14), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L671
	movq	%r10, -80(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-80(%rbp), %r10
	movq	%rax, %r8
.L672:
	movq	(%rbx), %rax
	movq	41112(%r14), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L674
	movq	%r8, -96(%rbp)
	movq	%r10, -80(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-80(%rbp), %r10
	movq	-96(%rbp), %r8
	movq	(%rax), %rsi
	movq	%rax, %r11
	testb	$1, %sil
	jne	.L677
.L679:
	movq	-72(%rbp), %r9
	movq	%r13, %r8
	movq	%r10, %rcx
	movq	%r11, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_124AddDescriptorsByTemplateEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEENS4_INS0_16NumberDictionaryEEENS4_INS0_8JSObjectEEERNS0_9ArgumentsE
.L678:
	testb	%al, %al
	je	.L744
	movq	(%r12), %rax
	movq	-1(%rax), %r15
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L682
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L683:
	movq	%r14, %rdi
	call	_ZN2v88internal3Map19CopyDropDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%r14, %rdi
	movl	$1, %ecx
	movq	(%rax), %rdx
	movq	%rax, %r15
	movq	%r15, %rsi
	movl	15(%rdx), %eax
	orl	$1048576, %eax
	movl	%eax, 15(%rdx)
	movq	-88(%rbp), %rdx
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	0(%r13), %rdi
	movq	(%r12), %rdx
	movq	%rdx, 55(%rdi)
	leaq	55(%rdi), %rsi
	testb	$1, %dl
	je	.L709
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	je	.L686
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	8(%rcx), %rax
.L686:
	testb	$24, %al
	je	.L709
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L748
	.p2align 4,,10
	.p2align 3
.L709:
	movq	(%r15), %rdi
	movq	0(%r13), %rdx
	movq	31(%rdi), %rax
	leaq	31(%rdi), %rsi
	testb	$1, %al
	jne	.L749
.L688:
	movq	%rdx, 31(%rdi)
	testb	$1, %dl
	je	.L708
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	je	.L690
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	8(%rcx), %rax
.L690:
	testb	$24, %al
	je	.L708
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L750
	.p2align 4,,10
	.p2align 3
.L708:
	movq	(%rbx), %rax
	movq	41112(%r14), %rdi
	movq	63(%rax), %rsi
	testq	%rdi, %rdi
	je	.L692
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L693:
	movq	(%rbx), %rax
	movq	41112(%r14), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L695
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-80(%rbp), %r8
	movq	%rax, %rcx
.L696:
	movq	(%rbx), %rax
	movq	41112(%r14), %rdi
	movq	47(%rax), %rsi
	testq	%rdi, %rdi
	je	.L698
	movq	%rcx, -88(%rbp)
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %rcx
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L699:
	testb	$1, %sil
	jne	.L701
.L703:
	movq	-72(%rbp), %r9
	movq	%r12, %r8
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_124AddDescriptorsByTemplateEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEENS4_INS0_16NumberDictionaryEEENS4_INS0_8JSObjectEEERNS0_9ArgumentsE
.L702:
	testb	%al, %al
	je	.L744
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	jne	.L751
.L705:
	movq	%r12, %rax
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L664:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L752
.L666:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r15, (%rsi)
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L674:
	movq	41088(%r14), %r11
	cmpq	41096(%r14), %r11
	je	.L753
.L676:
	leaq	8(%r11), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r11)
	testb	$1, %sil
	je	.L679
.L677:
	movq	-1(%rsi), %rax
	cmpw	$130, 11(%rax)
	jne	.L679
	movq	(%r15), %rdx
	movq	%r11, -104(%rbp)
	leaq	-64(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r8, -96(%rbp)
	movq	%r10, -80(%rbp)
	movl	15(%rdx), %eax
	orl	$35651584, %eax
	movl	%eax, 15(%rdx)
	movq	(%r15), %rax
	movq	296(%r14), %rdx
	movq	%rax, -64(%rbp)
	movswl	9(%rdx), %ecx
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi@PLT
	movq	-64(%rbp), %rax
	movq	$0, 47(%rax)
	movq	-64(%rbp), %rdi
	call	_ZN2v88internal3Map12GetVisitorIdES1_@PLT
	movq	-80(%rbp), %r10
	movq	-96(%rbp), %r8
	cmpl	$255, %eax
	movq	-104(%rbp), %r11
	ja	.L754
	movq	-64(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r13, %r9
	movq	%r10, %rcx
	movb	%al, 10(%rdx)
	movq	(%r15), %rdx
	movl	15(%rdx), %eax
	andl	$-67108865, %eax
	movl	%eax, 15(%rdx)
	movq	(%r15), %rdx
	movl	15(%rdx), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rdx)
	movq	(%r15), %rdx
	movl	15(%rdx), %eax
	andl	$536870911, %eax
	movl	%eax, 15(%rdx)
	movq	%r11, %rdx
	movq	(%rbx), %rax
	movslq	19(%rax), %rax
	pushq	-72(%rbp)
	andl	$1, %eax
	pushq	%rax
	call	_ZN2v88internal12_GLOBAL__N_124AddDescriptorsByTemplateEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_14NameDictionaryEEENS4_INS0_16NumberDictionaryEEENS4_INS0_10FixedArrayEEENS4_INS0_8JSObjectEEEbRNS0_9ArgumentsE
	popq	%rsi
	popq	%rdi
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L671:
	movq	41088(%r14), %r8
	cmpq	41096(%r14), %r8
	je	.L755
.L673:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r8)
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L668:
	movq	41088(%r14), %r10
	cmpq	41096(%r14), %r10
	je	.L756
.L670:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r10)
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L745:
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	519(%rax), %rsi
	movq	41112(%rdi), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L649
	movq	%rax, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	$0, -80(%rbp)
	movq	%rax, -88(%rbp)
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L653:
	movq	-1(%rax), %rax
	testb	$64, 13(%rax)
	je	.L655
	leaq	3104(%rdi), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	call	_ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L654
	movq	(%rax), %rax
	cmpq	%rax, 104(%r14)
	je	.L661
	testb	$1, %al
	jne	.L757
.L660:
	movq	-88(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$117, %esi
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L649:
	movq	41088(%rdi), %rax
	movq	%rax, -88(%rbp)
	cmpq	41096(%rdi), %rax
	je	.L758
.L651:
	movq	-88(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rcx)
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L682:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L759
.L684:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r15, (%rsi)
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L695:
	movq	41088(%r14), %rcx
	cmpq	41096(%r14), %rcx
	je	.L760
.L697:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rcx)
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L692:
	movq	41088(%r14), %r8
	cmpq	41096(%r14), %r8
	je	.L761
.L694:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r8)
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L698:
	movq	41088(%r14), %rdx
	cmpq	41096(%r14), %rdx
	je	.L762
.L700:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L749:
	movq	-1(%rax), %rax
	cmpw	$68, 11(%rax)
	jne	.L688
	leaq	.LC6(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L752:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L755:
	movq	%r14, %rdi
	movq	%rsi, -96(%rbp)
	movq	%r10, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	-80(%rbp), %r10
	movq	%rax, %r8
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L756:
	movq	%r14, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L753:
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%r10, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %r8
	movq	-80(%rbp), %r10
	movq	%rax, %r11
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L748:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L751:
	movq	41016(%r14), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L763
.L706:
	movq	41016(%r14), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L705
	movq	(%r12), %rax
	xorl	%r9d, %r9d
	leaq	.LC9(%rip), %r8
	xorl	%edx, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r13, %rdi
	movq	-1(%rax), %rcx
	call	_ZN2v88internal6Logger8MapEventEPKcNS0_3MapES4_S3_NS0_10HeapObjectE@PLT
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L757:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L660
.L661:
	movq	41112(%r14), %rdi
	movq	(%r12), %rsi
	testq	%rdi, %rdi
	je	.L764
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -80(%rbp)
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L750:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L701:
	movq	-1(%rsi), %rax
	cmpw	$130, 11(%rax)
	jne	.L703
	movq	(%r15), %rsi
	movq	%r12, %r9
	movq	%r14, %rdi
	movl	15(%rsi), %eax
	orl	$35651584, %eax
	movl	%eax, 15(%rsi)
	movq	(%r15), %rsi
	movl	15(%rsi), %eax
	andl	$-67108865, %eax
	movl	%eax, 15(%rsi)
	movq	(%r15), %rsi
	movl	15(%rsi), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rsi)
	movq	(%r15), %rsi
	movl	15(%rsi), %eax
	andl	$536870911, %eax
	movl	%eax, 15(%rsi)
	movq	%r15, %rsi
	pushq	-72(%rbp)
	pushq	$0
	call	_ZN2v88internal12_GLOBAL__N_124AddDescriptorsByTemplateEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_14NameDictionaryEEENS4_INS0_16NumberDictionaryEEENS4_INS0_10FixedArrayEEENS4_INS0_8JSObjectEEEbRNS0_9ArgumentsE
	popq	%rdx
	popq	%rcx
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L758:
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, -88(%rbp)
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L764:
	movq	41088(%r14), %rax
	movq	%rax, -80(%rbp)
	cmpq	41096(%r14), %rax
	je	.L765
.L663:
	movq	-80(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rcx)
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L759:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L762:
	movq	%r14, %rdi
	movq	%rcx, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L761:
	movq	%r14, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L760:
	movq	%r14, %rdi
	movq	%rsi, -88(%rbp)
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L697
.L763:
	movq	0(%r13), %rax
	leaq	-64(%rbp), %rdi
	movq	23(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	leaq	.LC7(%rip), %r8
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rax, %r9
	movq	0(%r13), %rax
	leaq	.LC8(%rip), %rsi
	movq	-1(%rax), %rcx
	call	_ZN2v88internal6Logger8MapEventEPKcNS0_3MapES4_S3_NS0_10HeapObjectE@PLT
	jmp	.L706
.L754:
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L765:
	movq	%r14, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, -80(%rbp)
	jmp	.L663
.L747:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18501:
	.size	_ZN2v88internal12_GLOBAL__N_111DefineClassEPNS0_7IsolateENS0_6HandleINS0_16ClassBoilerplateEEENS4_INS0_6ObjectEEENS4_INS0_10JSFunctionEEERNS0_9ArgumentsE, .-_ZN2v88internal12_GLOBAL__N_111DefineClassEPNS0_7IsolateENS0_6HandleINS0_16ClassBoilerplateEEENS4_INS0_6ObjectEEENS4_INS0_10JSFunctionEEERNS0_9ArgumentsE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB9345:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L772
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L775
.L766:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L772:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L775:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L766
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE9345:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL38Stats_Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC11:
	.string	"V8.Runtime_Runtime_ThrowNotSuperConstructor"
	.section	.rodata._ZN2v88internalL38Stats_Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"args[1].IsJSFunction()"
	.section	.text._ZN2v88internalL38Stats_Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Stats_Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL38Stats_Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateE:
.LFB18483:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L808
.L777:
	movq	_ZZN2v88internalL38Stats_Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic100(%rip), %rbx
	testq	%rbx, %rbx
	je	.L809
.L779:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L810
.L781:
	addl	$1, 41104(%r12)
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	testb	$1, %al
	jne	.L785
.L786:
	leaq	.LC12(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L809:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L811
.L780:
	movq	%rbx, _ZZN2v88internalL38Stats_Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic100(%rip)
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L810:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L812
.L782:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L783
	movq	(%rdi), %rax
	call	*8(%rax)
.L783:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L784
	movq	(%rdi), %rax
	call	*8(%rax)
.L784:
	leaq	.LC11(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L785:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L786
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_124ThrowNotSuperConstructorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_10JSFunctionEEE
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L787
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L787:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L813
.L776:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L814
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L808:
	.cfi_restore_state
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$243, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L813:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L812:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L811:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L780
.L814:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18483:
	.size	_ZN2v88internalL38Stats_Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL38Stats_Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Stats_Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"V8.Runtime_Runtime_ThrowUnsupportedSuperError"
	.section	.text._ZN2v88internalL40Stats_Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL40Stats_Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE.isra.0:
.LFB22545:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L845
.L816:
	movq	_ZZN2v88internalL40Stats_Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic28(%rip), %rbx
	testq	%rbx, %rbx
	je	.L846
.L818:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L847
.L820:
	addl	$1, 41104(%r12)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$180, %esi
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L824
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L824:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L848
.L815:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L849
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L846:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L850
.L819:
	movq	%rbx, _ZZN2v88internalL40Stats_Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic28(%rip)
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L847:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L851
.L821:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L822
	movq	(%rdi), %rax
	call	*8(%rax)
.L822:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L823
	movq	(%rdi), %rax
	call	*8(%rax)
.L823:
	leaq	.LC13(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L848:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L845:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$247, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L851:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L850:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L819
.L849:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22545:
	.size	_ZN2v88internalL40Stats_Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL40Stats_Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL39Stats_Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"V8.Runtime_Runtime_ThrowStaticPrototypeError"
	.section	.text._ZN2v88internalL39Stats_Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Stats_Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL39Stats_Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateE.isra.0:
.LFB22546:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L882
.L853:
	movq	_ZZN2v88internalL39Stats_Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic46(%rip), %rbx
	testq	%rbx, %rbx
	je	.L883
.L855:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L884
.L857:
	addl	$1, 41104(%r12)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$162, %esi
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L861
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L861:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L885
.L852:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L886
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L883:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L887
.L856:
	movq	%rbx, _ZZN2v88internalL39Stats_Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic46(%rip)
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L884:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L888
.L858:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L859
	movq	(%rdi), %rax
	call	*8(%rax)
.L859:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L860
	movq	(%rdi), %rax
	call	*8(%rax)
.L860:
	leaq	.LC14(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L885:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L882:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$244, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L888:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L887:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L856
.L886:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22546:
	.size	_ZN2v88internalL39Stats_Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL39Stats_Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL42Stats_Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"V8.Runtime_Runtime_ThrowSuperAlreadyCalledError"
	.section	.text._ZN2v88internalL42Stats_Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Stats_Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL42Stats_Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateE.isra.0:
.LFB22547:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L919
.L890:
	movq	_ZZN2v88internalL42Stats_Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic53(%rip), %rbx
	testq	%rbx, %rbx
	je	.L920
.L892:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L921
.L894:
	addl	$1, 41104(%r12)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$178, %esi
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L898
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L898:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L922
.L889:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L923
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L920:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L924
.L893:
	movq	%rbx, _ZZN2v88internalL42Stats_Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic53(%rip)
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L921:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L925
.L895:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L896
	movq	(%rdi), %rax
	call	*8(%rax)
.L896:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L897
	movq	(%rdi), %rax
	call	*8(%rax)
.L897:
	leaq	.LC15(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L922:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L919:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$245, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L925:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC15(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L924:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L893
.L923:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22547:
	.size	_ZN2v88internalL42Stats_Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL42Stats_Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL33Stats_Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"V8.Runtime_Runtime_ThrowSuperNotCalled"
	.section	.text._ZN2v88internalL33Stats_Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateE.isra.0:
.LFB22548:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L956
.L927:
	movq	_ZZN2v88internalL33Stats_Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateEE27trace_event_unique_atomic60(%rip), %rbx
	testq	%rbx, %rbx
	je	.L957
.L929:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L958
.L931:
	addl	$1, 41104(%r12)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$290, %esi
	movq	%r12, %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L935
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L935:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L959
.L926:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L960
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L957:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L961
.L930:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateEE27trace_event_unique_atomic60(%rip)
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L958:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L962
.L932:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L933
	movq	(%rdi), %rax
	call	*8(%rax)
.L933:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L934
	movq	(%rdi), %rax
	call	*8(%rax)
.L934:
	leaq	.LC16(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L959:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L956:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$246, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L962:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC16(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L961:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L930
.L960:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22548:
	.size	_ZN2v88internalL33Stats_Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL46Stats_Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"V8.Runtime_Runtime_ThrowConstructorNonCallableError"
	.section	.rodata._ZN2v88internalL46Stats_Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"args[0].IsJSFunction()"
	.section	.text._ZN2v88internalL46Stats_Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Stats_Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL46Stats_Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateE:
.LFB18470:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1004
.L964:
	movq	_ZZN2v88internalL46Stats_Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic36(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1005
.L966:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1006
.L968:
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L972
.L973:
	leaq	.LC18(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1005:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1007
.L967:
	movq	%rbx, _ZZN2v88internalL46Stats_Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic36(%rip)
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L972:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L973
	movq	23(%rax), %r14
	movq	15(%r14), %rax
	testb	$1, %al
	jne	.L1008
.L974:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L975:
	testb	%al, %al
	je	.L979
	movq	15(%r14), %r15
	testb	$1, %r15b
	jne	.L1009
.L977:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L980
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L981:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$39, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %rbx
	je	.L983
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L983:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1010
.L963:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1011
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L980:
	.cfi_restore_state
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L1012
.L982:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rdx)
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	-1(%r15), %rax
	cmpw	$136, 11(%rax)
	jne	.L977
	leaq	-168(%rbp), %rdi
	movq	%r15, -168(%rbp)
	movq	%rdi, -184(%rbp)
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	testb	%al, %al
	jne	.L1013
	.p2align 4,,10
	.p2align 3
.L979:
	andq	$-262144, %r14
	movq	24(%r14), %rax
	movq	-37464(%rax), %r15
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L1006:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1014
.L969:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L970
	movq	(%rdi), %rax
	call	*8(%rax)
.L970:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L971
	movq	(%rdi), %rax
	call	*8(%rax)
.L971:
	leaq	.LC17(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L1008:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L974
	leaq	-168(%rbp), %rdi
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$242, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L1010:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L1012:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L1007:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L1014:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC17(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L1013:
	movq	-184(%rbp), %rdi
	movq	%r15, -168(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	%rax, %r15
	jmp	.L977
.L1011:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18470:
	.size	_ZN2v88internalL46Stats_Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateE, .-_ZN2v88internalL46Stats_Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL25Stats_Runtime_DefineClassEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"V8.Runtime_Runtime_DefineClass"
	.section	.rodata._ZN2v88internalL25Stats_Runtime_DefineClassEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"args[0].IsClassBoilerplate()"
	.section	.text._ZN2v88internalL25Stats_Runtime_DefineClassEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Stats_Runtime_DefineClassEiPmPNS0_7IsolateE, @function
_ZN2v88internalL25Stats_Runtime_DefineClassEiPmPNS0_7IsolateE:
.LFB18502:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movslq	%edi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1052
.L1016:
	movq	_ZZN2v88internalL25Stats_Runtime_DefineClassEiPmPNS0_7IsolateEE28trace_event_unique_atomic647(%rip), %r14
	testq	%r14, %r14
	je	.L1053
.L1018:
	movq	$0, -160(%rbp)
	movzbl	(%r14), %eax
	testb	$5, %al
	jne	.L1054
.L1020:
	movq	%rbx, -176(%rbp)
	movq	41088(%r12), %r14
	movq	%r13, -168(%rbp)
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1024
.L1025:
	leaq	.LC20(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1053:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1055
.L1019:
	movq	%r14, _ZZN2v88internalL25Stats_Runtime_DefineClassEiPmPNS0_7IsolateEE28trace_event_unique_atomic647(%rip)
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1024:
	movq	-1(%rax), %rax
	cmpw	$123, 11(%rax)
	jne	.L1025
	movq	-8(%r13), %rax
	testb	$1, %al
	jne	.L1056
.L1026:
	leaq	.LC12(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1054:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1057
.L1021:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1022
	movq	(%rdi), %rax
	call	*8(%rax)
.L1022:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1023
	movq	(%rdi), %rax
	call	*8(%rax)
.L1023:
	leaq	.LC19(%rip), %rax
	movq	%r14, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1056:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L1026
	leaq	-8(%r13), %rcx
	leaq	-16(%r13), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-176(%rbp), %r8
	call	_ZN2v88internal12_GLOBAL__N_111DefineClassEPNS0_7IsolateENS0_6HandleINS0_16ClassBoilerplateEEENS4_INS0_6ObjectEEENS4_INS0_10JSFunctionEEERNS0_9ArgumentsE
	testq	%rax, %rax
	je	.L1058
	movq	(%rax), %r13
.L1029:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1032
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1032:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1059
.L1015:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1060
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1058:
	.cfi_restore_state
	movq	312(%r12), %r13
	jmp	.L1029
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$236, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1059:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1057:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC19(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r14, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1055:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L1019
.L1060:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18502:
	.size	_ZN2v88internalL25Stats_Runtime_DefineClassEiPmPNS0_7IsolateE, .-_ZN2v88internalL25Stats_Runtime_DefineClassEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL27Stats_Runtime_LoadFromSuperEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"V8.Runtime_Runtime_LoadFromSuper"
	.section	.rodata._ZN2v88internalL27Stats_Runtime_LoadFromSuperEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC22:
	.string	"args[1].IsJSObject()"
.LC23:
	.string	"args[2].IsName()"
	.section	.text._ZN2v88internalL27Stats_Runtime_LoadFromSuperEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_LoadFromSuperEiPmPNS0_7IsolateE, @function
_ZN2v88internalL27Stats_Runtime_LoadFromSuperEiPmPNS0_7IsolateE:
.LFB18514:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1098
.L1062:
	movq	_ZZN2v88internalL27Stats_Runtime_LoadFromSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic720(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1099
.L1064:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1100
.L1066:
	addl	$1, 41104(%r12)
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	testb	$1, %al
	jne	.L1070
.L1071:
	leaq	.LC22(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1099:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1101
.L1065:
	movq	%rbx, _ZZN2v88internalL27Stats_Runtime_LoadFromSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic720(%rip)
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1070:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L1071
	movq	-16(%r13), %rax
	leaq	-16(%r13), %rcx
	testb	$1, %al
	jne	.L1102
.L1072:
	leaq	.LC23(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1100:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1103
.L1067:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1068
	movq	(%rdi), %rax
	call	*8(%rax)
.L1068:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1069
	movq	(%rdi), %rax
	call	*8(%rax)
.L1069:
	leaq	.LC21(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L1102:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L1072
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_113LoadFromSuperEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS4_INS0_4NameEEE
	testq	%rax, %rax
	je	.L1104
	movq	(%rax), %r13
.L1075:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1078
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1078:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1105
.L1061:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1106
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1104:
	.cfi_restore_state
	movq	312(%r12), %r13
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1098:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$238, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1105:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1103:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC21(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1101:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1065
.L1106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18514:
	.size	_ZN2v88internalL27Stats_Runtime_LoadFromSuperEiPmPNS0_7IsolateE, .-_ZN2v88internalL27Stats_Runtime_LoadFromSuperEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL26Stats_Runtime_StoreToSuperEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"V8.Runtime_Runtime_StoreToSuper"
	.section	.text._ZN2v88internalL26Stats_Runtime_StoreToSuperEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL26Stats_Runtime_StoreToSuperEiPmPNS0_7IsolateE, @function
_ZN2v88internalL26Stats_Runtime_StoreToSuperEiPmPNS0_7IsolateE:
.LFB18522:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1144
.L1108:
	movq	_ZZN2v88internalL26Stats_Runtime_StoreToSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic792(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1145
.L1110:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1146
.L1112:
	addl	$1, 41104(%r12)
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rsi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	testb	$1, %al
	jne	.L1116
.L1117:
	leaq	.LC22(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1145:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1147
.L1111:
	movq	%rbx, _ZZN2v88internalL26Stats_Runtime_StoreToSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic792(%rip)
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L1117
	movq	-16(%r13), %rax
	leaq	-16(%r13), %rcx
	testb	$1, %al
	jne	.L1148
.L1118:
	leaq	.LC23(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1146:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1149
.L1113:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1114
	movq	(%rdi), %rax
	call	*8(%rax)
.L1114:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1115
	movq	(%rdi), %rax
	call	*8(%rax)
.L1115:
	leaq	.LC24(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1148:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L1118
	leaq	-24(%r13), %r8
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_112StoreToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEENS4_INS0_4NameEEES8_
	testq	%rax, %rax
	je	.L1150
	movq	(%rax), %r13
.L1121:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1124
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1124:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1151
.L1107:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1152
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1150:
	.cfi_restore_state
	movq	312(%r12), %r13
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1144:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$241, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1151:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1149:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC24(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1147:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1111
.L1152:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18522:
	.size	_ZN2v88internalL26Stats_Runtime_StoreToSuperEiPmPNS0_7IsolateE, .-_ZN2v88internalL26Stats_Runtime_StoreToSuperEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Stats_Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"V8.Runtime_Runtime_StoreKeyedToSuper"
	.section	.text._ZN2v88internalL31Stats_Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Stats_Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateE:
.LFB18526:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1188
.L1154:
	movq	_ZZN2v88internalL31Stats_Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic824(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1189
.L1156:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1190
.L1158:
	addl	$1, 41104(%r12)
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rsi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	testb	$1, %al
	jne	.L1162
.L1163:
	leaq	.LC22(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1189:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1191
.L1157:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic824(%rip)
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1162:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L1163
	leaq	-16(%r13), %rcx
	leaq	-24(%r13), %r8
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internalL17StoreKeyedToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6ObjectEEES7_S7_
	testq	%rax, %rax
	je	.L1192
	movq	(%rax), %r13
.L1165:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1168
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1168:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1193
.L1153:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1194
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1190:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1195
.L1159:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1160
	movq	(%rdi), %rax
	call	*8(%rax)
.L1160:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1161
	movq	(%rdi), %rax
	call	*8(%rax)
.L1161:
	leaq	.LC25(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1192:
	movq	312(%r12), %r13
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1188:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$240, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1193:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1191:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1195:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC25(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1159
.L1194:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18526:
	.size	_ZN2v88internalL31Stats_Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Stats_Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL30Stats_Runtime_HomeObjectSymbolEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"V8.Runtime_Runtime_HomeObjectSymbol"
	.section	.text._ZN2v88internalL30Stats_Runtime_HomeObjectSymbolEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_HomeObjectSymbolEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL30Stats_Runtime_HomeObjectSymbolEiPmPNS0_7IsolateE.isra.0:
.LFB22554:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1225
.L1197:
	movq	_ZZN2v88internalL30Stats_Runtime_HomeObjectSymbolEiPmPNS0_7IsolateEE28trace_event_unique_atomic108(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1226
.L1199:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1227
.L1201:
	leaq	-144(%rbp), %rdi
	movq	3720(%r12), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1228
.L1196:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1229
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1226:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1230
.L1200:
	movq	%rbx, _ZZN2v88internalL30Stats_Runtime_HomeObjectSymbolEiPmPNS0_7IsolateEE28trace_event_unique_atomic108(%rip)
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1227:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1231
.L1202:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1203
	movq	(%rdi), %rax
	call	*8(%rax)
.L1203:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1204
	movq	(%rdi), %rax
	call	*8(%rax)
.L1204:
	leaq	.LC26(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1228:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1225:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$237, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1231:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC26(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1230:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1200
.L1229:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22554:
	.size	_ZN2v88internalL30Stats_Runtime_HomeObjectSymbolEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL30Stats_Runtime_HomeObjectSymbolEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL32Stats_Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"V8.Runtime_Runtime_LoadKeyedFromSuper"
	.section	.text._ZN2v88internalL32Stats_Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateE.isra.0:
.LFB22599:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -208(%rbp)
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1304
.L1233:
	movq	_ZZN2v88internalL32Stats_Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic732(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1305
.L1235:
	movq	$0, -240(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1306
.L1237:
	addl	$1, 41104(%r12)
	movq	-8(%r13), %rax
	leaq	-8(%r13), %r9
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	testb	$1, %al
	jne	.L1307
.L1241:
	leaq	.LC22(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1305:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1308
.L1236:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic732(%rip)
	jmp	.L1235
	.p2align 4,,10
	.p2align 3
.L1307:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L1241
	movq	-16(%r13), %rax
	leaq	-16(%r13), %r10
	movl	$0, -244(%rbp)
	testb	$1, %al
	jne	.L1309
	sarq	$32, %rax
	js	.L1260
	movl	%eax, -244(%rbp)
	movl	%eax, %r15d
.L1245:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r15d, %r8d
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_114GetSuperHolderEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS1_9SuperModeENS0_11MaybeHandleINS0_4NameEEEj.isra.0
	testq	%rax, %rax
	je	.L1254
	movabsq	$824633720832, %rcx
	movq	%r13, -112(%rbp)
	leaq	-160(%rbp), %r13
	movq	%r13, %rdi
	movq	%rcx, -148(%rbp)
	movl	$3, -160(%rbp)
	movq	%r12, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -96(%rbp)
	movl	%r15d, -88(%rbp)
	movl	$-1, -84(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	je	.L1254
.L1268:
	movq	(%rax), %r13
.L1256:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1271
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1271:
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1310
.L1232:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1311
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1309:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1303
	movsd	7(%rax), %xmm0
	movsd	.LC0(%rip), %xmm2
	addsd	%xmm0, %xmm2
	movq	%xmm2, %rcx
	movq	%xmm2, %rdx
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	jne	.L1303
	movl	%edx, %ecx
	pxor	%xmm1, %xmm1
	movd	%xmm2, %r15d
	movd	%xmm2, -244(%rbp)
	cvtsi2sdq	%rcx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1275
	jne	.L1275
	cmpl	$-1, %edx
	jne	.L1245
.L1303:
	xorl	%edx, %edx
.L1247:
	testb	%dl, %dl
	jne	.L1260
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	jbe	.L1259
	.p2align 4,,10
	.p2align 3
.L1260:
	movq	%r10, %rsi
	movq	%r12, %rdi
	movq	%r9, -264(%rbp)
	call	_ZN2v88internal6Object13ConvertToNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-264(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L1254
.L1259:
	movq	(%r10), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1312
.L1262:
	movq	%r10, %rcx
	movq	%r9, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_113LoadFromSuperEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS4_INS0_4NameEEE
	testq	%rax, %rax
	jne	.L1268
	.p2align 4,,10
	.p2align 3
.L1254:
	movq	312(%r12), %r13
	jmp	.L1256
	.p2align 4,,10
	.p2align 3
.L1306:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1313
.L1238:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1239
	movq	(%rdi), %rax
	call	*8(%rax)
.L1239:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1240
	movq	(%rdi), %rax
	call	*8(%rax)
.L1240:
	leaq	.LC27(%rip), %rax
	movq	%rbx, -232(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%r14, -216(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1312:
	movq	%rax, -160(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L1263
	testb	$2, %al
	jne	.L1262
.L1263:
	leaq	-160(%rbp), %r15
	leaq	-244(%rbp), %rsi
	movq	%r10, -272(%rbp)
	movq	%r15, %rdi
	movq	%r9, -264(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-264(%rbp), %r9
	movq	-272(%rbp), %r10
	testb	%al, %al
	je	.L1262
	movl	-244(%rbp), %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rsi
	movq	%r12, %rdi
	movl	%r8d, -264(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_114GetSuperHolderEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS1_9SuperModeENS0_11MaybeHandleINS0_4NameEEEj.isra.0
	testq	%rax, %rax
	je	.L1254
	movl	-264(%rbp), %r8d
	movq	%r15, %rdi
	movabsq	$824633720832, %rcx
	movl	$3, -160(%rbp)
	movq	%rcx, -148(%rbp)
	movq	%r12, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%r13, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -96(%rbp)
	movl	%r8d, -88(%rbp)
	movl	$-1, -84(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L1268
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1304:
	movq	40960(%rsi), %rax
	movl	$239, %edx
	leaq	-200(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1310:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1232
	.p2align 4,,10
	.p2align 3
.L1313:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC27(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1238
	.p2align 4,,10
	.p2align 3
.L1308:
	leaq	.LC10(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1236
.L1275:
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L1247
.L1311:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22599:
	.size	_ZN2v88internalL32Stats_Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal34Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE:
.LFB18468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1318
	addl	$1, 41104(%rdx)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movl	$180, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L1316
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1316:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1318:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL40Stats_Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18468:
	.size	_ZN2v88internal34Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal40Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal40Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal40Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateE:
.LFB18471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1336
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %r13
	testb	$1, %al
	jne	.L1322
.L1323:
	leaq	.LC18(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1322:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1323
	movq	23(%rax), %rbx
	movq	15(%rbx), %rax
	testb	$1, %al
	jne	.L1337
.L1324:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L1325:
	testb	%al, %al
	je	.L1329
	movq	15(%rbx), %r15
	testb	$1, %r15b
	jne	.L1338
.L1327:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1330
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1331:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$39, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %rbx
	cmpq	41096(%r12), %r13
	je	.L1333
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1333:
	movq	%rbx, %rax
.L1319:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1339
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1330:
	.cfi_restore_state
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L1340
.L1332:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rdx)
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1338:
	movq	-1(%r15), %rax
	cmpw	$136, 11(%rax)
	jne	.L1327
	leaq	-64(%rbp), %rdi
	movq	%r15, -64(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	testb	%al, %al
	jne	.L1341
	.p2align 4,,10
	.p2align 3
.L1329:
	andq	$-262144, %rbx
	movq	24(%rbx), %rax
	movq	-37464(%rax), %r15
	jmp	.L1327
	.p2align 4,,10
	.p2align 3
.L1337:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L1324
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1336:
	call	_ZN2v88internalL46Stats_Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateE
	jmp	.L1319
	.p2align 4,,10
	.p2align 3
.L1340:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1341:
	movq	-72(%rbp), %rdi
	movq	%r15, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	%rax, %r15
	jmp	.L1327
.L1339:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18471:
	.size	_ZN2v88internal40Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal40Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateE:
.LFB18474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1346
	addl	$1, 41104(%rdx)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movl	$162, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L1344
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1344:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1346:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Stats_Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18474:
	.size	_ZN2v88internal33Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal33Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateE:
.LFB18477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1351
	addl	$1, 41104(%rdx)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movl	$178, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L1349
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1349:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1351:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL42Stats_Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18477:
	.size	_ZN2v88internal36Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateE, .-_ZN2v88internal36Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateE:
.LFB18480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1356
	addl	$1, 41104(%rdx)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movl	$290, %esi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L1354
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1354:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1356:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18480:
	.size	_ZN2v88internal27Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateE:
.LFB18484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1363
	addl	$1, 41104(%rdx)
	movq	-8(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L1359
.L1360:
	leaq	.LC12(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1359:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L1360
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_124ThrowNotSuperConstructorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_10JSFunctionEEE
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L1361
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1361:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1363:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL38Stats_Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18484:
	.size	_ZN2v88internal32Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal32Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_HomeObjectSymbolEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_HomeObjectSymbolEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_HomeObjectSymbolEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_HomeObjectSymbolEiPmPNS0_7IsolateE:
.LFB18487:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1366
	movq	3720(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1366:
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL30Stats_Runtime_HomeObjectSymbolEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18487:
	.size	_ZN2v88internal24Runtime_HomeObjectSymbolEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_HomeObjectSymbolEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Runtime_DefineClassEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Runtime_DefineClassEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Runtime_DefineClassEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Runtime_DefineClassEiPmPNS0_7IsolateE:
.LFB18503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1380
	movslq	%edi, %rdi
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	%rdi, -64(%rbp)
	movq	41088(%rdx), %r13
	movq	%rsi, -56(%rbp)
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1370
.L1371:
	leaq	.LC20(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1370:
	movq	-1(%rax), %rax
	cmpw	$123, 11(%rax)
	jne	.L1371
	movq	-8(%rsi), %rax
	testb	$1, %al
	jne	.L1381
.L1372:
	leaq	.LC12(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1381:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L1372
	leaq	-8(%rsi), %rcx
	leaq	-16(%rsi), %rdx
	movq	%r12, %rdi
	leaq	-64(%rbp), %r8
	call	_ZN2v88internal12_GLOBAL__N_111DefineClassEPNS0_7IsolateENS0_6HandleINS0_16ClassBoilerplateEEENS4_INS0_6ObjectEEENS4_INS0_10JSFunctionEEERNS0_9ArgumentsE
	testq	%rax, %rax
	je	.L1382
	movq	(%rax), %r14
.L1375:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1367
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1367:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1383
	addq	$32, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1382:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1380:
	call	_ZN2v88internalL25Stats_Runtime_DefineClassEiPmPNS0_7IsolateE
	movq	%rax, %r14
	jmp	.L1367
.L1383:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18503:
	.size	_ZN2v88internal19Runtime_DefineClassEiPmPNS0_7IsolateE, .-_ZN2v88internal19Runtime_DefineClassEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_LoadFromSuperEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_LoadFromSuperEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_LoadFromSuperEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_LoadFromSuperEiPmPNS0_7IsolateE:
.LFB18515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1395
	addl	$1, 41104(%rdx)
	movq	-8(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L1386
.L1387:
	leaq	.LC22(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1386:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L1387
	movq	-16(%rsi), %rax
	leaq	-16(%rsi), %rcx
	testb	$1, %al
	jne	.L1396
.L1388:
	leaq	.LC23(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1396:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L1388
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_113LoadFromSuperEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS4_INS0_4NameEEE
	testq	%rax, %rax
	je	.L1397
	movq	(%rax), %r14
.L1391:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1384
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1384:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1397:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L1391
	.p2align 4,,10
	.p2align 3
.L1395:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL27Stats_Runtime_LoadFromSuperEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18515:
	.size	_ZN2v88internal21Runtime_LoadFromSuperEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_LoadFromSuperEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateE:
.LFB18518:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1445
	addl	$1, 41104(%rdx)
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r9
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1446
.L1401:
	leaq	.LC22(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1446:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L1401
	movq	-16(%rsi), %rax
	leaq	-16(%rsi), %r10
	movl	$0, -148(%rbp)
	testb	$1, %al
	jne	.L1447
	sarq	$32, %rax
	js	.L1420
	movl	%eax, -148(%rbp)
	movl	%eax, %r15d
.L1405:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r15d, %r8d
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_114GetSuperHolderEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS1_9SuperModeENS0_11MaybeHandleINS0_4NameEEEj.isra.0
	testq	%rax, %rax
	je	.L1414
	movabsq	$824633720832, %rcx
	movq	%r13, -96(%rbp)
	leaq	-144(%rbp), %r13
	movq	%r13, %rdi
	movq	%rcx, -132(%rbp)
	movl	$3, -144(%rbp)
	movq	%r12, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%r15d, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	je	.L1414
.L1428:
	movq	(%rax), %r13
.L1416:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1398
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1398:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1448
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1449:
	.cfi_restore_state
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L1423
	testb	$2, %al
	jne	.L1422
.L1423:
	leaq	-144(%rbp), %r15
	leaq	-148(%rbp), %rsi
	movq	%r10, -176(%rbp)
	movq	%r15, %rdi
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-168(%rbp), %r9
	movq	-176(%rbp), %r10
	testb	%al, %al
	je	.L1422
	movl	-148(%rbp), %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rsi
	movq	%r12, %rdi
	movl	%r8d, -168(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_114GetSuperHolderEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS1_9SuperModeENS0_11MaybeHandleINS0_4NameEEEj.isra.0
	testq	%rax, %rax
	je	.L1414
	movl	-168(%rbp), %r8d
	movq	%r15, %rdi
	movabsq	$824633720832, %rcx
	movl	$3, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	%r12, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%r8d, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L1428
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	312(%r12), %r13
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1447:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1444
	movsd	7(%rax), %xmm0
	movsd	.LC0(%rip), %xmm2
	addsd	%xmm0, %xmm2
	movq	%xmm2, %rcx
	movq	%xmm2, %rdx
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	jne	.L1444
	movl	%edx, %ecx
	pxor	%xmm1, %xmm1
	movd	%xmm2, %r15d
	movd	%xmm2, -148(%rbp)
	cvtsi2sdq	%rcx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1432
	jne	.L1432
	cmpl	$-1, %edx
	jne	.L1405
.L1444:
	xorl	%edx, %edx
.L1407:
	testb	%dl, %dl
	jne	.L1420
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	jbe	.L1419
	.p2align 4,,10
	.p2align 3
.L1420:
	movq	%r10, %rsi
	movq	%r12, %rdi
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal6Object13ConvertToNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-168(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L1414
.L1419:
	movq	(%r10), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1449
.L1422:
	movq	%r10, %rcx
	movq	%r9, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_113LoadFromSuperEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_8JSObjectEEENS4_INS0_4NameEEE
	testq	%rax, %rax
	jne	.L1428
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1445:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL32Stats_Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L1398
.L1432:
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L1407
.L1448:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18518:
	.size	_ZN2v88internal26Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Runtime_StoreToSuperEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Runtime_StoreToSuperEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Runtime_StoreToSuperEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Runtime_StoreToSuperEiPmPNS0_7IsolateE:
.LFB18523:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1461
	addl	$1, 41104(%rdx)
	movq	-8(%r9), %rax
	leaq	-8(%rsi), %rsi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1452
.L1453:
	leaq	.LC22(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1452:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L1453
	movq	-16(%r9), %rax
	leaq	-16(%r9), %rcx
	testb	$1, %al
	jne	.L1462
.L1454:
	leaq	.LC23(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1462:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L1454
	leaq	-24(%r9), %r8
	movq	%r9, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_112StoreToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEENS4_INS0_4NameEEES8_
	testq	%rax, %rax
	je	.L1463
	movq	(%rax), %r14
.L1457:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1450
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1450:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1463:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1461:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL26Stats_Runtime_StoreToSuperEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18523:
	.size	_ZN2v88internal20Runtime_StoreToSuperEiPmPNS0_7IsolateE, .-_ZN2v88internal20Runtime_StoreToSuperEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateE:
.LFB18527:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1473
	addl	$1, 41104(%rdx)
	movq	-8(%r9), %rax
	leaq	-8(%rsi), %rsi
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	testb	$1, %al
	jne	.L1466
.L1467:
	leaq	.LC22(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1466:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L1467
	leaq	-16(%r9), %rcx
	leaq	-24(%r9), %r8
	movq	%r9, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internalL17StoreKeyedToSuperEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS3_INS0_6ObjectEEES7_S7_
	testq	%rax, %rax
	je	.L1474
	movq	(%rax), %r14
.L1469:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1464
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1464:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1474:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1473:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18527:
	.size	_ZN2v88internal25Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal34Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal34Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal34Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE:
.LFB22510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22510:
	.size	_GLOBAL__sub_I__ZN2v88internal34Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal34Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal34Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL31Stats_Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic824,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic824, @object
	.size	_ZZN2v88internalL31Stats_Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic824, 8
_ZZN2v88internalL31Stats_Runtime_StoreKeyedToSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic824:
	.zero	8
	.section	.bss._ZZN2v88internalL26Stats_Runtime_StoreToSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic792,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL26Stats_Runtime_StoreToSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic792, @object
	.size	_ZZN2v88internalL26Stats_Runtime_StoreToSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic792, 8
_ZZN2v88internalL26Stats_Runtime_StoreToSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic792:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic732,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic732, @object
	.size	_ZZN2v88internalL32Stats_Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic732, 8
_ZZN2v88internalL32Stats_Runtime_LoadKeyedFromSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic732:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_LoadFromSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic720,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_LoadFromSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic720, @object
	.size	_ZZN2v88internalL27Stats_Runtime_LoadFromSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic720, 8
_ZZN2v88internalL27Stats_Runtime_LoadFromSuperEiPmPNS0_7IsolateEE28trace_event_unique_atomic720:
	.zero	8
	.section	.bss._ZZN2v88internalL25Stats_Runtime_DefineClassEiPmPNS0_7IsolateEE28trace_event_unique_atomic647,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL25Stats_Runtime_DefineClassEiPmPNS0_7IsolateEE28trace_event_unique_atomic647, @object
	.size	_ZZN2v88internalL25Stats_Runtime_DefineClassEiPmPNS0_7IsolateEE28trace_event_unique_atomic647, 8
_ZZN2v88internalL25Stats_Runtime_DefineClassEiPmPNS0_7IsolateEE28trace_event_unique_atomic647:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_HomeObjectSymbolEiPmPNS0_7IsolateEE28trace_event_unique_atomic108,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_HomeObjectSymbolEiPmPNS0_7IsolateEE28trace_event_unique_atomic108, @object
	.size	_ZZN2v88internalL30Stats_Runtime_HomeObjectSymbolEiPmPNS0_7IsolateEE28trace_event_unique_atomic108, 8
_ZZN2v88internalL30Stats_Runtime_HomeObjectSymbolEiPmPNS0_7IsolateEE28trace_event_unique_atomic108:
	.zero	8
	.section	.bss._ZZN2v88internalL38Stats_Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic100,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Stats_Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic100, @object
	.size	_ZZN2v88internalL38Stats_Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic100, 8
_ZZN2v88internalL38Stats_Runtime_ThrowNotSuperConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic100:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateEE27trace_event_unique_atomic60,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateEE27trace_event_unique_atomic60, @object
	.size	_ZZN2v88internalL33Stats_Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateEE27trace_event_unique_atomic60, 8
_ZZN2v88internalL33Stats_Runtime_ThrowSuperNotCalledEiPmPNS0_7IsolateEE27trace_event_unique_atomic60:
	.zero	8
	.section	.bss._ZZN2v88internalL42Stats_Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic53,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Stats_Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic53, @object
	.size	_ZZN2v88internalL42Stats_Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic53, 8
_ZZN2v88internalL42Stats_Runtime_ThrowSuperAlreadyCalledErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic53:
	.zero	8
	.section	.bss._ZZN2v88internalL39Stats_Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic46,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Stats_Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic46, @object
	.size	_ZZN2v88internalL39Stats_Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic46, 8
_ZZN2v88internalL39Stats_Runtime_ThrowStaticPrototypeErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic46:
	.zero	8
	.section	.bss._ZZN2v88internalL46Stats_Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic36,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL46Stats_Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic36, @object
	.size	_ZZN2v88internalL46Stats_Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic36, 8
_ZZN2v88internalL46Stats_Runtime_ThrowConstructorNonCallableErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic36:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic28,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic28, @object
	.size	_ZZN2v88internalL40Stats_Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic28, 8
_ZZN2v88internalL40Stats_Runtime_ThrowUnsupportedSuperErrorEiPmPNS0_7IsolateEE27trace_event_unique_atomic28:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1127219200
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
