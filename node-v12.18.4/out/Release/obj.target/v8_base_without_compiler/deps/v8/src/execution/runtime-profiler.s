	.file	"runtime-profiler.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB1935:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE1935:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB1936:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1936:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB1938:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE1938:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB18557:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L14
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE18557:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.text._ZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonE
	.type	_ZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonE, @function
_ZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonE:
.LFB18566:
	.cfi_startproc
	endbr64
	movzbl	%dil, %edi
	leaq	_ZZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonEE7reasons(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	ret
	.cfi_endproc
.LFE18566:
	.size	_ZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonE, .-_ZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonE
	.section	.text._ZN2v88internallsERSoNS0_18OptimizationReasonE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoNS0_18OptimizationReasonE
	.type	_ZN2v88internallsERSoNS0_18OptimizationReasonE, @function
_ZN2v88internallsERSoNS0_18OptimizationReasonE:
.LFB18567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%sil, %esi
	leaq	_ZZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonEE7reasons(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	(%rax,%rsi,8), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	testq	%r13, %r13
	je	.L20
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18567:
	.size	_ZN2v88internallsERSoNS0_18OptimizationReasonE, .-_ZN2v88internallsERSoNS0_18OptimizationReasonE
	.section	.text._ZN2v88internal15RuntimeProfilerC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15RuntimeProfilerC2EPNS0_7IsolateE
	.type	_ZN2v88internal15RuntimeProfilerC2EPNS0_7IsolateE, @function
_ZN2v88internal15RuntimeProfilerC2EPNS0_7IsolateE:
.LFB18569:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movb	$0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18569:
	.size	_ZN2v88internal15RuntimeProfilerC2EPNS0_7IsolateE, .-_ZN2v88internal15RuntimeProfilerC2EPNS0_7IsolateE
	.globl	_ZN2v88internal15RuntimeProfilerC1EPNS0_7IsolateE
	.set	_ZN2v88internal15RuntimeProfilerC1EPNS0_7IsolateE,_ZN2v88internal15RuntimeProfilerC2EPNS0_7IsolateE
	.section	.rodata._ZN2v88internal15RuntimeProfiler8OptimizeENS0_10JSFunctionENS0_18OptimizationReasonE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"[marking "
.LC1:
	.string	"optimized"
	.section	.rodata._ZN2v88internal15RuntimeProfiler8OptimizeENS0_10JSFunctionENS0_18OptimizationReasonE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	" for %s recompilation, reason: %s"
	.section	.rodata._ZN2v88internal15RuntimeProfiler8OptimizeENS0_10JSFunctionENS0_18OptimizationReasonE.str1.1
.LC3:
	.string	"]\n"
	.section	.text._ZN2v88internal15RuntimeProfiler8OptimizeENS0_10JSFunctionENS0_18OptimizationReasonE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15RuntimeProfiler8OptimizeENS0_10JSFunctionENS0_18OptimizationReasonE
	.type	_ZN2v88internal15RuntimeProfiler8OptimizeENS0_10JSFunctionENS0_18OptimizationReasonE, @function
_ZN2v88internal15RuntimeProfiler8OptimizeENS0_10JSFunctionENS0_18OptimizationReasonE:
.LFB18572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%dl, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	%rsi, -40(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -24(%rbp)
	xorl	%ecx, %ecx
	leaq	_ZZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonEE7reasons(%rip), %rcx
	cmpb	$0, _ZN2v88internal14FLAG_trace_optE(%rip)
	movq	%rsi, -32(%rbp)
	movq	(%rcx,%rdx,8), %r12
	jne	.L26
.L23:
	leaq	-40(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal10JSFunction19MarkForOptimizationENS0_15ConcurrencyModeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	stdout(%rip), %rsi
	leaq	-32(%rbp), %rdi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	xorl	%eax, %eax
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L23
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18572:
	.size	_ZN2v88internal15RuntimeProfiler8OptimizeENS0_10JSFunctionENS0_18OptimizationReasonE, .-_ZN2v88internal15RuntimeProfiler8OptimizeENS0_10JSFunctionENS0_18OptimizationReasonE
	.section	.rodata._ZN2v88internal15RuntimeProfiler25AttemptOnStackReplacementEPNS0_16InterpretedFrameEi.str1.1,"aMS",@progbits,1
.LC4:
	.string	"[OSR - arming back edges in "
	.section	.text._ZN2v88internal15RuntimeProfiler25AttemptOnStackReplacementEPNS0_16InterpretedFrameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15RuntimeProfiler25AttemptOnStackReplacementEPNS0_16InterpretedFrameEi
	.type	_ZN2v88internal15RuntimeProfiler25AttemptOnStackReplacementEPNS0_16InterpretedFrameEi, @function
_ZN2v88internal15RuntimeProfiler25AttemptOnStackReplacementEPNS0_16InterpretedFrameEi:
.LFB18573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	call	*152(%rax)
	movq	%rax, -56(%rbp)
	movq	23(%rax), %r13
	cmpb	$0, _ZN2v88internal12FLAG_use_osrE(%rip)
	jne	.L40
.L28:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	31(%r13), %rax
	testb	$1, %al
	jne	.L42
.L30:
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	je	.L28
	movl	47(%r13), %eax
	testl	$15728640, %eax
	jne	.L28
	cmpb	$0, _ZN2v88internal14FLAG_trace_osrE(%rip)
	jne	.L43
.L33:
	movq	%r12, %rdi
	call	_ZNK2v88internal16InterpretedFrame16GetBytecodeArrayEv@PLT
	movq	%r12, %rdi
	movsbl	51(%rax), %r13d
	call	_ZNK2v88internal16InterpretedFrame16GetBytecodeArrayEv@PLT
	movl	$6, %edx
	addl	%r13d, %ebx
	cmpl	$6, %ebx
	cmovg	%edx, %ebx
	movb	%bl, 51(%rax)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L42:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L44
.L31:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	je	.L28
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L44:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L31
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L43:
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	stdout(%rip), %rsi
	leaq	-56(%rbp), %rdi
	call	_ZN2v88internal10JSFunction9PrintNameEP8_IO_FILE@PLT
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L33
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18573:
	.size	_ZN2v88internal15RuntimeProfiler25AttemptOnStackReplacementEPNS0_16InterpretedFrameEi, .-_ZN2v88internal15RuntimeProfiler25AttemptOnStackReplacementEPNS0_16InterpretedFrameEi
	.section	.text._ZN2v88internal15RuntimeProfiler8MaybeOSRENS0_10JSFunctionEPNS0_16InterpretedFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15RuntimeProfiler8MaybeOSRENS0_10JSFunctionEPNS0_16InterpretedFrameE
	.type	_ZN2v88internal15RuntimeProfiler8MaybeOSRENS0_10JSFunctionEPNS0_16InterpretedFrameE, @function
_ZN2v88internal15RuntimeProfiler8MaybeOSRENS0_10JSFunctionEPNS0_16InterpretedFrameE:
.LFB18575:
	.cfi_startproc
	endbr64
	movabsq	$287762808832, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	39(%rsi), %rax
	movq	7(%rax), %rax
	movslq	39(%rax), %rax
	movq	23(%rsi), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L47
	testb	$1, %dl
	jne	.L48
.L52:
	movq	39(%rsi), %rdx
	movq	7(%rdx), %rdx
	movq	-1(%rdx), %rcx
	cmpw	$155, 11(%rcx)
	je	.L95
.L47:
	movabsq	$287762808832, %rcx
	movq	23(%rsi), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L61
	testb	$1, %dl
	jne	.L56
.L59:
	movq	39(%rsi), %rdx
	movq	7(%rdx), %rdx
	movq	-1(%rdx), %rcx
	cmpw	$155, 11(%rcx)
	je	.L96
.L61:
	movq	47(%rsi), %rdx
	cmpl	$67, 59(%rdx)
	jne	.L97
.L64:
	movabsq	$287762808832, %rcx
	movq	23(%rsi), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L74
	testb	$1, %dl
	jne	.L70
.L73:
	movq	39(%rsi), %rdx
	movq	7(%rdx), %rdx
	movq	-1(%rdx), %rcx
	cmpw	$155, 11(%rcx)
	je	.L98
.L74:
	xorl	%r12d, %r12d
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movq	15(%rdx), %rdx
	testb	$1, %dl
	jne	.L47
	sarq	$32, %rdx
	cmpq	$2, %rdx
	jne	.L47
.L68:
	movq	23(%rsi), %rdx
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	movq	31(%rdx), %rcx
	addq	$180, %rax
	testb	$1, %cl
	jne	.L99
.L77:
	movq	7(%rdx), %rcx
	testb	$1, %cl
	jne	.L100
.L79:
	movq	7(%rdx), %rdx
	movq	7(%rdx), %rdx
.L78:
	movslq	11(%rdx), %rdx
	movl	$1, %r12d
	cmpq	%rdx, %rax
	jge	.L101
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L47
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L52
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L96:
	movq	15(%rdx), %rdx
	testb	$1, %dl
	jne	.L61
	sarq	$32, %rdx
	cmpq	$3, %rdx
	jne	.L61
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L97:
	movabsq	$287762808832, %rcx
	movq	23(%rsi), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L64
	testb	$1, %dl
	jne	.L102
.L65:
	movq	47(%rsi), %rdx
	testb	$62, 43(%rdx)
	jne	.L64
	movq	47(%rsi), %rdx
	movq	31(%rdx), %rdx
	movl	15(%rdx), %edx
	andl	$1, %edx
	je	.L68
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L98:
	movq	15(%rdx), %rdx
	testb	$1, %dl
	je	.L74
	cmpl	$3, %edx
	je	.L74
	andq	$-3, %rdx
	je	.L74
	movq	31(%rdx), %rdx
	movl	15(%rdx), %edx
	andl	$1, %edx
	je	.L68
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L101:
	movl	$1, %edx
	movq	%r8, %rsi
	call	_ZN2v88internal15RuntimeProfiler25AttemptOnStackReplacementEPNS0_16InterpretedFrameEi
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movq	-1(%rcx), %rsi
	cmpw	$86, 11(%rsi)
	jne	.L77
	movq	39(%rcx), %rsi
	testb	$1, %sil
	je	.L77
	movq	-1(%rsi), %rsi
	cmpw	$72, 11(%rsi)
	jne	.L77
	movq	31(%rcx), %rdx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L100:
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L79
	movq	7(%rdx), %rdx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L56:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L61
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L59
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L70:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L74
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L73
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L102:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L64
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L65
	jmp	.L64
	.cfi_endproc
.LFE18575:
	.size	_ZN2v88internal15RuntimeProfiler8MaybeOSRENS0_10JSFunctionEPNS0_16InterpretedFrameE, .-_ZN2v88internal15RuntimeProfiler8MaybeOSRENS0_10JSFunctionEPNS0_16InterpretedFrameE
	.section	.rodata._ZN2v88internal15RuntimeProfiler14ShouldOptimizeENS0_10JSFunctionENS0_13BytecodeArrayE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"[not yet optimizing "
	.section	.rodata._ZN2v88internal15RuntimeProfiler14ShouldOptimizeENS0_10JSFunctionENS0_13BytecodeArrayE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	", not enough ticks: %d/%d and "
	.section	.rodata._ZN2v88internal15RuntimeProfiler14ShouldOptimizeENS0_10JSFunctionENS0_13BytecodeArrayE.str1.1
.LC7:
	.string	"ICs changed]\n"
	.section	.rodata._ZN2v88internal15RuntimeProfiler14ShouldOptimizeENS0_10JSFunctionENS0_13BytecodeArrayE.str1.8
	.align 8
.LC8:
	.string	" too large for small function optimization: %d/%d]\n"
	.section	.text._ZN2v88internal15RuntimeProfiler14ShouldOptimizeENS0_10JSFunctionENS0_13BytecodeArrayE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15RuntimeProfiler14ShouldOptimizeENS0_10JSFunctionENS0_13BytecodeArrayE
	.type	_ZN2v88internal15RuntimeProfiler14ShouldOptimizeENS0_10JSFunctionENS0_13BytecodeArrayE, @function
_ZN2v88internal15RuntimeProfiler14ShouldOptimizeENS0_10JSFunctionENS0_13BytecodeArrayE:
.LFB18576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$16, %rsp
	movq	%rsi, -40(%rbp)
	movq	39(%rsi), %rax
	movslq	11(%rdx), %rdx
	movq	7(%rax), %rax
	movl	%edx, %ecx
	movl	39(%rax), %r14d
	imulq	$458129845, %rdx, %rax
	sarl	$31, %ecx
	sarq	$39, %rax
	subl	%ecx, %eax
	addl	$2, %eax
	cmpl	%r14d, %eax
	jle	.L103
	cmpb	$1, 8(%rdi)
	movq	%rdi, %r12
	je	.L110
	cmpq	$89, %rdx
	jle	.L108
.L110:
	xorl	%r13d, %r13d
	cmpb	$0, _ZN2v88internal22FLAG_trace_opt_verboseE(%rip)
	jne	.L115
.L103:
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	xorl	%eax, %eax
	leaq	.LC5(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	stdout(%rip), %rsi
	leaq	-40(%rbp), %rdi
	call	_ZN2v88internal10JSFunction9PrintNameEP8_IO_FILE@PLT
	xorl	%eax, %eax
	movl	$2, %edx
	movl	%r14d, %esi
	leaq	.LC6(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, 8(%r12)
	je	.L106
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L108:
	movl	$2, %r13d
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L106:
	movslq	11(%rbx), %rsi
	movl	$90, %edx
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L103
	.cfi_endproc
.LFE18576:
	.size	_ZN2v88internal15RuntimeProfiler14ShouldOptimizeENS0_10JSFunctionENS0_13BytecodeArrayE, .-_ZN2v88internal15RuntimeProfiler14ShouldOptimizeENS0_10JSFunctionENS0_13BytecodeArrayE
	.section	.rodata._ZN2v88internal15RuntimeProfiler13MaybeOptimizeENS0_10JSFunctionEPNS0_16InterpretedFrameE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"[function "
	.section	.rodata._ZN2v88internal15RuntimeProfiler13MaybeOptimizeENS0_10JSFunctionEPNS0_16InterpretedFrameE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	" is already in optimization queue]\n"
	.align 8
.LC11:
	.string	" has been marked manually for optimization]\n"
	.section	.text._ZN2v88internal15RuntimeProfiler13MaybeOptimizeENS0_10JSFunctionEPNS0_16InterpretedFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15RuntimeProfiler13MaybeOptimizeENS0_10JSFunctionEPNS0_16InterpretedFrameE
	.type	_ZN2v88internal15RuntimeProfiler13MaybeOptimizeENS0_10JSFunctionEPNS0_16InterpretedFrameE, @function
_ZN2v88internal15RuntimeProfiler13MaybeOptimizeENS0_10JSFunctionEPNS0_16InterpretedFrameE:
.LFB18574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	movabsq	$287762808832, %rdx
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%rsi, -56(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -24(%rbp)
	xorl	%ecx, %ecx
	movq	23(%rsi), %rax
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L118
	testb	$1, %al
	jne	.L119
.L123:
	movq	-56(%rbp), %rax
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	je	.L140
.L118:
	cmpb	$0, _ZN2v88internal27FLAG_testing_d8_test_runnerE(%rip)
	jne	.L141
.L127:
	cmpb	$0, _ZN2v88internal15FLAG_always_osrE(%rip)
	je	.L129
	movl	$6, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15RuntimeProfiler25AttemptOnStackReplacementEPNS0_16InterpretedFrameEi
.L130:
	movq	-56(%rbp), %rax
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andl	$15728640, %edx
	jne	.L116
	movq	23(%rax), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L142
.L131:
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L143
.L133:
	movq	7(%rax), %rax
	movq	7(%rax), %rdx
.L132:
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15RuntimeProfiler14ShouldOptimizeENS0_10JSFunctionENS0_13BytecodeArrayE
	testb	%al, %al
	jne	.L144
	.p2align 4,,10
	.p2align 3
.L116:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L118
	sarq	$32, %rax
	cmpq	$4, %rax
	jne	.L118
	cmpb	$0, _ZN2v88internal22FLAG_trace_opt_verboseE(%rip)
	je	.L116
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	stdout(%rip), %rsi
	leaq	-56(%rbp), %rdi
	call	_ZN2v88internal10JSFunction9PrintNameEP8_IO_FILE@PLT
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L129:
	movq	-56(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal15RuntimeProfiler8MaybeOSRENS0_10JSFunctionEPNS0_16InterpretedFrameE
	testb	%al, %al
	je	.L130
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L119:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L118
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L123
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L141:
	movq	-56(%rbp), %rsi
	movq	(%r12), %rdi
	call	_ZN2v88internal24PendingOptimizationTable30IsHeuristicOptimizationAllowedEPNS0_7IsolateENS0_10JSFunctionE@PLT
	testb	%al, %al
	jne	.L127
	cmpb	$0, _ZN2v88internal22FLAG_trace_opt_verboseE(%rip)
	je	.L116
	leaq	.LC9(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	stdout(%rip), %rsi
	leaq	-56(%rbp), %rdi
	call	_ZN2v88internal10JSFunction9PrintNameEP8_IO_FILE@PLT
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L142:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L131
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L131
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L131
	movq	31(%rdx), %rdx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L144:
	movq	-56(%rbp), %rdx
	movzbl	%al, %eax
	leaq	_ZZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonEE7reasons(%rip), %rcx
	cmpb	$0, _ZN2v88internal14FLAG_trace_optE(%rip)
	movq	(%rcx,%rax,8), %r12
	movq	%rdx, -40(%rbp)
	movq	%rdx, -32(%rbp)
	jne	.L146
.L134:
	leaq	-40(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal10JSFunction19MarkForOptimizationENS0_15ConcurrencyModeE@PLT
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L143:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L133
	movq	7(%rax), %rdx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L146:
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	stdout(%rip), %rsi
	leaq	-32(%rbp), %rdi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	xorl	%eax, %eax
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L134
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18574:
	.size	_ZN2v88internal15RuntimeProfiler13MaybeOptimizeENS0_10JSFunctionEPNS0_16InterpretedFrameE, .-_ZN2v88internal15RuntimeProfiler13MaybeOptimizeENS0_10JSFunctionEPNS0_16InterpretedFrameE
	.section	.rodata._ZN2v88internal15RuntimeProfiler29MarkCandidatesForOptimizationEv.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"disabled-by-default-v8.compile"
	.align 8
.LC13:
	.string	"V8.MarkCandidatesForOptimization"
	.section	.text._ZN2v88internal15RuntimeProfiler29MarkCandidatesForOptimizationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15RuntimeProfiler29MarkCandidatesForOptimizationEv
	.type	_ZN2v88internal15RuntimeProfiler29MarkCandidatesForOptimizationEv, @function
_ZN2v88internal15RuntimeProfiler29MarkCandidatesForOptimizationEv:
.LFB18577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1544, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%r12)
	movq	41088(%r12), %rax
	movq	(%rdi), %rdi
	movq	%rax, -1576(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -1568(%rbp)
	call	_ZN2v88internal7Isolate13use_optimizerEv@PLT
	testb	%al, %al
	je	.L194
	movq	_ZZN2v88internal15RuntimeProfiler29MarkCandidatesForOptimizationEvE28trace_event_unique_atomic207(%rip), %r13
	testq	%r13, %r13
	je	.L195
.L152:
	movq	$0, -1552(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L196
.L154:
	movq	(%r15), %rsi
	leaq	-1520(%rbp), %r14
	movl	_ZN2v88internal16FLAG_frame_countE(%rip), %r13d
	movq	%r14, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -104(%rbp)
	je	.L158
	movq	%r14, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
.L158:
	testl	%r13d, %r13d
	jle	.L159
	leal	1(%r13), %eax
	movl	$1, %ebx
	movl	%eax, -1556(%rbp)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L162:
	movq	7(%rcx), %rcx
	testb	$1, %cl
	jne	.L197
.L167:
	movq	%r14, %rdi
	addl	$1, %ebx
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	cmpl	%ebx, -1556(%rbp)
	je	.L159
.L173:
	movq	-104(%rbp), %r13
	testq	%r13, %r13
	je	.L159
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	cmpl	$12, %eax
	jne	.L167
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*152(%rax)
	movq	23(%rax), %rcx
	movq	7(%rcx), %rsi
	testb	$1, %sil
	je	.L162
	movq	-1(%rsi), %rsi
	cmpw	$72, 11(%rsi)
	jne	.L162
.L166:
	movabsq	$287762808832, %rdx
	movq	23(%rax), %rcx
	movq	7(%rcx), %rcx
	cmpq	%rdx, %rcx
	je	.L167
	testb	$1, %cl
	jne	.L168
.L171:
	movq	39(%rax), %rcx
	movq	7(%rcx), %rcx
	movq	-1(%rcx), %rcx
	cmpw	$155, 11(%rcx)
	jne	.L167
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%rax, -1584(%rbp)
	call	_ZN2v88internal15RuntimeProfiler13MaybeOptimizeENS0_10JSFunctionEPNS0_16InterpretedFrameE
	movq	-1584(%rbp), %rax
	movq	39(%rax), %rax
	movq	7(%rax), %rdx
	movl	39(%rdx), %eax
	cmpl	$2147483647, %eax
	je	.L167
	addl	$1, %eax
	movl	%eax, 39(%rdx)
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L159:
	movb	$0, 8(%r15)
	leaq	-1552(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
.L194:
	subl	$1, 41104(%r12)
	movq	-1576(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-1568(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L147
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L147:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L198
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	movq	-1(%rcx), %rcx
	cmpw	$91, 11(%rcx)
	jne	.L167
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L196:
	pxor	%xmm0, %xmm0
	xorl	%ebx, %ebx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L199
.L155:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L156
	movq	(%rdi), %rax
	call	*8(%rax)
.L156:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L157
	movq	(%rdi), %rax
	call	*8(%rax)
.L157:
	leaq	.LC13(%rip), %rax
	movq	%r13, -1544(%rbp)
	movq	%rax, -1536(%rbp)
	leaq	-1544(%rbp), %rax
	movq	%rbx, -1528(%rbp)
	movq	%rax, -1552(%rbp)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L195:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L200
.L153:
	movq	%r13, _ZZN2v88internal15RuntimeProfiler29MarkCandidatesForOptimizationEvE28trace_event_unique_atomic207(%rip)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L168:
	movq	-1(%rcx), %rsi
	cmpw	$165, 11(%rsi)
	je	.L167
	movq	-1(%rcx), %rcx
	cmpw	$166, 11(%rcx)
	jne	.L171
	jmp	.L167
.L200:
	leaq	.LC12(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L153
.L199:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %rbx
	addq	$64, %rsp
	jmp	.L155
.L198:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18577:
	.size	_ZN2v88internal15RuntimeProfiler29MarkCandidatesForOptimizationEv, .-_ZN2v88internal15RuntimeProfiler29MarkCandidatesForOptimizationEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonE, @function
_GLOBAL__sub_I__ZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonE:
.LFB22510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22510:
	.size	_GLOBAL__sub_I__ZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonE, .-_GLOBAL__sub_I__ZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonE
	.section	.bss._ZZN2v88internal15RuntimeProfiler29MarkCandidatesForOptimizationEvE28trace_event_unique_atomic207,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal15RuntimeProfiler29MarkCandidatesForOptimizationEvE28trace_event_unique_atomic207, @object
	.size	_ZZN2v88internal15RuntimeProfiler29MarkCandidatesForOptimizationEvE28trace_event_unique_atomic207, 8
_ZZN2v88internal15RuntimeProfiler29MarkCandidatesForOptimizationEvE28trace_event_unique_atomic207:
	.zero	8
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC14:
	.string	"do not optimize"
.LC15:
	.string	"hot and stable"
.LC16:
	.string	"small function"
	.section	.data.rel.ro.local._ZZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonEE7reasons,"aw"
	.align 16
	.type	_ZZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonEE7reasons, @object
	.size	_ZZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonEE7reasons, 24
_ZZN2v88internal26OptimizationReasonToStringENS0_18OptimizationReasonEE7reasons:
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
