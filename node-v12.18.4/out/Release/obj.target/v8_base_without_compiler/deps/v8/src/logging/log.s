	.file	"log.cc"
	.text
	.section	.text._ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv,"axG",@progbits,_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv
	.type	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv, @function
_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv:
.LFB7101:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7101:
	.size	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv, .-_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv
	.section	.text._ZN2v88internal6Logger22NativeContextMoveEventEmm,"axG",@progbits,_ZN2v88internal6Logger22NativeContextMoveEventEmm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6Logger22NativeContextMoveEventEmm
	.type	_ZN2v88internal6Logger22NativeContextMoveEventEmm, @function
_ZN2v88internal6Logger22NativeContextMoveEventEmm:
.LFB7136:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7136:
	.size	_ZN2v88internal6Logger22NativeContextMoveEventEmm, .-_ZN2v88internal6Logger22NativeContextMoveEventEmm
	.section	.text._ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci,"axG",@progbits,_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci,comdat
	.p2align 4
	.weak	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci
	.type	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci, @function
_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci:
.LFB7137:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7137:
	.size	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci, .-_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci
	.section	.text._ZN2v88internal6Logger27is_listening_to_code_eventsEv,"axG",@progbits,_ZN2v88internal6Logger27is_listening_to_code_eventsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.type	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, @function
_ZN2v88internal6Logger27is_listening_to_code_eventsEv:
.LFB7138:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L6
	movzbl	40(%rdi), %eax
	testb	%al, %al
	je	.L6
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	cmpq	$0, 80(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE7138:
	.size	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, .-_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.section	.text._ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm,"axG",@progbits,_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm
	.type	_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm, @function
_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm:
.LFB7159:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7159:
	.size	_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm, .-_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm
	.section	.text._ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm,"axG",@progbits,_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm
	.type	_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm, @function
_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm:
.LFB7160:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7160:
	.size	_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm, .-_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm
	.section	.text._ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm,"axG",@progbits,_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm
	.type	_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm, @function
_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm:
.LFB7161:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7161:
	.size	_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm, .-_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm
	.section	.text._ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm,"axG",@progbits,_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm
	.type	_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm, @function
_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm:
.LFB7162:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7162:
	.size	_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm, .-_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm
	.section	.text._ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm,"axG",@progbits,_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm
	.type	_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm, @function
_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm:
.LFB7163:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7163:
	.size	_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm, .-_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm
	.section	.text._ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv,"axG",@progbits,_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv
	.type	_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv, @function
_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv:
.LFB7164:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7164:
	.size	_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv, .-_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv
	.section	.text._ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi,"axG",@progbits,_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.type	_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi, @function
_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi:
.LFB7165:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7165:
	.size	_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi, .-_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.section	.text._ZN2v88internal25ExternalCodeEventListener13CallbackEventENS0_4NameEm,"axG",@progbits,_ZN2v88internal25ExternalCodeEventListener13CallbackEventENS0_4NameEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25ExternalCodeEventListener13CallbackEventENS0_4NameEm
	.type	_ZN2v88internal25ExternalCodeEventListener13CallbackEventENS0_4NameEm, @function
_ZN2v88internal25ExternalCodeEventListener13CallbackEventENS0_4NameEm:
.LFB7166:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7166:
	.size	_ZN2v88internal25ExternalCodeEventListener13CallbackEventENS0_4NameEm, .-_ZN2v88internal25ExternalCodeEventListener13CallbackEventENS0_4NameEm
	.section	.text._ZN2v88internal25ExternalCodeEventListener19GetterCallbackEventENS0_4NameEm,"axG",@progbits,_ZN2v88internal25ExternalCodeEventListener19GetterCallbackEventENS0_4NameEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25ExternalCodeEventListener19GetterCallbackEventENS0_4NameEm
	.type	_ZN2v88internal25ExternalCodeEventListener19GetterCallbackEventENS0_4NameEm, @function
_ZN2v88internal25ExternalCodeEventListener19GetterCallbackEventENS0_4NameEm:
.LFB7167:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7167:
	.size	_ZN2v88internal25ExternalCodeEventListener19GetterCallbackEventENS0_4NameEm, .-_ZN2v88internal25ExternalCodeEventListener19GetterCallbackEventENS0_4NameEm
	.section	.text._ZN2v88internal25ExternalCodeEventListener19SetterCallbackEventENS0_4NameEm,"axG",@progbits,_ZN2v88internal25ExternalCodeEventListener19SetterCallbackEventENS0_4NameEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25ExternalCodeEventListener19SetterCallbackEventENS0_4NameEm
	.type	_ZN2v88internal25ExternalCodeEventListener19SetterCallbackEventENS0_4NameEm, @function
_ZN2v88internal25ExternalCodeEventListener19SetterCallbackEventENS0_4NameEm:
.LFB7168:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7168:
	.size	_ZN2v88internal25ExternalCodeEventListener19SetterCallbackEventENS0_4NameEm, .-_ZN2v88internal25ExternalCodeEventListener19SetterCallbackEventENS0_4NameEm
	.section	.text._ZN2v88internal25ExternalCodeEventListener27SharedFunctionInfoMoveEventEmm,"axG",@progbits,_ZN2v88internal25ExternalCodeEventListener27SharedFunctionInfoMoveEventEmm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25ExternalCodeEventListener27SharedFunctionInfoMoveEventEmm
	.type	_ZN2v88internal25ExternalCodeEventListener27SharedFunctionInfoMoveEventEmm, @function
_ZN2v88internal25ExternalCodeEventListener27SharedFunctionInfoMoveEventEmm:
.LFB7169:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7169:
	.size	_ZN2v88internal25ExternalCodeEventListener27SharedFunctionInfoMoveEventEmm, .-_ZN2v88internal25ExternalCodeEventListener27SharedFunctionInfoMoveEventEmm
	.section	.text._ZN2v88internal25ExternalCodeEventListener22NativeContextMoveEventEmm,"axG",@progbits,_ZN2v88internal25ExternalCodeEventListener22NativeContextMoveEventEmm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25ExternalCodeEventListener22NativeContextMoveEventEmm
	.type	_ZN2v88internal25ExternalCodeEventListener22NativeContextMoveEventEmm, @function
_ZN2v88internal25ExternalCodeEventListener22NativeContextMoveEventEmm:
.LFB7170:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7170:
	.size	_ZN2v88internal25ExternalCodeEventListener22NativeContextMoveEventEmm, .-_ZN2v88internal25ExternalCodeEventListener22NativeContextMoveEventEmm
	.section	.text._ZN2v88internal25ExternalCodeEventListener13CodeMoveEventENS0_12AbstractCodeES2_,"axG",@progbits,_ZN2v88internal25ExternalCodeEventListener13CodeMoveEventENS0_12AbstractCodeES2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25ExternalCodeEventListener13CodeMoveEventENS0_12AbstractCodeES2_
	.type	_ZN2v88internal25ExternalCodeEventListener13CodeMoveEventENS0_12AbstractCodeES2_, @function
_ZN2v88internal25ExternalCodeEventListener13CodeMoveEventENS0_12AbstractCodeES2_:
.LFB7171:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7171:
	.size	_ZN2v88internal25ExternalCodeEventListener13CodeMoveEventENS0_12AbstractCodeES2_, .-_ZN2v88internal25ExternalCodeEventListener13CodeMoveEventENS0_12AbstractCodeES2_
	.section	.text._ZN2v88internal25ExternalCodeEventListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE,"axG",@progbits,_ZN2v88internal25ExternalCodeEventListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25ExternalCodeEventListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.type	_ZN2v88internal25ExternalCodeEventListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE, @function
_ZN2v88internal25ExternalCodeEventListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE:
.LFB7172:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7172:
	.size	_ZN2v88internal25ExternalCodeEventListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE, .-_ZN2v88internal25ExternalCodeEventListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.section	.text._ZN2v88internal25ExternalCodeEventListener17CodeMovingGCEventEv,"axG",@progbits,_ZN2v88internal25ExternalCodeEventListener17CodeMovingGCEventEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25ExternalCodeEventListener17CodeMovingGCEventEv
	.type	_ZN2v88internal25ExternalCodeEventListener17CodeMovingGCEventEv, @function
_ZN2v88internal25ExternalCodeEventListener17CodeMovingGCEventEv:
.LFB7173:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7173:
	.size	_ZN2v88internal25ExternalCodeEventListener17CodeMovingGCEventEv, .-_ZN2v88internal25ExternalCodeEventListener17CodeMovingGCEventEv
	.section	.text._ZN2v88internal25ExternalCodeEventListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi,"axG",@progbits,_ZN2v88internal25ExternalCodeEventListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25ExternalCodeEventListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.type	_ZN2v88internal25ExternalCodeEventListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi, @function
_ZN2v88internal25ExternalCodeEventListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi:
.LFB7174:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7174:
	.size	_ZN2v88internal25ExternalCodeEventListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi, .-_ZN2v88internal25ExternalCodeEventListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.section	.text._ZN2v88internal25ExternalCodeEventListener27is_listening_to_code_eventsEv,"axG",@progbits,_ZN2v88internal25ExternalCodeEventListener27is_listening_to_code_eventsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25ExternalCodeEventListener27is_listening_to_code_eventsEv
	.type	_ZN2v88internal25ExternalCodeEventListener27is_listening_to_code_eventsEv, @function
_ZN2v88internal25ExternalCodeEventListener27is_listening_to_code_eventsEv:
.LFB7175:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7175:
	.size	_ZN2v88internal25ExternalCodeEventListener27is_listening_to_code_eventsEv, .-_ZN2v88internal25ExternalCodeEventListener27is_listening_to_code_eventsEv
	.section	.text._ZN2v88internal15PerfBasicLogger13CodeMoveEventENS0_12AbstractCodeES2_,"axG",@progbits,_ZN2v88internal15PerfBasicLogger13CodeMoveEventENS0_12AbstractCodeES2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15PerfBasicLogger13CodeMoveEventENS0_12AbstractCodeES2_
	.type	_ZN2v88internal15PerfBasicLogger13CodeMoveEventENS0_12AbstractCodeES2_, @function
_ZN2v88internal15PerfBasicLogger13CodeMoveEventENS0_12AbstractCodeES2_:
.LFB20590:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE20590:
	.size	_ZN2v88internal15PerfBasicLogger13CodeMoveEventENS0_12AbstractCodeES2_, .-_ZN2v88internal15PerfBasicLogger13CodeMoveEventENS0_12AbstractCodeES2_
	.section	.text._ZN2v88internal15PerfBasicLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE,"axG",@progbits,_ZN2v88internal15PerfBasicLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15PerfBasicLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.type	_ZN2v88internal15PerfBasicLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE, @function
_ZN2v88internal15PerfBasicLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE:
.LFB20591:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE20591:
	.size	_ZN2v88internal15PerfBasicLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE, .-_ZN2v88internal15PerfBasicLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.section	.text._ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.type	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE, @function
_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE:
.LFB20620:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE20620:
	.size	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE, .-_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.section	.text._ZN2v88internal14LowLevelLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE,"axG",@progbits,_ZN2v88internal14LowLevelLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LowLevelLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.type	_ZN2v88internal14LowLevelLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE, @function
_ZN2v88internal14LowLevelLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE:
.LFB20622:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE20622:
	.size	_ZN2v88internal14LowLevelLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE, .-_ZN2v88internal14LowLevelLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.section	.text._ZN2v88internal9JitLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE,"axG",@progbits,_ZN2v88internal9JitLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9JitLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.type	_ZN2v88internal9JitLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE, @function
_ZN2v88internal9JitLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE:
.LFB20637:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE20637:
	.size	_ZN2v88internal9JitLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE, .-_ZN2v88internal9JitLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.section	.text._ZN2v88internal15PerfBasicLoggerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15PerfBasicLoggerD2Ev
	.type	_ZN2v88internal15PerfBasicLoggerD2Ev, @function
_ZN2v88internal15PerfBasicLoggerD2Ev:
.LFB20597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal15PerfBasicLoggerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	call	fclose@PLT
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rax
	movq	$0, 24(%rbx)
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L30
	addq	$8, %rsp
	movl	$516, %esi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20597:
	.size	_ZN2v88internal15PerfBasicLoggerD2Ev, .-_ZN2v88internal15PerfBasicLoggerD2Ev
	.globl	_ZN2v88internal15PerfBasicLoggerD1Ev
	.set	_ZN2v88internal15PerfBasicLoggerD1Ev,_ZN2v88internal15PerfBasicLoggerD2Ev
	.section	.text._ZN2v88internal14LowLevelLoggerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LowLevelLoggerD2Ev
	.type	_ZN2v88internal14LowLevelLoggerD2Ev, @function
_ZN2v88internal14LowLevelLoggerD2Ev:
.LFB20628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14LowLevelLoggerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	call	fclose@PLT
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rax
	movq	$0, 24(%rbx)
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L33
	addq	$8, %rsp
	movl	$516, %esi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20628:
	.size	_ZN2v88internal14LowLevelLoggerD2Ev, .-_ZN2v88internal14LowLevelLoggerD2Ev
	.globl	_ZN2v88internal14LowLevelLoggerD1Ev
	.set	_ZN2v88internal14LowLevelLoggerD1Ev,_ZN2v88internal14LowLevelLoggerD2Ev
	.section	.rodata._ZN2v88internal15PerfBasicLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%lx %x %.*s\n"
	.section	.text._ZN2v88internal15PerfBasicLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15PerfBasicLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci
	.type	_ZN2v88internal15PerfBasicLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci, @function
_ZN2v88internal15PerfBasicLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci:
.LFB20602:
	.cfi_startproc
	endbr64
	movq	%rdx, %r9
	movl	%ecx, %r8d
	movq	(%rsi), %rdx
	movl	8(%rsi), %ecx
	movq	24(%rdi), %rdi
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	jmp	_ZN2v84base2OS6FPrintEP8_IO_FILEPKcz@PLT
	.cfi_endproc
.LFE20602:
	.size	_ZN2v88internal15PerfBasicLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci, .-_ZN2v88internal15PerfBasicLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci
	.section	.text._ZN2v88internal9JitLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9JitLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci
	.type	_ZN2v88internal9JitLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci, @function
_ZN2v88internal9JitLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci:
.LFB20645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	pxor	%xmm0, %xmm0
	movslq	%ecx, %rcx
	movabsq	$4294967296, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	(%rsi), %r8
	movslq	8(%rsi), %rsi
	movq	%fs:40, %rdi
	movq	%rdi, -8(%rbp)
	xorl	%edi, %edi
	movq	%rdx, -40(%rbp)
	movq	8(%rax), %rdx
	leaq	-80(%rbp), %rdi
	movq	$0, -24(%rbp)
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%rdx, -16(%rbp)
	movups	%xmm0, -56(%rbp)
	call	*24(%rax)
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L40:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20645:
	.size	_ZN2v88internal9JitLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci, .-_ZN2v88internal9JitLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci
	.section	.text._ZN2v88internalL18AddFunctionAndCodeENS0_18SharedFunctionInfoENS0_12AbstractCodeEPNS0_6HandleIS1_EEPNS3_IS2_EEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL18AddFunctionAndCodeENS0_18SharedFunctionInfoENS0_12AbstractCodeEPNS0_6HandleIS1_EEPNS3_IS2_EEi, @function
_ZN2v88internalL18AddFunctionAndCodeENS0_18SharedFunctionInfoENS0_12AbstractCodeEPNS0_6HandleIS1_EEPNS3_IS2_EEi:
.LFB20765:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movslq	%r8d, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	testq	%rdx, %rdx
	je	.L42
	movq	%rdi, %rax
	movq	%rdx, %r12
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	movq	3520(%rdx), %rdi
	subq	$37592, %rdx
	testq	%rdi, %rdi
	je	.L43
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L44:
	movslq	%r13d, %rdx
	movq	%rax, (%r12,%rdx,8)
.L42:
	testq	%r14, %r14
	je	.L41
	andq	$-262144, %rbx
	movq	24(%rbx), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L47
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L48:
	movq	%rax, (%r14,%r13,8)
.L41:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L57
.L45:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rbx, (%rax)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L47:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L58
.L49:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r15, (%rax)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rdx
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L49
	.cfi_endproc
.LFE20765:
	.size	_ZN2v88internalL18AddFunctionAndCodeENS0_18SharedFunctionInfoENS0_12AbstractCodeEPNS0_6HandleIS1_EEPNS3_IS2_EEi, .-_ZN2v88internalL18AddFunctionAndCodeENS0_18SharedFunctionInfoENS0_12AbstractCodeEPNS0_6HandleIS1_EEPNS3_IS2_EEi
	.section	.text._ZN2v88internal14LowLevelLogger17CodeMovingGCEventEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LowLevelLogger17CodeMovingGCEventEv
	.type	_ZN2v88internal14LowLevelLogger17CodeMovingGCEventEv, @function
_ZN2v88internal14LowLevelLogger17CodeMovingGCEventEv:
.LFB20636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	24(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-9(%rbp), %rdi
	movb	$71, -9(%rbp)
	call	fwrite@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L62
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L62:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20636:
	.size	_ZN2v88internal14LowLevelLogger17CodeMovingGCEventEv, .-_ZN2v88internal14LowLevelLogger17CodeMovingGCEventEv
	.section	.text._ZN2v88internal9JitLoggerD2Ev,"axG",@progbits,_ZN2v88internal9JitLoggerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9JitLoggerD2Ev
	.type	_ZN2v88internal9JitLoggerD2Ev, @function
_ZN2v88internal9JitLoggerD2Ev:
.LFB24633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9JitLoggerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L63
	addq	$8, %rsp
	movl	$516, %esi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24633:
	.size	_ZN2v88internal9JitLoggerD2Ev, .-_ZN2v88internal9JitLoggerD2Ev
	.weak	_ZN2v88internal9JitLoggerD1Ev
	.set	_ZN2v88internal9JitLoggerD1Ev,_ZN2v88internal9JitLoggerD2Ev
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD1Ev
	.type	_ZN2v88internal8OFStreamD1Ev, @function
_ZN2v88internal8OFStreamD1Ev:
.LFB20395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE20395:
	.size	_ZN2v88internal8OFStreamD1Ev, .-_ZN2v88internal8OFStreamD1Ev
	.section	.text._ZN2v88internal8ProfilerD2Ev,"axG",@progbits,_ZN2v88internal8ProfilerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8ProfilerD2Ev
	.type	_ZN2v88internal8ProfilerD2Ev, @function
_ZN2v88internal8ProfilerD2Ev:
.LFB24617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8ProfilerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	529480(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -529480(%rdi)
	call	_ZN2v84base9SemaphoreD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base6ThreadD2Ev@PLT
	.cfi_endproc
.LFE24617:
	.size	_ZN2v88internal8ProfilerD2Ev, .-_ZN2v88internal8ProfilerD2Ev
	.weak	_ZN2v88internal8ProfilerD1Ev
	.set	_ZN2v88internal8ProfilerD1Ev,_ZN2v88internal8ProfilerD2Ev
	.section	.text._ZN2v88internal8ProfilerD0Ev,"axG",@progbits,_ZN2v88internal8ProfilerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8ProfilerD0Ev
	.type	_ZN2v88internal8ProfilerD0Ev, @function
_ZN2v88internal8ProfilerD0Ev:
.LFB24619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8ProfilerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	529480(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -529480(%rdi)
	call	_ZN2v84base9SemaphoreD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v84base6ThreadD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$529520, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24619:
	.size	_ZN2v88internal8ProfilerD0Ev, .-_ZN2v88internal8ProfilerD0Ev
	.section	.text._ZN2v88internal14SamplingThreadD2Ev,"axG",@progbits,_ZN2v88internal14SamplingThreadD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14SamplingThreadD2Ev
	.type	_ZN2v88internal14SamplingThreadD2Ev, @function
_ZN2v88internal14SamplingThreadD2Ev:
.LFB24583:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14SamplingThreadE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v84base6ThreadD2Ev@PLT
	.cfi_endproc
.LFE24583:
	.size	_ZN2v88internal14SamplingThreadD2Ev, .-_ZN2v88internal14SamplingThreadD2Ev
	.weak	_ZN2v88internal14SamplingThreadD1Ev
	.set	_ZN2v88internal14SamplingThreadD1Ev,_ZN2v88internal14SamplingThreadD2Ev
	.section	.text._ZN2v88internal14SamplingThreadD0Ev,"axG",@progbits,_ZN2v88internal14SamplingThreadD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14SamplingThreadD0Ev
	.type	_ZN2v88internal14SamplingThreadD0Ev, @function
_ZN2v88internal14SamplingThreadD0Ev:
.LFB24585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14SamplingThreadE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v84base6ThreadD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24585:
	.size	_ZN2v88internal14SamplingThreadD0Ev, .-_ZN2v88internal14SamplingThreadD0Ev
	.section	.text._ZN2v88internal6TickerD2Ev,"axG",@progbits,_ZN2v88internal6TickerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6TickerD2Ev
	.type	_ZN2v88internal6TickerD2Ev, @function
_ZN2v88internal6TickerD2Ev:
.LFB20663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6TickerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movzbl	32(%rdi), %eax
	testb	%al, %al
	jne	.L85
.L76:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L77
	movq	(%rdi), %rax
	call	*8(%rax)
.L77:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87sampler7SamplerD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	call	_ZN2v87sampler7Sampler4StopEv@PLT
	jmp	.L76
	.cfi_endproc
.LFE20663:
	.size	_ZN2v88internal6TickerD2Ev, .-_ZN2v88internal6TickerD2Ev
	.weak	_ZN2v88internal6TickerD1Ev
	.set	_ZN2v88internal6TickerD1Ev,_ZN2v88internal6TickerD2Ev
	.section	.text._ZN2v88internal14SamplingThread3RunEv,"axG",@progbits,_ZN2v88internal14SamplingThread3RunEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14SamplingThread3RunEv
	.type	_ZN2v88internal14SamplingThread3RunEv, @function
_ZN2v88internal14SamplingThread3RunEv:
.LFB20653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L90:
	call	_ZN2v87sampler7Sampler8DoSampleEv@PLT
	movslq	56(%rbx), %rdi
	call	_ZN2v84base2OS5SleepENS0_9TimeDeltaE@PLT
.L88:
	movq	48(%rbx), %rdi
	movzbl	32(%rdi), %eax
	testb	%al, %al
	jne	.L90
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20653:
	.size	_ZN2v88internal14SamplingThread3RunEv, .-_ZN2v88internal14SamplingThread3RunEv
	.section	.rodata._ZN2v88internal12_GLOBAL__N_122AppendCodeCreateHeaderERNS0_3Log14MessageBuilderENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCode4KindEPhiPNS_4base12ElapsedTimerE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"code-creation"
	.section	.text._ZN2v88internal12_GLOBAL__N_122AppendCodeCreateHeaderERNS0_3Log14MessageBuilderENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCode4KindEPhiPNS_4base12ElapsedTimerE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_122AppendCodeCreateHeaderERNS0_3Log14MessageBuilderENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCode4KindEPhiPNS_4base12ElapsedTimerE, @function
_ZN2v88internal12_GLOBAL__N_122AppendCodeCreateHeaderERNS0_3Log14MessageBuilderENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCode4KindEPhiPNS_4base12ElapsedTimerE:
.LFB20733:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	%esi, %rbx
	leaq	.LC1(%rip), %rsi
	subq	$40, %rsp
	movl	%edx, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %rdi
	leaq	_ZN2v88internalL15kLogEventsNamesE(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	-68(%rbp), %r10d
	movq	%rax, %r12
	movq	(%rax), %rax
	movl	%r10d, %esi
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %r12
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-64(%rbp), %rdi
	subq	(%r15), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	%rax, %rsi
	movq	(%r12), %rax
	leaq	16(%rax), %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	%r13d, %esi
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L94
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L94:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20733:
	.size	_ZN2v88internal12_GLOBAL__N_122AppendCodeCreateHeaderERNS0_3Log14MessageBuilderENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCode4KindEPhiPNS_4base12ElapsedTimerE, .-_ZN2v88internal12_GLOBAL__N_122AppendCodeCreateHeaderERNS0_3Log14MessageBuilderENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCode4KindEPhiPNS_4base12ElapsedTimerE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_121AppendFunctionMessageERNS0_3Log14MessageBuilderEPKcidiiPNS_4base12ElapsedTimerE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"function"
	.section	.text._ZN2v88internal12_GLOBAL__N_121AppendFunctionMessageERNS0_3Log14MessageBuilderEPKcidiiPNS_4base12ElapsedTimerE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_121AppendFunctionMessageERNS0_3Log14MessageBuilderEPKcidiiPNS_4base12ElapsedTimerE, @function
_ZN2v88internal12_GLOBAL__N_121AppendFunctionMessageERNS0_3Log14MessageBuilderEPKcidiiPNS_4base12ElapsedTimerE:
.LFB20752:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	.LC2(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$40, %rsp
	movsd	%xmm0, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	%r15d, %esi
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	%r14d, %esi
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	%r13d, %esi
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movsd	-72(%rbp), %xmm0
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %r12
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-64(%rbp), %rdi
	subq	(%rbx), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	%rax, %rsi
	movq	(%r12), %rax
	leaq	16(%rax), %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L98:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20752:
	.size	_ZN2v88internal12_GLOBAL__N_121AppendFunctionMessageERNS0_3Log14MessageBuilderEPKcidiiPNS_4base12ElapsedTimerE, .-_ZN2v88internal12_GLOBAL__N_121AppendFunctionMessageERNS0_3Log14MessageBuilderEPKcidiiPNS_4base12ElapsedTimerE
	.section	.text._ZN2v88internal14LowLevelLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LowLevelLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci
	.type	_ZN2v88internal14LowLevelLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci, @function
_ZN2v88internal14LowLevelLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci:
.LFB20633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	movl	$1, %edx
	pushq	%r13
	.cfi_offset 13, -32
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	%ecx, -64(%rbp)
	movq	24(%rdi), %rcx
	leaq	-65(%rbp), %rdi
	movb	$67, -65(%rbp)
	movq	%rax, -56(%rbp)
	movq	8(%rsi), %rax
	movl	$1, %esi
	movl	%eax, -48(%rbp)
	call	fwrite@PLT
	movq	24(%r12), %rcx
	leaq	-64(%rbp), %rdi
	movl	$24, %edx
	movl	$1, %esi
	call	fwrite@PLT
	movq	24(%r12), %rcx
	movslq	%r13d, %rdx
	movq	%r14, %rdi
	movl	$1, %esi
	call	fwrite@PLT
	movq	24(%r12), %rcx
	movslq	8(%rbx), %rdx
	movl	$1, %esi
	movq	(%rbx), %rdi
	call	fwrite@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L102:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20633:
	.size	_ZN2v88internal14LowLevelLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci, .-_ZN2v88internal14LowLevelLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci
	.section	.text._ZN2v88internal9JitLoggerD0Ev,"axG",@progbits,_ZN2v88internal9JitLoggerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9JitLoggerD0Ev
	.type	_ZN2v88internal9JitLoggerD0Ev, @function
_ZN2v88internal9JitLoggerD0Ev:
.LFB24635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9JitLoggerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$32, %rdi
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	16(%r12), %rdi
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L104
	movl	$516, %esi
	call	_ZdlPvm@PLT
.L104:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24635:
	.size	_ZN2v88internal9JitLoggerD0Ev, .-_ZN2v88internal9JitLoggerD0Ev
	.section	.text._ZN2v88internal8OFStreamD0Ev,"axG",@progbits,_ZN2v88internal8OFStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD0Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD0Ev:
.LFB26531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26531:
	.size	_ZTv0_n24_N2v88internal8OFStreamD0Ev, .-_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.section	.text._ZN2v88internal6Ticker11SampleStackERKNS_13RegisterStateE.part.0,"axG",@progbits,_ZN2v88internal6Ticker11SampleStackERKNS_13RegisterStateE,comdat
	.align 2
	.p2align 4
	.type	_ZN2v88internal6Ticker11SampleStackERKNS_13RegisterStateE.part.0, @function
_ZN2v88internal6Ticker11SampleStackERKNS_13RegisterStateE.part.0:
.LFB26333:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$56, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	pxor	%xmm0, %xmm0
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzwl	-48(%rbp), %eax
	movq	%rsi, %rdx
	movq	24(%rdi), %rsi
	pushq	$0
	movq	%rdi, %rbx
	leaq	-4160(%rbp), %r12
	andw	$-1024, %ax
	movq	%r12, %rdi
	movups	%xmm0, -40(%rbp)
	orb	$2, %ah
	movq	$0, -56(%rbp)
	movw	%ax, -48(%rbp)
	movl	$5, -4160(%rbp)
	movups	%xmm0, -4152(%rbp)
	call	_ZN2v88internal10TickSample4InitEPNS0_7IsolateERKNS_13RegisterStateENS1_17RecordCEntryFrameEbbNS_4base9TimeDeltaE@PLT
	movq	48(%rbx), %rdx
	movslq	529464(%rdx), %rsi
	leal	1(%rsi), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$25, %ecx
	addl	%ecx, %eax
	andl	$127, %eax
	subl	%ecx, %eax
	movl	529468(%rdx), %ecx
	popq	%rdi
	popq	%r8
	cmpl	%ecx, %eax
	jne	.L112
	movb	$1, 529472(%rdx)
.L111:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L116
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	imulq	$4136, %rsi, %rsi
	movl	$517, %ecx
	leaq	56(%rdx,%rsi), %rdi
	movq	%r12, %rsi
	rep movsq
	movl	%eax, 529464(%rdx)
	leaq	529480(%rdx), %rdi
	call	_ZN2v84base9Semaphore6SignalEv@PLT
	jmp	.L111
.L116:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26333:
	.size	_ZN2v88internal6Ticker11SampleStackERKNS_13RegisterStateE.part.0, .-_ZN2v88internal6Ticker11SampleStackERKNS_13RegisterStateE.part.0
	.section	.text._ZN2v88internal6Ticker11SampleStackERKNS_13RegisterStateE,"axG",@progbits,_ZN2v88internal6Ticker11SampleStackERKNS_13RegisterStateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6Ticker11SampleStackERKNS_13RegisterStateE
	.type	_ZN2v88internal6Ticker11SampleStackERKNS_13RegisterStateE, @function
_ZN2v88internal6Ticker11SampleStackERKNS_13RegisterStateE:
.LFB20668:
	.cfi_startproc
	endbr64
	cmpq	$0, 48(%rdi)
	je	.L117
	jmp	_ZN2v88internal6Ticker11SampleStackERKNS_13RegisterStateE.part.0
	.p2align 4,,10
	.p2align 3
.L117:
	ret
	.cfi_endproc
.LFE20668:
	.size	_ZN2v88internal6Ticker11SampleStackERKNS_13RegisterStateE, .-_ZN2v88internal6Ticker11SampleStackERKNS_13RegisterStateE
	.section	.text._ZN2v88internal8OFStreamD0Ev,"axG",@progbits,_ZN2v88internal8OFStreamD0Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD0Ev
	.type	_ZN2v88internal8OFStreamD0Ev, @function
_ZN2v88internal8OFStreamD0Ev:
.LFB20396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE20396:
	.size	_ZN2v88internal8OFStreamD0Ev, .-_ZN2v88internal8OFStreamD0Ev
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD1Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD1Ev:
.LFB26532:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE26532:
	.size	_ZTv0_n24_N2v88internal8OFStreamD1Ev, .-_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.section	.text._ZN2v88internal14LowLevelLoggerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LowLevelLoggerD0Ev
	.type	_ZN2v88internal14LowLevelLoggerD0Ev, @function
_ZN2v88internal14LowLevelLoggerD0Ev:
.LFB20630:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14LowLevelLoggerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	call	fclose@PLT
	movq	16(%r12), %rdi
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rax
	movq	$0, 24(%r12)
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L124
	movl	$516, %esi
	call	_ZdlPvm@PLT
.L124:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE20630:
	.size	_ZN2v88internal14LowLevelLoggerD0Ev, .-_ZN2v88internal14LowLevelLoggerD0Ev
	.section	.text._ZN2v88internal15PerfBasicLoggerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15PerfBasicLoggerD0Ev
	.type	_ZN2v88internal15PerfBasicLoggerD0Ev, @function
_ZN2v88internal15PerfBasicLoggerD0Ev:
.LFB20599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal15PerfBasicLoggerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	call	fclose@PLT
	movq	16(%r12), %rdi
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rax
	movq	$0, 24(%r12)
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L130
	movl	$516, %esi
	call	_ZdlPvm@PLT
.L130:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE20599:
	.size	_ZN2v88internal15PerfBasicLoggerD0Ev, .-_ZN2v88internal15PerfBasicLoggerD0Ev
	.section	.text._ZN2v88internal6TickerD0Ev,"axG",@progbits,_ZN2v88internal6TickerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6TickerD0Ev
	.type	_ZN2v88internal6TickerD0Ev, @function
_ZN2v88internal6TickerD0Ev:
.LFB20665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6TickerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movzbl	32(%rdi), %eax
	testb	%al, %al
	jne	.L145
.L136:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L137
	movq	(%rdi), %rax
	call	*8(%rax)
.L137:
	movq	%r12, %rdi
	call	_ZN2v87sampler7SamplerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	call	_ZN2v87sampler7Sampler4StopEv@PLT
	jmp	.L136
	.cfi_endproc
.LFE20665:
	.size	_ZN2v88internal6TickerD0Ev, .-_ZN2v88internal6TickerD0Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_116CodeLinePosEventEPNS0_9JitLoggerEmRNS0_27SourcePositionTableIteratorE.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_116CodeLinePosEventEPNS0_9JitLoggerEmRNS0_27SourcePositionTableIteratorE.part.0, @function
_ZN2v88internal12_GLOBAL__N_116CodeLinePosEventEPNS0_9JitLoggerEmRNS0_27SourcePositionTableIteratorE.part.0:
.LFB26355:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movaps	%xmm0, -128(%rbp)
	movq	%r15, %rdi
	movl	$4, -128(%rbp)
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	*24(%r14)
	cmpl	$-1, 24(%r13)
	movq	-96(%rbp), %rbx
	jne	.L147
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L148:
	pxor	%xmm0, %xmm0
	movq	%rax, %xmm1
	movq	8(%r14), %rax
	movq	%r15, %rdi
	movaps	%xmm0, (%r15)
	movaps	%xmm0, 16(%r15)
	movaps	%xmm0, 48(%r15)
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -64(%rbp)
	movups	%xmm0, -88(%rbp)
	movl	$3, -128(%rbp)
	movq	%rbx, -96(%rbp)
	movl	$0, -72(%rbp)
	call	*24(%r14)
	movq	%r13, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, 24(%r13)
	je	.L149
.L147:
	movq	40(%r13), %rax
	movslq	32(%r13), %rdx
	shrq	%rax
	andl	$1073741823, %eax
	subl	$1, %eax
	cmpb	$0, 48(%r13)
	cltq
	je	.L148
	pxor	%xmm0, %xmm0
	movq	%rax, %xmm2
	movq	8(%r14), %rax
	movq	%r15, %rdi
	movaps	%xmm0, (%r15)
	movaps	%xmm0, 16(%r15)
	movaps	%xmm0, 48(%r15)
	movq	%rdx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, -64(%rbp)
	movl	$3, -128(%rbp)
	movq	%rbx, -96(%rbp)
	movl	$1, -72(%rbp)
	movups	%xmm0, -88(%rbp)
	call	*24(%r14)
	movq	40(%r13), %rax
	movslq	32(%r13), %rdx
	shrq	%rax
	andl	$1073741823, %eax
	subl	$1, %eax
	cltq
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L149:
	movq	8(%r14), %rax
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movaps	%xmm0, -128(%rbp)
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	$5, -128(%rbp)
	movq	%r12, -120(%rbp)
	movq	%rbx, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	*24(%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L155
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L155:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26355:
	.size	_ZN2v88internal12_GLOBAL__N_116CodeLinePosEventEPNS0_9JitLoggerEmRNS0_27SourcePositionTableIteratorE.part.0, .-_ZN2v88internal12_GLOBAL__N_116CodeLinePosEventEPNS0_9JitLoggerEmRNS0_27SourcePositionTableIteratorE.part.0
	.section	.text._ZN2v88internal6Logger17CodeMovingGCEventEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger17CodeMovingGCEventEv
	.type	_ZN2v88internal6Logger17CodeMovingGCEventEv, @function
_ZN2v88internal6Logger17CodeMovingGCEventEv:
.LFB20741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	136(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L157
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L158
	cmpb	$0, 40(%rdi)
	je	.L158
.L161:
	movq	48(%rbx), %rax
	cmpb	$0, (%rax)
	jne	.L156
.L168:
	cmpq	$0, 8(%rax)
	je	.L156
	cmpb	$0, _ZN2v88internal12FLAG_ll_profE(%rip)
	jne	.L167
.L156:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	cmpq	$0, 80(%rbx)
	je	.L156
	movq	48(%rbx), %rax
	cmpb	$0, (%rax)
	jne	.L156
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L157:
	call	*%rax
	testb	%al, %al
	jne	.L161
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base2OS18SignalCodeMovingGCEv@PLT
	.cfi_endproc
.LFE20741:
	.size	_ZN2v88internal6Logger17CodeMovingGCEventEv, .-_ZN2v88internal6Logger17CodeMovingGCEventEv
	.section	.rodata._ZN2v88internal6Logger27SharedFunctionInfoMoveEventEmm.str1.1,"aMS",@progbits,1
.LC3:
	.string	"sfi-move"
	.section	.text._ZN2v88internal6Logger27SharedFunctionInfoMoveEventEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger27SharedFunctionInfoMoveEventEmm
	.type	_ZN2v88internal6Logger27SharedFunctionInfoMoveEventEmm, @function
_ZN2v88internal6Logger27SharedFunctionInfoMoveEventEmm:
.LFB20748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	136(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L170
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L171
	cmpb	$0, 40(%rdi)
	je	.L171
.L174:
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	je	.L169
.L172:
	movq	48(%rbx), %rsi
	cmpb	$0, (%rsi)
	jne	.L169
	cmpq	$0, 8(%rsi)
	je	.L169
	leaq	-64(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L169:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L180
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	cmpq	$0, 80(%rbx)
	je	.L169
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	jne	.L172
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L170:
	call	*%rax
	testb	%al, %al
	je	.L169
	jmp	.L174
.L180:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20748:
	.size	_ZN2v88internal6Logger27SharedFunctionInfoMoveEventEmm, .-_ZN2v88internal6Logger27SharedFunctionInfoMoveEventEmm
	.section	.rodata._ZN2v88internal6Logger13CodeMoveEventENS0_12AbstractCodeES2_.str1.1,"aMS",@progbits,1
.LC4:
	.string	"code-move"
	.section	.text._ZN2v88internal6Logger13CodeMoveEventENS0_12AbstractCodeES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger13CodeMoveEventENS0_12AbstractCodeES2_
	.type	_ZN2v88internal6Logger13CodeMoveEventENS0_12AbstractCodeES2_, @function
_ZN2v88internal6Logger13CodeMoveEventENS0_12AbstractCodeES2_:
.LFB20743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	136(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L182
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L183
	cmpb	$0, 40(%rdi)
	je	.L183
.L186:
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	je	.L181
.L184:
	movq	48(%rbx), %rsi
	cmpb	$0, (%rsi)
	jne	.L181
	cmpq	$0, 8(%rsi)
	je	.L181
	leaq	-64(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	-1(%r13), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	-1(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L181:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L192
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	cmpq	$0, 80(%rbx)
	je	.L181
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	jne	.L184
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L182:
	call	*%rax
	testb	%al, %al
	je	.L181
	jmp	.L186
.L192:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20743:
	.size	_ZN2v88internal6Logger13CodeMoveEventENS0_12AbstractCodeES2_, .-_ZN2v88internal6Logger13CodeMoveEventENS0_12AbstractCodeES2_
	.section	.rodata._ZN2v88internal6Logger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"code-disable-optimization"
	.section	.text._ZN2v88internal6Logger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.type	_ZN2v88internal6Logger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE, @function
_ZN2v88internal6Logger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE:
.LFB20740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rdx, -72(%rbp)
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	136(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L194
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L195
	cmpb	$0, 40(%rdi)
	je	.L195
.L198:
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	je	.L193
.L196:
	movq	48(%rbx), %rsi
	cmpb	$0, (%rsi)
	jne	.L193
	cmpq	$0, 8(%rsi)
	je	.L193
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	-72(%rbp), %rdi
	movq	%rax, %r13
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %r13
	movq	-72(%rbp), %rax
	movl	47(%rax), %edi
	shrl	$20, %edi
	andl	$15, %edi
	call	_ZN2v88internal16GetBailoutReasonENS0_13BailoutReasonE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L193:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L203
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	cmpq	$0, 80(%rbx)
	je	.L193
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	jne	.L196
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L194:
	call	*%rax
	testb	%al, %al
	je	.L193
	jmp	.L198
.L203:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20740:
	.size	_ZN2v88internal6Logger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE, .-_ZN2v88internal6Logger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.section	.rodata._ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE.str1.1,"aMS",@progbits,1
.LC6:
	.string	""
.LC7:
	.string	"~"
.LC8:
	.string	"*"
.LC9:
	.string	"<unknown wasm>"
	.section	.text._ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.type	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE, @function
_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE:
.LFB20738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	136(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L205
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L206
	cmpb	$0, 40(%rdi)
	je	.L206
.L209:
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	je	.L204
.L207:
	movq	48(%rbx), %rsi
	movq	%r10, -88(%rbp)
	cmpb	$0, (%rsi)
	jne	.L204
	cmpq	$0, 8(%rsi)
	je	.L204
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movl	8(%r12), %r8d
	movl	%r14d, %esi
	movq	%r15, %rdi
	movq	(%r12), %rcx
	leaq	160(%rbx), %r9
	movl	$5, %edx
	call	_ZN2v88internal12_GLOBAL__N_122AppendCodeCreateHeaderERNS0_3Log14MessageBuilderENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCode4KindEPhiPNS_4base12ElapsedTimerE
	testq	%r13, %r13
	movq	-88(%rbp), %r10
	jne	.L210
	leaq	.LC9(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
.L211:
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	56(%r12), %r13d
	addq	48(%r12), %r13
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %rdi
	movl	60(%r12), %eax
	testl	%eax, %eax
	je	.L212
	cmpl	$4, %eax
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rax
	cmove	%rax, %rsi
.L213:
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L204:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L220
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	cmpq	$0, 80(%rbx)
	je	.L204
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	jne	.L207
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L205:
	movq	%rcx, -88(%rbp)
	call	*%rax
	movq	-88(%rbp), %r10
	testb	%al, %al
	je	.L204
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L210:
	movq	%r10, %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilder12AppendStringENS0_6VectorIKcEE@PLT
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L212:
	cmpb	$2, 136(%r12)
	leaq	.LC6(%rip), %rsi
	leaq	.LC8(%rip), %rax
	cmovne	%rax, %rsi
	jmp	.L213
.L220:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20738:
	.size	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE, .-_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.section	.text._ZN2v88internalL13ComputeMarkerENS0_18SharedFunctionInfoENS0_12AbstractCodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL13ComputeMarkerENS0_18SharedFunctionInfoENS0_12AbstractCodeE, @function
_ZN2v88internalL13ComputeMarkerENS0_18SharedFunctionInfoENS0_12AbstractCodeE:
.LFB20553:
	.cfi_startproc
	movq	-1(%rsi), %rax
	cmpw	$69, 11(%rax)
	je	.L229
.L222:
	movl	47(%rdi), %eax
	leaq	.LC6(%rip), %r8
	testl	$15728640, %eax
	leaq	.LC7(%rip), %rax
	cmove	%rax, %r8
.L221:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	movl	43(%rsi), %eax
	leaq	.LC8(%rip), %r8
	shrl	%eax
	andl	$31, %eax
	je	.L221
	leaq	.LC6(%rip), %r8
	cmpl	$12, %eax
	je	.L222
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE20553:
	.size	_ZN2v88internalL13ComputeMarkerENS0_18SharedFunctionInfoENS0_12AbstractCodeE, .-_ZN2v88internalL13ComputeMarkerENS0_18SharedFunctionInfoENS0_12AbstractCodeE
	.section	.text._ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.type	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc, @function
_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc:
.LFB20583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	leaq	_ZN2v88internalL15kLogEventsNamesE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	(%rax,%rsi,8), %r15
	movl	$0, (%rbx)
	movq	%r15, %rdi
	call	strlen@PLT
	movl	$512, %edx
	leaq	4(%rbx), %rcx
	cmpl	$512, %eax
	cmovle	%eax, %edx
	movslq	%edx, %r8
	cmpq	$8, %r8
	jnb	.L231
	testb	$4, %r8b
	jne	.L244
	testq	%r8, %r8
	je	.L232
	movzbl	(%r15), %esi
	movb	%sil, 4(%rbx)
	testb	$2, %r8b
	je	.L232
	movzwl	-2(%r15,%r8), %esi
	movw	%si, -2(%rcx,%r8)
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L231:
	movq	(%r15), %rsi
	leaq	12(%rbx), %rdi
	andq	$-8, %rdi
	movq	%rsi, 4(%rbx)
	movq	-8(%r15,%r8), %rsi
	movq	%rsi, -8(%rcx,%r8)
	subq	%rdi, %rcx
	movq	%r15, %rsi
	subq	%rcx, %rsi
	addq	%r8, %rcx
	shrq	$3, %rcx
	rep movsq
.L232:
	cmpl	$511, %eax
	jg	.L245
	addl	$1, %edx
	movl	%edx, (%rbx)
	movb	$58, 4(%rbx,%r8)
.L236:
	movq	16(%r12), %r15
	movq	%r14, %rdi
	movl	$512, %ebx
	call	strlen@PLT
	movq	%r14, %rsi
	movslq	(%r15), %rdx
	subl	%edx, %ebx
	leaq	4(%r15,%rdx), %rdi
	cmpl	%eax, %ebx
	cmovg	%eax, %ebx
	movslq	%ebx, %rdx
	call	memcpy@PLT
	addl	%ebx, (%r15)
	movq	(%r12), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	144(%rax), %r9
	movq	16(%r12), %rax
	movl	(%rax), %r8d
	addq	$8, %rsp
	leaq	4(%rax), %rcx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%r9
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	movl	%edx, (%rbx)
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L244:
	movl	(%r15), %esi
	movl	%esi, 4(%rbx)
	movl	-4(%r15,%r8), %esi
	movl	%esi, -4(%rcx,%r8)
	jmp	.L232
	.cfi_endproc
.LFE20583:
	.size	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc, .-_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.section	.text._ZN2v88internal15CodeEventLogger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15CodeEventLogger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.type	_ZN2v88internal15CodeEventLogger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE, @function
_ZN2v88internal15CodeEventLogger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE:
.LFB20589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$28792, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movl	$1164404050, 4(%rax)
	movl	$7, (%rax)
	movw	%cx, 8(%rax)
	movb	$58, 10(%rax)
	movq	16(%rdi), %rbx
	movq	%rdx, -72(%rbp)
	testq	%rdx, %rdx
	je	.L248
	xorl	%ecx, %ecx
	leaq	-76(%rbp), %r8
	leaq	-64(%rbp), %rdi
	movl	$1, %edx
	leaq	-72(%rbp), %rsi
	movl	$0, -76(%rbp)
	movl	$512, %r14d
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movslq	(%rbx), %rax
	movq	-64(%rbp), %r15
	subl	%eax, %r14d
	cmpl	%r14d, -76(%rbp)
	cmovle	-76(%rbp), %r14d
	movq	%r15, %rsi
	leaq	4(%rbx,%rax), %rdi
	movslq	%r14d, %rdx
	call	memcpy@PLT
	addl	%r14d, (%rbx)
	movq	%r15, %rdi
	call	_ZdaPv@PLT
	movq	16(%r12), %rbx
.L248:
	movq	(%r12), %rax
	xorl	%edx, %edx
	movl	(%rbx), %r8d
	leaq	4(%rbx), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*144(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L251
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L251:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20589:
	.size	_ZN2v88internal15CodeEventLogger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE, .-_ZN2v88internal15CodeEventLogger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.section	.text._ZN2v88internal25ExternalCodeEventListenerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ExternalCodeEventListenerD2Ev
	.type	_ZN2v88internal25ExternalCodeEventListenerD2Ev, @function
_ZN2v88internal25ExternalCodeEventListenerD2Ev:
.LFB20607:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal25ExternalCodeEventListenerE(%rip), %rax
	cmpb	$0, 8(%rdi)
	movq	%rax, (%rdi)
	jne	.L276
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rax
	movq	41488(%rax), %r12
	leaq	56(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%r12), %r8
	movq	%rbx, %rax
	xorl	%edx, %edx
	movq	(%r12), %r14
	divq	%r8
	leaq	0(,%rdx,8), %rax
	movq	%rdx, %r10
	leaq	(%r14,%rax), %r15
	movq	%rax, -56(%rbp)
	movq	(%r15), %r11
	testq	%r11, %r11
	je	.L254
	movq	(%r11), %rdi
	movq	%r11, %r9
	movq	8(%rdi), %rcx
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L277:
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L254
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r9
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L254
	movq	%rsi, %rdi
.L256:
	cmpq	%rcx, %rbx
	jne	.L277
	movq	(%rdi), %rcx
	cmpq	%r9, %r11
	je	.L278
	testq	%rcx, %rcx
	je	.L258
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r10
	je	.L258
	movq	%r9, (%r14,%rdx,8)
	movq	(%rdi), %rcx
.L258:
	movq	%rcx, (%r9)
	call	_ZdlPv@PLT
	subq	$1, 24(%r12)
.L254:
	addq	$24, %rsp
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L262
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r10
	je	.L258
	movq	%r9, (%r14,%rdx,8)
	movq	-56(%rbp), %r15
	addq	(%r12), %r15
	movq	(%r15), %rax
.L257:
	leaq	16(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L279
.L259:
	movq	$0, (%r15)
	movq	(%rdi), %rcx
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L262:
	movq	%r9, %rax
	jmp	.L257
.L279:
	movq	%rcx, 16(%r12)
	jmp	.L259
	.cfi_endproc
.LFE20607:
	.size	_ZN2v88internal25ExternalCodeEventListenerD2Ev, .-_ZN2v88internal25ExternalCodeEventListenerD2Ev
	.globl	_ZN2v88internal25ExternalCodeEventListenerD1Ev
	.set	_ZN2v88internal25ExternalCodeEventListenerD1Ev,_ZN2v88internal25ExternalCodeEventListenerD2Ev
	.section	.text._ZN2v88internal25ExternalCodeEventListenerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ExternalCodeEventListenerD0Ev
	.type	_ZN2v88internal25ExternalCodeEventListenerD0Ev, @function
_ZN2v88internal25ExternalCodeEventListenerD0Ev:
.LFB20609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal25ExternalCodeEventListenerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	cmpb	$0, 8(%rdi)
	movq	%rax, (%rdi)
	jne	.L302
.L281:
	addq	$24, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	movq	16(%rdi), %rax
	movq	41488(%rax), %rbx
	leaq	56(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%rbx), %r8
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	(%rbx), %r14
	divq	%r8
	leaq	0(,%rdx,8), %rax
	movq	%rdx, %r10
	leaq	(%r14,%rax), %r15
	movq	%rax, -56(%rbp)
	movq	(%r15), %r11
	testq	%r11, %r11
	je	.L282
	movq	(%r11), %rdi
	movq	%r11, %r9
	movq	8(%rdi), %rcx
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L303:
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L282
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r9
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L282
	movq	%rsi, %rdi
.L284:
	cmpq	%rcx, %r12
	jne	.L303
	movq	(%rdi), %rcx
	cmpq	%r9, %r11
	je	.L304
	testq	%rcx, %rcx
	je	.L286
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r10
	je	.L286
	movq	%r9, (%r14,%rdx,8)
	movq	(%rdi), %rcx
.L286:
	movq	%rcx, (%r9)
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
.L282:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L304:
	testq	%rcx, %rcx
	je	.L290
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r10
	je	.L286
	movq	%r9, (%r14,%rdx,8)
	movq	-56(%rbp), %r15
	addq	(%rbx), %r15
	movq	(%r15), %rax
.L285:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L305
.L287:
	movq	$0, (%r15)
	movq	(%rdi), %rcx
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L290:
	movq	%r9, %rax
	jmp	.L285
.L305:
	movq	%rcx, 16(%rbx)
	jmp	.L287
	.cfi_endproc
.LFE20609:
	.size	_ZN2v88internal25ExternalCodeEventListenerD0Ev, .-_ZN2v88internal25ExternalCodeEventListenerD0Ev
	.section	.rodata._ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii.str1.1,"aMS",@progbits,1
.LC10:
	.string	"symbol(hash "
.LC11:
	.string	"%x"
.LC12:
	.string	"%d"
	.section	.text._ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.type	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii, @function
_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii:
.LFB20586:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -88(%rbp)
	movq	16(%rdi), %rbx
	movl	%r9d, -100(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZN2v88internalL15kLogEventsNamesE(%rip), %rax
	movl	$0, (%rbx)
	movq	(%rax,%rsi,8), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	movl	$512, %edx
	leaq	4(%rbx), %rcx
	cmpl	$512, %eax
	cmovle	%eax, %edx
	movslq	%edx, %r8
	cmpq	$8, %r8
	jnb	.L307
	testb	$4, %r8b
	jne	.L351
	testq	%r8, %r8
	je	.L308
	movzbl	(%r15), %esi
	movb	%sil, 4(%rbx)
	testb	$2, %r8b
	je	.L308
	movzwl	-2(%r15,%r8), %esi
	movw	%si, -2(%rcx,%r8)
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L307:
	movq	(%r15), %rsi
	leaq	12(%rbx), %rdi
	andq	$-8, %rdi
	movq	%rsi, 4(%rbx)
	movq	-8(%r15,%r8), %rsi
	movq	%rsi, -8(%rcx,%r8)
	subq	%rdi, %rcx
	movq	%r15, %rsi
	subq	%rcx, %rsi
	addq	%r8, %rcx
	shrq	$3, %rcx
	rep movsq
.L308:
	cmpl	$511, %eax
	jg	.L352
	addl	$1, %edx
	movl	%edx, (%rbx)
	movb	$58, 4(%rbx,%r8)
.L312:
	movq	-88(%rbp), %rdi
	movq	%r13, %rsi
	movq	16(%r14), %rbx
	movl	$512, %r15d
	call	_ZN2v88internalL13ComputeMarkerENS0_18SharedFunctionInfoENS0_12AbstractCodeE
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	call	strlen@PLT
	movslq	(%rbx), %rdx
	movq	-96(%rbp), %rsi
	subl	%edx, %r15d
	leaq	4(%rbx,%rdx), %rdi
	cmpl	%eax, %r15d
	cmovg	%eax, %r15d
	movslq	%r15d, %rdx
	call	memcpy@PLT
	addl	%r15d, (%rbx)
	leaq	-88(%rbp), %rdi
	movq	16(%r14), %r15
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L314
	xorl	%ecx, %ecx
	leaq	-64(%rbp), %rdi
	leaq	-72(%rbp), %rsi
	movl	$1, %edx
	leaq	-76(%rbp), %r8
	movl	$0, -76(%rbp)
	movl	$512, %ebx
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movslq	(%r15), %rax
	movq	-64(%rbp), %r8
	subl	%eax, %ebx
	cmpl	%ebx, -76(%rbp)
	cmovle	-76(%rbp), %ebx
	movq	%r8, %rsi
	leaq	4(%r15,%rax), %rdi
	movq	%r8, -96(%rbp)
	movslq	%ebx, %rdx
	call	memcpy@PLT
	addl	%ebx, (%r15)
	movq	-96(%rbp), %r8
	movq	%r8, %rdi
	call	_ZdaPv@PLT
.L314:
	movq	16(%r14), %rbx
	movslq	(%rbx), %rax
	cmpl	$511, %eax
	jg	.L315
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movb	$32, 4(%rbx,%rax)
	movq	16(%r14), %rbx
.L315:
	movq	-1(%r12), %rax
	cmpw	$63, 11(%rax)
	ja	.L316
	movq	%r12, -72(%rbp)
	testq	%r12, %r12
	jne	.L353
.L319:
	movslq	(%rbx), %r8
	cmpl	$511, %r8d
	jg	.L329
	leal	1(%r8), %eax
	movl	%eax, (%rbx)
	movb	$58, 4(%rbx,%r8)
	movq	16(%r14), %rbx
	movslq	(%rbx), %r8
.L329:
	movl	$512, %esi
	leaq	4(%rbx), %rcx
	subl	%r8d, %esi
	testl	%esi, %esi
	jle	.L333
	leaq	(%rcx,%r8), %rdi
	movl	-100(%rbp), %ecx
	xorl	%eax, %eax
	movslq	%esi, %rsi
	leaq	.LC12(%rip), %rdx
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	testl	%eax, %eax
	jle	.L334
	addl	(%rbx), %eax
	cmpl	$512, %eax
	jle	.L354
.L334:
	movq	16(%r14), %rcx
	movl	(%rcx), %r8d
	addq	$4, %rcx
.L333:
	movq	(%r14), %rax
	movq	-88(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*144(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L355
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	xorl	%ecx, %ecx
	leaq	-76(%rbp), %r8
	leaq	-64(%rbp), %rdi
	movl	$1, %edx
	leaq	-72(%rbp), %rsi
	movl	$0, -76(%rbp)
	movl	$512, %r12d
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movslq	(%rbx), %rax
	movq	-64(%rbp), %r15
	subl	%eax, %r12d
	cmpl	%r12d, -76(%rbp)
	cmovle	-76(%rbp), %r12d
	movq	%r15, %rsi
	leaq	4(%rbx,%rax), %rdi
	movslq	%r12d, %rdx
	call	memcpy@PLT
	addl	%r12d, (%rbx)
	movq	%r15, %rdi
	call	_ZdaPv@PLT
	movq	16(%r14), %rbx
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L316:
	movslq	(%rbx), %rax
	movl	$512, %edx
	movl	$12, %ecx
	leaq	.LC10(%rip), %rsi
	subl	%eax, %edx
	cmpl	$12, %edx
	cmovg	%ecx, %edx
	leaq	4(%rbx,%rax), %rcx
	movslq	%edx, %rax
	cmpq	$8, %rax
	jnb	.L320
	testb	$4, %al
	jne	.L356
	testq	%rax, %rax
	jne	.L357
.L321:
	addl	%edx, (%rbx)
	movl	7(%r12), %ecx
	movq	16(%r14), %rbx
	testb	$1, %cl
	jne	.L326
.L358:
	shrl	$2, %ecx
.L327:
	movslq	(%rbx), %rax
	movl	$512, %esi
	subl	%eax, %esi
	testl	%esi, %esi
	jle	.L331
	leaq	4(%rbx,%rax), %rdi
	movslq	%esi, %rsi
	leaq	.LC11(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	testl	%eax, %eax
	jle	.L331
	addl	(%rbx), %eax
	cmpl	$512, %eax
	jg	.L331
	movl	%eax, (%rbx)
	.p2align 4,,10
	.p2align 3
.L331:
	movq	16(%r14), %rbx
	movslq	(%rbx), %r8
	cmpl	$511, %r8d
	jg	.L329
	leal	1(%r8), %eax
	movl	%eax, (%rbx)
	movb	$41, 4(%rbx,%r8)
	movq	16(%r14), %rbx
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L352:
	movl	%edx, (%rbx)
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L320:
	movq	.LC10(%rip), %rdi
	movq	%rdi, (%rcx)
	movq	-8(%rsi,%rax), %rdi
	movq	%rdi, -8(%rcx,%rax)
	leaq	8(%rcx), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addq	%rcx, %rax
	subq	%rcx, %rsi
	andq	$-8, %rax
	cmpq	$8, %rax
	jb	.L321
	andq	$-8, %rax
	xorl	%ecx, %ecx
.L324:
	movq	(%rsi,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	addq	$8, %rcx
	cmpq	%rax, %rcx
	jb	.L324
	addl	%edx, (%rbx)
	movl	7(%r12), %ecx
	movq	16(%r14), %rbx
	testb	$1, %cl
	je	.L358
.L326:
	leaq	-64(%rbp), %rdi
	movq	%r12, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movl	%eax, %ecx
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L354:
	movl	%eax, (%rbx)
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L357:
	movzbl	.LC10(%rip), %edi
	movb	%dil, (%rcx)
	testb	$2, %al
	je	.L321
	movzwl	-2(%rsi,%rax), %esi
	movw	%si, -2(%rcx,%rax)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L351:
	movl	(%r15), %esi
	movl	%esi, 4(%rbx)
	movl	-4(%r15,%r8), %esi
	movl	%esi, -4(%rcx,%r8)
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L356:
	movl	.LC10(%rip), %edi
	movl	-4(%rsi,%rax), %esi
	movl	%edi, (%rcx)
	movl	%esi, -4(%rcx,%rax)
	jmp	.L321
.L355:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20586:
	.size	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii, .-_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.section	.rodata._ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"<wasm-unknown>"
.LC14:
	.string	"<anonymous>"
	.section	.text._ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.type	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE, @function
_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE:
.LFB20587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internalL15kLogEventsNamesE(%rip), %rax
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r15
	movq	(%rax,%rsi,8), %rsi
	movl	$0, (%r15)
	movq	%rsi, %rdi
	movq	%rsi, -56(%rbp)
	call	strlen@PLT
	movl	$512, %edx
	movq	-56(%rbp), %rsi
	leaq	4(%r15), %rcx
	cmpl	$512, %eax
	cmovle	%eax, %edx
	movslq	%edx, %r8
	cmpq	$8, %r8
	jnb	.L360
	testb	$4, %r8b
	jne	.L406
	testq	%r8, %r8
	je	.L361
	movzbl	(%rsi), %edi
	movb	%dil, 4(%r15)
	testb	$2, %r8b
	je	.L361
	movzwl	-2(%rsi,%r8), %esi
	movw	%si, -2(%rcx,%r8)
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L360:
	movq	(%rsi), %rdi
	movq	%rdi, 4(%r15)
	movq	-8(%rsi,%r8), %rdi
	movq	%rdi, -8(%rcx,%r8)
	leaq	12(%r15), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addq	%r8, %rcx
	shrq	$3, %rcx
	rep movsq
.L361:
	cmpl	$511, %eax
	jg	.L407
	addl	$1, %edx
	movl	%edx, (%r15)
	movb	$58, 4(%r15,%r8)
.L365:
	movq	16(%r12), %r15
	movl	$512, %eax
	movslq	(%r15), %rdx
	subl	%edx, %eax
	leaq	4(%r15,%rdx), %rdi
	testq	%rbx, %rbx
	jne	.L366
	movl	$14, %edx
	cmpl	$14, %eax
	leaq	.LC13(%rip), %rcx
	cmovg	%edx, %eax
	movslq	%eax, %rdx
	cmpq	$8, %rdx
	jb	.L408
	movq	.LC13(%rip), %rsi
	leaq	8(%rdi), %r8
	andq	$-8, %r8
	movq	%rsi, (%rdi)
	movq	-8(%rcx,%rdx), %rsi
	movq	%rsi, -8(%rdi,%rdx)
	subq	%r8, %rdi
	movq	%rcx, %rsi
	addq	%rdi, %rdx
	subq	%rdi, %rsi
	andq	$-8, %rdx
	cmpq	$8, %rdx
	jb	.L368
	andq	$-8, %rdx
	xorl	%ecx, %ecx
.L371:
	movq	(%rsi,%rcx), %rdi
	movq	%rdi, (%r8,%rcx)
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jb	.L371
.L368:
	addl	%eax, (%r15)
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L366:
	cmpl	%ebx, %eax
	movq	%r14, %rsi
	cmovl	%eax, %ebx
	movslq	%ebx, %rdx
	call	memcpy@PLT
	addl	%ebx, (%r15)
.L373:
	movq	16(%r12), %rbx
	movslq	(%rbx), %rcx
	cmpl	$511, %ecx
	jg	.L374
	leal	1(%rcx), %eax
	movl	%eax, (%rbx)
	movb	$45, 4(%rbx,%rcx)
	movq	16(%r12), %rbx
	movslq	(%rbx), %rcx
.L374:
	movl	56(%r13), %r8d
	movl	$512, %esi
	leaq	4(%rbx), %rdx
	subl	%ecx, %esi
	cmpl	$-1, %r8d
	je	.L409
	testl	%esi, %esi
	jle	.L382
	leaq	(%rdx,%rcx), %rdi
	xorl	%eax, %eax
	movslq	%esi, %rsi
	movl	%r8d, %ecx
	leaq	.LC12(%rip), %rdx
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	testl	%eax, %eax
	jle	.L384
	addl	(%rbx), %eax
	cmpl	$512, %eax
	jg	.L384
	movl	%eax, (%rbx)
.L384:
	movq	16(%r12), %rdx
	movl	(%rdx), %ecx
	addq	$4, %rdx
.L382:
	movq	(%r12), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	152(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	testb	$4, %dl
	jne	.L410
	testq	%rdx, %rdx
	je	.L368
	movzbl	.LC13(%rip), %esi
	movb	%sil, (%rdi)
	testb	$2, %dl
	je	.L368
	movzwl	-2(%rcx,%rdx), %ecx
	movw	%cx, -2(%rdi,%rdx)
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L409:
	cmpl	$11, %esi
	movl	$11, %eax
	cmovg	%eax, %esi
	addq	%rcx, %rdx
	leaq	.LC14(%rip), %rcx
	movslq	%esi, %rax
	cmpq	$8, %rax
	jnb	.L376
	testb	$4, %al
	jne	.L411
	testq	%rax, %rax
	je	.L377
	movzbl	.LC14(%rip), %edi
	movb	%dil, (%rdx)
	testb	$2, %al
	jne	.L412
.L377:
	addl	%esi, (%rbx)
	movq	16(%r12), %rdx
	movl	(%rdx), %ecx
	addq	$4, %rdx
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L407:
	movl	%edx, (%r15)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L376:
	movq	.LC14(%rip), %rdi
	movq	%rdi, (%rdx)
	movq	-8(%rcx,%rax), %rdi
	movq	%rdi, -8(%rdx,%rax)
	leaq	8(%rdx), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rdx
	subq	%rdx, %rcx
	addq	%rax, %rdx
	andq	$-8, %rdx
	cmpq	$8, %rdx
	jb	.L377
	andq	$-8, %rdx
	xorl	%eax, %eax
.L380:
	movq	(%rcx,%rax), %r8
	movq	%r8, (%rdi,%rax)
	addq	$8, %rax
	cmpq	%rdx, %rax
	jb	.L380
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L406:
	movl	(%rsi), %edi
	movl	%edi, 4(%r15)
	movl	-4(%rsi,%r8), %esi
	movl	%esi, -4(%rcx,%r8)
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L410:
	movl	.LC13(%rip), %esi
	movl	-4(%rcx,%rdx), %ecx
	movl	%esi, (%rdi)
	movl	%ecx, -4(%rdi,%rdx)
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L411:
	movl	.LC14(%rip), %edi
	movl	-4(%rcx,%rax), %ecx
	movl	%edi, (%rdx)
	movl	%ecx, -4(%rdx,%rax)
	jmp	.L377
.L412:
	movzwl	-2(%rcx,%rax), %ecx
	movw	%cx, -2(%rdx,%rax)
	jmp	.L377
	.cfi_endproc
.LFE20587:
	.size	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE, .-_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.section	.rodata._ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"symbol("
.LC16:
	.string	"\""
.LC17:
	.string	"\" "
.LC18:
	.string	"hash "
	.section	.text._ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.type	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE, @function
_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE:
.LFB20585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZN2v88internalL15kLogEventsNamesE(%rip), %rax
	movq	(%rax,%rsi,8), %rsi
	movl	$0, (%rbx)
	movq	%rsi, %rdi
	movq	%rsi, -88(%rbp)
	call	strlen@PLT
	movl	$512, %edx
	movq	-88(%rbp), %rsi
	leaq	4(%rbx), %rcx
	cmpl	$512, %eax
	cmovle	%eax, %edx
	movslq	%edx, %r8
	cmpq	$8, %r8
	jnb	.L414
	testb	$4, %r8b
	jne	.L477
	testq	%r8, %r8
	je	.L415
	movzbl	(%rsi), %edi
	movb	%dil, 4(%rbx)
	testb	$2, %r8b
	je	.L415
	movzwl	-2(%rsi,%r8), %esi
	movw	%si, -2(%rcx,%r8)
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L414:
	movq	(%rsi), %rdi
	movq	%rdi, 4(%rbx)
	movq	-8(%rsi,%r8), %rdi
	movq	%rdi, -8(%rcx,%r8)
	leaq	12(%rbx), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addq	%r8, %rcx
	shrq	$3, %rcx
	rep movsq
.L415:
	cmpl	$511, %eax
	jg	.L478
	addl	$1, %edx
	movl	%edx, (%rbx)
	movb	$58, 4(%rbx,%r8)
.L419:
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	16(%r14), %rbx
	call	_ZN2v88internalL13ComputeMarkerENS0_18SharedFunctionInfoENS0_12AbstractCodeE
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	call	strlen@PLT
	movslq	(%rbx), %rdx
	movl	$512, %ecx
	movq	-96(%rbp), %rsi
	subl	%edx, %ecx
	leaq	4(%rbx,%rdx), %rdi
	cmpl	%eax, %ecx
	cmovg	%eax, %ecx
	movslq	%ecx, %rdx
	movl	%ecx, -88(%rbp)
	call	memcpy@PLT
	movl	-88(%rbp), %ecx
	addl	%ecx, (%rbx)
	movq	16(%r14), %rbx
	movslq	(%rbx), %rax
	cmpl	$511, %eax
	jg	.L420
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	movb	$32, 4(%rbx,%rax)
	movq	16(%r14), %rbx
.L420:
	movq	-1(%r15), %rax
	cmpw	$63, 11(%rax)
	ja	.L421
	movq	%r15, -72(%rbp)
	testq	%r15, %r15
	jne	.L479
.L424:
	movq	(%r14), %rax
	movl	(%rbx), %r8d
	leaq	4(%rbx), %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*144(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L480
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	movl	(%rbx), %edx
	movl	$512, %eax
	movl	$7, %ecx
	leaq	4(%rbx), %r9
	leaq	.LC15(%rip), %r8
	subl	%edx, %eax
	cmpl	$7, %eax
	cmovg	%ecx, %eax
	movslq	%edx, %rcx
	addq	%r9, %rcx
	movslq	%eax, %rsi
	testq	%rsi, %rsi
	je	.L426
	xorl	%edx, %edx
.L425:
	movzbl	(%r8,%rdx), %edi
	movb	%dil, (%rcx,%rdx)
	addq	$1, %rdx
	cmpq	%rsi, %rdx
	jb	.L425
	movl	(%rbx), %edx
.L426:
	addl	%edx, %eax
	movl	%eax, (%rbx)
	movq	15(%r15), %rdx
	testb	$1, %dl
	jne	.L427
.L433:
	movl	$512, %ecx
	movl	$1, %edx
	leaq	.LC16(%rip), %r8
	subl	%eax, %ecx
	cltq
	testl	%ecx, %ecx
	cmovg	%edx, %ecx
	addq	%r9, %rax
	movslq	%ecx, %rdi
	testq	%rdi, %rdi
	je	.L429
	xorl	%edx, %edx
.L428:
	movzbl	(%r8,%rdx), %esi
	movb	%sil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jb	.L428
.L429:
	addl	%ecx, (%rbx)
	movq	15(%r15), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	jne	.L481
.L434:
	movl	(%rbx), %edx
	movl	$512, %eax
	movl	$2, %ecx
	leaq	.LC17(%rip), %r8
	subl	%edx, %eax
	cmpl	$2, %eax
	cmovg	%ecx, %eax
	movslq	%edx, %rcx
	addq	%r9, %rcx
	movslq	%eax, %rdi
	testq	%rdi, %rdi
	je	.L436
	xorl	%edx, %edx
.L435:
	movzbl	(%r8,%rdx), %esi
	movb	%sil, (%rcx,%rdx)
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jb	.L435
	movl	(%rbx), %edx
.L436:
	addl	%edx, %eax
	movl	%eax, (%rbx)
.L432:
	movl	$512, %edi
	movl	$5, %edx
	leaq	.LC18(%rip), %r8
	subl	%eax, %edi
	cltq
	cmpl	$5, %edi
	cmovg	%edx, %edi
	addq	%r9, %rax
	movslq	%edi, %rcx
	testq	%rcx, %rcx
	je	.L438
	xorl	%edx, %edx
.L437:
	movzbl	(%r8,%rdx), %esi
	movb	%sil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rcx, %rdx
	jb	.L437
.L438:
	addl	(%rbx), %edi
	movl	%edi, (%rbx)
	movl	7(%r15), %ecx
	testb	$1, %cl
	jne	.L439
	shrl	$2, %ecx
.L440:
	movl	$512, %esi
	subl	%edi, %esi
	testl	%esi, %esi
	jle	.L442
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	leaq	.LC11(%rip), %rdx
	xorl	%eax, %eax
	addq	%r9, %rdi
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movl	(%rbx), %edi
	testl	%eax, %eax
	jle	.L442
	addl	%edi, %eax
	cmpl	$512, %eax
	jg	.L442
	movl	%eax, (%rbx)
	movl	%eax, %edi
	.p2align 4,,10
	.p2align 3
.L442:
	cmpl	$511, %edi
	jg	.L476
	leal	1(%rdi), %eax
	movslq	%edi, %rdi
	movl	%eax, (%rbx)
	movb	$41, 4(%rbx,%rdi)
.L476:
	movq	16(%r14), %rbx
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L479:
	xorl	%ecx, %ecx
	leaq	-64(%rbp), %rdi
	leaq	-72(%rbp), %rsi
	movl	$1, %edx
	leaq	-76(%rbp), %r8
	movl	$0, -76(%rbp)
	movl	$512, %r15d
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movslq	(%rbx), %rax
	movq	-64(%rbp), %r8
	subl	%eax, %r15d
	cmpl	%r15d, -76(%rbp)
	cmovle	-76(%rbp), %r15d
	movq	%r8, %rsi
	leaq	4(%rbx,%rax), %rdi
	movq	%r8, -88(%rbp)
	movslq	%r15d, %rdx
	call	memcpy@PLT
	addl	%r15d, (%rbx)
	movq	-88(%rbp), %r8
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	16(%r14), %rbx
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L439:
	leaq	-64(%rbp), %rdi
	movq	%r9, -88(%rbp)
	movq	%r15, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movl	(%rbx), %edi
	movq	-88(%rbp), %r9
	movl	%eax, %ecx
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L481:
	xorl	%ecx, %ecx
	leaq	-64(%rbp), %rdi
	leaq	-72(%rbp), %rsi
	movl	$1, %edx
	leaq	-76(%rbp), %r8
	movq	%r9, -88(%rbp)
	movl	$0, -76(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movslq	(%rbx), %rdi
	movl	$512, %ecx
	movq	-88(%rbp), %r9
	movq	-64(%rbp), %r8
	subl	%edi, %ecx
	cmpl	%ecx, -76(%rbp)
	cmovle	-76(%rbp), %ecx
	addq	%r9, %rdi
	movq	%r8, %rsi
	movq	%r9, -104(%rbp)
	movslq	%ecx, %rdx
	movl	%ecx, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	memcpy@PLT
	movl	-96(%rbp), %ecx
	addl	%ecx, (%rbx)
	movq	-88(%rbp), %r8
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	-104(%rbp), %r9
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L478:
	movl	%edx, (%rbx)
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L477:
	movl	(%rsi), %edi
	movl	%edi, 4(%rbx)
	movl	-4(%rsi,%r8), %esi
	movl	%esi, -4(%rcx,%r8)
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L427:
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	-37504(%rcx), %rdx
	je	.L432
	jmp	.L433
.L480:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20585:
	.size	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE, .-_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.section	.text._ZN2v88internal14LowLevelLogger13CodeMoveEventENS0_12AbstractCodeES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LowLevelLogger13CodeMoveEventENS0_12AbstractCodeES2_
	.type	_ZN2v88internal14LowLevelLogger13CodeMoveEventENS0_12AbstractCodeES2_, @function
_ZN2v88internal14LowLevelLogger13CodeMoveEventENS0_12AbstractCodeES2_:
.LFB20634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	53(%rsi), %rax
	movq	-1(%rsi), %rdx
	cmpw	$69, 11(%rdx)
	je	.L493
.L486:
	movq	%rax, -64(%rbp)
	movq	-1(%rbx), %rdx
	leaq	53(%rbx), %rax
	leaq	-72(%rbp), %rdi
	cmpw	$69, 11(%rdx)
	je	.L494
.L490:
	movq	24(%r12), %rcx
	movl	$1, %edx
	movl	$1, %esi
	movq	%rax, -56(%rbp)
	movb	$77, -72(%rbp)
	call	fwrite@PLT
	movq	24(%r12), %rcx
	movl	$16, %edx
	movq	%r13, %rdi
	movl	$1, %esi
	call	fwrite@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L495
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore_state
	movl	43(%rsi), %ecx
	movq	%rsi, -64(%rbp)
	leaq	63(%rsi), %rax
	testl	%ecx, %ecx
	jns	.L486
	movq	%r13, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L494:
	movl	43(%rbx), %edx
	movq	%rbx, -72(%rbp)
	leaq	63(%rbx), %rax
	testl	%edx, %edx
	jns	.L490
	movq	%rdi, -88(%rbp)
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-88(%rbp), %rdi
	jmp	.L490
.L495:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20634:
	.size	_ZN2v88internal14LowLevelLogger13CodeMoveEventENS0_12AbstractCodeES2_, .-_ZN2v88internal14LowLevelLogger13CodeMoveEventENS0_12AbstractCodeES2_
	.section	.text._ZN2v88internal12_GLOBAL__N_122AppendCodeCreateHeaderERNS0_3Log14MessageBuilderENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPNS_4base12ElapsedTimerE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_122AppendCodeCreateHeaderERNS0_3Log14MessageBuilderENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPNS_4base12ElapsedTimerE, @function
_ZN2v88internal12_GLOBAL__N_122AppendCodeCreateHeaderERNS0_3Log14MessageBuilderENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPNS_4base12ElapsedTimerE:
.LFB20734:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-1(%rdx), %rax
	cmpw	$69, 11(%rax)
	je	.L509
	movl	11(%rdx), %r8d
.L500:
	movq	-1(%rbx), %rax
	leaq	53(%rbx), %rcx
	cmpw	$69, 11(%rax)
	je	.L510
.L504:
	movq	-1(%rbx), %rax
	movl	$12, %edx
	cmpw	$69, 11(%rax)
	jne	.L505
	movl	43(%rbx), %edx
	shrl	%edx
	andl	$31, %edx
.L505:
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122AppendCodeCreateHeaderERNS0_3Log14MessageBuilderENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCode4KindEPhiPNS_4base12ElapsedTimerE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L511
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	.cfi_restore_state
	movl	43(%rbx), %eax
	movq	%rbx, -32(%rbp)
	leaq	63(%rbx), %rcx
	testl	%eax, %eax
	jns	.L504
	leaq	-32(%rbp), %rdi
	movq	%r9, -56(%rbp)
	movl	%esi, -48(%rbp)
	movl	%r8d, -36(%rbp)
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-56(%rbp), %r9
	movl	-48(%rbp), %esi
	movl	-36(%rbp), %r8d
	movq	%rax, %rcx
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L509:
	movq	%rdx, -32(%rbp)
	movl	43(%rdx), %edx
	testl	%edx, %edx
	js	.L512
	movl	39(%rbx), %r8d
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L512:
	leaq	-32(%rbp), %rdi
	movl	%esi, -36(%rbp)
	movq	%rcx, -48(%rbp)
	call	_ZNK2v88internal4Code22OffHeapInstructionSizeEv@PLT
	movl	-36(%rbp), %esi
	movq	-48(%rbp), %r9
	movl	%eax, %r8d
	jmp	.L500
.L511:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20734:
	.size	_ZN2v88internal12_GLOBAL__N_122AppendCodeCreateHeaderERNS0_3Log14MessageBuilderENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPNS_4base12ElapsedTimerE, .-_ZN2v88internal12_GLOBAL__N_122AppendCodeCreateHeaderERNS0_3Log14MessageBuilderENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPNS_4base12ElapsedTimerE
	.section	.text._ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.type	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc, @function
_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc:
.LFB20735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	136(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L514
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L515
	cmpb	$0, 40(%rdi)
	je	.L515
.L518:
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	je	.L513
.L516:
	movq	48(%rbx), %rsi
	cmpb	$0, (%rsi)
	jne	.L513
	cmpq	$0, 8(%rsi)
	je	.L513
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r15, %rdi
	leaq	160(%rbx), %rcx
	call	_ZN2v88internal12_GLOBAL__N_122AppendCodeCreateHeaderERNS0_3Log14MessageBuilderENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPNS_4base12ElapsedTimerE
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L513:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L523
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L515:
	.cfi_restore_state
	cmpq	$0, 80(%rbx)
	je	.L513
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	jne	.L516
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L514:
	call	*%rax
	testb	%al, %al
	je	.L513
	jmp	.L518
.L523:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20735:
	.size	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc, .-_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.section	.text._ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.type	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE, @function
_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE:
.LFB20736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	136(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L525
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L526
	cmpb	$0, 40(%rdi)
	je	.L526
.L529:
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	je	.L524
.L527:
	movq	48(%rbx), %rsi
	cmpb	$0, (%rsi)
	jne	.L524
	cmpq	$0, 8(%rsi)
	je	.L524
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r15, %rdi
	leaq	160(%rbx), %rcx
	call	_ZN2v88internal12_GLOBAL__N_122AppendCodeCreateHeaderERNS0_3Log14MessageBuilderENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPNS_4base12ElapsedTimerE
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_4NameEEERS2_T_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L524:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L534
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L526:
	.cfi_restore_state
	cmpq	$0, 80(%rbx)
	je	.L524
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	jne	.L527
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L525:
	call	*%rax
	testb	%al, %al
	je	.L524
	jmp	.L529
.L534:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20736:
	.size	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE, .-_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.section	.text._ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.type	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE, @function
_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE:
.LFB20737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	136(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L536
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L537
	cmpb	$0, 40(%rdi)
	je	.L537
.L540:
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	je	.L535
.L538:
	movq	48(%rbx), %rax
	cmpb	$0, (%rax)
	jne	.L535
	cmpq	$0, 8(%rax)
	je	.L535
	movq	8(%rbx), %rax
	movl	$67, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	cmpq	%r12, %rax
	je	.L535
	leaq	-80(%rbp), %r8
	movq	48(%rbx), %rsi
	movq	%r8, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	-88(%rbp), %r8
	movq	%r12, %rdx
	movl	%r14d, %esi
	leaq	160(%rbx), %rcx
	movq	%r8, %rdi
	call	_ZN2v88internal12_GLOBAL__N_122AppendCodeCreateHeaderERNS0_3Log14MessageBuilderENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPNS_4base12ElapsedTimerE
	movq	-88(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_4NameEEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	-1(%r13), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r9
	call	_ZN2v88internalL13ComputeMarkerENS0_18SharedFunctionInfoENS0_12AbstractCodeE
	movq	%r9, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	-88(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L535:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L545
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L537:
	.cfi_restore_state
	cmpq	$0, 80(%rbx)
	je	.L535
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	jne	.L538
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L536:
	call	*%rax
	testb	%al, %al
	je	.L535
	jmp	.L540
.L545:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20737:
	.size	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE, .-_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.section	.text._ZN2v88internal6Logger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.type	_ZN2v88internal6Logger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE, @function
_ZN2v88internal6Logger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE:
.LFB20742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rdx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	136(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L547
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L548
	cmpb	$0, 40(%rdi)
	je	.L548
.L551:
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	je	.L546
.L549:
	movq	48(%rbx), %rsi
	cmpb	$0, (%rsi)
	jne	.L546
	cmpq	$0, 8(%rsi)
	je	.L546
	leaq	-64(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r13, %rdx
	movl	$16, %esi
	movq	%r14, %rdi
	leaq	160(%rbx), %rcx
	call	_ZN2v88internal12_GLOBAL__N_122AppendCodeCreateHeaderERNS0_3Log14MessageBuilderENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPNS_4base12ElapsedTimerE
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L546:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L556
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L548:
	.cfi_restore_state
	cmpq	$0, 80(%rbx)
	je	.L546
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	jne	.L549
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L547:
	call	*%rax
	testb	%al, %al
	je	.L546
	jmp	.L551
.L556:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20742:
	.size	_ZN2v88internal6Logger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE, .-_ZN2v88internal6Logger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.section	.rodata._ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc.str1.1,"aMS",@progbits,1
.LC19:
	.string	"unreachable code"
	.section	.text._ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.type	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc, @function
_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc:
.LFB20613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -88(%rbp)
	leaq	53(%rbx), %rax
	movq	-1(%rdx), %rdx
	cmpw	$69, 11(%rdx)
	je	.L570
.L561:
	movq	%rax, -104(%rbp)
	movq	-1(%rbx), %rax
	cmpw	$69, 11(%rax)
	je	.L571
	movl	11(%rbx), %eax
.L565:
	cltq
	movq	$0, -72(%rbp)
	movq	%rax, -96(%rbp)
	movq	16(%r13), %rax
	subq	$-128, %rax
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, -88(%rbp)
	cmpl	$22, %esi
	ja	.L566
	movl	%esi, %r12d
	movq	24(%r13), %rdi
	movq	%rcx, -56(%rbp)
	leaq	-112(%rbp), %rsi
	leaq	CSWTCH.451(%rip), %rax
	movl	(%rax,%r12,4), %eax
	movl	%eax, -64(%rbp)
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L572
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L570:
	.cfi_restore_state
	movl	43(%rbx), %edx
	movq	%rbx, -120(%rbp)
	leaq	63(%rbx), %rax
	testl	%edx, %edx
	jns	.L561
	leaq	-120(%rbp), %rdi
	movq	%rcx, -144(%rbp)
	movl	%esi, -132(%rbp)
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-144(%rbp), %rcx
	movl	-132(%rbp), %esi
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L571:
	movl	43(%rbx), %eax
	movq	%rbx, -120(%rbp)
	testl	%eax, %eax
	js	.L573
	movl	39(%rbx), %eax
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L573:
	leaq	-120(%rbp), %rdi
	movq	%rcx, -144(%rbp)
	movl	%esi, -132(%rbp)
	call	_ZNK2v88internal4Code22OffHeapInstructionSizeEv@PLT
	movl	-132(%rbp), %esi
	movq	-144(%rbp), %rcx
	jmp	.L565
.L566:
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L572:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20613:
	.size	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc, .-_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.section	.text._ZN2v88internal14LowLevelLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LowLevelLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci
	.type	_ZN2v88internal14LowLevelLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci, @function
_ZN2v88internal14LowLevelLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci:
.LFB20632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-88(%rbp), %r15
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%r8d, -80(%rbp)
	leaq	53(%rsi), %rax
	movq	-1(%rsi), %rdx
	cmpw	$69, 11(%rdx)
	je	.L593
.L578:
	movq	%rax, -72(%rbp)
	movq	-1(%rbx), %rax
	cmpw	$69, 11(%rax)
	je	.L594
	movl	11(%rbx), %eax
.L582:
	movq	24(%r12), %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	movl	%eax, -64(%rbp)
	movl	$1, %esi
	movb	$67, -88(%rbp)
	call	fwrite@PLT
	movq	24(%r12), %rcx
	leaq	-80(%rbp), %rdi
	movl	$24, %edx
	movl	$1, %esi
	call	fwrite@PLT
	movq	24(%r12), %rcx
	movslq	%r13d, %rdx
	movq	%r14, %rdi
	movl	$1, %esi
	call	fwrite@PLT
	movq	-1(%rbx), %rax
	cmpw	$69, 11(%rax)
	je	.L595
	movl	11(%rbx), %r13d
.L586:
	movq	-1(%rbx), %rax
	leaq	53(%rbx), %rdi
	cmpw	$69, 11(%rax)
	je	.L596
.L590:
	movq	24(%r12), %rcx
	movslq	%r13d, %rdx
	movl	$1, %esi
	call	fwrite@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L597
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L593:
	.cfi_restore_state
	movq	%rsi, -88(%rbp)
	leaq	63(%rsi), %rax
	movl	43(%rsi), %esi
	testl	%esi, %esi
	jns	.L578
	movq	%r15, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L596:
	movl	43(%rbx), %eax
	movq	%rbx, -88(%rbp)
	leaq	63(%rbx), %rdi
	testl	%eax, %eax
	jns	.L590
	movq	%r15, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rdi
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L595:
	movl	43(%rbx), %edx
	movq	%rbx, -88(%rbp)
	testl	%edx, %edx
	js	.L598
	movl	39(%rbx), %r13d
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L594:
	movl	43(%rbx), %ecx
	movq	%rbx, -88(%rbp)
	testl	%ecx, %ecx
	js	.L599
	movl	39(%rbx), %eax
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L599:
	movq	%r15, %rdi
	call	_ZNK2v88internal4Code22OffHeapInstructionSizeEv@PLT
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L598:
	movq	%r15, %rdi
	call	_ZNK2v88internal4Code22OffHeapInstructionSizeEv@PLT
	movl	%eax, %r13d
	jmp	.L586
.L597:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20632:
	.size	_ZN2v88internal14LowLevelLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci, .-_ZN2v88internal14LowLevelLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci
	.section	.text._ZN2v88internal15PerfBasicLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15PerfBasicLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci
	.type	_ZN2v88internal15PerfBasicLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci, @function
_ZN2v88internal15PerfBasicLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci:
.LFB20601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal35FLAG_perf_basic_prof_only_functionsE(%rip)
	je	.L604
	movq	-1(%rsi), %rax
	cmpw	$69, 11(%rax)
	je	.L623
.L604:
	movq	-1(%rsi), %rax
	cmpw	$69, 11(%rax)
	je	.L624
	movl	11(%rsi), %ecx
.L612:
	movq	-1(%rsi), %rax
	leaq	53(%rsi), %rdx
	cmpw	$69, 11(%rax)
	je	.L625
.L616:
	movq	24(%r12), %rdi
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v84base2OS6FPrintEP8_IO_FILEPKcz@PLT
.L600:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L626
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L623:
	.cfi_restore_state
	movl	43(%rsi), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$12, %eax
	je	.L604
	movq	-1(%rsi), %rcx
	cmpw	$69, 11(%rcx)
	je	.L605
.L608:
	movq	-1(%rsi), %rax
	cmpw	$69, 11(%rax)
	jne	.L600
	andl	$62, %edx
	je	.L604
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L605:
	cmpl	$3, %eax
	je	.L604
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L624:
	movl	43(%rsi), %edx
	movq	%rsi, -32(%rbp)
	testl	%edx, %edx
	js	.L627
	movl	39(%rsi), %ecx
	movq	-1(%rsi), %rax
	leaq	53(%rsi), %rdx
	cmpw	$69, 11(%rax)
	jne	.L616
	.p2align 4,,10
	.p2align 3
.L625:
	movl	43(%rsi), %eax
	movq	%rsi, -32(%rbp)
	leaq	63(%rsi), %rdx
	testl	%eax, %eax
	jns	.L616
	leaq	-32(%rbp), %rdi
	movl	%r8d, -52(%rbp)
	movq	%r9, -48(%rbp)
	movl	%ecx, -40(%rbp)
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movl	-52(%rbp), %r8d
	movq	-48(%rbp), %r9
	movl	-40(%rbp), %ecx
	movq	%rax, %rdx
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L627:
	leaq	-32(%rbp), %rdi
	movl	%r8d, -52(%rbp)
	movq	%r9, -48(%rbp)
	movq	%rsi, -40(%rbp)
	call	_ZNK2v88internal4Code22OffHeapInstructionSizeEv@PLT
	movq	-40(%rbp), %rsi
	movq	-48(%rbp), %r9
	movl	-52(%rbp), %r8d
	movl	%eax, %ecx
	jmp	.L612
.L626:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20601:
	.size	_ZN2v88internal15PerfBasicLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci, .-_ZN2v88internal15PerfBasicLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci
	.section	.text._ZN2v88internal9JitLogger13CodeMoveEventENS0_12AbstractCodeES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9JitLogger13CodeMoveEventENS0_12AbstractCodeES2_
	.type	_ZN2v88internal9JitLogger13CodeMoveEventENS0_12AbstractCodeES2_, @function
_ZN2v88internal9JitLogger13CodeMoveEventENS0_12AbstractCodeES2_:
.LFB20646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	32(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	$0, -88(%rbp)
	movl	$1, -112(%rbp)
	movq	-1(%rbx), %rax
	cmpw	$69, 11(%rax)
	sete	%al
	movzbl	%al, %eax
	movl	%eax, -108(%rbp)
	movq	-1(%rbx), %rdx
	leaq	53(%rbx), %rax
	cmpw	$69, 11(%rdx)
	je	.L643
.L632:
	movq	%rax, -104(%rbp)
	movq	-1(%rbx), %rax
	cmpw	$69, 11(%rax)
	je	.L644
	movl	11(%rbx), %eax
.L636:
	cltq
	movq	%rax, -96(%rbp)
	movq	-1(%r13), %rdx
	leaq	53(%r13), %rax
	cmpw	$69, 11(%rdx)
	je	.L645
.L640:
	movq	%rax, -72(%rbp)
	movq	8(%r12), %rax
	leaq	-112(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	*24(%r12)
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L646
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L643:
	.cfi_restore_state
	movl	43(%rbx), %esi
	movq	%rbx, -120(%rbp)
	leaq	63(%rbx), %rax
	testl	%esi, %esi
	jns	.L632
	leaq	-120(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L645:
	movl	43(%r13), %edx
	movq	%r13, -120(%rbp)
	leaq	63(%r13), %rax
	testl	%edx, %edx
	jns	.L640
	leaq	-120(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L644:
	movl	43(%rbx), %ecx
	movq	%rbx, -120(%rbp)
	testl	%ecx, %ecx
	js	.L647
	movl	39(%rbx), %eax
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L647:
	leaq	-120(%rbp), %rdi
	call	_ZNK2v88internal4Code22OffHeapInstructionSizeEv@PLT
	jmp	.L636
.L646:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20646:
	.size	_ZN2v88internal9JitLogger13CodeMoveEventENS0_12AbstractCodeES2_, .-_ZN2v88internal9JitLogger13CodeMoveEventENS0_12AbstractCodeES2_
	.section	.text._ZN2v88internal25ExternalCodeEventListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ExternalCodeEventListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.type	_ZN2v88internal25ExternalCodeEventListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE, @function
_ZN2v88internal25ExternalCodeEventListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE:
.LFB20621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -88(%rbp)
	leaq	53(%rsi), %rax
	movq	-1(%rsi), %rdx
	cmpw	$69, 11(%rdx)
	je	.L662
.L652:
	movq	%rax, -104(%rbp)
	movq	-1(%rsi), %rax
	cmpw	$69, 11(%rax)
	je	.L663
	movl	11(%rsi), %eax
.L656:
	movq	16(%r12), %rbx
	cltq
	movq	%rax, -96(%rbp)
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L657
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L658:
	movq	%rax, -88(%rbp)
	movq	16(%r12), %rax
	leaq	-112(%rbp), %rsi
	movq	24(%r12), %rdi
	movq	$0, -72(%rbp)
	subq	$-128, %rax
	movl	$9, -64(%rbp)
	movq	%rax, -80(%rbp)
	leaq	.LC6(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L664
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L657:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L665
.L659:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r13, (%rax)
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L663:
	movl	43(%rsi), %eax
	movq	%rsi, -120(%rbp)
	testl	%eax, %eax
	js	.L666
	movl	39(%rsi), %eax
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L662:
	movl	43(%rsi), %edx
	movq	%rsi, -120(%rbp)
	leaq	63(%rsi), %rax
	testl	%edx, %edx
	jns	.L652
	leaq	-120(%rbp), %rdi
	movq	%rsi, -136(%rbp)
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-136(%rbp), %rsi
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L666:
	leaq	-120(%rbp), %rdi
	call	_ZNK2v88internal4Code22OffHeapInstructionSizeEv@PLT
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L665:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L659
.L664:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20621:
	.size	_ZN2v88internal25ExternalCodeEventListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE, .-_ZN2v88internal25ExternalCodeEventListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.section	.rodata._ZN2v88internal6Logger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi.str1.1,"aMS",@progbits,1
.LC20:
	.string	"code-deopt"
.LC21:
	.string	"<unknown>"
	.section	.text._ZN2v88internal6Logger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.type	_ZN2v88internal6Logger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi, @function
_ZN2v88internal6Logger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi:
.LFB20719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$520, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -520(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	48(%rdi), %rax
	cmpb	$0, (%rax)
	jne	.L667
	cmpq	$0, 8(%rax)
	je	.L667
	movq	%rdi, %r13
	movq	%rsi, %rdi
	movq	%rcx, %rsi
	movl	%edx, %ebx
	call	_ZN2v88internal11Deoptimizer12GetDeoptInfoENS0_4CodeEm@PLT
	leaq	-480(%rbp), %r12
	movq	48(%r13), %rsi
	movq	%r12, %rdi
	movq	%rdx, -488(%rbp)
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %r14
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-504(%rbp), %rdi
	subq	160(%r13), %rax
	movq	%rax, -504(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	%rax, %rsi
	movq	(%r14), %rax
	leaq	16(%rax), %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	-520(%rbp), %rdx
	movq	%rax, %r13
	testb	$1, 43(%rdx)
	jne	.L685
	movl	39(%rdx), %esi
.L670:
	addl	$7, %esi
	movq	0(%r13), %rax
	andl	$-8, %esi
	addl	$95, %esi
	leaq	16(%rax), %rdi
	andl	$-32, %esi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	-520(%rbp), %rsi
	movq	%rax, %r13
	movl	43(%rsi), %edx
	addq	$63, %rsi
	testl	%edx, %edx
	js	.L686
.L674:
	movq	.LC22(%rip), %xmm1
	movq	%r13, %rdi
	leaq	-320(%rbp), %r13
	leaq	-432(%rbp), %r15
	leaq	-368(%rbp), %r14
	movhps	.LC23(%rip), %xmm1
	movaps	%xmm1, -544(%rbp)
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	movq	%r13, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movq	%rax, -432(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-544(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movq	%rax, -320(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -544(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-496(%rbp), %rax
	testb	$1, %al
	jne	.L675
	movq	%rax, %rdx
	shrq	$31, %rax
	shrq	%rdx
	movzwl	%ax, %eax
	andl	$1073741823, %edx
	orl	%eax, %edx
	jne	.L675
	movq	%r15, %rdi
	movl	$9, %edx
	leaq	.LC21(%rip), %rsi
	movl	$-1, %r15d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$-1, %r8d
	.p2align 4,,10
	.p2align 3
.L676:
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%r8d, -524(%rbp)
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	-524(%rbp), %r8d
	movq	%rax, %r9
	movq	(%rax), %rax
	movl	%r8d, %esi
	movq	%r9, -560(%rbp)
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	movq	-560(%rbp), %r9
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	%r15d, %esi
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	%r8, -560(%rbp)
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	movq	-560(%rbp), %r8
	xorl	%esi, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	%ebx, %edi
	leaq	-448(%rbp), %rbx
	call	_ZN2v88internal11Deoptimizer10MessageForENS0_14DeoptimizeKindE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	-384(%rbp), %rax
	movq	%rbx, -464(%rbp)
	leaq	-464(%rbp), %rdi
	movq	$0, -456(%rbp)
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L677
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L678
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L679:
	movq	-464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movzbl	-488(%rbp), %edi
	movq	%rax, %r15
	call	_ZN2v88internal24DeoptimizeReasonToStringENS0_16DeoptimizeReasonE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	-464(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L680
	call	_ZdlPv@PLT
.L680:
	movq	.LC22(%rip), %xmm0
	movq	%r12, %rdi
	movhps	.LC24(%rip), %xmm0
	movaps	%xmm0, -560(%rbp)
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movdqa	-560(%rbp), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-544(%rbp), %rdi
	je	.L681
	call	_ZdlPv@PLT
.L681:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r13, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-472(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L667:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L687
	addq	$520, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L685:
	.cfi_restore_state
	movl	39(%rdx), %eax
	addl	$71, %eax
	andl	$-8, %eax
	cltq
	addq	%rdx, %rax
	addq	$63, %rdx
	movslq	-1(%rax), %rcx
	leaq	7(%rax,%rcx), %rsi
	subl	%edx, %esi
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L675:
	movq	-520(%rbp), %rdx
	movq	%r15, %rsi
	leaq	-496(%rbp), %rdi
	call	_ZNK2v88internal14SourcePosition5PrintERSoNS0_4CodeE@PLT
	movq	-496(%rbp), %rax
	movq	%rax, %r8
	shrq	%rax
	shrq	$31, %r8
	andl	$1073741823, %eax
	movzwl	%r8w, %r8d
	leal	-1(%rax), %r15d
	subl	$1, %r8d
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L678:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L686:
	leaq	-520(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rsi
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L677:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L679
.L687:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20719:
	.size	_ZN2v88internal6Logger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi, .-_ZN2v88internal6Logger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.section	.text._ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.type	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE, @function
_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE:
.LFB20584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZN2v88internalL15kLogEventsNamesE(%rip), %rax
	movq	(%rax,%rsi,8), %r15
	movl	$0, (%rbx)
	movq	%r15, %rdi
	call	strlen@PLT
	movl	$512, %edx
	leaq	4(%rbx), %rcx
	cmpl	$512, %eax
	cmovle	%eax, %edx
	movslq	%edx, %r8
	cmpq	$8, %r8
	jnb	.L689
	testb	$4, %r8b
	jne	.L751
	testq	%r8, %r8
	je	.L690
	movzbl	(%r15), %esi
	movb	%sil, 4(%rbx)
	testb	$2, %r8b
	je	.L690
	movzwl	-2(%r15,%r8), %esi
	movw	%si, -2(%rcx,%r8)
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L689:
	movq	(%r15), %rsi
	leaq	12(%rbx), %rdi
	andq	$-8, %rdi
	movq	%rsi, 4(%rbx)
	movq	-8(%r15,%r8), %rsi
	movq	%rsi, -8(%rcx,%r8)
	subq	%rdi, %rcx
	movq	%r15, %rsi
	subq	%rcx, %rsi
	addq	%r8, %rcx
	shrq	$3, %rcx
	rep movsq
.L690:
	cmpl	$511, %eax
	jg	.L752
	addl	$1, %edx
	movl	%edx, (%rbx)
	movb	$58, 4(%rbx,%r8)
.L694:
	movq	16(%r13), %rbx
	movq	-1(%r14), %rax
	cmpw	$63, 11(%rax)
	ja	.L695
	movq	%r14, -72(%rbp)
	testq	%r14, %r14
	jne	.L753
.L698:
	movq	0(%r13), %rax
	xorl	%edx, %edx
	movl	(%rbx), %r8d
	leaq	4(%rbx), %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*144(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L754
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L695:
	.cfi_restore_state
	movl	(%rbx), %edx
	movl	$512, %eax
	movl	$7, %ecx
	leaq	4(%rbx), %r15
	leaq	.LC15(%rip), %r8
	subl	%edx, %eax
	cmpl	$7, %eax
	cmovg	%ecx, %eax
	movslq	%edx, %rcx
	addq	%r15, %rcx
	movslq	%eax, %rsi
	testq	%rsi, %rsi
	je	.L700
	xorl	%edx, %edx
.L699:
	movzbl	(%r8,%rdx), %edi
	movb	%dil, (%rcx,%rdx)
	addq	$1, %rdx
	cmpq	%rsi, %rdx
	jb	.L699
	movl	(%rbx), %edx
.L700:
	addl	%edx, %eax
	movl	%eax, (%rbx)
	movq	15(%r14), %rdx
	testb	$1, %dl
	jne	.L701
.L707:
	movl	$512, %ecx
	movl	$1, %edx
	leaq	.LC16(%rip), %r8
	subl	%eax, %ecx
	cltq
	testl	%ecx, %ecx
	cmovg	%edx, %ecx
	addq	%r15, %rax
	movslq	%ecx, %rdi
	testq	%rdi, %rdi
	je	.L703
	xorl	%edx, %edx
.L702:
	movzbl	(%r8,%rdx), %esi
	movb	%sil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jb	.L702
.L703:
	addl	%ecx, (%rbx)
	movq	15(%r14), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	jne	.L755
.L708:
	movl	(%rbx), %edx
	movl	$512, %eax
	movl	$2, %ecx
	leaq	.LC17(%rip), %r8
	subl	%edx, %eax
	cmpl	$2, %eax
	cmovg	%ecx, %eax
	movslq	%edx, %rcx
	addq	%r15, %rcx
	movslq	%eax, %rdi
	testq	%rdi, %rdi
	je	.L710
	xorl	%edx, %edx
.L709:
	movzbl	(%r8,%rdx), %esi
	movb	%sil, (%rcx,%rdx)
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jb	.L709
	movl	(%rbx), %edx
.L710:
	addl	%edx, %eax
	movl	%eax, (%rbx)
.L706:
	movl	$512, %edi
	movl	$5, %edx
	leaq	.LC18(%rip), %r8
	subl	%eax, %edi
	cltq
	cmpl	$5, %edi
	cmovg	%edx, %edi
	addq	%r15, %rax
	movslq	%edi, %rcx
	testq	%rcx, %rcx
	je	.L712
	xorl	%edx, %edx
.L711:
	movzbl	(%r8,%rdx), %esi
	movb	%sil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rcx, %rdx
	jb	.L711
.L712:
	addl	(%rbx), %edi
	movl	%edi, (%rbx)
	movl	7(%r14), %ecx
	testb	$1, %cl
	jne	.L713
	shrl	$2, %ecx
.L714:
	movl	$512, %esi
	subl	%edi, %esi
	testl	%esi, %esi
	jle	.L716
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	leaq	.LC11(%rip), %rdx
	xorl	%eax, %eax
	addq	%r15, %rdi
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movl	(%rbx), %edi
	testl	%eax, %eax
	jle	.L716
	addl	%edi, %eax
	cmpl	$512, %eax
	jg	.L716
	movl	%eax, (%rbx)
	movl	%eax, %edi
	.p2align 4,,10
	.p2align 3
.L716:
	cmpl	$511, %edi
	jg	.L750
	leal	1(%rdi), %eax
	movslq	%edi, %rdi
	movl	%eax, (%rbx)
	movb	$41, 4(%rbx,%rdi)
.L750:
	movq	16(%r13), %rbx
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L753:
	xorl	%ecx, %ecx
	leaq	-76(%rbp), %r8
	leaq	-64(%rbp), %rdi
	movl	$1, %edx
	leaq	-72(%rbp), %rsi
	movl	$0, -76(%rbp)
	movl	$512, %r14d
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movslq	(%rbx), %rax
	movq	-64(%rbp), %r15
	subl	%eax, %r14d
	cmpl	%r14d, -76(%rbp)
	cmovle	-76(%rbp), %r14d
	movq	%r15, %rsi
	leaq	4(%rbx,%rax), %rdi
	movslq	%r14d, %rdx
	call	memcpy@PLT
	addl	%r14d, (%rbx)
	movq	%r15, %rdi
	call	_ZdaPv@PLT
	movq	16(%r13), %rbx
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L713:
	leaq	-64(%rbp), %rdi
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movl	(%rbx), %edi
	movl	%eax, %ecx
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L755:
	leaq	-64(%rbp), %rdi
	leaq	-72(%rbp), %rsi
	xorl	%ecx, %ecx
	movl	$1, %edx
	leaq	-76(%rbp), %r8
	movl	$0, -76(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movslq	(%rbx), %rdi
	movl	$512, %ecx
	movq	-64(%rbp), %r8
	subl	%edi, %ecx
	cmpl	%ecx, -76(%rbp)
	cmovle	-76(%rbp), %ecx
	movq	%r8, %rsi
	addq	%r15, %rdi
	movq	%r8, -88(%rbp)
	movslq	%ecx, %rdx
	movl	%ecx, -92(%rbp)
	call	memcpy@PLT
	movl	-92(%rbp), %ecx
	addl	%ecx, (%rbx)
	movq	-88(%rbp), %r8
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L752:
	movl	%edx, (%rbx)
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L751:
	movl	(%r15), %esi
	movl	%esi, 4(%rbx)
	movl	-4(%r15,%r8), %esi
	movl	%esi, -4(%rcx,%r8)
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L701:
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	-37504(%rcx), %rdx
	je	.L706
	jmp	.L707
.L754:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20584:
	.size	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE, .-_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.section	.rodata._ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE.str1.1,"aMS",@progbits,1
.LC25:
	.string	"(location_) != nullptr"
.LC26:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.type	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE, @function
_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE:
.LFB20618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$112, %rsp
	movq	16(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L757
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L758:
	movq	16(%r12), %rdi
	call	_ZN2v88internal4Name14ToFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L773
	pxor	%xmm0, %xmm0
	leaq	53(%rbx), %rax
	movups	%xmm0, -88(%rbp)
	movq	-1(%rbx), %rdx
	cmpw	$69, 11(%rdx)
	je	.L774
.L764:
	movq	%rax, -104(%rbp)
	movq	-1(%rbx), %rax
	cmpw	$69, 11(%rax)
	je	.L775
	movl	11(%rbx), %eax
.L768:
	cltq
	movq	%r14, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	16(%r12), %rax
	movq	$0, -72(%rbp)
	subq	$-128, %rax
	movq	%rax, -80(%rbp)
	cmpl	$22, %r13d
	ja	.L769
	leaq	CSWTCH.451(%rip), %rax
	movq	24(%r12), %rdi
	leaq	-112(%rbp), %rsi
	movl	(%rax,%r13,4), %eax
	movl	%eax, -64(%rbp)
	leaq	.LC6(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L776
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L757:
	.cfi_restore_state
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L777
.L759:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r8, (%rsi)
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L775:
	movl	43(%rbx), %eax
	movq	%rbx, -120(%rbp)
	testl	%eax, %eax
	js	.L778
	movl	39(%rbx), %eax
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L774:
	movl	43(%rbx), %edx
	movq	%rbx, -120(%rbp)
	leaq	63(%rbx), %rax
	testl	%edx, %edx
	jns	.L764
	leaq	-120(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L773:
	leaq	.LC25(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L777:
	movq	%r14, %rdi
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L778:
	leaq	-120(%rbp), %rdi
	call	_ZNK2v88internal4Code22OffHeapInstructionSizeEv@PLT
	jmp	.L768
.L776:
	call	__stack_chk_fail@PLT
.L769:
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20618:
	.size	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE, .-_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.section	.text._ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.type	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE, @function
_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE:
.LFB20617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$104, %rsp
	movq	16(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L780
	movq	%rcx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L781:
	movq	16(%r12), %rdi
	call	_ZN2v88internal4Name14ToFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L796
	pxor	%xmm0, %xmm0
	leaq	53(%rbx), %rax
	movups	%xmm0, -104(%rbp)
	movq	-1(%rbx), %rdx
	cmpw	$69, 11(%rdx)
	je	.L797
.L787:
	movq	%rax, -120(%rbp)
	movq	-1(%rbx), %rax
	cmpw	$69, 11(%rax)
	je	.L798
	movl	11(%rbx), %eax
.L791:
	cltq
	movq	%r14, -104(%rbp)
	movq	%rax, -112(%rbp)
	movq	16(%r12), %rax
	movq	$0, -88(%rbp)
	subq	$-128, %rax
	movq	%rax, -96(%rbp)
	cmpl	$22, %r13d
	ja	.L792
	leaq	CSWTCH.451(%rip), %rax
	movq	24(%r12), %rdi
	leaq	-128(%rbp), %rsi
	movl	(%rax,%r13,4), %eax
	movl	%eax, -80(%rbp)
	leaq	.LC6(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L799
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L780:
	.cfi_restore_state
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L800
.L782:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r15, (%rsi)
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L798:
	movl	43(%rbx), %eax
	movq	%rbx, -136(%rbp)
	testl	%eax, %eax
	js	.L801
	movl	39(%rbx), %eax
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L797:
	movl	43(%rbx), %edx
	movq	%rbx, -136(%rbp)
	leaq	63(%rbx), %rax
	testl	%edx, %edx
	jns	.L787
	leaq	-136(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L796:
	leaq	.LC25(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L800:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L801:
	leaq	-136(%rbp), %rdi
	call	_ZNK2v88internal4Code22OffHeapInstructionSizeEv@PLT
	jmp	.L791
.L799:
	call	__stack_chk_fail@PLT
.L792:
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20617:
	.size	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE, .-_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.section	.text._ZN2v88internal9JitLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9JitLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci
	.type	_ZN2v88internal9JitLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci, @function
_ZN2v88internal9JitLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci:
.LFB20641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	leaq	53(%rsi), %rax
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	-1(%rsi), %rdx
	cmpw	$69, 11(%rdx)
	je	.L819
.L806:
	movq	%rax, -120(%rbp)
	movq	-1(%rsi), %rax
	cmpw	$69, 11(%rax)
	sete	%al
	movzbl	%al, %eax
	movl	%eax, -124(%rbp)
	movq	-1(%rsi), %rax
	cmpw	$69, 11(%rax)
	je	.L820
	movl	11(%rsi), %eax
.L810:
	cltq
	movq	%rax, -112(%rbp)
	testq	%r14, %r14
	jne	.L811
.L813:
	xorl	%eax, %eax
.L812:
	movq	%rax, -104(%rbp)
	movq	8(%r13), %rax
	movslq	%r8d, %r12
	leaq	-128(%rbp), %rdi
	movq	%r15, -88(%rbp)
	movq	%rax, -64(%rbp)
	movq	%r12, -80(%rbp)
	call	*24(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L821
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L811:
	.cfi_restore_state
	movq	31(%r14), %rax
	testb	$1, %al
	je	.L813
	movq	-1(%rax), %rcx
	leaq	-1(%rax), %rdx
	cmpw	$86, 11(%rcx)
	je	.L822
.L814:
	movq	(%rdx), %rax
	cmpw	$96, 11(%rax)
	jne	.L813
	movq	%r14, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L815
	movq	%r14, %rsi
	movl	%r8d, -152(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-152(%rbp), %r8d
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L820:
	movl	43(%rsi), %eax
	movq	%rsi, -136(%rbp)
	testl	%eax, %eax
	js	.L823
	movl	39(%rsi), %eax
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L819:
	movl	43(%rsi), %edx
	movq	%rsi, -136(%rbp)
	leaq	63(%rsi), %rax
	testl	%edx, %edx
	jns	.L806
	leaq	-136(%rbp), %rdi
	movl	%r8d, -156(%rbp)
	movq	%rsi, -152(%rbp)
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movl	-156(%rbp), %r8d
	movq	-152(%rbp), %rsi
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L823:
	leaq	-136(%rbp), %rdi
	movl	%r8d, -152(%rbp)
	call	_ZNK2v88internal4Code22OffHeapInstructionSizeEv@PLT
	movl	-152(%rbp), %r8d
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L822:
	movq	23(%rax), %rdx
	testb	$1, %dl
	je	.L813
	subq	$1, %rdx
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L815:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L824
.L816:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r14, (%rax)
	jmp	.L812
.L824:
	movq	%rbx, %rdi
	movl	%r8d, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-152(%rbp), %r8d
	jmp	.L816
.L821:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20641:
	.size	_ZN2v88internal9JitLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci, .-_ZN2v88internal9JitLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci
	.section	.text._ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.type	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii, @function
_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii:
.LFB20619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$136, %rsp
	movl	%r9d, -148(%rbp)
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	15(%rcx), %rax
	testb	$1, %al
	jne	.L855
.L826:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L827:
	testb	%al, %al
	je	.L831
	movq	15(%r12), %r8
	testb	$1, %r8b
	jne	.L856
.L829:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L832
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L833:
	movq	16(%r15), %rdi
	call	_ZN2v88internal4Name14ToFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L839
	movq	16(%r15), %rdx
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L836
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L837:
	movq	16(%r15), %rdi
	call	_ZN2v88internal4Name14ToFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L839
	pxor	%xmm0, %xmm0
	leaq	53(%rbx), %rax
	movups	%xmm0, -104(%rbp)
	movq	-1(%rbx), %rdx
	cmpw	$69, 11(%rdx)
	je	.L857
.L843:
	movq	%rax, -120(%rbp)
	movq	-1(%rbx), %rax
	cmpw	$69, 11(%rax)
	je	.L858
	movl	11(%rbx), %eax
.L847:
	cltq
	movq	%r12, %xmm0
	movq	%r14, %xmm1
	movq	%rax, -112(%rbp)
	movl	-148(%rbp), %eax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -104(%rbp)
	movl	%eax, -88(%rbp)
	movl	16(%rbp), %eax
	movl	%eax, -84(%rbp)
	cmpl	$22, %r13d
	ja	.L848
	leaq	CSWTCH.451(%rip), %rax
	movq	24(%r15), %rdi
	leaq	-128(%rbp), %rsi
	movl	(%rax,%r13,4), %eax
	movl	%eax, -80(%rbp)
	leaq	.LC6(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L859
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L836:
	.cfi_restore_state
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L860
.L838:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r14, (%rsi)
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L856:
	movq	-1(%r8), %rax
	cmpw	$136, 11(%rax)
	jne	.L829
	leaq	-128(%rbp), %rdi
	movq	%rdx, -160(%rbp)
	movq	%r8, -128(%rbp)
	movq	%r8, -176(%rbp)
	movq	%rdi, -168(%rbp)
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	movq	-160(%rbp), %rdx
	testb	%al, %al
	je	.L831
	movq	-176(%rbp), %r8
	movq	-168(%rbp), %rdi
	movq	%r8, -128(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	-160(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L832:
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L861
.L834:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r8, (%rsi)
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L831:
	andq	$-262144, %r12
	movq	24(%r12), %rax
	movq	-37464(%rax), %r8
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L855:
	movq	-1(%rax), %rcx
	cmpw	$136, 11(%rcx)
	jne	.L826
	leaq	-128(%rbp), %rdi
	movq	%rdx, -160(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	movq	-160(%rbp), %rdx
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L839:
	leaq	.LC25(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L857:
	movl	43(%rbx), %edx
	movq	%rbx, -136(%rbp)
	leaq	63(%rbx), %rax
	testl	%edx, %edx
	jns	.L843
	leaq	-136(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L858:
	movl	43(%rbx), %eax
	movq	%rbx, -136(%rbp)
	testl	%eax, %eax
	js	.L862
	movl	39(%rbx), %eax
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L861:
	movq	%rdx, %rdi
	movq	%r8, -168(%rbp)
	movq	%rdx, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %r8
	movq	-160(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L860:
	movq	%rdx, %rdi
	movq	%rdx, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L862:
	leaq	-136(%rbp), %rdi
	call	_ZNK2v88internal4Code22OffHeapInstructionSizeEv@PLT
	jmp	.L847
.L859:
	call	__stack_chk_fail@PLT
.L848:
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20619:
	.size	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii, .-_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.section	.text._ZN2v88internalL26EnumerateCompiledFunctionsEPNS0_4HeapEPNS0_6HandleINS0_18SharedFunctionInfoEEEPNS3_INS0_12AbstractCodeEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL26EnumerateCompiledFunctionsEPNS0_4HeapEPNS0_6HandleINS0_18SharedFunctionInfoEEEPNS3_INS0_12AbstractCodeEEE, @function
_ZN2v88internalL26EnumerateCompiledFunctionsEPNS0_4HeapEPNS0_6HandleINS0_18SharedFunctionInfoEEEPNS3_INS0_12AbstractCodeEEE:
.LFB20766:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18HeapObjectIteratorC1EPNS0_4HeapENS1_20HeapObjectsFilteringE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	je	.L864
	movabsq	$287762808832, %r15
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L865:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L921
.L894:
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	je	.L864
.L907:
	movq	-1(%rax), %rdx
	cmpw	$160, 11(%rdx)
	jne	.L865
	movq	%rax, -104(%rbp)
	movq	7(%rax), %rax
	cmpq	%r15, %rax
	je	.L894
	testb	$1, %al
	jne	.L867
	movq	-104(%rbp), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L922
.L868:
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L881
.L884:
	movq	-104(%rbp), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L923
.L882:
	leaq	-104(%rbp), %rdi
	call	_ZNK2v88internal18SharedFunctionInfo7GetCodeEv@PLT
	movq	%rax, %rsi
.L892:
	movq	-104(%rbp), %rdi
	movl	%r13d, %r8d
	movq	%r14, %rcx
	movq	%rbx, %rdx
	addl	$1, %r13d
	call	_ZN2v88internalL18AddFunctionAndCodeENS0_18SharedFunctionInfoENS0_12AbstractCodeEPNS0_6HandleIS1_EEPNS3_IS2_EEi
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	jne	.L907
	.p2align 4,,10
	.p2align 3
.L864:
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L924
	addq	$72, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L921:
	.cfi_restore_state
	movq	23(%rax), %rdi
	leaq	23(%rax), %rcx
	movq	31(%rdi), %rdx
	testb	$1, %dl
	jne	.L895
.L899:
	leaq	47(%rax), %rdx
	movq	47(%rax), %rax
	cmpl	$67, 59(%rax)
	je	.L894
	movq	(%rcx), %rax
	movq	7(%rax), %rax
	cmpq	%r15, %rax
	je	.L894
	testb	$1, %al
	jne	.L925
.L904:
	movq	(%rdx), %rax
	testb	$62, 43(%rax)
	jne	.L894
	movq	(%rdx), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	jne	.L894
	movq	(%rdx), %rsi
	movl	%r13d, %r8d
	movq	%r14, %rcx
	movq	%rbx, %rdx
	addl	$1, %r13d
	call	_ZN2v88internalL18AddFunctionAndCodeENS0_18SharedFunctionInfoENS0_12AbstractCodeEPNS0_6HandleIS1_EEPNS3_IS2_EEi
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L867:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L894
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	je	.L894
	movq	-104(%rbp), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	je	.L868
	.p2align 4,,10
	.p2align 3
.L922:
	movq	-1(%rdx), %rsi
	leaq	-1(%rdx), %rcx
	cmpw	$86, 11(%rsi)
	je	.L926
	movq	(%rcx), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L868
.L931:
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L927
.L874:
	movq	7(%rdx), %rdx
	testb	$1, %dl
	je	.L868
	movq	-1(%rdx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L868
	movq	-1(%rdx), %rcx
	movzwl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpw	$2, %cx
	jne	.L868
	movq	-1(%rdx), %rcx
	testb	$8, 11(%rcx)
	je	.L878
.L918:
	cmpq	$0, 15(%rdx)
	setne	%dl
	testb	%dl, %dl
	je	.L894
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L895:
	movq	-1(%rdx), %r8
	leaq	-1(%rdx), %rsi
	cmpw	$86, 11(%r8)
	je	.L928
.L898:
	movq	(%rsi), %rsi
	cmpw	$96, 11(%rsi)
	jne	.L899
	movq	7(%rdx), %rdx
	testb	$1, %dl
	je	.L899
	movq	-1(%rdx), %rsi
	cmpw	$63, 11(%rsi)
	ja	.L899
	movq	-1(%rdx), %rsi
	movzwl	11(%rsi), %esi
	andl	$7, %esi
	cmpw	$2, %si
	jne	.L899
	movq	-1(%rdx), %rsi
	testb	$8, 11(%rsi)
	je	.L900
.L919:
	cmpq	$0, 15(%rdx)
	setne	%dl
	testb	%dl, %dl
	je	.L894
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L881:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L884
.L887:
	movq	-104(%rbp), %rdx
	movq	31(%rdx), %rax
	testb	$1, %al
	jne	.L929
.L885:
	movq	7(%rdx), %rax
	testb	$1, %al
	jne	.L930
.L891:
	movq	-104(%rbp), %rax
	movq	7(%rax), %rax
	movq	7(%rax), %rsi
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L923:
	movq	-1(%rax), %rax
	cmpw	$91, 11(%rax)
	jne	.L882
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L926:
	movq	23(%rdx), %rcx
	testb	$1, %cl
	je	.L868
	movq	-1(%rcx), %rdx
	subq	$1, %rcx
	cmpw	$96, 11(%rdx)
	jne	.L868
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L929:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L885
	movq	39(%rax), %rcx
	testb	$1, %cl
	je	.L885
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L885
	movq	31(%rax), %rsi
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L928:
	movq	23(%rdx), %rdx
	testb	$1, %dl
	je	.L899
	leaq	-1(%rdx), %rsi
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L925:
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L894
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L904
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L930:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L891
	movq	-104(%rbp), %rax
	movq	7(%rax), %rsi
	jmp	.L892
.L927:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L874
	movq	23(%rdx), %rdx
	jmp	.L874
.L878:
	movq	-1(%rdx), %rcx
	testb	$8, 11(%rcx)
	jne	.L868
	jmp	.L918
.L900:
	movq	-1(%rdx), %rsi
	testb	$8, 11(%rsi)
	jne	.L899
	jmp	.L919
.L924:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20766:
	.size	_ZN2v88internalL26EnumerateCompiledFunctionsEPNS0_4HeapEPNS0_6HandleINS0_18SharedFunctionInfoEEEPNS3_INS0_12AbstractCodeEEE, .-_ZN2v88internalL26EnumerateCompiledFunctionsEPNS0_4HeapEPNS0_6HandleINS0_18SharedFunctionInfoEEEPNS3_INS0_12AbstractCodeEEE
	.section	.rodata._ZN2v88internal6Logger19GetterCallbackEventENS0_4NameEm.str1.1,"aMS",@progbits,1
.LC27:
	.string	"Callback"
.LC28:
	.string	"get "
	.section	.text._ZN2v88internal6Logger19GetterCallbackEventENS0_4NameEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger19GetterCallbackEventENS0_4NameEm
	.type	_ZN2v88internal6Logger19GetterCallbackEventENS0_4NameEm, @function
_ZN2v88internal6Logger19GetterCallbackEventENS0_4NameEm:
.LFB20731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	je	.L932
	movq	%rsi, %r13
	movq	48(%rdi), %rsi
	movq	%rdi, %rbx
	cmpb	$0, (%rsi)
	jne	.L932
	cmpq	$0, 8(%rsi)
	je	.L932
	leaq	-80(%rbp), %r15
	movq	%rdx, %r12
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	.LC27(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	$-2, %esi
	movq	%rax, %r14
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %r14
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-88(%rbp), %rdi
	subq	160(%rbx), %rax
	movq	%rax, -88(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	%rax, %rsi
	movq	(%r14), %rax
	leaq	16(%rax), %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	$1, %esi
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	.LC28(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_4NameEEERS2_T_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L932:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L937
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L937:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20731:
	.size	_ZN2v88internal6Logger19GetterCallbackEventENS0_4NameEm, .-_ZN2v88internal6Logger19GetterCallbackEventENS0_4NameEm
	.section	.rodata._ZN2v88internal6Logger19SetterCallbackEventENS0_4NameEm.str1.1,"aMS",@progbits,1
.LC29:
	.string	"set "
	.section	.text._ZN2v88internal6Logger19SetterCallbackEventENS0_4NameEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger19SetterCallbackEventENS0_4NameEm
	.type	_ZN2v88internal6Logger19SetterCallbackEventENS0_4NameEm, @function
_ZN2v88internal6Logger19SetterCallbackEventENS0_4NameEm:
.LFB20732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	je	.L938
	movq	%rsi, %r13
	movq	48(%rdi), %rsi
	movq	%rdi, %rbx
	cmpb	$0, (%rsi)
	jne	.L938
	cmpq	$0, 8(%rsi)
	je	.L938
	leaq	-80(%rbp), %r15
	movq	%rdx, %r12
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	.LC27(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	$-2, %esi
	movq	%rax, %r14
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %r14
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-88(%rbp), %rdi
	subq	160(%rbx), %rax
	movq	%rax, -88(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	%rax, %rsi
	movq	(%r14), %rax
	leaq	16(%rax), %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	$1, %esi
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	.LC29(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_4NameEEERS2_T_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L938:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L943
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L943:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20732:
	.size	_ZN2v88internal6Logger19SetterCallbackEventENS0_4NameEm, .-_ZN2v88internal6Logger19SetterCallbackEventENS0_4NameEm
	.section	.text._ZN2v88internal6Logger13CallbackEventENS0_4NameEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger13CallbackEventENS0_4NameEm
	.type	_ZN2v88internal6Logger13CallbackEventENS0_4NameEm, @function
_ZN2v88internal6Logger13CallbackEventENS0_4NameEm:
.LFB20730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	je	.L944
	movq	%rsi, %r13
	movq	48(%rdi), %rsi
	movq	%rdi, %rbx
	cmpb	$0, (%rsi)
	jne	.L944
	cmpq	$0, 8(%rsi)
	je	.L944
	leaq	-80(%rbp), %r15
	movq	%rdx, %r12
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	.LC27(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	$-2, %esi
	movq	%rax, %r14
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %r14
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-88(%rbp), %rdi
	subq	160(%rbx), %rax
	movq	%rax, -88(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	%rax, %rsi
	movq	(%r14), %rax
	leaq	16(%rax), %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	$1, %esi
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_4NameEEERS2_T_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L944:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L949
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L949:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20730:
	.size	_ZN2v88internal6Logger13CallbackEventENS0_4NameEm, .-_ZN2v88internal6Logger13CallbackEventENS0_4NameEm
	.section	.text._ZN2v88internal15CodeEventLoggerC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15CodeEventLoggerC2EPNS0_7IsolateE
	.type	_ZN2v88internal15CodeEventLoggerC2EPNS0_7IsolateE, @function
_ZN2v88internal15CodeEventLoggerC2EPNS0_7IsolateE:
.LFB20577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	%rsi, 8(%rdi)
	movl	$516, %edi
	call	_Znwm@PLT
	movl	$0, (%rax)
	movq	%rax, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20577:
	.size	_ZN2v88internal15CodeEventLoggerC2EPNS0_7IsolateE, .-_ZN2v88internal15CodeEventLoggerC2EPNS0_7IsolateE
	.globl	_ZN2v88internal15CodeEventLoggerC1EPNS0_7IsolateE
	.set	_ZN2v88internal15CodeEventLoggerC1EPNS0_7IsolateE,_ZN2v88internal15CodeEventLoggerC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal15CodeEventLoggerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15CodeEventLoggerD2Ev
	.type	_ZN2v88internal15CodeEventLoggerD2Ev, @function
_ZN2v88internal15CodeEventLoggerD2Ev:
.LFB20580:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L952
	movl	$516, %esi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L952:
	ret
	.cfi_endproc
.LFE20580:
	.size	_ZN2v88internal15CodeEventLoggerD2Ev, .-_ZN2v88internal15CodeEventLoggerD2Ev
	.globl	_ZN2v88internal15CodeEventLoggerD1Ev
	.set	_ZN2v88internal15CodeEventLoggerD1Ev,_ZN2v88internal15CodeEventLoggerD2Ev
	.section	.text._ZN2v88internal15CodeEventLoggerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15CodeEventLoggerD0Ev
	.type	_ZN2v88internal15CodeEventLoggerD0Ev, @function
_ZN2v88internal15CodeEventLoggerD0Ev:
.LFB20582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L955
	movl	$516, %esi
	call	_ZdlPvm@PLT
.L955:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE20582:
	.size	_ZN2v88internal15CodeEventLoggerD0Ev, .-_ZN2v88internal15CodeEventLoggerD0Ev
	.section	.rodata._ZN2v88internal15PerfBasicLoggerC2EPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC30:
	.string	"NewArray"
.LC31:
	.string	"size != -1"
	.section	.rodata._ZN2v88internal15PerfBasicLoggerC2EPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC32:
	.string	"(perf_output_handle_) != nullptr"
	.section	.text._ZN2v88internal15PerfBasicLoggerC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15PerfBasicLoggerC2EPNS0_7IsolateE
	.type	_ZN2v88internal15PerfBasicLoggerC2EPNS0_7IsolateE, @function
_ZN2v88internal15PerfBasicLoggerC2EPNS0_7IsolateE:
.LFB20594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movl	$516, %edi
	call	_Znwm@PLT
	movq	$0, 24(%rbx)
	movl	$33, %edi
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$0, (%rax)
	movq	%rax, 16(%rbx)
	leaq	16+_ZTVN2v88internal15PerfBasicLoggerE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L966
	movq	%rax, %r12
.L961:
	movq	$33, -24(%rbp)
	call	_ZN2v84base2OS19GetCurrentProcessIdEv@PLT
	movq	-24(%rbp), %rsi
	leaq	_ZN2v88internal15PerfBasicLogger21kFilenameFormatStringE(%rip), %rdx
	movq	%r12, %rdi
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	cmpl	$-1, %eax
	je	.L967
	movq	_ZN2v84base2OS15LogFileOpenModeE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 24(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L968
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%esi, %esi
	call	setvbuf@PLT
	addq	$16, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L967:
	.cfi_restore_state
	leaq	.LC31(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L968:
	leaq	.LC32(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L966:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$33, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L961
	leaq	.LC30(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE20594:
	.size	_ZN2v88internal15PerfBasicLoggerC2EPNS0_7IsolateE, .-_ZN2v88internal15PerfBasicLoggerC2EPNS0_7IsolateE
	.globl	_ZN2v88internal15PerfBasicLoggerC1EPNS0_7IsolateE
	.set	_ZN2v88internal15PerfBasicLoggerC1EPNS0_7IsolateE,_ZN2v88internal15PerfBasicLoggerC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal15PerfBasicLogger22WriteLogRecordedBufferEmiPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15PerfBasicLogger22WriteLogRecordedBufferEmiPKci
	.type	_ZN2v88internal15PerfBasicLogger22WriteLogRecordedBufferEmiPKci, @function
_ZN2v88internal15PerfBasicLogger22WriteLogRecordedBufferEmiPKci:
.LFB20600:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	movq	%rcx, %r9
	xorl	%eax, %eax
	movl	%edx, %ecx
	movq	%rsi, %rdx
	leaq	.LC0(%rip), %rsi
	jmp	_ZN2v84base2OS6FPrintEP8_IO_FILEPKcz@PLT
	.cfi_endproc
.LFE20600:
	.size	_ZN2v88internal15PerfBasicLogger22WriteLogRecordedBufferEmiPKci, .-_ZN2v88internal15PerfBasicLogger22WriteLogRecordedBufferEmiPKci
	.section	.text._ZN2v88internal25ExternalCodeEventListenerC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ExternalCodeEventListenerC2EPNS0_7IsolateE
	.type	_ZN2v88internal25ExternalCodeEventListenerC2EPNS0_7IsolateE, @function
_ZN2v88internal25ExternalCodeEventListenerC2EPNS0_7IsolateE:
.LFB20604:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal25ExternalCodeEventListenerE(%rip), %rax
	movb	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rsi, 16(%rdi)
	movq	$0, 24(%rdi)
	ret
	.cfi_endproc
.LFE20604:
	.size	_ZN2v88internal25ExternalCodeEventListenerC2EPNS0_7IsolateE, .-_ZN2v88internal25ExternalCodeEventListenerC2EPNS0_7IsolateE
	.globl	_ZN2v88internal25ExternalCodeEventListenerC1EPNS0_7IsolateE
	.set	_ZN2v88internal25ExternalCodeEventListenerC1EPNS0_7IsolateE,_ZN2v88internal25ExternalCodeEventListenerC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal25ExternalCodeEventListener13StopListeningEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ExternalCodeEventListener13StopListeningEv
	.type	_ZN2v88internal25ExternalCodeEventListener13StopListeningEv, @function
_ZN2v88internal25ExternalCodeEventListener13StopListeningEv:
.LFB20612:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rdi)
	jne	.L996
	ret
	.p2align 4,,10
	.p2align 3
.L996:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rax
	movq	41488(%rax), %r12
	leaq	56(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%r12), %r8
	movq	%rbx, %rax
	xorl	%edx, %edx
	movq	(%r12), %r14
	divq	%r8
	leaq	0(,%rdx,8), %rax
	movq	%rdx, %r10
	leaq	(%r14,%rax), %r15
	movq	%rax, -56(%rbp)
	movq	(%r15), %r11
	testq	%r11, %r11
	je	.L973
	movq	(%r11), %rdi
	movq	%r11, %r9
	movq	8(%rdi), %rcx
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L997:
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L973
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r9
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L973
	movq	%rsi, %rdi
.L975:
	cmpq	%rcx, %rbx
	jne	.L997
	movq	(%rdi), %rcx
	cmpq	%r9, %r11
	je	.L998
	testq	%rcx, %rcx
	je	.L977
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r10
	je	.L977
	movq	%r9, (%r14,%rdx,8)
	movq	(%rdi), %rcx
.L977:
	movq	%rcx, (%r9)
	call	_ZdlPv@PLT
	subq	$1, 24(%r12)
.L973:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movb	$0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L998:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L981
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r10
	je	.L977
	movq	%r9, (%r14,%rdx,8)
	movq	-56(%rbp), %r15
	addq	(%r12), %r15
	movq	(%r15), %rax
.L976:
	leaq	16(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L999
.L978:
	movq	$0, (%r15)
	movq	(%rdi), %rcx
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L981:
	movq	%r9, %rax
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L999:
	movq	%rcx, 16(%r12)
	jmp	.L978
	.cfi_endproc
.LFE20612:
	.size	_ZN2v88internal25ExternalCodeEventListener13StopListeningEv, .-_ZN2v88internal25ExternalCodeEventListener13StopListeningEv
	.section	.text._ZN2v88internal14LowLevelLoggerC2EPNS0_7IsolateEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LowLevelLoggerC2EPNS0_7IsolateEPKc
	.type	_ZN2v88internal14LowLevelLoggerC2EPNS0_7IsolateEPKc, @function
_ZN2v88internal14LowLevelLoggerC2EPNS0_7IsolateEPKc:
.LFB20625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movl	$516, %edi
	call	_Znwm@PLT
	movq	$0, 24(%rbx)
	movq	%r14, %rdi
	movl	$0, (%rax)
	movq	%rax, 16(%rbx)
	leaq	16+_ZTVN2v88internal14LowLevelLoggerE(%rip), %rax
	movq	%rax, (%rbx)
	call	strlen@PLT
	leaq	_ZSt7nothrow(%rip), %rsi
	leal	4(%rax), %r15d
	movq	%rax, %r13
	movslq	%r15d, %r15
	movq	%r15, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L1005
	movq	%rax, %r12
.L1001:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	memcpy@PLT
	movl	_ZN2v88internal14LowLevelLogger7kLogExtE(%rip), %eax
	movq	_ZN2v84base2OS15LogFileOpenModeE(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, (%r12,%r13)
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, 24(%rbx)
	movq	%rax, %rdi
	call	setvbuf@PLT
	movq	24(%rbx), %rcx
	leaq	-60(%rbp), %rdi
	movl	$4, %edx
	movl	$1, %esi
	movl	$3421816, -60(%rbp)
	call	fwrite@PLT
	movq	%r12, %rdi
	call	_ZdaPv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1006
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1005:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r15, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L1001
	leaq	.LC30(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L1006:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20625:
	.size	_ZN2v88internal14LowLevelLoggerC2EPNS0_7IsolateEPKc, .-_ZN2v88internal14LowLevelLoggerC2EPNS0_7IsolateEPKc
	.globl	_ZN2v88internal14LowLevelLoggerC1EPNS0_7IsolateEPKc
	.set	_ZN2v88internal14LowLevelLoggerC1EPNS0_7IsolateEPKc,_ZN2v88internal14LowLevelLoggerC2EPNS0_7IsolateEPKc
	.section	.text._ZN2v88internal14LowLevelLogger11LogCodeInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LowLevelLogger11LogCodeInfoEv
	.type	_ZN2v88internal14LowLevelLogger11LogCodeInfoEv, @function
_ZN2v88internal14LowLevelLogger11LogCodeInfoEv:
.LFB20631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %edx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	24(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rdi
	movl	$3421816, -12(%rbp)
	call	fwrite@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1010
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1010:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20631:
	.size	_ZN2v88internal14LowLevelLogger11LogCodeInfoEv, .-_ZN2v88internal14LowLevelLogger11LogCodeInfoEv
	.section	.text._ZN2v88internal14LowLevelLogger13LogWriteBytesEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LowLevelLogger13LogWriteBytesEPKci
	.type	_ZN2v88internal14LowLevelLogger13LogWriteBytesEPKci, @function
_ZN2v88internal14LowLevelLogger13LogWriteBytesEPKci:
.LFB20635:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movslq	%edx, %rdx
	movq	%rsi, %rdi
	movl	$1, %esi
	movq	24(%r8), %rcx
	jmp	fwrite@PLT
	.cfi_endproc
.LFE20635:
	.size	_ZN2v88internal14LowLevelLogger13LogWriteBytesEPKci, .-_ZN2v88internal14LowLevelLogger13LogWriteBytesEPKci
	.section	.text._ZN2v88internal9JitLoggerC2EPNS0_7IsolateEPFvPKNS_12JitCodeEventEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9JitLoggerC2EPNS0_7IsolateEPFvPKNS_12JitCodeEventEE
	.type	_ZN2v88internal9JitLoggerC2EPNS0_7IsolateEPFvPKNS_12JitCodeEventEE, @function
_ZN2v88internal9JitLoggerC2EPNS0_7IsolateEPFvPKNS_12JitCodeEventEE:
.LFB20639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	%rsi, 8(%rdi)
	movl	$516, %edi
	call	_Znwm@PLT
	movq	%r12, 24(%rbx)
	leaq	32(%rbx), %rdi
	movl	$0, (%rax)
	movq	%rax, 16(%rbx)
	leaq	16+_ZTVN2v88internal9JitLoggerE(%rip), %rax
	movq	%rax, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5MutexC1Ev@PLT
	.cfi_endproc
.LFE20639:
	.size	_ZN2v88internal9JitLoggerC2EPNS0_7IsolateEPFvPKNS_12JitCodeEventEE, .-_ZN2v88internal9JitLoggerC2EPNS0_7IsolateEPFvPKNS_12JitCodeEventEE
	.globl	_ZN2v88internal9JitLoggerC1EPNS0_7IsolateEPFvPKNS_12JitCodeEventEE
	.set	_ZN2v88internal9JitLoggerC1EPNS0_7IsolateEPFvPKNS_12JitCodeEventEE,_ZN2v88internal9JitLoggerC2EPNS0_7IsolateEPFvPKNS_12JitCodeEventEE
	.section	.text._ZN2v88internal9JitLogger23AddCodeLinePosInfoEventEPviiNS_12JitCodeEvent12PositionTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9JitLogger23AddCodeLinePosInfoEventEPviiNS_12JitCodeEvent12PositionTypeE
	.type	_ZN2v88internal9JitLogger23AddCodeLinePosInfoEventEPviiNS_12JitCodeEvent12PositionTypeE, @function
_ZN2v88internal9JitLogger23AddCodeLinePosInfoEventEPviiNS_12JitCodeEvent12PositionTypeE:
.LFB20647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	pxor	%xmm0, %xmm0
	movslq	%ecx, %rcx
	movq	%rcx, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rdi
	movq	%rdi, -8(%rbp)
	xorl	%edi, %edi
	movq	%rsi, -48(%rbp)
	movslq	%edx, %rsi
	movq	8(%rax), %rdx
	movaps	%xmm0, -32(%rbp)
	leaq	-80(%rbp), %rdi
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movl	$3, -80(%rbp)
	movl	%r8d, -24(%rbp)
	movq	%rdx, -16(%rbp)
	movups	%xmm0, -40(%rbp)
	call	*24(%rax)
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1017
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1017:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20647:
	.size	_ZN2v88internal9JitLogger23AddCodeLinePosInfoEventEPviiNS_12JitCodeEvent12PositionTypeE, .-_ZN2v88internal9JitLogger23AddCodeLinePosInfoEventEPviiNS_12JitCodeEvent12PositionTypeE
	.section	.text._ZN2v88internal9JitLogger21StartCodePosInfoEventEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9JitLogger21StartCodePosInfoEventEv
	.type	_ZN2v88internal9JitLogger21StartCodePosInfoEventEv, @function
_ZN2v88internal9JitLogger21StartCodePosInfoEventEv:
.LFB20648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	8(%rax), %rdx
	movq	%fs:40, %rcx
	movq	%rcx, -8(%rbp)
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %rdi
	movaps	%xmm0, -80(%rbp)
	movq	%rdx, -16(%rbp)
	movl	$4, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	movaps	%xmm0, -48(%rbp)
	movaps	%xmm0, -32(%rbp)
	call	*24(%rax)
	movq	-48(%rbp), %rax
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L1021
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1021:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20648:
	.size	_ZN2v88internal9JitLogger21StartCodePosInfoEventEv, .-_ZN2v88internal9JitLogger21StartCodePosInfoEventEv
	.section	.text._ZN2v88internal9JitLogger19EndCodePosInfoEventEmPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9JitLogger19EndCodePosInfoEventEmPv
	.type	_ZN2v88internal9JitLogger19EndCodePosInfoEventEmPv, @function
_ZN2v88internal9JitLogger19EndCodePosInfoEventEmPv:
.LFB20649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rcx
	movq	%rcx, -8(%rbp)
	xorl	%ecx, %ecx
	movaps	%xmm0, -48(%rbp)
	leaq	-80(%rbp), %rdi
	movq	%rdx, -48(%rbp)
	movq	8(%rax), %rdx
	movaps	%xmm0, -80(%rbp)
	movq	%rdx, -16(%rbp)
	movl	$5, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movaps	%xmm0, -64(%rbp)
	movaps	%xmm0, -32(%rbp)
	call	*24(%rax)
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1025
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1025:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20649:
	.size	_ZN2v88internal9JitLogger19EndCodePosInfoEventEmPv, .-_ZN2v88internal9JitLogger19EndCodePosInfoEventEmPv
	.section	.rodata._ZN2v88internal8ProfilerC2EPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC33:
	.string	"v8:Profiler"
	.section	.text._ZN2v88internal8ProfilerC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8ProfilerC2EPNS0_7IsolateE
	.type	_ZN2v88internal8ProfilerC2EPNS0_7IsolateE, @function
_ZN2v88internal8ProfilerC2EPNS0_7IsolateE:
.LFB20670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	leaq	-48(%rbp), %rsi
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	.LC33(%rip), %rax
	movl	$0, -40(%rbp)
	movq	%rax, -48(%rbp)
	call	_ZN2v84base6ThreadC2ERKNS1_7OptionsE@PLT
	movq	%r12, 48(%rbx)
	leaq	16+_ZTVN2v88internal8ProfilerE(%rip), %rax
	leaq	529464(%rbx), %rcx
	movq	%rax, (%rbx)
	pxor	%xmm0, %xmm0
	leaq	56(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L1027:
	movzwl	4112(%rax), %edx
	movl	$5, (%rax)
	addq	$4136, %rax
	movups	%xmm0, -4128(%rax)
	andw	$-1024, %dx
	movq	$0, -32(%rax)
	orb	$2, %dh
	movups	%xmm0, -16(%rax)
	movw	%dx, -24(%rax)
	cmpq	%rcx, %rax
	jne	.L1027
	movb	$0, 529472(%rbx)
	xorl	%esi, %esi
	leaq	529480(%rbx), %rdi
	movl	$0, 529464(%rbx)
	call	_ZN2v84base9SemaphoreC1Ei@PLT
	movl	$0, 529468(%rbx)
	movl	$0, 529512(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1031
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1031:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20670:
	.size	_ZN2v88internal8ProfilerC2EPNS0_7IsolateE, .-_ZN2v88internal8ProfilerC2EPNS0_7IsolateE
	.globl	_ZN2v88internal8ProfilerC1EPNS0_7IsolateE
	.set	_ZN2v88internal8ProfilerC1EPNS0_7IsolateE,_ZN2v88internal8ProfilerC2EPNS0_7IsolateE
	.section	.rodata._ZN2v88internal8Profiler9DisengageEv.str1.1,"aMS",@progbits,1
.LC34:
	.string	"profiler"
.LC35:
	.string	"end"
	.section	.text._ZN2v88internal8Profiler9DisengageEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Profiler9DisengageEv
	.type	_ZN2v88internal8Profiler9DisengageEv, @function
_ZN2v88internal8Profiler9DisengageEv:
.LFB20682:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	48(%rdi), %rax
	movq	41016(%rax), %rax
	movq	16(%rax), %r12
	movq	$0, 48(%r12)
	movzbl	32(%r12), %eax
	testb	%al, %al
	jne	.L1043
.L1033:
	movq	56(%r12), %rdi
	call	_ZN2v84base6Thread4JoinEv@PLT
	movl	$0, 529512(%rbx)
	movslq	529464(%rbx), %rcx
	leal	1(%rcx), %eax
	cltd
	shrl	$25, %edx
	addl	%edx, %eax
	andl	$127, %eax
	subl	%edx, %eax
	movl	529468(%rbx), %edx
	cmpl	%edx, %eax
	jne	.L1034
	movb	$1, 529472(%rbx)
.L1035:
	movq	%rbx, %rdi
	call	_ZN2v84base6Thread4JoinEv@PLT
	movq	48(%rbx), %rax
	movq	41016(%rax), %rax
	movq	8(%rax), %rdx
	cmpb	$0, 41812(%rdx)
	jne	.L1032
	cmpb	$0, 40(%rax)
	jne	.L1044
.L1032:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1045
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1044:
	.cfi_restore_state
	movq	48(%rax), %rsi
	cmpb	$0, (%rsi)
	jne	.L1032
	cmpq	$0, 8(%rsi)
	je	.L1032
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r12, %rdi
	leaq	.LC34(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	.LC35(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-40(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1034:
	imulq	$4136, %rcx, %rcx
	pxor	%xmm0, %xmm0
	leaq	529480(%rbx), %rdi
	leaq	56(%rbx,%rcx), %rdx
	leaq	4160(%rbx,%rcx), %rsi
	movl	$5, (%rdx)
	movq	$0, 4104(%rdx)
	movb	$0, 4112(%rdx)
	movups	%xmm0, 8(%rdx)
	movzbl	9(%rsi), %ecx
	andl	$-4, %ecx
	orl	$2, %ecx
	movb	%cl, 9(%rsi)
	movups	%xmm0, 4120(%rdx)
	movl	%eax, 529464(%rbx)
	call	_ZN2v84base9Semaphore6SignalEv@PLT
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1043:
	movq	%r12, %rdi
	call	_ZN2v87sampler7Sampler4StopEv@PLT
	jmp	.L1033
.L1045:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20682:
	.size	_ZN2v88internal8Profiler9DisengageEv, .-_ZN2v88internal8Profiler9DisengageEv
	.section	.text._ZN2v88internal6LoggerC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6LoggerC2EPNS0_7IsolateE
	.type	_ZN2v88internal6LoggerC2EPNS0_7IsolateE, @function
_ZN2v88internal6LoggerC2EPNS0_7IsolateE:
.LFB20697:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal6LoggerE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	leaq	96(%rdi), %rax
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 32(%rdi)
	movb	$0, 40(%rdi)
	movq	$0, 80(%rdi)
	movl	$0, 96(%rdi)
	movq	$0, 104(%rdi)
	movq	%rax, 112(%rdi)
	movq	%rax, 120(%rdi)
	movq	$0, 128(%rdi)
	movl	$0, 136(%rdi)
	movb	$0, 140(%rdi)
	movq	%rsi, 144(%rdi)
	movq	$0, 152(%rdi)
	movq	$0, 160(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	ret
	.cfi_endproc
.LFE20697:
	.size	_ZN2v88internal6LoggerC2EPNS0_7IsolateE, .-_ZN2v88internal6LoggerC2EPNS0_7IsolateE
	.globl	_ZN2v88internal6LoggerC1EPNS0_7IsolateE
	.set	_ZN2v88internal6LoggerC1EPNS0_7IsolateE,_ZN2v88internal6LoggerC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal6Logger23RemoveCodeEventListenerEPNS0_17CodeEventListenerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger23RemoveCodeEventListenerEPNS0_17CodeEventListenerE
	.type	_ZN2v88internal6Logger23RemoveCodeEventListenerEPNS0_17CodeEventListenerE, @function
_ZN2v88internal6Logger23RemoveCodeEventListenerEPNS0_17CodeEventListenerE:
.LFB20707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rax
	movq	41488(%rax), %r12
	leaq	56(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%r12), %rcx
	movq	%rbx, %rax
	xorl	%edx, %edx
	movq	(%r12), %r14
	divq	%rcx
	leaq	0(,%rdx,8), %rax
	leaq	(%r14,%rax), %r15
	movq	%rax, -56(%rbp)
	movq	(%r15), %r8
	testq	%r8, %r8
	je	.L1048
	movq	(%r8), %rdi
	movq	%rdx, %r11
	movq	%r8, %r10
	movq	8(%rdi), %rsi
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1068:
	movq	(%rdi), %r9
	testq	%r9, %r9
	je	.L1048
	movq	8(%r9), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L1048
	movq	%r9, %rdi
.L1050:
	cmpq	%rsi, %rbx
	jne	.L1068
	movq	(%rdi), %rsi
	cmpq	%r10, %r8
	je	.L1069
	testq	%rsi, %rsi
	je	.L1052
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L1052
	movq	%r10, (%r14,%rdx,8)
	movq	(%rdi), %rsi
.L1052:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 24(%r12)
.L1048:
	addq	$24, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L1069:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L1056
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L1052
	movq	%r10, (%r14,%rdx,8)
	movq	-56(%rbp), %r15
	addq	(%r12), %r15
	movq	(%r15), %rax
.L1051:
	leaq	16(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L1070
.L1053:
	movq	$0, (%r15)
	movq	(%rdi), %rsi
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1056:
	movq	%r10, %rax
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1070:
	movq	%rsi, 16(%r12)
	jmp	.L1053
	.cfi_endproc
.LFE20707:
	.size	_ZN2v88internal6Logger23RemoveCodeEventListenerEPNS0_17CodeEventListenerE, .-_ZN2v88internal6Logger23RemoveCodeEventListenerEPNS0_17CodeEventListenerE
	.section	.rodata._ZN2v88internal6Logger18ProfilerBeginEventEv.str1.1,"aMS",@progbits,1
.LC36:
	.string	"begin"
	.section	.text._ZN2v88internal6Logger18ProfilerBeginEventEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger18ProfilerBeginEventEv
	.type	_ZN2v88internal6Logger18ProfilerBeginEventEv, @function
_ZN2v88internal6Logger18ProfilerBeginEventEv:
.LFB20708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1071
	cmpq	$0, 8(%rsi)
	je	.L1071
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	leaq	.LC34(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	.LC36(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	_ZN2v88internal27FLAG_prof_sampling_intervalE(%rip), %esi
	movq	(%rax), %rdi
	addq	$16, %rdi
	call	_ZNSolsEi@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-40(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L1071:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1076
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1076:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20708:
	.size	_ZN2v88internal6Logger18ProfilerBeginEventEv, .-_ZN2v88internal6Logger18ProfilerBeginEventEv
	.section	.text._ZN2v88internal6Logger11StringEventEPKcS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger11StringEventEPKcS3_
	.type	_ZN2v88internal6Logger11StringEventEPKcS3_, @function
_ZN2v88internal6Logger11StringEventEPKcS3_:
.LFB20712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$48, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal8FLAG_logE(%rip)
	jne	.L1082
.L1077:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1083
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1082:
	.cfi_restore_state
	movq	%rsi, %r12
	movq	48(%rdi), %rsi
	movq	%rdx, -56(%rbp)
	cmpb	$0, (%rsi)
	jne	.L1077
	cmpq	$0, 8(%rsi)
	je	.L1077
	leaq	-48(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-40(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1077
.L1083:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20712:
	.size	_ZN2v88internal6Logger11StringEventEPKcS3_, .-_ZN2v88internal6Logger11StringEventEPKcS3_
	.section	.text._ZN2v88internal6Logger20UncheckedStringEventEPKcS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger20UncheckedStringEventEPKcS3_
	.type	_ZN2v88internal6Logger20UncheckedStringEventEPKcS3_, @function
_ZN2v88internal6Logger20UncheckedStringEventEPKcS3_:
.LFB20713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -40
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1084
	cmpq	$0, 8(%rsi)
	je	.L1084
	leaq	-64(%rbp), %r14
	movq	%rdx, %r12
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L1084:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1089
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1089:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20713:
	.size	_ZN2v88internal6Logger20UncheckedStringEventEPKcS3_, .-_ZN2v88internal6Logger20UncheckedStringEventEPKcS3_
	.section	.rodata._ZN2v88internal6Logger12IntPtrTEventEPKcl.str1.1,"aMS",@progbits,1
.LC37:
	.string	"%ld"
	.section	.text._ZN2v88internal6Logger12IntPtrTEventEPKcl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger12IntPtrTEventEPKcl
	.type	_ZN2v88internal6Logger12IntPtrTEventEPKcl, @function
_ZN2v88internal6Logger12IntPtrTEventEPKcl:
.LFB20714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$48, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal8FLAG_logE(%rip)
	jne	.L1095
.L1090:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1096
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1095:
	.cfi_restore_state
	movq	%rsi, %r12
	movq	48(%rdi), %rsi
	movq	%rdx, -56(%rbp)
	cmpb	$0, (%rsi)
	jne	.L1090
	cmpq	$0, 8(%rsi)
	je	.L1090
	leaq	-48(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	-56(%rbp), %rdx
	movq	%r13, %rdi
	xorl	%eax, %eax
	leaq	.LC37(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilder18AppendFormatStringEPKcz@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-40(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1090
.L1096:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20714:
	.size	_ZN2v88internal6Logger12IntPtrTEventEPKcl, .-_ZN2v88internal6Logger12IntPtrTEventEPKcl
	.section	.text._ZN2v88internal6Logger21UncheckedIntPtrTEventEPKcl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger21UncheckedIntPtrTEventEPKcl
	.type	_ZN2v88internal6Logger21UncheckedIntPtrTEventEPKcl, @function
_ZN2v88internal6Logger21UncheckedIntPtrTEventEPKcl:
.LFB20715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -40
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1097
	cmpq	$0, 8(%rsi)
	je	.L1097
	leaq	-64(%rbp), %r14
	movq	%rdx, %r12
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC37(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilder18AppendFormatStringEPKcz@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L1097:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1102
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1102:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20715:
	.size	_ZN2v88internal6Logger21UncheckedIntPtrTEventEPKcl, .-_ZN2v88internal6Logger21UncheckedIntPtrTEventEPKcl
	.section	.text._ZN2v88internal6Logger11HandleEventEPKcPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger11HandleEventEPKcPm
	.type	_ZN2v88internal6Logger11HandleEventEPKcPm, @function
_ZN2v88internal6Logger11HandleEventEPKcPm:
.LFB20716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$40, %rsp
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1103
	cmpq	$0, 8(%rsi)
	je	.L1103
	cmpb	$0, _ZN2v88internal16FLAG_log_handlesE(%rip)
	jne	.L1108
	.p2align 4,,10
	.p2align 3
.L1103:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1109
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1108:
	.cfi_restore_state
	leaq	-64(%rbp), %r13
	movq	%rdx, %r14
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1103
.L1109:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20716:
	.size	_ZN2v88internal6Logger11HandleEventEPKcPm, .-_ZN2v88internal6Logger11HandleEventEPKcPm
	.section	.rodata._ZN2v88internal6Logger16ApiSecurityCheckEv.str1.1,"aMS",@progbits,1
.LC38:
	.string	"api"
.LC39:
	.string	"check-security"
	.section	.text._ZN2v88internal6Logger16ApiSecurityCheckEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger16ApiSecurityCheckEv
	.type	_ZN2v88internal6Logger16ApiSecurityCheckEv, @function
_ZN2v88internal6Logger16ApiSecurityCheckEv:
.LFB20717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1110
	cmpq	$0, 8(%rsi)
	je	.L1110
	cmpb	$0, _ZN2v88internal12FLAG_log_apiE(%rip)
	jne	.L1115
	.p2align 4,,10
	.p2align 3
.L1110:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1116
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1115:
	.cfi_restore_state
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r12, %rdi
	leaq	.LC38(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	.LC39(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-40(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1110
.L1116:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20717:
	.size	_ZN2v88internal6Logger16ApiSecurityCheckEv, .-_ZN2v88internal6Logger16ApiSecurityCheckEv
	.section	.rodata._ZN2v88internal6Logger18SharedLibraryEventERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmml.str1.1,"aMS",@progbits,1
.LC40:
	.string	"shared-library"
	.section	.text._ZN2v88internal6Logger18SharedLibraryEventERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmml,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger18SharedLibraryEventERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmml
	.type	_ZN2v88internal6Logger18SharedLibraryEventERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmml, @function
_ZN2v88internal6Logger18SharedLibraryEventERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmml:
.LFB20718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1117
	cmpq	$0, 8(%rsi)
	je	.L1117
	cmpb	$0, _ZN2v88internal13FLAG_prof_cppE(%rip)
	jne	.L1122
	.p2align 4,,10
	.p2align 3
.L1117:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1123
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1122:
	.cfi_restore_state
	leaq	-80(%rbp), %r12
	movq	%rdx, %r15
	movq	%rcx, %r14
	movq	%r8, %r13
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r12, %rdi
	leaq	.LC40(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r13, %rsi
	movq	(%rax), %rdi
	addq	$16, %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1117
.L1123:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20718:
	.size	_ZN2v88internal6Logger18SharedLibraryEventERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmml, .-_ZN2v88internal6Logger18SharedLibraryEventERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmml
	.section	.rodata._ZN2v88internal8Profiler6EngageEv.str1.1,"aMS",@progbits,1
.LC41:
	.string	"Start()"
	.section	.text._ZN2v88internal8Profiler6EngageEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Profiler6EngageEv
	.type	_ZN2v88internal8Profiler6EngageEv, @function
_ZN2v88internal8Profiler6EngageEv:
.LFB20672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	-64(%rbp), %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base2OS25GetSharedLibraryAddressesEv@PLT
	movq	-56(%rbp), %r13
	movq	-64(%rbp), %r12
	cmpq	%r13, %r12
	jne	.L1129
	jmp	.L1130
	.p2align 4,,10
	.p2align 3
.L1128:
	addq	$56, %r12
	cmpq	%r12, %r13
	je	.L1130
.L1129:
	movq	48(%rbx), %rax
	movq	41016(%rax), %rdi
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L1128
	cmpb	$0, 40(%rdi)
	je	.L1128
	movq	40(%r12), %rcx
	movq	32(%r12), %rdx
	movq	%r12, %rsi
	addq	$56, %r12
	movq	-8(%r12), %r8
	call	_ZN2v88internal6Logger18SharedLibraryEventERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmml
	cmpq	%r12, %r13
	jne	.L1129
	.p2align 4,,10
	.p2align 3
.L1130:
	movl	$1, 529512(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v84base6Thread5StartEv@PLT
	testb	%al, %al
	je	.L1154
	movq	48(%rbx), %rax
	movq	41016(%rax), %r13
	movq	16(%r13), %r12
	movq	%rbx, 48(%r12)
	movzbl	32(%r12), %eax
	testb	%al, %al
	je	.L1155
.L1131:
	movl	$32, %edi
	movq	56(%r12), %r12
	call	_Znwm@PLT
	xorl	%esi, %esi
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZN2v84base9SemaphoreC1Ei@PLT
	movq	%rbx, 40(%r12)
	movq	%r12, %rdi
	call	_ZN2v84base6Thread5StartEv@PLT
	testb	%al, %al
	jne	.L1156
.L1132:
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger18ProfilerBeginEventEv
	movq	-56(%rbp), %rbx
	movq	-64(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1134
	.p2align 4,,10
	.p2align 3
.L1138:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1135
	call	_ZdlPv@PLT
	addq	$56, %r12
	cmpq	%r12, %rbx
	jne	.L1138
.L1136:
	movq	-64(%rbp), %r12
.L1134:
	testq	%r12, %r12
	je	.L1124
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1124:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1157
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1135:
	.cfi_restore_state
	addq	$56, %r12
	cmpq	%r12, %rbx
	jne	.L1138
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1156:
	movq	40(%r12), %rdi
	call	_ZN2v84base9Semaphore4WaitEv@PLT
	movq	40(%r12), %r14
	testq	%r14, %r14
	je	.L1133
	movq	%r14, %rdi
	call	_ZN2v84base9SemaphoreD1Ev@PLT
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1133:
	movq	$0, 40(%r12)
	jmp	.L1132
	.p2align 4,,10
	.p2align 3
.L1155:
	movq	%r12, %rdi
	call	_ZN2v87sampler7Sampler5StartEv@PLT
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1154:
	leaq	.LC41(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20672:
	.size	_ZN2v88internal8Profiler6EngageEv, .-_ZN2v88internal8Profiler6EngageEv
	.section	.rodata._ZN2v88internal6Logger16CurrentTimeEventEv.str1.1,"aMS",@progbits,1
.LC42:
	.string	"current-time"
	.section	.text._ZN2v88internal6Logger16CurrentTimeEventEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger16CurrentTimeEventEv
	.type	_ZN2v88internal6Logger16CurrentTimeEventEv, @function
_ZN2v88internal6Logger16CurrentTimeEventEv:
.LFB20720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1158
	cmpq	$0, 8(%rsi)
	je	.L1158
	leaq	-64(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %r13
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-72(%rbp), %rdi
	subq	160(%rbx), %rax
	movq	%rax, -72(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	0(%r13), %rdi
	movq	%rax, %rsi
	addq	$16, %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L1158:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1163
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1163:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20720:
	.size	_ZN2v88internal6Logger16CurrentTimeEventEv, .-_ZN2v88internal6Logger16CurrentTimeEventEv
	.section	.rodata._ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc.str1.1,"aMS",@progbits,1
.LC43:
	.string	"timer-event-start"
.LC44:
	.string	"timer-event-end"
.LC45:
	.string	"timer-event"
	.section	.text._ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc
	.type	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc, @function
_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc:
.LFB20721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1164
	cmpq	$0, 8(%rsi)
	je	.L1164
	leaq	-64(%rbp), %r14
	movq	%rdi, %rbx
	movq	%rdx, %r13
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	cmpl	$1, %r12d
	je	.L1166
	cmpl	$2, %r12d
	je	.L1167
	testl	%r12d, %r12d
	je	.L1173
.L1169:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %r12
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-72(%rbp), %rdi
	subq	160(%rbx), %rax
	movq	%rax, -72(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	(%r12), %rdi
	movq	%rax, %rsi
	addq	$16, %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L1164:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1174
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1166:
	.cfi_restore_state
	leaq	.LC44(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1173:
	leaq	.LC43(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1167:
	leaq	.LC45(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	jmp	.L1169
.L1174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20721:
	.size	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc, .-_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc
	.section	.text._ZN2v88internal6Logger10is_loggingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger10is_loggingEv
	.type	_ZN2v88internal6Logger10is_loggingEv, @function
_ZN2v88internal6Logger10is_loggingEv:
.LFB20722:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	cmpb	$0, 41812(%rdx)
	jne	.L1175
	movzbl	40(%rdi), %eax
.L1175:
	ret
	.cfi_endproc
.LFE20722:
	.size	_ZN2v88internal6Logger10is_loggingEv, .-_ZN2v88internal6Logger10is_loggingEv
	.section	.rodata._ZN2v88internal15TimerEventScopeINS0_30TimerEventRecompileSynchronousEE13LogTimerEventENS0_6Logger8StartEndE.str1.1,"aMS",@progbits,1
.LC46:
	.string	"V8.RecompileSynchronous"
	.section	.text._ZN2v88internal15TimerEventScopeINS0_30TimerEventRecompileSynchronousEE13LogTimerEventENS0_6Logger8StartEndE,"axG",@progbits,_ZN2v88internal15TimerEventScopeINS0_30TimerEventRecompileSynchronousEE13LogTimerEventENS0_6Logger8StartEndE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15TimerEventScopeINS0_30TimerEventRecompileSynchronousEE13LogTimerEventENS0_6Logger8StartEndE
	.type	_ZN2v88internal15TimerEventScopeINS0_30TimerEventRecompileSynchronousEE13LogTimerEventENS0_6Logger8StartEndE, @function
_ZN2v88internal15TimerEventScopeINS0_30TimerEventRecompileSynchronousEE13LogTimerEventENS0_6Logger8StartEndE:
.LFB23230:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	41632(%rdx), %rax
	testq	%rax, %rax
	je	.L1178
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rcx
	cmpq	%rcx, %rax
	je	.L1184
	leaq	.LC46(%rip), %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1184:
	movq	41016(%rdx), %rdi
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L1178
	cmpb	$0, 40(%rdi)
	jne	.L1185
.L1178:
	ret
	.p2align 4,,10
	.p2align 3
.L1185:
	leaq	.LC46(%rip), %rdx
	jmp	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc
	.cfi_endproc
.LFE23230:
	.size	_ZN2v88internal15TimerEventScopeINS0_30TimerEventRecompileSynchronousEE13LogTimerEventENS0_6Logger8StartEndE, .-_ZN2v88internal15TimerEventScopeINS0_30TimerEventRecompileSynchronousEE13LogTimerEventENS0_6Logger8StartEndE
	.section	.rodata._ZN2v88internal15TimerEventScopeINS0_29TimerEventRecompileConcurrentEE13LogTimerEventENS0_6Logger8StartEndE.str1.1,"aMS",@progbits,1
.LC47:
	.string	"V8.RecompileConcurrent"
	.section	.text._ZN2v88internal15TimerEventScopeINS0_29TimerEventRecompileConcurrentEE13LogTimerEventENS0_6Logger8StartEndE,"axG",@progbits,_ZN2v88internal15TimerEventScopeINS0_29TimerEventRecompileConcurrentEE13LogTimerEventENS0_6Logger8StartEndE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15TimerEventScopeINS0_29TimerEventRecompileConcurrentEE13LogTimerEventENS0_6Logger8StartEndE
	.type	_ZN2v88internal15TimerEventScopeINS0_29TimerEventRecompileConcurrentEE13LogTimerEventENS0_6Logger8StartEndE, @function
_ZN2v88internal15TimerEventScopeINS0_29TimerEventRecompileConcurrentEE13LogTimerEventENS0_6Logger8StartEndE:
.LFB23231:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	41632(%rdx), %rax
	testq	%rax, %rax
	je	.L1186
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rcx
	cmpq	%rcx, %rax
	je	.L1192
	leaq	.LC47(%rip), %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1192:
	movq	41016(%rdx), %rdi
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L1186
	cmpb	$0, 40(%rdi)
	jne	.L1193
.L1186:
	ret
	.p2align 4,,10
	.p2align 3
.L1193:
	leaq	.LC47(%rip), %rdx
	jmp	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc
	.cfi_endproc
.LFE23231:
	.size	_ZN2v88internal15TimerEventScopeINS0_29TimerEventRecompileConcurrentEE13LogTimerEventENS0_6Logger8StartEndE, .-_ZN2v88internal15TimerEventScopeINS0_29TimerEventRecompileConcurrentEE13LogTimerEventENS0_6Logger8StartEndE
	.section	.rodata._ZN2v88internal15TimerEventScopeINS0_25TimerEventCompileIgnitionEE13LogTimerEventENS0_6Logger8StartEndE.str1.1,"aMS",@progbits,1
.LC48:
	.string	"V8.CompileIgnition"
	.section	.text._ZN2v88internal15TimerEventScopeINS0_25TimerEventCompileIgnitionEE13LogTimerEventENS0_6Logger8StartEndE,"axG",@progbits,_ZN2v88internal15TimerEventScopeINS0_25TimerEventCompileIgnitionEE13LogTimerEventENS0_6Logger8StartEndE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15TimerEventScopeINS0_25TimerEventCompileIgnitionEE13LogTimerEventENS0_6Logger8StartEndE
	.type	_ZN2v88internal15TimerEventScopeINS0_25TimerEventCompileIgnitionEE13LogTimerEventENS0_6Logger8StartEndE, @function
_ZN2v88internal15TimerEventScopeINS0_25TimerEventCompileIgnitionEE13LogTimerEventENS0_6Logger8StartEndE:
.LFB23232:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	41632(%rdx), %rax
	testq	%rax, %rax
	je	.L1194
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rcx
	cmpq	%rcx, %rax
	je	.L1200
	leaq	.LC48(%rip), %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1200:
	movq	41016(%rdx), %rdi
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L1194
	cmpb	$0, 40(%rdi)
	jne	.L1201
.L1194:
	ret
	.p2align 4,,10
	.p2align 3
.L1201:
	leaq	.LC48(%rip), %rdx
	jmp	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc
	.cfi_endproc
.LFE23232:
	.size	_ZN2v88internal15TimerEventScopeINS0_25TimerEventCompileIgnitionEE13LogTimerEventENS0_6Logger8StartEndE, .-_ZN2v88internal15TimerEventScopeINS0_25TimerEventCompileIgnitionEE13LogTimerEventENS0_6Logger8StartEndE
	.section	.rodata._ZN2v88internal15TimerEventScopeINS0_25TimerEventCompileFullCodeEE13LogTimerEventENS0_6Logger8StartEndE.str1.1,"aMS",@progbits,1
.LC49:
	.string	"V8.CompileFullCode"
	.section	.text._ZN2v88internal15TimerEventScopeINS0_25TimerEventCompileFullCodeEE13LogTimerEventENS0_6Logger8StartEndE,"axG",@progbits,_ZN2v88internal15TimerEventScopeINS0_25TimerEventCompileFullCodeEE13LogTimerEventENS0_6Logger8StartEndE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15TimerEventScopeINS0_25TimerEventCompileFullCodeEE13LogTimerEventENS0_6Logger8StartEndE
	.type	_ZN2v88internal15TimerEventScopeINS0_25TimerEventCompileFullCodeEE13LogTimerEventENS0_6Logger8StartEndE, @function
_ZN2v88internal15TimerEventScopeINS0_25TimerEventCompileFullCodeEE13LogTimerEventENS0_6Logger8StartEndE:
.LFB23233:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	41632(%rdx), %rax
	testq	%rax, %rax
	je	.L1202
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rcx
	cmpq	%rcx, %rax
	je	.L1208
	leaq	.LC49(%rip), %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1208:
	movq	41016(%rdx), %rdi
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L1202
	cmpb	$0, 40(%rdi)
	jne	.L1209
.L1202:
	ret
	.p2align 4,,10
	.p2align 3
.L1209:
	leaq	.LC49(%rip), %rdx
	jmp	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc
	.cfi_endproc
.LFE23233:
	.size	_ZN2v88internal15TimerEventScopeINS0_25TimerEventCompileFullCodeEE13LogTimerEventENS0_6Logger8StartEndE, .-_ZN2v88internal15TimerEventScopeINS0_25TimerEventCompileFullCodeEE13LogTimerEventENS0_6Logger8StartEndE
	.section	.rodata._ZN2v88internal15TimerEventScopeINS0_22TimerEventOptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE.str1.1,"aMS",@progbits,1
.LC50:
	.string	"V8.OptimizeCode"
	.section	.text._ZN2v88internal15TimerEventScopeINS0_22TimerEventOptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE,"axG",@progbits,_ZN2v88internal15TimerEventScopeINS0_22TimerEventOptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15TimerEventScopeINS0_22TimerEventOptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE
	.type	_ZN2v88internal15TimerEventScopeINS0_22TimerEventOptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE, @function
_ZN2v88internal15TimerEventScopeINS0_22TimerEventOptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE:
.LFB23234:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	41632(%rdx), %rax
	testq	%rax, %rax
	je	.L1210
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rcx
	cmpq	%rcx, %rax
	je	.L1216
	leaq	.LC50(%rip), %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1216:
	movq	41016(%rdx), %rdi
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L1210
	cmpb	$0, 40(%rdi)
	jne	.L1217
.L1210:
	ret
	.p2align 4,,10
	.p2align 3
.L1217:
	leaq	.LC50(%rip), %rdx
	jmp	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc
	.cfi_endproc
.LFE23234:
	.size	_ZN2v88internal15TimerEventScopeINS0_22TimerEventOptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE, .-_ZN2v88internal15TimerEventScopeINS0_22TimerEventOptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE
	.section	.rodata._ZN2v88internal15TimerEventScopeINS0_21TimerEventCompileCodeEE13LogTimerEventENS0_6Logger8StartEndE.str1.1,"aMS",@progbits,1
.LC51:
	.string	"V8.CompileCode"
	.section	.text._ZN2v88internal15TimerEventScopeINS0_21TimerEventCompileCodeEE13LogTimerEventENS0_6Logger8StartEndE,"axG",@progbits,_ZN2v88internal15TimerEventScopeINS0_21TimerEventCompileCodeEE13LogTimerEventENS0_6Logger8StartEndE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15TimerEventScopeINS0_21TimerEventCompileCodeEE13LogTimerEventENS0_6Logger8StartEndE
	.type	_ZN2v88internal15TimerEventScopeINS0_21TimerEventCompileCodeEE13LogTimerEventENS0_6Logger8StartEndE, @function
_ZN2v88internal15TimerEventScopeINS0_21TimerEventCompileCodeEE13LogTimerEventENS0_6Logger8StartEndE:
.LFB23235:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	41632(%rdx), %rax
	testq	%rax, %rax
	je	.L1218
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rcx
	cmpq	%rcx, %rax
	je	.L1224
	leaq	.LC51(%rip), %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1224:
	movq	41016(%rdx), %rdi
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L1218
	cmpb	$0, 40(%rdi)
	jne	.L1225
.L1218:
	ret
	.p2align 4,,10
	.p2align 3
.L1225:
	leaq	.LC51(%rip), %rdx
	jmp	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc
	.cfi_endproc
.LFE23235:
	.size	_ZN2v88internal15TimerEventScopeINS0_21TimerEventCompileCodeEE13LogTimerEventENS0_6Logger8StartEndE, .-_ZN2v88internal15TimerEventScopeINS0_21TimerEventCompileCodeEE13LogTimerEventENS0_6Logger8StartEndE
	.section	.rodata._ZN2v88internal15TimerEventScopeINS0_31TimerEventCompileCodeBackgroundEE13LogTimerEventENS0_6Logger8StartEndE.str1.1,"aMS",@progbits,1
.LC52:
	.string	"V8.CompileCodeBackground"
	.section	.text._ZN2v88internal15TimerEventScopeINS0_31TimerEventCompileCodeBackgroundEE13LogTimerEventENS0_6Logger8StartEndE,"axG",@progbits,_ZN2v88internal15TimerEventScopeINS0_31TimerEventCompileCodeBackgroundEE13LogTimerEventENS0_6Logger8StartEndE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15TimerEventScopeINS0_31TimerEventCompileCodeBackgroundEE13LogTimerEventENS0_6Logger8StartEndE
	.type	_ZN2v88internal15TimerEventScopeINS0_31TimerEventCompileCodeBackgroundEE13LogTimerEventENS0_6Logger8StartEndE, @function
_ZN2v88internal15TimerEventScopeINS0_31TimerEventCompileCodeBackgroundEE13LogTimerEventENS0_6Logger8StartEndE:
.LFB23236:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	41632(%rdx), %rax
	testq	%rax, %rax
	je	.L1226
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rcx
	cmpq	%rcx, %rax
	je	.L1232
	leaq	.LC52(%rip), %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1232:
	movq	41016(%rdx), %rdi
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L1226
	cmpb	$0, 40(%rdi)
	jne	.L1233
.L1226:
	ret
	.p2align 4,,10
	.p2align 3
.L1233:
	leaq	.LC52(%rip), %rdx
	jmp	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc
	.cfi_endproc
.LFE23236:
	.size	_ZN2v88internal15TimerEventScopeINS0_31TimerEventCompileCodeBackgroundEE13LogTimerEventENS0_6Logger8StartEndE, .-_ZN2v88internal15TimerEventScopeINS0_31TimerEventCompileCodeBackgroundEE13LogTimerEventENS0_6Logger8StartEndE
	.section	.rodata._ZN2v88internal15TimerEventScopeINS0_24TimerEventDeoptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE.str1.1,"aMS",@progbits,1
.LC53:
	.string	"V8.DeoptimizeCode"
	.section	.text._ZN2v88internal15TimerEventScopeINS0_24TimerEventDeoptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE,"axG",@progbits,_ZN2v88internal15TimerEventScopeINS0_24TimerEventDeoptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15TimerEventScopeINS0_24TimerEventDeoptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE
	.type	_ZN2v88internal15TimerEventScopeINS0_24TimerEventDeoptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE, @function
_ZN2v88internal15TimerEventScopeINS0_24TimerEventDeoptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE:
.LFB23237:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	41632(%rdx), %rax
	testq	%rax, %rax
	je	.L1234
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rcx
	cmpq	%rcx, %rax
	je	.L1240
	leaq	.LC53(%rip), %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1240:
	movq	41016(%rdx), %rdi
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L1234
	cmpb	$0, 40(%rdi)
	jne	.L1241
.L1234:
	ret
	.p2align 4,,10
	.p2align 3
.L1241:
	leaq	.LC53(%rip), %rdx
	jmp	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc
	.cfi_endproc
.LFE23237:
	.size	_ZN2v88internal15TimerEventScopeINS0_24TimerEventDeoptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE, .-_ZN2v88internal15TimerEventScopeINS0_24TimerEventDeoptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE
	.section	.rodata._ZN2v88internal15TimerEventScopeINS0_17TimerEventExecuteEE13LogTimerEventENS0_6Logger8StartEndE.str1.1,"aMS",@progbits,1
.LC54:
	.string	"V8.Execute"
	.section	.text._ZN2v88internal15TimerEventScopeINS0_17TimerEventExecuteEE13LogTimerEventENS0_6Logger8StartEndE,"axG",@progbits,_ZN2v88internal15TimerEventScopeINS0_17TimerEventExecuteEE13LogTimerEventENS0_6Logger8StartEndE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15TimerEventScopeINS0_17TimerEventExecuteEE13LogTimerEventENS0_6Logger8StartEndE
	.type	_ZN2v88internal15TimerEventScopeINS0_17TimerEventExecuteEE13LogTimerEventENS0_6Logger8StartEndE, @function
_ZN2v88internal15TimerEventScopeINS0_17TimerEventExecuteEE13LogTimerEventENS0_6Logger8StartEndE:
.LFB23238:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	41632(%rdx), %rax
	testq	%rax, %rax
	je	.L1242
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rcx
	cmpq	%rcx, %rax
	je	.L1248
	leaq	.LC54(%rip), %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1248:
	movq	41016(%rdx), %rdi
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L1242
	cmpb	$0, 40(%rdi)
	jne	.L1249
.L1242:
	ret
	.p2align 4,,10
	.p2align 3
.L1249:
	leaq	.LC54(%rip), %rdx
	jmp	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc
	.cfi_endproc
.LFE23238:
	.size	_ZN2v88internal15TimerEventScopeINS0_17TimerEventExecuteEE13LogTimerEventENS0_6Logger8StartEndE, .-_ZN2v88internal15TimerEventScopeINS0_17TimerEventExecuteEE13LogTimerEventENS0_6Logger8StartEndE
	.section	.text._ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE
	.type	_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE, @function
_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE:
.LFB20723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$56, %rsp
	movq	%rdx, -72(%rbp)
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1250
	cmpq	$0, 8(%rsi)
	je	.L1250
	cmpb	$0, _ZN2v88internal12FLAG_log_apiE(%rip)
	jne	.L1255
	.p2align 4,,10
	.p2align 3
.L1250:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1256
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1255:
	.cfi_restore_state
	leaq	-64(%rbp), %r13
	movq	%rcx, %r14
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r13, %rdi
	leaq	.LC38(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	-72(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal10JSReceiver10class_nameEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_4NameEEERS2_T_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1250
.L1256:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20723:
	.size	_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE, .-_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE
	.section	.text._ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj
	.type	_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj, @function
_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj:
.LFB20724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%rdx, -72(%rbp)
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1257
	cmpq	$0, 8(%rsi)
	je	.L1257
	cmpb	$0, _ZN2v88internal12FLAG_log_apiE(%rip)
	jne	.L1262
	.p2align 4,,10
	.p2align 3
.L1257:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1263
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1262:
	.cfi_restore_state
	leaq	-64(%rbp), %r13
	movl	%ecx, %ebx
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r13, %rdi
	leaq	.LC38(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	-72(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal10JSReceiver10class_nameEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	%ebx, %esi
	movq	(%rax), %rdi
	addq	$16, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1257
.L1263:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20724:
	.size	_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj, .-_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj
	.section	.text._ZN2v88internal6Logger15ApiObjectAccessEPKcNS0_8JSObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger15ApiObjectAccessEPKcNS0_8JSObjectE
	.type	_ZN2v88internal6Logger15ApiObjectAccessEPKcNS0_8JSObjectE, @function
_ZN2v88internal6Logger15ApiObjectAccessEPKcNS0_8JSObjectE:
.LFB20725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$48, %rsp
	movq	%rdx, -56(%rbp)
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1264
	cmpq	$0, 8(%rsi)
	je	.L1264
	cmpb	$0, _ZN2v88internal12FLAG_log_apiE(%rip)
	jne	.L1269
	.p2align 4,,10
	.p2align 3
.L1264:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1270
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1269:
	.cfi_restore_state
	leaq	-48(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r13, %rdi
	leaq	.LC38(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	-56(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal10JSReceiver10class_nameEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-40(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1264
.L1270:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20725:
	.size	_ZN2v88internal6Logger15ApiObjectAccessEPKcNS0_8JSObjectE, .-_ZN2v88internal6Logger15ApiObjectAccessEPKcNS0_8JSObjectE
	.section	.text._ZN2v88internal6Logger12ApiEntryCallEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger12ApiEntryCallEPKc
	.type	_ZN2v88internal6Logger12ApiEntryCallEPKc, @function
_ZN2v88internal6Logger12ApiEntryCallEPKc:
.LFB20726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$32, %rsp
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1271
	cmpq	$0, 8(%rsi)
	je	.L1271
	cmpb	$0, _ZN2v88internal12FLAG_log_apiE(%rip)
	jne	.L1276
	.p2align 4,,10
	.p2align 3
.L1271:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1277
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1276:
	.cfi_restore_state
	leaq	-48(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r13, %rdi
	leaq	.LC38(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-40(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1271
.L1277:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20726:
	.size	_ZN2v88internal6Logger12ApiEntryCallEPKc, .-_ZN2v88internal6Logger12ApiEntryCallEPKc
	.section	.rodata._ZN2v88internal6Logger8NewEventEPKcPvm.str1.1,"aMS",@progbits,1
.LC55:
	.string	"new"
	.section	.text._ZN2v88internal6Logger8NewEventEPKcPvm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger8NewEventEPKcPvm
	.type	_ZN2v88internal6Logger8NewEventEPKcPvm, @function
_ZN2v88internal6Logger8NewEventEPKcPvm:
.LFB20727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1278
	cmpq	$0, 8(%rsi)
	je	.L1278
	cmpb	$0, _ZN2v88internal8FLAG_logE(%rip)
	jne	.L1283
	.p2align 4,,10
	.p2align 3
.L1278:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1284
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1283:
	.cfi_restore_state
	leaq	-64(%rbp), %r13
	movq	%rdx, %r14
	movq	%rcx, %rbx
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r13, %rdi
	leaq	.LC55(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	%ebx, %esi
	movq	(%rax), %rdi
	addq	$16, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1278
.L1284:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20727:
	.size	_ZN2v88internal6Logger8NewEventEPKcPvm, .-_ZN2v88internal6Logger8NewEventEPKcPvm
	.section	.rodata._ZN2v88internal6Logger11DeleteEventEPKcPv.str1.1,"aMS",@progbits,1
.LC56:
	.string	"delete"
	.section	.text._ZN2v88internal6Logger11DeleteEventEPKcPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger11DeleteEventEPKcPv
	.type	_ZN2v88internal6Logger11DeleteEventEPKcPv, @function
_ZN2v88internal6Logger11DeleteEventEPKcPv:
.LFB20728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$40, %rsp
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1285
	cmpq	$0, 8(%rsi)
	je	.L1285
	cmpb	$0, _ZN2v88internal8FLAG_logE(%rip)
	jne	.L1290
	.p2align 4,,10
	.p2align 3
.L1285:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1291
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1290:
	.cfi_restore_state
	leaq	-64(%rbp), %r13
	movq	%rdx, %r14
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r13, %rdi
	leaq	.LC56(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1285
.L1291:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20728:
	.size	_ZN2v88internal6Logger11DeleteEventEPKcPv, .-_ZN2v88internal6Logger11DeleteEventEPKcPv
	.section	.text._ZN2v88internal6Logger21CallbackEventInternalEPKcNS0_4NameEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger21CallbackEventInternalEPKcNS0_4NameEm
	.type	_ZN2v88internal6Logger21CallbackEventInternalEPKcNS0_4NameEm, @function
_ZN2v88internal6Logger21CallbackEventInternalEPKcNS0_4NameEm:
.LFB20729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	je	.L1292
	movq	%rsi, %r14
	movq	48(%rdi), %rsi
	movq	%rdi, %rbx
	cmpb	$0, (%rsi)
	jne	.L1292
	cmpq	$0, 8(%rsi)
	je	.L1292
	leaq	-80(%rbp), %r8
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%r8, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	-104(%rbp), %r8
	leaq	.LC1(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	.LC27(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	$-2, %esi
	movq	%rax, %r15
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %r15
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-88(%rbp), %rdi
	subq	160(%rbx), %rax
	movq	%rax, -88(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	%rax, %rsi
	movq	(%r15), %rax
	leaq	16(%rax), %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	$1, %esi
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_4NameEEERS2_T_@PLT
	movq	-104(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L1292:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1297
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1297:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20729:
	.size	_ZN2v88internal6Logger21CallbackEventInternalEPKcNS0_4NameEm, .-_ZN2v88internal6Logger21CallbackEventInternalEPKcNS0_4NameEm
	.section	.text._ZN2v88internal6Logger26CodeLinePosInfoRecordEventEmNS0_9ByteArrayE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger26CodeLinePosInfoRecordEventEmNS0_9ByteArrayE
	.type	_ZN2v88internal6Logger26CodeLinePosInfoRecordEventEmNS0_9ByteArrayE, @function
_ZN2v88internal6Logger26CodeLinePosInfoRecordEventEmNS0_9ByteArrayE:
.LFB20745:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r13
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	xorl	%edx, %edx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_9ByteArrayENS1_15IterationFilterE@PLT
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1298
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_116CodeLinePosEventEPNS0_9JitLoggerEmRNS0_27SourcePositionTableIteratorE.part.0
.L1298:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1305
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1305:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20745:
	.size	_ZN2v88internal6Logger26CodeLinePosInfoRecordEventEmNS0_9ByteArrayE, .-_ZN2v88internal6Logger26CodeLinePosInfoRecordEventEmNS0_9ByteArrayE
	.section	.text._ZN2v88internal6Logger26CodeLinePosInfoRecordEventEmNS0_6VectorIKhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger26CodeLinePosInfoRecordEventEmNS0_6VectorIKhEE
	.type	_ZN2v88internal6Logger26CodeLinePosInfoRecordEventEmNS0_6VectorIKhEE, @function
_ZN2v88internal6Logger26CodeLinePosInfoRecordEventEmNS0_6VectorIKhEE:
.LFB20746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rcx, %rdx
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r13
	movq	%rsi, %r12
	movq	%rax, %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_6VectorIKhEENS1_15IterationFilterE@PLT
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1306
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal12_GLOBAL__N_116CodeLinePosEventEPNS0_9JitLoggerEmRNS0_27SourcePositionTableIteratorE.part.0
.L1306:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1313
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1313:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20746:
	.size	_ZN2v88internal6Logger26CodeLinePosInfoRecordEventEmNS0_6VectorIKhEE, .-_ZN2v88internal6Logger26CodeLinePosInfoRecordEventEmNS0_6VectorIKhEE
	.section	.rodata._ZN2v88internal6Logger13CodeNameEventEmiPKc.str1.1,"aMS",@progbits,1
.LC57:
	.string	"snapshot-code-name"
	.section	.text._ZN2v88internal6Logger13CodeNameEventEmiPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger13CodeNameEventEmiPKc
	.type	_ZN2v88internal6Logger13CodeNameEventEmiPKc, @function
_ZN2v88internal6Logger13CodeNameEventEmiPKc:
.LFB20747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L1314
	movq	48(%rdi), %rsi
	leaq	-64(%rbp), %r14
	movl	%edx, %r15d
	movq	%rcx, %r12
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	leaq	.LC57(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	%r15d, %esi
	movq	%rax, %r13
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L1314:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1319
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1319:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20747:
	.size	_ZN2v88internal6Logger13CodeNameEventEmiPKc, .-_ZN2v88internal6Logger13CodeNameEventEmiPKc
	.section	.text._ZN2v88internal6Logger17MoveEventInternalENS0_17CodeEventListener16LogEventsAndTagsEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger17MoveEventInternalENS0_17CodeEventListener16LogEventsAndTagsEmm
	.type	_ZN2v88internal6Logger17MoveEventInternalENS0_17CodeEventListener16LogEventsAndTagsEmm, @function
_ZN2v88internal6Logger17MoveEventInternalENS0_17CodeEventListener16LogEventsAndTagsEmm:
.LFB20749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	je	.L1320
	movl	%esi, %ebx
	movq	48(%rdi), %rsi
	cmpb	$0, (%rsi)
	jne	.L1320
	cmpq	$0, 8(%rsi)
	je	.L1320
	leaq	-64(%rbp), %r14
	movq	%rdx, %r13
	movq	%rcx, %r12
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	leaq	_ZN2v88internalL15kLogEventsNamesE(%rip), %rax
	movslq	%ebx, %rsi
	movq	%r14, %rdi
	movq	(%rax,%rsi,8), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L1320:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1325
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1325:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20749:
	.size	_ZN2v88internal6Logger17MoveEventInternalENS0_17CodeEventListener16LogEventsAndTagsEmm, .-_ZN2v88internal6Logger17MoveEventInternalENS0_17CodeEventListener16LogEventsAndTagsEmm
	.section	.rodata._ZN2v88internal6Logger13ResourceEventEPKcS3_.str1.1,"aMS",@progbits,1
.LC58:
	.string	"%.0f"
	.section	.text._ZN2v88internal6Logger13ResourceEventEPKcS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger13ResourceEventEPKcS3_
	.type	_ZN2v88internal6Logger13ResourceEventEPKcS3_, @function
_ZN2v88internal6Logger13ResourceEventEPKcS3_:
.LFB20750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$56, %rsp
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1326
	cmpq	$0, 8(%rsi)
	je	.L1326
	cmpb	$0, _ZN2v88internal8FLAG_logE(%rip)
	jne	.L1333
	.p2align 4,,10
	.p2align 3
.L1326:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1334
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1333:
	.cfi_restore_state
	leaq	-64(%rbp), %r13
	movq	%rdx, %r14
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	-68(%rbp), %rsi
	leaq	-72(%rbp), %rdi
	call	_ZN2v84base2OS11GetUserTimeEPjS2_@PLT
	cmpl	$-1, %eax
	je	.L1329
	movq	-64(%rbp), %rax
	movl	-72(%rbp), %esi
	leaq	16(%rax), %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	-68(%rbp), %esi
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
.L1329:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*128(%rax)
	leaq	.LC58(%rip), %rsi
	movq	%r13, %rdi
	movl	$1, %eax
	call	_ZN2v88internal3Log14MessageBuilder18AppendFormatStringEPKcz@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1326
.L1334:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20750:
	.size	_ZN2v88internal6Logger13ResourceEventEPKcS3_, .-_ZN2v88internal6Logger13ResourceEventEPKcS3_
	.section	.rodata._ZN2v88internal6Logger16SuspectReadEventENS0_4NameENS0_6ObjectE.str1.1,"aMS",@progbits,1
.LC59:
	.string	"suspect-read"
	.section	.text._ZN2v88internal6Logger16SuspectReadEventENS0_4NameENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger16SuspectReadEventENS0_4NameENS0_6ObjectE
	.type	_ZN2v88internal6Logger16SuspectReadEventENS0_4NameENS0_6ObjectE, @function
_ZN2v88internal6Logger16SuspectReadEventENS0_4NameENS0_6ObjectE:
.LFB20751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1335
	cmpq	$0, 8(%rsi)
	je	.L1335
	cmpb	$0, _ZN2v88internal16FLAG_log_suspectE(%rip)
	jne	.L1344
	.p2align 4,,10
	.p2align 3
.L1335:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1345
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1344:
	.cfi_restore_state
	leaq	-64(%rbp), %r13
	movq	%rdi, %rbx
	movq	%rdx, %r14
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	testb	$1, %r14b
	jne	.L1346
.L1337:
	movq	8(%rbx), %rax
	movq	128(%rax), %r14
.L1341:
	movq	%r13, %rdi
	leaq	.LC59(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_4NameEEERS2_T_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1335
	.p2align 4,,10
	.p2align 3
.L1346:
	movq	-1(%r14), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L1337
	leaq	-72(%rbp), %rdi
	movq	%r14, -72(%rbp)
	call	_ZN2v88internal10JSReceiver10class_nameEv@PLT
	movq	%rax, %r14
	jmp	.L1341
.L1345:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20751:
	.size	_ZN2v88internal6Logger16SuspectReadEventENS0_4NameENS0_6ObjectE, .-_ZN2v88internal6Logger16SuspectReadEventENS0_4NameENS0_6ObjectE
	.section	.text._ZN2v88internal6Logger13FunctionEventEPKcidiiNS0_6StringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger13FunctionEventEPKcidiiNS0_6StringE
	.type	_ZN2v88internal6Logger13FunctionEventEPKcidiiNS0_6StringE, @function
_ZN2v88internal6Logger13FunctionEventEPKcidiiNS0_6StringE:
.LFB20753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edx, -84(%rbp)
	movq	48(%rdi), %rsi
	movl	%ecx, -72(%rbp)
	movl	%r8d, -68(%rbp)
	movsd	%xmm0, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1347
	cmpq	$0, 8(%rsi)
	je	.L1347
	cmpb	$0, _ZN2v88internal24FLAG_log_function_eventsE(%rip)
	jne	.L1354
	.p2align 4,,10
	.p2align 3
.L1347:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1355
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1354:
	.cfi_restore_state
	leaq	-64(%rbp), %r15
	movq	%rdi, %rbx
	movq	%r9, %r12
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movl	-68(%rbp), %r8d
	movl	-72(%rbp), %ecx
	movq	%r13, %rsi
	movsd	-80(%rbp), %xmm0
	movl	-84(%rbp), %edx
	leaq	160(%rbx), %r9
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_121AppendFunctionMessageERNS0_3Log14MessageBuilderEPKcidiiPNS_4base12ElapsedTimerE
	testq	%r12, %r12
	jne	.L1356
.L1350:
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1356:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_@PLT
	jmp	.L1350
.L1355:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20753:
	.size	_ZN2v88internal6Logger13FunctionEventEPKcidiiNS0_6StringE, .-_ZN2v88internal6Logger13FunctionEventEPKcidiiNS0_6StringE
	.section	.text._ZN2v88internal6Logger13FunctionEventEPKcidiiS3_m,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger13FunctionEventEPKcidiiS3_m
	.type	_ZN2v88internal6Logger13FunctionEventEPKcidiiS3_m, @function
_ZN2v88internal6Logger13FunctionEventEPKcidiiS3_m:
.LFB20754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 3, -48
	movl	%edx, -84(%rbp)
	movq	48(%rdi), %rsi
	movl	%ecx, -72(%rbp)
	movl	%r8d, -68(%rbp)
	movsd	%xmm0, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1357
	cmpq	$0, 8(%rsi)
	je	.L1357
	cmpb	$0, _ZN2v88internal24FLAG_log_function_eventsE(%rip)
	jne	.L1364
	.p2align 4,,10
	.p2align 3
.L1357:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1365
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1364:
	.cfi_restore_state
	leaq	-64(%rbp), %r15
	movq	%rdi, %rbx
	movq	%r9, %r14
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movl	-68(%rbp), %r8d
	movl	-72(%rbp), %ecx
	movq	%r12, %rsi
	movsd	-80(%rbp), %xmm0
	movl	-84(%rbp), %edx
	leaq	160(%rbx), %r9
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_121AppendFunctionMessageERNS0_3Log14MessageBuilderEPKcidiiPNS_4base12ElapsedTimerE
	cmpq	$0, 16(%rbp)
	jne	.L1366
.L1360:
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1366:
	movq	16(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilder12AppendStringEPKcm@PLT
	jmp	.L1360
.L1365:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20754:
	.size	_ZN2v88internal6Logger13FunctionEventEPKcidiiS3_m, .-_ZN2v88internal6Logger13FunctionEventEPKcidiiS3_m
	.section	.rodata._ZN2v88internal6Logger21CompilationCacheEventEPKcS3_NS0_18SharedFunctionInfoE.str1.1,"aMS",@progbits,1
.LC60:
	.string	"compilation-cache"
	.section	.text._ZN2v88internal6Logger21CompilationCacheEventEPKcS3_NS0_18SharedFunctionInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger21CompilationCacheEventEPKcS3_NS0_18SharedFunctionInfoE
	.type	_ZN2v88internal6Logger21CompilationCacheEventEPKcS3_NS0_18SharedFunctionInfoE, @function
_ZN2v88internal6Logger21CompilationCacheEventEPKcS3_NS0_18SharedFunctionInfoE:
.LFB20755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -104(%rbp)
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1367
	cmpq	$0, 8(%rsi)
	je	.L1367
	cmpb	$0, _ZN2v88internal24FLAG_log_function_eventsE(%rip)
	jne	.L1377
	.p2align 4,,10
	.p2align 3
.L1367:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1378
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1377:
	.cfi_restore_state
	leaq	-80(%rbp), %r14
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	-104(%rbp), %rdx
	movq	31(%rdx), %rax
	testb	$1, %al
	jne	.L1369
.L1372:
	movl	$-1, %r15d
.L1370:
	movq	%r14, %rdi
	leaq	.LC60(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r13, %rsi
	leaq	-104(%rbp), %r13
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	%r15d, %esi
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movl	%eax, %esi
	movq	(%r12), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	movl	%eax, %esi
	movq	(%r12), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %r12
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-88(%rbp), %rdi
	subq	160(%rbx), %rax
	movq	%rax, -88(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	(%r12), %rdi
	movq	%rax, %rsi
	addq	$16, %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1367
	.p2align 4,,10
	.p2align 3
.L1369:
	movq	-1(%rax), %rsi
	leaq	-1(%rax), %rcx
	cmpw	$86, 11(%rsi)
	je	.L1379
.L1371:
	movq	(%rcx), %rax
	cmpw	$96, 11(%rax)
	jne	.L1372
	movq	31(%rdx), %rax
	testb	$1, %al
	jne	.L1380
.L1374:
	movl	67(%rax), %r15d
	jmp	.L1370
	.p2align 4,,10
	.p2align 3
.L1379:
	movq	23(%rax), %rcx
	testb	$1, %cl
	je	.L1372
	subq	$1, %rcx
	jmp	.L1371
.L1380:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L1374
	movq	23(%rax), %rax
	jmp	.L1374
.L1378:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20755:
	.size	_ZN2v88internal6Logger21CompilationCacheEventEPKcS3_NS0_18SharedFunctionInfoE, .-_ZN2v88internal6Logger21CompilationCacheEventEPKcS3_NS0_18SharedFunctionInfoE
	.section	.rodata._ZN2v88internal6Logger11ScriptEventENS1_15ScriptEventTypeEi.str1.1,"aMS",@progbits,1
.LC61:
	.string	"script"
.LC62:
	.string	"reserve-id"
.LC63:
	.string	"create"
.LC64:
	.string	"deserialize"
.LC65:
	.string	"background-compile"
.LC66:
	.string	"streaming-compile"
	.section	.text._ZN2v88internal6Logger11ScriptEventENS1_15ScriptEventTypeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger11ScriptEventENS1_15ScriptEventTypeEi
	.type	_ZN2v88internal6Logger11ScriptEventENS1_15ScriptEventTypeEi, @function
_ZN2v88internal6Logger11ScriptEventENS1_15ScriptEventTypeEi:
.LFB20756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1381
	cmpq	$0, 8(%rsi)
	je	.L1381
	cmpb	$0, _ZN2v88internal24FLAG_log_function_eventsE(%rip)
	jne	.L1393
	.p2align 4,,10
	.p2align 3
.L1381:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1394
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1393:
	.cfi_restore_state
	leaq	-64(%rbp), %r14
	movq	%rdi, %rbx
	movl	%edx, %r13d
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	leaq	.LC61(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	cmpl	$4, %r12d
	ja	.L1383
	leaq	.L1385(%rip), %rdx
	movl	%r12d, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6Logger11ScriptEventENS1_15ScriptEventTypeEi,"a",@progbits
	.align 4
	.align 4
.L1385:
	.long	.L1389-.L1385
	.long	.L1388-.L1385
	.long	.L1387-.L1385
	.long	.L1386-.L1385
	.long	.L1384-.L1385
	.section	.text._ZN2v88internal6Logger11ScriptEventENS1_15ScriptEventTypeEi
	.p2align 4,,10
	.p2align 3
.L1384:
	leaq	.LC66(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
.L1383:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	%r13d, %esi
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %r12
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-72(%rbp), %rdi
	subq	160(%rbx), %rax
	movq	%rax, -72(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	(%r12), %rdi
	movq	%rax, %rsi
	addq	$16, %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1381
	.p2align 4,,10
	.p2align 3
.L1386:
	leaq	.LC65(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1387:
	leaq	.LC64(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1388:
	leaq	.LC63(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1389:
	leaq	.LC62(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	jmp	.L1383
.L1394:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20756:
	.size	_ZN2v88internal6Logger11ScriptEventENS1_15ScriptEventTypeEi, .-_ZN2v88internal6Logger11ScriptEventENS1_15ScriptEventTypeEi
	.section	.rodata._ZN2v88internal6Logger21RuntimeCallTimerEventEv.str1.1,"aMS",@progbits,1
.LC67:
	.string	"active-runtime-timer"
	.section	.text._ZN2v88internal6Logger21RuntimeCallTimerEventEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger21RuntimeCallTimerEventEv
	.type	_ZN2v88internal6Logger21RuntimeCallTimerEventEv, @function
_ZN2v88internal6Logger21RuntimeCallTimerEventEv:
.LFB20759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	40960(%rax), %rax
	movq	23248(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1395
	movq	48(%rdi), %rsi
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	leaq	.LC67(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-40(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L1395:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1400
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1400:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20759:
	.size	_ZN2v88internal6Logger21RuntimeCallTimerEventEv, .-_ZN2v88internal6Logger21RuntimeCallTimerEventEv
	.section	.rodata._ZN2v88internal6Logger9TickEventEPNS0_10TickSampleEb.str1.1,"aMS",@progbits,1
.LC68:
	.string	"tick"
.LC69:
	.string	"overflow"
	.section	.text._ZN2v88internal6Logger9TickEventEPNS0_10TickSampleEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger9TickEventEPNS0_10TickSampleEb
	.type	_ZN2v88internal6Logger9TickEventEPNS0_10TickSampleEb, @function
_ZN2v88internal6Logger9TickEventEPNS0_10TickSampleEb:
.LFB20760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1401
	cmpq	$0, 8(%rsi)
	je	.L1401
	cmpb	$0, _ZN2v88internal13FLAG_prof_cppE(%rip)
	jne	.L1415
	.p2align 4,,10
	.p2align 3
.L1401:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1416
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1415:
	.cfi_restore_state
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rdi, %r12
	movl	%edx, %r14d
	leaq	-80(%rbp), %r13
	cmpl	$1, %eax
	je	.L1417
.L1403:
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	leaq	.LC68(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	8(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %r15
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-88(%rbp), %rdi
	subq	160(%r12), %rax
	movq	%rax, -88(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	(%r15), %rdi
	movq	%rax, %rsi
	addq	$16, %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	testb	$1, 4113(%rbx)
	je	.L1405
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	$1, %esi
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
.L1414:
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	16(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	(%rbx), %esi
	movq	(%rax), %rdi
	addq	$16, %rdi
	call	_ZNSolsEi@PLT
	testb	%r14b, %r14b
	jne	.L1418
.L1407:
	cmpb	$0, 4112(%rbx)
	je	.L1408
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L1409:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	24(%rbx,%r12,8), %rsi
	addq	$1, %r12
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	movzbl	4112(%rbx), %eax
	cmpl	%r12d, %eax
	ja	.L1409
.L1408:
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1401
	.p2align 4,,10
	.p2align 3
.L1405:
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1418:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	.LC69(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1417:
	movq	8(%rdi), %rax
	movq	40960(%rax), %rax
	movq	23248(%rax), %r15
	movq	48(%rdi), %rsi
	testq	%r15, %r15
	je	.L1403
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	leaq	.LC67(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	(%r15), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	48(%r12), %rsi
	jmp	.L1403
.L1416:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20760:
	.size	_ZN2v88internal6Logger9TickEventEPNS0_10TickSampleEb, .-_ZN2v88internal6Logger9TickEventEPNS0_10TickSampleEb
	.section	.text._ZN2v88internal8Profiler3RunEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Profiler3RunEv
	.type	_ZN2v88internal8Profiler3RunEv, @function
_ZN2v88internal8Profiler3RunEv:
.LFB20683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	pxor	%xmm0, %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	-80(%rbp), %eax
	leaq	529480(%rdi), %r13
	movq	%rdi, %r14
	movq	%r13, %rdi
	movq	$0, -88(%rbp)
	leaq	-4192(%rbp), %r12
	leaq	529468(%r14), %r15
	andw	$-1024, %ax
	movups	%xmm0, -72(%rbp)
	leaq	529512(%r14), %rbx
	orb	$2, %ah
	movups	%xmm0, -4184(%rbp)
	movw	%ax, -80(%rbp)
	movl	$5, -4192(%rbp)
	call	_ZN2v84base9Semaphore4WaitEv@PLT
	movslq	529468(%r14), %rax
	movl	$517, %ecx
	movq	%r12, %rdi
	movzbl	529472(%r14), %edx
	imulq	$4136, %rax, %rax
	leaq	56(%r14,%rax), %rsi
	rep movsq
	movl	529468(%r14), %eax
	addl	$1, %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$25, %ecx
	addl	%ecx, %eax
	andl	$127, %eax
	subl	%ecx, %eax
	movl	%eax, 529468(%r14)
	movb	$0, 529472(%r14)
	jmp	.L1422
	.p2align 4,,10
	.p2align 3
.L1421:
	movq	%r13, %rdi
	call	_ZN2v84base9Semaphore4WaitEv@PLT
	movslq	(%r15), %rax
	movl	$517, %ecx
	movq	%r12, %rdi
	movzbl	529472(%r14), %edx
	imulq	$4136, %rax, %rax
	leaq	56(%r14,%rax), %rsi
	rep movsq
	movl	(%r15), %eax
	addl	$1, %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$25, %ecx
	addl	%ecx, %eax
	andl	$127, %eax
	subl	%ecx, %eax
	movl	%eax, (%r15)
	movb	$0, 529472(%r14)
.L1422:
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L1419
	movq	48(%r14), %rax
	movq	41016(%rax), %rdi
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L1421
	cmpb	$0, 40(%rdi)
	je	.L1421
	movq	%r12, %rsi
	call	_ZN2v88internal6Logger9TickEventEPNS0_10TickSampleEb
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1419:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1425
	addq	$4152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1425:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20683:
	.size	_ZN2v88internal8Profiler3RunEv, .-_ZN2v88internal8Profiler3RunEv
	.section	.rodata._ZN2v88internal6Logger7ICEventEPKcbNS0_3MapENS0_6ObjectEccS3_S3_.str1.1,"aMS",@progbits,1
.LC70:
	.string	"Keyed"
	.section	.text._ZN2v88internal6Logger7ICEventEPKcbNS0_3MapENS0_6ObjectEccS3_S3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger7ICEventEPKcbNS0_3MapENS0_6ObjectEccS3_S3_
	.type	_ZN2v88internal6Logger7ICEventEPKcbNS0_3MapENS0_6ObjectEccS3_S3_, @function
_ZN2v88internal6Logger7ICEventEPKcbNS0_3MapENS0_6ObjectEccS3_S3_:
.LFB20761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movl	16(%rbp), %r8d
	movl	%r9d, -128(%rbp)
	movq	24(%rbp), %r14
	movq	32(%rbp), %r13
	movq	%rsi, -152(%rbp)
	movl	%edx, -140(%rbp)
	movq	48(%rdi), %rsi
	movq	%rcx, -136(%rbp)
	movl	%r8d, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1426
	cmpq	$0, 8(%rsi)
	je	.L1426
	cmpb	$0, _ZN2v88internal13FLAG_trace_icE(%rip)
	jne	.L1441
	.p2align 4,,10
	.p2align 3
.L1426:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1442
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1441:
	.cfi_restore_state
	leaq	-96(%rbp), %r15
	movq	%rdi, %rbx
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movl	-140(%rbp), %edx
	movl	-120(%rbp), %r8d
	movl	-128(%rbp), %r9d
	movq	-136(%rbp), %rcx
	testb	%dl, %dl
	movq	-152(%rbp), %r10
	jne	.L1443
.L1429:
	movq	8(%rbx), %rdi
	leaq	-100(%rbp), %rdx
	leaq	-104(%rbp), %rsi
	movq	%rcx, -128(%rbp)
	movl	%r8d, -140(%rbp)
	movl	%r9d, -136(%rbp)
	movq	%r10, -120(%rbp)
	call	_ZN2v88internal7Isolate13GetAbstractPCEPiS2_@PLT
	movq	-120(%rbp), %r10
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%r10, %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	-104(%rbp), %esi
	movq	%rax, %rbx
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	-100(%rbp), %esi
	movq	%rax, %rbx
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	-136(%rbp), %r9d
	movq	%rax, %rdi
	movsbl	%r9b, %esi
	call	_ZN2v88internal3Log14MessageBuilderlsIcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	-140(%rbp), %r8d
	movq	%rax, %rdi
	movsbl	%r8b, %esi
	call	_ZN2v88internal3Log14MessageBuilderlsIcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	-128(%rbp), %rcx
	leaq	-80(%rbp), %rsi
	movq	%rax, %rbx
	movl	$268, %eax
	movq	%rcx, -80(%rbp)
	movw	%ax, -72(%rbp)
	movq	(%rbx), %rax
	leaq	16(%rax), %rdi
	call	_ZN2v88internallsERSoRKNS0_5AsHexE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	testb	$1, %r12b
	jne	.L1431
	movq	-96(%rbp), %rax
	movq	%r12, %rsi
	sarq	$32, %rsi
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
.L1432:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	testq	%r13, %r13
	je	.L1435
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
.L1435:
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-88(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1426
	.p2align 4,,10
	.p2align 3
.L1431:
	movq	-1(%r12), %rax
	cmpw	$65, 11(%rax)
	je	.L1444
	movq	-1(%r12), %rax
	cmpw	$64, 11(%rax)
	ja	.L1432
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_4NameEEERS2_T_@PLT
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1443:
	leaq	.LC70(%rip), %rsi
	movq	%r15, %rdi
	movq	%rcx, -128(%rbp)
	movl	%r8d, -140(%rbp)
	movl	%r9d, -136(%rbp)
	movq	%r10, -120(%rbp)
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movl	-140(%rbp), %r8d
	movl	-136(%rbp), %r9d
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r10
	jmp	.L1429
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	-96(%rbp), %rax
	movq	7(%r12), %xmm0
	leaq	16(%rax), %rdi
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	jmp	.L1432
.L1442:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20761:
	.size	_ZN2v88internal6Logger7ICEventEPKcbNS0_3MapENS0_6ObjectEccS3_S3_, .-_ZN2v88internal6Logger7ICEventEPKcbNS0_3MapENS0_6ObjectEccS3_S3_
	.section	.rodata._ZN2v88internal6Logger9MapCreateENS0_3MapE.str1.1,"aMS",@progbits,1
.LC71:
	.string	"map-create"
	.section	.text._ZN2v88internal6Logger9MapCreateENS0_3MapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger9MapCreateENS0_3MapE
	.type	_ZN2v88internal6Logger9MapCreateENS0_3MapE, @function
_ZN2v88internal6Logger9MapCreateENS0_3MapE:
.LFB20763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 3, -48
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1445
	cmpq	$0, 8(%rsi)
	je	.L1445
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	jne	.L1450
	.p2align 4,,10
	.p2align 3
.L1445:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1451
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1450:
	.cfi_restore_state
	leaq	-80(%rbp), %r14
	movq	%rdi, %rbx
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r14, %rdi
	leaq	.LC71(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %r13
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-88(%rbp), %rdi
	subq	160(%rbx), %rax
	movq	%rax, -88(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	%rax, %rsi
	movq	0(%r13), %rax
	leaq	16(%rax), %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	$268, %edx
	movq	%r12, -64(%rbp)
	leaq	-64(%rbp), %rsi
	movw	%dx, -56(%rbp)
	movq	(%rax), %rdi
	addq	$16, %rdi
	call	_ZN2v88internallsERSoRKNS0_5AsHexE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1445
.L1451:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20763:
	.size	_ZN2v88internal6Logger9MapCreateENS0_3MapE, .-_ZN2v88internal6Logger9MapCreateENS0_3MapE
	.section	.rodata._ZN2v88internal6Logger10MapDetailsENS0_3MapE.str1.1,"aMS",@progbits,1
.LC72:
	.string	"map-details"
	.section	.text._ZN2v88internal6Logger10MapDetailsENS0_3MapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger10MapDetailsENS0_3MapE
	.type	_ZN2v88internal6Logger10MapDetailsENS0_3MapE, @function
_ZN2v88internal6Logger10MapDetailsENS0_3MapE:
.LFB20764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -520(%rbp)
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L1452
	cmpq	$0, 8(%rsi)
	je	.L1452
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	jne	.L1464
	.p2align 4,,10
	.p2align 3
.L1452:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1465
	addq	$504, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1464:
	.cfi_restore_state
	leaq	-496(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	leaq	.LC72(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %r13
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-504(%rbp), %rdi
	subq	160(%rbx), %rax
	movq	%rax, -504(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	%rax, %rsi
	movq	0(%r13), %rax
	leaq	16(%rax), %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	$268, %edx
	leaq	-480(%rbp), %rsi
	movq	%rax, %r13
	movq	-520(%rbp), %rax
	movw	%dx, -472(%rbp)
	movq	%rax, -480(%rbp)
	movq	0(%r13), %rax
	leaq	16(%rax), %rdi
	call	_ZN2v88internallsERSoRKNS0_5AsHexE@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	cmpb	$0, _ZN2v88internal23FLAG_trace_maps_detailsE(%rip)
	jne	.L1466
.L1455:
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-488(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1452
	.p2align 4,,10
	.p2align 3
.L1466:
	movq	.LC22(%rip), %xmm1
	leaq	-320(%rbp), %r13
	leaq	-432(%rbp), %r15
	movq	%r13, %rdi
	leaq	-368(%rbp), %r14
	movhps	.LC23(%rip), %xmm1
	movaps	%xmm1, -544(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -432(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-544(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -544(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	-520(%rbp), %rdi
	movq	%r15, %rsi
	leaq	-448(%rbp), %r15
	call	_ZN2v88internal3Map15PrintMapDetailsERSo@PLT
	movq	-384(%rbp), %rax
	movq	%r15, -464(%rbp)
	leaq	-464(%rbp), %rdi
	movq	$0, -456(%rbp)
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L1457
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L1458
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1459:
	movq	-464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	-464(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1460
	call	_ZdlPv@PLT
.L1460:
	movq	.LC22(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC24(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-544(%rbp), %rdi
	je	.L1461
	call	_ZdlPv@PLT
.L1461:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%rbx, -432(%rbp)
	movq	-24(%rbx), %rax
	movq	%r13, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rdx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1455
.L1457:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1459
	.p2align 4,,10
	.p2align 3
.L1458:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1459
.L1465:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20764:
	.size	_ZN2v88internal6Logger10MapDetailsENS0_3MapE, .-_ZN2v88internal6Logger10MapDetailsENS0_3MapE
	.section	.rodata._ZN2v88internal6Logger8MapEventEPKcNS0_3MapES4_S3_NS0_10HeapObjectE.str1.1,"aMS",@progbits,1
.LC73:
	.string	"map"
	.section	.text._ZN2v88internal6Logger8MapEventEPKcNS0_3MapES4_S3_NS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger8MapEventEPKcNS0_3MapES4_S3_NS0_10HeapObjectE
	.type	_ZN2v88internal6Logger8MapEventEPKcNS0_3MapES4_S3_NS0_10HeapObjectE, @function
_ZN2v88internal6Logger8MapEventEPKcNS0_3MapES4_S3_NS0_10HeapObjectE:
.LFB20762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	48(%rdi), %rax
	cmpb	$0, (%rax)
	jne	.L1467
	cmpq	$0, 8(%rax)
	je	.L1467
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	je	.L1467
	movq	%rdi, %rbx
	movq	%rsi, %r15
	movq	%rdx, %r14
	movq	%rcx, %r12
	movq	%r9, %r13
	testq	%rcx, %rcx
	jne	.L1480
.L1470:
	movq	8(%rbx), %rdi
	movl	$-1, -112(%rbp)
	xorl	%edx, %edx
	movl	$-1, -108(%rbp)
	movq	40936(%rdi), %rax
	movl	8(%rax), %esi
	testl	%esi, %esi
	je	.L1481
.L1472:
	leaq	-96(%rbp), %r9
	movq	48(%rbx), %rsi
	movq	%r8, -136(%rbp)
	movq	%r9, %rdi
	movq	%rdx, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	-120(%rbp), %r9
	leaq	.LC73(%rip), %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %r15
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-104(%rbp), %rdi
	subq	160(%rbx), %rax
	movq	%rax, -104(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	%rax, %rsi
	movq	(%r15), %rax
	leaq	16(%rax), %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r14, -80(%rbp)
	leaq	-80(%rbp), %r14
	movq	%rax, %r15
	movl	$268, %eax
	movq	%r14, %rsi
	movw	%ax, -72(%rbp)
	movq	(%r15), %rax
	leaq	16(%rax), %rdi
	call	_ZN2v88internallsERSoRKNS0_5AsHexE@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	$268, %edx
	movq	%r12, -80(%rbp)
	movq	%r14, %rsi
	movw	%dx, -72(%rbp)
	movq	%rax, %r15
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZN2v88internallsERSoRKNS0_5AsHexE@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	-128(%rbp), %rdx
	movl	$268, %ecx
	movq	%r14, %rsi
	movw	%cx, -72(%rbp)
	movq	%rax, %r12
	movq	%rdx, -80(%rbp)
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZN2v88internallsERSoRKNS0_5AsHexE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	-112(%rbp), %esi
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	-108(%rbp), %esi
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	-136(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	testq	%r13, %r13
	movq	-120(%rbp), %r9
	jne	.L1482
.L1474:
	movq	%r9, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-88(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L1467:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1483
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1482:
	.cfi_restore_state
	movq	-1(%r13), %rax
	cmpw	$64, 11(%rax)
	jbe	.L1484
	movq	-1(%r13), %rax
	cmpw	$160, 11(%rax)
	jne	.L1474
	movq	%r14, %rdi
	movq	%r9, -120(%rbp)
	movq	%r13, -80(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	-120(%rbp), %r9
	movq	%rax, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_@PLT
	movq	-120(%rbp), %r9
	jmp	.L1474
	.p2align 4,,10
	.p2align 3
.L1481:
	leaq	-108(%rbp), %rdx
	leaq	-112(%rbp), %rsi
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal7Isolate13GetAbstractPCEPiS2_@PLT
	movq	-120(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L1472
	.p2align 4,,10
	.p2align 3
.L1480:
	movq	%rcx, %rsi
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal6Logger10MapDetailsENS0_3MapE
	movq	-120(%rbp), %r8
	jmp	.L1470
	.p2align 4,,10
	.p2align 3
.L1484:
	movq	%r9, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_4NameEEERS2_T_@PLT
	movq	-120(%rbp), %r9
	jmp	.L1474
.L1483:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20762:
	.size	_ZN2v88internal6Logger8MapEventEPKcNS0_3MapES4_S3_NS0_10HeapObjectE, .-_ZN2v88internal6Logger8MapEventEPKcNS0_3MapES4_S3_NS0_10HeapObjectE
	.section	.text._ZN2v88internal6Logger20LogAccessorCallbacksEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger20LogAccessorCallbacksEv
	.type	_ZN2v88internal6Logger20LogAccessorCallbacksEv, @function
_ZN2v88internal6Logger20LogAccessorCallbacksEv:
.LFB20772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movq	8(%rdi), %rax
	movq	%r13, %rdi
	leaq	37592(%rax), %rsi
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal18HeapObjectIteratorC1EPNS0_4HeapENS1_20HeapObjectsFilteringE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L1486
	jmp	.L1494
	.p2align 4,,10
	.p2align 3
.L1492:
	movq	%r13, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1494
.L1486:
	movq	-1(%rbx), %rax
	cmpw	$78, 11(%rax)
	jne	.L1492
	movq	7(%rbx), %r12
	movq	-1(%r12), %rax
	cmpw	$64, 11(%rax)
	ja	.L1492
	movq	39(%rbx), %rdx
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	cmpq	%rdx, %rax
	je	.L1488
	movq	7(%rdx), %r14
	testq	%r14, %r14
	jne	.L1512
.L1488:
	movq	31(%rbx), %rdx
	cmpq	%rax, %rdx
	je	.L1492
	movq	7(%rdx), %r15
	testq	%r15, %r15
	je	.L1492
	movq	-104(%rbp), %rax
	movq	8(%rax), %rax
	movq	41488(%rax), %rbx
	leaq	56(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1493
	.p2align 4,,10
	.p2align 3
.L1491:
	movq	8(%rbx), %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*72(%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1491
.L1493:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L1486
	.p2align 4,,10
	.p2align 3
.L1494:
	movq	%r13, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1513
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1512:
	.cfi_restore_state
	movq	-104(%rbp), %rax
	movq	8(%rax), %rax
	movq	41488(%rax), %r15
	leaq	56(%r15), %rax
	movq	%rax, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%r15), %r15
	testq	%r15, %r15
	je	.L1490
	.p2align 4,,10
	.p2align 3
.L1489:
	movq	8(%r15), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rcx
	call	*64(%rcx)
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.L1489
.L1490:
	movq	-112(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	jmp	.L1488
.L1513:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20772:
	.size	_ZN2v88internal6Logger20LogAccessorCallbacksEv, .-_ZN2v88internal6Logger20LogAccessorCallbacksEv
	.section	.text._ZN2v88internal6Logger10LogAllMapsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger10LogAllMapsEv
	.type	_ZN2v88internal6Logger10LogAllMapsEv, @function
_ZN2v88internal6Logger10LogAllMapsEv:
.LFB20773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-80(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%r13, %rdi
	leaq	37592(%rax), %rsi
	call	_ZN2v88internal18HeapObjectIteratorC1EPNS0_4HeapENS1_20HeapObjectsFilteringE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1515
	.p2align 4,,10
	.p2align 3
.L1518:
	movq	-1(%r12), %rax
	cmpw	$68, 11(%rax)
	jne	.L1527
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Logger9MapCreateENS0_3MapE
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Logger10MapDetailsENS0_3MapE
.L1527:
	movq	%r13, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L1518
.L1515:
	movq	%r13, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1528
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1528:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20773:
	.size	_ZN2v88internal6Logger10LogAllMapsEv, .-_ZN2v88internal6Logger10LogAllMapsEv
	.section	.text._ZN2v88internal6Logger7samplerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger7samplerEv
	.type	_ZN2v88internal6Logger7samplerEv, @function
_ZN2v88internal6Logger7samplerEv:
.LFB20778:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE20778:
	.size	_ZN2v88internal6Logger7samplerEv, .-_ZN2v88internal6Logger7samplerEv
	.section	.text._ZN2v88internal6Logger18StopProfilerThreadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger18StopProfilerThreadEv
	.type	_ZN2v88internal6Logger18StopProfilerThreadEv, @function
_ZN2v88internal6Logger18StopProfilerThreadEv:
.LFB20779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1530
	call	_ZN2v88internal8Profiler9DisengageEv
	movq	24(%rbx), %rdi
	movq	$0, 24(%rbx)
	testq	%rdi, %rdi
	je	.L1530
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1530:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20779:
	.size	_ZN2v88internal6Logger18StopProfilerThreadEv, .-_ZN2v88internal6Logger18StopProfilerThreadEv
	.section	.text._ZN2v88internal6Logger8TearDownEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger8TearDownEv
	.type	_ZN2v88internal6Logger8TearDownEv, @function
_ZN2v88internal6Logger8TearDownEv:
.LFB20780:
	.cfi_startproc
	endbr64
	cmpb	$0, 140(%rdi)
	je	.L1621
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movb	$0, 140(%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1541
	call	_ZN2v88internal8Profiler9DisengageEv
	movq	24(%rbx), %rdi
	movq	$0, 24(%rbx)
	testq	%rdi, %rdi
	je	.L1541
	movq	(%rdi), %rax
	call	*8(%rax)
.L1541:
	movq	16(%rbx), %r12
	movq	$0, 16(%rbx)
	testq	%r12, %r12
	je	.L1543
	movq	(%r12), %rax
	leaq	_ZN2v88internal6TickerD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1544
	leaq	16+_ZTVN2v88internal6TickerE(%rip), %rax
	movq	%rax, (%r12)
	movzbl	32(%r12), %eax
	testb	%al, %al
	jne	.L1681
.L1545:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1546
	movq	(%rdi), %rax
	call	*8(%rax)
.L1546:
	movq	%r12, %rdi
	call	_ZN2v87sampler7SamplerD2Ev@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1543:
	movq	56(%rbx), %r12
	testq	%r12, %r12
	je	.L1548
	movq	8(%rbx), %rax
	movq	41488(%rax), %r13
	leaq	56(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%r13), %r8
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	0(%r13), %r15
	divq	%r8
	leaq	0(,%rdx,8), %rax
	movq	%rdx, %r10
	movq	%rax, -64(%rbp)
	addq	%r15, %rax
	movq	(%rax), %r11
	movq	%rax, -56(%rbp)
	testq	%r11, %r11
	je	.L1549
	movq	(%r11), %rdi
	movq	%r11, %r9
	movq	8(%rdi), %rcx
	jmp	.L1551
	.p2align 4,,10
	.p2align 3
.L1682:
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L1549
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r9
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L1549
	movq	%rsi, %rdi
.L1551:
	cmpq	%rcx, %r12
	jne	.L1682
	movq	(%rdi), %rcx
	cmpq	%r9, %r11
	je	.L1683
	testq	%rcx, %rcx
	je	.L1553
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r10
	je	.L1553
	movq	%r9, (%r15,%rdx,8)
	movq	(%rdi), %rcx
.L1553:
	movq	%rcx, (%r9)
	call	_ZdlPv@PLT
	subq	$1, 24(%r13)
.L1549:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	56(%rbx), %r12
	movq	$0, 56(%rbx)
	testq	%r12, %r12
	je	.L1548
	movq	(%r12), %rax
	leaq	_ZN2v88internal15PerfBasicLoggerD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1556
	leaq	16+_ZTVN2v88internal15PerfBasicLoggerE(%rip), %rax
	movq	24(%r12), %rdi
	movq	%rax, (%r12)
	call	fclose@PLT
	movq	16(%r12), %rdi
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rax
	movq	$0, 24(%r12)
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L1557
	movl	$516, %esi
	call	_ZdlPvm@PLT
.L1557:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1548:
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L1559
	movq	8(%rbx), %rax
	movq	41488(%rax), %r13
	leaq	56(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%r13), %r8
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	0(%r13), %r15
	divq	%r8
	leaq	0(,%rdx,8), %rax
	movq	%rdx, %r10
	movq	%rax, -64(%rbp)
	addq	%r15, %rax
	movq	(%rax), %r11
	movq	%rax, -56(%rbp)
	testq	%r11, %r11
	je	.L1560
	movq	(%r11), %rdi
	movq	%r11, %r9
	movq	8(%rdi), %rcx
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1684:
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L1560
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r9
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L1560
	movq	%rsi, %rdi
.L1562:
	cmpq	%rcx, %r12
	jne	.L1684
	movq	(%rdi), %rcx
	cmpq	%r9, %r11
	je	.L1685
	testq	%rcx, %rcx
	je	.L1564
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r10
	je	.L1564
	movq	%r9, (%r15,%rdx,8)
	movq	(%rdi), %rcx
.L1564:
	movq	%rcx, (%r9)
	call	_ZdlPv@PLT
	subq	$1, 24(%r13)
.L1560:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	64(%rbx), %rdi
	movq	$0, 64(%rbx)
	testq	%rdi, %rdi
	je	.L1559
	movq	(%rdi), %rax
	call	*8(%rax)
.L1559:
	movq	72(%rbx), %r12
	testq	%r12, %r12
	je	.L1568
	movq	8(%rbx), %rax
	movq	41488(%rax), %r13
	leaq	56(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%r13), %r8
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	0(%r13), %r15
	divq	%r8
	leaq	0(,%rdx,8), %rax
	movq	%rdx, %r10
	movq	%rax, -64(%rbp)
	addq	%r15, %rax
	movq	(%rax), %r11
	movq	%rax, -56(%rbp)
	testq	%r11, %r11
	je	.L1569
	movq	(%r11), %rdi
	movq	%r11, %r9
	movq	8(%rdi), %rcx
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L1569
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r9
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L1569
	movq	%rsi, %rdi
.L1571:
	cmpq	%rcx, %r12
	jne	.L1686
	movq	(%rdi), %rcx
	cmpq	%r9, %r11
	je	.L1687
	testq	%rcx, %rcx
	je	.L1573
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r10
	je	.L1573
	movq	%r9, (%r15,%rdx,8)
	movq	(%rdi), %rcx
.L1573:
	movq	%rcx, (%r9)
	call	_ZdlPv@PLT
	subq	$1, 24(%r13)
.L1569:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	72(%rbx), %r12
	movq	$0, 72(%rbx)
	testq	%r12, %r12
	je	.L1568
	movq	(%r12), %rax
	leaq	_ZN2v88internal14LowLevelLoggerD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1576
	leaq	16+_ZTVN2v88internal14LowLevelLoggerE(%rip), %rax
	movq	24(%r12), %rdi
	movq	%rax, (%r12)
	call	fclose@PLT
	movq	16(%r12), %rdi
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rax
	movq	$0, 24(%r12)
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L1577
	movl	$516, %esi
	call	_ZdlPvm@PLT
.L1577:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1568:
	movq	80(%rbx), %r12
	testq	%r12, %r12
	je	.L1579
	movq	8(%rbx), %rax
	movq	41488(%rax), %r13
	leaq	56(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%r13), %r8
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	0(%r13), %r15
	divq	%r8
	leaq	0(,%rdx,8), %rax
	movq	%rdx, %r10
	movq	%rax, -64(%rbp)
	addq	%r15, %rax
	movq	(%rax), %r11
	movq	%rax, -56(%rbp)
	testq	%r11, %r11
	je	.L1580
	movq	(%r11), %rdi
	movq	%r11, %r9
	movq	8(%rdi), %rcx
	jmp	.L1582
	.p2align 4,,10
	.p2align 3
.L1688:
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L1580
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r9
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L1580
	movq	%rsi, %rdi
.L1582:
	cmpq	%rcx, %r12
	jne	.L1688
	movq	(%rdi), %rcx
	cmpq	%r9, %r11
	je	.L1689
	testq	%rcx, %rcx
	je	.L1584
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r10
	je	.L1584
	movq	%r9, (%r15,%rdx,8)
	movq	(%rdi), %rcx
.L1584:
	movq	%rcx, (%r9)
	call	_ZdlPv@PLT
	subq	$1, 24(%r13)
.L1580:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	80(%rbx), %rdi
	movq	$0, 80(%rbx)
	testq	%rdi, %rdi
	je	.L1579
	movq	(%rdi), %rax
	call	*8(%rax)
.L1579:
	movq	48(%rbx), %rdi
	addq	$24, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal3Log5CloseEv@PLT
	.p2align 4,,10
	.p2align 3
.L1621:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1687:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	testq	%rcx, %rcx
	je	.L1597
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r10
	je	.L1573
	movq	%r9, (%r15,%rdx,8)
	movq	-64(%rbp), %rax
	addq	0(%r13), %rax
	movq	%rax, -56(%rbp)
	movq	(%rax), %rax
.L1572:
	leaq	16(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L1690
.L1574:
	movq	-56(%rbp), %rax
	movq	$0, (%rax)
	movq	(%rdi), %rcx
	jmp	.L1573
	.p2align 4,,10
	.p2align 3
.L1689:
	testq	%rcx, %rcx
	je	.L1598
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r10
	je	.L1584
	movq	%r9, (%r15,%rdx,8)
	movq	-64(%rbp), %rax
	addq	0(%r13), %rax
	movq	%rax, -56(%rbp)
	movq	(%rax), %rax
.L1583:
	leaq	16(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L1691
.L1585:
	movq	-56(%rbp), %rax
	movq	$0, (%rax)
	movq	(%rdi), %rcx
	jmp	.L1584
	.p2align 4,,10
	.p2align 3
.L1685:
	testq	%rcx, %rcx
	je	.L1596
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r10
	je	.L1564
	movq	%r9, (%r15,%rdx,8)
	movq	-64(%rbp), %rax
	addq	0(%r13), %rax
	movq	%rax, -56(%rbp)
	movq	(%rax), %rax
.L1563:
	leaq	16(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L1692
.L1565:
	movq	-56(%rbp), %rax
	movq	$0, (%rax)
	movq	(%rdi), %rcx
	jmp	.L1564
	.p2align 4,,10
	.p2align 3
.L1683:
	testq	%rcx, %rcx
	je	.L1595
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r10
	je	.L1553
	movq	%r9, (%r15,%rdx,8)
	movq	-64(%rbp), %rax
	addq	0(%r13), %rax
	movq	%rax, -56(%rbp)
	movq	(%rax), %rax
.L1552:
	leaq	16(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L1693
.L1554:
	movq	-56(%rbp), %rax
	movq	$0, (%rax)
	movq	(%rdi), %rcx
	jmp	.L1553
	.p2align 4,,10
	.p2align 3
.L1681:
	movq	%r12, %rdi
	call	_ZN2v87sampler7Sampler4StopEv@PLT
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1544:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1556:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1548
	.p2align 4,,10
	.p2align 3
.L1576:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1568
	.p2align 4,,10
	.p2align 3
.L1596:
	movq	%r9, %rax
	jmp	.L1563
	.p2align 4,,10
	.p2align 3
.L1598:
	movq	%r9, %rax
	jmp	.L1583
	.p2align 4,,10
	.p2align 3
.L1597:
	movq	%r9, %rax
	jmp	.L1572
	.p2align 4,,10
	.p2align 3
.L1595:
	movq	%r9, %rax
	jmp	.L1552
	.p2align 4,,10
	.p2align 3
.L1692:
	movq	%rcx, 16(%r13)
	jmp	.L1565
	.p2align 4,,10
	.p2align 3
.L1691:
	movq	%rcx, 16(%r13)
	jmp	.L1585
	.p2align 4,,10
	.p2align 3
.L1690:
	movq	%rcx, 16(%r13)
	jmp	.L1574
	.p2align 4,,10
	.p2align 3
.L1693:
	movq	%rcx, 16(%r13)
	jmp	.L1554
	.cfi_endproc
.LFE20780:
	.size	_ZN2v88internal6Logger8TearDownEv, .-_ZN2v88internal6Logger8TearDownEv
	.section	.rodata._ZN2v88internal18ExistingCodeLogger13LogCodeObjectENS0_6ObjectE.str1.1,"aMS",@progbits,1
.LC74:
	.string	"A C to Wasm entry stub"
	.section	.rodata._ZN2v88internal18ExistingCodeLogger13LogCodeObjectENS0_6ObjectE.str1.8,"aMS",@progbits,1
	.align 8
.LC75:
	.string	"Unknown code from before profiling"
	.section	.rodata._ZN2v88internal18ExistingCodeLogger13LogCodeObjectENS0_6ObjectE.str1.1
.LC76:
	.string	"Regular expression code"
.LC77:
	.string	"A Wasm function"
.LC78:
	.string	"A JavaScript to Wasm adapter"
	.section	.rodata._ZN2v88internal18ExistingCodeLogger13LogCodeObjectENS0_6ObjectE.str1.8
	.align 8
.LC79:
	.string	"A WebAssembly.Function adapter"
	.section	.rodata._ZN2v88internal18ExistingCodeLogger13LogCodeObjectENS0_6ObjectE.str1.1
.LC80:
	.string	"A Wasm to C-API adapter"
.LC81:
	.string	"A Wasm to JavaScript adapter"
.LC82:
	.string	"A Wasm to Interpreter adapter"
.LC83:
	.string	"STUB code"
.LC84:
	.string	"unimplemented code"
	.section	.text._ZN2v88internal18ExistingCodeLogger13LogCodeObjectENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ExistingCodeLogger13LogCodeObjectENS0_6ObjectE
	.type	_ZN2v88internal18ExistingCodeLogger13LogCodeObjectENS0_6ObjectE, @function
_ZN2v88internal18ExistingCodeLogger13LogCodeObjectENS0_6ObjectE:
.LFB20781:
	.cfi_startproc
	endbr64
	movq	-1(%rsi), %rax
	cmpw	$69, 11(%rax)
	je	.L1727
	ret
	.p2align 4,,10
	.p2align 3
.L1727:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	43(%rsi), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$13, %eax
	ja	.L1696
	leaq	.L1698(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal18ExistingCodeLogger13LogCodeObjectENS0_6ObjectE,"a",@progbits
	.align 4
	.align 4
.L1698:
	.long	.L1694-.L1698
	.long	.L1694-.L1698
	.long	.L1708-.L1698
	.long	.L1707-.L1698
	.long	.L1716-.L1698
	.long	.L1705-.L1698
	.long	.L1704-.L1698
	.long	.L1703-.L1698
	.long	.L1702-.L1698
	.long	.L1701-.L1698
	.long	.L1700-.L1698
	.long	.L1699-.L1698
	.long	.L1694-.L1698
	.long	.L1697-.L1698
	.section	.text._ZN2v88internal18ExistingCodeLogger13LogCodeObjectENS0_6ObjectE
	.p2align 4,,10
	.p2align 3
.L1707:
	movl	59(%rsi), %edi
	leal	-64(%rdi), %eax
	cmpl	$1, %eax
	jbe	.L1717
	cmpl	$57, %edi
	jne	.L1709
.L1717:
	movq	(%rbx), %rax
	movl	$57, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	cmpq	%r12, (%rax)
	je	.L1728
.L1694:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1699:
	.cfi_restore_state
	leaq	.LC74(%rip), %r13
	movl	$18, %r15d
	.p2align 4,,10
	.p2align 3
.L1706:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1712
	movq	(%rdi), %rax
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	%r15d, %esi
	movq	16(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1700:
	.cfi_restore_state
	leaq	.LC82(%rip), %r13
	movl	$18, %r15d
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1701:
	leaq	.LC79(%rip), %r13
	movl	$18, %r15d
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1702:
	leaq	.LC78(%rip), %r13
	movl	$18, %r15d
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1703:
	leaq	.LC81(%rip), %r13
	movl	$18, %r15d
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1704:
	leaq	.LC80(%rip), %r13
	movl	$18, %r15d
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1705:
	leaq	.LC77(%rip), %r13
	movl	$11, %r15d
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1728:
	movl	59(%r12), %edi
.L1709:
	call	_ZN2v88internal8Builtins4nameEi@PLT
	movl	$8, %r15d
	movq	%rax, %r13
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1712:
	movq	(%rbx), %rax
	movq	41488(%rax), %rbx
	leaq	56(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1714
	.p2align 4,,10
	.p2align 3
.L1713:
	movq	8(%rbx), %rdi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	%r15d, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1713
.L1714:
	addq	$8, %rsp
	movq	%r14, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L1708:
	.cfi_restore_state
	leaq	.LC83(%rip), %r13
	movl	$18, %r15d
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1716:
	leaq	.LC76(%rip), %r13
	movl	$16, %r15d
	jmp	.L1706
.L1697:
	leaq	.LC84(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1696:
	leaq	.LC75(%rip), %r13
	movl	$18, %r15d
	jmp	.L1706
	.cfi_endproc
.LFE20781:
	.size	_ZN2v88internal18ExistingCodeLogger13LogCodeObjectENS0_6ObjectE, .-_ZN2v88internal18ExistingCodeLogger13LogCodeObjectENS0_6ObjectE
	.section	.text._ZN2v88internal6Logger13LogCodeObjectENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger13LogCodeObjectENS0_6ObjectE
	.type	_ZN2v88internal6Logger13LogCodeObjectENS0_6ObjectE, @function
_ZN2v88internal6Logger13LogCodeObjectENS0_6ObjectE:
.LFB20768:
	.cfi_startproc
	endbr64
	addq	$144, %rdi
	jmp	_ZN2v88internal18ExistingCodeLogger13LogCodeObjectENS0_6ObjectE
	.cfi_endproc
.LFE20768:
	.size	_ZN2v88internal6Logger13LogCodeObjectENS0_6ObjectE, .-_ZN2v88internal6Logger13LogCodeObjectENS0_6ObjectE
	.section	.text._ZN2v88internal18ExistingCodeLogger14LogCodeObjectsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ExistingCodeLogger14LogCodeObjectsEv
	.type	_ZN2v88internal18ExistingCodeLogger14LogCodeObjectsEv, @function
_ZN2v88internal18ExistingCodeLogger14LogCodeObjectsEv:
.LFB20782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	leaq	37592(%rax), %rsi
	call	_ZN2v88internal18HeapObjectIteratorC1EPNS0_4HeapENS1_20HeapObjectsFilteringE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L1735
	jmp	.L1731
	.p2align 4,,10
	.p2align 3
.L1732:
	movq	(%r14), %rax
	cmpw	$72, 11(%rax)
	je	.L1744
.L1733:
	movq	%r13, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1731
.L1735:
	movq	-1(%r12), %rax
	leaq	-1(%r12), %r14
	cmpw	$69, 11(%rax)
	jne	.L1732
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal18ExistingCodeLogger13LogCodeObjectENS0_6ObjectE
	movq	(%r14), %rax
	cmpw	$72, 11(%rax)
	jne	.L1733
.L1744:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal18ExistingCodeLogger13LogCodeObjectENS0_6ObjectE
	movq	%r13, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L1735
	.p2align 4,,10
	.p2align 3
.L1731:
	movq	%r13, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1745
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1745:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20782:
	.size	_ZN2v88internal18ExistingCodeLogger14LogCodeObjectsEv, .-_ZN2v88internal18ExistingCodeLogger14LogCodeObjectsEv
	.section	.text._ZN2v88internal6Logger14LogCodeObjectsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger14LogCodeObjectsEv
	.type	_ZN2v88internal6Logger14LogCodeObjectsEv, @function
_ZN2v88internal6Logger14LogCodeObjectsEv:
.LFB20769:
	.cfi_startproc
	endbr64
	addq	$144, %rdi
	jmp	_ZN2v88internal18ExistingCodeLogger14LogCodeObjectsEv
	.cfi_endproc
.LFE20769:
	.size	_ZN2v88internal6Logger14LogCodeObjectsEv, .-_ZN2v88internal6Logger14LogCodeObjectsEv
	.section	.text._ZN2v88internal18ExistingCodeLogger19LogExistingFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12AbstractCodeEEENS0_17CodeEventListener16LogEventsAndTagsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ExistingCodeLogger19LogExistingFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12AbstractCodeEEENS0_17CodeEventListener16LogEventsAndTagsE
	.type	_ZN2v88internal18ExistingCodeLogger19LogExistingFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12AbstractCodeEEENS0_17CodeEventListener16LogEventsAndTagsE, @function
_ZN2v88internal18ExistingCodeLogger19LogExistingFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12AbstractCodeEEENS0_17CodeEventListener16LogEventsAndTagsE:
.LFB20788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	31(%rax), %rcx
	testb	$1, %cl
	jne	.L1814
.L1751:
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L1815
.L1747:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1816
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1814:
	.cfi_restore_state
	movq	-1(%rcx), %rdi
	leaq	-1(%rcx), %rsi
	cmpw	$86, 11(%rdi)
	je	.L1817
.L1749:
	movq	(%rsi), %rcx
	cmpw	$96, 11(%rcx)
	jne	.L1751
	movq	31(%rax), %rsi
	movq	(%r15), %r13
	testb	$1, %sil
	jne	.L1818
.L1753:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1754
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %r14
.L1755:
	leaq	-64(%rbp), %r8
	movq	(%rbx), %rax
	movq	%rdx, -88(%rbp)
	movq	%r8, %rdi
	movq	%r8, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal6Script13GetLineNumberENS0_6HandleIS1_EEi@PLT
	movq	-72(%rbp), %r8
	leal	1(%rax), %r13d
	movq	(%rbx), %rax
	movq	%r8, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal6Script15GetColumnNumberENS0_6HandleIS1_EEi@PLT
	movq	-88(%rbp), %rdx
	addl	$1, %eax
	movl	%eax, -80(%rbp)
	movq	(%r14), %rax
	movq	15(%rax), %rcx
	testb	$1, %cl
	jne	.L1819
.L1757:
	movq	(%r15), %rcx
	movq	8(%r15), %rdi
	movq	(%rdx), %r14
	movq	(%rbx), %r15
	movq	128(%rcx), %r8
	movslq	51(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1774
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	testq	%rsi, %rsi
	jne	.L1775
	cmpl	$15, %r12d
	je	.L1794
	cmpl	$17, %r12d
	je	.L1795
	cmpl	$11, %r12d
	movl	$19, %ecx
	cmove	%ecx, %r12d
.L1775:
	movl	-80(%rbp), %ebx
	subq	$8, %rsp
	movq	%r15, %rcx
	movl	%r12d, %esi
	movl	%r13d, %r9d
	movq	%r14, %rdx
	pushq	%rbx
	call	*%rax
	popq	%rcx
	popq	%rsi
	jmp	.L1747
	.p2align 4,,10
	.p2align 3
.L1815:
	movq	-1(%rax), %rax
	cmpw	$88, 11(%rax)
	jne	.L1747
	movq	(%rbx), %rax
	movq	7(%rax), %rax
	movq	(%r15), %rdx
	movq	47(%rax), %rax
	cmpq	%rax, 88(%rdx)
	je	.L1747
	movq	7(%rax), %rax
	xorl	%r12d, %r12d
	cmpq	%rax, _ZN2v88internal3Smi5kZeroE(%rip)
	je	.L1781
	movq	7(%rax), %r12
.L1781:
	movq	8(%r15), %r13
	movq	(%rbx), %rax
	testq	%r13, %r13
	je	.L1782
	movq	0(%r13), %rdx
	leaq	-64(%rbp), %rdi
	movq	56(%rdx), %rbx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	*%rbx
	jmp	.L1747
	.p2align 4,,10
	.p2align 3
.L1817:
	movq	23(%rcx), %rsi
	testb	$1, %sil
	je	.L1751
	subq	$1, %rsi
	jmp	.L1749
	.p2align 4,,10
	.p2align 3
.L1782:
	movq	41488(%rdx), %rbx
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	leaq	56(%rbx), %r14
	movq	%rax, %r13
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1784
	.p2align 4,,10
	.p2align 3
.L1783:
	movq	8(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1783
.L1784:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1747
	.p2align 4,,10
	.p2align 3
.L1754:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L1820
.L1756:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L1755
.L1820:
	movq	%r13, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1756
	.p2align 4,,10
	.p2align 3
.L1819:
	movq	-1(%rcx), %rsi
	cmpw	$63, 11(%rsi)
	ja	.L1757
	movq	(%r15), %rsi
	movq	%rcx, %r8
	movq	41112(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L1759
	movq	%rcx, %rsi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %rdx
	movq	(%rax), %r8
.L1760:
	movq	(%r14), %rax
	movq	8(%r15), %rdi
	movq	(%rdx), %rdx
	movq	(%rbx), %rcx
	movslq	51(%rax), %rsi
	testl	%r13d, %r13d
	jle	.L1762
	testq	%rdi, %rdi
	je	.L1763
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	testq	%rsi, %rsi
	jne	.L1764
	cmpl	$15, %r12d
	je	.L1786
	cmpl	$17, %r12d
	je	.L1787
	cmpl	$11, %r12d
	movl	$19, %esi
	cmove	%esi, %r12d
.L1764:
	movl	-80(%rbp), %ebx
	subq	$8, %rsp
	movl	%r13d, %r9d
	movl	%r12d, %esi
	pushq	%rbx
	call	*%rax
	popq	%r9
	popq	%r10
	jmp	.L1747
	.p2align 4,,10
	.p2align 3
.L1818:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L1753
	movq	23(%rsi), %rsi
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1774:
	movq	41488(%rcx), %rbx
	testq	%rsi, %rsi
	jne	.L1776
	cmpl	$15, %r12d
	je	.L1797
	cmpl	$17, %r12d
	je	.L1798
	cmpl	$11, %r12d
	movl	$19, %eax
	cmove	%eax, %r12d
.L1776:
	leaq	56(%rbx), %rax
	movq	%r8, -72(%rbp)
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rbx
	movq	-72(%rbp), %r8
	testq	%rbx, %rbx
	je	.L1778
	.p2align 4,,10
	.p2align 3
.L1777:
	movq	8(%rbx), %rdi
	movl	-80(%rbp), %eax
	subq	$8, %rsp
	movq	%r8, -72(%rbp)
	movq	%r14, %rdx
	movl	%r13d, %r9d
	movq	%r15, %rcx
	movl	%r12d, %esi
	movq	(%rdi), %r10
	pushq	%rax
	call	*40(%r10)
	movq	(%rbx), %rbx
	popq	%rax
	movq	-72(%rbp), %r8
	popq	%rdx
	testq	%rbx, %rbx
	jne	.L1777
.L1778:
	movq	-88(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1747
	.p2align 4,,10
	.p2align 3
.L1795:
	movl	$21, %r12d
	jmp	.L1775
	.p2align 4,,10
	.p2align 3
.L1794:
	movl	$20, %r12d
	jmp	.L1775
.L1759:
	movq	41088(%rsi), %rax
	cmpq	41096(%rsi), %rax
	je	.L1821
.L1761:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rsi)
	movq	%rcx, (%rax)
	jmp	.L1760
.L1762:
	testq	%rdi, %rdi
	je	.L1769
	cmpq	$1, %rsi
	movq	(%rdi), %rax
	sbbl	%esi, %esi
	andl	$4, %esi
	addl	$17, %esi
	call	*32(%rax)
	jmp	.L1747
.L1798:
	movl	$21, %r12d
	jmp	.L1776
.L1797:
	movl	$20, %r12d
	jmp	.L1776
.L1763:
	movq	(%r15), %rax
	movq	41488(%rax), %rbx
	testq	%rsi, %rsi
	jne	.L1766
	cmpl	$15, %r12d
	je	.L1789
	cmpl	$17, %r12d
	je	.L1790
	cmpl	$11, %r12d
	movl	$19, %eax
	cmove	%eax, %r12d
.L1766:
	leaq	56(%rbx), %r15
	movq	%rcx, -96(%rbp)
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rbx
	movq	-72(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rcx
	testq	%rbx, %rbx
	je	.L1768
	movq	%r15, -88(%rbp)
	movq	%rbx, %r14
	movq	%rdx, %r15
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L1767:
	movq	8(%r14), %rdi
	movl	-80(%rbp), %eax
	subq	$8, %rsp
	movq	%r8, -72(%rbp)
	movl	%r13d, %r9d
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %r10
	pushq	%rax
	call	*40(%r10)
	movq	(%r14), %r14
	popq	%rdi
	popq	%r8
	movq	-72(%rbp), %r8
	testq	%r14, %r14
	jne	.L1767
	movq	-88(%rbp), %r15
.L1768:
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1747
.L1769:
	movq	(%r15), %rax
	cmpq	$1, %rsi
	movq	%rcx, -88(%rbp)
	sbbl	%ebx, %ebx
	movq	%rdx, -80(%rbp)
	movq	41488(%rax), %r13
	movq	%r8, -72(%rbp)
	andl	$4, %ebx
	addl	$17, %ebx
	leaq	56(%r13), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%r13), %r13
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rcx
	testq	%r13, %r13
	je	.L1773
	movq	%r12, -72(%rbp)
	movq	%rdx, %r14
	movq	%r8, %r12
	movq	%rcx, %r15
	.p2align 4,,10
	.p2align 3
.L1772:
	movq	8(%r13), %rdi
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	%ebx, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	0(%r13), %r13
	testq	%r13, %r13
	jne	.L1772
	movq	-72(%rbp), %r12
.L1773:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1747
.L1787:
	movl	$21, %r12d
	jmp	.L1764
.L1821:
	movq	%rsi, %rdi
	movq	%rdx, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %rcx
	movq	-72(%rbp), %rsi
	jmp	.L1761
.L1786:
	movl	$20, %r12d
	jmp	.L1764
.L1790:
	movl	$21, %r12d
	jmp	.L1766
.L1789:
	movl	$20, %r12d
	jmp	.L1766
.L1816:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20788:
	.size	_ZN2v88internal18ExistingCodeLogger19LogExistingFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12AbstractCodeEEENS0_17CodeEventListener16LogEventsAndTagsE, .-_ZN2v88internal18ExistingCodeLogger19LogExistingFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12AbstractCodeEEENS0_17CodeEventListener16LogEventsAndTagsE
	.section	.text._ZN2v88internal6Logger19LogExistingFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12AbstractCodeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger19LogExistingFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12AbstractCodeEEE
	.type	_ZN2v88internal6Logger19LogExistingFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12AbstractCodeEEE, @function
_ZN2v88internal6Logger19LogExistingFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12AbstractCodeEEE:
.LFB20770:
	.cfi_startproc
	endbr64
	addq	$144, %rdi
	movl	$15, %ecx
	jmp	_ZN2v88internal18ExistingCodeLogger19LogExistingFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12AbstractCodeEEENS0_17CodeEventListener16LogEventsAndTagsE
	.cfi_endproc
.LFE20770:
	.size	_ZN2v88internal6Logger19LogExistingFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12AbstractCodeEEE, .-_ZN2v88internal6Logger19LogExistingFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12AbstractCodeEEE
	.section	.text._ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE8_M_eraseEPSt13_Rb_tree_nodeIiE,"axG",@progbits,_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE8_M_eraseEPSt13_Rb_tree_nodeIiE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE8_M_eraseEPSt13_Rb_tree_nodeIiE
	.type	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE8_M_eraseEPSt13_Rb_tree_nodeIiE, @function
_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE8_M_eraseEPSt13_Rb_tree_nodeIiE:
.LFB24645:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1831
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L1825:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE8_M_eraseEPSt13_Rb_tree_nodeIiE
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1825
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1831:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE24645:
	.size	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE8_M_eraseEPSt13_Rb_tree_nodeIiE, .-_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE8_M_eraseEPSt13_Rb_tree_nodeIiE
	.section	.text._ZN2v88internal6LoggerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6LoggerD2Ev
	.type	_ZN2v88internal6LoggerD2Ev, @function
_ZN2v88internal6LoggerD2Ev:
.LFB20703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6LoggerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	48(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L1835
	leaq	360(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	80(%r12), %rdi
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	movq	%rax, 96(%r12)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%r12)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	96(%r12), %rdi
	movq	%rax, 16(%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 96(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	movl	$416, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1835:
	movq	104(%rbx), %r12
	leaq	88(%rbx), %r13
	testq	%r12, %r12
	je	.L1839
.L1836:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE8_M_eraseEPSt13_Rb_tree_nodeIiE
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1836
.L1839:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1838
	movq	(%rdi), %rax
	call	*8(%rax)
.L1838:
	movq	72(%rbx), %r12
	testq	%r12, %r12
	je	.L1840
	movq	(%r12), %rax
	leaq	_ZN2v88internal14LowLevelLoggerD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1841
	leaq	16+_ZTVN2v88internal14LowLevelLoggerE(%rip), %rax
	movq	24(%r12), %rdi
	movq	%rax, (%r12)
	call	fclose@PLT
	movq	16(%r12), %rdi
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rax
	movq	$0, 24(%r12)
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L1842
	movl	$516, %esi
	call	_ZdlPvm@PLT
.L1842:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1840:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1843
	movq	(%rdi), %rax
	call	*8(%rax)
.L1843:
	movq	56(%rbx), %r12
	testq	%r12, %r12
	je	.L1844
	movq	(%r12), %rax
	leaq	_ZN2v88internal15PerfBasicLoggerD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1845
	leaq	16+_ZTVN2v88internal15PerfBasicLoggerE(%rip), %rax
	movq	24(%r12), %rdi
	movq	%rax, (%r12)
	call	fclose@PLT
	movq	16(%r12), %rdi
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rax
	movq	$0, 24(%r12)
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L1846
	movl	$516, %esi
	call	_ZdlPvm@PLT
.L1846:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1844:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1847
	movq	(%rdi), %rax
	call	*8(%rax)
.L1847:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L1834
	movq	(%r12), %rax
	leaq	_ZN2v88internal6TickerD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1849
	leaq	16+_ZTVN2v88internal6TickerE(%rip), %rax
	movq	%rax, (%r12)
	movzbl	32(%r12), %eax
	testb	%al, %al
	jne	.L1884
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1851
.L1885:
	movq	(%rdi), %rax
	call	*8(%rax)
.L1851:
	movq	%r12, %rdi
	call	_ZN2v87sampler7SamplerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1834:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1884:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v87sampler7Sampler4StopEv@PLT
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L1885
	jmp	.L1851
	.p2align 4,,10
	.p2align 3
.L1845:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L1841:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1840
	.p2align 4,,10
	.p2align 3
.L1849:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE20703:
	.size	_ZN2v88internal6LoggerD2Ev, .-_ZN2v88internal6LoggerD2Ev
	.globl	_ZN2v88internal6LoggerD1Ev
	.set	_ZN2v88internal6LoggerD1Ev,_ZN2v88internal6LoggerD2Ev
	.section	.text._ZN2v88internal6LoggerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6LoggerD0Ev
	.type	_ZN2v88internal6LoggerD0Ev, @function
_ZN2v88internal6LoggerD0Ev:
.LFB20705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal6LoggerD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE20705:
	.size	_ZN2v88internal6LoggerD0Ev, .-_ZN2v88internal6LoggerD0Ev
	.section	.text._ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_
	.type	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_, @function
_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_:
.LFB24668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L1889
	movl	(%rsi), %r13d
	jmp	.L1890
	.p2align 4,,10
	.p2align 3
.L1906:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L1891
.L1907:
	movq	%rax, %r12
.L1890:
	movl	32(%r12), %ecx
	cmpl	%ecx, %r13d
	jl	.L1906
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L1907
.L1891:
	testb	%dl, %dl
	jne	.L1908
	cmpl	%ecx, %r13d
	jle	.L1899
.L1898:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L1909
.L1896:
	movl	$40, %edi
	movl	%r8d, -52(%rbp)
	call	_Znwm@PLT
	movl	-52(%rbp), %r8d
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%rax, %r13
	movl	(%r14), %eax
	movq	%r13, %rsi
	movl	%r8d, %edi
	movl	%eax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1908:
	.cfi_restore_state
	cmpq	24(%rbx), %r12
	je	.L1898
.L1900:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpl	32(%rax), %r13d
	jg	.L1898
	movq	%rax, %r12
.L1899:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1909:
	.cfi_restore_state
	xorl	%r8d, %r8d
	cmpl	32(%r12), %r13d
	setl	%r8b
	jmp	.L1896
	.p2align 4,,10
	.p2align 3
.L1889:
	movq	%r15, %r12
	cmpq	24(%rdi), %r15
	je	.L1902
	movl	(%rsi), %r13d
	jmp	.L1900
	.p2align 4,,10
	.p2align 3
.L1902:
	movl	$1, %r8d
	jmp	.L1896
	.cfi_endproc
.LFE24668:
	.size	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_, .-_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_
	.section	.rodata._ZN2v88internal6Logger21EnsureLogScriptSourceENS0_6ScriptE.str1.1,"aMS",@progbits,1
.LC85:
	.string	"script-source"
	.section	.text._ZN2v88internal6Logger21EnsureLogScriptSourceENS0_6ScriptE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger21EnsureLogScriptSourceENS0_6ScriptE
	.type	_ZN2v88internal6Logger21EnsureLogScriptSourceENS0_6ScriptE, @function
_ZN2v88internal6Logger21EnsureLogScriptSourceENS0_6ScriptE:
.LFB20758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	(%rsi), %r12d
	testb	%r12b, %r12b
	jne	.L1923
	cmpq	$0, 8(%rsi)
	je	.L1910
	leaq	-80(%rbp), %r14
	movq	%rdi, %rbx
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movslq	67(%r13), %rsi
	movq	104(%rbx), %rax
	leaq	96(%rbx), %rdi
	movl	%esi, -84(%rbp)
	movl	%esi, %edx
	testq	%rax, %rax
	je	.L1912
	movq	%rdi, %rcx
	jmp	.L1913
	.p2align 4,,10
	.p2align 3
.L1929:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1914
.L1913:
	cmpl	%edx, 32(%rax)
	jge	.L1929
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1913
.L1914:
	cmpq	%rdi, %rcx
	je	.L1912
	cmpl	%esi, 32(%rcx)
	jg	.L1912
	movl	$1, %r12d
.L1917:
	movq	-72(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L1910:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1930
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1923:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L1910
	.p2align 4,,10
	.p2align 3
.L1912:
	leaq	-84(%rbp), %rsi
	leaq	88(%rbx), %rdi
	call	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiESaIiEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_
	movq	7(%r13), %r15
	testb	$1, %r15b
	je	.L1917
	movq	-1(%r15), %rax
	cmpw	$63, 11(%rax)
	ja	.L1917
	leaq	.LC85(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movl	-84(%rbp), %esi
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	15(%r13), %rsi
	testb	$1, %sil
	jne	.L1931
.L1920:
	leaq	.LC21(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
.L1921:
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	$1, %r12d
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	jmp	.L1917
.L1931:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L1920
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	jmp	.L1921
.L1930:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20758:
	.size	_ZN2v88internal6Logger21EnsureLogScriptSourceENS0_6ScriptE, .-_ZN2v88internal6Logger21EnsureLogScriptSourceENS0_6ScriptE
	.section	.rodata._ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii.str1.1,"aMS",@progbits,1
.LC86:
	.string	" "
.LC87:
	.string	":"
.LC88:
	.string	"code-source-info"
.LC89:
	.string	"C"
.LC90:
	.string	"O"
.LC91:
	.string	"I"
.LC92:
	.string	"F"
.LC93:
	.string	"S"
	.section	.text._ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.type	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii, @function
_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii:
.LFB20739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rdx
	subq	$168, %rsp
	movq	%rcx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	136(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1933
	movq	8(%rdi), %rax
	cmpb	$0, 41812(%rax)
	jne	.L1934
	cmpb	$0, 40(%rdi)
	je	.L1934
.L1937:
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	je	.L1932
.L1935:
	movq	48(%r14), %rsi
	movl	%r9d, -176(%rbp)
	cmpb	$0, (%rsi)
	jne	.L1932
	cmpq	$0, 8(%rsi)
	je	.L1932
	leaq	-128(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%rbx, %rdx
	movl	%r15d, %esi
	movq	%r12, %rdi
	leaq	160(%r14), %rcx
	leaq	-168(%rbp), %r15
	call	_ZN2v88internal12_GLOBAL__N_122AppendCodeCreateHeaderERNS0_3Log14MessageBuilderENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPNS_4base12ElapsedTimerE
	movq	%r15, %rdi
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_@PLT
	leaq	.LC86(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_4NameEEERS2_T_@PLT
	leaq	.LC87(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movl	-176(%rbp), %r9d
	movq	%rax, %r13
	movq	(%rax), %rax
	movl	%r9d, %esi
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	movq	%r13, %rdi
	leaq	.LC87(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movl	16(%rbp), %esi
	movq	%rax, %r13
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%rax, %rdi
	movq	-168(%rbp), %rax
	leaq	-1(%rax), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	-168(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rax, %r9
	call	_ZN2v88internalL13ComputeMarkerENS0_18SharedFunctionInfoENS0_12AbstractCodeE
	movq	%r9, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-120(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	cmpb	$0, _ZN2v88internal20FLAG_log_source_codeE(%rip)
	je	.L1932
	movq	-168(%rbp), %rax
	movq	31(%rax), %rdx
	movq	%rdx, %r13
	notq	%r13
	andl	$1, %r13d
	je	.L1994
	.p2align 4,,10
	.p2align 3
.L1932:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1995
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1934:
	.cfi_restore_state
	cmpq	$0, 80(%r14)
	je	.L1932
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	jne	.L1935
	jmp	.L1932
	.p2align 4,,10
	.p2align 3
.L1933:
	movl	%r9d, -176(%rbp)
	call	*%rax
	movl	-176(%rbp), %r9d
	testb	%al, %al
	je	.L1932
	jmp	.L1937
	.p2align 4,,10
	.p2align 3
.L1994:
	movq	-1(%rdx), %rcx
	leaq	-1(%rdx), %rax
	cmpw	$86, 11(%rcx)
	je	.L1996
.L1938:
	movq	(%rax), %rax
	cmpw	$96, 11(%rax)
	jne	.L1932
	movq	%rdx, %rsi
	movq	%r14, %rdi
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal6Logger21EnsureLogScriptSourceENS0_6ScriptE
	movb	%al, -184(%rbp)
	testb	%al, %al
	je	.L1932
	leaq	-144(%rbp), %rax
	movq	48(%r14), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	movq	%r14, %rdi
	leaq	.LC88(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	leaq	53(%rbx), %rsi
	movq	%rax, %r14
	movq	-1(%rbx), %rax
	movq	-192(%rbp), %rdx
	cmpw	$69, 11(%rax)
	je	.L1997
.L1942:
	movq	%r14, %rdi
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	-192(%rbp), %rdx
	movq	%rax, %r14
	movq	(%rax), %rax
	movslq	67(%rdx), %rsi
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movl	%eax, %esi
	movq	(%r14), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	movl	%eax, %esi
	movq	(%r14), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	-1(%rbx), %rax
	cmpw	$69, 11(%rax)
	je	.L1998
	movq	31(%rbx), %rsi
	testb	$1, %sil
	jne	.L1948
.L1952:
	movq	%rbx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	leaq	-37592(%rax), %rdx
	cmpq	-37280(%rax), %rsi
	je	.L1999
.L1988:
	movq	7(%rsi), %rsi
.L1947:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_9ByteArrayENS1_15IterationFilterE@PLT
	cmpl	$-1, -104(%rbp)
	jne	.L1955
	jmp	.L1953
	.p2align 4,,10
	.p2align 3
.L1954:
	movq	%r12, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	movl	-104(%rbp), %r14d
	cmpl	$-1, %r14d
	je	.L2000
.L1955:
	movq	-176(%rbp), %rdi
	leaq	.LC89(%rip), %rsi
	movq	-88(%rbp), %r14
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movl	-96(%rbp), %esi
	movq	%rax, %r15
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	leaq	.LC90(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r14, %rsi
	shrq	%rsi
	movq	(%rax), %rdi
	andl	$1073741823, %esi
	subl	$1, %esi
	addq	$16, %rdi
	call	_ZNSolsEi@PLT
	testb	$1, %r14b
	jne	.L1954
	shrq	$31, %r14
	andl	$65535, %r14d
	je	.L1954
	movq	-176(%rbp), %rdi
	leaq	.LC91(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	leal	-1(%r14), %esi
	movq	(%rax), %rdi
	addq	$16, %rdi
	call	_ZNSolsEi@PLT
	movzbl	-184(%rbp), %r13d
	jmp	.L1954
.L1996:
	movq	23(%rdx), %rdx
	testb	$1, %dl
	je	.L1932
	leaq	-1(%rdx), %rax
	jmp	.L1938
.L2000:
	movq	-176(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	testb	%r13b, %r13b
	jne	.L2001
.L1957:
	movq	-176(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
.L1967:
	movq	-176(%rbp), %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-136(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L1932
.L1998:
	movq	23(%rbx), %rsi
	testb	$1, %sil
	je	.L1988
	movq	-1(%rsi), %rax
	cmpw	$70, 11(%rax)
	jne	.L1988
	jmp	.L1947
.L2001:
	movq	15(%rbx), %rax
	movq	71(%rax), %rax
	leaq	7(%rax), %rcx
	movq	%rcx, -192(%rbp)
	movslq	11(%rax), %rcx
	shrq	$4, %rcx
	testl	%ecx, %ecx
	jle	.L1958
	addq	$15, %rax
	xorl	%r13d, %r13d
	movq	%rax, -200(%rbp)
	jmp	.L1961
	.p2align 4,,10
	.p2align 3
.L1960:
	movq	-192(%rbp), %rax
	addl	$1, %r13d
	movq	(%rax), %rax
	movq	%rax, -184(%rbp)
	sarq	$36, %rax
	cmpl	%r13d, %eax
	jle	.L2002
.L1961:
	movl	%r13d, %eax
	movq	-200(%rbp), %rcx
	movq	-176(%rbp), %rdi
	leaq	.LC92(%rip), %rsi
	sall	$4, %eax
	cltq
	movq	8(%rax,%rcx), %r15
	movq	(%rax,%rcx), %r12
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	cmpl	$-1, %r15d
	je	.L1959
	movq	-144(%rbp), %rax
	movl	%r15d, %esi
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	cmpl	%r15d, %r14d
	cmovl	%r15d, %r14d
.L1959:
	movq	-176(%rbp), %rdi
	leaq	.LC90(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movq	%r12, %rsi
	shrq	%rsi
	movq	(%rax), %rdi
	andl	$1073741823, %esi
	subl	$1, %esi
	addq	$16, %rdi
	call	_ZNSolsEi@PLT
	testb	$1, %r12b
	jne	.L1960
	shrq	$31, %r12
	andl	$65535, %r12d
	je	.L1960
	movq	-176(%rbp), %rdi
	leaq	.LC91(%rip), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	leal	-1(%r12), %esi
	movq	(%rax), %rdi
	addq	$16, %rdi
	call	_ZNSolsEi@PLT
	jmp	.L1960
.L2002:
	movq	-176(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	15(%rbx), %rax
	movq	%rax, -152(%rbp)
	movq	-144(%rbp), %rax
	movq	16(%rax), %rcx
	movq	-24(%rcx), %rcx
	leaq	16(%rax,%rcx), %rsi
	movl	24(%rsi), %ecx
	andl	$-75, %ecx
	orl	$8, %ecx
	movl	%ecx, 24(%rsi)
	cmpl	$-1, %r14d
	je	.L1968
	addl	$1, %r14d
	xorl	%ebx, %ebx
	leaq	-152(%rbp), %r13
	leaq	.LC93(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L1963:
	movq	-176(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	movl	%ebx, %esi
	movq	%r13, %rdi
	addl	$1, %ebx
	movq	%rax, %r15
	call	_ZN2v88internal18DeoptimizationData18GetInlinedFunctionEi@PLT
	movq	%r15, %rdi
	leaq	-1(%rax), %rsi
	call	_ZN2v88internal3Log14MessageBuilderlsIPvEERS2_T_@PLT
	cmpl	%r14d, %ebx
	jne	.L1963
	movq	-144(%rbp), %rax
.L1968:
	movq	16(%rax), %rdx
	movq	-24(%rdx), %rdx
	leaq	16(%rax,%rdx), %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$2, %eax
	movl	%eax, 24(%rdx)
	jmp	.L1967
.L1948:
	movq	-1(%rsi), %rax
	cmpw	$70, 11(%rax)
	jne	.L1952
	jmp	.L1947
.L1997:
	cmpl	$0, 43(%rbx)
	movq	%rbx, -128(%rbp)
	leaq	63(%rbx), %rsi
	jns	.L1942
	movq	%r12, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-192(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L1942
.L1999:
	movq	976(%rdx), %rsi
	jmp	.L1947
.L1953:
	movq	-176(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	jmp	.L1957
.L1995:
	call	__stack_chk_fail@PLT
.L1958:
	movq	-176(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	15(%rbx), %rax
	movq	%rax, -152(%rbp)
	movq	-144(%rbp), %rax
	movq	16(%rax), %rdx
	movq	-24(%rdx), %rdx
	leaq	16(%rax,%rdx), %rcx
	movl	24(%rcx), %edx
	andl	$-75, %edx
	orl	$8, %edx
	movl	%edx, 24(%rcx)
	jmp	.L1968
	.cfi_endproc
.LFE20739:
	.size	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii, .-_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.section	.rodata._ZN2v88internal6Logger13ScriptDetailsENS0_6ScriptE.str1.1,"aMS",@progbits,1
.LC94:
	.string	"script-details"
	.section	.text._ZN2v88internal6Logger13ScriptDetailsENS0_6ScriptE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger13ScriptDetailsENS0_6ScriptE
	.type	_ZN2v88internal6Logger13ScriptDetailsENS0_6ScriptE, @function
_ZN2v88internal6Logger13ScriptDetailsENS0_6ScriptE:
.LFB20757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 12, -48
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rsi)
	jne	.L2003
	cmpq	$0, 8(%rsi)
	je	.L2003
	cmpb	$0, _ZN2v88internal24FLAG_log_function_eventsE(%rip)
	jne	.L2013
	.p2align 4,,10
	.p2align 3
.L2003:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2014
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2013:
	.cfi_restore_state
	leaq	-64(%rbp), %r14
	movq	%rdi, %r12
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderC1EPS1_@PLT
	leaq	.LC94(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsIPKcEERS2_T_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movslq	67(%r13), %rsi
	movq	%rax, %r15
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	15(%r13), %rsi
	testb	$1, %sil
	jne	.L2015
.L2006:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movslq	27(%r13), %rsi
	movq	%rax, %r15
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	movq	%r15, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movslq	35(%r13), %rsi
	movq	%rax, %r15
	movq	(%rax), %rax
	leaq	16(%rax), %rdi
	call	_ZNSolsEi@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_12LogSeparatorEEERS2_T_@PLT
	movq	111(%r13), %rsi
	testb	$1, %sil
	jne	.L2016
.L2009:
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilder14WriteToLogFileEv@PLT
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger21EnsureLogScriptSourceENS0_6ScriptE
	jmp	.L2003
	.p2align 4,,10
	.p2align 3
.L2015:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L2006
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_@PLT
	jmp	.L2006
	.p2align 4,,10
	.p2align 3
.L2016:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L2009
	movq	%r14, %rdi
	call	_ZN2v88internal3Log14MessageBuilderlsINS0_6StringEEERS2_T_@PLT
	jmp	.L2009
.L2014:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20757:
	.size	_ZN2v88internal6Logger13ScriptDetailsENS0_6ScriptE, .-_ZN2v88internal6Logger13ScriptDetailsENS0_6ScriptE
	.section	.text._ZN2v88internal8NewArrayINS0_6HandleINS0_18SharedFunctionInfoEEEEEPT_m,"axG",@progbits,_ZN2v88internal8NewArrayINS0_6HandleINS0_18SharedFunctionInfoEEEEEPT_m,comdat
	.p2align 4
	.weak	_ZN2v88internal8NewArrayINS0_6HandleINS0_18SharedFunctionInfoEEEEEPT_m
	.type	_ZN2v88internal8NewArrayINS0_6HandleINS0_18SharedFunctionInfoEEEEEPT_m, @function
_ZN2v88internal8NewArrayINS0_6HandleINS0_18SharedFunctionInfoEEEEEPT_m:
.LFB24702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZSt7nothrow(%rip), %rsi
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdi
	movq	$-1, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	0(,%rdi,8), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmova	%rax, %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L2019
	movq	%rbx, %rsi
	subq	$1, %rsi
	js	.L2017
	leaq	-2(%rbx), %rdx
	movl	$1, %edi
	cmpq	$-1, %rdx
	cmovge	%rbx, %rdi
	cmpq	$1, %rbx
	je	.L2032
	cmpq	$-1, %rdx
	jl	.L2032
	movq	%rdi, %rsi
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	shrq	%rsi
	.p2align 4,,10
	.p2align 3
.L2023:
	movq	%rdx, %rcx
	addq	$1, %rdx
	salq	$4, %rcx
	movups	%xmm0, (%rax,%rcx)
	cmpq	%rdx, %rsi
	jne	.L2023
	movq	%rdi, %rcx
	andq	$-2, %rcx
	leaq	(%rax,%rcx,8), %rdx
	cmpq	%rcx, %rdi
	je	.L2017
.L2021:
	movq	$0, (%rdx)
.L2017:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2032:
	.cfi_restore_state
	movq	%rax, %rdx
	jmp	.L2021
.L2019:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L2036
	movq	%rbx, %rdi
	subq	$1, %rdi
	js	.L2017
	leaq	-2(%rbx), %rcx
	movl	$1, %edx
	addq	$1, %rcx
	cmovge	%rbx, %rdx
	jl	.L2033
	subq	$1, %rbx
	je	.L2033
	movq	%rdx, %rsi
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	shrq	%rsi
.L2027:
	movq	%rcx, %rdi
	addq	$1, %rcx
	salq	$4, %rdi
	movups	%xmm0, (%rax,%rdi)
	cmpq	%rcx, %rsi
	jne	.L2027
	movq	%rdx, %rsi
	andq	$-2, %rsi
	leaq	(%rax,%rsi,8), %rcx
	cmpq	%rsi, %rdx
	je	.L2017
.L2025:
	movq	$0, (%rcx)
	jmp	.L2017
.L2036:
	leaq	.LC30(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L2033:
	movq	%rax, %rcx
	jmp	.L2025
	.cfi_endproc
.LFE24702:
	.size	_ZN2v88internal8NewArrayINS0_6HandleINS0_18SharedFunctionInfoEEEEEPT_m, .-_ZN2v88internal8NewArrayINS0_6HandleINS0_18SharedFunctionInfoEEEEEPT_m
	.section	.text._ZN2v88internal8NewArrayINS0_6HandleINS0_12AbstractCodeEEEEEPT_m,"axG",@progbits,_ZN2v88internal8NewArrayINS0_6HandleINS0_12AbstractCodeEEEEEPT_m,comdat
	.p2align 4
	.weak	_ZN2v88internal8NewArrayINS0_6HandleINS0_12AbstractCodeEEEEEPT_m
	.type	_ZN2v88internal8NewArrayINS0_6HandleINS0_12AbstractCodeEEEEEPT_m, @function
_ZN2v88internal8NewArrayINS0_6HandleINS0_12AbstractCodeEEEEEPT_m:
.LFB24707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZSt7nothrow(%rip), %rsi
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdi
	movq	$-1, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	0(,%rdi,8), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmova	%rax, %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L2039
	movq	%rbx, %rsi
	subq	$1, %rsi
	js	.L2037
	leaq	-2(%rbx), %rdx
	movl	$1, %edi
	cmpq	$-1, %rdx
	cmovge	%rbx, %rdi
	cmpq	$1, %rbx
	je	.L2052
	cmpq	$-1, %rdx
	jl	.L2052
	movq	%rdi, %rsi
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	shrq	%rsi
	.p2align 4,,10
	.p2align 3
.L2043:
	movq	%rdx, %rcx
	addq	$1, %rdx
	salq	$4, %rcx
	movups	%xmm0, (%rax,%rcx)
	cmpq	%rdx, %rsi
	jne	.L2043
	movq	%rdi, %rcx
	andq	$-2, %rcx
	leaq	(%rax,%rcx,8), %rdx
	cmpq	%rcx, %rdi
	je	.L2037
.L2041:
	movq	$0, (%rdx)
.L2037:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2052:
	.cfi_restore_state
	movq	%rax, %rdx
	jmp	.L2041
.L2039:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L2056
	movq	%rbx, %rdi
	subq	$1, %rdi
	js	.L2037
	leaq	-2(%rbx), %rcx
	movl	$1, %edx
	addq	$1, %rcx
	cmovge	%rbx, %rdx
	jl	.L2053
	subq	$1, %rbx
	je	.L2053
	movq	%rdx, %rsi
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	shrq	%rsi
.L2047:
	movq	%rcx, %rdi
	addq	$1, %rcx
	salq	$4, %rdi
	movups	%xmm0, (%rax,%rdi)
	cmpq	%rcx, %rsi
	jne	.L2047
	movq	%rdx, %rsi
	andq	$-2, %rsi
	leaq	(%rax,%rsi,8), %rcx
	cmpq	%rsi, %rdx
	je	.L2037
.L2045:
	movq	$0, (%rcx)
	jmp	.L2037
.L2056:
	leaq	.LC30(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L2053:
	movq	%rax, %rcx
	jmp	.L2045
	.cfi_endproc
.LFE24707:
	.size	_ZN2v88internal8NewArrayINS0_6HandleINS0_12AbstractCodeEEEEEPT_m, .-_ZN2v88internal8NewArrayINS0_6HandleINS0_12AbstractCodeEEEEEPT_m
	.section	.text._ZN2v88internal18ExistingCodeLogger20LogCompiledFunctionsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ExistingCodeLogger20LogCompiledFunctionsEv
	.type	_ZN2v88internal18ExistingCodeLogger20LogCompiledFunctionsEv, @function
_ZN2v88internal18ExistingCodeLogger20LogCompiledFunctionsEv:
.LFB20783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	41088(%rax), %rcx
	leaq	37592(%rax), %r15
	addl	$1, 41104(%rax)
	movq	%r15, %rdi
	movq	%rax, -104(%rbp)
	movq	%rcx, -136(%rbp)
	movq	41096(%rax), %rcx
	movq	%rcx, -112(%rbp)
	call	_ZN2v88internalL26EnumerateCompiledFunctionsEPNS0_4HeapEPNS0_6HandleINS0_18SharedFunctionInfoEEEPNS3_INS0_12AbstractCodeEEE
	movslq	%eax, %r12
	movq	%r12, %rdi
	movq	%r12, %r13
	call	_ZN2v88internal8NewArrayINS0_6HandleINS0_18SharedFunctionInfoEEEEEPT_m
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8NewArrayINS0_6HandleINS0_12AbstractCodeEEEEEPT_m
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	movq	%rax, -120(%rbp)
	movq	%rax, %r12
	call	_ZN2v88internalL26EnumerateCompiledFunctionsEPNS0_4HeapEPNS0_6HandleINS0_18SharedFunctionInfoEEEPNS3_INS0_12AbstractCodeEEE
	testl	%r13d, %r13d
	jle	.L2058
	leal	-1(%r13), %eax
	leaq	8(%r14,%rax,8), %r13
	jmp	.L2070
	.p2align 4,,10
	.p2align 3
.L2060:
	movq	(%rbx), %rax
	movl	$67, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	(%r12), %rdx
	cmpq	%rax, %rdx
	je	.L2066
	testq	%rdx, %rdx
	je	.L2068
	testq	%rax, %rax
	je	.L2068
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	je	.L2066
.L2068:
	movq	(%r12), %rdx
	movq	(%r14), %rsi
	movl	$15, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal18ExistingCodeLogger19LogExistingFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12AbstractCodeEEENS0_17CodeEventListener16LogEventsAndTagsE
.L2066:
	addq	$8, %r14
	addq	$8, %r12
	cmpq	%r14, %r13
	je	.L2058
.L2070:
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	call	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%r14), %rax
	movq	(%rax), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	je	.L2060
	movq	-1(%rax), %rax
	cmpw	$91, 11(%rax)
	jne	.L2060
	movq	(%r14), %rax
	movq	(%rbx), %rcx
	movq	(%rax), %rax
	movq	7(%rax), %rax
	movq	41112(%rcx), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2062
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L2063:
	movq	(%r14), %rsi
	movl	$12, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal18ExistingCodeLogger19LogExistingFunctionENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_12AbstractCodeEEENS0_17CodeEventListener16LogEventsAndTagsE
	jmp	.L2060
	.p2align 4,,10
	.p2align 3
.L2058:
	leaq	-96(%rbp), %r12
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIteratorC1EPNS0_4HeapENS1_20HeapObjectsFilteringE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	je	.L2071
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L2073:
	movq	-1(%rax), %rax
	movq	%r12, %rdi
	cmpw	$1102, 11(%rax)
	sete	%al
	movzbl	%al, %eax
	addl	%eax, %r13d
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	jne	.L2073
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movslq	%r13d, %rsi
	leaq	0(,%rsi,8), %rdi
	movq	%rsi, -144(%rbp)
	call	_Znam@PLT
	movq	-144(%rbp), %rsi
	movq	%rax, %r14
	testq	%rsi, %rsi
	je	.L2074
	cmpl	$1, %r13d
	je	.L2075
	movq	%rsi, %rcx
	movq	-152(%rbp), %rdx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	.p2align 4,,10
	.p2align 3
.L2076:
	movq	%rdx, %rax
	addq	$1, %rdx
	salq	$4, %rax
	movups	%xmm0, (%r14,%rax)
	cmpq	%rcx, %rdx
	jne	.L2076
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	(%r14,%rdx,8), %rax
	cmpq	%rdx, %rsi
	je	.L2074
.L2075:
	movq	$0, (%rax)
.L2074:
	movq	%r15, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIteratorC1EPNS0_4HeapENS1_20HeapObjectsFilteringE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2078
.L2090:
	xorl	%eax, %eax
	movl	%r13d, -144(%rbp)
	subq	$37592, %r15
	movq	%rbx, -152(%rbp)
	movl	%eax, %r13d
	jmp	.L2083
	.p2align 4,,10
	.p2align 3
.L2079:
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2118
.L2083:
	movq	-1(%rsi), %rax
	cmpw	$1102, 11(%rax)
	jne	.L2079
	movq	41112(%r15), %rdi
	movslq	%r13d, %rax
	leaq	(%r14,%rax,8), %rbx
	testq	%rdi, %rdi
	je	.L2080
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L2081:
	movq	%rax, (%rbx)
	movq	%r12, %rdi
	addl	$1, %r13d
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L2083
.L2118:
	movl	-144(%rbp), %r13d
	movq	-152(%rbp), %rbx
.L2078:
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	testl	%r13d, %r13d
	je	.L2084
	leal	-1(%r13), %eax
	movq	%r14, %r15
	leaq	8(%r14,%rax,8), %r12
	.p2align 4,,10
	.p2align 3
.L2085:
	movq	(%r15), %rax
	movq	(%rbx), %rsi
	addq	$8, %r15
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm12NativeModule12LogWasmCodesEPNS0_7IsolateE@PLT
	cmpq	%r12, %r15
	jne	.L2085
.L2084:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	je	.L2086
	movq	%rax, %rdi
	call	_ZdaPv@PLT
.L2086:
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L2087
	movq	%rax, %rdi
	call	_ZdaPv@PLT
.L2087:
	movq	-104(%rbp), %rax
	movq	-136(%rbp), %rcx
	subl	$1, 41104(%rax)
	movq	%rcx, 41088(%rax)
	movq	-112(%rbp), %rcx
	cmpq	41096(%rax), %rcx
	je	.L2057
	movq	%rcx, 41096(%rax)
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2057:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2119
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2080:
	.cfi_restore_state
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L2120
.L2082:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2081
	.p2align 4,,10
	.p2align 3
.L2062:
	movq	41088(%rcx), %rdx
	cmpq	%rdx, 41096(%rcx)
	je	.L2121
.L2064:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L2063
	.p2align 4,,10
	.p2align 3
.L2120:
	movq	%r15, %rdi
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	jmp	.L2082
	.p2align 4,,10
	.p2align 3
.L2071:
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	xorl	%edi, %edi
	call	_Znam@PLT
	movq	%r15, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal18HeapObjectIteratorC1EPNS0_4HeapENS1_20HeapObjectsFilteringE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L2090
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	jmp	.L2084
.L2121:
	movq	%rcx, %rdi
	movq	%rsi, -152(%rbp)
	movq	%rcx, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L2064
.L2119:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20783:
	.size	_ZN2v88internal18ExistingCodeLogger20LogCompiledFunctionsEv, .-_ZN2v88internal18ExistingCodeLogger20LogCompiledFunctionsEv
	.section	.text._ZN2v88internal25ExternalCodeEventListener15LogExistingCodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ExternalCodeEventListener15LogExistingCodeEv
	.type	_ZN2v88internal25ExternalCodeEventListener15LogExistingCodeEv, @function
_ZN2v88internal25ExternalCodeEventListener15LogExistingCodeEv:
.LFB20610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -56(%rbp)
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	16(%rdi), %rax
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18ExistingCodeLogger14LogCodeObjectsEv
	movq	%r13, %rdi
	call	_ZN2v88internal18ExistingCodeLogger20LogCompiledFunctionsEv
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2122
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2122:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2126
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2126:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20610:
	.size	_ZN2v88internal25ExternalCodeEventListener15LogExistingCodeEv, .-_ZN2v88internal25ExternalCodeEventListener15LogExistingCodeEv
	.section	.text._ZN2v88internal6Logger20LogCompiledFunctionsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger20LogCompiledFunctionsEv
	.type	_ZN2v88internal6Logger20LogCompiledFunctionsEv, @function
_ZN2v88internal6Logger20LogCompiledFunctionsEv:
.LFB20771:
	.cfi_startproc
	endbr64
	addq	$144, %rdi
	jmp	_ZN2v88internal18ExistingCodeLogger20LogCompiledFunctionsEv
	.cfi_endproc
.LFE20771:
	.size	_ZN2v88internal6Logger20LogCompiledFunctionsEv, .-_ZN2v88internal6Logger20LogCompiledFunctionsEv
	.section	.text._ZNSt10_HashtableIPN2v88internal17CodeEventListenerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal17CodeEventListenerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal17CodeEventListenerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.type	_ZNSt10_HashtableIPN2v88internal17CodeEventListenerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, @function
_ZNSt10_HashtableIPN2v88internal17CodeEventListenerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm:
.LFB25592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L2129
	movq	(%rbx), %r8
.L2130:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L2139
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L2140:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2129:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L2153
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2154
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L2132:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L2134
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L2135
	.p2align 4,,10
	.p2align 3
.L2136:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L2137:
	testq	%rsi, %rsi
	je	.L2134
.L2135:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L2136
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L2142
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L2135
	.p2align 4,,10
	.p2align 3
.L2134:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L2138
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L2138:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L2130
	.p2align 4,,10
	.p2align 3
.L2139:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L2141
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L2141:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L2140
	.p2align 4,,10
	.p2align 3
.L2142:
	movq	%rdx, %rdi
	jmp	.L2137
	.p2align 4,,10
	.p2align 3
.L2153:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L2132
.L2154:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE25592:
	.size	_ZNSt10_HashtableIPN2v88internal17CodeEventListenerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, .-_ZNSt10_HashtableIPN2v88internal17CodeEventListenerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.section	.text._ZN2v88internal25ExternalCodeEventListener14StartListeningEPNS_16CodeEventHandlerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ExternalCodeEventListener14StartListeningEPNS_16CodeEventHandlerE
	.type	_ZN2v88internal25ExternalCodeEventListener14StartListeningEPNS_16CodeEventHandlerE, @function
_ZN2v88internal25ExternalCodeEventListener14StartListeningEPNS_16CodeEventHandlerE:
.LFB20611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 8(%rdi)
	jne	.L2155
	testq	%rsi, %rsi
	je	.L2155
	movq	16(%rdi), %rax
	movq	%rsi, 24(%rdi)
	movq	%rdi, %r12
	movq	41488(%rax), %r14
	leaq	56(%r14), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%r14), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r13
	testq	%rax, %rax
	je	.L2157
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2159
	.p2align 4,,10
	.p2align 3
.L2170:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2157
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r13
	jne	.L2157
.L2159:
	cmpq	%rsi, %r12
	jne	.L2170
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movb	$0, 8(%r12)
.L2155:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2171
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2157:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r12, 8(%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	$0, (%rax)
	call	_ZNSt10_HashtableIPN2v88internal17CodeEventListenerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	16(%r12), %r13
	movb	$1, 8(%r12)
	movq	%r12, -56(%rbp)
	addl	$1, 41104(%r13)
	movq	16(%r12), %rax
	leaq	-64(%rbp), %r12
	movq	41088(%r13), %r15
	movq	%r12, %rdi
	movq	41096(%r13), %r14
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18ExistingCodeLogger14LogCodeObjectsEv
	movq	%r12, %rdi
	call	_ZN2v88internal18ExistingCodeLogger20LogCompiledFunctionsEv
	subl	$1, 41104(%r13)
	movq	%r15, 41088(%r13)
	cmpq	41096(%r13), %r14
	je	.L2155
	movq	%r14, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L2155
.L2171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20611:
	.size	_ZN2v88internal25ExternalCodeEventListener14StartListeningEPNS_16CodeEventHandlerE, .-_ZN2v88internal25ExternalCodeEventListener14StartListeningEPNS_16CodeEventHandlerE
	.section	.rodata._ZN2v88internal6Logger20AddCodeEventListenerEPNS0_17CodeEventListenerE.str1.1,"aMS",@progbits,1
.LC95:
	.string	"result"
	.section	.text._ZN2v88internal6Logger20AddCodeEventListenerEPNS0_17CodeEventListenerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger20AddCodeEventListenerEPNS0_17CodeEventListenerE
	.type	_ZN2v88internal6Logger20AddCodeEventListenerEPNS0_17CodeEventListenerE, @function
_ZN2v88internal6Logger20AddCodeEventListenerEPNS0_17CodeEventListenerE:
.LFB20706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	8(%rdi), %rax
	movq	%rsi, %r12
	movq	41488(%rax), %r13
	leaq	56(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%r13), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	0(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r15
	testq	%rax, %rax
	je	.L2173
	movq	(%rax), %rcx
	movq	8(%rcx), %r8
	jmp	.L2175
	.p2align 4,,10
	.p2align 3
.L2184:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2173
	movq	8(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %r15
	jne	.L2173
.L2175:
	cmpq	%r8, %r12
	jne	.L2184
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	leaq	.LC95(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2173:
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r12, 8(%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	$0, (%rax)
	call	_ZNSt10_HashtableIPN2v88internal17CodeEventListenerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	popq	%r12
	movq	%r14, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE20706:
	.size	_ZN2v88internal6Logger20AddCodeEventListenerEPNS0_17CodeEventListenerE, .-_ZN2v88internal6Logger20AddCodeEventListenerEPNS0_17CodeEventListenerE
	.section	.rodata._ZN2v88internal6Logger5SetUpEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC96:
	.string	"isolate-"
.LC97:
	.string	"-"
.LC98:
	.string	"SamplingThread"
	.section	.text._ZN2v88internal6Logger5SetUpEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger5SetUpEPNS0_7IsolateE
	.type	_ZN2v88internal6Logger5SetUpEPNS0_7IsolateE, @function
_ZN2v88internal6Logger5SetUpEPNS0_7IsolateE:
.LFB20776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$904, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -880(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 140(%rdi)
	je	.L2282
.L2186:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2283
	addq	$904, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2282:
	.cfi_restore_state
	movb	$1, 140(%rdi)
	leaq	-704(%rbp), %rbx
	movq	%rdi, %r12
	movq	.LC22(%rip), %xmm1
	movq	%rbx, %rdi
	movq	%rbx, -904(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r15
	leaq	-816(%rbp), %r13
	movhps	.LC23(%rip), %xmm1
	leaq	-320(%rbp), %r14
	movaps	%xmm1, -896(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movw	%ax, -480(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, -704(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -816(%rbp)
	movups	%xmm0, -472(%rbp)
	movups	%xmm0, -456(%rbp)
	movq	$0, -488(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -816(%rbp,%rax)
	movq	-816(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	-752(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-896(%rbp), %xmm1
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rcx, %rdi
	movaps	%xmm0, -800(%rbp)
	movaps	%xmm1, -816(%rbp)
	movq	%rax, -704(%rbp)
	movaps	%xmm0, -784(%rbp)
	movaps	%xmm0, -768(%rbp)
	movq	%rcx, -936(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-720(%rbp), %rdx
	movq	%rbx, %rdi
	leaq	-808(%rbp), %rsi
	movq	%rcx, -808(%rbp)
	leaq	-432(%rbp), %rbx
	movq	%rdx, -944(%rbp)
	movq	%rdx, -736(%rbp)
	movl	$16, -744(%rbp)
	movq	$0, -728(%rbp)
	movb	$0, -720(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	%r14, %rdi
	movq	%r14, -928(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movw	%dx, -96(%rbp)
	movq	%rsi, -432(%rbp)
	movq	%r15, -320(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rsi), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rsi, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	xorl	%esi, %esi
	addq	-24(%rax), %rbx
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movdqa	-896(%rbp), %xmm1
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -912(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	-336(%rbp), %rax
	movq	%r14, %rdi
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-424(%rbp), %rsi
	movq	%rax, -920(%rbp)
	movq	%rax, -352(%rbp)
	movq	%rcx, -424(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	_ZN2v88internal12FLAG_logfileE(%rip), %r15
	movzbl	(%r15), %eax
	movq	%r15, %r14
	testb	%al, %al
	je	.L2281
	.p2align 4,,10
	.p2align 3
.L2187:
	movsbl	%al, %edi
	call	_ZN2v84base2OS20isDirectorySeparatorEc@PLT
	cmpb	$1, %al
	sbbl	$-1, %ebx
	movzbl	1(%r14), %eax
	addq	$1, %r14
	testb	%al, %al
	jne	.L2187
	movzbl	(%r15), %eax
	testb	%al, %al
	je	.L2281
	movq	%r12, -896(%rbp)
	leaq	-864(%rbp), %r14
	movq	%r15, %r12
	jmp	.L2191
	.p2align 4,,10
	.p2align 3
.L2285:
	movzbl	1(%r12), %eax
	cmpb	$112, %al
	je	.L2196
	jg	.L2197
	testb	%al, %al
	je	.L2279
	cmpb	$37, %al
	jne	.L2199
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	addq	$2, %r12
	movb	$37, -864(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	(%r12), %eax
	.p2align 4,,10
	.p2align 3
.L2201:
	testb	%al, %al
	je	.L2279
.L2191:
	testl	%ebx, %ebx
	je	.L2284
.L2194:
	leaq	1(%r12), %r15
	cmpb	$37, %al
	je	.L2285
	movsbl	%al, %edi
	call	_ZN2v84base2OS20isDirectorySeparatorEc@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	cmpb	$1, %al
	movzbl	(%r12), %eax
	adcl	$-1, %ebx
	movb	%al, -864(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	1(%r12), %eax
	movq	%r15, %r12
	jmp	.L2201
	.p2align 4,,10
	.p2align 3
.L2284:
	cmpb	$0, _ZN2v88internal24FLAG_logfile_per_isolateE(%rip)
	movl	$-1, %ebx
	je	.L2194
	movl	$8, %edx
	leaq	.LC96(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-880(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC97(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	call	_ZN2v84base2OS19GetCurrentProcessIdEv@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC97(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	(%r12), %eax
	jmp	.L2194
	.p2align 4,,10
	.p2align 3
.L2197:
	cmpb	$116, %al
	jne	.L2199
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	addq	$2, %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*128(%rax)
	movq	%r13, %rdi
	cvttsd2siq	%xmm0, %rsi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	movzbl	(%r12), %eax
	jmp	.L2201
	.p2align 4,,10
	.p2align 3
.L2279:
	movq	-896(%rbp), %r12
.L2190:
	movq	-768(%rbp), %rax
	leaq	-832(%rbp), %rbx
	movq	$0, -840(%rbp)
	leaq	-848(%rbp), %r13
	movq	%rbx, -848(%rbp)
	movb	$0, -832(%rbp)
	testq	%rax, %rax
	je	.L2286
	movq	-784(%rbp), %r8
	movq	-776(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L2203
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L2204:
	movq	-848(%rbp), %rdx
	movl	$416, %edi
	movq	%rdx, -896(%rbp)
	call	_Znwm@PLT
	movq	-896(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal3LogC1EPNS0_6LoggerEPKc@PLT
	movq	-848(%rbp), %rdi
	movq	%r15, 48(%r12)
	cmpq	%rbx, %rdi
	je	.L2205
	call	_ZdlPv@PLT
.L2205:
	cmpb	$0, _ZN2v88internal20FLAG_perf_basic_profE(%rip)
	jne	.L2287
.L2206:
	cmpb	$0, _ZN2v88internal14FLAG_perf_profE(%rip)
	jne	.L2288
.L2210:
	cmpb	$0, _ZN2v88internal12FLAG_ll_profE(%rip)
	jne	.L2289
.L2212:
	movl	$64, %edi
	leaq	16+_ZTVN2v88internal6TickerE(%rip), %r15
	call	_Znwm@PLT
	movl	_ZN2v88internal27FLAG_prof_sampling_intervalE(%rip), %edx
	movq	-880(%rbp), %rsi
	movq	%rax, %rbx
	movq	%rax, %rdi
	movl	%edx, -896(%rbp)
	call	_ZN2v87sampler7SamplerC2EPNS_7IsolateE@PLT
	movq	%r15, (%rbx)
	movl	$64, %edi
	movq	$0, 48(%rbx)
	call	_Znwm@PLT
	movq	%r14, %rsi
	movl	$65536, -856(%rbp)
	movq	%rax, %r13
	leaq	.LC98(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -864(%rbp)
	call	_ZN2v84base6ThreadC2ERKNS1_7OptionsE@PLT
	movl	-896(%rbp), %edx
	leaq	16+_ZTVN2v88internal14SamplingThreadE(%rip), %rax
	movq	%rbx, 48(%r13)
	movq	%rax, 0(%r13)
	movl	%edx, 56(%r13)
	movq	%r13, 56(%rbx)
	movq	16(%r12), %r13
	movq	%rbx, 16(%r12)
	testq	%r13, %r13
	je	.L2220
	movq	0(%r13), %rax
	leaq	_ZN2v88internal6TickerD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2221
	movq	%r15, 0(%r13)
	movzbl	32(%r13), %eax
	testb	%al, %al
	jne	.L2290
.L2222:
	movq	56(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2223
	movq	(%rdi), %rax
	call	*8(%rax)
.L2223:
	movq	%r13, %rdi
	call	_ZN2v87sampler7SamplerD2Ev@PLT
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2220:
	cmpb	$0, _ZN2v88internal8FLAG_logE(%rip)
	jne	.L2224
	cmpb	$0, _ZN2v88internal12FLAG_log_apiE(%rip)
	je	.L2291
.L2224:
	movb	$1, 40(%r12)
.L2225:
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	cmpb	$0, _ZN2v88internal13FLAG_prof_cppE(%rip)
	movq	%rax, 160(%r12)
	jne	.L2292
	cmpb	$0, 40(%r12)
	jne	.L2293
.L2229:
	movq	.LC22(%rip), %xmm2
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC24(%rip), %xmm2
	movaps	%xmm2, -880(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-920(%rbp), %rdi
	je	.L2230
	call	_ZdlPv@PLT
.L2230:
	movq	-912(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-928(%rbp), %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movdqa	-880(%rbp), %xmm3
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-736(%rbp), %rdi
	movq	%rax, -704(%rbp)
	movaps	%xmm3, -816(%rbp)
	cmpq	-944(%rbp), %rdi
	je	.L2231
	call	_ZdlPv@PLT
.L2231:
	movq	-936(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -808(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-904(%rbp), %rdi
	movq	%rax, -816(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -816(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -704(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2186
	.p2align 4,,10
	.p2align 3
.L2199:
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	addq	$2, %r12
	movb	$37, -864(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movzbl	-1(%r12), %eax
	movb	%al, -864(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	(%r12), %eax
	jmp	.L2201
	.p2align 4,,10
	.p2align 3
.L2196:
	call	_ZN2v84base2OS19GetCurrentProcessIdEv@PLT
	movq	%r13, %rdi
	addq	$2, %r12
	movl	%eax, %esi
	call	_ZNSolsEi@PLT
	movzbl	(%r12), %eax
	jmp	.L2201
	.p2align 4,,10
	.p2align 3
.L2203:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L2204
	.p2align 4,,10
	.p2align 3
.L2291:
	cmpb	$0, _ZN2v88internal13FLAG_log_codeE(%rip)
	jne	.L2224
	cmpb	$0, _ZN2v88internal16FLAG_log_handlesE(%rip)
	jne	.L2224
	cmpb	$0, _ZN2v88internal16FLAG_log_suspectE(%rip)
	jne	.L2224
	cmpb	$0, _ZN2v88internal12FLAG_ll_profE(%rip)
	jne	.L2224
	cmpb	$0, _ZN2v88internal20FLAG_perf_basic_profE(%rip)
	jne	.L2224
	cmpb	$0, _ZN2v88internal14FLAG_perf_profE(%rip)
	jne	.L2224
	cmpb	$0, _ZN2v88internal20FLAG_log_source_codeE(%rip)
	jne	.L2224
	cmpb	$0, _ZN2v88internal30FLAG_log_internal_timer_eventsE(%rip)
	jne	.L2224
	cmpb	$0, _ZN2v88internal13FLAG_prof_cppE(%rip)
	jne	.L2224
	cmpb	$0, _ZN2v88internal13FLAG_trace_icE(%rip)
	jne	.L2224
	cmpb	$0, _ZN2v88internal24FLAG_log_function_eventsE(%rip)
	je	.L2225
	jmp	.L2224
	.p2align 4,,10
	.p2align 3
.L2290:
	movq	%r13, %rdi
	call	_ZN2v87sampler7Sampler4StopEv@PLT
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2292:
	movl	$529520, %edi
	call	_Znwm@PLT
	movq	%r14, %rsi
	movl	$0, -856(%rbp)
	movq	%rax, %r13
	leaq	.LC33(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -864(%rbp)
	call	_ZN2v84base6ThreadC2ERKNS1_7OptionsE@PLT
	leaq	16+_ZTVN2v88internal8ProfilerE(%rip), %rax
	leaq	56(%r13), %rdx
	pxor	%xmm0, %xmm0
	movq	%rax, 0(%r13)
	movq	-880(%rbp), %rax
	movq	%rax, 48(%r13)
	leaq	529464(%r13), %rax
	.p2align 4,,10
	.p2align 3
.L2227:
	movzwl	4112(%rdx), %ecx
	movups	%xmm0, 8(%rdx)
	addq	$4136, %rdx
	movl	$5, -4136(%rdx)
	andw	$-1024, %cx
	movq	$0, -32(%rdx)
	orb	$2, %ch
	movups	%xmm0, -16(%rdx)
	movw	%cx, -24(%rdx)
	cmpq	%rdx, %rax
	jne	.L2227
	movl	$0, 529464(%r13)
	leaq	529480(%r13), %rdi
	xorl	%esi, %esi
	movb	$0, 529472(%r13)
	call	_ZN2v84base9SemaphoreC1Ei@PLT
	movl	$0, 529468(%r13)
	movl	$0, 529512(%r13)
	movq	24(%r12), %rdi
	movq	%r13, 24(%r12)
	testq	%rdi, %rdi
	je	.L2228
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	24(%r12), %r13
.L2228:
	movb	$1, 40(%r12)
	movq	%r13, %rdi
	call	_ZN2v88internal8Profiler6EngageEv
	cmpb	$0, 40(%r12)
	je	.L2229
.L2293:
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger20AddCodeEventListenerEPNS0_17CodeEventListenerE
	jmp	.L2229
	.p2align 4,,10
	.p2align 3
.L2289:
	movq	-768(%rbp), %rax
	movq	%rbx, -848(%rbp)
	movq	$0, -840(%rbp)
	movb	$0, -832(%rbp)
	testq	%rax, %rax
	je	.L2213
	movq	-784(%rbp), %r8
	movq	-776(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L2214
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L2215:
	movq	-848(%rbp), %r13
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-880(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal14LowLevelLoggerC1EPNS0_7IsolateEPKc
	movq	72(%r12), %r13
	movq	%r15, 72(%r12)
	testq	%r13, %r13
	je	.L2216
	movq	0(%r13), %rax
	leaq	_ZN2v88internal14LowLevelLoggerD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2217
	leaq	16+_ZTVN2v88internal14LowLevelLoggerE(%rip), %rax
	movq	24(%r13), %rdi
	movq	%rax, 0(%r13)
	call	fclose@PLT
	movq	16(%r13), %rdi
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rax
	movq	$0, 24(%r13)
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L2218
	movl	$516, %esi
	call	_ZdlPvm@PLT
.L2218:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2216:
	movq	-848(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2219
	call	_ZdlPv@PLT
.L2219:
	movq	72(%r12), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger20AddCodeEventListenerEPNS0_17CodeEventListenerE
	jmp	.L2212
	.p2align 4,,10
	.p2align 3
.L2288:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	-880(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal13PerfJitLoggerC1EPNS0_7IsolateE@PLT
	movq	64(%r12), %rdi
	movq	%r15, 64(%r12)
	testq	%rdi, %rdi
	je	.L2211
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	64(%r12), %r15
.L2211:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger20AddCodeEventListenerEPNS0_17CodeEventListenerE
	jmp	.L2210
	.p2align 4,,10
	.p2align 3
.L2287:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-880(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal15PerfBasicLoggerC1EPNS0_7IsolateE
	movq	56(%r12), %r8
	movq	%r15, 56(%r12)
	testq	%r8, %r8
	je	.L2207
	movq	(%r8), %rax
	leaq	_ZN2v88internal15PerfBasicLoggerD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2208
	leaq	16+_ZTVN2v88internal15PerfBasicLoggerE(%rip), %rax
	movq	24(%r8), %rdi
	movq	%r8, -896(%rbp)
	movq	%rax, (%r8)
	call	fclose@PLT
	movq	-896(%rbp), %r8
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rax
	movq	16(%r8), %rdi
	movq	$0, 24(%r8)
	movq	%rax, (%r8)
	testq	%rdi, %rdi
	je	.L2209
	movl	$516, %esi
	call	_ZdlPvm@PLT
	movq	-896(%rbp), %r8
.L2209:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	movq	56(%r12), %r15
.L2207:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger20AddCodeEventListenerEPNS0_17CodeEventListenerE
	jmp	.L2206
	.p2align 4,,10
	.p2align 3
.L2286:
	leaq	-736(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L2204
	.p2align 4,,10
	.p2align 3
.L2221:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2220
	.p2align 4,,10
	.p2align 3
.L2214:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L2215
	.p2align 4,,10
	.p2align 3
.L2281:
	leaq	-864(%rbp), %r14
	jmp	.L2190
	.p2align 4,,10
	.p2align 3
.L2213:
	leaq	-736(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L2215
	.p2align 4,,10
	.p2align 3
.L2217:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2216
	.p2align 4,,10
	.p2align 3
.L2208:
	movq	%r8, %rdi
	call	*%rax
	movq	56(%r12), %r15
	jmp	.L2207
.L2283:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20776:
	.size	_ZN2v88internal6Logger5SetUpEPNS0_7IsolateE, .-_ZN2v88internal6Logger5SetUpEPNS0_7IsolateE
	.section	.text._ZN2v88internal6Logger19SetCodeEventHandlerEjPFvPKNS_12JitCodeEventEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Logger19SetCodeEventHandlerEjPFvPKNS_12JitCodeEventEE
	.type	_ZN2v88internal6Logger19SetCodeEventHandlerEjPFvPKNS_12JitCodeEventEE, @function
_ZN2v88internal6Logger19SetCodeEventHandlerEjPFvPKNS_12JitCodeEventEE:
.LFB20777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	80(%rdi), %r13
	movl	%esi, -52(%rbp)
	testq	%r13, %r13
	je	.L2296
	movq	8(%rdi), %rax
	movq	41488(%rax), %r14
	leaq	56(%r14), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%r14), %rsi
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	(%r14), %rax
	movq	%rax, -64(%rbp)
	leaq	0(,%rdx,8), %rcx
	movq	%rdx, %r11
	addq	%rcx, %rax
	movq	%rcx, -80(%rbp)
	movq	(%rax), %r9
	movq	%rax, -72(%rbp)
	testq	%r9, %r9
	je	.L2297
	movq	(%r9), %rdi
	movq	%r9, %r10
	movq	8(%rdi), %rcx
	jmp	.L2299
	.p2align 4,,10
	.p2align 3
.L2338:
	movq	(%rdi), %r8
	testq	%r8, %r8
	je	.L2297
	movq	8(%r8), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r11
	jne	.L2297
	movq	%r8, %rdi
.L2299:
	cmpq	%rcx, %r13
	jne	.L2338
	movq	(%rdi), %rcx
	cmpq	%r10, %r9
	je	.L2339
	testq	%rcx, %rcx
	je	.L2301
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r11
	je	.L2301
	movq	-64(%rbp), %rax
	movq	%r10, (%rax,%rdx,8)
	movq	(%rdi), %rcx
.L2301:
	movq	%rcx, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 24(%r14)
.L2297:
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	80(%rbx), %rdi
	movq	$0, 80(%rbx)
	testq	%rdi, %rdi
	je	.L2296
	movq	(%rdi), %rax
	call	*8(%rax)
.L2296:
	testq	%r12, %r12
	je	.L2294
	movq	8(%rbx), %rsi
	movq	45752(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L2306
	call	_ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE@PLT
.L2306:
	movl	$72, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal15CodeEventLoggerE(%rip), %rdx
	movl	$516, %edi
	movq	%rax, %r13
	movq	8(%rbx), %rax
	movq	%rdx, 0(%r13)
	movq	%rax, 8(%r13)
	call	_Znwm@PLT
	movq	%r12, 24(%r13)
	leaq	32(%r13), %rdi
	movl	$0, (%rax)
	movq	%rax, 16(%r13)
	leaq	16+_ZTVN2v88internal9JitLoggerE(%rip), %rax
	movq	%rax, 0(%r13)
	call	_ZN2v84base5MutexC1Ev@PLT
	movq	80(%rbx), %rdi
	movq	%r13, 80(%rbx)
	testq	%rdi, %rdi
	je	.L2307
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	80(%rbx), %r13
.L2307:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Logger20AddCodeEventListenerEPNS0_17CodeEventListenerE
	testb	$1, -52(%rbp)
	jne	.L2340
.L2294:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2340:
	.cfi_restore_state
	movq	8(%rbx), %r12
	leaq	144(%rbx), %r13
	movq	%r13, %rdi
	movq	41088(%r12), %r15
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	call	_ZN2v88internal18ExistingCodeLogger14LogCodeObjectsEv
	movq	%r13, %rdi
	call	_ZN2v88internal18ExistingCodeLogger20LogCompiledFunctionsEv
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r14
	je	.L2294
	movq	%r14, 41096(%r12)
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L2339:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L2312
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r11
	je	.L2301
	movq	-64(%rbp), %rax
	movq	%r10, (%rax,%rdx,8)
	movq	-80(%rbp), %rax
	addq	(%r14), %rax
	movq	%rax, -72(%rbp)
	movq	(%rax), %rax
.L2300:
	leaq	16(%r14), %rdx
	cmpq	%rdx, %rax
	je	.L2341
.L2302:
	movq	-72(%rbp), %rax
	movq	$0, (%rax)
	movq	(%rdi), %rcx
	jmp	.L2301
	.p2align 4,,10
	.p2align 3
.L2312:
	movq	%r10, %rax
	jmp	.L2300
	.p2align 4,,10
	.p2align 3
.L2341:
	movq	%rcx, 16(%r14)
	jmp	.L2302
	.cfi_endproc
.LFE20777:
	.size	_ZN2v88internal6Logger19SetCodeEventHandlerEjPFvPKNS_12JitCodeEventEE, .-_ZN2v88internal6Logger19SetCodeEventHandlerEjPFvPKNS_12JitCodeEventEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal15CodeEventLoggerC2EPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal15CodeEventLoggerC2EPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal15CodeEventLoggerC2EPNS0_7IsolateE:
.LFB26277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26277:
	.size	_GLOBAL__sub_I__ZN2v88internal15CodeEventLoggerC2EPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal15CodeEventLoggerC2EPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal15CodeEventLoggerC2EPNS0_7IsolateE
	.section	.rodata.CSWTCH.451,"a"
	.align 32
	.type	CSWTCH.451, @object
	.size	CSWTCH.451, 92
CSWTCH.451:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	4
	.long	8
	.long	10
	.long	0
	.hidden	_ZTCN2v88internal8OFStreamE0_So
	.weak	_ZTCN2v88internal8OFStreamE0_So
	.section	.rodata._ZTCN2v88internal8OFStreamE0_So,"aG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal8OFStreamE0_So, @object
	.size	_ZTCN2v88internal8OFStreamE0_So, 80
_ZTCN2v88internal8OFStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal8OFStreamE, @object
	.size	_ZTTN2v88internal8OFStreamE, 32
_ZTTN2v88internal8OFStreamE:
	.quad	_ZTVN2v88internal8OFStreamE+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+64
	.quad	_ZTVN2v88internal8OFStreamE+64
	.weak	_ZTVN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal8OFStreamE, @object
	.size	_ZTVN2v88internal8OFStreamE, 80
_ZTVN2v88internal8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8OFStreamD1Ev
	.quad	_ZN2v88internal8OFStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.weak	_ZTVN2v88internal15CodeEventLoggerE
	.section	.data.rel.ro._ZTVN2v88internal15CodeEventLoggerE,"awG",@progbits,_ZTVN2v88internal15CodeEventLoggerE,comdat
	.align 8
	.type	_ZTVN2v88internal15CodeEventLoggerE, @object
	.size	_ZTVN2v88internal15CodeEventLoggerE, 176
_ZTVN2v88internal15CodeEventLoggerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.quad	_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm
	.quad	_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm
	.quad	_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm
	.quad	_ZN2v88internal15CodeEventLogger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm
	.quad	_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm
	.quad	_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.quad	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN2v88internal15PerfBasicLoggerE
	.section	.data.rel.ro.local._ZTVN2v88internal15PerfBasicLoggerE,"awG",@progbits,_ZTVN2v88internal15PerfBasicLoggerE,comdat
	.align 8
	.type	_ZTVN2v88internal15PerfBasicLoggerE, @object
	.size	_ZTVN2v88internal15PerfBasicLoggerE, 176
_ZTVN2v88internal15PerfBasicLoggerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal15PerfBasicLoggerD1Ev
	.quad	_ZN2v88internal15PerfBasicLoggerD0Ev
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.quad	_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm
	.quad	_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm
	.quad	_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm
	.quad	_ZN2v88internal15CodeEventLogger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.quad	_ZN2v88internal15PerfBasicLogger13CodeMoveEventENS0_12AbstractCodeES2_
	.quad	_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm
	.quad	_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm
	.quad	_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv
	.quad	_ZN2v88internal15PerfBasicLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.quad	_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.quad	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv
	.quad	_ZN2v88internal15PerfBasicLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci
	.quad	_ZN2v88internal15PerfBasicLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci
	.weak	_ZTVN2v88internal25ExternalCodeEventListenerE
	.section	.data.rel.ro.local._ZTVN2v88internal25ExternalCodeEventListenerE,"awG",@progbits,_ZTVN2v88internal25ExternalCodeEventListenerE,comdat
	.align 8
	.type	_ZTVN2v88internal25ExternalCodeEventListenerE, @object
	.size	_ZTVN2v88internal25ExternalCodeEventListenerE, 160
_ZTVN2v88internal25ExternalCodeEventListenerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal25ExternalCodeEventListenerD1Ev
	.quad	_ZN2v88internal25ExternalCodeEventListenerD0Ev
	.quad	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.quad	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.quad	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.quad	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.quad	_ZN2v88internal25ExternalCodeEventListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.quad	_ZN2v88internal25ExternalCodeEventListener13CallbackEventENS0_4NameEm
	.quad	_ZN2v88internal25ExternalCodeEventListener19GetterCallbackEventENS0_4NameEm
	.quad	_ZN2v88internal25ExternalCodeEventListener19SetterCallbackEventENS0_4NameEm
	.quad	_ZN2v88internal25ExternalCodeEventListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.quad	_ZN2v88internal25ExternalCodeEventListener13CodeMoveEventENS0_12AbstractCodeES2_
	.quad	_ZN2v88internal25ExternalCodeEventListener27SharedFunctionInfoMoveEventEmm
	.quad	_ZN2v88internal25ExternalCodeEventListener22NativeContextMoveEventEmm
	.quad	_ZN2v88internal25ExternalCodeEventListener17CodeMovingGCEventEv
	.quad	_ZN2v88internal25ExternalCodeEventListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.quad	_ZN2v88internal25ExternalCodeEventListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.quad	_ZN2v88internal25ExternalCodeEventListener27is_listening_to_code_eventsEv
	.weak	_ZTVN2v88internal14LowLevelLoggerE
	.section	.data.rel.ro.local._ZTVN2v88internal14LowLevelLoggerE,"awG",@progbits,_ZTVN2v88internal14LowLevelLoggerE,comdat
	.align 8
	.type	_ZTVN2v88internal14LowLevelLoggerE, @object
	.size	_ZTVN2v88internal14LowLevelLoggerE, 176
_ZTVN2v88internal14LowLevelLoggerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14LowLevelLoggerD1Ev
	.quad	_ZN2v88internal14LowLevelLoggerD0Ev
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.quad	_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm
	.quad	_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm
	.quad	_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm
	.quad	_ZN2v88internal15CodeEventLogger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.quad	_ZN2v88internal14LowLevelLogger13CodeMoveEventENS0_12AbstractCodeES2_
	.quad	_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm
	.quad	_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm
	.quad	_ZN2v88internal14LowLevelLogger17CodeMovingGCEventEv
	.quad	_ZN2v88internal14LowLevelLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.quad	_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.quad	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv
	.quad	_ZN2v88internal14LowLevelLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci
	.quad	_ZN2v88internal14LowLevelLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci
	.weak	_ZTVN2v88internal9JitLoggerE
	.section	.data.rel.ro.local._ZTVN2v88internal9JitLoggerE,"awG",@progbits,_ZTVN2v88internal9JitLoggerE,comdat
	.align 8
	.type	_ZTVN2v88internal9JitLoggerE, @object
	.size	_ZTVN2v88internal9JitLoggerE, 176
_ZTVN2v88internal9JitLoggerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal9JitLoggerD1Ev
	.quad	_ZN2v88internal9JitLoggerD0Ev
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.quad	_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm
	.quad	_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm
	.quad	_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm
	.quad	_ZN2v88internal15CodeEventLogger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.quad	_ZN2v88internal9JitLogger13CodeMoveEventENS0_12AbstractCodeES2_
	.quad	_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm
	.quad	_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm
	.quad	_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv
	.quad	_ZN2v88internal9JitLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.quad	_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.quad	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv
	.quad	_ZN2v88internal9JitLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci
	.quad	_ZN2v88internal9JitLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci
	.weak	_ZTVN2v88internal14SamplingThreadE
	.section	.data.rel.ro.local._ZTVN2v88internal14SamplingThreadE,"awG",@progbits,_ZTVN2v88internal14SamplingThreadE,comdat
	.align 8
	.type	_ZTVN2v88internal14SamplingThreadE, @object
	.size	_ZTVN2v88internal14SamplingThreadE, 40
_ZTVN2v88internal14SamplingThreadE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14SamplingThreadD1Ev
	.quad	_ZN2v88internal14SamplingThreadD0Ev
	.quad	_ZN2v88internal14SamplingThread3RunEv
	.weak	_ZTVN2v88internal6TickerE
	.section	.data.rel.ro.local._ZTVN2v88internal6TickerE,"awG",@progbits,_ZTVN2v88internal6TickerE,comdat
	.align 8
	.type	_ZTVN2v88internal6TickerE, @object
	.size	_ZTVN2v88internal6TickerE, 40
_ZTVN2v88internal6TickerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6TickerD1Ev
	.quad	_ZN2v88internal6TickerD0Ev
	.quad	_ZN2v88internal6Ticker11SampleStackERKNS_13RegisterStateE
	.weak	_ZTVN2v88internal8ProfilerE
	.section	.data.rel.ro.local._ZTVN2v88internal8ProfilerE,"awG",@progbits,_ZTVN2v88internal8ProfilerE,comdat
	.align 8
	.type	_ZTVN2v88internal8ProfilerE, @object
	.size	_ZTVN2v88internal8ProfilerE, 40
_ZTVN2v88internal8ProfilerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8ProfilerD1Ev
	.quad	_ZN2v88internal8ProfilerD0Ev
	.quad	_ZN2v88internal8Profiler3RunEv
	.weak	_ZTVN2v88internal6LoggerE
	.section	.data.rel.ro.local._ZTVN2v88internal6LoggerE,"awG",@progbits,_ZTVN2v88internal6LoggerE,comdat
	.align 8
	.type	_ZTVN2v88internal6LoggerE, @object
	.size	_ZTVN2v88internal6LoggerE, 160
_ZTVN2v88internal6LoggerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6LoggerD1Ev
	.quad	_ZN2v88internal6LoggerD0Ev
	.quad	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.quad	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.quad	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.quad	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.quad	_ZN2v88internal6Logger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.quad	_ZN2v88internal6Logger13CallbackEventENS0_4NameEm
	.quad	_ZN2v88internal6Logger19GetterCallbackEventENS0_4NameEm
	.quad	_ZN2v88internal6Logger19SetterCallbackEventENS0_4NameEm
	.quad	_ZN2v88internal6Logger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.quad	_ZN2v88internal6Logger13CodeMoveEventENS0_12AbstractCodeES2_
	.quad	_ZN2v88internal6Logger27SharedFunctionInfoMoveEventEmm
	.quad	_ZN2v88internal6Logger22NativeContextMoveEventEmm
	.quad	_ZN2v88internal6Logger17CodeMovingGCEventEv
	.quad	_ZN2v88internal6Logger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.quad	_ZN2v88internal6Logger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.quad	_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.globl	_ZN2v88internal6Logger5kNextE
	.section	.rodata._ZN2v88internal6Logger5kNextE,"a"
	.align 4
	.type	_ZN2v88internal6Logger5kNextE, @object
	.size	_ZN2v88internal6Logger5kNextE, 4
_ZN2v88internal6Logger5kNextE:
	.zero	4
	.globl	_ZN2v88internal14LowLevelLogger7kLogExtE
	.section	.rodata._ZN2v88internal14LowLevelLogger7kLogExtE,"a"
	.type	_ZN2v88internal14LowLevelLogger7kLogExtE, @object
	.size	_ZN2v88internal14LowLevelLogger7kLogExtE, 4
_ZN2v88internal14LowLevelLogger7kLogExtE:
	.string	".ll"
	.globl	_ZN2v88internal15PerfBasicLogger22kFilenameBufferPaddingE
	.section	.rodata._ZN2v88internal15PerfBasicLogger22kFilenameBufferPaddingE,"a"
	.align 4
	.type	_ZN2v88internal15PerfBasicLogger22kFilenameBufferPaddingE, @object
	.size	_ZN2v88internal15PerfBasicLogger22kFilenameBufferPaddingE, 4
_ZN2v88internal15PerfBasicLogger22kFilenameBufferPaddingE:
	.long	16
	.globl	_ZN2v88internal15PerfBasicLogger21kFilenameFormatStringE
	.section	.rodata._ZN2v88internal15PerfBasicLogger21kFilenameFormatStringE,"a"
	.align 16
	.type	_ZN2v88internal15PerfBasicLogger21kFilenameFormatStringE, @object
	.size	_ZN2v88internal15PerfBasicLogger21kFilenameFormatStringE, 17
_ZN2v88internal15PerfBasicLogger21kFilenameFormatStringE:
	.string	"/tmp/perf-%d.map"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC99:
	.string	"code-delete"
.LC100:
	.string	"code-moving-gc"
.LC101:
	.string	"Builtin"
.LC102:
	.string	"Eval"
.LC103:
	.string	"Function"
.LC104:
	.string	"InterpretedFunction"
.LC105:
	.string	"Handler"
.LC106:
	.string	"BytecodeHandler"
.LC107:
	.string	"LazyCompile"
.LC108:
	.string	"RegExp"
.LC109:
	.string	"Script"
.LC110:
	.string	"Stub"
	.section	.data.rel.ro.local._ZN2v88internalL15kLogEventsNamesE,"aw"
	.align 32
	.type	_ZN2v88internalL15kLogEventsNamesE, @object
	.size	_ZN2v88internalL15kLogEventsNamesE, 176
_ZN2v88internalL15kLogEventsNamesE:
	.quad	.LC1
	.quad	.LC5
	.quad	.LC4
	.quad	.LC99
	.quad	.LC100
	.quad	.LC3
	.quad	.LC57
	.quad	.LC68
	.quad	.LC101
	.quad	.LC27
	.quad	.LC102
	.quad	.LC103
	.quad	.LC104
	.quad	.LC105
	.quad	.LC106
	.quad	.LC107
	.quad	.LC108
	.quad	.LC109
	.quad	.LC110
	.quad	.LC103
	.quad	.LC107
	.quad	.LC109
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC22:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC23:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC24:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
