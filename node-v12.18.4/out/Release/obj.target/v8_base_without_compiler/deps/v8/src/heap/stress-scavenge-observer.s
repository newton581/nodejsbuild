	.file	"stress-scavenge-observer.cc"
	.text
	.section	.text._ZN2v88internal18AllocationObserver15GetNextStepSizeEv,"axG",@progbits,_ZN2v88internal18AllocationObserver15GetNextStepSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18AllocationObserver15GetNextStepSizeEv
	.type	_ZN2v88internal18AllocationObserver15GetNextStepSizeEv, @function
_ZN2v88internal18AllocationObserver15GetNextStepSizeEv:
.LFB7286:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE7286:
	.size	_ZN2v88internal18AllocationObserver15GetNextStepSizeEv, .-_ZN2v88internal18AllocationObserver15GetNextStepSizeEv
	.section	.text._ZN2v88internal22StressScavengeObserverD2Ev,"axG",@progbits,_ZN2v88internal22StressScavengeObserverD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal22StressScavengeObserverD2Ev
	.type	_ZN2v88internal22StressScavengeObserverD2Ev, @function
_ZN2v88internal22StressScavengeObserverD2Ev:
.LFB24295:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE24295:
	.size	_ZN2v88internal22StressScavengeObserverD2Ev, .-_ZN2v88internal22StressScavengeObserverD2Ev
	.weak	_ZN2v88internal22StressScavengeObserverD1Ev
	.set	_ZN2v88internal22StressScavengeObserverD1Ev,_ZN2v88internal22StressScavengeObserverD2Ev
	.section	.text._ZN2v88internal8NewSpace4SizeEv,"axG",@progbits,_ZN2v88internal8NewSpace4SizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8NewSpace4SizeEv
	.type	_ZN2v88internal8NewSpace4SizeEv, @function
_ZN2v88internal8NewSpace4SizeEv:
.LFB9451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movslq	360(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	movq	352(%rbx), %rcx
	movq	104(%rbx), %rdx
	imulq	%rax, %r12
	popq	%rbx
	subq	40(%rcx), %rdx
	leaq	(%rdx,%r12), %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9451:
	.size	_ZN2v88internal8NewSpace4SizeEv, .-_ZN2v88internal8NewSpace4SizeEv
	.section	.text._ZN2v88internal22StressScavengeObserverD0Ev,"axG",@progbits,_ZN2v88internal22StressScavengeObserverD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal22StressScavengeObserverD0Ev
	.type	_ZN2v88internal22StressScavengeObserverD0Ev, @function
_ZN2v88internal22StressScavengeObserverD0Ev:
.LFB24297:
	.cfi_startproc
	endbr64
	movl	$48, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24297:
	.size	_ZN2v88internal22StressScavengeObserverD0Ev, .-_ZN2v88internal22StressScavengeObserverD0Ev
	.section	.rodata._ZN2v88internal22StressScavengeObserver4StepEimm.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"[Scavenge] %.2lf%% of the new space capacity reached\n"
	.section	.rodata._ZN2v88internal22StressScavengeObserver4StepEimm.str1.1,"aMS",@progbits,1
.LC2:
	.string	"[Scavenge] GC requested\n"
	.section	.text._ZN2v88internal22StressScavengeObserver4StepEimm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22StressScavengeObserver4StepEimm
	.type	_ZN2v88internal22StressScavengeObserver4StepEimm, @function
_ZN2v88internal22StressScavengeObserver4StepEimm:
.LFB19475:
	.cfi_startproc
	endbr64
	cmpb	$0, 36(%rdi)
	je	.L27
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %rax
	movq	248(%rax), %rax
	movq	312(%rax), %rax
	shrq	$18, %rax
	movq	%rax, %r12
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	imulq	%r12, %rax
	testq	%rax, %rax
	je	.L7
	movq	24(%rbx), %rax
	leaq	_ZN2v88internal8NewSpace4SizeEv(%rip), %rdx
	movq	248(%rax), %r13
	movq	0(%r13), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L9
	movslq	360(%r13), %r12
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	movq	352(%r13), %rcx
	movq	104(%r13), %rdx
	imulq	%rax, %r12
	subq	40(%rcx), %rdx
	addq	%rdx, %r12
.L10:
	movq	24(%rbx), %rax
	movq	248(%rax), %rax
	movq	312(%rax), %rax
	shrq	$18, %rax
	movq	%rax, %r13
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	testq	%r12, %r12
	js	.L11
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
.L12:
	imulq	%r13, %rax
	mulsd	.LC0(%rip), %xmm0
	testq	%rax, %rax
	js	.L13
	pxor	%xmm1, %xmm1
	cmpb	$0, _ZN2v88internal26FLAG_trace_stress_scavengeE(%rip)
	cvtsi2sdq	%rax, %xmm1
	divsd	%xmm1, %xmm0
	jne	.L28
.L15:
	cmpb	$0, _ZN2v88internal23FLAG_fuzzer_gc_analysisE(%rip)
	jne	.L29
	cvttsd2sil	%xmm0, %eax
	cmpl	32(%rbx), %eax
	jl	.L7
	movq	24(%rbx), %rax
	cmpb	$0, _ZN2v88internal26FLAG_trace_stress_scavengeE(%rip)
	leaq	-37592(%rax), %rdi
	jne	.L30
.L19:
	movb	$1, 36(%rbx)
	addq	$24, %rsp
	addq	$37512, %rdi
	movl	$2, %esi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	maxsd	40(%rbx), %xmm0
	movsd	%xmm0, 40(%rbx)
.L7:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	%r12, %rdx
	andl	$1, %r12d
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%r12, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm1, %xmm1
	shrq	%rdx
	orq	%rax, %rdx
	cmpb	$0, _ZN2v88internal26FLAG_trace_stress_scavengeE(%rip)
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm1
	divsd	%xmm1, %xmm0
	je	.L15
.L28:
	movq	24(%rbx), %rax
	leaq	.LC1(%rip), %rsi
	movsd	%xmm0, -40(%rbp)
	leaq	-37592(%rax), %rdi
	movl	$1, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movsd	-40(%rbp), %xmm0
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%r13, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L30:
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movq	24(%rbx), %rax
	leaq	-37592(%rax), %rdi
	jmp	.L19
	.cfi_endproc
.LFE19475:
	.size	_ZN2v88internal22StressScavengeObserver4StepEimm, .-_ZN2v88internal22StressScavengeObserver4StepEimm
	.section	.rodata._ZN2v88internal22StressScavengeObserverC2EPNS0_4HeapE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"[StressScavenge] %d%% is the new limit\n"
	.section	.text._ZN2v88internal22StressScavengeObserverC2EPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22StressScavengeObserverC2EPNS0_4HeapE
	.type	_ZN2v88internal22StressScavengeObserverC2EPNS0_4HeapE, @function
_ZN2v88internal22StressScavengeObserverC2EPNS0_4HeapE:
.LFB19473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movdqa	.LC3(%rip), %xmm0
	leaq	16+_ZTVN2v88internal22StressScavengeObserverE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	_ZN2v88internal20FLAG_stress_scavengeE(%rip), %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	%rsi, 24(%rdi)
	movb	$0, 36(%rdi)
	movq	$0x000000000, 40(%rdi)
	movups	%xmm0, 8(%rdi)
	testl	%r12d, %r12d
	jle	.L32
	leaq	-37592(%rsi), %rdi
	call	_ZN2v88internal7Isolate10fuzzer_rngEv@PLT
	leal	1(%r12), %esi
	movq	%rax, %rdi
	call	_ZN2v84base21RandomNumberGenerator7NextIntEi@PLT
	movl	%eax, %r12d
.L32:
	cmpb	$0, _ZN2v88internal26FLAG_trace_stress_scavengeE(%rip)
	movl	%r12d, 32(%rbx)
	je	.L31
	cmpb	$0, _ZN2v88internal23FLAG_fuzzer_gc_analysisE(%rip)
	je	.L35
.L31:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	24(%rbx), %rdi
	movl	%r12d, %edx
	popq	%rbx
	leaq	.LC5(%rip), %rsi
	popq	%r12
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	subq	$37592, %rdi
	jmp	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	.cfi_endproc
.LFE19473:
	.size	_ZN2v88internal22StressScavengeObserverC2EPNS0_4HeapE, .-_ZN2v88internal22StressScavengeObserverC2EPNS0_4HeapE
	.globl	_ZN2v88internal22StressScavengeObserverC1EPNS0_4HeapE
	.set	_ZN2v88internal22StressScavengeObserverC1EPNS0_4HeapE,_ZN2v88internal22StressScavengeObserverC2EPNS0_4HeapE
	.section	.text._ZNK2v88internal22StressScavengeObserver14HasRequestedGCEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal22StressScavengeObserver14HasRequestedGCEv
	.type	_ZNK2v88internal22StressScavengeObserver14HasRequestedGCEv, @function
_ZNK2v88internal22StressScavengeObserver14HasRequestedGCEv:
.LFB19476:
	.cfi_startproc
	endbr64
	movzbl	36(%rdi), %eax
	ret
	.cfi_endproc
.LFE19476:
	.size	_ZNK2v88internal22StressScavengeObserver14HasRequestedGCEv, .-_ZNK2v88internal22StressScavengeObserver14HasRequestedGCEv
	.section	.rodata._ZN2v88internal22StressScavengeObserver15RequestedGCDoneEv.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"[Scavenge] %d%% is the new limit\n"
	.section	.text._ZN2v88internal22StressScavengeObserver15RequestedGCDoneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22StressScavengeObserver15RequestedGCDoneEv
	.type	_ZN2v88internal22StressScavengeObserver15RequestedGCDoneEv, @function
_ZN2v88internal22StressScavengeObserver15RequestedGCDoneEv:
.LFB19477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal8NewSpace4SizeEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	24(%rdi), %rax
	movq	248(%rax), %r12
	movq	(%r12), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L38
	movslq	360(%r12), %rbx
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	movq	352(%r12), %rcx
	movq	104(%r12), %rdx
	imulq	%rax, %rbx
	subq	40(%rcx), %rdx
	addq	%rdx, %rbx
.L39:
	movq	24(%r13), %rax
	movq	248(%rax), %rax
	movq	312(%rax), %rax
	shrq	$18, %rax
	movq	%rax, %r12
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	testq	%rbx, %rbx
	js	.L40
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rbx, %xmm0
.L41:
	imulq	%r12, %rax
	mulsd	.LC0(%rip), %xmm0
	testq	%rax, %rax
	js	.L42
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
.L43:
	divsd	%xmm1, %xmm0
	movl	_ZN2v88internal20FLAG_stress_scavengeE(%rip), %ebx
	cvttsd2sil	%xmm0, %r12d
	cmpl	%ebx, %r12d
	jge	.L44
	movq	24(%r13), %rax
	movsd	%xmm0, -40(%rbp)
	subl	%r12d, %ebx
	leaq	-37592(%rax), %rdi
	call	_ZN2v88internal7Isolate10fuzzer_rngEv@PLT
	leal	1(%rbx), %esi
	movq	%rax, %rdi
	call	_ZN2v84base21RandomNumberGenerator7NextIntEi@PLT
	movsd	-40(%rbp), %xmm0
	leal	(%r12,%rax), %ebx
.L44:
	cmpb	$0, _ZN2v88internal26FLAG_trace_stress_scavengeE(%rip)
	movl	%ebx, 32(%r13)
	jne	.L47
	movb	$0, 36(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm1, %xmm1
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%rbx, %rdx
	andl	$1, %ebx
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rbx, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L47:
	movq	24(%r13), %rax
	leaq	.LC1(%rip), %rsi
	leaq	-37592(%rax), %rdi
	movl	$1, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movq	24(%r13), %rax
	movl	32(%r13), %edx
	leaq	.LC6(%rip), %rsi
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movb	$0, 36(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L39
	.cfi_endproc
.LFE19477:
	.size	_ZN2v88internal22StressScavengeObserver15RequestedGCDoneEv, .-_ZN2v88internal22StressScavengeObserver15RequestedGCDoneEv
	.section	.text._ZNK2v88internal22StressScavengeObserver22MaxNewSpaceSizeReachedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal22StressScavengeObserver22MaxNewSpaceSizeReachedEv
	.type	_ZNK2v88internal22StressScavengeObserver22MaxNewSpaceSizeReachedEv, @function
_ZNK2v88internal22StressScavengeObserver22MaxNewSpaceSizeReachedEv:
.LFB19478:
	.cfi_startproc
	endbr64
	movsd	40(%rdi), %xmm0
	ret
	.cfi_endproc
.LFE19478:
	.size	_ZNK2v88internal22StressScavengeObserver22MaxNewSpaceSizeReachedEv, .-_ZNK2v88internal22StressScavengeObserver22MaxNewSpaceSizeReachedEv
	.section	.text._ZN2v88internal22StressScavengeObserver9NextLimitEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22StressScavengeObserver9NextLimitEi
	.type	_ZN2v88internal22StressScavengeObserver9NextLimitEi, @function
_ZN2v88internal22StressScavengeObserver9NextLimitEi:
.LFB19479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	_ZN2v88internal20FLAG_stress_scavengeE(%rip), %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	cmpl	%esi, %r12d
	jle	.L49
	movq	24(%rdi), %rdi
	movl	%esi, %ebx
	subl	%ebx, %r12d
	subq	$37592, %rdi
	call	_ZN2v88internal7Isolate10fuzzer_rngEv@PLT
	leal	1(%r12), %esi
	movq	%rax, %rdi
	call	_ZN2v84base21RandomNumberGenerator7NextIntEi@PLT
	leal	(%rax,%rbx), %r12d
.L49:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19479:
	.size	_ZN2v88internal22StressScavengeObserver9NextLimitEi, .-_ZN2v88internal22StressScavengeObserver9NextLimitEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal22StressScavengeObserverC2EPNS0_4HeapE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal22StressScavengeObserverC2EPNS0_4HeapE, @function
_GLOBAL__sub_I__ZN2v88internal22StressScavengeObserverC2EPNS0_4HeapE:
.LFB24347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24347:
	.size	_GLOBAL__sub_I__ZN2v88internal22StressScavengeObserverC2EPNS0_4HeapE, .-_GLOBAL__sub_I__ZN2v88internal22StressScavengeObserverC2EPNS0_4HeapE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal22StressScavengeObserverC2EPNS0_4HeapE
	.weak	_ZTVN2v88internal22StressScavengeObserverE
	.section	.data.rel.ro.local._ZTVN2v88internal22StressScavengeObserverE,"awG",@progbits,_ZTVN2v88internal22StressScavengeObserverE,comdat
	.align 8
	.type	_ZTVN2v88internal22StressScavengeObserverE, @object
	.size	_ZTVN2v88internal22StressScavengeObserverE, 48
_ZTVN2v88internal22StressScavengeObserverE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal22StressScavengeObserverD1Ev
	.quad	_ZN2v88internal22StressScavengeObserverD0Ev
	.quad	_ZN2v88internal22StressScavengeObserver4StepEimm
	.quad	_ZN2v88internal18AllocationObserver15GetNextStepSizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1079574528
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.quad	64
	.quad	64
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
