	.file	"wasm-engine.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB2897:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE2897:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB2898:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2898:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB2900:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2900:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB3211:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE3211:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CancelableTask3RunEv
	.type	_ZN2v88internal14CancelableTask3RunEv, @function
_ZN2v88internal14CancelableTask3RunEv:
.LFB3853:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, 16(%rdi)
	jne	.L6
	movq	(%rdi), %rax
	jmp	*24(%rax)
.L6:
	ret
	.cfi_endproc
.LFE3853:
	.size	_ZN2v88internal14CancelableTask3RunEv, .-_ZN2v88internal14CancelableTask3RunEv
	.section	.text._ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitorD2Ev, @function
_ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitorD2Ev:
.LFB26269:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE26269:
	.size	_ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitorD2Ev, .-_ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitorD2Ev
	.set	_ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitorD1Ev,_ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitorD2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB26414:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE26414:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitorD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitorD0Ev, @function
_ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitorD0Ev:
.LFB26271:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26271:
	.size	_ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitorD0Ev, .-_ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitorD0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB26416:
	.cfi_startproc
	endbr64
	movl	$848, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26416:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB26418:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE26418:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB26419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L13
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L13:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26419:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.rodata._ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitor11VisitThreadES4_PNS0_14ThreadLocalTopE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"archived threads in combination with wasm not supported"
	.section	.text._ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitor11VisitThreadES4_PNS0_14ThreadLocalTopE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitor11VisitThreadES4_PNS0_14ThreadLocalTopE, @function
_ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitor11VisitThreadES4_PNS0_14ThreadLocalTopE:
.LFB18881:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18881:
	.size	_ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitor11VisitThreadES4_PNS0_14ThreadLocalTopE, .-_ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitor11VisitThreadES4_PNS0_14ThreadLocalTopE
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB26282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE26282:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD2Ev, @function
_ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD2Ev:
.LFB23523:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE23523:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD2Ev, .-_ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD2Ev
	.set	_ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD1Ev,_ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD2Ev
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD0Ev, @function
_ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD0Ev:
.LFB23525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23525:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD0Ev, .-_ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD0Ev
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskD0Ev, @function
_ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskD0Ev:
.LFB18875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	cmpq	$0, 24(%rdi)
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	je	.L25
	cmpq	$0, 16(%rdi)
	je	.L25
	movq	8(%rdi), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%r12), %rax
	movq	%r13, %rdi
	movq	$0, (%rax)
	movq	$0, 16(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L25:
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE18875:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskD0Ev, .-_ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskD0Ev
	.section	.text._ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_4wasm12_GLOBAL__N_120WasmGCForegroundTaskEESaISA_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS5_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_4wasm12_GLOBAL__N_120WasmGCForegroundTaskEESaISA_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS5_.isra.0, @function
_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_4wasm12_GLOBAL__N_120WasmGCForegroundTaskEESaISA_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS5_.isra.0:
.LFB26859:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	(%rdi), %r13
	divq	%r9
	leaq	0(,%rdx,8), %r15
	leaq	0(%r13,%r15), %r14
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L38
	movq	(%r12), %r8
	movq	%rdi, %rbx
	movq	%rdx, %r11
	movq	%r12, %r10
	movq	8(%r8), %rcx
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L45:
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L38
	movq	8(%rdi), %rcx
	xorl	%edx, %edx
	movq	%r8, %r10
	movq	%rcx, %rax
	divq	%r9
	cmpq	%rdx, %r11
	jne	.L38
	movq	%rdi, %r8
.L30:
	cmpq	%rcx, %rsi
	jne	.L45
	movq	(%r8), %rcx
	cmpq	%r10, %r12
	je	.L46
	testq	%rcx, %rcx
	je	.L32
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r9
	cmpq	%rdx, %r11
	je	.L32
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r8), %rcx
.L32:
	movq	%rcx, (%r10)
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L39
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r9
	cmpq	%rdx, %r11
	je	.L32
	movq	%r10, 0(%r13,%rdx,8)
	addq	(%rbx), %r15
	movq	(%r15), %rax
	movq	%r15, %r14
.L31:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L47
.L33:
	movq	$0, (%r14)
	movq	(%r8), %rcx
	jmp	.L32
.L39:
	movq	%r10, %rax
	jmp	.L31
.L47:
	movq	%rcx, 16(%rbx)
	jmp	.L33
	.cfi_endproc
.LFE26859:
	.size	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_4wasm12_GLOBAL__N_120WasmGCForegroundTaskEESaISA_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS5_.isra.0, .-_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_4wasm12_GLOBAL__N_120WasmGCForegroundTaskEESaISA_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS5_.isra.0
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD2Ev, @function
_ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD2Ev:
.LFB23445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	addq	$48, %rax
	movq	%rax, 32(%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L50
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L51
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L55
.L50:
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L50
.L55:
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L50
	.cfi_endproc
.LFE23445:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD2Ev, .-_ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD2Ev
	.set	_ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD1Ev,_ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD2Ev
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskD2Ev, @function
_ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskD2Ev:
.LFB18873:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskE(%rip), %rax
	cmpq	$0, 24(%rdi)
	movq	%rax, (%rdi)
	je	.L59
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	$0, 16(%rdi)
	movq	%rdi, %rbx
	je	.L56
	movq	8(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	$0, (%rax)
	movq	$0, 16(%rbx)
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE18873:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskD2Ev, .-_ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskD2Ev
	.set	_ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskD1Ev,_ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskD2Ev
	.section	.text._ZThn32_N2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD1Ev,"ax",@progbits
	.p2align 4
	.type	_ZThn32_N2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD1Ev, @function
_ZThn32_N2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD1Ev:
.LFB27022:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	subq	$32, %rdi
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE27022:
	.size	_ZThn32_N2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD1Ev, .-_ZThn32_N2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD1Ev
	.section	.text._ZThn32_N2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD0Ev,"ax",@progbits
	.p2align 4
	.type	_ZThn32_N2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD0Ev, @function
_ZThn32_N2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD0Ev:
.LFB27025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27025:
	.size	_ZThn32_N2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD0Ev, .-_ZThn32_N2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD0Ev
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal14CancelableTask3RunEv
	.type	_ZThn32_N2v88internal14CancelableTask3RunEv, @function
_ZThn32_N2v88internal14CancelableTask3RunEv:
.LFB27026:
	.cfi_startproc
	endbr64
	leaq	-32(%rdi), %r8
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, -16(%rdi)
	jne	.L65
	movq	-32(%rdi), %rax
	movq	%r8, %rdi
	movq	24(%rax), %rax
	jmp	*%rax
.L65:
	ret
	.cfi_endproc
.LFE27026:
	.size	_ZThn32_N2v88internal14CancelableTask3RunEv, .-_ZThn32_N2v88internal14CancelableTask3RunEv
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB27023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27023:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB26283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26283:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB27027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE27027:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.text._ZThn32_N2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD0Ev,"ax",@progbits
	.p2align 4
	.type	_ZThn32_N2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD0Ev, @function
_ZThn32_N2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD0Ev:
.LFB27024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, -32(%rdi)
	addq	$48, %rax
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L75
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L76
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L80
.L75:
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -32(%rbx)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L75
.L80:
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L75
	.cfi_endproc
.LFE27024:
	.size	_ZThn32_N2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD0Ev, .-_ZThn32_N2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD0Ev
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD0Ev, @function
_ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD0Ev:
.LFB23447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	addq	$48, %rax
	movq	%rax, 32(%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L83
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L84
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L88
.L83:
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L83
.L88:
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L83
	.cfi_endproc
.LFE23447:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD0Ev, .-_ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD0Ev
	.section	.text._ZThn32_N2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD1Ev,"ax",@progbits
	.p2align 4
	.type	_ZThn32_N2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD1Ev, @function
_ZThn32_N2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD1Ev:
.LFB27028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	addq	$48, %rax
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L91
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L92
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L96
.L91:
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	leaq	-32(%rbx), %rdi
	movq	%rax, -32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L91
.L96:
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L91
	.cfi_endproc
.LFE27028:
	.size	_ZThn32_N2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD1Ev, .-_ZThn32_N2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD1Ev
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTask11RunInternalEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTask11RunInternalEv, @function
_ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTask11RunInternalEv:
.LFB19264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	56(%rdi), %r12
	testq	%r12, %r12
	je	.L97
	movl	8(%r12), %eax
	leaq	8(%r12), %rbx
.L100:
	testl	%eax, %eax
	je	.L97
	leal	1(%rax), %edx
	lock cmpxchgl	%edx, (%rbx)
	jne	.L100
	movl	8(%r12), %eax
	testl	%eax, %eax
	je	.L101
	movq	48(%rdi), %r8
	testq	%r8, %r8
	je	.L101
	movq	40(%rdi), %rax
	movl	$1, %edx
	movq	%r8, %rdi
	movq	40960(%rax), %rsi
	call	_ZNK2v88internal4wasm12NativeModule14SampleCodeSizeEPNS0_8CountersENS2_16CodeSamplingTimeE@PLT
.L101:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L123
	movl	$-1, %eax
	lock xaddl	%eax, (%rbx)
	cmpl	$1, %eax
	je	.L124
.L97:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L97
.L124:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L105
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L106:
	cmpl	$1, %eax
	jne	.L97
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L106
	.cfi_endproc
.LFE19264:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTask11RunInternalEv, .-_ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTask11RunInternalEv
	.section	.rodata._ZN2v88internal10CodeTracerC2Ei.str1.1,"aMS",@progbits,1
.LC1:
	.string	"code-%d-%d.asm"
.LC2:
	.string	"code-%d.asm"
.LC3:
	.string	""
	.section	.text._ZN2v88internal10CodeTracerC2Ei,"axG",@progbits,_ZN2v88internal10CodeTracerC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10CodeTracerC2Ei
	.type	_ZN2v88internal10CodeTracerC2Ei, @function
_ZN2v88internal10CodeTracerC2Ei:
.LFB7564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$16, %rdi
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	%rdi, (%rbx)
	movq	$128, 8(%rbx)
	movq	$0, 144(%rbx)
	movl	$0, 152(%rbx)
	je	.L133
	movq	_ZN2v88internal28FLAG_redirect_code_traces_toE(%rip), %rdx
	testq	%rdx, %rdx
	je	.L128
	movl	$128, %ecx
	movl	$128, %esi
	call	_ZN2v88internal7StrNCpyENS0_6VectorIcEEPKcm@PLT
.L129:
	movq	(%rbx), %rdi
	xorl	%ecx, %ecx
	popq	%rbx
	xorl	%edx, %edx
	popq	%r12
	leaq	.LC3(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10WriteCharsEPKcS2_ib@PLT
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movl	%esi, %r12d
	testl	%esi, %esi
	js	.L130
	call	_ZN2v84base2OS19GetCurrentProcessIdEv@PLT
	movq	8(%rbx), %r9
	movq	(%rbx), %rdi
	movl	%r12d, %r8d
	movl	%eax, %ecx
	leaq	.LC1(%rip), %rdx
	xorl	%eax, %eax
	movq	%r9, %rsi
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L133:
	movq	stdout(%rip), %rax
	movq	%rax, 144(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	call	_ZN2v84base2OS19GetCurrentProcessIdEv@PLT
	movq	(%rbx), %rdi
	movq	8(%rbx), %rsi
	leaq	.LC2(%rip), %rdx
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	jmp	.L129
	.cfi_endproc
.LFE7564:
	.size	_ZN2v88internal10CodeTracerC2Ei, .-_ZN2v88internal10CodeTracerC2Ei
	.weak	_ZN2v88internal10CodeTracerC1Ei
	.set	_ZN2v88internal10CodeTracerC1Ei,_ZN2v88internal10CodeTracerC2Ei
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB9608:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L140
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L143
.L134:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L134
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE9608:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.text._ZN2v88internal4wasm10WasmEngineC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngineC2Ev
	.type	_ZN2v88internal4wasm10WasmEngineC2Ev, @function
_ZN2v88internal4wasm10WasmEngineC2Ev:
.LFB19120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movq	$0, -8(%rdi)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	104(%rbx), %rax
	movq	%rbx, %rsi
	movss	.LC4(%rip), %xmm0
	movq	%rax, 56(%rbx)
	leaq	160(%rbx), %rax
	movl	_ZN2v88internal24FLAG_wasm_max_code_spaceE(%rip), %edx
	leaq	280(%rbx), %rdi
	movq	%rax, 112(%rbx)
	leaq	216(%rbx), %rax
	movq	%rax, 168(%rbx)
	leaq	272(%rbx), %rax
	sall	$20, %edx
	movq	%rax, 224(%rbx)
	movq	$0, 48(%rbx)
	movq	$1, 64(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 104(%rbx)
	movq	$1, 120(%rbx)
	movq	$0, 128(%rbx)
	movq	$0, 136(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 160(%rbx)
	movq	$1, 176(%rbx)
	movq	$0, 184(%rbx)
	movq	$0, 192(%rbx)
	movq	$0, 208(%rbx)
	movq	$0, 216(%rbx)
	movq	$1, 232(%rbx)
	movq	$0, 240(%rbx)
	movq	$0, 248(%rbx)
	movq	$0, 264(%rbx)
	movq	$0, 272(%rbx)
	movss	%xmm0, 88(%rbx)
	movss	%xmm0, 144(%rbx)
	movss	%xmm0, 200(%rbx)
	movss	%xmm0, 256(%rbx)
	call	_ZN2v88internal4wasm15WasmCodeManagerC1EPNS1_17WasmMemoryTrackerEm@PLT
	leaq	16+_ZTVN2v88internal19AccountingAllocatorE(%rip), %rax
	pxor	%xmm1, %xmm1
	leaq	432(%rbx), %rdi
	movq	%rax, 408(%rbx)
	movups	%xmm1, 416(%rbx)
	call	_ZN2v88internal21CancelableTaskManagerC1Ev@PLT
	leaq	592(%rbx), %rdi
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	680(%rbx), %rax
	pxor	%xmm1, %xmm1
	movss	.LC4(%rip), %xmm0
	movq	%rax, 632(%rbx)
	leaq	752(%rbx), %rax
	movq	%rax, 704(%rbx)
	leaq	808(%rbx), %rax
	movq	$1, 640(%rbx)
	movq	$0, 648(%rbx)
	movq	$0, 656(%rbx)
	movq	$0, 672(%rbx)
	movq	$0, 696(%rbx)
	movq	$1, 712(%rbx)
	movq	$0, 720(%rbx)
	movq	$0, 728(%rbx)
	movq	$0, 744(%rbx)
	movq	$0, 752(%rbx)
	movq	%rax, 760(%rbx)
	movq	$1, 768(%rbx)
	movq	$0, 776(%rbx)
	movq	$0, 784(%rbx)
	movq	$0, 800(%rbx)
	movq	$0, 808(%rbx)
	movq	$0, 816(%rbx)
	movq	$0, 824(%rbx)
	movss	%xmm0, 664(%rbx)
	movups	%xmm1, 680(%rbx)
	movss	%xmm0, 736(%rbx)
	movss	%xmm0, 792(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19120:
	.size	_ZN2v88internal4wasm10WasmEngineC2Ev, .-_ZN2v88internal4wasm10WasmEngineC2Ev
	.globl	_ZN2v88internal4wasm10WasmEngineC1Ev
	.set	_ZN2v88internal4wasm10WasmEngineC1Ev,_ZN2v88internal4wasm10WasmEngineC2Ev
	.section	.text._ZN2v88internal4wasm10WasmEngine12SyncValidateEPNS0_7IsolateERKNS1_12WasmFeaturesERKNS1_15ModuleWireBytesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine12SyncValidateEPNS0_7IsolateERKNS1_12WasmFeaturesERKNS1_15ModuleWireBytesE
	.type	_ZN2v88internal4wasm10WasmEngine12SyncValidateEPNS0_7IsolateERKNS1_12WasmFeaturesERKNS1_15ModuleWireBytesE, @function
_ZN2v88internal4wasm10WasmEngine12SyncValidateEPNS0_7IsolateERKNS1_12WasmFeaturesERKNS1_15ModuleWireBytesE:
.LFB19125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rcx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L158
	movq	8(%rcx), %rcx
	xorl	%r12d, %r12d
	testl	%ecx, %ecx
	jne	.L161
.L146:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L162
	leaq	-24(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	leaq	-96(%rbp), %r11
	addq	%rdx, %rcx
	xorl	%r9d, %r9d
	movl	$1, %r8d
	addq	$408, %rdi
	pushq	%rdi
	movq	%r11, %rdi
	pushq	40960(%rsi)
	movq	%r10, %rsi
	call	_ZN2v88internal4wasm16DecodeWasmModuleERKNS1_12WasmFeaturesEPKhS6_bNS1_12ModuleOriginEPNS0_8CountersEPNS0_19AccountingAllocatorE@PLT
	movl	-80(%rbp), %eax
	movq	-72(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movdqa	-96(%rbp), %xmm1
	movq	-88(%rbp), %r13
	movaps	%xmm0, -96(%rbp)
	movl	%eax, -144(%rbp)
	leaq	-56(%rbp), %rax
	popq	%rdx
	movaps	%xmm1, -160(%rbp)
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L163
	movq	-56(%rbp), %rax
	movq	%rdi, -136(%rbp)
	movq	%rax, -120(%rbp)
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	leaq	-120(%rbp), %rax
	sete	%r12b
	cmpq	%rax, %rdi
	je	.L149
	call	_ZdlPv@PLT
	movq	-152(%rbp), %r13
.L149:
	testq	%r13, %r13
	je	.L146
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L152
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L153:
	cmpl	$1, %eax
	jne	.L146
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L155
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L156:
	cmpl	$1, %eax
	jne	.L146
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L152:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L158:
	xorl	%r12d, %r12d
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L163:
	cmpq	$0, -64(%rbp)
	sete	%r12b
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L155:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L156
.L162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19125:
	.size	_ZN2v88internal4wasm10WasmEngine12SyncValidateEPNS0_7IsolateERKNS1_12WasmFeaturesERKNS1_15ModuleWireBytesE, .-_ZN2v88internal4wasm10WasmEngine12SyncValidateEPNS0_7IsolateERKNS1_12WasmFeaturesERKNS1_15ModuleWireBytesE
	.section	.rodata._ZN2v88internal4wasm10WasmEngine26SyncCompileTranslatedAsmJsEPNS0_7IsolateEPNS1_12ErrorThrowerERKNS1_15ModuleWireBytesENS0_6VectorIKhEENS0_6HandleINS0_10HeapNumberEEENS0_12LanguageModeE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"unreachable code"
	.section	.text._ZN2v88internal4wasm10WasmEngine26SyncCompileTranslatedAsmJsEPNS0_7IsolateEPNS1_12ErrorThrowerERKNS1_15ModuleWireBytesENS0_6VectorIKhEENS0_6HandleINS0_10HeapNumberEEENS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine26SyncCompileTranslatedAsmJsEPNS0_7IsolateEPNS1_12ErrorThrowerERKNS1_15ModuleWireBytesENS0_6VectorIKhEENS0_6HandleINS0_10HeapNumberEEENS0_12LanguageModeE
	.type	_ZN2v88internal4wasm10WasmEngine26SyncCompileTranslatedAsmJsEPNS0_7IsolateEPNS1_12ErrorThrowerERKNS1_15ModuleWireBytesENS0_6VectorIKhEENS0_6HandleINS0_10HeapNumberEEENS0_12LanguageModeE, @function
_ZN2v88internal4wasm10WasmEngine26SyncCompileTranslatedAsmJsEPNS0_7IsolateEPNS1_12ErrorThrowerERKNS1_15ModuleWireBytesENS0_6VectorIKhEENS0_6HandleINS0_10HeapNumberEEENS0_12LanguageModeE:
.LFB19135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	leaq	-112(%rbp), %r10
	leaq	-136(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$216, %rsp
	movq	%r8, -232(%rbp)
	movq	(%rcx), %rdx
	movq	%r9, -240(%rbp)
	movq	8(%rcx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	xorl	%eax, %eax
	cmpb	$0, 24(%rbp)
	setne	%al
	addq	$408, %rdi
	addq	%rdx, %rcx
	xorl	%r8d, %r8d
	pushq	%rdi
	leal	1(%rax), %r9d
	movq	%r10, %rdi
	pushq	40960(%rsi)
	leaq	_ZN2v88internal4wasmL18kAsmjsWasmFeaturesE(%rip), %rsi
	call	_ZN2v88internal4wasm16DecodeWasmModuleERKNS1_12WasmFeaturesEPKhS6_bNS1_12ModuleOriginEPNS0_8CountersEPNS0_19AccountingAllocatorE@PLT
	movl	-96(%rbp), %edx
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rcx
	movdqa	-112(%rbp), %xmm1
	movq	-112(%rbp), %rax
	movq	%r15, -152(%rbp)
	movq	-104(%rbp), %rsi
	popq	%rdi
	movl	%edx, -160(%rbp)
	movq	-88(%rbp), %rdx
	movaps	%xmm1, -176(%rbp)
	popq	%r8
	movaps	%xmm0, -112(%rbp)
	cmpq	%rcx, %rdx
	je	.L203
	movq	%rdx, -152(%rbp)
	movq	-72(%rbp), %rdx
	movq	%rdx, -136(%rbp)
.L167:
	movq	-80(%rbp), %rdx
	movq	%rdx, -144(%rbp)
	testq	%rdx, %rdx
	jne	.L204
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movq	%rbx, %r9
	movq	%r14, %rcx
	subq	$8, %rsp
	punpcklqdq	%xmm2, %xmm0
	leaq	-216(%rbp), %rax
	movq	%r12, %rsi
	pushq	%rax
	leaq	-192(%rbp), %r13
	leaq	-208(%rbp), %rdi
	movaps	%xmm0, -192(%rbp)
	movq	%r13, %r8
	pxor	%xmm0, %xmm0
	leaq	_ZN2v88internal4wasmL18kAsmjsWasmFeaturesE(%rip), %rdx
	movq	$0, -216(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_ZN2v88internal4wasm21CompileToNativeModuleEPNS0_7IsolateERKNS1_12WasmFeaturesEPNS1_12ErrorThrowerESt10shared_ptrIKNS1_10WasmModuleEERKNS1_15ModuleWireBytesEPNS0_6HandleINS0_10FixedArrayEEE@PLT
	movq	-184(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L170
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L171
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
	cmpl	$1, %eax
	je	.L205
	.p2align 4,,10
	.p2align 3
.L170:
	cmpq	$0, -208(%rbp)
	je	.L206
	movq	-240(%rbp), %r14
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movq	-232(%rbp), %rsi
	movslq	%r14d, %rdx
	movq	%rax, %rbx
	movq	(%rax), %rax
	leaq	15(%rax), %rdi
	call	memcpy@PLT
	movq	16(%rbp), %r8
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm3
	movq	-216(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, %rcx
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	call	_ZN2v88internal11AsmWasmData3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_10FixedArrayEEENS8_INS0_9ByteArrayEEENS8_INS0_10HeapNumberEEE@PLT
	movq	-184(%rbp), %r12
	movq	%rax, %r13
	testq	%r12, %r12
	je	.L177
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L180
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L181:
	cmpl	$1, %eax
	jne	.L177
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L183
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L184:
	cmpl	$1, %eax
	je	.L207
	.p2align 4,,10
	.p2align 3
.L177:
	movq	-200(%rbp), %r12
	testq	%r12, %r12
	je	.L186
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L187
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L208
	.p2align 4,,10
	.p2align 3
.L186:
	movq	-152(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L192
	call	_ZdlPv@PLT
.L192:
	movq	-168(%rbp), %r12
	testq	%r12, %r12
	je	.L194
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L195
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L209
	.p2align 4,,10
	.p2align 3
.L194:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L210
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movl	8(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rdi)
	cmpl	$1, %eax
	jne	.L170
.L205:
	movq	(%rdi), %rax
	movq	%rdi, -248(%rbp)
	call	*16(%rax)
	testq	%rbx, %rbx
	movq	-248(%rbp), %rdi
	je	.L174
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L175:
	cmpl	$1, %eax
	jne	.L170
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L187:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L186
.L208:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L190
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L191:
	cmpl	$1, %eax
	jne	.L186
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L195:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L194
.L209:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L198
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L199:
	cmpl	$1, %eax
	jne	.L194
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L206:
	xorl	%r13d, %r13d
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L203:
	movdqu	-72(%rbp), %xmm4
	movups	%xmm4, -136(%rbp)
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L180:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L198:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L174:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L190:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L207:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L183:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L184
.L210:
	call	__stack_chk_fail@PLT
.L204:
	movq	-152(%rbp), %rsi
	leaq	_ZSt4cout(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19135:
	.size	_ZN2v88internal4wasm10WasmEngine26SyncCompileTranslatedAsmJsEPNS0_7IsolateEPNS1_12ErrorThrowerERKNS1_15ModuleWireBytesENS0_6VectorIKhEENS0_6HandleINS0_10HeapNumberEEENS0_12LanguageModeE, .-_ZN2v88internal4wasm10WasmEngine26SyncCompileTranslatedAsmJsEPNS0_7IsolateEPNS1_12ErrorThrowerERKNS1_15ModuleWireBytesENS0_6VectorIKhEENS0_6HandleINS0_10HeapNumberEEENS0_12LanguageModeE
	.section	.text._ZN2v88internal4wasm10WasmEngine23FinalizeTranslatedAsmJsEPNS0_7IsolateENS0_6HandleINS0_11AsmWasmDataEEENS5_INS0_6ScriptEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine23FinalizeTranslatedAsmJsEPNS0_7IsolateENS0_6HandleINS0_11AsmWasmDataEEENS5_INS0_6ScriptEEE
	.type	_ZN2v88internal4wasm10WasmEngine23FinalizeTranslatedAsmJsEPNS0_7IsolateENS0_6HandleINS0_11AsmWasmDataEEENS5_INS0_6ScriptEEE, @function
_ZN2v88internal4wasm10WasmEngine23FinalizeTranslatedAsmJsEPNS0_7IsolateENS0_6HandleINS0_11AsmWasmDataEEENS5_INS0_6ScriptEEE:
.LFB19140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	7(%rax), %rdx
	movq	7(%rdx), %rdx
	movq	24(%rdx), %rdx
	movq	8(%rdx), %r14
	movq	(%rdx), %r13
	testq	%r14, %r14
	je	.L212
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L213
	lock addl	$1, 8(%r14)
	movq	(%rbx), %rax
.L212:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L214
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L215:
	movq	%r13, %xmm0
	movq	%r14, %xmm1
	movq	%r12, %rdi
	movq	%r15, %rdx
	punpcklqdq	%xmm1, %xmm0
	leaq	-80(%rbp), %rsi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEENS8_INS0_10FixedArrayEEE@PLT
	movq	-72(%rbp), %r13
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L218
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L219
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L239
	.p2align 4,,10
	.p2align 3
.L218:
	movq	(%rbx), %rax
	movq	(%r12), %r14
	movq	23(%rax), %r13
	leaq	55(%r14), %r15
	movq	%r13, 55(%r14)
	testb	$1, %r13b
	je	.L227
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L240
	testb	$24, %al
	je	.L227
.L243:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L241
	.p2align 4,,10
	.p2align 3
.L227:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L242
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	addl	$1, 8(%r14)
	movq	(%rbx), %rax
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L240:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L243
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L219:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L218
.L239:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L222
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L223:
	cmpl	$1, %eax
	jne	.L218
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L214:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L244
.L216:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L241:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L222:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L244:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L216
.L242:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19140:
	.size	_ZN2v88internal4wasm10WasmEngine23FinalizeTranslatedAsmJsEPNS0_7IsolateENS0_6HandleINS0_11AsmWasmDataEEENS5_INS0_6ScriptEEE, .-_ZN2v88internal4wasm10WasmEngine23FinalizeTranslatedAsmJsEPNS0_7IsolateENS0_6HandleINS0_11AsmWasmDataEEENS5_INS0_6ScriptEEE
	.section	.rodata._ZN2v88internal4wasm10WasmEngine11SyncCompileEPNS0_7IsolateERKNS1_12WasmFeaturesEPNS1_12ErrorThrowerERKNS1_15ModuleWireBytesE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"%s @+%u"
	.section	.text._ZN2v88internal4wasm10WasmEngine11SyncCompileEPNS0_7IsolateERKNS1_12WasmFeaturesEPNS1_12ErrorThrowerERKNS1_15ModuleWireBytesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine11SyncCompileEPNS0_7IsolateERKNS1_12WasmFeaturesEPNS1_12ErrorThrowerERKNS1_15ModuleWireBytesE
	.type	_ZN2v88internal4wasm10WasmEngine11SyncCompileEPNS0_7IsolateERKNS1_12WasmFeaturesEPNS1_12ErrorThrowerERKNS1_15ModuleWireBytesE, @function
_ZN2v88internal4wasm10WasmEngine11SyncCompileEPNS0_7IsolateERKNS1_12WasmFeaturesEPNS1_12ErrorThrowerERKNS1_15ModuleWireBytesE:
.LFB19147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$408, %rdi
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r10
	leaq	-136(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$200, %rsp
	movq	(%r8), %rdx
	movq	8(%r8), %rcx
	xorl	%r8d, %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	pushq	%rdi
	movq	%r10, %rdi
	pushq	40960(%rsi)
	addq	%rdx, %rcx
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm16DecodeWasmModuleERKNS1_12WasmFeaturesEPKhS6_bNS1_12ModuleOriginEPNS0_8CountersEPNS0_19AccountingAllocatorE@PLT
	movdqa	-112(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	popq	%rdi
	movl	-96(%rbp), %r8d
	movq	-88(%rbp), %rdx
	leaq	-72(%rbp), %rax
	movq	%r15, -152(%rbp)
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movaps	%xmm1, -176(%rbp)
	movl	%r8d, -160(%rbp)
	popq	%r9
	movaps	%xmm0, -112(%rbp)
	cmpq	%rax, %rdx
	je	.L283
	movq	-72(%rbp), %rax
	movq	%rdx, -152(%rbp)
	movq	%rax, -136(%rbp)
.L247:
	movq	-80(%rbp), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L248
	movq	%r14, %rdi
	movl	%r8d, %ecx
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12ErrorThrower12CompileErrorEPKcz@PLT
	xorl	%r14d, %r14d
.L249:
	movq	-152(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L273
	call	_ZdlPv@PLT
.L273:
	movq	-168(%rbp), %r12
	testq	%r12, %r12
	je	.L275
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L276
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L284
	.p2align 4,,10
	.p2align 3
.L275:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L285
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L275
.L284:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L279
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L280:
	cmpl	$1, %eax
	jne	.L275
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L248:
	subq	$8, %rsp
	movq	%rsi, %xmm2
	movq	%r13, %rdx
	movq	%r12, %r9
	leaq	-216(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rcx
	pushq	%rax
	punpcklqdq	%xmm2, %xmm0
	leaq	-192(%rbp), %r8
	leaq	-208(%rbp), %rdi
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r8, -232(%rbp)
	movq	$0, -216(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_ZN2v88internal4wasm21CompileToNativeModuleEPNS0_7IsolateERKNS1_12WasmFeaturesEPNS1_12ErrorThrowerESt10shared_ptrIKNS1_10WasmModuleEERKNS1_15ModuleWireBytesEPNS0_6HandleINS0_10FixedArrayEEE@PLT
	movq	-184(%rbp), %r14
	popq	%rax
	movq	-232(%rbp), %r8
	popq	%rdx
	testq	%r14, %r14
	je	.L251
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L252
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
.L253:
	cmpl	$1, %eax
	jne	.L251
	movq	(%r14), %rax
	movq	%r8, -232(%rbp)
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	movq	-232(%rbp), %r8
	je	.L255
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L256:
	cmpl	$1, %eax
	jne	.L251
	movq	(%r14), %rax
	movq	%r8, -232(%rbp)
	movq	%r14, %rdi
	call	*24(%rax)
	movq	-232(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L251:
	movq	-208(%rbp), %rax
	xorl	%r14d, %r14d
	testq	%rax, %rax
	je	.L258
	movq	208(%rax), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r8, -232(%rbp)
	addq	$408, %rdx
	call	_ZN2v88internal4wasm16CreateWasmScriptEPNS0_7IsolateERKNS1_15ModuleWireBytesERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-232(%rbp), %r8
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movdqa	-208(%rbp), %xmm4
	movq	-216(%rbp), %rcx
	movq	%rax, %rdx
	movq	%rax, %r12
	movq	%r8, %rsi
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	call	_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEENS8_INS0_10FixedArrayEEE@PLT
	movq	-184(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L260
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L261
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
.L262:
	cmpl	$1, %eax
	je	.L286
.L260:
	movq	41472(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal5Debug14OnAfterCompileENS0_6HandleINS0_6ScriptEEE@PLT
.L258:
	movq	-200(%rbp), %r12
	testq	%r12, %r12
	je	.L249
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L268
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L269:
	cmpl	$1, %eax
	jne	.L249
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L271
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L272:
	cmpl	$1, %eax
	jne	.L249
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L283:
	movdqu	-72(%rbp), %xmm3
	movq	%r15, %rdx
	movups	%xmm3, -136(%rbp)
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L252:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L268:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L279:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L261:
	movl	8(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rdi)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L286:
	movq	(%rdi), %rax
	movq	%rdi, -232(%rbp)
	call	*16(%rax)
	testq	%r13, %r13
	movq	-232(%rbp), %rdi
	je	.L264
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L265:
	cmpl	$1, %eax
	jne	.L260
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L255:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L271:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L264:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L265
.L285:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19147:
	.size	_ZN2v88internal4wasm10WasmEngine11SyncCompileEPNS0_7IsolateERKNS1_12WasmFeaturesEPNS1_12ErrorThrowerERKNS1_15ModuleWireBytesE, .-_ZN2v88internal4wasm10WasmEngine11SyncCompileEPNS0_7IsolateERKNS1_12WasmFeaturesEPNS1_12ErrorThrowerERKNS1_15ModuleWireBytesE
	.section	.text._ZN2v88internal4wasm10WasmEngine15SyncInstantiateEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENSA_INS0_13JSArrayBufferEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine15SyncInstantiateEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENSA_INS0_13JSArrayBufferEEE
	.type	_ZN2v88internal4wasm10WasmEngine15SyncInstantiateEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENSA_INS0_13JSArrayBufferEEE, @function
_ZN2v88internal4wasm10WasmEngine15SyncInstantiateEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENSA_INS0_13JSArrayBufferEEE:
.LFB19151:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	%r8, %rcx
	movq	%r9, %r8
	jmp	_ZN2v88internal4wasm27InstantiateToInstanceObjectEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENS9_INS0_13JSArrayBufferEEE@PLT
	.cfi_endproc
.LFE19151:
	.size	_ZN2v88internal4wasm10WasmEngine15SyncInstantiateEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENSA_INS0_13JSArrayBufferEEE, .-_ZN2v88internal4wasm10WasmEngine15SyncInstantiateEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENSA_INS0_13JSArrayBufferEEE
	.section	.rodata._ZN2v88internal4wasm10WasmEngine16AsyncInstantiateEPNS0_7IsolateESt10unique_ptrINS1_27InstantiationResultResolverESt14default_deleteIS6_EENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"WebAssembly.instantiate()"
	.section	.text._ZN2v88internal4wasm10WasmEngine16AsyncInstantiateEPNS0_7IsolateESt10unique_ptrINS1_27InstantiationResultResolverESt14default_deleteIS6_EENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine16AsyncInstantiateEPNS0_7IsolateESt10unique_ptrINS1_27InstantiationResultResolverESt14default_deleteIS6_EENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEE
	.type	_ZN2v88internal4wasm10WasmEngine16AsyncInstantiateEPNS0_7IsolateESt10unique_ptrINS1_27InstantiationResultResolverESt14default_deleteIS6_EENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEE, @function
_ZN2v88internal4wasm10WasmEngine16AsyncInstantiateEPNS0_7IsolateESt10unique_ptrINS1_27InstantiationResultResolverESt14default_deleteIS6_EENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEE:
.LFB19154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%r8, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rax
	movq	%rsi, -112(%rbp)
	movq	%rax, -104(%rbp)
	leaq	-72(%rbp), %rax
	movq	%rax, -88(%rbp)
	movl	$0, -96(%rbp)
	movq	$0, -80(%rbp)
	movb	$0, -72(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88TryCatch10SetVerboseEb@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88TryCatch17SetCaptureMessageEb@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	-168(%rbp), %rcx
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm27InstantiateToInstanceObjectEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENS9_INS0_13JSArrayBufferEEE@PLT
	testq	%rax, %rax
	je	.L289
	movq	0(%r13), %rdi
	movq	%rax, %rsi
	movq	(%rdi), %rax
	call	*(%rax)
	movq	%r12, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm12ErrorThrowerD1Ev@PLT
.L288:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L298
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	movq	12480(%rbx), %r15
	cmpq	%r15, 96(%rbx)
	je	.L291
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L292
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L293:
	movq	96(%rbx), %rax
	movq	0(%r13), %rdi
	movb	$0, 12545(%rbx)
	movq	%rax, 12480(%rbx)
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm12ErrorThrower5ResetEv@PLT
.L295:
	movq	%r12, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm12ErrorThrowerD1Ev@PLT
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L292:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L299
.L294:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r15, (%rsi)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L291:
	movq	0(%r13), %r13
	movq	%r14, %rdi
	movq	0(%r13), %rax
	movq	8(%rax), %rbx
	call	_ZN2v88internal4wasm12ErrorThrower5ReifyEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	*%rbx
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L299:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L294
.L298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19154:
	.size	_ZN2v88internal4wasm10WasmEngine16AsyncInstantiateEPNS0_7IsolateESt10unique_ptrINS1_27InstantiationResultResolverESt14default_deleteIS6_EENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEE, .-_ZN2v88internal4wasm10WasmEngine16AsyncInstantiateEPNS0_7IsolateESt10unique_ptrINS1_27InstantiationResultResolverESt14default_deleteIS6_EENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEE
	.section	.text._ZN2v88internal4wasm10WasmEngine15CompileFunctionEPNS0_7IsolateEPNS1_12NativeModuleEjNS1_13ExecutionTierE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine15CompileFunctionEPNS0_7IsolateEPNS1_12NativeModuleEjNS1_13ExecutionTierE
	.type	_ZN2v88internal4wasm10WasmEngine15CompileFunctionEPNS0_7IsolateEPNS1_12NativeModuleEjNS1_13ExecutionTierE, @function
_ZN2v88internal4wasm10WasmEngine15CompileFunctionEPNS0_7IsolateEPNS1_12NativeModuleEjNS1_13ExecutionTierE:
.LFB19171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %ecx
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	salq	$5, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	208(%rdx), %rax
	leaq	-21(%rbp), %rdx
	movb	$0, -9(%rbp)
	movq	$0, -21(%rbp)
	addq	136(%rax), %rcx
	movl	$0, -13(%rbp)
	call	_ZN2v88internal4wasm19WasmCompilationUnit19CompileWasmFunctionEPNS0_7IsolateEPNS1_12NativeModuleEPNS1_12WasmFeaturesEPKNS1_12WasmFunctionENS1_13ExecutionTierE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L303:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19171:
	.size	_ZN2v88internal4wasm10WasmEngine15CompileFunctionEPNS0_7IsolateEPNS1_12NativeModuleEjNS1_13ExecutionTierE, .-_ZN2v88internal4wasm10WasmEngine15CompileFunctionEPNS0_7IsolateEPNS1_12NativeModuleEjNS1_13ExecutionTierE
	.section	.text._ZN2v88internal4wasm10WasmEngine18ExportNativeModuleENS0_6HandleINS0_16WasmModuleObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine18ExportNativeModuleENS0_6HandleINS0_16WasmModuleObjectEEE
	.type	_ZN2v88internal4wasm10WasmEngine18ExportNativeModuleENS0_6HandleINS0_16WasmModuleObjectEEE, @function
_ZN2v88internal4wasm10WasmEngine18ExportNativeModuleENS0_6HandleINS0_16WasmModuleObjectEEE:
.LFB19172:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rdx
	movq	%rdi, %rax
	movq	23(%rdx), %rdx
	movq	7(%rdx), %rdx
	movq	24(%rdx), %rdx
	movq	(%rdx), %rcx
	movq	%rcx, (%rdi)
	movq	8(%rdx), %rdx
	movq	%rdx, 8(%rdi)
	testq	%rdx, %rdx
	je	.L304
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L306
	lock addl	$1, 8(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	addl	$1, 8(%rdx)
.L304:
	ret
	.cfi_endproc
.LFE19172:
	.size	_ZN2v88internal4wasm10WasmEngine18ExportNativeModuleENS0_6HandleINS0_16WasmModuleObjectEEE, .-_ZN2v88internal4wasm10WasmEngine18ExportNativeModuleENS0_6HandleINS0_16WasmModuleObjectEEE
	.section	.text._ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv
	.type	_ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv, @function
_ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv:
.LFB19206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	592(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	696(%rbx), %r12
	testq	%r12, %r12
	je	.L316
.L311:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	movl	$160, %edi
	call	_ZN2v88internal8MallocednwEm@PLT
	movl	$-1, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal10CodeTracerC1Ei
	movq	696(%rbx), %rdi
	movq	%r12, 696(%rbx)
	testq	%rdi, %rdi
	je	.L311
	call	_ZN2v88internal8MalloceddlEPv@PLT
	movq	696(%rbx), %r12
	jmp	.L311
	.cfi_endproc
.LFE19206:
	.size	_ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv, .-_ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv
	.section	.text._ZN2v88internal4wasm10WasmEngine20HasRunningCompileJobEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine20HasRunningCompileJobEPNS0_7IsolateE
	.type	_ZN2v88internal4wasm10WasmEngine20HasRunningCompileJobEPNS0_7IsolateE, @function
_ZN2v88internal4wasm10WasmEngine20HasRunningCompileJobEPNS0_7IsolateE:
.LFB19209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	592(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	648(%r12), %rax
	testq	%rax, %rax
	jne	.L319
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L324:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L320
.L319:
	movq	8(%rax), %rdx
	cmpq	(%rdx), %rbx
	jne	.L324
	movl	$1, %r12d
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L320:
	xorl	%r12d, %r12d
.L318:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19209:
	.size	_ZN2v88internal4wasm10WasmEngine20HasRunningCompileJobEPNS0_7IsolateE, .-_ZN2v88internal4wasm10WasmEngine20HasRunningCompileJobEPNS0_7IsolateE
	.section	.text.unlikely._ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE,"ax",@progbits
	.align 2
.LCOLDB8:
	.section	.text._ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE,"ax",@progbits
.LHOTB8:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE
	.type	_ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE, @function
_ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE:
.LFB19233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	592(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	712(%r12), %rsi
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	704(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L326
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movq	8(%rcx), %rdi
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L337:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L326
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L336
.L328:
	cmpq	%rbx, %rdi
	jne	.L337
	movq	16(%rcx), %rax
	movq	%r13, %rdi
	movb	$1, 48(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
.L336:
	.cfi_restore_state
	jmp	.L326
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE
	.cfi_startproc
	.type	_ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE.cold, @function
_ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE.cold:
.LFSB19233:
.L326:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	16, %rax
	ud2
	.cfi_endproc
.LFE19233:
	.section	.text._ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE
	.size	_ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE, .-_ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE
	.section	.text.unlikely._ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE
	.size	_ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE.cold, .-_ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE.cold
.LCOLDE8:
	.section	.text._ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE
.LHOTE8:
	.section	.text._ZN2v88internal4wasm10WasmEngine26RemoveIsolateFromCurrentGCEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine26RemoveIsolateFromCurrentGCEPNS0_7IsolateE
	.type	_ZN2v88internal4wasm10WasmEngine26RemoveIsolateFromCurrentGCEPNS0_7IsolateE, @function
_ZN2v88internal4wasm10WasmEngine26RemoveIsolateFromCurrentGCEPNS0_7IsolateE:
.LFB19291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	824(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_4wasm12_GLOBAL__N_120WasmGCForegroundTaskEESaISA_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS5_.isra.0
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	ret
	.cfi_endproc
.LFE19291:
	.size	_ZN2v88internal4wasm10WasmEngine26RemoveIsolateFromCurrentGCEPNS0_7IsolateE, .-_ZN2v88internal4wasm10WasmEngine26RemoveIsolateFromCurrentGCEPNS0_7IsolateE
	.section	.text._ZN2v88internal4wasm10WasmEngine24InitializeOncePerProcessEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine24InitializeOncePerProcessEv
	.type	_ZN2v88internal4wasm10WasmEngine24InitializeOncePerProcessEv, @function
_ZN2v88internal4wasm10WasmEngine24InitializeOncePerProcessEv:
.LFB19315:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal23FLAG_wasm_shared_engineE(%rip)
	jne	.L359
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$848, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	call	_Znwm@PLT
	movq	%rax, %rbx
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rdi
	leaq	16(%rbx), %r12
	movabsq	$4294967297, %rax
	movq	%rax, 8(%rbx)
	movq	$0, 16(%rbx)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	120(%rbx), %rax
	movq	%r12, %rsi
	movss	.LC4(%rip), %xmm0
	movq	%rax, 72(%rbx)
	leaq	176(%rbx), %rax
	movl	_ZN2v88internal24FLAG_wasm_max_code_spaceE(%rip), %edx
	leaq	296(%rbx), %rdi
	movq	%rax, 128(%rbx)
	leaq	232(%rbx), %rax
	movq	%rax, 184(%rbx)
	leaq	288(%rbx), %rax
	sall	$20, %edx
	movq	%rax, 240(%rbx)
	movq	$0, 64(%rbx)
	movq	$1, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 120(%rbx)
	movq	$1, 136(%rbx)
	movq	$0, 144(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 168(%rbx)
	movq	$0, 176(%rbx)
	movq	$1, 192(%rbx)
	movq	$0, 200(%rbx)
	movq	$0, 208(%rbx)
	movq	$0, 224(%rbx)
	movq	$0, 232(%rbx)
	movq	$1, 248(%rbx)
	movq	$0, 256(%rbx)
	movq	$0, 264(%rbx)
	movq	$0, 280(%rbx)
	movq	$0, 288(%rbx)
	movss	%xmm0, 104(%rbx)
	movss	%xmm0, 160(%rbx)
	movss	%xmm0, 216(%rbx)
	movss	%xmm0, 272(%rbx)
	call	_ZN2v88internal4wasm15WasmCodeManagerC1EPNS1_17WasmMemoryTrackerEm@PLT
	leaq	16+_ZTVN2v88internal19AccountingAllocatorE(%rip), %rax
	pxor	%xmm1, %xmm1
	leaq	448(%rbx), %rdi
	movq	%rax, 424(%rbx)
	movups	%xmm1, 432(%rbx)
	call	_ZN2v88internal21CancelableTaskManagerC1Ev@PLT
	leaq	608(%rbx), %rdi
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	696(%rbx), %rax
	pxor	%xmm1, %xmm1
	movss	.LC4(%rip), %xmm0
	movq	%rax, 648(%rbx)
	leaq	768(%rbx), %rax
	movq	%rax, 720(%rbx)
	leaq	824(%rbx), %rax
	movq	$1, 656(%rbx)
	movq	$0, 664(%rbx)
	movq	$0, 672(%rbx)
	movq	$0, 688(%rbx)
	movq	$0, 712(%rbx)
	movq	$1, 728(%rbx)
	movq	$0, 736(%rbx)
	movq	$0, 744(%rbx)
	movq	$0, 760(%rbx)
	movq	$0, 768(%rbx)
	movq	%rax, 776(%rbx)
	movq	$1, 784(%rbx)
	movq	$0, 792(%rbx)
	movq	$0, 800(%rbx)
	movq	$0, 816(%rbx)
	movq	$0, 824(%rbx)
	movq	$0, 832(%rbx)
	movq	$0, 840(%rbx)
	movss	%xmm0, 680(%rbx)
	movups	%xmm1, 696(%rbx)
	movss	%xmm0, 752(%rbx)
	movss	%xmm0, 808(%rbx)
	movzbl	_ZGVZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object(%rip), %eax
	testb	%al, %al
	je	.L360
.L343:
	movq	8+_ZZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object(%rip), %r13
	movq	%r12, %xmm0
	movq	%rbx, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, _ZZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object(%rip)
	testq	%r13, %r13
	je	.L340
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L347
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L348:
	cmpl	$1, %eax
	jne	.L340
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L350
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L351:
	cmpl	$1, %eax
	jne	.L340
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L360:
	leaq	_ZGVZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	pxor	%xmm1, %xmm1
	testl	%eax, %eax
	je	.L343
	leaq	_ZGVZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object(%rip), %rdi
	movaps	%xmm1, _ZZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L350:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L351
	.cfi_endproc
.LFE19315:
	.size	_ZN2v88internal4wasm10WasmEngine24InitializeOncePerProcessEv, .-_ZN2v88internal4wasm10WasmEngine24InitializeOncePerProcessEv
	.section	.text._ZN2v88internal4wasm10WasmEngine14GlobalTearDownEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine14GlobalTearDownEv
	.type	_ZN2v88internal4wasm10WasmEngine14GlobalTearDownEv, @function
_ZN2v88internal4wasm10WasmEngine14GlobalTearDownEv:
.LFB19322:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal23FLAG_wasm_shared_engineE(%rip)
	jne	.L382
	ret
	.p2align 4,,10
	.p2align 3
.L382:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movzbl	_ZGVZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object(%rip), %eax
	testb	%al, %al
	je	.L383
.L365:
	movq	8+_ZZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object(%rip), %r12
	pxor	%xmm0, %xmm0
	movaps	%xmm0, _ZZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object(%rip)
	testq	%r12, %r12
	je	.L361
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L368
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L369:
	cmpl	$1, %eax
	jne	.L361
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L370
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L371:
	cmpl	$1, %eax
	jne	.L361
	movq	(%r12), %rax
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L383:
	leaq	_ZGVZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L365
	pxor	%xmm0, %xmm0
	leaq	_ZGVZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object(%rip), %rdi
	movaps	%xmm0, _ZZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L370:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L371
	.cfi_endproc
.LFE19322:
	.size	_ZN2v88internal4wasm10WasmEngine14GlobalTearDownEv, .-_ZN2v88internal4wasm10WasmEngine14GlobalTearDownEv
	.section	.text._ZN2v88internal4wasm10WasmEngine13GetWasmEngineEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine13GetWasmEngineEv
	.type	_ZN2v88internal4wasm10WasmEngine13GetWasmEngineEv, @function
_ZN2v88internal4wasm10WasmEngine13GetWasmEngineEv:
.LFB19323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpb	$0, _ZN2v88internal23FLAG_wasm_shared_engineE(%rip)
	je	.L385
	movzbl	_ZGVZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object(%rip), %eax
	testb	%al, %al
	je	.L398
.L387:
	movq	_ZZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object(%rip), %rax
	movq	%rax, (%r12)
	movq	8+_ZZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object(%rip), %rax
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L384
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L390
	lock addl	$1, 8(%rax)
.L384:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	movq	$0, (%rdi)
	movl	$848, %edi
	call	_Znwm@PLT
	movq	%rax, %rbx
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rdi
	leaq	16(%rbx), %r13
	movabsq	$4294967297, %rax
	movq	%rax, 8(%rbx)
	movq	$0, 16(%rbx)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	120(%rbx), %rax
	movl	_ZN2v88internal24FLAG_wasm_max_code_spaceE(%rip), %edx
	movss	.LC4(%rip), %xmm0
	movq	%rax, 72(%rbx)
	leaq	176(%rbx), %rax
	movq	%r13, %rsi
	leaq	296(%rbx), %rdi
	movq	%rax, 128(%rbx)
	leaq	232(%rbx), %rax
	sall	$20, %edx
	movq	%rax, 184(%rbx)
	leaq	288(%rbx), %rax
	movss	%xmm0, 104(%rbx)
	movss	%xmm0, 160(%rbx)
	movss	%xmm0, 216(%rbx)
	movss	%xmm0, 272(%rbx)
	movq	%rax, 240(%rbx)
	movq	$0, 64(%rbx)
	movq	$1, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 120(%rbx)
	movq	$1, 136(%rbx)
	movq	$0, 144(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 168(%rbx)
	movq	$0, 176(%rbx)
	movq	$1, 192(%rbx)
	movq	$0, 200(%rbx)
	movq	$0, 208(%rbx)
	movq	$0, 224(%rbx)
	movq	$0, 232(%rbx)
	movq	$1, 248(%rbx)
	movq	$0, 256(%rbx)
	movq	$0, 264(%rbx)
	movq	$0, 280(%rbx)
	movq	$0, 288(%rbx)
	call	_ZN2v88internal4wasm15WasmCodeManagerC1EPNS1_17WasmMemoryTrackerEm@PLT
	leaq	16+_ZTVN2v88internal19AccountingAllocatorE(%rip), %rax
	pxor	%xmm1, %xmm1
	leaq	448(%rbx), %rdi
	movq	%rax, 424(%rbx)
	movups	%xmm1, 432(%rbx)
	call	_ZN2v88internal21CancelableTaskManagerC1Ev@PLT
	leaq	608(%rbx), %rdi
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	696(%rbx), %rax
	movq	%rbx, %xmm2
	movss	.LC4(%rip), %xmm0
	movq	%rax, 648(%rbx)
	pxor	%xmm1, %xmm1
	leaq	768(%rbx), %rax
	movss	%xmm0, 680(%rbx)
	movss	%xmm0, 752(%rbx)
	movss	%xmm0, 808(%rbx)
	movq	%r13, %xmm0
	movq	%rax, 720(%rbx)
	punpcklqdq	%xmm2, %xmm0
	leaq	824(%rbx), %rax
	movq	%rax, 776(%rbx)
	movq	%r12, %rax
	movq	$1, 656(%rbx)
	movq	$0, 664(%rbx)
	movq	$0, 672(%rbx)
	movq	$0, 688(%rbx)
	movq	$0, 712(%rbx)
	movq	$1, 728(%rbx)
	movq	$0, 736(%rbx)
	movq	$0, 744(%rbx)
	movq	$0, 760(%rbx)
	movq	$0, 768(%rbx)
	movq	$1, 784(%rbx)
	movq	$0, 792(%rbx)
	movq	$0, 800(%rbx)
	movq	$0, 816(%rbx)
	movq	$0, 824(%rbx)
	movq	$0, 832(%rbx)
	movq	$0, 840(%rbx)
	movups	%xmm1, 696(%rbx)
	movups	%xmm0, (%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L387
	pxor	%xmm0, %xmm0
	leaq	_ZGVZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object(%rip), %rdi
	movaps	%xmm0, _ZZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L390:
	addl	$1, 8(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19323:
	.size	_ZN2v88internal4wasm10WasmEngine13GetWasmEngineEv, .-_ZN2v88internal4wasm10WasmEngine13GetWasmEngineEv
	.section	.text._ZN2v88internal4wasm13max_mem_pagesEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm13max_mem_pagesEv
	.type	_ZN2v88internal4wasm13max_mem_pagesEv, @function
_ZN2v88internal4wasm13max_mem_pagesEv:
.LFB19330:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal23FLAG_wasm_max_mem_pagesE(%rip), %eax
	movl	$32767, %edx
	cmpl	$32767, %eax
	cmovnb	%edx, %eax
	ret
	.cfi_endproc
.LFE19330:
	.size	_ZN2v88internal4wasm13max_mem_pagesEv, .-_ZN2v88internal4wasm13max_mem_pagesEv
	.section	.text._ZN2v88internal4wasm22max_table_init_entriesEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm22max_table_init_entriesEv
	.type	_ZN2v88internal4wasm22max_table_init_entriesEv, @function
_ZN2v88internal4wasm22max_table_init_entriesEv:
.LFB19332:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal24FLAG_wasm_max_table_sizeE(%rip), %eax
	movl	$10000000, %edx
	cmpl	$10000000, %eax
	cmovnb	%edx, %eax
	ret
	.cfi_endproc
.LFE19332:
	.size	_ZN2v88internal4wasm22max_table_init_entriesEv, .-_ZN2v88internal4wasm22max_table_init_entriesEv
	.section	.text._ZNSt8_Rb_treeIPN2v88internal4wasm12NativeModuleES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal4wasm12NativeModuleES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal4wasm12NativeModuleES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIPN2v88internal4wasm12NativeModuleES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIPN2v88internal4wasm12NativeModuleES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB23066:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L411
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L405:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal4wasm12NativeModuleES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L405
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE23066:
	.size	_ZNSt8_Rb_treeIPN2v88internal4wasm12NativeModuleES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIPN2v88internal4wasm12NativeModuleES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.type	_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, @function
_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E:
.LFB23109:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L422
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L416:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L416
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE23109:
	.size	_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, .-_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E:
.LFB23244:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L441
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L430:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L427
	call	_ZdlPv@PLT
.L427:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L428
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L425
.L429:
	movq	%rbx, %r12
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L428:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L429
.L425:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE23244:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E:
.LFB23252:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L461
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L450:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	movq	136(%r12), %rdi
	movq	16(%r12), %rbx
	leaq	152(%r12), %rax
	cmpq	%rax, %rdi
	je	.L446
	call	_ZdlPv@PLT
.L446:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L447
	call	_ZdlPv@PLT
.L447:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L448
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L444
.L449:
	movq	%rbx, %r12
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L448:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L449
.L444:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE23252:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	.section	.text._ZN2v88internal4wasm10WasmEngine27DumpAndResetTurboStatisticsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine27DumpAndResetTurboStatisticsEv
	.type	_ZN2v88internal4wasm10WasmEngine27DumpAndResetTurboStatisticsEv, @function
_ZN2v88internal4wasm10WasmEngine27DumpAndResetTurboStatisticsEv:
.LFB19205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	592(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpq	$0, 688(%r12)
	je	.L465
	leaq	-320(%rbp), %r15
	leaq	-400(%rbp), %r13
	movq	%r15, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%ax, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rbx, -320(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r13, %rdi
	leaq	-416(%rbp), %rsi
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	movq	688(%r12), %rax
	movb	$0, -408(%rbp)
	movq	%rax, -416(%rbp)
	call	_ZN2v88internallsERSoRKNS0_21AsPrintableStatisticsE@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %rdi
	testq	%rdi, %rdi
	je	.L504
	cmpb	$0, 56(%rdi)
	je	.L467
	movsbl	67(%rdi), %esi
.L468:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r15, %rdi
	movq	%rbx, -320(%rbp)
	movq	%rax, -400(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	688(%r12), %r13
	movq	$0, 688(%r12)
	testq	%r13, %r13
	je	.L465
	leaq	168(%r13), %rdi
	leaq	120(%r13), %r15
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	136(%r13), %r12
	testq	%r12, %r12
	je	.L475
.L469:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	movq	136(%r12), %rdi
	movq	16(%r12), %rbx
	leaq	152(%r12), %rax
	cmpq	%rax, %rdi
	je	.L472
	call	_ZdlPv@PLT
.L472:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L473
	call	_ZdlPv@PLT
.L473:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L474
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L475
.L476:
	movq	%rbx, %r12
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L480:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L481
.L470:
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	cmpq	%rax, %rdi
	je	.L478
	call	_ZdlPv@PLT
.L478:
	movq	%r13, %rdi
	call	_ZN2v88internal8MalloceddlEPv@PLT
.L465:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L505
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	.cfi_restore_state
	movq	%rdi, -424(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-424(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L468
	call	*%rax
	movsbl	%al, %esi
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L474:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L476
.L475:
	movq	88(%r13), %r12
	leaq	72(%r13), %r15
	testq	%r12, %r12
	je	.L470
.L471:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L479
	call	_ZdlPv@PLT
.L479:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L480
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L470
.L481:
	movq	%rbx, %r12
	jmp	.L471
.L505:
	call	__stack_chk_fail@PLT
.L504:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE19205:
	.size	_ZN2v88internal4wasm10WasmEngine27DumpAndResetTurboStatisticsEv, .-_ZN2v88internal4wasm10WasmEngine27DumpAndResetTurboStatisticsEv
	.section	.text._ZN2v88internal4wasm10WasmEngineD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngineD2Ev
	.type	_ZN2v88internal4wasm10WasmEngineD2Ev, @function
_ZN2v88internal4wasm10WasmEngineD2Ev:
.LFB19123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	432(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal21CancelableTaskManager13CancelAndWaitEv@PLT
	movq	824(%r12), %r13
	testq	%r13, %r13
	je	.L507
	movq	72(%r13), %rbx
	testq	%rbx, %rbx
	je	.L508
	.p2align 4,,10
	.p2align 3
.L509:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L509
.L508:
	movq	64(%r13), %rax
	movq	56(%r13), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	56(%r13), %rdi
	leaq	104(%r13), %rax
	movq	$0, 80(%r13)
	movq	$0, 72(%r13)
	cmpq	%rax, %rdi
	je	.L510
	call	_ZdlPv@PLT
.L510:
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	je	.L511
	.p2align 4,,10
	.p2align 3
.L512:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L512
.L511:
	movq	8(%r13), %rax
	movq	0(%r13), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	0(%r13), %rdi
	leaq	48(%r13), %rax
	movq	$0, 24(%r13)
	movq	$0, 16(%r13)
	cmpq	%rax, %rdi
	je	.L513
	call	_ZdlPv@PLT
.L513:
	movl	$128, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L507:
	movq	776(%r12), %rbx
	testq	%rbx, %rbx
	je	.L514
	.p2align 4,,10
	.p2align 3
.L525:
	movq	%rbx, %r15
	movq	(%rbx), %rbx
	movq	16(%r15), %r13
	testq	%r13, %r13
	je	.L515
	movq	128(%r13), %rax
	testq	%rax, %rax
	je	.L516
	.p2align 4,,10
	.p2align 3
.L517:
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	jne	.L517
.L516:
	movq	120(%r13), %rax
	movq	112(%r13), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	112(%r13), %rdi
	leaq	160(%r13), %rax
	movq	$0, 136(%r13)
	movq	$0, 128(%r13)
	cmpq	%rax, %rdi
	je	.L518
	call	_ZdlPv@PLT
.L518:
	movq	72(%r13), %rax
	testq	%rax, %rax
	je	.L519
	.p2align 4,,10
	.p2align 3
.L520:
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	jne	.L520
.L519:
	movq	64(%r13), %rax
	movq	56(%r13), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	56(%r13), %rdi
	leaq	104(%r13), %rax
	movq	$0, 80(%r13)
	movq	$0, 72(%r13)
	cmpq	%rax, %rdi
	je	.L521
	call	_ZdlPv@PLT
.L521:
	movq	16(%r13), %rax
	testq	%rax, %rax
	je	.L522
	.p2align 4,,10
	.p2align 3
.L523:
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	jne	.L523
.L522:
	movq	8(%r13), %rax
	movq	0(%r13), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	0(%r13), %rdi
	leaq	48(%r13), %rax
	movq	$0, 24(%r13)
	movq	$0, 16(%r13)
	cmpq	%rax, %rdi
	je	.L524
	call	_ZdlPv@PLT
.L524:
	movl	$176, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L515:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L525
.L514:
	movq	768(%r12), %rax
	movq	760(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	760(%r12), %rdi
	leaq	808(%r12), %rax
	movq	$0, 784(%r12)
	movq	$0, 776(%r12)
	cmpq	%rax, %rdi
	je	.L526
	call	_ZdlPv@PLT
.L526:
	movq	720(%r12), %rbx
	testq	%rbx, %rbx
	je	.L527
	.p2align 4,,10
	.p2align 3
.L546:
	movq	%rbx, %rax
	movq	%rbx, -56(%rbp)
	movq	(%rbx), %rbx
	movq	16(%rax), %r13
	testq	%r13, %r13
	je	.L528
	movq	112(%r13), %r15
	testq	%r15, %r15
	je	.L530
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L531
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r15)
	cmpl	$1, %edx
	je	.L651
	.p2align 4,,10
	.p2align 3
.L530:
	movq	96(%r13), %r15
	testq	%r15, %r15
	je	.L537
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L538
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r15)
	cmpl	$1, %edx
	je	.L652
	.p2align 4,,10
	.p2align 3
.L537:
	movq	64(%r13), %rdi
	testq	%rdi, %rdi
	je	.L543
	call	_ZdlPv@PLT
.L543:
	movq	16(%r13), %r15
	testq	%r15, %r15
	je	.L544
.L545:
	movq	24(%r15), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal4wasm12NativeModuleES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	%r15, %rdi
	movq	16(%r15), %r15
	call	_ZdlPv@PLT
	testq	%r15, %r15
	jne	.L545
.L544:
	movl	$120, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L528:
	movq	-56(%rbp), %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L546
.L527:
	movq	712(%r12), %rax
	movq	704(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	704(%r12), %rdi
	leaq	752(%r12), %rax
	movq	$0, 728(%r12)
	movq	$0, 720(%r12)
	cmpq	%rax, %rdi
	je	.L547
	call	_ZdlPv@PLT
.L547:
	movq	696(%r12), %rdi
	testq	%rdi, %rdi
	je	.L548
	call	_ZN2v88internal8MalloceddlEPv@PLT
.L548:
	movq	688(%r12), %r13
	testq	%r13, %r13
	je	.L549
	leaq	168(%r13), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	136(%r13), %r15
	leaq	120(%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%r15, %r15
	je	.L550
.L555:
	movq	-56(%rbp), %rdi
	movq	24(%r15), %rsi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	movq	136(%r15), %rdi
	leaq	152(%r15), %rax
	movq	16(%r15), %rbx
	cmpq	%rax, %rdi
	je	.L551
	call	_ZdlPv@PLT
.L551:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L552
	call	_ZdlPv@PLT
.L552:
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	cmpq	%rax, %rdi
	je	.L553
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L550
.L554:
	movq	%rbx, %r15
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L553:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L554
.L550:
	movq	88(%r13), %r15
	leaq	72(%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%r15, %r15
	je	.L556
.L560:
	movq	-56(%rbp), %rdi
	movq	24(%r15), %rsi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	movq	16(%r15), %rbx
	cmpq	%rax, %rdi
	je	.L557
	call	_ZdlPv@PLT
.L557:
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	cmpq	%rax, %rdi
	je	.L558
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L556
.L559:
	movq	%rbx, %r15
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L558:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L559
.L556:
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	cmpq	%rax, %rdi
	je	.L561
	call	_ZdlPv@PLT
.L561:
	movq	%r13, %rdi
	call	_ZN2v88internal8MalloceddlEPv@PLT
.L549:
	movq	648(%r12), %r13
	testq	%r13, %r13
	jne	.L565
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L653:
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm15AsyncCompileJobD1Ev@PLT
	movq	%r15, %rdi
	movl	$320, %esi
	call	_ZdlPvm@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L562
.L564:
	movq	%rbx, %r13
.L565:
	movq	16(%r13), %r15
	movq	0(%r13), %rbx
	testq	%r15, %r15
	jne	.L653
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L564
.L562:
	movq	640(%r12), %rax
	movq	632(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	632(%r12), %rdi
	leaq	680(%r12), %rax
	movq	$0, 656(%r12)
	movq	$0, 648(%r12)
	cmpq	%rax, %rdi
	je	.L566
	call	_ZdlPv@PLT
.L566:
	leaq	592(%r12), %rdi
	leaq	360(%r12), %r13
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal21CancelableTaskManagerD1Ev@PLT
	leaq	408(%r12), %rdi
	call	_ZN2v88internal19AccountingAllocatorD1Ev@PLT
	movq	376(%r12), %rbx
	testq	%rbx, %rbx
	je	.L567
.L568:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmS0_ImPN2v88internal4wasm12NativeModuleEEESt10_Select1stIS8_ESt4lessImESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L568
.L567:
	leaq	320(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm17WasmMemoryTrackerD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L538:
	.cfi_restore_state
	movl	8(%r15), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r15)
	cmpl	$1, %edx
	jne	.L537
.L652:
	movq	(%r15), %rdx
	movq	%rax, -64(%rbp)
	movq	%r15, %rdi
	call	*16(%rdx)
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L541
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L542:
	cmpl	$1, %eax
	jne	.L537
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L531:
	movl	8(%r15), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r15)
	cmpl	$1, %edx
	jne	.L530
.L651:
	movq	(%r15), %rdx
	movq	%rax, -64(%rbp)
	movq	%r15, %rdi
	call	*16(%rdx)
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L534
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L535:
	cmpl	$1, %eax
	jne	.L530
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L534:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L541:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L542
	.cfi_endproc
.LFE19123:
	.size	_ZN2v88internal4wasm10WasmEngineD2Ev, .-_ZN2v88internal4wasm10WasmEngineD2Ev
	.globl	_ZN2v88internal4wasm10WasmEngineD1Ev
	.set	_ZN2v88internal4wasm10WasmEngineD1Ev,_ZN2v88internal4wasm10WasmEngineD2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB26417:
	.cfi_startproc
	endbr64
	addq	$16, %rdi
	jmp	_ZN2v88internal4wasm10WasmEngineD1Ev
	.cfi_endproc
.LFE26417:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN2v88internal4wasm10WasmEngine26GetOrCreateTurboStatisticsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine26GetOrCreateTurboStatisticsEv
	.type	_ZN2v88internal4wasm10WasmEngine26GetOrCreateTurboStatisticsEv, @function
_ZN2v88internal4wasm10WasmEngine26GetOrCreateTurboStatisticsEv:
.LFB19174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	592(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$24, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	688(%rbx), %r12
	testq	%r12, %r12
	je	.L690
.L656:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L690:
	.cfi_restore_state
	movl	$208, %edi
	call	_ZN2v88internal8MallocednwEm@PLT
	movl	$26, %ecx
	movq	%rax, %r12
	xorl	%eax, %eax
	movq	%r12, %rdi
	rep stosq
	leaq	48(%r12), %rax
	leaq	168(%r12), %rdi
	movq	%rax, 32(%r12)
	leaq	80(%r12), %rax
	movq	%rax, 96(%r12)
	movq	%rax, 104(%r12)
	leaq	128(%r12), %rax
	movq	%rax, 144(%r12)
	movq	%rax, 152(%r12)
	call	_ZN2v84base5MutexC1Ev@PLT
	movq	688(%rbx), %r14
	movq	%r12, 688(%rbx)
	testq	%r14, %r14
	je	.L656
	leaq	168(%r14), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	136(%r14), %r15
	leaq	120(%r14), %rax
	movq	%rax, -56(%rbp)
	testq	%r15, %r15
	je	.L663
.L657:
	movq	-56(%rbp), %rdi
	movq	24(%r15), %rsi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics10PhaseStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	movq	136(%r15), %rdi
	leaq	152(%r15), %rax
	movq	16(%r15), %r12
	cmpq	%rax, %rdi
	je	.L660
	call	_ZdlPv@PLT
.L660:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L661
	call	_ZdlPv@PLT
.L661:
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	cmpq	%rax, %rdi
	je	.L662
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L663
.L664:
	movq	%r12, %r15
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L662:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L664
.L663:
	movq	88(%r14), %r15
	leaq	72(%r14), %rax
	movq	%rax, -56(%rbp)
	testq	%r15, %r15
	je	.L658
.L659:
	movq	-56(%rbp), %rdi
	movq	24(%r15), %rsi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v88internal21CompilationStatistics12OrderedStatsEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	movq	16(%r15), %r12
	cmpq	%rax, %rdi
	je	.L667
	call	_ZdlPv@PLT
.L667:
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	cmpq	%rax, %rdi
	je	.L668
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L658
.L669:
	movq	%r12, %r15
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L668:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L669
.L658:
	movq	32(%r14), %rdi
	leaq	48(%r14), %rax
	cmpq	%rax, %rdi
	je	.L666
	call	_ZdlPv@PLT
.L666:
	movq	%r14, %rdi
	call	_ZN2v88internal8MalloceddlEPv@PLT
	movq	688(%rbx), %r12
	jmp	.L656
	.cfi_endproc
.LFE19174:
	.size	_ZN2v88internal4wasm10WasmEngine26GetOrCreateTurboStatisticsEv, .-_ZN2v88internal4wasm10WasmEngine26GetOrCreateTurboStatisticsEv
	.section	.rodata._ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_.str1.1,"aMS",@progbits,1
.LC9:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB23364:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L705
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L701
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L706
.L693:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L700:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L707
	testq	%r13, %r13
	jg	.L696
	testq	%r9, %r9
	jne	.L699
.L697:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L707:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L696
.L699:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L706:
	testq	%rsi, %rsi
	jne	.L694
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L696:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L697
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L701:
	movl	$8, %r14d
	jmp	.L693
.L705:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L694:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L693
	.cfi_endproc
.LFE23364:
	.size	_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.rodata._ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC10:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_default_appendEm
	.type	_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_default_appendEm, @function
_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_default_appendEm:
.LFB23414:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L727
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%r12), %rax
	movq	%rcx, %rsi
	subq	(%rdi), %rsi
	subq	%rcx, %rax
	movq	%rdx, %rdi
	movq	%rsi, %r13
	sarq	$3, %rax
	sarq	$3, %r13
	subq	%r13, %rdi
	cmpq	%rbx, %rax
	jb	.L710
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L727:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L710:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L730
	cmpq	%rbx, %r13
	movq	%rbx, %r14
	movq	%rsi, -56(%rbp)
	cmovnb	%r13, %r14
	addq	%r13, %r14
	cmpq	%rdx, %r14
	cmova	%rdx, %r14
	salq	$3, %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	leaq	0(,%rbx,8), %rdx
	movq	%rax, %r15
	leaq	(%rax,%rsi), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%r12), %r8
	movq	8(%r12), %rdx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L731
	testq	%r8, %r8
	jne	.L714
.L715:
	addq	%r13, %rbx
	addq	%r15, %r14
	movq	%r15, (%r12)
	leaq	(%r15,%rbx,8), %rax
	movq	%r14, 16(%r12)
	movq	%rax, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L731:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L714:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L715
.L730:
	leaq	.LC10(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23414:
	.size	_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_default_appendEm, .-_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_default_appendEm
	.section	.text._ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_St10unique_ptrINS1_4wasm10WasmEngine11IsolateInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_St10unique_ptrINS1_4wasm10WasmEngine11IsolateInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_St10unique_ptrINS1_4wasm10WasmEngine11IsolateInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm
	.type	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_St10unique_ptrINS1_4wasm10WasmEngine11IsolateInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm, @function
_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_St10unique_ptrINS1_4wasm10WasmEngine11IsolateInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm:
.LFB24266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L733
	movq	(%rbx), %r8
.L734:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L743
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L744:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L733:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L757
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L758
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L736:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L738
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L740:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L741:
	testq	%rsi, %rsi
	je	.L738
.L739:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L740
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L746
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L739
	.p2align 4,,10
	.p2align 3
.L738:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L742
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L742:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L743:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L745
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L745:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L746:
	movq	%rdx, %rdi
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L757:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L736
.L758:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE24266:
	.size	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_St10unique_ptrINS1_4wasm10WasmEngine11IsolateInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm, .-_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_St10unique_ptrINS1_4wasm10WasmEngine11IsolateInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm
	.section	.text._ZNSt8__detail9_Map_baseIPN2v88internal7IsolateESt4pairIKS4_St10unique_ptrINS2_4wasm10WasmEngine11IsolateInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_,"axG",@progbits,_ZNSt8__detail9_Map_baseIPN2v88internal7IsolateESt4pairIKS4_St10unique_ptrINS2_4wasm10WasmEngine11IsolateInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIPN2v88internal7IsolateESt4pairIKS4_St10unique_ptrINS2_4wasm10WasmEngine11IsolateInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	.type	_ZNSt8__detail9_Map_baseIPN2v88internal7IsolateESt4pairIKS4_St10unique_ptrINS2_4wasm10WasmEngine11IsolateInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_, @function
_ZNSt8__detail9_Map_baseIPN2v88internal7IsolateESt4pairIKS4_St10unique_ptrINS2_4wasm10WasmEngine11IsolateInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_:
.LFB23200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %r13
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	%r13, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L760
	movq	(%rax), %rcx
	movq	8(%rcx), %r8
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L772:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L760
	movq	8(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L760
.L762:
	cmpq	%r8, %r13
	jne	.L772
	popq	%rbx
	leaq	16(%rcx), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L760:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movq	(%rbx), %rax
	movl	$1, %r8d
	movq	$0, 16(%rcx)
	movq	%rax, 8(%rcx)
	call	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_St10unique_ptrINS1_4wasm10WasmEngine11IsolateInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm
	popq	%rbx
	popq	%r12
	addq	$16, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23200:
	.size	_ZNSt8__detail9_Map_baseIPN2v88internal7IsolateESt4pairIKS4_St10unique_ptrINS2_4wasm10WasmEngine11IsolateInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_, .-_ZNSt8__detail9_Map_baseIPN2v88internal7IsolateESt4pairIKS4_St10unique_ptrINS2_4wasm10WasmEngine11IsolateInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	.section	.text._ZZN2v88internal4wasm10WasmEngine10AddIsolateEPNS0_7IsolateEENUlPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPvE_4_FUNES6_S7_S8_S9_,"ax",@progbits
	.p2align 4
	.type	_ZZN2v88internal4wasm10WasmEngine10AddIsolateEPNS0_7IsolateEENUlPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPvE_4_FUNES6_S7_S8_S9_, @function
_ZZN2v88internal4wasm10WasmEngine10AddIsolateEPNS0_7IsolateEENUlPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPvE_4_FUNES6_S7_S8_S9_:
.LFB19226:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	45752(%rdi), %rbx
	movq	40960(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -72(%rbp)
	leaq	592(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	-72(%rbp), %rsi
	leaq	704(%rbx), %rdi
	call	_ZNSt8__detail9_Map_baseIPN2v88internal7IsolateESt4pairIKS4_St10unique_ptrINS2_4wasm10WasmEngine11IsolateInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	movq	(%rax), %r15
	movq	24(%r15), %r12
	addq	$8, %r15
	cmpq	%r15, %r12
	je	.L774
	.p2align 4,,10
	.p2align 3
.L775:
	movq	32(%r12), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	call	_ZNK2v88internal4wasm12NativeModule14SampleCodeSizeEPNS0_8CountersENS2_16CodeSamplingTimeE@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r15
	jne	.L775
.L774:
	movq	824(%rbx), %rax
	testq	%rax, %rax
	je	.L776
	movq	120(%rax), %rbx
	testq	%rbx, %rbx
	jne	.L786
.L776:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L787
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L786:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	movq	40960(%rax), %r12
	call	_ZN2v84base9TimeTicks3NowEv@PLT
	leaq	-64(%rbp), %rdi
	subq	%rbx, %rax
	addq	$4920, %r12
	movq	%rax, -64(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movl	$2147483647, %esi
	movq	%r12, %rdi
	cmpq	$2147483647, %rax
	cmovle	%rax, %rsi
	movl	$0, %eax
	testq	%rsi, %rsi
	cmovs	%rax, %rsi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	jmp	.L776
.L787:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19226:
	.size	_ZZN2v88internal4wasm10WasmEngine10AddIsolateEPNS0_7IsolateEENUlPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPvE_4_FUNES6_S7_S8_S9_, .-_ZZN2v88internal4wasm10WasmEngine10AddIsolateEPNS0_7IsolateEENUlPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPvE_4_FUNES6_S7_S8_S9_
	.section	.text._ZN2v88internal4wasm10WasmEngine29LogOutstandingCodesForIsolateEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine29LogOutstandingCodesForIsolateEPNS0_7IsolateE
	.type	_ZN2v88internal4wasm10WasmEngine29LogOutstandingCodesForIsolateEPNS0_7IsolateE, @function
_ZN2v88internal4wasm10WasmEngine29LogOutstandingCodesForIsolateEPNS0_7IsolateE:
.LFB19234:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$16, %rsp
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal4wasm8WasmCode14ShouldBeLoggedEPNS0_7IsolateE@PLT
	testb	%al, %al
	jne	.L806
.L788:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L806:
	.cfi_restore_state
	leaq	592(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	704(%rbx), %rdi
	leaq	-40(%rbp), %rsi
	call	_ZNSt8__detail9_Map_baseIPN2v88internal7IsolateESt4pairIKS4_St10unique_ptrINS2_4wasm10WasmEngine11IsolateInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	(%rax), %rax
	movq	64(%rax), %r13
	movq	72(%rax), %r12
	movq	$0, 80(%rax)
	movups	%xmm0, 64(%rax)
	movq	%r13, %rbx
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	cmpq	%r12, %r13
	je	.L805
	.p2align 4,,10
	.p2align 3
.L793:
	movq	(%rbx), %rdi
	movq	-40(%rbp), %rsi
	addq	$8, %rbx
	call	_ZNK2v88internal4wasm8WasmCode7LogCodeEPNS0_7IsolateE@PLT
	cmpq	%rbx, %r12
	jne	.L793
	subq	%r13, %r12
	movq	%r13, %rdi
	sarq	$3, %r12
	movslq	%r12d, %rsi
	movabsq	$2305843009213693951, %r12
	andq	%r12, %rsi
	call	_ZN2v88internal4wasm8WasmCode17DecrementRefCountENS0_6VectorIKPS2_EE@PLT
.L805:
	testq	%r13, %r13
	je	.L788
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19234:
	.size	_ZN2v88internal4wasm10WasmEngine29LogOutstandingCodesForIsolateEPNS0_7IsolateE, .-_ZN2v88internal4wasm10WasmEngine29LogOutstandingCodesForIsolateEPNS0_7IsolateE
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTask3RunEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTask3RunEv, @function
_ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTask3RunEv:
.LFB18876:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L807
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	$0, 16(%rdi)
	movq	%rdi, %rbx
	je	.L809
	movq	8(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	$0, (%rax)
	movq	$0, 16(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	24(%rbx), %rsi
.L809:
	movq	32(%rbx), %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm10WasmEngine29LogOutstandingCodesForIsolateEPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L807:
	ret
	.cfi_endproc
.LFE18876:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTask3RunEv, .-_ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTask3RunEv
	.section	.text._ZN2v88internal4wasm10WasmEngine10AddIsolateEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine10AddIsolateEPNS0_7IsolateE
	.type	_ZN2v88internal4wasm10WasmEngine10AddIsolateEPNS0_7IsolateE, @function
_ZN2v88internal4wasm10WasmEngine10AddIsolateEPNS0_7IsolateE:
.LFB19224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	592(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	$120, %edi
	call	_Znwm@PLT
	movq	%r13, %rdi
	movl	$0, 8(%rax)
	movq	%rax, %r12
	addq	$8, %rax
	movq	$0, 8(%rax)
	movq	%rax, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	call	_ZN2v88internal4wasm8WasmCode14ShouldBeLoggedEPNS0_7IsolateE@PLT
	pxor	%xmm0, %xmm0
	movdqu	40960(%r13), %xmm2
	movb	%al, 48(%r12)
	movq	40968(%r13), %rax
	movups	%xmm0, 56(%r12)
	movups	%xmm0, 72(%r12)
	movups	%xmm0, 88(%r12)
	movups	%xmm2, 104(%r12)
	testq	%rax, %rax
	je	.L814
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L815
	lock addl	$1, 8(%rax)
.L814:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	(%rax), %rax
	call	*48(%rax)
	movdqa	-80(%rbp), %xmm0
	movq	96(%r12), %r14
	pxor	%xmm1, %xmm1
	movaps	%xmm1, -80(%rbp)
	movups	%xmm0, 88(%r12)
	testq	%r14, %r14
	je	.L817
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L818
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r14)
	cmpl	$1, %edx
	je	.L873
.L821:
	movq	-72(%rbp), %r14
	testq	%r14, %r14
	je	.L817
	testq	%rax, %rax
	je	.L826
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r14)
	cmpl	$1, %edx
	je	.L874
	.p2align 4,,10
	.p2align 3
.L817:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	712(%rbx), %rsi
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %r14
	movq	%r13, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%r13, %rax
	divq	%rsi
	movq	704(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L830
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L875:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L830
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L830
.L832:
	cmpq	%r13, %rdi
	jne	.L875
	movq	112(%r12), %rbx
	testq	%rbx, %rbx
	je	.L833
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L834
	movl	$-1, %edx
	lock xaddl	%edx, 8(%rbx)
.L835:
	cmpl	$1, %edx
	je	.L876
	.p2align 4,,10
	.p2align 3
.L833:
	movq	96(%r12), %rbx
	testq	%rbx, %rbx
	je	.L840
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L841
	movl	$-1, %edx
	lock xaddl	%edx, 8(%rbx)
.L842:
	cmpl	$1, %edx
	je	.L877
.L840:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L846
	call	_ZdlPv@PLT
.L846:
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	je	.L849
.L847:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal4wasm12NativeModuleES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L847
.L849:
	movq	%r12, %rdi
	movl	$120, %esi
	call	_ZdlPvm@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L848:
	xorl	%ecx, %ecx
	leaq	37592(%r13), %rdi
	movl	$2, %edx
	leaq	_ZZN2v88internal4wasm10WasmEngine10AddIsolateEPNS0_7IsolateEENUlPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPvE_4_FUNES6_S7_S8_S9_(%rip), %rsi
	call	_ZN2v88internal4Heap21AddGCEpilogueCallbackEPFvPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPvES4_S6_@PLT
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L878
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L815:
	.cfi_restore_state
	addl	$1, 8(%rax)
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L818:
	movl	8(%r14), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r14)
	cmpl	$1, %edx
	jne	.L821
.L873:
	movq	(%r14), %rdx
	movq	%rax, -88(%rbp)
	movq	%r14, %rdi
	call	*16(%rdx)
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L822
	movl	$-1, %edx
	lock xaddl	%edx, 12(%r14)
.L823:
	cmpl	$1, %edx
	jne	.L821
	movq	(%r14), %rdx
	movq	%rax, -88(%rbp)
	movq	%r14, %rdi
	call	*24(%rdx)
	movq	-88(%rbp), %rax
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L830:
	leaq	704(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r9, %rsi
	movl	$1, %r8d
	call	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_St10unique_ptrINS1_4wasm10WasmEngine11IsolateInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L826:
	movl	8(%r14), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r14)
	cmpl	$1, %edx
	jne	.L817
.L874:
	movq	(%r14), %rdx
	movq	%rax, -88(%rbp)
	movq	%r14, %rdi
	call	*16(%rdx)
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L828
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L829:
	cmpl	$1, %eax
	jne	.L817
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L828:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L822:
	movl	12(%r14), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%r14)
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L877:
	movq	(%rbx), %rdx
	movq	%rax, -88(%rbp)
	movq	%rbx, %rdi
	call	*16(%rdx)
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L844
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rbx)
.L845:
	cmpl	$1, %eax
	jne	.L840
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L876:
	movq	(%rbx), %rdx
	movq	%rax, -88(%rbp)
	movq	%rbx, %rdi
	call	*16(%rdx)
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L837
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rbx)
.L838:
	cmpl	$1, %eax
	jne	.L833
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L834:
	movl	8(%rbx), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%rbx)
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L841:
	movl	8(%rbx), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%rbx)
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L844:
	movl	12(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rbx)
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L837:
	movl	12(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rbx)
	jmp	.L838
.L878:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19224:
	.size	_ZN2v88internal4wasm10WasmEngine10AddIsolateEPNS0_7IsolateE, .-_ZN2v88internal4wasm10WasmEngine10AddIsolateEPNS0_7IsolateE
	.section	.text._ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm
	.type	_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm, @function
_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm:
.LFB24326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L880
	movq	(%rbx), %r8
.L881:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L890
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L891:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L880:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L904
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L905
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L883:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L885
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L887:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L888:
	testq	%rsi, %rsi
	je	.L885
.L886:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L887
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L893
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L886
	.p2align 4,,10
	.p2align 3
.L885:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L889
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L889:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L890:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L892
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L892:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L893:
	movq	%rdx, %rdi
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L904:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L883
.L905:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE24326:
	.size	_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm, .-_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm
	.section	.text._ZNSt8__detail9_Map_baseIPN2v88internal4wasm12NativeModuleESt4pairIKS5_St10unique_ptrINS3_10WasmEngine16NativeModuleInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS5_ESt4hashIS5_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS7_,"axG",@progbits,_ZNSt8__detail9_Map_baseIPN2v88internal4wasm12NativeModuleESt4pairIKS5_St10unique_ptrINS3_10WasmEngine16NativeModuleInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS5_ESt4hashIS5_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIPN2v88internal4wasm12NativeModuleESt4pairIKS5_St10unique_ptrINS3_10WasmEngine16NativeModuleInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS5_ESt4hashIS5_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS7_
	.type	_ZNSt8__detail9_Map_baseIPN2v88internal4wasm12NativeModuleESt4pairIKS5_St10unique_ptrINS3_10WasmEngine16NativeModuleInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS5_ESt4hashIS5_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS7_, @function
_ZNSt8__detail9_Map_baseIPN2v88internal4wasm12NativeModuleESt4pairIKS5_St10unique_ptrINS3_10WasmEngine16NativeModuleInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS5_ESt4hashIS5_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS7_:
.LFB23226:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %r13
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	%r13, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L907
	movq	(%rax), %rcx
	movq	8(%rcx), %r8
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L919:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L907
	movq	8(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L907
.L909:
	cmpq	%r8, %r13
	jne	.L919
	popq	%rbx
	leaq	16(%rcx), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L907:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movq	(%rbx), %rax
	movl	$1, %r8d
	movq	$0, 16(%rcx)
	movq	%rax, 8(%rcx)
	call	_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm
	popq	%rbx
	popq	%r12
	addq	$16, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23226:
	.size	_ZNSt8__detail9_Map_baseIPN2v88internal4wasm12NativeModuleESt4pairIKS5_St10unique_ptrINS3_10WasmEngine16NativeModuleInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS5_ESt4hashIS5_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS7_, .-_ZNSt8__detail9_Map_baseIPN2v88internal4wasm12NativeModuleESt4pairIKS5_St10unique_ptrINS3_10WasmEngine16NativeModuleInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS5_ESt4hashIS5_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS7_
	.section	.text._ZN2v88internal4wasm10WasmEngine7LogCodeEPNS1_8WasmCodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine7LogCodeEPNS1_8WasmCodeE
	.type	_ZN2v88internal4wasm10WasmEngine7LogCodeEPNS1_8WasmCodeE, @function
_ZN2v88internal4wasm10WasmEngine7LogCodeEPNS1_8WasmCodeE:
.LFB19229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	592(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	704(%rbx), %r12
	subq	$72, %rsp
	movq	%rdi, -96(%rbp)
	movq	%r14, %rdi
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-88(%rbp), %rax
	leaq	760(%rbx), %rdi
	leaq	-80(%rbp), %rsi
	leaq	-72(%rbp), %rbx
	movq	48(%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZNSt8__detail9_Map_baseIPN2v88internal4wasm12NativeModuleESt4pairIKS5_St10unique_ptrINS3_10WasmEngine16NativeModuleInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS5_ESt4hashIS5_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS7_
	movq	(%rax), %rax
	movq	16(%rax), %r15
	testq	%r15, %r15
	jne	.L928
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L923:
	movq	72(%r13), %rsi
	cmpq	64(%r13), %rsi
	je	.L939
.L925:
	cmpq	%rsi, 80(%r13)
	je	.L926
.L940:
	movq	-88(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 72(%r13)
.L927:
	movq	-88(%rbp), %rax
	lock addl	$1, 140(%rax)
.L922:
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.L929
.L928:
	movq	8(%r15), %rax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZNSt8__detail9_Map_baseIPN2v88internal7IsolateESt4pairIKS4_St10unique_ptrINS2_4wasm10WasmEngine11IsolateInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	movq	(%rax), %r13
	cmpb	$0, 48(%r13)
	je	.L922
	cmpq	$0, 56(%r13)
	jne	.L923
	movq	-72(%rbp), %rsi
	movl	$40, %edi
	movq	%rsi, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdx
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskE(%rip), %rcx
	leaq	56(%r13), %rdi
	movq	%rcx, (%rax)
	movq	%r14, 8(%rax)
	movq	%rdi, 16(%rax)
	movq	%rsi, 24(%rax)
	movq	%rdx, 32(%rax)
	movq	88(%r13), %rdi
	movq	%rax, 56(%r13)
	movq	(%rdi), %rsi
	movq	(%rsi), %r8
	movq	%rax, -64(%rbp)
	leaq	-64(%rbp), %rsi
	call	*%r8
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L923
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	72(%r13), %rsi
	cmpq	64(%r13), %rsi
	jne	.L925
	.p2align 4,,10
	.p2align 3
.L939:
	movq	-72(%rbp), %rax
	movl	$64, %esi
	leaq	37512(%rax), %rdi
	call	_ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE@PLT
	movq	72(%r13), %rsi
	cmpq	%rsi, 80(%r13)
	jne	.L940
	.p2align 4,,10
	.p2align 3
.L926:
	leaq	-88(%rbp), %r8
	leaq	64(%r13), %rdi
	movq	%r8, %rdx
	call	_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L929:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L941
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L941:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19229:
	.size	_ZN2v88internal4wasm10WasmEngine7LogCodeEPNS1_8WasmCodeE, .-_ZN2v88internal4wasm10WasmEngine7LogCodeEPNS1_8WasmCodeE
	.section	.text._ZN2v88internal4wasm10WasmEngine34SampleTopTierCodeSizeInAllIsolatesERKSt10shared_ptrINS1_12NativeModuleEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine34SampleTopTierCodeSizeInAllIsolatesERKSt10shared_ptrINS1_12NativeModuleEE
	.type	_ZN2v88internal4wasm10WasmEngine34SampleTopTierCodeSizeInAllIsolatesERKSt10shared_ptrINS1_12NativeModuleEE, @function
_ZN2v88internal4wasm10WasmEngine34SampleTopTierCodeSizeInAllIsolatesERKSt10shared_ptrINS1_12NativeModuleEE:
.LFB19265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	592(%rdi), %rax
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	0(%r13), %r14
	movq	768(%r12), %rdi
	xorl	%edx, %edx
	movq	%r14, %rax
	divq	%rdi
	movq	760(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r15
	testq	%rax, %rax
	je	.L943
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L973:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L943
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r15
	jne	.L943
.L945:
	cmpq	%rsi, %r14
	jne	.L973
	addq	$16, %rcx
.L954:
	movq	(%rcx), %rax
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.L950
	leaq	704(%r12), %rax
	movq	%rax, -112(%rbp)
	leaq	-72(%rbp), %rax
	movq	%rax, -120(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L951:
	movq	8(%rbx), %rax
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	%rax, -72(%rbp)
	call	_ZNSt8__detail9_Map_baseIPN2v88internal7IsolateESt4pairIKS4_St10unique_ptrINS2_4wasm10WasmEngine11IsolateInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	movq	8(%r13), %rdx
	movq	-72(%rbp), %r12
	movq	(%rax), %rax
	movq	0(%r13), %r10
	movq	88(%rax), %r15
	movq	(%r15), %rax
	movq	(%rax), %r14
	testq	%rdx, %rdx
	je	.L947
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L948
	lock addl	$1, 12(%rdx)
.L947:
	movl	$64, %edi
	movq	%rdx, -104(%rbp)
	movq	%r10, -96(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %r10
	movq	%r15, %rdi
	movq	-104(%rbp), %rdx
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskE(%rip), %rcx
	movq	-128(%rbp), %rsi
	movq	%rcx, (%rax)
	addq	$48, %rcx
	addq	$32, %rax
	movq	%rcx, (%rax)
	movq	%r12, 8(%rax)
	movq	%r10, 16(%rax)
	movq	%rdx, 24(%rax)
	movq	%rax, -64(%rbp)
	call	*%r14
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L974
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L951
.L950:
	movq	-136(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L975
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L948:
	.cfi_restore_state
	addl	$1, 12(%rdx)
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L974:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L951
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L943:
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	760(%r12), %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	%r14, 8(%rax)
	movq	$0, 16(%rax)
	call	_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm
	leaq	16(%rax), %rcx
	jmp	.L954
.L975:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19265:
	.size	_ZN2v88internal4wasm10WasmEngine34SampleTopTierCodeSizeInAllIsolatesERKSt10shared_ptrINS1_12NativeModuleEE, .-_ZN2v88internal4wasm10WasmEngine34SampleTopTierCodeSizeInAllIsolatesERKSt10shared_ptrINS1_12NativeModuleEE
	.section	.text._ZNSt10_HashtableIPN2v88internal4wasm15AsyncCompileJobESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal4wasm15AsyncCompileJobESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal4wasm15AsyncCompileJobESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm
	.type	_ZNSt10_HashtableIPN2v88internal4wasm15AsyncCompileJobESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm, @function
_ZNSt10_HashtableIPN2v88internal4wasm15AsyncCompileJobESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm:
.LFB24391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L977
	movq	(%rbx), %r8
.L978:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L987
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L988:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L977:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L1001
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1002
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L980:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L982
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L984:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L985:
	testq	%rsi, %rsi
	je	.L982
.L983:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L984
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L990
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L983
	.p2align 4,,10
	.p2align 3
.L982:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L986
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L986:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L987:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L989
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L989:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L990:
	movq	%rdx, %rdi
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L1001:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L980
.L1002:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE24391:
	.size	_ZNSt10_HashtableIPN2v88internal4wasm15AsyncCompileJobESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm, .-_ZNSt10_HashtableIPN2v88internal4wasm15AsyncCompileJobESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm
	.section	.text._ZN2v88internal4wasm10WasmEngine21CreateAsyncCompileJobEPNS0_7IsolateERKNS1_12WasmFeaturesESt10unique_ptrIA_hSt14default_deleteIS9_EEmNS0_6HandleINS0_7ContextEEEPKcSt10shared_ptrINS1_25CompilationResultResolverEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine21CreateAsyncCompileJobEPNS0_7IsolateERKNS1_12WasmFeaturesESt10unique_ptrIA_hSt14default_deleteIS9_EEmNS0_6HandleINS0_7ContextEEEPKcSt10shared_ptrINS1_25CompilationResultResolverEE
	.type	_ZN2v88internal4wasm10WasmEngine21CreateAsyncCompileJobEPNS0_7IsolateERKNS1_12WasmFeaturesESt10unique_ptrIA_hSt14default_deleteIS9_EEmNS0_6HandleINS0_7ContextEEEPKcSt10shared_ptrINS1_25CompilationResultResolverEE, @function
_ZN2v88internal4wasm10WasmEngine21CreateAsyncCompileJobEPNS0_7IsolateERKNS1_12WasmFeaturesESt10unique_ptrIA_hSt14default_deleteIS9_EEmNS0_6HandleINS0_7ContextEEEPKcSt10shared_ptrINS1_25CompilationResultResolverEE:
.LFB19207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$320, %edi
	subq	$72, %rsp
	movq	16(%rbp), %rdx
	movq	24(%rbp), %rax
	movq	%r9, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movdqu	(%rax), %xmm1
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movq	(%rcx), %rsi
	movq	$0, (%rcx)
	movups	%xmm0, (%rax)
	movq	%rsi, -88(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r9
	movq	%r13, %rsi
	movq	%rax, %r12
	leaq	-80(%rbp), %rax
	leaq	-88(%rbp), %rcx
	movq	%r15, %r8
	pushq	%rax
	movq	%r12, %rdi
	pushq	%rdx
	movq	%r14, %rdx
	call	_ZN2v88internal4wasm15AsyncCompileJobC1EPNS0_7IsolateERKNS1_12WasmFeaturesESt10unique_ptrIA_hSt14default_deleteIS9_EEmNS0_6HandleINS0_7ContextEEEPKcSt10shared_ptrINS1_25CompilationResultResolverEE@PLT
	movq	-72(%rbp), %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L1005
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L1006
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1033
	.p2align 4,,10
	.p2align 3
.L1005:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1011
	call	_ZdaPv@PLT
.L1011:
	leaq	592(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	640(%rbx), %rsi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	632(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L1012
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1034:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1012
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r14
	jne	.L1012
.L1014:
	cmpq	%rdi, %r12
	jne	.L1034
	addq	$16, %rcx
.L1016:
	movq	(%rcx), %r14
	movq	%r12, (%rcx)
	testq	%r14, %r14
	je	.L1015
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15AsyncCompileJobD1Ev@PLT
	movl	$320, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1015:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1035
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1006:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1005
.L1033:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L1009
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1010:
	cmpl	$1, %eax
	jne	.L1005
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1012:
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	632(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	%r12, 8(%rax)
	movq	$0, 16(%rax)
	call	_ZNSt10_HashtableIPN2v88internal4wasm15AsyncCompileJobESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSD_10_Hash_nodeISB_Lb0EEEm
	leaq	16(%rax), %rcx
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1009:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1010
.L1035:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19207:
	.size	_ZN2v88internal4wasm10WasmEngine21CreateAsyncCompileJobEPNS0_7IsolateERKNS1_12WasmFeaturesESt10unique_ptrIA_hSt14default_deleteIS9_EEmNS0_6HandleINS0_7ContextEEEPKcSt10shared_ptrINS1_25CompilationResultResolverEE, .-_ZN2v88internal4wasm10WasmEngine21CreateAsyncCompileJobEPNS0_7IsolateERKNS1_12WasmFeaturesESt10unique_ptrIA_hSt14default_deleteIS9_EEmNS0_6HandleINS0_7ContextEEEPKcSt10shared_ptrINS1_25CompilationResultResolverEE
	.section	.text._ZN2v88internal4wasm10WasmEngine25StartStreamingCompilationEPNS0_7IsolateERKNS1_12WasmFeaturesENS0_6HandleINS0_7ContextEEEPKcSt10shared_ptrINS1_25CompilationResultResolverEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine25StartStreamingCompilationEPNS0_7IsolateERKNS1_12WasmFeaturesENS0_6HandleINS0_7ContextEEEPKcSt10shared_ptrINS1_25CompilationResultResolverEE
	.type	_ZN2v88internal4wasm10WasmEngine25StartStreamingCompilationEPNS0_7IsolateERKNS1_12WasmFeaturesENS0_6HandleINS0_7ContextEEEPKcSt10shared_ptrINS1_25CompilationResultResolverEE, @function
_ZN2v88internal4wasm10WasmEngine25StartStreamingCompilationEPNS0_7IsolateERKNS1_12WasmFeaturesENS0_6HandleINS0_7ContextEEEPKcSt10shared_ptrINS1_25CompilationResultResolverEE:
.LFB19170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	pushq	%rbx
	movq	%rcx, %rdx
	leaq	-72(%rbp), %rcx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	16(%rbp), %rax
	movq	%fs:40, %rbx
	movq	%rbx, -40(%rbp)
	xorl	%ebx, %ebx
	movq	$0, -72(%rbp)
	movdqu	(%rax), %xmm1
	movups	%xmm0, (%rax)
	leaq	-64(%rbp), %rax
	pushq	%rax
	pushq	%r9
	movq	%r8, %r9
	xorl	%r8d, %r8d
	movaps	%xmm1, -64(%rbp)
	call	_ZN2v88internal4wasm10WasmEngine21CreateAsyncCompileJobEPNS0_7IsolateERKNS1_12WasmFeaturesESt10unique_ptrIA_hSt14default_deleteIS9_EEmNS0_6HandleINS0_7ContextEEEPKcSt10shared_ptrINS1_25CompilationResultResolverEE
	movq	-72(%rbp), %rdi
	movq	%rax, %r14
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L1037
	call	_ZdaPv@PLT
.L1037:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L1039
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1040
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1050
	.p2align 4,,10
	.p2align 3
.L1039:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm15AsyncCompileJob22CreateStreamingDecoderEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1051
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1040:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1039
.L1050:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1043
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1044:
	cmpl	$1, %eax
	jne	.L1039
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1043:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1044
.L1051:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19170:
	.size	_ZN2v88internal4wasm10WasmEngine25StartStreamingCompilationEPNS0_7IsolateERKNS1_12WasmFeaturesENS0_6HandleINS0_7ContextEEEPKcSt10shared_ptrINS1_25CompilationResultResolverEE, .-_ZN2v88internal4wasm10WasmEngine25StartStreamingCompilationEPNS0_7IsolateERKNS1_12WasmFeaturesENS0_6HandleINS0_7ContextEEEPKcSt10shared_ptrINS1_25CompilationResultResolverEE
	.section	.rodata._ZN2v88internal4wasm10WasmEngine12AsyncCompileEPNS0_7IsolateERKNS1_12WasmFeaturesESt10shared_ptrINS1_25CompilationResultResolverEERKNS1_15ModuleWireBytesEbPKc.str1.1,"aMS",@progbits,1
.LC11:
	.string	"(location_) != nullptr"
.LC12:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal4wasm10WasmEngine12AsyncCompileEPNS0_7IsolateERKNS1_12WasmFeaturesESt10shared_ptrINS1_25CompilationResultResolverEERKNS1_15ModuleWireBytesEbPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine12AsyncCompileEPNS0_7IsolateERKNS1_12WasmFeaturesESt10shared_ptrINS1_25CompilationResultResolverEERKNS1_15ModuleWireBytesEbPKc
	.type	_ZN2v88internal4wasm10WasmEngine12AsyncCompileEPNS0_7IsolateERKNS1_12WasmFeaturesESt10shared_ptrINS1_25CompilationResultResolverEERKNS1_15ModuleWireBytesEbPKc, @function
_ZN2v88internal4wasm10WasmEngine12AsyncCompileEPNS0_7IsolateERKNS1_12WasmFeaturesESt10shared_ptrINS1_25CompilationResultResolverEERKNS1_15ModuleWireBytesEbPKc:
.LFB19155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$136, %rsp
	movq	16(%rbp), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal27FLAG_wasm_async_compilationE(%rip)
	jne	.L1053
	movq	%r15, %xmm1
	movq	%rsi, %xmm0
	leaq	-72(%rbp), %rax
	movl	$0, -96(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -88(%rbp)
	leaq	-112(%rbp), %r15
	movq	$0, -80(%rbp)
	movb	$0, -72(%rbp)
	movaps	%xmm0, -112(%rbp)
	testb	%r9b, %r9b
	jne	.L1098
	movq	%r15, %rcx
	call	_ZN2v88internal4wasm10WasmEngine11SyncCompileEPNS0_7IsolateERKNS1_12WasmFeaturesEPNS1_12ErrorThrowerERKNS1_15ModuleWireBytesE
	movl	-96(%rbp), %edi
	movq	%rax, %r12
	testl	%edi, %edi
	jne	.L1099
.L1056:
	testq	%r12, %r12
	je	.L1100
	movq	0(%r13), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*(%rax)
.L1057:
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm12ErrorThrowerD1Ev@PLT
.L1052:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1101
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1053:
	.cfi_restore_state
	cmpb	$0, _ZN2v88internal24FLAG_wasm_test_streamingE(%rip)
	je	.L1060
	movq	(%rcx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, -152(%rbp)
	movq	8(%rcx), %rax
	movups	%xmm0, (%rcx)
	movq	41112(%r12), %rdi
	movq	12464(%rsi), %rsi
	movq	%rax, -160(%rbp)
	testq	%rdi, %rdi
	je	.L1061
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-168(%rbp), %r10
	movq	%rax, %r9
.L1062:
	leaq	-128(%rbp), %r13
	movq	%r14, %rdi
	movq	%r12, %rsi
	xorl	%r8d, %r8d
	pushq	%r13
	leaq	-136(%rbp), %rcx
	movq	%r10, %rdx
	movq	-152(%rbp), %xmm0
	pushq	%r15
	movhps	-160(%rbp), %xmm0
	movq	$0, -136(%rbp)
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal4wasm10WasmEngine21CreateAsyncCompileJobEPNS0_7IsolateERKNS1_12WasmFeaturesESt10unique_ptrIA_hSt14default_deleteIS9_EEmNS0_6HandleINS0_7ContextEEEPKcSt10shared_ptrINS1_25CompilationResultResolverEE
	movq	-136(%rbp), %rdi
	popq	%rcx
	movq	%rax, %r14
	popq	%rsi
	testq	%rdi, %rdi
	je	.L1064
	call	_ZdaPv@PLT
.L1064:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L1066
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1067
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L1068:
	cmpl	$1, %eax
	je	.L1102
.L1066:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm15AsyncCompileJob22CreateStreamingDecoderEv@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	-128(%rbp), %rdi
	call	_ZN2v88internal4wasm16StreamingDecoder15OnBytesReceivedENS0_6VectorIKhEE@PLT
	movq	-128(%rbp), %rdi
	call	_ZN2v88internal4wasm16StreamingDecoder6FinishEv@PLT
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L1052
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1074
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L1075:
	cmpl	$1, %eax
	jne	.L1052
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L1077
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1078:
	cmpl	$1, %eax
	jne	.L1052
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1098:
	movslq	8(%r8), %rdi
	movq	%rdx, -168(%rbp)
	call	_Znam@PLT
	movslq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rdx, -160(%rbp)
	call	memcpy@PLT
	movq	%r12, %rsi
	leaq	-128(%rbp), %r8
	movq	%r15, %rcx
	movq	-160(%rbp), %rdx
	movq	-168(%rbp), %r10
	movq	%r14, %rdi
	movq	%rax, -128(%rbp)
	movq	%rax, -152(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%r10, %rdx
	call	_ZN2v88internal4wasm10WasmEngine11SyncCompileEPNS0_7IsolateERKNS1_12WasmFeaturesEPNS1_12ErrorThrowerERKNS1_15ModuleWireBytesE
	movq	-152(%rbp), %r9
	movq	%rax, %r12
	movq	%r9, %rdi
	call	_ZdaPv@PLT
	movl	-96(%rbp), %edi
	testl	%edi, %edi
	je	.L1056
.L1099:
	movq	0(%r13), %r12
	movq	%r15, %rdi
	movq	(%r12), %rax
	movq	8(%rax), %rbx
	call	_ZN2v88internal4wasm12ErrorThrower5ReifyEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	*%rbx
	jmp	.L1057
	.p2align 4,,10
	.p2align 3
.L1060:
	movslq	8(%r8), %rdi
	movq	%rdx, -152(%rbp)
	call	_Znam@PLT
	movq	(%rbx), %rsi
	movslq	8(%rbx), %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	pxor	%xmm0, %xmm0
	movdqu	0(%r13), %xmm2
	movq	-152(%rbp), %r10
	movups	%xmm0, 0(%r13)
	movq	41112(%r12), %rdi
	movq	%rax, %rcx
	movq	12464(%r12), %rsi
	movaps	%xmm2, -128(%rbp)
	testq	%rdi, %rdi
	je	.L1079
	movq	%r10, -160(%rbp)
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %r10
	movq	%rax, %r9
.L1080:
	leaq	-128(%rbp), %rax
	movq	%r10, %rdx
	movq	%r14, %rdi
	movq	%r12, %rsi
	pushq	%rax
	pushq	%r15
	movslq	8(%rbx), %r8
	movq	%rcx, -136(%rbp)
	leaq	-136(%rbp), %rcx
	call	_ZN2v88internal4wasm10WasmEngine21CreateAsyncCompileJobEPNS0_7IsolateERKNS1_12WasmFeaturesESt10unique_ptrIA_hSt14default_deleteIS9_EEmNS0_6HandleINS0_7ContextEEEPKcSt10shared_ptrINS1_25CompilationResultResolverEE
	movq	-136(%rbp), %rdi
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L1082
	call	_ZdaPv@PLT
.L1082:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L1084
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1085
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L1086:
	cmpl	$1, %eax
	je	.L1103
.L1084:
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm15AsyncCompileJob5StartEv@PLT
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1061:
	movq	41088(%r12), %r9
	cmpq	%r9, 41096(%r12)
	je	.L1104
.L1063:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r9)
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1079:
	movq	41088(%r12), %r9
	cmpq	41096(%r12), %r9
	je	.L1105
.L1081:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r9)
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1074:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1067:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L1068
	.p2align 4,,10
	.p2align 3
.L1085:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L1086
	.p2align 4,,10
	.p2align 3
.L1102:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L1070
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1071:
	cmpl	$1, %eax
	jne	.L1066
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L1103:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L1088
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1089:
	cmpl	$1, %eax
	jne	.L1084
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1084
	.p2align 4,,10
	.p2align 3
.L1100:
	leaq	.LC11(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1077:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1070:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1104:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	movq	%rdx, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %r10
	movq	-168(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1088:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1105:
	movq	%r12, %rdi
	movq	%r10, -168(%rbp)
	movq	%rsi, -160(%rbp)
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %r10
	movq	-160(%rbp), %rsi
	movq	-152(%rbp), %rcx
	movq	%rax, %r9
	jmp	.L1081
.L1101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19155:
	.size	_ZN2v88internal4wasm10WasmEngine12AsyncCompileEPNS0_7IsolateERKNS1_12WasmFeaturesESt10shared_ptrINS1_25CompilationResultResolverEERKNS1_15ModuleWireBytesEbPKc, .-_ZN2v88internal4wasm10WasmEngine12AsyncCompileEPNS0_7IsolateERKNS1_12WasmFeaturesESt10shared_ptrINS1_25CompilationResultResolverEERKNS1_15ModuleWireBytesEbPKc
	.section	.text._ZNSt10_HashtableIPN2v88internal4wasm15AsyncCompileJobESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSD_20_Node_const_iteratorISB_Lb0ELb0EEE,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal4wasm15AsyncCompileJobESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSD_20_Node_const_iteratorISB_Lb0ELb0EEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal4wasm15AsyncCompileJobESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSD_20_Node_const_iteratorISB_Lb0ELb0EEE
	.type	_ZNSt10_HashtableIPN2v88internal4wasm15AsyncCompileJobESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSD_20_Node_const_iteratorISB_Lb0ELb0EEE, @function
_ZNSt10_HashtableIPN2v88internal4wasm15AsyncCompileJobESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSD_20_Node_const_iteratorISB_Lb0ELb0EEE:
.LFB24437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%r12), %rax
	movq	%rdi, %rbx
	movq	8(%rdi), %rsi
	movq	(%rbx), %r8
	divq	%rsi
	leaq	0(,%rdx,8), %r10
	movq	%rdx, %rdi
	leaq	(%r8,%r10), %r9
	movq	(%r9), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1107:
	movq	%rdx, %rcx
	movq	(%rdx), %rdx
	cmpq	%rdx, %r12
	jne	.L1107
	movq	(%r12), %r13
	cmpq	%rcx, %rax
	je	.L1122
	testq	%r13, %r13
	je	.L1110
	movq	8(%r13), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %rdi
	je	.L1110
	movq	%rcx, (%r8,%rdx,8)
	movq	(%r12), %r13
.L1110:
	movq	%r13, (%rcx)
	movq	16(%r12), %r14
	testq	%r14, %r14
	je	.L1112
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15AsyncCompileJobD1Ev@PLT
	movl	$320, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1112:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	%r13, %rax
	subq	$1, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1122:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L1113
	movq	8(%r13), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %rdi
	je	.L1110
	movq	%rcx, (%r8,%rdx,8)
	addq	(%rbx), %r10
	movq	(%r10), %rax
	movq	%r10, %r9
.L1109:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L1123
.L1111:
	movq	$0, (%r9)
	movq	(%r12), %r13
	jmp	.L1110
.L1113:
	movq	%rcx, %rax
	jmp	.L1109
.L1123:
	movq	%r13, 16(%rbx)
	jmp	.L1111
	.cfi_endproc
.LFE24437:
	.size	_ZNSt10_HashtableIPN2v88internal4wasm15AsyncCompileJobESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSD_20_Node_const_iteratorISB_Lb0ELb0EEE, .-_ZNSt10_HashtableIPN2v88internal4wasm15AsyncCompileJobESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSD_20_Node_const_iteratorISB_Lb0ELb0EEE
	.section	.text.unlikely._ZN2v88internal4wasm10WasmEngine16RemoveCompileJobEPNS1_15AsyncCompileJobE,"ax",@progbits
	.align 2
.LCOLDB13:
	.section	.text._ZN2v88internal4wasm10WasmEngine16RemoveCompileJobEPNS1_15AsyncCompileJobE,"ax",@progbits
.LHOTB13:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine16RemoveCompileJobEPNS1_15AsyncCompileJobE
	.type	_ZN2v88internal4wasm10WasmEngine16RemoveCompileJobEPNS1_15AsyncCompileJobE, @function
_ZN2v88internal4wasm10WasmEngine16RemoveCompileJobEPNS1_15AsyncCompileJobE:
.LFB19208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	592(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	640(%rbx), %rcx
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	632(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L1125
	movq	(%rax), %rsi
	movq	%rdx, %r8
	movq	8(%rsi), %rdi
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1137:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L1125
	movq	8(%rsi), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r8
	jne	.L1136
.L1127:
	cmpq	%r12, %rdi
	jne	.L1137
	movq	16(%rsi), %rax
	movq	$0, 16(%rsi)
	leaq	632(%rbx), %rdi
	movq	%rax, 0(%r13)
	call	_ZNSt10_HashtableIPN2v88internal4wasm15AsyncCompileJobESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSD_20_Node_const_iteratorISB_Lb0ELb0EEE
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1136:
	.cfi_restore_state
	jmp	.L1125
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal4wasm10WasmEngine16RemoveCompileJobEPNS1_15AsyncCompileJobE
	.cfi_startproc
	.type	_ZN2v88internal4wasm10WasmEngine16RemoveCompileJobEPNS1_15AsyncCompileJobE.cold, @function
_ZN2v88internal4wasm10WasmEngine16RemoveCompileJobEPNS1_15AsyncCompileJobE.cold:
.LFSB19208:
.L1125:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	16, %rax
	ud2
	.cfi_endproc
.LFE19208:
	.section	.text._ZN2v88internal4wasm10WasmEngine16RemoveCompileJobEPNS1_15AsyncCompileJobE
	.size	_ZN2v88internal4wasm10WasmEngine16RemoveCompileJobEPNS1_15AsyncCompileJobE, .-_ZN2v88internal4wasm10WasmEngine16RemoveCompileJobEPNS1_15AsyncCompileJobE
	.section	.text.unlikely._ZN2v88internal4wasm10WasmEngine16RemoveCompileJobEPNS1_15AsyncCompileJobE
	.size	_ZN2v88internal4wasm10WasmEngine16RemoveCompileJobEPNS1_15AsyncCompileJobE.cold, .-_ZN2v88internal4wasm10WasmEngine16RemoveCompileJobEPNS1_15AsyncCompileJobE.cold
.LCOLDE13:
	.section	.text._ZN2v88internal4wasm10WasmEngine16RemoveCompileJobEPNS1_15AsyncCompileJobE
.LHOTE13:
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v88internal4wasm15AsyncCompileJobESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v88internal4wasm15AsyncCompileJobESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88internal4wasm15AsyncCompileJobESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN2v88internal4wasm15AsyncCompileJobESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN2v88internal4wasm15AsyncCompileJobESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB24446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movabsq	$1152921504606846975, %rsi
	subq	$56, %rsp
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -88(%rbp)
	movq	%rax, -80(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L1162
	movq	%r15, %r9
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L1153
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1163
.L1140:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -64(%rbp)
	leaq	8(%rax), %rsi
	movq	%r13, -72(%rbp)
.L1152:
	movq	(%rdx), %rax
	movq	-64(%rbp), %rcx
	movq	$0, (%rdx)
	movq	%rax, (%rcx,%r9)
	cmpq	%r12, %r15
	je	.L1142
	movq	%rcx, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L1146:
	movq	(%r14), %rsi
	movq	$0, (%r14)
	movq	%rsi, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1143
	movq	%rdi, -56(%rbp)
	addq	$8, %r14
	addq	$8, %r13
	call	_ZN2v88internal4wasm15AsyncCompileJobD1Ev@PLT
	movq	-56(%rbp), %rdi
	movl	$320, %esi
	call	_ZdlPvm@PLT
	cmpq	%r14, %r15
	jne	.L1146
.L1144:
	movq	-64(%rbp), %rcx
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rcx,%rax), %rsi
.L1142:
	movq	-80(%rbp), %rax
	cmpq	%rax, %r15
	je	.L1147
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L1155
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1149:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1149
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rsi,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L1150
.L1148:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L1150:
	leaq	8(%rsi,%r9), %rsi
.L1147:
	testq	%r12, %r12
	je	.L1151
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rsi
.L1151:
	movq	-64(%rbp), %xmm0
	movq	-88(%rbp), %rax
	movq	%rsi, %xmm2
	movq	-72(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1143:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L1146
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1163:
	testq	%rdi, %rdi
	jne	.L1141
	movq	$0, -72(%rbp)
	movl	$8, %esi
	movq	$0, -64(%rbp)
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1153:
	movl	$8, %r13d
	jmp	.L1140
.L1155:
	movq	%rsi, %rdx
	jmp	.L1148
.L1141:
	cmpq	%rsi, %rdi
	movq	%rsi, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L1140
.L1162:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24446:
	.size	_ZNSt6vectorISt10unique_ptrIN2v88internal4wasm15AsyncCompileJobESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN2v88internal4wasm15AsyncCompileJobESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZN2v88internal4wasm10WasmEngine26DeleteCompileJobsOnIsolateEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine26DeleteCompileJobsOnIsolateEPNS0_7IsolateE
	.type	_ZN2v88internal4wasm10WasmEngine26DeleteCompileJobsOnIsolateEPNS0_7IsolateE, @function
_ZN2v88internal4wasm10WasmEngine26DeleteCompileJobsOnIsolateEPNS0_7IsolateE:
.LFB19220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	592(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	addq	$632, %r13
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%r13), %r12
	testq	%r12, %r12
	jne	.L1165
	jmp	.L1166
	.p2align 4,,10
	.p2align 3
.L1186:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L1166
.L1165:
	movq	8(%r12), %rax
	cmpq	(%rax), %rbx
	jne	.L1186
	movq	-56(%rbp), %rsi
	cmpq	-48(%rbp), %rsi
	je	.L1169
	movq	16(%r12), %rax
	movq	$0, 16(%r12)
	movq	%rax, (%rsi)
	addq	$8, -56(%rbp)
.L1170:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNSt10_HashtableIPN2v88internal4wasm15AsyncCompileJobESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSD_20_Node_const_iteratorISB_Lb0ELb0EEE
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.L1165
.L1166:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rbx
	movq	-64(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1171
	.p2align 4,,10
	.p2align 3
.L1175:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L1172
	movq	%r13, %rdi
	addq	$8, %r12
	call	_ZN2v88internal4wasm15AsyncCompileJobD1Ev@PLT
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	cmpq	%r12, %rbx
	jne	.L1175
.L1173:
	movq	-64(%rbp), %r12
.L1171:
	testq	%r12, %r12
	je	.L1164
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1164:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1187
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1172:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1175
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1169:
	leaq	16(%r12), %rdx
	leaq	-64(%rbp), %rdi
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal4wasm15AsyncCompileJobESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L1170
.L1187:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19220:
	.size	_ZN2v88internal4wasm10WasmEngine26DeleteCompileJobsOnIsolateEPNS0_7IsolateE, .-_ZN2v88internal4wasm10WasmEngine26DeleteCompileJobsOnIsolateEPNS0_7IsolateE
	.section	.text._ZN2v88internal4wasm10WasmEngine26DeleteCompileJobsOnContextENS0_6HandleINS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine26DeleteCompileJobsOnContextENS0_6HandleINS0_7ContextEEE
	.type	_ZN2v88internal4wasm10WasmEngine26DeleteCompileJobsOnContextENS0_6HandleINS0_7ContextEEE, @function
_ZN2v88internal4wasm10WasmEngine26DeleteCompileJobsOnContextENS0_6HandleINS0_7ContextEEE:
.LFB19210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	592(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	648(%r13), %r12
	testq	%r12, %r12
	je	.L1189
	addq	$632, %r13
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1191:
	movq	-56(%rbp), %rsi
	cmpq	-48(%rbp), %rsi
	je	.L1194
	movq	16(%r12), %rax
	movq	$0, 16(%r12)
	movq	%rax, (%rsi)
	addq	$8, -56(%rbp)
.L1195:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNSt10_HashtableIPN2v88internal4wasm15AsyncCompileJobESt4pairIKS4_St10unique_ptrIS3_St14default_deleteIS3_EEESaISB_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSD_18_Mod_range_hashingENSD_20_Default_ranged_hashENSD_20_Prime_rehash_policyENSD_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSD_20_Node_const_iteratorISB_Lb0ELb0EEE
	movq	%rax, %r12
	testq	%r12, %r12
	je	.L1189
.L1190:
	movq	8(%r12), %rax
	movq	56(%rax), %rax
	cmpq	%rbx, %rax
	je	.L1191
	testq	%rax, %rax
	je	.L1192
	testq	%rbx, %rbx
	je	.L1192
	movq	(%rax), %rax
	cmpq	%rax, (%rbx)
	je	.L1191
.L1192:
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L1190
.L1189:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rbx
	movq	-64(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1196
	.p2align 4,,10
	.p2align 3
.L1200:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L1197
	movq	%r13, %rdi
	addq	$8, %r12
	call	_ZN2v88internal4wasm15AsyncCompileJobD1Ev@PLT
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	cmpq	%r12, %rbx
	jne	.L1200
.L1198:
	movq	-64(%rbp), %r12
.L1196:
	testq	%r12, %r12
	je	.L1188
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1188:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1213
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1197:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1200
	jmp	.L1198
	.p2align 4,,10
	.p2align 3
.L1194:
	leaq	16(%r12), %rdx
	leaq	-64(%rbp), %rdi
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal4wasm15AsyncCompileJobESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L1195
.L1213:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19210:
	.size	_ZN2v88internal4wasm10WasmEngine26DeleteCompileJobsOnContextENS0_6HandleINS0_7ContextEEE, .-_ZN2v88internal4wasm10WasmEngine26DeleteCompileJobsOnContextENS0_6HandleINS0_7ContextEEE
	.section	.text._ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_St10unique_ptrINS1_4wasm10WasmEngine11IsolateInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSF_20_Node_const_iteratorISD_Lb0ELb0EEE,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_St10unique_ptrINS1_4wasm10WasmEngine11IsolateInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSF_20_Node_const_iteratorISD_Lb0ELb0EEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_St10unique_ptrINS1_4wasm10WasmEngine11IsolateInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSF_20_Node_const_iteratorISD_Lb0ELb0EEE
	.type	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_St10unique_ptrINS1_4wasm10WasmEngine11IsolateInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSF_20_Node_const_iteratorISD_Lb0ELb0EEE, @function
_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_St10unique_ptrINS1_4wasm10WasmEngine11IsolateInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSF_20_Node_const_iteratorISD_Lb0ELb0EEE:
.LFB24486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rsi
	movq	8(%r12), %rax
	movq	(%rbx), %r8
	divq	%rsi
	leaq	0(,%rdx,8), %r10
	movq	%rdx, %rdi
	leaq	(%r8,%r10), %r9
	movq	(%r9), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1215:
	movq	%rdx, %rcx
	movq	(%rdx), %rdx
	cmpq	%rdx, %r12
	jne	.L1215
	movq	(%r12), %r14
	cmpq	%rcx, %rax
	je	.L1254
	testq	%r14, %r14
	je	.L1218
	movq	8(%r14), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %rdi
	je	.L1218
	movq	%rcx, (%r8,%rdx,8)
	movq	(%r12), %r14
.L1218:
	movq	%r14, (%rcx)
	movq	16(%r12), %r15
	testq	%r15, %r15
	je	.L1220
	movq	112(%r15), %r13
	testq	%r13, %r13
	je	.L1222
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L1223
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r13)
.L1224:
	cmpl	$1, %edx
	jne	.L1222
	movq	0(%r13), %rdx
	movq	%rax, -56(%rbp)
	movq	%r13, %rdi
	call	*16(%rdx)
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L1226
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1227:
	cmpl	$1, %eax
	jne	.L1222
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L1222:
	movq	96(%r15), %r13
	testq	%r13, %r13
	je	.L1229
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L1230
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r13)
.L1231:
	cmpl	$1, %edx
	jne	.L1229
	movq	0(%r13), %rdx
	movq	%rax, -56(%rbp)
	movq	%r13, %rdi
	call	*16(%rdx)
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L1233
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1234:
	cmpl	$1, %eax
	jne	.L1229
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L1229:
	movq	64(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1235
	call	_ZdlPv@PLT
.L1235:
	movq	16(%r15), %r13
	testq	%r13, %r13
	je	.L1237
.L1236:
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal4wasm12NativeModuleES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1236
.L1237:
	movl	$120, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1220:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1254:
	.cfi_restore_state
	testq	%r14, %r14
	je	.L1238
	movq	8(%r14), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %rdi
	je	.L1218
	movq	%rcx, (%r8,%rdx,8)
	addq	(%rbx), %r10
	movq	(%r10), %rax
	movq	%r10, %r9
.L1217:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L1255
.L1219:
	movq	$0, (%r9)
	movq	(%r12), %r14
	jmp	.L1218
.L1238:
	movq	%rcx, %rax
	jmp	.L1217
.L1230:
	movl	8(%r13), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r13)
	jmp	.L1231
.L1223:
	movl	8(%r13), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r13)
	jmp	.L1224
.L1233:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1234
.L1226:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1227
.L1255:
	movq	%r14, 16(%rbx)
	jmp	.L1219
	.cfi_endproc
.LFE24486:
	.size	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_St10unique_ptrINS1_4wasm10WasmEngine11IsolateInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSF_20_Node_const_iteratorISD_Lb0ELb0EEE, .-_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_St10unique_ptrINS1_4wasm10WasmEngine11IsolateInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSF_20_Node_const_iteratorISD_Lb0ELb0EEE
	.section	.text._ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSF_20_Node_const_iteratorISD_Lb0ELb0EEE,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSF_20_Node_const_iteratorISD_Lb0ELb0EEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSF_20_Node_const_iteratorISD_Lb0ELb0EEE
	.type	_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSF_20_Node_const_iteratorISD_Lb0ELb0EEE, @function
_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSF_20_Node_const_iteratorISD_Lb0ELb0EEE:
.LFB24609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	8(%r12), %rax
	movq	0(%r13), %r8
	divq	%rsi
	leaq	0(,%rdx,8), %r10
	movq	%rdx, %rdi
	leaq	(%r8,%r10), %r9
	movq	(%r9), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1257:
	movq	%rdx, %rcx
	movq	(%rdx), %rdx
	cmpq	%rdx, %r12
	jne	.L1257
	movq	(%r12), %r14
	cmpq	%rcx, %rax
	je	.L1299
	testq	%r14, %r14
	je	.L1260
	movq	8(%r14), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %rdi
	je	.L1260
	movq	%rcx, (%r8,%rdx,8)
	movq	(%r12), %r14
.L1260:
	movq	%r14, (%rcx)
	movq	16(%r12), %r15
	testq	%r15, %r15
	je	.L1262
	movq	128(%r15), %rbx
	testq	%rbx, %rbx
	je	.L1266
	.p2align 4,,10
	.p2align 3
.L1263:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1263
.L1266:
	movq	120(%r15), %rax
	movq	112(%r15), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	112(%r15), %rdi
	leaq	160(%r15), %rax
	movq	$0, 136(%r15)
	movq	$0, 128(%r15)
	cmpq	%rax, %rdi
	je	.L1264
	call	_ZdlPv@PLT
.L1264:
	movq	72(%r15), %rbx
	testq	%rbx, %rbx
	je	.L1270
	.p2align 4,,10
	.p2align 3
.L1267:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1267
.L1270:
	movq	64(%r15), %rax
	movq	56(%r15), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	56(%r15), %rdi
	leaq	104(%r15), %rax
	movq	$0, 80(%r15)
	movq	$0, 72(%r15)
	cmpq	%rax, %rdi
	je	.L1268
	call	_ZdlPv@PLT
.L1268:
	movq	16(%r15), %rbx
	testq	%rbx, %rbx
	je	.L1274
	.p2align 4,,10
	.p2align 3
.L1271:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1271
.L1274:
	movq	8(%r15), %rax
	movq	(%r15), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	(%r15), %rdi
	leaq	48(%r15), %rax
	movq	$0, 24(%r15)
	movq	$0, 16(%r15)
	cmpq	%rax, %rdi
	je	.L1272
	call	_ZdlPv@PLT
.L1272:
	movl	$176, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1262:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 24(%r13)
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1299:
	.cfi_restore_state
	testq	%r14, %r14
	je	.L1275
	movq	8(%r14), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %rdi
	je	.L1260
	movq	%rcx, (%r8,%rdx,8)
	addq	0(%r13), %r10
	movq	(%r10), %rax
	movq	%r10, %r9
.L1259:
	leaq	16(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L1300
.L1261:
	movq	$0, (%r9)
	movq	(%r12), %r14
	jmp	.L1260
.L1275:
	movq	%rcx, %rax
	jmp	.L1259
.L1300:
	movq	%r14, 16(%r13)
	jmp	.L1261
	.cfi_endproc
.LFE24609:
	.size	_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSF_20_Node_const_iteratorISD_Lb0ELb0EEE, .-_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSF_20_Node_const_iteratorISD_Lb0ELb0EEE
	.section	.text._ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St6vectorIPNS2_8WasmCodeESaIS9_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSE_10_Hash_nodeISC_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St6vectorIPNS2_8WasmCodeESaIS9_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSE_10_Hash_nodeISC_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St6vectorIPNS2_8WasmCodeESaIS9_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSE_10_Hash_nodeISC_Lb0EEEm
	.type	_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St6vectorIPNS2_8WasmCodeESaIS9_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSE_10_Hash_nodeISC_Lb0EEEm, @function
_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St6vectorIPNS2_8WasmCodeESaIS9_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSE_10_Hash_nodeISC_Lb0EEEm:
.LFB24789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L1302
	movq	(%rbx), %r8
.L1303:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L1312
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L1313:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1302:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L1326
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1327
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L1305:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1307
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1309:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1310:
	testq	%rsi, %rsi
	je	.L1307
.L1308:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L1309
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L1315
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1308
	.p2align 4,,10
	.p2align 3
.L1307:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L1311
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1311:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1312:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L1314
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L1314:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L1313
	.p2align 4,,10
	.p2align 3
.L1315:
	movq	%rdx, %rdi
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1326:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L1305
.L1327:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE24789:
	.size	_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St6vectorIPNS2_8WasmCodeESaIS9_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSE_10_Hash_nodeISC_Lb0EEEm, .-_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St6vectorIPNS2_8WasmCodeESaIS9_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSE_10_Hash_nodeISC_Lb0EEEm
	.section	.text._ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.type	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, @function
_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm:
.LFB25072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L1329
	movq	(%rbx), %r8
.L1330:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L1339
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L1340:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1329:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L1353
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1354
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L1332:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1334
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L1335
	.p2align 4,,10
	.p2align 3
.L1336:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1337:
	testq	%rsi, %rsi
	je	.L1334
.L1335:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L1336
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L1342
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1335
	.p2align 4,,10
	.p2align 3
.L1334:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L1338
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1338:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1339:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L1341
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L1341:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L1340
	.p2align 4,,10
	.p2align 3
.L1342:
	movq	%rdx, %rdi
	jmp	.L1337
	.p2align 4,,10
	.p2align 3
.L1353:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L1332
.L1354:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE25072:
	.size	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, .-_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.section	.text._ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb0EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb0EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb0EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.type	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb0EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm, @function
_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb0EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm:
.LFB24331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %r13
	movq	8(%rdi), %r9
	movq	%r13, %rax
	divq	%r9
	movq	(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L1356
	movq	(%rax), %rdi
	movq	8(%rdi), %r10
	jmp	.L1358
	.p2align 4,,10
	.p2align 3
.L1369:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1356
	movq	8(%rdi), %r10
	xorl	%edx, %edx
	movq	%r10, %rax
	divq	%r9
	cmpq	%rdx, %r14
	jne	.L1356
.L1358:
	cmpq	%r10, %r13
	jne	.L1369
	addq	$8, %rsp
	movq	%rdi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1356:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%r13, %rdx
	movq	%r15, %r8
	movq	%r14, %rsi
	movq	%rax, %r9
	movq	$0, (%rax)
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	%r9, %rcx
	movq	%rax, 8(%r9)
	call	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	addq	$8, %rsp
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24331:
	.size	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb0EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm, .-_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb0EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.section	.text._ZN2v88internal4wasm10WasmEngine18ImportNativeModuleEPNS0_7IsolateESt10shared_ptrINS1_12NativeModuleEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine18ImportNativeModuleEPNS0_7IsolateESt10shared_ptrINS1_12NativeModuleEE
	.type	_ZN2v88internal4wasm10WasmEngine18ImportNativeModuleEPNS0_7IsolateESt10shared_ptrINS1_12NativeModuleEE, @function
_ZN2v88internal4wasm10WasmEngine18ImportNativeModuleEPNS0_7IsolateESt10shared_ptrINS1_12NativeModuleEE:
.LFB19173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%rsi, -120(%rbp)
	movq	-120(%rbp), %rdi
	leaq	-96(%rbp), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	232(%rax), %rdx
	movq	%rax, -112(%rbp)
	movq	8(%rdx), %rcx
	movq	(%rdx), %rdx
	movq	%rdx, -96(%rbp)
	movq	208(%rax), %rdx
	movq	%rcx, -88(%rbp)
	addq	$408, %rdx
	call	_ZN2v88internal4wasm16CreateWasmScriptEPNS0_7IsolateERKNS1_15ModuleWireBytesERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-120(%rbp), %rdi
	leaq	-104(%rbp), %rdx
	movq	$0, -104(%rbp)
	movq	%rax, %r12
	movq	-112(%rbp), %rax
	movq	208(%rax), %rsi
	call	_ZN2v88internal4wasm23CompileJsToWasmWrappersEPNS0_7IsolateEPKNS1_10WasmModuleEPNS0_6HandleINS0_10FixedArrayEEE@PLT
	movq	-112(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	144(%rax), %r8
	movdqu	0(%r13), %xmm1
	movq	-104(%rbp), %rcx
	movups	%xmm0, 0(%r13)
	movq	-120(%rbp), %rdi
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEENS8_INS0_10FixedArrayEEEm@PLT
	movq	-72(%rbp), %r15
	movq	%rax, -128(%rbp)
	testq	%r15, %r15
	je	.L1372
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L1373
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
	cmpl	$1, %eax
	je	.L1395
	.p2align 4,,10
	.p2align 3
.L1372:
	leaq	592(%rbx), %r13
	leaq	-120(%rbp), %r15
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	704(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZNSt8__detail9_Map_baseIPN2v88internal7IsolateESt4pairIKS4_St10unique_ptrINS2_4wasm10WasmEngine11IsolateInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	movq	(%rax), %r8
	movq	16(%r8), %rdx
	leaq	8(%r8), %r10
	testq	%rdx, %rdx
	je	.L1378
	movq	-112(%rbp), %rcx
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1396:
	movq	16(%rdx), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L1380
.L1397:
	movq	%rax, %rdx
.L1379:
	movq	32(%rdx), %rdi
	cmpq	%rdi, %rcx
	jb	.L1396
	movq	24(%rdx), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L1397
.L1380:
	testb	%sil, %sil
	jne	.L1398
	cmpq	%rdi, %rcx
	ja	.L1386
.L1387:
	leaq	-112(%rbp), %rsi
	leaq	760(%rbx), %rdi
	call	_ZNSt8__detail9_Map_baseIPN2v88internal4wasm12NativeModuleESt4pairIKS5_St10unique_ptrINS3_10WasmEngine16NativeModuleInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS5_ESt4hashIS5_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS7_
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	(%rax), %rdi
	movq	%rdi, -80(%rbp)
	call	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb0EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-120(%rbp), %rax
	movq	%r12, %rsi
	movq	41472(%rax), %rdi
	call	_ZN2v88internal5Debug14OnAfterCompileENS0_6HandleINS0_6ScriptEEE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1399
	movq	-128(%rbp), %rax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1398:
	.cfi_restore_state
	cmpq	%rdx, 24(%r8)
	je	.L1386
.L1388:
	movq	%rdx, %rdi
	movq	%rcx, -160(%rbp)
	movq	%r10, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-160(%rbp), %rcx
	movq	-136(%rbp), %rdx
	cmpq	32(%rax), %rcx
	movq	-144(%rbp), %r8
	movq	-152(%rbp), %r10
	jbe	.L1387
.L1386:
	movl	$1, %r9d
	cmpq	%r10, %rdx
	jne	.L1400
.L1385:
	movl	$40, %edi
	movq	%r8, -136(%rbp)
	movl	%r9d, -160(%rbp)
	movq	%r10, -152(%rbp)
	movq	%rdx, -144(%rbp)
	call	_Znwm@PLT
	movq	-152(%rbp), %r10
	movl	-160(%rbp), %r9d
	movq	%rax, %rsi
	movq	-112(%rbp), %rax
	movq	-144(%rbp), %rdx
	movq	%r10, %rcx
	movl	%r9d, %edi
	movq	%rax, 32(%rsi)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	movq	-136(%rbp), %r8
	addq	$1, 40(%r8)
	jmp	.L1387
	.p2align 4,,10
	.p2align 3
.L1373:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	cmpl	$1, %eax
	jne	.L1372
.L1395:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L1376
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L1377:
	cmpl	$1, %eax
	jne	.L1372
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1400:
	xorl	%r9d, %r9d
	cmpq	32(%rdx), %rcx
	setb	%r9b
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1378:
	movq	%r10, %rdx
	cmpq	%r10, 24(%r8)
	je	.L1391
	movq	-112(%rbp), %rcx
	jmp	.L1388
	.p2align 4,,10
	.p2align 3
.L1376:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1391:
	movl	$1, %r9d
	jmp	.L1385
.L1399:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19173:
	.size	_ZN2v88internal4wasm10WasmEngine18ImportNativeModuleEPNS0_7IsolateESt10shared_ptrINS1_12NativeModuleEE, .-_ZN2v88internal4wasm10WasmEngine18ImportNativeModuleEPNS0_7IsolateESt10shared_ptrINS1_12NativeModuleEE
	.section	.text._ZN2v88internal4wasm10WasmEngine15NewNativeModuleEPNS0_7IsolateERKNS1_12WasmFeaturesEmbSt10shared_ptrIKNS1_10WasmModuleEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine15NewNativeModuleEPNS0_7IsolateERKNS1_12WasmFeaturesEmbSt10shared_ptrIKNS1_10WasmModuleEE
	.type	_ZN2v88internal4wasm10WasmEngine15NewNativeModuleEPNS0_7IsolateERKNS1_12WasmFeaturesEmbSt10shared_ptrIKNS1_10WasmModuleEE, @function
_ZN2v88internal4wasm10WasmEngine15NewNativeModuleEPNS0_7IsolateERKNS1_12WasmFeaturesEmbSt10shared_ptrIKNS1_10WasmModuleEE:
.LFB19238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rcx, %r10
	pxor	%xmm0, %xmm0
	movzbl	%r9b, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	addq	$280, %rsi
	subq	$104, %rsp
	movq	%rdx, -88(%rbp)
	movq	16(%rbp), %rdx
	movdqu	(%rdx), %xmm1
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, %rcx
	leaq	-80(%rbp), %rax
	movups	%xmm0, (%rdx)
	pushq	%rax
	movq	%rbx, %rdx
	pushq	%r9
	movq	%r8, %r9
	movq	%r10, %r8
	movq	%rax, -104(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal4wasm15WasmCodeManager15NewNativeModuleEPNS1_10WasmEngineEPNS0_7IsolateERKNS1_12WasmFeaturesEmbSt10shared_ptrIKNS1_10WasmModuleEE@PLT
	movq	-72(%rbp), %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L1403
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L1404
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L1470
	.p2align 4,,10
	.p2align 3
.L1403:
	leaq	592(%rbx), %rax
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	$176, %edi
	call	_Znwm@PLT
	movl	$22, %ecx
	movss	.LC4(%rip), %xmm0
	movq	(%r14), %r9
	movq	%rax, %r15
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r9, -112(%rbp)
	rep stosq
	leaq	48(%r15), %rax
	movl	$24, %edi
	movq	$1, 8(%r15)
	movq	%rax, -120(%rbp)
	movq	%rax, (%r15)
	leaq	104(%r15), %rax
	movq	%rax, -128(%rbp)
	movq	%rax, 56(%r15)
	leaq	160(%r15), %rax
	movq	$1, 64(%r15)
	movq	%rax, 112(%r15)
	movq	$1, 120(%r15)
	movss	%xmm0, 32(%r15)
	movss	%xmm0, 88(%r15)
	movss	%xmm0, 144(%r15)
	movq	%rax, -136(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %r9
	movq	768(%rbx), %rcx
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %r13
	movq	%r9, 8(%rax)
	movq	%r15, 16(%rax)
	movq	%r9, %rax
	divq	%rcx
	movq	760(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L1409
	movq	(%rax), %r12
	movq	8(%r12), %rsi
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L1471:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L1409
	movq	8(%r12), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r10
	jne	.L1409
.L1411:
	cmpq	%rsi, %r9
	jne	.L1471
	movq	128(%r15), %rax
	testq	%rax, %rax
	je	.L1414
	movq	%rbx, -112(%rbp)
	movq	%rax, %rbx
.L1415:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1415
	movq	-112(%rbp), %rbx
.L1414:
	movq	120(%r15), %rax
	movq	112(%r15), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	112(%r15), %rdi
	movq	$0, 136(%r15)
	movq	$0, 128(%r15)
	cmpq	%rdi, -136(%rbp)
	je	.L1412
	call	_ZdlPv@PLT
.L1412:
	movq	72(%r15), %rax
	testq	%rax, %rax
	je	.L1419
	movq	%rbx, -112(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1416:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1416
	movq	-112(%rbp), %rbx
.L1419:
	movq	64(%r15), %rax
	movq	56(%r15), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, 80(%r15)
	movq	56(%r15), %rdi
	movq	$0, 72(%r15)
	cmpq	%rdi, -128(%rbp)
	je	.L1417
	call	_ZdlPv@PLT
.L1417:
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.L1423
	movq	%rbx, -112(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1420:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1420
	movq	-112(%rbp), %rbx
.L1423:
	movq	8(%r15), %rax
	movq	(%r15), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, 24(%r15)
	movq	(%r15), %rdi
	movq	$0, 16(%r15)
	cmpq	%rdi, -120(%rbp)
	je	.L1421
	call	_ZdlPv@PLT
.L1421:
	movq	%r15, %rdi
	movl	$176, %esi
	call	_ZdlPvm@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L1404:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L1403
.L1470:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L1407
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1408:
	cmpl	$1, %eax
	jne	.L1403
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1403
	.p2align 4,,10
	.p2align 3
.L1409:
	leaq	760(%rbx), %rdi
	movq	%r13, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	call	_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm
	movq	%rax, %r12
.L1424:
	movq	16(%r12), %rdi
	movq	-104(%rbp), %rdx
	leaq	-88(%rbp), %r12
	movl	$1, %ecx
	movq	%r12, %rsi
	movq	%rdi, -80(%rbp)
	call	_ZNSt10_HashtableIPN2v88internal7IsolateES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb0EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	leaq	704(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIPN2v88internal7IsolateESt4pairIKS4_St10unique_ptrINS2_4wasm10WasmEngine11IsolateInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	movq	(%r14), %r13
	movq	(%rax), %rbx
	movq	16(%rbx), %r12
	leaq	8(%rbx), %r8
	testq	%r12, %r12
	jne	.L1426
	jmp	.L1472
	.p2align 4,,10
	.p2align 3
.L1473:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L1427
.L1474:
	movq	%rax, %r12
.L1426:
	movq	32(%r12), %rcx
	cmpq	%r13, %rcx
	ja	.L1473
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L1474
.L1427:
	testb	%dl, %dl
	jne	.L1475
	cmpq	%r13, %rcx
	jnb	.L1434
.L1433:
	movl	$1, %r15d
	cmpq	%r12, %r8
	jne	.L1476
.L1432:
	movl	$40, %edi
	movq	%r8, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %r8
	movq	%r12, %rdx
	movl	%r15d, %edi
	movq	%r13, 32(%rax)
	movq	%rax, %rsi
	movq	%r8, %rcx
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
.L1434:
	movq	-96(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1477
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1475:
	.cfi_restore_state
	cmpq	%r12, 24(%rbx)
	je	.L1433
.L1435:
	movq	%r12, %rdi
	movq	%r8, -104(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-104(%rbp), %r8
	cmpq	32(%rax), %r13
	jbe	.L1434
	movl	$1, %r15d
	cmpq	%r12, %r8
	je	.L1432
.L1476:
	xorl	%r15d, %r15d
	cmpq	32(%r12), %r13
	setb	%r15b
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1472:
	movq	%r8, %r12
	cmpq	24(%rbx), %r8
	jne	.L1435
	movl	$1, %r15d
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1407:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1408
.L1477:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19238:
	.size	_ZN2v88internal4wasm10WasmEngine15NewNativeModuleEPNS0_7IsolateERKNS1_12WasmFeaturesEmbSt10shared_ptrIKNS1_10WasmModuleEE, .-_ZN2v88internal4wasm10WasmEngine15NewNativeModuleEPNS0_7IsolateERKNS1_12WasmFeaturesEmbSt10shared_ptrIKNS1_10WasmModuleEE
	.section	.text._ZN2v88internal4wasm10WasmEngine15NewNativeModuleEPNS0_7IsolateERKNS1_12WasmFeaturesESt10shared_ptrIKNS1_10WasmModuleEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine15NewNativeModuleEPNS0_7IsolateERKNS1_12WasmFeaturesESt10shared_ptrIKNS1_10WasmModuleEE
	.type	_ZN2v88internal4wasm10WasmEngine15NewNativeModuleEPNS0_7IsolateERKNS1_12WasmFeaturesESt10shared_ptrIKNS1_10WasmModuleEE, @function
_ZN2v88internal4wasm10WasmEngine15NewNativeModuleEPNS0_7IsolateERKNS1_12WasmFeaturesESt10shared_ptrIKNS1_10WasmModuleEE:
.LFB19236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	movq	(%r8), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm15WasmCodeManager28EstimateNativeModuleCodeSizeEPKNS1_10WasmModuleE@PLT
	subq	$8, %rsp
	pxor	%xmm0, %xmm0
	movq	%r14, %rdx
	movq	%rax, %r8
	leaq	-80(%rbp), %rax
	movdqu	(%rbx), %xmm1
	movups	%xmm0, (%rbx)
	pushq	%rax
	movq	%r13, %rsi
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	movq	%r12, %rdi
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal4wasm10WasmEngine15NewNativeModuleEPNS0_7IsolateERKNS1_12WasmFeaturesEmbSt10shared_ptrIKNS1_10WasmModuleEE
	movq	-72(%rbp), %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L1478
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1481
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1488
	.p2align 4,,10
	.p2align 3
.L1478:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1489
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1481:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1478
.L1488:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1484
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1485:
	cmpl	$1, %eax
	jne	.L1478
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1484:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1485
.L1489:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19236:
	.size	_ZN2v88internal4wasm10WasmEngine15NewNativeModuleEPNS0_7IsolateERKNS1_12WasmFeaturesESt10shared_ptrIKNS1_10WasmModuleEE, .-_ZN2v88internal4wasm10WasmEngine15NewNativeModuleEPNS0_7IsolateERKNS1_12WasmFeaturesESt10shared_ptrIKNS1_10WasmModuleEE
	.section	.text._ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE
	.type	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE, @function
_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE:
.LFB25181:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	0(,%rsi,8), %r9
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rcx, %rdi
	movq	(%rbx), %rcx
	movq	(%rdi), %r12
	leaq	(%rcx,%r9), %rax
	cmpq	%rdx, (%rax)
	je	.L1500
	testq	%r12, %r12
	je	.L1493
	movq	8(%r12), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	cmpq	%rdx, %rsi
	je	.L1493
	movq	%r8, (%rcx,%rdx,8)
	movq	(%rdi), %r12
.L1493:
	movq	%r12, (%r8)
	call	_ZdlPv@PLT
	movq	%r12, %rax
	subq	$1, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1500:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L1492
	movq	8(%r12), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	cmpq	%rdx, %rsi
	je	.L1493
	movq	%r8, (%rcx,%rdx,8)
	movq	(%rbx), %rax
	addq	%r9, %rax
	movq	(%rax), %rdx
.L1492:
	leaq	16(%rbx), %rcx
	cmpq	%rcx, %rdx
	je	.L1501
.L1494:
	movq	$0, (%rax)
	movq	(%rdi), %r12
	jmp	.L1493
	.p2align 4,,10
	.p2align 3
.L1501:
	movq	%r12, 16(%rbx)
	jmp	.L1494
	.cfi_endproc
.LFE25181:
	.size	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE, .-_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE
	.section	.rodata._ZN2v88internal4wasm10WasmEngine16FreeNativeModuleEPNS1_12NativeModuleE.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"[wasm-gc] Native module %p died, reducing dead code objects to %zu.\n"
	.section	.text.unlikely._ZN2v88internal4wasm10WasmEngine16FreeNativeModuleEPNS1_12NativeModuleE,"ax",@progbits
	.align 2
.LCOLDB15:
	.section	.text._ZN2v88internal4wasm10WasmEngine16FreeNativeModuleEPNS1_12NativeModuleE,"ax",@progbits
.LHOTB15:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine16FreeNativeModuleEPNS1_12NativeModuleE
	.type	_ZN2v88internal4wasm10WasmEngine16FreeNativeModuleEPNS1_12NativeModuleE, @function
_ZN2v88internal4wasm10WasmEngine16FreeNativeModuleEPNS1_12NativeModuleE:
.LFB19250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%rdi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	592(%rdi), %rax
	movq	%rax, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	760(%rbx), %rax
	movq	768(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rax, -120(%rbp)
	movq	%r14, %rax
	divq	%rsi
	movq	760(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L1503
	movq	(%rax), %r8
	movq	%rdx, %rdi
	movq	8(%r8), %rcx
	jmp	.L1505
	.p2align 4,,10
	.p2align 3
.L1598:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L1503
	movq	8(%r8), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %rdi
	jne	.L1597
.L1505:
	cmpq	%r14, %rcx
	jne	.L1598
	movq	16(%r8), %rax
	movq	-104(%rbp), %rcx
	movq	%r8, -128(%rbp)
	movq	%r14, %r13
	addq	$704, %rcx
	movq	16(%rax), %rax
	movq	%rcx, -88(%rbp)
	leaq	-64(%rbp), %rcx
	movq	%rax, -72(%rbp)
	movq	%rcx, -96(%rbp)
	testq	%rax, %rax
	je	.L1544
	.p2align 4,,10
	.p2align 3
.L1534:
	movq	-72(%rbp), %rax
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZNSt8__detail9_Map_baseIPN2v88internal7IsolateESt4pairIKS4_St10unique_ptrINS2_4wasm10WasmEngine11IsolateInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	movq	(%rax), %r15
	movq	16(%r15), %r9
	leaq	8(%r15), %r14
	testq	%r9, %r9
	je	.L1506
	movq	%r14, %r12
	movq	%r9, %rbx
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1600:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1599
.L1507:
	cmpq	32(%rbx), %r13
	ja	.L1600
	movq	16(%rbx), %rax
	jnb	.L1601
	movq	%rbx, %r12
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.L1507
.L1599:
	cmpq	%r12, 24(%r15)
	jne	.L1523
	cmpq	%r12, %r14
	je	.L1596
	.p2align 4,,10
	.p2align 3
.L1523:
	movq	64(%r15), %rax
	movq	72(%r15), %rsi
	subq	%rax, %rsi
	sarq	$3, %rsi
	jne	.L1602
.L1528:
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	jne	.L1534
	movq	%r13, %r14
.L1544:
	movq	-104(%rbp), %rax
	movq	824(%rax), %rax
	testq	%rax, %rax
	je	.L1536
	movq	72(%rax), %rcx
	movq	-104(%rbp), %rbx
	testq	%rcx, %rcx
	jne	.L1537
	jmp	.L1538
	.p2align 4,,10
	.p2align 3
.L1539:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1538
.L1537:
	movq	8(%rcx), %rax
	cmpq	%r14, 48(%rax)
	jne	.L1539
	movq	824(%rbx), %rdi
	xorl	%edx, %edx
	divq	64(%rdi)
	movq	56(%rdi), %rax
	leaq	56(%rdi), %r8
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L1540:
	movq	%rax, %rdx
	movq	(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1540
	movq	%r8, %rdi
	call	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE
	movq	%rax, %rcx
	testq	%rcx, %rcx
	jne	.L1537
.L1538:
	cmpb	$0, _ZN2v88internal23FLAG_trace_wasm_code_gcE(%rip)
	jne	.L1603
.L1536:
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdi
	call	_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSF_20_Node_const_iteratorISD_Lb0ELb0EEE
	movq	-112(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1604
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1601:
	.cfi_restore_state
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L1514
	jmp	.L1520
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	%rsi, %r12
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L1520
.L1514:
	cmpq	%r13, 32(%rsi)
	ja	.L1605
	movq	24(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.L1514
	.p2align 4,,10
	.p2align 3
.L1520:
	testq	%rax, %rax
	je	.L1515
.L1606:
	cmpq	%r13, 32(%rax)
	jb	.L1519
	movq	%rax, %rbx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1606
.L1515:
	cmpq	%rbx, 24(%r15)
	jne	.L1521
	cmpq	%r12, %r14
	je	.L1596
.L1521:
	cmpq	%r12, %rbx
	je	.L1523
	movq	%r13, -80(%rbp)
	movq	%r15, %r13
	.p2align 4,,10
	.p2align 3
.L1525:
	movq	%rbx, %rdi
	movq	%rbx, %r15
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	subq	$1, 40(%r13)
	cmpq	%r12, %rbx
	jne	.L1525
	movq	%r13, %r15
	movq	-80(%rbp), %r13
	jmp	.L1523
	.p2align 4,,10
	.p2align 3
.L1519:
	movq	24(%rax), %rax
	jmp	.L1520
	.p2align 4,,10
	.p2align 3
.L1602:
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L1527:
	leaq	0(,%r9,8), %rdi
	.p2align 4,,10
	.p2align 3
.L1531:
	leaq	(%rax,%rdi), %rdx
	movq	(%rdx), %rcx
	cmpq	%r13, 48(%rcx)
	jne	.L1529
	subq	$1, %rsi
	movq	(%rax,%rsi,8), %rax
	movq	%rax, (%rdx)
	movq	64(%r15), %rax
	cmpq	%rsi, %r9
	jb	.L1531
.L1529:
	addq	$1, %r9
	cmpq	%rsi, %r9
	jb	.L1527
	movq	72(%r15), %rdi
	movq	%rdi, %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rsi, %rdx
	jb	.L1607
	jbe	.L1528
	leaq	(%rax,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L1528
	movq	%rax, 72(%r15)
	jmp	.L1528
.L1607:
	subq	%rdx, %rsi
	leaq	64(%r15), %rdi
	call	_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_default_appendEm
	jmp	.L1528
.L1603:
	movq	-104(%rbp), %rax
	movq	%r14, %rsi
	leaq	.LC14(%rip), %rdi
	movq	824(%rax), %rax
	movq	80(%rax), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1536
.L1506:
	cmpq	%r14, 24(%r15)
	jne	.L1523
.L1524:
	movq	$0, 16(%r15)
	movq	%r14, 24(%r15)
	movq	%r14, 32(%r15)
	movq	$0, 40(%r15)
	jmp	.L1523
.L1596:
	movq	%r9, %rbx
.L1522:
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal4wasm12NativeModuleES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1522
	jmp	.L1524
.L1604:
	call	__stack_chk_fail@PLT
.L1597:
	jmp	.L1503
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal4wasm10WasmEngine16FreeNativeModuleEPNS1_12NativeModuleE
	.cfi_startproc
	.type	_ZN2v88internal4wasm10WasmEngine16FreeNativeModuleEPNS1_12NativeModuleE.cold, @function
_ZN2v88internal4wasm10WasmEngine16FreeNativeModuleEPNS1_12NativeModuleE.cold:
.LFSB19250:
.L1503:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	16, %rax
	ud2
	.cfi_endproc
.LFE19250:
	.section	.text._ZN2v88internal4wasm10WasmEngine16FreeNativeModuleEPNS1_12NativeModuleE
	.size	_ZN2v88internal4wasm10WasmEngine16FreeNativeModuleEPNS1_12NativeModuleE, .-_ZN2v88internal4wasm10WasmEngine16FreeNativeModuleEPNS1_12NativeModuleE
	.section	.text.unlikely._ZN2v88internal4wasm10WasmEngine16FreeNativeModuleEPNS1_12NativeModuleE
	.size	_ZN2v88internal4wasm10WasmEngine16FreeNativeModuleEPNS1_12NativeModuleE.cold, .-_ZN2v88internal4wasm10WasmEngine16FreeNativeModuleEPNS1_12NativeModuleE.cold
.LCOLDE15:
	.section	.text._ZN2v88internal4wasm10WasmEngine16FreeNativeModuleEPNS1_12NativeModuleE
.LHOTE15:
	.section	.rodata._ZN2v88internal4wasm10WasmEngine18FreeDeadCodeLockedERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"s"
.LC17:
	.string	"disabled-by-default-v8.wasm"
.LC18:
	.string	"FreeDeadCode"
	.section	.rodata._ZN2v88internal4wasm10WasmEngine18FreeDeadCodeLockedERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEE.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"[wasm-gc] Freeing %zu code object%s of module %p.\n"
	.section	.text._ZN2v88internal4wasm10WasmEngine18FreeDeadCodeLockedERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine18FreeDeadCodeLockedERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEE
	.type	_ZN2v88internal4wasm10WasmEngine18FreeDeadCodeLockedERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEE, @function
_ZN2v88internal4wasm10WasmEngine18FreeDeadCodeLockedERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEE:
.LFB19286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal4wasm10WasmEngine18FreeDeadCodeLockedERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEEE28trace_event_unique_atomic850(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1654
	movq	$0, -112(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1655
.L1612:
	leaq	760(%r13), %rax
	movq	16(%r12), %r12
	movq	%rax, -136(%rbp)
	leaq	-120(%rbp), %rax
	movq	%rax, -144(%rbp)
	testq	%r12, %r12
	je	.L1625
	.p2align 4,,10
	.p2align 3
.L1624:
	movq	8(%r12), %rax
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	%rax, -120(%rbp)
	call	_ZNSt8__detail9_Map_baseIPN2v88internal4wasm12NativeModuleESt4pairIKS5_St10unique_ptrINS3_10WasmEngine16NativeModuleInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS5_ESt4hashIS5_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS7_
	cmpb	$0, _ZN2v88internal23FLAG_trace_wasm_code_gcE(%rip)
	movq	(%rax), %r14
	je	.L1617
	movq	24(%r12), %rax
	subq	16(%r12), %rax
	leaq	.LC16(%rip), %rdx
	leaq	.LC19(%rip), %rdi
	movq	%rax, %rsi
	movq	-120(%rbp), %rcx
	sarq	$3, %rsi
	cmpq	$8, %rax
	leaq	.LC3(%rip), %rax
	cmove	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L1617:
	movq	16(%r12), %rsi
	movq	24(%r12), %rbx
	cmpq	%rsi, %rbx
	je	.L1630
	leaq	112(%r14), %r13
	movq	%rsi, %r15
	.p2align 4,,10
	.p2align 3
.L1623:
	movq	(%r15), %r11
	movq	120(%r14), %rdi
	xorl	%edx, %edx
	movq	%r11, %rax
	divq	%rdi
	movq	112(%r14), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r10
	testq	%r8, %r8
	je	.L1620
	movq	(%r8), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1622
	.p2align 4,,10
	.p2align 3
.L1656:
	movq	(%rcx), %r9
	testq	%r9, %r9
	je	.L1620
	movq	8(%r9), %rsi
	xorl	%edx, %edx
	movq	%rcx, %r8
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L1620
	movq	%r9, %rcx
.L1622:
	cmpq	%rsi, %r11
	jne	.L1656
	movq	%r8, %rdx
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE
.L1620:
	addq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L1623
	movq	16(%r12), %rsi
	movq	24(%r12), %rdx
.L1619:
	subq	%rsi, %rdx
	movq	-120(%rbp), %rdi
	sarq	$3, %rdx
	call	_ZN2v88internal4wasm12NativeModule8FreeCodeENS0_6VectorIKPNS1_8WasmCodeEEE@PLT
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L1624
.L1625:
	leaq	-112(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1657
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1630:
	.cfi_restore_state
	movq	%rsi, %rdx
	jmp	.L1619
.L1654:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1658
.L1611:
	movq	%rbx, _ZZN2v88internal4wasm10WasmEngine18FreeDeadCodeLockedERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEEE28trace_event_unique_atomic850(%rip)
	movq	$0, -112(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	je	.L1612
.L1655:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1659
.L1613:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1614
	movq	(%rdi), %rax
	call	*8(%rax)
.L1614:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1615
	movq	(%rdi), %rax
	call	*8(%rax)
.L1615:
	leaq	.LC18(%rip), %rax
	movq	%rbx, -104(%rbp)
	movq	%rax, -96(%rbp)
	leaq	-104(%rbp), %rax
	movq	%r14, -88(%rbp)
	movq	%rax, -112(%rbp)
	jmp	.L1612
.L1658:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1611
.L1659:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC18(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1613
.L1657:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19286:
	.size	_ZN2v88internal4wasm10WasmEngine18FreeDeadCodeLockedERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEE, .-_ZN2v88internal4wasm10WasmEngine18FreeDeadCodeLockedERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEE
	.section	.text._ZN2v88internal4wasm10WasmEngine12FreeDeadCodeERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine12FreeDeadCodeERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEE
	.type	_ZN2v88internal4wasm10WasmEngine12FreeDeadCodeERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEE, @function
_ZN2v88internal4wasm10WasmEngine12FreeDeadCodeERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEE:
.LFB19285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	592(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm10WasmEngine18FreeDeadCodeLockedERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEE
	addq	$8, %rsp
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE19285:
	.size	_ZN2v88internal4wasm10WasmEngine12FreeDeadCodeERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEE, .-_ZN2v88internal4wasm10WasmEngine12FreeDeadCodeERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEE
	.section	.text._ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	.type	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm, @function
_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm:
.LFB25344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L1663
	movq	(%rbx), %r8
.L1664:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L1673
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L1674:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1663:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L1687
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1688
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L1666:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1668
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L1669
	.p2align 4,,10
	.p2align 3
.L1670:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1671:
	testq	%rsi, %rsi
	je	.L1668
.L1669:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L1670
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L1676
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1669
	.p2align 4,,10
	.p2align 3
.L1668:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L1672
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1672:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L1664
	.p2align 4,,10
	.p2align 3
.L1673:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L1675
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L1675:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L1674
	.p2align 4,,10
	.p2align 3
.L1676:
	movq	%rdx, %rdi
	jmp	.L1671
	.p2align 4,,10
	.p2align 3
.L1687:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L1666
.L1688:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE25344:
	.size	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm, .-_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	.section	.text._ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKS4_NS6_10_AllocNodeISaINS6_10_Hash_nodeIS4_Lb0EEEEEEEESt4pairINS6_14_Node_iteratorIS4_Lb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKS4_NS6_10_AllocNodeISaINS6_10_Hash_nodeIS4_Lb0EEEEEEEESt4pairINS6_14_Node_iteratorIS4_Lb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0, @function
_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKS4_NS6_10_AllocNodeISaINS6_10_Hash_nodeIS4_Lb0EEEEEEEESt4pairINS6_14_Node_iteratorIS4_Lb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0:
.LFB27032:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %r13
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	%r13, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L1690
	movq	(%rax), %rcx
	movq	8(%rcx), %r8
	jmp	.L1692
	.p2align 4,,10
	.p2align 3
.L1703:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1690
	movq	8(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L1690
.L1692:
	cmpq	%r8, %r13
	jne	.L1703
	popq	%rbx
	movq	%rcx, %rax
	popq	%r12
	xorl	%edx, %edx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1690:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movq	(%rbx), %rax
	movl	$1, %r8d
	movq	%rax, 8(%rcx)
	call	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27032:
	.size	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKS4_NS6_10_AllocNodeISaINS6_10_Hash_nodeIS4_Lb0EEEEEEEESt4pairINS6_14_Node_iteratorIS4_Lb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0, .-_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKS4_NS6_10_AllocNodeISaINS6_10_Hash_nodeIS4_Lb0EEEEEEEESt4pairINS6_14_Node_iteratorIS4_Lb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0
	.section	.rodata._ZN2v88internal4wasm10WasmEngine9TriggerGCEa.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"[wasm-gc] Starting GC. Total number of potentially dead code objects: %zu\n"
	.section	.text._ZN2v88internal4wasm10WasmEngine9TriggerGCEa,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine9TriggerGCEa
	.type	_ZN2v88internal4wasm10WasmEngine9TriggerGCEa, @function
_ZN2v88internal4wasm10WasmEngine9TriggerGCEa:
.LFB19287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 816(%rdi)
	movl	$128, %edi
	call	_Znwm@PLT
	movss	.LC4(%rip), %xmm0
	movq	824(%r15), %r12
	leaq	48(%rax), %rdx
	movq	$1, 8(%rax)
	movq	%rdx, (%rax)
	leaq	104(%rax), %rdx
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	%rdx, 56(%rax)
	movq	$1, 64(%rax)
	movq	$0, 72(%rax)
	movq	$0, 80(%rax)
	movq	$0, 96(%rax)
	movq	$0, 104(%rax)
	movb	%bl, 112(%rax)
	movb	$0, 113(%rax)
	movq	$0, 120(%rax)
	movq	%rax, 824(%r15)
	movss	%xmm0, 32(%rax)
	movss	%xmm0, 88(%rax)
	testq	%r12, %r12
	je	.L1705
	movq	72(%r12), %rbx
	testq	%rbx, %rbx
	je	.L1709
	.p2align 4,,10
	.p2align 3
.L1706:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1706
.L1709:
	movq	64(%r12), %rax
	movq	56(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	56(%r12), %rdi
	leaq	104(%r12), %rax
	movq	$0, 80(%r12)
	movq	$0, 72(%r12)
	cmpq	%rax, %rdi
	je	.L1707
	call	_ZdlPv@PLT
.L1707:
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	je	.L1713
	.p2align 4,,10
	.p2align 3
.L1710:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1710
.L1713:
	movq	8(%r12), %rax
	movq	(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	(%r12), %rdi
	leaq	48(%r12), %rax
	movq	$0, 24(%r12)
	movq	$0, 16(%r12)
	cmpq	%rax, %rdi
	je	.L1711
	call	_ZdlPv@PLT
.L1711:
	movl	$128, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1705:
	call	_ZN2v84base9TimeTicks16IsHighResolutionEv@PLT
	testb	%al, %al
	jne	.L1795
.L1714:
	movq	776(%r15), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	jne	.L1715
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1718:
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L1741
.L1715:
	movq	-88(%rbp), %rax
	movq	16(%rax), %rax
	cmpq	$0, 80(%rax)
	movq	%rax, -96(%rbp)
	je	.L1718
	movq	-88(%rbp), %rax
	leaq	760(%r15), %rdi
	leaq	8(%rax), %rsi
	call	_ZNSt8__detail9_Map_baseIPN2v88internal4wasm12NativeModuleESt4pairIKS5_St10unique_ptrINS3_10WasmEngine16NativeModuleInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS5_ESt4hashIS5_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS7_
	movq	(%rax), %rax
	movq	16(%rax), %r14
	testq	%r14, %r14
	je	.L1740
	.p2align 4,,10
	.p2align 3
.L1719:
	movq	824(%r15), %r13
	movq	8(%r14), %r12
	xorl	%edx, %edx
	movq	8(%r13), %rsi
	movq	%r12, %rax
	movq	%r12, -72(%rbp)
	divq	%rsi
	movq	0(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rdi
	leaq	0(,%rdx,8), %r10
	testq	%rax, %rax
	je	.L1722
	movq	(%rax), %rbx
	movq	8(%rbx), %rcx
	jmp	.L1724
	.p2align 4,,10
	.p2align 3
.L1796:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1722
	movq	8(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %rdi
	jne	.L1722
.L1724:
	cmpq	%rcx, %r12
	jne	.L1796
	addq	$16, %rbx
	cmpq	$0, (%rbx)
	je	.L1797
.L1738:
	leaq	37512(%r12), %rdi
	movl	$128, %esi
	call	_ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE@PLT
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.L1719
.L1740:
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %r12
	movq	72(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1718
	.p2align 4,,10
	.p2align 3
.L1720:
	movq	8(%rbx), %rax
	movq	%r12, %rsi
	movq	%rax, -64(%rbp)
	movq	824(%r15), %rax
	leaq	56(%rax), %rdi
	call	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKS4_NS6_10_AllocNodeISaINS6_10_Hash_nodeIS4_Lb0EEEEEEEESt4pairINS6_14_Node_iteratorIS4_Lb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1720
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	jne	.L1715
.L1741:
	cmpb	$0, _ZN2v88internal23FLAG_trace_wasm_code_gcE(%rip)
	jne	.L1798
.L1704:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1799
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1722:
	.cfi_restore_state
	movl	$24, %edi
	movq	%r10, -104(%rbp)
	call	_Znwm@PLT
	leaq	32(%r13), %rdi
	movl	$1, %ecx
	movq	$0, (%rax)
	movq	%rax, %rbx
	movq	-72(%rbp), %rax
	movq	$0, 16(%rbx)
	movq	%rax, 8(%rbx)
	movq	24(%r13), %rdx
	movq	8(%r13), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r8
	testb	%al, %al
	jne	.L1725
	movq	0(%r13), %r9
	movq	-104(%rbp), %r10
	movq	(%r9,%r10), %rax
	testq	%rax, %rax
	je	.L1735
.L1802:
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	0(%r13), %rax
	movq	(%rax,%r10), %rax
	movq	%rbx, (%rax)
.L1736:
	addq	$16, %rbx
	addq	$1, 24(%r13)
	movq	-72(%rbp), %r12
	cmpq	$0, (%rbx)
	jne	.L1738
	.p2align 4,,10
	.p2align 3
.L1797:
	movl	$48, %edi
	call	_Znwm@PLT
	movq	45640(%r12), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal14CancelableTaskC2EPNS0_21CancelableTaskManagerE@PLT
	leaq	16+_ZTVN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskE(%rip), %rax
	movq	%r12, 40(%r13)
	leaq	-72(%rbp), %rsi
	movq	%rax, 0(%r13)
	addq	$48, %rax
	leaq	704(%r15), %rdi
	movq	%rax, 32(%r13)
	movq	%r13, (%rbx)
	addq	$32, %r13
	call	_ZNSt8__detail9_Map_baseIPN2v88internal7IsolateESt4pairIKS4_St10unique_ptrINS2_4wasm10WasmEngine11IsolateInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	leaq	-64(%rbp), %rsi
	movq	(%rax), %rax
	movq	88(%rax), %rdi
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	%r13, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1739
	movq	(%rdi), %rax
	call	*8(%rax)
.L1739:
	movq	-72(%rbp), %r12
	jmp	.L1738
	.p2align 4,,10
	.p2align 3
.L1725:
	cmpq	$1, %rdx
	je	.L1800
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1801
	leaq	0(,%rdx,8), %rdx
	movq	%r8, -112(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-112(%rbp), %r8
	movq	%rax, %r9
	leaq	48(%r13), %rax
	movq	%rax, -104(%rbp)
.L1728:
	movq	16(%r13), %rsi
	movq	$0, 16(%r13)
	testq	%rsi, %rsi
	je	.L1730
	xorl	%edi, %edi
	leaq	16(%r13), %r10
	jmp	.L1731
	.p2align 4,,10
	.p2align 3
.L1732:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1733:
	testq	%rsi, %rsi
	je	.L1730
.L1731:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r8
	leaq	(%r9,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L1732
	movq	16(%r13), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r13)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L1744
	movq	%rcx, (%r9,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1731
	.p2align 4,,10
	.p2align 3
.L1730:
	movq	0(%r13), %rdi
	cmpq	%rdi, -104(%rbp)
	je	.L1734
	movq	%r8, -112(%rbp)
	movq	%r9, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %r9
.L1734:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r8, 8(%r13)
	divq	%r8
	movq	%r9, 0(%r13)
	leaq	0(,%rdx,8), %r10
	movq	(%r9,%r10), %rax
	testq	%rax, %rax
	jne	.L1802
.L1735:
	movq	16(%r13), %rax
	movq	%rax, (%rbx)
	movq	%rbx, 16(%r13)
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L1737
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%r13)
	movq	0(%r13), %rax
	movq	%rbx, (%rax,%rdx,8)
.L1737:
	movq	0(%r13), %rax
	leaq	16(%r13), %rdx
	movq	%rdx, (%rax,%r10)
	jmp	.L1736
	.p2align 4,,10
	.p2align 3
.L1744:
	movq	%rdx, %rdi
	jmp	.L1733
	.p2align 4,,10
	.p2align 3
.L1798:
	movq	824(%r15), %rax
	leaq	.LC20(%rip), %rdi
	movq	80(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1795:
	movq	824(%r15), %rbx
	call	_ZN2v84base9TimeTicks3NowEv@PLT
	movq	%rax, 120(%rbx)
	jmp	.L1714
.L1800:
	leaq	48(%r13), %r9
	movq	$0, 48(%r13)
	movq	%r9, -104(%rbp)
	jmp	.L1728
.L1801:
	call	_ZSt17__throw_bad_allocv@PLT
.L1799:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19287:
	.size	_ZN2v88internal4wasm10WasmEngine9TriggerGCEa, .-_ZN2v88internal4wasm10WasmEngine9TriggerGCEa
	.section	.rodata._ZN2v88internal4wasm10WasmEngine26PotentiallyFinishCurrentGCEv.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"[wasm-gc] Remaining dead code objects: %zu; outstanding isolates: %zu.\n"
	.align 8
.LC22:
	.string	"[wasm-gc] Took %d us; found %zu dead code objects, freed %zu.\n"
	.section	.text._ZN2v88internal4wasm10WasmEngine26PotentiallyFinishCurrentGCEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine26PotentiallyFinishCurrentGCEv
	.type	_ZN2v88internal4wasm10WasmEngine26PotentiallyFinishCurrentGCEv, @function
_ZN2v88internal4wasm10WasmEngine26PotentiallyFinishCurrentGCEv:
.LFB19292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	824(%rdi), %rax
	cmpb	$0, _ZN2v88internal23FLAG_trace_wasm_code_gcE(%rip)
	movq	24(%rax), %r14
	jne	.L1900
.L1804:
	testq	%r14, %r14
	jne	.L1803
	movq	72(%rax), %r15
	leaq	-64(%rbp), %rsi
	movq	$1, -104(%rbp)
	movq	%rsi, -160(%rbp)
	movq	%rsi, -112(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movl	$0x3f800000, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	testq	%r15, %r15
	je	.L1901
	leaq	760(%r12), %rax
	leaq	-120(%rbp), %r13
	movq	%rax, -144(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L1819:
	movq	8(%r15), %rsi
	movq	768(%r12), %r8
	xorl	%edx, %edx
	movq	%rsi, -120(%rbp)
	movq	48(%rsi), %r9
	movq	%r9, %rax
	divq	%r8
	movq	760(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rbx
	testq	%rax, %rax
	je	.L1807
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L1809
	.p2align 4,,10
	.p2align 3
.L1902:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1807
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%r8
	cmpq	%rdx, %rbx
	jne	.L1807
.L1809:
	cmpq	%rdi, %r9
	jne	.L1902
	addq	$16, %rcx
.L1839:
	movq	(%rcx), %rbx
	movq	%rsi, %rax
	xorl	%edx, %edx
	movq	64(%rbx), %rdi
	divq	%rdi
	movq	56(%rbx), %rax
	movq	(%rax,%rdx,8), %r9
	movq	%rdx, %r10
	testq	%r9, %r9
	je	.L1810
	movq	(%r9), %rax
	movq	8(%rax), %rcx
	jmp	.L1812
	.p2align 4,,10
	.p2align 3
.L1903:
	movq	(%rax), %r8
	testq	%r8, %r8
	je	.L1810
	movq	8(%r8), %rcx
	movq	%rax, %r9
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L1810
	movq	%r8, %rax
.L1812:
	cmpq	%rcx, %rsi
	jne	.L1903
	leaq	56(%rbx), %rdi
	movq	%rax, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	call	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE
.L1810:
	leaq	112(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKS4_NS6_10_AllocNodeISaINS6_10_Hash_nodeIS4_Lb0EEEEEEEESt4pairINS6_14_Node_iteratorIS4_Lb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0
	movq	-120(%rbp), %rax
	lock subl	$1, 140(%rax)
	je	.L1904
.L1813:
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.L1819
.L1806:
	movq	-152(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm10WasmEngine18FreeDeadCodeLockedERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEE
	movq	824(%r12), %r13
	movq	120(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L1905
	cmpb	$0, _ZN2v88internal23FLAG_trace_wasm_code_gcE(%rip)
	jne	.L1906
.L1822:
	movzbl	113(%r13), %r14d
	movq	$0, 824(%r12)
	movq	72(%r13), %rbx
	testq	%rbx, %rbx
	je	.L1826
	.p2align 4,,10
	.p2align 3
.L1823:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1823
.L1826:
	movq	64(%r13), %rax
	movq	56(%r13), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	56(%r13), %rdi
	leaq	104(%r13), %rax
	movq	$0, 80(%r13)
	movq	$0, 72(%r13)
	cmpq	%rax, %rdi
	je	.L1824
	call	_ZdlPv@PLT
.L1824:
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	je	.L1830
	.p2align 4,,10
	.p2align 3
.L1827:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1827
.L1830:
	movq	8(%r13), %rax
	movq	0(%r13), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	0(%r13), %rdi
	leaq	48(%r13), %rax
	movq	$0, 24(%r13)
	movq	$0, 16(%r13)
	cmpq	%rax, %rdi
	je	.L1828
	call	_ZdlPv@PLT
.L1828:
	movl	$128, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	testb	%r14b, %r14b
	jne	.L1907
.L1831:
	movq	-96(%rbp), %r12
	testq	%r12, %r12
	jne	.L1832
	jmp	.L1836
	.p2align 4,,10
	.p2align 3
.L1908:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1836
.L1837:
	movq	%rbx, %r12
.L1832:
	movq	16(%r12), %rdi
	movq	(%r12), %rbx
	testq	%rdi, %rdi
	jne	.L1908
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1837
.L1836:
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, -88(%rbp)
	movq	-112(%rbp), %rdi
	movq	$0, -96(%rbp)
	cmpq	-160(%rbp), %rdi
	je	.L1803
	call	_ZdlPv@PLT
.L1803:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1909
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1904:
	.cfi_restore_state
	movq	-120(%rbp), %rax
	movq	-104(%rbp), %r8
	xorl	%edx, %edx
	movq	48(%rax), %r9
	movq	%r9, %rax
	divq	%r8
	movq	-112(%rbp), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rbx
	testq	%rax, %rax
	je	.L1814
	movq	(%rax), %rdi
	movq	8(%rdi), %rcx
	jmp	.L1816
	.p2align 4,,10
	.p2align 3
.L1910:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1814
	movq	8(%rdi), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %rbx
	jne	.L1814
.L1816:
	cmpq	%rcx, %r9
	jne	.L1910
	addq	$16, %rdi
.L1838:
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L1817
	movq	-120(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 8(%rdi)
.L1818:
	addq	$1, %r14
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1807:
	movl	$24, %edi
	movq	%r9, -136(%rbp)
	call	_Znwm@PLT
	movq	-136(%rbp), %r9
	movq	%rbx, %rsi
	movq	-144(%rbp), %rdi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	%r9, 8(%rax)
	movq	%r9, %rdx
	movq	$0, 16(%rax)
	call	_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St10unique_ptrINS2_10WasmEngine16NativeModuleInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm
	movq	-120(%rbp), %rsi
	leaq	16(%rax), %rcx
	jmp	.L1839
	.p2align 4,,10
	.p2align 3
.L1907:
	movsbl	%r14b, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm10WasmEngine9TriggerGCEa
	jmp	.L1831
	.p2align 4,,10
	.p2align 3
.L1906:
	movq	80(%r13), %rdx
	movq	%r14, %rcx
	movl	%r15d, %esi
	xorl	%eax, %eax
	leaq	.LC22(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	824(%r12), %r13
	jmp	.L1822
	.p2align 4,,10
	.p2align 3
.L1905:
	call	_ZN2v84base9TimeTicks3NowEv@PLT
	leaq	-120(%rbp), %rdi
	subq	%rbx, %rax
	movq	%rax, -120(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movl	$2147483647, %esi
	movq	720(%r12), %rbx
	cmpq	$2147483647, %rax
	movq	%rax, %r15
	movl	$0, %eax
	cmovg	%rsi, %r15
	testq	%r15, %r15
	cmovs	%rax, %r15
	testq	%rbx, %rbx
	je	.L1899
	.p2align 4,,10
	.p2align 3
.L1821:
	movq	16(%rbx), %rax
	movl	%r15d, %esi
	movq	104(%rax), %rdi
	addq	$4920, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1821
.L1899:
	cmpb	$0, _ZN2v88internal23FLAG_trace_wasm_code_gcE(%rip)
	movq	824(%r12), %r13
	je	.L1822
	jmp	.L1906
	.p2align 4,,10
	.p2align 3
.L1900:
	movq	80(%rax), %rsi
	movq	%r14, %rdx
	xorl	%eax, %eax
	leaq	.LC21(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	824(%r12), %rax
	movq	24(%rax), %r14
	jmp	.L1804
	.p2align 4,,10
	.p2align 3
.L1817:
	movq	%r13, %rdx
	call	_ZNSt6vectorIPN2v88internal4wasm8WasmCodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L1818
	.p2align 4,,10
	.p2align 3
.L1814:
	movl	$40, %edi
	movq	%r9, -136(%rbp)
	call	_Znwm@PLT
	movq	-136(%rbp), %r9
	movq	-152(%rbp), %rdi
	movq	%rbx, %rsi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	%r9, 8(%rax)
	movq	%r9, %rdx
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	call	_ZNSt10_HashtableIPN2v88internal4wasm12NativeModuleESt4pairIKS4_St6vectorIPNS2_8WasmCodeESaIS9_EEESaISC_ENSt8__detail10_Select1stESt8equal_toIS4_ESt4hashIS4_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSE_10_Hash_nodeISC_Lb0EEEm
	leaq	16(%rax), %rdi
	jmp	.L1838
	.p2align 4,,10
	.p2align 3
.L1901:
	leaq	-112(%rbp), %rax
	movq	%rax, -152(%rbp)
	jmp	.L1806
.L1909:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19292:
	.size	_ZN2v88internal4wasm10WasmEngine26PotentiallyFinishCurrentGCEv, .-_ZN2v88internal4wasm10WasmEngine26PotentiallyFinishCurrentGCEv
	.section	.text.unlikely._ZN2v88internal4wasm10WasmEngine13RemoveIsolateEPNS0_7IsolateE,"ax",@progbits
	.align 2
.LCOLDB23:
	.section	.text._ZN2v88internal4wasm10WasmEngine13RemoveIsolateEPNS0_7IsolateE,"ax",@progbits
.LHOTB23:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine13RemoveIsolateEPNS0_7IsolateE
	.type	_ZN2v88internal4wasm10WasmEngine13RemoveIsolateEPNS0_7IsolateE, @function
_ZN2v88internal4wasm10WasmEngine13RemoveIsolateEPNS0_7IsolateE:
.LFB19228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	592(%rdi), %rax
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	712(%r15), %rdi
	movq	%r14, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	704(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L1912
	movq	(%rax), %rsi
	movq	%rdx, %r8
	movq	8(%rsi), %rcx
	jmp	.L1914
	.p2align 4,,10
	.p2align 3
.L2013:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L1912
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L2012
.L1914:
	cmpq	%r14, %rcx
	jne	.L2013
	movq	16(%rsi), %rax
	movq	$0, 16(%rsi)
	leaq	704(%r15), %rdi
	movq	%rax, %rbx
	movq	%rax, -112(%rbp)
	call	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_St10unique_ptrINS1_4wasm10WasmEngine11IsolateInfoESt14default_deleteIS9_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSF_20_Node_const_iteratorISD_Lb0ELb0EEE
	leaq	760(%r15), %rdx
	movq	%rbx, %rax
	movq	24(%rbx), %r12
	movq	%rdx, -88(%rbp)
	addq	$8, %rax
	leaq	-64(%rbp), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -96(%rbp)
	cmpq	%rax, %r12
	je	.L1954
	.p2align 4,,10
	.p2align 3
.L1926:
	movq	32(%r12), %rax
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNSt8__detail9_Map_baseIPN2v88internal4wasm12NativeModuleESt4pairIKS5_St10unique_ptrINS3_10WasmEngine16NativeModuleInfoESt14default_deleteISA_EEESaISE_ENS_10_Select1stESt8equal_toIS5_ESt4hashIS5_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS7_
	xorl	%edx, %edx
	movq	(%rax), %rbx
	movq	%r14, %rax
	movq	8(%rbx), %r8
	movq	(%rbx), %r13
	divq	%r8
	leaq	0(,%rdx,8), %rax
	movq	%rdx, %r10
	movq	%rax, -104(%rbp)
	addq	%r13, %rax
	movq	(%rax), %r11
	movq	%rax, -72(%rbp)
	testq	%r11, %r11
	je	.L1915
	movq	(%r11), %rdi
	movq	%r11, %r9
	movq	8(%rdi), %rcx
	jmp	.L1917
	.p2align 4,,10
	.p2align 3
.L2014:
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L1915
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r9
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L1915
	movq	%rsi, %rdi
.L1917:
	cmpq	%r14, %rcx
	jne	.L2014
	movq	(%rdi), %rcx
	cmpq	%r9, %r11
	je	.L2015
	testq	%rcx, %rcx
	je	.L1919
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r10
	je	.L1919
	movq	%r9, 0(%r13,%rdx,8)
	movq	(%rdi), %rcx
.L1919:
	movq	%rcx, (%r9)
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
.L1915:
	movq	824(%r15), %rsi
	testq	%rsi, %rsi
	jne	.L2016
.L1921:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, -80(%rbp)
	jne	.L1926
.L1954:
	movq	824(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1928
	movq	%r14, %rsi
	call	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_4wasm12_GLOBAL__N_120WasmGCForegroundTaskEESaISA_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS5_.isra.0
	testq	%rax, %rax
	jne	.L2017
.L1928:
	movq	-112(%rbp), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L1930
	movq	$0, 24(%rax)
.L1930:
	movq	-112(%rbp), %rbx
	movq	72(%rbx), %rax
	movq	64(%rbx), %rdi
	cmpq	%rdi, %rax
	je	.L1932
	subq	%rdi, %rax
	sarq	$3, %rax
	movslq	%eax, %rsi
	movabsq	$2305843009213693951, %rax
	andq	%rax, %rsi
	call	_ZN2v88internal4wasm8WasmCode17DecrementRefCountENS0_6VectorIKPS2_EE@PLT
	movq	64(%rbx), %rax
	cmpq	72(%rbx), %rax
	je	.L1932
	movq	%rax, 72(%rbx)
.L1932:
	movq	-112(%rbp), %rax
	movq	112(%rax), %r12
	testq	%r12, %r12
	je	.L1936
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1933
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L1934:
	cmpl	$1, %eax
	jne	.L1936
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1937
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1938:
	cmpl	$1, %eax
	je	.L2018
	.p2align 4,,10
	.p2align 3
.L1936:
	movq	-112(%rbp), %rax
	movq	96(%rax), %r12
	testq	%r12, %r12
	je	.L1941
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1942
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L1943:
	cmpl	$1, %eax
	jne	.L1941
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1945
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1946:
	cmpl	$1, %eax
	je	.L2019
	.p2align 4,,10
	.p2align 3
.L1941:
	movq	-112(%rbp), %rax
	movq	64(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1947
	call	_ZdlPv@PLT
.L1947:
	movq	-112(%rbp), %rax
	movq	16(%rax), %rbx
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.L1949
.L1948:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal4wasm12NativeModuleES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1948
.L1949:
	movq	-112(%rbp), %rdi
	movl	$120, %esi
	call	_ZdlPvm@PLT
	movq	-120(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2020
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2016:
	.cfi_restore_state
	movq	72(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1921
	.p2align 4,,10
	.p2align 3
.L1925:
	movq	8(%rbx), %r13
	movq	64(%rsi), %r9
	xorl	%edx, %edx
	movq	%r13, %rax
	divq	%r9
	movq	56(%rsi), %rax
	movq	(%rax,%rdx,8), %r10
	movq	%rdx, %r11
	testq	%r10, %r10
	je	.L1922
	movq	(%r10), %rcx
	movq	8(%rcx), %r8
	jmp	.L1924
	.p2align 4,,10
	.p2align 3
.L2021:
	movq	(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L1922
	movq	8(%rdi), %r8
	xorl	%edx, %edx
	movq	%rcx, %r10
	movq	%r8, %rax
	divq	%r9
	cmpq	%rdx, %r11
	jne	.L1922
	movq	%rdi, %rcx
.L1924:
	cmpq	%r8, %r13
	jne	.L2021
	leaq	56(%rsi), %rdi
	movq	%r10, %rdx
	movq	%r11, %rsi
	call	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE
.L1922:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1921
	movq	824(%r15), %rsi
	jmp	.L1925
	.p2align 4,,10
	.p2align 3
.L2015:
	testq	%rcx, %rcx
	je	.L1956
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r10
	je	.L1919
	movq	%r9, 0(%r13,%rdx,8)
	movq	-104(%rbp), %rax
	leaq	16(%rbx), %rdx
	addq	(%rbx), %rax
	movq	%rax, -72(%rbp)
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2022
.L1920:
	movq	-72(%rbp), %rax
	movq	$0, (%rax)
	movq	(%rdi), %rcx
	jmp	.L1919
	.p2align 4,,10
	.p2align 3
.L1956:
	movq	%r9, %rax
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	jne	.L1920
.L2022:
	movq	%rcx, 16(%rbx)
	jmp	.L1920
.L2017:
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm10WasmEngine26PotentiallyFinishCurrentGCEv
	jmp	.L1928
.L1933:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L1934
.L1942:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L1943
.L2019:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1941
.L2018:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1936
.L1945:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1946
.L1937:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1938
.L2020:
	call	__stack_chk_fail@PLT
.L2012:
	jmp	.L1912
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal4wasm10WasmEngine13RemoveIsolateEPNS0_7IsolateE
	.cfi_startproc
	.type	_ZN2v88internal4wasm10WasmEngine13RemoveIsolateEPNS0_7IsolateE.cold, @function
_ZN2v88internal4wasm10WasmEngine13RemoveIsolateEPNS0_7IsolateE.cold:
.LFSB19228:
.L1912:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	16, %rax
	ud2
	.cfi_endproc
.LFE19228:
	.section	.text._ZN2v88internal4wasm10WasmEngine13RemoveIsolateEPNS0_7IsolateE
	.size	_ZN2v88internal4wasm10WasmEngine13RemoveIsolateEPNS0_7IsolateE, .-_ZN2v88internal4wasm10WasmEngine13RemoveIsolateEPNS0_7IsolateE
	.section	.text.unlikely._ZN2v88internal4wasm10WasmEngine13RemoveIsolateEPNS0_7IsolateE
	.size	_ZN2v88internal4wasm10WasmEngine13RemoveIsolateEPNS0_7IsolateE.cold, .-_ZN2v88internal4wasm10WasmEngine13RemoveIsolateEPNS0_7IsolateE.cold
.LCOLDE23:
	.section	.text._ZN2v88internal4wasm10WasmEngine13RemoveIsolateEPNS0_7IsolateE
.LHOTE23:
	.section	.rodata._ZN2v88internal4wasm10WasmEngine19ReportLiveCodeForGCEPNS0_7IsolateENS0_6VectorIPNS1_8WasmCodeEEE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"ReportLiveCodeForGC"
	.section	.rodata._ZN2v88internal4wasm10WasmEngine19ReportLiveCodeForGCEPNS0_7IsolateENS0_6VectorIPNS1_8WasmCodeEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"[wasm-gc] Isolate %d reporting %zu live code objects.\n"
	.section	.text._ZN2v88internal4wasm10WasmEngine19ReportLiveCodeForGCEPNS0_7IsolateENS0_6VectorIPNS1_8WasmCodeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine19ReportLiveCodeForGCEPNS0_7IsolateENS0_6VectorIPNS1_8WasmCodeEEE
	.type	_ZN2v88internal4wasm10WasmEngine19ReportLiveCodeForGCEPNS0_7IsolateENS0_6VectorIPNS1_8WasmCodeEEE, @function
_ZN2v88internal4wasm10WasmEngine19ReportLiveCodeForGCEPNS0_7IsolateENS0_6VectorIPNS1_8WasmCodeEEE:
.LFB19268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal4wasm10WasmEngine19ReportLiveCodeForGCEPNS0_7IsolateENS0_6VectorIPNS1_8WasmCodeEEEE28trace_event_unique_atomic777(%rip), %rdx
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L2067
.L2025:
	movq	$0, -112(%rbp)
	movzbl	(%r14), %eax
	testb	$5, %al
	jne	.L2068
.L2027:
	cmpb	$0, _ZN2v88internal23FLAG_trace_wasm_code_gcE(%rip)
	jne	.L2069
.L2031:
	leaq	592(%r12), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	824(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2032
	movq	%r13, %rsi
	call	_ZNSt10_HashtableIPN2v88internal7IsolateESt4pairIKS3_PNS1_4wasm12_GLOBAL__N_120WasmGCForegroundTaskEESaISA_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS5_.isra.0
	testq	%rax, %rax
	jne	.L2070
.L2032:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	leaq	-112(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
.L2023:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2071
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2070:
	.cfi_restore_state
	movq	824(%r12), %rax
	movq	40960(%r13), %rdi
	leaq	(%rbx,%r15,8), %r13
	movsbl	112(%rax), %esi
	addq	$2048, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	cmpq	%r13, %rbx
	je	.L2038
	.p2align 4,,10
	.p2align 3
.L2039:
	movq	824(%r12), %rdi
	movq	(%rbx), %r15
	xorl	%edx, %edx
	movq	64(%rdi), %r8
	movq	%r15, %rax
	divq	%r8
	movq	56(%rdi), %rax
	movq	(%rax,%rdx,8), %r9
	movq	%rdx, %r10
	testq	%r9, %r9
	je	.L2035
	movq	(%r9), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2037
	.p2align 4,,10
	.p2align 3
.L2072:
	movq	(%rcx), %r11
	testq	%r11, %r11
	je	.L2035
	movq	8(%r11), %rsi
	xorl	%edx, %edx
	movq	%rcx, %r9
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L2035
	movq	%r11, %rcx
.L2037:
	cmpq	%rsi, %r15
	jne	.L2072
	addq	$56, %rdi
	movq	%r9, %rdx
	movq	%r10, %rsi
	call	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS6_15_Hash_node_baseEPNS6_10_Hash_nodeIS4_Lb0EEE
.L2035:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L2039
.L2038:
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm10WasmEngine26PotentiallyFinishCurrentGCEv
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	leaq	-112(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	jmp	.L2023
	.p2align 4,,10
	.p2align 3
.L2069:
	movl	40800(%r13), %esi
	movq	%r15, %rdx
	leaq	.LC25(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2068:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	$0, -120(%rbp)
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2073
.L2028:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2029
	movq	(%rdi), %rax
	call	*8(%rax)
.L2029:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2030
	movq	(%rdi), %rax
	call	*8(%rax)
.L2030:
	leaq	.LC24(%rip), %rax
	movq	%r14, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	-120(%rbp), %rax
	movq	%rax, -88(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -112(%rbp)
	jmp	.L2027
	.p2align 4,,10
	.p2align 3
.L2067:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2074
.L2026:
	movq	%r14, _ZZN2v88internal4wasm10WasmEngine19ReportLiveCodeForGCEPNS0_7IsolateENS0_6VectorIPNS1_8WasmCodeEEEE28trace_event_unique_atomic777(%rip)
	jmp	.L2025
	.p2align 4,,10
	.p2align 3
.L2073:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r14, %rdx
	movl	$88, %esi
	pushq	%rcx
	leaq	.LC24(%rip), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, -120(%rbp)
	addq	$64, %rsp
	jmp	.L2028
	.p2align 4,,10
	.p2align 3
.L2074:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L2026
.L2071:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19268:
	.size	_ZN2v88internal4wasm10WasmEngine19ReportLiveCodeForGCEPNS0_7IsolateENS0_6VectorIPNS1_8WasmCodeEEE, .-_ZN2v88internal4wasm10WasmEngine19ReportLiveCodeForGCEPNS0_7IsolateENS0_6VectorIPNS1_8WasmCodeEEE
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTask11RunInternalEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTask11RunInternalEv, @function
_ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTask11RunInternalEv:
.LFB18899:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEE22ArchivedThreadsVisitor(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	-32(%rbp), %rsi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	%rdx, -32(%rbp)
	movq	41168(%rax), %rdi
	movq	45752(%rax), %r12
	call	_ZN2v88internal13ThreadManager22IterateArchivedThreadsEPNS0_13ThreadVisitorE@PLT
	movq	40(%rbx), %rsi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm10WasmEngine19ReportLiveCodeForGCEPNS0_7IsolateENS0_6VectorIPNS1_8WasmCodeEEE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2078
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2078:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18899:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTask11RunInternalEv, .-_ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTask11RunInternalEv
	.section	.text._ZN2v88internal4wasm10WasmEngine28ReportLiveCodeFromStackForGCEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine28ReportLiveCodeFromStackForGCEPNS0_7IsolateE
	.type	_ZN2v88internal4wasm10WasmEngine28ReportLiveCodeFromStackForGCEPNS0_7IsolateE, @function
_ZN2v88internal4wasm10WasmEngine28ReportLiveCodeFromStackForGCEPNS0_7IsolateE:
.LFB19270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1568(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1504(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1584(%rbp), %rbx
	subq	$1624, %rsp
	movq	%rdi, -1640(%rbp)
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rbx, -1632(%rbp)
	movq	$1, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	$0, -1608(%rbp)
	movl	$0x3f800000, -1600(%rbp)
	movq	$0, -1592(%rbp)
	movq	$0, -1584(%rbp)
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r14
	leaq	-1632(%rbp), %rax
	movq	%rax, -1648(%rbp)
	testq	%r14, %r14
	jne	.L2080
	jmp	.L2087
	.p2align 4,,10
	.p2align 3
.L2083:
	movq	%r12, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %r14
	testq	%r14, %r14
	je	.L2087
.L2080:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	cmpl	$5, %eax
	jne	.L2083
	movq	%r14, %rdi
	call	_ZNK2v88internal17WasmCompiledFrame9wasm_codeEv@PLT
	movq	-1624(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rax, %r9
	divq	%rdi
	movq	-1632(%rbp), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L2084
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2086
	.p2align 4,,10
	.p2align 3
.L2118:
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L2084
.L2086:
	cmpq	%rsi, %r9
	je	.L2083
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L2118
.L2084:
	movl	$16, %edi
	movq	%r9, -1656(%rbp)
	call	_Znwm@PLT
	movq	-1656(%rbp), %r9
	movq	-1648(%rbp), %rdi
	movq	%r14, %rsi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	%r9, 8(%rax)
	movq	%r9, %rdx
	call	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	jmp	.L2083
	.p2align 4,,10
	.p2align 3
.L2087:
	movq	41168(%r13), %rdi
	leaq	16+_ZTVZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEE22ArchivedThreadsVisitor(%rip), %rax
	movq	%r12, %rsi
	movq	%rax, -1504(%rbp)
	call	_ZN2v88internal13ThreadManager22IterateArchivedThreadsEPNS0_13ThreadVisitorE@PLT
	movq	-1616(%rbp), %r12
	xorl	%ecx, %ecx
	movq	%r12, %rax
	testq	%r12, %r12
	je	.L2119
	.p2align 4,,10
	.p2align 3
.L2081:
	movq	(%rax), %rax
	addq	$1, %rcx
	testq	%rax, %rax
	jne	.L2081
	leaq	0(,%rcx,8), %rdi
	movq	%rcx, -1648(%rbp)
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	movq	$-1, %rax
	cmovg	%rax, %rdi
	call	_Znam@PLT
	movq	-1648(%rbp), %rcx
	movq	%rax, %r8
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2089:
	movq	8(%r12), %rsi
	addq	$8, %rdx
	movq	%rsi, -8(%rdx)
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L2089
	movq	-1640(%rbp), %rdi
	movq	%r8, %rdx
	movq	%r13, %rsi
	movq	%r8, -1648(%rbp)
	call	_ZN2v88internal4wasm10WasmEngine19ReportLiveCodeForGCEPNS0_7IsolateENS0_6VectorIPNS1_8WasmCodeEEE
	movq	-1648(%rbp), %r8
	movq	%r8, %rdi
	call	_ZdaPv@PLT
.L2094:
	movq	-1616(%rbp), %r12
	testq	%r12, %r12
	je	.L2093
	.p2align 4,,10
	.p2align 3
.L2090:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L2090
.L2093:
	movq	-1624(%rbp), %rax
	movq	-1632(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-1632(%rbp), %rdi
	movq	$0, -1608(%rbp)
	movq	$0, -1616(%rbp)
	cmpq	%rbx, %rdi
	je	.L2091
	call	_ZdlPv@PLT
.L2091:
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2120
	addq	$1624, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2119:
	.cfi_restore_state
	movq	-1640(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm10WasmEngine19ReportLiveCodeForGCEPNS0_7IsolateENS0_6VectorIPNS1_8WasmCodeEEE
	jmp	.L2094
.L2120:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19270:
	.size	_ZN2v88internal4wasm10WasmEngine28ReportLiveCodeFromStackForGCEPNS0_7IsolateE, .-_ZN2v88internal4wasm10WasmEngine28ReportLiveCodeFromStackForGCEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal4wasm10WasmEngine22AddPotentiallyDeadCodeEPNS1_8WasmCodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"[wasm-gc] Triggering GC (potentially dead: %zu bytes; limit: %zu bytes).\n"
	.align 8
.LC27:
	.string	"[wasm-gc] Scheduling another GC after the current one (potentially dead: %zu bytes; limit: %zu bytes).\n"
	.section	.text.unlikely._ZN2v88internal4wasm10WasmEngine22AddPotentiallyDeadCodeEPNS1_8WasmCodeE,"ax",@progbits
	.align 2
.LCOLDB28:
	.section	.text._ZN2v88internal4wasm10WasmEngine22AddPotentiallyDeadCodeEPNS1_8WasmCodeE,"ax",@progbits
.LHOTB28:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm10WasmEngine22AddPotentiallyDeadCodeEPNS1_8WasmCodeE
	.type	_ZN2v88internal4wasm10WasmEngine22AddPotentiallyDeadCodeEPNS1_8WasmCodeE, @function
_ZN2v88internal4wasm10WasmEngine22AddPotentiallyDeadCodeEPNS1_8WasmCodeE:
.LFB19280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	592(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	-56(%rbp), %r8
	movq	768(%r12), %rsi
	xorl	%edx, %edx
	movq	48(%r8), %r9
	movq	%r9, %rax
	divq	%rsi
	movq	760(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L2122
	movq	(%rax), %rcx
	movq	%rdx, %r10
	movq	8(%rcx), %rdi
	jmp	.L2124
	.p2align 4,,10
	.p2align 3
.L2167:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2122
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L2166
.L2124:
	cmpq	%rdi, %r9
	jne	.L2167
	movq	16(%rcx), %rbx
	movq	%r8, %rax
	xorl	%edx, %edx
	movq	120(%rbx), %r9
	divq	%r9
	movq	112(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2125
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.L2125
	movq	8(%rcx), %rsi
	xorl	%edi, %edi
	jmp	.L2130
	.p2align 4,,10
	.p2align 3
.L2126:
	testq	%rdi, %rdi
	jne	.L2131
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2129
.L2168:
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r9
	cmpq	%rdx, %r10
	jne	.L2129
.L2130:
	cmpq	%rsi, %r8
	jne	.L2126
	movq	(%rcx), %rcx
	addq	$1, %rdi
	testq	%rcx, %rcx
	jne	.L2168
	.p2align 4,,10
	.p2align 3
.L2129:
	testq	%rdi, %rdi
	je	.L2125
.L2131:
	xorl	%r14d, %r14d
.L2128:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2125:
	.cfi_restore_state
	leaq	-56(%rbp), %rsi
	leaq	56(%rbx), %rdi
	call	_ZNSt10_HashtableIPN2v88internal4wasm8WasmCodeES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_insertIRKS4_NS6_10_AllocNodeISaINS6_10_Hash_nodeIS4_Lb0EEEEEEEESt4pairINS6_14_Node_iteratorIS4_Lb1ELb0EEEbEOT_RKT0_St17integral_constantIbLb1EEm.constprop.0
	movl	%edx, %r14d
	testb	%dl, %dl
	je	.L2131
	movq	-56(%rbp), %rdx
	movzbl	_ZN2v88internal17FLAG_wasm_code_gcE(%rip), %r15d
	movq	816(%r12), %rax
	addq	8(%rdx), %rax
	movq	%rax, 816(%r12)
	testb	%r15b, %r15b
	je	.L2128
	xorl	%edx, %edx
	cmpb	$0, _ZN2v88internal24FLAG_stress_wasm_code_gcE(%rip)
	je	.L2169
.L2133:
	cmpq	%rax, %rdx
	jnb	.L2128
	movq	824(%r12), %rcx
	movzbl	168(%rbx), %eax
	testq	%rcx, %rcx
	je	.L2170
	cmpb	$0, 113(%rcx)
	jne	.L2128
	cmpb	$127, %al
	je	.L2138
	addl	$1, %eax
	movb	%al, 168(%rbx)
.L2138:
	cmpb	$0, _ZN2v88internal23FLAG_trace_wasm_code_gcE(%rip)
	jne	.L2171
.L2139:
	movq	824(%r12), %rdx
	movl	%r15d, %r14d
	movb	%al, 113(%rdx)
	jmp	.L2128
	.p2align 4,,10
	.p2align 3
.L2169:
	movabsq	$-3689348814741910323, %rcx
	movq	304(%r12), %rdx
	movq	%rdx, %rax
	mulq	%rcx
	shrq	$3, %rdx
	movq	816(%r12), %rax
	addq	$65536, %rdx
	jmp	.L2133
	.p2align 4,,10
	.p2align 3
.L2170:
	cmpb	$127, %al
	je	.L2136
	addl	$1, %eax
	movb	%al, 168(%rbx)
.L2136:
	cmpb	$0, _ZN2v88internal23FLAG_trace_wasm_code_gcE(%rip)
	jne	.L2172
.L2137:
	movsbl	168(%rbx), %esi
	movq	%r12, %rdi
	movl	%r15d, %r14d
	call	_ZN2v88internal4wasm10WasmEngine9TriggerGCEa
	jmp	.L2128
.L2171:
	movq	816(%r12), %rsi
	xorl	%eax, %eax
	leaq	.LC27(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	168(%rbx), %eax
	jmp	.L2139
.L2172:
	movq	816(%r12), %rsi
	leaq	.LC26(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2137
.L2166:
	jmp	.L2122
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal4wasm10WasmEngine22AddPotentiallyDeadCodeEPNS1_8WasmCodeE
	.cfi_startproc
	.type	_ZN2v88internal4wasm10WasmEngine22AddPotentiallyDeadCodeEPNS1_8WasmCodeE.cold, @function
_ZN2v88internal4wasm10WasmEngine22AddPotentiallyDeadCodeEPNS1_8WasmCodeE.cold:
.LFSB19280:
.L2122:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	16, %rax
	ud2
	.cfi_endproc
.LFE19280:
	.section	.text._ZN2v88internal4wasm10WasmEngine22AddPotentiallyDeadCodeEPNS1_8WasmCodeE
	.size	_ZN2v88internal4wasm10WasmEngine22AddPotentiallyDeadCodeEPNS1_8WasmCodeE, .-_ZN2v88internal4wasm10WasmEngine22AddPotentiallyDeadCodeEPNS1_8WasmCodeE
	.section	.text.unlikely._ZN2v88internal4wasm10WasmEngine22AddPotentiallyDeadCodeEPNS1_8WasmCodeE
	.size	_ZN2v88internal4wasm10WasmEngine22AddPotentiallyDeadCodeEPNS1_8WasmCodeE.cold, .-_ZN2v88internal4wasm10WasmEngine22AddPotentiallyDeadCodeEPNS1_8WasmCodeE.cold
.LCOLDE28:
	.section	.text._ZN2v88internal4wasm10WasmEngine22AddPotentiallyDeadCodeEPNS1_8WasmCodeE
.LHOTE28:
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm10WasmEngineC2Ev,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm10WasmEngineC2Ev, @function
_GLOBAL__sub_I__ZN2v88internal4wasm10WasmEngineC2Ev:
.LFB26428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26428:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm10WasmEngineC2Ev, .-_GLOBAL__sub_I__ZN2v88internal4wasm10WasmEngineC2Ev
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm10WasmEngineC2Ev
	.weak	_ZTVN2v88internal14CancelableTaskE
	.section	.data.rel.ro._ZTVN2v88internal14CancelableTaskE,"awG",@progbits,_ZTVN2v88internal14CancelableTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal14CancelableTaskE, @object
	.size	_ZTVN2v88internal14CancelableTaskE, 88
_ZTVN2v88internal14CancelableTaskE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	__cxa_pure_virtual
	.quad	-32
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskE,"aw"
	.align 8
	.type	_ZTVN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskE, @object
	.size	_ZTVN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskE, 40
_ZTVN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskD1Ev
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTaskD0Ev
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_112LogCodesTask3RunEv
	.section	.data.rel.ro.local._ZTVZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEE22ArchivedThreadsVisitor,"aw"
	.align 8
	.type	_ZTVZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEE22ArchivedThreadsVisitor, @object
	.size	_ZTVZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEE22ArchivedThreadsVisitor, 40
_ZTVZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEE22ArchivedThreadsVisitor:
	.quad	0
	.quad	0
	.quad	_ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitor11VisitThreadES4_PNS0_14ThreadLocalTopE
	.quad	_ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitorD1Ev
	.quad	_ZZN2v88internal4wasm12_GLOBAL__N_122CheckNoArchivedThreadsEPNS0_7IsolateEEN22ArchivedThreadsVisitorD0Ev
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskE,"aw"
	.align 8
	.type	_ZTVN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskE, @object
	.size	_ZTVN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskE, 88
_ZTVN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD1Ev
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD0Ev
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTask11RunInternalEv
	.quad	-32
	.quad	0
	.quad	_ZThn32_N2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD1Ev
	.quad	_ZThn32_N2v88internal4wasm12_GLOBAL__N_120WasmGCForegroundTaskD0Ev
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskE,"aw"
	.align 8
	.type	_ZTVN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskE, @object
	.size	_ZTVN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskE, 88
_ZTVN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD1Ev
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD0Ev
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTask11RunInternalEv
	.quad	-32
	.quad	0
	.quad	_ZThn32_N2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD1Ev
	.quad	_ZThn32_N2v88internal4wasm12_GLOBAL__N_125SampleTopTierCodeSizeTaskD0Ev
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm10WasmEngineESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.bss._ZGVZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object,"aw",@nobits
	.align 8
	.type	_ZGVZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object, @object
	.size	_ZGVZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object, 8
_ZGVZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object:
	.zero	8
	.section	.bss._ZZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object,"aw",@nobits
	.align 16
	.type	_ZZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object, @object
	.size	_ZZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object, 16
_ZZN2v88internal4wasm12_GLOBAL__N_119GetSharedWasmEngineEvE6object:
	.zero	16
	.section	.bss._ZZN2v88internal4wasm10WasmEngine18FreeDeadCodeLockedERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEEE28trace_event_unique_atomic850,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal4wasm10WasmEngine18FreeDeadCodeLockedERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEEE28trace_event_unique_atomic850, @object
	.size	_ZZN2v88internal4wasm10WasmEngine18FreeDeadCodeLockedERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEEE28trace_event_unique_atomic850, 8
_ZZN2v88internal4wasm10WasmEngine18FreeDeadCodeLockedERKSt13unordered_mapIPNS1_12NativeModuleESt6vectorIPNS1_8WasmCodeESaIS8_EESt4hashIS5_ESt8equal_toIS5_ESaISt4pairIKS5_SA_EEEE28trace_event_unique_atomic850:
	.zero	8
	.section	.bss._ZZN2v88internal4wasm10WasmEngine19ReportLiveCodeForGCEPNS0_7IsolateENS0_6VectorIPNS1_8WasmCodeEEEE28trace_event_unique_atomic777,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal4wasm10WasmEngine19ReportLiveCodeForGCEPNS0_7IsolateENS0_6VectorIPNS1_8WasmCodeEEEE28trace_event_unique_atomic777, @object
	.size	_ZZN2v88internal4wasm10WasmEngine19ReportLiveCodeForGCEPNS0_7IsolateENS0_6VectorIPNS1_8WasmCodeEEEE28trace_event_unique_atomic777, 8
_ZZN2v88internal4wasm10WasmEngine19ReportLiveCodeForGCEPNS0_7IsolateENS0_6VectorIPNS1_8WasmCodeEEEE28trace_event_unique_atomic777:
	.zero	8
	.section	.rodata._ZN2v88internal4wasmL18kAsmjsWasmFeaturesE,"a"
	.align 8
	.type	_ZN2v88internal4wasmL18kAsmjsWasmFeaturesE, @object
	.size	_ZN2v88internal4wasmL18kAsmjsWasmFeaturesE, 13
_ZN2v88internal4wasmL18kAsmjsWasmFeaturesE:
	.zero	13
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 8
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	16
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC4:
	.long	1065353216
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
