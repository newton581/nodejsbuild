	.file	"bailout-reason.cc"
	.text
	.section	.text._ZN2v88internal16GetBailoutReasonENS0_13BailoutReasonE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal16GetBailoutReasonENS0_13BailoutReasonE
	.type	_ZN2v88internal16GetBailoutReasonENS0_13BailoutReasonE, @function
_ZN2v88internal16GetBailoutReasonENS0_13BailoutReasonE:
.LFB2559:
	.cfi_startproc
	endbr64
	movzbl	%dil, %edi
	leaq	_ZZN2v88internal16GetBailoutReasonENS0_13BailoutReasonEE15error_messages_(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	ret
	.cfi_endproc
.LFE2559:
	.size	_ZN2v88internal16GetBailoutReasonENS0_13BailoutReasonE, .-_ZN2v88internal16GetBailoutReasonENS0_13BailoutReasonE
	.section	.text._ZN2v88internal14GetAbortReasonENS0_11AbortReasonE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal14GetAbortReasonENS0_11AbortReasonE
	.type	_ZN2v88internal14GetAbortReasonENS0_11AbortReasonE, @function
_ZN2v88internal14GetAbortReasonENS0_11AbortReasonE:
.LFB2560:
	.cfi_startproc
	endbr64
	movzbl	%dil, %edi
	leaq	_ZZN2v88internal14GetAbortReasonENS0_11AbortReasonEE15error_messages_(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	ret
	.cfi_endproc
.LFE2560:
	.size	_ZN2v88internal14GetAbortReasonENS0_11AbortReasonE, .-_ZN2v88internal14GetAbortReasonENS0_11AbortReasonE
	.section	.text._ZN2v88internal18IsValidAbortReasonEi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18IsValidAbortReasonEi
	.type	_ZN2v88internal18IsValidAbortReasonEi, @function
_ZN2v88internal18IsValidAbortReasonEi:
.LFB2561:
	.cfi_startproc
	endbr64
	cmpl	$58, %edi
	setbe	%al
	ret
	.cfi_endproc
.LFE2561:
	.size	_ZN2v88internal18IsValidAbortReasonEi, .-_ZN2v88internal18IsValidAbortReasonEi
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"no reason"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"32 bit value in register is not zero-extended"
	.align 8
.LC2:
	.string	"API call returned invalid object"
	.align 8
.LC3:
	.string	"Allocating non-empty packed array"
	.align 8
.LC4:
	.string	"Allocation is not double aligned"
	.align 8
.LC5:
	.string	"Expected optimized code cell or optimization sentinel"
	.align 8
.LC6:
	.string	"Expected undefined or cell in register"
	.align 8
.LC7:
	.string	"The function_data field should be a BytecodeArray on interpreter entry"
	.section	.rodata.str1.1
.LC8:
	.string	"Input string too long"
.LC9:
	.string	"Invalid bytecode"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"Cannot advance current bytecode, "
	.align 8
.LC11:
	.string	"Invalid ElementsKind for InternalPackedArray"
	.section	.rodata.str1.1
.LC12:
	.string	"Invalid HandleScope level"
.LC13:
	.string	"Invalid jump table index"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"invalid parameters and registers in generator"
	.align 8
.LC15:
	.string	"Missing bytecode array from function"
	.section	.rodata.str1.1
.LC16:
	.string	"The object is not tagged"
.LC17:
	.string	"The object is tagged"
.LC18:
	.string	"Offset out of range"
.LC19:
	.string	"Operand is a smi"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"Operand is a smi and not a bound function"
	.align 8
.LC21:
	.string	"Operand is a smi and not a constructor"
	.align 8
.LC22:
	.string	"Operand is a smi and not a function"
	.align 8
.LC23:
	.string	"Operand is a smi and not a generator object"
	.align 8
.LC24:
	.string	"Operand is not a bound function"
	.section	.rodata.str1.1
.LC25:
	.string	"Operand is not a constructor"
.LC26:
	.string	"Operand is not a fixed array"
.LC27:
	.string	"Operand is not a function"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"Operand is not a generator object"
	.section	.rodata.str1.1
.LC29:
	.string	"Operand is not a smi"
.LC30:
	.string	"Promise already settled"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"Received invalid return address"
	.align 8
.LC32:
	.string	"Register did not match expected root"
	.align 8
.LC33:
	.string	"Return address not found in frame"
	.align 8
.LC34:
	.string	"Should not directly enter OSR-compiled function"
	.align 8
.LC35:
	.string	"Stack access below stack pointer"
	.section	.rodata.str1.1
.LC36:
	.string	"Stack frame types must match"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"Unaligned cell in write barrier"
	.align 8
.LC38:
	.string	"Unexpected ElementsKind in array constructor"
	.section	.rodata.str1.1
.LC39:
	.string	"Unexpected FPCR mode."
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"Unexpected runtime function id for the InvokeIntrinsic bytecode"
	.align 8
.LC41:
	.string	"Unexpected initial map for Array function"
	.align 8
.LC42:
	.string	"Unexpected initial map for InternalArray function"
	.align 8
.LC43:
	.string	"Unexpected level after return from api call"
	.section	.rodata.str1.1
.LC44:
	.string	"Unexpected negative value"
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"Unexpectedly returned from dropping frames"
	.align 8
.LC46:
	.string	"Unexpectedly returned from a throw"
	.align 8
.LC47:
	.string	"Should not return after throwing a wasm trap"
	.align 8
.LC48:
	.string	"The stack pointer is not the expected value"
	.section	.rodata.str1.1
.LC49:
	.string	"Unexpected value"
.LC50:
	.string	"Unsupported module operation"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"Unsupported non-primitive compare"
	.align 8
.LC52:
	.string	"Wrong address or value passed to RecordWrite"
	.align 8
.LC53:
	.string	"Wrong number of arguments for intrinsic"
	.align 8
.LC54:
	.string	"Wrong value in code start register passed"
	.align 8
.LC55:
	.string	"Wrong context passed to function"
	.align 8
.LC56:
	.string	"Wrong number of arguments for InternalPackedArray"
	.align 8
.LC57:
	.string	"thread_in_wasm flag was already set"
	.align 8
.LC58:
	.string	"thread_in_wasm flag was not set"
	.section	.data.rel.ro.local._ZZN2v88internal14GetAbortReasonENS0_11AbortReasonEE15error_messages_,"aw"
	.align 32
	.type	_ZZN2v88internal14GetAbortReasonENS0_11AbortReasonEE15error_messages_, @object
	.size	_ZZN2v88internal14GetAbortReasonENS0_11AbortReasonEE15error_messages_, 472
_ZZN2v88internal14GetAbortReasonENS0_11AbortReasonEE15error_messages_:
	.quad	.LC0
	.quad	.LC1
	.quad	.LC2
	.quad	.LC3
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"Bailed out due to dependency change"
	.section	.rodata.str1.1
.LC60:
	.string	"Code generation failed"
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"Cyclic object state detected by escape analysis"
	.section	.rodata.str1.1
.LC62:
	.string	"Function is being debugged"
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"Optimized graph construction failed"
	.align 8
.LC64:
	.string	"Function is too big to be optimized"
	.section	.rodata.str1.1
.LC65:
	.string	"LiveEdit"
.LC66:
	.string	"Native function literal"
	.section	.rodata.str1.8
	.align 8
.LC67:
	.string	"Not enough virtual registers (regalloc)"
	.section	.rodata.str1.1
.LC68:
	.string	"Optimization disabled"
	.section	.rodata.str1.8
	.align 8
.LC69:
	.string	"Optimization is always disabled"
	.section	.data.rel.ro.local._ZZN2v88internal16GetBailoutReasonENS0_13BailoutReasonEE15error_messages_,"aw"
	.align 32
	.type	_ZZN2v88internal16GetBailoutReasonENS0_13BailoutReasonEE15error_messages_, @object
	.size	_ZZN2v88internal16GetBailoutReasonENS0_13BailoutReasonEE15error_messages_, 96
_ZZN2v88internal16GetBailoutReasonENS0_13BailoutReasonEE15error_messages_:
	.quad	.LC0
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.quad	.LC68
	.quad	.LC69
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
