	.file	"dtoa.cc"
	.text
	.section	.rodata._ZN2v88internal13DoubleToAsciiEdNS0_8DtoaModeEiNS0_6VectorIcEEPiS4_S4_.str1.1,"aMS",@progbits,1
.LC2:
	.string	"unreachable code"
	.section	.text._ZN2v88internal13DoubleToAsciiEdNS0_8DtoaModeEiNS0_6VectorIcEEPiS4_S4_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal13DoubleToAsciiEdNS0_8DtoaModeEiNS0_6VectorIcEEPiS4_S4_
	.type	_ZN2v88internal13DoubleToAsciiEdNS0_8DtoaModeEiNS0_6VectorIcEEPiS4_S4_, @function
_ZN2v88internal13DoubleToAsciiEdNS0_8DtoaModeEiNS0_6VectorIcEEPiS4_S4_:
.LFB5041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movq	%xmm0, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$24, %rsp
	testq	%rdx, %rdx
	jns	.L2
	xorpd	.LC0(%rip), %xmm0
	movl	$1, %eax
.L2:
	ucomisd	.LC1(%rip), %xmm0
	movl	%eax, (%r8)
	jnp	.L24
.L3:
	cmpl	$2, %r12d
	jne	.L6
	testl	%r14d, %r14d
	jne	.L6
	movb	$0, 0(%r13)
	movl	$0, (%rbx)
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	cmpl	$1, %r12d
	je	.L7
	cmpl	$2, %r12d
	jne	.L25
	movq	16(%rbp), %r9
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%r15, %rcx
	movl	%r14d, %esi
	movl	$1, %edi
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_@PLT
	movsd	-56(%rbp), %xmm0
.L10:
	testb	%al, %al
	jne	.L1
	movq	16(%rbp), %r9
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%r15, %rcx
	movl	%r14d, %esi
	movl	%r12d, %edi
	call	_ZN2v88internal10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_@PLT
	movslq	(%rbx), %rax
	movb	$0, 0(%r13,%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	testl	%r12d, %r12d
	je	.L26
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	jne	.L3
	movl	$48, %eax
	movw	%ax, 0(%r13)
	movq	16(%rbp), %rax
	movl	$1, (%rbx)
	movl	$1, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movq	16(%rbp), %r8
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdx
	movl	%r14d, %edi
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal13FastFixedDtoaEdiNS0_6VectorIcEEPiS3_@PLT
	movsd	-56(%rbp), %xmm0
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L26:
	movq	16(%rbp), %r9
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%r15, %rcx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_@PLT
	movsd	-56(%rbp), %xmm0
	jmp	.L10
	.cfi_endproc
.LFE5041:
	.size	_ZN2v88internal13DoubleToAsciiEdNS0_8DtoaModeEiNS0_6VectorIcEEPiS4_S4_, .-_ZN2v88internal13DoubleToAsciiEdNS0_8DtoaModeEiNS0_6VectorIcEEPiS4_S4_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal13DoubleToAsciiEdNS0_8DtoaModeEiNS0_6VectorIcEEPiS4_S4_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal13DoubleToAsciiEdNS0_8DtoaModeEiNS0_6VectorIcEEPiS4_S4_, @function
_GLOBAL__sub_I__ZN2v88internal13DoubleToAsciiEdNS0_8DtoaModeEiNS0_6VectorIcEEPiS4_S4_:
.LFB5809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5809:
	.size	_GLOBAL__sub_I__ZN2v88internal13DoubleToAsciiEdNS0_8DtoaModeEiNS0_6VectorIcEEPiS4_S4_, .-_GLOBAL__sub_I__ZN2v88internal13DoubleToAsciiEdNS0_8DtoaModeEiNS0_6VectorIcEEPiS4_S4_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal13DoubleToAsciiEdNS0_8DtoaModeEiNS0_6VectorIcEEPiS4_S4_
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
