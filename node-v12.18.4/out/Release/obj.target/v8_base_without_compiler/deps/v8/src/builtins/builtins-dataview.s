	.file	"builtins-dataview.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.rodata._ZN2v88internalL32Builtin_Impl_DataViewConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"DataView"
.LC1:
	.string	"(location_) != nullptr"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL32Builtin_Impl_DataViewConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Builtin_Impl_DataViewConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL32Builtin_Impl_DataViewConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18352:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-8(,%rdi,8), %eax
	addl	$1, 41104(%rdx)
	movq	88(%r12), %rcx
	movslq	%eax, %rdx
	subq	%rdx, %r15
	cmpq	%rcx, (%r15)
	je	.L86
	subl	$8, %eax
	movq	%rsi, %r8
	cltq
	subq	%rax, %r8
	cmpl	$5, %edi
	jle	.L87
	leaq	-8(%rsi), %r13
	cmpl	$6, %edi
	je	.L88
	leaq	-16(%rsi), %r9
	cmpl	$7, %edi
	je	.L89
	leaq	-24(%rsi), %r10
.L14:
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L15
.L16:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$44, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
.L85:
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L8:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L52
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L52:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L90
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	leaq	88(%r12), %r13
	movq	%r13, %r9
.L10:
	movq	0(%r13), %rdx
	movq	%r9, %r10
	testb	$1, %dl
	je	.L16
.L15:
	movq	-1(%rdx), %rax
	cmpw	$1059, 11(%rax)
	jne	.L16
	movq	(%r9), %rax
	testb	$1, %al
	jne	.L17
	testq	%rax, %rax
	js	.L17
.L18:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	.LC0(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$8, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L91
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%r9, %rsi
	movl	$195, %edx
	movq	%r12, %rdi
	movq	%r8, -96(%rbp)
	movq	%r10, -88(%rbp)
	call	_ZN2v88internal6Object14ConvertToIndexEPNS0_7IsolateENS0_6HandleIS1_EENS0_15MessageTemplateE@PLT
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L83
	movq	(%rax), %rax
	testb	$1, %al
	je	.L92
	movsd	7(%rax), %xmm0
	movq	0(%r13), %rdx
.L21:
	movsd	.LC3(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L22
	cvttsd2siq	%xmm0, %rcx
.L23:
	movq	23(%rdx), %r11
	cmpq	%r11, %rcx
	ja	.L93
	movq	(%r10), %rax
	cmpq	%rax, 88(%r12)
	jne	.L25
	subq	%rcx, %r11
	movq	%r11, -96(%rbp)
.L26:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r8, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal8JSObject3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEENS2_INS0_14AllocationSiteEEE@PLT
	xorl	%r15d, %r15d
	movq	-88(%rbp), %rcx
	testq	%rax, %rax
	je	.L83
	movq	%rcx, -88(%rbp)
	movq	%r14, -104(%rbp)
	movq	%r13, -120(%rbp)
	movq	%rbx, -112(%rbp)
	movq	%rax, %rbx
	movq	%r12, -128(%rbp)
	movl	%r15d, %r12d
.L47:
	movq	(%rbx), %r13
	leaq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	movq	(%rax), %r14
	movl	$24, %eax
	movq	-1(%r13), %rsi
	leaq	-1(%r13), %r15
	movzwl	11(%rsi), %edi
	cmpw	$1057, %di
	je	.L43
	movsbl	13(%rsi), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
.L43:
	leal	(%r12,%rax), %esi
	movslq	%esi, %rsi
	addq	%r15, %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L54
	movq	%r14, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rdi
	movq	%rax, -136(%rbp)
	testl	$262144, %edi
	je	.L45
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rsi
	movq	8(%rax), %rdi
.L45:
	andl	$24, %edi
	je	.L54
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L54
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L54:
	addl	$8, %r12d
	cmpl	$16, %r12d
	jne	.L47
	movq	-120(%rbp), %r13
	movq	%rbx, %r8
	movq	-88(%rbp), %rcx
	movq	(%r8), %rdi
	movq	-104(%rbp), %r14
	movq	0(%r13), %r15
	movq	-112(%rbp), %rbx
	movq	-128(%rbp), %r12
	leaq	23(%rdi), %rsi
	movq	%r15, 23(%rdi)
	testb	$1, %r15b
	je	.L53
	movq	%r15, %r9
	andq	$-262144, %r9
	movq	8(%r9), %rax
	movq	%r9, -88(%rbp)
	testl	$262144, %eax
	je	.L49
	movq	%r15, %rdx
	movq	%r8, -128(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r9
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %rsi
	movq	8(%r9), %rax
	movq	-104(%rbp), %rdi
.L49:
	testb	$24, %al
	je	.L53
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L53
	movq	%r15, %rdx
	movq	%r8, -104(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %r8
	movq	-88(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L53:
	movq	(%r8), %rax
	movq	-96(%rbp), %rdi
	movq	%rdi, 39(%rax)
	movq	(%r8), %rax
	movq	%rcx, 31(%rax)
	movq	0(%r13), %rdx
	movq	(%r8), %rax
	addq	31(%rdx), %rcx
	movq	%rcx, 47(%rax)
	movq	(%r8), %r13
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L83:
	movq	312(%r12), %r13
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L22:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rcx
	btcq	$63, %rcx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L93:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r9, %rdx
	movl	$195, %esi
.L84:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L25:
	testb	$1, %al
	jne	.L27
	testq	%rax, %rax
	js	.L27
	testq	%rcx, %rcx
	js	.L28
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rcx, %xmm2
.L30:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L35:
	addsd	%xmm0, %xmm2
	testq	%r11, %r11
	js	.L37
	pxor	%xmm3, %xmm3
	cvtsi2sdq	%r11, %xmm3
.L38:
	comisd	%xmm3, %xmm2
	ja	.L94
	comisd	%xmm1, %xmm0
	jnb	.L40
	cvttsd2siq	%xmm0, %rax
	movq	%rax, -96(%rbp)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$194, %edx
	movq	%r10, %rsi
	movq	%r12, %rdi
	movq	%r8, -104(%rbp)
	movq	%r11, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal6Object14ConvertToIndexEPNS0_7IsolateENS0_6HandleIS1_EENS0_15MessageTemplateE@PLT
	movq	.LC3(%rip), %rdx
	movq	-88(%rbp), %rcx
	testq	%rax, %rax
	movq	-96(%rbp), %r11
	movq	-104(%rbp), %r8
	movq	%rdx, %xmm1
	je	.L83
	movq	(%rax), %rax
	testq	%rcx, %rcx
	js	.L32
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rcx, %xmm2
.L33:
	testb	$1, %al
	je	.L30
	movsd	7(%rax), %xmm0
	jmp	.L35
.L37:
	movq	%r11, %rax
	andl	$1, %r11d
	pxor	%xmm3, %xmm3
	shrq	%rax
	orq	%r11, %rax
	cvtsi2sdq	%rax, %xmm3
	addsd	%xmm3, %xmm3
	jmp	.L38
.L40:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rax
	btcq	$63, %rax
	movq	%rax, -96(%rbp)
	jmp	.L26
.L94:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$194, %esi
	jmp	.L84
.L32:
	movq	%rcx, %rdx
	movq	%rcx, %rsi
	pxor	%xmm2, %xmm2
	shrq	%rdx
	andl	$1, %esi
	orq	%rsi, %rdx
	cvtsi2sdq	%rdx, %xmm2
	addsd	%xmm2, %xmm2
	jmp	.L33
.L28:
	movq	%rcx, %rdx
	movq	%rcx, %rsi
	pxor	%xmm2, %xmm2
	shrq	%rdx
	andl	$1, %esi
	orq	%rsi, %rdx
	cvtsi2sdq	%rdx, %xmm2
	addsd	%xmm2, %xmm2
	jmp	.L30
.L92:
	movq	0(%r13), %rdx
	jmp	.L18
.L90:
	call	__stack_chk_fail@PLT
.L88:
	leaq	88(%r12), %r9
	jmp	.L10
.L89:
	leaq	88(%r12), %r10
	jmp	.L14
	.cfi_endproc
.LFE18352:
	.size	_ZN2v88internalL32Builtin_Impl_DataViewConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL32Builtin_Impl_DataViewConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L101
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L104
.L95:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L95
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL38Builtin_Impl_Stats_DataViewConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC5:
	.string	"V8.Builtin_DataViewConstructor"
	.section	.text._ZN2v88internalL38Builtin_Impl_Stats_DataViewConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Builtin_Impl_Stats_DataViewConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL38Builtin_Impl_Stats_DataViewConstructorEiPmPNS0_7IsolateE:
.LFB18350:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L134
.L106:
	movq	_ZZN2v88internalL38Builtin_Impl_Stats_DataViewConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic21(%rip), %rbx
	testq	%rbx, %rbx
	je	.L135
.L108:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L136
.L110:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL32Builtin_Impl_DataViewConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L137
.L114:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L138
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L139
.L109:
	movq	%rbx, _ZZN2v88internalL38Builtin_Impl_Stats_DataViewConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic21(%rip)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L136:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L140
.L111:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L112
	movq	(%rdi), %rax
	call	*8(%rax)
.L112:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L113
	movq	(%rdi), %rax
	call	*8(%rax)
.L113:
	leaq	.LC5(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L137:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L134:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$735, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L140:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L139:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L109
.L138:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18350:
	.size	_ZN2v88internalL38Builtin_Impl_Stats_DataViewConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL38Builtin_Impl_Stats_DataViewConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Builtin_DataViewConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Builtin_DataViewConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Builtin_DataViewConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Builtin_DataViewConstructorEiPmPNS0_7IsolateE:
.LFB18351:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L145
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL32Builtin_Impl_DataViewConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore 6
	jmp	_ZN2v88internalL38Builtin_Impl_Stats_DataViewConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18351:
	.size	_ZN2v88internal27Builtin_DataViewConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal27Builtin_DataViewConstructorEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal27Builtin_DataViewConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal27Builtin_DataViewConstructorEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal27Builtin_DataViewConstructorEiPmPNS0_7IsolateE:
.LFB22090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22090:
	.size	_GLOBAL__sub_I__ZN2v88internal27Builtin_DataViewConstructorEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal27Builtin_DataViewConstructorEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal27Builtin_DataViewConstructorEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL38Builtin_Impl_Stats_DataViewConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic21,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Builtin_Impl_Stats_DataViewConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic21, @object
	.size	_ZZN2v88internalL38Builtin_Impl_Stats_DataViewConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic21, 8
_ZZN2v88internalL38Builtin_Impl_Stats_DataViewConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic21:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC3:
	.long	0
	.long	1138753536
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
