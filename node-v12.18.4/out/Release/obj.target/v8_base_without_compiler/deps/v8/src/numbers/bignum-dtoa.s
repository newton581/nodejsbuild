	.file	"bignum-dtoa.cc"
	.text
	.section	.text._ZN2v88internalL21GenerateCountedDigitsEiPiPNS0_6BignumES3_NS0_6VectorIcEES1_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL21GenerateCountedDigitsEiPiPNS0_6BignumES3_NS0_6VectorIcEES1_.isra.0, @function
_ZN2v88internalL21GenerateCountedDigitsEiPiPNS0_6BignumES3_NS0_6VectorIcEES1_.isra.0:
.LFB5830:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	leal	-1(%rdi), %ecx
	pushq	%r14
	movslq	%ecx, %rax
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	addq	%r8, %rax
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	%edi, -52(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%r9, -64(%rbp)
	movl	%ecx, -56(%rbp)
	movq	%rax, -72(%rbp)
	testl	%ecx, %ecx
	jle	.L2
	leal	-2(%rdi), %eax
	movq	%r8, %rbx
	leaq	1(%r8,%rax), %r13
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%r15, %rsi
	movq	%r12, %rdi
	addq	$1, %rbx
	call	_ZN2v88internal6Bignum21DivideModuloIntBignumERKS1_@PLT
	movl	$10, %esi
	movq	%r12, %rdi
	addl	$48, %eax
	movb	%al, -1(%rbx)
	call	_ZN2v88internal6Bignum16MultiplyByUInt32Ej@PLT
	cmpq	%r13, %rbx
	jne	.L4
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal6Bignum21DivideModuloIntBignumERKS1_@PLT
	movq	%r12, %rsi
	movq	%r12, %rdi
	movq	%r15, %rdx
	movl	%eax, %ebx
	call	_ZN2v88internal6Bignum11PlusCompareERKS1_S3_S3_@PLT
	movl	-56(%rbp), %r12d
	movl	%eax, %r8d
	leal	49(%rbx), %eax
	addl	$48, %ebx
	testl	%r8d, %r8d
	cmovns	%eax, %ebx
	movq	-72(%rbp), %rax
	movb	%bl, (%rax)
	subl	%eax, %r12d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L17:
	movb	$48, (%rax)
	addb	$1, -1(%rax)
	subq	$1, %rax
	leal	(%r12,%rax), %edx
	testl	%edx, %edx
	jle	.L5
.L7:
	cmpb	$58, (%rax)
	je	.L17
.L5:
	cmpb	$58, (%r14)
	jne	.L8
	movq	-80(%rbp), %rax
	movb	$49, (%r14)
	addl	$1, (%rax)
.L8:
	movq	-64(%rbp), %rax
	movl	-52(%rbp), %ecx
	movl	%ecx, (%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	%rdx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal6Bignum21DivideModuloIntBignumERKS1_@PLT
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal6Bignum11PlusCompareERKS1_S3_S3_@PLT
	movl	%eax, %r8d
	leal	49(%rbx), %eax
	addl	$48, %ebx
	testl	%r8d, %r8d
	cmovns	%eax, %ebx
	movq	-72(%rbp), %rax
	movb	%bl, (%rax)
	jmp	.L5
	.cfi_endproc
.LFE5830:
	.size	_ZN2v88internalL21GenerateCountedDigitsEiPiPNS0_6BignumES3_NS0_6VectorIcEES1_.isra.0, .-_ZN2v88internalL21GenerateCountedDigitsEiPiPNS0_6BignumES3_NS0_6VectorIcEES1_.isra.0
	.section	.rodata._ZN2v88internal10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_.str1.1,"aMS",@progbits,1
.LC5:
	.string	"unreachable code"
	.section	.text._ZN2v88internal10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_
	.type	_ZN2v88internal10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_, @function
_ZN2v88internal10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_:
.LFB5050:
	.cfi_startproc
	endbr64
	movabsq	$4503599627370495, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%xmm0, %r14
	pushq	%r13
	andq	%r14, %rcx
	pushq	%r12
	pushq	%rbx
	subq	$2264, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edi, -2236(%rbp)
	movl	%esi, -2276(%rbp)
	movq	%rdx, -2232(%rbp)
	movq	%r9, -2248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -2272(%rbp)
	movabsq	$9218868437227405312, %rax
	andq	%r14, %rax
	movq	%rax, -2288(%rbp)
	jne	.L101
	movabsq	$4503599627370496, %rsi
	movq	%rcx, %rax
	movq	%rcx, %rdx
	notq	%rax
	movq	%rax, -2256(%rbp)
	movl	$-1074, %eax
	.p2align 4,,10
	.p2align 3
.L21:
	addq	%rdx, %rdx
	subl	$1, %eax
	testq	%rsi, %rdx
	je	.L21
	movq	%rcx, %r8
.L20:
	addl	$52, %eax
	pxor	%xmm0, %xmm0
	movsd	.LC3(%rip), %xmm3
	movsd	.LC2(%rip), %xmm4
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LC0(%rip), %xmm0
	subsd	.LC1(%rip), %xmm0
	movapd	%xmm0, %xmm2
	movapd	%xmm0, %xmm1
	andpd	%xmm3, %xmm2
	ucomisd	%xmm2, %xmm4
	jbe	.L22
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm2, %xmm2
	movsd	.LC4(%rip), %xmm4
	andnpd	%xmm0, %xmm3
	cvtsi2sdq	%rax, %xmm2
	cmpnlesd	%xmm2, %xmm1
	andpd	%xmm4, %xmm1
	addsd	%xmm2, %xmm1
	orpd	%xmm3, %xmm1
.L22:
	cvttsd2sil	%xmm1, %eax
	cmpl	$1, -2236(%rbp)
	movl	%eax, -2240(%rbp)
	jne	.L23
	notl	%eax
	cmpl	-2276(%rbp), %eax
	jg	.L102
.L23:
	leaq	-2224(%rbp), %r12
	leaq	-1680(%rbp), %r13
	movq	%r8, -2304(%rbp)
	movq	%r12, %rdi
	movq	%rcx, -2296(%rbp)
	leaq	-1136(%rbp), %rbx
	call	_ZN2v88internal6BignumC1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal6BignumC1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal6BignumC1Ev@PLT
	leaq	-592(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -2264(%rbp)
	call	_ZN2v88internal6BignumC1Ev@PLT
	cmpq	$0, -2288(%rbp)
	movq	-2296(%rbp), %rcx
	movq	-2304(%rbp), %r8
	je	.L25
	shrq	$52, %r14
	andl	$2047, %r14d
	leal	-1075(%r14), %r9d
	cmpl	$1074, %r14d
	jg	.L103
	movl	$1075, %eax
	subl	%r14d, %eax
	movl	%eax, %r14d
	movl	-2240(%rbp), %eax
	testl	%eax, %eax
	jns	.L58
.L59:
	movl	-2240(%rbp), %edx
	movl	$10, %esi
	movq	%r12, %rdi
	movq	%r8, -2304(%rbp)
	movq	%rcx, -2296(%rbp)
	negl	%edx
	call	_ZN2v88internal6Bignum17AssignPowerUInt16Eti@PLT
	movl	-2236(%rbp), %ecx
	movq	-2304(%rbp), %r8
	testl	%ecx, %ecx
	je	.L104
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Bignum16MultiplyByUInt64Em@PLT
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Bignum12AssignUInt16Et@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L101:
	movabsq	$4503599627370496, %r8
	addq	%rcx, %r8
	movq	%r8, %rax
	notq	%rax
	movq	%rax, -2256(%rbp)
	movq	%xmm0, %rax
	shrq	$52, %rax
	andl	$2047, %eax
	subl	$1075, %eax
	jmp	.L20
.L25:
	movl	-2240(%rbp), %edx
	movl	$1074, %r14d
	testl	%edx, %edx
	js	.L59
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%r8, %rsi
	movq	%r12, %rdi
	movq	%rcx, -2288(%rbp)
	call	_ZN2v88internal6Bignum12AssignUInt64Em@PLT
	movl	-2240(%rbp), %edx
	movl	$10, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Bignum17AssignPowerUInt16Eti@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	movl	-2236(%rbp), %esi
	testl	%esi, %esi
	je	.L105
.L28:
	movzbl	-2256(%rbp), %eax
	movq	-2264(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	andl	$1, %eax
	movb	%al, -2256(%rbp)
	je	.L32
	call	_ZN2v88internal6Bignum11PlusCompareERKS1_S3_S3_@PLT
	notl	%eax
	shrl	$31, %eax
	testb	%al, %al
	je	.L34
.L109:
	movl	-2240(%rbp), %eax
	movq	-2248(%rbp), %rcx
	addl	$1, %eax
	movl	%eax, (%rcx)
.L35:
	movl	-2236(%rbp), %eax
	cmpl	$1, %eax
	je	.L37
.L110:
	cmpl	$2, %eax
	jne	.L106
	movq	-2232(%rbp), %rbx
	movq	%r15, %r9
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	-2248(%rbp), %rsi
	movl	-2276(%rbp), %edi
	movq	%rbx, %r8
	call	_ZN2v88internalL21GenerateCountedDigitsEiPiPNS0_6BignumES3_NS0_6VectorIcEES1_.isra.0
	movslq	(%r15), %rax
	addq	%rbx, %rax
	movq	%rax, -2272(%rbp)
.L54:
	movb	$0, (%rax)
.L18:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L107
	addq	$2264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L108
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	call	_ZN2v88internal6Bignum11PlusCompareERKS1_S3_S3_@PLT
	testl	%eax, %eax
	setg	%al
	testb	%al, %al
	jne	.L109
.L34:
	movq	-2248(%rbp), %rax
	movl	-2240(%rbp), %ecx
	movl	$10, %esi
	movq	%r12, %rdi
	movl	%ecx, (%rax)
	call	_ZN2v88internal6Bignum16MultiplyByUInt32Ej@PLT
	movq	-2264(%rbp), %r14
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal6Bignum7CompareERKS1_S3_@PLT
	movl	$10, %esi
	movq	%rbx, %rdi
	testl	%eax, %eax
	jne	.L36
	call	_ZN2v88internal6Bignum16MultiplyByUInt32Ej@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Bignum12AssignBignumERKS1_@PLT
	movl	-2236(%rbp), %eax
	cmpl	$1, %eax
	jne	.L110
	.p2align 4,,10
	.p2align 3
.L37:
	movq	-2248(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, %edx
	negl	%edx
	cmpl	%edx, -2276(%rbp)
	jl	.L111
	je	.L112
	movq	-2232(%rbp), %rbx
	movq	%r15, %r9
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	-2276(%rbp), %edi
	movq	-2248(%rbp), %rsi
	movq	%rbx, %r8
	addl	%eax, %edi
	call	_ZN2v88internalL21GenerateCountedDigitsEiPiPNS0_6BignumES3_NS0_6VectorIcEES1_.isra.0
	movslq	(%r15), %rax
	addq	%rbx, %rax
	movq	%rax, -2272(%rbp)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L108:
	movq	-2264(%rbp), %r14
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal6Bignum7CompareERKS1_S3_@PLT
	movl	$0, (%r15)
	testl	%eax, %eax
	cmove	%rbx, %r14
.L48:
	cmpb	$0, -2256(%rbp)
	jne	.L41
	cmpq	%rbx, %r14
	je	.L44
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L113:
	testb	%r14b, %r14b
	jne	.L49
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Bignum16MultiplyByUInt32Ej@PLT
	movl	$10, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Bignum16MultiplyByUInt32Ej@PLT
.L44:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Bignum21DivideModuloIntBignumERKS1_@PLT
	movq	-2232(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r8d
	movslq	(%r15), %rax
	leal	1(%rax), %edx
	movl	%edx, (%r15)
	leal	48(%r8), %edx
	movb	%dl, (%rcx,%rax)
	call	_ZN2v88internal6Bignum7CompareERKS1_S3_@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	shrl	$31, %eax
	movb	%al, -2236(%rbp)
	movl	%eax, %r14d
	call	_ZN2v88internal6Bignum11PlusCompareERKS1_S3_S3_@PLT
	testl	%eax, %eax
	setg	%dl
	jle	.L113
.L43:
	cmpb	$0, -2236(%rbp)
	jne	.L63
.L53:
	movl	(%r15), %eax
	movq	-2232(%rbp), %rbx
	subl	$1, %eax
	cltq
	addb	$1, (%rbx,%rax)
.L100:
	movslq	(%r15), %rax
.L51:
	addq	-2232(%rbp), %rax
	movq	%rax, -2272(%rbp)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L103:
	movabsq	$4503599627370496, %rsi
	movq	%r12, %rdi
	movl	%r9d, -2288(%rbp)
	addq	%rcx, %rsi
	call	_ZN2v88internal6Bignum12AssignUInt64Em@PLT
	movl	-2288(%rbp), %r9d
	movq	%r12, %rdi
	movl	%r9d, %esi
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	movl	-2240(%rbp), %edx
	movq	%r13, %rdi
	movl	$10, %esi
	call	_ZN2v88internal6Bignum17AssignPowerUInt16Eti@PLT
	movl	-2236(%rbp), %edi
	testl	%edi, %edi
	jne	.L28
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	movq	-2264(%rbp), %r14
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Bignum12AssignUInt16Et@PLT
	movl	-2288(%rbp), %r9d
	movq	%r14, %rdi
	movl	%r9d, %esi
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Bignum12AssignUInt16Et@PLT
	movl	-2288(%rbp), %r9d
	movq	%rbx, %rdi
	movl	%r9d, %esi
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	movq	-2296(%rbp), %rcx
	testq	%rcx, %rcx
	jne	.L28
.L30:
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	movq	-2264(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L41:
	cmpq	%rbx, %r14
	je	.L47
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L114:
	testb	%r14b, %r14b
	jne	.L63
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Bignum16MultiplyByUInt32Ej@PLT
	movl	$10, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Bignum16MultiplyByUInt32Ej@PLT
.L47:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Bignum21DivideModuloIntBignumERKS1_@PLT
	movq	-2232(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r8d
	movslq	(%r15), %rax
	leal	1(%rax), %edx
	movl	%edx, (%r15)
	leal	48(%r8), %edx
	movb	%dl, (%rcx,%rax)
	call	_ZN2v88internal6Bignum7CompareERKS1_S3_@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	testl	%eax, %eax
	setle	%r14b
	movb	%r14b, -2236(%rbp)
	call	_ZN2v88internal6Bignum11PlusCompareERKS1_S3_S3_@PLT
	notl	%eax
	shrl	$31, %eax
	movl	%eax, %edx
	testb	%al, %al
	je	.L114
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L102:
	movq	-2232(%rbp), %rax
	movl	-2276(%rbp), %esi
	movb	$0, (%rax)
	movq	-2248(%rbp), %rax
	negl	%esi
	movl	$0, (%r15)
	movl	%esi, (%rax)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L36:
	call	_ZN2v88internal6Bignum16MultiplyByUInt32Ej@PLT
	movq	-2264(%rbp), %rdi
	movl	$10, %esi
	call	_ZN2v88internal6Bignum16MultiplyByUInt32Ej@PLT
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L111:
	movl	-2276(%rbp), %esi
	movq	-2248(%rbp), %rax
	negl	%esi
	movl	%esi, (%rax)
	movq	-2272(%rbp), %rax
	movl	$0, (%r15)
	jmp	.L54
.L104:
	movq	-2264(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal6Bignum12AssignBignumERKS1_@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Bignum12AssignBignumERKS1_@PLT
	movq	-2304(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal6Bignum16MultiplyByUInt64Em@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Bignum12AssignUInt16Et@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	movq	-2296(%rbp), %rcx
	testq	%rcx, %rcx
	jne	.L28
	movabsq	$4503599627370496, %rax
	cmpq	%rax, -2288(%rbp)
	je	.L28
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	movq	-2264(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	jmp	.L28
.L63:
	testb	%dl, %dl
	jne	.L115
.L49:
	cmpb	$0, -2236(%rbp)
	jne	.L100
	jmp	.L53
.L46:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Bignum21DivideModuloIntBignumERKS1_@PLT
	movq	-2232(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r8d
	movslq	(%r15), %rax
	leal	1(%rax), %edx
	movl	%edx, (%r15)
	leal	48(%r8), %edx
	movb	%dl, (%rcx,%rax)
	call	_ZN2v88internal6Bignum7CompareERKS1_S3_@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	testl	%eax, %eax
	setle	-2236(%rbp)
	call	_ZN2v88internal6Bignum11PlusCompareERKS1_S3_S3_@PLT
	notl	%eax
	shrl	$31, %eax
	cmpb	$0, -2236(%rbp)
	movl	%eax, %edx
	jne	.L63
	testb	%al, %al
	jne	.L53
.L98:
	movq	%r12, %rdi
	movl	$10, %esi
	call	_ZN2v88internal6Bignum16MultiplyByUInt32Ej@PLT
	movq	%rbx, %rdi
	movl	$10, %esi
	call	_ZN2v88internal6Bignum16MultiplyByUInt32Ej@PLT
	movl	$10, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Bignum16MultiplyByUInt32Ej@PLT
	jmp	.L48
.L42:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Bignum21DivideModuloIntBignumERKS1_@PLT
	movq	-2232(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r8d
	movslq	(%r15), %rax
	leal	1(%rax), %edx
	movl	%edx, (%r15)
	leal	48(%r8), %edx
	movb	%dl, (%rcx,%rax)
	call	_ZN2v88internal6Bignum7CompareERKS1_S3_@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	shrl	$31, %eax
	movb	%al, -2236(%rbp)
	movl	%eax, -2240(%rbp)
	call	_ZN2v88internal6Bignum11PlusCompareERKS1_S3_S3_@PLT
	testl	%eax, %eax
	setg	%dl
	jg	.L43
	movl	-2240(%rbp), %ecx
	testb	%cl, %cl
	je	.L98
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Bignum9ShiftLeftEi@PLT
	movq	-2264(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Bignum12AssignUInt16Et@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Bignum12AssignUInt16Et@PLT
	movq	-2288(%rbp), %rcx
	testq	%rcx, %rcx
	jne	.L28
	jmp	.L30
.L115:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Bignum11PlusCompareERKS1_S3_S3_@PLT
	movl	%eax, %edx
	movslq	(%r15), %rax
	testl	%edx, %edx
	js	.L51
	leal	-1(%rax), %ecx
	movslq	%ecx, %rcx
	addq	-2232(%rbp), %rcx
	movzbl	(%rcx), %esi
	testl	%edx, %edx
	jne	.L99
	testb	$1, %sil
	je	.L51
.L99:
	addl	$1, %esi
	movb	%sil, (%rcx)
	movslq	(%r15), %rax
	jmp	.L51
.L112:
	movl	$10, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Bignum16MultiplyByUInt32Ej@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Bignum11PlusCompareERKS1_S3_S3_@PLT
	testl	%eax, %eax
	js	.L57
	movq	-2232(%rbp), %rbx
	movq	-2248(%rbp), %rax
	movb	$49, (%rbx)
	movl	$1, (%r15)
	addl	$1, (%rax)
	movslq	(%r15), %rax
	addq	%rbx, %rax
	movq	%rax, -2272(%rbp)
	jmp	.L54
.L57:
	movl	$0, (%r15)
	movq	-2272(%rbp), %rax
	jmp	.L54
.L107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5050:
	.size	_ZN2v88internal10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_, .-_ZN2v88internal10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_, @function
_GLOBAL__sub_I__ZN2v88internal10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_:
.LFB5827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5827:
	.size	_GLOBAL__sub_I__ZN2v88internal10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_, .-_GLOBAL__sub_I__ZN2v88internal10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10BignumDtoaEdNS0_14BignumDtoaModeEiNS0_6VectorIcEEPiS4_
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	1352628734
	.long	1070810131
	.align 8
.LC1:
	.long	3654794683
	.long	1037794527
	.align 8
.LC2:
	.long	0
	.long	1127219200
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC4:
	.long	0
	.long	1072693248
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
