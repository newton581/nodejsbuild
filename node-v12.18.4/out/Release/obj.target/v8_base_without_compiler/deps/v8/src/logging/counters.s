	.file	"counters.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB3457:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE3457:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5068:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5068:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5069:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5069:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci,"axG",@progbits,_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci,comdat
	.p2align 4
	.weak	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci
	.type	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci, @function
_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci:
.LFB9476:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9476:
	.size	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci, .-_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB23033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE23033:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB23186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23186:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB23034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23034:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB23187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE23187:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal10StatsTableC2EPNS0_8CountersE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StatsTableC2EPNS0_8CountersE
	.type	_ZN2v88internal10StatsTableC2EPNS0_8CountersE, @function
_ZN2v88internal10StatsTableC2EPNS0_8CountersE:
.LFB18653:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE18653:
	.size	_ZN2v88internal10StatsTableC2EPNS0_8CountersE, .-_ZN2v88internal10StatsTableC2EPNS0_8CountersE
	.globl	_ZN2v88internal10StatsTableC1EPNS0_8CountersE
	.set	_ZN2v88internal10StatsTableC1EPNS0_8CountersE,_ZN2v88internal10StatsTableC2EPNS0_8CountersE
	.section	.text._ZN2v88internal10StatsTable18SetCounterFunctionEPFPiPKcE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StatsTable18SetCounterFunctionEPFPiPKcE
	.type	_ZN2v88internal10StatsTable18SetCounterFunctionEPFPiPKcE, @function
_ZN2v88internal10StatsTable18SetCounterFunctionEPFPiPKcE:
.LFB18655:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE18655:
	.size	_ZN2v88internal10StatsTable18SetCounterFunctionEPFPiPKcE, .-_ZN2v88internal10StatsTable18SetCounterFunctionEPFPiPKcE
	.section	.text._ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv
	.type	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv, @function
_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv:
.LFB18656:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	8(%rdi), %r8
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L17
	movq	%r8, %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L17:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18656:
	.size	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv, .-_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv
	.section	.text._ZN2v88internal22StatsCounterThreadSafeC2EPNS0_8CountersEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22StatsCounterThreadSafeC2EPNS0_8CountersEPKc
	.type	_ZN2v88internal22StatsCounterThreadSafeC2EPNS0_8CountersEPKc, @function
_ZN2v88internal22StatsCounterThreadSafeC2EPNS0_8CountersEPKc:
.LFB18658:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	$0, 16(%rdi)
	movq	%rdx, %xmm1
	addq	$24, %rdi
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -24(%rdi)
	jmp	_ZN2v84base5MutexC1Ev@PLT
	.cfi_endproc
.LFE18658:
	.size	_ZN2v88internal22StatsCounterThreadSafeC2EPNS0_8CountersEPKc, .-_ZN2v88internal22StatsCounterThreadSafeC2EPNS0_8CountersEPKc
	.globl	_ZN2v88internal22StatsCounterThreadSafeC1EPNS0_8CountersEPKc
	.set	_ZN2v88internal22StatsCounterThreadSafeC1EPNS0_8CountersEPKc,_ZN2v88internal22StatsCounterThreadSafeC2EPNS0_8CountersEPKc
	.section	.text._ZN2v88internal22StatsCounterThreadSafe3SetEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22StatsCounterThreadSafe3SetEi
	.type	_ZN2v88internal22StatsCounterThreadSafe3SetEi, @function
_ZN2v88internal22StatsCounterThreadSafe3SetEi:
.LFB18660:
	.cfi_startproc
	endbr64
	cmpq	$0, 16(%rdi)
	je	.L19
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	24(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rax
	movq	%r13, %rdi
	movl	%r12d, (%rax)
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	ret
	.cfi_endproc
.LFE18660:
	.size	_ZN2v88internal22StatsCounterThreadSafe3SetEi, .-_ZN2v88internal22StatsCounterThreadSafe3SetEi
	.section	.text._ZN2v88internal22StatsCounterThreadSafe9IncrementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22StatsCounterThreadSafe9IncrementEv
	.type	_ZN2v88internal22StatsCounterThreadSafe9IncrementEv, @function
_ZN2v88internal22StatsCounterThreadSafe9IncrementEv:
.LFB18661:
	.cfi_startproc
	endbr64
	cmpq	$0, 16(%rdi)
	je	.L24
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	24(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	addl	$1, (%rax)
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	ret
	.cfi_endproc
.LFE18661:
	.size	_ZN2v88internal22StatsCounterThreadSafe9IncrementEv, .-_ZN2v88internal22StatsCounterThreadSafe9IncrementEv
	.section	.text._ZN2v88internal22StatsCounterThreadSafe9IncrementEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22StatsCounterThreadSafe9IncrementEi
	.type	_ZN2v88internal22StatsCounterThreadSafe9IncrementEi, @function
_ZN2v88internal22StatsCounterThreadSafe9IncrementEi:
.LFB18662:
	.cfi_startproc
	endbr64
	cmpq	$0, 16(%rdi)
	je	.L29
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	24(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rax
	movq	%r13, %rdi
	addl	%r12d, (%rax)
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	ret
	.cfi_endproc
.LFE18662:
	.size	_ZN2v88internal22StatsCounterThreadSafe9IncrementEi, .-_ZN2v88internal22StatsCounterThreadSafe9IncrementEi
	.section	.text._ZN2v88internal22StatsCounterThreadSafe9DecrementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22StatsCounterThreadSafe9DecrementEv
	.type	_ZN2v88internal22StatsCounterThreadSafe9DecrementEv, @function
_ZN2v88internal22StatsCounterThreadSafe9DecrementEv:
.LFB18663:
	.cfi_startproc
	endbr64
	cmpq	$0, 16(%rdi)
	je	.L34
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	24(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	subl	$1, (%rax)
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	ret
	.cfi_endproc
.LFE18663:
	.size	_ZN2v88internal22StatsCounterThreadSafe9DecrementEv, .-_ZN2v88internal22StatsCounterThreadSafe9DecrementEv
	.section	.text._ZN2v88internal22StatsCounterThreadSafe9DecrementEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22StatsCounterThreadSafe9DecrementEi
	.type	_ZN2v88internal22StatsCounterThreadSafe9DecrementEi, @function
_ZN2v88internal22StatsCounterThreadSafe9DecrementEi:
.LFB18664:
	.cfi_startproc
	endbr64
	cmpq	$0, 16(%rdi)
	je	.L39
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	24(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	16(%rbx), %rax
	movq	%r13, %rdi
	subl	%r12d, (%rax)
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	ret
	.cfi_endproc
.LFE18664:
	.size	_ZN2v88internal22StatsCounterThreadSafe9DecrementEi, .-_ZN2v88internal22StatsCounterThreadSafe9DecrementEi
	.section	.text._ZN2v88internal9Histogram9AddSampleEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9Histogram9AddSampleEi
	.type	_ZN2v88internal9Histogram9AddSampleEi, @function
_ZN2v88internal9Histogram9AddSampleEi:
.LFB18665:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %r8
	testq	%r8, %r8
	je	.L44
	movq	32(%rdi), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L44
	movq	%r8, %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L44:
	ret
	.cfi_endproc
.LFE18665:
	.size	_ZN2v88internal9Histogram9AddSampleEi, .-_ZN2v88internal9Histogram9AddSampleEi
	.section	.text._ZNK2v88internal9Histogram15CreateHistogramEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9Histogram15CreateHistogramEv
	.type	_ZNK2v88internal9Histogram15CreateHistogramEv, @function
_ZNK2v88internal9Histogram15CreateHistogramEv:
.LFB18666:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movslq	16(%rdi), %rcx
	movl	12(%rdi), %edx
	movl	8(%rdi), %esi
	movq	32(%rax), %rax
	movq	(%rdi), %r8
	testq	%rax, %rax
	je	.L53
	movq	%r8, %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L53:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18666:
	.size	_ZNK2v88internal9Histogram15CreateHistogramEv, .-_ZNK2v88internal9Histogram15CreateHistogramEv
	.section	.text._ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE
	.type	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE, @function
_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE:
.LFB18667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	cmpq	$0, 24(%rdi)
	je	.L55
	movq	%rsi, %r13
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rax, 0(%r13)
.L55:
	testq	%rbx, %rbx
	je	.L54
	movq	41632(%rbx), %rax
	testq	%rax, %rax
	je	.L54
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rdx
	movq	(%r12), %r12
	cmpq	%rdx, %rax
	je	.L67
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	41016(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L68
.L54:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdx
	movq	%r13, %rdi
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	.cfi_endproc
.LFE18667:
	.size	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE, .-_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE
	.section	.text._ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE
	.type	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE, @function
_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE:
.LFB18668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 24(%rdi)
	je	.L71
	cmpl	$1, 40(%rdi)
	movq	%rsi, %r13
	je	.L93
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-48(%rbp), %rdi
	subq	0(%r13), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v84base9TimeDelta14InMillisecondsEv@PLT
	movq	%rax, %rsi
.L73:
	movq	$0, 0(%r13)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L71
	movq	32(%rbx), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L71
	call	*%rax
.L71:
	testq	%r12, %r12
	je	.L69
	movq	41632(%r12), %rax
	testq	%rax, %rax
	je	.L69
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rdx
	movq	(%rbx), %r13
	cmpq	%rdx, %rax
	je	.L94
	movl	$1, %esi
	movq	%r13, %rdi
	call	*%rax
.L69:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L95
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-48(%rbp), %rdi
	subq	0(%r13), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	%rax, %rsi
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L94:
	movq	41016(%r12), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L69
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L69
.L95:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18668:
	.size	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE, .-_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE
	.section	.text._ZN2v88internal14TimedHistogram13RecordAbandonEPNS_4base12ElapsedTimerEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TimedHistogram13RecordAbandonEPNS_4base12ElapsedTimerEPNS0_7IsolateE
	.type	_ZN2v88internal14TimedHistogram13RecordAbandonEPNS_4base12ElapsedTimerEPNS0_7IsolateE, @function
_ZN2v88internal14TimedHistogram13RecordAbandonEPNS_4base12ElapsedTimerEPNS0_7IsolateE:
.LFB18669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 24(%rdi)
	je	.L98
	movq	$0, (%rsi)
	cmpl	$1, 40(%rdi)
	movabsq	$9223372036854775807, %rax
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	je	.L120
	call	_ZNK2v84base9TimeDelta14InMillisecondsEv@PLT
	movq	%rax, %rsi
.L100:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L98
	movq	32(%rbx), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L98
	call	*%rax
.L98:
	testq	%r12, %r12
	je	.L96
	movq	41632(%r12), %rax
	testq	%rax, %rax
	je	.L96
	leaq	_ZN2v88internal6Logger26DefaultEventLoggerSentinelEPKci(%rip), %rdx
	movq	(%rbx), %r13
	cmpq	%rdx, %rax
	je	.L121
	movl	$1, %esi
	movq	%r13, %rdi
	call	*%rax
.L96:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L122
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	%rax, %rsi
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L121:
	movq	41016(%r12), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L96
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10TimerEventENS1_8StartEndEPKc@PLT
	jmp	.L96
.L122:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18669:
	.size	_ZN2v88internal14TimedHistogram13RecordAbandonEPNS_4base12ElapsedTimerEPNS0_7IsolateE, .-_ZN2v88internal14TimedHistogram13RecordAbandonEPNS_4base12ElapsedTimerEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal8CountersC2EPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"c:V8.WasmGeneratedCodeBytes"
.LC1:
	.string	"c:V8.WasmRelocBytes"
	.section	.rodata._ZN2v88internal8CountersC2EPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"c:V8.WasmLazilyCompiledFunctions"
	.section	.rodata._ZN2v88internal8CountersC2EPNS0_7IsolateE.str1.1
.LC3:
	.string	"c:V8.LiftoffCompiledFunctions"
	.section	.rodata._ZN2v88internal8CountersC2EPNS0_7IsolateE.str1.8
	.align 8
.LC4:
	.string	"c:V8.LiftoffUnsupportedFunctions"
	.section	.rodata._ZN2v88internal8CountersC2EPNS0_7IsolateE.str1.1
.LC5:
	.string	"V8.GCContext"
.LC6:
	.string	"V8.GCIdleNotification"
.LC7:
	.string	"V8.GCIncrementalMarking"
.LC8:
	.string	"V8.GCIncrementalMarkingStart"
	.section	.rodata._ZN2v88internal8CountersC2EPNS0_7IsolateE.str1.8
	.align 8
.LC9:
	.string	"V8.GCIncrementalMarkingFinalize"
	.section	.rodata._ZN2v88internal8CountersC2EPNS0_7IsolateE.str1.1
.LC10:
	.string	"V8.GCLowMemoryNotification"
.LC11:
	.string	"V8.CollectSourcePositions"
.LC12:
	.string	"V8.CompileMicroSeconds"
.LC13:
	.string	"V8.CompileEvalMicroSeconds"
	.section	.rodata._ZN2v88internal8CountersC2EPNS0_7IsolateE.str1.8
	.align 8
.LC14:
	.string	"V8.CompileSerializeMicroSeconds"
	.align 8
.LC15:
	.string	"V8.CompileDeserializeMicroSeconds"
	.section	.rodata._ZN2v88internal8CountersC2EPNS0_7IsolateE.str1.1
.LC16:
	.string	"V8.CompileScriptMicroSeconds"
.LC17:
	.string	"V8.Execute"
	.section	.rodata._ZN2v88internal8CountersC2EPNS0_7IsolateE.str1.8
	.align 8
.LC18:
	.string	"V8.AsmWasmTranslationMicroSeconds"
	.align 8
.LC19:
	.string	"V8.WasmLazyCompilationMicroSeconds"
	.section	.rodata._ZN2v88internal8CountersC2EPNS0_7IsolateE.str1.1
.LC20:
	.string	"V8.CompileLazyMicroSeconds"
	.section	.rodata._ZN2v88internal8CountersC2EPNS0_7IsolateE.str1.8
	.align 8
.LC21:
	.string	"V8.MemoryExternalFragmentationTotal"
	.align 8
.LC22:
	.string	"V8.MemoryExternalFragmentationOldSpace"
	.align 8
.LC23:
	.string	"V8.MemoryExternalFragmentationCodeSpace"
	.align 8
.LC24:
	.string	"V8.MemoryExternalFragmentationMapSpace"
	.align 8
.LC25:
	.string	"V8.MemoryExternalFragmentationLoSpace"
	.align 8
.LC26:
	.string	"V8.MemoryHeapSampleTotalCommitted"
	.section	.rodata._ZN2v88internal8CountersC2EPNS0_7IsolateE.str1.1
.LC27:
	.string	"V8.MemoryHeapSampleTotalUsed"
	.section	.rodata._ZN2v88internal8CountersC2EPNS0_7IsolateE.str1.8
	.align 8
.LC28:
	.string	"V8.MemoryHeapSampleMapSpaceCommitted"
	.align 8
.LC29:
	.string	"V8.MemoryHeapSampleCodeSpaceCommitted"
	.align 8
.LC30:
	.string	"V8.MemoryHeapSampleMaximumCommitted"
	.section	.text._ZN2v88internal8CountersC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8CountersC2EPNS0_7IsolateE
	.type	_ZN2v88internal8CountersC2EPNS0_7IsolateE, @function
_ZN2v88internal8CountersC2EPNS0_7IsolateE:
.LFB18695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rsi, 16(%rdi)
	movq	%rax, 8144(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 2256(%rdi)
	movq	$0, 2312(%rdi)
	movq	$0, 2368(%rdi)
	movq	$0, 2424(%rdi)
	movq	$0, 2480(%rdi)
	movq	$0, 2536(%rdi)
	movq	$0, 2592(%rdi)
	movq	$0, 2648(%rdi)
	movq	$0, 2704(%rdi)
	movq	$0, 2760(%rdi)
	movq	$0, 2816(%rdi)
	movq	$0, 2872(%rdi)
	movq	$0, 2928(%rdi)
	movq	$0, 2984(%rdi)
	movq	$0, 3040(%rdi)
	movq	$0, 5488(%rdi)
	movq	$0, 8152(%rdi)
	movq	%rdi, 8136(%rdi)
	leaq	8160(%rdi), %rdi
	movups	%xmm0, -8160(%rdi)
	movups	%xmm0, -8136(%rdi)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	.LC1(%rip), %rax
	movq	%rbx, 8200(%rbx)
	leaq	8224(%rbx), %rdi
	movq	%rax, 8208(%rbx)
	movq	$0, 8216(%rbx)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	.LC2(%rip), %rax
	movq	%rbx, 8264(%rbx)
	leaq	8288(%rbx), %rdi
	movq	%rax, 8272(%rbx)
	movq	$0, 8280(%rbx)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	.LC3(%rip), %rax
	movq	%rbx, 8328(%rbx)
	leaq	8352(%rbx), %rdi
	movq	%rax, 8336(%rbx)
	movq	$0, 8344(%rbx)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	.LC4(%rip), %rax
	movq	%rbx, 8392(%rbx)
	leaq	8416(%rbx), %rdi
	movq	%rax, 8400(%rbx)
	movq	$0, 8408(%rbx)
	call	_ZN2v84base5MutexC1Ev@PLT
	movb	$0, 23256(%rbx)
	pxor	%xmm0, %xmm0
	leaq	23264(%rbx), %rax
	movups	%xmm0, 23240(%rbx)
	leaq	50888(%rbx), %rdi
	movl	$-1, 23260(%rbx)
	.p2align 4,,10
	.p2align 3
.L124:
	movq	$0, (%rax)
	addq	$24, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rdi, %rax
	jne	.L124
	leaq	_ZZN2v88internal16RuntimeCallStatsC4EvE6kNames(%rip), %rdx
	leaq	23272(%rbx), %rax
	pxor	%xmm0, %xmm0
	leaq	9208(%rdx), %rsi
	.p2align 4,,10
	.p2align 3
.L125:
	movq	(%rdx), %rcx
	addq	$8, %rdx
	movups	%xmm0, (%rax)
	addq	$24, %rax
	movq	%rcx, -32(%rax)
	cmpq	%rdx, %rsi
	jne	.L125
	call	_ZN2v84base5MutexC1Ev@PLT
	movb	$0, 50952(%rbx)
	pxor	%xmm0, %xmm0
	leaq	_ZZN2v88internal8CountersC4EPNS0_7IsolateEE11kHistograms(%rip), %rax
	movb	$0, 50956(%rbx)
	leaq	1728(%rax), %r9
	movq	$0, 50944(%rbx)
	movups	%xmm0, 50928(%rbx)
	.p2align 4,,10
	.p2align 3
.L126:
	movq	(%rax), %rdx
	movl	24(%rax), %ecx
	addq	$32, %rax
	movl	-12(%rax), %esi
	movl	-16(%rax), %edi
	movq	-24(%rax), %r8
	addq	%rbx, %rdx
	movl	%edi, 8(%rdx)
	movq	%r8, (%rdx)
	movl	%esi, 12(%rdx)
	movl	%ecx, 16(%rdx)
	movq	$0, 24(%rdx)
	movq	%rbx, 32(%rdx)
	cmpq	%r9, %rax
	jne	.L126
	leaq	.LC6(%rip), %rdi
	leaq	.LC5(%rip), %rax
	movl	$50, 2224(%rbx)
	movq	%rdi, 2264(%rbx)
	leaq	.LC7(%rip), %rdi
	leaq	_ZZN2v88internal8CountersC4EPNS0_7IsolateEE16kTimedHistograms(%rip), %rdx
	movq	%rdi, 2320(%rbx)
	leaq	.LC8(%rip), %rdi
	leaq	1200(%rdx), %r8
	movq	%rdi, 2376(%rbx)
	leaq	.LC9(%rip), %rdi
	movq	%rax, 2208(%rbx)
	movabsq	$42949672960000, %rax
	movq	%rdi, 2432(%rbx)
	leaq	.LC10(%rip), %rdi
	movq	%rax, 2216(%rbx)
	movq	%rax, 2272(%rbx)
	movq	%rax, 2328(%rbx)
	movq	%rax, 2384(%rbx)
	movq	%rax, 2440(%rbx)
	movq	$0, 2232(%rbx)
	movq	%rbx, 2240(%rbx)
	movl	$0, 2248(%rbx)
	movq	$0, 2256(%rbx)
	movl	$50, 2280(%rbx)
	movq	$0, 2288(%rbx)
	movq	%rbx, 2296(%rbx)
	movl	$0, 2304(%rbx)
	movq	$0, 2312(%rbx)
	movl	$50, 2336(%rbx)
	movq	$0, 2344(%rbx)
	movq	%rbx, 2352(%rbx)
	movl	$0, 2360(%rbx)
	movq	$0, 2368(%rbx)
	movl	$50, 2392(%rbx)
	movq	$0, 2400(%rbx)
	movq	%rbx, 2408(%rbx)
	movl	$0, 2416(%rbx)
	movq	$0, 2424(%rbx)
	movl	$50, 2448(%rbx)
	movq	$0, 2456(%rbx)
	movq	%rbx, 2464(%rbx)
	movq	%rdi, 2488(%rbx)
	leaq	.LC12(%rip), %rdi
	movq	%rdi, 2600(%rbx)
	leaq	.LC13(%rip), %rdi
	movq	%rdi, 2656(%rbx)
	leaq	.LC14(%rip), %rdi
	movq	%rax, 2496(%rbx)
	leaq	.LC11(%rip), %rax
	movq	%rdi, 2712(%rbx)
	movabsq	$429496729600000, %rdi
	movq	%rax, 2544(%rbx)
	movabsq	$4294967296000000, %rax
	movq	%rdi, 2720(%rbx)
	leaq	.LC15(%rip), %rdi
	movl	$0, 2472(%rbx)
	movq	$0, 2480(%rbx)
	movl	$50, 2504(%rbx)
	movq	$0, 2512(%rbx)
	movq	%rbx, 2520(%rbx)
	movl	$0, 2528(%rbx)
	movq	$0, 2536(%rbx)
	movq	%rax, 2552(%rbx)
	movl	$50, 2560(%rbx)
	movq	$0, 2568(%rbx)
	movq	%rbx, 2576(%rbx)
	movl	$1, 2584(%rbx)
	movq	$0, 2592(%rbx)
	movq	%rax, 2608(%rbx)
	movl	$50, 2616(%rbx)
	movq	$0, 2624(%rbx)
	movq	%rbx, 2632(%rbx)
	movl	$1, 2640(%rbx)
	movq	$0, 2648(%rbx)
	movq	%rax, 2664(%rbx)
	movl	$50, 2672(%rbx)
	movq	$0, 2680(%rbx)
	movq	%rbx, 2688(%rbx)
	movl	$1, 2696(%rbx)
	movq	$0, 2704(%rbx)
	movl	$50, 2728(%rbx)
	movq	%rdi, 2768(%rbx)
	leaq	.LC16(%rip), %rdi
	movq	%rdi, 2824(%rbx)
	leaq	.LC17(%rip), %rdi
	movq	%rdi, 2880(%rbx)
	leaq	.LC18(%rip), %rdi
	movq	%rdi, 2936(%rbx)
	leaq	.LC19(%rip), %rdi
	movq	$0, 2736(%rbx)
	movq	%rbx, 2744(%rbx)
	movl	$1, 2752(%rbx)
	movq	$0, 2760(%rbx)
	movq	%rax, 2776(%rbx)
	movl	$50, 2784(%rbx)
	movq	$0, 2792(%rbx)
	movq	%rbx, 2800(%rbx)
	movl	$1, 2808(%rbx)
	movq	$0, 2816(%rbx)
	movq	%rax, 2832(%rbx)
	movl	$50, 2840(%rbx)
	movq	$0, 2848(%rbx)
	movq	%rbx, 2856(%rbx)
	movl	$1, 2864(%rbx)
	movq	$0, 2872(%rbx)
	movq	%rax, 2888(%rbx)
	movl	$50, 2896(%rbx)
	movq	$0, 2904(%rbx)
	movq	%rbx, 2912(%rbx)
	movl	$1, 2920(%rbx)
	movq	$0, 2928(%rbx)
	movq	%rax, 2944(%rbx)
	movl	$50, 2952(%rbx)
	movq	$0, 2960(%rbx)
	movq	%rbx, 2968(%rbx)
	movl	$1, 2976(%rbx)
	movq	$0, 2984(%rbx)
	movq	%rdi, 2992(%rbx)
	movq	%rax, 3000(%rbx)
	movl	$50, 3008(%rbx)
	movq	$0, 3016(%rbx)
	movq	%rbx, 3024(%rbx)
	movl	$1, 3032(%rbx)
	movq	$0, 3040(%rbx)
	.p2align 4,,10
	.p2align 3
.L127:
	movq	(%rdx), %rax
	movl	20(%rdx), %ecx
	addq	$24, %rdx
	movl	-8(%rdx), %esi
	movq	-16(%rdx), %rdi
	addq	%rbx, %rax
	movq	%rdi, (%rax)
	movl	$0, 8(%rax)
	movl	%esi, 12(%rax)
	movl	$50, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rbx, 32(%rax)
	movl	%ecx, 40(%rax)
	cmpq	%rdx, %r8
	jne	.L127
	leaq	.LC20(%rip), %rax
	leaq	.LC22(%rip), %rdi
	movl	$50, 5464(%rbx)
	movq	%rax, 5448(%rbx)
	movabsq	$42949672960000000, %rax
	movq	%rax, 5456(%rbx)
	leaq	.LC21(%rip), %rax
	movq	%rdi, 5536(%rbx)
	leaq	.LC23(%rip), %rdi
	movq	%rax, 5496(%rbx)
	movabsq	$433791696896, %rax
	movq	%rdi, 5576(%rbx)
	leaq	.LC24(%rip), %rdi
	movq	%rax, 5504(%rbx)
	movq	%rax, 5544(%rbx)
	movq	%rax, 5584(%rbx)
	movq	%rdi, 5616(%rbx)
	leaq	.LC25(%rip), %rdi
	movq	%rax, 5624(%rbx)
	movq	%rax, 5664(%rbx)
	leaq	.LC26(%rip), %rax
	movq	%rdi, 5656(%rbx)
	leaq	.LC27(%rip), %rdi
	movq	%rax, 5696(%rbx)
	movabsq	$2147483648001000, %rax
	movq	$0, 5472(%rbx)
	movq	%rbx, 5480(%rbx)
	movq	$0, 5488(%rbx)
	movl	$100, 5512(%rbx)
	movq	$0, 5520(%rbx)
	movq	%rbx, 5528(%rbx)
	movl	$100, 5552(%rbx)
	movq	$0, 5560(%rbx)
	movq	%rbx, 5568(%rbx)
	movl	$100, 5592(%rbx)
	movq	$0, 5600(%rbx)
	movq	%rbx, 5608(%rbx)
	movl	$100, 5632(%rbx)
	movq	$0, 5640(%rbx)
	movq	%rbx, 5648(%rbx)
	movl	$100, 5672(%rbx)
	movq	$0, 5680(%rbx)
	movq	%rbx, 5688(%rbx)
	movq	%rax, 5704(%rbx)
	movq	%rdi, 5736(%rbx)
	leaq	.LC28(%rip), %rdi
	movq	%rdi, 5776(%rbx)
	leaq	.LC29(%rip), %rdi
	movq	%rdi, 5816(%rbx)
	leaq	.LC30(%rip), %rdi
	movl	$50, 5712(%rbx)
	movq	$0, 5720(%rbx)
	movq	%rbx, 5728(%rbx)
	movl	$50, 5752(%rbx)
	movq	$0, 5760(%rbx)
	movq	%rbx, 5768(%rbx)
	movl	$50, 5792(%rbx)
	movq	$0, 5800(%rbx)
	movq	%rbx, 5808(%rbx)
	movl	$50, 5832(%rbx)
	movq	$0, 5840(%rbx)
	movq	%rbx, 5848(%rbx)
	movq	%rdi, 5856(%rbx)
	movl	$50, 5872(%rbx)
	movq	$0, 5880(%rbx)
	movq	%rbx, 5888(%rbx)
	movq	%rax, 5744(%rbx)
	movq	%rax, 5784(%rbx)
	movq	%rax, 5824(%rbx)
	movq	%rax, 5864(%rbx)
	leaq	_ZZN2v88internal8CountersC4EPNS0_7IsolateEE14kStatsCounters(%rip), %rax
	leaq	8512(%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L128:
	movq	(%rax), %rdx
	movq	8(%rax), %rcx
	addq	$16, %rax
	addq	%rbx, %rdx
	movq	%rbx, (%rdx)
	movq	%rcx, 8(%rdx)
	movq	$0, 16(%rdx)
	movb	$0, 24(%rdx)
	cmpq	%rsi, %rax
	jne	.L128
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18695:
	.size	_ZN2v88internal8CountersC2EPNS0_7IsolateE, .-_ZN2v88internal8CountersC2EPNS0_7IsolateE
	.globl	_ZN2v88internal8CountersC1EPNS0_7IsolateE
	.set	_ZN2v88internal8CountersC1EPNS0_7IsolateE,_ZN2v88internal8CountersC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal8Counters20ResetCounterFunctionEPFPiPKcE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Counters20ResetCounterFunctionEPFPiPKcE
	.type	_ZN2v88internal8Counters20ResetCounterFunctionEPFPiPKcE, @function
_ZN2v88internal8Counters20ResetCounterFunctionEPFPiPKcE:
.LFB18697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rsi, 24(%rdi)
	movb	$0, 5920(%rdi)
	movb	$0, 5952(%rdi)
	movb	$0, 5984(%rdi)
	movb	$0, 6016(%rdi)
	movb	$0, 6048(%rdi)
	movb	$0, 6080(%rdi)
	movb	$0, 6112(%rdi)
	movb	$0, 6144(%rdi)
	movb	$0, 6176(%rdi)
	movb	$0, 6208(%rdi)
	movb	$0, 6240(%rdi)
	movb	$0, 6272(%rdi)
	movb	$0, 6304(%rdi)
	movb	$0, 6336(%rdi)
	movb	$0, 6368(%rdi)
	movb	$0, 6400(%rdi)
	movb	$0, 6432(%rdi)
	movb	$0, 6464(%rdi)
	movb	$0, 6496(%rdi)
	movb	$0, 6528(%rdi)
	movb	$0, 6560(%rdi)
	movb	$0, 6592(%rdi)
	movb	$0, 6624(%rdi)
	movb	$0, 6656(%rdi)
	movb	$0, 6688(%rdi)
	movb	$0, 6720(%rdi)
	movb	$0, 6752(%rdi)
	movb	$0, 6784(%rdi)
	movb	$0, 6816(%rdi)
	movb	$0, 6848(%rdi)
	movb	$0, 6880(%rdi)
	movb	$0, 6912(%rdi)
	movb	$0, 6944(%rdi)
	movb	$0, 6976(%rdi)
	movb	$0, 7008(%rdi)
	movb	$0, 7040(%rdi)
	movb	$0, 7072(%rdi)
	movb	$0, 7104(%rdi)
	movb	$0, 7136(%rdi)
	movb	$0, 7168(%rdi)
	movb	$0, 7200(%rdi)
	movb	$0, 7232(%rdi)
	movb	$0, 7264(%rdi)
	movb	$0, 7296(%rdi)
	movb	$0, 7328(%rdi)
	movb	$0, 7360(%rdi)
	movb	$0, 7392(%rdi)
	movb	$0, 7424(%rdi)
	movb	$0, 7456(%rdi)
	movb	$0, 7488(%rdi)
	movb	$0, 7520(%rdi)
	movb	$0, 7552(%rdi)
	movb	$0, 7584(%rdi)
	movb	$0, 7616(%rdi)
	movb	$0, 7648(%rdi)
	movb	$0, 7680(%rdi)
	movb	$0, 7712(%rdi)
	movb	$0, 7744(%rdi)
	movb	$0, 7776(%rdi)
	movq	8136(%rbx), %rax
	movq	8144(%rdi), %rdi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L141
	call	*%rax
.L136:
	movq	%rax, 8152(%rbx)
	movq	8200(%rbx), %rax
	movq	8208(%rbx), %rdi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L142
	call	*%rax
.L137:
	movq	%rax, 8216(%rbx)
	movq	8264(%rbx), %rax
	movq	8272(%rbx), %rdi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L143
	call	*%rax
.L138:
	movq	%rax, 8280(%rbx)
	movq	8328(%rbx), %rax
	movq	8336(%rbx), %rdi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L144
	call	*%rax
.L139:
	movq	%rax, 8344(%rbx)
	movq	8392(%rbx), %rax
	movq	8400(%rbx), %rdi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L145
	call	*%rax
.L140:
	movq	%rax, 8408(%rbx)
	movb	$0, 7808(%rbx)
	movb	$0, 7840(%rbx)
	movb	$0, 7872(%rbx)
	movb	$0, 7904(%rbx)
	movb	$0, 7936(%rbx)
	movb	$0, 7968(%rbx)
	movb	$0, 8000(%rbx)
	movb	$0, 8032(%rbx)
	movb	$0, 8064(%rbx)
	movb	$0, 8096(%rbx)
	movb	$0, 8128(%rbx)
	movb	$0, 8512(%rbx)
	movb	$0, 8480(%rbx)
	movb	$0, 8576(%rbx)
	movb	$0, 8544(%rbx)
	movb	$0, 8640(%rbx)
	movb	$0, 8608(%rbx)
	movb	$0, 8704(%rbx)
	movb	$0, 8672(%rbx)
	movb	$0, 8768(%rbx)
	movb	$0, 8736(%rbx)
	movb	$0, 8832(%rbx)
	movb	$0, 8800(%rbx)
	movb	$0, 8896(%rbx)
	movb	$0, 8864(%rbx)
	movb	$0, 8960(%rbx)
	movb	$0, 8928(%rbx)
	movb	$0, 9024(%rbx)
	movb	$0, 8992(%rbx)
	movb	$0, 9088(%rbx)
	movb	$0, 9056(%rbx)
	movb	$0, 9152(%rbx)
	movb	$0, 9120(%rbx)
	movb	$0, 9216(%rbx)
	movb	$0, 9184(%rbx)
	movb	$0, 9280(%rbx)
	movb	$0, 9248(%rbx)
	movb	$0, 9344(%rbx)
	movb	$0, 9312(%rbx)
	movb	$0, 9408(%rbx)
	movb	$0, 9376(%rbx)
	movb	$0, 9472(%rbx)
	movb	$0, 9440(%rbx)
	movb	$0, 9536(%rbx)
	movb	$0, 9504(%rbx)
	movb	$0, 9600(%rbx)
	movb	$0, 9568(%rbx)
	movb	$0, 9664(%rbx)
	movb	$0, 9632(%rbx)
	movb	$0, 9728(%rbx)
	movb	$0, 9696(%rbx)
	movb	$0, 9792(%rbx)
	movb	$0, 9760(%rbx)
	movb	$0, 9856(%rbx)
	movb	$0, 9824(%rbx)
	movb	$0, 9920(%rbx)
	movb	$0, 9888(%rbx)
	movb	$0, 9984(%rbx)
	movb	$0, 9952(%rbx)
	movb	$0, 10048(%rbx)
	movb	$0, 10016(%rbx)
	movb	$0, 10112(%rbx)
	movb	$0, 10080(%rbx)
	movb	$0, 10176(%rbx)
	movb	$0, 10144(%rbx)
	movb	$0, 10240(%rbx)
	movb	$0, 10208(%rbx)
	movb	$0, 10304(%rbx)
	movb	$0, 10272(%rbx)
	movb	$0, 10368(%rbx)
	movb	$0, 10336(%rbx)
	movb	$0, 10432(%rbx)
	movb	$0, 10400(%rbx)
	movb	$0, 10496(%rbx)
	movb	$0, 10464(%rbx)
	movb	$0, 10560(%rbx)
	movb	$0, 10528(%rbx)
	movb	$0, 10624(%rbx)
	movb	$0, 10592(%rbx)
	movb	$0, 10688(%rbx)
	movb	$0, 10656(%rbx)
	movb	$0, 10752(%rbx)
	movb	$0, 10720(%rbx)
	movb	$0, 10816(%rbx)
	movb	$0, 10784(%rbx)
	movb	$0, 10880(%rbx)
	movb	$0, 10848(%rbx)
	movb	$0, 10944(%rbx)
	movb	$0, 10912(%rbx)
	movb	$0, 11008(%rbx)
	movb	$0, 10976(%rbx)
	movb	$0, 11072(%rbx)
	movb	$0, 11040(%rbx)
	movb	$0, 11136(%rbx)
	movb	$0, 11104(%rbx)
	movb	$0, 11200(%rbx)
	movb	$0, 11168(%rbx)
	movb	$0, 11264(%rbx)
	movb	$0, 11232(%rbx)
	movb	$0, 11328(%rbx)
	movb	$0, 11296(%rbx)
	movb	$0, 11392(%rbx)
	movb	$0, 11360(%rbx)
	movb	$0, 11456(%rbx)
	movb	$0, 11424(%rbx)
	movb	$0, 11520(%rbx)
	movb	$0, 11488(%rbx)
	movb	$0, 11584(%rbx)
	movb	$0, 11552(%rbx)
	movb	$0, 11648(%rbx)
	movb	$0, 11616(%rbx)
	movb	$0, 11712(%rbx)
	movb	$0, 11680(%rbx)
	movb	$0, 11776(%rbx)
	movb	$0, 11744(%rbx)
	movb	$0, 11840(%rbx)
	movb	$0, 11808(%rbx)
	movb	$0, 11904(%rbx)
	movb	$0, 11872(%rbx)
	movb	$0, 11968(%rbx)
	movb	$0, 11936(%rbx)
	movb	$0, 12032(%rbx)
	movb	$0, 12000(%rbx)
	movb	$0, 12096(%rbx)
	movb	$0, 12064(%rbx)
	movb	$0, 12160(%rbx)
	movb	$0, 12128(%rbx)
	movb	$0, 12224(%rbx)
	movb	$0, 12192(%rbx)
	movb	$0, 12288(%rbx)
	movb	$0, 12256(%rbx)
	movb	$0, 12352(%rbx)
	movb	$0, 12320(%rbx)
	movb	$0, 12416(%rbx)
	movb	$0, 12384(%rbx)
	movb	$0, 12480(%rbx)
	movb	$0, 12448(%rbx)
	movb	$0, 12544(%rbx)
	movb	$0, 12512(%rbx)
	movb	$0, 12608(%rbx)
	movb	$0, 12576(%rbx)
	movb	$0, 12672(%rbx)
	movb	$0, 12640(%rbx)
	movb	$0, 12736(%rbx)
	movb	$0, 12704(%rbx)
	movb	$0, 12800(%rbx)
	movb	$0, 12768(%rbx)
	movb	$0, 12864(%rbx)
	movb	$0, 12832(%rbx)
	movb	$0, 12928(%rbx)
	movb	$0, 12896(%rbx)
	movb	$0, 12992(%rbx)
	movb	$0, 12960(%rbx)
	movb	$0, 13056(%rbx)
	movb	$0, 13024(%rbx)
	movb	$0, 13120(%rbx)
	movb	$0, 13088(%rbx)
	movb	$0, 13184(%rbx)
	movb	$0, 13152(%rbx)
	movb	$0, 13248(%rbx)
	movb	$0, 13216(%rbx)
	movb	$0, 13312(%rbx)
	movb	$0, 13280(%rbx)
	movb	$0, 13376(%rbx)
	movb	$0, 13344(%rbx)
	movb	$0, 13440(%rbx)
	movb	$0, 13408(%rbx)
	movb	$0, 13504(%rbx)
	movb	$0, 13472(%rbx)
	movb	$0, 13568(%rbx)
	movb	$0, 13536(%rbx)
	movb	$0, 13632(%rbx)
	movb	$0, 13600(%rbx)
	movb	$0, 13696(%rbx)
	movb	$0, 13664(%rbx)
	movb	$0, 13760(%rbx)
	movb	$0, 13728(%rbx)
	movb	$0, 13824(%rbx)
	movb	$0, 13792(%rbx)
	movb	$0, 13888(%rbx)
	movb	$0, 13856(%rbx)
	movb	$0, 13952(%rbx)
	movb	$0, 13920(%rbx)
	movb	$0, 14016(%rbx)
	movb	$0, 13984(%rbx)
	movb	$0, 14080(%rbx)
	movb	$0, 14048(%rbx)
	movb	$0, 14144(%rbx)
	movb	$0, 14112(%rbx)
	movb	$0, 14208(%rbx)
	movb	$0, 14176(%rbx)
	movb	$0, 14272(%rbx)
	movb	$0, 14240(%rbx)
	movb	$0, 14336(%rbx)
	movb	$0, 14304(%rbx)
	movb	$0, 14400(%rbx)
	movb	$0, 14368(%rbx)
	movb	$0, 14464(%rbx)
	movb	$0, 14432(%rbx)
	movb	$0, 14528(%rbx)
	movb	$0, 14496(%rbx)
	movb	$0, 14592(%rbx)
	movb	$0, 14560(%rbx)
	movb	$0, 14656(%rbx)
	movb	$0, 14624(%rbx)
	movb	$0, 14720(%rbx)
	movb	$0, 14688(%rbx)
	movb	$0, 14784(%rbx)
	movb	$0, 14752(%rbx)
	movb	$0, 14848(%rbx)
	movb	$0, 14816(%rbx)
	movb	$0, 14912(%rbx)
	movb	$0, 14880(%rbx)
	movb	$0, 14976(%rbx)
	movb	$0, 14944(%rbx)
	movb	$0, 15040(%rbx)
	movb	$0, 15008(%rbx)
	movb	$0, 15104(%rbx)
	movb	$0, 15072(%rbx)
	movb	$0, 15168(%rbx)
	movb	$0, 15136(%rbx)
	movb	$0, 15232(%rbx)
	movb	$0, 15200(%rbx)
	movb	$0, 15296(%rbx)
	movb	$0, 15264(%rbx)
	movb	$0, 15360(%rbx)
	movb	$0, 15328(%rbx)
	movb	$0, 15424(%rbx)
	movb	$0, 15392(%rbx)
	movb	$0, 15488(%rbx)
	movb	$0, 15456(%rbx)
	movb	$0, 15552(%rbx)
	movb	$0, 15520(%rbx)
	movb	$0, 15616(%rbx)
	movb	$0, 15584(%rbx)
	movb	$0, 15680(%rbx)
	movb	$0, 15648(%rbx)
	movb	$0, 15744(%rbx)
	movb	$0, 15712(%rbx)
	movb	$0, 15808(%rbx)
	movb	$0, 15776(%rbx)
	movb	$0, 15872(%rbx)
	movb	$0, 15840(%rbx)
	movb	$0, 15936(%rbx)
	movb	$0, 15904(%rbx)
	movb	$0, 16000(%rbx)
	movb	$0, 15968(%rbx)
	movb	$0, 16064(%rbx)
	movb	$0, 16032(%rbx)
	movb	$0, 16128(%rbx)
	movb	$0, 16096(%rbx)
	movb	$0, 16192(%rbx)
	movb	$0, 16160(%rbx)
	movb	$0, 16256(%rbx)
	movb	$0, 16224(%rbx)
	movb	$0, 16320(%rbx)
	movb	$0, 16288(%rbx)
	movb	$0, 16384(%rbx)
	movb	$0, 16352(%rbx)
	movb	$0, 16448(%rbx)
	movb	$0, 16416(%rbx)
	movb	$0, 16512(%rbx)
	movb	$0, 16480(%rbx)
	movb	$0, 16576(%rbx)
	movb	$0, 16544(%rbx)
	movb	$0, 16640(%rbx)
	movb	$0, 16608(%rbx)
	movb	$0, 16704(%rbx)
	movb	$0, 16672(%rbx)
	movb	$0, 16768(%rbx)
	movb	$0, 16736(%rbx)
	movb	$0, 16832(%rbx)
	movb	$0, 16800(%rbx)
	movb	$0, 16896(%rbx)
	movb	$0, 16864(%rbx)
	movb	$0, 16960(%rbx)
	movb	$0, 16928(%rbx)
	movb	$0, 17024(%rbx)
	movb	$0, 16992(%rbx)
	movb	$0, 17088(%rbx)
	movb	$0, 17056(%rbx)
	movb	$0, 17152(%rbx)
	movb	$0, 17120(%rbx)
	movb	$0, 17216(%rbx)
	movb	$0, 17184(%rbx)
	movb	$0, 17280(%rbx)
	movb	$0, 17248(%rbx)
	movb	$0, 17344(%rbx)
	movb	$0, 17312(%rbx)
	movb	$0, 17408(%rbx)
	movb	$0, 17376(%rbx)
	movb	$0, 17472(%rbx)
	movb	$0, 17440(%rbx)
	movb	$0, 17536(%rbx)
	movb	$0, 17504(%rbx)
	movb	$0, 17600(%rbx)
	movb	$0, 17568(%rbx)
	movb	$0, 17664(%rbx)
	movb	$0, 17632(%rbx)
	movb	$0, 17728(%rbx)
	movb	$0, 17696(%rbx)
	movb	$0, 17792(%rbx)
	movb	$0, 17760(%rbx)
	movb	$0, 17856(%rbx)
	movb	$0, 17824(%rbx)
	movb	$0, 17920(%rbx)
	movb	$0, 17888(%rbx)
	movb	$0, 17984(%rbx)
	movb	$0, 17952(%rbx)
	movb	$0, 18048(%rbx)
	movb	$0, 18016(%rbx)
	movb	$0, 18112(%rbx)
	movb	$0, 18080(%rbx)
	movb	$0, 18176(%rbx)
	movb	$0, 18144(%rbx)
	movb	$0, 18240(%rbx)
	movb	$0, 18208(%rbx)
	movb	$0, 18304(%rbx)
	movb	$0, 18272(%rbx)
	movb	$0, 18368(%rbx)
	movb	$0, 18336(%rbx)
	movb	$0, 18432(%rbx)
	movb	$0, 18400(%rbx)
	movb	$0, 18496(%rbx)
	movb	$0, 18464(%rbx)
	movb	$0, 18560(%rbx)
	movb	$0, 18528(%rbx)
	movb	$0, 18624(%rbx)
	movb	$0, 18592(%rbx)
	movb	$0, 18688(%rbx)
	movb	$0, 18656(%rbx)
	movb	$0, 18752(%rbx)
	movb	$0, 18720(%rbx)
	movb	$0, 18816(%rbx)
	movb	$0, 18784(%rbx)
	movb	$0, 18880(%rbx)
	movb	$0, 18848(%rbx)
	movb	$0, 18944(%rbx)
	movb	$0, 18912(%rbx)
	movb	$0, 19008(%rbx)
	movb	$0, 18976(%rbx)
	movb	$0, 19072(%rbx)
	movb	$0, 19040(%rbx)
	movb	$0, 19136(%rbx)
	movb	$0, 19104(%rbx)
	movb	$0, 19200(%rbx)
	movb	$0, 19168(%rbx)
	movb	$0, 19264(%rbx)
	movb	$0, 19232(%rbx)
	movb	$0, 19328(%rbx)
	movb	$0, 19296(%rbx)
	movb	$0, 19392(%rbx)
	movb	$0, 19360(%rbx)
	movb	$0, 19456(%rbx)
	movb	$0, 19424(%rbx)
	movb	$0, 19520(%rbx)
	movb	$0, 19488(%rbx)
	movb	$0, 19584(%rbx)
	movb	$0, 19552(%rbx)
	movb	$0, 19648(%rbx)
	movb	$0, 19616(%rbx)
	movb	$0, 19712(%rbx)
	movb	$0, 19680(%rbx)
	movb	$0, 19776(%rbx)
	movb	$0, 19744(%rbx)
	movb	$0, 19840(%rbx)
	movb	$0, 19808(%rbx)
	movb	$0, 19904(%rbx)
	movb	$0, 19872(%rbx)
	movb	$0, 19968(%rbx)
	movb	$0, 19936(%rbx)
	movb	$0, 20032(%rbx)
	movb	$0, 20000(%rbx)
	movb	$0, 20096(%rbx)
	movb	$0, 20064(%rbx)
	movb	$0, 20160(%rbx)
	movb	$0, 20128(%rbx)
	movb	$0, 20224(%rbx)
	movb	$0, 20192(%rbx)
	movb	$0, 20288(%rbx)
	movb	$0, 20256(%rbx)
	movb	$0, 20352(%rbx)
	movb	$0, 20320(%rbx)
	movb	$0, 20416(%rbx)
	movb	$0, 20384(%rbx)
	movb	$0, 20480(%rbx)
	movb	$0, 20448(%rbx)
	movb	$0, 20544(%rbx)
	movb	$0, 20512(%rbx)
	movb	$0, 20608(%rbx)
	movb	$0, 20576(%rbx)
	movb	$0, 20672(%rbx)
	movb	$0, 20640(%rbx)
	movb	$0, 20736(%rbx)
	movb	$0, 20704(%rbx)
	movb	$0, 20800(%rbx)
	movb	$0, 20768(%rbx)
	movb	$0, 20864(%rbx)
	movb	$0, 20832(%rbx)
	movb	$0, 20928(%rbx)
	movb	$0, 20896(%rbx)
	movb	$0, 20992(%rbx)
	movb	$0, 20960(%rbx)
	movb	$0, 21056(%rbx)
	movb	$0, 21024(%rbx)
	movb	$0, 21120(%rbx)
	movb	$0, 21088(%rbx)
	movb	$0, 21184(%rbx)
	movb	$0, 21152(%rbx)
	movb	$0, 21248(%rbx)
	movb	$0, 21216(%rbx)
	movb	$0, 21312(%rbx)
	movb	$0, 21280(%rbx)
	movb	$0, 21376(%rbx)
	movb	$0, 21344(%rbx)
	movb	$0, 21440(%rbx)
	movb	$0, 21408(%rbx)
	movb	$0, 21504(%rbx)
	movb	$0, 21472(%rbx)
	movb	$0, 21568(%rbx)
	movb	$0, 21536(%rbx)
	movb	$0, 21632(%rbx)
	movb	$0, 21600(%rbx)
	movb	$0, 21696(%rbx)
	movb	$0, 21664(%rbx)
	movb	$0, 21760(%rbx)
	movb	$0, 21728(%rbx)
	movb	$0, 21824(%rbx)
	movb	$0, 21792(%rbx)
	movb	$0, 21888(%rbx)
	movb	$0, 21856(%rbx)
	movb	$0, 21952(%rbx)
	movb	$0, 21920(%rbx)
	movb	$0, 22016(%rbx)
	movb	$0, 21984(%rbx)
	movb	$0, 22080(%rbx)
	movb	$0, 22048(%rbx)
	movb	$0, 22144(%rbx)
	movb	$0, 22112(%rbx)
	movb	$0, 22208(%rbx)
	movb	$0, 22176(%rbx)
	movb	$0, 22272(%rbx)
	movb	$0, 22240(%rbx)
	movb	$0, 22336(%rbx)
	movb	$0, 22304(%rbx)
	movb	$0, 22400(%rbx)
	movb	$0, 22368(%rbx)
	movb	$0, 22464(%rbx)
	movb	$0, 22432(%rbx)
	movb	$0, 22528(%rbx)
	movb	$0, 22496(%rbx)
	movb	$0, 22592(%rbx)
	movb	$0, 22560(%rbx)
	movb	$0, 22656(%rbx)
	movb	$0, 22624(%rbx)
	movb	$0, 22720(%rbx)
	movb	$0, 22688(%rbx)
	movb	$0, 22784(%rbx)
	movb	$0, 22752(%rbx)
	movb	$0, 22848(%rbx)
	movb	$0, 22816(%rbx)
	movb	$0, 22912(%rbx)
	movb	$0, 22880(%rbx)
	movb	$0, 22976(%rbx)
	movb	$0, 22944(%rbx)
	movb	$0, 23040(%rbx)
	movb	$0, 23008(%rbx)
	movb	$0, 23104(%rbx)
	movb	$0, 23072(%rbx)
	movb	$0, 23168(%rbx)
	movb	$0, 23136(%rbx)
	movb	$0, 23232(%rbx)
	movb	$0, 23200(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L142:
	xorl	%eax, %eax
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L143:
	xorl	%eax, %eax
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L144:
	xorl	%eax, %eax
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L145:
	xorl	%eax, %eax
	jmp	.L140
	.cfi_endproc
.LFE18697:
	.size	_ZN2v88internal8Counters20ResetCounterFunctionEPFPiPKcE, .-_ZN2v88internal8Counters20ResetCounterFunctionEPFPiPKcE
	.section	.text._ZN2v88internal8Counters28ResetCreateHistogramFunctionEPFPvPKciimE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Counters28ResetCreateHistogramFunctionEPFPvPKciimE
	.type	_ZN2v88internal8Counters28ResetCreateHistogramFunctionEPFPvPKciimE, @function
_ZN2v88internal8Counters28ResetCreateHistogramFunctionEPFPvPKciimE:
.LFB18698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	64(%rdi), %rcx
	movl	60(%rdi), %edx
	movq	%rsi, 32(%rdi)
	movq	80(%rbx), %rax
	movl	56(%rdi), %esi
	movq	48(%rdi), %rdi
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L278
	call	*%rax
.L148:
	movslq	104(%rbx), %rcx
	movl	100(%rbx), %edx
	movq	%rax, 72(%rbx)
	movq	120(%rbx), %rax
	movl	96(%rbx), %esi
	movq	88(%rbx), %rdi
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L279
	call	*%rax
.L149:
	movl	140(%rbx), %edx
	movl	136(%rbx), %esi
	movq	%rax, 112(%rbx)
	movq	160(%rbx), %rax
	movslq	144(%rbx), %rcx
	movq	128(%rbx), %rdi
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L280
	call	*%rax
.L150:
	movq	%rax, 152(%rbx)
	movq	200(%rbx), %rax
	movslq	184(%rbx), %rcx
	movl	180(%rbx), %edx
	movq	32(%rax), %rax
	movl	176(%rbx), %esi
	movq	168(%rbx), %rdi
	testq	%rax, %rax
	je	.L281
	call	*%rax
.L151:
	movq	%rax, 192(%rbx)
	movq	240(%rbx), %rax
	movslq	224(%rbx), %rcx
	movl	220(%rbx), %edx
	movq	32(%rax), %rax
	movl	216(%rbx), %esi
	movq	208(%rbx), %rdi
	testq	%rax, %rax
	je	.L282
	call	*%rax
.L152:
	movq	%rax, 232(%rbx)
	movq	280(%rbx), %rax
	movslq	264(%rbx), %rcx
	movl	260(%rbx), %edx
	movq	32(%rax), %rax
	movl	256(%rbx), %esi
	movq	248(%rbx), %rdi
	testq	%rax, %rax
	je	.L283
	call	*%rax
.L153:
	movq	%rax, 272(%rbx)
	movq	320(%rbx), %rax
	movslq	304(%rbx), %rcx
	movl	300(%rbx), %edx
	movq	32(%rax), %rax
	movl	296(%rbx), %esi
	movq	288(%rbx), %rdi
	testq	%rax, %rax
	je	.L284
	call	*%rax
.L154:
	movq	%rax, 312(%rbx)
	movq	360(%rbx), %rax
	movslq	344(%rbx), %rcx
	movl	340(%rbx), %edx
	movq	32(%rax), %rax
	movl	336(%rbx), %esi
	movq	328(%rbx), %rdi
	testq	%rax, %rax
	je	.L285
	call	*%rax
.L155:
	movq	%rax, 352(%rbx)
	movq	400(%rbx), %rax
	movslq	384(%rbx), %rcx
	movl	380(%rbx), %edx
	movq	32(%rax), %rax
	movl	376(%rbx), %esi
	movq	368(%rbx), %rdi
	testq	%rax, %rax
	je	.L286
	call	*%rax
.L156:
	movq	%rax, 392(%rbx)
	movq	440(%rbx), %rax
	movslq	424(%rbx), %rcx
	movl	420(%rbx), %edx
	movq	32(%rax), %rax
	movl	416(%rbx), %esi
	movq	408(%rbx), %rdi
	testq	%rax, %rax
	je	.L287
	call	*%rax
.L157:
	movq	%rax, 432(%rbx)
	movq	480(%rbx), %rax
	movslq	464(%rbx), %rcx
	movl	460(%rbx), %edx
	movq	32(%rax), %rax
	movl	456(%rbx), %esi
	movq	448(%rbx), %rdi
	testq	%rax, %rax
	je	.L288
	call	*%rax
.L158:
	movq	%rax, 472(%rbx)
	movq	520(%rbx), %rax
	movslq	504(%rbx), %rcx
	movl	500(%rbx), %edx
	movq	32(%rax), %rax
	movl	496(%rbx), %esi
	movq	488(%rbx), %rdi
	testq	%rax, %rax
	je	.L289
	call	*%rax
.L159:
	movq	%rax, 512(%rbx)
	movq	560(%rbx), %rax
	movslq	544(%rbx), %rcx
	movl	540(%rbx), %edx
	movq	32(%rax), %rax
	movl	536(%rbx), %esi
	movq	528(%rbx), %rdi
	testq	%rax, %rax
	je	.L290
	call	*%rax
.L160:
	movq	%rax, 552(%rbx)
	movq	600(%rbx), %rax
	movslq	584(%rbx), %rcx
	movl	580(%rbx), %edx
	movq	32(%rax), %rax
	movl	576(%rbx), %esi
	movq	568(%rbx), %rdi
	testq	%rax, %rax
	je	.L291
	call	*%rax
.L161:
	movq	%rax, 592(%rbx)
	movq	640(%rbx), %rax
	movslq	624(%rbx), %rcx
	movl	620(%rbx), %edx
	movq	32(%rax), %rax
	movl	616(%rbx), %esi
	movq	608(%rbx), %rdi
	testq	%rax, %rax
	je	.L292
	call	*%rax
.L162:
	movq	%rax, 632(%rbx)
	movq	680(%rbx), %rax
	movslq	664(%rbx), %rcx
	movl	660(%rbx), %edx
	movq	32(%rax), %rax
	movl	656(%rbx), %esi
	movq	648(%rbx), %rdi
	testq	%rax, %rax
	je	.L293
	call	*%rax
.L163:
	movq	%rax, 672(%rbx)
	movq	720(%rbx), %rax
	movslq	704(%rbx), %rcx
	movl	700(%rbx), %edx
	movq	32(%rax), %rax
	movl	696(%rbx), %esi
	movq	688(%rbx), %rdi
	testq	%rax, %rax
	je	.L294
	call	*%rax
.L164:
	movq	%rax, 712(%rbx)
	movq	760(%rbx), %rax
	movslq	744(%rbx), %rcx
	movl	740(%rbx), %edx
	movq	32(%rax), %rax
	movl	736(%rbx), %esi
	movq	728(%rbx), %rdi
	testq	%rax, %rax
	je	.L295
	call	*%rax
.L165:
	movq	%rax, 752(%rbx)
	movq	800(%rbx), %rax
	movslq	784(%rbx), %rcx
	movl	780(%rbx), %edx
	movq	32(%rax), %rax
	movl	776(%rbx), %esi
	movq	768(%rbx), %rdi
	testq	%rax, %rax
	je	.L296
	call	*%rax
.L166:
	movq	%rax, 792(%rbx)
	movq	840(%rbx), %rax
	movslq	824(%rbx), %rcx
	movl	820(%rbx), %edx
	movq	32(%rax), %rax
	movl	816(%rbx), %esi
	movq	808(%rbx), %rdi
	testq	%rax, %rax
	je	.L297
	call	*%rax
.L167:
	movq	%rax, 832(%rbx)
	movq	880(%rbx), %rax
	movslq	864(%rbx), %rcx
	movl	860(%rbx), %edx
	movq	32(%rax), %rax
	movl	856(%rbx), %esi
	movq	848(%rbx), %rdi
	testq	%rax, %rax
	je	.L298
	call	*%rax
.L168:
	movq	%rax, 872(%rbx)
	movq	920(%rbx), %rax
	movslq	904(%rbx), %rcx
	movl	900(%rbx), %edx
	movq	32(%rax), %rax
	movl	896(%rbx), %esi
	movq	888(%rbx), %rdi
	testq	%rax, %rax
	je	.L299
	call	*%rax
.L169:
	movq	%rax, 912(%rbx)
	movq	960(%rbx), %rax
	movslq	944(%rbx), %rcx
	movl	940(%rbx), %edx
	movq	32(%rax), %rax
	movl	936(%rbx), %esi
	movq	928(%rbx), %rdi
	testq	%rax, %rax
	je	.L300
	call	*%rax
.L170:
	movq	%rax, 952(%rbx)
	movq	1000(%rbx), %rax
	movslq	984(%rbx), %rcx
	movl	980(%rbx), %edx
	movq	32(%rax), %rax
	movl	976(%rbx), %esi
	movq	968(%rbx), %rdi
	testq	%rax, %rax
	je	.L301
	call	*%rax
.L171:
	movq	%rax, 992(%rbx)
	movq	1040(%rbx), %rax
	movslq	1024(%rbx), %rcx
	movl	1020(%rbx), %edx
	movq	32(%rax), %rax
	movl	1016(%rbx), %esi
	movq	1008(%rbx), %rdi
	testq	%rax, %rax
	je	.L302
	call	*%rax
.L172:
	movq	%rax, 1032(%rbx)
	movq	1080(%rbx), %rax
	movslq	1064(%rbx), %rcx
	movl	1060(%rbx), %edx
	movq	32(%rax), %rax
	movl	1056(%rbx), %esi
	movq	1048(%rbx), %rdi
	testq	%rax, %rax
	je	.L303
	call	*%rax
.L173:
	movq	%rax, 1072(%rbx)
	movq	1120(%rbx), %rax
	movslq	1104(%rbx), %rcx
	movl	1100(%rbx), %edx
	movq	32(%rax), %rax
	movl	1096(%rbx), %esi
	movq	1088(%rbx), %rdi
	testq	%rax, %rax
	je	.L304
	call	*%rax
.L174:
	movq	%rax, 1112(%rbx)
	movq	1160(%rbx), %rax
	movslq	1144(%rbx), %rcx
	movl	1140(%rbx), %edx
	movq	32(%rax), %rax
	movl	1136(%rbx), %esi
	movq	1128(%rbx), %rdi
	testq	%rax, %rax
	je	.L305
	call	*%rax
.L175:
	movq	%rax, 1152(%rbx)
	movq	1200(%rbx), %rax
	movslq	1184(%rbx), %rcx
	movl	1180(%rbx), %edx
	movq	32(%rax), %rax
	movl	1176(%rbx), %esi
	movq	1168(%rbx), %rdi
	testq	%rax, %rax
	je	.L306
	call	*%rax
.L176:
	movq	%rax, 1192(%rbx)
	movq	1240(%rbx), %rax
	movslq	1224(%rbx), %rcx
	movl	1220(%rbx), %edx
	movq	32(%rax), %rax
	movl	1216(%rbx), %esi
	movq	1208(%rbx), %rdi
	testq	%rax, %rax
	je	.L307
	call	*%rax
.L177:
	movq	%rax, 1232(%rbx)
	movq	1280(%rbx), %rax
	movslq	1264(%rbx), %rcx
	movl	1260(%rbx), %edx
	movq	32(%rax), %rax
	movl	1256(%rbx), %esi
	movq	1248(%rbx), %rdi
	testq	%rax, %rax
	je	.L308
	call	*%rax
.L178:
	movq	%rax, 1272(%rbx)
	movq	1320(%rbx), %rax
	movslq	1304(%rbx), %rcx
	movl	1300(%rbx), %edx
	movq	32(%rax), %rax
	movl	1296(%rbx), %esi
	movq	1288(%rbx), %rdi
	testq	%rax, %rax
	je	.L309
	call	*%rax
.L179:
	movq	%rax, 1312(%rbx)
	movq	1360(%rbx), %rax
	movslq	1344(%rbx), %rcx
	movl	1340(%rbx), %edx
	movq	32(%rax), %rax
	movl	1336(%rbx), %esi
	movq	1328(%rbx), %rdi
	testq	%rax, %rax
	je	.L310
	call	*%rax
.L180:
	movq	%rax, 1352(%rbx)
	movq	1400(%rbx), %rax
	movslq	1384(%rbx), %rcx
	movl	1380(%rbx), %edx
	movq	32(%rax), %rax
	movl	1376(%rbx), %esi
	movq	1368(%rbx), %rdi
	testq	%rax, %rax
	je	.L311
	call	*%rax
.L181:
	movq	%rax, 1392(%rbx)
	movq	1440(%rbx), %rax
	movslq	1424(%rbx), %rcx
	movl	1420(%rbx), %edx
	movq	32(%rax), %rax
	movl	1416(%rbx), %esi
	movq	1408(%rbx), %rdi
	testq	%rax, %rax
	je	.L312
	call	*%rax
.L182:
	movq	%rax, 1432(%rbx)
	movq	1480(%rbx), %rax
	movslq	1464(%rbx), %rcx
	movl	1460(%rbx), %edx
	movq	32(%rax), %rax
	movl	1456(%rbx), %esi
	movq	1448(%rbx), %rdi
	testq	%rax, %rax
	je	.L313
	call	*%rax
.L183:
	movq	%rax, 1472(%rbx)
	movq	1520(%rbx), %rax
	movslq	1504(%rbx), %rcx
	movl	1500(%rbx), %edx
	movq	32(%rax), %rax
	movl	1496(%rbx), %esi
	movq	1488(%rbx), %rdi
	testq	%rax, %rax
	je	.L314
	call	*%rax
.L184:
	movq	%rax, 1512(%rbx)
	movq	1560(%rbx), %rax
	movslq	1544(%rbx), %rcx
	movl	1540(%rbx), %edx
	movq	32(%rax), %rax
	movl	1536(%rbx), %esi
	movq	1528(%rbx), %rdi
	testq	%rax, %rax
	je	.L315
	call	*%rax
.L185:
	movq	%rax, 1552(%rbx)
	movq	1600(%rbx), %rax
	movslq	1584(%rbx), %rcx
	movl	1580(%rbx), %edx
	movq	32(%rax), %rax
	movl	1576(%rbx), %esi
	movq	1568(%rbx), %rdi
	testq	%rax, %rax
	je	.L316
	call	*%rax
.L186:
	movq	%rax, 1592(%rbx)
	movq	1640(%rbx), %rax
	movslq	1624(%rbx), %rcx
	movl	1620(%rbx), %edx
	movq	32(%rax), %rax
	movl	1616(%rbx), %esi
	movq	1608(%rbx), %rdi
	testq	%rax, %rax
	je	.L317
	call	*%rax
.L187:
	movq	%rax, 1632(%rbx)
	movq	1680(%rbx), %rax
	movslq	1664(%rbx), %rcx
	movl	1660(%rbx), %edx
	movq	32(%rax), %rax
	movl	1656(%rbx), %esi
	movq	1648(%rbx), %rdi
	testq	%rax, %rax
	je	.L318
	call	*%rax
.L188:
	movq	%rax, 1672(%rbx)
	movq	1720(%rbx), %rax
	movslq	1704(%rbx), %rcx
	movl	1700(%rbx), %edx
	movq	32(%rax), %rax
	movl	1696(%rbx), %esi
	movq	1688(%rbx), %rdi
	testq	%rax, %rax
	je	.L319
	call	*%rax
.L189:
	movq	%rax, 1712(%rbx)
	movq	1760(%rbx), %rax
	movslq	1744(%rbx), %rcx
	movl	1740(%rbx), %edx
	movq	32(%rax), %rax
	movl	1736(%rbx), %esi
	movq	1728(%rbx), %rdi
	testq	%rax, %rax
	je	.L320
	call	*%rax
.L190:
	movq	%rax, 1752(%rbx)
	movq	1800(%rbx), %rax
	movslq	1784(%rbx), %rcx
	movl	1780(%rbx), %edx
	movq	32(%rax), %rax
	movl	1776(%rbx), %esi
	movq	1768(%rbx), %rdi
	testq	%rax, %rax
	je	.L321
	call	*%rax
.L191:
	movq	%rax, 1792(%rbx)
	movq	1840(%rbx), %rax
	movslq	1824(%rbx), %rcx
	movl	1820(%rbx), %edx
	movq	32(%rax), %rax
	movl	1816(%rbx), %esi
	movq	1808(%rbx), %rdi
	testq	%rax, %rax
	je	.L322
	call	*%rax
.L192:
	movq	%rax, 1832(%rbx)
	movq	1880(%rbx), %rax
	movslq	1864(%rbx), %rcx
	movl	1860(%rbx), %edx
	movq	32(%rax), %rax
	movl	1856(%rbx), %esi
	movq	1848(%rbx), %rdi
	testq	%rax, %rax
	je	.L323
	call	*%rax
.L193:
	movq	%rax, 1872(%rbx)
	movq	1920(%rbx), %rax
	movslq	1904(%rbx), %rcx
	movl	1900(%rbx), %edx
	movq	32(%rax), %rax
	movl	1896(%rbx), %esi
	movq	1888(%rbx), %rdi
	testq	%rax, %rax
	je	.L324
	call	*%rax
.L194:
	movq	%rax, 1912(%rbx)
	movq	1960(%rbx), %rax
	movslq	1944(%rbx), %rcx
	movl	1940(%rbx), %edx
	movq	32(%rax), %rax
	movl	1936(%rbx), %esi
	movq	1928(%rbx), %rdi
	testq	%rax, %rax
	je	.L325
	call	*%rax
.L195:
	movq	%rax, 1952(%rbx)
	movq	2000(%rbx), %rax
	movslq	1984(%rbx), %rcx
	movl	1980(%rbx), %edx
	movq	32(%rax), %rax
	movl	1976(%rbx), %esi
	movq	1968(%rbx), %rdi
	testq	%rax, %rax
	je	.L326
	call	*%rax
.L196:
	movq	%rax, 1992(%rbx)
	movq	2040(%rbx), %rax
	movslq	2024(%rbx), %rcx
	movl	2020(%rbx), %edx
	movq	32(%rax), %rax
	movl	2016(%rbx), %esi
	movq	2008(%rbx), %rdi
	testq	%rax, %rax
	je	.L327
	call	*%rax
.L197:
	movq	%rax, 2032(%rbx)
	movq	2080(%rbx), %rax
	movslq	2064(%rbx), %rcx
	movl	2060(%rbx), %edx
	movq	32(%rax), %rax
	movl	2056(%rbx), %esi
	movq	2048(%rbx), %rdi
	testq	%rax, %rax
	je	.L328
	call	*%rax
.L198:
	movq	%rax, 2072(%rbx)
	movq	2120(%rbx), %rax
	movslq	2104(%rbx), %rcx
	movl	2100(%rbx), %edx
	movq	32(%rax), %rax
	movl	2096(%rbx), %esi
	movq	2088(%rbx), %rdi
	testq	%rax, %rax
	je	.L329
	call	*%rax
.L199:
	movq	%rax, 2112(%rbx)
	movq	2160(%rbx), %rax
	movslq	2144(%rbx), %rcx
	movl	2140(%rbx), %edx
	movq	32(%rax), %rax
	movl	2136(%rbx), %esi
	movq	2128(%rbx), %rdi
	testq	%rax, %rax
	je	.L330
	call	*%rax
.L200:
	movq	%rax, 2152(%rbx)
	movq	2200(%rbx), %rax
	movslq	2184(%rbx), %rcx
	movl	2180(%rbx), %edx
	movq	32(%rax), %rax
	movl	2176(%rbx), %esi
	movq	2168(%rbx), %rdi
	testq	%rax, %rax
	je	.L331
	call	*%rax
.L201:
	movq	%rax, 2192(%rbx)
	movq	2240(%rbx), %rax
	movslq	2224(%rbx), %rcx
	movl	2220(%rbx), %edx
	movq	32(%rax), %rax
	movl	2216(%rbx), %esi
	movq	2208(%rbx), %rdi
	testq	%rax, %rax
	je	.L332
	call	*%rax
.L202:
	movq	%rax, 2232(%rbx)
	movq	2296(%rbx), %rax
	movslq	2280(%rbx), %rcx
	movl	2276(%rbx), %edx
	movq	32(%rax), %rax
	movl	2272(%rbx), %esi
	movq	2264(%rbx), %rdi
	testq	%rax, %rax
	je	.L333
	call	*%rax
.L203:
	movq	%rax, 2288(%rbx)
	movq	2352(%rbx), %rax
	movslq	2336(%rbx), %rcx
	movl	2332(%rbx), %edx
	movq	32(%rax), %rax
	movl	2328(%rbx), %esi
	movq	2320(%rbx), %rdi
	testq	%rax, %rax
	je	.L334
	call	*%rax
.L204:
	movq	%rax, 2344(%rbx)
	movq	2408(%rbx), %rax
	movslq	2392(%rbx), %rcx
	movl	2388(%rbx), %edx
	movq	32(%rax), %rax
	movl	2384(%rbx), %esi
	movq	2376(%rbx), %rdi
	testq	%rax, %rax
	je	.L335
	call	*%rax
.L205:
	movq	%rax, 2400(%rbx)
	movq	2464(%rbx), %rax
	movslq	2448(%rbx), %rcx
	movl	2444(%rbx), %edx
	movq	32(%rax), %rax
	movl	2440(%rbx), %esi
	movq	2432(%rbx), %rdi
	testq	%rax, %rax
	je	.L336
	call	*%rax
.L206:
	movq	%rax, 2456(%rbx)
	movq	2520(%rbx), %rax
	movslq	2504(%rbx), %rcx
	movl	2500(%rbx), %edx
	movq	32(%rax), %rax
	movl	2496(%rbx), %esi
	movq	2488(%rbx), %rdi
	testq	%rax, %rax
	je	.L337
	call	*%rax
.L207:
	movq	%rax, 2512(%rbx)
	movq	2576(%rbx), %rax
	movslq	2560(%rbx), %rcx
	movl	2556(%rbx), %edx
	movq	32(%rax), %rax
	movl	2552(%rbx), %esi
	movq	2544(%rbx), %rdi
	testq	%rax, %rax
	je	.L338
	call	*%rax
.L208:
	movq	%rax, 2568(%rbx)
	movq	2632(%rbx), %rax
	movslq	2616(%rbx), %rcx
	movl	2612(%rbx), %edx
	movq	32(%rax), %rax
	movl	2608(%rbx), %esi
	movq	2600(%rbx), %rdi
	testq	%rax, %rax
	je	.L339
	call	*%rax
.L209:
	movq	%rax, 2624(%rbx)
	movq	2688(%rbx), %rax
	movslq	2672(%rbx), %rcx
	movl	2668(%rbx), %edx
	movq	32(%rax), %rax
	movl	2664(%rbx), %esi
	movq	2656(%rbx), %rdi
	testq	%rax, %rax
	je	.L340
	call	*%rax
.L210:
	movq	%rax, 2680(%rbx)
	movq	2744(%rbx), %rax
	movslq	2728(%rbx), %rcx
	movl	2724(%rbx), %edx
	movq	32(%rax), %rax
	movl	2720(%rbx), %esi
	movq	2712(%rbx), %rdi
	testq	%rax, %rax
	je	.L341
	call	*%rax
.L211:
	movq	%rax, 2736(%rbx)
	movq	2800(%rbx), %rax
	movslq	2784(%rbx), %rcx
	movl	2780(%rbx), %edx
	movq	32(%rax), %rax
	movl	2776(%rbx), %esi
	movq	2768(%rbx), %rdi
	testq	%rax, %rax
	je	.L342
	call	*%rax
.L212:
	movq	%rax, 2792(%rbx)
	movq	2856(%rbx), %rax
	movslq	2840(%rbx), %rcx
	movl	2836(%rbx), %edx
	movq	32(%rax), %rax
	movl	2832(%rbx), %esi
	movq	2824(%rbx), %rdi
	testq	%rax, %rax
	je	.L343
	call	*%rax
.L213:
	movq	%rax, 2848(%rbx)
	movq	2912(%rbx), %rax
	movslq	2896(%rbx), %rcx
	movl	2892(%rbx), %edx
	movq	32(%rax), %rax
	movl	2888(%rbx), %esi
	movq	2880(%rbx), %rdi
	testq	%rax, %rax
	je	.L344
	call	*%rax
.L214:
	movq	%rax, 2904(%rbx)
	movq	2968(%rbx), %rax
	movslq	2952(%rbx), %rcx
	movl	2948(%rbx), %edx
	movq	32(%rax), %rax
	movl	2944(%rbx), %esi
	movq	2936(%rbx), %rdi
	testq	%rax, %rax
	je	.L345
	call	*%rax
.L215:
	movq	%rax, 2960(%rbx)
	movq	3024(%rbx), %rax
	movslq	3008(%rbx), %rcx
	movl	3004(%rbx), %edx
	movq	32(%rax), %rax
	movl	3000(%rbx), %esi
	movq	2992(%rbx), %rdi
	testq	%rax, %rax
	je	.L346
	call	*%rax
.L216:
	movq	%rax, 3016(%rbx)
	movq	3080(%rbx), %rax
	movslq	3064(%rbx), %rcx
	movl	3060(%rbx), %edx
	movq	32(%rax), %rax
	movl	3056(%rbx), %esi
	movq	3048(%rbx), %rdi
	testq	%rax, %rax
	je	.L347
	call	*%rax
.L217:
	movq	%rax, 3072(%rbx)
	movq	3128(%rbx), %rax
	movslq	3112(%rbx), %rcx
	movl	3108(%rbx), %edx
	movq	32(%rax), %rax
	movl	3104(%rbx), %esi
	movq	3096(%rbx), %rdi
	testq	%rax, %rax
	je	.L348
	call	*%rax
.L218:
	movq	%rax, 3120(%rbx)
	movq	3176(%rbx), %rax
	movslq	3160(%rbx), %rcx
	movl	3156(%rbx), %edx
	movq	32(%rax), %rax
	movl	3152(%rbx), %esi
	movq	3144(%rbx), %rdi
	testq	%rax, %rax
	je	.L349
	call	*%rax
.L219:
	movq	%rax, 3168(%rbx)
	movq	3224(%rbx), %rax
	movslq	3208(%rbx), %rcx
	movl	3204(%rbx), %edx
	movq	32(%rax), %rax
	movl	3200(%rbx), %esi
	movq	3192(%rbx), %rdi
	testq	%rax, %rax
	je	.L350
	call	*%rax
.L220:
	movq	%rax, 3216(%rbx)
	movq	3272(%rbx), %rax
	movslq	3256(%rbx), %rcx
	movl	3252(%rbx), %edx
	movq	32(%rax), %rax
	movl	3248(%rbx), %esi
	movq	3240(%rbx), %rdi
	testq	%rax, %rax
	je	.L351
	call	*%rax
.L221:
	movq	%rax, 3264(%rbx)
	movq	3320(%rbx), %rax
	movslq	3304(%rbx), %rcx
	movl	3300(%rbx), %edx
	movq	32(%rax), %rax
	movl	3296(%rbx), %esi
	movq	3288(%rbx), %rdi
	testq	%rax, %rax
	je	.L352
	call	*%rax
.L222:
	movq	%rax, 3312(%rbx)
	movq	3368(%rbx), %rax
	movslq	3352(%rbx), %rcx
	movl	3348(%rbx), %edx
	movq	32(%rax), %rax
	movl	3344(%rbx), %esi
	movq	3336(%rbx), %rdi
	testq	%rax, %rax
	je	.L353
	call	*%rax
.L223:
	movq	%rax, 3360(%rbx)
	movq	3416(%rbx), %rax
	movslq	3400(%rbx), %rcx
	movl	3396(%rbx), %edx
	movq	32(%rax), %rax
	movl	3392(%rbx), %esi
	movq	3384(%rbx), %rdi
	testq	%rax, %rax
	je	.L354
	call	*%rax
.L224:
	movq	%rax, 3408(%rbx)
	movq	3464(%rbx), %rax
	movslq	3448(%rbx), %rcx
	movl	3444(%rbx), %edx
	movq	32(%rax), %rax
	movl	3440(%rbx), %esi
	movq	3432(%rbx), %rdi
	testq	%rax, %rax
	je	.L355
	call	*%rax
.L225:
	movq	%rax, 3456(%rbx)
	movq	3512(%rbx), %rax
	movslq	3496(%rbx), %rcx
	movl	3492(%rbx), %edx
	movq	32(%rax), %rax
	movl	3488(%rbx), %esi
	movq	3480(%rbx), %rdi
	testq	%rax, %rax
	je	.L356
	call	*%rax
.L226:
	movq	%rax, 3504(%rbx)
	movq	3560(%rbx), %rax
	movslq	3544(%rbx), %rcx
	movl	3540(%rbx), %edx
	movq	32(%rax), %rax
	movl	3536(%rbx), %esi
	movq	3528(%rbx), %rdi
	testq	%rax, %rax
	je	.L357
	call	*%rax
.L227:
	movq	%rax, 3552(%rbx)
	movq	3608(%rbx), %rax
	movslq	3592(%rbx), %rcx
	movl	3588(%rbx), %edx
	movq	32(%rax), %rax
	movl	3584(%rbx), %esi
	movq	3576(%rbx), %rdi
	testq	%rax, %rax
	je	.L358
	call	*%rax
.L228:
	movq	%rax, 3600(%rbx)
	movq	3656(%rbx), %rax
	movslq	3640(%rbx), %rcx
	movl	3636(%rbx), %edx
	movq	32(%rax), %rax
	movl	3632(%rbx), %esi
	movq	3624(%rbx), %rdi
	testq	%rax, %rax
	je	.L359
	call	*%rax
.L229:
	movq	%rax, 3648(%rbx)
	movq	3704(%rbx), %rax
	movslq	3688(%rbx), %rcx
	movl	3684(%rbx), %edx
	movq	32(%rax), %rax
	movl	3680(%rbx), %esi
	movq	3672(%rbx), %rdi
	testq	%rax, %rax
	je	.L360
	call	*%rax
.L230:
	movq	%rax, 3696(%rbx)
	movq	3752(%rbx), %rax
	movslq	3736(%rbx), %rcx
	movl	3732(%rbx), %edx
	movq	32(%rax), %rax
	movl	3728(%rbx), %esi
	movq	3720(%rbx), %rdi
	testq	%rax, %rax
	je	.L361
	call	*%rax
.L231:
	movq	%rax, 3744(%rbx)
	movq	3800(%rbx), %rax
	movslq	3784(%rbx), %rcx
	movl	3780(%rbx), %edx
	movq	32(%rax), %rax
	movl	3776(%rbx), %esi
	movq	3768(%rbx), %rdi
	testq	%rax, %rax
	je	.L362
	call	*%rax
.L232:
	movq	%rax, 3792(%rbx)
	movq	3848(%rbx), %rax
	movslq	3832(%rbx), %rcx
	movl	3828(%rbx), %edx
	movq	32(%rax), %rax
	movl	3824(%rbx), %esi
	movq	3816(%rbx), %rdi
	testq	%rax, %rax
	je	.L363
	call	*%rax
.L233:
	movq	%rax, 3840(%rbx)
	movq	3896(%rbx), %rax
	movslq	3880(%rbx), %rcx
	movl	3876(%rbx), %edx
	movq	32(%rax), %rax
	movl	3872(%rbx), %esi
	movq	3864(%rbx), %rdi
	testq	%rax, %rax
	je	.L364
	call	*%rax
.L234:
	movq	%rax, 3888(%rbx)
	movq	3944(%rbx), %rax
	movslq	3928(%rbx), %rcx
	movl	3924(%rbx), %edx
	movq	32(%rax), %rax
	movl	3920(%rbx), %esi
	movq	3912(%rbx), %rdi
	testq	%rax, %rax
	je	.L365
	call	*%rax
.L235:
	movq	%rax, 3936(%rbx)
	movq	3992(%rbx), %rax
	movslq	3976(%rbx), %rcx
	movl	3972(%rbx), %edx
	movq	32(%rax), %rax
	movl	3968(%rbx), %esi
	movq	3960(%rbx), %rdi
	testq	%rax, %rax
	je	.L366
	call	*%rax
.L236:
	movq	%rax, 3984(%rbx)
	movq	4040(%rbx), %rax
	movslq	4024(%rbx), %rcx
	movl	4020(%rbx), %edx
	movq	32(%rax), %rax
	movl	4016(%rbx), %esi
	movq	4008(%rbx), %rdi
	testq	%rax, %rax
	je	.L367
	call	*%rax
.L237:
	movq	%rax, 4032(%rbx)
	movq	4088(%rbx), %rax
	movslq	4072(%rbx), %rcx
	movl	4068(%rbx), %edx
	movq	32(%rax), %rax
	movl	4064(%rbx), %esi
	movq	4056(%rbx), %rdi
	testq	%rax, %rax
	je	.L368
	call	*%rax
.L238:
	movq	%rax, 4080(%rbx)
	movq	4136(%rbx), %rax
	movslq	4120(%rbx), %rcx
	movl	4116(%rbx), %edx
	movq	32(%rax), %rax
	movl	4112(%rbx), %esi
	movq	4104(%rbx), %rdi
	testq	%rax, %rax
	je	.L369
	call	*%rax
.L239:
	movq	%rax, 4128(%rbx)
	movq	4184(%rbx), %rax
	movslq	4168(%rbx), %rcx
	movl	4164(%rbx), %edx
	movq	32(%rax), %rax
	movl	4160(%rbx), %esi
	movq	4152(%rbx), %rdi
	testq	%rax, %rax
	je	.L370
	call	*%rax
.L240:
	movq	%rax, 4176(%rbx)
	movq	4232(%rbx), %rax
	movslq	4216(%rbx), %rcx
	movl	4212(%rbx), %edx
	movq	32(%rax), %rax
	movl	4208(%rbx), %esi
	movq	4200(%rbx), %rdi
	testq	%rax, %rax
	je	.L371
	call	*%rax
.L241:
	movq	%rax, 4224(%rbx)
	movq	4280(%rbx), %rax
	movslq	4264(%rbx), %rcx
	movl	4260(%rbx), %edx
	movq	32(%rax), %rax
	movl	4256(%rbx), %esi
	movq	4248(%rbx), %rdi
	testq	%rax, %rax
	je	.L372
	call	*%rax
.L242:
	movq	%rax, 4272(%rbx)
	movq	4328(%rbx), %rax
	movslq	4312(%rbx), %rcx
	movl	4308(%rbx), %edx
	movq	32(%rax), %rax
	movl	4304(%rbx), %esi
	movq	4296(%rbx), %rdi
	testq	%rax, %rax
	je	.L373
	call	*%rax
.L243:
	movq	%rax, 4320(%rbx)
	movq	4376(%rbx), %rax
	movslq	4360(%rbx), %rcx
	movl	4356(%rbx), %edx
	movq	32(%rax), %rax
	movl	4352(%rbx), %esi
	movq	4344(%rbx), %rdi
	testq	%rax, %rax
	je	.L374
	call	*%rax
.L244:
	movq	%rax, 4368(%rbx)
	movq	4424(%rbx), %rax
	movslq	4408(%rbx), %rcx
	movl	4404(%rbx), %edx
	movq	32(%rax), %rax
	movl	4400(%rbx), %esi
	movq	4392(%rbx), %rdi
	testq	%rax, %rax
	je	.L375
	call	*%rax
.L245:
	movq	%rax, 4416(%rbx)
	movq	4472(%rbx), %rax
	movslq	4456(%rbx), %rcx
	movl	4452(%rbx), %edx
	movq	32(%rax), %rax
	movl	4448(%rbx), %esi
	movq	4440(%rbx), %rdi
	testq	%rax, %rax
	je	.L376
	call	*%rax
.L246:
	movq	%rax, 4464(%rbx)
	movq	4520(%rbx), %rax
	movslq	4504(%rbx), %rcx
	movl	4500(%rbx), %edx
	movq	32(%rax), %rax
	movl	4496(%rbx), %esi
	movq	4488(%rbx), %rdi
	testq	%rax, %rax
	je	.L377
	call	*%rax
.L247:
	movq	%rax, 4512(%rbx)
	movq	4568(%rbx), %rax
	movslq	4552(%rbx), %rcx
	movl	4548(%rbx), %edx
	movq	32(%rax), %rax
	movl	4544(%rbx), %esi
	movq	4536(%rbx), %rdi
	testq	%rax, %rax
	je	.L378
	call	*%rax
.L248:
	movq	%rax, 4560(%rbx)
	movq	4616(%rbx), %rax
	movslq	4600(%rbx), %rcx
	movl	4596(%rbx), %edx
	movq	32(%rax), %rax
	movl	4592(%rbx), %esi
	movq	4584(%rbx), %rdi
	testq	%rax, %rax
	je	.L379
	call	*%rax
.L249:
	movq	%rax, 4608(%rbx)
	movq	4664(%rbx), %rax
	movslq	4648(%rbx), %rcx
	movl	4644(%rbx), %edx
	movq	32(%rax), %rax
	movl	4640(%rbx), %esi
	movq	4632(%rbx), %rdi
	testq	%rax, %rax
	je	.L380
	call	*%rax
.L250:
	movq	%rax, 4656(%rbx)
	movq	4712(%rbx), %rax
	movslq	4696(%rbx), %rcx
	movl	4692(%rbx), %edx
	movq	32(%rax), %rax
	movl	4688(%rbx), %esi
	movq	4680(%rbx), %rdi
	testq	%rax, %rax
	je	.L381
	call	*%rax
.L251:
	movq	%rax, 4704(%rbx)
	movq	4760(%rbx), %rax
	movslq	4744(%rbx), %rcx
	movl	4740(%rbx), %edx
	movq	32(%rax), %rax
	movl	4736(%rbx), %esi
	movq	4728(%rbx), %rdi
	testq	%rax, %rax
	je	.L382
	call	*%rax
.L252:
	movq	%rax, 4752(%rbx)
	movq	4808(%rbx), %rax
	movslq	4792(%rbx), %rcx
	movl	4788(%rbx), %edx
	movq	32(%rax), %rax
	movl	4784(%rbx), %esi
	movq	4776(%rbx), %rdi
	testq	%rax, %rax
	je	.L383
	call	*%rax
.L253:
	movq	%rax, 4800(%rbx)
	movq	4856(%rbx), %rax
	movslq	4840(%rbx), %rcx
	movl	4836(%rbx), %edx
	movq	32(%rax), %rax
	movl	4832(%rbx), %esi
	movq	4824(%rbx), %rdi
	testq	%rax, %rax
	je	.L384
	call	*%rax
.L254:
	movq	%rax, 4848(%rbx)
	movq	4904(%rbx), %rax
	movslq	4888(%rbx), %rcx
	movl	4884(%rbx), %edx
	movq	32(%rax), %rax
	movl	4880(%rbx), %esi
	movq	4872(%rbx), %rdi
	testq	%rax, %rax
	je	.L385
	call	*%rax
.L255:
	movq	%rax, 4896(%rbx)
	movq	4952(%rbx), %rax
	movslq	4936(%rbx), %rcx
	movl	4932(%rbx), %edx
	movq	32(%rax), %rax
	movl	4928(%rbx), %esi
	movq	4920(%rbx), %rdi
	testq	%rax, %rax
	je	.L386
	call	*%rax
.L256:
	movq	%rax, 4944(%rbx)
	movq	5000(%rbx), %rax
	movslq	4984(%rbx), %rcx
	movl	4980(%rbx), %edx
	movq	32(%rax), %rax
	movl	4976(%rbx), %esi
	movq	4968(%rbx), %rdi
	testq	%rax, %rax
	je	.L387
	call	*%rax
.L257:
	movq	%rax, 4992(%rbx)
	movq	5048(%rbx), %rax
	movslq	5032(%rbx), %rcx
	movl	5028(%rbx), %edx
	movq	32(%rax), %rax
	movl	5024(%rbx), %esi
	movq	5016(%rbx), %rdi
	testq	%rax, %rax
	je	.L388
	call	*%rax
.L258:
	movq	%rax, 5040(%rbx)
	movq	5096(%rbx), %rax
	movslq	5080(%rbx), %rcx
	movl	5076(%rbx), %edx
	movq	32(%rax), %rax
	movl	5072(%rbx), %esi
	movq	5064(%rbx), %rdi
	testq	%rax, %rax
	je	.L389
	call	*%rax
.L259:
	movq	%rax, 5088(%rbx)
	movq	5144(%rbx), %rax
	movslq	5128(%rbx), %rcx
	movl	5124(%rbx), %edx
	movq	32(%rax), %rax
	movl	5120(%rbx), %esi
	movq	5112(%rbx), %rdi
	testq	%rax, %rax
	je	.L390
	call	*%rax
.L260:
	movq	%rax, 5136(%rbx)
	movq	5192(%rbx), %rax
	movslq	5176(%rbx), %rcx
	movl	5172(%rbx), %edx
	movq	32(%rax), %rax
	movl	5168(%rbx), %esi
	movq	5160(%rbx), %rdi
	testq	%rax, %rax
	je	.L391
	call	*%rax
.L261:
	movq	%rax, 5184(%rbx)
	movq	5240(%rbx), %rax
	movslq	5224(%rbx), %rcx
	movl	5220(%rbx), %edx
	movq	32(%rax), %rax
	movl	5216(%rbx), %esi
	movq	5208(%rbx), %rdi
	testq	%rax, %rax
	je	.L392
	call	*%rax
.L262:
	movq	%rax, 5232(%rbx)
	movq	5288(%rbx), %rax
	movslq	5272(%rbx), %rcx
	movl	5268(%rbx), %edx
	movq	32(%rax), %rax
	movl	5264(%rbx), %esi
	movq	5256(%rbx), %rdi
	testq	%rax, %rax
	je	.L393
	call	*%rax
.L263:
	movq	%rax, 5280(%rbx)
	movq	5336(%rbx), %rax
	movslq	5320(%rbx), %rcx
	movl	5316(%rbx), %edx
	movq	32(%rax), %rax
	movl	5312(%rbx), %esi
	movq	5304(%rbx), %rdi
	testq	%rax, %rax
	je	.L394
	call	*%rax
.L264:
	movq	%rax, 5328(%rbx)
	movq	5384(%rbx), %rax
	movslq	5368(%rbx), %rcx
	movl	5364(%rbx), %edx
	movq	32(%rax), %rax
	movl	5360(%rbx), %esi
	movq	5352(%rbx), %rdi
	testq	%rax, %rax
	je	.L395
	call	*%rax
.L265:
	movq	%rax, 5376(%rbx)
	movq	5432(%rbx), %rax
	movslq	5416(%rbx), %rcx
	movl	5412(%rbx), %edx
	movq	32(%rax), %rax
	movl	5408(%rbx), %esi
	movq	5400(%rbx), %rdi
	testq	%rax, %rax
	je	.L396
	call	*%rax
.L266:
	movq	%rax, 5424(%rbx)
	movq	5480(%rbx), %rax
	movslq	5464(%rbx), %rcx
	movl	5460(%rbx), %edx
	movq	32(%rax), %rax
	movl	5456(%rbx), %esi
	movq	5448(%rbx), %rdi
	testq	%rax, %rax
	je	.L397
	call	*%rax
.L267:
	movq	%rax, 5472(%rbx)
	movq	5528(%rbx), %rax
	movslq	5512(%rbx), %rcx
	movl	5508(%rbx), %edx
	movq	32(%rax), %rax
	movl	5504(%rbx), %esi
	movq	5496(%rbx), %rdi
	testq	%rax, %rax
	je	.L398
	call	*%rax
.L268:
	movq	%rax, 5520(%rbx)
	movq	5568(%rbx), %rax
	movslq	5552(%rbx), %rcx
	movl	5548(%rbx), %edx
	movq	32(%rax), %rax
	movl	5544(%rbx), %esi
	movq	5536(%rbx), %rdi
	testq	%rax, %rax
	je	.L399
	call	*%rax
.L269:
	movq	%rax, 5560(%rbx)
	movq	5608(%rbx), %rax
	movslq	5592(%rbx), %rcx
	movl	5588(%rbx), %edx
	movq	32(%rax), %rax
	movl	5584(%rbx), %esi
	movq	5576(%rbx), %rdi
	testq	%rax, %rax
	je	.L400
	call	*%rax
.L270:
	movq	%rax, 5600(%rbx)
	movq	5648(%rbx), %rax
	movslq	5632(%rbx), %rcx
	movl	5628(%rbx), %edx
	movq	32(%rax), %rax
	movl	5624(%rbx), %esi
	movq	5616(%rbx), %rdi
	testq	%rax, %rax
	je	.L401
	call	*%rax
.L271:
	movq	%rax, 5640(%rbx)
	movq	5688(%rbx), %rax
	movslq	5672(%rbx), %rcx
	movl	5668(%rbx), %edx
	movq	32(%rax), %rax
	movl	5664(%rbx), %esi
	movq	5656(%rbx), %rdi
	testq	%rax, %rax
	je	.L402
	call	*%rax
.L272:
	movq	%rax, 5680(%rbx)
	movq	5728(%rbx), %rax
	movslq	5712(%rbx), %rcx
	movl	5708(%rbx), %edx
	movq	32(%rax), %rax
	movl	5704(%rbx), %esi
	movq	5696(%rbx), %rdi
	testq	%rax, %rax
	je	.L403
	call	*%rax
.L273:
	movq	%rax, 5720(%rbx)
	movq	5768(%rbx), %rax
	movslq	5752(%rbx), %rcx
	movl	5748(%rbx), %edx
	movq	32(%rax), %rax
	movl	5744(%rbx), %esi
	movq	5736(%rbx), %rdi
	testq	%rax, %rax
	je	.L404
	call	*%rax
.L274:
	movq	%rax, 5760(%rbx)
	movq	5808(%rbx), %rax
	movslq	5792(%rbx), %rcx
	movl	5788(%rbx), %edx
	movq	32(%rax), %rax
	movl	5784(%rbx), %esi
	movq	5776(%rbx), %rdi
	testq	%rax, %rax
	je	.L405
	call	*%rax
.L275:
	movq	%rax, 5800(%rbx)
	movq	5848(%rbx), %rax
	movslq	5832(%rbx), %rcx
	movl	5828(%rbx), %edx
	movq	32(%rax), %rax
	movl	5824(%rbx), %esi
	movq	5816(%rbx), %rdi
	testq	%rax, %rax
	je	.L406
	call	*%rax
.L276:
	movq	%rax, 5840(%rbx)
	movq	5888(%rbx), %rax
	movslq	5872(%rbx), %rcx
	movl	5868(%rbx), %edx
	movq	32(%rax), %rax
	movl	5864(%rbx), %esi
	movq	5856(%rbx), %rdi
	testq	%rax, %rax
	je	.L407
	call	*%rax
.L277:
	movq	%rax, 5880(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L279:
	xorl	%eax, %eax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L280:
	xorl	%eax, %eax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L281:
	xorl	%eax, %eax
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L282:
	xorl	%eax, %eax
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L283:
	xorl	%eax, %eax
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L284:
	xorl	%eax, %eax
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L285:
	xorl	%eax, %eax
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L286:
	xorl	%eax, %eax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L287:
	xorl	%eax, %eax
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L288:
	xorl	%eax, %eax
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L289:
	xorl	%eax, %eax
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L290:
	xorl	%eax, %eax
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L291:
	xorl	%eax, %eax
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L292:
	xorl	%eax, %eax
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L293:
	xorl	%eax, %eax
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L294:
	xorl	%eax, %eax
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L295:
	xorl	%eax, %eax
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L296:
	xorl	%eax, %eax
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L297:
	xorl	%eax, %eax
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L298:
	xorl	%eax, %eax
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L299:
	xorl	%eax, %eax
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L300:
	xorl	%eax, %eax
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L301:
	xorl	%eax, %eax
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L302:
	xorl	%eax, %eax
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L303:
	xorl	%eax, %eax
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L304:
	xorl	%eax, %eax
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L305:
	xorl	%eax, %eax
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L306:
	xorl	%eax, %eax
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L307:
	xorl	%eax, %eax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L308:
	xorl	%eax, %eax
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L309:
	xorl	%eax, %eax
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L310:
	xorl	%eax, %eax
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L311:
	xorl	%eax, %eax
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L312:
	xorl	%eax, %eax
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L313:
	xorl	%eax, %eax
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L314:
	xorl	%eax, %eax
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L315:
	xorl	%eax, %eax
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L316:
	xorl	%eax, %eax
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L317:
	xorl	%eax, %eax
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L318:
	xorl	%eax, %eax
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L319:
	xorl	%eax, %eax
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L320:
	xorl	%eax, %eax
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L321:
	xorl	%eax, %eax
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L322:
	xorl	%eax, %eax
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L323:
	xorl	%eax, %eax
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L324:
	xorl	%eax, %eax
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L325:
	xorl	%eax, %eax
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L326:
	xorl	%eax, %eax
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L327:
	xorl	%eax, %eax
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L328:
	xorl	%eax, %eax
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L329:
	xorl	%eax, %eax
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L330:
	xorl	%eax, %eax
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L331:
	xorl	%eax, %eax
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L332:
	xorl	%eax, %eax
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L333:
	xorl	%eax, %eax
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L334:
	xorl	%eax, %eax
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L335:
	xorl	%eax, %eax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L336:
	xorl	%eax, %eax
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L337:
	xorl	%eax, %eax
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L338:
	xorl	%eax, %eax
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L339:
	xorl	%eax, %eax
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L340:
	xorl	%eax, %eax
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L341:
	xorl	%eax, %eax
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L342:
	xorl	%eax, %eax
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L343:
	xorl	%eax, %eax
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L344:
	xorl	%eax, %eax
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L345:
	xorl	%eax, %eax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L346:
	xorl	%eax, %eax
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L347:
	xorl	%eax, %eax
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L348:
	xorl	%eax, %eax
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L349:
	xorl	%eax, %eax
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L350:
	xorl	%eax, %eax
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L351:
	xorl	%eax, %eax
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L352:
	xorl	%eax, %eax
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L353:
	xorl	%eax, %eax
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L354:
	xorl	%eax, %eax
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L355:
	xorl	%eax, %eax
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L356:
	xorl	%eax, %eax
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L357:
	xorl	%eax, %eax
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L358:
	xorl	%eax, %eax
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L359:
	xorl	%eax, %eax
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L360:
	xorl	%eax, %eax
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L361:
	xorl	%eax, %eax
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L362:
	xorl	%eax, %eax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L363:
	xorl	%eax, %eax
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L364:
	xorl	%eax, %eax
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L365:
	xorl	%eax, %eax
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L366:
	xorl	%eax, %eax
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L367:
	xorl	%eax, %eax
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L368:
	xorl	%eax, %eax
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L369:
	xorl	%eax, %eax
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L370:
	xorl	%eax, %eax
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L371:
	xorl	%eax, %eax
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L372:
	xorl	%eax, %eax
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L373:
	xorl	%eax, %eax
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L374:
	xorl	%eax, %eax
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L375:
	xorl	%eax, %eax
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L376:
	xorl	%eax, %eax
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L377:
	xorl	%eax, %eax
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L378:
	xorl	%eax, %eax
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L379:
	xorl	%eax, %eax
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L380:
	xorl	%eax, %eax
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L381:
	xorl	%eax, %eax
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L382:
	xorl	%eax, %eax
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L383:
	xorl	%eax, %eax
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L384:
	xorl	%eax, %eax
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L385:
	xorl	%eax, %eax
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L386:
	xorl	%eax, %eax
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L387:
	xorl	%eax, %eax
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L388:
	xorl	%eax, %eax
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L389:
	xorl	%eax, %eax
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L390:
	xorl	%eax, %eax
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L391:
	xorl	%eax, %eax
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L392:
	xorl	%eax, %eax
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L393:
	xorl	%eax, %eax
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L394:
	xorl	%eax, %eax
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L395:
	xorl	%eax, %eax
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L396:
	xorl	%eax, %eax
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L397:
	xorl	%eax, %eax
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L398:
	xorl	%eax, %eax
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L399:
	xorl	%eax, %eax
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L400:
	xorl	%eax, %eax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L401:
	xorl	%eax, %eax
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L402:
	xorl	%eax, %eax
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L403:
	xorl	%eax, %eax
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L404:
	xorl	%eax, %eax
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L405:
	xorl	%eax, %eax
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L406:
	xorl	%eax, %eax
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L407:
	xorl	%eax, %eax
	jmp	.L277
	.cfi_endproc
.LFE18698:
	.size	_ZN2v88internal8Counters28ResetCreateHistogramFunctionEPFPvPKciimE, .-_ZN2v88internal8Counters28ResetCreateHistogramFunctionEPFPvPKciimE
	.section	.rodata._ZN2v88internal22RuntimeCallStatEntries5Entry5PrintERSo.str1.1,"aMS",@progbits,1
.LC32:
	.string	"ms "
.LC33:
	.string	"%"
.LC34:
	.string	" "
	.section	.text._ZN2v88internal22RuntimeCallStatEntries5Entry5PrintERSo,"axG",@progbits,_ZN2v88internal22RuntimeCallStatEntries5Entry5PrintERSo,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal22RuntimeCallStatEntries5Entry5PrintERSo
	.type	_ZN2v88internal22RuntimeCallStatEntries5Entry5PrintERSo, @function
_ZN2v88internal22RuntimeCallStatEntries5Entry5PrintERSo:
.LFB18705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	-24(%rax), %rax
	movq	$2, 8(%rsi,%rax)
	movq	(%rsi), %rax
	movq	-24(%rax), %rdx
	addq	%rsi, %rdx
	movl	24(%rdx), %eax
	andl	$-261, %eax
	orl	$4, %eax
	movl	%eax, 24(%rdx)
	movq	(%rsi), %rax
	movq	-24(%rax), %rax
	movq	$2, 8(%rsi,%rax)
	movq	(%rsi), %rax
	movq	-24(%rax), %rax
	movq	$50, 16(%rsi,%rax)
	movq	(%rdi), %r13
	testq	%r13, %r13
	je	.L417
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L411:
	movq	(%r12), %rax
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movq	-24(%rax), %rax
	movq	$10, 16(%r12,%rax)
	cvtsi2sdq	8(%rbx), %xmm0
	divsd	.LC31(%rip), %xmm0
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movl	$3, %edx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	$6, 16(%r12,%rax)
	movsd	24(%rbx), %xmm0
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC33(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	$10, 16(%r12,%rax)
	movq	16(%rbx), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC34(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	$6, 16(%r12,%rax)
	movsd	32(%rbx), %xmm0
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC33(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r13
	testq	%r13, %r13
	je	.L418
	cmpb	$0, 56(%r13)
	je	.L413
	movsbl	67(%r13), %esi
.L414:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	addq	$8, %rsp
	popq	%rbx
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSo5flushEv@PLT
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L414
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L417:
	movq	(%rsi), %rax
	movq	-24(%rax), %rdi
	addq	%rsi, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L411
.L418:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE18705:
	.size	_ZN2v88internal22RuntimeCallStatEntries5Entry5PrintERSo, .-_ZN2v88internal22RuntimeCallStatEntries5Entry5PrintERSo
	.section	.text._ZN2v88internal22RuntimeCallStatEntries5Entry8SetTotalENS_4base9TimeDeltaEm,"axG",@progbits,_ZN2v88internal22RuntimeCallStatEntries5Entry8SetTotalENS_4base9TimeDeltaEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal22RuntimeCallStatEntries5Entry8SetTotalENS_4base9TimeDeltaEm
	.type	_ZN2v88internal22RuntimeCallStatEntries5Entry8SetTotalENS_4base9TimeDeltaEm, @function
_ZN2v88internal22RuntimeCallStatEntries5Entry8SetTotalENS_4base9TimeDeltaEm:
.LFB18706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-40(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r13, %rdi
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%rsi, -40(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	testq	%rax, %rax
	jne	.L420
	movq	$0x000000000, 24(%r12)
	movsd	.LC36(%rip), %xmm1
.L421:
	movq	16(%r12), %rax
	testq	%rax, %rax
	js	.L422
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	mulsd	%xmm1, %xmm0
	testq	%rbx, %rbx
	js	.L424
.L427:
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rbx, %xmm1
.L425:
	divsd	%xmm1, %xmm0
	movsd	%xmm0, 32(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm0
	testq	%rbx, %rbx
	jns	.L427
.L424:
	movq	%rbx, %rax
	andl	$1, %ebx
	pxor	%xmm1, %xmm1
	shrq	%rax
	orq	%rbx, %rax
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L420:
	pxor	%xmm0, %xmm0
	movsd	.LC36(%rip), %xmm1
	movq	%r13, %rdi
	cvtsi2sdq	8(%r12), %xmm0
	movsd	%xmm1, -56(%rbp)
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, -48(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	pxor	%xmm2, %xmm2
	movsd	-48(%rbp), %xmm0
	movsd	-56(%rbp), %xmm1
	cvtsi2sdq	%rax, %xmm2
	divsd	%xmm2, %xmm0
	movsd	%xmm0, 24(%r12)
	jmp	.L421
	.cfi_endproc
.LFE18706:
	.size	_ZN2v88internal22RuntimeCallStatEntries5Entry8SetTotalENS_4base9TimeDeltaEm, .-_ZN2v88internal22RuntimeCallStatEntries5Entry8SetTotalENS_4base9TimeDeltaEm
	.section	.text._ZN2v88internal18RuntimeCallCounter5ResetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18RuntimeCallCounter5ResetEv
	.type	_ZN2v88internal18RuntimeCallCounter5ResetEv, @function
_ZN2v88internal18RuntimeCallCounter5ResetEv:
.LFB18707:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18707:
	.size	_ZN2v88internal18RuntimeCallCounter5ResetEv, .-_ZN2v88internal18RuntimeCallCounter5ResetEv
	.section	.text._ZN2v88internal16RuntimeCallStats5ResetEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal16RuntimeCallStats5ResetEv.part.0, @function
_ZN2v88internal16RuntimeCallStats5ResetEv.part.0:
.LFB23107:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L431:
	call	*_ZN2v88internal16RuntimeCallTimer3NowE(%rip)
	movq	%r14, %rdi
	movq	%rax, %r13
	subq	16(%rbx), %rax
	addq	%rax, 24(%rbx)
	movq	(%rbx), %rax
	movq	$0, 16(%rbx)
	addq	$1, 8(%rax)
	movq	(%rbx), %r15
	movq	24(%rbx), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	addq	%rax, 16(%r15)
	movq	$0, 24(%rbx)
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L432
	movq	%r13, 16(%rax)
.L432:
	movq	%rax, (%r12)
.L434:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L430
	movq	(%r12), %rbx
	cmpq	$0, 16(%rbx)
	jne	.L431
	movq	8(%rbx), %rax
	jmp	.L432
.L430:
	leaq	24(%r12), %rax
	leaq	27648(%r12), %rdx
	.p2align 4,,10
	.p2align 3
.L435:
	movq	%rax, %rdi
	call	_ZN2v88internal18RuntimeCallCounter5ResetEv
	addq	$24, %rax
	cmpq	%rax, %rdx
	jne	.L435
	movb	$1, 16(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L442
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L442:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23107:
	.size	_ZN2v88internal16RuntimeCallStats5ResetEv.part.0, .-_ZN2v88internal16RuntimeCallStats5ResetEv.part.0
	.section	.text._ZN2v88internal18RuntimeCallCounter4DumpEPNS_7tracing11TracedValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18RuntimeCallCounter4DumpEPNS_7tracing11TracedValueE
	.type	_ZN2v88internal18RuntimeCallCounter4DumpEPNS_7tracing11TracedValueE, @function
_ZN2v88internal18RuntimeCallCounter4DumpEPNS_7tracing11TracedValueE:
.LFB18708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rsi
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v87tracing11TracedValue10BeginArrayEPKc@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cvtsi2sdq	8(%rbx), %xmm0
	call	_ZN2v87tracing11TracedValue12AppendDoubleEd@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cvtsi2sdq	16(%rbx), %xmm0
	call	_ZN2v87tracing11TracedValue12AppendDoubleEd@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87tracing11TracedValue8EndArrayEv@PLT
	.cfi_endproc
.LFE18708:
	.size	_ZN2v88internal18RuntimeCallCounter4DumpEPNS_7tracing11TracedValueE, .-_ZN2v88internal18RuntimeCallCounter4DumpEPNS_7tracing11TracedValueE
	.section	.text._ZN2v88internal18RuntimeCallCounter3AddEPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18RuntimeCallCounter3AddEPS1_
	.type	_ZN2v88internal18RuntimeCallCounter3AddEPS1_, @function
_ZN2v88internal18RuntimeCallCounter3AddEPS1_:
.LFB18709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	addq	%rax, 8(%rdi)
	leaq	-32(%rbp), %rdi
	movq	16(%rsi), %rax
	movq	%rax, -32(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	addq	%rax, 16(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L448
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L448:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18709:
	.size	_ZN2v88internal18RuntimeCallCounter3AddEPS1_, .-_ZN2v88internal18RuntimeCallCounter3AddEPS1_
	.section	.text._ZN2v88internal16RuntimeCallTimer8SnapshotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RuntimeCallTimer8SnapshotEv
	.type	_ZN2v88internal16RuntimeCallTimer8SnapshotEv, @function
_ZN2v88internal16RuntimeCallTimer8SnapshotEv:
.LFB18710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-64(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r13, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	*_ZN2v88internal16RuntimeCallTimer3NowE(%rip)
	movq	%rax, %r14
	subq	16(%r13), %rax
	addq	%rax, 24(%r13)
	movq	$0, 16(%r13)
	.p2align 4,,10
	.p2align 3
.L450:
	movq	(%rbx), %r15
	movq	24(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	addq	%rax, 16(%r15)
	movq	$0, 24(%rbx)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L450
	movq	%r14, 16(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L454
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L454:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18710:
	.size	_ZN2v88internal16RuntimeCallTimer8SnapshotEv, .-_ZN2v88internal16RuntimeCallTimer8SnapshotEv
	.section	.text._ZN2v88internal16RuntimeCallStatsC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RuntimeCallStatsC2Ev
	.type	_ZN2v88internal16RuntimeCallStatsC2Ev, @function
_ZN2v88internal16RuntimeCallStatsC2Ev:
.LFB18712:
	.cfi_startproc
	endbr64
	movb	$0, 16(%rdi)
	pxor	%xmm0, %xmm0
	leaq	24(%rdi), %rax
	leaq	27648(%rdi), %rdx
	movl	$-1, 20(%rdi)
	movups	%xmm0, (%rdi)
	.p2align 4,,10
	.p2align 3
.L456:
	movq	$0, (%rax)
	addq	$24, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rdx, %rax
	jne	.L456
	leaq	_ZZN2v88internal16RuntimeCallStatsC4EvE6kNames(%rip), %rdx
	leaq	32(%rdi), %rax
	pxor	%xmm0, %xmm0
	leaq	9208(%rdx), %rsi
	.p2align 4,,10
	.p2align 3
.L457:
	movq	(%rdx), %rcx
	addq	$8, %rdx
	movups	%xmm0, (%rax)
	addq	$24, %rax
	movq	%rcx, -32(%rax)
	cmpq	%rsi, %rdx
	jne	.L457
	ret
	.cfi_endproc
.LFE18712:
	.size	_ZN2v88internal16RuntimeCallStatsC2Ev, .-_ZN2v88internal16RuntimeCallStatsC2Ev
	.globl	_ZN2v88internal16RuntimeCallStatsC1Ev
	.set	_ZN2v88internal16RuntimeCallStatsC1Ev,_ZN2v88internal16RuntimeCallStatsC2Ev
	.section	.text._ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE
	.type	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE, @function
_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE:
.LFB18714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	leaq	3(%rdx,%rdx,2), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	(%rdi,%rax,8), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %r13
	movq	%rdi, %rbx
	movq	%r14, (%rsi)
	movq	%r13, 8(%rsi)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	cmpl	$4, %eax
	je	.L461
	call	*_ZN2v88internal16RuntimeCallTimer3NowE(%rip)
	testq	%r13, %r13
	je	.L462
	movq	%rax, %rdx
	subq	16(%r13), %rdx
	addq	%rdx, 24(%r13)
	movq	$0, 16(%r13)
.L462:
	movq	%rax, 16(%r12)
.L461:
	movq	%r12, (%rbx)
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18714:
	.size	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE, .-_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE
	.section	.rodata._ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE.str1.1,"aMS",@progbits,1
.LC37:
	.string	"stack_top == timer"
.LC38:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE
	.type	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE, @function
_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE:
.LFB18715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L470
	movq	%rsi, %rbx
	cmpq	%rax, %rsi
	jne	.L488
	cmpq	$0, 16(%rsi)
	movq	%rdi, %r12
	je	.L489
	call	*_ZN2v88internal16RuntimeCallTimer3NowE(%rip)
	leaq	-48(%rbp), %rdi
	movq	%rax, %r13
	subq	16(%rbx), %rax
	addq	%rax, 24(%rbx)
	movq	(%rbx), %rax
	movq	$0, 16(%rbx)
	addq	$1, 8(%rax)
	movq	(%rbx), %r14
	movq	24(%rbx), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	addq	%rax, 16(%r14)
	movq	$0, 24(%rbx)
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L474
	movq	%r13, 16(%rax)
.L474:
	movq	%rax, (%r12)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L476
	movq	(%rax), %rax
.L476:
	movq	%rax, 8(%r12)
.L470:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L490
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L489:
	.cfi_restore_state
	movq	8(%rsi), %rax
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L488:
	leaq	.LC37(%rip), %rsi
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L490:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18715:
	.size	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE, .-_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE
	.section	.text._ZN2v88internal16RuntimeCallStats3AddEPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RuntimeCallStats3AddEPS1_
	.type	_ZN2v88internal16RuntimeCallStats3AddEPS1_, @function
_ZN2v88internal16RuntimeCallStats3AddEPS1_:
.LFB18716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-48(%rbp), %r14
	leaq	27664(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	40(%rsi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	40(%rdi), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L492:
	movq	-8(%r12), %rax
	addq	%rax, -8(%rbx)
	movq	%r14, %rdi
	addq	$24, %r12
	movq	-24(%r12), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	addq	%rax, (%rbx)
	addq	$24, %rbx
	cmpq	%r13, %r12
	jne	.L492
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L496
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L496:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18716:
	.size	_ZN2v88internal16RuntimeCallStats3AddEPS1_, .-_ZN2v88internal16RuntimeCallStats3AddEPS1_
	.section	.text._ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE
	.type	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE, @function
_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE:
.LFB18717:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L497
	movslq	%esi, %rsi
	leaq	3(%rsi,%rsi,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	movq	%rdx, (%rax)
	movq	%rdx, 8(%rdi)
.L497:
	ret
	.cfi_endproc
.LFE18717:
	.size	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE, .-_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE
	.section	.text._ZN2v88internal16RuntimeCallStats23IsCalledOnTheSameThreadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RuntimeCallStats23IsCalledOnTheSameThreadEv
	.type	_ZN2v88internal16RuntimeCallStats23IsCalledOnTheSameThreadEv, @function
_ZN2v88internal16RuntimeCallStats23IsCalledOnTheSameThreadEv:
.LFB18718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$-1, 20(%rdi)
	je	.L503
	call	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv@PLT
	cmpl	%eax, 20(%rbx)
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	.cfi_restore_state
	call	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv@PLT
	movl	%eax, 20(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18718:
	.size	_ZN2v88internal16RuntimeCallStats23IsCalledOnTheSameThreadEv, .-_ZN2v88internal16RuntimeCallStats23IsCalledOnTheSameThreadEv
	.section	.text._ZN2v88internal16RuntimeCallStats5ResetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RuntimeCallStats5ResetEv
	.type	_ZN2v88internal16RuntimeCallStats5ResetEv, @function
_ZN2v88internal16RuntimeCallStats5ResetEv:
.LFB18736:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L508
	ret
	.p2align 4,,10
	.p2align 3
.L508:
	jmp	_ZN2v88internal16RuntimeCallStats5ResetEv.part.0
	.cfi_endproc
.LFE18736:
	.size	_ZN2v88internal16RuntimeCallStats5ResetEv, .-_ZN2v88internal16RuntimeCallStats5ResetEv
	.section	.text._ZN2v88internal16RuntimeCallStats4DumpEPNS_7tracing11TracedValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RuntimeCallStats4DumpEPNS_7tracing11TracedValueE
	.type	_ZN2v88internal16RuntimeCallStats4DumpEPNS_7tracing11TracedValueE, @function
_ZN2v88internal16RuntimeCallStats4DumpEPNS_7tracing11TracedValueE:
.LFB18737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	27648(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	24(%rdi), %rbx
	.p2align 4,,10
	.p2align 3
.L511:
	cmpq	$0, 8(%rbx)
	jle	.L510
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal18RuntimeCallCounter4DumpEPNS_7tracing11TracedValueE
.L510:
	addq	$24, %rbx
	cmpq	%r12, %rbx
	jne	.L511
	popq	%rbx
	popq	%r12
	movb	$0, 16(%r14)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18737:
	.size	_ZN2v88internal16RuntimeCallStats4DumpEPNS_7tracing11TracedValueE, .-_ZN2v88internal16RuntimeCallStats4DumpEPNS_7tracing11TracedValueE
	.section	.text._ZN2v88internal28WorkerThreadRuntimeCallStatsC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28WorkerThreadRuntimeCallStatsC2Ev
	.type	_ZN2v88internal28WorkerThreadRuntimeCallStatsC2Ev, @function
_ZN2v88internal28WorkerThreadRuntimeCallStatsC2Ev:
.LFB18760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v84base5MutexC1Ev@PLT
	pxor	%xmm0, %xmm0
	movb	$0, 64(%rbx)
	movq	$0, 56(%rbx)
	movb	$0, 68(%rbx)
	movups	%xmm0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18760:
	.size	_ZN2v88internal28WorkerThreadRuntimeCallStatsC2Ev, .-_ZN2v88internal28WorkerThreadRuntimeCallStatsC2Ev
	.globl	_ZN2v88internal28WorkerThreadRuntimeCallStatsC1Ev
	.set	_ZN2v88internal28WorkerThreadRuntimeCallStatsC1Ev,_ZN2v88internal28WorkerThreadRuntimeCallStatsC2Ev
	.section	.text._ZN2v88internal28WorkerThreadRuntimeCallStatsD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28WorkerThreadRuntimeCallStatsD2Ev
	.type	_ZN2v88internal28WorkerThreadRuntimeCallStatsD2Ev, @function
_ZN2v88internal28WorkerThreadRuntimeCallStatsD2Ev:
.LFB18763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpb	$0, 64(%rdi)
	jne	.L528
.L517:
	movq	48(%r13), %rbx
	movq	40(%r13), %r12
	cmpq	%r12, %rbx
	je	.L518
	.p2align 4,,10
	.p2align 3
.L522:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L519
	movl	$27648, %esi
	addq	$8, %r12
	call	_ZdlPvm@PLT
	cmpq	%rbx, %r12
	jne	.L522
.L520:
	movq	40(%r13), %r12
.L518:
	testq	%r12, %r12
	je	.L523
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L523:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5MutexD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L519:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L522
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L528:
	movl	68(%rdi), %edi
	call	_ZN2v84base6Thread20DeleteThreadLocalKeyEi@PLT
	jmp	.L517
	.cfi_endproc
.LFE18763:
	.size	_ZN2v88internal28WorkerThreadRuntimeCallStatsD2Ev, .-_ZN2v88internal28WorkerThreadRuntimeCallStatsD2Ev
	.globl	_ZN2v88internal28WorkerThreadRuntimeCallStatsD1Ev
	.set	_ZN2v88internal28WorkerThreadRuntimeCallStatsD1Ev,_ZN2v88internal28WorkerThreadRuntimeCallStatsD2Ev
	.section	.text._ZN2v88internal28WorkerThreadRuntimeCallStats6GetKeyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28WorkerThreadRuntimeCallStats6GetKeyEv
	.type	_ZN2v88internal28WorkerThreadRuntimeCallStats6GetKeyEv, @function
_ZN2v88internal28WorkerThreadRuntimeCallStats6GetKeyEv:
.LFB18765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 64(%rdi)
	je	.L530
	movl	68(%rdi), %eax
.L529:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L530:
	.cfi_restore_state
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	cmpb	$0, 64(%rbx)
	movl	%eax, 68(%rbx)
	jne	.L529
	movb	$1, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18765:
	.size	_ZN2v88internal28WorkerThreadRuntimeCallStats6GetKeyEv, .-_ZN2v88internal28WorkerThreadRuntimeCallStats6GetKeyEv
	.section	.text._ZN2v88internal28WorkerThreadRuntimeCallStats14AddToMainTableEPNS0_16RuntimeCallStatsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28WorkerThreadRuntimeCallStats14AddToMainTableEPNS0_16RuntimeCallStatsE
	.type	_ZN2v88internal28WorkerThreadRuntimeCallStats14AddToMainTableEPNS0_16RuntimeCallStatsE, @function
_ZN2v88internal28WorkerThreadRuntimeCallStats14AddToMainTableEPNS0_16RuntimeCallStatsE:
.LFB18771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rdi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	48(%r15), %rax
	movq	40(%r15), %rsi
	movq	%rax, -72(%rbp)
	cmpq	%rax, %rsi
	je	.L535
	leaq	40(%rbx), %rax
	movq	%rsi, %r15
	leaq	-64(%rbp), %r13
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L538:
	movq	(%r15), %rbx
	movq	-80(%rbp), %r14
	leaq	40(%rbx), %r12
	addq	$27664, %rbx
	.p2align 4,,10
	.p2align 3
.L536:
	movq	-8(%r12), %rax
	addq	%rax, -8(%r14)
	movq	%r13, %rdi
	addq	$24, %r12
	movq	-24(%r12), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	addq	%rax, (%r14)
	addq	$24, %r14
	cmpq	%r12, %rbx
	jne	.L536
	movq	(%r15), %rdi
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L546
.L537:
	addq	$8, %r15
	cmpq	%r15, -72(%rbp)
	jne	.L538
.L535:
	movq	-88(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L547
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	.cfi_restore_state
	call	_ZN2v88internal16RuntimeCallStats5ResetEv.part.0
	jmp	.L537
.L547:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18771:
	.size	_ZN2v88internal28WorkerThreadRuntimeCallStats14AddToMainTableEPNS0_16RuntimeCallStatsE, .-_ZN2v88internal28WorkerThreadRuntimeCallStats14AddToMainTableEPNS0_16RuntimeCallStatsE
	.section	.rodata._ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD2Ev.str1.8,"aMS",@progbits,1
	.align 8
.LC39:
	.string	"disabled-by-default-v8.runtime_stats"
	.section	.rodata._ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD2Ev.str1.1,"aMS",@progbits,1
.LC40:
	.string	"runtime-call-stats"
.LC41:
	.string	"V8.RuntimeStats"
	.section	.text._ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD2Ev
	.type	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD2Ev, @function
_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD2Ev:
.LFB18777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	jne	.L577
.L548:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L578
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L577:
	.cfi_restore_state
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testb	$2, %al
	je	.L548
	movq	%rdi, %rbx
	leaq	-72(%rbp), %rdi
	call	_ZN2v87tracing11TracedValue6CreateEv@PLT
	movq	-72(%rbp), %rsi
	movq	(%rbx), %rdi
	call	_ZN2v88internal16RuntimeCallStats4DumpEPNS_7tracing11TracedValueE
	movq	_ZZN2v88internal33WorkerThreadRuntimeCallStatsScopeD4EvE28trace_event_unique_atomic597(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L579
.L552:
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L580
.L554:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L548
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L579:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L581
.L553:
	movq	%r12, _ZZN2v88internal33WorkerThreadRuntimeCallStatsScopeD4EvE28trace_event_unique_atomic597(%rip)
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L580:
	leaq	.LC40(%rip), %rax
	movb	$8, -73(%rbp)
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rax
	movq	$0, -40(%rbp)
	movq	$0, -72(%rbp)
	movq	%rax, -56(%rbp)
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L582
.L555:
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L556
	movq	(%rdi), %rax
	call	*8(%rax)
.L556:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L554
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L554
.L582:
	subq	$8, %rsp
	leaq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$16
	leaq	.LC41(%rip), %rcx
	movl	$73, %esi
	pushq	%rdx
	leaq	-56(%rbp), %rdx
	pushq	%rdx
	leaq	-73(%rbp), %rdx
	pushq	%rdx
	leaq	-64(%rbp), %rdx
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$1
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L555
.L581:
	leaq	.LC39(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L553
.L578:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18777:
	.size	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD2Ev, .-_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD2Ev
	.globl	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD1Ev
	.set	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD1Ev,_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeD2Ev
	.section	.rodata._ZNSt6vectorIN2v88internal22RuntimeCallStatEntries5EntryESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_.str1.1,"aMS",@progbits,1
.LC42:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal22RuntimeCallStatEntries5EntryESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal22RuntimeCallStatEntries5EntryESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal22RuntimeCallStatEntries5EntryESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal22RuntimeCallStatEntries5EntryESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal22RuntimeCallStatEntries5EntryESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB22435:
	.cfi_startproc
	endbr64
	movabsq	$-3689348814741910323, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r14
	movq	%rsi, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movabsq	$230584300921369395, %rcx
	cmpq	%rcx, %rax
	je	.L600
	movq	%r12, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L592
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L601
.L585:
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r8
	movq	%rax, %r13
	leaq	(%rax,%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	40(%r13), %rbx
.L591:
	movdqu	(%rdx), %xmm3
	movdqu	16(%rdx), %xmm4
	movq	32(%rdx), %rax
	movups	%xmm3, 0(%r13,%r8)
	movq	%rax, 32(%r13,%r8)
	movups	%xmm4, 16(%r13,%r8)
	cmpq	%r14, %r12
	je	.L587
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L588:
	movdqu	(%rax), %xmm1
	addq	$40, %rax
	addq	$40, %rdx
	movups	%xmm1, -40(%rdx)
	movdqu	-24(%rax), %xmm2
	movups	%xmm2, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L588
	leaq	-40(%r12), %rax
	subq	%r14, %rax
	shrq	$3, %rax
	leaq	80(%r13,%rax,8), %rbx
.L587:
	cmpq	%rsi, %r12
	je	.L589
	subq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-40(%rsi), %rax
	movq	%r12, %rsi
	shrq	$3, %rax
	leaq	40(,%rax,8), %rdx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, %rbx
.L589:
	testq	%r14, %r14
	je	.L590
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L590:
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rbx, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L601:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L586
	movq	$0, -56(%rbp)
	movl	$40, %ebx
	xorl	%r13d, %r13d
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L592:
	movl	$40, %ebx
	jmp	.L585
.L586:
	cmpq	%rcx, %rdi
	movq	%rcx, %rbx
	cmovbe	%rdi, %rbx
	imulq	$40, %rbx, %rbx
	jmp	.L585
.L600:
	leaq	.LC42(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22435:
	.size	_ZNSt6vectorIN2v88internal22RuntimeCallStatEntries5EntryESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal22RuntimeCallStatEntries5EntryESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZN2v88internal22RuntimeCallStatEntries3AddEPNS0_18RuntimeCallCounterE,"axG",@progbits,_ZN2v88internal22RuntimeCallStatEntries3AddEPNS0_18RuntimeCallCounterE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal22RuntimeCallStatEntries3AddEPNS0_18RuntimeCallCounterE
	.type	_ZN2v88internal22RuntimeCallStatEntries3AddEPNS0_18RuntimeCallCounterE, @function
_ZN2v88internal22RuntimeCallStatEntries3AddEPNS0_18RuntimeCallCounterE:
.LFB18700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	jne	.L611
.L602:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L612
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L611:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	16(%rsi), %rdx
	movq	%rdi, %rbx
	leaq	-88(%rbp), %rdi
	movq	%rsi, %r12
	movq	%rdx, -88(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	%r13, -64(%rbp)
	movq	24(%rbx), %rsi
	movapd	.LC43(%rip), %xmm0
	movq	%rax, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	cmpq	32(%rbx), %rsi
	je	.L604
	movdqa	-80(%rbp), %xmm1
	movups	%xmm1, (%rsi)
	movdqa	-64(%rbp), %xmm2
	movups	%xmm2, 16(%rsi)
	movq	-48(%rbp), %rax
	movq	%rax, 32(%rsi)
	addq	$40, 24(%rbx)
.L605:
	movq	16(%r12), %rax
	addq	%rax, 8(%rbx)
	movq	8(%r12), %rax
	addq	%rax, (%rbx)
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L604:
	leaq	-80(%rbp), %rdx
	leaq	16(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal22RuntimeCallStatEntries5EntryESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L605
.L612:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18700:
	.size	_ZN2v88internal22RuntimeCallStatEntries3AddEPNS0_18RuntimeCallCounterE, .-_ZN2v88internal22RuntimeCallStatEntries3AddEPNS0_18RuntimeCallCounterE
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v88internal16RuntimeCallStatsESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v88internal16RuntimeCallStatsESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88internal16RuntimeCallStatsESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN2v88internal16RuntimeCallStatsESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN2v88internal16RuntimeCallStatsESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_:
.LFB22479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r14
	movq	(%rdi), %r8
	movabsq	$1152921504606846975, %rdi
	movq	%r14, %rax
	subq	%r8, %rax
	sarq	$3, %rax
	cmpq	%rdi, %rax
	je	.L637
	movq	%rsi, %rbx
	movq	%rsi, %r9
	subq	%r8, %rsi
	testq	%rax, %rax
	je	.L628
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %r10
	cmpq	%r10, %rax
	jbe	.L638
.L615:
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	%rax, %r13
	addq	%rax, %r15
	movq	-88(%rbp), %rdx
	leaq	8(%rax), %r12
.L627:
	movq	(%rdx), %rax
	movq	$0, (%rdx)
	movq	%rax, 0(%r13,%rsi)
	cmpq	%r8, %rbx
	je	.L617
	movq	%r13, %rax
	movq	%r8, %r12
	.p2align 4,,10
	.p2align 3
.L621:
	movq	(%r12), %rdx
	movq	$0, (%r12)
	movq	%rdx, (%rax)
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L618
	movl	$27648, %esi
	movq	%rcx, -80(%rbp)
	addq	$8, %r12
	movq	%r9, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %rcx
	addq	$8, %rax
	cmpq	%r12, %rbx
	jne	.L621
.L619:
	movq	%rbx, %rax
	subq	%r8, %rax
	leaq	8(%r13,%rax), %r12
.L617:
	cmpq	%r14, %rbx
	je	.L622
	subq	%rbx, %r14
	leaq	-8(%r14), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	addq	$1, %rdx
	testq	%rsi, %rsi
	je	.L630
	movq	%rdx, %rdi
	xorl	%eax, %eax
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L624:
	movdqu	(%rbx,%rax), %xmm1
	movups	%xmm1, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L624
	movq	%rdx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rax
	leaq	(%rbx,%rax), %r9
	addq	%r12, %rax
	cmpq	%rdi, %rdx
	je	.L625
.L623:
	movq	(%r9), %rdx
	movq	%rdx, (%rax)
.L625:
	leaq	8(%r12,%rsi), %r12
.L622:
	testq	%r8, %r8
	je	.L626
	movq	%r8, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rcx
.L626:
	movq	%r13, %xmm0
	movq	%r12, %xmm2
	movq	%r15, 16(%rcx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rcx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L618:
	.cfi_restore_state
	addq	$8, %r12
	addq	$8, %rax
	cmpq	%r12, %rbx
	jne	.L621
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L638:
	testq	%r10, %r10
	jne	.L616
	movl	$8, %r12d
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L628:
	movl	$8, %r15d
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L630:
	movq	%r12, %rax
	jmp	.L623
.L637:
	leaq	.LC42(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L616:
	cmpq	%rdi, %r10
	cmovbe	%r10, %rdi
	movq	%rdi, %r15
	salq	$3, %r15
	jmp	.L615
	.cfi_endproc
.LFE22479:
	.size	_ZNSt6vectorISt10unique_ptrIN2v88internal16RuntimeCallStatsESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN2v88internal16RuntimeCallStatsESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.section	.text._ZN2v88internal28WorkerThreadRuntimeCallStats8NewTableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28WorkerThreadRuntimeCallStats8NewTableEv
	.type	_ZN2v88internal28WorkerThreadRuntimeCallStats8NewTableEv, @function
_ZN2v88internal28WorkerThreadRuntimeCallStats8NewTableEv:
.LFB18767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$27648, %edi
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	movb	$0, 16(%rax)
	leaq	24(%rax), %rax
	movl	$-1, -4(%rax)
	leaq	27648(%r12), %rdx
	movups	%xmm0, -24(%rax)
	.p2align 4,,10
	.p2align 3
.L640:
	movq	$0, (%rax)
	addq	$24, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rdx, %rax
	jne	.L640
	leaq	_ZZN2v88internal16RuntimeCallStatsC4EvE6kNames(%rip), %rdx
	leaq	32(%r12), %rax
	pxor	%xmm0, %xmm0
	leaq	9208(%rdx), %rcx
	.p2align 4,,10
	.p2align 3
.L641:
	movq	(%rdx), %rsi
	addq	$8, %rdx
	movups	%xmm0, (%rax)
	addq	$24, %rax
	movq	%rsi, -32(%rax)
	cmpq	%rcx, %rdx
	jne	.L641
	movq	%r13, %rdi
	movq	%r12, -32(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	48(%r13), %rsi
	cmpq	56(%r13), %rsi
	je	.L642
	movq	-32(%rbp), %rax
	movq	$0, -32(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 48(%r13)
.L643:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L639
	movl	$27648, %esi
	call	_ZdlPvm@PLT
.L639:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L652
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L642:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	40(%r13), %rdi
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal16RuntimeCallStatsESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	jmp	.L643
.L652:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18767:
	.size	_ZN2v88internal28WorkerThreadRuntimeCallStats8NewTableEv, .-_ZN2v88internal28WorkerThreadRuntimeCallStats8NewTableEv
	.section	.text._ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC2EPNS0_28WorkerThreadRuntimeCallStatsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC2EPNS0_28WorkerThreadRuntimeCallStatsE
	.type	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC2EPNS0_28WorkerThreadRuntimeCallStatsE, @function
_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC2EPNS0_28WorkerThreadRuntimeCallStatsE:
.LFB18773:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L674
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	cmpb	$0, 64(%rsi)
	je	.L656
	movl	68(%rsi), %edi
.L657:
	movq	%rsi, -24(%rbp)
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movq	-24(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, (%rbx)
	je	.L675
.L659:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testb	$2, %al
	jne	.L676
.L653:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L656:
	.cfi_restore_state
	movq	%rsi, -24(%rbp)
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	movq	-24(%rbp), %rsi
	movl	%eax, %edi
	cmpb	$0, 64(%rsi)
	movl	%eax, 68(%rsi)
	jne	.L657
	movb	$1, 64(%rsi)
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L676:
	movq	(%rbx), %rdi
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L653
	addq	$16, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal16RuntimeCallStats5ResetEv.part.0
	.p2align 4,,10
	.p2align 3
.L675:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	_ZN2v88internal28WorkerThreadRuntimeCallStats8NewTableEv
	movq	-24(%rbp), %rsi
	movq	%rax, (%rbx)
	movq	%rax, %r12
	cmpb	$0, 64(%rsi)
	je	.L660
	movl	68(%rsi), %edi
.L661:
	movq	%r12, %rsi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	jmp	.L659
.L660:
	movq	%rsi, -24(%rbp)
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	movq	-24(%rbp), %rsi
	movl	%eax, %edi
	cmpb	$0, 64(%rsi)
	movl	%eax, 68(%rsi)
	jne	.L661
	movb	$1, 64(%rsi)
	jmp	.L661
	.cfi_endproc
.LFE18773:
	.size	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC2EPNS0_28WorkerThreadRuntimeCallStatsE, .-_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC2EPNS0_28WorkerThreadRuntimeCallStatsE
	.globl	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC1EPNS0_28WorkerThreadRuntimeCallStatsE
	.set	_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC1EPNS0_28WorkerThreadRuntimeCallStatsE,_ZN2v88internal33WorkerThreadRuntimeCallStatsScopeC2EPNS0_28WorkerThreadRuntimeCallStatsE
	.section	.text._ZSt16__insertion_sortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_T0_,"axG",@progbits,_ZSt16__insertion_sortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_T0_,comdat
	.p2align 4
	.weak	_ZSt16__insertion_sortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_T0_
	.type	_ZSt16__insertion_sortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_T0_, @function
_ZSt16__insertion_sortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_T0_:
.LFB22666:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	movq	(%rdi), %rax
	cmpq	%rax, %rdx
	je	.L688
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	-40(%rax), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rdx, %rsi
	je	.L677
	movq	-32(%rsi), %rbx
	movq	-24(%rsi), %r12
	cmpq	-32(%rax), %rbx
	jl	.L679
	.p2align 4,,10
	.p2align 3
.L692:
	jg	.L680
	cmpq	%r12, -24(%rax)
	ja	.L679
.L680:
	movq	%rsi, %rax
	movq	-40(%rsi), %rcx
	movsd	-16(%rsi), %xmm0
	movsd	-8(%rsi), %xmm1
	cmpq	8(%rax), %rbx
	jl	.L683
	jg	.L684
.L691:
	cmpq	16(%rax), %r12
	jnb	.L684
.L683:
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm3
	addq	$40, %rax
	movq	-8(%rax), %rdx
	movups	%xmm2, -80(%rax)
	movups	%xmm3, -64(%rax)
	movq	%rdx, -48(%rax)
	cmpq	8(%rax), %rbx
	jl	.L683
	jle	.L691
.L684:
	unpcklpd	%xmm1, %xmm0
	movq	%rcx, -40(%rax)
	leaq	-40(%rsi), %rcx
	movq	%rbx, -32(%rax)
	movq	%rcx, %rsi
	movq	%r12, -24(%rax)
	movups	%xmm0, -16(%rax)
	movq	(%r14), %rdx
	cmpq	%rdx, %rcx
	je	.L677
.L693:
	movq	0(%r13), %rax
	movq	-32(%rsi), %rbx
	movq	-24(%rsi), %r12
	cmpq	-32(%rax), %rbx
	jge	.L692
.L679:
	movq	%rax, %r8
	movq	-40(%rsi), %r15
	movsd	-16(%rsi), %xmm0
	leaq	-40(%rsi), %rcx
	subq	%rsi, %r8
	movsd	-8(%rsi), %xmm1
	testq	%r8, %r8
	jle	.L681
	movq	%r8, %rdx
	movq	%rcx, %rdi
	movsd	%xmm1, -64(%rbp)
	movsd	%xmm0, -56(%rbp)
	call	memmove@PLT
	movq	(%r14), %rdx
	movsd	-64(%rbp), %xmm1
	movq	%rax, %rcx
	movsd	-56(%rbp), %xmm0
	movq	0(%r13), %rax
.L681:
	unpcklpd	%xmm1, %xmm0
	movq	%r15, -40(%rax)
	movq	%rcx, %rsi
	movq	%rbx, -32(%rax)
	movq	%r12, -24(%rax)
	movups	%xmm0, -16(%rax)
	cmpq	%rdx, %rcx
	jne	.L693
.L677:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L688:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE22666:
	.size	_ZSt16__insertion_sortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_T0_, .-_ZSt16__insertion_sortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_T0_
	.section	.text._ZSt22__move_median_to_firstISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_SF_SF_T0_,"axG",@progbits,_ZSt22__move_median_to_firstISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_SF_SF_T0_,comdat
	.p2align 4
	.weak	_ZSt22__move_median_to_firstISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_SF_SF_T0_
	.type	_ZSt22__move_median_to_firstISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_SF_SF_T0_, @function
_ZSt22__move_median_to_firstISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_SF_SF_T0_:
.LFB22826:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rax
	movq	(%rsi), %rdx
	movq	-32(%rax), %rsi
	movq	-32(%rdx), %r8
	cmpq	%r8, %rsi
	jg	.L695
	jl	.L696
	movq	-24(%rax), %r10
	cmpq	%r10, -24(%rdx)
	jb	.L695
.L696:
	movq	(%rcx), %rcx
	movq	-32(%rcx), %r9
	cmpq	%r9, %r8
	jl	.L702
	jg	.L703
	movq	-24(%rcx), %r11
	cmpq	%r11, -24(%rdx)
	jb	.L702
.L703:
	cmpq	%r9, %rsi
	jl	.L704
	jg	.L705
	movq	-24(%rcx), %rsi
	cmpq	%rsi, -24(%rax)
	jnb	.L705
.L704:
	movq	(%rdi), %rax
	movdqu	-40(%rcx), %xmm4
	movq	-40(%rax), %rdi
	movq	-32(%rax), %rsi
	movups	%xmm4, -40(%rax)
	movdqu	-24(%rcx), %xmm5
	movq	-24(%rax), %rdx
	movsd	-16(%rax), %xmm0
	movsd	-8(%rax), %xmm1
	movups	%xmm5, -24(%rax)
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L695:
	movq	(%rcx), %rcx
	movq	-32(%rcx), %r9
	cmpq	%r9, %rsi
	jl	.L697
	jg	.L698
	movq	-24(%rcx), %rsi
	cmpq	%rsi, -24(%rax)
	jnb	.L698
.L697:
	movq	(%rdi), %rdx
	movdqu	-40(%rax), %xmm2
	movq	-40(%rdx), %rdi
	movq	-32(%rdx), %rsi
	movups	%xmm2, -40(%rdx)
	movdqu	-24(%rax), %xmm3
	movsd	-8(%rdx), %xmm1
	movq	-24(%rdx), %rcx
	movsd	-16(%rdx), %xmm0
	movups	%xmm3, -24(%rdx)
.L706:
	movq	-8(%rax), %r8
	unpcklpd	%xmm1, %xmm0
	movq	%r8, -8(%rdx)
	movq	%rdi, -40(%rax)
	movq	%rsi, -32(%rax)
	movq	%rcx, -24(%rax)
	movups	%xmm0, -16(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L698:
	cmpq	%r9, %r8
	jl	.L700
	jg	.L701
	movq	-24(%rcx), %rax
	cmpq	%rax, -24(%rdx)
	jb	.L700
.L701:
	movq	(%rdi), %rax
	movdqu	-40(%rdx), %xmm6
	movq	-40(%rax), %rdi
	movq	-32(%rax), %rsi
	movups	%xmm6, -40(%rax)
	movdqu	-24(%rdx), %xmm7
	movq	-24(%rax), %rcx
	movsd	-16(%rax), %xmm0
	movsd	-8(%rax), %xmm1
	movups	%xmm7, -24(%rax)
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L702:
	movq	(%rdi), %rax
	movdqu	-40(%rdx), %xmm4
	movq	-40(%rax), %rdi
	movq	-32(%rax), %rsi
	movups	%xmm4, -40(%rax)
	movdqu	-24(%rdx), %xmm5
	movsd	-8(%rax), %xmm1
	movq	-24(%rax), %rcx
	movsd	-16(%rax), %xmm0
	movups	%xmm5, -24(%rax)
.L708:
	movq	-8(%rdx), %r8
	unpcklpd	%xmm1, %xmm0
	movq	%r8, -8(%rax)
	movq	%rdi, -40(%rdx)
	movq	%rsi, -32(%rdx)
	movq	%rcx, -24(%rdx)
	movups	%xmm0, -16(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L700:
	movq	(%rdi), %rax
	movdqu	-40(%rcx), %xmm6
	movq	-40(%rax), %rdi
	movq	-32(%rax), %rsi
	movups	%xmm6, -40(%rax)
	movdqu	-24(%rcx), %xmm7
	movsd	-8(%rax), %xmm1
	movq	-24(%rax), %rdx
	movsd	-16(%rax), %xmm0
	movups	%xmm7, -24(%rax)
.L707:
	movq	-8(%rcx), %r8
	unpcklpd	%xmm1, %xmm0
	movq	%r8, -8(%rax)
	movq	%rdi, -40(%rcx)
	movq	%rsi, -32(%rcx)
	movq	%rdx, -24(%rcx)
	movups	%xmm0, -16(%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L705:
	movq	(%rdi), %rdx
	movdqu	-40(%rax), %xmm6
	movq	-40(%rdx), %rdi
	movq	-32(%rdx), %rsi
	movups	%xmm6, -40(%rdx)
	movdqu	-24(%rax), %xmm7
	movq	-24(%rdx), %rcx
	movsd	-16(%rdx), %xmm0
	movsd	-8(%rdx), %xmm1
	movups	%xmm7, -24(%rdx)
	jmp	.L706
	.cfi_endproc
.LFE22826:
	.size	_ZSt22__move_median_to_firstISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_SF_SF_T0_, .-_ZSt22__move_median_to_firstISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_SF_SF_T0_
	.section	.text._ZSt13__adjust_heapISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElS6_NS1_5__ops15_Iter_less_iterEEvT_T0_SG_T1_T2_,"axG",@progbits,_ZSt13__adjust_heapISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElS6_NS1_5__ops15_Iter_less_iterEEvT_T0_SG_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt13__adjust_heapISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElS6_NS1_5__ops15_Iter_less_iterEEvT_T0_SG_T1_T2_
	.type	_ZSt13__adjust_heapISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElS6_NS1_5__ops15_Iter_less_iterEEvT_T0_SG_T1_T2_, @function
_ZSt13__adjust_heapISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElS6_NS1_5__ops15_Iter_less_iterEEvT_T0_SG_T1_T2_:
.LFB22956:
	.cfi_startproc
	endbr64
	leaq	-1(%rdx), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rax, %r10
	shrq	$63, %r10
	addq	%rax, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	(%rdi), %rdi
	sarq	%r10
	cmpq	%r10, %rsi
	jge	.L710
	movq	%rsi, %r9
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L724:
	movq	-24(%rax), %rbx
	cmpq	-24(%r8), %rbx
	cmovbe	%r8, %rax
	cmovbe	%r11, %rcx
.L711:
	leaq	(%r9,%r9,4), %r8
	movdqu	-40(%rax), %xmm2
	movq	%rdi, %rbx
	salq	$3, %r8
	subq	%r8, %rbx
	movups	%xmm2, -40(%rbx)
	movdqu	-24(%rax), %xmm3
	movups	%xmm3, -24(%rbx)
	movq	-8(%rax), %r9
	movq	%r9, -8(%rbx)
	cmpq	%r10, %rcx
	jge	.L712
	movq	%rcx, %r9
.L713:
	leaq	1(%r9), %r8
	movq	%rdi, %rbx
	leaq	(%r8,%r8), %r11
	leaq	-1(%r11), %rcx
	leaq	(%r11,%r8,8), %r8
	leaq	(%rcx,%rcx,4), %rax
	salq	$3, %r8
	salq	$3, %rax
	subq	%rax, %rbx
	movq	%rbx, %rax
	movq	%rdi, %rbx
	subq	%r8, %rbx
	movq	%rbx, %r8
	movq	-32(%rbx), %rbx
	cmpq	%rbx, -32(%rax)
	jg	.L711
	jge	.L724
	movq	%r8, %rax
	movq	%r11, %rcx
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L710:
	leaq	(%rsi,%rsi,4), %rax
	movq	%rdi, %rcx
	salq	$3, %rax
	subq	%rax, %rcx
	movq	%rcx, %rax
	movq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L712:
	testb	$1, %dl
	jne	.L714
	subq	$2, %rdx
	movq	%rdx, %r8
	shrq	$63, %r8
	addq	%r8, %rdx
	sarq	%rdx
	cmpq	%rcx, %rdx
	je	.L725
.L714:
	leaq	-1(%rcx), %r8
	movq	16(%rbp), %r11
	movq	24(%rbp), %r9
	movq	%r8, %rdx
	movq	32(%rbp), %r10
	movsd	40(%rbp), %xmm0
	shrq	$63, %rdx
	movsd	48(%rbp), %xmm1
	addq	%r8, %rdx
	sarq	%rdx
	cmpq	%rsi, %rcx
	jle	.L715
.L718:
	leaq	(%rdx,%rdx,4), %rax
	movq	%rdi, %rbx
	salq	$3, %rax
	subq	%rax, %rbx
	movq	%rbx, %rax
	cmpq	%r9, -32(%rbx)
	jl	.L716
	jg	.L723
	cmpq	%r10, -24(%rbx)
	jnb	.L723
.L716:
	leaq	(%rcx,%rcx,4), %rcx
	movdqu	-40(%rax), %xmm4
	movq	%rdi, %rbx
	salq	$3, %rcx
	subq	%rcx, %rbx
	movups	%xmm4, -40(%rbx)
	movdqu	-24(%rax), %xmm5
	movups	%xmm5, -24(%rbx)
	movq	-8(%rax), %r8
	movq	%r8, -8(%rbx)
	leaq	-1(%rdx), %r8
	movq	%r8, %rcx
	shrq	$63, %rcx
	addq	%r8, %rcx
	sarq	%rcx
	movq	%rcx, %r8
	movq	%rdx, %rcx
	cmpq	%rdx, %rsi
	jge	.L715
	movq	%r8, %rdx
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L723:
	leaq	(%rcx,%rcx,4), %rax
	salq	$3, %rax
	subq	%rax, %rdi
	movq	%rdi, %rax
.L715:
	unpcklpd	%xmm1, %xmm0
	movq	%r11, -40(%rax)
	movq	%r9, -32(%rax)
	movq	%r10, -24(%rax)
	movups	%xmm0, -16(%rax)
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L725:
	.cfi_restore_state
	leaq	1(%rcx,%rcx), %rcx
	movq	%rdi, %rbx
	leaq	(%rcx,%rcx,4), %rdx
	salq	$3, %rdx
	subq	%rdx, %rbx
	movdqu	-40(%rbx), %xmm6
	movups	%xmm6, -40(%rax)
	movdqu	-24(%rbx), %xmm7
	movups	%xmm7, -24(%rax)
	movq	-8(%rbx), %r8
	movq	%r8, -8(%rax)
	movq	%rbx, %rax
	jmp	.L714
	.cfi_endproc
.LFE22956:
	.size	_ZSt13__adjust_heapISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElS6_NS1_5__ops15_Iter_less_iterEEvT_T0_SG_T1_T2_, .-_ZSt13__adjust_heapISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElS6_NS1_5__ops15_Iter_less_iterEEvT_T0_SG_T1_T2_
	.section	.text._ZSt13__heap_selectISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_SF_T0_,"axG",@progbits,_ZSt13__heap_selectISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_SF_T0_,comdat
	.p2align 4
	.weak	_ZSt13__heap_selectISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_SF_T0_
	.type	_ZSt13__heap_selectISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_SF_T0_, @function
_ZSt13__heap_selectISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_SF_T0_:
.LFB22823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rdi), %rdi
	movq	(%rsi), %r15
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rdi, %r12
	subq	%r15, %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -136(%rbp)
	cmpq	$40, %r12
	jle	.L727
	movabsq	$-3689348814741910323, %rax
	sarq	$3, %r12
	leaq	-104(%rbp), %r15
	imulq	%rax, %r12
	leaq	-2(%r12), %rax
	movq	%rax, %rcx
	shrq	$63, %rcx
	addq	%rax, %rcx
	sarq	%rcx
	leaq	1(%rcx), %rax
	movq	%rcx, %r13
	imulq	$-40, %rax, %rax
	leaq	(%rdi,%rax), %r14
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L728:
	subq	$1, %r13
.L729:
	movq	32(%r14), %rdx
	movdqu	(%r14), %xmm0
	subq	$8, %rsp
	movq	%r13, %rsi
	movdqu	16(%r14), %xmm1
	movq	-136(%rbp), %rax
	movq	%r15, %rdi
	addq	$40, %r14
	movq	%rdx, -64(%rbp)
	movq	%r12, %rdx
	pushq	-64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZSt13__adjust_heapISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElS6_NS1_5__ops15_Iter_less_iterEEvT_T0_SG_T1_T2_
	addq	$48, %rsp
	testq	%r13, %r13
	jne	.L728
	movq	-128(%rbp), %rax
	movq	(%rax), %r15
.L727:
	movabsq	$-3689348814741910323, %r12
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	cmpq	%rax, %r15
	jbe	.L726
	.p2align 4,,10
	.p2align 3
.L730:
	movq	(%rbx), %rdx
	movq	-32(%rdx), %rsi
	cmpq	%rsi, -32(%r15)
	jl	.L731
	jg	.L732
	movq	-24(%rdx), %rcx
	cmpq	%rcx, -24(%r15)
	jnb	.L732
.L731:
	movdqu	-40(%rdx), %xmm4
	movq	-128(%rbp), %rax
	movq	%rdx, -104(%rbp)
	xorl	%esi, %esi
	movq	-8(%r15), %rcx
	movdqu	-40(%r15), %xmm2
	subq	$8, %rsp
	leaq	-104(%rbp), %rdi
	movq	(%rax), %rax
	movups	%xmm4, -40(%r15)
	movdqu	-24(%rdx), %xmm5
	movdqu	-24(%r15), %xmm3
	movq	%rcx, -64(%rbp)
	movups	%xmm5, -24(%r15)
	movq	-8(%rdx), %rcx
	subq	%rax, %rdx
	sarq	$3, %rdx
	movaps	%xmm2, -96(%rbp)
	movq	%rcx, -8(%r15)
	imulq	%r12, %rdx
	pushq	-64(%rbp)
	movaps	%xmm3, -80(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZSt13__adjust_heapISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElS6_NS1_5__ops15_Iter_less_iterEEvT_T0_SG_T1_T2_
	movq	-120(%rbp), %rax
	addq	$48, %rsp
	movq	(%rax), %rax
.L732:
	subq	$40, %r15
	cmpq	%rax, %r15
	ja	.L730
.L726:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L738
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L738:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22823:
	.size	_ZSt13__heap_selectISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_SF_T0_, .-_ZSt13__heap_selectISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_SF_T0_
	.section	.text._ZSt16__introsort_loopISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElNS1_5__ops15_Iter_less_iterEEvT_SF_T0_T1_,"axG",@progbits,_ZSt16__introsort_loopISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElNS1_5__ops15_Iter_less_iterEEvT_SF_T0_T1_,comdat
	.p2align 4
	.weak	_ZSt16__introsort_loopISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElNS1_5__ops15_Iter_less_iterEEvT_SF_T0_T1_
	.type	_ZSt16__introsort_loopISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElNS1_5__ops15_Iter_less_iterEEvT_SF_T0_T1_, @function
_ZSt16__introsort_loopISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElNS1_5__ops15_Iter_less_iterEEvT_SF_T0_T1_:
.LFB22431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r8
	movq	(%rsi), %r15
	movq	%rdi, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r8, %rax
	subq	%r15, %rax
	cmpq	$640, %rax
	jle	.L739
	leaq	-112(%rbp), %rax
	movq	%rsi, %r12
	leaq	-104(%rbp), %rbx
	movq	%rax, -168(%rbp)
	leaq	-96(%rbp), %r13
	testq	%rdx, %rdx
	je	.L744
	leaq	-120(%rbp), %rax
	movq	%rsi, -176(%rbp)
	movq	%rdx, %r12
	movq	%r8, %r14
	movq	%rax, -184(%rbp)
.L741:
	leaq	40(%r15), %rax
	movq	%r14, %rdi
	movq	%r13, %rcx
	movq	%r14, -120(%rbp)
	movq	%rax, -96(%rbp)
	movq	%r14, %rax
	movq	-168(%rbp), %rsi
	subq	$1, %r12
	movabsq	$-3689348814741910323, %rdx
	subq	%r15, %rax
	sarq	$3, %rax
	imulq	%rax, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	movq	%rbx, %rdx
	sarq	%rax
	leaq	(%rax,%rax,4), %rax
	salq	$3, %rax
	subq	%rax, %rdi
	leaq	-40(%r14), %rax
	movq	%rdi, -104(%rbp)
	movq	-184(%rbp), %rdi
	movq	%rax, -112(%rbp)
	movq	%rax, -152(%rbp)
	call	_ZSt22__move_median_to_firstISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_SF_SF_T0_
	movq	-152(%rbp), %rax
	movq	-32(%r14), %rsi
	leaq	-80(%r14), %rdx
	movq	%rax, %rcx
	movq	%r15, %rax
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L764:
	movq	-24(%r14), %r10
	cmpq	%r10, 16(%rdx)
	jnb	.L748
.L747:
	subq	$40, %rdx
	subq	$40, %rcx
.L746:
	movq	8(%rdx), %rdi
	movq	%rcx, %r15
	cmpq	%rdi, %rsi
	jg	.L747
	jge	.L764
	.p2align 4,,10
	.p2align 3
.L748:
	addq	$40, %rax
	cmpq	-32(%rax), %rsi
	jl	.L748
	jg	.L750
	movq	-24(%rax), %r11
	cmpq	%r11, -24(%r14)
	jb	.L748
	.p2align 4,,10
	.p2align 3
.L750:
	cmpq	%rax, %rcx
	jbe	.L765
	movdqu	-40(%rax), %xmm2
	movq	(%rdx), %r8
	movsd	24(%rdx), %xmm0
	movsd	32(%rdx), %xmm1
	movups	%xmm2, (%rdx)
	movdqu	-24(%rax), %xmm3
	movq	16(%rdx), %rsi
	unpcklpd	%xmm1, %xmm0
	movups	%xmm3, 16(%rdx)
	movq	-8(%rax), %r9
	movq	%r9, 32(%rdx)
	movq	%r8, -40(%rax)
	movq	%rdi, -32(%rax)
	movq	%rsi, -24(%rax)
	movups	%xmm0, -16(%rax)
	movq	-32(%r14), %rsi
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L765:
	movq	-176(%rbp), %r14
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rcx, -104(%rbp)
	movq	(%r14), %rax
	movq	%rcx, -152(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZSt16__introsort_loopISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElNS1_5__ops15_Iter_less_iterEEvT_SF_T0_T1_
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %rax
	movq	%rcx, (%r14)
	movq	(%rax), %r14
	movq	%r14, %rax
	subq	%rcx, %rax
	cmpq	$640, %rax
	jle	.L739
	testq	%r12, %r12
	jne	.L741
	movq	-176(%rbp), %r12
.L744:
	movq	-160(%rbp), %rsi
	movq	(%r12), %rax
	movq	%r13, %rdi
	movabsq	$-3689348814741910323, %r14
	movq	(%rsi), %rdx
	movq	%rbx, %rsi
	movq	%rax, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rdx, -96(%rbp)
	movq	-168(%rbp), %rdx
	movq	%rax, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZSt13__heap_selectISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_SF_T0_
	movq	-136(%rbp), %r13
	movq	-128(%rbp), %r12
	movq	%r13, %rax
	subq	%r12, %rax
	cmpq	$40, %rax
	jle	.L739
	.p2align 4,,10
	.p2align 3
.L742:
	movdqu	-40(%r13), %xmm6
	movq	32(%r12), %rax
	movq	%r13, %r15
	xorl	%esi, %esi
	addq	$40, %r12
	subq	$8, %rsp
	movq	%rbx, %rdi
	movq	%r13, -104(%rbp)
	movdqu	-40(%r12), %xmm4
	movups	%xmm6, -40(%r12)
	movdqu	-24(%r13), %xmm7
	subq	%r12, %r15
	movdqu	-24(%r12), %xmm5
	movq	%rax, -64(%rbp)
	movq	%r15, %rdx
	movups	%xmm7, -24(%r12)
	movq	-8(%r13), %rax
	sarq	$3, %rdx
	movaps	%xmm4, -96(%rbp)
	imulq	%r14, %rdx
	movq	%rax, -8(%r12)
	movaps	%xmm5, -80(%rbp)
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	pushq	-88(%rbp)
	pushq	-96(%rbp)
	call	_ZSt13__adjust_heapISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElS6_NS1_5__ops15_Iter_less_iterEEvT_T0_SG_T1_T2_
	addq	$48, %rsp
	cmpq	$40, %r15
	jg	.L742
.L739:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L766
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L766:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22431:
	.size	_ZSt16__introsort_loopISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElNS1_5__ops15_Iter_less_iterEEvT_SF_T0_T1_, .-_ZSt16__introsort_loopISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElNS1_5__ops15_Iter_less_iterEEvT_SF_T0_T1_
	.section	.rodata._ZN2v88internal16RuntimeCallStats5PrintERSo.str1.1,"aMS",@progbits,1
.LC44:
	.string	"Runtime Function/C++ Builtin"
.LC45:
	.string	"Time"
.LC46:
	.string	"Count"
.LC47:
	.string	"Total"
	.section	.text._ZN2v88internal16RuntimeCallStats5PrintERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RuntimeCallStats5PrintERSo
	.type	_ZN2v88internal16RuntimeCallStats5PrintERSo, @function
_ZN2v88internal16RuntimeCallStats5PrintERSo:
.LFB18720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -208(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -192(%rbp)
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L819
.L768:
	leaq	24(%r12), %rbx
	leaq	-224(%rbp), %r13
	addq	$27648, %r12
	.p2align 4,,10
	.p2align 3
.L770:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	addq	$24, %rbx
	call	_ZN2v88internal22RuntimeCallStatEntries3AddEPNS0_18RuntimeCallCounterE
	cmpq	%rbx, %r12
	jne	.L770
	cmpq	$0, -224(%rbp)
	movq	-208(%rbp), %r12
	jne	.L820
.L771:
	testq	%r12, %r12
	je	.L767
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L767:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L821
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L820:
	.cfi_restore_state
	movq	-200(%rbp), %r13
	cmpq	%r12, %r13
	je	.L772
	movq	%r13, %rbx
	leaq	-176(%rbp), %r15
	movabsq	$-3689348814741910323, %rdx
	movq	%r12, -232(%rbp)
	subq	%r12, %rbx
	movq	%r15, %rdi
	movq	%r13, -176(%rbp)
	movq	%rbx, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	movl	$63, %edx
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	leaq	-232(%rbp), %rax
	movslq	%edx, %rdx
	movq	%rax, %rsi
	movq	%rax, -248(%rbp)
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEElNS1_5__ops15_Iter_less_iterEEvT_SF_T0_T1_
	cmpq	$640, %rbx
	movq	-248(%rbp), %rax
	jle	.L773
	leaq	-640(%r13), %rbx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%r13, -232(%rbp)
	movq	%rbx, -176(%rbp)
	call	_ZSt16__insertion_sortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_T0_
	cmpq	%r12, %rbx
	je	.L772
	.p2align 4,,10
	.p2align 3
.L774:
	movq	%rbx, %rax
	movq	-40(%rbx), %rdi
	movq	-32(%rbx), %rcx
	movq	-24(%rbx), %rsi
	movsd	-16(%rbx), %xmm0
	movsd	-8(%rbx), %xmm1
	cmpq	8(%rax), %rcx
	jl	.L776
	jg	.L777
.L822:
	cmpq	16(%rax), %rsi
	jnb	.L777
.L776:
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm3
	addq	$40, %rax
	movq	-8(%rax), %rdx
	movups	%xmm2, -80(%rax)
	movups	%xmm3, -64(%rax)
	movq	%rdx, -48(%rax)
	cmpq	8(%rax), %rcx
	jl	.L776
	jle	.L822
.L777:
	unpcklpd	%xmm1, %xmm0
	subq	$40, %rbx
	movq	%rdi, -40(%rax)
	movq	%rcx, -32(%rax)
	movq	%rsi, -24(%rax)
	movups	%xmm0, -16(%rax)
	cmpq	%r12, %rbx
	jne	.L774
.L772:
	movq	(%r14), %rax
	movl	$28, %edx
	leaq	.LC44(%rip), %rsi
	movq	%r14, %rdi
	movq	-24(%rax), %rax
	movq	$50, 16(%r14,%rax)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rax
	movl	$4, %edx
	movq	%r14, %rdi
	leaq	.LC45(%rip), %rsi
	movq	-24(%rax), %rax
	movq	$12, 16(%r14,%rax)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rax
	movl	$5, %edx
	movq	%r14, %rdi
	leaq	.LC46(%rip), %rsi
	movq	-24(%rax), %rax
	movq	$18, 16(%r14,%rax)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %r12
	testq	%r12, %r12
	je	.L783
	cmpb	$0, 56(%r12)
	je	.L781
	movsbl	67(%r12), %esi
.L782:
	movq	%r14, %rdi
	leaq	-112(%rbp), %rbx
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	-128(%rbp), %rdi
	movl	$61, %edx
	movl	$88, %esi
	movq	%rax, %r12
	movq	%rbx, -128(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r13
	testq	%r13, %r13
	je	.L783
	cmpb	$0, 56(%r13)
	je	.L784
	movsbl	67(%r13), %esi
.L785:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L786
	call	_ZdlPv@PLT
.L786:
	movq	-200(%rbp), %r12
	movq	-208(%rbp), %rbx
	cmpq	%r12, %rbx
	je	.L790
	.p2align 4,,10
	.p2align 3
.L789:
	movq	-224(%rbp), %rdx
	movq	-216(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal22RuntimeCallStatEntries5Entry8SetTotalENS_4base9TimeDeltaEm
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$40, %rbx
	call	_ZN2v88internal22RuntimeCallStatEntries5Entry5PrintERSo
	cmpq	%rbx, %r12
	jne	.L789
.L790:
	leaq	-96(%rbp), %rdi
	movl	$45, %edx
	movl	$88, %esi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r13
	testq	%r13, %r13
	je	.L783
	cmpb	$0, 56(%r13)
	je	.L791
	movsbl	67(%r13), %esi
.L792:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L793
	call	_ZdlPv@PLT
.L793:
	movq	-216(%rbp), %rax
	leaq	-232(%rbp), %rdi
	movq	-224(%rbp), %rbx
	movq	%rax, -232(%rbp)
	leaq	.LC47(%rip), %rax
	movq	%rax, -176(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movapd	.LC43(%rip), %xmm0
	movq	%r14, %rsi
	leaq	-176(%rbp), %rdi
	movq	%rax, -168(%rbp)
	movq	%rbx, -160(%rbp)
	movups	%xmm0, -152(%rbp)
	call	_ZN2v88internal22RuntimeCallStatEntries5Entry5PrintERSo
	movq	-208(%rbp), %r12
	jmp	.L771
.L819:
	movq	(%rdi), %rbx
	movq	%rbx, -248(%rbp)
	movq	%rbx, %r15
	call	*_ZN2v88internal16RuntimeCallTimer3NowE(%rip)
	movq	%rax, -256(%rbp)
	subq	16(%rbx), %rax
	movq	$0, 16(%rbx)
	addq	%rax, 24(%rbx)
	leaq	-176(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L769:
	movq	(%r15), %r13
	movq	24(%r15), %rax
	movq	%rbx, %rdi
	movq	%rax, -176(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	addq	%rax, 16(%r13)
	movq	$0, 24(%r15)
	movq	8(%r15), %rdx
	movq	%rdx, %r15
	testq	%rdx, %rdx
	jne	.L769
	movq	-248(%rbp), %rax
	movq	-256(%rbp), %rcx
	movq	%rcx, 16(%rax)
	jmp	.L768
.L791:
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L792
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L792
.L784:
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L785
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L785
.L781:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L782
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L782
.L773:
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r12, -232(%rbp)
	movq	%r13, -176(%rbp)
	call	_ZSt16__insertion_sortISt16reverse_iteratorIN9__gnu_cxx17__normal_iteratorIPN2v88internal22RuntimeCallStatEntries5EntryESt6vectorIS6_SaIS6_EEEEENS1_5__ops15_Iter_less_iterEEvT_SF_T0_
	jmp	.L772
.L821:
	call	__stack_chk_fail@PLT
.L783:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE18720:
	.size	_ZN2v88internal16RuntimeCallStats5PrintERSo, .-_ZN2v88internal16RuntimeCallStats5PrintERSo
	.section	.text._ZN2v88internal16RuntimeCallStats5PrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RuntimeCallStats5PrintEv
	.type	_ZN2v88internal16RuntimeCallStats5PrintEv, @function
_ZN2v88internal16RuntimeCallStats5PrintEv:
.LFB18719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-304(%rbp), %r13
	leaq	-384(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	subq	$352, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	movw	%ax, -80(%rbp)
	movq	%rbx, -304(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -384(%rbp)
	addq	$40, %rax
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5PrintERSo
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -304(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-320(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -304(%rbp)
	movq	%rax, -384(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L826
	addq	$352, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L826:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18719:
	.size	_ZN2v88internal16RuntimeCallStats5PrintEv, .-_ZN2v88internal16RuntimeCallStats5PrintEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal12TracingFlags13runtime_statsE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal12TracingFlags13runtime_statsE, @function
_GLOBAL__sub_I__ZN2v88internal12TracingFlags13runtime_statsE:
.LFB23068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23068:
	.size	_GLOBAL__sub_I__ZN2v88internal12TracingFlags13runtime_statsE, .-_GLOBAL__sub_I__ZN2v88internal12TracingFlags13runtime_statsE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal12TracingFlags13runtime_statsE
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.bss._ZZN2v88internal33WorkerThreadRuntimeCallStatsScopeD4EvE28trace_event_unique_atomic597,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal33WorkerThreadRuntimeCallStatsScopeD4EvE28trace_event_unique_atomic597, @object
	.size	_ZZN2v88internal33WorkerThreadRuntimeCallStatsScopeD4EvE28trace_event_unique_atomic597, 8
_ZZN2v88internal33WorkerThreadRuntimeCallStatsScopeD4EvE28trace_event_unique_atomic597:
	.zero	8
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC48:
	.string	"GC_MC_INCREMENTAL"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC49:
	.string	"GC_MC_INCREMENTAL_EMBEDDER_PROLOGUE"
	.align 8
.LC50:
	.string	"GC_MC_INCREMENTAL_EMBEDDER_TRACING"
	.align 8
.LC51:
	.string	"GC_MC_INCREMENTAL_EXTERNAL_EPILOGUE"
	.align 8
.LC52:
	.string	"GC_MC_INCREMENTAL_EXTERNAL_PROLOGUE"
	.section	.rodata.str1.1
.LC53:
	.string	"GC_MC_INCREMENTAL_FINALIZE"
	.section	.rodata.str1.8
	.align 8
.LC54:
	.string	"GC_MC_INCREMENTAL_FINALIZE_BODY"
	.align 8
.LC55:
	.string	"GC_MC_INCREMENTAL_LAYOUT_CHANGE"
	.section	.rodata.str1.1
.LC56:
	.string	"GC_MC_INCREMENTAL_START"
.LC57:
	.string	"GC_MC_INCREMENTAL_SWEEPING"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"GC_HEAP_EMBEDDER_TRACING_EPILOGUE"
	.section	.rodata.str1.1
.LC59:
	.string	"GC_HEAP_EPILOGUE"
	.section	.rodata.str1.8
	.align 8
.LC60:
	.string	"GC_HEAP_EPILOGUE_REDUCE_NEW_SPACE"
	.section	.rodata.str1.1
.LC61:
	.string	"GC_HEAP_EXTERNAL_EPILOGUE"
.LC62:
	.string	"GC_HEAP_EXTERNAL_PROLOGUE"
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"GC_HEAP_EXTERNAL_WEAK_GLOBAL_HANDLES"
	.section	.rodata.str1.1
.LC64:
	.string	"GC_HEAP_PROLOGUE"
.LC65:
	.string	"GC_MC_CLEAR"
.LC66:
	.string	"GC_MC_EPILOGUE"
.LC67:
	.string	"GC_MC_EVACUATE"
.LC68:
	.string	"GC_MC_FINISH"
.LC69:
	.string	"GC_MC_MARK"
.LC70:
	.string	"GC_MC_PROLOGUE"
.LC71:
	.string	"GC_MC_SWEEP"
.LC72:
	.string	"GC_MC_CLEAR_DEPENDENT_CODE"
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"GC_MC_CLEAR_FLUSHABLE_BYTECODE"
	.align 8
.LC74:
	.string	"GC_MC_CLEAR_FLUSHED_JS_FUNCTIONS"
	.section	.rodata.str1.1
.LC75:
	.string	"GC_MC_CLEAR_MAPS"
.LC76:
	.string	"GC_MC_CLEAR_SLOTS_BUFFER"
.LC77:
	.string	"GC_MC_CLEAR_STORE_BUFFER"
.LC78:
	.string	"GC_MC_CLEAR_STRING_TABLE"
.LC79:
	.string	"GC_MC_CLEAR_WEAK_COLLECTIONS"
.LC80:
	.string	"GC_MC_CLEAR_WEAK_LISTS"
.LC81:
	.string	"GC_MC_CLEAR_WEAK_REFERENCES"
.LC82:
	.string	"GC_MC_EVACUATE_CANDIDATES"
.LC83:
	.string	"GC_MC_EVACUATE_CLEAN_UP"
.LC84:
	.string	"GC_MC_EVACUATE_COPY"
.LC85:
	.string	"GC_MC_EVACUATE_COPY_PARALLEL"
.LC86:
	.string	"GC_MC_EVACUATE_EPILOGUE"
.LC87:
	.string	"GC_MC_EVACUATE_PROLOGUE"
.LC88:
	.string	"GC_MC_EVACUATE_REBALANCE"
	.section	.rodata.str1.8
	.align 8
.LC89:
	.string	"GC_MC_EVACUATE_UPDATE_POINTERS"
	.align 8
.LC90:
	.string	"GC_MC_EVACUATE_UPDATE_POINTERS_PARALLEL"
	.align 8
.LC91:
	.string	"GC_MC_EVACUATE_UPDATE_POINTERS_SLOTS_MAIN"
	.align 8
.LC92:
	.string	"GC_MC_EVACUATE_UPDATE_POINTERS_SLOTS_MAP_SPACE"
	.align 8
.LC93:
	.string	"GC_MC_EVACUATE_UPDATE_POINTERS_TO_NEW_ROOTS"
	.align 8
.LC94:
	.string	"GC_MC_EVACUATE_UPDATE_POINTERS_WEAK"
	.section	.rodata.str1.1
.LC95:
	.string	"GC_MC_MARK_EMBEDDER_PROLOGUE"
.LC96:
	.string	"GC_MC_MARK_EMBEDDER_TRACING"
	.section	.rodata.str1.8
	.align 8
.LC97:
	.string	"GC_MC_MARK_EMBEDDER_TRACING_CLOSURE"
	.section	.rodata.str1.1
.LC98:
	.string	"GC_MC_MARK_FINISH_INCREMENTAL"
.LC99:
	.string	"GC_MC_MARK_MAIN"
.LC100:
	.string	"GC_MC_MARK_ROOTS"
.LC101:
	.string	"GC_MC_MARK_WEAK_CLOSURE"
	.section	.rodata.str1.8
	.align 8
.LC102:
	.string	"GC_MC_MARK_WEAK_CLOSURE_EPHEMERON"
	.align 8
.LC103:
	.string	"GC_MC_MARK_WEAK_CLOSURE_EPHEMERON_MARKING"
	.align 8
.LC104:
	.string	"GC_MC_MARK_WEAK_CLOSURE_EPHEMERON_LINEAR"
	.align 8
.LC105:
	.string	"GC_MC_MARK_WEAK_CLOSURE_WEAK_HANDLES"
	.align 8
.LC106:
	.string	"GC_MC_MARK_WEAK_CLOSURE_WEAK_ROOTS"
	.align 8
.LC107:
	.string	"GC_MC_MARK_WEAK_CLOSURE_HARMONY"
	.section	.rodata.str1.1
.LC108:
	.string	"GC_MC_SWEEP_CODE"
.LC109:
	.string	"GC_MC_SWEEP_MAP"
.LC110:
	.string	"GC_MC_SWEEP_OLD"
.LC111:
	.string	"GC_MINOR_MC"
.LC112:
	.string	"GC_MINOR_MC_CLEAR"
	.section	.rodata.str1.8
	.align 8
.LC113:
	.string	"GC_MINOR_MC_CLEAR_STRING_TABLE"
	.section	.rodata.str1.1
.LC114:
	.string	"GC_MINOR_MC_CLEAR_WEAK_LISTS"
.LC115:
	.string	"GC_MINOR_MC_EVACUATE"
.LC116:
	.string	"GC_MINOR_MC_EVACUATE_CLEAN_UP"
.LC117:
	.string	"GC_MINOR_MC_EVACUATE_COPY"
	.section	.rodata.str1.8
	.align 8
.LC118:
	.string	"GC_MINOR_MC_EVACUATE_COPY_PARALLEL"
	.section	.rodata.str1.1
.LC119:
	.string	"GC_MINOR_MC_EVACUATE_EPILOGUE"
.LC120:
	.string	"GC_MINOR_MC_EVACUATE_PROLOGUE"
	.section	.rodata.str1.8
	.align 8
.LC121:
	.string	"GC_MINOR_MC_EVACUATE_REBALANCE"
	.align 8
.LC122:
	.string	"GC_MINOR_MC_EVACUATE_UPDATE_POINTERS"
	.align 8
.LC123:
	.string	"GC_MINOR_MC_EVACUATE_UPDATE_POINTERS_PARALLEL"
	.align 8
.LC124:
	.string	"GC_MINOR_MC_EVACUATE_UPDATE_POINTERS_SLOTS"
	.align 8
.LC125:
	.string	"GC_MINOR_MC_EVACUATE_UPDATE_POINTERS_TO_NEW_ROOTS"
	.align 8
.LC126:
	.string	"GC_MINOR_MC_EVACUATE_UPDATE_POINTERS_WEAK"
	.section	.rodata.str1.1
.LC127:
	.string	"GC_MINOR_MC_MARK"
	.section	.rodata.str1.8
	.align 8
.LC128:
	.string	"GC_MINOR_MC_MARK_GLOBAL_HANDLES"
	.section	.rodata.str1.1
.LC129:
	.string	"GC_MINOR_MC_MARK_PARALLEL"
.LC130:
	.string	"GC_MINOR_MC_MARK_SEED"
.LC131:
	.string	"GC_MINOR_MC_MARK_ROOTS"
.LC132:
	.string	"GC_MINOR_MC_MARK_WEAK"
.LC133:
	.string	"GC_MINOR_MC_MARKING_DEQUE"
.LC134:
	.string	"GC_MINOR_MC_RESET_LIVENESS"
.LC135:
	.string	"GC_MINOR_MC_SWEEPING"
.LC136:
	.string	"GC_SCAVENGER_FAST_PROMOTE"
.LC137:
	.string	"GC_SCAVENGER_SCAVENGE"
	.section	.rodata.str1.8
	.align 8
.LC138:
	.string	"GC_SCAVENGER_PROCESS_ARRAY_BUFFERS"
	.align 8
.LC139:
	.string	"GC_SCAVENGER_SCAVENGE_WEAK_GLOBAL_HANDLES_IDENTIFY"
	.align 8
.LC140:
	.string	"GC_SCAVENGER_SCAVENGE_WEAK_GLOBAL_HANDLES_PROCESS"
	.align 8
.LC141:
	.string	"GC_SCAVENGER_SCAVENGE_PARALLEL"
	.section	.rodata.str1.1
.LC142:
	.string	"GC_SCAVENGER_SCAVENGE_ROOTS"
	.section	.rodata.str1.8
	.align 8
.LC143:
	.string	"GC_SCAVENGER_SCAVENGE_UPDATE_REFS"
	.section	.rodata.str1.1
.LC144:
	.string	"GC_SCAVENGER_SCAVENGE_WEAK"
	.section	.rodata.str1.8
	.align 8
.LC145:
	.string	"GC_SCAVENGER_SCAVENGE_FINALIZE"
	.align 8
.LC146:
	.string	"GC_BACKGROUND_ARRAY_BUFFER_FREE"
	.section	.rodata.str1.1
.LC147:
	.string	"GC_BACKGROUND_STORE_BUFFER"
.LC148:
	.string	"GC_BACKGROUND_UNMAPPER"
	.section	.rodata.str1.8
	.align 8
.LC149:
	.string	"GC_MC_BACKGROUND_EVACUATE_COPY"
	.align 8
.LC150:
	.string	"GC_MC_BACKGROUND_EVACUATE_UPDATE_POINTERS"
	.section	.rodata.str1.1
.LC151:
	.string	"GC_MC_BACKGROUND_MARKING"
.LC152:
	.string	"GC_MC_BACKGROUND_SWEEPING"
	.section	.rodata.str1.8
	.align 8
.LC153:
	.string	"GC_MINOR_MC_BACKGROUND_EVACUATE_COPY"
	.align 8
.LC154:
	.string	"GC_MINOR_MC_BACKGROUND_EVACUATE_UPDATE_POINTERS"
	.align 8
.LC155:
	.string	"GC_MINOR_MC_BACKGROUND_MARKING"
	.align 8
.LC156:
	.string	"GC_SCAVENGER_BACKGROUND_SCAVENGE_PARALLEL"
	.section	.rodata.str1.1
.LC157:
	.string	"AccessorGetterCallback"
.LC158:
	.string	"AccessorSetterCallback"
.LC159:
	.string	"ArrayLengthGetter"
.LC160:
	.string	"ArrayLengthSetter"
.LC161:
	.string	"BoundFunctionLengthGetter"
.LC162:
	.string	"BoundFunctionNameGetter"
	.section	.rodata.str1.8
	.align 8
.LC163:
	.string	"CodeGenerationFromStringsCallbacks"
	.section	.rodata.str1.1
.LC164:
	.string	"CompileAnalyse"
.LC165:
	.string	"CompileBackgroundAnalyse"
.LC166:
	.string	"CompileBackgroundCompileTask"
.LC167:
	.string	"CompileBackgroundEval"
.LC168:
	.string	"CompileBackgroundFunction"
.LC169:
	.string	"CompileBackgroundIgnition"
	.section	.rodata.str1.8
	.align 8
.LC170:
	.string	"CompileBackgroundRewriteReturnResult"
	.align 8
.LC171:
	.string	"CompileBackgroundScopeAnalysis"
	.section	.rodata.str1.1
.LC172:
	.string	"CompileBackgroundScript"
.LC173:
	.string	"CompileCollectSourcePositions"
.LC174:
	.string	"CompileDeserialize"
.LC175:
	.string	"CompileEnqueueOnDispatcher"
.LC176:
	.string	"CompileEval"
	.section	.rodata.str1.8
	.align 8
.LC177:
	.string	"CompileFinalizeBackgroundCompileTask"
	.section	.rodata.str1.1
.LC178:
	.string	"CompileFinishNowOnDispatcher"
.LC179:
	.string	"CompileFunction"
	.section	.rodata.str1.8
	.align 8
.LC180:
	.string	"CompileGetFromOptimizedCodeMap"
	.section	.rodata.str1.1
.LC181:
	.string	"CompileIgnition"
.LC182:
	.string	"CompileIgnitionFinalization"
.LC183:
	.string	"CompileRewriteReturnResult"
.LC184:
	.string	"CompileScopeAnalysis"
.LC185:
	.string	"CompileScript"
.LC186:
	.string	"CompileSerialize"
.LC187:
	.string	"CompileWaitForDispatcher"
.LC188:
	.string	"DeoptimizeCode"
.LC189:
	.string	"DeserializeContext"
.LC190:
	.string	"DeserializeIsolate"
.LC191:
	.string	"FunctionCallback"
.LC192:
	.string	"FunctionLengthGetter"
.LC193:
	.string	"FunctionPrototypeGetter"
.LC194:
	.string	"FunctionPrototypeSetter"
.LC195:
	.string	"GC_Custom_AllAvailableGarbage"
	.section	.rodata.str1.8
	.align 8
.LC196:
	.string	"GC_Custom_IncrementalMarkingObserver"
	.section	.rodata.str1.1
.LC197:
	.string	"GC_Custom_SlowAllocateRaw"
.LC198:
	.string	"GCEpilogueCallback"
.LC199:
	.string	"GCPrologueCallback"
.LC200:
	.string	"Genesis"
.LC201:
	.string	"GetMoreDataCallback"
.LC202:
	.string	"IndexedDefinerCallback"
.LC203:
	.string	"IndexedDeleterCallback"
.LC204:
	.string	"IndexedDescriptorCallback"
.LC205:
	.string	"IndexedEnumeratorCallback"
.LC206:
	.string	"IndexedGetterCallback"
.LC207:
	.string	"IndexedQueryCallback"
.LC208:
	.string	"IndexedSetterCallback"
.LC209:
	.string	"Invoke"
.LC210:
	.string	"InvokeApiFunction"
.LC211:
	.string	"InvokeApiInterruptCallbacks"
.LC212:
	.string	"JS_Execution"
.LC213:
	.string	"Map_SetPrototype"
	.section	.rodata.str1.8
	.align 8
.LC214:
	.string	"Map_TransitionToAccessorProperty"
	.section	.rodata.str1.1
.LC215:
	.string	"Map_TransitionToDataProperty"
.LC216:
	.string	"MessageListenerCallback"
.LC217:
	.string	"NamedDefinerCallback"
.LC218:
	.string	"NamedDeleterCallback"
.LC219:
	.string	"NamedDescriptorCallback"
.LC220:
	.string	"NamedEnumeratorCallback"
.LC221:
	.string	"NamedGetterCallback"
.LC222:
	.string	"NamedQueryCallback"
.LC223:
	.string	"NamedSetterCallback"
.LC224:
	.string	"Object_DeleteProperty"
.LC225:
	.string	"ObjectVerify"
.LC226:
	.string	"OptimizeCode"
.LC227:
	.string	"ParseArrowFunctionLiteral"
	.section	.rodata.str1.8
	.align 8
.LC228:
	.string	"ParseBackgroundArrowFunctionLiteral"
	.align 8
.LC229:
	.string	"ParseBackgroundFunctionLiteral"
	.section	.rodata.str1.1
.LC230:
	.string	"ParseBackgroundProgram"
.LC231:
	.string	"ParseEval"
.LC232:
	.string	"ParseFunction"
.LC233:
	.string	"ParseFunctionLiteral"
.LC234:
	.string	"ParseProgram"
.LC235:
	.string	"PreParseArrowFunctionLiteral"
	.section	.rodata.str1.8
	.align 8
.LC236:
	.string	"PreParseBackgroundArrowFunctionLiteral"
	.align 8
.LC237:
	.string	"PreParseBackgroundWithVariableResolution"
	.align 8
.LC238:
	.string	"PreParseWithVariableResolution"
	.section	.rodata.str1.1
.LC239:
	.string	"PropertyCallback"
	.section	.rodata.str1.8
	.align 8
.LC240:
	.string	"PrototypeMap_TransitionToAccessorProperty"
	.align 8
.LC241:
	.string	"PrototypeMap_TransitionToDataProperty"
	.align 8
.LC242:
	.string	"PrototypeObject_DeleteProperty"
	.section	.rodata.str1.1
.LC243:
	.string	"RecompileConcurrent"
.LC244:
	.string	"RecompileSynchronous"
.LC245:
	.string	"ReconfigureToDataProperty"
.LC246:
	.string	"StringLengthGetter"
.LC247:
	.string	"TestCounter1"
.LC248:
	.string	"TestCounter2"
.LC249:
	.string	"TestCounter3"
.LC250:
	.string	"DebugBreakOnBytecode"
.LC251:
	.string	"LoadLookupSlotForCall"
.LC252:
	.string	"ArrayIncludes_Slow"
.LC253:
	.string	"ArrayIndexOf"
.LC254:
	.string	"ArrayIsArray"
.LC255:
	.string	"ArraySpeciesConstructor"
.LC256:
	.string	"GrowArrayElements"
.LC257:
	.string	"IsArray"
.LC258:
	.string	"NewArray"
.LC259:
	.string	"NormalizeElements"
.LC260:
	.string	"TransitionElementsKind"
	.section	.rodata.str1.8
	.align 8
.LC261:
	.string	"TransitionElementsKindWithKind"
	.section	.rodata.str1.1
.LC262:
	.string	"AtomicsLoad64"
.LC263:
	.string	"AtomicsStore64"
.LC264:
	.string	"AtomicsAdd"
.LC265:
	.string	"AtomicsAnd"
.LC266:
	.string	"AtomicsCompareExchange"
.LC267:
	.string	"AtomicsExchange"
.LC268:
	.string	"AtomicsNumWaitersForTesting"
.LC269:
	.string	"AtomicsOr"
.LC270:
	.string	"AtomicsSub"
.LC271:
	.string	"AtomicsXor"
.LC272:
	.string	"SetAllowAtomicsWait"
.LC273:
	.string	"BigIntBinaryOp"
.LC274:
	.string	"BigIntCompareToBigInt"
.LC275:
	.string	"BigIntCompareToNumber"
.LC276:
	.string	"BigIntCompareToString"
.LC277:
	.string	"BigIntEqualToBigInt"
.LC278:
	.string	"BigIntEqualToNumber"
.LC279:
	.string	"BigIntEqualToString"
.LC280:
	.string	"BigIntToBoolean"
.LC281:
	.string	"BigIntToNumber"
.LC282:
	.string	"BigIntUnaryOp"
.LC283:
	.string	"ToBigInt"
.LC284:
	.string	"DefineClass"
.LC285:
	.string	"HomeObjectSymbol"
.LC286:
	.string	"LoadFromSuper"
.LC287:
	.string	"LoadKeyedFromSuper"
.LC288:
	.string	"StoreKeyedToSuper"
.LC289:
	.string	"StoreToSuper"
	.section	.rodata.str1.8
	.align 8
.LC290:
	.string	"ThrowConstructorNonCallableError"
	.section	.rodata.str1.1
.LC291:
	.string	"ThrowNotSuperConstructor"
.LC292:
	.string	"ThrowStaticPrototypeError"
.LC293:
	.string	"ThrowSuperAlreadyCalledError"
.LC294:
	.string	"ThrowSuperNotCalled"
.LC295:
	.string	"ThrowUnsupportedSuperError"
.LC296:
	.string	"MapGrow"
.LC297:
	.string	"MapShrink"
.LC298:
	.string	"SetGrow"
.LC299:
	.string	"SetShrink"
.LC300:
	.string	"TheHole"
.LC301:
	.string	"WeakCollectionDelete"
.LC302:
	.string	"WeakCollectionSet"
.LC303:
	.string	"CompileForOnStackReplacement"
.LC304:
	.string	"CompileLazy"
.LC305:
	.string	"CompileOptimized_Concurrent"
	.section	.rodata.str1.8
	.align 8
.LC306:
	.string	"CompileOptimized_NotConcurrent"
	.section	.rodata.str1.1
.LC307:
	.string	"EvictOptimizedCodeSlot"
.LC308:
	.string	"FunctionFirstExecution"
.LC309:
	.string	"InstantiateAsmJs"
.LC310:
	.string	"NotifyDeoptimized"
.LC311:
	.string	"ResolvePossiblyDirectEval"
.LC312:
	.string	"DateCurrentTime"
.LC313:
	.string	"ClearStepping"
.LC314:
	.string	"CollectGarbage"
.LC315:
	.string	"DebugAsyncFunctionEntered"
.LC316:
	.string	"DebugAsyncFunctionSuspended"
.LC317:
	.string	"DebugAsyncFunctionResumed"
.LC318:
	.string	"DebugAsyncFunctionFinished"
.LC319:
	.string	"DebugBreakAtEntry"
.LC320:
	.string	"DebugCollectCoverage"
.LC321:
	.string	"DebugGetLoadedScriptIds"
.LC322:
	.string	"DebugOnFunctionCall"
.LC323:
	.string	"DebugPopPromise"
	.section	.rodata.str1.8
	.align 8
.LC324:
	.string	"DebugPrepareStepInSuspendedGenerator"
	.section	.rodata.str1.1
.LC325:
	.string	"DebugPushPromise"
.LC326:
	.string	"DebugToggleBlockCoverage"
.LC327:
	.string	"DebugTogglePreciseCoverage"
.LC328:
	.string	"FunctionGetInferredName"
.LC329:
	.string	"GetBreakLocations"
.LC330:
	.string	"GetGeneratorScopeCount"
.LC331:
	.string	"GetGeneratorScopeDetails"
.LC332:
	.string	"GetHeapUsage"
.LC333:
	.string	"HandleDebuggerStatement"
.LC334:
	.string	"IsBreakOnException"
.LC335:
	.string	"LiveEditPatchScript"
.LC336:
	.string	"ProfileCreateSnapshotDataBlob"
.LC337:
	.string	"ScheduleBreak"
.LC338:
	.string	"ScriptLocationFromLine2"
	.section	.rodata.str1.8
	.align 8
.LC339:
	.string	"SetGeneratorScopeVariableValue"
	.section	.rodata.str1.1
.LC340:
	.string	"IncBlockCounter"
.LC341:
	.string	"ForInEnumerate"
.LC342:
	.string	"ForInHasProperty"
.LC343:
	.string	"Call"
.LC344:
	.string	"FunctionGetScriptSource"
.LC345:
	.string	"FunctionGetScriptId"
	.section	.rodata.str1.8
	.align 8
.LC346:
	.string	"FunctionGetScriptSourcePosition"
	.section	.rodata.str1.1
.LC347:
	.string	"FunctionGetSourceCode"
.LC348:
	.string	"FunctionIsAPIFunction"
.LC349:
	.string	"IsFunction"
.LC350:
	.string	"AsyncFunctionAwaitCaught"
.LC351:
	.string	"AsyncFunctionAwaitUncaught"
.LC352:
	.string	"AsyncFunctionEnter"
.LC353:
	.string	"AsyncFunctionReject"
.LC354:
	.string	"AsyncFunctionResolve"
.LC355:
	.string	"AsyncGeneratorAwaitCaught"
.LC356:
	.string	"AsyncGeneratorAwaitUncaught"
	.section	.rodata.str1.8
	.align 8
.LC357:
	.string	"AsyncGeneratorHasCatchHandlerForPC"
	.section	.rodata.str1.1
.LC358:
	.string	"AsyncGeneratorReject"
.LC359:
	.string	"AsyncGeneratorResolve"
.LC360:
	.string	"AsyncGeneratorYield"
.LC361:
	.string	"CreateJSGeneratorObject"
.LC362:
	.string	"GeneratorClose"
.LC363:
	.string	"GeneratorGetFunction"
.LC364:
	.string	"GeneratorGetResumeMode"
	.section	.rodata.str1.8
	.align 8
.LC365:
	.string	"ElementsTransitionAndStoreIC_Miss"
	.section	.rodata.str1.1
.LC366:
	.string	"KeyedLoadIC_Miss"
.LC367:
	.string	"KeyedStoreIC_Miss"
.LC368:
	.string	"StoreInArrayLiteralIC_Miss"
.LC369:
	.string	"KeyedStoreIC_Slow"
.LC370:
	.string	"LoadElementWithInterceptor"
.LC371:
	.string	"LoadGlobalIC_Miss"
.LC372:
	.string	"LoadGlobalIC_Slow"
.LC373:
	.string	"LoadIC_Miss"
.LC374:
	.string	"LoadPropertyWithInterceptor"
.LC375:
	.string	"StoreCallbackProperty"
.LC376:
	.string	"StoreGlobalIC_Miss"
.LC377:
	.string	"StoreGlobalICNoFeedback_Miss"
.LC378:
	.string	"StoreGlobalIC_Slow"
.LC379:
	.string	"StoreIC_Miss"
.LC380:
	.string	"StoreInArrayLiteralIC_Slow"
.LC381:
	.string	"StorePropertyWithInterceptor"
.LC382:
	.string	"CloneObjectIC_Miss"
.LC383:
	.string	"KeyedHasIC_Miss"
.LC384:
	.string	"HasElementWithInterceptor"
.LC385:
	.string	"AccessCheck"
.LC386:
	.string	"AllocateByteArray"
.LC387:
	.string	"AllocateInYoungGeneration"
.LC388:
	.string	"AllocateInOldGeneration"
.LC389:
	.string	"AllocateSeqOneByteString"
.LC390:
	.string	"AllocateSeqTwoByteString"
.LC391:
	.string	"AllowDynamicFunction"
.LC392:
	.string	"CreateAsyncFromSyncIterator"
.LC393:
	.string	"CreateListFromArrayLike"
	.section	.rodata.str1.8
	.align 8
.LC394:
	.string	"FatalProcessOutOfMemoryInAllocateRaw"
	.align 8
.LC395:
	.string	"FatalProcessOutOfMemoryInvalidArrayLength"
	.section	.rodata.str1.1
.LC396:
	.string	"GetAndResetRuntimeCallStats"
.LC397:
	.string	"GetTemplateObject"
.LC398:
	.string	"IncrementUseCounter"
.LC399:
	.string	"BytecodeBudgetInterrupt"
.LC400:
	.string	"NewReferenceError"
.LC401:
	.string	"NewSyntaxError"
.LC402:
	.string	"NewTypeError"
.LC403:
	.string	"OrdinaryHasInstance"
.LC404:
	.string	"PromoteScheduledException"
.LC405:
	.string	"ReportMessage"
.LC406:
	.string	"ReThrow"
.LC407:
	.string	"RunMicrotaskCallback"
.LC408:
	.string	"PerformMicrotaskCheckpoint"
.LC409:
	.string	"StackGuard"
.LC410:
	.string	"Throw"
.LC411:
	.string	"ThrowApplyNonFunction"
.LC412:
	.string	"ThrowCalledNonCallable"
	.section	.rodata.str1.8
	.align 8
.LC413:
	.string	"ThrowConstructedNonConstructable"
	.align 8
.LC414:
	.string	"ThrowConstructorReturnedNonObject"
	.section	.rodata.str1.1
.LC415:
	.string	"ThrowInvalidStringLength"
	.section	.rodata.str1.8
	.align 8
.LC416:
	.string	"ThrowInvalidTypedArrayAlignment"
	.section	.rodata.str1.1
.LC417:
	.string	"ThrowIteratorError"
	.section	.rodata.str1.8
	.align 8
.LC418:
	.string	"ThrowIteratorResultNotAnObject"
	.section	.rodata.str1.1
.LC419:
	.string	"ThrowNotConstructor"
	.section	.rodata.str1.8
	.align 8
.LC420:
	.string	"ThrowPatternAssignmentNonCoercible"
	.section	.rodata.str1.1
.LC421:
	.string	"ThrowRangeError"
.LC422:
	.string	"ThrowReferenceError"
	.section	.rodata.str1.8
	.align 8
.LC423:
	.string	"ThrowAccessedUninitializedVariable"
	.section	.rodata.str1.1
.LC424:
	.string	"ThrowStackOverflow"
	.section	.rodata.str1.8
	.align 8
.LC425:
	.string	"ThrowSymbolAsyncIteratorInvalid"
	.section	.rodata.str1.1
.LC426:
	.string	"ThrowSymbolIteratorInvalid"
.LC427:
	.string	"ThrowThrowMethodMissing"
.LC428:
	.string	"ThrowTypeError"
.LC429:
	.string	"ThrowTypeErrorIfStrict"
.LC430:
	.string	"Typeof"
.LC431:
	.string	"UnwindAndFindExceptionHandler"
.LC432:
	.string	"FormatList"
.LC433:
	.string	"FormatListToParts"
.LC434:
	.string	"StringToLowerCaseIntl"
.LC435:
	.string	"StringToUpperCaseIntl"
.LC436:
	.string	"CreateArrayLiteral"
	.section	.rodata.str1.8
	.align 8
.LC437:
	.string	"CreateArrayLiteralWithoutAllocationSite"
	.section	.rodata.str1.1
.LC438:
	.string	"CreateObjectLiteral"
	.section	.rodata.str1.8
	.align 8
.LC439:
	.string	"CreateObjectLiteralWithoutAllocationSite"
	.section	.rodata.str1.1
.LC440:
	.string	"CreateRegExpLiteral"
.LC441:
	.string	"DynamicImportCall"
.LC442:
	.string	"GetImportMetaObject"
.LC443:
	.string	"GetModuleNamespace"
.LC444:
	.string	"GetHoleNaNLower"
.LC445:
	.string	"GetHoleNaNUpper"
.LC446:
	.string	"IsSmi"
.LC447:
	.string	"IsValidSmi"
.LC448:
	.string	"MaxSmi"
.LC449:
	.string	"NumberToString"
.LC450:
	.string	"StringParseFloat"
.LC451:
	.string	"StringParseInt"
.LC452:
	.string	"StringToNumber"
.LC453:
	.string	"AddDictionaryProperty"
.LC454:
	.string	"AddPrivateField"
.LC455:
	.string	"AddPrivateBrand"
.LC456:
	.string	"AllocateHeapNumber"
.LC457:
	.string	"ClassOf"
.LC458:
	.string	"CollectTypeProfile"
	.section	.rodata.str1.8
	.align 8
.LC459:
	.string	"CompleteInobjectSlackTrackingForMap"
	.section	.rodata.str1.1
.LC460:
	.string	"CopyDataProperties"
	.section	.rodata.str1.8
	.align 8
.LC461:
	.string	"CopyDataPropertiesWithExcludedProperties"
	.section	.rodata.str1.1
.LC462:
	.string	"CreateDataProperty"
.LC463:
	.string	"CreateIterResultObject"
.LC464:
	.string	"CreatePrivateAccessors"
	.section	.rodata.str1.8
	.align 8
.LC465:
	.string	"DefineAccessorPropertyUnchecked"
	.section	.rodata.str1.1
.LC466:
	.string	"DefineDataPropertyInLiteral"
.LC467:
	.string	"DefineGetterPropertyUnchecked"
.LC468:
	.string	"DefineSetterPropertyUnchecked"
.LC469:
	.string	"DeleteProperty"
.LC470:
	.string	"GetDerivedMap"
.LC471:
	.string	"GetFunctionName"
.LC472:
	.string	"GetOwnPropertyDescriptor"
.LC473:
	.string	"GetOwnPropertyKeys"
.LC474:
	.string	"GetProperty"
.LC475:
	.string	"HasFastPackedElements"
.LC476:
	.string	"HasInPrototypeChain"
.LC477:
	.string	"HasProperty"
.LC478:
	.string	"InternalSetPrototype"
.LC479:
	.string	"IsJSReceiver"
	.section	.rodata.str1.8
	.align 8
.LC480:
	.string	"JSReceiverPreventExtensionsDontThrow"
	.align 8
.LC481:
	.string	"JSReceiverPreventExtensionsThrow"
	.section	.rodata.str1.1
.LC482:
	.string	"JSReceiverGetPrototypeOf"
	.section	.rodata.str1.8
	.align 8
.LC483:
	.string	"JSReceiverSetPrototypeOfDontThrow"
	.section	.rodata.str1.1
.LC484:
	.string	"JSReceiverSetPrototypeOfThrow"
.LC485:
	.string	"LoadPrivateGetter"
.LC486:
	.string	"LoadPrivateSetter"
.LC487:
	.string	"NewObject"
.LC488:
	.string	"ObjectCreate"
.LC489:
	.string	"ObjectEntries"
.LC490:
	.string	"ObjectEntriesSkipFastPath"
.LC491:
	.string	"ObjectGetOwnPropertyNames"
	.section	.rodata.str1.8
	.align 8
.LC492:
	.string	"ObjectGetOwnPropertyNamesTryFast"
	.section	.rodata.str1.1
.LC493:
	.string	"ObjectHasOwnProperty"
.LC494:
	.string	"ObjectIsExtensible"
.LC495:
	.string	"ObjectKeys"
.LC496:
	.string	"ObjectValues"
.LC497:
	.string	"ObjectValuesSkipFastPath"
	.section	.rodata.str1.8
	.align 8
.LC498:
	.string	"OptimizeObjectForAddingMultipleProperties"
	.align 8
.LC499:
	.string	"PerformSideEffectCheckForObject"
	.section	.rodata.str1.1
.LC500:
	.string	"SetDataProperties"
.LC501:
	.string	"SetKeyedProperty"
.LC502:
	.string	"SetNamedProperty"
.LC503:
	.string	"StoreDataPropertyInLiteral"
.LC504:
	.string	"ShrinkPropertyDictionary"
.LC505:
	.string	"ToFastProperties"
.LC506:
	.string	"ToLength"
.LC507:
	.string	"ToName"
.LC508:
	.string	"ToNumber"
.LC509:
	.string	"ToNumeric"
.LC510:
	.string	"ToObject"
.LC511:
	.string	"ToStringRT"
.LC512:
	.string	"TryMigrateInstance"
.LC513:
	.string	"Add"
.LC514:
	.string	"Equal"
.LC515:
	.string	"GreaterThan"
.LC516:
	.string	"GreaterThanOrEqual"
.LC517:
	.string	"LessThan"
.LC518:
	.string	"LessThanOrEqual"
.LC519:
	.string	"NotEqual"
.LC520:
	.string	"StrictEqual"
.LC521:
	.string	"StrictNotEqual"
.LC522:
	.string	"EnqueueMicrotask"
.LC523:
	.string	"PromiseHookAfter"
.LC524:
	.string	"PromiseHookBefore"
.LC525:
	.string	"PromiseHookInit"
.LC526:
	.string	"AwaitPromisesInit"
.LC527:
	.string	"AwaitPromisesInitOld"
.LC528:
	.string	"PromiseMarkAsHandled"
.LC529:
	.string	"PromiseRejectEventFromStack"
.LC530:
	.string	"PromiseRevokeReject"
.LC531:
	.string	"PromiseStatus"
.LC532:
	.string	"RejectPromise"
.LC533:
	.string	"ResolvePromise"
.LC534:
	.string	"PromiseRejectAfterResolved"
.LC535:
	.string	"PromiseResolveAfterResolved"
.LC536:
	.string	"CheckProxyGetSetTrapResult"
.LC537:
	.string	"CheckProxyHasTrapResult"
.LC538:
	.string	"CheckProxyDeleteTrapResult"
.LC539:
	.string	"GetPropertyWithReceiver"
.LC540:
	.string	"SetPropertyWithReceiver"
.LC541:
	.string	"IsRegExp"
.LC542:
	.string	"RegExpExec"
.LC543:
	.string	"RegExpExecMultiple"
.LC544:
	.string	"RegExpInitializeAndCompile"
.LC545:
	.string	"RegExpReplaceRT"
.LC546:
	.string	"RegExpSplit"
	.section	.rodata.str1.8
	.align 8
.LC547:
	.string	"StringReplaceNonGlobalRegExpWithFunction"
	.section	.rodata.str1.1
.LC548:
	.string	"StringSplit"
.LC549:
	.string	"DeclareEvalFunction"
.LC550:
	.string	"DeclareEvalVar"
.LC551:
	.string	"DeclareGlobals"
.LC552:
	.string	"DeleteLookupSlot"
.LC553:
	.string	"LoadLookupSlot"
.LC554:
	.string	"LoadLookupSlotInsideTypeof"
.LC555:
	.string	"NewArgumentsElements"
.LC556:
	.string	"NewClosure"
.LC557:
	.string	"NewClosure_Tenured"
.LC558:
	.string	"NewFunctionContext"
.LC559:
	.string	"NewRestParameter"
.LC560:
	.string	"NewScriptContext"
.LC561:
	.string	"NewSloppyArguments"
.LC562:
	.string	"NewSloppyArguments_Generic"
.LC563:
	.string	"NewStrictArguments"
.LC564:
	.string	"PushBlockContext"
.LC565:
	.string	"PushCatchContext"
.LC566:
	.string	"PushModuleContext"
.LC567:
	.string	"PushWithContext"
.LC568:
	.string	"StoreLookupSlot_Sloppy"
	.section	.rodata.str1.8
	.align 8
.LC569:
	.string	"StoreLookupSlot_SloppyHoisting"
	.section	.rodata.str1.1
.LC570:
	.string	"StoreLookupSlot_Strict"
.LC571:
	.string	"ThrowConstAssignError"
.LC572:
	.string	"FlattenString"
.LC573:
	.string	"GetSubstitution"
.LC574:
	.string	"InternalizeString"
.LC575:
	.string	"StringAdd"
.LC576:
	.string	"StringBuilderConcat"
.LC577:
	.string	"StringCharCodeAt"
.LC578:
	.string	"StringEqual"
.LC579:
	.string	"StringEscapeQuotes"
.LC580:
	.string	"StringGreaterThan"
.LC581:
	.string	"StringGreaterThanOrEqual"
.LC582:
	.string	"StringIncludes"
.LC583:
	.string	"StringIndexOf"
.LC584:
	.string	"StringIndexOfUnchecked"
.LC585:
	.string	"StringLastIndexOf"
.LC586:
	.string	"StringLessThan"
.LC587:
	.string	"StringLessThanOrEqual"
.LC588:
	.string	"StringMaxLength"
	.section	.rodata.str1.8
	.align 8
.LC589:
	.string	"StringReplaceOneCharWithString"
	.section	.rodata.str1.1
.LC590:
	.string	"StringCompareSequence"
.LC591:
	.string	"StringSubstring"
.LC592:
	.string	"StringToArray"
.LC593:
	.string	"StringTrim"
.LC594:
	.string	"CreatePrivateNameSymbol"
.LC595:
	.string	"CreatePrivateSymbol"
.LC596:
	.string	"SymbolDescriptiveString"
.LC597:
	.string	"SymbolIsPrivate"
.LC598:
	.string	"Abort"
.LC599:
	.string	"AbortJS"
.LC600:
	.string	"AbortCSAAssert"
.LC601:
	.string	"ArraySpeciesProtector"
.LC602:
	.string	"ClearFunctionFeedback"
.LC603:
	.string	"ClearMegamorphicStubCache"
.LC604:
	.string	"CloneWasmModule"
.LC605:
	.string	"CompleteInobjectSlackTracking"
.LC606:
	.string	"ConstructConsString"
.LC607:
	.string	"ConstructDouble"
.LC608:
	.string	"ConstructSlicedString"
.LC609:
	.string	"DebugPrint"
.LC610:
	.string	"DebugTrace"
.LC611:
	.string	"DebugTrackRetainingPath"
.LC612:
	.string	"DeoptimizeFunction"
.LC613:
	.string	"DeserializeWasmModule"
.LC614:
	.string	"DisallowCodegenFromStrings"
.LC615:
	.string	"DisallowWasmCodegen"
.LC616:
	.string	"DisassembleFunction"
.LC617:
	.string	"EnableCodeLoggingForTesting"
	.section	.rodata.str1.8
	.align 8
.LC618:
	.string	"EnsureFeedbackVectorForFunction"
	.section	.rodata.str1.1
.LC619:
	.string	"FreezeWasmLazyCompilation"
.LC620:
	.string	"GetCallable"
.LC621:
	.string	"GetInitializerFunction"
.LC622:
	.string	"GetOptimizationStatus"
.LC623:
	.string	"GetUndetectable"
.LC624:
	.string	"GetWasmExceptionId"
.LC625:
	.string	"GetWasmExceptionValues"
.LC626:
	.string	"GetWasmRecoveredTrapCount"
.LC627:
	.string	"GlobalPrint"
.LC628:
	.string	"HasDictionaryElements"
.LC629:
	.string	"HasDoubleElements"
	.section	.rodata.str1.8
	.align 8
.LC630:
	.string	"HasElementsInALargeObjectSpace"
	.section	.rodata.str1.1
.LC631:
	.string	"HasFastElements"
.LC632:
	.string	"HasFastProperties"
.LC633:
	.string	"HasFixedBigInt64Elements"
.LC634:
	.string	"HasFixedBigUint64Elements"
.LC635:
	.string	"HasFixedFloat32Elements"
.LC636:
	.string	"HasFixedFloat64Elements"
.LC637:
	.string	"HasFixedInt16Elements"
.LC638:
	.string	"HasFixedInt32Elements"
.LC639:
	.string	"HasFixedInt8Elements"
.LC640:
	.string	"HasFixedUint16Elements"
.LC641:
	.string	"HasFixedUint32Elements"
.LC642:
	.string	"HasFixedUint8ClampedElements"
.LC643:
	.string	"HasFixedUint8Elements"
.LC644:
	.string	"HasHoleyElements"
.LC645:
	.string	"HasObjectElements"
.LC646:
	.string	"HasPackedElements"
.LC647:
	.string	"HasSloppyArgumentsElements"
.LC648:
	.string	"HasSmiElements"
.LC649:
	.string	"HasSmiOrObjectElements"
.LC650:
	.string	"HaveSameMap"
.LC651:
	.string	"HeapObjectVerify"
.LC652:
	.string	"ICsAreEnabled"
.LC653:
	.string	"InYoungGeneration"
.LC654:
	.string	"IsAsmWasmCode"
	.section	.rodata.str1.8
	.align 8
.LC655:
	.string	"IsConcurrentRecompilationSupported"
	.section	.rodata.str1.1
.LC656:
	.string	"IsLiftoffFunction"
.LC657:
	.string	"IsThreadInWasm"
.LC658:
	.string	"IsWasmCode"
.LC659:
	.string	"IsWasmTrapHandlerEnabled"
.LC660:
	.string	"RegexpHasBytecode"
.LC661:
	.string	"RegexpHasNativeCode"
.LC662:
	.string	"MapIteratorProtector"
.LC663:
	.string	"NeverOptimizeFunction"
.LC664:
	.string	"NotifyContextDisposed"
.LC665:
	.string	"OptimizeFunctionOnNextCall"
.LC666:
	.string	"OptimizeOsr"
	.section	.rodata.str1.8
	.align 8
.LC667:
	.string	"PrepareFunctionForOptimization"
	.section	.rodata.str1.1
.LC668:
	.string	"PrintWithNameForAssert"
.LC669:
	.string	"RedirectToWasmInterpreter"
.LC670:
	.string	"RunningInSimulator"
.LC671:
	.string	"SerializeWasmModule"
.LC672:
	.string	"SetAllocationTimeout"
.LC673:
	.string	"SetForceSlowPath"
.LC674:
	.string	"SetIteratorProtector"
.LC675:
	.string	"SetWasmCompileControls"
.LC676:
	.string	"SetWasmInstantiateControls"
.LC677:
	.string	"SetWasmThreadsEnabled"
.LC678:
	.string	"StringIteratorProtector"
.LC679:
	.string	"SystemBreak"
.LC680:
	.string	"TraceEnter"
.LC681:
	.string	"TraceExit"
.LC682:
	.string	"TurbofanStaticAssert"
	.section	.rodata.str1.8
	.align 8
.LC683:
	.string	"UnblockConcurrentRecompilation"
	.section	.rodata.str1.1
.LC684:
	.string	"WasmGetNumberOfInstances"
.LC685:
	.string	"WasmNumInterpretedCalls"
.LC686:
	.string	"WasmTierUpFunction"
.LC687:
	.string	"WasmTraceMemory"
.LC688:
	.string	"DeoptimizeNow"
.LC689:
	.string	"ArrayBufferDetach"
.LC690:
	.string	"TypedArrayCopyElements"
.LC691:
	.string	"TypedArrayGetBuffer"
.LC692:
	.string	"TypedArraySet"
.LC693:
	.string	"TypedArraySortFast"
.LC694:
	.string	"ThrowWasmError"
.LC695:
	.string	"ThrowWasmStackOverflow"
.LC696:
	.string	"WasmI32AtomicWait"
.LC697:
	.string	"WasmI64AtomicWait"
.LC698:
	.string	"WasmAtomicNotify"
.LC699:
	.string	"WasmExceptionGetValues"
.LC700:
	.string	"WasmExceptionGetTag"
.LC701:
	.string	"WasmMemoryGrow"
.LC702:
	.string	"WasmRunInterpreter"
.LC703:
	.string	"WasmStackGuard"
.LC704:
	.string	"WasmThrowCreate"
.LC705:
	.string	"WasmThrowTypeError"
.LC706:
	.string	"WasmRefFunc"
.LC707:
	.string	"WasmFunctionTableGet"
.LC708:
	.string	"WasmFunctionTableSet"
.LC709:
	.string	"WasmTableInit"
.LC710:
	.string	"WasmTableCopy"
.LC711:
	.string	"WasmTableGrow"
.LC712:
	.string	"WasmTableFill"
.LC713:
	.string	"WasmIsValidFuncRefValue"
.LC714:
	.string	"WasmCompileLazy"
.LC715:
	.string	"WasmNewMultiReturnFixedArray"
.LC716:
	.string	"WasmNewMultiReturnJSArray"
.LC717:
	.string	"HandleApiCall"
.LC718:
	.string	"HandleApiCallAsFunction"
.LC719:
	.string	"HandleApiCallAsConstructor"
.LC720:
	.string	"EmptyFunction"
.LC721:
	.string	"Illegal"
.LC722:
	.string	"StrictPoisonPillThrower"
.LC723:
	.string	"UnsupportedThrower"
.LC724:
	.string	"ArrayConcat"
.LC725:
	.string	"ArrayPrototypeFill"
.LC726:
	.string	"ArrayPop"
.LC727:
	.string	"ArrayPush"
.LC728:
	.string	"ArrayShift"
.LC729:
	.string	"ArrayUnshift"
.LC730:
	.string	"ArrayBufferConstructor"
	.section	.rodata.str1.8
	.align 8
.LC731:
	.string	"ArrayBufferConstructor_DoNotInitialize"
	.align 8
.LC732:
	.string	"ArrayBufferPrototypeGetByteLength"
	.section	.rodata.str1.1
.LC733:
	.string	"ArrayBufferIsView"
.LC734:
	.string	"ArrayBufferPrototypeSlice"
.LC735:
	.string	"BigIntConstructor"
.LC736:
	.string	"BigIntAsUintN"
.LC737:
	.string	"BigIntAsIntN"
.LC738:
	.string	"BigIntPrototypeToLocaleString"
.LC739:
	.string	"BigIntPrototypeToString"
.LC740:
	.string	"BigIntPrototypeValueOf"
	.section	.rodata.str1.8
	.align 8
.LC741:
	.string	"CallSitePrototypeGetColumnNumber"
	.align 8
.LC742:
	.string	"CallSitePrototypeGetEvalOrigin"
	.section	.rodata.str1.1
.LC743:
	.string	"CallSitePrototypeGetFileName"
.LC744:
	.string	"CallSitePrototypeGetFunction"
	.section	.rodata.str1.8
	.align 8
.LC745:
	.string	"CallSitePrototypeGetFunctionName"
	.align 8
.LC746:
	.string	"CallSitePrototypeGetLineNumber"
	.align 8
.LC747:
	.string	"CallSitePrototypeGetMethodName"
	.section	.rodata.str1.1
.LC748:
	.string	"CallSitePrototypeGetPosition"
	.section	.rodata.str1.8
	.align 8
.LC749:
	.string	"CallSitePrototypeGetPromiseIndex"
	.align 8
.LC750:
	.string	"CallSitePrototypeGetScriptNameOrSourceURL"
	.section	.rodata.str1.1
.LC751:
	.string	"CallSitePrototypeGetThis"
.LC752:
	.string	"CallSitePrototypeGetTypeName"
.LC753:
	.string	"CallSitePrototypeIsAsync"
	.section	.rodata.str1.8
	.align 8
.LC754:
	.string	"CallSitePrototypeIsConstructor"
	.section	.rodata.str1.1
.LC755:
	.string	"CallSitePrototypeIsEval"
.LC756:
	.string	"CallSitePrototypeIsNative"
.LC757:
	.string	"CallSitePrototypeIsPromiseAll"
.LC758:
	.string	"CallSitePrototypeIsToplevel"
.LC759:
	.string	"CallSitePrototypeToString"
.LC760:
	.string	"ConsoleDebug"
.LC761:
	.string	"ConsoleError"
.LC762:
	.string	"ConsoleInfo"
.LC763:
	.string	"ConsoleLog"
.LC764:
	.string	"ConsoleWarn"
.LC765:
	.string	"ConsoleDir"
.LC766:
	.string	"ConsoleDirXml"
.LC767:
	.string	"ConsoleTable"
.LC768:
	.string	"ConsoleTrace"
.LC769:
	.string	"ConsoleGroup"
.LC770:
	.string	"ConsoleGroupCollapsed"
.LC771:
	.string	"ConsoleGroupEnd"
.LC772:
	.string	"ConsoleClear"
.LC773:
	.string	"ConsoleCount"
.LC774:
	.string	"ConsoleCountReset"
.LC775:
	.string	"ConsoleAssert"
.LC776:
	.string	"ConsoleProfile"
.LC777:
	.string	"ConsoleProfileEnd"
.LC778:
	.string	"ConsoleTime"
.LC779:
	.string	"ConsoleTimeLog"
.LC780:
	.string	"ConsoleTimeEnd"
.LC781:
	.string	"ConsoleTimeStamp"
.LC782:
	.string	"ConsoleContext"
.LC783:
	.string	"DataViewConstructor"
.LC784:
	.string	"DateConstructor"
.LC785:
	.string	"DatePrototypeGetYear"
.LC786:
	.string	"DatePrototypeSetYear"
.LC787:
	.string	"DateNow"
.LC788:
	.string	"DateParse"
.LC789:
	.string	"DatePrototypeSetDate"
.LC790:
	.string	"DatePrototypeSetFullYear"
.LC791:
	.string	"DatePrototypeSetHours"
.LC792:
	.string	"DatePrototypeSetMilliseconds"
.LC793:
	.string	"DatePrototypeSetMinutes"
.LC794:
	.string	"DatePrototypeSetMonth"
.LC795:
	.string	"DatePrototypeSetSeconds"
.LC796:
	.string	"DatePrototypeSetTime"
.LC797:
	.string	"DatePrototypeSetUTCDate"
.LC798:
	.string	"DatePrototypeSetUTCFullYear"
.LC799:
	.string	"DatePrototypeSetUTCHours"
	.section	.rodata.str1.8
	.align 8
.LC800:
	.string	"DatePrototypeSetUTCMilliseconds"
	.section	.rodata.str1.1
.LC801:
	.string	"DatePrototypeSetUTCMinutes"
.LC802:
	.string	"DatePrototypeSetUTCMonth"
.LC803:
	.string	"DatePrototypeSetUTCSeconds"
.LC804:
	.string	"DatePrototypeToDateString"
.LC805:
	.string	"DatePrototypeToISOString"
.LC806:
	.string	"DatePrototypeToUTCString"
.LC807:
	.string	"DatePrototypeToString"
.LC808:
	.string	"DatePrototypeToTimeString"
.LC809:
	.string	"DatePrototypeToJson"
.LC810:
	.string	"DateUTC"
.LC811:
	.string	"ErrorConstructor"
.LC812:
	.string	"ErrorCaptureStackTrace"
.LC813:
	.string	"ErrorPrototypeToString"
.LC814:
	.string	"MakeError"
.LC815:
	.string	"MakeRangeError"
.LC816:
	.string	"MakeSyntaxError"
.LC817:
	.string	"MakeTypeError"
.LC818:
	.string	"MakeURIError"
.LC819:
	.string	"ExtrasUtilsUncurryThis"
.LC820:
	.string	"ExtrasUtilsCallReflectApply"
.LC821:
	.string	"FunctionConstructor"
.LC822:
	.string	"FunctionPrototypeBind"
.LC823:
	.string	"FunctionPrototypeToString"
.LC824:
	.string	"GeneratorFunctionConstructor"
.LC825:
	.string	"AsyncFunctionConstructor"
.LC826:
	.string	"GlobalDecodeURI"
.LC827:
	.string	"GlobalDecodeURIComponent"
.LC828:
	.string	"GlobalEncodeURI"
.LC829:
	.string	"GlobalEncodeURIComponent"
.LC830:
	.string	"GlobalEscape"
.LC831:
	.string	"GlobalUnescape"
.LC832:
	.string	"GlobalEval"
.LC833:
	.string	"JsonParse"
.LC834:
	.string	"JsonStringify"
.LC835:
	.string	"MapPrototypeClear"
.LC836:
	.string	"NumberPrototypeToExponential"
.LC837:
	.string	"NumberPrototypeToFixed"
.LC838:
	.string	"NumberPrototypeToLocaleString"
.LC839:
	.string	"NumberPrototypeToPrecision"
.LC840:
	.string	"NumberPrototypeToString"
.LC841:
	.string	"ObjectDefineGetter"
.LC842:
	.string	"ObjectDefineProperties"
.LC843:
	.string	"ObjectDefineProperty"
.LC844:
	.string	"ObjectDefineSetter"
.LC845:
	.string	"ObjectFreeze"
	.section	.rodata.str1.8
	.align 8
.LC846:
	.string	"ObjectGetOwnPropertyDescriptors"
	.section	.rodata.str1.1
.LC847:
	.string	"ObjectGetOwnPropertySymbols"
.LC848:
	.string	"ObjectIsFrozen"
.LC849:
	.string	"ObjectIsSealed"
.LC850:
	.string	"ObjectLookupGetter"
.LC851:
	.string	"ObjectLookupSetter"
	.section	.rodata.str1.8
	.align 8
.LC852:
	.string	"ObjectPrototypePropertyIsEnumerable"
	.section	.rodata.str1.1
.LC853:
	.string	"ObjectPrototypeGetProto"
.LC854:
	.string	"ObjectPrototypeSetProto"
.LC855:
	.string	"ObjectSeal"
.LC856:
	.string	"IsPromise"
.LC857:
	.string	"ReflectDefineProperty"
	.section	.rodata.str1.8
	.align 8
.LC858:
	.string	"ReflectGetOwnPropertyDescriptor"
	.section	.rodata.str1.1
.LC859:
	.string	"ReflectOwnKeys"
.LC860:
	.string	"ReflectSet"
.LC861:
	.string	"RegExpCapture1Getter"
.LC862:
	.string	"RegExpCapture2Getter"
.LC863:
	.string	"RegExpCapture3Getter"
.LC864:
	.string	"RegExpCapture4Getter"
.LC865:
	.string	"RegExpCapture5Getter"
.LC866:
	.string	"RegExpCapture6Getter"
.LC867:
	.string	"RegExpCapture7Getter"
.LC868:
	.string	"RegExpCapture8Getter"
.LC869:
	.string	"RegExpCapture9Getter"
.LC870:
	.string	"RegExpInputGetter"
.LC871:
	.string	"RegExpInputSetter"
.LC872:
	.string	"RegExpLastMatchGetter"
.LC873:
	.string	"RegExpLastParenGetter"
.LC874:
	.string	"RegExpLeftContextGetter"
.LC875:
	.string	"RegExpPrototypeToString"
.LC876:
	.string	"RegExpRightContextGetter"
.LC877:
	.string	"SetPrototypeClear"
	.section	.rodata.str1.8
	.align 8
.LC878:
	.string	"SharedArrayBufferPrototypeGetByteLength"
	.align 8
.LC879:
	.string	"SharedArrayBufferPrototypeSlice"
	.section	.rodata.str1.1
.LC880:
	.string	"AtomicsNotify"
.LC881:
	.string	"AtomicsIsLockFree"
.LC882:
	.string	"AtomicsWait"
.LC883:
	.string	"AtomicsWake"
.LC884:
	.string	"StringFromCodePoint"
.LC885:
	.string	"StringPrototypeLastIndexOf"
.LC886:
	.string	"StringPrototypeLocaleCompare"
.LC887:
	.string	"StringRaw"
.LC888:
	.string	"SymbolConstructor"
.LC889:
	.string	"SymbolFor"
.LC890:
	.string	"SymbolKeyFor"
.LC891:
	.string	"TypedArrayPrototypeBuffer"
.LC892:
	.string	"TypedArrayPrototypeCopyWithin"
.LC893:
	.string	"TypedArrayPrototypeFill"
.LC894:
	.string	"TypedArrayPrototypeIncludes"
.LC895:
	.string	"TypedArrayPrototypeIndexOf"
	.section	.rodata.str1.8
	.align 8
.LC896:
	.string	"TypedArrayPrototypeLastIndexOf"
	.section	.rodata.str1.1
.LC897:
	.string	"TypedArrayPrototypeReverse"
	.section	.rodata.str1.8
	.align 8
.LC898:
	.string	"AsyncGeneratorFunctionConstructor"
	.section	.rodata.str1.1
.LC899:
	.string	"IsTraceCategoryEnabled"
.LC900:
	.string	"Trace"
	.section	.rodata.str1.8
	.align 8
.LC901:
	.string	"FinalizationGroupCleanupIteratorNext"
	.section	.rodata.str1.1
.LC902:
	.string	"FinalizationGroupCleanupSome"
.LC903:
	.string	"FinalizationGroupConstructor"
.LC904:
	.string	"FinalizationGroupRegister"
.LC905:
	.string	"FinalizationGroupUnregister"
.LC906:
	.string	"WeakRefConstructor"
.LC907:
	.string	"WeakRefDeref"
.LC908:
	.string	"CollatorConstructor"
.LC909:
	.string	"CollatorInternalCompare"
.LC910:
	.string	"CollatorPrototypeCompare"
.LC911:
	.string	"CollatorSupportedLocalesOf"
	.section	.rodata.str1.8
	.align 8
.LC912:
	.string	"CollatorPrototypeResolvedOptions"
	.align 8
.LC913:
	.string	"DatePrototypeToLocaleDateString"
	.section	.rodata.str1.1
.LC914:
	.string	"DatePrototypeToLocaleString"
	.section	.rodata.str1.8
	.align 8
.LC915:
	.string	"DatePrototypeToLocaleTimeString"
	.section	.rodata.str1.1
.LC916:
	.string	"DateTimeFormatConstructor"
.LC917:
	.string	"DateTimeFormatInternalFormat"
.LC918:
	.string	"DateTimeFormatPrototypeFormat"
	.section	.rodata.str1.8
	.align 8
.LC919:
	.string	"DateTimeFormatPrototypeFormatRange"
	.align 8
.LC920:
	.string	"DateTimeFormatPrototypeFormatRangeToParts"
	.align 8
.LC921:
	.string	"DateTimeFormatPrototypeFormatToParts"
	.align 8
.LC922:
	.string	"DateTimeFormatPrototypeResolvedOptions"
	.align 8
.LC923:
	.string	"DateTimeFormatSupportedLocalesOf"
	.section	.rodata.str1.1
.LC924:
	.string	"IntlGetCanonicalLocales"
.LC925:
	.string	"ListFormatConstructor"
	.section	.rodata.str1.8
	.align 8
.LC926:
	.string	"ListFormatPrototypeResolvedOptions"
	.section	.rodata.str1.1
.LC927:
	.string	"ListFormatSupportedLocalesOf"
.LC928:
	.string	"LocaleConstructor"
.LC929:
	.string	"LocalePrototypeBaseName"
.LC930:
	.string	"LocalePrototypeCalendar"
.LC931:
	.string	"LocalePrototypeCaseFirst"
.LC932:
	.string	"LocalePrototypeCollation"
.LC933:
	.string	"LocalePrototypeHourCycle"
.LC934:
	.string	"LocalePrototypeLanguage"
.LC935:
	.string	"LocalePrototypeMaximize"
.LC936:
	.string	"LocalePrototypeMinimize"
.LC937:
	.string	"LocalePrototypeNumeric"
	.section	.rodata.str1.8
	.align 8
.LC938:
	.string	"LocalePrototypeNumberingSystem"
	.section	.rodata.str1.1
.LC939:
	.string	"LocalePrototypeRegion"
.LC940:
	.string	"LocalePrototypeScript"
.LC941:
	.string	"LocalePrototypeToString"
.LC942:
	.string	"NumberFormatConstructor"
	.section	.rodata.str1.8
	.align 8
.LC943:
	.string	"NumberFormatInternalFormatNumber"
	.align 8
.LC944:
	.string	"NumberFormatPrototypeFormatNumber"
	.align 8
.LC945:
	.string	"NumberFormatPrototypeFormatToParts"
	.align 8
.LC946:
	.string	"NumberFormatPrototypeResolvedOptions"
	.align 8
.LC947:
	.string	"NumberFormatSupportedLocalesOf"
	.section	.rodata.str1.1
.LC948:
	.string	"PluralRulesConstructor"
	.section	.rodata.str1.8
	.align 8
.LC949:
	.string	"PluralRulesPrototypeResolvedOptions"
	.section	.rodata.str1.1
.LC950:
	.string	"PluralRulesPrototypeSelect"
.LC951:
	.string	"PluralRulesSupportedLocalesOf"
.LC952:
	.string	"RelativeTimeFormatConstructor"
	.section	.rodata.str1.8
	.align 8
.LC953:
	.string	"RelativeTimeFormatPrototypeFormat"
	.align 8
.LC954:
	.string	"RelativeTimeFormatPrototypeFormatToParts"
	.align 8
.LC955:
	.string	"RelativeTimeFormatPrototypeResolvedOptions"
	.align 8
.LC956:
	.string	"RelativeTimeFormatSupportedLocalesOf"
	.section	.rodata.str1.1
.LC957:
	.string	"SegmenterConstructor"
	.section	.rodata.str1.8
	.align 8
.LC958:
	.string	"SegmenterPrototypeResolvedOptions"
	.section	.rodata.str1.1
.LC959:
	.string	"SegmenterPrototypeSegment"
.LC960:
	.string	"SegmenterSupportedLocalesOf"
	.section	.rodata.str1.8
	.align 8
.LC961:
	.string	"SegmentIteratorPrototypeBreakType"
	.align 8
.LC962:
	.string	"SegmentIteratorPrototypeFollowing"
	.align 8
.LC963:
	.string	"SegmentIteratorPrototypePreceding"
	.section	.rodata.str1.1
.LC964:
	.string	"SegmentIteratorPrototypeIndex"
.LC965:
	.string	"SegmentIteratorPrototypeNext"
.LC966:
	.string	"StringPrototypeNormalizeIntl"
	.section	.rodata.str1.8
	.align 8
.LC967:
	.string	"StringPrototypeToLocaleLowerCase"
	.align 8
.LC968:
	.string	"StringPrototypeToLocaleUpperCase"
	.align 8
.LC969:
	.string	"StringPrototypeToUpperCaseIntl"
	.section	.rodata.str1.1
.LC970:
	.string	"V8BreakIteratorConstructor"
	.section	.rodata.str1.8
	.align 8
.LC971:
	.string	"V8BreakIteratorInternalAdoptText"
	.align 8
.LC972:
	.string	"V8BreakIteratorInternalBreakType"
	.align 8
.LC973:
	.string	"V8BreakIteratorInternalCurrent"
	.section	.rodata.str1.1
.LC974:
	.string	"V8BreakIteratorInternalFirst"
.LC975:
	.string	"V8BreakIteratorInternalNext"
	.section	.rodata.str1.8
	.align 8
.LC976:
	.string	"V8BreakIteratorPrototypeAdoptText"
	.align 8
.LC977:
	.string	"V8BreakIteratorPrototypeBreakType"
	.align 8
.LC978:
	.string	"V8BreakIteratorPrototypeCurrent"
	.section	.rodata.str1.1
.LC979:
	.string	"V8BreakIteratorPrototypeFirst"
.LC980:
	.string	"V8BreakIteratorPrototypeNext"
	.section	.rodata.str1.8
	.align 8
.LC981:
	.string	"V8BreakIteratorPrototypeResolvedOptions"
	.align 8
.LC982:
	.string	"V8BreakIteratorSupportedLocalesOf"
	.section	.rodata.str1.1
.LC983:
	.string	"API_ArrayBuffer_Cast"
.LC984:
	.string	"API_ArrayBuffer_Detach"
.LC985:
	.string	"API_ArrayBuffer_New"
.LC986:
	.string	"API_Array_CloneElementAt"
.LC987:
	.string	"API_Array_New"
.LC988:
	.string	"API_BigInt64Array_New"
.LC989:
	.string	"API_BigInt_NewFromWords"
.LC990:
	.string	"API_BigIntObject_BigIntValue"
.LC991:
	.string	"API_BigIntObject_New"
.LC992:
	.string	"API_BigUint64Array_New"
	.section	.rodata.str1.8
	.align 8
.LC993:
	.string	"API_BooleanObject_BooleanValue"
	.section	.rodata.str1.1
.LC994:
	.string	"API_BooleanObject_New"
.LC995:
	.string	"API_Context_New"
.LC996:
	.string	"API_Context_NewRemoteContext"
.LC997:
	.string	"API_DataView_New"
.LC998:
	.string	"API_Date_New"
.LC999:
	.string	"API_Date_NumberValue"
.LC1000:
	.string	"API_Debug_Call"
.LC1001:
	.string	"API_debug_GetPrivateFields"
.LC1002:
	.string	"API_Error_New"
.LC1003:
	.string	"API_External_New"
.LC1004:
	.string	"API_Float32Array_New"
.LC1005:
	.string	"API_Float64Array_New"
.LC1006:
	.string	"API_Function_Call"
.LC1007:
	.string	"API_Function_New"
.LC1008:
	.string	"API_Function_NewInstance"
	.section	.rodata.str1.8
	.align 8
.LC1009:
	.string	"API_FunctionTemplate_GetFunction"
	.section	.rodata.str1.1
.LC1010:
	.string	"API_FunctionTemplate_New"
	.section	.rodata.str1.8
	.align 8
.LC1011:
	.string	"API_FunctionTemplate_NewRemoteInstance"
	.align 8
.LC1012:
	.string	"API_FunctionTemplate_NewWithCache"
	.align 8
.LC1013:
	.string	"API_FunctionTemplate_NewWithFastHandler"
	.section	.rodata.str1.1
.LC1014:
	.string	"API_Int16Array_New"
.LC1015:
	.string	"API_Int32Array_New"
.LC1016:
	.string	"API_Int8Array_New"
	.section	.rodata.str1.8
	.align 8
.LC1017:
	.string	"API_Isolate_DateTimeConfigurationChangeNotification"
	.align 8
.LC1018:
	.string	"API_Isolate_LocaleConfigurationChangeNotification"
	.section	.rodata.str1.1
.LC1019:
	.string	"API_FinalizationGroup_Cleanup"
.LC1020:
	.string	"API_JSON_Parse"
.LC1021:
	.string	"API_JSON_Stringify"
.LC1022:
	.string	"API_Map_AsArray"
.LC1023:
	.string	"API_Map_Clear"
.LC1024:
	.string	"API_Map_Delete"
.LC1025:
	.string	"API_Map_Get"
.LC1026:
	.string	"API_Map_Has"
.LC1027:
	.string	"API_Map_New"
.LC1028:
	.string	"API_Map_Set"
.LC1029:
	.string	"API_Message_GetEndColumn"
.LC1030:
	.string	"API_Message_GetLineNumber"
.LC1031:
	.string	"API_Message_GetSourceLine"
.LC1032:
	.string	"API_Message_GetStartColumn"
.LC1033:
	.string	"API_Module_Evaluate"
.LC1034:
	.string	"API_Module_InstantiateModule"
	.section	.rodata.str1.8
	.align 8
.LC1035:
	.string	"API_Module_SetSyntheticModuleExport"
	.section	.rodata.str1.1
.LC1036:
	.string	"API_NumberObject_New"
.LC1037:
	.string	"API_NumberObject_NumberValue"
.LC1038:
	.string	"API_Object_CallAsConstructor"
.LC1039:
	.string	"API_Object_CallAsFunction"
.LC1040:
	.string	"API_Object_CreateDataProperty"
.LC1041:
	.string	"API_Object_DefineOwnProperty"
.LC1042:
	.string	"API_Object_DefineProperty"
.LC1043:
	.string	"API_Object_Delete"
.LC1044:
	.string	"API_Object_DeleteProperty"
.LC1045:
	.string	"API_Object_ForceSet"
.LC1046:
	.string	"API_Object_Get"
	.section	.rodata.str1.8
	.align 8
.LC1047:
	.string	"API_Object_GetOwnPropertyDescriptor"
	.align 8
.LC1048:
	.string	"API_Object_GetOwnPropertyNames"
	.align 8
.LC1049:
	.string	"API_Object_GetPropertyAttributes"
	.section	.rodata.str1.1
.LC1050:
	.string	"API_Object_GetPropertyNames"
	.section	.rodata.str1.8
	.align 8
.LC1051:
	.string	"API_Object_GetRealNamedProperty"
	.align 8
.LC1052:
	.string	"API_Object_GetRealNamedPropertyAttributes"
	.align 8
.LC1053:
	.string	"API_Object_GetRealNamedPropertyAttributesInPrototypeChain"
	.align 8
.LC1054:
	.string	"API_Object_GetRealNamedPropertyInPrototypeChain"
	.section	.rodata.str1.1
.LC1055:
	.string	"API_Object_Has"
.LC1056:
	.string	"API_Object_HasOwnProperty"
	.section	.rodata.str1.8
	.align 8
.LC1057:
	.string	"API_Object_HasRealIndexedProperty"
	.align 8
.LC1058:
	.string	"API_Object_HasRealNamedCallbackProperty"
	.align 8
.LC1059:
	.string	"API_Object_HasRealNamedProperty"
	.section	.rodata.str1.1
.LC1060:
	.string	"API_Object_New"
	.section	.rodata.str1.8
	.align 8
.LC1061:
	.string	"API_Object_ObjectProtoToString"
	.section	.rodata.str1.1
.LC1062:
	.string	"API_Object_Set"
.LC1063:
	.string	"API_Object_SetAccessor"
.LC1064:
	.string	"API_Object_SetIntegrityLevel"
.LC1065:
	.string	"API_Object_SetPrivate"
.LC1066:
	.string	"API_Object_SetPrototype"
.LC1067:
	.string	"API_ObjectTemplate_New"
	.section	.rodata.str1.8
	.align 8
.LC1068:
	.string	"API_ObjectTemplate_NewInstance"
	.section	.rodata.str1.1
.LC1069:
	.string	"API_Object_ToArrayIndex"
.LC1070:
	.string	"API_Object_ToBigInt"
.LC1071:
	.string	"API_Object_ToDetailString"
.LC1072:
	.string	"API_Object_ToInt32"
.LC1073:
	.string	"API_Object_ToInteger"
.LC1074:
	.string	"API_Object_ToNumber"
.LC1075:
	.string	"API_Object_ToObject"
.LC1076:
	.string	"API_Object_ToString"
.LC1077:
	.string	"API_Object_ToUint32"
.LC1078:
	.string	"API_Persistent_New"
.LC1079:
	.string	"API_Private_New"
.LC1080:
	.string	"API_Promise_Catch"
.LC1081:
	.string	"API_Promise_Chain"
.LC1082:
	.string	"API_Promise_HasRejectHandler"
.LC1083:
	.string	"API_Promise_Resolver_New"
.LC1084:
	.string	"API_Promise_Resolver_Reject"
.LC1085:
	.string	"API_Promise_Resolver_Resolve"
.LC1086:
	.string	"API_Promise_Result"
.LC1087:
	.string	"API_Promise_Status"
.LC1088:
	.string	"API_Promise_Then"
.LC1089:
	.string	"API_Proxy_New"
.LC1090:
	.string	"API_RangeError_New"
.LC1091:
	.string	"API_ReferenceError_New"
.LC1092:
	.string	"API_RegExp_New"
.LC1093:
	.string	"API_ScriptCompiler_Compile"
	.section	.rodata.str1.8
	.align 8
.LC1094:
	.string	"API_ScriptCompiler_CompileFunctionInContext"
	.align 8
.LC1095:
	.string	"API_ScriptCompiler_CompileUnbound"
	.section	.rodata.str1.1
.LC1096:
	.string	"API_Script_Run"
.LC1097:
	.string	"API_Set_Add"
.LC1098:
	.string	"API_Set_AsArray"
.LC1099:
	.string	"API_Set_Clear"
.LC1100:
	.string	"API_Set_Delete"
.LC1101:
	.string	"API_Set_Has"
.LC1102:
	.string	"API_Set_New"
.LC1103:
	.string	"API_SharedArrayBuffer_New"
.LC1104:
	.string	"API_String_Concat"
.LC1105:
	.string	"API_String_NewExternalOneByte"
.LC1106:
	.string	"API_String_NewExternalTwoByte"
.LC1107:
	.string	"API_String_NewFromOneByte"
.LC1108:
	.string	"API_String_NewFromTwoByte"
.LC1109:
	.string	"API_String_NewFromUtf8"
.LC1110:
	.string	"API_StringObject_New"
.LC1111:
	.string	"API_StringObject_StringValue"
.LC1112:
	.string	"API_String_Write"
.LC1113:
	.string	"API_String_WriteUtf8"
.LC1114:
	.string	"API_Symbol_New"
.LC1115:
	.string	"API_SymbolObject_New"
.LC1116:
	.string	"API_SymbolObject_SymbolValue"
.LC1117:
	.string	"API_SyntaxError_New"
.LC1118:
	.string	"API_TracedGlobal_New"
.LC1119:
	.string	"API_TryCatch_StackTrace"
.LC1120:
	.string	"API_TypeError_New"
.LC1121:
	.string	"API_Uint16Array_New"
.LC1122:
	.string	"API_Uint32Array_New"
.LC1123:
	.string	"API_Uint8Array_New"
.LC1124:
	.string	"API_Uint8ClampedArray_New"
.LC1125:
	.string	"API_UnboundScript_GetId"
	.section	.rodata.str1.8
	.align 8
.LC1126:
	.string	"API_UnboundScript_GetLineNumber"
	.section	.rodata.str1.1
.LC1127:
	.string	"API_UnboundScript_GetName"
	.section	.rodata.str1.8
	.align 8
.LC1128:
	.string	"API_UnboundScript_GetSourceMappingURL"
	.align 8
.LC1129:
	.string	"API_UnboundScript_GetSourceURL"
	.align 8
.LC1130:
	.string	"API_ValueDeserializer_ReadHeader"
	.align 8
.LC1131:
	.string	"API_ValueDeserializer_ReadValue"
	.align 8
.LC1132:
	.string	"API_ValueSerializer_WriteValue"
	.section	.rodata.str1.1
.LC1133:
	.string	"API_Value_InstanceOf"
.LC1134:
	.string	"API_Value_Int32Value"
.LC1135:
	.string	"API_Value_IntegerValue"
.LC1136:
	.string	"API_Value_NumberValue"
.LC1137:
	.string	"API_Value_TypeOf"
.LC1138:
	.string	"API_Value_Uint32Value"
.LC1139:
	.string	"API_WeakMap_Get"
.LC1140:
	.string	"API_WeakMap_New"
.LC1141:
	.string	"API_WeakMap_Set"
	.section	.rodata.str1.8
	.align 8
.LC1142:
	.string	"KeyedLoadIC_KeyedLoadSloppyArgumentsStub"
	.section	.rodata.str1.1
.LC1143:
	.string	"KeyedLoadIC_LoadElementDH"
	.section	.rodata.str1.8
	.align 8
.LC1144:
	.string	"KeyedLoadIC_LoadIndexedInterceptorStub"
	.align 8
.LC1145:
	.string	"KeyedLoadIC_LoadIndexedStringDH"
	.section	.rodata.str1.1
.LC1146:
	.string	"KeyedLoadIC_SlowStub"
	.section	.rodata.str1.8
	.align 8
.LC1147:
	.string	"KeyedStoreIC_ElementsTransitionAndStoreStub"
	.align 8
.LC1148:
	.string	"KeyedStoreIC_KeyedStoreSloppyArgumentsStub"
	.section	.rodata.str1.1
.LC1149:
	.string	"KeyedStoreIC_SlowStub"
.LC1150:
	.string	"KeyedStoreIC_StoreElementStub"
	.section	.rodata.str1.8
	.align 8
.LC1151:
	.string	"KeyedStoreIC_StoreFastElementStub"
	.align 8
.LC1152:
	.string	"LoadGlobalIC_LoadScriptContextField"
	.section	.rodata.str1.1
.LC1153:
	.string	"LoadGlobalIC_SlowStub"
.LC1154:
	.string	"LoadIC_FunctionPrototypeStub"
	.section	.rodata.str1.8
	.align 8
.LC1155:
	.string	"LoadIC_HandlerCacheHit_Accessor"
	.section	.rodata.str1.1
.LC1156:
	.string	"LoadIC_LoadAccessorDH"
	.section	.rodata.str1.8
	.align 8
.LC1157:
	.string	"LoadIC_LoadAccessorFromPrototypeDH"
	.align 8
.LC1158:
	.string	"LoadIC_LoadApiGetterFromPrototypeDH"
	.section	.rodata.str1.1
.LC1159:
	.string	"LoadIC_LoadCallback"
.LC1160:
	.string	"LoadIC_LoadConstantDH"
	.section	.rodata.str1.8
	.align 8
.LC1161:
	.string	"LoadIC_LoadConstantFromPrototypeDH"
	.section	.rodata.str1.1
.LC1162:
	.string	"LoadIC_LoadFieldDH"
	.section	.rodata.str1.8
	.align 8
.LC1163:
	.string	"LoadIC_LoadFieldFromPrototypeDH"
	.section	.rodata.str1.1
.LC1164:
	.string	"LoadIC_LoadGlobalDH"
	.section	.rodata.str1.8
	.align 8
.LC1165:
	.string	"LoadIC_LoadGlobalFromPrototypeDH"
	.align 8
.LC1166:
	.string	"LoadIC_LoadIntegerIndexedExoticDH"
	.section	.rodata.str1.1
.LC1167:
	.string	"LoadIC_LoadInterceptorDH"
	.section	.rodata.str1.8
	.align 8
.LC1168:
	.string	"LoadIC_LoadInterceptorFromPrototypeDH"
	.align 8
.LC1169:
	.string	"LoadIC_LoadNativeDataPropertyDH"
	.align 8
.LC1170:
	.string	"LoadIC_LoadNativeDataPropertyFromPrototypeDH"
	.section	.rodata.str1.1
.LC1171:
	.string	"LoadIC_LoadNonexistentDH"
	.section	.rodata.str1.8
	.align 8
.LC1172:
	.string	"LoadIC_LoadNonMaskingInterceptorDH"
	.section	.rodata.str1.1
.LC1173:
	.string	"LoadIC_LoadNormalDH"
	.section	.rodata.str1.8
	.align 8
.LC1174:
	.string	"LoadIC_LoadNormalFromPrototypeDH"
	.section	.rodata.str1.1
.LC1175:
	.string	"LoadIC_NonReceiver"
.LC1176:
	.string	"LoadIC_Premonomorphic"
.LC1177:
	.string	"LoadIC_SlowStub"
.LC1178:
	.string	"LoadIC_StringLength"
.LC1179:
	.string	"LoadIC_StringWrapperLength"
.LC1180:
	.string	"StoreGlobalIC_SlowStub"
	.section	.rodata.str1.8
	.align 8
.LC1181:
	.string	"StoreGlobalIC_StoreScriptContextField"
	.section	.rodata.str1.1
.LC1182:
	.string	"StoreGlobalIC_Premonomorphic"
	.section	.rodata.str1.8
	.align 8
.LC1183:
	.string	"StoreIC_HandlerCacheHit_Accessor"
	.section	.rodata.str1.1
.LC1184:
	.string	"StoreIC_NonReceiver"
.LC1185:
	.string	"StoreIC_Premonomorphic"
.LC1186:
	.string	"StoreIC_SlowStub"
.LC1187:
	.string	"StoreIC_StoreAccessorDH"
	.section	.rodata.str1.8
	.align 8
.LC1188:
	.string	"StoreIC_StoreAccessorOnPrototypeDH"
	.align 8
.LC1189:
	.string	"StoreIC_StoreApiSetterOnPrototypeDH"
	.section	.rodata.str1.1
.LC1190:
	.string	"StoreIC_StoreFieldDH"
.LC1191:
	.string	"StoreIC_StoreGlobalDH"
	.section	.rodata.str1.8
	.align 8
.LC1192:
	.string	"StoreIC_StoreGlobalTransitionDH"
	.section	.rodata.str1.1
.LC1193:
	.string	"StoreIC_StoreInterceptorStub"
	.section	.rodata.str1.8
	.align 8
.LC1194:
	.string	"StoreIC_StoreNativeDataPropertyDH"
	.align 8
.LC1195:
	.string	"StoreIC_StoreNativeDataPropertyOnPrototypeDH"
	.section	.rodata.str1.1
.LC1196:
	.string	"StoreIC_StoreNormalDH"
.LC1197:
	.string	"StoreIC_StoreTransitionDH"
	.section	.rodata.str1.8
	.align 8
.LC1198:
	.string	"StoreInArrayLiteralIC_SlowStub"
	.section	.data.rel.ro.local._ZZN2v88internal16RuntimeCallStatsC4EvE6kNames,"aw"
	.align 32
	.type	_ZZN2v88internal16RuntimeCallStatsC4EvE6kNames, @object
	.size	_ZZN2v88internal16RuntimeCallStatsC4EvE6kNames, 9208
_ZZN2v88internal16RuntimeCallStatsC4EvE6kNames:
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.quad	.LC71
	.quad	.LC72
	.quad	.LC73
	.quad	.LC74
	.quad	.LC75
	.quad	.LC76
	.quad	.LC77
	.quad	.LC78
	.quad	.LC79
	.quad	.LC80
	.quad	.LC81
	.quad	.LC82
	.quad	.LC83
	.quad	.LC84
	.quad	.LC85
	.quad	.LC86
	.quad	.LC87
	.quad	.LC88
	.quad	.LC89
	.quad	.LC90
	.quad	.LC91
	.quad	.LC92
	.quad	.LC93
	.quad	.LC94
	.quad	.LC95
	.quad	.LC96
	.quad	.LC97
	.quad	.LC98
	.quad	.LC99
	.quad	.LC100
	.quad	.LC101
	.quad	.LC102
	.quad	.LC103
	.quad	.LC104
	.quad	.LC105
	.quad	.LC106
	.quad	.LC107
	.quad	.LC108
	.quad	.LC109
	.quad	.LC110
	.quad	.LC111
	.quad	.LC112
	.quad	.LC113
	.quad	.LC114
	.quad	.LC115
	.quad	.LC116
	.quad	.LC117
	.quad	.LC118
	.quad	.LC119
	.quad	.LC120
	.quad	.LC121
	.quad	.LC122
	.quad	.LC123
	.quad	.LC124
	.quad	.LC125
	.quad	.LC126
	.quad	.LC127
	.quad	.LC128
	.quad	.LC129
	.quad	.LC130
	.quad	.LC131
	.quad	.LC132
	.quad	.LC133
	.quad	.LC134
	.quad	.LC135
	.quad	.LC136
	.quad	.LC137
	.quad	.LC138
	.quad	.LC139
	.quad	.LC140
	.quad	.LC141
	.quad	.LC142
	.quad	.LC143
	.quad	.LC144
	.quad	.LC145
	.quad	.LC146
	.quad	.LC147
	.quad	.LC148
	.quad	.LC149
	.quad	.LC150
	.quad	.LC151
	.quad	.LC152
	.quad	.LC153
	.quad	.LC154
	.quad	.LC155
	.quad	.LC156
	.quad	.LC157
	.quad	.LC158
	.quad	.LC159
	.quad	.LC160
	.quad	.LC161
	.quad	.LC162
	.quad	.LC163
	.quad	.LC164
	.quad	.LC165
	.quad	.LC166
	.quad	.LC167
	.quad	.LC168
	.quad	.LC169
	.quad	.LC170
	.quad	.LC171
	.quad	.LC172
	.quad	.LC173
	.quad	.LC174
	.quad	.LC175
	.quad	.LC176
	.quad	.LC177
	.quad	.LC178
	.quad	.LC179
	.quad	.LC180
	.quad	.LC181
	.quad	.LC182
	.quad	.LC183
	.quad	.LC184
	.quad	.LC185
	.quad	.LC186
	.quad	.LC187
	.quad	.LC188
	.quad	.LC189
	.quad	.LC190
	.quad	.LC191
	.quad	.LC192
	.quad	.LC193
	.quad	.LC194
	.quad	.LC195
	.quad	.LC196
	.quad	.LC197
	.quad	.LC198
	.quad	.LC199
	.quad	.LC200
	.quad	.LC201
	.quad	.LC202
	.quad	.LC203
	.quad	.LC204
	.quad	.LC205
	.quad	.LC206
	.quad	.LC207
	.quad	.LC208
	.quad	.LC209
	.quad	.LC210
	.quad	.LC211
	.quad	.LC212
	.quad	.LC213
	.quad	.LC214
	.quad	.LC215
	.quad	.LC216
	.quad	.LC217
	.quad	.LC218
	.quad	.LC219
	.quad	.LC220
	.quad	.LC221
	.quad	.LC222
	.quad	.LC223
	.quad	.LC224
	.quad	.LC225
	.quad	.LC226
	.quad	.LC227
	.quad	.LC228
	.quad	.LC229
	.quad	.LC230
	.quad	.LC231
	.quad	.LC232
	.quad	.LC233
	.quad	.LC234
	.quad	.LC235
	.quad	.LC236
	.quad	.LC237
	.quad	.LC238
	.quad	.LC239
	.quad	.LC240
	.quad	.LC241
	.quad	.LC242
	.quad	.LC243
	.quad	.LC244
	.quad	.LC245
	.quad	.LC246
	.quad	.LC247
	.quad	.LC248
	.quad	.LC249
	.quad	.LC250
	.quad	.LC251
	.quad	.LC252
	.quad	.LC253
	.quad	.LC254
	.quad	.LC255
	.quad	.LC256
	.quad	.LC257
	.quad	.LC258
	.quad	.LC259
	.quad	.LC260
	.quad	.LC261
	.quad	.LC262
	.quad	.LC263
	.quad	.LC264
	.quad	.LC265
	.quad	.LC266
	.quad	.LC267
	.quad	.LC268
	.quad	.LC269
	.quad	.LC270
	.quad	.LC271
	.quad	.LC272
	.quad	.LC273
	.quad	.LC274
	.quad	.LC275
	.quad	.LC276
	.quad	.LC277
	.quad	.LC278
	.quad	.LC279
	.quad	.LC280
	.quad	.LC281
	.quad	.LC282
	.quad	.LC283
	.quad	.LC284
	.quad	.LC285
	.quad	.LC286
	.quad	.LC287
	.quad	.LC288
	.quad	.LC289
	.quad	.LC290
	.quad	.LC291
	.quad	.LC292
	.quad	.LC293
	.quad	.LC294
	.quad	.LC295
	.quad	.LC296
	.quad	.LC297
	.quad	.LC298
	.quad	.LC299
	.quad	.LC300
	.quad	.LC301
	.quad	.LC302
	.quad	.LC303
	.quad	.LC304
	.quad	.LC305
	.quad	.LC306
	.quad	.LC307
	.quad	.LC308
	.quad	.LC309
	.quad	.LC310
	.quad	.LC311
	.quad	.LC312
	.quad	.LC313
	.quad	.LC314
	.quad	.LC315
	.quad	.LC316
	.quad	.LC317
	.quad	.LC318
	.quad	.LC319
	.quad	.LC320
	.quad	.LC321
	.quad	.LC322
	.quad	.LC323
	.quad	.LC324
	.quad	.LC325
	.quad	.LC326
	.quad	.LC327
	.quad	.LC328
	.quad	.LC329
	.quad	.LC330
	.quad	.LC331
	.quad	.LC332
	.quad	.LC333
	.quad	.LC334
	.quad	.LC335
	.quad	.LC336
	.quad	.LC337
	.quad	.LC338
	.quad	.LC339
	.quad	.LC340
	.quad	.LC341
	.quad	.LC342
	.quad	.LC343
	.quad	.LC344
	.quad	.LC345
	.quad	.LC346
	.quad	.LC347
	.quad	.LC348
	.quad	.LC349
	.quad	.LC350
	.quad	.LC351
	.quad	.LC352
	.quad	.LC353
	.quad	.LC354
	.quad	.LC355
	.quad	.LC356
	.quad	.LC357
	.quad	.LC358
	.quad	.LC359
	.quad	.LC360
	.quad	.LC361
	.quad	.LC362
	.quad	.LC363
	.quad	.LC364
	.quad	.LC365
	.quad	.LC366
	.quad	.LC367
	.quad	.LC368
	.quad	.LC369
	.quad	.LC370
	.quad	.LC371
	.quad	.LC372
	.quad	.LC373
	.quad	.LC374
	.quad	.LC375
	.quad	.LC376
	.quad	.LC377
	.quad	.LC378
	.quad	.LC379
	.quad	.LC380
	.quad	.LC381
	.quad	.LC382
	.quad	.LC383
	.quad	.LC384
	.quad	.LC385
	.quad	.LC386
	.quad	.LC387
	.quad	.LC388
	.quad	.LC389
	.quad	.LC390
	.quad	.LC391
	.quad	.LC392
	.quad	.LC393
	.quad	.LC394
	.quad	.LC395
	.quad	.LC396
	.quad	.LC397
	.quad	.LC398
	.quad	.LC399
	.quad	.LC400
	.quad	.LC401
	.quad	.LC402
	.quad	.LC403
	.quad	.LC404
	.quad	.LC405
	.quad	.LC406
	.quad	.LC407
	.quad	.LC408
	.quad	.LC409
	.quad	.LC410
	.quad	.LC411
	.quad	.LC412
	.quad	.LC413
	.quad	.LC414
	.quad	.LC415
	.quad	.LC416
	.quad	.LC417
	.quad	.LC418
	.quad	.LC419
	.quad	.LC420
	.quad	.LC421
	.quad	.LC422
	.quad	.LC423
	.quad	.LC424
	.quad	.LC425
	.quad	.LC426
	.quad	.LC427
	.quad	.LC428
	.quad	.LC429
	.quad	.LC430
	.quad	.LC431
	.quad	.LC432
	.quad	.LC433
	.quad	.LC434
	.quad	.LC435
	.quad	.LC436
	.quad	.LC437
	.quad	.LC438
	.quad	.LC439
	.quad	.LC440
	.quad	.LC441
	.quad	.LC442
	.quad	.LC443
	.quad	.LC444
	.quad	.LC445
	.quad	.LC446
	.quad	.LC447
	.quad	.LC448
	.quad	.LC449
	.quad	.LC450
	.quad	.LC451
	.quad	.LC452
	.quad	.LC453
	.quad	.LC454
	.quad	.LC455
	.quad	.LC456
	.quad	.LC457
	.quad	.LC458
	.quad	.LC459
	.quad	.LC460
	.quad	.LC461
	.quad	.LC462
	.quad	.LC463
	.quad	.LC464
	.quad	.LC465
	.quad	.LC466
	.quad	.LC467
	.quad	.LC468
	.quad	.LC469
	.quad	.LC470
	.quad	.LC471
	.quad	.LC472
	.quad	.LC473
	.quad	.LC474
	.quad	.LC475
	.quad	.LC476
	.quad	.LC477
	.quad	.LC478
	.quad	.LC479
	.quad	.LC480
	.quad	.LC481
	.quad	.LC482
	.quad	.LC483
	.quad	.LC484
	.quad	.LC485
	.quad	.LC486
	.quad	.LC487
	.quad	.LC488
	.quad	.LC489
	.quad	.LC490
	.quad	.LC491
	.quad	.LC492
	.quad	.LC493
	.quad	.LC494
	.quad	.LC495
	.quad	.LC496
	.quad	.LC497
	.quad	.LC498
	.quad	.LC499
	.quad	.LC500
	.quad	.LC501
	.quad	.LC502
	.quad	.LC503
	.quad	.LC504
	.quad	.LC505
	.quad	.LC506
	.quad	.LC507
	.quad	.LC508
	.quad	.LC509
	.quad	.LC510
	.quad	.LC511
	.quad	.LC512
	.quad	.LC513
	.quad	.LC514
	.quad	.LC515
	.quad	.LC516
	.quad	.LC517
	.quad	.LC518
	.quad	.LC519
	.quad	.LC520
	.quad	.LC521
	.quad	.LC522
	.quad	.LC523
	.quad	.LC524
	.quad	.LC525
	.quad	.LC526
	.quad	.LC527
	.quad	.LC528
	.quad	.LC529
	.quad	.LC530
	.quad	.LC531
	.quad	.LC532
	.quad	.LC533
	.quad	.LC534
	.quad	.LC535
	.quad	.LC536
	.quad	.LC537
	.quad	.LC538
	.quad	.LC539
	.quad	.LC540
	.quad	.LC541
	.quad	.LC542
	.quad	.LC543
	.quad	.LC544
	.quad	.LC545
	.quad	.LC546
	.quad	.LC547
	.quad	.LC548
	.quad	.LC549
	.quad	.LC550
	.quad	.LC551
	.quad	.LC552
	.quad	.LC553
	.quad	.LC554
	.quad	.LC555
	.quad	.LC556
	.quad	.LC557
	.quad	.LC558
	.quad	.LC559
	.quad	.LC560
	.quad	.LC561
	.quad	.LC562
	.quad	.LC563
	.quad	.LC564
	.quad	.LC565
	.quad	.LC566
	.quad	.LC567
	.quad	.LC568
	.quad	.LC569
	.quad	.LC570
	.quad	.LC571
	.quad	.LC572
	.quad	.LC573
	.quad	.LC574
	.quad	.LC575
	.quad	.LC576
	.quad	.LC577
	.quad	.LC578
	.quad	.LC579
	.quad	.LC580
	.quad	.LC581
	.quad	.LC582
	.quad	.LC583
	.quad	.LC584
	.quad	.LC585
	.quad	.LC586
	.quad	.LC587
	.quad	.LC588
	.quad	.LC589
	.quad	.LC590
	.quad	.LC591
	.quad	.LC592
	.quad	.LC593
	.quad	.LC594
	.quad	.LC595
	.quad	.LC596
	.quad	.LC597
	.quad	.LC598
	.quad	.LC599
	.quad	.LC600
	.quad	.LC601
	.quad	.LC602
	.quad	.LC603
	.quad	.LC604
	.quad	.LC605
	.quad	.LC606
	.quad	.LC607
	.quad	.LC608
	.quad	.LC609
	.quad	.LC610
	.quad	.LC611
	.quad	.LC612
	.quad	.LC613
	.quad	.LC614
	.quad	.LC615
	.quad	.LC616
	.quad	.LC617
	.quad	.LC618
	.quad	.LC619
	.quad	.LC620
	.quad	.LC621
	.quad	.LC622
	.quad	.LC623
	.quad	.LC624
	.quad	.LC625
	.quad	.LC626
	.quad	.LC627
	.quad	.LC628
	.quad	.LC629
	.quad	.LC630
	.quad	.LC631
	.quad	.LC632
	.quad	.LC633
	.quad	.LC634
	.quad	.LC635
	.quad	.LC636
	.quad	.LC637
	.quad	.LC638
	.quad	.LC639
	.quad	.LC640
	.quad	.LC641
	.quad	.LC642
	.quad	.LC643
	.quad	.LC644
	.quad	.LC645
	.quad	.LC646
	.quad	.LC647
	.quad	.LC648
	.quad	.LC649
	.quad	.LC650
	.quad	.LC651
	.quad	.LC652
	.quad	.LC653
	.quad	.LC654
	.quad	.LC655
	.quad	.LC656
	.quad	.LC657
	.quad	.LC658
	.quad	.LC659
	.quad	.LC660
	.quad	.LC661
	.quad	.LC662
	.quad	.LC663
	.quad	.LC664
	.quad	.LC665
	.quad	.LC666
	.quad	.LC667
	.quad	.LC668
	.quad	.LC669
	.quad	.LC670
	.quad	.LC671
	.quad	.LC672
	.quad	.LC673
	.quad	.LC674
	.quad	.LC675
	.quad	.LC676
	.quad	.LC677
	.quad	.LC678
	.quad	.LC679
	.quad	.LC680
	.quad	.LC681
	.quad	.LC682
	.quad	.LC683
	.quad	.LC684
	.quad	.LC685
	.quad	.LC686
	.quad	.LC687
	.quad	.LC688
	.quad	.LC689
	.quad	.LC690
	.quad	.LC691
	.quad	.LC692
	.quad	.LC693
	.quad	.LC694
	.quad	.LC695
	.quad	.LC696
	.quad	.LC697
	.quad	.LC698
	.quad	.LC699
	.quad	.LC700
	.quad	.LC701
	.quad	.LC702
	.quad	.LC703
	.quad	.LC704
	.quad	.LC705
	.quad	.LC706
	.quad	.LC707
	.quad	.LC708
	.quad	.LC709
	.quad	.LC710
	.quad	.LC711
	.quad	.LC712
	.quad	.LC713
	.quad	.LC714
	.quad	.LC715
	.quad	.LC716
	.quad	.LC717
	.quad	.LC718
	.quad	.LC719
	.quad	.LC720
	.quad	.LC721
	.quad	.LC722
	.quad	.LC723
	.quad	.LC724
	.quad	.LC725
	.quad	.LC726
	.quad	.LC727
	.quad	.LC728
	.quad	.LC729
	.quad	.LC730
	.quad	.LC731
	.quad	.LC732
	.quad	.LC733
	.quad	.LC734
	.quad	.LC735
	.quad	.LC736
	.quad	.LC737
	.quad	.LC738
	.quad	.LC739
	.quad	.LC740
	.quad	.LC741
	.quad	.LC742
	.quad	.LC743
	.quad	.LC744
	.quad	.LC745
	.quad	.LC746
	.quad	.LC747
	.quad	.LC748
	.quad	.LC749
	.quad	.LC750
	.quad	.LC751
	.quad	.LC752
	.quad	.LC753
	.quad	.LC754
	.quad	.LC755
	.quad	.LC756
	.quad	.LC757
	.quad	.LC758
	.quad	.LC759
	.quad	.LC760
	.quad	.LC761
	.quad	.LC762
	.quad	.LC763
	.quad	.LC764
	.quad	.LC765
	.quad	.LC766
	.quad	.LC767
	.quad	.LC768
	.quad	.LC769
	.quad	.LC770
	.quad	.LC771
	.quad	.LC772
	.quad	.LC773
	.quad	.LC774
	.quad	.LC775
	.quad	.LC776
	.quad	.LC777
	.quad	.LC778
	.quad	.LC779
	.quad	.LC780
	.quad	.LC781
	.quad	.LC782
	.quad	.LC783
	.quad	.LC784
	.quad	.LC785
	.quad	.LC786
	.quad	.LC787
	.quad	.LC788
	.quad	.LC789
	.quad	.LC790
	.quad	.LC791
	.quad	.LC792
	.quad	.LC793
	.quad	.LC794
	.quad	.LC795
	.quad	.LC796
	.quad	.LC797
	.quad	.LC798
	.quad	.LC799
	.quad	.LC800
	.quad	.LC801
	.quad	.LC802
	.quad	.LC803
	.quad	.LC804
	.quad	.LC805
	.quad	.LC806
	.quad	.LC807
	.quad	.LC808
	.quad	.LC809
	.quad	.LC810
	.quad	.LC811
	.quad	.LC812
	.quad	.LC813
	.quad	.LC814
	.quad	.LC815
	.quad	.LC816
	.quad	.LC817
	.quad	.LC818
	.quad	.LC819
	.quad	.LC820
	.quad	.LC821
	.quad	.LC822
	.quad	.LC823
	.quad	.LC824
	.quad	.LC825
	.quad	.LC826
	.quad	.LC827
	.quad	.LC828
	.quad	.LC829
	.quad	.LC830
	.quad	.LC831
	.quad	.LC832
	.quad	.LC833
	.quad	.LC834
	.quad	.LC835
	.quad	.LC836
	.quad	.LC837
	.quad	.LC838
	.quad	.LC839
	.quad	.LC840
	.quad	.LC841
	.quad	.LC842
	.quad	.LC843
	.quad	.LC844
	.quad	.LC845
	.quad	.LC846
	.quad	.LC847
	.quad	.LC848
	.quad	.LC849
	.quad	.LC850
	.quad	.LC851
	.quad	.LC852
	.quad	.LC853
	.quad	.LC854
	.quad	.LC855
	.quad	.LC856
	.quad	.LC857
	.quad	.LC858
	.quad	.LC859
	.quad	.LC860
	.quad	.LC861
	.quad	.LC862
	.quad	.LC863
	.quad	.LC864
	.quad	.LC865
	.quad	.LC866
	.quad	.LC867
	.quad	.LC868
	.quad	.LC869
	.quad	.LC870
	.quad	.LC871
	.quad	.LC872
	.quad	.LC873
	.quad	.LC874
	.quad	.LC875
	.quad	.LC876
	.quad	.LC877
	.quad	.LC878
	.quad	.LC879
	.quad	.LC880
	.quad	.LC881
	.quad	.LC882
	.quad	.LC883
	.quad	.LC884
	.quad	.LC885
	.quad	.LC886
	.quad	.LC887
	.quad	.LC888
	.quad	.LC889
	.quad	.LC890
	.quad	.LC891
	.quad	.LC892
	.quad	.LC893
	.quad	.LC894
	.quad	.LC895
	.quad	.LC896
	.quad	.LC897
	.quad	.LC898
	.quad	.LC899
	.quad	.LC900
	.quad	.LC901
	.quad	.LC902
	.quad	.LC903
	.quad	.LC904
	.quad	.LC905
	.quad	.LC906
	.quad	.LC907
	.quad	.LC908
	.quad	.LC909
	.quad	.LC910
	.quad	.LC911
	.quad	.LC912
	.quad	.LC913
	.quad	.LC914
	.quad	.LC915
	.quad	.LC916
	.quad	.LC917
	.quad	.LC918
	.quad	.LC919
	.quad	.LC920
	.quad	.LC921
	.quad	.LC922
	.quad	.LC923
	.quad	.LC924
	.quad	.LC925
	.quad	.LC926
	.quad	.LC927
	.quad	.LC928
	.quad	.LC929
	.quad	.LC930
	.quad	.LC931
	.quad	.LC932
	.quad	.LC933
	.quad	.LC934
	.quad	.LC935
	.quad	.LC936
	.quad	.LC937
	.quad	.LC938
	.quad	.LC939
	.quad	.LC940
	.quad	.LC941
	.quad	.LC942
	.quad	.LC943
	.quad	.LC944
	.quad	.LC945
	.quad	.LC946
	.quad	.LC947
	.quad	.LC948
	.quad	.LC949
	.quad	.LC950
	.quad	.LC951
	.quad	.LC952
	.quad	.LC953
	.quad	.LC954
	.quad	.LC955
	.quad	.LC956
	.quad	.LC957
	.quad	.LC958
	.quad	.LC959
	.quad	.LC960
	.quad	.LC961
	.quad	.LC962
	.quad	.LC963
	.quad	.LC964
	.quad	.LC965
	.quad	.LC966
	.quad	.LC967
	.quad	.LC968
	.quad	.LC969
	.quad	.LC970
	.quad	.LC971
	.quad	.LC972
	.quad	.LC973
	.quad	.LC974
	.quad	.LC975
	.quad	.LC976
	.quad	.LC977
	.quad	.LC978
	.quad	.LC979
	.quad	.LC980
	.quad	.LC981
	.quad	.LC982
	.quad	.LC983
	.quad	.LC984
	.quad	.LC985
	.quad	.LC986
	.quad	.LC987
	.quad	.LC988
	.quad	.LC989
	.quad	.LC990
	.quad	.LC991
	.quad	.LC992
	.quad	.LC993
	.quad	.LC994
	.quad	.LC995
	.quad	.LC996
	.quad	.LC997
	.quad	.LC998
	.quad	.LC999
	.quad	.LC1000
	.quad	.LC1001
	.quad	.LC1002
	.quad	.LC1003
	.quad	.LC1004
	.quad	.LC1005
	.quad	.LC1006
	.quad	.LC1007
	.quad	.LC1008
	.quad	.LC1009
	.quad	.LC1010
	.quad	.LC1011
	.quad	.LC1012
	.quad	.LC1013
	.quad	.LC1014
	.quad	.LC1015
	.quad	.LC1016
	.quad	.LC1017
	.quad	.LC1018
	.quad	.LC1019
	.quad	.LC1020
	.quad	.LC1021
	.quad	.LC1022
	.quad	.LC1023
	.quad	.LC1024
	.quad	.LC1025
	.quad	.LC1026
	.quad	.LC1027
	.quad	.LC1028
	.quad	.LC1029
	.quad	.LC1030
	.quad	.LC1031
	.quad	.LC1032
	.quad	.LC1033
	.quad	.LC1034
	.quad	.LC1035
	.quad	.LC1036
	.quad	.LC1037
	.quad	.LC1038
	.quad	.LC1039
	.quad	.LC1040
	.quad	.LC1041
	.quad	.LC1042
	.quad	.LC1043
	.quad	.LC1044
	.quad	.LC1045
	.quad	.LC1046
	.quad	.LC1047
	.quad	.LC1048
	.quad	.LC1049
	.quad	.LC1050
	.quad	.LC1051
	.quad	.LC1052
	.quad	.LC1053
	.quad	.LC1054
	.quad	.LC1055
	.quad	.LC1056
	.quad	.LC1057
	.quad	.LC1058
	.quad	.LC1059
	.quad	.LC1060
	.quad	.LC1061
	.quad	.LC1062
	.quad	.LC1063
	.quad	.LC1064
	.quad	.LC1065
	.quad	.LC1066
	.quad	.LC1067
	.quad	.LC1068
	.quad	.LC1069
	.quad	.LC1070
	.quad	.LC1071
	.quad	.LC1072
	.quad	.LC1073
	.quad	.LC1074
	.quad	.LC1075
	.quad	.LC1076
	.quad	.LC1077
	.quad	.LC1078
	.quad	.LC1079
	.quad	.LC1080
	.quad	.LC1081
	.quad	.LC1082
	.quad	.LC1083
	.quad	.LC1084
	.quad	.LC1085
	.quad	.LC1086
	.quad	.LC1087
	.quad	.LC1088
	.quad	.LC1089
	.quad	.LC1090
	.quad	.LC1091
	.quad	.LC1092
	.quad	.LC1093
	.quad	.LC1094
	.quad	.LC1095
	.quad	.LC1096
	.quad	.LC1097
	.quad	.LC1098
	.quad	.LC1099
	.quad	.LC1100
	.quad	.LC1101
	.quad	.LC1102
	.quad	.LC1103
	.quad	.LC1104
	.quad	.LC1105
	.quad	.LC1106
	.quad	.LC1107
	.quad	.LC1108
	.quad	.LC1109
	.quad	.LC1110
	.quad	.LC1111
	.quad	.LC1112
	.quad	.LC1113
	.quad	.LC1114
	.quad	.LC1115
	.quad	.LC1116
	.quad	.LC1117
	.quad	.LC1118
	.quad	.LC1119
	.quad	.LC1120
	.quad	.LC1121
	.quad	.LC1122
	.quad	.LC1123
	.quad	.LC1124
	.quad	.LC1125
	.quad	.LC1126
	.quad	.LC1127
	.quad	.LC1128
	.quad	.LC1129
	.quad	.LC1130
	.quad	.LC1131
	.quad	.LC1132
	.quad	.LC1133
	.quad	.LC1134
	.quad	.LC1135
	.quad	.LC1136
	.quad	.LC1137
	.quad	.LC1138
	.quad	.LC1139
	.quad	.LC1140
	.quad	.LC1141
	.quad	.LC1142
	.quad	.LC1143
	.quad	.LC1144
	.quad	.LC1145
	.quad	.LC1146
	.quad	.LC1147
	.quad	.LC1148
	.quad	.LC1149
	.quad	.LC1150
	.quad	.LC1151
	.quad	.LC1152
	.quad	.LC1153
	.quad	.LC1154
	.quad	.LC1155
	.quad	.LC1156
	.quad	.LC1157
	.quad	.LC1158
	.quad	.LC1159
	.quad	.LC1160
	.quad	.LC1161
	.quad	.LC1162
	.quad	.LC1163
	.quad	.LC1164
	.quad	.LC1165
	.quad	.LC1166
	.quad	.LC1167
	.quad	.LC1168
	.quad	.LC1169
	.quad	.LC1170
	.quad	.LC1171
	.quad	.LC1172
	.quad	.LC1173
	.quad	.LC1174
	.quad	.LC1175
	.quad	.LC1176
	.quad	.LC1177
	.quad	.LC1178
	.quad	.LC1179
	.quad	.LC1180
	.quad	.LC1181
	.quad	.LC1182
	.quad	.LC1183
	.quad	.LC1184
	.quad	.LC1185
	.quad	.LC1186
	.quad	.LC1187
	.quad	.LC1188
	.quad	.LC1189
	.quad	.LC1190
	.quad	.LC1191
	.quad	.LC1192
	.quad	.LC1193
	.quad	.LC1194
	.quad	.LC1195
	.quad	.LC1196
	.quad	.LC1197
	.quad	.LC1198
	.globl	_ZN2v88internal16RuntimeCallTimer3NowE
	.section	.data.rel._ZN2v88internal16RuntimeCallTimer3NowE,"aw"
	.align 8
	.type	_ZN2v88internal16RuntimeCallTimer3NowE, @object
	.size	_ZN2v88internal16RuntimeCallTimer3NowE, 8
_ZN2v88internal16RuntimeCallTimer3NowE:
	.quad	_ZN2v84base9TimeTicks17HighResolutionNowEv
	.section	.rodata.str1.1
.LC1199:
	.string	"c:V8.GlobalHandles"
.LC1200:
	.string	"c:V8.OsMemoryAllocated"
.LC1201:
	.string	"c:V8.MapsNormalized"
.LC1202:
	.string	"c:V8.MapsCreated"
	.section	.rodata.str1.8
	.align 8
.LC1203:
	.string	"c:V8.ObjectElementsTransitions"
	.align 8
.LC1204:
	.string	"c:V8.ObjectPropertiesToDictionary"
	.align 8
.LC1205:
	.string	"c:V8.ObjectElementsToDictionary"
	.section	.rodata.str1.1
.LC1206:
	.string	"c:V8.AliveAfterLastGC"
.LC1207:
	.string	"c:V8.ObjsSinceLastYoung"
.LC1208:
	.string	"c:V8.ObjsSinceLastFull"
.LC1209:
	.string	"c:V8.StringTableCapacity"
.LC1210:
	.string	"c:V8.NumberOfSymbols"
.LC1211:
	.string	"c:V8.InlinedCopiedElements"
.LC1212:
	.string	"c:V8.CompilationCacheHits"
.LC1213:
	.string	"c:V8.CompilationCacheMisses"
.LC1214:
	.string	"c:V8.TotalEvalSize"
.LC1215:
	.string	"c:V8.TotalLoadSize"
.LC1216:
	.string	"c:V8.TotalParseSize"
.LC1217:
	.string	"c:V8.TotalPreparseSkipped"
.LC1218:
	.string	"c:V8.TotalCompileSize"
	.section	.rodata.str1.8
	.align 8
.LC1219:
	.string	"c:V8.ContextsCreatedFromScratch"
	.align 8
.LC1220:
	.string	"c:V8.ContextsCreatedBySnapshot"
	.section	.rodata.str1.1
.LC1221:
	.string	"c:V8.PcToCode"
.LC1222:
	.string	"c:V8.PcToCodeCached"
.LC1223:
	.string	"c:V8.StoreBufferOverflows"
.LC1224:
	.string	"c:V8.TotalCompiledCodeSize"
	.section	.rodata.str1.8
	.align 8
.LC1225:
	.string	"c:V8.GCCompactorCausedByRequest"
	.align 8
.LC1226:
	.string	"c:V8.GCCompactorCausedByPromotedData"
	.align 8
.LC1227:
	.string	"c:V8.GCCompactorCausedByOldspaceExhaustion"
	.section	.rodata.str1.1
.LC1228:
	.string	"c:V8.GCLastResortFromJS"
.LC1229:
	.string	"c:V8.GCLastResortFromHandles"
.LC1230:
	.string	"c:V8.COWArraysConverted"
	.section	.rodata.str1.8
	.align 8
.LC1231:
	.string	"c:V8.ConstructedObjectsRuntime"
	.align 8
.LC1232:
	.string	"c:V8.MegamorphicStubCacheUpdates"
	.section	.rodata.str1.1
.LC1233:
	.string	"c:V8.EnumCacheHits"
.LC1234:
	.string	"c:V8.EnumCacheMisses"
.LC1235:
	.string	"c:V8.StringAddRuntime"
.LC1236:
	.string	"c:V8.SubStringRuntime"
.LC1237:
	.string	"c:V8.RegExpEntryRuntime"
.LC1238:
	.string	"c:V8.StackInterrupts"
.LC1239:
	.string	"c:V8.RuntimeProfilerTicks"
.LC1240:
	.string	"c:V8.SoftDeoptsExecuted"
	.section	.rodata.str1.8
	.align 8
.LC1241:
	.string	"c:V8.MemoryNewSpaceBytesAvailable"
	.align 8
.LC1242:
	.string	"c:V8.MemoryNewSpaceBytesCommitted"
	.section	.rodata.str1.1
.LC1243:
	.string	"c:V8.MemoryNewSpaceBytesUsed"
	.section	.rodata.str1.8
	.align 8
.LC1244:
	.string	"c:V8.MemoryOldSpaceBytesAvailable"
	.align 8
.LC1245:
	.string	"c:V8.MemoryOldSpaceBytesCommitted"
	.section	.rodata.str1.1
.LC1246:
	.string	"c:V8.MemoryOldSpaceBytesUsed"
	.section	.rodata.str1.8
	.align 8
.LC1247:
	.string	"c:V8.MemoryCodeSpaceBytesAvailable"
	.align 8
.LC1248:
	.string	"c:V8.MemoryCodeSpaceBytesCommitted"
	.section	.rodata.str1.1
.LC1249:
	.string	"c:V8.MemoryCodeSpaceBytesUsed"
	.section	.rodata.str1.8
	.align 8
.LC1250:
	.string	"c:V8.MemoryMapSpaceBytesAvailable"
	.align 8
.LC1251:
	.string	"c:V8.MemoryMapSpaceBytesCommitted"
	.section	.rodata.str1.1
.LC1252:
	.string	"c:V8.MemoryMapSpaceBytesUsed"
	.section	.rodata.str1.8
	.align 8
.LC1253:
	.string	"c:V8.MemoryLoSpaceBytesAvailable"
	.align 8
.LC1254:
	.string	"c:V8.MemoryLoSpaceBytesCommitted"
	.section	.rodata.str1.1
.LC1255:
	.string	"c:V8.MemoryLoSpaceBytesUsed"
.LC1256:
	.string	"c:V8.TotalBaselineCodeSize"
	.section	.rodata.str1.8
	.align 8
.LC1257:
	.string	"c:V8.TotalBaselineCompileCount"
	.section	.rodata.str1.1
.LC1258:
	.string	"c:V8.WriteBarriers"
.LC1259:
	.string	"c:V8.ConstructedObjects"
.LC1260:
	.string	"c:V8.FastNewClosureTotal"
.LC1261:
	.string	"c:V8.RegExpEntryNative"
.LC1262:
	.string	"c:V8.StringAddNative"
.LC1263:
	.string	"c:V8.SubStringNative"
.LC1264:
	.string	"c:V8.ICKeyedLoadGenericSmi"
.LC1265:
	.string	"c:V8.ICKeyedLoadGenericSymbol"
.LC1266:
	.string	"c:V8.ICKeyedLoadGenericSlow"
	.section	.rodata.str1.8
	.align 8
.LC1267:
	.string	"c:V8.MegamorphicStubCacheProbes"
	.align 8
.LC1268:
	.string	"c:V8.MegamorphicStubCacheMisses"
	.align 8
.LC1269:
	.string	"c:V8.CountOf_INTERNALIZED_STRING_TYPE"
	.align 8
.LC1270:
	.string	"c:V8.SizeOf_INTERNALIZED_STRING_TYPE"
	.align 8
.LC1271:
	.string	"c:V8.CountOf_EXTERNAL_INTERNALIZED_STRING_TYPE"
	.align 8
.LC1272:
	.string	"c:V8.SizeOf_EXTERNAL_INTERNALIZED_STRING_TYPE"
	.align 8
.LC1273:
	.string	"c:V8.CountOf_ONE_BYTE_INTERNALIZED_STRING_TYPE"
	.align 8
.LC1274:
	.string	"c:V8.SizeOf_ONE_BYTE_INTERNALIZED_STRING_TYPE"
	.align 8
.LC1275:
	.string	"c:V8.CountOf_EXTERNAL_ONE_BYTE_INTERNALIZED_STRING_TYPE"
	.align 8
.LC1276:
	.string	"c:V8.SizeOf_EXTERNAL_ONE_BYTE_INTERNALIZED_STRING_TYPE"
	.align 8
.LC1277:
	.string	"c:V8.CountOf_UNCACHED_EXTERNAL_INTERNALIZED_STRING_TYPE"
	.align 8
.LC1278:
	.string	"c:V8.SizeOf_UNCACHED_EXTERNAL_INTERNALIZED_STRING_TYPE"
	.align 8
.LC1279:
	.string	"c:V8.CountOf_UNCACHED_EXTERNAL_ONE_BYTE_INTERNALIZED_STRING_TYPE"
	.align 8
.LC1280:
	.string	"c:V8.SizeOf_UNCACHED_EXTERNAL_ONE_BYTE_INTERNALIZED_STRING_TYPE"
	.section	.rodata.str1.1
.LC1281:
	.string	"c:V8.CountOf_STRING_TYPE"
.LC1282:
	.string	"c:V8.SizeOf_STRING_TYPE"
.LC1283:
	.string	"c:V8.CountOf_CONS_STRING_TYPE"
.LC1284:
	.string	"c:V8.SizeOf_CONS_STRING_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1285:
	.string	"c:V8.CountOf_EXTERNAL_STRING_TYPE"
	.align 8
.LC1286:
	.string	"c:V8.SizeOf_EXTERNAL_STRING_TYPE"
	.align 8
.LC1287:
	.string	"c:V8.CountOf_SLICED_STRING_TYPE"
	.align 8
.LC1288:
	.string	"c:V8.SizeOf_SLICED_STRING_TYPE"
	.section	.rodata.str1.1
.LC1289:
	.string	"c:V8.CountOf_THIN_STRING_TYPE"
.LC1290:
	.string	"c:V8.SizeOf_THIN_STRING_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1291:
	.string	"c:V8.CountOf_ONE_BYTE_STRING_TYPE"
	.align 8
.LC1292:
	.string	"c:V8.SizeOf_ONE_BYTE_STRING_TYPE"
	.align 8
.LC1293:
	.string	"c:V8.CountOf_CONS_ONE_BYTE_STRING_TYPE"
	.align 8
.LC1294:
	.string	"c:V8.SizeOf_CONS_ONE_BYTE_STRING_TYPE"
	.align 8
.LC1295:
	.string	"c:V8.CountOf_EXTERNAL_ONE_BYTE_STRING_TYPE"
	.align 8
.LC1296:
	.string	"c:V8.SizeOf_EXTERNAL_ONE_BYTE_STRING_TYPE"
	.align 8
.LC1297:
	.string	"c:V8.CountOf_SLICED_ONE_BYTE_STRING_TYPE"
	.align 8
.LC1298:
	.string	"c:V8.SizeOf_SLICED_ONE_BYTE_STRING_TYPE"
	.align 8
.LC1299:
	.string	"c:V8.CountOf_THIN_ONE_BYTE_STRING_TYPE"
	.align 8
.LC1300:
	.string	"c:V8.SizeOf_THIN_ONE_BYTE_STRING_TYPE"
	.align 8
.LC1301:
	.string	"c:V8.CountOf_UNCACHED_EXTERNAL_STRING_TYPE"
	.align 8
.LC1302:
	.string	"c:V8.SizeOf_UNCACHED_EXTERNAL_STRING_TYPE"
	.align 8
.LC1303:
	.string	"c:V8.CountOf_UNCACHED_EXTERNAL_ONE_BYTE_STRING_TYPE"
	.align 8
.LC1304:
	.string	"c:V8.SizeOf_UNCACHED_EXTERNAL_ONE_BYTE_STRING_TYPE"
	.section	.rodata.str1.1
.LC1305:
	.string	"c:V8.CountOf_SYMBOL_TYPE"
.LC1306:
	.string	"c:V8.SizeOf_SYMBOL_TYPE"
.LC1307:
	.string	"c:V8.CountOf_HEAP_NUMBER_TYPE"
.LC1308:
	.string	"c:V8.SizeOf_HEAP_NUMBER_TYPE"
.LC1309:
	.string	"c:V8.CountOf_BIGINT_TYPE"
.LC1310:
	.string	"c:V8.SizeOf_BIGINT_TYPE"
.LC1311:
	.string	"c:V8.CountOf_ODDBALL_TYPE"
.LC1312:
	.string	"c:V8.SizeOf_ODDBALL_TYPE"
.LC1313:
	.string	"c:V8.CountOf_MAP_TYPE"
.LC1314:
	.string	"c:V8.SizeOf_MAP_TYPE"
.LC1315:
	.string	"c:V8.CountOf_CODE_TYPE"
.LC1316:
	.string	"c:V8.SizeOf_CODE_TYPE"
.LC1317:
	.string	"c:V8.CountOf_BYTE_ARRAY_TYPE"
.LC1318:
	.string	"c:V8.SizeOf_BYTE_ARRAY_TYPE"
.LC1319:
	.string	"c:V8.CountOf_FOREIGN_TYPE"
.LC1320:
	.string	"c:V8.SizeOf_FOREIGN_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1321:
	.string	"c:V8.CountOf_BYTECODE_ARRAY_TYPE"
	.align 8
.LC1322:
	.string	"c:V8.SizeOf_BYTECODE_ARRAY_TYPE"
	.section	.rodata.str1.1
.LC1323:
	.string	"c:V8.CountOf_FREE_SPACE_TYPE"
.LC1324:
	.string	"c:V8.SizeOf_FREE_SPACE_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1325:
	.string	"c:V8.CountOf_FIXED_DOUBLE_ARRAY_TYPE"
	.align 8
.LC1326:
	.string	"c:V8.SizeOf_FIXED_DOUBLE_ARRAY_TYPE"
	.align 8
.LC1327:
	.string	"c:V8.CountOf_FEEDBACK_METADATA_TYPE"
	.align 8
.LC1328:
	.string	"c:V8.SizeOf_FEEDBACK_METADATA_TYPE"
	.section	.rodata.str1.1
.LC1329:
	.string	"c:V8.CountOf_FILLER_TYPE"
.LC1330:
	.string	"c:V8.SizeOf_FILLER_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1331:
	.string	"c:V8.CountOf_ACCESS_CHECK_INFO_TYPE"
	.align 8
.LC1332:
	.string	"c:V8.SizeOf_ACCESS_CHECK_INFO_TYPE"
	.align 8
.LC1333:
	.string	"c:V8.CountOf_ACCESSOR_INFO_TYPE"
	.align 8
.LC1334:
	.string	"c:V8.SizeOf_ACCESSOR_INFO_TYPE"
	.align 8
.LC1335:
	.string	"c:V8.CountOf_ACCESSOR_PAIR_TYPE"
	.align 8
.LC1336:
	.string	"c:V8.SizeOf_ACCESSOR_PAIR_TYPE"
	.align 8
.LC1337:
	.string	"c:V8.CountOf_ALIASED_ARGUMENTS_ENTRY_TYPE"
	.align 8
.LC1338:
	.string	"c:V8.SizeOf_ALIASED_ARGUMENTS_ENTRY_TYPE"
	.align 8
.LC1339:
	.string	"c:V8.CountOf_ALLOCATION_MEMENTO_TYPE"
	.align 8
.LC1340:
	.string	"c:V8.SizeOf_ALLOCATION_MEMENTO_TYPE"
	.align 8
.LC1341:
	.string	"c:V8.CountOf_ARRAY_BOILERPLATE_DESCRIPTION_TYPE"
	.align 8
.LC1342:
	.string	"c:V8.SizeOf_ARRAY_BOILERPLATE_DESCRIPTION_TYPE"
	.align 8
.LC1343:
	.string	"c:V8.CountOf_ASM_WASM_DATA_TYPE"
	.align 8
.LC1344:
	.string	"c:V8.SizeOf_ASM_WASM_DATA_TYPE"
	.align 8
.LC1345:
	.string	"c:V8.CountOf_ASYNC_GENERATOR_REQUEST_TYPE"
	.align 8
.LC1346:
	.string	"c:V8.SizeOf_ASYNC_GENERATOR_REQUEST_TYPE"
	.align 8
.LC1347:
	.string	"c:V8.CountOf_CLASS_POSITIONS_TYPE"
	.align 8
.LC1348:
	.string	"c:V8.SizeOf_CLASS_POSITIONS_TYPE"
	.section	.rodata.str1.1
.LC1349:
	.string	"c:V8.CountOf_DEBUG_INFO_TYPE"
.LC1350:
	.string	"c:V8.SizeOf_DEBUG_INFO_TYPE"
.LC1351:
	.string	"c:V8.CountOf_ENUM_CACHE_TYPE"
.LC1352:
	.string	"c:V8.SizeOf_ENUM_CACHE_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1353:
	.string	"c:V8.CountOf_FUNCTION_TEMPLATE_INFO_TYPE"
	.align 8
.LC1354:
	.string	"c:V8.SizeOf_FUNCTION_TEMPLATE_INFO_TYPE"
	.align 8
.LC1355:
	.string	"c:V8.CountOf_FUNCTION_TEMPLATE_RARE_DATA_TYPE"
	.align 8
.LC1356:
	.string	"c:V8.SizeOf_FUNCTION_TEMPLATE_RARE_DATA_TYPE"
	.align 8
.LC1357:
	.string	"c:V8.CountOf_INTERCEPTOR_INFO_TYPE"
	.align 8
.LC1358:
	.string	"c:V8.SizeOf_INTERCEPTOR_INFO_TYPE"
	.align 8
.LC1359:
	.string	"c:V8.CountOf_INTERPRETER_DATA_TYPE"
	.align 8
.LC1360:
	.string	"c:V8.SizeOf_INTERPRETER_DATA_TYPE"
	.align 8
.LC1361:
	.string	"c:V8.CountOf_OBJECT_TEMPLATE_INFO_TYPE"
	.align 8
.LC1362:
	.string	"c:V8.SizeOf_OBJECT_TEMPLATE_INFO_TYPE"
	.align 8
.LC1363:
	.string	"c:V8.CountOf_PROMISE_CAPABILITY_TYPE"
	.align 8
.LC1364:
	.string	"c:V8.SizeOf_PROMISE_CAPABILITY_TYPE"
	.align 8
.LC1365:
	.string	"c:V8.CountOf_PROMISE_REACTION_TYPE"
	.align 8
.LC1366:
	.string	"c:V8.SizeOf_PROMISE_REACTION_TYPE"
	.align 8
.LC1367:
	.string	"c:V8.CountOf_PROTOTYPE_INFO_TYPE"
	.align 8
.LC1368:
	.string	"c:V8.SizeOf_PROTOTYPE_INFO_TYPE"
	.section	.rodata.str1.1
.LC1369:
	.string	"c:V8.CountOf_SCRIPT_TYPE"
.LC1370:
	.string	"c:V8.SizeOf_SCRIPT_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1371:
	.string	"c:V8.CountOf_SOURCE_POSITION_TABLE_WITH_FRAME_CACHE_TYPE"
	.align 8
.LC1372:
	.string	"c:V8.SizeOf_SOURCE_POSITION_TABLE_WITH_FRAME_CACHE_TYPE"
	.align 8
.LC1373:
	.string	"c:V8.CountOf_SOURCE_TEXT_MODULE_INFO_ENTRY_TYPE"
	.align 8
.LC1374:
	.string	"c:V8.SizeOf_SOURCE_TEXT_MODULE_INFO_ENTRY_TYPE"
	.align 8
.LC1375:
	.string	"c:V8.CountOf_STACK_FRAME_INFO_TYPE"
	.align 8
.LC1376:
	.string	"c:V8.SizeOf_STACK_FRAME_INFO_TYPE"
	.align 8
.LC1377:
	.string	"c:V8.CountOf_STACK_TRACE_FRAME_TYPE"
	.align 8
.LC1378:
	.string	"c:V8.SizeOf_STACK_TRACE_FRAME_TYPE"
	.align 8
.LC1379:
	.string	"c:V8.CountOf_TEMPLATE_OBJECT_DESCRIPTION_TYPE"
	.align 8
.LC1380:
	.string	"c:V8.SizeOf_TEMPLATE_OBJECT_DESCRIPTION_TYPE"
	.section	.rodata.str1.1
.LC1381:
	.string	"c:V8.CountOf_TUPLE2_TYPE"
.LC1382:
	.string	"c:V8.SizeOf_TUPLE2_TYPE"
.LC1383:
	.string	"c:V8.CountOf_TUPLE3_TYPE"
.LC1384:
	.string	"c:V8.SizeOf_TUPLE3_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1385:
	.string	"c:V8.CountOf_WASM_CAPI_FUNCTION_DATA_TYPE"
	.align 8
.LC1386:
	.string	"c:V8.SizeOf_WASM_CAPI_FUNCTION_DATA_TYPE"
	.align 8
.LC1387:
	.string	"c:V8.CountOf_WASM_DEBUG_INFO_TYPE"
	.align 8
.LC1388:
	.string	"c:V8.SizeOf_WASM_DEBUG_INFO_TYPE"
	.align 8
.LC1389:
	.string	"c:V8.CountOf_WASM_EXCEPTION_TAG_TYPE"
	.align 8
.LC1390:
	.string	"c:V8.SizeOf_WASM_EXCEPTION_TAG_TYPE"
	.align 8
.LC1391:
	.string	"c:V8.CountOf_WASM_EXPORTED_FUNCTION_DATA_TYPE"
	.align 8
.LC1392:
	.string	"c:V8.SizeOf_WASM_EXPORTED_FUNCTION_DATA_TYPE"
	.align 8
.LC1393:
	.string	"c:V8.CountOf_WASM_INDIRECT_FUNCTION_TABLE_TYPE"
	.align 8
.LC1394:
	.string	"c:V8.SizeOf_WASM_INDIRECT_FUNCTION_TABLE_TYPE"
	.align 8
.LC1395:
	.string	"c:V8.CountOf_WASM_JS_FUNCTION_DATA_TYPE"
	.align 8
.LC1396:
	.string	"c:V8.SizeOf_WASM_JS_FUNCTION_DATA_TYPE"
	.align 8
.LC1397:
	.string	"c:V8.CountOf_CALLABLE_TASK_TYPE"
	.align 8
.LC1398:
	.string	"c:V8.SizeOf_CALLABLE_TASK_TYPE"
	.align 8
.LC1399:
	.string	"c:V8.CountOf_CALLBACK_TASK_TYPE"
	.align 8
.LC1400:
	.string	"c:V8.SizeOf_CALLBACK_TASK_TYPE"
	.align 8
.LC1401:
	.string	"c:V8.CountOf_PROMISE_FULFILL_REACTION_JOB_TASK_TYPE"
	.align 8
.LC1402:
	.string	"c:V8.SizeOf_PROMISE_FULFILL_REACTION_JOB_TASK_TYPE"
	.align 8
.LC1403:
	.string	"c:V8.CountOf_PROMISE_REJECT_REACTION_JOB_TASK_TYPE"
	.align 8
.LC1404:
	.string	"c:V8.SizeOf_PROMISE_REJECT_REACTION_JOB_TASK_TYPE"
	.align 8
.LC1405:
	.string	"c:V8.CountOf_PROMISE_RESOLVE_THENABLE_JOB_TASK_TYPE"
	.align 8
.LC1406:
	.string	"c:V8.SizeOf_PROMISE_RESOLVE_THENABLE_JOB_TASK_TYPE"
	.section	.rodata.str1.1
.LC1407:
	.string	"c:V8.CountOf_SORT_STATE_TYPE"
.LC1408:
	.string	"c:V8.SizeOf_SORT_STATE_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1409:
	.string	"c:V8.CountOf_INTERNAL_CLASS_TYPE"
	.align 8
.LC1410:
	.string	"c:V8.SizeOf_INTERNAL_CLASS_TYPE"
	.section	.rodata.str1.1
.LC1411:
	.string	"c:V8.CountOf_SMI_PAIR_TYPE"
.LC1412:
	.string	"c:V8.SizeOf_SMI_PAIR_TYPE"
.LC1413:
	.string	"c:V8.CountOf_SMI_BOX_TYPE"
.LC1414:
	.string	"c:V8.SizeOf_SMI_BOX_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1415:
	.string	"c:V8.CountOf_SOURCE_TEXT_MODULE_TYPE"
	.align 8
.LC1416:
	.string	"c:V8.SizeOf_SOURCE_TEXT_MODULE_TYPE"
	.align 8
.LC1417:
	.string	"c:V8.CountOf_SYNTHETIC_MODULE_TYPE"
	.align 8
.LC1418:
	.string	"c:V8.SizeOf_SYNTHETIC_MODULE_TYPE"
	.align 8
.LC1419:
	.string	"c:V8.CountOf_ALLOCATION_SITE_TYPE"
	.align 8
.LC1420:
	.string	"c:V8.SizeOf_ALLOCATION_SITE_TYPE"
	.align 8
.LC1421:
	.string	"c:V8.CountOf_EMBEDDER_DATA_ARRAY_TYPE"
	.align 8
.LC1422:
	.string	"c:V8.SizeOf_EMBEDDER_DATA_ARRAY_TYPE"
	.section	.rodata.str1.1
.LC1423:
	.string	"c:V8.CountOf_FIXED_ARRAY_TYPE"
.LC1424:
	.string	"c:V8.SizeOf_FIXED_ARRAY_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1425:
	.string	"c:V8.CountOf_OBJECT_BOILERPLATE_DESCRIPTION_TYPE"
	.align 8
.LC1426:
	.string	"c:V8.SizeOf_OBJECT_BOILERPLATE_DESCRIPTION_TYPE"
	.align 8
.LC1427:
	.string	"c:V8.CountOf_CLOSURE_FEEDBACK_CELL_ARRAY_TYPE"
	.align 8
.LC1428:
	.string	"c:V8.SizeOf_CLOSURE_FEEDBACK_CELL_ARRAY_TYPE"
	.section	.rodata.str1.1
.LC1429:
	.string	"c:V8.CountOf_HASH_TABLE_TYPE"
.LC1430:
	.string	"c:V8.SizeOf_HASH_TABLE_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1431:
	.string	"c:V8.CountOf_ORDERED_HASH_MAP_TYPE"
	.align 8
.LC1432:
	.string	"c:V8.SizeOf_ORDERED_HASH_MAP_TYPE"
	.align 8
.LC1433:
	.string	"c:V8.CountOf_ORDERED_HASH_SET_TYPE"
	.align 8
.LC1434:
	.string	"c:V8.SizeOf_ORDERED_HASH_SET_TYPE"
	.align 8
.LC1435:
	.string	"c:V8.CountOf_ORDERED_NAME_DICTIONARY_TYPE"
	.align 8
.LC1436:
	.string	"c:V8.SizeOf_ORDERED_NAME_DICTIONARY_TYPE"
	.align 8
.LC1437:
	.string	"c:V8.CountOf_NAME_DICTIONARY_TYPE"
	.align 8
.LC1438:
	.string	"c:V8.SizeOf_NAME_DICTIONARY_TYPE"
	.align 8
.LC1439:
	.string	"c:V8.CountOf_GLOBAL_DICTIONARY_TYPE"
	.align 8
.LC1440:
	.string	"c:V8.SizeOf_GLOBAL_DICTIONARY_TYPE"
	.align 8
.LC1441:
	.string	"c:V8.CountOf_NUMBER_DICTIONARY_TYPE"
	.align 8
.LC1442:
	.string	"c:V8.SizeOf_NUMBER_DICTIONARY_TYPE"
	.align 8
.LC1443:
	.string	"c:V8.CountOf_SIMPLE_NUMBER_DICTIONARY_TYPE"
	.align 8
.LC1444:
	.string	"c:V8.SizeOf_SIMPLE_NUMBER_DICTIONARY_TYPE"
	.align 8
.LC1445:
	.string	"c:V8.CountOf_STRING_TABLE_TYPE"
	.section	.rodata.str1.1
.LC1446:
	.string	"c:V8.SizeOf_STRING_TABLE_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1447:
	.string	"c:V8.CountOf_EPHEMERON_HASH_TABLE_TYPE"
	.align 8
.LC1448:
	.string	"c:V8.SizeOf_EPHEMERON_HASH_TABLE_TYPE"
	.section	.rodata.str1.1
.LC1449:
	.string	"c:V8.CountOf_SCOPE_INFO_TYPE"
.LC1450:
	.string	"c:V8.SizeOf_SCOPE_INFO_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1451:
	.string	"c:V8.CountOf_SCRIPT_CONTEXT_TABLE_TYPE"
	.align 8
.LC1452:
	.string	"c:V8.SizeOf_SCRIPT_CONTEXT_TABLE_TYPE"
	.align 8
.LC1453:
	.string	"c:V8.CountOf_AWAIT_CONTEXT_TYPE"
	.align 8
.LC1454:
	.string	"c:V8.SizeOf_AWAIT_CONTEXT_TYPE"
	.align 8
.LC1455:
	.string	"c:V8.CountOf_BLOCK_CONTEXT_TYPE"
	.align 8
.LC1456:
	.string	"c:V8.SizeOf_BLOCK_CONTEXT_TYPE"
	.align 8
.LC1457:
	.string	"c:V8.CountOf_CATCH_CONTEXT_TYPE"
	.align 8
.LC1458:
	.string	"c:V8.SizeOf_CATCH_CONTEXT_TYPE"
	.align 8
.LC1459:
	.string	"c:V8.CountOf_DEBUG_EVALUATE_CONTEXT_TYPE"
	.align 8
.LC1460:
	.string	"c:V8.SizeOf_DEBUG_EVALUATE_CONTEXT_TYPE"
	.align 8
.LC1461:
	.string	"c:V8.CountOf_EVAL_CONTEXT_TYPE"
	.section	.rodata.str1.1
.LC1462:
	.string	"c:V8.SizeOf_EVAL_CONTEXT_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1463:
	.string	"c:V8.CountOf_FUNCTION_CONTEXT_TYPE"
	.align 8
.LC1464:
	.string	"c:V8.SizeOf_FUNCTION_CONTEXT_TYPE"
	.align 8
.LC1465:
	.string	"c:V8.CountOf_MODULE_CONTEXT_TYPE"
	.align 8
.LC1466:
	.string	"c:V8.SizeOf_MODULE_CONTEXT_TYPE"
	.align 8
.LC1467:
	.string	"c:V8.CountOf_NATIVE_CONTEXT_TYPE"
	.align 8
.LC1468:
	.string	"c:V8.SizeOf_NATIVE_CONTEXT_TYPE"
	.align 8
.LC1469:
	.string	"c:V8.CountOf_SCRIPT_CONTEXT_TYPE"
	.align 8
.LC1470:
	.string	"c:V8.SizeOf_SCRIPT_CONTEXT_TYPE"
	.align 8
.LC1471:
	.string	"c:V8.CountOf_WITH_CONTEXT_TYPE"
	.section	.rodata.str1.1
.LC1472:
	.string	"c:V8.SizeOf_WITH_CONTEXT_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1473:
	.string	"c:V8.CountOf_WEAK_FIXED_ARRAY_TYPE"
	.align 8
.LC1474:
	.string	"c:V8.SizeOf_WEAK_FIXED_ARRAY_TYPE"
	.align 8
.LC1475:
	.string	"c:V8.CountOf_TRANSITION_ARRAY_TYPE"
	.align 8
.LC1476:
	.string	"c:V8.SizeOf_TRANSITION_ARRAY_TYPE"
	.align 8
.LC1477:
	.string	"c:V8.CountOf_CALL_HANDLER_INFO_TYPE"
	.align 8
.LC1478:
	.string	"c:V8.SizeOf_CALL_HANDLER_INFO_TYPE"
	.section	.rodata.str1.1
.LC1479:
	.string	"c:V8.CountOf_CELL_TYPE"
.LC1480:
	.string	"c:V8.SizeOf_CELL_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1481:
	.string	"c:V8.CountOf_CODE_DATA_CONTAINER_TYPE"
	.align 8
.LC1482:
	.string	"c:V8.SizeOf_CODE_DATA_CONTAINER_TYPE"
	.align 8
.LC1483:
	.string	"c:V8.CountOf_DESCRIPTOR_ARRAY_TYPE"
	.align 8
.LC1484:
	.string	"c:V8.SizeOf_DESCRIPTOR_ARRAY_TYPE"
	.align 8
.LC1485:
	.string	"c:V8.CountOf_FEEDBACK_CELL_TYPE"
	.align 8
.LC1486:
	.string	"c:V8.SizeOf_FEEDBACK_CELL_TYPE"
	.align 8
.LC1487:
	.string	"c:V8.CountOf_FEEDBACK_VECTOR_TYPE"
	.align 8
.LC1488:
	.string	"c:V8.SizeOf_FEEDBACK_VECTOR_TYPE"
	.align 8
.LC1489:
	.string	"c:V8.CountOf_LOAD_HANDLER_TYPE"
	.section	.rodata.str1.1
.LC1490:
	.string	"c:V8.SizeOf_LOAD_HANDLER_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1491:
	.string	"c:V8.CountOf_PREPARSE_DATA_TYPE"
	.align 8
.LC1492:
	.string	"c:V8.SizeOf_PREPARSE_DATA_TYPE"
	.align 8
.LC1493:
	.string	"c:V8.CountOf_PROPERTY_ARRAY_TYPE"
	.align 8
.LC1494:
	.string	"c:V8.SizeOf_PROPERTY_ARRAY_TYPE"
	.align 8
.LC1495:
	.string	"c:V8.CountOf_PROPERTY_CELL_TYPE"
	.align 8
.LC1496:
	.string	"c:V8.SizeOf_PROPERTY_CELL_TYPE"
	.align 8
.LC1497:
	.string	"c:V8.CountOf_SHARED_FUNCTION_INFO_TYPE"
	.align 8
.LC1498:
	.string	"c:V8.SizeOf_SHARED_FUNCTION_INFO_TYPE"
	.align 8
.LC1499:
	.string	"c:V8.CountOf_SMALL_ORDERED_HASH_MAP_TYPE"
	.align 8
.LC1500:
	.string	"c:V8.SizeOf_SMALL_ORDERED_HASH_MAP_TYPE"
	.align 8
.LC1501:
	.string	"c:V8.CountOf_SMALL_ORDERED_HASH_SET_TYPE"
	.align 8
.LC1502:
	.string	"c:V8.SizeOf_SMALL_ORDERED_HASH_SET_TYPE"
	.align 8
.LC1503:
	.string	"c:V8.CountOf_SMALL_ORDERED_NAME_DICTIONARY_TYPE"
	.align 8
.LC1504:
	.string	"c:V8.SizeOf_SMALL_ORDERED_NAME_DICTIONARY_TYPE"
	.align 8
.LC1505:
	.string	"c:V8.CountOf_STORE_HANDLER_TYPE"
	.align 8
.LC1506:
	.string	"c:V8.SizeOf_STORE_HANDLER_TYPE"
	.align 8
.LC1507:
	.string	"c:V8.CountOf_UNCOMPILED_DATA_WITHOUT_PREPARSE_DATA_TYPE"
	.align 8
.LC1508:
	.string	"c:V8.SizeOf_UNCOMPILED_DATA_WITHOUT_PREPARSE_DATA_TYPE"
	.align 8
.LC1509:
	.string	"c:V8.CountOf_UNCOMPILED_DATA_WITH_PREPARSE_DATA_TYPE"
	.align 8
.LC1510:
	.string	"c:V8.SizeOf_UNCOMPILED_DATA_WITH_PREPARSE_DATA_TYPE"
	.align 8
.LC1511:
	.string	"c:V8.CountOf_WEAK_ARRAY_LIST_TYPE"
	.align 8
.LC1512:
	.string	"c:V8.SizeOf_WEAK_ARRAY_LIST_TYPE"
	.section	.rodata.str1.1
.LC1513:
	.string	"c:V8.CountOf_WEAK_CELL_TYPE"
.LC1514:
	.string	"c:V8.SizeOf_WEAK_CELL_TYPE"
.LC1515:
	.string	"c:V8.CountOf_JS_PROXY_TYPE"
.LC1516:
	.string	"c:V8.SizeOf_JS_PROXY_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1517:
	.string	"c:V8.CountOf_JS_GLOBAL_OBJECT_TYPE"
	.align 8
.LC1518:
	.string	"c:V8.SizeOf_JS_GLOBAL_OBJECT_TYPE"
	.align 8
.LC1519:
	.string	"c:V8.CountOf_JS_GLOBAL_PROXY_TYPE"
	.align 8
.LC1520:
	.string	"c:V8.SizeOf_JS_GLOBAL_PROXY_TYPE"
	.align 8
.LC1521:
	.string	"c:V8.CountOf_JS_MODULE_NAMESPACE_TYPE"
	.align 8
.LC1522:
	.string	"c:V8.SizeOf_JS_MODULE_NAMESPACE_TYPE"
	.align 8
.LC1523:
	.string	"c:V8.CountOf_JS_SPECIAL_API_OBJECT_TYPE"
	.align 8
.LC1524:
	.string	"c:V8.SizeOf_JS_SPECIAL_API_OBJECT_TYPE"
	.align 8
.LC1525:
	.string	"c:V8.CountOf_JS_PRIMITIVE_WRAPPER_TYPE"
	.align 8
.LC1526:
	.string	"c:V8.SizeOf_JS_PRIMITIVE_WRAPPER_TYPE"
	.align 8
.LC1527:
	.string	"c:V8.CountOf_JS_API_OBJECT_TYPE"
	.align 8
.LC1528:
	.string	"c:V8.SizeOf_JS_API_OBJECT_TYPE"
	.section	.rodata.str1.1
.LC1529:
	.string	"c:V8.CountOf_JS_OBJECT_TYPE"
.LC1530:
	.string	"c:V8.SizeOf_JS_OBJECT_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1531:
	.string	"c:V8.CountOf_JS_ARGUMENTS_TYPE"
	.section	.rodata.str1.1
.LC1532:
	.string	"c:V8.SizeOf_JS_ARGUMENTS_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1533:
	.string	"c:V8.CountOf_JS_ARRAY_BUFFER_TYPE"
	.align 8
.LC1534:
	.string	"c:V8.SizeOf_JS_ARRAY_BUFFER_TYPE"
	.align 8
.LC1535:
	.string	"c:V8.CountOf_JS_ARRAY_ITERATOR_TYPE"
	.align 8
.LC1536:
	.string	"c:V8.SizeOf_JS_ARRAY_ITERATOR_TYPE"
	.section	.rodata.str1.1
.LC1537:
	.string	"c:V8.CountOf_JS_ARRAY_TYPE"
.LC1538:
	.string	"c:V8.SizeOf_JS_ARRAY_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1539:
	.string	"c:V8.CountOf_JS_ASYNC_FROM_SYNC_ITERATOR_TYPE"
	.align 8
.LC1540:
	.string	"c:V8.SizeOf_JS_ASYNC_FROM_SYNC_ITERATOR_TYPE"
	.align 8
.LC1541:
	.string	"c:V8.CountOf_JS_ASYNC_FUNCTION_OBJECT_TYPE"
	.align 8
.LC1542:
	.string	"c:V8.SizeOf_JS_ASYNC_FUNCTION_OBJECT_TYPE"
	.align 8
.LC1543:
	.string	"c:V8.CountOf_JS_ASYNC_GENERATOR_OBJECT_TYPE"
	.align 8
.LC1544:
	.string	"c:V8.SizeOf_JS_ASYNC_GENERATOR_OBJECT_TYPE"
	.align 8
.LC1545:
	.string	"c:V8.CountOf_JS_CONTEXT_EXTENSION_OBJECT_TYPE"
	.align 8
.LC1546:
	.string	"c:V8.SizeOf_JS_CONTEXT_EXTENSION_OBJECT_TYPE"
	.section	.rodata.str1.1
.LC1547:
	.string	"c:V8.CountOf_JS_DATE_TYPE"
.LC1548:
	.string	"c:V8.SizeOf_JS_DATE_TYPE"
.LC1549:
	.string	"c:V8.CountOf_JS_ERROR_TYPE"
.LC1550:
	.string	"c:V8.SizeOf_JS_ERROR_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1551:
	.string	"c:V8.CountOf_JS_GENERATOR_OBJECT_TYPE"
	.align 8
.LC1552:
	.string	"c:V8.SizeOf_JS_GENERATOR_OBJECT_TYPE"
	.section	.rodata.str1.1
.LC1553:
	.string	"c:V8.CountOf_JS_MAP_TYPE"
.LC1554:
	.string	"c:V8.SizeOf_JS_MAP_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1555:
	.string	"c:V8.CountOf_JS_MAP_KEY_ITERATOR_TYPE"
	.align 8
.LC1556:
	.string	"c:V8.SizeOf_JS_MAP_KEY_ITERATOR_TYPE"
	.align 8
.LC1557:
	.string	"c:V8.CountOf_JS_MAP_KEY_VALUE_ITERATOR_TYPE"
	.align 8
.LC1558:
	.string	"c:V8.SizeOf_JS_MAP_KEY_VALUE_ITERATOR_TYPE"
	.align 8
.LC1559:
	.string	"c:V8.CountOf_JS_MAP_VALUE_ITERATOR_TYPE"
	.align 8
.LC1560:
	.string	"c:V8.SizeOf_JS_MAP_VALUE_ITERATOR_TYPE"
	.align 8
.LC1561:
	.string	"c:V8.CountOf_JS_MESSAGE_OBJECT_TYPE"
	.align 8
.LC1562:
	.string	"c:V8.SizeOf_JS_MESSAGE_OBJECT_TYPE"
	.section	.rodata.str1.1
.LC1563:
	.string	"c:V8.CountOf_JS_PROMISE_TYPE"
.LC1564:
	.string	"c:V8.SizeOf_JS_PROMISE_TYPE"
.LC1565:
	.string	"c:V8.CountOf_JS_REGEXP_TYPE"
.LC1566:
	.string	"c:V8.SizeOf_JS_REGEXP_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1567:
	.string	"c:V8.CountOf_JS_REGEXP_STRING_ITERATOR_TYPE"
	.align 8
.LC1568:
	.string	"c:V8.SizeOf_JS_REGEXP_STRING_ITERATOR_TYPE"
	.section	.rodata.str1.1
.LC1569:
	.string	"c:V8.CountOf_JS_SET_TYPE"
.LC1570:
	.string	"c:V8.SizeOf_JS_SET_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1571:
	.string	"c:V8.CountOf_JS_SET_KEY_VALUE_ITERATOR_TYPE"
	.align 8
.LC1572:
	.string	"c:V8.SizeOf_JS_SET_KEY_VALUE_ITERATOR_TYPE"
	.align 8
.LC1573:
	.string	"c:V8.CountOf_JS_SET_VALUE_ITERATOR_TYPE"
	.align 8
.LC1574:
	.string	"c:V8.SizeOf_JS_SET_VALUE_ITERATOR_TYPE"
	.align 8
.LC1575:
	.string	"c:V8.CountOf_JS_STRING_ITERATOR_TYPE"
	.align 8
.LC1576:
	.string	"c:V8.SizeOf_JS_STRING_ITERATOR_TYPE"
	.section	.rodata.str1.1
.LC1577:
	.string	"c:V8.CountOf_JS_WEAK_REF_TYPE"
.LC1578:
	.string	"c:V8.SizeOf_JS_WEAK_REF_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1579:
	.string	"c:V8.CountOf_JS_FINALIZATION_GROUP_CLEANUP_ITERATOR_TYPE"
	.align 8
.LC1580:
	.string	"c:V8.SizeOf_JS_FINALIZATION_GROUP_CLEANUP_ITERATOR_TYPE"
	.align 8
.LC1581:
	.string	"c:V8.CountOf_JS_FINALIZATION_GROUP_TYPE"
	.align 8
.LC1582:
	.string	"c:V8.SizeOf_JS_FINALIZATION_GROUP_TYPE"
	.section	.rodata.str1.1
.LC1583:
	.string	"c:V8.CountOf_JS_WEAK_MAP_TYPE"
.LC1584:
	.string	"c:V8.SizeOf_JS_WEAK_MAP_TYPE"
.LC1585:
	.string	"c:V8.CountOf_JS_WEAK_SET_TYPE"
.LC1586:
	.string	"c:V8.SizeOf_JS_WEAK_SET_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1587:
	.string	"c:V8.CountOf_JS_TYPED_ARRAY_TYPE"
	.align 8
.LC1588:
	.string	"c:V8.SizeOf_JS_TYPED_ARRAY_TYPE"
	.align 8
.LC1589:
	.string	"c:V8.CountOf_JS_DATA_VIEW_TYPE"
	.section	.rodata.str1.1
.LC1590:
	.string	"c:V8.SizeOf_JS_DATA_VIEW_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1591:
	.string	"c:V8.CountOf_JS_INTL_V8_BREAK_ITERATOR_TYPE"
	.align 8
.LC1592:
	.string	"c:V8.SizeOf_JS_INTL_V8_BREAK_ITERATOR_TYPE"
	.align 8
.LC1593:
	.string	"c:V8.CountOf_JS_INTL_COLLATOR_TYPE"
	.align 8
.LC1594:
	.string	"c:V8.SizeOf_JS_INTL_COLLATOR_TYPE"
	.align 8
.LC1595:
	.string	"c:V8.CountOf_JS_INTL_DATE_TIME_FORMAT_TYPE"
	.align 8
.LC1596:
	.string	"c:V8.SizeOf_JS_INTL_DATE_TIME_FORMAT_TYPE"
	.align 8
.LC1597:
	.string	"c:V8.CountOf_JS_INTL_LIST_FORMAT_TYPE"
	.align 8
.LC1598:
	.string	"c:V8.SizeOf_JS_INTL_LIST_FORMAT_TYPE"
	.align 8
.LC1599:
	.string	"c:V8.CountOf_JS_INTL_LOCALE_TYPE"
	.align 8
.LC1600:
	.string	"c:V8.SizeOf_JS_INTL_LOCALE_TYPE"
	.align 8
.LC1601:
	.string	"c:V8.CountOf_JS_INTL_NUMBER_FORMAT_TYPE"
	.align 8
.LC1602:
	.string	"c:V8.SizeOf_JS_INTL_NUMBER_FORMAT_TYPE"
	.align 8
.LC1603:
	.string	"c:V8.CountOf_JS_INTL_PLURAL_RULES_TYPE"
	.align 8
.LC1604:
	.string	"c:V8.SizeOf_JS_INTL_PLURAL_RULES_TYPE"
	.align 8
.LC1605:
	.string	"c:V8.CountOf_JS_INTL_RELATIVE_TIME_FORMAT_TYPE"
	.align 8
.LC1606:
	.string	"c:V8.SizeOf_JS_INTL_RELATIVE_TIME_FORMAT_TYPE"
	.align 8
.LC1607:
	.string	"c:V8.CountOf_JS_INTL_SEGMENT_ITERATOR_TYPE"
	.align 8
.LC1608:
	.string	"c:V8.SizeOf_JS_INTL_SEGMENT_ITERATOR_TYPE"
	.align 8
.LC1609:
	.string	"c:V8.CountOf_JS_INTL_SEGMENTER_TYPE"
	.align 8
.LC1610:
	.string	"c:V8.SizeOf_JS_INTL_SEGMENTER_TYPE"
	.align 8
.LC1611:
	.string	"c:V8.CountOf_WASM_EXCEPTION_TYPE"
	.align 8
.LC1612:
	.string	"c:V8.SizeOf_WASM_EXCEPTION_TYPE"
	.section	.rodata.str1.1
.LC1613:
	.string	"c:V8.CountOf_WASM_GLOBAL_TYPE"
.LC1614:
	.string	"c:V8.SizeOf_WASM_GLOBAL_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1615:
	.string	"c:V8.CountOf_WASM_INSTANCE_TYPE"
	.align 8
.LC1616:
	.string	"c:V8.SizeOf_WASM_INSTANCE_TYPE"
	.section	.rodata.str1.1
.LC1617:
	.string	"c:V8.CountOf_WASM_MEMORY_TYPE"
.LC1618:
	.string	"c:V8.SizeOf_WASM_MEMORY_TYPE"
.LC1619:
	.string	"c:V8.CountOf_WASM_MODULE_TYPE"
.LC1620:
	.string	"c:V8.SizeOf_WASM_MODULE_TYPE"
.LC1621:
	.string	"c:V8.CountOf_WASM_TABLE_TYPE"
.LC1622:
	.string	"c:V8.SizeOf_WASM_TABLE_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1623:
	.string	"c:V8.CountOf_JS_BOUND_FUNCTION_TYPE"
	.align 8
.LC1624:
	.string	"c:V8.SizeOf_JS_BOUND_FUNCTION_TYPE"
	.section	.rodata.str1.1
.LC1625:
	.string	"c:V8.CountOf_JS_FUNCTION_TYPE"
.LC1626:
	.string	"c:V8.SizeOf_JS_FUNCTION_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC1627:
	.string	"c:V8.CountOf_CODE_TYPE-OPTIMIZED_FUNCTION"
	.align 8
.LC1628:
	.string	"c:V8.SizeOf_CODE_TYPE-OPTIMIZED_FUNCTION"
	.align 8
.LC1629:
	.string	"c:V8.CountOf_CODE_TYPE-BYTECODE_HANDLER"
	.align 8
.LC1630:
	.string	"c:V8.SizeOf_CODE_TYPE-BYTECODE_HANDLER"
	.section	.rodata.str1.1
.LC1631:
	.string	"c:V8.CountOf_CODE_TYPE-STUB"
.LC1632:
	.string	"c:V8.SizeOf_CODE_TYPE-STUB"
	.section	.rodata.str1.8
	.align 8
.LC1633:
	.string	"c:V8.CountOf_CODE_TYPE-BUILTIN"
	.section	.rodata.str1.1
.LC1634:
	.string	"c:V8.SizeOf_CODE_TYPE-BUILTIN"
.LC1635:
	.string	"c:V8.CountOf_CODE_TYPE-REGEXP"
.LC1636:
	.string	"c:V8.SizeOf_CODE_TYPE-REGEXP"
	.section	.rodata.str1.8
	.align 8
.LC1637:
	.string	"c:V8.CountOf_CODE_TYPE-WASM_FUNCTION"
	.align 8
.LC1638:
	.string	"c:V8.SizeOf_CODE_TYPE-WASM_FUNCTION"
	.align 8
.LC1639:
	.string	"c:V8.CountOf_CODE_TYPE-WASM_TO_CAPI_FUNCTION"
	.align 8
.LC1640:
	.string	"c:V8.SizeOf_CODE_TYPE-WASM_TO_CAPI_FUNCTION"
	.align 8
.LC1641:
	.string	"c:V8.CountOf_CODE_TYPE-WASM_TO_JS_FUNCTION"
	.align 8
.LC1642:
	.string	"c:V8.SizeOf_CODE_TYPE-WASM_TO_JS_FUNCTION"
	.align 8
.LC1643:
	.string	"c:V8.CountOf_CODE_TYPE-JS_TO_WASM_FUNCTION"
	.align 8
.LC1644:
	.string	"c:V8.SizeOf_CODE_TYPE-JS_TO_WASM_FUNCTION"
	.align 8
.LC1645:
	.string	"c:V8.CountOf_CODE_TYPE-JS_TO_JS_FUNCTION"
	.align 8
.LC1646:
	.string	"c:V8.SizeOf_CODE_TYPE-JS_TO_JS_FUNCTION"
	.align 8
.LC1647:
	.string	"c:V8.CountOf_CODE_TYPE-WASM_INTERPRETER_ENTRY"
	.align 8
.LC1648:
	.string	"c:V8.SizeOf_CODE_TYPE-WASM_INTERPRETER_ENTRY"
	.align 8
.LC1649:
	.string	"c:V8.CountOf_CODE_TYPE-C_WASM_ENTRY"
	.align 8
.LC1650:
	.string	"c:V8.SizeOf_CODE_TYPE-C_WASM_ENTRY"
	.align 8
.LC1651:
	.string	"c:V8.CountOf_FIXED_ARRAY-BYTECODE_ARRAY_CONSTANT_POOL_SUB_TYPE"
	.align 8
.LC1652:
	.string	"c:V8.SizeOf_FIXED_ARRAY-BYTECODE_ARRAY_CONSTANT_POOL_SUB_TYPE"
	.align 8
.LC1653:
	.string	"c:V8.CountOf_FIXED_ARRAY-BYTECODE_ARRAY_HANDLER_TABLE_SUB_TYPE"
	.align 8
.LC1654:
	.string	"c:V8.SizeOf_FIXED_ARRAY-BYTECODE_ARRAY_HANDLER_TABLE_SUB_TYPE"
	.align 8
.LC1655:
	.string	"c:V8.CountOf_FIXED_ARRAY-CODE_STUBS_TABLE_SUB_TYPE"
	.align 8
.LC1656:
	.string	"c:V8.SizeOf_FIXED_ARRAY-CODE_STUBS_TABLE_SUB_TYPE"
	.align 8
.LC1657:
	.string	"c:V8.CountOf_FIXED_ARRAY-COMPILATION_CACHE_TABLE_SUB_TYPE"
	.align 8
.LC1658:
	.string	"c:V8.SizeOf_FIXED_ARRAY-COMPILATION_CACHE_TABLE_SUB_TYPE"
	.align 8
.LC1659:
	.string	"c:V8.CountOf_FIXED_ARRAY-CONTEXT_SUB_TYPE"
	.align 8
.LC1660:
	.string	"c:V8.SizeOf_FIXED_ARRAY-CONTEXT_SUB_TYPE"
	.align 8
.LC1661:
	.string	"c:V8.CountOf_FIXED_ARRAY-COPY_ON_WRITE_SUB_TYPE"
	.align 8
.LC1662:
	.string	"c:V8.SizeOf_FIXED_ARRAY-COPY_ON_WRITE_SUB_TYPE"
	.align 8
.LC1663:
	.string	"c:V8.CountOf_FIXED_ARRAY-DEOPTIMIZATION_DATA_SUB_TYPE"
	.align 8
.LC1664:
	.string	"c:V8.SizeOf_FIXED_ARRAY-DEOPTIMIZATION_DATA_SUB_TYPE"
	.align 8
.LC1665:
	.string	"c:V8.CountOf_FIXED_ARRAY-DESCRIPTOR_ARRAY_SUB_TYPE"
	.align 8
.LC1666:
	.string	"c:V8.SizeOf_FIXED_ARRAY-DESCRIPTOR_ARRAY_SUB_TYPE"
	.align 8
.LC1667:
	.string	"c:V8.CountOf_FIXED_ARRAY-EMBEDDED_OBJECT_SUB_TYPE"
	.align 8
.LC1668:
	.string	"c:V8.SizeOf_FIXED_ARRAY-EMBEDDED_OBJECT_SUB_TYPE"
	.align 8
.LC1669:
	.string	"c:V8.CountOf_FIXED_ARRAY-ENUM_CACHE_SUB_TYPE"
	.align 8
.LC1670:
	.string	"c:V8.SizeOf_FIXED_ARRAY-ENUM_CACHE_SUB_TYPE"
	.align 8
.LC1671:
	.string	"c:V8.CountOf_FIXED_ARRAY-ENUM_INDICES_CACHE_SUB_TYPE"
	.align 8
.LC1672:
	.string	"c:V8.SizeOf_FIXED_ARRAY-ENUM_INDICES_CACHE_SUB_TYPE"
	.align 8
.LC1673:
	.string	"c:V8.CountOf_FIXED_ARRAY-DEPENDENT_CODE_SUB_TYPE"
	.align 8
.LC1674:
	.string	"c:V8.SizeOf_FIXED_ARRAY-DEPENDENT_CODE_SUB_TYPE"
	.align 8
.LC1675:
	.string	"c:V8.CountOf_FIXED_ARRAY-DICTIONARY_ELEMENTS_SUB_TYPE"
	.align 8
.LC1676:
	.string	"c:V8.SizeOf_FIXED_ARRAY-DICTIONARY_ELEMENTS_SUB_TYPE"
	.align 8
.LC1677:
	.string	"c:V8.CountOf_FIXED_ARRAY-DICTIONARY_PROPERTIES_SUB_TYPE"
	.align 8
.LC1678:
	.string	"c:V8.SizeOf_FIXED_ARRAY-DICTIONARY_PROPERTIES_SUB_TYPE"
	.align 8
.LC1679:
	.string	"c:V8.CountOf_FIXED_ARRAY-EMPTY_PROPERTIES_DICTIONARY_SUB_TYPE"
	.align 8
.LC1680:
	.string	"c:V8.SizeOf_FIXED_ARRAY-EMPTY_PROPERTIES_DICTIONARY_SUB_TYPE"
	.align 8
.LC1681:
	.string	"c:V8.CountOf_FIXED_ARRAY-PACKED_ELEMENTS_SUB_TYPE"
	.align 8
.LC1682:
	.string	"c:V8.SizeOf_FIXED_ARRAY-PACKED_ELEMENTS_SUB_TYPE"
	.align 8
.LC1683:
	.string	"c:V8.CountOf_FIXED_ARRAY-FAST_PROPERTIES_SUB_TYPE"
	.align 8
.LC1684:
	.string	"c:V8.SizeOf_FIXED_ARRAY-FAST_PROPERTIES_SUB_TYPE"
	.align 8
.LC1685:
	.string	"c:V8.CountOf_FIXED_ARRAY-FAST_TEMPLATE_INSTANTIATIONS_CACHE_SUB_TYPE"
	.align 8
.LC1686:
	.string	"c:V8.SizeOf_FIXED_ARRAY-FAST_TEMPLATE_INSTANTIATIONS_CACHE_SUB_TYPE"
	.align 8
.LC1687:
	.string	"c:V8.CountOf_FIXED_ARRAY-HANDLER_TABLE_SUB_TYPE"
	.align 8
.LC1688:
	.string	"c:V8.SizeOf_FIXED_ARRAY-HANDLER_TABLE_SUB_TYPE"
	.align 8
.LC1689:
	.string	"c:V8.CountOf_FIXED_ARRAY-JS_COLLECTION_SUB_TYPE"
	.align 8
.LC1690:
	.string	"c:V8.SizeOf_FIXED_ARRAY-JS_COLLECTION_SUB_TYPE"
	.align 8
.LC1691:
	.string	"c:V8.CountOf_FIXED_ARRAY-JS_WEAK_COLLECTION_SUB_TYPE"
	.align 8
.LC1692:
	.string	"c:V8.SizeOf_FIXED_ARRAY-JS_WEAK_COLLECTION_SUB_TYPE"
	.align 8
.LC1693:
	.string	"c:V8.CountOf_FIXED_ARRAY-NOSCRIPT_SHARED_FUNCTION_INFOS_SUB_TYPE"
	.align 8
.LC1694:
	.string	"c:V8.SizeOf_FIXED_ARRAY-NOSCRIPT_SHARED_FUNCTION_INFOS_SUB_TYPE"
	.align 8
.LC1695:
	.string	"c:V8.CountOf_FIXED_ARRAY-NUMBER_STRING_CACHE_SUB_TYPE"
	.align 8
.LC1696:
	.string	"c:V8.SizeOf_FIXED_ARRAY-NUMBER_STRING_CACHE_SUB_TYPE"
	.align 8
.LC1697:
	.string	"c:V8.CountOf_FIXED_ARRAY-OBJECT_TO_CODE_SUB_TYPE"
	.align 8
.LC1698:
	.string	"c:V8.SizeOf_FIXED_ARRAY-OBJECT_TO_CODE_SUB_TYPE"
	.align 8
.LC1699:
	.string	"c:V8.CountOf_FIXED_ARRAY-OPTIMIZED_CODE_LITERALS_SUB_TYPE"
	.align 8
.LC1700:
	.string	"c:V8.SizeOf_FIXED_ARRAY-OPTIMIZED_CODE_LITERALS_SUB_TYPE"
	.align 8
.LC1701:
	.string	"c:V8.CountOf_FIXED_ARRAY-OPTIMIZED_CODE_MAP_SUB_TYPE"
	.align 8
.LC1702:
	.string	"c:V8.SizeOf_FIXED_ARRAY-OPTIMIZED_CODE_MAP_SUB_TYPE"
	.align 8
.LC1703:
	.string	"c:V8.CountOf_FIXED_ARRAY-PROTOTYPE_USERS_SUB_TYPE"
	.align 8
.LC1704:
	.string	"c:V8.SizeOf_FIXED_ARRAY-PROTOTYPE_USERS_SUB_TYPE"
	.align 8
.LC1705:
	.string	"c:V8.CountOf_FIXED_ARRAY-REGEXP_MULTIPLE_CACHE_SUB_TYPE"
	.align 8
.LC1706:
	.string	"c:V8.SizeOf_FIXED_ARRAY-REGEXP_MULTIPLE_CACHE_SUB_TYPE"
	.align 8
.LC1707:
	.string	"c:V8.CountOf_FIXED_ARRAY-RETAINED_MAPS_SUB_TYPE"
	.align 8
.LC1708:
	.string	"c:V8.SizeOf_FIXED_ARRAY-RETAINED_MAPS_SUB_TYPE"
	.align 8
.LC1709:
	.string	"c:V8.CountOf_FIXED_ARRAY-SCOPE_INFO_SUB_TYPE"
	.align 8
.LC1710:
	.string	"c:V8.SizeOf_FIXED_ARRAY-SCOPE_INFO_SUB_TYPE"
	.align 8
.LC1711:
	.string	"c:V8.CountOf_FIXED_ARRAY-SCRIPT_LIST_SUB_TYPE"
	.align 8
.LC1712:
	.string	"c:V8.SizeOf_FIXED_ARRAY-SCRIPT_LIST_SUB_TYPE"
	.align 8
.LC1713:
	.string	"c:V8.CountOf_FIXED_ARRAY-SERIALIZED_OBJECTS_SUB_TYPE"
	.align 8
.LC1714:
	.string	"c:V8.SizeOf_FIXED_ARRAY-SERIALIZED_OBJECTS_SUB_TYPE"
	.align 8
.LC1715:
	.string	"c:V8.CountOf_FIXED_ARRAY-SHARED_FUNCTION_INFOS_SUB_TYPE"
	.align 8
.LC1716:
	.string	"c:V8.SizeOf_FIXED_ARRAY-SHARED_FUNCTION_INFOS_SUB_TYPE"
	.align 8
.LC1717:
	.string	"c:V8.CountOf_FIXED_ARRAY-SINGLE_CHARACTER_STRING_CACHE_SUB_TYPE"
	.align 8
.LC1718:
	.string	"c:V8.SizeOf_FIXED_ARRAY-SINGLE_CHARACTER_STRING_CACHE_SUB_TYPE"
	.align 8
.LC1719:
	.string	"c:V8.CountOf_FIXED_ARRAY-SLOW_TEMPLATE_INSTANTIATIONS_CACHE_SUB_TYPE"
	.align 8
.LC1720:
	.string	"c:V8.SizeOf_FIXED_ARRAY-SLOW_TEMPLATE_INSTANTIATIONS_CACHE_SUB_TYPE"
	.align 8
.LC1721:
	.string	"c:V8.CountOf_FIXED_ARRAY-STRING_SPLIT_CACHE_SUB_TYPE"
	.align 8
.LC1722:
	.string	"c:V8.SizeOf_FIXED_ARRAY-STRING_SPLIT_CACHE_SUB_TYPE"
	.align 8
.LC1723:
	.string	"c:V8.CountOf_FIXED_ARRAY-STRING_TABLE_SUB_TYPE"
	.align 8
.LC1724:
	.string	"c:V8.SizeOf_FIXED_ARRAY-STRING_TABLE_SUB_TYPE"
	.align 8
.LC1725:
	.string	"c:V8.CountOf_FIXED_ARRAY-TEMPLATE_INFO_SUB_TYPE"
	.align 8
.LC1726:
	.string	"c:V8.SizeOf_FIXED_ARRAY-TEMPLATE_INFO_SUB_TYPE"
	.align 8
.LC1727:
	.string	"c:V8.CountOf_FIXED_ARRAY-FEEDBACK_METADATA_SUB_TYPE"
	.align 8
.LC1728:
	.string	"c:V8.SizeOf_FIXED_ARRAY-FEEDBACK_METADATA_SUB_TYPE"
	.align 8
.LC1729:
	.string	"c:V8.CountOf_FIXED_ARRAY-WEAK_NEW_SPACE_OBJECT_TO_CODE_SUB_TYPE"
	.align 8
.LC1730:
	.string	"c:V8.SizeOf_FIXED_ARRAY-WEAK_NEW_SPACE_OBJECT_TO_CODE_SUB_TYPE"
	.section	.data.rel.ro.local._ZZN2v88internal8CountersC4EPNS0_7IsolateEE14kStatsCounters,"aw"
	.align 32
	.type	_ZZN2v88internal8CountersC4EPNS0_7IsolateEE14kStatsCounters, @object
	.size	_ZZN2v88internal8CountersC4EPNS0_7IsolateEE14kStatsCounters, 8512
_ZZN2v88internal8CountersC4EPNS0_7IsolateEE14kStatsCounters:
	.quad	5896
	.quad	.LC1199
	.quad	5928
	.quad	.LC1200
	.quad	5960
	.quad	.LC1201
	.quad	5992
	.quad	.LC1202
	.quad	6024
	.quad	.LC1203
	.quad	6056
	.quad	.LC1204
	.quad	6088
	.quad	.LC1205
	.quad	6120
	.quad	.LC1206
	.quad	6152
	.quad	.LC1207
	.quad	6184
	.quad	.LC1208
	.quad	6216
	.quad	.LC1209
	.quad	6248
	.quad	.LC1210
	.quad	6280
	.quad	.LC1211
	.quad	6312
	.quad	.LC1212
	.quad	6344
	.quad	.LC1213
	.quad	6376
	.quad	.LC1214
	.quad	6408
	.quad	.LC1215
	.quad	6440
	.quad	.LC1216
	.quad	6472
	.quad	.LC1217
	.quad	6504
	.quad	.LC1218
	.quad	6536
	.quad	.LC1219
	.quad	6568
	.quad	.LC1220
	.quad	6600
	.quad	.LC1221
	.quad	6632
	.quad	.LC1222
	.quad	6664
	.quad	.LC1223
	.quad	6696
	.quad	.LC1224
	.quad	6728
	.quad	.LC1225
	.quad	6760
	.quad	.LC1226
	.quad	6792
	.quad	.LC1227
	.quad	6824
	.quad	.LC1228
	.quad	6856
	.quad	.LC1229
	.quad	6888
	.quad	.LC1230
	.quad	6920
	.quad	.LC1231
	.quad	6952
	.quad	.LC1232
	.quad	6984
	.quad	.LC1233
	.quad	7016
	.quad	.LC1234
	.quad	7048
	.quad	.LC1235
	.quad	7080
	.quad	.LC1236
	.quad	7112
	.quad	.LC1237
	.quad	7144
	.quad	.LC1238
	.quad	7176
	.quad	.LC1239
	.quad	7208
	.quad	.LC1240
	.quad	7240
	.quad	.LC1241
	.quad	7272
	.quad	.LC1242
	.quad	7304
	.quad	.LC1243
	.quad	7336
	.quad	.LC1244
	.quad	7368
	.quad	.LC1245
	.quad	7400
	.quad	.LC1246
	.quad	7432
	.quad	.LC1247
	.quad	7464
	.quad	.LC1248
	.quad	7496
	.quad	.LC1249
	.quad	7528
	.quad	.LC1250
	.quad	7560
	.quad	.LC1251
	.quad	7592
	.quad	.LC1252
	.quad	7624
	.quad	.LC1253
	.quad	7656
	.quad	.LC1254
	.quad	7688
	.quad	.LC1255
	.quad	7720
	.quad	.LC1256
	.quad	7752
	.quad	.LC1257
	.quad	7784
	.quad	.LC1258
	.quad	7816
	.quad	.LC1259
	.quad	7848
	.quad	.LC1260
	.quad	7880
	.quad	.LC1261
	.quad	7912
	.quad	.LC1262
	.quad	7944
	.quad	.LC1263
	.quad	7976
	.quad	.LC1264
	.quad	8008
	.quad	.LC1265
	.quad	8040
	.quad	.LC1266
	.quad	8072
	.quad	.LC1267
	.quad	8104
	.quad	.LC1268
	.quad	8488
	.quad	.LC1269
	.quad	8456
	.quad	.LC1270
	.quad	8552
	.quad	.LC1271
	.quad	8520
	.quad	.LC1272
	.quad	8616
	.quad	.LC1273
	.quad	8584
	.quad	.LC1274
	.quad	8680
	.quad	.LC1275
	.quad	8648
	.quad	.LC1276
	.quad	8744
	.quad	.LC1277
	.quad	8712
	.quad	.LC1278
	.quad	8808
	.quad	.LC1279
	.quad	8776
	.quad	.LC1280
	.quad	8872
	.quad	.LC1281
	.quad	8840
	.quad	.LC1282
	.quad	8936
	.quad	.LC1283
	.quad	8904
	.quad	.LC1284
	.quad	9000
	.quad	.LC1285
	.quad	8968
	.quad	.LC1286
	.quad	9064
	.quad	.LC1287
	.quad	9032
	.quad	.LC1288
	.quad	9128
	.quad	.LC1289
	.quad	9096
	.quad	.LC1290
	.quad	9192
	.quad	.LC1291
	.quad	9160
	.quad	.LC1292
	.quad	9256
	.quad	.LC1293
	.quad	9224
	.quad	.LC1294
	.quad	9320
	.quad	.LC1295
	.quad	9288
	.quad	.LC1296
	.quad	9384
	.quad	.LC1297
	.quad	9352
	.quad	.LC1298
	.quad	9448
	.quad	.LC1299
	.quad	9416
	.quad	.LC1300
	.quad	9512
	.quad	.LC1301
	.quad	9480
	.quad	.LC1302
	.quad	9576
	.quad	.LC1303
	.quad	9544
	.quad	.LC1304
	.quad	9640
	.quad	.LC1305
	.quad	9608
	.quad	.LC1306
	.quad	9704
	.quad	.LC1307
	.quad	9672
	.quad	.LC1308
	.quad	9768
	.quad	.LC1309
	.quad	9736
	.quad	.LC1310
	.quad	9832
	.quad	.LC1311
	.quad	9800
	.quad	.LC1312
	.quad	9896
	.quad	.LC1313
	.quad	9864
	.quad	.LC1314
	.quad	9960
	.quad	.LC1315
	.quad	9928
	.quad	.LC1316
	.quad	10024
	.quad	.LC1317
	.quad	9992
	.quad	.LC1318
	.quad	10088
	.quad	.LC1319
	.quad	10056
	.quad	.LC1320
	.quad	10152
	.quad	.LC1321
	.quad	10120
	.quad	.LC1322
	.quad	10216
	.quad	.LC1323
	.quad	10184
	.quad	.LC1324
	.quad	10280
	.quad	.LC1325
	.quad	10248
	.quad	.LC1326
	.quad	10344
	.quad	.LC1327
	.quad	10312
	.quad	.LC1328
	.quad	10408
	.quad	.LC1329
	.quad	10376
	.quad	.LC1330
	.quad	10472
	.quad	.LC1331
	.quad	10440
	.quad	.LC1332
	.quad	10536
	.quad	.LC1333
	.quad	10504
	.quad	.LC1334
	.quad	10600
	.quad	.LC1335
	.quad	10568
	.quad	.LC1336
	.quad	10664
	.quad	.LC1337
	.quad	10632
	.quad	.LC1338
	.quad	10728
	.quad	.LC1339
	.quad	10696
	.quad	.LC1340
	.quad	10792
	.quad	.LC1341
	.quad	10760
	.quad	.LC1342
	.quad	10856
	.quad	.LC1343
	.quad	10824
	.quad	.LC1344
	.quad	10920
	.quad	.LC1345
	.quad	10888
	.quad	.LC1346
	.quad	10984
	.quad	.LC1347
	.quad	10952
	.quad	.LC1348
	.quad	11048
	.quad	.LC1349
	.quad	11016
	.quad	.LC1350
	.quad	11112
	.quad	.LC1351
	.quad	11080
	.quad	.LC1352
	.quad	11176
	.quad	.LC1353
	.quad	11144
	.quad	.LC1354
	.quad	11240
	.quad	.LC1355
	.quad	11208
	.quad	.LC1356
	.quad	11304
	.quad	.LC1357
	.quad	11272
	.quad	.LC1358
	.quad	11368
	.quad	.LC1359
	.quad	11336
	.quad	.LC1360
	.quad	11432
	.quad	.LC1361
	.quad	11400
	.quad	.LC1362
	.quad	11496
	.quad	.LC1363
	.quad	11464
	.quad	.LC1364
	.quad	11560
	.quad	.LC1365
	.quad	11528
	.quad	.LC1366
	.quad	11624
	.quad	.LC1367
	.quad	11592
	.quad	.LC1368
	.quad	11688
	.quad	.LC1369
	.quad	11656
	.quad	.LC1370
	.quad	11752
	.quad	.LC1371
	.quad	11720
	.quad	.LC1372
	.quad	11816
	.quad	.LC1373
	.quad	11784
	.quad	.LC1374
	.quad	11880
	.quad	.LC1375
	.quad	11848
	.quad	.LC1376
	.quad	11944
	.quad	.LC1377
	.quad	11912
	.quad	.LC1378
	.quad	12008
	.quad	.LC1379
	.quad	11976
	.quad	.LC1380
	.quad	12072
	.quad	.LC1381
	.quad	12040
	.quad	.LC1382
	.quad	12136
	.quad	.LC1383
	.quad	12104
	.quad	.LC1384
	.quad	12200
	.quad	.LC1385
	.quad	12168
	.quad	.LC1386
	.quad	12264
	.quad	.LC1387
	.quad	12232
	.quad	.LC1388
	.quad	12328
	.quad	.LC1389
	.quad	12296
	.quad	.LC1390
	.quad	12392
	.quad	.LC1391
	.quad	12360
	.quad	.LC1392
	.quad	12456
	.quad	.LC1393
	.quad	12424
	.quad	.LC1394
	.quad	12520
	.quad	.LC1395
	.quad	12488
	.quad	.LC1396
	.quad	12584
	.quad	.LC1397
	.quad	12552
	.quad	.LC1398
	.quad	12648
	.quad	.LC1399
	.quad	12616
	.quad	.LC1400
	.quad	12712
	.quad	.LC1401
	.quad	12680
	.quad	.LC1402
	.quad	12776
	.quad	.LC1403
	.quad	12744
	.quad	.LC1404
	.quad	12840
	.quad	.LC1405
	.quad	12808
	.quad	.LC1406
	.quad	12904
	.quad	.LC1407
	.quad	12872
	.quad	.LC1408
	.quad	12968
	.quad	.LC1409
	.quad	12936
	.quad	.LC1410
	.quad	13032
	.quad	.LC1411
	.quad	13000
	.quad	.LC1412
	.quad	13096
	.quad	.LC1413
	.quad	13064
	.quad	.LC1414
	.quad	13160
	.quad	.LC1415
	.quad	13128
	.quad	.LC1416
	.quad	13224
	.quad	.LC1417
	.quad	13192
	.quad	.LC1418
	.quad	13288
	.quad	.LC1419
	.quad	13256
	.quad	.LC1420
	.quad	13352
	.quad	.LC1421
	.quad	13320
	.quad	.LC1422
	.quad	13416
	.quad	.LC1423
	.quad	13384
	.quad	.LC1424
	.quad	13480
	.quad	.LC1425
	.quad	13448
	.quad	.LC1426
	.quad	13544
	.quad	.LC1427
	.quad	13512
	.quad	.LC1428
	.quad	13608
	.quad	.LC1429
	.quad	13576
	.quad	.LC1430
	.quad	13672
	.quad	.LC1431
	.quad	13640
	.quad	.LC1432
	.quad	13736
	.quad	.LC1433
	.quad	13704
	.quad	.LC1434
	.quad	13800
	.quad	.LC1435
	.quad	13768
	.quad	.LC1436
	.quad	13864
	.quad	.LC1437
	.quad	13832
	.quad	.LC1438
	.quad	13928
	.quad	.LC1439
	.quad	13896
	.quad	.LC1440
	.quad	13992
	.quad	.LC1441
	.quad	13960
	.quad	.LC1442
	.quad	14056
	.quad	.LC1443
	.quad	14024
	.quad	.LC1444
	.quad	14120
	.quad	.LC1445
	.quad	14088
	.quad	.LC1446
	.quad	14184
	.quad	.LC1447
	.quad	14152
	.quad	.LC1448
	.quad	14248
	.quad	.LC1449
	.quad	14216
	.quad	.LC1450
	.quad	14312
	.quad	.LC1451
	.quad	14280
	.quad	.LC1452
	.quad	14376
	.quad	.LC1453
	.quad	14344
	.quad	.LC1454
	.quad	14440
	.quad	.LC1455
	.quad	14408
	.quad	.LC1456
	.quad	14504
	.quad	.LC1457
	.quad	14472
	.quad	.LC1458
	.quad	14568
	.quad	.LC1459
	.quad	14536
	.quad	.LC1460
	.quad	14632
	.quad	.LC1461
	.quad	14600
	.quad	.LC1462
	.quad	14696
	.quad	.LC1463
	.quad	14664
	.quad	.LC1464
	.quad	14760
	.quad	.LC1465
	.quad	14728
	.quad	.LC1466
	.quad	14824
	.quad	.LC1467
	.quad	14792
	.quad	.LC1468
	.quad	14888
	.quad	.LC1469
	.quad	14856
	.quad	.LC1470
	.quad	14952
	.quad	.LC1471
	.quad	14920
	.quad	.LC1472
	.quad	15016
	.quad	.LC1473
	.quad	14984
	.quad	.LC1474
	.quad	15080
	.quad	.LC1475
	.quad	15048
	.quad	.LC1476
	.quad	15144
	.quad	.LC1477
	.quad	15112
	.quad	.LC1478
	.quad	15208
	.quad	.LC1479
	.quad	15176
	.quad	.LC1480
	.quad	15272
	.quad	.LC1481
	.quad	15240
	.quad	.LC1482
	.quad	15336
	.quad	.LC1483
	.quad	15304
	.quad	.LC1484
	.quad	15400
	.quad	.LC1485
	.quad	15368
	.quad	.LC1486
	.quad	15464
	.quad	.LC1487
	.quad	15432
	.quad	.LC1488
	.quad	15528
	.quad	.LC1489
	.quad	15496
	.quad	.LC1490
	.quad	15592
	.quad	.LC1491
	.quad	15560
	.quad	.LC1492
	.quad	15656
	.quad	.LC1493
	.quad	15624
	.quad	.LC1494
	.quad	15720
	.quad	.LC1495
	.quad	15688
	.quad	.LC1496
	.quad	15784
	.quad	.LC1497
	.quad	15752
	.quad	.LC1498
	.quad	15848
	.quad	.LC1499
	.quad	15816
	.quad	.LC1500
	.quad	15912
	.quad	.LC1501
	.quad	15880
	.quad	.LC1502
	.quad	15976
	.quad	.LC1503
	.quad	15944
	.quad	.LC1504
	.quad	16040
	.quad	.LC1505
	.quad	16008
	.quad	.LC1506
	.quad	16104
	.quad	.LC1507
	.quad	16072
	.quad	.LC1508
	.quad	16168
	.quad	.LC1509
	.quad	16136
	.quad	.LC1510
	.quad	16232
	.quad	.LC1511
	.quad	16200
	.quad	.LC1512
	.quad	16296
	.quad	.LC1513
	.quad	16264
	.quad	.LC1514
	.quad	16360
	.quad	.LC1515
	.quad	16328
	.quad	.LC1516
	.quad	16424
	.quad	.LC1517
	.quad	16392
	.quad	.LC1518
	.quad	16488
	.quad	.LC1519
	.quad	16456
	.quad	.LC1520
	.quad	16552
	.quad	.LC1521
	.quad	16520
	.quad	.LC1522
	.quad	16616
	.quad	.LC1523
	.quad	16584
	.quad	.LC1524
	.quad	16680
	.quad	.LC1525
	.quad	16648
	.quad	.LC1526
	.quad	16744
	.quad	.LC1527
	.quad	16712
	.quad	.LC1528
	.quad	16808
	.quad	.LC1529
	.quad	16776
	.quad	.LC1530
	.quad	16872
	.quad	.LC1531
	.quad	16840
	.quad	.LC1532
	.quad	16936
	.quad	.LC1533
	.quad	16904
	.quad	.LC1534
	.quad	17000
	.quad	.LC1535
	.quad	16968
	.quad	.LC1536
	.quad	17064
	.quad	.LC1537
	.quad	17032
	.quad	.LC1538
	.quad	17128
	.quad	.LC1539
	.quad	17096
	.quad	.LC1540
	.quad	17192
	.quad	.LC1541
	.quad	17160
	.quad	.LC1542
	.quad	17256
	.quad	.LC1543
	.quad	17224
	.quad	.LC1544
	.quad	17320
	.quad	.LC1545
	.quad	17288
	.quad	.LC1546
	.quad	17384
	.quad	.LC1547
	.quad	17352
	.quad	.LC1548
	.quad	17448
	.quad	.LC1549
	.quad	17416
	.quad	.LC1550
	.quad	17512
	.quad	.LC1551
	.quad	17480
	.quad	.LC1552
	.quad	17576
	.quad	.LC1553
	.quad	17544
	.quad	.LC1554
	.quad	17640
	.quad	.LC1555
	.quad	17608
	.quad	.LC1556
	.quad	17704
	.quad	.LC1557
	.quad	17672
	.quad	.LC1558
	.quad	17768
	.quad	.LC1559
	.quad	17736
	.quad	.LC1560
	.quad	17832
	.quad	.LC1561
	.quad	17800
	.quad	.LC1562
	.quad	17896
	.quad	.LC1563
	.quad	17864
	.quad	.LC1564
	.quad	17960
	.quad	.LC1565
	.quad	17928
	.quad	.LC1566
	.quad	18024
	.quad	.LC1567
	.quad	17992
	.quad	.LC1568
	.quad	18088
	.quad	.LC1569
	.quad	18056
	.quad	.LC1570
	.quad	18152
	.quad	.LC1571
	.quad	18120
	.quad	.LC1572
	.quad	18216
	.quad	.LC1573
	.quad	18184
	.quad	.LC1574
	.quad	18280
	.quad	.LC1575
	.quad	18248
	.quad	.LC1576
	.quad	18344
	.quad	.LC1577
	.quad	18312
	.quad	.LC1578
	.quad	18408
	.quad	.LC1579
	.quad	18376
	.quad	.LC1580
	.quad	18472
	.quad	.LC1581
	.quad	18440
	.quad	.LC1582
	.quad	18536
	.quad	.LC1583
	.quad	18504
	.quad	.LC1584
	.quad	18600
	.quad	.LC1585
	.quad	18568
	.quad	.LC1586
	.quad	18664
	.quad	.LC1587
	.quad	18632
	.quad	.LC1588
	.quad	18728
	.quad	.LC1589
	.quad	18696
	.quad	.LC1590
	.quad	18792
	.quad	.LC1591
	.quad	18760
	.quad	.LC1592
	.quad	18856
	.quad	.LC1593
	.quad	18824
	.quad	.LC1594
	.quad	18920
	.quad	.LC1595
	.quad	18888
	.quad	.LC1596
	.quad	18984
	.quad	.LC1597
	.quad	18952
	.quad	.LC1598
	.quad	19048
	.quad	.LC1599
	.quad	19016
	.quad	.LC1600
	.quad	19112
	.quad	.LC1601
	.quad	19080
	.quad	.LC1602
	.quad	19176
	.quad	.LC1603
	.quad	19144
	.quad	.LC1604
	.quad	19240
	.quad	.LC1605
	.quad	19208
	.quad	.LC1606
	.quad	19304
	.quad	.LC1607
	.quad	19272
	.quad	.LC1608
	.quad	19368
	.quad	.LC1609
	.quad	19336
	.quad	.LC1610
	.quad	19432
	.quad	.LC1611
	.quad	19400
	.quad	.LC1612
	.quad	19496
	.quad	.LC1613
	.quad	19464
	.quad	.LC1614
	.quad	19560
	.quad	.LC1615
	.quad	19528
	.quad	.LC1616
	.quad	19624
	.quad	.LC1617
	.quad	19592
	.quad	.LC1618
	.quad	19688
	.quad	.LC1619
	.quad	19656
	.quad	.LC1620
	.quad	19752
	.quad	.LC1621
	.quad	19720
	.quad	.LC1622
	.quad	19816
	.quad	.LC1623
	.quad	19784
	.quad	.LC1624
	.quad	19880
	.quad	.LC1625
	.quad	19848
	.quad	.LC1626
	.quad	19944
	.quad	.LC1627
	.quad	19912
	.quad	.LC1628
	.quad	20008
	.quad	.LC1629
	.quad	19976
	.quad	.LC1630
	.quad	20072
	.quad	.LC1631
	.quad	20040
	.quad	.LC1632
	.quad	20136
	.quad	.LC1633
	.quad	20104
	.quad	.LC1634
	.quad	20200
	.quad	.LC1635
	.quad	20168
	.quad	.LC1636
	.quad	20264
	.quad	.LC1637
	.quad	20232
	.quad	.LC1638
	.quad	20328
	.quad	.LC1639
	.quad	20296
	.quad	.LC1640
	.quad	20392
	.quad	.LC1641
	.quad	20360
	.quad	.LC1642
	.quad	20456
	.quad	.LC1643
	.quad	20424
	.quad	.LC1644
	.quad	20520
	.quad	.LC1645
	.quad	20488
	.quad	.LC1646
	.quad	20584
	.quad	.LC1647
	.quad	20552
	.quad	.LC1648
	.quad	20648
	.quad	.LC1649
	.quad	20616
	.quad	.LC1650
	.quad	20712
	.quad	.LC1651
	.quad	20680
	.quad	.LC1652
	.quad	20776
	.quad	.LC1653
	.quad	20744
	.quad	.LC1654
	.quad	20840
	.quad	.LC1655
	.quad	20808
	.quad	.LC1656
	.quad	20904
	.quad	.LC1657
	.quad	20872
	.quad	.LC1658
	.quad	20968
	.quad	.LC1659
	.quad	20936
	.quad	.LC1660
	.quad	21032
	.quad	.LC1661
	.quad	21000
	.quad	.LC1662
	.quad	21096
	.quad	.LC1663
	.quad	21064
	.quad	.LC1664
	.quad	21160
	.quad	.LC1665
	.quad	21128
	.quad	.LC1666
	.quad	21224
	.quad	.LC1667
	.quad	21192
	.quad	.LC1668
	.quad	21288
	.quad	.LC1669
	.quad	21256
	.quad	.LC1670
	.quad	21352
	.quad	.LC1671
	.quad	21320
	.quad	.LC1672
	.quad	21416
	.quad	.LC1673
	.quad	21384
	.quad	.LC1674
	.quad	21480
	.quad	.LC1675
	.quad	21448
	.quad	.LC1676
	.quad	21544
	.quad	.LC1677
	.quad	21512
	.quad	.LC1678
	.quad	21608
	.quad	.LC1679
	.quad	21576
	.quad	.LC1680
	.quad	21672
	.quad	.LC1681
	.quad	21640
	.quad	.LC1682
	.quad	21736
	.quad	.LC1683
	.quad	21704
	.quad	.LC1684
	.quad	21800
	.quad	.LC1685
	.quad	21768
	.quad	.LC1686
	.quad	21864
	.quad	.LC1687
	.quad	21832
	.quad	.LC1688
	.quad	21928
	.quad	.LC1689
	.quad	21896
	.quad	.LC1690
	.quad	21992
	.quad	.LC1691
	.quad	21960
	.quad	.LC1692
	.quad	22056
	.quad	.LC1693
	.quad	22024
	.quad	.LC1694
	.quad	22120
	.quad	.LC1695
	.quad	22088
	.quad	.LC1696
	.quad	22184
	.quad	.LC1697
	.quad	22152
	.quad	.LC1698
	.quad	22248
	.quad	.LC1699
	.quad	22216
	.quad	.LC1700
	.quad	22312
	.quad	.LC1701
	.quad	22280
	.quad	.LC1702
	.quad	22376
	.quad	.LC1703
	.quad	22344
	.quad	.LC1704
	.quad	22440
	.quad	.LC1705
	.quad	22408
	.quad	.LC1706
	.quad	22504
	.quad	.LC1707
	.quad	22472
	.quad	.LC1708
	.quad	22568
	.quad	.LC1709
	.quad	22536
	.quad	.LC1710
	.quad	22632
	.quad	.LC1711
	.quad	22600
	.quad	.LC1712
	.quad	22696
	.quad	.LC1713
	.quad	22664
	.quad	.LC1714
	.quad	22760
	.quad	.LC1715
	.quad	22728
	.quad	.LC1716
	.quad	22824
	.quad	.LC1717
	.quad	22792
	.quad	.LC1718
	.quad	22888
	.quad	.LC1719
	.quad	22856
	.quad	.LC1720
	.quad	22952
	.quad	.LC1721
	.quad	22920
	.quad	.LC1722
	.quad	23016
	.quad	.LC1723
	.quad	22984
	.quad	.LC1724
	.quad	23080
	.quad	.LC1725
	.quad	23048
	.quad	.LC1726
	.quad	23144
	.quad	.LC1727
	.quad	23112
	.quad	.LC1728
	.quad	23208
	.quad	.LC1729
	.quad	23176
	.quad	.LC1730
	.section	.rodata.str1.1
.LC1731:
	.string	"V8.GCCompactor"
.LC1732:
	.string	"V8.GCCompactorBackground"
.LC1733:
	.string	"V8.GCCompactorForeground"
.LC1734:
	.string	"V8.GCFinalizeMC"
.LC1735:
	.string	"V8.GCFinalizeMCBackground"
.LC1736:
	.string	"V8.GCFinalizeMCForeground"
.LC1737:
	.string	"V8.GCFinalizeMCReduceMemory"
	.section	.rodata.str1.8
	.align 8
.LC1738:
	.string	"V8.GCFinalizeMCReduceMemoryBackground"
	.align 8
.LC1739:
	.string	"V8.GCFinalizeMCReduceMemoryForeground"
	.section	.rodata.str1.1
.LC1740:
	.string	"V8.GCScavenger"
.LC1741:
	.string	"V8.GCScavengerBackground"
.LC1742:
	.string	"V8.GCScavengerForeground"
.LC1743:
	.string	"V8.TurboFanOptimizePrepare"
.LC1744:
	.string	"V8.TurboFanOptimizeExecute"
.LC1745:
	.string	"V8.TurboFanOptimizeFinalize"
	.section	.rodata.str1.8
	.align 8
.LC1746:
	.string	"V8.TurboFanOptimizeTotalForeground"
	.align 8
.LC1747:
	.string	"V8.TurboFanOptimizeTotalBackground"
	.section	.rodata.str1.1
.LC1748:
	.string	"V8.TurboFanOptimizeTotalTime"
	.section	.rodata.str1.8
	.align 8
.LC1749:
	.string	"V8.TurboFanOptimizeNonConcurrentTotalTime"
	.align 8
.LC1750:
	.string	"V8.TurboFanOptimizeConcurrentTotalTime"
	.align 8
.LC1751:
	.string	"V8.TurboFanOptimizeForOnStackReplacementPrepare"
	.align 8
.LC1752:
	.string	"V8.TurboFanOptimizeForOnStackReplacementExecute"
	.align 8
.LC1753:
	.string	"V8.TurboFanOptimizeForOnStackReplacementFinalize"
	.align 8
.LC1754:
	.string	"V8.TurboFanOptimizeForOnStackReplacementTotalTime"
	.align 8
.LC1755:
	.string	"V8.WasmDecodeModuleMicroSeconds.asm"
	.align 8
.LC1756:
	.string	"V8.WasmDecodeModuleMicroSeconds.wasm"
	.align 8
.LC1757:
	.string	"V8.WasmDecodeFunctionMicroSeconds.asm"
	.align 8
.LC1758:
	.string	"V8.WasmDecodeFunctionMicroSeconds.wasm"
	.align 8
.LC1759:
	.string	"V8.WasmCompileModuleMicroSeconds.asm"
	.align 8
.LC1760:
	.string	"V8.WasmCompileModuleMicroSeconds.wasm"
	.align 8
.LC1761:
	.string	"V8.WasmCompileModuleAsyncMicroSeconds"
	.align 8
.LC1762:
	.string	"V8.WasmCompileModuleStreamingMicroSeconds"
	.align 8
.LC1763:
	.string	"V8.WasmDeserializeModuleStreamingMicroSeconds"
	.align 8
.LC1764:
	.string	"V8.WasmTierUpModuleMicroSeconds"
	.align 8
.LC1765:
	.string	"V8.WasmCompileFunctionMicroSeconds.asm"
	.align 8
.LC1766:
	.string	"V8.WasmCompileFunctionMicroSeconds.wasm"
	.section	.rodata.str1.1
.LC1767:
	.string	"V8.LiftoffCompileMicroSeconds"
	.section	.rodata.str1.8
	.align 8
.LC1768:
	.string	"V8.WasmInstantiateModuleMicroSeconds.wasm"
	.align 8
.LC1769:
	.string	"V8.WasmInstantiateModuleMicroSeconds.asm"
	.section	.rodata.str1.1
.LC1770:
	.string	"V8.WasmCodeGCTime"
	.section	.rodata.str1.8
	.align 8
.LC1771:
	.string	"V8.CompileScriptMicroSeconds.ProduceCache"
	.align 8
.LC1772:
	.string	"V8.CompileScriptMicroSeconds.IsolateCacheHit"
	.align 8
.LC1773:
	.string	"V8.CompileScriptMicroSeconds.ConsumeCache"
	.align 8
.LC1774:
	.string	"V8.CompileScriptMicroSeconds.ConsumeCache.Failed"
	.align 8
.LC1775:
	.string	"V8.CompileScriptMicroSeconds.NoCache.Other"
	.align 8
.LC1776:
	.string	"V8.CompileScriptMicroSeconds.NoCache.InlineScript"
	.align 8
.LC1777:
	.string	"V8.CompileScriptMicroSeconds.NoCache.ScriptTooSmall"
	.align 8
.LC1778:
	.string	"V8.CompileScriptMicroSeconds.NoCache.CacheTooCold"
	.align 8
.LC1779:
	.string	"V8.CompileScriptMicroSeconds.BackgroundThread"
	.align 8
.LC1780:
	.string	"V8.CompileFunctionMicroSeconds.BackgroundThread"
	.section	.data.rel.ro.local._ZZN2v88internal8CountersC4EPNS0_7IsolateEE16kTimedHistograms,"aw"
	.align 32
	.type	_ZZN2v88internal8CountersC4EPNS0_7IsolateEE16kTimedHistograms, @object
	.size	_ZZN2v88internal8CountersC4EPNS0_7IsolateEE16kTimedHistograms, 1200
_ZZN2v88internal8CountersC4EPNS0_7IsolateEE16kTimedHistograms:
	.quad	3048
	.quad	.LC1731
	.long	10000
	.long	0
	.quad	3096
	.quad	.LC1732
	.long	10000
	.long	0
	.quad	3144
	.quad	.LC1733
	.long	10000
	.long	0
	.quad	3192
	.quad	.LC1734
	.long	10000
	.long	0
	.quad	3240
	.quad	.LC1735
	.long	10000
	.long	0
	.quad	3288
	.quad	.LC1736
	.long	10000
	.long	0
	.quad	3336
	.quad	.LC1737
	.long	10000
	.long	0
	.quad	3384
	.quad	.LC1738
	.long	10000
	.long	0
	.quad	3432
	.quad	.LC1739
	.long	10000
	.long	0
	.quad	3480
	.quad	.LC1740
	.long	10000
	.long	0
	.quad	3528
	.quad	.LC1741
	.long	10000
	.long	0
	.quad	3576
	.quad	.LC1742
	.long	10000
	.long	0
	.quad	3624
	.quad	.LC1743
	.long	1000000
	.long	1
	.quad	3672
	.quad	.LC1744
	.long	1000000
	.long	1
	.quad	3720
	.quad	.LC1745
	.long	1000000
	.long	1
	.quad	3768
	.quad	.LC1746
	.long	10000000
	.long	1
	.quad	3816
	.quad	.LC1747
	.long	10000000
	.long	1
	.quad	3864
	.quad	.LC1748
	.long	10000000
	.long	1
	.quad	3912
	.quad	.LC1749
	.long	10000000
	.long	1
	.quad	3960
	.quad	.LC1750
	.long	10000000
	.long	1
	.quad	4008
	.quad	.LC1751
	.long	1000000
	.long	1
	.quad	4056
	.quad	.LC1752
	.long	1000000
	.long	1
	.quad	4104
	.quad	.LC1753
	.long	1000000
	.long	1
	.quad	4152
	.quad	.LC1754
	.long	10000000
	.long	1
	.quad	4200
	.quad	.LC1755
	.long	1000000
	.long	1
	.quad	4248
	.quad	.LC1756
	.long	1000000
	.long	1
	.quad	4296
	.quad	.LC1757
	.long	1000000
	.long	1
	.quad	4344
	.quad	.LC1758
	.long	1000000
	.long	1
	.quad	4392
	.quad	.LC1759
	.long	10000000
	.long	1
	.quad	4440
	.quad	.LC1760
	.long	10000000
	.long	1
	.quad	4488
	.quad	.LC1761
	.long	100000000
	.long	1
	.quad	4536
	.quad	.LC1762
	.long	100000000
	.long	1
	.quad	4584
	.quad	.LC1763
	.long	100000000
	.long	1
	.quad	4632
	.quad	.LC1764
	.long	100000000
	.long	1
	.quad	4680
	.quad	.LC1765
	.long	1000000
	.long	1
	.quad	4728
	.quad	.LC1766
	.long	1000000
	.long	1
	.quad	4776
	.quad	.LC1767
	.long	10000000
	.long	1
	.quad	4824
	.quad	.LC1768
	.long	10000000
	.long	1
	.quad	4872
	.quad	.LC1769
	.long	10000000
	.long	1
	.quad	4920
	.quad	.LC1770
	.long	1000000
	.long	1
	.quad	4968
	.quad	.LC1771
	.long	1000000
	.long	1
	.quad	5016
	.quad	.LC1772
	.long	1000000
	.long	1
	.quad	5064
	.quad	.LC1773
	.long	1000000
	.long	1
	.quad	5112
	.quad	.LC1774
	.long	1000000
	.long	1
	.quad	5160
	.quad	.LC1775
	.long	1000000
	.long	1
	.quad	5208
	.quad	.LC1776
	.long	1000000
	.long	1
	.quad	5256
	.quad	.LC1777
	.long	1000000
	.long	1
	.quad	5304
	.quad	.LC1778
	.long	1000000
	.long	1
	.quad	5352
	.quad	.LC1779
	.long	1000000
	.long	1
	.quad	5400
	.quad	.LC1780
	.long	1000000
	.long	1
	.section	.rodata.str1.1
.LC1781:
	.string	"V8.GCBackgroundMarking"
.LC1782:
	.string	"V8.GCBackgroundScavenger"
.LC1783:
	.string	"V8.GCBackgroundSweeping"
.LC1784:
	.string	"V8.CodeCacheRejectReason"
.LC1785:
	.string	"V8.ErrorsThrownPerContext"
.LC1786:
	.string	"V8.DebugFeatureUsage"
.LC1787:
	.string	"V8.GCIncrementalMarkingReason"
.LC1788:
	.string	"V8.GCIncrementalMarkingSum"
.LC1789:
	.string	"V8.GCMarkCompactReason"
.LC1790:
	.string	"V8.GCFinalizeMC.Clear"
.LC1791:
	.string	"V8.GCFinalizeMC.Epilogue"
.LC1792:
	.string	"V8.GCFinalizeMC.Evacuate"
.LC1793:
	.string	"V8.GCFinalizeMC.Finish"
.LC1794:
	.string	"V8.GCFinalizeMC.Mark"
.LC1795:
	.string	"V8.GCFinalizeMC.Prologue"
.LC1796:
	.string	"V8.GCFinalizeMC.Sweep"
.LC1797:
	.string	"V8.GCScavenger.ScavengeMain"
.LC1798:
	.string	"V8.GCScavenger.ScavengeRoots"
.LC1799:
	.string	"V8.GCMarkCompactor"
.LC1800:
	.string	"V8.GCMarkingSum"
	.section	.rodata.str1.8
	.align 8
.LC1801:
	.string	"V8.GCMainThreadMarkingThroughput"
	.section	.rodata.str1.1
.LC1802:
	.string	"V8.GCScavengeReason"
.LC1803:
	.string	"V8.GCYoungGenerationHandling"
.LC1804:
	.string	"V8.WasmFunctionsPerModule.asm"
	.section	.rodata.str1.8
	.align 8
.LC1805:
	.string	"V8.WasmFunctionsPerModule.wasm"
	.align 8
.LC1806:
	.string	"V8.ArrayBufferLargeAllocations"
	.section	.rodata.str1.1
.LC1807:
	.string	"V8.ArrayBufferNewSizeFailures"
.LC1808:
	.string	"V8.SharedArrayAllocationSizes"
.LC1809:
	.string	"V8.WasmFunctionSizeBytes.asm"
.LC1810:
	.string	"V8.WasmFunctionSizeBytes.wasm"
.LC1811:
	.string	"V8.WasmModuleSizeBytes.asm"
.LC1812:
	.string	"V8.WasmModuleSizeBytes.wasm"
.LC1813:
	.string	"V8.WasmMinMemPagesCount.asm"
.LC1814:
	.string	"V8.WasmMinMemPagesCount.wasm"
.LC1815:
	.string	"V8.WasmMaxMemPagesCount.wasm"
	.section	.rodata.str1.8
	.align 8
.LC1816:
	.string	"V8.WasmDecodeModulePeakMemoryBytes.asm"
	.align 8
.LC1817:
	.string	"V8.WasmDecodeModulePeakMemoryBytes.wasm"
	.align 8
.LC1818:
	.string	"V8.AsmWasmTranslationPeakMemoryBytes"
	.align 8
.LC1819:
	.string	"V8.WasmCompileFunctionPeakMemoryBytes"
	.section	.rodata.str1.1
.LC1820:
	.string	"V8.AsmModuleSizeBytes"
	.section	.rodata.str1.8
	.align 8
.LC1821:
	.string	"V8.AsmWasmTranslationThroughput"
	.align 8
.LC1822:
	.string	"V8.WasmLazyCompilationThroughput"
	.align 8
.LC1823:
	.string	"V8.CompileScript.CacheBehaviour"
	.section	.rodata.str1.1
.LC1824:
	.string	"V8.WasmMemoryAllocationResult"
.LC1825:
	.string	"V8.WasmAddressSpaceUsageMiB"
.LC1826:
	.string	"V8.WasmModuleCodeSizeMiB"
	.section	.rodata.str1.8
	.align 8
.LC1827:
	.string	"V8.WasmModuleCodeSizeBaselineMiB"
	.align 8
.LC1828:
	.string	"V8.WasmModuleCodeSizeTopTierMiB"
	.section	.rodata.str1.1
.LC1829:
	.string	"V8.WasmModuleCodeSizeFreed"
	.section	.rodata.str1.8
	.align 8
.LC1830:
	.string	"V8.WasmModuleCodeSizePercentFreed"
	.align 8
.LC1831:
	.string	"V8.WasmModuleNumberOfCodeGCsTriggered"
	.align 8
.LC1832:
	.string	"V8.WasmModuleNumberOfCodeSpaces"
	.section	.rodata.str1.1
.LC1833:
	.string	"V8.LiftoffBailoutReasons"
.LC1834:
	.string	"V8.TurboFan1KTicks"
	.section	.data.rel.ro.local._ZZN2v88internal8CountersC4EPNS0_7IsolateEE11kHistograms,"aw"
	.align 32
	.type	_ZZN2v88internal8CountersC4EPNS0_7IsolateEE11kHistograms, @object
	.size	_ZZN2v88internal8CountersC4EPNS0_7IsolateEE11kHistograms, 1728
_ZZN2v88internal8CountersC4EPNS0_7IsolateEE11kHistograms:
	.quad	48
	.quad	.LC1781
	.long	0
	.long	10000
	.long	101
	.zero	4
	.quad	88
	.quad	.LC1782
	.long	0
	.long	10000
	.long	101
	.zero	4
	.quad	128
	.quad	.LC1783
	.long	0
	.long	10000
	.long	101
	.zero	4
	.quad	168
	.quad	.LC1784
	.long	1
	.long	6
	.long	6
	.zero	4
	.quad	208
	.quad	.LC1785
	.long	0
	.long	200
	.long	20
	.zero	4
	.quad	248
	.quad	.LC1786
	.long	1
	.long	7
	.long	7
	.zero	4
	.quad	288
	.quad	.LC1787
	.long	0
	.long	22
	.long	23
	.zero	4
	.quad	328
	.quad	.LC1788
	.long	0
	.long	10000
	.long	101
	.zero	4
	.quad	368
	.quad	.LC1789
	.long	0
	.long	22
	.long	23
	.zero	4
	.quad	408
	.quad	.LC1790
	.long	0
	.long	10000
	.long	101
	.zero	4
	.quad	448
	.quad	.LC1791
	.long	0
	.long	10000
	.long	101
	.zero	4
	.quad	488
	.quad	.LC1792
	.long	0
	.long	10000
	.long	101
	.zero	4
	.quad	528
	.quad	.LC1793
	.long	0
	.long	10000
	.long	101
	.zero	4
	.quad	568
	.quad	.LC1794
	.long	0
	.long	10000
	.long	101
	.zero	4
	.quad	608
	.quad	.LC1795
	.long	0
	.long	10000
	.long	101
	.zero	4
	.quad	648
	.quad	.LC1796
	.long	0
	.long	10000
	.long	101
	.zero	4
	.quad	688
	.quad	.LC1797
	.long	0
	.long	10000
	.long	101
	.zero	4
	.quad	728
	.quad	.LC1798
	.long	0
	.long	10000
	.long	101
	.zero	4
	.quad	768
	.quad	.LC1799
	.long	0
	.long	10000
	.long	101
	.zero	4
	.quad	808
	.quad	.LC1800
	.long	0
	.long	10000
	.long	101
	.zero	4
	.quad	848
	.quad	.LC1801
	.long	0
	.long	100000
	.long	50
	.zero	4
	.quad	888
	.quad	.LC1802
	.long	0
	.long	22
	.long	23
	.zero	4
	.quad	928
	.quad	.LC1803
	.long	0
	.long	2
	.long	3
	.zero	4
	.quad	968
	.quad	.LC1804
	.long	1
	.long	1000000
	.long	51
	.zero	4
	.quad	1008
	.quad	.LC1805
	.long	1
	.long	1000000
	.long	51
	.zero	4
	.quad	1048
	.quad	.LC1806
	.long	0
	.long	4096
	.long	13
	.zero	4
	.quad	1088
	.quad	.LC1807
	.long	0
	.long	4096
	.long	13
	.zero	4
	.quad	1128
	.quad	.LC1808
	.long	0
	.long	4096
	.long	13
	.zero	4
	.quad	1168
	.quad	.LC1809
	.long	1
	.long	1073741824
	.long	51
	.zero	4
	.quad	1208
	.quad	.LC1810
	.long	1
	.long	1073741824
	.long	51
	.zero	4
	.quad	1248
	.quad	.LC1811
	.long	1
	.long	1073741824
	.long	51
	.zero	4
	.quad	1288
	.quad	.LC1812
	.long	1
	.long	1073741824
	.long	51
	.zero	4
	.quad	1328
	.quad	.LC1813
	.long	1
	.long	131072
	.long	51
	.zero	4
	.quad	1368
	.quad	.LC1814
	.long	1
	.long	131072
	.long	51
	.zero	4
	.quad	1408
	.quad	.LC1815
	.long	1
	.long	131072
	.long	51
	.zero	4
	.quad	1448
	.quad	.LC1816
	.long	1
	.long	1073741824
	.long	51
	.zero	4
	.quad	1488
	.quad	.LC1817
	.long	1
	.long	1073741824
	.long	51
	.zero	4
	.quad	1528
	.quad	.LC1818
	.long	1
	.long	1073741824
	.long	51
	.zero	4
	.quad	1568
	.quad	.LC1819
	.long	1
	.long	1073741824
	.long	51
	.zero	4
	.quad	1608
	.quad	.LC1820
	.long	1
	.long	1073741824
	.long	51
	.zero	4
	.quad	1648
	.quad	.LC1821
	.long	1
	.long	100
	.long	20
	.zero	4
	.quad	1688
	.quad	.LC1822
	.long	1
	.long	10000
	.long	50
	.zero	4
	.quad	1728
	.quad	.LC1823
	.long	0
	.long	20
	.long	21
	.zero	4
	.quad	1768
	.quad	.LC1824
	.long	0
	.long	3
	.long	4
	.zero	4
	.quad	1808
	.quad	.LC1825
	.long	0
	.long	1048576
	.long	128
	.zero	4
	.quad	1848
	.quad	.LC1826
	.long	0
	.long	1024
	.long	64
	.zero	4
	.quad	1888
	.quad	.LC1827
	.long	0
	.long	1024
	.long	64
	.zero	4
	.quad	1928
	.quad	.LC1828
	.long	0
	.long	1024
	.long	64
	.zero	4
	.quad	1968
	.quad	.LC1829
	.long	0
	.long	1024
	.long	64
	.zero	4
	.quad	2008
	.quad	.LC1830
	.long	0
	.long	100
	.long	32
	.zero	4
	.quad	2048
	.quad	.LC1831
	.long	1
	.long	128
	.long	20
	.zero	4
	.quad	2088
	.quad	.LC1832
	.long	1
	.long	128
	.long	20
	.zero	4
	.quad	2128
	.quad	.LC1833
	.long	0
	.long	20
	.long	21
	.zero	4
	.quad	2168
	.quad	.LC1834
	.long	0
	.long	100000
	.long	200
	.zero	4
	.globl	_ZN2v88internal12TracingFlags8ic_statsE
	.section	.bss._ZN2v88internal12TracingFlags8ic_statsE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal12TracingFlags8ic_statsE, @object
	.size	_ZN2v88internal12TracingFlags8ic_statsE, 4
_ZN2v88internal12TracingFlags8ic_statsE:
	.zero	4
	.globl	_ZN2v88internal12TracingFlags8gc_statsE
	.section	.bss._ZN2v88internal12TracingFlags8gc_statsE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal12TracingFlags8gc_statsE, @object
	.size	_ZN2v88internal12TracingFlags8gc_statsE, 4
_ZN2v88internal12TracingFlags8gc_statsE:
	.zero	4
	.globl	_ZN2v88internal12TracingFlags2gcE
	.section	.bss._ZN2v88internal12TracingFlags2gcE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal12TracingFlags2gcE, @object
	.size	_ZN2v88internal12TracingFlags2gcE, 4
_ZN2v88internal12TracingFlags2gcE:
	.zero	4
	.globl	_ZN2v88internal12TracingFlags13runtime_statsE
	.section	.bss._ZN2v88internal12TracingFlags13runtime_statsE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal12TracingFlags13runtime_statsE, @object
	.size	_ZN2v88internal12TracingFlags13runtime_statsE, 4
_ZN2v88internal12TracingFlags13runtime_statsE:
	.zero	4
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC31:
	.long	0
	.long	1083129856
	.align 8
.LC36:
	.long	0
	.long	1079574528
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC43:
	.long	0
	.long	1079574528
	.long	0
	.long	1079574528
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
