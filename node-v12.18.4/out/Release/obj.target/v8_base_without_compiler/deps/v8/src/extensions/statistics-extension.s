	.file	"statistics-extension.cc"
	.text
	.section	.text._ZN2v89ExtensionD2Ev,"axG",@progbits,_ZN2v89ExtensionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v89ExtensionD2Ev
	.type	_ZN2v89ExtensionD2Ev, @function
_ZN2v89ExtensionD2Ev:
.LFB2485:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v89ExtensionE(%rip), %rax
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L1:
	ret
	.cfi_endproc
.LFE2485:
	.size	_ZN2v89ExtensionD2Ev, .-_ZN2v89ExtensionD2Ev
	.weak	_ZN2v89ExtensionD1Ev
	.set	_ZN2v89ExtensionD1Ev,_ZN2v89ExtensionD2Ev
	.section	.text._ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE,"axG",@progbits,_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.type	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, @function
_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE:
.LFB2488:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2488:
	.size	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, .-_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.section	.text._ZN2v88internal5Space15CommittedMemoryEv,"axG",@progbits,_ZN2v88internal5Space15CommittedMemoryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal5Space15CommittedMemoryEv
	.type	_ZN2v88internal5Space15CommittedMemoryEv, @function
_ZN2v88internal5Space15CommittedMemoryEv:
.LFB9216:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE9216:
	.size	_ZN2v88internal5Space15CommittedMemoryEv, .-_ZN2v88internal5Space15CommittedMemoryEv
	.section	.text._ZN2v88internal10PagedSpace9AvailableEv,"axG",@progbits,_ZN2v88internal10PagedSpace9AvailableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace9AvailableEv
	.type	_ZN2v88internal10PagedSpace9AvailableEv, @function
_ZN2v88internal10PagedSpace9AvailableEv:
.LFB9394:
	.cfi_startproc
	endbr64
	movq	96(%rdi), %rax
	movq	40(%rax), %rax
	ret
	.cfi_endproc
.LFE9394:
	.size	_ZN2v88internal10PagedSpace9AvailableEv, .-_ZN2v88internal10PagedSpace9AvailableEv
	.section	.text._ZN2v88internal10PagedSpace4SizeEv,"axG",@progbits,_ZN2v88internal10PagedSpace4SizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace4SizeEv
	.type	_ZN2v88internal10PagedSpace4SizeEv, @function
_ZN2v88internal10PagedSpace4SizeEv:
.LFB9395:
	.cfi_startproc
	endbr64
	movq	184(%rdi), %rax
	ret
	.cfi_endproc
.LFE9395:
	.size	_ZN2v88internal10PagedSpace4SizeEv, .-_ZN2v88internal10PagedSpace4SizeEv
	.section	.text._ZN2v88internal8NewSpace15CommittedMemoryEv,"axG",@progbits,_ZN2v88internal8NewSpace15CommittedMemoryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8NewSpace15CommittedMemoryEv
	.type	_ZN2v88internal8NewSpace15CommittedMemoryEv, @function
_ZN2v88internal8NewSpace15CommittedMemoryEv:
.LFB9458:
	.cfi_startproc
	endbr64
	movq	448(%rdi), %rax
	addq	288(%rdi), %rax
	ret
	.cfi_endproc
.LFE9458:
	.size	_ZN2v88internal8NewSpace15CommittedMemoryEv, .-_ZN2v88internal8NewSpace15CommittedMemoryEv
	.section	.text._ZN2v88internal16LargeObjectSpace4SizeEv,"axG",@progbits,_ZN2v88internal16LargeObjectSpace4SizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16LargeObjectSpace4SizeEv
	.type	_ZN2v88internal16LargeObjectSpace4SizeEv, @function
_ZN2v88internal16LargeObjectSpace4SizeEv:
.LFB9515:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rax
	ret
	.cfi_endproc
.LFE9515:
	.size	_ZN2v88internal16LargeObjectSpace4SizeEv, .-_ZN2v88internal16LargeObjectSpace4SizeEv
	.section	.text._ZN2v88internal19StatisticsExtensionD2Ev,"axG",@progbits,_ZN2v88internal19StatisticsExtensionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19StatisticsExtensionD2Ev
	.type	_ZN2v88internal19StatisticsExtensionD2Ev, @function
_ZN2v88internal19StatisticsExtensionD2Ev:
.LFB24909:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v89ExtensionE(%rip), %rax
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L10
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L10:
	ret
	.cfi_endproc
.LFE24909:
	.size	_ZN2v88internal19StatisticsExtensionD2Ev, .-_ZN2v88internal19StatisticsExtensionD2Ev
	.weak	_ZN2v88internal19StatisticsExtensionD1Ev
	.set	_ZN2v88internal19StatisticsExtensionD1Ev,_ZN2v88internal19StatisticsExtensionD2Ev
	.section	.text._ZN2v88internal19StatisticsExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19StatisticsExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.type	_ZN2v88internal19StatisticsExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, @function
_ZN2v88internal19StatisticsExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE:
.LFB20019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	_ZN2v88internal19StatisticsExtension11GetCountersERKNS_20FunctionCallbackInfoINS_5ValueEEE(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20019:
	.size	_ZN2v88internal19StatisticsExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, .-_ZN2v88internal19StatisticsExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.section	.text._ZN2v88internal8NewSpace4SizeEv,"axG",@progbits,_ZN2v88internal8NewSpace4SizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8NewSpace4SizeEv
	.type	_ZN2v88internal8NewSpace4SizeEv, @function
_ZN2v88internal8NewSpace4SizeEv:
.LFB9454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movslq	360(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	movq	352(%rbx), %rcx
	movq	104(%rbx), %rdx
	imulq	%rax, %r12
	popq	%rbx
	subq	40(%rcx), %rdx
	leaq	(%rdx,%r12), %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9454:
	.size	_ZN2v88internal8NewSpace4SizeEv, .-_ZN2v88internal8NewSpace4SizeEv
	.section	.text._ZN2v88internal19StatisticsExtensionD0Ev,"axG",@progbits,_ZN2v88internal19StatisticsExtensionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19StatisticsExtensionD0Ev
	.type	_ZN2v88internal19StatisticsExtensionD0Ev, @function
_ZN2v88internal19StatisticsExtensionD0Ev:
.LFB24911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v89ExtensionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L17
	movq	(%rdi), %rax
	call	*8(%rax)
.L17:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24911:
	.size	_ZN2v88internal19StatisticsExtensionD0Ev, .-_ZN2v88internal19StatisticsExtensionD0Ev
	.section	.text._ZN2v89ExtensionD0Ev,"axG",@progbits,_ZN2v89ExtensionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v89ExtensionD0Ev
	.type	_ZN2v89ExtensionD0Ev, @function
_ZN2v89ExtensionD0Ev:
.LFB2487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v89ExtensionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE2487:
	.size	_ZN2v89ExtensionD0Ev, .-_ZN2v89ExtensionD0Ev
	.section	.text._ZN2v88internal8NewSpace9AvailableEv,"axG",@progbits,_ZN2v88internal8NewSpace9AvailableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8NewSpace9AvailableEv
	.type	_ZN2v88internal8NewSpace9AvailableEv, @function
_ZN2v88internal8NewSpace9AvailableEv:
.LFB9460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	312(%rdi), %rbx
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	leaq	_ZN2v88internal8NewSpace4SizeEv(%rip), %rdx
	shrq	$18, %rbx
	imulq	%rax, %rbx
	movq	(%r12), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L29
	movslq	360(%r12), %r13
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	movq	352(%r12), %rdx
	movq	%rax, %r8
	movq	104(%r12), %rax
	imulq	%r8, %r13
	subq	40(%rdx), %rax
	leaq	(%rax,%r13), %rax
.L30:
	addq	$8, %rsp
	subq	%rax, %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L30
	.cfi_endproc
.LFE9460:
	.size	_ZN2v88internal8NewSpace9AvailableEv, .-_ZN2v88internal8NewSpace9AvailableEv
	.section	.rodata._ZN2v88internal19StatisticsExtension11GetCountersERKNS_20FunctionCallbackInfoINS_5ValueEEE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"global_handles"
.LC2:
	.string	"memory_allocated"
.LC3:
	.string	"maps_normalized"
.LC4:
	.string	"maps_created"
.LC5:
	.string	"elements_transitions"
.LC6:
	.string	"props_to_dictionary"
.LC7:
	.string	"elements_to_dictionary"
.LC8:
	.string	"alive_after_last_gc"
.LC9:
	.string	"objs_since_last_young"
.LC10:
	.string	"objs_since_last_full"
.LC11:
	.string	"string_table_capacity"
.LC12:
	.string	"number_of_symbols"
.LC13:
	.string	"inlined_copied_elements"
.LC14:
	.string	"compilation_cache_hits"
.LC15:
	.string	"compilation_cache_misses"
.LC16:
	.string	"total_eval_size"
.LC17:
	.string	"total_load_size"
.LC18:
	.string	"total_parse_size"
.LC19:
	.string	"total_preparse_skipped"
.LC20:
	.string	"total_compile_size"
.LC21:
	.string	"contexts_created_from_scratch"
.LC22:
	.string	"contexts_created_by_snapshot"
.LC23:
	.string	"pc_to_code"
.LC24:
	.string	"pc_to_code_cached"
.LC25:
	.string	"store_buffer_overflows"
.LC26:
	.string	"total_compiled_code_size"
	.section	.rodata._ZN2v88internal19StatisticsExtension11GetCountersERKNS_20FunctionCallbackInfoINS_5ValueEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"gc_compactor_caused_by_request"
	.align 8
.LC28:
	.string	"gc_compactor_caused_by_promoted_data"
	.align 8
.LC29:
	.string	"gc_compactor_caused_by_oldspace_exhaustion"
	.section	.rodata._ZN2v88internal19StatisticsExtension11GetCountersERKNS_20FunctionCallbackInfoINS_5ValueEEE.str1.1
.LC30:
	.string	"gc_last_resort_from_js"
.LC31:
	.string	"gc_last_resort_from_handles"
.LC32:
	.string	"cow_arrays_converted"
.LC33:
	.string	"constructed_objects_runtime"
	.section	.rodata._ZN2v88internal19StatisticsExtension11GetCountersERKNS_20FunctionCallbackInfoINS_5ValueEEE.str1.8
	.align 8
.LC34:
	.string	"megamorphic_stub_cache_updates"
	.section	.rodata._ZN2v88internal19StatisticsExtension11GetCountersERKNS_20FunctionCallbackInfoINS_5ValueEEE.str1.1
.LC35:
	.string	"enum_cache_hits"
.LC36:
	.string	"enum_cache_misses"
.LC37:
	.string	"string_add_runtime"
.LC38:
	.string	"sub_string_runtime"
.LC39:
	.string	"regexp_entry_runtime"
.LC40:
	.string	"stack_interrupts"
.LC41:
	.string	"runtime_profiler_ticks"
.LC42:
	.string	"soft_deopts_executed"
.LC43:
	.string	"new_space_bytes_available"
.LC44:
	.string	"new_space_bytes_committed"
.LC45:
	.string	"new_space_bytes_used"
.LC46:
	.string	"old_space_bytes_available"
.LC47:
	.string	"old_space_bytes_committed"
.LC48:
	.string	"old_space_bytes_used"
.LC49:
	.string	"code_space_bytes_available"
.LC50:
	.string	"code_space_bytes_committed"
.LC51:
	.string	"code_space_bytes_used"
.LC52:
	.string	"map_space_bytes_available"
.LC53:
	.string	"map_space_bytes_committed"
.LC54:
	.string	"map_space_bytes_used"
.LC55:
	.string	"lo_space_bytes_available"
.LC56:
	.string	"lo_space_bytes_committed"
.LC57:
	.string	"lo_space_bytes_used"
.LC58:
	.string	"total_baseline_code_size"
.LC59:
	.string	"total_baseline_compile_count"
.LC60:
	.string	"write_barriers"
.LC61:
	.string	"constructed_objects"
.LC62:
	.string	"fast_new_closure_total"
.LC63:
	.string	"regexp_entry_native"
.LC64:
	.string	"string_add_native"
.LC65:
	.string	"sub_string_native"
.LC66:
	.string	"ic_keyed_load_generic_smi"
.LC67:
	.string	"ic_keyed_load_generic_symbol"
.LC68:
	.string	"ic_keyed_load_generic_slow"
.LC69:
	.string	"megamorphic_stub_cache_probes"
.LC70:
	.string	"megamorphic_stub_cache_misses"
.LC71:
	.string	"total_committed_bytes"
.LC72:
	.string	"new_space_live_bytes"
.LC73:
	.string	"new_space_available_bytes"
.LC74:
	.string	"new_space_commited_bytes"
.LC75:
	.string	"old_space_live_bytes"
.LC76:
	.string	"old_space_available_bytes"
.LC77:
	.string	"old_space_commited_bytes"
.LC78:
	.string	"code_space_live_bytes"
.LC79:
	.string	"code_space_available_bytes"
.LC80:
	.string	"code_space_commited_bytes"
.LC81:
	.string	"lo_space_live_bytes"
.LC82:
	.string	"lo_space_available_bytes"
.LC83:
	.string	"lo_space_commited_bytes"
.LC84:
	.string	"code_lo_space_live_bytes"
.LC85:
	.string	"code_lo_space_available_bytes"
.LC86:
	.string	"code_lo_space_commited_bytes"
	.section	.rodata._ZN2v88internal19StatisticsExtension11GetCountersERKNS_20FunctionCallbackInfoINS_5ValueEEE.str1.8
	.align 8
.LC87:
	.string	"amount_of_external_allocated_memory"
	.section	.rodata._ZN2v88internal19StatisticsExtension11GetCountersERKNS_20FunctionCallbackInfoINS_5ValueEEE.str1.1
.LC88:
	.string	"reloc_info_total_size"
	.section	.rodata._ZN2v88internal19StatisticsExtension11GetCountersERKNS_20FunctionCallbackInfoINS_5ValueEEE.str1.8
	.align 8
.LC89:
	.string	"source_position_table_total_size"
	.section	.text._ZN2v88internal19StatisticsExtension11GetCountersERKNS_20FunctionCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19StatisticsExtension11GetCountersERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal19StatisticsExtension11GetCountersERKNS_20FunctionCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal19StatisticsExtension11GetCountersERKNS_20FunctionCallbackInfoINS_5ValueEEE:
.LFB20023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1496, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	16(%r14), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	leaq	37592(%rdi), %rax
	movq	%rdi, -1520(%rbp)
	movq	%rax, -1528(%rbp)
	testl	%esi, %esi
	jg	.L118
.L33:
	movq	-1520(%rbp), %rax
	movq	40960(%rax), %rbx
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	%rax, %r15
	leaq	.LC1(%rip), %rax
	movq	%rax, -1176(%rbp)
	leaq	.LC2(%rip), %rax
	movq	%rax, -1160(%rbp)
	leaq	.LC3(%rip), %rax
	movq	%rax, -1144(%rbp)
	leaq	.LC4(%rip), %rax
	movq	%rax, -1128(%rbp)
	leaq	.LC5(%rip), %rax
	movq	%rax, -1112(%rbp)
	leaq	.LC6(%rip), %rax
	movq	%rax, -1096(%rbp)
	leaq	.LC7(%rip), %rax
	movq	%rax, -1080(%rbp)
	leaq	.LC8(%rip), %rax
	movq	%rax, -1064(%rbp)
	leaq	.LC9(%rip), %rax
	movq	%rax, -1048(%rbp)
	leaq	.LC10(%rip), %rax
	movq	%rax, -1032(%rbp)
	leaq	.LC11(%rip), %rax
	movq	%rax, -1016(%rbp)
	leaq	.LC12(%rip), %rax
	movq	%rax, -1000(%rbp)
	leaq	.LC13(%rip), %rax
	movq	%rax, -984(%rbp)
	leaq	.LC14(%rip), %rax
	movq	%rax, -968(%rbp)
	leaq	.LC15(%rip), %rax
	movq	%rax, -952(%rbp)
	leaq	.LC16(%rip), %rax
	movq	%rax, -936(%rbp)
	leaq	.LC17(%rip), %rax
	movq	%rax, -920(%rbp)
	leaq	.LC18(%rip), %rax
	movq	%rax, -904(%rbp)
	leaq	.LC19(%rip), %rax
	movq	%rax, -888(%rbp)
	leaq	.LC20(%rip), %rax
	movq	%rax, -872(%rbp)
	leaq	.LC21(%rip), %rax
	movq	%rax, -856(%rbp)
	leaq	.LC22(%rip), %rax
	movq	%rax, -840(%rbp)
	leaq	.LC23(%rip), %rax
	movq	%rax, -824(%rbp)
	leaq	.LC24(%rip), %rax
	movq	%rax, -808(%rbp)
	leaq	.LC25(%rip), %rax
	movq	%rax, -792(%rbp)
	leaq	.LC26(%rip), %rax
	movq	%rax, -776(%rbp)
	leaq	.LC27(%rip), %rax
	movq	%rax, -760(%rbp)
	leaq	.LC28(%rip), %rax
	movq	%rax, -744(%rbp)
	leaq	.LC29(%rip), %rax
	movq	%rax, -728(%rbp)
	leaq	.LC30(%rip), %rax
	movq	%rax, -712(%rbp)
	leaq	.LC31(%rip), %rax
	movq	%rax, -696(%rbp)
	leaq	.LC32(%rip), %rax
	movq	%rax, -680(%rbp)
	leaq	.LC33(%rip), %rax
	movq	%rax, -664(%rbp)
	leaq	.LC34(%rip), %rax
	movq	%rax, -648(%rbp)
	leaq	.LC35(%rip), %rax
	movq	%rax, -632(%rbp)
	leaq	.LC36(%rip), %rax
	movq	%rax, -616(%rbp)
	leaq	.LC37(%rip), %rax
	movq	%rax, -600(%rbp)
	leaq	.LC38(%rip), %rax
	movq	%rax, -584(%rbp)
	leaq	.LC39(%rip), %rax
	movq	%rax, -568(%rbp)
	leaq	.LC40(%rip), %rax
	movq	%rax, -552(%rbp)
	leaq	.LC41(%rip), %rax
	movq	%rax, -536(%rbp)
	leaq	.LC42(%rip), %rax
	movq	%rax, -520(%rbp)
	leaq	.LC43(%rip), %rax
	movq	%rax, -504(%rbp)
	leaq	.LC44(%rip), %rax
	movq	%rax, -488(%rbp)
	leaq	.LC45(%rip), %rax
	movq	%rax, -472(%rbp)
	leaq	.LC46(%rip), %rax
	movq	%rax, -456(%rbp)
	leaq	.LC47(%rip), %rax
	movq	%rax, -440(%rbp)
	leaq	.LC48(%rip), %rax
	movq	%rax, -424(%rbp)
	leaq	.LC49(%rip), %rax
	movq	%rax, -408(%rbp)
	leaq	.LC50(%rip), %rax
	movq	%rax, -392(%rbp)
	leaq	.LC51(%rip), %rax
	movq	%rax, -376(%rbp)
	leaq	.LC52(%rip), %rax
	movq	%rax, -360(%rbp)
	leaq	.LC53(%rip), %rax
	movq	%rax, -344(%rbp)
	leaq	.LC54(%rip), %rax
	movq	%rax, -328(%rbp)
	leaq	.LC55(%rip), %rax
	movq	%rax, -312(%rbp)
	leaq	.LC56(%rip), %rax
	movq	%rax, -296(%rbp)
	leaq	.LC57(%rip), %rax
	movq	%rax, -280(%rbp)
	leaq	.LC58(%rip), %rax
	movq	%rax, -264(%rbp)
	leaq	.LC59(%rip), %rax
	movq	%rax, -248(%rbp)
	leaq	.LC60(%rip), %rax
	movq	%rax, -232(%rbp)
	leaq	.LC61(%rip), %rax
	movq	%rax, -216(%rbp)
	leaq	.LC62(%rip), %rax
	movq	%rax, -200(%rbp)
	leaq	.LC63(%rip), %rax
	movq	%rax, -184(%rbp)
	leaq	.LC64(%rip), %rax
	movq	%rax, -168(%rbp)
	leaq	.LC65(%rip), %rax
	movq	%rax, -152(%rbp)
	leaq	.LC66(%rip), %rax
	movq	%rax, -136(%rbp)
	leaq	.LC67(%rip), %rax
	movq	%rax, -120(%rbp)
	leaq	.LC68(%rip), %rax
	movq	%rax, -104(%rbp)
	leaq	.LC69(%rip), %rax
	movq	%rax, -88(%rbp)
	leaq	.LC70(%rip), %rax
	movq	%rax, -72(%rbp)
	leaq	5896(%rbx), %rax
	movq	%rax, -1184(%rbp)
	leaq	5928(%rbx), %rax
	movq	%rax, -1168(%rbp)
	leaq	5960(%rbx), %rax
	movq	%rax, -1152(%rbp)
	leaq	5992(%rbx), %rax
	movq	%rax, -1136(%rbp)
	leaq	6024(%rbx), %rax
	movq	%rax, -1120(%rbp)
	leaq	6056(%rbx), %rax
	movq	%rax, -1104(%rbp)
	leaq	6088(%rbx), %rax
	movq	%rax, -1088(%rbp)
	leaq	6120(%rbx), %rax
	movq	%rax, -1072(%rbp)
	leaq	6152(%rbx), %rax
	movq	%rax, -1056(%rbp)
	leaq	6184(%rbx), %rax
	movq	%rax, -1040(%rbp)
	leaq	6216(%rbx), %rax
	movq	%rax, -1024(%rbp)
	leaq	6248(%rbx), %rax
	movq	%rax, -1008(%rbp)
	leaq	6280(%rbx), %rax
	movq	%rax, -992(%rbp)
	leaq	6312(%rbx), %rax
	movq	%rax, -976(%rbp)
	leaq	6344(%rbx), %rax
	movq	%rax, -960(%rbp)
	leaq	6376(%rbx), %rax
	movq	%rax, -944(%rbp)
	leaq	6408(%rbx), %rax
	movq	%rax, -928(%rbp)
	leaq	6440(%rbx), %rax
	movq	%rax, -912(%rbp)
	leaq	6472(%rbx), %rax
	movq	%rax, -896(%rbp)
	leaq	6504(%rbx), %rax
	movq	%rax, -880(%rbp)
	leaq	6536(%rbx), %rax
	movq	%rax, -864(%rbp)
	leaq	6568(%rbx), %rax
	movq	%rax, -848(%rbp)
	leaq	6600(%rbx), %rax
	movq	%rax, -832(%rbp)
	leaq	6632(%rbx), %rax
	movq	%rax, -816(%rbp)
	leaq	6664(%rbx), %rax
	movq	%rax, -800(%rbp)
	leaq	6696(%rbx), %rax
	movq	%rax, -784(%rbp)
	leaq	6728(%rbx), %rax
	movq	%rax, -768(%rbp)
	leaq	6760(%rbx), %rax
	movq	%rax, -752(%rbp)
	leaq	6792(%rbx), %rax
	movq	%rax, -736(%rbp)
	leaq	6824(%rbx), %rax
	movq	%rax, -720(%rbp)
	leaq	6856(%rbx), %rax
	movq	%rax, -704(%rbp)
	leaq	6888(%rbx), %rax
	movq	%rax, -688(%rbp)
	leaq	6920(%rbx), %rax
	movq	%rax, -672(%rbp)
	leaq	6952(%rbx), %rax
	movq	%rax, -656(%rbp)
	leaq	6984(%rbx), %rax
	movq	%rax, -640(%rbp)
	leaq	7016(%rbx), %rax
	movq	%rax, -624(%rbp)
	leaq	7048(%rbx), %rax
	movq	%rax, -608(%rbp)
	leaq	7080(%rbx), %rax
	movq	%rax, -592(%rbp)
	leaq	7112(%rbx), %rax
	movq	%rax, -576(%rbp)
	leaq	7144(%rbx), %rax
	movq	%rax, -560(%rbp)
	leaq	7176(%rbx), %rax
	movq	%rax, -544(%rbp)
	leaq	7208(%rbx), %rax
	movq	%rax, -528(%rbp)
	leaq	7240(%rbx), %rax
	movq	%rax, -512(%rbp)
	leaq	7272(%rbx), %rax
	movq	%rax, -496(%rbp)
	leaq	7304(%rbx), %rax
	movq	%rax, -480(%rbp)
	leaq	7336(%rbx), %rax
	movq	%rax, -464(%rbp)
	leaq	7368(%rbx), %rax
	movq	%rax, -448(%rbp)
	leaq	7400(%rbx), %rax
	movq	%rax, -432(%rbp)
	leaq	7432(%rbx), %rax
	movq	%rax, -416(%rbp)
	leaq	7464(%rbx), %rax
	movq	%rax, -400(%rbp)
	leaq	7496(%rbx), %rax
	movq	%rax, -384(%rbp)
	leaq	7528(%rbx), %rax
	movq	%rax, -368(%rbp)
	leaq	7560(%rbx), %rax
	movq	%rax, -352(%rbp)
	leaq	7592(%rbx), %rax
	movq	%rax, -336(%rbp)
	leaq	7624(%rbx), %rax
	movq	%rax, -320(%rbp)
	leaq	7656(%rbx), %rax
	movq	%rax, -304(%rbp)
	leaq	7688(%rbx), %rax
	movq	%rax, -288(%rbp)
	leaq	7720(%rbx), %rax
	movq	%rax, -272(%rbp)
	leaq	7752(%rbx), %rax
	movq	%rax, -256(%rbp)
	leaq	7784(%rbx), %rax
	movq	%rax, -240(%rbp)
	leaq	7816(%rbx), %rax
	movq	%rax, -224(%rbp)
	leaq	7848(%rbx), %rax
	movq	%rax, -208(%rbp)
	leaq	7880(%rbx), %rax
	movq	%rax, -192(%rbp)
	leaq	7912(%rbx), %rax
	movq	%rax, -176(%rbp)
	leaq	7944(%rbx), %rax
	movq	%rax, -160(%rbp)
	leaq	7976(%rbx), %rax
	movq	%rax, -144(%rbp)
	leaq	8008(%rbx), %rax
	movq	%rax, -128(%rbp)
	leaq	8040(%rbx), %rax
	movq	%rax, -112(%rbp)
	leaq	8072(%rbx), %rax
	addq	$8104, %rbx
	movq	%rax, -96(%rbp)
	leaq	-1184(%rbp), %rax
	movq	%rbx, -80(%rbp)
	movq	%rax, %rbx
	movq	%rax, -1512(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -1504(%rbp)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L122:
	movq	16(%rdx), %rax
	testq	%rax, %rax
	je	.L40
.L43:
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cvtsi2sdl	(%rax), %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r13, %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -1496(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L119
.L44:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-1496(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L120
.L40:
	addq	$16, %rbx
	cmpq	%rbx, -1504(%rbp)
	je	.L121
.L46:
	movq	(%rbx), %rdx
	movq	(%r14), %rax
	movq	8(%rbx), %r13
	cmpb	$0, 24(%rdx)
	movq	8(%rax), %r12
	jne	.L122
	movb	$1, 24(%rdx)
	movq	%rdx, %rdi
	movq	%rdx, -1496(%rbp)
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	-1496(%rbp), %rdx
	movq	%rax, 16(%rdx)
	testq	%rax, %rax
	je	.L40
	cmpb	$0, 24(%rdx)
	jne	.L43
	movb	$1, 24(%rdx)
	movq	%rdx, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	-1496(%rbp), %rdx
	movq	%rax, 16(%rdx)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L120:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	addq	$16, %rbx
	cmpq	%rbx, -1504(%rbp)
	jne	.L46
	.p2align 4,,10
	.p2align 3
.L121:
	leaq	.LC71(%rip), %rax
	movq	-1520(%rbp), %rcx
	leaq	_ZN2v88internal8NewSpace4SizeEv(%rip), %rbx
	movq	%rax, -1432(%rbp)
	leaq	.LC72(%rip), %rax
	movq	%rax, -1416(%rbp)
	leaq	.LC73(%rip), %rax
	movq	%rax, -1400(%rbp)
	leaq	.LC74(%rip), %rax
	movq	%rax, -1384(%rbp)
	leaq	.LC75(%rip), %rax
	movq	%rax, -1368(%rbp)
	leaq	.LC76(%rip), %rax
	movq	%rax, -1352(%rbp)
	leaq	.LC77(%rip), %rax
	movq	%rax, -1336(%rbp)
	leaq	.LC78(%rip), %rax
	movq	%rax, -1320(%rbp)
	leaq	.LC79(%rip), %rax
	movq	%rax, -1304(%rbp)
	leaq	.LC80(%rip), %rax
	movq	%rax, -1288(%rbp)
	leaq	.LC81(%rip), %rax
	movq	%rax, -1272(%rbp)
	leaq	.LC82(%rip), %rax
	movq	%rax, -1256(%rbp)
	leaq	.LC83(%rip), %rax
	movq	%rax, -1240(%rbp)
	leaq	.LC84(%rip), %rax
	movq	%rax, -1224(%rbp)
	leaq	.LC85(%rip), %rax
	movq	%rax, -1208(%rbp)
	leaq	.LC86(%rip), %rax
	movq	%rax, -1192(%rbp)
	movq	39640(%rcx), %rax
	movq	80(%rax), %rax
	movq	%rax, -1440(%rbp)
	movq	37840(%rcx), %r13
	movq	0(%r13), %rax
	movq	72(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L47
	movslq	360(%r13), %r12
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	movq	352(%r13), %rdx
	movq	%rax, %r8
	movq	104(%r13), %rax
	imulq	%r8, %r12
	subq	40(%rdx), %rax
	leaq	(%rax,%r12), %rax
.L48:
	movq	%rax, -1424(%rbp)
	movq	-1520(%rbp), %rax
	leaq	_ZN2v88internal8NewSpace9AvailableEv(%rip), %rdx
	movq	37840(%rax), %r13
	movq	0(%r13), %rax
	movq	96(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L49
	movq	312(%r13), %r12
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	shrq	$18, %r12
	imulq	%rax, %r12
	movq	0(%r13), %rax
	movq	72(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L50
	movslq	360(%r13), %rbx
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	movq	352(%r13), %rdx
	movq	%rax, %r8
	movq	104(%r13), %rax
	imulq	%r8, %rbx
	subq	40(%rdx), %rax
	leaq	(%rax,%rbx), %rax
.L51:
	subq	%rax, %r12
.L52:
	movq	-1520(%rbp), %rax
	leaq	_ZN2v88internal8NewSpace15CommittedMemoryEv(%rip), %rdx
	movq	%r12, -1408(%rbp)
	movq	37840(%rax), %rdi
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L53
	movq	288(%rdi), %rax
	addq	448(%rdi), %rax
.L54:
	movq	%rax, -1392(%rbp)
	movq	-1520(%rbp), %rax
	leaq	_ZN2v88internal10PagedSpace4SizeEv(%rip), %r13
	movq	37848(%rax), %rdi
	movq	(%rdi), %rax
	movq	72(%rax), %rdx
	cmpq	%r13, %rdx
	jne	.L55
	movq	184(%rdi), %rdx
.L56:
	movq	%rdx, -1376(%rbp)
	movq	96(%rax), %rdx
	leaq	_ZN2v88internal10PagedSpace9AvailableEv(%rip), %r12
	cmpq	%r12, %rdx
	jne	.L57
	movq	96(%rdi), %rdx
	movq	40(%rdx), %rdx
.L58:
	movq	56(%rax), %rax
	leaq	_ZN2v88internal5Space15CommittedMemoryEv(%rip), %rbx
	movq	%rdx, -1360(%rbp)
	cmpq	%rbx, %rax
	jne	.L59
	movq	80(%rdi), %rax
.L60:
	movq	%rax, -1344(%rbp)
	movq	-1520(%rbp), %rax
	movq	37856(%rax), %rdi
	movq	(%rdi), %rax
	movq	72(%rax), %rdx
	cmpq	%r13, %rdx
	jne	.L61
	movq	184(%rdi), %rdx
.L62:
	movq	%rdx, -1328(%rbp)
	movq	96(%rax), %rdx
	cmpq	%r12, %rdx
	jne	.L63
	movq	96(%rdi), %rdx
	movq	40(%rdx), %rdx
.L64:
	movq	56(%rax), %rax
	movq	%rdx, -1312(%rbp)
	cmpq	%rbx, %rax
	jne	.L65
	movq	80(%rdi), %rax
.L66:
	movq	%rax, -1296(%rbp)
	movq	-1520(%rbp), %rax
	leaq	_ZN2v88internal16LargeObjectSpace4SizeEv(%rip), %r12
	movq	37872(%rax), %rdi
	movq	(%rdi), %rax
	movq	72(%rax), %rdx
	cmpq	%r12, %rdx
	jne	.L67
	movq	104(%rdi), %rdx
.L68:
	movq	%rdx, -1280(%rbp)
	call	*96(%rax)
	movq	%rax, -1264(%rbp)
	movq	-1520(%rbp), %rax
	movq	37872(%rax), %rdi
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L69
	movq	80(%rdi), %rax
.L70:
	movq	%rax, -1248(%rbp)
	movq	-1520(%rbp), %rax
	movq	37880(%rax), %rdi
	movq	(%rdi), %rax
	movq	72(%rax), %rdx
	cmpq	%r12, %rdx
	jne	.L71
	movq	104(%rdi), %rdx
.L72:
	movq	%rdx, -1232(%rbp)
	call	*96(%rax)
	movq	%rax, -1216(%rbp)
	movq	-1520(%rbp), %rax
	movq	37880(%rax), %rdi
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L73
	movq	80(%rdi), %rax
.L74:
	movq	%rax, -1200(%rbp)
	leaq	-1440(%rbp), %rbx
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L125:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L76:
	movq	%r12, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r13, %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -1496(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L123
.L77:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-1496(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L124
	addq	$16, %rbx
	cmpq	%rbx, -1512(%rbp)
	je	.L79
.L81:
	movq	(%r14), %rax
	movq	8(%rbx), %r13
	movq	8(%rax), %r12
	movq	(%rbx), %rax
	testq	%rax, %rax
	jns	.L125
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L119:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L124:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	addq	$16, %rbx
	cmpq	%rbx, -1512(%rbp)
	jne	.L81
	.p2align 4,,10
	.p2align 3
.L79:
	movq	(%r14), %rax
	pxor	%xmm0, %xmm0
	movq	8(%rax), %rbx
	movq	-1528(%rbp), %rax
	cvtsi2sdq	-37560(%rax), %xmm0
	movq	%rbx, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	leaq	.LC87(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L126
.L82:
	movq	%rbx, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L127
	movq	(%r14), %rax
	testq	%r15, %r15
	je	.L128
.L84:
	movq	(%r15), %rdx
.L85:
	movq	%rdx, 24(%rax)
	movq	(%r14), %rax
	leaq	-1488(%rbp), %r12
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	addq	$37592, %rsi
	call	_ZN2v88internal18HeapObjectIteratorC1EPNS0_4HeapENS1_20HeapObjectsFilteringE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	je	.L108
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L87:
	movq	-1(%rax), %rdx
	cmpw	$72, 11(%rdx)
	je	.L129
.L95:
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	je	.L130
.L101:
	movq	-1(%rax), %rdx
	cmpw	$69, 11(%rdx)
	jne	.L87
	movq	7(%rax), %rdx
	movq	23(%rax), %rax
	movslq	11(%rdx), %rdx
	addl	$23, %edx
	andl	$-8, %edx
	addl	%edx, %ebx
	movq	%rax, %rdx
	testb	$1, %al
	jne	.L88
.L90:
	movq	7(%rax), %rdx
.L89:
	movl	11(%rdx), %edx
	testl	%edx, %edx
	jle	.L95
	movq	%rax, %rdx
	testb	$1, %al
	jne	.L92
.L94:
	movq	7(%rax), %rdx
.L99:
	movslq	11(%rdx), %rax
	movq	%r12, %rdi
	addl	$23, %eax
	andl	$-8, %eax
	addl	%eax, %r13d
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	jne	.L101
.L130:
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
.L86:
	movq	(%r14), %rax
	movq	8(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%rbx, %rdi
	leaq	.LC88(%rip), %rsi
	movq	%rax, -1496(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L131
.L102:
	movq	%rbx, %rdi
	movq	%rdx, -1504(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-1496(%rbp), %rcx
	movq	-1504(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L132
.L103:
	movq	(%r14), %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
	movq	8(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	leaq	.LC89(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L133
.L104:
	movq	%rbx, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L134
.L105:
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	addq	$1496, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L129:
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L96
.L100:
	andq	$-262144, %rax
	movq	24(%rax), %rax
	leaq	-37592(%rax), %rcx
	cmpq	-37280(%rax), %rdx
	je	.L136
	movq	7(%rdx), %rdx
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L88:
	movq	-1(%rax), %rcx
	cmpw	$70, 11(%rcx)
	jne	.L90
	jmp	.L89
.L118:
	movq	8(%r14), %rdi
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	movq	(%r14), %rax
	je	.L116
	movq	8(%rax), %rsi
	movl	16(%r14), %ecx
	leaq	88(%rsi), %rdi
	testl	%ecx, %ecx
	jle	.L36
	movq	8(%r14), %rdi
.L36:
	call	_ZNK2v85Value12BooleanValueEPNS_7IsolateE@PLT
	testb	%al, %al
	jne	.L37
.L117:
	movq	(%r14), %rax
.L116:
	movq	8(%rax), %rdi
	jmp	.L33
.L92:
	movq	-1(%rax), %rcx
	cmpw	$70, 11(%rcx)
	jne	.L94
	jmp	.L99
.L96:
	movq	-1(%rdx), %rcx
	cmpw	$70, 11(%rcx)
	jne	.L100
	jmp	.L99
.L136:
	movq	976(%rcx), %rdx
	jmp	.L99
.L73:
	call	*%rax
	jmp	.L74
.L71:
	call	*%rdx
	movq	%rax, %rdx
	movq	-1520(%rbp), %rax
	movq	37880(%rax), %rdi
	movq	(%rdi), %rax
	jmp	.L72
.L69:
	call	*%rax
	jmp	.L70
.L67:
	call	*%rdx
	movq	%rax, %rdx
	movq	-1520(%rbp), %rax
	movq	37872(%rax), %rdi
	movq	(%rdi), %rax
	jmp	.L68
.L65:
	call	*%rax
	jmp	.L66
.L63:
	call	*%rdx
	movq	%rax, %rdx
	movq	-1520(%rbp), %rax
	movq	37856(%rax), %rdi
	movq	(%rdi), %rax
	jmp	.L64
.L61:
	call	*%rdx
	movq	%rax, %rdx
	movq	-1520(%rbp), %rax
	movq	37856(%rax), %rdi
	movq	(%rdi), %rax
	jmp	.L62
.L59:
	call	*%rax
	jmp	.L60
.L57:
	call	*%rdx
	movq	%rax, %rdx
	movq	-1520(%rbp), %rax
	movq	37848(%rax), %rdi
	movq	(%rdi), %rax
	jmp	.L58
.L55:
	call	*%rdx
	movq	%rax, %rdx
	movq	-1520(%rbp), %rax
	movq	37848(%rax), %rdi
	movq	(%rdi), %rax
	jmp	.L56
.L53:
	call	*%rax
	jmp	.L54
.L49:
	movq	%r13, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L52
.L47:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L48
.L37:
	movq	-1528(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	$4, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal4Heap17CollectAllGarbageEiNS0_23GarbageCollectionReasonENS_15GCCallbackFlagsE@PLT
	jmp	.L117
.L50:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L51
.L108:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	jmp	.L86
.L131:
	movq	%rax, -1504(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1504(%rbp), %rdx
	jmp	.L102
.L126:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L82
.L127:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	(%r14), %rax
	testq	%r15, %r15
	jne	.L84
.L128:
	movq	16(%rax), %rdx
	jmp	.L85
.L132:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L103
.L133:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L104
.L134:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L105
.L135:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20023:
	.size	_ZN2v88internal19StatisticsExtension11GetCountersERKNS_20FunctionCallbackInfoINS_5ValueEEE, .-_ZN2v88internal19StatisticsExtension11GetCountersERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal19StatisticsExtension7kSourceE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal19StatisticsExtension7kSourceE, @function
_GLOBAL__sub_I__ZN2v88internal19StatisticsExtension7kSourceE:
.LFB24961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24961:
	.size	_GLOBAL__sub_I__ZN2v88internal19StatisticsExtension7kSourceE, .-_GLOBAL__sub_I__ZN2v88internal19StatisticsExtension7kSourceE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal19StatisticsExtension7kSourceE
	.weak	_ZTVN2v89ExtensionE
	.section	.data.rel.ro.local._ZTVN2v89ExtensionE,"awG",@progbits,_ZTVN2v89ExtensionE,comdat
	.align 8
	.type	_ZTVN2v89ExtensionE, @object
	.size	_ZTVN2v89ExtensionE, 40
_ZTVN2v89ExtensionE:
	.quad	0
	.quad	0
	.quad	_ZN2v89ExtensionD1Ev
	.quad	_ZN2v89ExtensionD0Ev
	.quad	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.weak	_ZTVN2v88internal19StatisticsExtensionE
	.section	.data.rel.ro.local._ZTVN2v88internal19StatisticsExtensionE,"awG",@progbits,_ZTVN2v88internal19StatisticsExtensionE,comdat
	.align 8
	.type	_ZTVN2v88internal19StatisticsExtensionE, @object
	.size	_ZTVN2v88internal19StatisticsExtensionE, 40
_ZTVN2v88internal19StatisticsExtensionE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal19StatisticsExtensionD1Ev
	.quad	_ZN2v88internal19StatisticsExtensionD0Ev
	.quad	_ZN2v88internal19StatisticsExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.globl	_ZN2v88internal19StatisticsExtension7kSourceE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC90:
	.string	"native function getV8Statistics();"
	.section	.data.rel.ro.local._ZN2v88internal19StatisticsExtension7kSourceE,"aw"
	.align 8
	.type	_ZN2v88internal19StatisticsExtension7kSourceE, @object
	.size	_ZN2v88internal19StatisticsExtension7kSourceE, 8
_ZN2v88internal19StatisticsExtension7kSourceE:
	.quad	.LC90
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
