	.file	"instruction-selector-x64.cc"
	.text
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_120MachineTypeForNarrowEPNS1_4NodeES4_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_120MachineTypeForNarrowEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler12_GLOBAL__N_120MachineTypeForNarrowEPNS1_4NodeES4_:
.LFB17528:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdi
	cmpw	$429, 16(%rdi)
	je	.L19
	movq	(%rbx), %rdi
	movzwl	16(%rdi), %edx
.L5:
	xorl	%eax, %eax
	cmpw	$429, %dx
	je	.L20
.L9:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	movq	(%rbx), %rdi
	movzwl	16(%rdi), %ecx
	movl	%ecx, %edx
	cmpw	$23, %cx
	je	.L3
	cmpl	$24, %ecx
	jne	.L5
	movq	48(%rdi), %rsi
.L6:
	movl	%eax, %ecx
	movzbl	%ah, %eax
	cmpb	$2, %cl
	je	.L21
	cmpb	$3, %cl
	je	.L22
	cmpb	$4, %cl
	jne	.L5
	cmpb	$2, %al
	je	.L23
	cmpb	$3, %al
	jne	.L5
	movl	$772, %eax
	testq	%rsi, %rsi
	js	.L5
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movslq	44(%rdi), %rsi
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L22:
	cmpb	$2, %al
	je	.L24
	cmpb	$3, %al
	jne	.L5
	movl	$771, %eax
	cmpq	$65535, %rsi
	jbe	.L9
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L21:
	cmpb	$2, %al
	je	.L25
	cmpb	$3, %al
	jne	.L5
	movl	$770, %eax
	cmpq	$255, %rsi
	jbe	.L9
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L25:
	subq	$-128, %rsi
	movl	$514, %eax
	cmpq	$255, %rsi
	jbe	.L9
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L24:
	addq	$32768, %rsi
	movl	$515, %eax
	cmpq	$65535, %rsi
	jbe	.L9
	jmp	.L5
.L23:
	movl	$516, %eax
	jmp	.L9
	.cfi_endproc
.LFE17528:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_120MachineTypeForNarrowEPNS1_4NodeES4_, .-_ZN2v88internal8compiler12_GLOBAL__N_120MachineTypeForNarrowEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0, @function
_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0:
.LFB21842:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movzbl	23(%rdi), %eax
	movq	32(%rdi), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L27
	leaq	32(%rdi), %rbx
	cmpq	%r8, %rsi
	je	.L26
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	je	.L31
.L39:
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L31:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L26
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	16(%r8), %rax
	leaq	16(%r8), %rbx
	cmpq	%rax, %rsi
	je	.L26
	movq	%r8, %rdi
	movq	%rax, %r8
	leaq	-24(%rdi), %r12
	testq	%r8, %r8
	jne	.L39
	jmp	.L31
	.cfi_endproc
.LFE21842:
	.size	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0, .-_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	.section	.text._ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1, @function
_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1:
.LFB21841:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L41
	movq	40(%rdi), %r8
	leaq	40(%rdi), %rbx
	cmpq	%r8, %rsi
	je	.L40
	leaq	-48(%rdi), %r12
	testq	%r8, %r8
	je	.L45
.L53:
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L45:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L40
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	32(%rdi), %rdi
	movq	24(%rdi), %r8
	cmpq	%r8, %rsi
	je	.L40
	leaq	24(%rdi), %rbx
	leaq	-48(%rdi), %r12
	testq	%r8, %r8
	jne	.L53
	jmp	.L45
	.cfi_endproc
.LFE21841:
	.size	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1, .-_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi, @function
_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi:
.LFB17447:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L55
	movq	16(%r14), %r14
.L55:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r15, %rsi
	movq	%r13, %rdi
	movabsq	$34359738369, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	addq	$8, %rsp
	movq	%r14, %rcx
	salq	$3, %rdx
	movl	%r12d, %esi
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	movabsq	$927712935937, %rbx
	xorl	%r8d, %r8d
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17447:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi, .-_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_17VisitRREPNS1_19InstructionSelectorEPNS1_4NodeEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_17VisitRREPNS1_19InstructionSelectorEPNS1_4NodeEi, @function
_ZN2v88internal8compiler12_GLOBAL__N_17VisitRREPNS1_19InstructionSelectorEPNS1_4NodeEi:
.LFB17448:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L58
	movq	16(%r14), %r14
.L58:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r15, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	addq	$8, %rsp
	movq	%r14, %rcx
	salq	$3, %rdx
	movl	%r12d, %esi
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	movabsq	$927712935937, %rbx
	xorl	%r8d, %r8d
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17448:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_17VisitRREPNS1_19InstructionSelectorEPNS1_4NodeEi, .-_ZN2v88internal8compiler12_GLOBAL__N_17VisitRREPNS1_19InstructionSelectorEPNS1_4NodeEi
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_112VisitCompareEPNS1_19InstructionSelectorEiPNS1_4NodeES6_PNS1_17FlagsContinuationEb,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_112VisitCompareEPNS1_19InstructionSelectorEiPNS1_4NodeES6_PNS1_17FlagsContinuationEb, @function
_ZN2v88internal8compiler12_GLOBAL__N_112VisitCompareEPNS1_19InstructionSelectorEiPNS1_4NodeES6_PNS1_17FlagsContinuationEb:
.LFB17527:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	testb	%r9b, %r9b
	jne	.L66
.L62:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movl	%r12d, %ecx
	movq	-56(%rbp), %r8
	salq	$3, %rdx
	addq	$24, %rsp
	salq	$3, %rcx
	movl	%r15d, %esi
	movabsq	$377957122049, %rbx
	movq	%r13, %rdi
	movabsq	$34359738369, %r12
	orq	%rbx, %rdx
	orq	%r12, %rcx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandES3_PNS1_17FlagsContinuationE@PLT
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	%rcx, %rsi
	movq	%r8, -56(%rbp)
	call	_ZNK2v88internal8compiler19InstructionSelector9IsDefinedEPNS1_4NodeE@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L67
.L63:
	movq	%r14, %rax
	movq	%rbx, %r14
	movq	%rax, %rbx
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector6IsUsedEPNS1_4NodeE@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	jne	.L62
	jmp	.L63
	.cfi_endproc
.LFE17527:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_112VisitCompareEPNS1_19InstructionSelectorEiPNS1_4NodeES6_PNS1_17FlagsContinuationEb, .-_ZN2v88internal8compiler12_GLOBAL__N_112VisitCompareEPNS1_19InstructionSelectorEiPNS1_4NodeES6_PNS1_17FlagsContinuationEb
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_18VisitRROEPNS1_19InstructionSelectorEPNS1_4NodeEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_18VisitRROEPNS1_19InstructionSelectorEPNS1_4NodeEi, @function
_ZN2v88internal8compiler12_GLOBAL__N_18VisitRROEPNS1_19InstructionSelectorEPNS1_4NodeEi:
.LFB17449:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$24, %rsp
	movzbl	23(%rsi), %eax
	movl	%edx, -52(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L69
	leaq	40(%rsi), %rax
.L70:
	movq	(%rax), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r13), %eax
	salq	$3, %r15
	movabsq	$34359738369, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L71
	movq	32(%r13), %rax
	leaq	16(%rax), %rbx
.L71:
	movq	(%rbx), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movl	-52(%rbp), %esi
	pushq	$0
	salq	$3, %rdx
	movq	%r15, %r8
	movq	%r14, %rcx
	movabsq	$1065151889409, %rbx
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L70
	.cfi_endproc
.LFE17449:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_18VisitRROEPNS1_19InstructionSelectorEPNS1_4NodeEi, .-_ZN2v88internal8compiler12_GLOBAL__N_18VisitRROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_18VisitDivEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_18VisitDivEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_18VisitDivEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE:
.LFB17419:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	%edx, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movabsq	$5222680231929, %rax
	movq	%rax, -64(%rbp)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L74
	leaq	40(%rsi), %rax
.L75:
	movq	(%rax), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r13), %eax
	movl	%ebx, %r15d
	movabsq	$927712935937, %r8
	salq	$3, %r15
	andl	$15, %eax
	orq	%r8, %r15
	cmpl	$15, %eax
	jne	.L76
	movq	32(%r13), %rax
	leaq	16(%rax), %r14
.L76:
	movq	(%r14), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$790273982465, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	-68(%rbp), %esi
	pushq	%rax
	leaq	0(,%rbx,8), %rdx
	movq	%r15, %r8
	movq	%r12, %rdi
	movabsq	$790273982465, %rcx
	movl	$1, %r9d
	orq	%rcx, %rdx
	movq	%r14, %rcx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L75
.L79:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17419:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_18VisitDivEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_18VisitDivEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_18VisitModEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_18VisitModEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_18VisitModEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE:
.LFB17420:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$40, %rsp
	movl	%edx, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movabsq	$824633720825, %rax
	movq	%rax, -64(%rbp)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L81
	leaq	40(%rsi), %rax
.L82:
	movq	(%rax), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r13), %eax
	salq	$3, %r15
	movabsq	$927712935937, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L83
	movq	32(%r13), %rax
	leaq	16(%rax), %rbx
.L83:
	movq	(%rbx), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$790273982465, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	-68(%rbp), %esi
	movl	$1, %r9d
	movabsq	$5188320493569, %rbx
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L86
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L82
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17420:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_18VisitModEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_18VisitModEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_112VisitMulHighEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_112VisitMulHighEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_112VisitMulHighEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE:
.LFB17418:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	32(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L88
	leaq	40(%rsi), %rax
.L89:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	call	_ZNK2v88internal8compiler19InstructionSelector9IsDefinedEPNS1_4NodeE@PLT
	testb	%al, %al
	je	.L97
.L90:
	movabsq	$824633720825, %rax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, -72(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	-72(%rbp), %r8d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %rax
	salq	$3, %r8
	orq	%rax, %r8
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r15d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$790273982465, %rcx
	salq	$3, %r15
	orq	%rcx, %r15
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movq	-72(%rbp), %r8
	movl	$1, %r9d
	movabsq	$5188320493569, %rbx
	movq	%r15, %rcx
	movl	%r13d, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	leaq	16(%r15), %rax
	movq	16(%r15), %r15
	addq	$8, %rax
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L97:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector6IsUsedEPNS1_4NodeE@PLT
	testb	%al, %al
	je	.L90
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector9IsDefinedEPNS1_4NodeE@PLT
	testb	%al, %al
	je	.L99
.L92:
	movq	%r15, %rax
	movq	%rbx, %r15
	movq	%rax, %rbx
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L99:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector6IsUsedEPNS1_4NodeE@PLT
	testb	%al, %al
	jne	.L90
	jmp	.L92
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17418:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_112VisitMulHighEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_112VisitMulHighEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_114VisitFloatUnopEPNS1_19InstructionSelectorEPNS1_4NodeES6_NS1_10ArchOpcodeES7_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_114VisitFloatUnopEPNS1_19InstructionSelectorEPNS1_4NodeES6_NS1_10ArchOpcodeES7_, @function
_ZN2v88internal8compiler12_GLOBAL__N_114VisitFloatUnopEPNS1_19InstructionSelectorEPNS1_4NodeES6_NS1_10ArchOpcodeES7_:
.LFB17451:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	%r8d, -68(%rbp)
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$13, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	orq	%rcx, %rbx
	movq	%rbx, -64(%rbp)
	testb	$32, 36(%r12)
	je	.L101
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$584115552257, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	leaq	-64(%rbp), %r9
	movq	%r14, %rcx
	salq	$3, %rdx
	movl	$1, %r8d
	movl	%r15d, %esi
	movq	%r12, %rdi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
.L100:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	leaq	0(,%rbx,8), %r15
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	orq	%rcx, %r15
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movl	-68(%rbp), %esi
	leaq	-64(%rbp), %r9
	salq	$3, %rdx
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	jmp	.L100
.L105:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17451:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_114VisitFloatUnopEPNS1_19InstructionSelectorEPNS1_4NodeES6_NS1_10ArchOpcodeES7_, .-_ZN2v88internal8compiler12_GLOBAL__N_114VisitFloatUnopEPNS1_19InstructionSelectorEPNS1_4NodeES6_NS1_10ArchOpcodeES7_
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_115VisitFloatBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeES7_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_115VisitFloatBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeES7_, @function
_ZN2v88internal8compiler12_GLOBAL__N_115VisitFloatBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeES7_:
.LFB17450:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$24, %rsp
	movzbl	23(%r14), %eax
	movq	32(%rsi), %rsi
	movl	%ecx, -60(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L107
	movq	16(%rsi), %rsi
.L107:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	addq	$8, %rbx
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %r13
	movabsq	$377957122049, %rax
	orq	%rax, %r13
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L109
	movq	32(%r14), %rax
	leaq	24(%rax), %rbx
.L109:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$34359738369, %r8
	orq	%r8, %rbx
	testb	$32, 36(%r12)
	je	.L110
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %r14
	movl	%eax, -56(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	-56(%rbp), %edx
	movq	%r13, %rcx
	pushq	$0
	movl	%r15d, %esi
	movq	%rbx, %r8
	movq	%r12, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	orq	%r14, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rcx
	popq	%rsi
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%r15d, %edx
	movl	-60(%rbp), %esi
	pushq	$0
	salq	$3, %rdx
	movq	%rbx, %r8
	movq	%r13, %rcx
	movabsq	$1065151889409, %r15
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	orq	%r15, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17450:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_115VisitFloatBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeES7_, .-_ZN2v88internal8compiler12_GLOBAL__N_115VisitFloatBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeES7_
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_119VisitFloat32CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_119VisitFloat32CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE, @function
_ZN2v88internal8compiler12_GLOBAL__N_119VisitFloat32CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE:
.LFB17533:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	-17(%rsi), %eax
	movq	-8(%rsi), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L115
	leaq	16(%rbx), %rsi
	movq	16(%rbx), %rbx
	addq	$8, %rsi
.L115:
	movl	36(%r13), %eax
	movq	(%rsi), %r12
	movq	%r13, %rdi
	movq	%rbx, %rsi
	shrl	$5, %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%r14d, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	andl	$-47, %r14d
	movl	%eax, -52(%rbp)
	addl	$190, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movabsq	$34359738369, %r12
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	-52(%rbp), %ecx
	movl	%ebx, %edx
	addq	$24, %rsp
	salq	$3, %rdx
	movq	%r15, %r8
	movl	%r14d, %esi
	movq	%r13, %rdi
	movabsq	$377957122049, %rbx
	salq	$3, %rcx
	orq	%rbx, %rdx
	orq	%r12, %rcx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandES3_PNS1_17FlagsContinuationE@PLT
	.cfi_endproc
.LFE17533:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_119VisitFloat32CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE, .-_ZN2v88internal8compiler12_GLOBAL__N_119VisitFloat32CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_119VisitFloat64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_119VisitFloat64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE, @function
_ZN2v88internal8compiler12_GLOBAL__N_119VisitFloat64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE:
.LFB17534:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	-17(%rsi), %eax
	movq	-8(%rsi), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L121
	leaq	16(%rbx), %rsi
	movq	16(%rbx), %rbx
	addq	$8, %rsi
.L121:
	movl	36(%r13), %eax
	movq	(%rsi), %r12
	movq	%r13, %rdi
	movq	%rbx, %rsi
	shrl	$5, %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%r14d, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	andl	$-40, %r14d
	movl	%eax, -52(%rbp)
	addl	$195, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movabsq	$34359738369, %r12
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	-52(%rbp), %ecx
	movl	%ebx, %edx
	addq	$24, %rsp
	salq	$3, %rdx
	movq	%r15, %r8
	movl	%r14d, %esi
	movq	%r13, %rdi
	movabsq	$377957122049, %rbx
	salq	$3, %rcx
	orq	%rbx, %rdx
	orq	%r12, %rcx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandES3_PNS1_17FlagsContinuationE@PLT
	.cfi_endproc
.LFE17534:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_119VisitFloat64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE, .-_ZN2v88internal8compiler12_GLOBAL__N_119VisitFloat64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
	.section	.text._ZN2v88internal8compiler16OperandGenerator16DefineAsRegisterEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler16OperandGenerator16DefineAsRegisterEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16OperandGenerator16DefineAsRegisterEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16OperandGenerator16DefineAsRegisterEPNS1_4NodeE, @function
_ZN2v88internal8compiler16OperandGenerator16DefineAsRegisterEPNS1_4NodeE:
.LFB15272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	(%r12), %rdi
	movq	%r13, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %eax
	addq	$8, %rsp
	movabsq	$927712935937, %rbx
	salq	$3, %rax
	orq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15272:
	.size	_ZN2v88internal8compiler16OperandGenerator16DefineAsRegisterEPNS1_4NodeE, .-_ZN2v88internal8compiler16OperandGenerator16DefineAsRegisterEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16OperandGenerator17DefineSameAsFirstEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler16OperandGenerator17DefineSameAsFirstEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16OperandGenerator17DefineSameAsFirstEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16OperandGenerator17DefineSameAsFirstEPNS1_4NodeE, @function
_ZN2v88internal8compiler16OperandGenerator17DefineSameAsFirstEPNS1_4NodeE:
.LFB15273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	(%r12), %rdi
	movq	%r13, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %eax
	addq	$8, %rsp
	movabsq	$1065151889409, %rbx
	salq	$3, %rax
	orq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15273:
	.size	_ZN2v88internal8compiler16OperandGenerator17DefineSameAsFirstEPNS1_4NodeE, .-_ZN2v88internal8compiler16OperandGenerator17DefineSameAsFirstEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16OperandGenerator3UseEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler16OperandGenerator3UseEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16OperandGenerator3UseEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16OperandGenerator3UseEPNS1_4NodeE, @function
_ZN2v88internal8compiler16OperandGenerator3UseEPNS1_4NodeE:
.LFB15279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	(%r12), %rdi
	movq	%r13, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %eax
	addq	$8, %rsp
	movabsq	$34359738369, %rbx
	salq	$3, %rax
	orq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15279:
	.size	_ZN2v88internal8compiler16OperandGenerator3UseEPNS1_4NodeE, .-_ZN2v88internal8compiler16OperandGenerator3UseEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE, @function
_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE:
.LFB15284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	(%r12), %rdi
	movq	%r13, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %eax
	addq	$8, %rsp
	movabsq	$377957122049, %rbx
	salq	$3, %rax
	orq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15284:
	.size	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE, .-_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i,"axG",@progbits,_ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i
	.type	_ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i, @function
_ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i:
.LFB17364:
	.cfi_startproc
	endbr64
	movq	(%rcx), %rax
	cmpw	$429, 16(%rax)
	je	.L134
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rdi
	movq	%rcx, %rbx
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_@PLT
	testb	%al, %al
	jne	.L149
.L142:
	xorl	%eax, %eax
.L133:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movq	(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZNK2v88internal8compiler19InstructionSelector14GetEffectLevelEPNS1_4NodeE@PLT
	cmpl	%r14d, %eax
	jne	.L142
	movq	(%rbx), %rdi
	subl	$95, %r13d
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	cmpl	$140, %r13d
	ja	.L142
	leaq	.L138(%rip), %rcx
	movslq	(%rcx,%r13,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i,"aG",@progbits,_ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i,comdat
	.align 4
	.align 4
.L138:
	.long	.L137-.L138
	.long	.L139-.L138
	.long	.L137-.L138
	.long	.L139-.L138
	.long	.L137-.L138
	.long	.L139-.L138
	.long	.L141-.L138
	.long	.L140-.L138
	.long	.L137-.L138
	.long	.L139-.L138
	.long	.L141-.L138
	.long	.L140-.L138
	.long	.L137-.L138
	.long	.L139-.L138
	.long	.L137-.L138
	.long	.L139-.L138
	.long	.L137-.L138
	.long	.L139-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L142-.L138
	.long	.L137-.L138
	.section	.text._ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i,"axG",@progbits,_ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i,comdat
.L137:
	subl	$5, %eax
	cmpb	$3, %al
	setbe	%al
	jmp	.L133
.L139:
	cmpb	$4, %al
	sete	%al
	jmp	.L133
.L140:
	cmpb	$2, %al
	sete	%al
	jmp	.L133
.L141:
	cmpb	$3, %al
	sete	%al
	jmp	.L133
	.cfi_endproc
.LFE17364:
	.size	_ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i, .-_ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i
	.section	.text._ZN2v88internal8compiler19InstructionSelector19VisitAbortCSAAssertEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector19VisitAbortCSAAssertEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector19VisitAbortCSAAssertEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector19VisitAbortCSAAssertEPNS1_4NodeE:
.LFB17372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L151
	movq	16(%r13), %r13
.L151:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %ecx
	addq	$8, %rsp
	movq	%r12, %rdi
	salq	$3, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movabsq	$5188320493569, %rbx
	movl	$18, %esi
	orq	%rbx, %rcx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17372:
	.size	_ZN2v88internal8compiler19InstructionSelector19VisitAbortCSAAssertEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector19VisitAbortCSAAssertEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector18VisitUnalignedLoadEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler19InstructionSelector18VisitUnalignedLoadEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector18VisitUnalignedLoadEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector18VisitUnalignedLoadEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector18VisitUnalignedLoadEPNS1_4NodeE:
.LFB21824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21824:
	.size	_ZN2v88internal8compiler19InstructionSelector18VisitUnalignedLoadEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector18VisitUnalignedLoadEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector19VisitUnalignedStoreEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector19VisitUnalignedStoreEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector19VisitUnalignedStoreEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector19VisitUnalignedStoreEPNS1_4NodeE:
.LFB21826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21826:
	.size	_ZN2v88internal8compiler19InstructionSelector19VisitUnalignedStoreEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector19VisitUnalignedStoreEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector22VisitWord32ReverseBitsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector22VisitWord32ReverseBitsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector22VisitWord32ReverseBitsEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector22VisitWord32ReverseBitsEPNS1_4NodeE:
.LFB21816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21816:
	.size	_ZN2v88internal8compiler19InstructionSelector22VisitWord32ReverseBitsEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector22VisitWord32ReverseBitsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector22VisitWord64ReverseBitsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector22VisitWord64ReverseBitsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector22VisitWord64ReverseBitsEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector22VisitWord64ReverseBitsEPNS1_4NodeE:
.LFB21820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21820:
	.size	_ZN2v88internal8compiler19InstructionSelector22VisitWord64ReverseBitsEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector22VisitWord64ReverseBitsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector23VisitWord64ReverseBytesEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector23VisitWord64ReverseBytesEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector23VisitWord64ReverseBytesEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector23VisitWord64ReverseBytesEPNS1_4NodeE:
.LFB17408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L162
	movq	16(%r12), %r12
.L162:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$139, %esi
	movabsq	$1065151889409, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17408:
	.size	_ZN2v88internal8compiler19InstructionSelector23VisitWord64ReverseBytesEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector23VisitWord64ReverseBytesEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector23VisitWord32ReverseBytesEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector23VisitWord32ReverseBytesEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector23VisitWord32ReverseBytesEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector23VisitWord32ReverseBytesEPNS1_4NodeE:
.LFB17409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L165
	movq	16(%r12), %r12
.L165:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$140, %esi
	movabsq	$1065151889409, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17409:
	.size	_ZN2v88internal8compiler19InstructionSelector23VisitWord32ReverseBytesEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector23VisitWord32ReverseBytesEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitInt32MulHighEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitInt32MulHighEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitInt32MulHighEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitInt32MulHighEPNS1_4NodeE:
.LFB17424:
	.cfi_startproc
	endbr64
	movl	$115, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_112VisitMulHighEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17424:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitInt32MulHighEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitInt32MulHighEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitInt32DivEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitInt32DivEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitInt32DivEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitInt32DivEPNS1_4NodeE:
.LFB17425:
	.cfi_startproc
	endbr64
	movl	$118, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_18VisitDivEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17425:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitInt32DivEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitInt32DivEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitInt64DivEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitInt64DivEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitInt64DivEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitInt64DivEPNS1_4NodeE:
.LFB17426:
	.cfi_startproc
	endbr64
	movl	$117, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_18VisitDivEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17426:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitInt64DivEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitInt64DivEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitUint32DivEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitUint32DivEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitUint32DivEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitUint32DivEPNS1_4NodeE:
.LFB17427:
	.cfi_startproc
	endbr64
	movl	$120, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_18VisitDivEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17427:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitUint32DivEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitUint32DivEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitUint64DivEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitUint64DivEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitUint64DivEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitUint64DivEPNS1_4NodeE:
.LFB17428:
	.cfi_startproc
	endbr64
	movl	$119, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_18VisitDivEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17428:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitUint64DivEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitUint64DivEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitInt32ModEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitInt32ModEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitInt32ModEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitInt32ModEPNS1_4NodeE:
.LFB17429:
	.cfi_startproc
	endbr64
	movl	$118, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_18VisitModEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17429:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitInt32ModEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitInt32ModEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitInt64ModEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitInt64ModEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitInt64ModEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitInt64ModEPNS1_4NodeE:
.LFB17430:
	.cfi_startproc
	endbr64
	movl	$117, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_18VisitModEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17430:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitInt64ModEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitInt64ModEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitUint32ModEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitUint32ModEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitUint32ModEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitUint32ModEPNS1_4NodeE:
.LFB17431:
	.cfi_startproc
	endbr64
	movl	$120, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_18VisitModEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17431:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitUint32ModEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitUint32ModEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitUint64ModEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitUint64ModEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitUint64ModEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitUint64ModEPNS1_4NodeE:
.LFB17432:
	.cfi_startproc
	endbr64
	movl	$119, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_18VisitModEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17432:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitUint64ModEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitUint64ModEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector18VisitUint32MulHighEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector18VisitUint32MulHighEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector18VisitUint32MulHighEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector18VisitUint32MulHighEPNS1_4NodeE:
.LFB17433:
	.cfi_startproc
	endbr64
	movl	$116, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_112VisitMulHighEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17433:
	.size	_ZN2v88internal8compiler19InstructionSelector18VisitUint32MulHighEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector18VisitUint32MulHighEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector30VisitTryTruncateFloat32ToInt64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector30VisitTryTruncateFloat32ToInt64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector30VisitTryTruncateFloat32ToInt64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector30VisitTryTruncateFloat32ToInt64EPNS1_4NodeE:
.LFB17434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L178
	movq	16(%r14), %r14
.L178:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %r14
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movabsq	$377957122049, %rax
	movq	%r12, %rdi
	movaps	%xmm0, -64(%rbp)
	orq	%rax, %rbx
	movq	%rbx, -72(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	movq	%r13, %rdi
	movl	$1, %esi
	orq	%r14, %rbx
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14FindProjectionEPNS1_4NodeEm@PLT
	movl	$1, %edx
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L179
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	movl	$2, %edx
	orq	%rbx, %r14
	movq	%r14, -56(%rbp)
.L179:
	pushq	$0
	leaq	-64(%rbp), %rcx
	leaq	-72(%rbp), %r9
	movl	$1, %r8d
	pushq	$0
	movl	$172, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L184
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L184:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17434:
	.size	_ZN2v88internal8compiler19InstructionSelector30VisitTryTruncateFloat32ToInt64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector30VisitTryTruncateFloat32ToInt64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector30VisitTryTruncateFloat64ToInt64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector30VisitTryTruncateFloat64ToInt64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector30VisitTryTruncateFloat64ToInt64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector30VisitTryTruncateFloat64ToInt64EPNS1_4NodeE:
.LFB17435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L186
	movq	16(%r14), %r14
.L186:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %r14
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movabsq	$377957122049, %rax
	movq	%r12, %rdi
	movaps	%xmm0, -64(%rbp)
	orq	%rax, %rbx
	movq	%rbx, -72(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	movq	%r13, %rdi
	movl	$1, %esi
	orq	%r14, %rbx
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14FindProjectionEPNS1_4NodeEm@PLT
	movl	$1, %edx
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L187
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	movl	$2, %edx
	orq	%rbx, %r14
	movq	%r14, -56(%rbp)
.L187:
	pushq	$0
	leaq	-64(%rbp), %rcx
	leaq	-72(%rbp), %r9
	movl	$1, %r8d
	pushq	$0
	movl	$173, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L192
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L192:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17435:
	.size	_ZN2v88internal8compiler19InstructionSelector30VisitTryTruncateFloat64ToInt64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector30VisitTryTruncateFloat64ToInt64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector31VisitTryTruncateFloat32ToUint64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector31VisitTryTruncateFloat32ToUint64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector31VisitTryTruncateFloat32ToUint64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector31VisitTryTruncateFloat32ToUint64EPNS1_4NodeE:
.LFB17436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L194
	movq	16(%r14), %r14
.L194:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %r14
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movabsq	$377957122049, %rax
	movq	%r12, %rdi
	movaps	%xmm0, -64(%rbp)
	orq	%rax, %rbx
	movq	%rbx, -72(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	movq	%r13, %rdi
	movl	$1, %esi
	orq	%r14, %rbx
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14FindProjectionEPNS1_4NodeEm@PLT
	movl	$1, %edx
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L195
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	movl	$2, %edx
	orq	%rbx, %r14
	movq	%r14, -56(%rbp)
.L195:
	pushq	$0
	leaq	-64(%rbp), %rcx
	leaq	-72(%rbp), %r9
	movl	$1, %r8d
	pushq	$0
	movl	$174, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L200:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17436:
	.size	_ZN2v88internal8compiler19InstructionSelector31VisitTryTruncateFloat32ToUint64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector31VisitTryTruncateFloat32ToUint64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector31VisitTryTruncateFloat64ToUint64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector31VisitTryTruncateFloat64ToUint64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector31VisitTryTruncateFloat64ToUint64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector31VisitTryTruncateFloat64ToUint64EPNS1_4NodeE:
.LFB17437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L202
	movq	16(%r14), %r14
.L202:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %r14
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movabsq	$377957122049, %rax
	movq	%r12, %rdi
	movaps	%xmm0, -64(%rbp)
	orq	%rax, %rbx
	movq	%rbx, -72(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	movq	%r13, %rdi
	movl	$1, %esi
	orq	%r14, %rbx
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14FindProjectionEPNS1_4NodeEm@PLT
	movl	$1, %edx
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L203
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	movl	$2, %edx
	orq	%rbx, %r14
	movq	%r14, -56(%rbp)
.L203:
	pushq	$0
	leaq	-64(%rbp), %rcx
	leaq	-72(%rbp), %r9
	movl	$1, %r8d
	pushq	$0
	movl	$175, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L208
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L208:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17437:
	.size	_ZN2v88internal8compiler19InstructionSelector31VisitTryTruncateFloat64ToUint64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector31VisitTryTruncateFloat64ToUint64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitChangeUint32ToUint64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitChangeUint32ToUint64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitChangeUint32ToUint64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitChangeUint32ToUint64EPNS1_4NodeE:
.LFB17440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L210
	movq	16(%r14), %r14
.L210:
	movq	(%r14), %rdi
	movzwl	16(%rdi), %eax
	cmpw	$339, %ax
	ja	.L211
	cmpw	$298, %ax
	ja	.L212
	cmpw	$55, %ax
	jne	.L214
	movzbl	23(%r14), %eax
	movq	32(%r14), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L228
.L219:
	movq	(%rdx), %rax
	movzwl	16(%rax), %ecx
	subw	$307, %cx
	cmpw	$4, %cx
	ja	.L214
	movl	$1, %eax
	salq	%cl, %rax
	testb	$21, %al
	je	.L214
.L215:
	popq	%rbx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE@PLT
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	cmpw	$473, %ax
	je	.L215
	jbe	.L229
	cmpw	$502, %ax
	jne	.L214
.L218:
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	subl	$2, %eax
	cmpb	$2, %al
	jbe	.L215
.L214:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movabsq	$34359738369, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r14, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$214, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	subw	$429, %ax
	cmpw	$1, %ax
	jbe	.L218
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L212:
	subw	$299, %ax
	cmpw	$40, %ax
	ja	.L214
	leaq	.L216(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector25VisitChangeUint32ToUint64EPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L216:
	.long	.L215-.L216
	.long	.L215-.L216
	.long	.L215-.L216
	.long	.L215-.L216
	.long	.L215-.L216
	.long	.L215-.L216
	.long	.L215-.L216
	.long	.L215-.L216
	.long	.L214-.L216
	.long	.L215-.L216
	.long	.L214-.L216
	.long	.L215-.L216
	.long	.L214-.L216
	.long	.L215-.L216
	.long	.L215-.L216
	.long	.L215-.L216
	.long	.L215-.L216
	.long	.L215-.L216
	.long	.L215-.L216
	.long	.L214-.L216
	.long	.L214-.L216
	.long	.L214-.L216
	.long	.L214-.L216
	.long	.L214-.L216
	.long	.L214-.L216
	.long	.L214-.L216
	.long	.L214-.L216
	.long	.L214-.L216
	.long	.L214-.L216
	.long	.L214-.L216
	.long	.L214-.L216
	.long	.L214-.L216
	.long	.L214-.L216
	.long	.L214-.L216
	.long	.L214-.L216
	.long	.L215-.L216
	.long	.L214-.L216
	.long	.L215-.L216
	.long	.L215-.L216
	.long	.L215-.L216
	.long	.L215-.L216
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitChangeUint32ToUint64EPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L228:
	movq	16(%rdx), %rdx
	jmp	.L219
	.cfi_endproc
.LFE17440:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitChangeUint32ToUint64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitChangeUint32ToUint64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector29VisitChangeTaggedToCompressedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector29VisitChangeTaggedToCompressedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector29VisitChangeTaggedToCompressedEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector29VisitChangeTaggedToCompressedEPNS1_4NodeE:
.LFB17441:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE@PLT
	.cfi_endproc
.LFE17441:
	.size	_ZN2v88internal8compiler19InstructionSelector29VisitChangeTaggedToCompressedEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector29VisitChangeTaggedToCompressedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector43VisitChangeTaggedPointerToCompressedPointerEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector43VisitChangeTaggedPointerToCompressedPointerEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector43VisitChangeTaggedPointerToCompressedPointerEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector43VisitChangeTaggedPointerToCompressedPointerEPNS1_4NodeE:
.LFB21832:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE@PLT
	.cfi_endproc
.LFE21832:
	.size	_ZN2v88internal8compiler19InstructionSelector43VisitChangeTaggedPointerToCompressedPointerEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector43VisitChangeTaggedPointerToCompressedPointerEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector41VisitChangeTaggedSignedToCompressedSignedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector41VisitChangeTaggedSignedToCompressedSignedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector41VisitChangeTaggedSignedToCompressedSignedEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector41VisitChangeTaggedSignedToCompressedSignedEPNS1_4NodeE:
.LFB21834:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE@PLT
	.cfi_endproc
.LFE21834:
	.size	_ZN2v88internal8compiler19InstructionSelector41VisitChangeTaggedSignedToCompressedSignedEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector41VisitChangeTaggedSignedToCompressedSignedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitWord64ClzEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitWord64ClzEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitWord64ClzEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitWord64ClzEPNS1_4NodeE:
.LFB17452:
	.cfi_startproc
	endbr64
	movl	$133, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17452:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitWord64ClzEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitWord64ClzEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitWord32ClzEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitWord32ClzEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitWord32ClzEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitWord32ClzEPNS1_4NodeE:
.LFB17453:
	.cfi_startproc
	endbr64
	movl	$134, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17453:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitWord32ClzEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitWord32ClzEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitWord64CtzEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitWord64CtzEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitWord64CtzEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitWord64CtzEPNS1_4NodeE:
.LFB17454:
	.cfi_startproc
	endbr64
	movl	$135, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17454:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitWord64CtzEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitWord64CtzEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitWord32CtzEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitWord32CtzEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitWord32CtzEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitWord32CtzEPNS1_4NodeE:
.LFB17455:
	.cfi_startproc
	endbr64
	movl	$136, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17455:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitWord32CtzEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitWord32CtzEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitWord64PopcntEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitWord64PopcntEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitWord64PopcntEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitWord64PopcntEPNS1_4NodeE:
.LFB17456:
	.cfi_startproc
	endbr64
	movl	$137, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17456:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitWord64PopcntEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitWord64PopcntEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitWord32PopcntEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitWord32PopcntEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitWord32PopcntEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitWord32PopcntEPNS1_4NodeE:
.LFB17457:
	.cfi_startproc
	endbr64
	movl	$138, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17457:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitWord32PopcntEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitWord32PopcntEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitFloat64SqrtEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64SqrtEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64SqrtEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitFloat64SqrtEPNS1_4NodeE:
.LFB17458:
	.cfi_startproc
	endbr64
	movl	$163, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17458:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64SqrtEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitFloat64SqrtEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitFloat32SqrtEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitFloat32SqrtEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitFloat32SqrtEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitFloat32SqrtEPNS1_4NodeE:
.LFB17459:
	.cfi_startproc
	endbr64
	movl	$150, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17459:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitFloat32SqrtEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitFloat32SqrtEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitChangeFloat64ToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitChangeFloat64ToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitChangeFloat64ToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitChangeFloat64ToInt32EPNS1_4NodeE:
.LFB17460:
	.cfi_startproc
	endbr64
	movl	$170, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17460:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitChangeFloat64ToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitChangeFloat64ToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitChangeFloat64ToInt64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitChangeFloat64ToInt64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitChangeFloat64ToInt64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitChangeFloat64ToInt64EPNS1_4NodeE:
.LFB17461:
	.cfi_startproc
	endbr64
	movl	$173, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17461:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitChangeFloat64ToInt64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitChangeFloat64ToInt64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector26VisitChangeFloat64ToUint32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector26VisitChangeFloat64ToUint32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector26VisitChangeFloat64ToUint32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector26VisitChangeFloat64ToUint32EPNS1_4NodeE:
.LFB17462:
	.cfi_startproc
	endbr64
	movl	$4194475, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17462:
	.size	_ZN2v88internal8compiler19InstructionSelector26VisitChangeFloat64ToUint32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector26VisitChangeFloat64ToUint32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector27VisitTruncateFloat64ToInt64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector27VisitTruncateFloat64ToInt64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector27VisitTruncateFloat64ToInt64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector27VisitTruncateFloat64ToInt64EPNS1_4NodeE:
.LFB21838:
	.cfi_startproc
	endbr64
	movl	$173, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE21838:
	.size	_ZN2v88internal8compiler19InstructionSelector27VisitTruncateFloat64ToInt64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector27VisitTruncateFloat64ToInt64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat64ToUint32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat64ToUint32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat64ToUint32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat64ToUint32EPNS1_4NodeE:
.LFB17464:
	.cfi_startproc
	endbr64
	movl	$171, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17464:
	.size	_ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat64ToUint32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat64ToUint32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector26VisitChangeFloat64ToUint64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector26VisitChangeFloat64ToUint64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector26VisitChangeFloat64ToUint64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector26VisitChangeFloat64ToUint64EPNS1_4NodeE:
.LFB17465:
	.cfi_startproc
	endbr64
	movl	$175, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17465:
	.size	_ZN2v88internal8compiler19InstructionSelector26VisitChangeFloat64ToUint64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector26VisitChangeFloat64ToUint64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector29VisitTruncateFloat64ToFloat32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector29VisitTruncateFloat64ToFloat32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector29VisitTruncateFloat64ToFloat32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector29VisitTruncateFloat64ToFloat32EPNS1_4NodeE:
.LFB17466:
	.cfi_startproc
	endbr64
	movl	$169, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17466:
	.size	_ZN2v88internal8compiler19InstructionSelector29VisitTruncateFloat64ToFloat32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector29VisitTruncateFloat64ToFloat32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector27VisitChangeFloat32ToFloat64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector27VisitChangeFloat32ToFloat64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector27VisitChangeFloat32ToFloat64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector27VisitChangeFloat32ToFloat64EPNS1_4NodeE:
.LFB17467:
	.cfi_startproc
	endbr64
	movl	$151, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17467:
	.size	_ZN2v88internal8compiler19InstructionSelector27VisitChangeFloat32ToFloat64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector27VisitChangeFloat32ToFloat64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector27VisitTruncateFloat32ToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector27VisitTruncateFloat32ToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector27VisitTruncateFloat32ToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector27VisitTruncateFloat32ToInt32EPNS1_4NodeE:
.LFB17468:
	.cfi_startproc
	endbr64
	movl	$152, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17468:
	.size	_ZN2v88internal8compiler19InstructionSelector27VisitTruncateFloat32ToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector27VisitTruncateFloat32ToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat32ToUint32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat32ToUint32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat32ToUint32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat32ToUint32EPNS1_4NodeE:
.LFB17469:
	.cfi_startproc
	endbr64
	movl	$153, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17469:
	.size	_ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat32ToUint32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat32ToUint32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitChangeInt32ToFloat64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitChangeInt32ToFloat64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitChangeInt32ToFloat64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitChangeInt32ToFloat64EPNS1_4NodeE:
.LFB17470:
	.cfi_startproc
	endbr64
	movl	$176, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17470:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitChangeInt32ToFloat64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitChangeInt32ToFloat64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitChangeInt64ToFloat64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitChangeInt64ToFloat64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitChangeInt64ToFloat64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitChangeInt64ToFloat64EPNS1_4NodeE:
.LFB17471:
	.cfi_startproc
	endbr64
	movl	$179, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17471:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitChangeInt64ToFloat64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitChangeInt64ToFloat64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector26VisitChangeUint32ToFloat64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector26VisitChangeUint32ToFloat64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector26VisitChangeUint32ToFloat64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector26VisitChangeUint32ToFloat64EPNS1_4NodeE:
.LFB17472:
	.cfi_startproc
	endbr64
	movl	$182, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17472:
	.size	_ZN2v88internal8compiler19InstructionSelector26VisitChangeUint32ToFloat64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector26VisitChangeUint32ToFloat64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector24VisitRoundFloat64ToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector24VisitRoundFloat64ToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector24VisitRoundFloat64ToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector24VisitRoundFloat64ToInt32EPNS1_4NodeE:
.LFB21836:
	.cfi_startproc
	endbr64
	movl	$170, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE21836:
	.size	_ZN2v88internal8compiler19InstructionSelector24VisitRoundFloat64ToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector24VisitRoundFloat64ToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector24VisitRoundInt32ToFloat32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector24VisitRoundInt32ToFloat32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector24VisitRoundInt32ToFloat32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector24VisitRoundInt32ToFloat32EPNS1_4NodeE:
.LFB17474:
	.cfi_startproc
	endbr64
	movl	$177, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17474:
	.size	_ZN2v88internal8compiler19InstructionSelector24VisitRoundInt32ToFloat32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector24VisitRoundInt32ToFloat32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector24VisitRoundInt64ToFloat32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector24VisitRoundInt64ToFloat32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector24VisitRoundInt64ToFloat32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector24VisitRoundInt64ToFloat32EPNS1_4NodeE:
.LFB17475:
	.cfi_startproc
	endbr64
	movl	$178, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17475:
	.size	_ZN2v88internal8compiler19InstructionSelector24VisitRoundInt64ToFloat32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector24VisitRoundInt64ToFloat32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitRoundUint64ToFloat32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitRoundUint64ToFloat32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitRoundUint64ToFloat32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitRoundUint64ToFloat32EPNS1_4NodeE:
.LFB17476:
	.cfi_startproc
	endbr64
	movl	$180, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17476:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitRoundUint64ToFloat32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitRoundUint64ToFloat32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector24VisitRoundInt64ToFloat64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector24VisitRoundInt64ToFloat64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector24VisitRoundInt64ToFloat64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector24VisitRoundInt64ToFloat64EPNS1_4NodeE:
.LFB21840:
	.cfi_startproc
	endbr64
	movl	$179, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE21840:
	.size	_ZN2v88internal8compiler19InstructionSelector24VisitRoundInt64ToFloat64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector24VisitRoundInt64ToFloat64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitRoundUint64ToFloat64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitRoundUint64ToFloat64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitRoundUint64ToFloat64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitRoundUint64ToFloat64EPNS1_4NodeE:
.LFB17478:
	.cfi_startproc
	endbr64
	movl	$181, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17478:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitRoundUint64ToFloat64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitRoundUint64ToFloat64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitRoundUint32ToFloat32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitRoundUint32ToFloat32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitRoundUint32ToFloat32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitRoundUint32ToFloat32EPNS1_4NodeE:
.LFB17479:
	.cfi_startproc
	endbr64
	movl	$183, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17479:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitRoundUint32ToFloat32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitRoundUint32ToFloat32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector26VisitBitcastFloat32ToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector26VisitBitcastFloat32ToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector26VisitBitcastFloat32ToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector26VisitBitcastFloat32ToInt32EPNS1_4NodeE:
.LFB17480:
	.cfi_startproc
	endbr64
	movl	$227, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17480:
	.size	_ZN2v88internal8compiler19InstructionSelector26VisitBitcastFloat32ToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector26VisitBitcastFloat32ToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector26VisitBitcastFloat64ToInt64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector26VisitBitcastFloat64ToInt64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector26VisitBitcastFloat64ToInt64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector26VisitBitcastFloat64ToInt64EPNS1_4NodeE:
.LFB17481:
	.cfi_startproc
	endbr64
	movl	$228, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17481:
	.size	_ZN2v88internal8compiler19InstructionSelector26VisitBitcastFloat64ToInt64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector26VisitBitcastFloat64ToInt64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector26VisitBitcastInt32ToFloat32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector26VisitBitcastInt32ToFloat32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector26VisitBitcastInt32ToFloat32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector26VisitBitcastInt32ToFloat32EPNS1_4NodeE:
.LFB17482:
	.cfi_startproc
	endbr64
	movl	$229, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17482:
	.size	_ZN2v88internal8compiler19InstructionSelector26VisitBitcastInt32ToFloat32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector26VisitBitcastInt32ToFloat32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector26VisitBitcastInt64ToFloat64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector26VisitBitcastInt64ToFloat64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector26VisitBitcastInt64ToFloat64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector26VisitBitcastInt64ToFloat64EPNS1_4NodeE:
.LFB17483:
	.cfi_startproc
	endbr64
	movl	$230, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17483:
	.size	_ZN2v88internal8compiler19InstructionSelector26VisitBitcastInt64ToFloat64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector26VisitBitcastInt64ToFloat64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector28VisitFloat64ExtractLowWord32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector28VisitFloat64ExtractLowWord32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector28VisitFloat64ExtractLowWord32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector28VisitFloat64ExtractLowWord32EPNS1_4NodeE:
.LFB17484:
	.cfi_startproc
	endbr64
	movl	$184, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17484:
	.size	_ZN2v88internal8compiler19InstructionSelector28VisitFloat64ExtractLowWord32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector28VisitFloat64ExtractLowWord32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector29VisitFloat64ExtractHighWord32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector29VisitFloat64ExtractHighWord32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector29VisitFloat64ExtractHighWord32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector29VisitFloat64ExtractHighWord32EPNS1_4NodeE:
.LFB17485:
	.cfi_startproc
	endbr64
	movl	$185, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17485:
	.size	_ZN2v88internal8compiler19InstructionSelector29VisitFloat64ExtractHighWord32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector29VisitFloat64ExtractHighWord32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector27VisitSignExtendWord8ToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector27VisitSignExtendWord8ToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector27VisitSignExtendWord8ToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector27VisitSignExtendWord8ToInt32EPNS1_4NodeE:
.LFB17486:
	.cfi_startproc
	endbr64
	movl	$204, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17486:
	.size	_ZN2v88internal8compiler19InstructionSelector27VisitSignExtendWord8ToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector27VisitSignExtendWord8ToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord16ToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord16ToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord16ToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord16ToInt32EPNS1_4NodeE:
.LFB17487:
	.cfi_startproc
	endbr64
	movl	$209, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17487:
	.size	_ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord16ToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord16ToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector27VisitSignExtendWord8ToInt64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector27VisitSignExtendWord8ToInt64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector27VisitSignExtendWord8ToInt64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector27VisitSignExtendWord8ToInt64EPNS1_4NodeE:
.LFB17488:
	.cfi_startproc
	endbr64
	movl	$206, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17488:
	.size	_ZN2v88internal8compiler19InstructionSelector27VisitSignExtendWord8ToInt64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector27VisitSignExtendWord8ToInt64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord16ToInt64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord16ToInt64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord16ToInt64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord16ToInt64EPNS1_4NodeE:
.LFB17489:
	.cfi_startproc
	endbr64
	movl	$211, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17489:
	.size	_ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord16ToInt64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord16ToInt64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord32ToInt64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord32ToInt64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord32ToInt64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord32ToInt64EPNS1_4NodeE:
.LFB17490:
	.cfi_startproc
	endbr64
	movl	$215, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17490:
	.size	_ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord32ToInt64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord32ToInt64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitFloat32RoundDownEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitFloat32RoundDownEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitFloat32RoundDownEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitFloat32RoundDownEPNS1_4NodeE:
.LFB17491:
	.cfi_startproc
	endbr64
	movl	$4194458, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitRREPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17491:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitFloat32RoundDownEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitFloat32RoundDownEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitFloat64RoundDownEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitFloat64RoundDownEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitFloat64RoundDownEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitFloat64RoundDownEPNS1_4NodeE:
.LFB17492:
	.cfi_startproc
	endbr64
	movl	$4194468, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitRREPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17492:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitFloat64RoundDownEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitFloat64RoundDownEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector19VisitFloat32RoundUpEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector19VisitFloat32RoundUpEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector19VisitFloat32RoundUpEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector19VisitFloat32RoundUpEPNS1_4NodeE:
.LFB17493:
	.cfi_startproc
	endbr64
	movl	$8388762, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitRREPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17493:
	.size	_ZN2v88internal8compiler19InstructionSelector19VisitFloat32RoundUpEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector19VisitFloat32RoundUpEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector19VisitFloat64RoundUpEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector19VisitFloat64RoundUpEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector19VisitFloat64RoundUpEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector19VisitFloat64RoundUpEPNS1_4NodeE:
.LFB17494:
	.cfi_startproc
	endbr64
	movl	$8388772, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitRREPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17494:
	.size	_ZN2v88internal8compiler19InstructionSelector19VisitFloat64RoundUpEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector19VisitFloat64RoundUpEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitFloat32RoundTruncateEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitFloat32RoundTruncateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitFloat32RoundTruncateEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitFloat32RoundTruncateEPNS1_4NodeE:
.LFB17495:
	.cfi_startproc
	endbr64
	movl	$12583066, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitRREPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17495:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitFloat32RoundTruncateEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitFloat32RoundTruncateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTruncateEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTruncateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTruncateEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTruncateEPNS1_4NodeE:
.LFB17496:
	.cfi_startproc
	endbr64
	movl	$12583076, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitRREPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17496:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTruncateEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTruncateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitFloat32RoundTiesEvenEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitFloat32RoundTiesEvenEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitFloat32RoundTiesEvenEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitFloat32RoundTiesEvenEPNS1_4NodeE:
.LFB17497:
	.cfi_startproc
	endbr64
	movl	$154, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitRREPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17497:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitFloat32RoundTiesEvenEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitFloat32RoundTiesEvenEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTiesEvenEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTiesEvenEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTiesEvenEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTiesEvenEPNS1_4NodeE:
.LFB17498:
	.cfi_startproc
	endbr64
	movl	$164, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitRREPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17498:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTiesEvenEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTiesEvenEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat64ToWord32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat64ToWord32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat64ToWord32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat64ToWord32EPNS1_4NodeE:
.LFB17499:
	.cfi_startproc
	endbr64
	movl	$26, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_17VisitRREPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17499:
	.size	_ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat64ToWord32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat64ToWord32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat32AddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32AddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32AddEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat32AddEPNS1_4NodeE:
.LFB17501:
	.cfi_startproc
	endbr64
	movl	$144, %ecx
	movl	$191, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_115VisitFloatBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeES7_
	.cfi_endproc
.LFE17501:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32AddEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat32AddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat32SubEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32SubEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32SubEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat32SubEPNS1_4NodeE:
.LFB17502:
	.cfi_startproc
	endbr64
	movl	$145, %ecx
	movl	$192, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_115VisitFloatBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeES7_
	.cfi_endproc
.LFE17502:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32SubEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat32SubEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat32MulEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32MulEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32MulEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat32MulEPNS1_4NodeE:
.LFB17503:
	.cfi_startproc
	endbr64
	movl	$146, %ecx
	movl	$193, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_115VisitFloatBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeES7_
	.cfi_endproc
.LFE17503:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32MulEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat32MulEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat32DivEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32DivEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32DivEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat32DivEPNS1_4NodeE:
.LFB17504:
	.cfi_startproc
	endbr64
	movl	$147, %ecx
	movl	$194, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_115VisitFloatBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeES7_
	.cfi_endproc
.LFE17504:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32DivEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat32DivEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat32AbsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32AbsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32AbsEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat32AbsEPNS1_4NodeE:
.LFB17505:
	.cfi_startproc
	endbr64
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L286
	movq	16(%rdx), %rdx
.L286:
	movl	$148, %r8d
	movl	$202, %ecx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_114VisitFloatUnopEPNS1_19InstructionSelectorEPNS1_4NodeES6_NS1_10ArchOpcodeES7_
	.cfi_endproc
.LFE17505:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32AbsEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat32AbsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat32MaxEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32MaxEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32MaxEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat32MaxEPNS1_4NodeE:
.LFB17506:
	.cfi_startproc
	endbr64
	movl	$165, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_18VisitRROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17506:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32MaxEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat32MaxEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat32MinEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32MinEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32MinEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat32MinEPNS1_4NodeE:
.LFB17507:
	.cfi_startproc
	endbr64
	movl	$167, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_18VisitRROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17507:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32MinEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat32MinEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat64AddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64AddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64AddEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat64AddEPNS1_4NodeE:
.LFB17508:
	.cfi_startproc
	endbr64
	movl	$156, %ecx
	movl	$196, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_115VisitFloatBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeES7_
	.cfi_endproc
.LFE17508:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64AddEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat64AddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat64SubEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64SubEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64SubEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat64SubEPNS1_4NodeE:
.LFB17509:
	.cfi_startproc
	endbr64
	movl	$157, %ecx
	movl	$197, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_115VisitFloatBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeES7_
	.cfi_endproc
.LFE17509:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64SubEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat64SubEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat64MulEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64MulEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64MulEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat64MulEPNS1_4NodeE:
.LFB17510:
	.cfi_startproc
	endbr64
	movl	$158, %ecx
	movl	$198, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_115VisitFloatBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeES7_
	.cfi_endproc
.LFE17510:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64MulEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat64MulEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat64DivEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64DivEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64DivEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat64DivEPNS1_4NodeE:
.LFB17511:
	.cfi_startproc
	endbr64
	movl	$159, %ecx
	movl	$199, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_115VisitFloatBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeES7_
	.cfi_endproc
.LFE17511:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64DivEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat64DivEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat64ModEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64ModEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64ModEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat64ModEPNS1_4NodeE:
.LFB17512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movabsq	$824633720825, %rax
	movq	%rax, -64(%rbp)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L294
	leaq	40(%rsi), %rax
.L295:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L296
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L296:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r14, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$160, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L299
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L295
.L299:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17512:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64ModEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat64ModEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat64MaxEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64MaxEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64MaxEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat64MaxEPNS1_4NodeE:
.LFB17513:
	.cfi_startproc
	endbr64
	movl	$166, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_18VisitRROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17513:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64MaxEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat64MaxEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat64MinEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64MinEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64MinEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat64MinEPNS1_4NodeE:
.LFB17514:
	.cfi_startproc
	endbr64
	movl	$168, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_18VisitRROEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.cfi_endproc
.LFE17514:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64MinEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat64MinEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat64AbsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64AbsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64AbsEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat64AbsEPNS1_4NodeE:
.LFB17515:
	.cfi_startproc
	endbr64
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L303
	movq	16(%rdx), %rdx
.L303:
	movl	$161, %r8d
	movl	$200, %ecx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_114VisitFloatUnopEPNS1_19InstructionSelectorEPNS1_4NodeES6_NS1_10ArchOpcodeES7_
	.cfi_endproc
.LFE17515:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64AbsEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat64AbsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTiesAwayEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTiesAwayEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTiesAwayEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTiesAwayEPNS1_4NodeE:
.LFB21818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21818:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTiesAwayEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTiesAwayEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat32NegEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32NegEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32NegEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat32NegEPNS1_4NodeE:
.LFB17517:
	.cfi_startproc
	endbr64
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L307
	movq	16(%rdx), %rdx
.L307:
	movl	$149, %r8d
	movl	$203, %ecx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_114VisitFloatUnopEPNS1_19InstructionSelectorEPNS1_4NodeES6_NS1_10ArchOpcodeES7_
	.cfi_endproc
.LFE17517:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32NegEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat32NegEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat64NegEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64NegEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64NegEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat64NegEPNS1_4NodeE:
.LFB17518:
	.cfi_startproc
	endbr64
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L309
	movq	16(%rdx), %rdx
.L309:
	movl	$162, %r8d
	movl	$201, %ecx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_114VisitFloatUnopEPNS1_19InstructionSelectorEPNS1_4NodeES6_NS1_10ArchOpcodeES7_
	.cfi_endproc
.LFE17518:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64NegEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat64NegEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector24VisitFloat64Ieee754BinopEPNS1_4NodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector24VisitFloat64Ieee754BinopEPNS1_4NodeEi
	.type	_ZN2v88internal8compiler19InstructionSelector24VisitFloat64Ieee754BinopEPNS1_4NodeEi, @function
_ZN2v88internal8compiler19InstructionSelector24VisitFloat64Ieee754BinopEPNS1_4NodeEi:
.LFB17519:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movzbl	23(%rsi), %eax
	movl	%edx, -52(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L311
	leaq	40(%rsi), %rax
.L312:
	movq	(%rax), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r13), %eax
	movl	%ebx, %r15d
	movabsq	$3058016714753, %r8
	salq	$3, %r15
	andl	$15, %eax
	orq	%r8, %r15
	cmpl	$15, %eax
	jne	.L313
	movq	32(%r13), %rax
	leaq	16(%rax), %r14
.L313:
	movq	(%r14), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$858993459201, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	-52(%rbp), %esi
	movq	%r15, %r8
	pushq	$0
	leaq	0(,%rbx,8), %rdx
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movabsq	$858993459201, %rcx
	orq	%rcx, %rdx
	movq	%r14, %rcx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	orl	$1073741824, 4(%rax)
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L312
	.cfi_endproc
.LFE17519:
	.size	_ZN2v88internal8compiler19InstructionSelector24VisitFloat64Ieee754BinopEPNS1_4NodeEi, .-_ZN2v88internal8compiler19InstructionSelector24VisitFloat64Ieee754BinopEPNS1_4NodeEi
	.section	.text._ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi
	.type	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi, @function
_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi:
.LFB17520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L316
	movq	16(%r12), %r12
.L316:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r15, %rsi
	movq	%r14, %rdi
	movabsq	$858993459201, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%r13d, %esi
	movq	%r14, %rdi
	xorl	%r9d, %r9d
	leaq	0(,%rbx,8), %rdx
	xorl	%r8d, %r8d
	movabsq	$858993459201, %rcx
	orq	%rcx, %rdx
	movq	%r12, %rcx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	orl	$1073741824, 4(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17520:
	.size	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi, .-_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi
	.section	.text._ZN2v88internal8compiler19InstructionSelector26IsTailCallAddressImmediateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector26IsTailCallAddressImmediateEv
	.type	_ZN2v88internal8compiler19InstructionSelector26IsTailCallAddressImmediateEv, @function
_ZN2v88internal8compiler19InstructionSelector26IsTailCallAddressImmediateEv:
.LFB17523:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE17523:
	.size	_ZN2v88internal8compiler19InstructionSelector26IsTailCallAddressImmediateEv, .-_ZN2v88internal8compiler19InstructionSelector26IsTailCallAddressImmediateEv
	.section	.text._ZN2v88internal8compiler19InstructionSelector38GetTempsCountForTailCallFromJSFunctionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector38GetTempsCountForTailCallFromJSFunctionEv
	.type	_ZN2v88internal8compiler19InstructionSelector38GetTempsCountForTailCallFromJSFunctionEv, @function
_ZN2v88internal8compiler19InstructionSelector38GetTempsCountForTailCallFromJSFunctionEv:
.LFB17524:
	.cfi_startproc
	endbr64
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE17524:
	.size	_ZN2v88internal8compiler19InstructionSelector38GetTempsCountForTailCallFromJSFunctionEv, .-_ZN2v88internal8compiler19InstructionSelector38GetTempsCountForTailCallFromJSFunctionEv
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitFloat32EqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitFloat32EqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitFloat32EqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitFloat32EqualEPNS1_4NodeE:
.LFB17552:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rdx
	movq	$0, -64(%rbp)
	movabsq	$77309411333, %rax
	movq	%rax, -80(%rbp)
	movl	$-1, -56(%rbp)
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_119VisitFloat32CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L323
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L323:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17552:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitFloat32EqualEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitFloat32EqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20VisitFloat32LessThanEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20VisitFloat32LessThanEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector20VisitFloat32LessThanEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector20VisitFloat32LessThanEPNS1_4NodeE:
.LFB17553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rdx
	movq	$0, -64(%rbp)
	movabsq	$38654705669, %rax
	movq	%rax, -80(%rbp)
	movl	$-1, -56(%rbp)
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_119VisitFloat32CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L327
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L327:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17553:
	.size	_ZN2v88internal8compiler19InstructionSelector20VisitFloat32LessThanEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector20VisitFloat32LessThanEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector27VisitFloat32LessThanOrEqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector27VisitFloat32LessThanOrEqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector27VisitFloat32LessThanOrEqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector27VisitFloat32LessThanOrEqualEPNS1_4NodeE:
.LFB17554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rdx
	movq	$0, -64(%rbp)
	movabsq	$30064771077, %rax
	movq	%rax, -80(%rbp)
	movl	$-1, -56(%rbp)
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_119VisitFloat32CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L331
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L331:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17554:
	.size	_ZN2v88internal8compiler19InstructionSelector27VisitFloat32LessThanOrEqualEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector27VisitFloat32LessThanOrEqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitFloat64EqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64EqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64EqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitFloat64EqualEPNS1_4NodeE:
.LFB17555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rdx
	movq	$0, -64(%rbp)
	movabsq	$77309411333, %rax
	movq	%rax, -80(%rbp)
	movl	$-1, -56(%rbp)
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_119VisitFloat64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L335
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L335:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17555:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64EqualEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitFloat64EqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20VisitFloat64LessThanEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20VisitFloat64LessThanEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector20VisitFloat64LessThanEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector20VisitFloat64LessThanEPNS1_4NodeE:
.LFB17556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$88, %rsp
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	32(%rsi), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L368
	leaq	16(%r14), %rax
	movq	16(%r14), %r14
.L368:
	movq	(%r14), %rsi
	movzwl	16(%rsi), %edx
	cmpw	$26, %dx
	sete	%cl
	je	.L340
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
.L341:
	movq	8(%rax), %rsi
	addq	$8, %rax
	movq	(%rsi), %rax
	movzwl	16(%rax), %eax
	cmpw	$26, %ax
	je	.L343
	movq	(%r12), %rdi
	testb	$1, 18(%rdi)
	je	.L343
	testb	%dl, %dl
	jne	.L369
.L343:
	testb	%cl, %cl
	je	.L344
	ucomisd	.LC1(%rip), %xmm0
	jp	.L344
	jne	.L344
	cmpw	$372, %ax
	je	.L346
.L344:
	leaq	-112(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r12, -80(%rbp)
	movabsq	$38654705669, %rax
	movq	$0, -96(%rbp)
	movq	%rax, -112(%rbp)
	movl	$-1, -88(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_119VisitFloat64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
.L336:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L370
	addq	$88, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	movsd	48(%rsi), %xmm0
	movl	$1, %edx
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L369:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L346:
	movabsq	$4294967301, %rax
	movq	$0, -96(%rbp)
	movq	%rax, -112(%rbp)
	movl	36(%r13), %eax
	movl	$-1, -88(%rbp)
	shrl	$5, %eax
	movq	%r12, -80(%rbp)
	movq	32(%rsi), %rcx
	andl	$1, %eax
	cmpb	$1, %al
	movzbl	23(%rsi), %eax
	sbbl	%r10d, %r10d
	andl	$-40, %r10d
	andl	$15, %eax
	addl	$195, %r10d
	cmpl	$15, %eax
	je	.L371
.L349:
	xorl	%r9d, %r9d
	leaq	-112(%rbp), %r8
	movq	%r14, %rdx
	movl	%r10d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_112VisitCompareEPNS1_19InstructionSelectorEiPNS1_4NodeES6_PNS1_17FlagsContinuationEb
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L371:
	movq	16(%rcx), %rcx
	jmp	.L349
.L370:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17556:
	.size	_ZN2v88internal8compiler19InstructionSelector20VisitFloat64LessThanEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector20VisitFloat64LessThanEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector27VisitFloat64LessThanOrEqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector27VisitFloat64LessThanOrEqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector27VisitFloat64LessThanOrEqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector27VisitFloat64LessThanOrEqualEPNS1_4NodeE:
.LFB17557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rdx
	movq	$0, -64(%rbp)
	movabsq	$30064771077, %rax
	movq	%rax, -80(%rbp)
	movl	$-1, -56(%rbp)
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_119VisitFloat64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L375
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L375:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17557:
	.size	_ZN2v88internal8compiler19InstructionSelector27VisitFloat64LessThanOrEqualEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector27VisitFloat64LessThanOrEqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector27VisitFloat64InsertLowWord32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector27VisitFloat64InsertLowWord32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector27VisitFloat64InsertLowWord32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector27VisitFloat64InsertLowWord32EPNS1_4NodeE:
.LFB17558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L377
	leaq	40(%rsi), %rax
.L378:
	movq	(%rax), %rbx
	movq	0(%r13), %rax
	movq	%r12, %rdi
	cmpw	$26, 16(%rax)
	movq	%rbx, %rsi
	jne	.L379
	movl	52(%rax), %ecx
	testl	%ecx, %ecx
	je	.L381
.L379:
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %r14
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$34359738369, %r8
	orq	%r8, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$186, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	leaq	16(%r13), %rax
	movq	16(%r13), %r13
	addq	$8, %rax
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L381:
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %r13
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$34359738369, %rcx
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	leaq	-40(%rbp), %rsp
	movq	%r13, %rcx
	salq	$3, %rdx
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movabsq	$927712935937, %rbx
	movl	$188, %esi
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17558:
	.size	_ZN2v88internal8compiler19InstructionSelector27VisitFloat64InsertLowWord32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector27VisitFloat64InsertLowWord32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector28VisitFloat64InsertHighWord32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector28VisitFloat64InsertHighWord32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector28VisitFloat64InsertHighWord32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector28VisitFloat64InsertHighWord32EPNS1_4NodeE:
.LFB17559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L383
	leaq	40(%rsi), %rax
.L384:
	movq	(%rax), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %r14
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$34359738369, %r8
	orq	%r8, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$187, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L383:
	.cfi_restore_state
	leaq	16(%r13), %rax
	movq	16(%r13), %r13
	addq	$8, %rax
	jmp	.L384
	.cfi_endproc
.LFE17559:
	.size	_ZN2v88internal8compiler19InstructionSelector28VisitFloat64InsertHighWord32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector28VisitFloat64InsertHighWord32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector22VisitFloat64SilenceNaNEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector22VisitFloat64SilenceNaNEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector22VisitFloat64SilenceNaNEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector22VisitFloat64SilenceNaNEPNS1_4NodeE:
.LFB17560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L387
	movq	16(%r12), %r12
.L387:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$189, %esi
	movabsq	$1065151889409, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17560:
	.size	_ZN2v88internal8compiler19InstructionSelector22VisitFloat64SilenceNaNEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector22VisitFloat64SilenceNaNEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector18VisitMemoryBarrierEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector18VisitMemoryBarrierEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector18VisitMemoryBarrierEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector18VisitMemoryBarrierEPNS1_4NodeE:
.LFB17561:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$141, %esi
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandEmPS3_@PLT
	.cfi_endproc
.LFE17561:
	.size	_ZN2v88internal8compiler19InstructionSelector18VisitMemoryBarrierEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector18VisitMemoryBarrierEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitS128ZeroEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitS128ZeroEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitS128ZeroEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitS128ZeroEPNS1_4NodeE:
.LFB17582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	addq	$8, %rsp
	movq	%r12, %rdi
	salq	$3, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$377, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandEmPS3_@PLT
	.cfi_endproc
.LFE17582:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitS128ZeroEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitS128ZeroEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitF64x2SplatEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitF64x2SplatEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitF64x2SplatEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitF64x2SplatEPNS1_4NodeE:
.LFB17583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L393
	movq	16(%r12), %r12
.L393:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$34359738369, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$238, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17583:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitF64x2SplatEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitF64x2SplatEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitF32x4SplatEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitF32x4SplatEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitF32x4SplatEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitF32x4SplatEPNS1_4NodeE:
.LFB17584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L396
	movq	16(%r12), %r12
.L396:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$34359738369, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$253, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17584:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitF32x4SplatEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitF32x4SplatEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitI64x2SplatEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitI64x2SplatEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitI64x2SplatEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitI64x2SplatEPNS1_4NodeE:
.LFB17585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L399
	movq	16(%r12), %r12
.L399:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$34359738369, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$273, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17585:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitI64x2SplatEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitI64x2SplatEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitI32x4SplatEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitI32x4SplatEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitI32x4SplatEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitI32x4SplatEPNS1_4NodeE:
.LFB17586:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L402
	movq	16(%r12), %r12
.L402:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$34359738369, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$293, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17586:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitI32x4SplatEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitI32x4SplatEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitI16x8SplatEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitI16x8SplatEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitI16x8SplatEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitI16x8SplatEPNS1_4NodeE:
.LFB17587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L405
	movq	16(%r12), %r12
.L405:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$34359738369, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$320, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17587:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitI16x8SplatEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitI16x8SplatEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitI8x16SplatEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitI8x16SplatEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitI8x16SplatEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitI8x16SplatEPNS1_4NodeE:
.LFB17588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L408
	movq	16(%r12), %r12
.L408:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$34359738369, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$351, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17588:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitI8x16SplatEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitI8x16SplatEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI64x2ShlEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2ShlEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2ShlEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI64x2ShlEPNS1_4NodeE:
.LFB17601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L411
	leaq	40(%r14), %rax
.L412:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$927712935937, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L413
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L413:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$277, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L416
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L412
.L416:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17601:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2ShlEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI64x2ShlEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI64x2ShrUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2ShrUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2ShrUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI64x2ShrUEPNS1_4NodeE:
.LFB17602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L418
	leaq	40(%r14), %rax
.L419:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$927712935937, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L420
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L420:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$288, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L423
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L419
.L423:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17602:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2ShrUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI64x2ShrUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI32x4ShlEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4ShlEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4ShlEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI32x4ShlEPNS1_4NodeE:
.LFB17603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L425
	leaq	40(%r14), %rax
.L426:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$927712935937, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L427
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L427:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$300, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L430
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L426
.L430:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17603:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4ShlEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI32x4ShlEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI32x4ShrSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4ShrSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4ShrSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI32x4ShrSEPNS1_4NodeE:
.LFB17604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L432
	leaq	40(%r14), %rax
.L433:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$927712935937, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L434
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L434:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$301, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L437
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L433
.L437:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17604:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4ShrSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI32x4ShrSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI32x4ShrUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4ShrUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4ShrUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI32x4ShrUEPNS1_4NodeE:
.LFB17605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L439
	leaq	40(%r14), %rax
.L440:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$927712935937, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L441
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L441:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$315, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L444
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L440
.L444:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17605:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4ShrUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI32x4ShrUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI16x8ShlEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8ShlEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8ShlEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI16x8ShlEPNS1_4NodeE:
.LFB17606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L446
	leaq	40(%r14), %rax
.L447:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$927712935937, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L448
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L448:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$326, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L451
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L446:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L447
.L451:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17606:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8ShlEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI16x8ShlEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI16x8ShrSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8ShrSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8ShrSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI16x8ShrSEPNS1_4NodeE:
.LFB17607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L453
	leaq	40(%r14), %rax
.L454:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$927712935937, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L455
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L455:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$327, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L458
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L453:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L454
.L458:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17607:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8ShrSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI16x8ShrSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI16x8ShrUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8ShrUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8ShrUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI16x8ShrUEPNS1_4NodeE:
.LFB17608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L460
	leaq	40(%r14), %rax
.L461:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$927712935937, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L462
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L462:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$343, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L465
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L460:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L461
.L465:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17608:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8ShrUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI16x8ShrUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI8x16ShlEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16ShlEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16ShlEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI8x16ShlEPNS1_4NodeE:
.LFB17609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movabsq	$377957122049, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	%eax, %eax
	salq	$3, %rax
	orq	%r13, %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movzbl	23(%r14), %eax
	orq	%r13, %rbx
	movq	%rbx, -72(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L467
	leaq	40(%r14), %rax
.L468:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$927712935937, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L469
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L469:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-80(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$2, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$356, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L472
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L468
.L472:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17609:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16ShlEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI8x16ShlEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI8x16ShrSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16ShrSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16ShrSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI8x16ShrSEPNS1_4NodeE:
.LFB17610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movabsq	$377957122049, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	%eax, %eax
	salq	$3, %rax
	orq	%r13, %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movzbl	23(%r14), %eax
	orq	%r13, %rbx
	movq	%rbx, -72(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L474
	leaq	40(%r14), %rax
.L475:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$927712935937, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L476
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L476:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-80(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$2, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$357, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L479
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L474:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L475
.L479:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17610:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16ShrSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI8x16ShrSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI8x16ShrUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16ShrUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16ShrUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI8x16ShrUEPNS1_4NodeE:
.LFB17611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movabsq	$377957122049, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	%eax, %eax
	salq	$3, %rax
	orq	%r13, %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movzbl	23(%r14), %eax
	orq	%r13, %rbx
	movq	%rbx, -72(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L481
	leaq	40(%r14), %rax
.L482:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$927712935937, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L483
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L483:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-80(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$2, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$372, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L486
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L482
.L486:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17611:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16ShrUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI8x16ShrUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector23VisitF32x4SConvertI32x4EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector23VisitF32x4SConvertI32x4EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector23VisitF32x4SConvertI32x4EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector23VisitF32x4SConvertI32x4EPNS1_4NodeE:
.LFB17612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L488
	movq	16(%r12), %r12
.L488:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$256, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17612:
	.size	_ZN2v88internal8compiler19InstructionSelector23VisitF32x4SConvertI32x4EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector23VisitF32x4SConvertI32x4EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitF32x4AbsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4AbsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4AbsEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitF32x4AbsEPNS1_4NodeE:
.LFB17613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L491
	movq	16(%r12), %r12
.L491:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$258, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17613:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4AbsEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitF32x4AbsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitF32x4NegEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4NegEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4NegEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitF32x4NegEPNS1_4NodeE:
.LFB17614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L494
	movq	16(%r12), %r12
.L494:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$259, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17614:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4NegEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitF32x4NegEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitF32x4RecipApproxEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitF32x4RecipApproxEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitF32x4RecipApproxEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitF32x4RecipApproxEPNS1_4NodeE:
.LFB17615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L497
	movq	16(%r12), %r12
.L497:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$260, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17615:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitF32x4RecipApproxEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitF32x4RecipApproxEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitF32x4RecipSqrtApproxEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitF32x4RecipSqrtApproxEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitF32x4RecipSqrtApproxEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitF32x4RecipSqrtApproxEPNS1_4NodeE:
.LFB17616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L500
	movq	16(%r12), %r12
.L500:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$261, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17616:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitF32x4RecipSqrtApproxEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitF32x4RecipSqrtApproxEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI64x2NegEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2NegEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2NegEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI64x2NegEPNS1_4NodeE:
.LFB17617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L503
	movq	16(%r12), %r12
.L503:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$276, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17617:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2NegEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI64x2NegEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector26VisitI32x4SConvertI16x8LowEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector26VisitI32x4SConvertI16x8LowEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector26VisitI32x4SConvertI16x8LowEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector26VisitI32x4SConvertI16x8LowEPNS1_4NodeE:
.LFB17618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L506
	movq	16(%r12), %r12
.L506:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$297, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17618:
	.size	_ZN2v88internal8compiler19InstructionSelector26VisitI32x4SConvertI16x8LowEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector26VisitI32x4SConvertI16x8LowEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector27VisitI32x4SConvertI16x8HighEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector27VisitI32x4SConvertI16x8HighEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector27VisitI32x4SConvertI16x8HighEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector27VisitI32x4SConvertI16x8HighEPNS1_4NodeE:
.LFB17619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L509
	movq	16(%r12), %r12
.L509:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$298, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17619:
	.size	_ZN2v88internal8compiler19InstructionSelector27VisitI32x4SConvertI16x8HighEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector27VisitI32x4SConvertI16x8HighEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI32x4NegEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4NegEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4NegEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI32x4NegEPNS1_4NodeE:
.LFB17620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L512
	movq	16(%r12), %r12
.L512:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$299, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17620:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4NegEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI32x4NegEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector26VisitI32x4UConvertI16x8LowEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector26VisitI32x4UConvertI16x8LowEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector26VisitI32x4UConvertI16x8LowEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector26VisitI32x4UConvertI16x8LowEPNS1_4NodeE:
.LFB17621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L515
	movq	16(%r12), %r12
.L515:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$313, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17621:
	.size	_ZN2v88internal8compiler19InstructionSelector26VisitI32x4UConvertI16x8LowEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector26VisitI32x4UConvertI16x8LowEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector27VisitI32x4UConvertI16x8HighEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector27VisitI32x4UConvertI16x8HighEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector27VisitI32x4UConvertI16x8HighEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector27VisitI32x4UConvertI16x8HighEPNS1_4NodeE:
.LFB17622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L518
	movq	16(%r12), %r12
.L518:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$314, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17622:
	.size	_ZN2v88internal8compiler19InstructionSelector27VisitI32x4UConvertI16x8HighEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector27VisitI32x4UConvertI16x8HighEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector26VisitI16x8SConvertI8x16LowEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector26VisitI16x8SConvertI8x16LowEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector26VisitI16x8SConvertI8x16LowEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector26VisitI16x8SConvertI8x16LowEPNS1_4NodeE:
.LFB17623:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L521
	movq	16(%r12), %r12
.L521:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$323, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17623:
	.size	_ZN2v88internal8compiler19InstructionSelector26VisitI16x8SConvertI8x16LowEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector26VisitI16x8SConvertI8x16LowEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector27VisitI16x8SConvertI8x16HighEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector27VisitI16x8SConvertI8x16HighEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector27VisitI16x8SConvertI8x16HighEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector27VisitI16x8SConvertI8x16HighEPNS1_4NodeE:
.LFB17624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L524
	movq	16(%r12), %r12
.L524:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$324, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17624:
	.size	_ZN2v88internal8compiler19InstructionSelector27VisitI16x8SConvertI8x16HighEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector27VisitI16x8SConvertI8x16HighEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI16x8NegEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8NegEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8NegEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI16x8NegEPNS1_4NodeE:
.LFB17625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L527
	movq	16(%r12), %r12
.L527:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$325, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17625:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8NegEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI16x8NegEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector26VisitI16x8UConvertI8x16LowEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector26VisitI16x8UConvertI8x16LowEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector26VisitI16x8UConvertI8x16LowEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector26VisitI16x8UConvertI8x16LowEPNS1_4NodeE:
.LFB17626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L530
	movq	16(%r12), %r12
.L530:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$341, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17626:
	.size	_ZN2v88internal8compiler19InstructionSelector26VisitI16x8UConvertI8x16LowEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector26VisitI16x8UConvertI8x16LowEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector27VisitI16x8UConvertI8x16HighEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector27VisitI16x8UConvertI8x16HighEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector27VisitI16x8UConvertI8x16HighEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector27VisitI16x8UConvertI8x16HighEPNS1_4NodeE:
.LFB17627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L533
	movq	16(%r12), %r12
.L533:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$342, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17627:
	.size	_ZN2v88internal8compiler19InstructionSelector27VisitI16x8UConvertI8x16HighEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector27VisitI16x8UConvertI8x16HighEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI8x16NegEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16NegEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16NegEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI8x16NegEPNS1_4NodeE:
.LFB17628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L536
	movq	16(%r12), %r12
.L536:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$355, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17628:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16NegEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI8x16NegEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitS128NotEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitS128NotEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitS128NotEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitS128NotEPNS1_4NodeE:
.LFB17629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L539
	movq	16(%r12), %r12
.L539:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$378, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17629:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitS128NotEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitS128NotEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitF64x2AddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2AddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2AddEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitF64x2AddEPNS1_4NodeE:
.LFB17630:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L542
	leaq	40(%rsi), %rax
.L543:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L544
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L544:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$243, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L542:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L543
	.cfi_endproc
.LFE17630:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2AddEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitF64x2AddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitF64x2SubEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2SubEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2SubEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitF64x2SubEPNS1_4NodeE:
.LFB17631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L547
	leaq	40(%rsi), %rax
.L548:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L549
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L549:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$244, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L547:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L548
	.cfi_endproc
.LFE17631:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2SubEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitF64x2SubEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitF64x2MulEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2MulEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2MulEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitF64x2MulEPNS1_4NodeE:
.LFB17632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L552
	leaq	40(%rsi), %rax
.L553:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L554
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L554:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$245, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L552:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L553
	.cfi_endproc
.LFE17632:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2MulEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitF64x2MulEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitF64x2DivEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2DivEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2DivEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitF64x2DivEPNS1_4NodeE:
.LFB17633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L557
	leaq	40(%rsi), %rax
.L558:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L559
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L559:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$246, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L557:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L558
	.cfi_endproc
.LFE17633:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2DivEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitF64x2DivEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitF64x2MinEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2MinEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2MinEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitF64x2MinEPNS1_4NodeE:
.LFB17634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L562
	leaq	40(%rsi), %rax
.L563:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L564
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L564:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$247, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L562:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L563
	.cfi_endproc
.LFE17634:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2MinEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitF64x2MinEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitF64x2MaxEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2MaxEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2MaxEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitF64x2MaxEPNS1_4NodeE:
.LFB17635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L567
	leaq	40(%rsi), %rax
.L568:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L569
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L569:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$248, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L567:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L568
	.cfi_endproc
.LFE17635:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2MaxEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitF64x2MaxEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitF64x2EqEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitF64x2EqEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitF64x2EqEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitF64x2EqEPNS1_4NodeE:
.LFB17636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L572
	leaq	40(%rsi), %rax
.L573:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L574
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L574:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$249, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L573
	.cfi_endproc
.LFE17636:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitF64x2EqEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitF64x2EqEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitF64x2NeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitF64x2NeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitF64x2NeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitF64x2NeEPNS1_4NodeE:
.LFB17637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L577
	leaq	40(%rsi), %rax
.L578:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L579
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L579:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$250, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L577:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L578
	.cfi_endproc
.LFE17637:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitF64x2NeEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitF64x2NeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitF64x2LtEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitF64x2LtEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitF64x2LtEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitF64x2LtEPNS1_4NodeE:
.LFB17638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L582
	leaq	40(%rsi), %rax
.L583:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L584
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L584:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$251, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L582:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L583
	.cfi_endproc
.LFE17638:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitF64x2LtEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitF64x2LtEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitF64x2LeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitF64x2LeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitF64x2LeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitF64x2LeEPNS1_4NodeE:
.LFB17639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L587
	leaq	40(%rsi), %rax
.L588:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L589
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L589:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$252, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L587:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L588
	.cfi_endproc
.LFE17639:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitF64x2LeEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitF64x2LeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitF32x4AddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4AddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4AddEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitF32x4AddEPNS1_4NodeE:
.LFB17640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L592
	leaq	40(%rsi), %rax
.L593:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L594
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L594:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$262, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L592:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L593
	.cfi_endproc
.LFE17640:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4AddEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitF32x4AddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector18VisitF32x4AddHorizEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector18VisitF32x4AddHorizEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector18VisitF32x4AddHorizEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector18VisitF32x4AddHorizEPNS1_4NodeE:
.LFB17641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L597
	leaq	40(%rsi), %rax
.L598:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L599
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L599:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$263, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L597:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L598
	.cfi_endproc
.LFE17641:
	.size	_ZN2v88internal8compiler19InstructionSelector18VisitF32x4AddHorizEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector18VisitF32x4AddHorizEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitF32x4SubEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4SubEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4SubEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitF32x4SubEPNS1_4NodeE:
.LFB17642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L602
	leaq	40(%rsi), %rax
.L603:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L604
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L604:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$264, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L602:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L603
	.cfi_endproc
.LFE17642:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4SubEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitF32x4SubEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitF32x4MulEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4MulEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4MulEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitF32x4MulEPNS1_4NodeE:
.LFB17643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L607
	leaq	40(%rsi), %rax
.L608:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L609
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L609:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$265, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L608
	.cfi_endproc
.LFE17643:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4MulEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitF32x4MulEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitF32x4DivEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4DivEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4DivEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitF32x4DivEPNS1_4NodeE:
.LFB17644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L612
	leaq	40(%rsi), %rax
.L613:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L614
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L614:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$266, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L612:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L613
	.cfi_endproc
.LFE17644:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4DivEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitF32x4DivEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitF32x4MinEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4MinEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4MinEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitF32x4MinEPNS1_4NodeE:
.LFB17645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L617
	leaq	40(%rsi), %rax
.L618:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L619
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L619:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$267, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L617:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L618
	.cfi_endproc
.LFE17645:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4MinEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitF32x4MinEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitF32x4MaxEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4MaxEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4MaxEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitF32x4MaxEPNS1_4NodeE:
.LFB17646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L622
	leaq	40(%rsi), %rax
.L623:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L624
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L624:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$268, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L622:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L623
	.cfi_endproc
.LFE17646:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4MaxEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitF32x4MaxEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitF32x4EqEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitF32x4EqEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitF32x4EqEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitF32x4EqEPNS1_4NodeE:
.LFB17647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L627
	leaq	40(%rsi), %rax
.L628:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L629
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L629:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$269, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L627:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L628
	.cfi_endproc
.LFE17647:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitF32x4EqEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitF32x4EqEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitF32x4NeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitF32x4NeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitF32x4NeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitF32x4NeEPNS1_4NodeE:
.LFB17648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L632
	leaq	40(%rsi), %rax
.L633:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L634
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L634:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$270, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L632:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L633
	.cfi_endproc
.LFE17648:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitF32x4NeEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitF32x4NeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitF32x4LtEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitF32x4LtEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitF32x4LtEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitF32x4LtEPNS1_4NodeE:
.LFB17649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L637
	leaq	40(%rsi), %rax
.L638:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L639
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L639:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$271, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L637:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L638
	.cfi_endproc
.LFE17649:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitF32x4LtEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitF32x4LtEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitF32x4LeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitF32x4LeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitF32x4LeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitF32x4LeEPNS1_4NodeE:
.LFB17650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L642
	leaq	40(%rsi), %rax
.L643:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L644
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L644:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$272, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L642:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L643
	.cfi_endproc
.LFE17650:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitF32x4LeEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitF32x4LeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI64x2AddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2AddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2AddEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI64x2AddEPNS1_4NodeE:
.LFB17651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L647
	leaq	40(%rsi), %rax
.L648:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L649
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L649:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$279, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L647:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L648
	.cfi_endproc
.LFE17651:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2AddEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI64x2AddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI64x2SubEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2SubEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2SubEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI64x2SubEPNS1_4NodeE:
.LFB17652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L652
	leaq	40(%rsi), %rax
.L653:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L654
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L654:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$280, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L652:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L653
	.cfi_endproc
.LFE17652:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2SubEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI64x2SubEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitI64x2EqEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitI64x2EqEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitI64x2EqEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitI64x2EqEPNS1_4NodeE:
.LFB17653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L657
	leaq	40(%rsi), %rax
.L658:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L659
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L659:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$284, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L657:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L658
	.cfi_endproc
.LFE17653:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitI64x2EqEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitI64x2EqEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI64x2GtSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GtSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GtSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GtSEPNS1_4NodeE:
.LFB17654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L662
	leaq	40(%rsi), %rax
.L663:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L664
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L664:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$286, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L662:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L663
	.cfi_endproc
.LFE17654:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GtSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GtSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI32x4AddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4AddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4AddEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI32x4AddEPNS1_4NodeE:
.LFB17655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L667
	leaq	40(%rsi), %rax
.L668:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L669
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L669:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$302, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L667:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L668
	.cfi_endproc
.LFE17655:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4AddEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI32x4AddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector18VisitI32x4AddHorizEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector18VisitI32x4AddHorizEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector18VisitI32x4AddHorizEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector18VisitI32x4AddHorizEPNS1_4NodeE:
.LFB17656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L672
	leaq	40(%rsi), %rax
.L673:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L674
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L674:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$303, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L672:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L673
	.cfi_endproc
.LFE17656:
	.size	_ZN2v88internal8compiler19InstructionSelector18VisitI32x4AddHorizEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector18VisitI32x4AddHorizEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI32x4SubEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4SubEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4SubEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI32x4SubEPNS1_4NodeE:
.LFB17657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L677
	leaq	40(%rsi), %rax
.L678:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L679
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L679:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$304, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L677:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L678
	.cfi_endproc
.LFE17657:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4SubEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI32x4SubEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI32x4MulEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4MulEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4MulEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI32x4MulEPNS1_4NodeE:
.LFB17658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L682
	leaq	40(%rsi), %rax
.L683:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L684
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L684:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$305, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L682:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L683
	.cfi_endproc
.LFE17658:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4MulEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI32x4MulEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI32x4MinSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MinSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MinSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MinSEPNS1_4NodeE:
.LFB17659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L687
	leaq	40(%rsi), %rax
.L688:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L689
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L689:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$306, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L687:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L688
	.cfi_endproc
.LFE17659:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MinSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MinSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI32x4MaxSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MaxSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MaxSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MaxSEPNS1_4NodeE:
.LFB17660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L692
	leaq	40(%rsi), %rax
.L693:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L694
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L694:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$307, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L692:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L693
	.cfi_endproc
.LFE17660:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MaxSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MaxSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitI32x4EqEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitI32x4EqEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitI32x4EqEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitI32x4EqEPNS1_4NodeE:
.LFB17661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L697
	leaq	40(%rsi), %rax
.L698:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L699
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L699:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$308, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L697:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L698
	.cfi_endproc
.LFE17661:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitI32x4EqEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitI32x4EqEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI32x4GtSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GtSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GtSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GtSEPNS1_4NodeE:
.LFB17662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L702
	leaq	40(%rsi), %rax
.L703:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L704
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L704:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$310, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L702:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L703
	.cfi_endproc
.LFE17662:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GtSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GtSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI32x4GeSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GeSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GeSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GeSEPNS1_4NodeE:
.LFB17663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L707
	leaq	40(%rsi), %rax
.L708:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L709
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L709:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$311, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L707:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L708
	.cfi_endproc
.LFE17663:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GeSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GeSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI32x4MinUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MinUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MinUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MinUEPNS1_4NodeE:
.LFB17664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L712
	leaq	40(%rsi), %rax
.L713:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L714
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L714:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$316, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L712:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L713
	.cfi_endproc
.LFE17664:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MinUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MinUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI32x4MaxUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MaxUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MaxUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MaxUEPNS1_4NodeE:
.LFB17665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L717
	leaq	40(%rsi), %rax
.L718:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L719
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L719:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$317, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L717:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L718
	.cfi_endproc
.LFE17665:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MaxUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MaxUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI32x4GeUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GeUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GeUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GeUEPNS1_4NodeE:
.LFB17666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L722
	leaq	40(%rsi), %rax
.L723:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L724
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L724:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$319, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L722:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L723
	.cfi_endproc
.LFE17666:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GeUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GeUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector23VisitI16x8SConvertI32x4EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector23VisitI16x8SConvertI32x4EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector23VisitI16x8SConvertI32x4EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector23VisitI16x8SConvertI32x4EPNS1_4NodeE:
.LFB17667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L727
	leaq	40(%rsi), %rax
.L728:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L729
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L729:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$328, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L727:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L728
	.cfi_endproc
.LFE17667:
	.size	_ZN2v88internal8compiler19InstructionSelector23VisitI16x8SConvertI32x4EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector23VisitI16x8SConvertI32x4EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI16x8AddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8AddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8AddEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI16x8AddEPNS1_4NodeE:
.LFB17668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L732
	leaq	40(%rsi), %rax
.L733:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L734
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L734:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$329, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L732:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L733
	.cfi_endproc
.LFE17668:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8AddEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI16x8AddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector22VisitI16x8AddSaturateSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector22VisitI16x8AddSaturateSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector22VisitI16x8AddSaturateSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector22VisitI16x8AddSaturateSEPNS1_4NodeE:
.LFB17669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L737
	leaq	40(%rsi), %rax
.L738:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L739
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L739:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$330, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L737:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L738
	.cfi_endproc
.LFE17669:
	.size	_ZN2v88internal8compiler19InstructionSelector22VisitI16x8AddSaturateSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector22VisitI16x8AddSaturateSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector18VisitI16x8AddHorizEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector18VisitI16x8AddHorizEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector18VisitI16x8AddHorizEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector18VisitI16x8AddHorizEPNS1_4NodeE:
.LFB17670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L742
	leaq	40(%rsi), %rax
.L743:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L744
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L744:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$331, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L742:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L743
	.cfi_endproc
.LFE17670:
	.size	_ZN2v88internal8compiler19InstructionSelector18VisitI16x8AddHorizEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector18VisitI16x8AddHorizEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI16x8SubEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8SubEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8SubEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI16x8SubEPNS1_4NodeE:
.LFB17671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L747
	leaq	40(%rsi), %rax
.L748:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L749
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L749:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$332, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L747:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L748
	.cfi_endproc
.LFE17671:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8SubEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI16x8SubEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector22VisitI16x8SubSaturateSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector22VisitI16x8SubSaturateSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector22VisitI16x8SubSaturateSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector22VisitI16x8SubSaturateSEPNS1_4NodeE:
.LFB17672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L752
	leaq	40(%rsi), %rax
.L753:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L754
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L754:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$333, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L752:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L753
	.cfi_endproc
.LFE17672:
	.size	_ZN2v88internal8compiler19InstructionSelector22VisitI16x8SubSaturateSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector22VisitI16x8SubSaturateSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI16x8MulEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8MulEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8MulEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI16x8MulEPNS1_4NodeE:
.LFB17673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L757
	leaq	40(%rsi), %rax
.L758:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L759
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L759:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$334, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L757:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L758
	.cfi_endproc
.LFE17673:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8MulEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI16x8MulEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI16x8MinSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MinSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MinSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MinSEPNS1_4NodeE:
.LFB17674:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L762
	leaq	40(%rsi), %rax
.L763:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L764
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L764:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$335, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L762:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L763
	.cfi_endproc
.LFE17674:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MinSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MinSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI16x8MaxSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MaxSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MaxSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MaxSEPNS1_4NodeE:
.LFB17675:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L767
	leaq	40(%rsi), %rax
.L768:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L769
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L769:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$336, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L767:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L768
	.cfi_endproc
.LFE17675:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MaxSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MaxSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitI16x8EqEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitI16x8EqEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitI16x8EqEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitI16x8EqEPNS1_4NodeE:
.LFB17676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L772
	leaq	40(%rsi), %rax
.L773:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L774
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L774:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$337, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L772:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L773
	.cfi_endproc
.LFE17676:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitI16x8EqEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitI16x8EqEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI16x8GtSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GtSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GtSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GtSEPNS1_4NodeE:
.LFB17677:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L777
	leaq	40(%rsi), %rax
.L778:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L779
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L779:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$339, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L777:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L778
	.cfi_endproc
.LFE17677:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GtSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GtSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI16x8GeSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GeSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GeSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GeSEPNS1_4NodeE:
.LFB17678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L782
	leaq	40(%rsi), %rax
.L783:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L784
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L784:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$340, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L782:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L783
	.cfi_endproc
.LFE17678:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GeSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GeSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector22VisitI16x8AddSaturateUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector22VisitI16x8AddSaturateUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector22VisitI16x8AddSaturateUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector22VisitI16x8AddSaturateUEPNS1_4NodeE:
.LFB17679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L787
	leaq	40(%rsi), %rax
.L788:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L789
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L789:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$345, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L787:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L788
	.cfi_endproc
.LFE17679:
	.size	_ZN2v88internal8compiler19InstructionSelector22VisitI16x8AddSaturateUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector22VisitI16x8AddSaturateUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector22VisitI16x8SubSaturateUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector22VisitI16x8SubSaturateUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector22VisitI16x8SubSaturateUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector22VisitI16x8SubSaturateUEPNS1_4NodeE:
.LFB17680:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L792
	leaq	40(%rsi), %rax
.L793:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L794
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L794:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$346, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L792:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L793
	.cfi_endproc
.LFE17680:
	.size	_ZN2v88internal8compiler19InstructionSelector22VisitI16x8SubSaturateUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector22VisitI16x8SubSaturateUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI16x8MinUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MinUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MinUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MinUEPNS1_4NodeE:
.LFB17681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L797
	leaq	40(%rsi), %rax
.L798:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L799
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L799:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$347, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L797:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L798
	.cfi_endproc
.LFE17681:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MinUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MinUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI16x8MaxUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MaxUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MaxUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MaxUEPNS1_4NodeE:
.LFB17682:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L802
	leaq	40(%rsi), %rax
.L803:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L804
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L804:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$348, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L802:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L803
	.cfi_endproc
.LFE17682:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MaxUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MaxUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI16x8GeUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GeUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GeUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GeUEPNS1_4NodeE:
.LFB17683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L807
	leaq	40(%rsi), %rax
.L808:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L809
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L809:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$350, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L807:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L808
	.cfi_endproc
.LFE17683:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GeUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GeUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector23VisitI8x16SConvertI16x8EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector23VisitI8x16SConvertI16x8EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector23VisitI8x16SConvertI16x8EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector23VisitI8x16SConvertI16x8EPNS1_4NodeE:
.LFB17684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L812
	leaq	40(%rsi), %rax
.L813:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L814
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L814:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$354, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L812:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L813
	.cfi_endproc
.LFE17684:
	.size	_ZN2v88internal8compiler19InstructionSelector23VisitI8x16SConvertI16x8EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector23VisitI8x16SConvertI16x8EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI8x16AddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16AddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16AddEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI8x16AddEPNS1_4NodeE:
.LFB17685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L817
	leaq	40(%rsi), %rax
.L818:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L819
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L819:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$358, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L817:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L818
	.cfi_endproc
.LFE17685:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16AddEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI8x16AddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector22VisitI8x16AddSaturateSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector22VisitI8x16AddSaturateSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector22VisitI8x16AddSaturateSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector22VisitI8x16AddSaturateSEPNS1_4NodeE:
.LFB17686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L822
	leaq	40(%rsi), %rax
.L823:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L824
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L824:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$359, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L822:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L823
	.cfi_endproc
.LFE17686:
	.size	_ZN2v88internal8compiler19InstructionSelector22VisitI8x16AddSaturateSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector22VisitI8x16AddSaturateSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI8x16SubEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16SubEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16SubEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI8x16SubEPNS1_4NodeE:
.LFB17687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L827
	leaq	40(%rsi), %rax
.L828:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L829
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L829:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$360, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L827:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L828
	.cfi_endproc
.LFE17687:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16SubEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI8x16SubEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector22VisitI8x16SubSaturateSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector22VisitI8x16SubSaturateSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector22VisitI8x16SubSaturateSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector22VisitI8x16SubSaturateSEPNS1_4NodeE:
.LFB17688:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L832
	leaq	40(%rsi), %rax
.L833:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L834
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L834:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$361, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L832:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L833
	.cfi_endproc
.LFE17688:
	.size	_ZN2v88internal8compiler19InstructionSelector22VisitI8x16SubSaturateSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector22VisitI8x16SubSaturateSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI8x16MinSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MinSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MinSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MinSEPNS1_4NodeE:
.LFB17689:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L837
	leaq	40(%rsi), %rax
.L838:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L839
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L839:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$363, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L837:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L838
	.cfi_endproc
.LFE17689:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MinSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MinSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI8x16MaxSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MaxSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MaxSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MaxSEPNS1_4NodeE:
.LFB17690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L842
	leaq	40(%rsi), %rax
.L843:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L844
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L844:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$364, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L842:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L843
	.cfi_endproc
.LFE17690:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MaxSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MaxSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitI8x16EqEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitI8x16EqEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitI8x16EqEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitI8x16EqEPNS1_4NodeE:
.LFB17691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L847
	leaq	40(%rsi), %rax
.L848:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L849
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L849:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$365, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L847:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L848
	.cfi_endproc
.LFE17691:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitI8x16EqEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitI8x16EqEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI8x16GtSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GtSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GtSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GtSEPNS1_4NodeE:
.LFB17692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L852
	leaq	40(%rsi), %rax
.L853:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L854
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L854:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$367, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L852:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L853
	.cfi_endproc
.LFE17692:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GtSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GtSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI8x16GeSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GeSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GeSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GeSEPNS1_4NodeE:
.LFB17693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L857
	leaq	40(%rsi), %rax
.L858:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L859
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L859:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$368, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L857:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L858
	.cfi_endproc
.LFE17693:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GeSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GeSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector22VisitI8x16AddSaturateUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector22VisitI8x16AddSaturateUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector22VisitI8x16AddSaturateUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector22VisitI8x16AddSaturateUEPNS1_4NodeE:
.LFB17694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L862
	leaq	40(%rsi), %rax
.L863:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L864
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L864:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$370, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L862:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L863
	.cfi_endproc
.LFE17694:
	.size	_ZN2v88internal8compiler19InstructionSelector22VisitI8x16AddSaturateUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector22VisitI8x16AddSaturateUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector22VisitI8x16SubSaturateUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector22VisitI8x16SubSaturateUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector22VisitI8x16SubSaturateUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector22VisitI8x16SubSaturateUEPNS1_4NodeE:
.LFB17695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L867
	leaq	40(%rsi), %rax
.L868:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L869
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L869:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$371, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L867:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L868
	.cfi_endproc
.LFE17695:
	.size	_ZN2v88internal8compiler19InstructionSelector22VisitI8x16SubSaturateUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector22VisitI8x16SubSaturateUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI8x16MinUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MinUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MinUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MinUEPNS1_4NodeE:
.LFB17696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L872
	leaq	40(%rsi), %rax
.L873:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L874
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L874:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$373, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L872:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L873
	.cfi_endproc
.LFE17696:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MinUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MinUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI8x16MaxUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MaxUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MaxUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MaxUEPNS1_4NodeE:
.LFB17697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L877
	leaq	40(%rsi), %rax
.L878:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L879
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L879:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$374, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L877:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L878
	.cfi_endproc
.LFE17697:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MaxUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MaxUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI8x16GeUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GeUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GeUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GeUEPNS1_4NodeE:
.LFB17698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L882
	leaq	40(%rsi), %rax
.L883:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L884
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L884:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$376, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L882:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L883
	.cfi_endproc
.LFE17698:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GeUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GeUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitS128AndEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitS128AndEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitS128AndEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitS128AndEPNS1_4NodeE:
.LFB17699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L887
	leaq	40(%rsi), %rax
.L888:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L889
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L889:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$379, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L887:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L888
	.cfi_endproc
.LFE17699:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitS128AndEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitS128AndEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector11VisitS128OrEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector11VisitS128OrEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector11VisitS128OrEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector11VisitS128OrEPNS1_4NodeE:
.LFB17700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L892
	leaq	40(%rsi), %rax
.L893:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L894
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L894:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$380, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L892:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L893
	.cfi_endproc
.LFE17700:
	.size	_ZN2v88internal8compiler19InstructionSelector11VisitS128OrEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector11VisitS128OrEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitS128XorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitS128XorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitS128XorEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitS128XorEPNS1_4NodeE:
.LFB17701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L897
	leaq	40(%rsi), %rax
.L898:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L899
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L899:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$381, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L897:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L898
	.cfi_endproc
.LFE17701:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitS128XorEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitS128XorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitI64x2NeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitI64x2NeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitI64x2NeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitI64x2NeEPNS1_4NodeE:
.LFB17702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L902
	leaq	40(%r14), %rax
.L903:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$377957122049, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L904
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L904:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$285, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L907
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L902:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L903
.L907:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17702:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitI64x2NeEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitI64x2NeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI64x2GeSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GeSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GeSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GeSEPNS1_4NodeE:
.LFB17703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L909
	leaq	40(%r14), %rax
.L910:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$377957122049, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L911
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L911:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$287, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L914
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L909:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L910
.L914:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17703:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GeSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GeSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI64x2GtUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GtUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GtUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GtUEPNS1_4NodeE:
.LFB17704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L916
	leaq	40(%r14), %rax
.L917:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$377957122049, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L918
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L918:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$291, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L921
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L916:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L917
.L921:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17704:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GtUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GtUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI64x2GeUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GeUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GeUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GeUEPNS1_4NodeE:
.LFB17705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L923
	leaq	40(%r14), %rax
.L924:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$377957122049, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L925
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L925:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$292, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L928
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L923:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L924
.L928:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17705:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GeUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GeUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitI32x4NeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitI32x4NeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitI32x4NeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitI32x4NeEPNS1_4NodeE:
.LFB17706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L930
	leaq	40(%r14), %rax
.L931:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$377957122049, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L932
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L932:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$309, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L935
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L930:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L931
.L935:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17706:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitI32x4NeEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitI32x4NeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI32x4GtUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GtUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GtUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GtUEPNS1_4NodeE:
.LFB17707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L937
	leaq	40(%r14), %rax
.L938:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$377957122049, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L939
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L939:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$318, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L942
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L937:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L938
.L942:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17707:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GtUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GtUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitI16x8NeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitI16x8NeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitI16x8NeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitI16x8NeEPNS1_4NodeE:
.LFB17708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L944
	leaq	40(%r14), %rax
.L945:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$377957122049, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L946
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L946:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$338, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L949
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L944:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L945
.L949:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17708:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitI16x8NeEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitI16x8NeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI16x8GtUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GtUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GtUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GtUEPNS1_4NodeE:
.LFB17709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L951
	leaq	40(%r14), %rax
.L952:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$377957122049, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L953
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L953:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$349, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L956
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L951:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L952
.L956:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17709:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GtUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GtUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitI8x16NeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitI8x16NeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitI8x16NeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitI8x16NeEPNS1_4NodeE:
.LFB17710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L958
	leaq	40(%r14), %rax
.L959:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$377957122049, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L960
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L960:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$366, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L963
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L958:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L959
.L963:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17710:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitI8x16NeEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitI8x16NeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI8x16GtUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GtUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GtUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GtUEPNS1_4NodeE:
.LFB17711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L965
	leaq	40(%r14), %rax
.L966:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$377957122049, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L967
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L967:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$375, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L970
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L965:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L966
.L970:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17711:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GtUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GtUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitS1x2AnyTrueEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitS1x2AnyTrueEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitS1x2AnyTrueEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitS1x2AnyTrueEPNS1_4NodeE:
.LFB17712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movabsq	$377957122049, %rdx
	movl	%eax, %eax
	salq	$3, %rax
	orq	%rdx, %rax
	leaq	32(%r14), %rdx
	movq	%rax, -64(%rbp)
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L972
	movq	32(%r14), %rax
	leaq	16(%rax), %rdx
.L972:
	movabsq	$927712935937, %r15
	movq	(%rdx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%ebx, %r13d
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	salq	$3, %r13
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	orq	%r15, %r13
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	leaq	-64(%rbp), %r9
	movq	%r13, %rcx
	movq	%r12, %rdi
	leaq	0(,%rbx,8), %rdx
	movl	$1, %r8d
	movl	$409, %esi
	orq	%r15, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L976
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L976:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17712:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitS1x2AnyTrueEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitS1x2AnyTrueEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitS1x4AnyTrueEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitS1x4AnyTrueEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitS1x4AnyTrueEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitS1x4AnyTrueEPNS1_4NodeE:
.LFB17713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movabsq	$377957122049, %rdx
	movl	%eax, %eax
	salq	$3, %rax
	orq	%rdx, %rax
	leaq	32(%r14), %rdx
	movq	%rax, -64(%rbp)
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L978
	movq	32(%r14), %rax
	leaq	16(%rax), %rdx
.L978:
	movabsq	$927712935937, %r15
	movq	(%rdx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%ebx, %r13d
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	salq	$3, %r13
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	orq	%r15, %r13
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	leaq	-64(%rbp), %r9
	movq	%r13, %rcx
	movq	%r12, %rdi
	leaq	0(,%rbx,8), %rdx
	movl	$1, %r8d
	movl	$411, %esi
	orq	%r15, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L982
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L982:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17713:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitS1x4AnyTrueEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitS1x4AnyTrueEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitS1x8AnyTrueEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitS1x8AnyTrueEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitS1x8AnyTrueEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitS1x8AnyTrueEPNS1_4NodeE:
.LFB17714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movabsq	$377957122049, %rdx
	movl	%eax, %eax
	salq	$3, %rax
	orq	%rdx, %rax
	leaq	32(%r14), %rdx
	movq	%rax, -64(%rbp)
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L984
	movq	32(%r14), %rax
	leaq	16(%rax), %rdx
.L984:
	movabsq	$927712935937, %r15
	movq	(%rdx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%ebx, %r13d
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	salq	$3, %r13
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	orq	%r15, %r13
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	leaq	-64(%rbp), %r9
	movq	%r13, %rcx
	movq	%r12, %rdi
	leaq	0(,%rbx,8), %rdx
	movl	$1, %r8d
	movl	$413, %esi
	orq	%r15, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L988
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L988:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17714:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitS1x8AnyTrueEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitS1x8AnyTrueEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitS1x16AnyTrueEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitS1x16AnyTrueEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitS1x16AnyTrueEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitS1x16AnyTrueEPNS1_4NodeE:
.LFB17715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movabsq	$377957122049, %rdx
	movl	%eax, %eax
	salq	$3, %rax
	orq	%rdx, %rax
	leaq	32(%r14), %rdx
	movq	%rax, -64(%rbp)
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L990
	movq	32(%r14), %rax
	leaq	16(%rax), %rdx
.L990:
	movabsq	$927712935937, %r15
	movq	(%rdx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%ebx, %r13d
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	salq	$3, %r13
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	orq	%r15, %r13
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	leaq	-64(%rbp), %r9
	movq	%r13, %rcx
	movq	%r12, %rdi
	leaq	0(,%rbx,8), %rdx
	movl	$1, %r8d
	movl	$415, %esi
	orq	%r15, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L994
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L994:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17715:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitS1x16AnyTrueEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitS1x16AnyTrueEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitS1x2AllTrueEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitS1x2AllTrueEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitS1x2AllTrueEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitS1x2AllTrueEPNS1_4NodeE:
.LFB17716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movabsq	$377957122049, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	%eax, %eax
	salq	$3, %rax
	orq	%r13, %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movzbl	23(%r14), %eax
	orq	%r13, %rbx
	leaq	32(%r14), %rdx
	movq	%rbx, -72(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L996
	movq	32(%r14), %rax
	leaq	16(%rax), %rdx
.L996:
	movabsq	$927712935937, %r15
	movq	(%rdx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%ebx, %r13d
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	salq	$3, %r13
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	orq	%r15, %r13
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	leaq	-80(%rbp), %r9
	movq	%r13, %rcx
	movq	%r12, %rdi
	leaq	0(,%rbx,8), %rdx
	movl	$2, %r8d
	movl	$410, %esi
	orq	%r15, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1000
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1000:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17716:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitS1x2AllTrueEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitS1x2AllTrueEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitS1x4AllTrueEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitS1x4AllTrueEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitS1x4AllTrueEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitS1x4AllTrueEPNS1_4NodeE:
.LFB17717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movabsq	$377957122049, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	%eax, %eax
	salq	$3, %rax
	orq	%r13, %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movzbl	23(%r14), %eax
	orq	%r13, %rbx
	leaq	32(%r14), %rdx
	movq	%rbx, -72(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1002
	movq	32(%r14), %rax
	leaq	16(%rax), %rdx
.L1002:
	movabsq	$927712935937, %r15
	movq	(%rdx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%ebx, %r13d
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	salq	$3, %r13
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	orq	%r15, %r13
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	leaq	-80(%rbp), %r9
	movq	%r13, %rcx
	movq	%r12, %rdi
	leaq	0(,%rbx,8), %rdx
	movl	$2, %r8d
	movl	$412, %esi
	orq	%r15, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1006
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1006:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17717:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitS1x4AllTrueEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitS1x4AllTrueEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitS1x8AllTrueEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitS1x8AllTrueEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitS1x8AllTrueEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitS1x8AllTrueEPNS1_4NodeE:
.LFB17718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movabsq	$377957122049, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	%eax, %eax
	salq	$3, %rax
	orq	%r13, %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movzbl	23(%r14), %eax
	orq	%r13, %rbx
	leaq	32(%r14), %rdx
	movq	%rbx, -72(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1008
	movq	32(%r14), %rax
	leaq	16(%rax), %rdx
.L1008:
	movabsq	$927712935937, %r15
	movq	(%rdx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%ebx, %r13d
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	salq	$3, %r13
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	orq	%r15, %r13
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	leaq	-80(%rbp), %r9
	movq	%r13, %rcx
	movq	%r12, %rdi
	leaq	0(,%rbx,8), %rdx
	movl	$2, %r8d
	movl	$414, %esi
	orq	%r15, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1012
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1012:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17718:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitS1x8AllTrueEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitS1x8AllTrueEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitS1x16AllTrueEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitS1x16AllTrueEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitS1x16AllTrueEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitS1x16AllTrueEPNS1_4NodeE:
.LFB17719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movabsq	$377957122049, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	%eax, %eax
	salq	$3, %rax
	orq	%r13, %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movzbl	23(%r14), %eax
	orq	%r13, %rbx
	leaq	32(%r14), %rdx
	movq	%rbx, -72(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1014
	movq	32(%r14), %rax
	leaq	16(%rax), %rdx
.L1014:
	movabsq	$927712935937, %r15
	movq	(%rdx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%ebx, %r13d
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	salq	$3, %r13
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	orq	%r15, %r13
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	leaq	-80(%rbp), %r9
	movq	%r13, %rcx
	movq	%r12, %rdi
	leaq	0(,%rbx,8), %rdx
	movl	$2, %r8d
	movl	$416, %esi
	orq	%r15, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1018
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1018:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17719:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitS1x16AllTrueEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitS1x16AllTrueEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitS128SelectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitS128SelectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitS128SelectEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitS128SelectEPNS1_4NodeE:
.LFB17720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$24, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1020
	leaq	48(%rsi), %rax
.L1021:
	movq	(%rax), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %r14
	movabsq	$377957122049, %rax
	orq	%rax, %r14
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1022
	leaq	8(%rbx), %rax
.L1023:
	movq	(%rax), %rsi
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%r15d, %r8d
	movabsq	$377957122049, %rax
	salq	$3, %r8
	orq	%rax, %r8
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1024
	movq	32(%r13), %rax
	leaq	16(%rax), %rbx
.L1024:
	movq	(%rbx), %r15
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r15d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r15
	orq	%rcx, %r15
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	pushq	$0
	movl	%ebx, %edx
	movq	-56(%rbp), %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r14, %r9
	movq	%r15, %rcx
	movabsq	$1065151889409, %rbx
	movq	%r12, %rdi
	movl	$382, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1022:
	.cfi_restore_state
	movq	32(%r13), %rax
	addq	$24, %rax
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1020:
	movq	32(%rsi), %rax
	addq	$32, %rax
	jmp	.L1021
	.cfi_endproc
.LFE17720:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitS128SelectEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitS128SelectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitF64x2AbsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2AbsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2AbsEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitF64x2AbsEPNS1_4NodeE:
.LFB17721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$13, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	leaq	32(%r14), %rdx
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -48(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1027
	movq	32(%r14), %rax
	leaq	16(%rax), %rdx
.L1027:
	movq	(%rdx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	leaq	-48(%rbp), %r9
	movq	%r13, %rcx
	salq	$3, %rdx
	movl	$1, %r8d
	movl	$241, %esi
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1031
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1031:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17721:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2AbsEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitF64x2AbsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitF64x2NegEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2NegEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2NegEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitF64x2NegEPNS1_4NodeE:
.LFB17722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$13, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	leaq	32(%r14), %rdx
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -48(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1033
	movq	32(%r14), %rax
	leaq	16(%rax), %rdx
.L1033:
	movq	(%rdx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	leaq	-48(%rbp), %r9
	movq	%r13, %rcx
	salq	$3, %rdx
	movl	$1, %r8d
	movl	$242, %esi
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1037
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1037:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17722:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2NegEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitF64x2NegEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector23VisitF32x4UConvertI32x4EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector23VisitF32x4UConvertI32x4EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector23VisitF32x4UConvertI32x4EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector23VisitF32x4UConvertI32x4EPNS1_4NodeE:
.LFB17723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1039
	movq	16(%r12), %r12
.L1039:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$257, %esi
	movabsq	$1065151889409, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.cfi_endproc
.LFE17723:
	.size	_ZN2v88internal8compiler19InstructionSelector23VisitF32x4UConvertI32x4EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector23VisitF32x4UConvertI32x4EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI64x2ShrSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2ShrSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2ShrSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI64x2ShrSEPNS1_4NodeE:
.LFB17724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%r15), %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movabsq	$377957122049, %rdx
	movl	%eax, %eax
	salq	$3, %rax
	orq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movzbl	23(%r15), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1042
	leaq	40(%r15), %rax
.L1043:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$2989297238017, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1044
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L1044:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r14, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$278, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1047
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1042:
	.cfi_restore_state
	movq	32(%r15), %rax
	addq	$24, %rax
	jmp	.L1043
.L1047:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17724:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2ShrSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI64x2ShrSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI64x2MulEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2MulEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2MulEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI64x2MulEPNS1_4NodeE:
.LFB17725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movabsq	$377957122049, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movabsq	$47244640256, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movl	%eax, %ebx
	movq	%rbx, %rdx
	orq	%r14, %rdx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movq	16(%r12), %rdi
	leaq	0(,%rbx,8), %rax
	orq	%r15, %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movq	%r14, %rdx
	movl	$14, %esi
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movzbl	23(%r13), %eax
	orq	%r15, %rbx
	movq	%rbx, -72(%rbp)
	leaq	32(%r13), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1049
	leaq	40(%r13), %rax
.L1050:
	movq	(%rax), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r13), %eax
	salq	$3, %r15
	movabsq	$927712935937, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1051
	movq	32(%r13), %rax
	leaq	16(%rax), %rbx
.L1051:
	movq	(%rbx), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-80(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$2, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r14, %rcx
	movl	$281, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1054
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1049:
	.cfi_restore_state
	movq	32(%r13), %rax
	addq	$24, %rax
	jmp	.L1050
.L1054:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17725:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2MulEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI64x2MulEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI64x2MinSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MinSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MinSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MinSEPNS1_4NodeE:
.LFB17726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, 36(%r12)
	je	.L1056
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r13), %eax
	movq	%rbx, -80(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1057
	leaq	40(%r13), %rax
.L1058:
	movq	(%rax), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r13), %eax
	salq	$3, %r15
	movabsq	$858993459201, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1059
	movq	32(%r13), %rax
	leaq	16(%rax), %r14
.L1059:
	movq	(%r14), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-80(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movabsq	$1065151889409, %rbx
	orq	%rbx, %rdx
.L1066:
	movq	%r15, %r8
	movq	%r14, %rcx
	movl	$282, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1067
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1056:
	.cfi_restore_state
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movabsq	$47244640256, %rdx
	movl	$14, %esi
	movabsq	$377957122049, %r15
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movq	16(%r12), %rdi
	orq	%r15, %rbx
	movq	%rbx, -80(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	%eax, %eax
	salq	$3, %rax
	orq	%r15, %rax
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movl	%eax, %eax
	salq	$3, %rax
	orq	%rax, %r15
	movzbl	23(%r13), %eax
	movq	%r15, -64(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1061
	leaq	40(%r13), %rax
.L1062:
	movq	(%rax), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r13), %eax
	salq	$3, %r15
	movabsq	$377957122049, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1063
	movq	32(%r13), %rax
	leaq	16(%rax), %r14
.L1063:
	movq	(%r14), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	subq	$8, %rsp
	leaq	-80(%rbp), %rax
	salq	$3, %rdx
	pushq	%rax
	movl	$3, %r9d
	movabsq	$1065151889409, %rbx
	orq	%rbx, %rdx
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L1057:
	movq	32(%r13), %rax
	addq	$24, %rax
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1061:
	movq	32(%r13), %rax
	addq	$24, %rax
	jmp	.L1062
.L1067:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17726:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MinSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MinSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI64x2MaxSEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MaxSEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MaxSEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MaxSEPNS1_4NodeE:
.LFB17727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1069
	leaq	40(%r14), %rax
.L1070:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$858993459201, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1071
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L1071:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$283, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1074
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1069:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L1070
.L1074:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17727:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MaxSEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MaxSEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI64x2MinUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MinUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MinUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MinUEPNS1_4NodeE:
.LFB17728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movabsq	$377957122049, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movabsq	$47244640256, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movl	%eax, %ebx
	movq	%rbx, %rdx
	orq	%r14, %rdx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movq	16(%r12), %rdi
	leaq	0(,%rbx,8), %rax
	orq	%r15, %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movq	%r14, %rdx
	movl	$14, %esi
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movzbl	23(%r13), %eax
	orq	%r15, %rbx
	movq	%rbx, -72(%rbp)
	leaq	32(%r13), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1076
	leaq	40(%r13), %rax
.L1077:
	movq	(%rax), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r13), %eax
	salq	$3, %r15
	movabsq	$858993459201, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1078
	movq	32(%r13), %rax
	leaq	16(%rax), %rbx
.L1078:
	movq	(%rbx), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-80(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$2, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r14, %rcx
	movl	$289, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1081
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1076:
	.cfi_restore_state
	movq	32(%r13), %rax
	addq	$24, %rax
	jmp	.L1077
.L1081:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17728:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MinUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MinUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitI64x2MaxUEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MaxUEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MaxUEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MaxUEPNS1_4NodeE:
.LFB17729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movabsq	$377957122049, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movabsq	$47244640256, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movl	%eax, %ebx
	movq	%rbx, %rdx
	orq	%r14, %rdx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movq	16(%r12), %rdi
	leaq	0(,%rbx,8), %rax
	orq	%r15, %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movq	%r14, %rdx
	movl	$14, %esi
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movzbl	23(%r13), %eax
	orq	%r15, %rbx
	movq	%rbx, -72(%rbp)
	leaq	32(%r13), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1083
	leaq	40(%r13), %rax
.L1084:
	movq	(%rax), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r13), %eax
	salq	$3, %r15
	movabsq	$858993459201, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1085
	movq	32(%r13), %rax
	leaq	16(%rax), %rbx
.L1085:
	movq	(%rbx), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-80(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$2, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r14, %rcx
	movl	$290, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1088
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1083:
	.cfi_restore_state
	movq	32(%r13), %rax
	addq	$24, %rax
	jmp	.L1084
.L1088:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17729:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MaxUEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MaxUEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector23VisitI32x4SConvertF32x4EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector23VisitI32x4SConvertF32x4EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector23VisitI32x4SConvertF32x4EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector23VisitI32x4SConvertF32x4EPNS1_4NodeE:
.LFB17730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	leaq	32(%r14), %rdx
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -48(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1090
	movq	32(%r14), %rax
	leaq	16(%rax), %rdx
.L1090:
	movq	(%rdx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	leaq	-48(%rbp), %r9
	movq	%r13, %rcx
	salq	$3, %rdx
	movl	$1, %r8d
	movl	$296, %esi
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1094
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1094:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17730:
	.size	_ZN2v88internal8compiler19InstructionSelector23VisitI32x4SConvertF32x4EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector23VisitI32x4SConvertF32x4EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector23VisitI32x4UConvertF32x4EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector23VisitI32x4UConvertF32x4EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector23VisitI32x4UConvertF32x4EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector23VisitI32x4UConvertF32x4EPNS1_4NodeE:
.LFB17731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movabsq	$377957122049, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movabsq	$47244640256, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movl	%eax, %ebx
	movq	%rbx, %rdx
	orq	%r13, %rdx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movq	16(%r12), %rdi
	leaq	0(,%rbx,8), %rax
	orq	%r15, %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movq	%r13, %rdx
	movl	$14, %esi
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movzbl	23(%r14), %eax
	orq	%r15, %rbx
	leaq	32(%r14), %rdx
	movq	%rbx, -72(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1096
	movq	32(%r14), %rax
	leaq	16(%rax), %rdx
.L1096:
	movq	(%rdx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	leaq	-80(%rbp), %r9
	movq	%r13, %rcx
	salq	$3, %rdx
	movl	$2, %r8d
	movl	$312, %esi
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1100
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1100:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17731:
	.size	_ZN2v88internal8compiler19InstructionSelector23VisitI32x4UConvertF32x4EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector23VisitI32x4UConvertF32x4EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector23VisitI16x8UConvertI32x4EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector23VisitI16x8UConvertI32x4EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector23VisitI16x8UConvertI32x4EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector23VisitI16x8UConvertI32x4EPNS1_4NodeE:
.LFB17732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1102
	leaq	40(%rsi), %rax
.L1103:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1104
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L1104:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$344, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1102:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L1103
	.cfi_endproc
.LFE17732:
	.size	_ZN2v88internal8compiler19InstructionSelector23VisitI16x8UConvertI32x4EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector23VisitI16x8UConvertI32x4EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector23VisitI8x16UConvertI16x8EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector23VisitI8x16UConvertI16x8EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector23VisitI8x16UConvertI16x8EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector23VisitI8x16UConvertI16x8EPNS1_4NodeE:
.LFB17733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1107
	leaq	40(%rsi), %rax
.L1108:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r15), %eax
	salq	$3, %r14
	movabsq	$377957122049, %r8
	orq	%r8, %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1109
	movq	32(%r15), %rax
	leaq	16(%rax), %rbx
.L1109:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	%r14, %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	xorl	%r9d, %r9d
	movl	$369, %esi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1107:
	.cfi_restore_state
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L1108
	.cfi_endproc
.LFE17733:
	.size	_ZN2v88internal8compiler19InstructionSelector23VisitI8x16UConvertI16x8EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector23VisitI8x16UConvertI16x8EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitI8x16MulEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16MulEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16MulEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitI8x16MulEPNS1_4NodeE:
.LFB17734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	16(%r12), %rdi
	movl	$14, %esi
	movabsq	$47244640256, %rdx
	movl	%eax, %ebx
	orq	%rbx, %rdx
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movzbl	23(%r14), %eax
	movq	%rbx, -64(%rbp)
	leaq	32(%r14), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1112
	leaq	40(%r14), %rax
.L1113:
	movq	(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movzbl	23(%r14), %eax
	salq	$3, %r15
	movabsq	$927712935937, %r8
	orq	%r8, %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1114
	movq	32(%r14), %rax
	leaq	16(%rax), %rbx
.L1114:
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$927712935937, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%ebx, %edx
	pushq	%rax
	salq	$3, %rdx
	movl	$1, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movq	%r13, %rcx
	movl	$362, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1117
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1112:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L1113
.L1117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17734:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16MulEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitI8x16MulEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitInt32AbsWithOverflowEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitInt32AbsWithOverflowEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitInt32AbsWithOverflowEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitInt32AbsWithOverflowEPNS1_4NodeE:
.LFB17735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17735:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitInt32AbsWithOverflowEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitInt32AbsWithOverflowEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitInt64AbsWithOverflowEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitInt64AbsWithOverflowEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitInt64AbsWithOverflowEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitInt64AbsWithOverflowEPNS1_4NodeE:
.LFB21822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21822:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitInt64AbsWithOverflowEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitInt64AbsWithOverflowEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector29SupportedMachineOperatorFlagsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector29SupportedMachineOperatorFlagsEv
	.type	_ZN2v88internal8compiler19InstructionSelector29SupportedMachineOperatorFlagsEv, @function
_ZN2v88internal8compiler19InstructionSelector29SupportedMachineOperatorFlagsEv:
.LFB17743:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal11CpuFeatures10supported_E(%rip), %ecx
	movl	%ecx, %eax
	shrl	$10, %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%edx, %edx
	andl	$-49152, %edx
	addl	$63743, %edx
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$-49152, %eax
	addl	$63488, %eax
	andl	$2, %ecx
	cmovne	%edx, %eax
	ret
	.cfi_endproc
.LFE17743:
	.size	_ZN2v88internal8compiler19InstructionSelector29SupportedMachineOperatorFlagsEv, .-_ZN2v88internal8compiler19InstructionSelector29SupportedMachineOperatorFlagsEv
	.section	.text._ZN2v88internal8compiler19InstructionSelector21AlignmentRequirementsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21AlignmentRequirementsEv
	.type	_ZN2v88internal8compiler19InstructionSelector21AlignmentRequirementsEv, @function
_ZN2v88internal8compiler19InstructionSelector21AlignmentRequirementsEv:
.LFB17744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %eax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17744:
	.size	_ZN2v88internal8compiler19InstructionSelector21AlignmentRequirementsEv, .-_ZN2v88internal8compiler19InstructionSelector21AlignmentRequirementsEv
	.section	.text._ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC2EPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC5EPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC2EPNS1_4NodeE
	.type	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC2EPNS1_4NodeE, @function
_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC2EPNS1_4NodeE:
.LFB18886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rsi), %rax
	movq	%rax, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$24, %rsp
	movq	%rsi, (%rbx)
	movzbl	23(%rsi), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L1131
	movq	32(%rsi), %rsi
	leaq	16(%rsi), %rcx
.L1131:
	movq	(%rcx), %rdx
	movl	$0, 16(%rbx)
	movq	%rdx, 8(%rbx)
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$23, %dx
	sete	%r8b
	movb	%r8b, 20(%rbx)
	jne	.L1132
	movl	44(%rcx), %edx
	movl	%edx, 16(%rbx)
.L1132:
	movzbl	23(%rdi), %edx
	addq	$8, %rax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L1134
	movq	32(%rdi), %rax
	addq	$24, %rax
.L1134:
	movq	(%rax), %rsi
	movl	$0, 32(%rbx)
	movq	%rsi, 24(%rbx)
	movq	(%rsi), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$23, %ax
	sete	%cl
	movb	%cl, 36(%rbx)
	jne	.L1135
	movl	44(%rdx), %eax
	movl	%eax, 32(%rbx)
.L1130:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1135:
	.cfi_restore_state
	movq	(%rdi), %rax
	testb	$1, 18(%rax)
	je	.L1130
	testb	%r8b, %r8b
	je	.L1130
	movdqu	8(%rbx), %xmm0
	movq	8(%rbx), %rax
	movl	$0, 16(%rbx)
	movq	%rsi, 8(%rbx)
	movaps	%xmm0, -32(%rbp)
	movq	%rax, 24(%rbx)
	movl	-24(%rbp), %eax
	movb	%cl, 20(%rbx)
	movl	%eax, 32(%rbx)
	movzbl	-20(%rbp), %eax
	movb	%al, 36(%rbx)
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	24(%rbx), %rsi
	movq	(%rbx), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	.cfi_endproc
.LFE18886:
	.size	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC2EPNS1_4NodeE, .-_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC2EPNS1_4NodeE
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	.set	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE,_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC2EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC5EPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE
	.type	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE, @function
_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE:
.LFB18899:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rsi), %rax
	movq	%rax, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$40, %rsp
	movq	%rsi, (%rbx)
	movzbl	23(%rsi), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L1147
	movq	32(%rsi), %rcx
	addq	$16, %rcx
.L1147:
	movq	(%rcx), %rdx
	movq	$0, 16(%rbx)
	movb	$0, 24(%rbx)
	movq	%rdx, 8(%rbx)
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %edx
	cmpl	$23, %edx
	je	.L1162
	cmpl	$24, %edx
	je	.L1163
.L1149:
	movzbl	23(%rdi), %edx
	addq	$8, %rax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L1151
	movq	32(%rdi), %rax
	addq	$24, %rax
.L1151:
	movq	(%rax), %rsi
	movq	$0, 40(%rbx)
	movb	$0, 48(%rbx)
	movq	%rsi, 32(%rbx)
	movq	(%rsi), %rdx
	movzwl	16(%rdx), %eax
	cmpl	$23, %eax
	je	.L1164
	xorl	%ecx, %ecx
	cmpl	$24, %eax
	je	.L1165
.L1153:
	movq	(%rdi), %rax
	testb	$1, 18(%rax)
	je	.L1146
	cmpb	$0, 24(%rbx)
	je	.L1146
	testb	%cl, %cl
	je	.L1166
.L1146:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1165:
	.cfi_restore_state
	movq	48(%rdx), %rax
	movb	$1, 48(%rbx)
	movl	$1, %ecx
	movq	%rax, 40(%rbx)
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1163:
	movq	48(%rcx), %rdx
	movb	$1, 24(%rbx)
	movq	%rdx, 16(%rbx)
	jmp	.L1149
	.p2align 4,,10
	.p2align 3
.L1164:
	movl	44(%rdx), %eax
	movb	$1, 48(%rbx)
	movl	$1, %ecx
	movq	%rax, 40(%rbx)
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1162:
	movl	44(%rcx), %esi
	movb	$1, 24(%rbx)
	movq	%rsi, 16(%rbx)
	jmp	.L1149
	.p2align 4,,10
	.p2align 3
.L1166:
	movzbl	24(%rbx), %eax
	movzbl	48(%rbx), %edx
	movdqu	8(%rbx), %xmm0
	movdqu	32(%rbx), %xmm1
	movb	%dl, 24(%rbx)
	movb	%al, 48(%rbx)
	movups	%xmm1, 8(%rbx)
	movups	%xmm0, 32(%rbx)
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	32(%rbx), %rsi
	movq	(%rbx), %rdi
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	.cfi_endproc
.LFE18899:
	.size	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE, .-_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	.set	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE,_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC5EPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE
	.type	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE, @function
_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE:
.LFB18905:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rsi), %rax
	movq	%rax, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$40, %rsp
	movq	%rsi, (%rbx)
	movzbl	23(%rsi), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L1168
	movq	32(%rsi), %rcx
	addq	$16, %rcx
.L1168:
	movq	(%rcx), %rdx
	movq	$0, 16(%rbx)
	movb	$0, 24(%rbx)
	movq	%rdx, 8(%rbx)
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %edx
	cmpl	$23, %edx
	je	.L1183
	cmpl	$24, %edx
	je	.L1184
.L1170:
	movzbl	23(%rdi), %edx
	addq	$8, %rax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L1172
	movq	32(%rdi), %rax
	addq	$24, %rax
.L1172:
	movq	(%rax), %rsi
	movq	$0, 40(%rbx)
	movb	$0, 48(%rbx)
	movq	%rsi, 32(%rbx)
	movq	(%rsi), %rdx
	movzwl	16(%rdx), %eax
	cmpl	$23, %eax
	je	.L1185
	xorl	%ecx, %ecx
	cmpl	$24, %eax
	je	.L1186
.L1174:
	movq	(%rdi), %rax
	testb	$1, 18(%rax)
	je	.L1167
	cmpb	$0, 24(%rbx)
	je	.L1167
	testb	%cl, %cl
	je	.L1187
.L1167:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1186:
	.cfi_restore_state
	movq	48(%rdx), %rax
	movb	$1, 48(%rbx)
	movl	$1, %ecx
	movq	%rax, 40(%rbx)
	jmp	.L1174
	.p2align 4,,10
	.p2align 3
.L1184:
	movq	48(%rcx), %rdx
	movb	$1, 24(%rbx)
	movq	%rdx, 16(%rbx)
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1185:
	movslq	44(%rdx), %rax
	movb	$1, 48(%rbx)
	movl	$1, %ecx
	movq	%rax, 40(%rbx)
	jmp	.L1174
	.p2align 4,,10
	.p2align 3
.L1183:
	movslq	44(%rcx), %rdx
	movb	$1, 24(%rbx)
	movq	%rdx, 16(%rbx)
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1187:
	movzbl	24(%rbx), %eax
	movzbl	48(%rbx), %edx
	movdqu	8(%rbx), %xmm0
	movdqu	32(%rbx), %xmm1
	movb	%dl, 24(%rbx)
	movb	%al, 48(%rbx)
	movups	%xmm1, 8(%rbx)
	movups	%xmm0, 32(%rbx)
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	32(%rbx), %rsi
	movq	(%rbx), %rdi
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	.cfi_endproc
.LFE18905:
	.size	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE, .-_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	.set	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE,_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12ScaleMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_310ELS6_302EEC2EPNS1_4NodeEb,"axG",@progbits,_ZN2v88internal8compiler12ScaleMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_310ELS6_302EEC5EPNS1_4NodeEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12ScaleMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_310ELS6_302EEC2EPNS1_4NodeEb
	.type	_ZN2v88internal8compiler12ScaleMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_310ELS6_302EEC2EPNS1_4NodeEb, @function
_ZN2v88internal8compiler12ScaleMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_310ELS6_302EEC2EPNS1_4NodeEb:
.LFB18911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movl	$-1, (%rdi)
	movb	$0, 4(%rdi)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1190
	movq	32(%rsi), %rax
	movl	8(%rax), %eax
.L1190:
	cmpl	$1, %eax
	jle	.L1188
	leaq	-80(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	movq	(%rbx), %rax
	movzwl	16(%rax), %eax
	cmpl	$302, %eax
	je	.L1202
	cmpl	$310, %eax
	je	.L1203
.L1188:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1204
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1202:
	.cfi_restore_state
	cmpb	$0, -44(%rbp)
	je	.L1188
	movl	-48(%rbp), %eax
	cmpl	$3, %eax
	ja	.L1188
	movl	%eax, 0(%r13)
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1203:
	cmpb	$0, -44(%rbp)
	je	.L1188
	movl	-48(%rbp), %eax
	cmpl	$1, %eax
	je	.L1205
	cmpl	$2, %eax
	je	.L1206
	cmpl	$4, %eax
	je	.L1207
	cmpl	$8, %eax
	je	.L1208
	testb	%r12b, %r12b
	je	.L1188
	cmpl	$3, %eax
	je	.L1209
	cmpl	$5, %eax
	je	.L1210
	cmpl	$9, %eax
	jne	.L1188
	movl	$3, 0(%r13)
	movb	$1, 4(%r13)
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1205:
	movl	$0, 0(%r13)
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1206:
	movl	$1, 0(%r13)
	jmp	.L1188
.L1207:
	movl	$2, 0(%r13)
	jmp	.L1188
.L1208:
	movl	$3, 0(%r13)
	jmp	.L1188
.L1209:
	movl	$1, 0(%r13)
	movb	$1, 4(%r13)
	jmp	.L1188
.L1210:
	movl	$2, 0(%r13)
	movb	$1, 4(%r13)
	jmp	.L1188
.L1204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18911:
	.size	_ZN2v88internal8compiler12ScaleMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_310ELS6_302EEC2EPNS1_4NodeEb, .-_ZN2v88internal8compiler12ScaleMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_310ELS6_302EEC2EPNS1_4NodeEb
	.weak	_ZN2v88internal8compiler12ScaleMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_310ELS6_302EEC1EPNS1_4NodeEb
	.set	_ZN2v88internal8compiler12ScaleMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_310ELS6_302EEC1EPNS1_4NodeEb,_ZN2v88internal8compiler12ScaleMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_310ELS6_302EEC2EPNS1_4NodeEb
	.section	.text._ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC5EPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE
	.type	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE, @function
_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE:
.LFB18968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rsi), %rax
	movq	%rax, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$40, %rsp
	movq	%rsi, (%rbx)
	movzbl	23(%rsi), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L1212
	movq	32(%rsi), %rsi
	leaq	16(%rsi), %rcx
.L1212:
	movq	(%rcx), %rdx
	movq	$0x000000000, 16(%rbx)
	movq	%rdx, 8(%rbx)
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$26, %dx
	sete	%r8b
	movb	%r8b, 24(%rbx)
	jne	.L1213
	movsd	48(%rcx), %xmm0
	movsd	%xmm0, 16(%rbx)
.L1213:
	movzbl	23(%rdi), %edx
	addq	$8, %rax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L1215
	movq	32(%rdi), %rax
	addq	$24, %rax
.L1215:
	movq	(%rax), %rsi
	movq	$0x000000000, 40(%rbx)
	movq	%rsi, 32(%rbx)
	movq	(%rsi), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$26, %ax
	sete	%cl
	movb	%cl, 48(%rbx)
	jne	.L1216
	movsd	48(%rdx), %xmm0
	movsd	%xmm0, 40(%rbx)
.L1211:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1216:
	.cfi_restore_state
	movq	(%rdi), %rax
	testb	$1, 18(%rax)
	je	.L1211
	testb	%r8b, %r8b
	je	.L1211
	movzbl	24(%rbx), %eax
	movdqu	8(%rbx), %xmm0
	movb	%cl, 24(%rbx)
	movdqu	32(%rbx), %xmm1
	movb	%al, 48(%rbx)
	movups	%xmm1, 8(%rbx)
	movups	%xmm0, 32(%rbx)
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	32(%rbx), %rsi
	movq	(%rbx), %rdi
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	.cfi_endproc
.LFE18968:
	.size	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE, .-_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC1EPNS1_4NodeE
	.set	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC1EPNS1_4NodeE,_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC2EPNS1_4NodeE
	.section	.rodata._ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_.str1.1,"aMS",@progbits,1
.LC2:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_:
.LFB19469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	8(%rdi), %r14
	movq	%r13, %rax
	subq	%r14, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L1243
	movq	%rsi, %rcx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	%rdx, %r15
	subq	%r14, %rcx
	testq	%rax, %rax
	je	.L1237
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1244
	movl	$2147483632, %esi
	movl	$2147483632, %r9d
.L1229:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L1245
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1232:
	addq	%rax, %r9
	leaq	16(%rax), %r10
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1244:
	testq	%rdx, %rdx
	jne	.L1246
	movl	$16, %r10d
	xorl	%r9d, %r9d
	xorl	%eax, %eax
.L1230:
	movl	(%r15), %edi
	movzbl	4(%r15), %esi
	addq	%rax, %rcx
	movq	8(%r15), %rdx
	movl	%edi, (%rcx)
	movb	%sil, 4(%rcx)
	movq	%rdx, 8(%rcx)
	cmpq	%r14, %rbx
	je	.L1233
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1234:
	movl	(%rdx), %r8d
	movzbl	4(%rdx), %edi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	-8(%rdx), %rsi
	movl	%r8d, -16(%rcx)
	movb	%dil, -12(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L1234
	movq	%rbx, %rdx
	subq	%r14, %rdx
	leaq	16(%rax,%rdx), %r10
.L1233:
	cmpq	%r13, %rbx
	je	.L1235
	movq	%rbx, %rdx
	movq	%r10, %rcx
	.p2align 4,,10
	.p2align 3
.L1236:
	movl	(%rdx), %r8d
	movzbl	4(%rdx), %edi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	-8(%rdx), %rsi
	movl	%r8d, -16(%rcx)
	movb	%dil, -12(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r13
	jne	.L1236
	subq	%rbx, %r13
	addq	%r13, %r10
.L1235:
	movq	%rax, %xmm0
	movq	%r10, %xmm1
	movq	%r9, 24(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1237:
	.cfi_restore_state
	movl	$16, %esi
	movl	$16, %r9d
	jmp	.L1229
.L1245:
	movq	%rcx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rcx
	jmp	.L1232
.L1246:
	cmpq	$134217727, %rdx
	movl	$134217727, %r9d
	cmovbe	%rdx, %r9
	salq	$4, %r9
	movq	%r9, %rsi
	jmp	.L1229
.L1243:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE19469:
	.size	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.section	.text._ZN2v88internal8compiler19InstructionSelector18EmitPrepareResultsEPNS0_10ZoneVectorINS1_13PushParameterEEEPKNS1_14CallDescriptorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector18EmitPrepareResultsEPNS0_10ZoneVectorINS1_13PushParameterEEEPKNS1_14CallDescriptorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector18EmitPrepareResultsEPNS0_10ZoneVectorINS1_13PushParameterEEEPKNS1_14CallDescriptorEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector18EmitPrepareResultsEPNS0_10ZoneVectorINS1_13PushParameterEEEPKNS1_14CallDescriptorEPNS1_4NodeE:
.LFB17522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %r10
	movq	16(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r14, %r10
	je	.L1247
	leaq	-80(%rbp), %rax
	movq	%rdi, %r12
	movq	%r10, %r15
	xorl	%r13d, %r13d
	movq	%rax, -104(%rbp)
	jmp	.L1259
	.p2align 4,,10
	.p2align 3
.L1258:
	addq	$16, %r15
	cmpq	%r15, %r14
	je	.L1247
.L1259:
	movl	8(%r15), %eax
	testb	$1, %al
	je	.L1258
	testl	%eax, %eax
	jns	.L1258
	movzbl	12(%r15), %esi
	leal	-1(%rsi), %eax
	cmpb	$13, %al
	ja	.L1250
	movzbl	%al, %eax
	leaq	CSWTCH.731(%rip), %rdi
	movl	$1, %edx
	movq	(%r15), %r8
	movl	(%rdi,%rax,4), %ecx
	sall	%cl, %edx
	leal	14(%rdx), %eax
	addl	$7, %edx
	cmovns	%edx, %eax
	sarl	$3, %eax
	addl	%eax, %r13d
	testq	%r8, %r8
	je	.L1258
	movzbl	13(%r15), %eax
	cmpb	$12, %sil
	je	.L1274
	cmpb	$6, %al
	jne	.L1253
	cmpb	$13, %sil
	je	.L1273
.L1253:
	movq	%r8, %rsi
	movq	%r12, %rdi
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-112(%rbp), %r8
	movq	%r12, %rdi
	movl	%eax, %ebx
	movq	%r8, %rsi
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movq	16(%r12), %rdx
	movq	-104(%rbp), %rdi
	movl	%r13d, %esi
	movabsq	$927712935937, %rax
	orq	%rax, %rbx
	movq	%rdx, -112(%rbp)
	movq	%rbx, -88(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %ecx
	movq	-112(%rbp), %rdx
	testl	%ecx, %ecx
	jne	.L1254
	cmpb	$19, -76(%rbp)
	je	.L1275
.L1254:
	movq	160(%rdx), %rsi
	movq	%rsi, %rbx
	subq	152(%rdx), %rbx
	sarq	$4, %rbx
	cmpq	168(%rdx), %rsi
	je	.L1256
	movzbl	-76(%rbp), %edi
	movq	-72(%rbp), %rax
	movl	%ecx, (%rsi)
	movb	%dil, 4(%rsi)
	movq	%rax, 8(%rsi)
	addq	$16, 160(%rdx)
.L1257:
	movq	%rbx, %rax
	salq	$32, %rax
	orq	$11, %rax
.L1255:
	pushq	$0
	movq	-104(%rbp), %r9
	movl	$1, %edx
	leaq	-88(%rbp), %rcx
	pushq	$0
	movl	$1, %r8d
	movl	$237, %esi
	movq	%r12, %rdi
	addq	$16, %r15
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	popq	%rdx
	cmpq	%r15, %r14
	jne	.L1259
	.p2align 4,,10
	.p2align 3
.L1247:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1276
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1274:
	.cfi_restore_state
	cmpb	$6, %al
	jne	.L1253
.L1273:
	movq	%r8, %rdx
	movq	%r12, %rdi
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	-112(%rbp), %r8
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1275:
	movslq	-72(%rbp), %rax
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L1255
	.p2align 4,,10
	.p2align 3
.L1256:
	leaq	144(%rdx), %rdi
	movq	-104(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1257
.L1250:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1276:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17522:
	.size	_ZN2v88internal8compiler19InstructionSelector18EmitPrepareResultsEPNS0_10ZoneVectorINS1_13PushParameterEEEPKNS1_14CallDescriptorEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector18EmitPrepareResultsEPNS0_10ZoneVectorINS1_13PushParameterEEEPKNS1_14CallDescriptorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE, @function
_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE:
.LFB15292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	(%rsi), %rdi
	movq	16(%rax), %r12
	movzwl	16(%rdi), %eax
	cmpw	$60, %ax
	ja	.L1278
	cmpw	$22, %ax
	jbe	.L1279
	subl	$23, %eax
	cmpw	$37, %ax
	ja	.L1279
	leaq	.L1281(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,"aG",@progbits,_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,comdat
	.align 4
	.align 4
.L1281:
	.long	.L1289-.L1281
	.long	.L1291-.L1281
	.long	.L1287-.L1281
	.long	.L1285-.L1281
	.long	.L1286-.L1281
	.long	.L1285-.L1281
	.long	.L1279-.L1281
	.long	.L1284-.L1281
	.long	.L1283-.L1281
	.long	.L1282-.L1281
	.long	.L1282-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1279-.L1281
	.long	.L1280-.L1281
	.section	.text._ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,comdat
	.p2align 4,,10
	.p2align 3
.L1278:
	cmpw	$284, %ax
	je	.L1290
	cmpw	$428, %ax
	jne	.L1279
.L1291:
	movq	48(%rdi), %rax
	movl	$1, -48(%rbp)
	movl	$1, %edx
	movl	$19, %ecx
	movb	$19, -44(%rbp)
	movq	%rax, -40(%rbp)
	.p2align 4,,10
	.p2align 3
.L1293:
	movq	160(%r12), %rsi
	movq	%rsi, %rbx
	subq	152(%r12), %rbx
	sarq	$4, %rbx
	cmpq	168(%r12), %rsi
	je	.L1299
	movl	%edx, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rax, 8(%rsi)
	addq	$16, 160(%r12)
.L1300:
	movq	%rbx, %rax
	salq	$32, %rax
	orq	$11, %rax
.L1298:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1303
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1289:
	.cfi_restore_state
	movl	44(%rdi), %esi
	leaq	-48(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-48(%rbp), %edx
	movzbl	-44(%rbp), %ecx
	movq	-40(%rbp), %rax
.L1292:
	movl	%edx, -48(%rbp)
	movb	%cl, -44(%rbp)
	movq	%rax, -40(%rbp)
	testl	%edx, %edx
	jne	.L1293
	cmpb	$19, %cl
	jne	.L1293
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L1298
.L1287:
	movslq	44(%rdi), %rax
	movl	$2, %edx
	movl	$2, -48(%rbp)
	movl	$19, %ecx
	movb	$19, -44(%rbp)
	movq	%rax, -40(%rbp)
	jmp	.L1293
.L1284:
	call	_ZN2v88internal8compiler14HeapConstantOfEPKNS1_8OperatorE@PLT
	movb	$19, -44(%rbp)
	movl	$6, %edx
	movl	$19, %ecx
	movl	$6, -48(%rbp)
	movq	%rax, -40(%rbp)
	jmp	.L1293
.L1283:
	call	_ZN2v88internal8compiler14HeapConstantOfEPKNS1_8OperatorE@PLT
	movb	$19, -44(%rbp)
	movl	$5, %edx
	movl	$19, %ecx
	movl	$5, -48(%rbp)
	movq	%rax, -40(%rbp)
	jmp	.L1293
.L1282:
	movq	56(%rdi), %rdx
	movq	48(%rdi), %rsi
	leaq	-48(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1ENS1_26RelocatablePtrConstantInfoE@PLT
	movl	-48(%rbp), %edx
	movzbl	-44(%rbp), %ecx
	movq	-40(%rbp), %rax
	jmp	.L1292
.L1280:
	call	_ZN2v88internal8compiler25DeadValueRepresentationOfEPKNS1_8OperatorE@PLT
	cmpb	$13, %al
	ja	.L1279
	leaq	.L1295(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,"aG",@progbits,_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,comdat
	.align 4
	.align 4
.L1295:
	.long	.L1279-.L1295
	.long	.L1297-.L1295
	.long	.L1279-.L1295
	.long	.L1279-.L1295
	.long	.L1297-.L1295
	.long	.L1279-.L1295
	.long	.L1297-.L1295
	.long	.L1297-.L1295
	.long	.L1297-.L1295
	.long	.L1297-.L1295
	.long	.L1297-.L1295
	.long	.L1297-.L1295
	.long	.L1296-.L1295
	.long	.L1294-.L1295
	.section	.text._ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,comdat
.L1285:
	movq	48(%rdi), %rax
	movl	$3, %edx
	movl	$3, -48(%rbp)
	movl	$19, %ecx
	movb	$19, -44(%rbp)
	movq	%rax, -40(%rbp)
	jmp	.L1293
.L1286:
	movq	48(%rdi), %rax
	movl	$4, %edx
	movl	$4, -48(%rbp)
	movl	$19, %ecx
	movb	$19, -44(%rbp)
	movq	%rax, -40(%rbp)
	jmp	.L1293
	.p2align 4,,10
	.p2align 3
.L1299:
	leaq	-48(%rbp), %rdx
	leaq	144(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1290:
	call	_ZN2v88internal8compiler20StringConstantBaseOfEPKNS1_8OperatorE@PLT
	movb	$19, -44(%rbp)
	movl	$8, %edx
	movl	$19, %ecx
	movl	$8, -48(%rbp)
	movq	%rax, -40(%rbp)
	jmp	.L1293
.L1279:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1297:
	leaq	-48(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-48(%rbp), %edx
	movzbl	-44(%rbp), %ecx
	movq	-40(%rbp), %rax
	jmp	.L1292
.L1294:
	movb	$19, -44(%rbp)
	movl	$3, %edx
	xorl	%eax, %eax
	movl	$19, %ecx
	movl	$3, -48(%rbp)
	movq	$0, -40(%rbp)
	jmp	.L1293
.L1296:
	movb	$19, -44(%rbp)
	movl	$2, %edx
	xorl	%eax, %eax
	movl	$19, %ecx
	movl	$2, -48(%rbp)
	movq	$0, -40(%rbp)
	jmp	.L1293
.L1303:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15292:
	.size	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE, .-_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_116VisitWord32ShiftEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWord32ShiftEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_116VisitWord32ShiftEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE:
.LFB17392:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	%edx, -116(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -104(%rbp)
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	movq	-88(%rbp), %r14
	movq	-72(%rbp), %rbx
	movq	(%r14), %rax
	cmpw	$473, 16(%rax)
	je	.L1328
.L1305:
	movq	(%rbx), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L1307
	ja	.L1308
	cmpw	$23, %ax
	je	.L1309
	cmpw	$24, %ax
	jne	.L1311
	movq	48(%rdx), %rax
	movl	$4294967294, %edx
	addq	$2147483647, %rax
	cmpq	%rdx, %rax
	setbe	%al
	testb	%al, %al
	jne	.L1309
	.p2align 4,,10
	.p2align 3
.L1311:
	movq	-104(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-104(%rbp), %rdi
	movq	%rbx, %rsi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movq	-104(%rbp), %rdi
	movq	%r14, %rsi
	salq	$3, %r15
	movabsq	$2989297238017, %r8
	orq	%r8, %r15
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-104(%rbp), %rdi
	movq	%r14, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %ecx
	movq	-104(%rbp), %rdi
	movq	%r12, %rsi
	movabsq	$377957122049, %rbx
	salq	$3, %rcx
	orq	%rbx, %rcx
	movq	%rcx, -128(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-104(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	-128(%rbp), %rcx
	pushq	$0
	movl	-116(%rbp), %esi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	movabsq	$1065151889409, %rbx
	movq	%r15, %r8
	movq	%r13, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
.L1304:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1329
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1308:
	.cfi_restore_state
	cmpw	$32, %ax
	jne	.L1311
.L1309:
	movq	%rbx, %rsi
	leaq	-104(%rbp), %rdi
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	movq	-104(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-104(%rbp), %rdi
	movq	%r14, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %ecx
	movq	-104(%rbp), %rdi
	movq	%r12, %rsi
	movabsq	$377957122049, %rbx
	salq	$3, %rcx
	orq	%rbx, %rcx
	movq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-104(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movl	-116(%rbp), %esi
	pushq	$0
	salq	$3, %rdx
	movq	%r14, %rcx
	xorl	%r9d, %r9d
	movabsq	$1065151889409, %rbx
	movq	%r15, %r8
	movq	%r13, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1307:
	cmpq	$0, 48(%rdx)
	sete	%al
	testb	%al, %al
	je	.L1311
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1328:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L1305
	movzbl	23(%r14), %eax
	movq	32(%r14), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1305
	movq	16(%r14), %r14
	jmp	.L1305
.L1329:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17392:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWord32ShiftEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_116VisitWord32ShiftEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitWord32ShrEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitWord32ShrEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitWord32ShrEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitWord32ShrEPNS1_4NodeE:
.LFB17398:
	.cfi_startproc
	endbr64
	movl	$128, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWord32ShiftEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17398:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitWord32ShrEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitWord32ShrEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitWord32SarEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitWord32SarEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitWord32SarEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitWord32SarEPNS1_4NodeE:
.LFB17402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -136(%rbp)
	leaq	-128(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L1332
	movq	-120(%rbp), %rsi
	movq	(%rsi), %rax
	cmpw	$302, 16(%rax)
	je	.L1342
.L1332:
	movl	$130, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWord32ShiftEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
.L1331:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1343
	addq	$112, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1342:
	.cfi_restore_state
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -44(%rbp)
	je	.L1332
	movl	-48(%rbp), %eax
	cmpl	$16, %eax
	je	.L1344
	cmpl	$24, %eax
	jne	.L1332
	cmpb	$0, -92(%rbp)
	je	.L1332
	cmpl	$24, -96(%rbp)
	jne	.L1332
	movq	-72(%rbp), %rsi
	leaq	-136(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler16OperandGenerator3UseEPNS1_4NodeE
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler16OperandGenerator16DefineAsRegisterEPNS1_4NodeE
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rdx
	movl	$204, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1344:
	cmpb	$0, -92(%rbp)
	je	.L1332
	cmpl	$16, -96(%rbp)
	jne	.L1332
	movq	-72(%rbp), %rsi
	leaq	-136(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler16OperandGenerator3UseEPNS1_4NodeE
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler16OperandGenerator16DefineAsRegisterEPNS1_4NodeE
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rdx
	movl	$209, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	jmp	.L1331
.L1343:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17402:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitWord32SarEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitWord32SarEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitWord32RorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitWord32RorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitWord32RorEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitWord32RorEPNS1_4NodeE:
.LFB17404:
	.cfi_startproc
	endbr64
	movl	$132, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWord32ShiftEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17404:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitWord32RorEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitWord32RorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_116VisitWord64ShiftEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWord64ShiftEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_116VisitWord64ShiftEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE:
.LFB17393:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -184(%rbp)
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	movq	-144(%rbp), %rsi
	movq	-168(%rbp), %r15
	movq	(%rsi), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L1347
	ja	.L1348
	cmpw	$23, %ax
	je	.L1349
	cmpw	$24, %ax
	jne	.L1351
	movq	48(%rdx), %rdx
	movl	$4294967294, %ecx
	addq	$2147483647, %rdx
	cmpq	%rcx, %rdx
	setbe	%dl
	testb	%dl, %dl
	je	.L1352
	.p2align 4,,10
	.p2align 3
.L1349:
	leaq	-184(%rbp), %rdi
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	movq	%rax, %r8
	jmp	.L1367
	.p2align 4,,10
	.p2align 3
.L1348:
	cmpw	$32, %ax
	je	.L1349
.L1352:
	cmpw	$318, %ax
	je	.L1368
.L1351:
	movq	-184(%rbp), %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-200(%rbp), %rsi
	movq	-184(%rbp), %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r8d
	movabsq	$2989297238017, %rbx
	salq	$3, %r8
	orq	%rbx, %r8
.L1367:
	movq	-184(%rbp), %rdi
	movq	%r15, %rsi
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-184(%rbp), %rdi
	movq	%r15, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r15d
	movq	-184(%rbp), %rdi
	movq	%r12, %rsi
	movabsq	$377957122049, %rcx
	salq	$3, %r15
	orq	%rcx, %r15
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-184(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	xorl	%r9d, %r9d
	pushq	$0
	salq	$3, %rdx
	movq	-200(%rbp), %r8
	movq	%r15, %rcx
	movabsq	$1065151889409, %rbx
	movl	%r14d, %esi
	movq	%r13, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1369
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1347:
	.cfi_restore_state
	cmpq	$0, 48(%rdx)
	sete	%dl
	testb	%dl, %dl
	je	.L1352
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1368:
	leaq	-112(%rbp), %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -64(%rbp)
	movq	-200(%rbp), %rsi
	je	.L1351
	cmpq	$63, -72(%rbp)
	jne	.L1351
	movq	-104(%rbp), %rsi
	jmp	.L1351
.L1369:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17393:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWord64ShiftEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_116VisitWord64ShiftEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitWord64RorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitWord64RorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitWord64RorEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitWord64RorEPNS1_4NodeE:
.LFB17405:
	.cfi_startproc
	endbr64
	movl	$131, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWord64ShiftEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17405:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitWord64RorEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitWord64RorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_18VisitMulEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_18VisitMulEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_18VisitMulEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE:
.LFB17417:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -104(%rbp)
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	movq	-72(%rbp), %rbx
	movq	-88(%rbp), %r15
	movq	(%rbx), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L1372
	ja	.L1373
	cmpw	$23, %ax
	je	.L1374
	cmpw	$24, %ax
	jne	.L1376
	movq	48(%rdx), %rax
	movl	$4294967294, %edx
	addq	$2147483647, %rax
	cmpq	%rdx, %rax
	setbe	%al
	testb	%al, %al
	je	.L1376
	.p2align 4,,10
	.p2align 3
.L1374:
	movq	%rbx, %rsi
	leaq	-104(%rbp), %rdi
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	movq	-104(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-104(%rbp), %rdi
	movq	%r15, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r15d
	movq	-104(%rbp), %rdi
	movq	%r12, %rsi
	movabsq	$34359738369, %rcx
	salq	$3, %r15
	orq	%rcx, %r15
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-104(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	-120(%rbp), %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r15, %rcx
	movl	%r14d, %esi
	movabsq	$927712935937, %rbx
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L1371
	.p2align 4,,10
	.p2align 3
.L1373:
	cmpw	$32, %ax
	je	.L1374
.L1376:
	movq	-104(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rdi, -120(%rbp)
	call	_ZNK2v88internal8compiler19InstructionSelector9IsDefinedEPNS1_4NodeE@PLT
	movq	-120(%rbp), %rdi
	testb	%al, %al
	je	.L1392
.L1379:
	movq	-104(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-104(%rbp), %rdi
	movq	%r15, %rsi
	movl	%eax, -120(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	-120(%rbp), %r15d
	movq	-104(%rbp), %rdi
	movq	%rbx, %rsi
	movabsq	$34359738369, %r8
	salq	$3, %r15
	orq	%r8, %r15
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-104(%rbp), %rdi
	movq	%rbx, %rsi
	movabsq	$377957122049, %rbx
	movl	%eax, -120(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	-120(%rbp), %ecx
	movq	-104(%rbp), %rdi
	movq	%r12, %rsi
	salq	$3, %rcx
	orq	%rbx, %rcx
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-104(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	-120(%rbp), %rcx
	pushq	$0
	salq	$3, %rdx
	xorl	%r9d, %r9d
	movq	%r15, %r8
	movabsq	$1065151889409, %rbx
	movl	%r14d, %esi
	movq	%r13, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
.L1371:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1393
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1372:
	.cfi_restore_state
	cmpq	$0, 48(%rdx)
	sete	%al
	testb	%al, %al
	je	.L1376
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1392:
	movq	%rbx, %rsi
	call	_ZNK2v88internal8compiler19InstructionSelector6IsUsedEPNS1_4NodeE@PLT
	testb	%al, %al
	je	.L1379
	movq	%rbx, %rax
	movq	%r15, %rbx
	movq	%rax, %r15
	jmp	.L1379
.L1393:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17417:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_18VisitMulEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_18VisitMulEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitInt64MulEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitInt64MulEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitInt64MulEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitInt64MulEPNS1_4NodeE:
.LFB17423:
	.cfi_startproc
	endbr64
	movl	$113, %edx
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_18VisitMulEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17423:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitInt64MulEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitInt64MulEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_117TryVisitWordShiftINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELi32EEEbPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeEPNS1_17FlagsContinuationE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_117TryVisitWordShiftINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELi32EEEbPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeEPNS1_17FlagsContinuationE, @function
_ZN2v88internal8compiler12_GLOBAL__N_117TryVisitWordShiftINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELi32EEEbPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeEPNS1_17FlagsContinuationE:
.LFB18965:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -144(%rbp)
	leaq	-128(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	movq	-104(%rbp), %r8
	movq	(%r8), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L1396
	ja	.L1397
	cmpw	$23, %ax
	je	.L1398
	cmpw	$24, %ax
	jne	.L1411
	movq	48(%rdx), %rax
	movl	$4294967294, %ecx
	addq	$2147483647, %rax
	cmpq	%rcx, %rax
	setbe	%al
	testb	%al, %al
	jne	.L1401
	.p2align 4,,10
	.p2align 3
.L1395:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1412
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1397:
	.cfi_restore_state
	cmpw	$32, %ax
	jne	.L1411
.L1401:
	movl	48(%rdx), %edx
	xorl	%eax, %eax
	movq	%r8, -152(%rbp)
	andl	$31, %edx
	je	.L1395
.L1413:
	movq	-120(%rbp), %r9
	movq	-144(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r9, -160(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-144(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movq	-160(%rbp), %r9
	movq	-144(%rbp), %rdi
	movabsq	$1065151889409, %rax
	salq	$3, %r15
	pxor	%xmm0, %xmm0
	movq	%r9, %rsi
	orq	%rax, %r15
	movaps	%xmm0, -80(%rbp)
	movq	%r15, -136(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-160(%rbp), %r9
	movq	-144(%rbp), %rdi
	movl	%eax, %r12d
	movq	%r9, %rsi
	salq	$3, %r12
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movq	-152(%rbp), %r8
	movabsq	$377957122049, %rax
	leaq	-144(%rbp), %rdi
	orq	%rax, %r12
	movq	%r8, %rsi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	subq	$8, %rsp
	leaq	-80(%rbp), %r9
	movl	%r14d, %esi
	pushq	%rbx
	movl	$1, %edx
	leaq	-136(%rbp), %rcx
	movq	%r13, %rdi
	movl	$2, %r8d
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE@PLT
	popq	%rax
	movl	$1, %eax
	popq	%rdx
	jmp	.L1395
	.p2align 4,,10
	.p2align 3
.L1411:
	xorl	%eax, %eax
	jmp	.L1395
	.p2align 4,,10
	.p2align 3
.L1396:
	cmpq	$0, 48(%rdx)
	sete	%al
	testb	%al, %al
	jne	.L1401
	jmp	.L1395
	.p2align 4,,10
	.p2align 3
.L1398:
	movl	44(%rdx), %edx
	xorl	%eax, %eax
	movq	%r8, -152(%rbp)
	andl	$31, %edx
	je	.L1395
	jmp	.L1413
.L1412:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18965:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_117TryVisitWordShiftINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELi32EEEbPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeEPNS1_17FlagsContinuationE, .-_ZN2v88internal8compiler12_GLOBAL__N_117TryVisitWordShiftINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELi32EEEbPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeEPNS1_17FlagsContinuationE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_117TryVisitWordShiftINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELi64EEEbPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeEPNS1_17FlagsContinuationE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_117TryVisitWordShiftINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELi64EEEbPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeEPNS1_17FlagsContinuationE, @function
_ZN2v88internal8compiler12_GLOBAL__N_117TryVisitWordShiftINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELi64EEEbPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeEPNS1_17FlagsContinuationE:
.LFB18966:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -160(%rbp)
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	movq	-112(%rbp), %r8
	movq	(%r8), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L1415
	ja	.L1416
	cmpw	$23, %ax
	je	.L1417
	cmpw	$24, %ax
	jne	.L1430
	movq	48(%rdx), %rax
	movl	$4294967294, %ecx
	addq	$2147483647, %rax
	cmpq	%rcx, %rax
	setbe	%al
	testb	%al, %al
	jne	.L1420
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1431
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1416:
	.cfi_restore_state
	cmpw	$32, %ax
	jne	.L1430
.L1420:
	movl	48(%rdx), %edx
	xorl	%eax, %eax
	movq	%r8, -168(%rbp)
	andl	$63, %edx
	je	.L1414
.L1432:
	movq	-136(%rbp), %r9
	movq	-160(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r9, -176(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-160(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movq	-176(%rbp), %r9
	movq	-160(%rbp), %rdi
	movabsq	$1065151889409, %rax
	salq	$3, %r15
	pxor	%xmm0, %xmm0
	movq	%r9, %rsi
	orq	%rax, %r15
	movaps	%xmm0, -80(%rbp)
	movq	%r15, -152(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-176(%rbp), %r9
	movq	-160(%rbp), %rdi
	movl	%eax, %r12d
	movq	%r9, %rsi
	salq	$3, %r12
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movq	-168(%rbp), %r8
	movabsq	$377957122049, %rax
	leaq	-160(%rbp), %rdi
	orq	%rax, %r12
	movq	%r8, %rsi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	subq	$8, %rsp
	leaq	-80(%rbp), %r9
	movl	%r14d, %esi
	pushq	%rbx
	movl	$1, %edx
	leaq	-152(%rbp), %rcx
	movq	%r13, %rdi
	movl	$2, %r8d
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE@PLT
	popq	%rax
	movl	$1, %eax
	popq	%rdx
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1430:
	xorl	%eax, %eax
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1415:
	cmpq	$0, 48(%rdx)
	sete	%al
	testb	%al, %al
	jne	.L1420
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1417:
	movl	44(%rdx), %edx
	xorl	%eax, %eax
	movq	%r8, -168(%rbp)
	andl	$63, %edx
	je	.L1414
	jmp	.L1432
.L1431:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18966:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_117TryVisitWordShiftINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELi64EEEbPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeEPNS1_17FlagsContinuationE, .-_ZN2v88internal8compiler12_GLOBAL__N_117TryVisitWordShiftINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELi64EEEbPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeEPNS1_17FlagsContinuationE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_119VisitAtomicExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_119VisitAtomicExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_119VisitAtomicExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE:
.LFB17537:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	%edx, -100(%rbp)
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movq	%rdi, -96(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1434
	movq	40(%rsi), %r14
	leaq	48(%rsi), %rax
.L1435:
	movq	(%rax), %rsi
	movq	%r12, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-112(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %eax
	movq	-96(%rbp), %rdi
	movq	%r13, %rsi
	movabsq	$927712935937, %rdx
	salq	$3, %rax
	orq	%rdx, %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-96(%rbp), %rdi
	movq	%r13, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	movabsq	$927712935937, %rdx
	orq	%rdx, %rbx
	movq	%rbx, -72(%rbp)
	movq	(%r14), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L1436
	ja	.L1437
	cmpw	$23, %ax
	je	.L1438
	cmpw	$24, %ax
	jne	.L1440
	movq	48(%rdx), %rax
	movl	$4294967294, %edx
	addq	$2147483647, %rax
	cmpq	%rdx, %rax
	setbe	%al
	testb	%al, %al
	jne	.L1438
	.p2align 4,,10
	.p2align 3
.L1440:
	movq	-96(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-96(%rbp), %rdi
	movq	%r14, %rsi
	movl	$1536, %r14d
	movl	%eax, %ebx
	movabsq	$927712935937, %rax
	salq	$3, %rbx
	orq	%rax, %rbx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
.L1442:
	movq	-96(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-96(%rbp), %rdi
	movq	%r15, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	pushq	$0
	movl	-100(%rbp), %esi
	salq	$3, %rbx
	pushq	$0
	movl	$1, %edx
	leaq	-88(%rbp), %rcx
	leaq	-80(%rbp), %r9
	movabsq	$1065151889409, %rax
	orl	%r14d, %esi
	movl	$3, %r8d
	movq	%r12, %rdi
	orq	%rax, %rbx
	movq	%rbx, -88(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1454
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1437:
	.cfi_restore_state
	cmpw	$32, %ax
	jne	.L1440
.L1438:
	movq	%r14, %rsi
	leaq	-96(%rbp), %rdi
	movl	$1024, %r14d
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	movq	%rax, %rbx
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1434:
	leaq	16(%r13), %rax
	movq	8(%rax), %r14
	movq	16(%r13), %r13
	addq	$16, %rax
	jmp	.L1435
	.p2align 4,,10
	.p2align 3
.L1436:
	cmpq	$0, 48(%rdx)
	sete	%al
	testb	%al, %al
	je	.L1440
	jmp	.L1438
.L1454:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17537:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_119VisitAtomicExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_119VisitAtomicExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector22VisitWord32AtomicStoreEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector22VisitWord32AtomicStoreEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector22VisitWord32AtomicStoreEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector22VisitWord32AtomicStoreEPNS1_4NodeE:
.LFB17564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler27AtomicStoreRepresentationOfEPKNS1_8OperatorE@PLT
	leal	-2(%rax), %edx
	cmpb	$2, %dl
	ja	.L1456
	movzbl	%al, %eax
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	leal	35(%rax,%rax), %edx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_119VisitAtomicExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
.L1456:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17564:
	.size	_ZN2v88internal8compiler19InstructionSelector22VisitWord32AtomicStoreEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector22VisitWord32AtomicStoreEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector22VisitWord64AtomicStoreEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector22VisitWord64AtomicStoreEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector22VisitWord64AtomicStoreEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector22VisitWord64AtomicStoreEPNS1_4NodeE:
.LFB17565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler27AtomicStoreRepresentationOfEPKNS1_8OperatorE@PLT
	leal	-2(%rax), %edx
	cmpb	$3, %dl
	ja	.L1459
	movzbl	%al, %eax
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	leal	443(%rax), %edx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_119VisitAtomicExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
.L1459:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17565:
	.size	_ZN2v88internal8compiler19InstructionSelector22VisitWord64AtomicStoreEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector22VisitWord64AtomicStoreEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitWord32AtomicExchangeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitWord32AtomicExchangeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitWord32AtomicExchangeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitWord32AtomicExchangeEPNS1_4NodeE:
.LFB17566:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movl	%eax, %edx
	movzbl	%ah, %eax
	cmpb	$2, %dl
	je	.L1472
	cmpb	$3, %dl
	je	.L1473
	cmpb	$4, %dl
	jne	.L1464
	subl	$2, %eax
	movl	$43, %edx
	cmpb	$1, %al
	ja	.L1464
.L1463:
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_119VisitAtomicExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1473:
	.cfi_restore_state
	cmpb	$2, %al
	je	.L1468
	cmpb	$3, %al
	je	.L1474
.L1464:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1472:
	cmpb	$2, %al
	je	.L1466
	cmpb	$3, %al
	jne	.L1464
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$40, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_119VisitAtomicExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1468:
	.cfi_restore_state
	movl	$41, %edx
	jmp	.L1463
	.p2align 4,,10
	.p2align 3
.L1474:
	movl	$42, %edx
	jmp	.L1463
	.p2align 4,,10
	.p2align 3
.L1466:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$39, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_119VisitAtomicExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17566:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitWord32AtomicExchangeEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitWord32AtomicExchangeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitWord64AtomicExchangeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitWord64AtomicExchangeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitWord64AtomicExchangeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitWord64AtomicExchangeEPNS1_4NodeE:
.LFB17567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movl	%eax, %edx
	movzbl	%ah, %eax
	cmpb	$2, %dl
	je	.L1490
	cmpb	$3, %dl
	je	.L1491
	cmpb	$4, %dl
	jne	.L1480
	cmpb	$3, %al
	je	.L1492
.L1478:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1491:
	cmpb	$3, %al
	jne	.L1478
	movl	$446, %r8d
	jmp	.L1477
	.p2align 4,,10
	.p2align 3
.L1490:
	cmpb	$3, %al
	jne	.L1478
	movl	$445, %r8d
.L1477:
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movl	%r8d, %edx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_119VisitAtomicExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1492:
	.cfi_restore_state
	movl	$447, %r8d
	jmp	.L1477
	.p2align 4,,10
	.p2align 3
.L1480:
	cmpb	$5, %al
	jne	.L1478
	movl	$448, %r8d
	cmpb	$5, %dl
	je	.L1477
	jmp	.L1478
	.cfi_endproc
.LFE17567:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitWord64AtomicExchangeEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitWord64AtomicExchangeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE:
.LFB17535:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	%edx, -116(%rbp)
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movq	%rdi, -104(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1494
	movq	40(%rsi), %r14
	leaq	48(%rsi), %rax
.L1495:
	movq	(%rax), %rsi
	movq	%r12, %rdi
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-128(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %eax
	movq	-104(%rbp), %rdi
	movq	%r13, %rsi
	movabsq	$927712935937, %rdx
	salq	$3, %rax
	orq	%rdx, %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-104(%rbp), %rdi
	movq	%r13, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	movabsq	$927712935937, %rdx
	orq	%rdx, %rbx
	movq	%rbx, -72(%rbp)
	movq	(%r14), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L1496
	ja	.L1497
	cmpw	$23, %ax
	je	.L1498
	cmpw	$24, %ax
	jne	.L1500
	movq	48(%rdx), %rax
	movl	$4294967294, %edx
	addq	$2147483647, %rax
	cmpq	%rdx, %rax
	setbe	%al
	testb	%al, %al
	jne	.L1498
	.p2align 4,,10
	.p2align 3
.L1500:
	movq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-104(%rbp), %rdi
	movq	%r14, %rsi
	movl	$1536, %r14d
	movl	%eax, %ebx
	movabsq	$927712935937, %rax
	salq	$3, %rbx
	orq	%rax, %rbx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
.L1502:
	movq	-104(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-104(%rbp), %rdi
	movq	%r15, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	movabsq	$790273982465, %rax
	orq	%rax, %rbx
	movq	-104(%rbp), %rax
	movq	%rbx, -96(%rbp)
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movl	-116(%rbp), %esi
	leaq	-96(%rbp), %rcx
	leaq	-80(%rbp), %r9
	movl	%eax, %eax
	movl	$3, %r8d
	movq	%r12, %rdi
	movabsq	$377957122049, %rdx
	salq	$3, %rax
	orl	%r14d, %esi
	orq	%rdx, %rax
	movl	$1, %edx
	movq	%rax, -88(%rbp)
	leaq	-88(%rbp), %rax
	pushq	%rax
	pushq	$1
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1514
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1497:
	.cfi_restore_state
	cmpw	$32, %ax
	jne	.L1500
.L1498:
	movq	%r14, %rsi
	leaq	-104(%rbp), %rdi
	movl	$1024, %r14d
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	movq	%rax, %rbx
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1494:
	leaq	16(%r13), %rax
	movq	8(%rax), %r14
	movq	16(%r13), %r13
	addq	$16, %rax
	jmp	.L1495
	.p2align 4,,10
	.p2align 3
.L1496:
	cmpq	$0, 48(%rdx)
	sete	%al
	testb	%al, %al
	je	.L1500
	jmp	.L1498
.L1514:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17535:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector32VisitWord32AtomicBinaryOperationEPNS1_4NodeENS1_10ArchOpcodeES5_S5_S5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector32VisitWord32AtomicBinaryOperationEPNS1_4NodeENS1_10ArchOpcodeES5_S5_S5_S5_
	.type	_ZN2v88internal8compiler19InstructionSelector32VisitWord32AtomicBinaryOperationEPNS1_4NodeENS1_10ArchOpcodeES5_S5_S5_S5_, @function
_ZN2v88internal8compiler19InstructionSelector32VisitWord32AtomicBinaryOperationEPNS1_4NodeENS1_10ArchOpcodeES5_S5_S5_S5_:
.LFB17570:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	(%rsi), %rdi
	movl	16(%rbp), %r14d
	movl	%r8d, -52(%rbp)
	movl	%r9d, -56(%rbp)
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movl	%eax, %edx
	movzbl	%ah, %eax
	cmpb	$2, %dl
	je	.L1525
	cmpb	$3, %dl
	je	.L1526
	cmpb	$4, %dl
	jne	.L1518
	subl	$2, %eax
	cmpb	$1, %al
	ja	.L1518
.L1517:
	addq	$24, %rsp
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1526:
	.cfi_restore_state
	cmpb	$2, %al
	je	.L1522
	cmpb	$3, %al
	je	.L1527
.L1518:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1525:
	cmpb	$2, %al
	je	.L1520
	cmpb	$3, %al
	jne	.L1518
	movl	%r13d, %r14d
	jmp	.L1517
	.p2align 4,,10
	.p2align 3
.L1522:
	movl	-52(%rbp), %r14d
	jmp	.L1517
	.p2align 4,,10
	.p2align 3
.L1527:
	movl	-56(%rbp), %r14d
	jmp	.L1517
	.p2align 4,,10
	.p2align 3
.L1520:
	movl	%ebx, %r14d
	jmp	.L1517
	.cfi_endproc
.LFE17570:
	.size	_ZN2v88internal8compiler19InstructionSelector32VisitWord32AtomicBinaryOperationEPNS1_4NodeENS1_10ArchOpcodeES5_S5_S5_S5_, .-_ZN2v88internal8compiler19InstructionSelector32VisitWord32AtomicBinaryOperationEPNS1_4NodeENS1_10ArchOpcodeES5_S5_S5_S5_
	.section	.text._ZN2v88internal8compiler19InstructionSelector32VisitWord64AtomicBinaryOperationEPNS1_4NodeENS1_10ArchOpcodeES5_S5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector32VisitWord64AtomicBinaryOperationEPNS1_4NodeENS1_10ArchOpcodeES5_S5_S5_
	.type	_ZN2v88internal8compiler19InstructionSelector32VisitWord64AtomicBinaryOperationEPNS1_4NodeENS1_10ArchOpcodeES5_S5_S5_, @function
_ZN2v88internal8compiler19InstructionSelector32VisitWord64AtomicBinaryOperationEPNS1_4NodeENS1_10ArchOpcodeES5_S5_S5_:
.LFB17576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$24, %rsp
	movq	(%rsi), %rdi
	movl	%r9d, -52(%rbp)
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movl	%eax, %edx
	movzbl	%ah, %eax
	cmpb	$2, %dl
	je	.L1546
	cmpb	$3, %dl
	je	.L1547
	cmpb	$4, %dl
	jne	.L1533
	cmpb	$3, %al
	je	.L1548
.L1531:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1547:
	cmpb	$3, %al
	jne	.L1531
	movl	%ebx, %r15d
.L1530:
	addq	$24, %rsp
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1546:
	.cfi_restore_state
	cmpb	$3, %al
	je	.L1530
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1548:
	movl	%r13d, %r15d
	jmp	.L1530
	.p2align 4,,10
	.p2align 3
.L1533:
	cmpb	$5, %al
	jne	.L1531
	movl	-52(%rbp), %r15d
	cmpb	$5, %dl
	je	.L1530
	jmp	.L1531
	.cfi_endproc
.LFE17576:
	.size	_ZN2v88internal8compiler19InstructionSelector32VisitWord64AtomicBinaryOperationEPNS1_4NodeENS1_10ArchOpcodeES5_S5_S5_, .-_ZN2v88internal8compiler19InstructionSelector32VisitWord64AtomicBinaryOperationEPNS1_4NodeENS1_10ArchOpcodeES5_S5_S5_
	.section	.text._ZN2v88internal8compiler19InstructionSelector19VisitWord64AtomicOrEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector19VisitWord64AtomicOrEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector19VisitWord64AtomicOrEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector19VisitWord64AtomicOrEPNS1_4NodeE:
.LFB17580:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movl	%eax, %edx
	movzbl	%ah, %eax
	cmpb	$2, %dl
	je	.L1564
	cmpb	$3, %dl
	je	.L1565
	cmpb	$4, %dl
	jne	.L1554
	cmpb	$3, %al
	je	.L1566
.L1552:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1565:
	cmpb	$3, %al
	jne	.L1552
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movl	$438, %edx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1564:
	.cfi_restore_state
	cmpb	$3, %al
	jne	.L1552
	movl	$437, %edx
.L1551:
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1566:
	.cfi_restore_state
	movl	$439, %edx
	jmp	.L1551
	.p2align 4,,10
	.p2align 3
.L1554:
	cmpb	$5, %dl
	jne	.L1552
	movl	$440, %edx
	cmpb	$5, %al
	je	.L1551
	jmp	.L1552
	.cfi_endproc
.LFE17580:
	.size	_ZN2v88internal8compiler19InstructionSelector19VisitWord64AtomicOrEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector19VisitWord64AtomicOrEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicXorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicXorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicXorEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicXorEPNS1_4NodeE:
.LFB17581:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movl	%eax, %edx
	movzbl	%ah, %eax
	cmpb	$2, %dl
	je	.L1582
	cmpb	$3, %dl
	je	.L1583
	cmpb	$4, %dl
	jne	.L1572
	cmpb	$3, %al
	je	.L1584
.L1570:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1583:
	cmpb	$3, %al
	jne	.L1570
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movl	$442, %edx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1582:
	.cfi_restore_state
	cmpb	$3, %al
	jne	.L1570
	movl	$441, %edx
.L1569:
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1584:
	.cfi_restore_state
	movl	$443, %edx
	jmp	.L1569
	.p2align 4,,10
	.p2align 3
.L1572:
	cmpb	$5, %dl
	jne	.L1570
	movl	$444, %edx
	cmpb	$5, %al
	je	.L1569
	jmp	.L1570
	.cfi_endproc
.LFE17581:
	.size	_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicXorEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicXorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicAddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicAddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicAddEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicAddEPNS1_4NodeE:
.LFB17577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movl	%eax, %edx
	movzbl	%ah, %eax
	cmpb	$2, %dl
	je	.L1600
	cmpb	$3, %dl
	je	.L1601
	cmpb	$4, %dl
	jne	.L1590
	cmpb	$3, %al
	je	.L1602
.L1588:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1601:
	cmpb	$3, %al
	jne	.L1588
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movl	$426, %edx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1600:
	.cfi_restore_state
	cmpb	$3, %al
	jne	.L1588
	movl	$425, %edx
.L1587:
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1602:
	.cfi_restore_state
	movl	$427, %edx
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1590:
	cmpb	$5, %dl
	jne	.L1588
	movl	$428, %edx
	cmpb	$5, %al
	je	.L1587
	jmp	.L1588
	.cfi_endproc
.LFE17577:
	.size	_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicAddEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicAddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicSubEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicSubEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicSubEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicSubEPNS1_4NodeE:
.LFB17578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movl	%eax, %edx
	movzbl	%ah, %eax
	cmpb	$2, %dl
	je	.L1618
	cmpb	$3, %dl
	je	.L1619
	cmpb	$4, %dl
	jne	.L1608
	cmpb	$3, %al
	je	.L1620
.L1606:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1619:
	cmpb	$3, %al
	jne	.L1606
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movl	$430, %edx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1618:
	.cfi_restore_state
	cmpb	$3, %al
	jne	.L1606
	movl	$429, %edx
.L1605:
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1620:
	.cfi_restore_state
	movl	$431, %edx
	jmp	.L1605
	.p2align 4,,10
	.p2align 3
.L1608:
	cmpb	$5, %dl
	jne	.L1606
	movl	$432, %edx
	cmpb	$5, %al
	je	.L1605
	jmp	.L1606
	.cfi_endproc
.LFE17578:
	.size	_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicSubEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicSubEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicAndEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicAndEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicAndEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicAndEPNS1_4NodeE:
.LFB17579:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movl	%eax, %edx
	movzbl	%ah, %eax
	cmpb	$2, %dl
	je	.L1636
	cmpb	$3, %dl
	je	.L1637
	cmpb	$4, %dl
	jne	.L1626
	cmpb	$3, %al
	je	.L1638
.L1624:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1637:
	cmpb	$3, %al
	jne	.L1624
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movl	$434, %edx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1636:
	.cfi_restore_state
	cmpb	$3, %al
	jne	.L1624
	movl	$433, %edx
.L1623:
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1638:
	.cfi_restore_state
	movl	$435, %edx
	jmp	.L1623
	.p2align 4,,10
	.p2align 3
.L1626:
	cmpb	$5, %dl
	jne	.L1624
	movl	$436, %edx
	cmpb	$5, %al
	je	.L1623
	jmp	.L1624
	.cfi_endproc
.LFE17579:
	.size	_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicAndEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicAndEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicXorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicXorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicXorEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicXorEPNS1_4NodeE:
.LFB17575:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movl	%eax, %edx
	movzbl	%ah, %eax
	cmpb	$2, %dl
	je	.L1650
	cmpb	$3, %dl
	je	.L1651
	cmpb	$4, %dl
	jne	.L1642
	subl	$2, %eax
	movl	$73, %edx
	cmpb	$1, %al
	ja	.L1642
.L1641:
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1651:
	.cfi_restore_state
	cmpb	$2, %al
	je	.L1646
	cmpb	$3, %al
	je	.L1652
.L1642:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1650:
	cmpb	$2, %al
	je	.L1644
	cmpb	$3, %al
	jne	.L1642
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$70, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1646:
	.cfi_restore_state
	movl	$71, %edx
	jmp	.L1641
	.p2align 4,,10
	.p2align 3
.L1652:
	movl	$72, %edx
	jmp	.L1641
	.p2align 4,,10
	.p2align 3
.L1644:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$69, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17575:
	.size	_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicXorEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicXorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicAddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicAddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicAddEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicAddEPNS1_4NodeE:
.LFB17571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movl	%eax, %edx
	movzbl	%ah, %eax
	cmpb	$2, %dl
	je	.L1664
	cmpb	$3, %dl
	je	.L1665
	cmpb	$4, %dl
	jne	.L1656
	subl	$2, %eax
	movl	$53, %edx
	cmpb	$1, %al
	ja	.L1656
.L1655:
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1665:
	.cfi_restore_state
	cmpb	$2, %al
	je	.L1660
	cmpb	$3, %al
	je	.L1666
.L1656:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1664:
	cmpb	$2, %al
	je	.L1658
	cmpb	$3, %al
	jne	.L1656
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$50, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1660:
	.cfi_restore_state
	movl	$51, %edx
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1666:
	movl	$52, %edx
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1658:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$49, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17571:
	.size	_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicAddEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicAddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicSubEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicSubEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicSubEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicSubEPNS1_4NodeE:
.LFB17572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movl	%eax, %edx
	movzbl	%ah, %eax
	cmpb	$2, %dl
	je	.L1678
	cmpb	$3, %dl
	je	.L1679
	cmpb	$4, %dl
	jne	.L1670
	subl	$2, %eax
	movl	$58, %edx
	cmpb	$1, %al
	ja	.L1670
.L1669:
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1679:
	.cfi_restore_state
	cmpb	$2, %al
	je	.L1674
	cmpb	$3, %al
	je	.L1680
.L1670:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1678:
	cmpb	$2, %al
	je	.L1672
	cmpb	$3, %al
	jne	.L1670
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$55, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1674:
	.cfi_restore_state
	movl	$56, %edx
	jmp	.L1669
	.p2align 4,,10
	.p2align 3
.L1680:
	movl	$57, %edx
	jmp	.L1669
	.p2align 4,,10
	.p2align 3
.L1672:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$54, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17572:
	.size	_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicSubEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicSubEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicAndEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicAndEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicAndEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicAndEPNS1_4NodeE:
.LFB17573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movl	%eax, %edx
	movzbl	%ah, %eax
	cmpb	$2, %dl
	je	.L1692
	cmpb	$3, %dl
	je	.L1693
	cmpb	$4, %dl
	jne	.L1684
	subl	$2, %eax
	movl	$63, %edx
	cmpb	$1, %al
	ja	.L1684
.L1683:
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1693:
	.cfi_restore_state
	cmpb	$2, %al
	je	.L1688
	cmpb	$3, %al
	je	.L1694
.L1684:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1692:
	cmpb	$2, %al
	je	.L1686
	cmpb	$3, %al
	jne	.L1684
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$60, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1688:
	.cfi_restore_state
	movl	$61, %edx
	jmp	.L1683
	.p2align 4,,10
	.p2align 3
.L1694:
	movl	$62, %edx
	jmp	.L1683
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$59, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17573:
	.size	_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicAndEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicAndEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector19VisitWord32AtomicOrEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector19VisitWord32AtomicOrEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector19VisitWord32AtomicOrEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector19VisitWord32AtomicOrEPNS1_4NodeE:
.LFB17574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movl	%eax, %edx
	movzbl	%ah, %eax
	cmpb	$2, %dl
	je	.L1706
	cmpb	$3, %dl
	je	.L1707
	cmpb	$4, %dl
	jne	.L1698
	subl	$2, %eax
	movl	$68, %edx
	cmpb	$1, %al
	ja	.L1698
.L1697:
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1707:
	.cfi_restore_state
	cmpb	$2, %al
	je	.L1702
	cmpb	$3, %al
	je	.L1708
.L1698:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1706:
	cmpb	$2, %al
	je	.L1700
	cmpb	$3, %al
	jne	.L1698
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$65, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1702:
	.cfi_restore_state
	movl	$66, %edx
	jmp	.L1697
	.p2align 4,,10
	.p2align 3
.L1708:
	movl	$67, %edx
	jmp	.L1697
	.p2align 4,,10
	.p2align 3
.L1700:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$64, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitAtomicBinopEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17574:
	.size	_ZN2v88internal8compiler19InstructionSelector19VisitWord32AtomicOrEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector19VisitWord32AtomicOrEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_126VisitAtomicCompareExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_126VisitAtomicCompareExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_126VisitAtomicCompareExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE:
.LFB17536:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movl	%edx, -116(%rbp)
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movq	%rdi, -112(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1710
	movq	40(%rsi), %r14
	movq	48(%rsi), %rsi
	leaq	56(%r15), %rax
.L1711:
	movq	(%rax), %r9
	movq	%r12, %rdi
	movq	%rsi, -128(%rbp)
	movq	%r9, -136(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-128(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movq	-136(%rbp), %r9
	movq	-112(%rbp), %rdi
	movabsq	$790273982465, %rax
	salq	$3, %rbx
	orq	%rax, %rbx
	movq	%r9, %rsi
	movq	%r9, -128(%rbp)
	movq	%rbx, -96(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-128(%rbp), %r9
	movq	-112(%rbp), %rdi
	movl	%eax, %ebx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %eax
	movq	-112(%rbp), %rdi
	movq	%r13, %rsi
	movabsq	$927712935937, %rdx
	salq	$3, %rax
	orq	%rdx, %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-112(%rbp), %rdi
	movq	%r13, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	movabsq	$927712935937, %rdx
	orq	%rdx, %rbx
	movq	%rbx, -80(%rbp)
	movq	(%r14), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L1712
	ja	.L1713
	cmpw	$23, %ax
	je	.L1714
	cmpw	$24, %ax
	jne	.L1716
	movq	48(%rdx), %rax
	movl	$4294967294, %edx
	addq	$2147483647, %rax
	cmpq	%rdx, %rax
	setbe	%al
	testb	%al, %al
	jne	.L1714
	.p2align 4,,10
	.p2align 3
.L1716:
	movq	-112(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-112(%rbp), %rdi
	movq	%r14, %rsi
	movl	$1536, %r14d
	movl	%eax, %ebx
	movabsq	$927712935937, %rax
	salq	$3, %rbx
	orq	%rax, %rbx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
.L1718:
	movq	-112(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, -72(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-112(%rbp), %rdi
	movq	%r15, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	pushq	$0
	movl	-116(%rbp), %esi
	salq	$3, %rbx
	pushq	$0
	movl	$1, %edx
	leaq	-104(%rbp), %rcx
	leaq	-96(%rbp), %r9
	movabsq	$790273982465, %rax
	orl	%r14d, %esi
	movl	$4, %r8d
	movq	%r12, %rdi
	orq	%rax, %rbx
	movq	%rbx, -104(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1730
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1713:
	.cfi_restore_state
	cmpw	$32, %ax
	jne	.L1716
.L1714:
	movq	%r14, %rsi
	leaq	-112(%rbp), %rdi
	movl	$1024, %r14d
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	movq	%rax, %rbx
	jmp	.L1718
	.p2align 4,,10
	.p2align 3
.L1710:
	leaq	16(%r13), %rax
	movq	8(%rax), %r14
	movq	16(%rax), %rsi
	movq	16(%r13), %r13
	addq	$24, %rax
	jmp	.L1711
	.p2align 4,,10
	.p2align 3
.L1712:
	cmpq	$0, 48(%rdx)
	sete	%al
	testb	%al, %al
	je	.L1716
	jmp	.L1714
.L1730:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17536:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_126VisitAtomicCompareExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_126VisitAtomicCompareExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector32VisitWord32AtomicCompareExchangeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector32VisitWord32AtomicCompareExchangeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector32VisitWord32AtomicCompareExchangeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector32VisitWord32AtomicCompareExchangeEPNS1_4NodeE:
.LFB17568:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movl	%eax, %edx
	movzbl	%ah, %eax
	cmpb	$2, %dl
	je	.L1742
	cmpb	$3, %dl
	je	.L1743
	cmpb	$4, %dl
	jne	.L1734
	subl	$2, %eax
	movl	$48, %edx
	cmpb	$1, %al
	ja	.L1734
.L1733:
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_126VisitAtomicCompareExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1743:
	.cfi_restore_state
	cmpb	$2, %al
	je	.L1738
	cmpb	$3, %al
	je	.L1744
.L1734:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1742:
	cmpb	$2, %al
	je	.L1736
	cmpb	$3, %al
	jne	.L1734
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$45, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_126VisitAtomicCompareExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1738:
	.cfi_restore_state
	movl	$46, %edx
	jmp	.L1733
	.p2align 4,,10
	.p2align 3
.L1744:
	movl	$47, %edx
	jmp	.L1733
	.p2align 4,,10
	.p2align 3
.L1736:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$44, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_126VisitAtomicCompareExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17568:
	.size	_ZN2v88internal8compiler19InstructionSelector32VisitWord32AtomicCompareExchangeEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector32VisitWord32AtomicCompareExchangeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector32VisitWord64AtomicCompareExchangeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector32VisitWord64AtomicCompareExchangeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector32VisitWord64AtomicCompareExchangeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector32VisitWord64AtomicCompareExchangeEPNS1_4NodeE:
.LFB17569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movl	%eax, %edx
	movzbl	%ah, %eax
	cmpb	$2, %dl
	je	.L1760
	cmpb	$3, %dl
	je	.L1761
	cmpb	$4, %dl
	jne	.L1750
	cmpb	$3, %al
	je	.L1762
.L1748:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1761:
	cmpb	$3, %al
	jne	.L1748
	movl	$450, %r8d
	jmp	.L1747
	.p2align 4,,10
	.p2align 3
.L1760:
	cmpb	$3, %al
	jne	.L1748
	movl	$449, %r8d
.L1747:
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movl	%r8d, %edx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_126VisitAtomicCompareExchangeEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.p2align 4,,10
	.p2align 3
.L1762:
	.cfi_restore_state
	movl	$451, %r8d
	jmp	.L1747
	.p2align 4,,10
	.p2align 3
.L1750:
	cmpb	$5, %al
	jne	.L1748
	movl	$452, %r8d
	cmpb	$5, %dl
	je	.L1747
	jmp	.L1748
	.cfi_endproc
.LFE17569:
	.size	_ZN2v88internal8compiler19InstructionSelector32VisitWord64AtomicCompareExchangeEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector32VisitWord64AtomicCompareExchangeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitI8x16ReplaceLaneEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitI8x16ReplaceLaneEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitI8x16ReplaceLaneEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitI8x16ReplaceLaneEPNS1_4NodeE:
.LFB17600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	44(%rax), %r14d
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1764
	leaq	40(%rsi), %rax
.L1765:
	movq	(%rax), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	leaq	-80(%rbp), %rdx
	salq	$3, %r13
	movl	%r14d, %esi
	movq	%rdx, %rdi
	movq	%rdx, -88(%rbp)
	movq	16(%r12), %rbx
	movabsq	$34359738369, %rax
	orq	%rax, %r13
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	movq	-88(%rbp), %rdx
	testl	%eax, %eax
	jne	.L1766
	cmpb	$19, -76(%rbp)
	je	.L1773
.L1766:
	movq	160(%rbx), %rsi
	movq	%rsi, %r14
	subq	152(%rbx), %r14
	sarq	$4, %r14
	cmpq	168(%rbx), %rsi
	je	.L1768
	movq	-72(%rbp), %rdx
	movzbl	-76(%rbp), %ecx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rbx)
.L1769:
	movq	%r14, %r8
	salq	$32, %r8
	orq	$11, %r8
.L1767:
	movzbl	23(%r15), %eax
	movq	32(%r15), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1770
	movq	16(%r14), %r14
.L1770:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	pushq	$0
	movl	%ebx, %edx
	movq	-88(%rbp), %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %r9
	movq	%r14, %rcx
	movabsq	$1065151889409, %rbx
	movl	$353, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1774
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1773:
	.cfi_restore_state
	movslq	-72(%rbp), %r8
	salq	$32, %r8
	orq	$3, %r8
	jmp	.L1767
	.p2align 4,,10
	.p2align 3
.L1764:
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L1765
	.p2align 4,,10
	.p2align 3
.L1768:
	leaq	144(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1769
.L1774:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17600:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitI8x16ReplaceLaneEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitI8x16ReplaceLaneEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler16OperandGenerator13TempImmediateEi,"axG",@progbits,_ZN2v88internal8compiler16OperandGenerator13TempImmediateEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16OperandGenerator13TempImmediateEi
	.type	_ZN2v88internal8compiler16OperandGenerator13TempImmediateEi, @function
_ZN2v88internal8compiler16OperandGenerator13TempImmediateEi:
.LFB15304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	movq	16(%rax), %r12
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-64(%rbp), %eax
	testl	%eax, %eax
	jne	.L1776
	cmpb	$19, -60(%rbp)
	je	.L1782
.L1776:
	movq	160(%r12), %rsi
	movq	%rsi, %rbx
	subq	152(%r12), %rbx
	sarq	$4, %rbx
	cmpq	168(%r12), %rsi
	je	.L1778
	movq	-56(%rbp), %rdx
	movzbl	-60(%rbp), %ecx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%r12)
.L1779:
	movq	%rbx, %rax
	salq	$32, %rax
	orq	$11, %rax
.L1777:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1783
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1782:
	.cfi_restore_state
	movslq	-56(%rbp), %rax
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L1777
	.p2align 4,,10
	.p2align 3
.L1778:
	leaq	144(%r12), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1779
.L1783:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15304:
	.size	_ZN2v88internal8compiler16OperandGenerator13TempImmediateEi, .-_ZN2v88internal8compiler16OperandGenerator13TempImmediateEi
	.section	.text._ZN2v88internal8compiler19InstructionSelector11VisitSwitchEPNS1_4NodeERKNS1_10SwitchInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector11VisitSwitchEPNS1_4NodeERKNS1_10SwitchInfoE
	.type	_ZN2v88internal8compiler19InstructionSelector11VisitSwitchEPNS1_4NodeERKNS1_10SwitchInfoE, @function
_ZN2v88internal8compiler19InstructionSelector11VisitSwitchEPNS1_4NodeERKNS1_10SwitchInfoE:
.LFB17539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movq	%rdi, -64(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1785
	movq	16(%r14), %r14
.L1785:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-64(%rbp), %rdi
	movq	%r14, %rsi
	movabsq	$377957122049, %r14
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	orq	%r14, %rbx
	cmpl	$1, 360(%r12)
	movq	%rbx, -56(%rbp)
	jne	.L1786
	movq	0(%r13), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	movq	%rax, %rdx
	sarq	$4, %rdx
	cmpq	$79, %rax
	ja	.L1798
.L1786:
	leaq	-56(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector22EmitBinarySearchSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE@PLT
.L1784:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1799
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1798:
	.cfi_restore_state
	movq	16(%r13), %rax
	leaq	3(%rdx,%rdx,4), %rdx
	leaq	13(%rax), %rcx
	cmpq	%rdx, %rcx
	ja	.L1786
	cmpl	$-2147483648, 8(%r13)
	je	.L1786
	cmpq	$131072, %rax
	ja	.L1786
	movq	-64(%rbp), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movl	8(%r13), %esi
	movl	%eax, %edx
	salq	$3, %rdx
	orq	%r14, %rdx
	movq	%rdx, -48(%rbp)
	testl	%esi, %esi
	je	.L1787
	negl	%esi
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal8compiler16OperandGenerator13TempImmediateEi
	subq	$8, %rsp
	movq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	$0
	movq	-56(%rbp), %rcx
	movq	%rax, %r8
	movl	$1255, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
.L1788:
	leaq	-48(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15EmitTableSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE@PLT
	jmp	.L1784
.L1787:
	movq	-56(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$214, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	jmp	.L1788
.L1799:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17539:
	.size	_ZN2v88internal8compiler19InstructionSelector11VisitSwitchEPNS1_4NodeERKNS1_10SwitchInfoE, .-_ZN2v88internal8compiler19InstructionSelector11VisitSwitchEPNS1_4NodeERKNS1_10SwitchInfoE
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitS8x16ShuffleEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitS8x16ShuffleEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitS8x16ShuffleEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitS8x16ShuffleEPNS1_4NodeE:
.LFB17742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-258(%rbp), %rcx
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$296, %rsp
	movq	%rdi, -328(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19InstructionSelector19CanonicalizeShuffleEPNS1_4NodeEPhPb@PLT
	movzbl	-258(%rbp), %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	leaq	-257(%rbp), %rsi
	movq	%rbx, -256(%rbp)
	movb	%al, -316(%rbp)
	movaps	%xmm0, -224(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector14TryMatchConcatEPKhPh@PLT
	movb	%al, -317(%rbp)
	testb	%al, %al
	jne	.L1869
	movzbl	-73(%rbp), %eax
	movzbl	-258(%rbp), %r15d
	movzbl	-80(%rbp), %esi
	movzbl	-79(%rbp), %r8d
	movb	%al, -280(%rbp)
	movzbl	-72(%rbp), %eax
	cmpb	$1, %r15b
	sbbl	%edx, %edx
	movzbl	-78(%rbp), %r9d
	movzbl	-77(%rbp), %r10d
	movb	%al, -288(%rbp)
	movzbl	-71(%rbp), %eax
	andl	$16, %edx
	movzbl	-76(%rbp), %r11d
	movzbl	-75(%rbp), %ebx
	addl	$15, %edx
	movb	%al, -292(%rbp)
	movzbl	-70(%rbp), %eax
	movzbl	-74(%rbp), %r14d
	movb	%al, -296(%rbp)
	movzbl	-69(%rbp), %eax
	movb	%al, -304(%rbp)
	movzbl	-68(%rbp), %eax
	movb	%al, -312(%rbp)
	movzbl	-67(%rbp), %eax
	movb	%al, -313(%rbp)
	movzbl	-66(%rbp), %eax
	movb	%al, -314(%rbp)
	movzbl	-65(%rbp), %eax
	movb	%al, -315(%rbp)
	leaq	_ZN2v88internal8compiler12_GLOBAL__N_1L13arch_shufflesE(%rip), %rax
	leaq	408(%rax), %rdi
	.p2align 4,,10
	.p2align 3
.L1806:
	movzbl	(%rax), %ecx
	xorl	%esi, %ecx
	testb	%dl, %cl
	jne	.L1804
	movzbl	1(%rax), %ecx
	xorl	%r8d, %ecx
	testb	%dl, %cl
	jne	.L1804
	movzbl	2(%rax), %ecx
	xorl	%r9d, %ecx
	testb	%dl, %cl
	jne	.L1804
	movzbl	3(%rax), %ecx
	xorl	%r10d, %ecx
	testb	%dl, %cl
	jne	.L1804
	movzbl	4(%rax), %ecx
	xorl	%r11d, %ecx
	testb	%dl, %cl
	jne	.L1804
	movzbl	5(%rax), %ecx
	xorl	%ebx, %ecx
	testb	%dl, %cl
	jne	.L1804
	movzbl	6(%rax), %ecx
	xorl	%r14d, %ecx
	testb	%dl, %cl
	jne	.L1804
	movzbl	-280(%rbp), %ecx
	xorb	7(%rax), %cl
	testb	%dl, %cl
	jne	.L1804
	movzbl	-288(%rbp), %ecx
	xorb	8(%rax), %cl
	testb	%dl, %cl
	jne	.L1804
	movzbl	-292(%rbp), %ecx
	xorb	9(%rax), %cl
	testb	%dl, %cl
	jne	.L1804
	movzbl	-296(%rbp), %ecx
	xorb	10(%rax), %cl
	testb	%dl, %cl
	jne	.L1804
	movzbl	-304(%rbp), %ecx
	xorb	11(%rax), %cl
	testb	%dl, %cl
	jne	.L1804
	movzbl	-312(%rbp), %ecx
	xorb	12(%rax), %cl
	testb	%dl, %cl
	jne	.L1804
	movzbl	-313(%rbp), %ecx
	xorb	13(%rax), %cl
	testb	%dl, %cl
	jne	.L1804
	movzbl	-314(%rbp), %ecx
	xorb	14(%rax), %cl
	testb	%dl, %cl
	jne	.L1804
	movzbl	-315(%rbp), %ecx
	xorb	15(%rax), %cl
	testb	%dl, %cl
	jne	.L1804
	movl	16(%rax), %edx
	cmpl	$383, %edx
	jne	.L1870
.L1805:
	movl	%r15d, %eax
	movq	%r12, %rdi
	movl	$4, %ebx
	xorl	$1, %eax
	movb	%al, -316(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector10Pack4LanesEPKh@PLT
	leaq	-76(%rbp), %rdi
	movl	%eax, -208(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector10Pack4LanesEPKh@PLT
	leaq	-72(%rbp), %rdi
	movl	%eax, -204(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector10Pack4LanesEPKh@PLT
	leaq	-68(%rbp), %rdi
	movl	%eax, -200(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector10Pack4LanesEPKh@PLT
	movl	%eax, -196(%rbp)
	movq	-256(%rbp), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movabsq	$377957122049, %rdx
	movl	$383, -292(%rbp)
	movq	$1, -304(%rbp)
	movl	%eax, %eax
	salq	$3, %rax
	orq	%rdx, %rax
	movq	%rax, -224(%rbp)
	jmp	.L1817
	.p2align 4,,10
	.p2align 3
.L1804:
	addq	$24, %rax
	cmpq	%rax, %rdi
	jne	.L1806
	leaq	-92(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector19TryMatch32x4ShuffleEPKhPh@PLT
	testb	%al, %al
	je	.L1807
	movzbl	-92(%rbp), %ebx
	movzbl	-89(%rbp), %eax
	movq	%r12, %rdi
	movzbl	-258(%rbp), %r15d
	sall	$6, %eax
	andl	$3, %ebx
	orl	%eax, %ebx
	movzbl	-91(%rbp), %eax
	sall	$2, %eax
	andl	$12, %eax
	orl	%eax, %ebx
	movzbl	-90(%rbp), %eax
	sall	$4, %eax
	andl	$48, %eax
	orl	%eax, %ebx
	testb	%r15b, %r15b
	je	.L1808
	call	_ZN2v88internal8compiler19InstructionSelector16TryMatchIdentityEPKh@PLT
	testb	%al, %al
	jne	.L1871
	movl	$384, -292(%rbp)
	movzbl	%bl, %ebx
	movl	%ebx, -208(%rbp)
	movl	$1, %ebx
.L1811:
	leaq	32(%r13), %rax
	movq	32(%r13), %r12
	movq	$0, -304(%rbp)
	movq	%rax, -280(%rbp)
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1845
	movb	$1, -316(%rbp)
	jmp	.L1846
	.p2align 4,,10
	.p2align 3
.L1870:
	movl	%edx, -292(%rbp)
	movzbl	20(%rax), %r15d
	xorl	%ebx, %ebx
.L1802:
	leaq	32(%r13), %rax
	movq	32(%r13), %r12
	movq	$0, -304(%rbp)
	movq	%rax, -280(%rbp)
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1831
	movb	$0, -316(%rbp)
.L1846:
	movq	16(%r12), %r12
.L1830:
	cmpb	$0, -316(%rbp)
	je	.L1831
.L1845:
	leaq	-256(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%r8, -288(%rbp)
	call	_ZN2v88internal8compiler16OperandGenerator16DefineAsRegisterEPNS1_4NodeE
	movq	-288(%rbp), %r8
	movq	%rax, -248(%rbp)
	testb	%r15b, %r15b
	je	.L1833
.L1874:
	movq	%r8, %rdi
	movq	%r12, %rsi
	movq	%r8, -288(%rbp)
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	movq	-288(%rbp), %r8
	movq	%rax, %r14
.L1834:
	pxor	%xmm0, %xmm0
	cmpb	$0, -258(%rbp)
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	%r14, -176(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	jne	.L1853
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1836
	movq	-280(%rbp), %r12
	addq	$8, %r12
.L1837:
	cmpb	$0, -317(%rbp)
	movq	(%r12), %rsi
	movq	%r8, %rdi
	je	.L1838
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	movl	$2, %r8d
	movl	$2, -296(%rbp)
	movq	%rax, -168(%rbp)
.L1835:
	testl	%ebx, %ebx
	je	.L1872
.L1866:
	movslq	-296(%rbp), %rax
	leaq	-176(%rbp), %rdi
	xorl	%r15d, %r15d
	movl	%ebx, -280(%rbp)
	movq	%rdi, -312(%rbp)
	leaq	-208(%rbp), %r13
	leaq	-240(%rbp), %r12
	movq	%r15, %r14
	leaq	(%rdi,%rax,8), %rax
	movq	%rax, -288(%rbp)
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L1840:
	movq	160(%rbx), %rsi
	movq	%rsi, %r15
	subq	152(%rbx), %r15
	sarq	$4, %r15
	cmpq	168(%rbx), %rsi
	je	.L1842
	movq	-232(%rbp), %rax
	movzbl	-236(%rbp), %r8d
	movl	%edi, (%rsi)
	movb	%r8b, 4(%rsi)
	movq	%rax, 8(%rsi)
	addq	$16, 160(%rbx)
.L1843:
	movq	%r15, %rax
	salq	$32, %rax
	orq	$11, %rax
.L1841:
	movq	-288(%rbp), %rbx
	movq	%rax, (%rbx,%r14,8)
	addq	$1, %r14
	cmpl	%r14d, -280(%rbp)
	jle	.L1873
.L1844:
	movq	-256(%rbp), %rax
	movl	0(%r13,%r14,4), %esi
	movq	%r12, %rdi
	movq	16(%rax), %rbx
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-240(%rbp), %edi
	testl	%edi, %edi
	jne	.L1840
	cmpb	$19, -236(%rbp)
	jne	.L1840
	movslq	-232(%rbp), %rax
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L1841
.L1831:
	leaq	-256(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%r8, -288(%rbp)
	call	_ZN2v88internal8compiler16OperandGenerator17DefineSameAsFirstEPNS1_4NodeE
	movq	-288(%rbp), %r8
	movq	%rax, -248(%rbp)
	testb	%r15b, %r15b
	jne	.L1874
.L1833:
	movq	-256(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r8, -288(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-256(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, %r14d
	movabsq	$34359738369, %rax
	salq	$3, %r14
	orq	%rax, %r14
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movq	-288(%rbp), %r8
	jmp	.L1834
	.p2align 4,,10
	.p2align 3
.L1873:
	movl	-280(%rbp), %ebx
	addl	-296(%rbp), %ebx
	movl	%ebx, %r8d
.L1839:
	leaq	-224(%rbp), %rax
	movq	-312(%rbp), %r9
	movl	-292(%rbp), %esi
	movl	$1, %edx
	pushq	%rax
	movq	-328(%rbp), %rdi
	leaq	-248(%rbp), %rcx
	pushq	-304(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	popq	%rdx
.L1800:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1875
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1842:
	.cfi_restore_state
	leaq	144(%rbx), %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1843
.L1853:
	movl	$1, -296(%rbp)
	movl	$1, %r8d
	testl	%ebx, %ebx
	jne	.L1866
.L1872:
	leaq	-176(%rbp), %rax
	movq	%rax, -312(%rbp)
	jmp	.L1839
.L1869:
	movq	-328(%rbp), %rdi
	movq	%r13, %rsi
	movl	$1, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector17SwapShuffleInputsEPNS1_4NodeE@PLT
	movzbl	-257(%rbp), %eax
	movb	$0, -258(%rbp)
	movl	$389, -292(%rbp)
	movzbl	-317(%rbp), %r15d
	movl	%eax, -208(%rbp)
	jmp	.L1802
.L1838:
	call	_ZN2v88internal8compiler16OperandGenerator3UseEPNS1_4NodeE
	movl	$2, %r8d
	movl	$2, -296(%rbp)
	movq	%rax, -168(%rbp)
	jmp	.L1835
.L1808:
	call	_ZN2v88internal8compiler19InstructionSelector13TryMatchBlendEPKh@PLT
	movl	%eax, %r15d
	testb	%al, %al
	je	.L1812
	cmpb	$4, -92(%rbp)
	sbbl	%eax, %eax
	notl	%eax
	andl	$3, %eax
	cmpb	$3, -91(%rbp)
	jbe	.L1814
	orl	$12, %eax
.L1814:
	cmpb	$3, -90(%rbp)
	jbe	.L1815
	orl	$48, %eax
.L1815:
	cmpb	$3, -89(%rbp)
	ja	.L1876
.L1868:
	movzbl	%al, %eax
	movl	$1, %ebx
	movl	$386, -292(%rbp)
	movl	%eax, -208(%rbp)
	movq	$0, -304(%rbp)
.L1817:
	leaq	32(%r13), %rax
	movq	32(%r13), %r12
	movq	%rax, -280(%rbp)
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1830
	jmp	.L1846
	.p2align 4,,10
	.p2align 3
.L1836:
	movq	-280(%rbp), %rax
	movq	(%rax), %r12
	addq	$24, %r12
	jmp	.L1837
.L1807:
	leaq	-88(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector19TryMatch16x8ShuffleEPKhPh@PLT
	testb	%al, %al
	jne	.L1877
	movzbl	-80(%rbp), %eax
	cmpb	-79(%rbp), %al
	je	.L1878
.L1828:
	movzbl	-258(%rbp), %r15d
	jmp	.L1805
.L1812:
	cmpb	$4, -92(%rbp)
	movzbl	%bl, %ebx
	sbbl	%eax, %eax
	movl	%ebx, -208(%rbp)
	notl	%eax
	andl	$3, %eax
	cmpb	$3, -91(%rbp)
	jbe	.L1819
	orl	$12, %eax
.L1819:
	cmpb	$3, -90(%rbp)
	jbe	.L1820
	orl	$48, %eax
.L1820:
	cmpb	$3, -89(%rbp)
	jbe	.L1821
	orl	$-64, %eax
.L1821:
	movsbl	%al, %eax
	movl	$2, %ebx
	movl	$385, -292(%rbp)
	movl	%eax, -204(%rbp)
	jmp	.L1811
.L1876:
	orl	$-64, %eax
	jmp	.L1868
.L1877:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13TryMatchBlendEPKh@PLT
	movl	%eax, %r15d
	testb	%al, %al
	je	.L1823
	xorl	%eax, %eax
	cmpb	$7, -87(%rbp)
	seta	%al
	cmpb	$7, -88(%rbp)
	leal	(%rax,%rax), %edx
	seta	%al
	orl	%edx, %eax
	xorl	%edx, %edx
	cmpb	$7, -86(%rbp)
	seta	%dl
	sall	$2, %edx
	orl	%edx, %eax
	xorl	%edx, %edx
	cmpb	$7, -85(%rbp)
	seta	%dl
	sall	$3, %edx
	orl	%edx, %eax
	xorl	%edx, %edx
	cmpb	$7, -84(%rbp)
	seta	%dl
	sall	$4, %edx
	orl	%edx, %eax
	xorl	%edx, %edx
	cmpb	$7, -83(%rbp)
	seta	%dl
	sall	$5, %edx
	orl	%edx, %eax
	xorl	%edx, %edx
	cmpb	$7, -82(%rbp)
	seta	%dl
	sall	$6, %edx
	orl	%edx, %eax
	xorl	%edx, %edx
	cmpb	$7, -81(%rbp)
	seta	%dl
	sall	$7, %edx
	orl	%edx, %eax
	jmp	.L1868
.L1878:
	cmpb	-78(%rbp), %al
	jne	.L1828
	cmpb	-77(%rbp), %al
	jne	.L1828
	cmpb	-76(%rbp), %al
	jne	.L1828
	cmpb	-75(%rbp), %al
	jne	.L1828
	cmpb	-74(%rbp), %al
	jne	.L1828
	cmpb	-73(%rbp), %al
	jne	.L1828
	cmpb	-72(%rbp), %al
	jne	.L1828
	cmpb	-71(%rbp), %al
	jne	.L1828
	cmpb	-70(%rbp), %al
	jne	.L1828
	cmpb	-69(%rbp), %al
	jne	.L1828
	cmpb	-68(%rbp), %al
	jne	.L1828
	cmpb	-67(%rbp), %al
	jne	.L1828
	cmpb	-66(%rbp), %al
	jne	.L1828
	cmpb	-65(%rbp), %al
	jne	.L1828
	movl	%eax, -208(%rbp)
	movl	$1, %r15d
	movl	$1, %ebx
	movb	$0, -317(%rbp)
	movl	$391, -292(%rbp)
	jmp	.L1802
.L1871:
	movq	-328(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE@PLT
	jmp	.L1800
.L1823:
	movzbl	-80(%rbp), %eax
	testb	$1, %al
	jne	.L1824
	movzbl	%al, %ecx
	movzbl	-79(%rbp), %edi
	leal	1(%rcx), %esi
	cmpl	%esi, %edi
	jne	.L1824
	cmpb	-78(%rbp), %al
	jne	.L1824
	cmpb	-77(%rbp), %dil
	jne	.L1824
	cmpb	-76(%rbp), %al
	jne	.L1824
	cmpb	-75(%rbp), %dil
	jne	.L1824
	cmpb	-74(%rbp), %al
	jne	.L1824
	cmpb	-73(%rbp), %dil
	jne	.L1824
	cmpb	-72(%rbp), %al
	jne	.L1824
	cmpb	-71(%rbp), %dil
	jne	.L1824
	cmpb	-70(%rbp), %al
	jne	.L1824
	cmpb	-69(%rbp), %dil
	jne	.L1824
	cmpb	-68(%rbp), %al
	jne	.L1824
	cmpb	-67(%rbp), %dil
	jne	.L1824
	cmpb	%al, -66(%rbp)
	jne	.L1824
	cmpb	-65(%rbp), %dil
	jne	.L1824
	sarl	%ecx
	movl	$1, %ebx
	movl	$390, -292(%rbp)
	movl	%ecx, -208(%rbp)
	movq	$0, -304(%rbp)
	jmp	.L1817
.L1824:
	movzbl	-88(%rbp), %r9d
	testb	$4, %r9b
	jne	.L1828
	movzbl	-87(%rbp), %r8d
	cmpb	$7, %r9b
	seta	%dl
	movl	%r8d, %eax
	testb	$4, %al
	jne	.L1828
	cmpb	$7, %r8b
	movzbl	-86(%rbp), %edi
	seta	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	orl	%eax, %edx
	movl	%edi, %eax
	testb	$4, %al
	jne	.L1828
	cmpb	$7, %dil
	movzbl	-85(%rbp), %ecx
	seta	%al
	movzbl	%al, %eax
	sall	$2, %eax
	orl	%eax, %edx
	movl	%ecx, %eax
	testb	$4, %al
	jne	.L1828
	cmpb	$7, %cl
	seta	%al
	movzbl	%al, %eax
	sall	$3, %eax
	orl	%eax, %edx
	movzbl	-84(%rbp), %eax
	testb	$4, %al
	je	.L1828
	xorl	%esi, %esi
	cmpb	$7, %al
	seta	%sil
	sall	$4, %esi
	orl	%esi, %edx
	movzbl	-83(%rbp), %esi
	testb	$4, %sil
	je	.L1828
	cmpb	$7, %sil
	movzbl	-82(%rbp), %ebx
	seta	%r10b
	movzbl	%r10b, %r10d
	sall	$5, %r10d
	orl	%r10d, %edx
	testb	$4, %bl
	je	.L1828
	cmpb	$7, %bl
	seta	%r10b
	movzbl	%r10b, %r10d
	sall	$6, %r10d
	orl	%r10d, %edx
	movzbl	-81(%rbp), %r10d
	testb	$4, %r10b
	je	.L1828
	cmpb	$7, %r10b
	seta	%r11b
	xorl	%r14d, %r14d
	movzbl	%r11b, %r11d
	sall	$7, %r11d
	orl	%r11d, %edx
	movzbl	-258(%rbp), %r11d
	testb	%r11b, %r11b
	sete	%r14b
	andl	$3, %r9d
	sall	$6, %ecx
	andl	$3, %eax
	sall	$4, %edi
	orl	%r9d, %ecx
	sall	$4, %ebx
	addl	$387, %r14d
	sall	$2, %r8d
	andl	$48, %edi
	sall	$6, %r10d
	movl	%r14d, -292(%rbp)
	andl	$12, %r8d
	orl	%r10d, %eax
	orl	%r8d, %ecx
	orl	%edi, %ecx
	movzbl	%cl, %ecx
	movl	%ecx, -208(%rbp)
	leal	0(,%rsi,4), %ecx
	andl	$12, %ecx
	orl	%ecx, %eax
	movl	%ebx, %ecx
	movl	$2, %ebx
	andl	$48, %ecx
	orl	%ecx, %eax
	movzbl	%al, %eax
	movl	%eax, -204(%rbp)
	testb	%r11b, %r11b
	jne	.L1811
	movzbl	%dl, %edx
	xorl	%r15d, %r15d
	movl	$3, %ebx
	movl	%edx, -200(%rbp)
	jmp	.L1811
.L1875:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17742:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitS8x16ShuffleEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitS8x16ShuffleEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitI16x8ExtractLaneEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitI16x8ExtractLaneEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitI16x8ExtractLaneEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitI16x8ExtractLaneEPNS1_4NodeE:
.LFB17593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	44(%rax), %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	jne	.L1880
	cmpb	$19, -76(%rbp)
	je	.L1887
.L1880:
	movq	160(%rbx), %rsi
	movq	%rsi, %r14
	subq	152(%rbx), %r14
	sarq	$4, %r14
	cmpq	168(%rbx), %rsi
	je	.L1882
	movq	-72(%rbp), %rdx
	movzbl	-76(%rbp), %ecx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rbx)
.L1883:
	salq	$32, %r14
	orq	$11, %r14
.L1881:
	movzbl	23(%r15), %eax
	movq	32(%r15), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1884
	movq	16(%r13), %r13
.L1884:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	xorl	%r9d, %r9d
	pushq	$0
	salq	$3, %rdx
	movq	%r14, %r8
	movq	%r13, %rcx
	movabsq	$927712935937, %rbx
	movl	$321, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1888
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1887:
	.cfi_restore_state
	movslq	-72(%rbp), %r14
	salq	$32, %r14
	orq	$3, %r14
	jmp	.L1881
	.p2align 4,,10
	.p2align 3
.L1882:
	leaq	144(%rbx), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1883
.L1888:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17593:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitI16x8ExtractLaneEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitI16x8ExtractLaneEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitI64x2ExtractLaneEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitI64x2ExtractLaneEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitI64x2ExtractLaneEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitI64x2ExtractLaneEPNS1_4NodeE:
.LFB17591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	44(%rax), %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	jne	.L1890
	cmpb	$19, -76(%rbp)
	je	.L1897
.L1890:
	movq	160(%rbx), %rsi
	movq	%rsi, %r14
	subq	152(%rbx), %r14
	sarq	$4, %r14
	cmpq	168(%rbx), %rsi
	je	.L1892
	movq	-72(%rbp), %rdx
	movzbl	-76(%rbp), %ecx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rbx)
.L1893:
	salq	$32, %r14
	orq	$11, %r14
.L1891:
	movzbl	23(%r15), %eax
	movq	32(%r15), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1894
	movq	16(%r13), %r13
.L1894:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	xorl	%r9d, %r9d
	pushq	$0
	salq	$3, %rdx
	movq	%r14, %r8
	movq	%r13, %rcx
	movabsq	$927712935937, %rbx
	movl	$274, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1898
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1897:
	.cfi_restore_state
	movslq	-72(%rbp), %r14
	salq	$32, %r14
	orq	$3, %r14
	jmp	.L1891
	.p2align 4,,10
	.p2align 3
.L1892:
	leaq	144(%rbx), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1893
.L1898:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17591:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitI64x2ExtractLaneEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitI64x2ExtractLaneEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitI32x4ExtractLaneEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitI32x4ExtractLaneEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitI32x4ExtractLaneEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitI32x4ExtractLaneEPNS1_4NodeE:
.LFB17592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	44(%rax), %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	jne	.L1900
	cmpb	$19, -76(%rbp)
	je	.L1907
.L1900:
	movq	160(%rbx), %rsi
	movq	%rsi, %r14
	subq	152(%rbx), %r14
	sarq	$4, %r14
	cmpq	168(%rbx), %rsi
	je	.L1902
	movq	-72(%rbp), %rdx
	movzbl	-76(%rbp), %ecx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rbx)
.L1903:
	salq	$32, %r14
	orq	$11, %r14
.L1901:
	movzbl	23(%r15), %eax
	movq	32(%r15), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1904
	movq	16(%r13), %r13
.L1904:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	xorl	%r9d, %r9d
	pushq	$0
	salq	$3, %rdx
	movq	%r14, %r8
	movq	%r13, %rcx
	movabsq	$927712935937, %rbx
	movl	$294, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1908
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1907:
	.cfi_restore_state
	movslq	-72(%rbp), %r14
	salq	$32, %r14
	orq	$3, %r14
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L1902:
	leaq	144(%rbx), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1903
.L1908:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17592:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitI32x4ExtractLaneEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitI32x4ExtractLaneEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitI8x16ExtractLaneEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitI8x16ExtractLaneEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitI8x16ExtractLaneEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitI8x16ExtractLaneEPNS1_4NodeE:
.LFB17594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	44(%rax), %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	jne	.L1910
	cmpb	$19, -76(%rbp)
	je	.L1917
.L1910:
	movq	160(%rbx), %rsi
	movq	%rsi, %r14
	subq	152(%rbx), %r14
	sarq	$4, %r14
	cmpq	168(%rbx), %rsi
	je	.L1912
	movq	-72(%rbp), %rdx
	movzbl	-76(%rbp), %ecx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rbx)
.L1913:
	salq	$32, %r14
	orq	$11, %r14
.L1911:
	movzbl	23(%r15), %eax
	movq	32(%r15), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1914
	movq	16(%r13), %r13
.L1914:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	xorl	%r9d, %r9d
	pushq	$0
	salq	$3, %rdx
	movq	%r14, %r8
	movq	%r13, %rcx
	movabsq	$927712935937, %rbx
	movl	$352, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1918
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1917:
	.cfi_restore_state
	movslq	-72(%rbp), %r14
	salq	$32, %r14
	orq	$3, %r14
	jmp	.L1911
	.p2align 4,,10
	.p2align 3
.L1912:
	leaq	144(%rbx), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1913
.L1918:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17594:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitI8x16ExtractLaneEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitI8x16ExtractLaneEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitF64x2ExtractLaneEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitF64x2ExtractLaneEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitF64x2ExtractLaneEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitF64x2ExtractLaneEPNS1_4NodeE:
.LFB17589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	44(%rax), %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	jne	.L1920
	cmpb	$19, -76(%rbp)
	je	.L1927
.L1920:
	movq	160(%rbx), %rsi
	movq	%rsi, %r14
	subq	152(%rbx), %r14
	sarq	$4, %r14
	cmpq	168(%rbx), %rsi
	je	.L1922
	movq	-72(%rbp), %rdx
	movzbl	-76(%rbp), %ecx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rbx)
.L1923:
	salq	$32, %r14
	orq	$11, %r14
.L1921:
	movzbl	23(%r15), %eax
	movq	32(%r15), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1924
	movq	16(%r13), %r13
.L1924:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	xorl	%r9d, %r9d
	pushq	$0
	salq	$3, %rdx
	movq	%r14, %r8
	movq	%r13, %rcx
	movabsq	$927712935937, %rbx
	movl	$239, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1928
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1927:
	.cfi_restore_state
	movslq	-72(%rbp), %r14
	salq	$32, %r14
	orq	$3, %r14
	jmp	.L1921
	.p2align 4,,10
	.p2align 3
.L1922:
	leaq	144(%rbx), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1923
.L1928:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17589:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitF64x2ExtractLaneEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitF64x2ExtractLaneEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitF32x4ExtractLaneEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitF32x4ExtractLaneEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitF32x4ExtractLaneEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitF32x4ExtractLaneEPNS1_4NodeE:
.LFB17590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	44(%rax), %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	jne	.L1930
	cmpb	$19, -76(%rbp)
	je	.L1937
.L1930:
	movq	160(%rbx), %rsi
	movq	%rsi, %r14
	subq	152(%rbx), %r14
	sarq	$4, %r14
	cmpq	168(%rbx), %rsi
	je	.L1932
	movq	-72(%rbp), %rdx
	movzbl	-76(%rbp), %ecx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rbx)
.L1933:
	salq	$32, %r14
	orq	$11, %r14
.L1931:
	movzbl	23(%r15), %eax
	movq	32(%r15), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1934
	movq	16(%r13), %r13
.L1934:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	xorl	%r9d, %r9d
	pushq	$0
	salq	$3, %rdx
	movq	%r14, %r8
	movq	%r13, %rcx
	movabsq	$927712935937, %rbx
	movl	$254, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1938
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1937:
	.cfi_restore_state
	movslq	-72(%rbp), %r14
	salq	$32, %r14
	orq	$3, %r14
	jmp	.L1931
	.p2align 4,,10
	.p2align 3
.L1932:
	leaq	144(%rbx), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1933
.L1938:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17590:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitF32x4ExtractLaneEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitF32x4ExtractLaneEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitStackSlotEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitStackSlotEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitStackSlotEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitStackSlotEPNS1_4NodeE:
.LFB17371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler25StackSlotRepresentationOfEPKNS1_8OperatorE@PLT
	movq	368(%r12), %rdx
	movl	(%rax), %eax
	movl	4(%rdx), %r8d
	movl	%eax, %esi
	leal	14(%rax), %ecx
	addl	$7, %esi
	movl	%r8d, %ebx
	cmovns	%esi, %ecx
	andl	$15, %eax
	sarl	$3, %ecx
	cmpl	$1, %eax
	sbbl	%edi, %edi
	subl	$1, %edi
	cmpl	$1, %eax
	sbbl	%eax, %eax
	subl	%eax, %ebx
	leal	(%rbx,%rcx), %esi
	andl	%edi, %esi
	movq	%r15, %rdi
	movl	%esi, %eax
	movl	%esi, 4(%rdx)
	subl	12(%rdx), %esi
	subl	%r8d, %eax
	addl	%eax, 8(%rdx)
	subl	$1, %esi
	movq	16(%r12), %rbx
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	jne	.L1941
	cmpb	$19, -76(%rbp)
	je	.L1948
.L1941:
	movq	160(%rbx), %rsi
	movq	%rsi, %r14
	subq	152(%rbx), %r14
	sarq	$4, %r14
	cmpq	168(%rbx), %rsi
	je	.L1943
	movq	-72(%rbp), %rdx
	movzbl	-76(%rbp), %ecx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rbx)
.L1944:
	salq	$32, %r14
	orq	$11, %r14
.L1942:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	salq	$3, %rdx
	movq	%r14, %rcx
	movl	$28, %esi
	movq	%r12, %rdi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1949
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1948:
	.cfi_restore_state
	movslq	-72(%rbp), %r14
	salq	$32, %r14
	orq	$3, %r14
	jmp	.L1942
	.p2align 4,,10
	.p2align 3
.L1943:
	leaq	144(%rbx), %rdi
	movq	%r15, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1944
.L1949:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17371:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitStackSlotEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitStackSlotEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPm,"axG",@progbits,_ZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPm
	.type	_ZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPm, @function
_ZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPm:
.LFB17365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movq	24(%rbp), %r15
	movl	%edx, -92(%rbp)
	movl	%r9d, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L1951
	movq	%r13, %rax
	movq	%rcx, %rsi
	orq	%r8, %rax
	jne	.L1999
	movq	(%r15), %rax
	movq	%rcx, -88(%rbp)
	leaq	1(%rax), %rdx
	leaq	(%rbx,%rax,8), %r13
	movq	%rdx, (%r15)
	movq	(%rdi), %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-88(%rbp), %rsi
	movq	(%r12), %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movq	%rbx, 0(%r13)
.L1998:
	movl	$1, %r14d
	.p2align 4,,10
	.p2align 3
.L1950:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2000
	addq	$88, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1999:
	.cfi_restore_state
	movq	(%rcx), %rdx
	movzwl	16(%rdx), %eax
	cmpl	$23, %eax
	je	.L2001
	cmpl	$24, %eax
	je	.L2002
.L1954:
	movq	(%r15), %rax
	movq	%r8, -120(%rbp)
	movq	%rsi, -104(%rbp)
	leaq	1(%rax), %rdx
	movq	%rdx, (%r15)
	movq	(%r12), %rdi
	leaq	(%rbx,%rax,8), %rdx
	movq	%rdx, -112(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-104(%rbp), %rsi
	movq	(%r12), %rdi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %r14
	movq	-112(%rbp), %rdx
	movabsq	$377957122049, %r10
	orq	%r10, %r14
	testq	%r13, %r13
	movq	-120(%rbp), %r8
	movq	%r14, (%rdx)
	je	.L2003
	movq	(%r15), %rax
	movq	%r13, %rsi
	movq	%r8, -112(%rbp)
	leaq	1(%rax), %rdx
	movq	%rdx, (%r15)
	movq	(%r12), %rdi
	leaq	(%rbx,%rax,8), %rdx
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	(%r12), %rdi
	movq	%r13, %rsi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %r14
	movq	-104(%rbp), %rdx
	movabsq	$377957122049, %r10
	movq	-112(%rbp), %r8
	orq	%r14, %r10
	movq	%r10, (%rdx)
	testq	%r8, %r8
	je	.L1955
	movq	(%r15), %rax
	cmpl	$1, -88(%rbp)
	leaq	1(%rax), %rdx
	leaq	(%rbx,%rax,8), %r13
	movq	%rdx, (%r15)
	je	.L2004
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
.L1995:
	movq	%rax, 0(%r13)
	movslq	-92(%rbp), %rbx
	leaq	_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE11kMRnI_modes(%rip), %rax
	movl	(%rax,%rbx,4), %r14d
	jmp	.L1950
	.p2align 4,,10
	.p2align 3
.L2001:
	movl	44(%rdx), %eax
	testl	%eax, %eax
	jne	.L1954
	.p2align 4,,10
	.p2align 3
.L1951:
	movq	(%r15), %rax
	leaq	1(%rax), %rsi
	leaq	(%rbx,%rax,8), %rdx
	testq	%r8, %r8
	je	.L1975
	testq	%r13, %r13
	je	.L2005
	movq	%rsi, (%r15)
	movq	(%r12), %rdi
	movq	%r13, %rsi
	movq	%r8, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	(%r12), %rdi
	movq	%r13, %rsi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %r14
	movq	-104(%rbp), %rdx
	movabsq	$377957122049, %rax
	orq	%rax, %r14
	cmpl	$1, -88(%rbp)
	movq	-112(%rbp), %r8
	movq	%r14, (%rdx)
	movq	(%r15), %rax
	leaq	1(%rax), %rdx
	leaq	(%rbx,%rax,8), %r13
	movq	%rdx, (%r15)
	je	.L2006
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
.L1996:
	movq	%rax, 0(%r13)
	movslq	-92(%rbp), %rbx
	leaq	_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE10kMnI_modes(%rip), %rax
	movl	(%rax,%rbx,4), %r14d
	jmp	.L1950
	.p2align 4,,10
	.p2align 3
.L2002:
	cmpq	$0, 48(%rdx)
	je	.L1951
	jmp	.L1954
	.p2align 4,,10
	.p2align 3
.L2006:
	movq	(%r12), %rax
	movq	16(%rax), %r14
	movq	(%r8), %rax
	movzwl	16(%rax), %edx
	cmpw	$23, %dx
	je	.L1978
	cmpw	$24, %dx
	jne	.L1959
	movq	48(%rax), %rax
	movl	$1, -80(%rbp)
	movl	$19, %edx
	movb	$19, -76(%rbp)
	negq	%rax
	movq	%rax, -72(%rbp)
.L1981:
	movq	160(%r14), %rsi
	movq	%rsi, %r12
	subq	152(%r14), %r12
	sarq	$4, %r12
	cmpq	168(%r14), %rsi
	je	.L1983
	movl	-88(%rbp), %ecx
	movb	%dl, 4(%rsi)
	movq	%rax, 8(%rsi)
	movl	%ecx, (%rsi)
	addq	$16, 160(%r14)
.L1984:
	movq	%r12, %rax
	salq	$32, %rax
	orq	$11, %rax
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L1955:
	movslq	-92(%rbp), %rbx
	leaq	_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE10kMRn_modes(%rip), %rax
	movl	(%rax,%rbx,4), %r14d
	jmp	.L1950
	.p2align 4,,10
	.p2align 3
.L2003:
	testq	%r8, %r8
	je	.L1998
	movq	(%r15), %rax
	cmpl	$1, -88(%rbp)
	leaq	1(%rax), %rdx
	leaq	(%rbx,%rax,8), %r13
	movq	%rdx, (%r15)
	je	.L2007
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
.L1997:
	movq	%rax, 0(%r13)
	movl	$2, %r14d
	jmp	.L1950
	.p2align 4,,10
	.p2align 3
.L2005:
	movq	%rsi, (%r15)
	movq	%r12, %rdi
	movq	%r8, %rsi
	movl	$1, %r14d
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	movq	-88(%rbp), %rdx
	movq	%rax, (%rdx)
	jmp	.L1950
	.p2align 4,,10
	.p2align 3
.L1975:
	movq	%rsi, (%r15)
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	movq	-88(%rbp), %rdx
	movq	%rax, (%rdx)
	movslq	-92(%rbp), %rdx
	leaq	_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE9kMn_modes(%rip), %rax
	movl	(%rax,%rdx,4), %r14d
	cmpl	$3, %r14d
	jne	.L1950
	movq	(%r15), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	1(%rdx), %rax
	movq	%rdx, -88(%rbp)
	movq	%rax, (%r15)
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	movq	-88(%rbp), %rdx
	movq	%rax, (%rbx,%rdx,8)
	jmp	.L1950
	.p2align 4,,10
	.p2align 3
.L2004:
	movq	(%r12), %rax
	movq	16(%rax), %r14
	movq	(%r8), %rax
	movzwl	16(%rax), %edx
	cmpw	$23, %dx
	je	.L1957
	cmpw	$24, %dx
	jne	.L1959
	movq	48(%rax), %rax
	movl	$1, -80(%rbp)
	movl	$19, %edx
	movb	$19, -76(%rbp)
	negq	%rax
	movq	%rax, -72(%rbp)
.L1961:
	movq	160(%r14), %rsi
	movq	%rsi, %r12
	subq	152(%r14), %r12
	sarq	$4, %r12
	cmpq	168(%r14), %rsi
	je	.L1963
	movl	-88(%rbp), %ecx
	movb	%dl, 4(%rsi)
	movq	%rax, 8(%rsi)
	movl	%ecx, (%rsi)
	addq	$16, 160(%r14)
.L1964:
	movq	%r12, %rax
	salq	$32, %rax
	orq	$11, %rax
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L2007:
	movq	(%r12), %rax
	movq	16(%rax), %r12
	movq	(%r8), %rax
	movzwl	16(%rax), %edx
	cmpw	$23, %dx
	je	.L1968
	cmpw	$24, %dx
	jne	.L1959
	movq	48(%rax), %rax
	movl	$1, -80(%rbp)
	movl	$19, %edx
	movb	$19, -76(%rbp)
	negq	%rax
	movq	%rax, -72(%rbp)
.L1971:
	movq	160(%r12), %rsi
	movq	%rsi, %rbx
	subq	152(%r12), %rbx
	sarq	$4, %rbx
	cmpq	168(%r12), %rsi
	je	.L1973
	movl	-88(%rbp), %ecx
	movb	%dl, 4(%rsi)
	movq	%rax, 8(%rsi)
	movl	%ecx, (%rsi)
	addq	$16, 160(%r12)
.L1974:
	movq	%rbx, %rax
	salq	$32, %rax
	orq	$11, %rax
	jmp	.L1997
	.p2align 4,,10
	.p2align 3
.L1978:
	movl	44(%rax), %esi
	leaq	-80(%rbp), %rdi
	negl	%esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	movzbl	-76(%rbp), %edx
	movl	%eax, %ecx
	movl	%eax, -88(%rbp)
	movq	-72(%rbp), %rax
	testl	%ecx, %ecx
	jne	.L1981
	cmpb	$19, %dl
	jne	.L1981
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L1957:
	movl	44(%rax), %esi
	leaq	-80(%rbp), %rdi
	negl	%esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	movzbl	-76(%rbp), %edx
	movl	%eax, %ecx
	movl	%eax, -88(%rbp)
	movq	-72(%rbp), %rax
	testl	%ecx, %ecx
	jne	.L1961
	cmpb	$19, %dl
	jne	.L1961
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L1968:
	movl	44(%rax), %esi
	leaq	-80(%rbp), %rdi
	negl	%esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	movzbl	-76(%rbp), %edx
	movl	%eax, %ecx
	movl	%eax, -88(%rbp)
	movq	-72(%rbp), %rax
	testl	%ecx, %ecx
	jne	.L1971
	cmpb	$19, %dl
	jne	.L1971
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L1997
	.p2align 4,,10
	.p2align 3
.L1983:
	leaq	-80(%rbp), %rdx
	leaq	144(%r14), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1984
.L1963:
	leaq	-80(%rbp), %rdx
	leaq	144(%r14), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1964
.L1973:
	leaq	-80(%rbp), %rdx
	leaq	144(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1974
.L2000:
	call	__stack_chk_fail@PLT
.L1959:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17365:
	.size	_ZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPm, .-_ZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPm
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_17EmitLeaEPNS1_19InstructionSelectorEiPNS1_4NodeES6_iS6_S6_NS1_16DisplacementModeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_17EmitLeaEPNS1_19InstructionSelectorEiPNS1_4NodeES6_iS6_S6_NS1_16DisplacementModeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_17EmitLeaEPNS1_19InstructionSelectorEiPNS1_4NodeES6_iS6_S6_NS1_16DisplacementModeE:
.LFB17395:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	movq	%rcx, %rsi
	movq	%r9, %rcx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movl	%r8d, %edx
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movl	24(%rbp), %r9d
	movq	16(%rbp), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-112(%rbp), %rax
	movq	%rdi, -120(%rbp)
	leaq	-120(%rbp), %rdi
	pushq	%rax
	pushq	%r15
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -112(%rbp)
	call	_ZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPm
	movq	-120(%rbp), %rdi
	movq	%r12, %rsi
	movq	$0, -104(%rbp)
	movl	%eax, -132(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-120(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	-132(%rbp), %esi
	pushq	$0
	movq	%r15, %r9
	pushq	$0
	salq	$3, %rbx
	movq	-112(%rbp), %r8
	leaq	-104(%rbp), %rcx
	movabsq	$927712935937, %rax
	sall	$9, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	orq	%rax, %rbx
	orl	%r13d, %esi
	movq	%rbx, -104(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	addq	$32, %rsp
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2011
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2011:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17395:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_17EmitLeaEPNS1_19InstructionSelectorEiPNS1_4NodeES6_iS6_S6_NS1_16DisplacementModeE, .-_ZN2v88internal8compiler12_GLOBAL__N_17EmitLeaEPNS1_19InstructionSelectorEiPNS1_4NodeES6_iS6_S6_NS1_16DisplacementModeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitWord32ShlEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitWord32ShlEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitWord32ShlEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitWord32ShlEPNS1_4NodeE:
.LFB17396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2014
	movq	32(%rsi), %rax
	movl	8(%rax), %eax
.L2014:
	cmpl	$1, %eax
	jle	.L2016
	leaq	-64(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	movq	(%r12), %rax
	movzwl	16(%rax), %eax
	cmpl	$302, %eax
	je	.L2043
	cmpl	$310, %eax
	je	.L2044
.L2016:
	movl	$126, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWord32ShiftEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
.L2012:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2045
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2043:
	.cfi_restore_state
	cmpb	$0, -28(%rbp)
	je	.L2016
	movl	-32(%rbp), %r8d
	cmpl	$3, %r8d
	ja	.L2016
.L2019:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rcx
	xorl	%r9d, %r9d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2046
.L2032:
	pushq	$0
	movq	%r12, %rdx
	movl	$231, %esi
	movq	%r13, %rdi
	pushq	$0
	call	_ZN2v88internal8compiler12_GLOBAL__N_17EmitLeaEPNS1_19InstructionSelectorEiPNS1_4NodeES6_iS6_S6_NS1_16DisplacementModeE
	popq	%rax
	popq	%rdx
	jmp	.L2012
	.p2align 4,,10
	.p2align 3
.L2046:
	movq	16(%rcx), %rcx
	jmp	.L2032
	.p2align 4,,10
	.p2align 3
.L2044:
	cmpb	$0, -28(%rbp)
	je	.L2016
	movl	-32(%rbp), %eax
	cmpl	$1, %eax
	je	.L2023
	cmpl	$2, %eax
	je	.L2024
	movl	$2, %r8d
	cmpl	$4, %eax
	je	.L2019
	cmpl	$8, %eax
	je	.L2027
	cmpl	$3, %eax
	je	.L2028
	cmpl	$5, %eax
	je	.L2029
	cmpl	$9, %eax
	jne	.L2016
	movl	$3, %r8d
.L2031:
	movzbl	23(%r12), %eax
	movq	32(%r12), %r9
	andl	$15, %eax
	movq	%r9, %rcx
	cmpl	$15, %eax
	jne	.L2032
	movq	16(%r9), %r9
	movq	%r9, %rcx
	jmp	.L2032
	.p2align 4,,10
	.p2align 3
.L2023:
	xorl	%r8d, %r8d
	jmp	.L2019
	.p2align 4,,10
	.p2align 3
.L2024:
	movl	$1, %r8d
	jmp	.L2019
.L2027:
	movl	$3, %r8d
	jmp	.L2019
.L2028:
	movl	$1, %r8d
	jmp	.L2031
.L2029:
	movl	$2, %r8d
	jmp	.L2031
.L2045:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17396:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitWord32ShlEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitWord32ShlEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitWord64ShlEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitWord64ShlEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitWord64ShlEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitWord64ShlEPNS1_4NodeE:
.LFB17397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movq	%rdi, -120(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2049
	movq	32(%rsi), %rax
	movl	8(%rax), %eax
.L2049:
	leaq	-112(%rbp), %r14
	cmpl	$1, %eax
	jle	.L2051
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	movq	(%r12), %rax
	movzwl	16(%rax), %eax
	cmpl	$321, %eax
	je	.L2087
	cmpl	$329, %eax
	je	.L2088
.L2051:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movzwl	16(%rax), %eax
	cmpw	$465, %ax
	je	.L2079
	cmpw	$462, %ax
	je	.L2079
.L2067:
	movl	$125, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWord64ShiftEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
.L2047:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2089
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2087:
	.cfi_restore_state
	cmpb	$0, -64(%rbp)
	je	.L2051
	movq	-72(%rbp), %r8
	cmpq	$3, %r8
	ja	.L2051
.L2055:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rcx
	xorl	%r9d, %r9d
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2073
	movq	16(%rcx), %rcx
.L2073:
	pushq	$0
	movl	$232, %esi
	movq	%r12, %rdx
	movq	%r13, %rdi
	pushq	$0
	call	_ZN2v88internal8compiler12_GLOBAL__N_17EmitLeaEPNS1_19InstructionSelectorEiPNS1_4NodeES6_iS6_S6_NS1_16DisplacementModeE
	popq	%rcx
	popq	%rsi
	jmp	.L2047
	.p2align 4,,10
	.p2align 3
.L2079:
	cmpb	$0, -64(%rbp)
	je	.L2067
	movq	-72(%rbp), %rax
	subq	$32, %rax
	cmpq	$31, %rax
	ja	.L2067
	movq	-80(%rbp), %rsi
	leaq	-120(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	movq	-104(%rbp), %rdx
	movq	%rax, %rbx
	movzbl	23(%rdx), %eax
	leaq	32(%rdx), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2072
	movq	32(%rdx), %rcx
	addq	$16, %rcx
.L2072:
	movq	(%rcx), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler16OperandGenerator17DefineSameAsFirstEPNS1_4NodeE
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	pushq	$0
	movq	%rax, %rdx
	movq	%r14, %rcx
	movl	$125, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	jmp	.L2047
	.p2align 4,,10
	.p2align 3
.L2088:
	cmpb	$0, -64(%rbp)
	je	.L2051
	movq	-72(%rbp), %rax
	cmpq	$1, %rax
	je	.L2058
	cmpq	$2, %rax
	je	.L2059
	movl	$2, %r8d
	cmpq	$4, %rax
	je	.L2055
	cmpq	$8, %rax
	je	.L2061
	cmpq	$3, %rax
	je	.L2062
	cmpq	$5, %rax
	je	.L2063
	cmpq	$9, %rax
	jne	.L2051
	movl	$3, %r8d
.L2065:
	movzbl	23(%r12), %eax
	movq	32(%r12), %r9
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2090
	movq	%r9, %rcx
	jmp	.L2073
	.p2align 4,,10
	.p2align 3
.L2058:
	xorl	%r8d, %r8d
	jmp	.L2055
	.p2align 4,,10
	.p2align 3
.L2059:
	movl	$1, %r8d
	jmp	.L2055
.L2061:
	movl	$3, %r8d
	jmp	.L2055
.L2062:
	movl	$1, %r8d
	jmp	.L2065
.L2063:
	movl	$2, %r8d
	jmp	.L2065
.L2089:
	call	__stack_chk_fail@PLT
.L2090:
	movq	16(%r9), %r9
	movq	%r9, %rcx
	jmp	.L2073
	.cfi_endproc
.LFE17397:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitWord64ShlEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitWord64ShlEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitInt32MulEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitInt32MulEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitInt32MulEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitInt32MulEPNS1_4NodeE:
.LFB17421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2093
	movq	32(%rsi), %rax
	movl	8(%rax), %eax
.L2093:
	cmpl	$1, %eax
	jle	.L2095
	leaq	-64(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	movq	(%r12), %rax
	movzwl	16(%rax), %eax
	cmpl	$302, %eax
	je	.L2122
	cmpl	$310, %eax
	je	.L2123
.L2095:
	movl	$114, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_18VisitMulEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
.L2091:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2124
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2122:
	.cfi_restore_state
	cmpb	$0, -28(%rbp)
	je	.L2095
	movl	-32(%rbp), %r8d
	cmpl	$3, %r8d
	ja	.L2095
.L2098:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rcx
	xorl	%r9d, %r9d
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2125
.L2111:
	pushq	$0
	movq	%r12, %rdx
	movl	$231, %esi
	movq	%r13, %rdi
	pushq	$0
	call	_ZN2v88internal8compiler12_GLOBAL__N_17EmitLeaEPNS1_19InstructionSelectorEiPNS1_4NodeES6_iS6_S6_NS1_16DisplacementModeE
	popq	%rax
	popq	%rdx
	jmp	.L2091
	.p2align 4,,10
	.p2align 3
.L2125:
	movq	16(%rcx), %rcx
	jmp	.L2111
	.p2align 4,,10
	.p2align 3
.L2123:
	cmpb	$0, -28(%rbp)
	je	.L2095
	movl	-32(%rbp), %eax
	cmpl	$1, %eax
	je	.L2102
	cmpl	$2, %eax
	je	.L2103
	movl	$2, %r8d
	cmpl	$4, %eax
	je	.L2098
	cmpl	$8, %eax
	je	.L2106
	cmpl	$3, %eax
	je	.L2107
	cmpl	$5, %eax
	je	.L2108
	cmpl	$9, %eax
	jne	.L2095
	movl	$3, %r8d
.L2110:
	movzbl	23(%r12), %eax
	movq	32(%r12), %r9
	andl	$15, %eax
	movq	%r9, %rcx
	cmpl	$15, %eax
	jne	.L2111
	movq	16(%r9), %r9
	movq	%r9, %rcx
	jmp	.L2111
	.p2align 4,,10
	.p2align 3
.L2102:
	xorl	%r8d, %r8d
	jmp	.L2098
	.p2align 4,,10
	.p2align 3
.L2103:
	movl	$1, %r8d
	jmp	.L2098
.L2106:
	movl	$3, %r8d
	jmp	.L2098
.L2107:
	movl	$1, %r8d
	jmp	.L2110
.L2108:
	movl	$2, %r8d
	jmp	.L2110
.L2124:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17421:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitInt32MulEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitInt32MulEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitI64x2ReplaceLaneEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitI64x2ReplaceLaneEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitI64x2ReplaceLaneEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitI64x2ReplaceLaneEPNS1_4NodeE:
.LFB17597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	44(%rax), %r14d
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2127
	leaq	40(%rsi), %rax
.L2128:
	movq	(%rax), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	leaq	-80(%rbp), %rdx
	salq	$3, %r13
	movl	%r14d, %esi
	movq	%rdx, %rdi
	movq	%rdx, -88(%rbp)
	movq	16(%r12), %rbx
	movabsq	$34359738369, %rax
	orq	%rax, %r13
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	movq	-88(%rbp), %rdx
	testl	%eax, %eax
	jne	.L2129
	cmpb	$19, -76(%rbp)
	je	.L2136
.L2129:
	movq	160(%rbx), %rsi
	movq	%rsi, %r14
	subq	152(%rbx), %r14
	sarq	$4, %r14
	cmpq	168(%rbx), %rsi
	je	.L2131
	movq	-72(%rbp), %rdx
	movzbl	-76(%rbp), %ecx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rbx)
.L2132:
	movq	%r14, %r8
	salq	$32, %r8
	orq	$11, %r8
.L2130:
	movzbl	23(%r15), %eax
	movq	32(%r15), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2133
	movq	16(%r14), %r14
.L2133:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	pushq	$0
	movl	%ebx, %edx
	movq	-88(%rbp), %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %r9
	movq	%r14, %rcx
	movabsq	$1065151889409, %rbx
	movl	$275, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2137
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2136:
	.cfi_restore_state
	movslq	-72(%rbp), %r8
	salq	$32, %r8
	orq	$3, %r8
	jmp	.L2130
	.p2align 4,,10
	.p2align 3
.L2127:
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L2128
	.p2align 4,,10
	.p2align 3
.L2131:
	leaq	144(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L2132
.L2137:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17597:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitI64x2ReplaceLaneEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitI64x2ReplaceLaneEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitF64x2ReplaceLaneEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitF64x2ReplaceLaneEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitF64x2ReplaceLaneEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitF64x2ReplaceLaneEPNS1_4NodeE:
.LFB17595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	44(%rax), %r14d
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2139
	leaq	40(%rsi), %rax
.L2140:
	movq	(%rax), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	leaq	-80(%rbp), %rdx
	salq	$3, %r13
	movl	%r14d, %esi
	movq	%rdx, %rdi
	movq	%rdx, -88(%rbp)
	movq	16(%r12), %rbx
	movabsq	$34359738369, %rax
	orq	%rax, %r13
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	movq	-88(%rbp), %rdx
	testl	%eax, %eax
	jne	.L2141
	cmpb	$19, -76(%rbp)
	je	.L2148
.L2141:
	movq	160(%rbx), %rsi
	movq	%rsi, %r14
	subq	152(%rbx), %r14
	sarq	$4, %r14
	cmpq	168(%rbx), %rsi
	je	.L2143
	movq	-72(%rbp), %rdx
	movzbl	-76(%rbp), %ecx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rbx)
.L2144:
	movq	%r14, %r8
	salq	$32, %r8
	orq	$11, %r8
.L2142:
	movzbl	23(%r15), %eax
	movq	32(%r15), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2145
	movq	16(%r14), %r14
.L2145:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	pushq	$0
	movl	%ebx, %edx
	movq	-88(%rbp), %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %r9
	movq	%r14, %rcx
	movabsq	$1065151889409, %rbx
	movl	$240, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2149
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2148:
	.cfi_restore_state
	movslq	-72(%rbp), %r8
	salq	$32, %r8
	orq	$3, %r8
	jmp	.L2142
	.p2align 4,,10
	.p2align 3
.L2139:
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L2140
	.p2align 4,,10
	.p2align 3
.L2143:
	leaq	144(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L2144
.L2149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17595:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitF64x2ReplaceLaneEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitF64x2ReplaceLaneEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitI16x8ReplaceLaneEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitI16x8ReplaceLaneEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitI16x8ReplaceLaneEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitI16x8ReplaceLaneEPNS1_4NodeE:
.LFB17599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	44(%rax), %r14d
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2151
	leaq	40(%rsi), %rax
.L2152:
	movq	(%rax), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	leaq	-80(%rbp), %rdx
	salq	$3, %r13
	movl	%r14d, %esi
	movq	%rdx, %rdi
	movq	%rdx, -88(%rbp)
	movq	16(%r12), %rbx
	movabsq	$34359738369, %rax
	orq	%rax, %r13
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	movq	-88(%rbp), %rdx
	testl	%eax, %eax
	jne	.L2153
	cmpb	$19, -76(%rbp)
	je	.L2160
.L2153:
	movq	160(%rbx), %rsi
	movq	%rsi, %r14
	subq	152(%rbx), %r14
	sarq	$4, %r14
	cmpq	168(%rbx), %rsi
	je	.L2155
	movq	-72(%rbp), %rdx
	movzbl	-76(%rbp), %ecx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rbx)
.L2156:
	movq	%r14, %r8
	salq	$32, %r8
	orq	$11, %r8
.L2154:
	movzbl	23(%r15), %eax
	movq	32(%r15), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2157
	movq	16(%r14), %r14
.L2157:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	pushq	$0
	movl	%ebx, %edx
	movq	-88(%rbp), %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %r9
	movq	%r14, %rcx
	movabsq	$1065151889409, %rbx
	movl	$322, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2161
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2160:
	.cfi_restore_state
	movslq	-72(%rbp), %r8
	salq	$32, %r8
	orq	$3, %r8
	jmp	.L2154
	.p2align 4,,10
	.p2align 3
.L2151:
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L2152
	.p2align 4,,10
	.p2align 3
.L2155:
	leaq	144(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L2156
.L2161:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17599:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitI16x8ReplaceLaneEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitI16x8ReplaceLaneEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitF32x4ReplaceLaneEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitF32x4ReplaceLaneEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitF32x4ReplaceLaneEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitF32x4ReplaceLaneEPNS1_4NodeE:
.LFB17596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	44(%rax), %r14d
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2163
	leaq	40(%rsi), %rax
.L2164:
	movq	(%rax), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	leaq	-80(%rbp), %rdx
	salq	$3, %r13
	movl	%r14d, %esi
	movq	%rdx, %rdi
	movq	%rdx, -88(%rbp)
	movq	16(%r12), %rbx
	movabsq	$34359738369, %rax
	orq	%rax, %r13
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	movq	-88(%rbp), %rdx
	testl	%eax, %eax
	jne	.L2165
	cmpb	$19, -76(%rbp)
	je	.L2172
.L2165:
	movq	160(%rbx), %rsi
	movq	%rsi, %r14
	subq	152(%rbx), %r14
	sarq	$4, %r14
	cmpq	168(%rbx), %rsi
	je	.L2167
	movq	-72(%rbp), %rdx
	movzbl	-76(%rbp), %ecx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rbx)
.L2168:
	movq	%r14, %r8
	salq	$32, %r8
	orq	$11, %r8
.L2166:
	movzbl	23(%r15), %eax
	movq	32(%r15), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2169
	movq	16(%r14), %r14
.L2169:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	pushq	$0
	movl	%ebx, %edx
	movq	-88(%rbp), %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %r9
	movq	%r14, %rcx
	movabsq	$1065151889409, %rbx
	movl	$255, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2173
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2172:
	.cfi_restore_state
	movslq	-72(%rbp), %r8
	salq	$32, %r8
	orq	$3, %r8
	jmp	.L2166
	.p2align 4,,10
	.p2align 3
.L2163:
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L2164
	.p2align 4,,10
	.p2align 3
.L2167:
	leaq	144(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L2168
.L2173:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17596:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitF32x4ReplaceLaneEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitF32x4ReplaceLaneEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitI32x4ReplaceLaneEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitI32x4ReplaceLaneEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitI32x4ReplaceLaneEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitI32x4ReplaceLaneEPNS1_4NodeE:
.LFB17598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	44(%rax), %r14d
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2175
	leaq	40(%rsi), %rax
.L2176:
	movq	(%rax), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	leaq	-80(%rbp), %rdx
	salq	$3, %r13
	movl	%r14d, %esi
	movq	%rdx, %rdi
	movq	%rdx, -88(%rbp)
	movq	16(%r12), %rbx
	movabsq	$34359738369, %rax
	orq	%rax, %r13
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	movq	-88(%rbp), %rdx
	testl	%eax, %eax
	jne	.L2177
	cmpb	$19, -76(%rbp)
	je	.L2184
.L2177:
	movq	160(%rbx), %rsi
	movq	%rsi, %r14
	subq	152(%rbx), %r14
	sarq	$4, %r14
	cmpq	168(%rbx), %rsi
	je	.L2179
	movq	-72(%rbp), %rdx
	movzbl	-76(%rbp), %ecx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rbx)
.L2180:
	movq	%r14, %r8
	salq	$32, %r8
	orq	$11, %r8
.L2178:
	movzbl	23(%r15), %eax
	movq	32(%r15), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2181
	movq	16(%r14), %r14
.L2181:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	pushq	$0
	movl	%ebx, %edx
	movq	-88(%rbp), %r8
	pushq	$0
	salq	$3, %rdx
	movq	%r13, %r9
	movq	%r14, %rcx
	movabsq	$1065151889409, %rbx
	movl	$295, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2185
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2184:
	.cfi_restore_state
	movslq	-72(%rbp), %r8
	salq	$32, %r8
	orq	$3, %r8
	jmp	.L2178
	.p2align 4,,10
	.p2align 3
.L2175:
	movq	32(%rsi), %rax
	addq	$24, %rax
	jmp	.L2176
	.p2align 4,,10
	.p2align 3
.L2179:
	leaq	144(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L2180
.L2185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17598:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitI32x4ReplaceLaneEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitI32x4ReplaceLaneEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE24OwnedByAddressingOperandEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE24OwnedByAddressingOperandEPNS1_4NodeE,comdat
	.p2align 4
	.weak	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE24OwnedByAddressingOperandEPNS1_4NodeE
	.type	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE24OwnedByAddressingOperandEPNS1_4NodeE, @function
_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE24OwnedByAddressingOperandEPNS1_4NodeE:
.LFB20078:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	$1, %eax
	testq	%rdx, %rdx
	je	.L2186
	movq	(%rdx), %rsi
.L2195:
	movl	16(%rdx), %ecx
	movl	%ecx, %eax
	shrl	%eax
	andl	$1, %ecx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rdx,%rax,8), %rdx
	movq	(%rdx), %rax
	jne	.L2188
	movq	%rax, %rdx
	movq	(%rax), %rax
.L2188:
	movzwl	16(%rax), %eax
	cmpw	$431, %ax
	je	.L2189
	ja	.L2190
	cmpw	$325, %ax
	je	.L2191
	jbe	.L2214
	subw	$429, %ax
	cmpw	$1, %ax
	ja	.L2199
.L2191:
	testq	%rsi, %rsi
	je	.L2200
.L2215:
	movq	%rsi, %rdx
	movq	(%rsi), %rsi
	jmp	.L2195
	.p2align 4,,10
	.p2align 3
.L2214:
	cmpw	$306, %ax
	je	.L2191
.L2199:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2190:
	cmpw	$502, %ax
	je	.L2191
	cmpw	$503, %ax
	jne	.L2199
.L2189:
	movzbl	23(%rdx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2193
	addq	$48, %rdx
.L2194:
	cmpq	(%rdx), %rdi
	je	.L2199
	testq	%rsi, %rsi
	jne	.L2215
.L2200:
	movl	$1, %eax
.L2186:
	ret
	.p2align 4,,10
	.p2align 3
.L2193:
	movq	32(%rdx), %rdx
	addq	$32, %rdx
	jmp	.L2194
	.cfi_endproc
.LFE20078:
	.size	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE24OwnedByAddressingOperandEPNS1_4NodeE, .-_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE24OwnedByAddressingOperandEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_E10SwapInputsEv,"axG",@progbits,_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_E10SwapInputsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_E10SwapInputsEv
	.type	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_E10SwapInputsEv, @function
_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_E10SwapInputsEv:
.LFB20088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	32(%rdi), %eax
	movdqu	8(%rdi), %xmm0
	movq	8(%rdi), %r13
	movq	24(%rdi), %r15
	movl	%eax, 16(%rdi)
	movzbl	36(%rdi), %eax
	movaps	%xmm0, -64(%rbp)
	movq	(%rdi), %rsi
	movb	%al, 20(%rdi)
	movl	-56(%rbp), %eax
	movq	%r15, 8(%rdi)
	movl	%eax, 32(%rdi)
	movzbl	-52(%rbp), %eax
	movq	%r13, 24(%rdi)
	movb	%al, 36(%rdi)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2217
	movq	32(%rsi), %rdi
	leaq	32(%rsi), %r14
	cmpq	%rdi, %r15
	je	.L2229
	leaq	-24(%rsi), %r12
	testq	%rdi, %rdi
	je	.L2221
.L2246:
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2221:
	movq	%r15, (%r14)
	testq	%r15, %r15
	je	.L2222
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2222:
	movq	(%rbx), %rsi
	movq	24(%rbx), %r13
	movzbl	23(%rsi), %eax
	leaq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2223
.L2229:
	movq	8(%r14), %rdi
	cmpq	%rdi, %r13
	je	.L2216
	addq	$8, %r14
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L2227
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2227:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L2216
	addq	$24, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L2216:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2223:
	.cfi_restore_state
	movq	32(%rsi), %rsi
	leaq	16(%rsi), %r14
	jmp	.L2229
	.p2align 4,,10
	.p2align 3
.L2217:
	movq	32(%rsi), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r14
	cmpq	%rdi, %r15
	je	.L2229
	leaq	-24(%rsi), %r12
	testq	%rdi, %rdi
	jne	.L2246
	jmp	.L2221
	.cfi_endproc
.LFE20088:
	.size	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_E10SwapInputsEv, .-_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_E10SwapInputsEv
	.section	.text._ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_E10SwapInputsEv,"axG",@progbits,_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_E10SwapInputsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_E10SwapInputsEv
	.type	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_E10SwapInputsEv, @function
_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_E10SwapInputsEv:
.LFB20091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	24(%rdi), %rax
	movdqu	8(%rdi), %xmm0
	movdqu	32(%rdi), %xmm1
	movzbl	48(%rdi), %edx
	movq	(%rdi), %rsi
	movq	32(%rdi), %r14
	movq	%rax, -48(%rbp)
	movb	%dl, 24(%rdi)
	movb	%al, 48(%rdi)
	movups	%xmm1, 8(%rdi)
	movups	%xmm0, 32(%rdi)
	movzbl	23(%rsi), %eax
	movaps	%xmm0, -64(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2248
	movq	32(%rsi), %rdi
	leaq	32(%rsi), %r13
	cmpq	%rdi, %r14
	je	.L2251
.L2249:
	leaq	-24(%rsi), %r12
	testq	%rdi, %rdi
	je	.L2252
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2252:
	movq	%r14, 0(%r13)
	testq	%r14, %r14
	je	.L2253
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2253:
	movq	(%rbx), %rsi
	movq	32(%rbx), %r14
	movzbl	23(%rsi), %eax
	leaq	32(%rsi), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2254
.L2260:
	movq	8(%r13), %rdi
	cmpq	%rdi, %r14
	je	.L2247
	addq	$8, %r13
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L2258
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2258:
	movq	%r14, 0(%r13)
	testq	%r14, %r14
	je	.L2247
	addq	$32, %rsp
	movq	%r12, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L2247:
	.cfi_restore_state
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2254:
	.cfi_restore_state
	movq	32(%rsi), %rsi
	leaq	16(%rsi), %r13
	jmp	.L2260
	.p2align 4,,10
	.p2align 3
.L2248:
	movq	32(%rsi), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %r13
	cmpq	%rdi, %r14
	jne	.L2249
.L2251:
	movq	32(%rbx), %r14
	jmp	.L2260
	.cfi_endproc
.LFE20091:
	.size	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_E10SwapInputsEv, .-_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_E10SwapInputsEv
	.section	.text._ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE24OwnedByAddressingOperandEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE24OwnedByAddressingOperandEPNS1_4NodeE,comdat
	.p2align 4
	.weak	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE24OwnedByAddressingOperandEPNS1_4NodeE
	.type	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE24OwnedByAddressingOperandEPNS1_4NodeE, @function
_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE24OwnedByAddressingOperandEPNS1_4NodeE:
.LFB20096:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	$1, %eax
	testq	%rdx, %rdx
	je	.L2277
	movq	(%rdx), %rsi
.L2286:
	movl	16(%rdx), %ecx
	movl	%ecx, %eax
	shrl	%eax
	andl	$1, %ecx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rdx,%rax,8), %rdx
	movq	(%rdx), %rax
	jne	.L2279
	movq	%rax, %rdx
	movq	(%rax), %rax
.L2279:
	movzwl	16(%rax), %eax
	cmpw	$431, %ax
	je	.L2280
	ja	.L2281
	cmpw	$325, %ax
	je	.L2282
	jbe	.L2305
	subw	$429, %ax
	cmpw	$1, %ax
	ja	.L2290
.L2282:
	testq	%rsi, %rsi
	je	.L2291
.L2306:
	movq	%rsi, %rdx
	movq	(%rsi), %rsi
	jmp	.L2286
	.p2align 4,,10
	.p2align 3
.L2305:
	cmpw	$306, %ax
	je	.L2282
.L2290:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2281:
	cmpw	$502, %ax
	je	.L2282
	cmpw	$503, %ax
	jne	.L2290
.L2280:
	movzbl	23(%rdx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2284
	addq	$48, %rdx
.L2285:
	cmpq	(%rdx), %rdi
	je	.L2290
	testq	%rsi, %rsi
	jne	.L2306
.L2291:
	movl	$1, %eax
.L2277:
	ret
	.p2align 4,,10
	.p2align 3
.L2284:
	movq	32(%rdx), %rdx
	addq	$32, %rdx
	jmp	.L2285
	.cfi_endproc
.LFE20096:
	.size	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE24OwnedByAddressingOperandEPNS1_4NodeE, .-_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE24OwnedByAddressingOperandEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_306ELS6_308ELS6_310ELS6_302EEC2EPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_306ELS6_308ELS6_310ELS6_302EEC5EPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_306ELS6_308ELS6_310ELS6_302EEC2EPNS1_4NodeE
	.type	_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_306ELS6_308ELS6_310ELS6_302EEC2EPNS1_4NodeE, @function
_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_306ELS6_308ELS6_310ELS6_302EEC2EPNS1_4NodeE:
.LFB20101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rsi), %r15
	pushq	%r14
	movq	%r15, %rdx
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movzbl	18(%rax), %ecx
	movq	%rsi, (%rdi)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2308
	movq	32(%rsi), %rax
	leaq	16(%rax), %rdx
.L2308:
	movq	(%rdx), %r13
	movl	$0, 16(%rbx)
	movq	%r13, 8(%rbx)
	movq	0(%r13), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$23, %ax
	sete	%sil
	movb	%sil, 20(%rbx)
	jne	.L2309
	movl	44(%rdx), %eax
	movl	%eax, 16(%rbx)
.L2309:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2310
	leaq	8(%r15), %rax
.L2311:
	movq	(%rax), %r14
	movl	$0, 32(%rbx)
	movq	%r14, 24(%rbx)
	movq	(%r14), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$23, %ax
	sete	%dil
	movb	%dil, 36(%rbx)
	jne	.L2312
	movl	44(%rdx), %eax
	movl	%eax, 32(%rbx)
.L2313:
	movq	(%r12), %rax
	movl	$-1, 40(%rbx)
	movb	$0, 44(%rbx)
	movzbl	18(%rax), %r12d
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2324
	movq	32(%r13), %rax
	movl	8(%rax), %eax
.L2324:
	cmpl	$1, %eax
	jle	.L2326
	leaq	-96(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	movq	0(%r13), %rax
	movzwl	16(%rax), %eax
	cmpl	$302, %eax
	je	.L2426
	cmpl	$310, %eax
	je	.L2427
.L2326:
	andl	$1, %r12d
	je	.L2307
	movq	24(%rbx), %r12
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2428
.L2344:
	cmpl	$1, %eax
	jle	.L2346
	leaq	-96(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	movq	(%r12), %rax
	movzwl	16(%rax), %eax
	cmpl	$302, %eax
	je	.L2429
	cmpl	$310, %eax
	je	.L2430
.L2346:
	movq	8(%rbx), %rax
	movq	(%rax), %rax
	movzwl	16(%rax), %eax
	subw	$306, %ax
	testw	$-3, %ax
	je	.L2307
	movq	24(%rbx), %rsi
	movq	(%rsi), %rax
	movzwl	16(%rax), %eax
	subw	$306, %ax
	testw	$-3, %ax
	je	.L2431
	.p2align 4,,10
	.p2align 3
.L2307:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2432
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2312:
	.cfi_restore_state
	andl	$1, %ecx
	je	.L2313
	testb	%sil, %sil
	je	.L2313
	movdqu	8(%rbx), %xmm0
	movq	8(%rbx), %r13
	movb	%dil, 20(%rbx)
	movq	%r14, 8(%rbx)
	movq	32(%r12), %rdi
	movaps	%xmm0, -96(%rbp)
	movl	-88(%rbp), %eax
	movl	$0, 16(%rbx)
	movl	%eax, 32(%rbx)
	movzbl	-84(%rbp), %eax
	movq	%r13, 24(%rbx)
	movb	%al, 36(%rbx)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2314
	movq	%r12, %rsi
	cmpq	%rdi, %r14
	je	.L2371
.L2315:
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	je	.L2318
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2318:
	movq	%r14, (%r15)
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	(%rbx), %rsi
	movq	24(%rbx), %r13
	movzbl	23(%rsi), %eax
	leaq	32(%rsi), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2372
.L2371:
	movq	8(%r15), %rdi
	addq	$8, %r15
	cmpq	%rdi, %r13
	je	.L2418
.L2320:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L2321
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2321:
	movq	%r13, (%r15)
	testq	%r13, %r13
	je	.L2418
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2418:
	movq	8(%rbx), %r13
	jmp	.L2313
	.p2align 4,,10
	.p2align 3
.L2310:
	movq	32(%r12), %rax
	addq	$24, %rax
	jmp	.L2311
	.p2align 4,,10
	.p2align 3
.L2426:
	cmpb	$0, -60(%rbp)
	je	.L2326
	movl	-64(%rbp), %edx
	cmpl	$3, %edx
	ja	.L2326
	xorl	%eax, %eax
.L2337:
	movl	%edx, 40(%rbx)
	movb	%al, 44(%rbx)
	jmp	.L2307
	.p2align 4,,10
	.p2align 3
.L2428:
	movq	32(%r12), %rax
	movl	8(%rax), %eax
	jmp	.L2344
	.p2align 4,,10
	.p2align 3
.L2427:
	movzbl	-60(%rbp), %eax
	testb	%al, %al
	je	.L2326
	movl	-64(%rbp), %edx
	cmpl	$1, %edx
	je	.L2333
	cmpl	$2, %edx
	je	.L2334
	cmpl	$4, %edx
	je	.L2335
	cmpl	$8, %edx
	je	.L2433
	cmpl	$3, %edx
	je	.L2419
	cmpl	$5, %edx
	je	.L2420
	cmpl	$9, %edx
	jne	.L2326
	movl	$3, %edx
	jmp	.L2337
	.p2align 4,,10
	.p2align 3
.L2431:
	movdqu	8(%rbx), %xmm2
	movq	8(%rbx), %rax
	movq	%rsi, 8(%rbx)
	movl	32(%rbx), %edx
	movq	(%rbx), %rdi
	movaps	%xmm2, -96(%rbp)
	movq	%rax, 24(%rbx)
	movl	-88(%rbp), %eax
	movl	%edx, 16(%rbx)
	movzbl	36(%rbx), %edx
	movl	%eax, 32(%rbx)
	movzbl	-84(%rbp), %eax
	movb	%dl, 20(%rbx)
	movb	%al, 36(%rbx)
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	24(%rbx), %rsi
	movq	(%rbx), %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	jmp	.L2307
	.p2align 4,,10
	.p2align 3
.L2333:
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L2337
	.p2align 4,,10
	.p2align 3
.L2372:
	movq	(%r15), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L2418
	leaq	24(%rsi), %r15
	jmp	.L2320
	.p2align 4,,10
	.p2align 3
.L2429:
	cmpb	$0, -60(%rbp)
	je	.L2346
	movl	-64(%rbp), %edx
	cmpl	$3, %edx
	ja	.L2346
	xorl	%eax, %eax
.L2360:
	movdqu	8(%rbx), %xmm1
	movq	(%rbx), %rsi
	movb	%al, 44(%rbx)
	movl	32(%rbx), %eax
	movq	8(%rbx), %r13
	movl	%edx, 40(%rbx)
	movaps	%xmm1, -96(%rbp)
	movq	24(%rbx), %r15
	movq	32(%rsi), %rdi
	leaq	32(%rsi), %r14
	movl	%eax, 16(%rbx)
	movzbl	36(%rbx), %eax
	movq	%r15, 8(%rbx)
	movb	%al, 20(%rbx)
	movl	-88(%rbp), %eax
	movq	%r13, 24(%rbx)
	movl	%eax, 32(%rbx)
	movzbl	-84(%rbp), %eax
	movb	%al, 36(%rbx)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2361
	cmpq	%r15, %rdi
	je	.L2374
.L2362:
	leaq	-24(%rsi), %r12
	testq	%rdi, %rdi
	je	.L2365
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2365:
	movq	%r15, (%r14)
	testq	%r15, %r15
	je	.L2366
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2366:
	movq	(%rbx), %rsi
	movq	24(%rbx), %r13
	movzbl	23(%rsi), %eax
	leaq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2374
	movq	32(%rsi), %rsi
	leaq	16(%rsi), %r14
.L2374:
	movq	8(%r14), %rdi
	cmpq	%rdi, %r13
	je	.L2307
	addq	$8, %r14
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L2369
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2369:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L2307
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L2307
	.p2align 4,,10
	.p2align 3
.L2430:
	movzbl	-60(%rbp), %eax
	testb	%al, %al
	je	.L2346
	movl	-64(%rbp), %edx
	cmpl	$1, %edx
	je	.L2353
	cmpl	$2, %edx
	je	.L2354
	cmpl	$4, %edx
	je	.L2355
	cmpl	$8, %edx
	je	.L2356
	cmpl	$3, %edx
	je	.L2422
	cmpl	$5, %edx
	je	.L2423
	cmpl	$9, %edx
	jne	.L2346
.L2421:
	movl	$3, %edx
	jmp	.L2360
	.p2align 4,,10
	.p2align 3
.L2314:
	movq	16(%rdi), %rax
	leaq	16(%rdi), %rdx
	cmpq	%rax, %r14
	je	.L2372
	movq	%rdi, %rsi
	movq	%rdx, %r15
	movq	%rax, %rdi
	jmp	.L2315
	.p2align 4,,10
	.p2align 3
.L2361:
	movq	16(%rdi), %rax
	leaq	16(%rdi), %r14
	movq	%rdi, %rsi
	cmpq	%rax, %r15
	je	.L2374
	movq	%rax, %rdi
	jmp	.L2362
	.p2align 4,,10
	.p2align 3
.L2334:
	xorl	%eax, %eax
.L2419:
	movl	$1, %edx
	jmp	.L2337
.L2335:
	xorl	%eax, %eax
.L2420:
	movl	$2, %edx
	jmp	.L2337
.L2433:
	xorl	%eax, %eax
	movl	$3, %edx
	jmp	.L2337
.L2353:
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L2360
.L2354:
	xorl	%eax, %eax
.L2422:
	movl	$1, %edx
	jmp	.L2360
.L2355:
	xorl	%eax, %eax
.L2423:
	movl	$2, %edx
	jmp	.L2360
.L2356:
	xorl	%eax, %eax
	jmp	.L2421
.L2432:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20101:
	.size	_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_306ELS6_308ELS6_310ELS6_302EEC2EPNS1_4NodeE, .-_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_306ELS6_308ELS6_310ELS6_302EEC2EPNS1_4NodeE
	.weak	_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_306ELS6_308ELS6_310ELS6_302EEC1EPNS1_4NodeE
	.set	_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_306ELS6_308ELS6_310ELS6_302EEC1EPNS1_4NodeE,_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_306ELS6_308ELS6_310ELS6_302EEC2EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE10InitializeEPNS1_4NodeENS_4base5FlagsINS1_13AddressOptionEhEE,"axG",@progbits,_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE10InitializeEPNS1_4NodeENS_4base5FlagsINS1_13AddressOptionEhEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE10InitializeEPNS1_4NodeENS_4base5FlagsINS1_13AddressOptionEhEE
	.type	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE10InitializeEPNS1_4NodeENS_4base5FlagsINS1_13AddressOptionEhEE, @function
_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE10InitializeEPNS1_4NodeENS_4base5FlagsINS1_13AddressOptionEhEE:
.LFB19599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L2435
	testb	$14, %al
	je	.L2434
	movq	%rsi, -160(%rbp)
	movl	%r14d, %r9d
	movq	32(%rsi), %r8
	leaq	32(%rsi), %rax
	andl	$1, %r9d
.L2556:
	movq	%r8, -152(%rbp)
	movl	$0, -144(%rbp)
	movq	(%r8), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$23, %dx
	sete	-140(%rbp)
	jne	.L2502
	movl	44(%rcx), %ecx
	movl	%ecx, -144(%rbp)
.L2502:
	movq	8(%rax), %rsi
	movl	$0, -128(%rbp)
	addq	$8, %rax
	movq	%rsi, -136(%rbp)
	movq	(%rsi), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$23, %ax
	sete	%dil
	movb	%dil, -124(%rbp)
	jne	.L2441
	movl	44(%rcx), %eax
	movl	%eax, -128(%rbp)
	testb	%r9b, %r9b
	je	.L2443
.L2444:
	movq	-152(%rbp), %rsi
	movl	$1, %edx
	movb	$0, -116(%rbp)
	leaq	-176(%rbp), %rdi
	movl	$-1, -120(%rbp)
	call	_ZN2v88internal8compiler12ScaleMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_310ELS6_302EEC1EPNS1_4NodeEb
	movl	-176(%rbp), %r15d
	cmpl	$-1, %r15d
	je	.L2557
.L2496:
	movzbl	-172(%rbp), %eax
	movl	%r15d, -120(%rbp)
	movb	%al, -116(%rbp)
.L2445:
	movq	-152(%rbp), %r13
	movq	-136(%rbp), %rdx
.L2448:
	cmpl	$-1, %r15d
	je	.L2450
.L2503:
	movq	%r13, %rdi
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE24OwnedByAddressingOperandEPNS1_4NodeE
	movq	-184(%rbp), %rdx
	testb	%al, %al
	je	.L2450
	movzbl	23(%r13), %eax
	movq	32(%r13), %r9
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2451
	movq	16(%r9), %r9
.L2451:
	movq	(%rdx), %rcx
	movzbl	-116(%rbp), %r8d
	movzwl	16(%rcx), %eax
	cmpw	$308, %ax
	je	.L2558
.L2455:
	cmpw	$306, %ax
	je	.L2559
	cmpb	$0, -124(%rbp)
	je	.L2465
	movq	%r13, %rsi
	xorl	%edi, %edi
	xorl	%r13d, %r13d
	jmp	.L2462
.L2487:
	movq	%r13, %rax
	movq	%rdx, %r13
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2475:
	andl	$2, %r14d
	je	.L2519
	movq	%rdx, %rsi
	xorl	%edi, %edi
	xorl	%edx, %edx
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L2495:
	movq	%r13, %xmm0
	movq	%rdx, %xmm1
	movl	%edi, 40(%rbx)
	movq	%rsi, 8(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movl	%r15d, 16(%rbx)
	movb	$1, (%rbx)
	movups	%xmm0, 24(%rbx)
.L2434:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2560
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2441:
	.cfi_restore_state
	testb	%r9b, %r9b
	jne	.L2561
.L2443:
	leaq	-176(%rbp), %rdi
	movl	$1, %edx
	movq	%r8, %rsi
	movl	$-1, -120(%rbp)
	movb	$0, -116(%rbp)
	call	_ZN2v88internal8compiler12ScaleMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_310ELS6_302EEC1EPNS1_4NodeEb
	movl	-176(%rbp), %r15d
	cmpl	$-1, %r15d
	jne	.L2496
	movl	-120(%rbp), %r15d
	jmp	.L2445
	.p2align 4,,10
	.p2align 3
.L2450:
	movq	0(%r13), %rax
	movzwl	16(%rax), %eax
	cmpw	$308, %ax
	je	.L2562
.L2454:
	cmpw	$306, %ax
	je	.L2563
.L2466:
	cmpb	$0, -124(%rbp)
	je	.L2475
	xorl	%r15d, %r15d
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	xorl	%r9d, %r9d
.L2458:
	testq	%rdx, %rdx
	je	.L2489
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %eax
.L2462:
	cmpw	$23, %ax
	jne	.L2564
	movslq	44(%rcx), %rax
.L2493:
	testq	%rax, %rax
	movl	$0, %eax
	cmove	%rax, %rdx
.L2489:
	testb	%r8b, %r8b
	je	.L2494
	testq	%r13, %r13
	je	.L2565
.L2555:
	xorl	%r15d, %r15d
	jmp	.L2495
	.p2align 4,,10
	.p2align 3
.L2435:
	movq	32(%rsi), %rdx
	cmpl	$1, 8(%rdx)
	jle	.L2434
	movl	%r14d, %r9d
	movq	%rsi, -160(%rbp)
	leaq	16(%rdx), %rax
	movq	16(%rdx), %r8
	andl	$1, %r9d
	jmp	.L2556
	.p2align 4,,10
	.p2align 3
.L2561:
	cmpw	$23, %dx
	jne	.L2444
	movdqu	-152(%rbp), %xmm2
	movq	-152(%rbp), %rax
	movb	%dil, -140(%rbp)
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	movaps	%xmm2, -112(%rbp)
	movq	%rax, -136(%rbp)
	movl	-104(%rbp), %eax
	movl	$0, -144(%rbp)
	movl	%eax, -128(%rbp)
	movzbl	-100(%rbp), %eax
	movb	%al, -124(%rbp)
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	-136(%rbp), %rsi
	movq	-160(%rbp), %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	jmp	.L2444
	.p2align 4,,10
	.p2align 3
.L2557:
	movq	-136(%rbp), %rsi
	leaq	-168(%rbp), %rdi
	movl	$1, %edx
	call	_ZN2v88internal8compiler12ScaleMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_310ELS6_302EEC1EPNS1_4NodeEb
	movl	-168(%rbp), %eax
	cmpl	$-1, %eax
	jne	.L2566
	movq	-152(%rbp), %r13
	movq	-136(%rbp), %rdx
	movq	0(%r13), %rax
	movzwl	16(%rax), %eax
	leal	-306(%rax), %ecx
	testw	$-3, %cx
	jne	.L2447
.L2552:
	movl	-120(%rbp), %r15d
	jmp	.L2448
	.p2align 4,,10
	.p2align 3
.L2564:
	cmpw	$24, %ax
	jne	.L2492
	movq	48(%rcx), %rax
	jmp	.L2493
	.p2align 4,,10
	.p2align 3
.L2565:
	movq	%r9, %r13
.L2494:
	andl	$2, %r14d
	jne	.L2516
	testl	%r15d, %r15d
	jne	.L2555
	movq	%rdx, %rax
	movq	%r9, %rdx
	jmp	.L2504
	.p2align 4,,10
	.p2align 3
.L2516:
	movq	%r9, %rsi
	jmp	.L2495
	.p2align 4,,10
	.p2align 3
.L2562:
	movq	%r13, %rdi
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE24OwnedByAddressingOperandEPNS1_4NodeE
	movq	-184(%rbp), %rdx
	testb	%al, %al
	je	.L2466
	leaq	-112(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_306ELS6_308ELS6_310ELS6_302EEC1EPNS1_4NodeE
	cmpb	$0, -76(%rbp)
	movq	-184(%rbp), %rdx
	je	.L2467
	movl	-72(%rbp), %r15d
	movq	-104(%rbp), %r9
	movq	-88(%rbp), %rcx
	cmpl	$-1, %r15d
	je	.L2553
	movq	24(%r9), %rsi
	testq	%rsi, %rsi
	je	.L2469
	movl	16(%rsi), %edi
	movl	%edi, %eax
	shrl	%eax
	andl	$1, %edi
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rsi,%rax,8), %rax
	jne	.L2470
	movq	(%rax), %rax
.L2470:
	cmpq	%r13, %rax
	je	.L2471
.L2553:
	movq	%rdx, %r13
	xorl	%r15d, %r15d
	movq	%rcx, %rdx
	movl	$1, %edi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	jmp	.L2458
	.p2align 4,,10
	.p2align 3
.L2563:
	movq	%r13, %rdi
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE24OwnedByAddressingOperandEPNS1_4NodeE
	movq	-184(%rbp), %rdx
	testb	%al, %al
	je	.L2466
	leaq	-112(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_306ELS6_308ELS6_310ELS6_302EEC1EPNS1_4NodeE
	movl	-72(%rbp), %r15d
	movq	-104(%rbp), %r9
	movq	-88(%rbp), %rax
	movq	-184(%rbp), %rdx
	cmpl	$-1, %r15d
	je	.L2476
	movq	24(%r9), %rcx
	movzbl	-76(%rbp), %r8d
	testq	%rcx, %rcx
	je	.L2477
	movl	16(%rcx), %r10d
	movl	%r10d, %esi
	shrl	%esi
	andl	$1, %r10d
	leaq	3(%rsi,%rsi,2), %rsi
	leaq	(%rcx,%rsi,8), %rdi
	jne	.L2478
	movq	(%rdi), %rdi
.L2478:
	cmpq	%r13, %rdi
	je	.L2567
.L2477:
	testb	%r8b, %r8b
	je	.L2568
	xorl	%r15d, %r15d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
.L2481:
	movq	%rdx, %r13
	xorl	%edi, %edi
	movq	%rax, %rdx
	jmp	.L2458
	.p2align 4,,10
	.p2align 3
.L2559:
	movq	%rdx, %rdi
	movb	%r8b, -193(%rbp)
	movq	%r9, -192(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE24OwnedByAddressingOperandEPNS1_4NodeE
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %r9
	testb	%al, %al
	movzbl	-193(%rbp), %r8d
	jne	.L2569
.L2456:
	cmpb	$0, -124(%rbp)
	jne	.L2492
.L2465:
	movq	%r13, %rsi
	xorl	%edi, %edi
	movq	%rdx, %r13
	xorl	%edx, %edx
	testb	%r8b, %r8b
	jne	.L2555
	jmp	.L2494
	.p2align 4,,10
	.p2align 3
.L2566:
	movl	%eax, -120(%rbp)
	movzbl	-164(%rbp), %eax
	leaq	-160(%rbp), %rdi
	movb	%al, -116(%rbp)
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_E10SwapInputsEv
	movl	-120(%rbp), %r15d
	jmp	.L2445
	.p2align 4,,10
	.p2align 3
.L2447:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %ecx
	subw	$306, %cx
	testw	$-3, %cx
	je	.L2570
	movl	-120(%rbp), %r15d
	cmpl	$-1, %r15d
	je	.L2454
	jmp	.L2503
	.p2align 4,,10
	.p2align 3
.L2519:
	xorl	%eax, %eax
	xorl	%edi, %edi
.L2504:
	movq	%rdx, %rsi
	xorl	%r15d, %r15d
	movq	%rax, %rdx
	jmp	.L2495
	.p2align 4,,10
	.p2align 3
.L2558:
	movq	%rdx, %rdi
	movb	%r8b, -193(%rbp)
	movq	%r9, -192(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE24OwnedByAddressingOperandEPNS1_4NodeE
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %r9
	testb	%al, %al
	movzbl	-193(%rbp), %r8d
	je	.L2456
	movq	%rdx, %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_306ELS6_308ELS6_310ELS6_302EEC1EPNS1_4NodeE
	cmpb	$0, -76(%rbp)
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %r9
	movzbl	-193(%rbp), %r8d
	je	.L2457
	movq	%r13, %rsi
	movq	-88(%rbp), %rdx
	movq	-104(%rbp), %r13
	movl	$1, %edi
	jmp	.L2458
	.p2align 4,,10
	.p2align 3
.L2570:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_E10SwapInputsEv
	movq	-152(%rbp), %r13
	movq	-136(%rbp), %rdx
	jmp	.L2552
	.p2align 4,,10
	.p2align 3
.L2467:
	movq	0(%r13), %rax
	movzwl	16(%rax), %eax
	jmp	.L2454
.L2568:
	cmpb	$0, -124(%rbp)
	je	.L2487
	movq	24(%r13), %rsi
	testq	%rsi, %rsi
	je	.L2512
	movl	16(%rsi), %edi
	movl	%edi, %ecx
	shrl	%ecx
	andl	$1, %edi
	leaq	3(%rcx,%rcx,2), %rcx
	leaq	(%rsi,%rcx,8), %rcx
	jne	.L2488
	movq	(%rcx), %rcx
.L2488:
	cmpq	%rcx, %r12
	je	.L2571
.L2514:
	movq	%rdx, %rax
	xorl	%r15d, %r15d
	movq	%r13, %rdx
	xorl	%esi, %esi
	xorl	%r9d, %r9d
	jmp	.L2481
.L2476:
	movzbl	-76(%rbp), %r8d
	jmp	.L2477
.L2457:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %eax
	jmp	.L2455
.L2569:
	movq	%rdx, %rsi
	leaq	-112(%rbp), %rdi
	movb	%r8b, -193(%rbp)
	movq	%r9, -192(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES7_EELS6_306ELS6_308ELS6_310ELS6_302EEC1EPNS1_4NodeE
	cmpb	$0, -76(%rbp)
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %r9
	movzbl	-193(%rbp), %r8d
	je	.L2465
	movq	%r13, %rsi
	movq	-88(%rbp), %rdx
	movq	-104(%rbp), %r13
	xorl	%edi, %edi
	jmp	.L2458
.L2469:
	movq	%rdx, %r13
	xorl	%r15d, %r15d
	movq	%rcx, %rdx
	movl	$1, %edi
	xorl	%r8d, %r8d
	jmp	.L2458
.L2471:
	cmpq	$0, (%rsi)
	jne	.L2553
	movzbl	23(%r9), %eax
	movq	32(%r9), %rsi
	leaq	32(%r9), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2473
	leaq	16(%rsi), %rdi
.L2473:
	movq	%r9, %rsi
	movq	%rdx, %r13
	movq	(%rdi), %r9
	movzbl	-68(%rbp), %r8d
	movq	%rcx, %rdx
	movl	$1, %edi
	jmp	.L2458
.L2567:
	movq	(%rcx), %rsi
	testq	%rsi, %rsi
	jne	.L2477
	testb	%r8b, %r8b
	je	.L2572
	movzbl	23(%r9), %ecx
	movq	32(%r9), %rsi
	leaq	32(%r9), %rdi
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L2483
	leaq	16(%rsi), %rdi
.L2483:
	movq	%r9, %rsi
	movzbl	-68(%rbp), %r8d
	movq	(%rdi), %r9
	jmp	.L2481
.L2512:
	movq	%rdx, %rax
	xorl	%r9d, %r9d
	movq	%r13, %rdx
	xorl	%r15d, %r15d
	jmp	.L2481
.L2571:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.L2514
	movq	%rax, %rcx
	xorl	%r15d, %r15d
	movq	%rdx, %rax
	movq	%rcx, %rdx
	jmp	.L2481
.L2560:
	call	__stack_chk_fail@PLT
.L2572:
	cmpb	$0, -124(%rbp)
	je	.L2487
	movq	24(%r13), %r10
	testq	%r10, %r10
	je	.L2509
	movl	16(%r10), %edi
	movl	%edi, %ecx
	shrl	%ecx
	addq	$1, %rcx
	imulq	$24, %rcx, %rcx
	addq	%r10, %rcx
	andb	$1, %dil
	jne	.L2485
	movq	(%rcx), %rcx
.L2485:
	cmpq	%rcx, %r12
	jne	.L2512
	cmpq	$0, (%r10)
	jne	.L2512
	movzbl	23(%r9), %ecx
	movq	32(%r9), %rsi
	leaq	32(%r9), %rdi
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L2486
	leaq	16(%rsi), %rdi
.L2486:
	movq	%rax, %rcx
	movq	%r9, %rsi
	movq	%rdx, %rax
	movzbl	-68(%rbp), %r8d
	movq	(%rdi), %r9
	movq	%rcx, %rdx
	jmp	.L2481
.L2492:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2509:
	movq	%rdx, %rax
	xorl	%r9d, %r9d
	movq	%rdi, %rdx
	xorl	%r15d, %r15d
	jmp	.L2481
	.cfi_endproc
.LFE19599:
	.size	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE10InitializeEPNS1_4NodeENS_4base5FlagsINS1_13AddressOptionEhEE, .-_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE10InitializeEPNS1_4NodeENS_4base5FlagsINS1_13AddressOptionEhEE
	.section	.text._ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeEb,"axG",@progbits,_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC5EPNS1_4NodeEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeEb
	.type	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeEb, @function
_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeEb:
.LFB20361:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movzbl	23(%rsi), %ecx
	leaq	32(%rsi), %rax
	movq	%rax, %r8
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L2574
	movq	32(%rsi), %rcx
	leaq	16(%rcx), %r8
.L2574:
	movq	(%r8), %rcx
	movq	$0, 16(%rdi)
	movb	$0, 24(%rdi)
	movq	%rcx, 8(%rdi)
	movq	(%rcx), %r8
	movzwl	16(%r8), %ecx
	cmpl	$23, %ecx
	je	.L2584
	cmpl	$24, %ecx
	je	.L2585
.L2576:
	movzbl	23(%rsi), %ecx
	addq	$8, %rax
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L2578
	movq	32(%rsi), %rax
	addq	$24, %rax
.L2578:
	movq	(%rax), %rax
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movq	%rax, 32(%rdi)
	movq	(%rax), %rcx
	movzwl	16(%rcx), %eax
	cmpl	$23, %eax
	je	.L2586
	cmpl	$24, %eax
	je	.L2581
	testb	%dl, %dl
	je	.L2573
	cmpb	$0, 24(%rdi)
	je	.L2587
	jmp	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_E10SwapInputsEv
	.p2align 4,,10
	.p2align 3
.L2581:
	movq	48(%rcx), %rax
	movb	$1, 48(%rdi)
	movq	%rax, 40(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L2585:
	movq	48(%r8), %rcx
	movb	$1, 24(%rdi)
	movq	%rcx, 16(%rdi)
	jmp	.L2576
	.p2align 4,,10
	.p2align 3
.L2573:
	ret
	.p2align 4,,10
	.p2align 3
.L2584:
	movslq	44(%r8), %rcx
	movb	$1, 24(%rdi)
	movq	%rcx, 16(%rdi)
	jmp	.L2576
	.p2align 4,,10
	.p2align 3
.L2586:
	movslq	44(%rcx), %rax
	movb	$1, 48(%rdi)
	movq	%rax, 40(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L2587:
	ret
	.cfi_endproc
.LFE20361:
	.size	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeEb, .-_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeEb
	.weak	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeEb
	.set	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeEb,_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeEb
	.section	.text._ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES7_EELS6_325ELS6_327ELS6_329ELS6_321EEC2EPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES7_EELS6_325ELS6_327ELS6_329ELS6_321EEC5EPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES7_EELS6_325ELS6_327ELS6_329ELS6_321EEC2EPNS1_4NodeE
	.type	_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES7_EELS6_325ELS6_327ELS6_329ELS6_321EEC2EPNS1_4NodeE, @function
_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES7_EELS6_325ELS6_327ELS6_329ELS6_321EEC2EPNS1_4NodeE:
.LFB20083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movzbl	18(%rax), %edx
	andl	$1, %edx
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeEb
	movq	(%rbx), %rax
	movq	8(%r12), %rbx
	movl	$-1, 56(%r12)
	movb	$0, 60(%r12)
	movzbl	18(%rax), %r13d
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2590
	movq	32(%rbx), %rax
	movl	8(%rax), %eax
.L2590:
	cmpl	$1, %eax
	jle	.L2592
	leaq	-96(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	movq	(%rbx), %rax
	movzwl	16(%rax), %eax
	cmpl	$321, %eax
	je	.L2669
	cmpl	$329, %eax
	je	.L2670
.L2592:
	andl	$1, %r13d
	je	.L2588
	movq	32(%r12), %rbx
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2671
.L2609:
	cmpl	$1, %eax
	jle	.L2611
	leaq	-96(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	movq	(%rbx), %rax
	movzwl	16(%rax), %eax
	cmpl	$321, %eax
	je	.L2672
	cmpl	$329, %eax
	je	.L2673
.L2611:
	movq	8(%r12), %rax
	movq	(%rax), %rax
	movzwl	16(%rax), %eax
	andl	$-3, %eax
	cmpw	$325, %ax
	je	.L2588
	movq	32(%r12), %rax
	movq	(%rax), %rax
	movzwl	16(%rax), %eax
	andl	$-3, %eax
	cmpw	$325, %ax
	je	.L2674
	.p2align 4,,10
	.p2align 3
.L2588:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2675
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2669:
	.cfi_restore_state
	cmpb	$0, -48(%rbp)
	je	.L2592
	movq	-56(%rbp), %rdx
	xorl	%eax, %eax
	cmpq	$3, %rdx
	ja	.L2592
.L2596:
	movl	%edx, 56(%r12)
	movb	%al, 60(%r12)
	jmp	.L2588
	.p2align 4,,10
	.p2align 3
.L2671:
	movq	32(%rbx), %rax
	movl	8(%rax), %eax
	jmp	.L2609
	.p2align 4,,10
	.p2align 3
.L2670:
	movzbl	-48(%rbp), %eax
	testb	%al, %al
	je	.L2592
	movq	-56(%rbp), %rdx
	cmpq	$1, %rdx
	je	.L2599
	cmpq	$2, %rdx
	je	.L2600
	cmpq	$4, %rdx
	je	.L2601
	cmpq	$8, %rdx
	je	.L2676
	cmpq	$3, %rdx
	je	.L2665
	cmpq	$5, %rdx
	je	.L2664
	cmpq	$9, %rdx
	jne	.L2592
	movl	$3, %edx
	jmp	.L2596
	.p2align 4,,10
	.p2align 3
.L2674:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_E10SwapInputsEv
	jmp	.L2588
	.p2align 4,,10
	.p2align 3
.L2599:
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L2596
	.p2align 4,,10
	.p2align 3
.L2672:
	cmpb	$0, -48(%rbp)
	je	.L2611
	movq	-56(%rbp), %rax
	cmpq	$3, %rax
	ja	.L2611
.L2666:
	xorl	%edx, %edx
.L2615:
	movdqu	8(%r12), %xmm0
	movq	(%r12), %rsi
	movl	%eax, 56(%r12)
	movq	24(%r12), %rax
	movq	32(%r12), %r14
	movb	%dl, 60(%r12)
	movdqu	32(%r12), %xmm1
	movzbl	48(%r12), %edx
	movaps	%xmm0, -96(%rbp)
	leaq	32(%rsi), %rbx
	movq	%rax, -80(%rbp)
	movb	%dl, 24(%r12)
	movb	%al, 48(%r12)
	movups	%xmm1, 8(%r12)
	movups	%xmm0, 32(%r12)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2625
	movq	32(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L2628
.L2626:
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	je	.L2629
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2629:
	movq	%r14, (%rbx)
	testq	%r14, %r14
	je	.L2630
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2630:
	movq	(%r12), %rsi
	movq	32(%r12), %r13
	movzbl	23(%rsi), %eax
	leaq	32(%rsi), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2636
	movq	32(%rsi), %rsi
	leaq	16(%rsi), %rbx
.L2636:
	movq	8(%rbx), %rdi
	cmpq	%rdi, %r13
	je	.L2588
	addq	$8, %rbx
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L2633
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L2633:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L2588
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L2588
	.p2align 4,,10
	.p2align 3
.L2673:
	movzbl	-48(%rbp), %edx
	testb	%dl, %dl
	je	.L2611
	movq	-56(%rbp), %rax
	cmpq	$1, %rax
	je	.L2618
	cmpq	$2, %rax
	je	.L2619
	cmpq	$4, %rax
	je	.L2620
	cmpq	$8, %rax
	je	.L2621
	cmpq	$3, %rax
	je	.L2622
	cmpq	$5, %rax
	je	.L2623
	cmpq	$9, %rax
	jne	.L2611
	movl	$3, %eax
	jmp	.L2615
	.p2align 4,,10
	.p2align 3
.L2625:
	movq	32(%rsi), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rbx
	cmpq	%rdi, %r14
	jne	.L2626
.L2628:
	movq	32(%r12), %r13
	jmp	.L2636
	.p2align 4,,10
	.p2align 3
.L2600:
	xorl	%eax, %eax
.L2665:
	movl	$1, %edx
	jmp	.L2596
.L2601:
	xorl	%eax, %eax
.L2664:
	movl	$2, %edx
	jmp	.L2596
.L2676:
	xorl	%eax, %eax
	movl	$3, %edx
	jmp	.L2596
.L2618:
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L2615
.L2619:
	movl	$1, %eax
	xorl	%edx, %edx
	jmp	.L2615
.L2620:
	movl	$2, %eax
	xorl	%edx, %edx
	jmp	.L2615
.L2621:
	movl	$3, %eax
	jmp	.L2666
.L2622:
	movl	$1, %eax
	jmp	.L2615
.L2623:
	movl	$2, %eax
	jmp	.L2615
.L2675:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20083:
	.size	_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES7_EELS6_325ELS6_327ELS6_329ELS6_321EEC2EPNS1_4NodeE, .-_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES7_EELS6_325ELS6_327ELS6_329ELS6_321EEC2EPNS1_4NodeE
	.weak	_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES7_EELS6_325ELS6_327ELS6_329ELS6_321EEC1EPNS1_4NodeE
	.set	_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES7_EELS6_325ELS6_327ELS6_329ELS6_321EEC1EPNS1_4NodeE,_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES7_EELS6_325ELS6_327ELS6_329ELS6_321EEC2EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE10InitializeEPNS1_4NodeENS_4base5FlagsINS1_13AddressOptionEhEE,"axG",@progbits,_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE10InitializeEPNS1_4NodeENS_4base5FlagsINS1_13AddressOptionEhEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE10InitializeEPNS1_4NodeENS_4base5FlagsINS1_13AddressOptionEhEE
	.type	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE10InitializeEPNS1_4NodeENS_4base5FlagsINS1_13AddressOptionEhEE, @function
_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE10InitializeEPNS1_4NodeENS_4base5FlagsINS1_13AddressOptionEhEE:
.LFB19579:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2679
	movq	32(%rsi), %rax
	movl	8(%rax), %eax
.L2679:
	cmpl	$1, %eax
	jle	.L2677
	movl	%r12d, %edx
	leaq	-192(%rbp), %r15
	movq	%r13, %rsi
	movl	%r12d, %r14d
	andl	$1, %edx
	movq	%r15, %rdi
	andl	$1, %r14d
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC2EPNS1_4NodeEb
	movq	-184(%rbp), %rdx
	movl	$-1, -136(%rbp)
	movb	$0, -132(%rbp)
	movzbl	23(%rdx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2818
	cmpl	$1, %eax
	jle	.L2685
.L2827:
	movq	%rdx, %rsi
	leaq	-128(%rbp), %rdi
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	movq	-200(%rbp), %rdx
	movq	(%rdx), %rax
	movzwl	16(%rax), %eax
	cmpl	$321, %eax
	je	.L2819
	cmpl	$329, %eax
	je	.L2820
.L2685:
	movq	-160(%rbp), %rcx
	testb	%r14b, %r14b
	jne	.L2821
	movl	-136(%rbp), %r15d
.L2689:
	movq	-184(%rbp), %r14
.L2720:
	cmpl	$-1, %r15d
	jne	.L2696
.L2722:
	movq	(%r14), %rax
	movzwl	16(%rax), %eax
	cmpw	$327, %ax
	je	.L2822
.L2726:
	cmpw	$325, %ax
	je	.L2823
.L2738:
	cmpb	$0, -144(%rbp)
	je	.L2747
	xorl	%r15d, %r15d
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	xorl	%r9d, %r9d
.L2730:
	testq	%rcx, %rcx
	je	.L2761
	movq	(%rcx), %rdx
	movzwl	16(%rdx), %eax
.L2734:
	cmpw	$23, %ax
	jne	.L2824
	movslq	44(%rdx), %rax
.L2765:
	testq	%rax, %rax
	movl	$0, %eax
	cmove	%rax, %rcx
.L2761:
	testb	%r8b, %r8b
	je	.L2766
	testq	%r14, %r14
	je	.L2825
.L2817:
	xorl	%r15d, %r15d
.L2767:
	movq	%r14, %xmm0
	movq	%rcx, %xmm1
	movl	%edi, 40(%rbx)
	movq	%rsi, 8(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movl	%r15d, 16(%rbx)
	movb	$1, (%rbx)
	movups	%xmm0, 24(%rbx)
.L2677:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2826
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2818:
	.cfi_restore_state
	movq	32(%rdx), %rax
	movl	8(%rax), %eax
	cmpl	$1, %eax
	jg	.L2827
	jmp	.L2685
	.p2align 4,,10
	.p2align 3
.L2821:
	movzbl	23(%rcx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2702
	movq	32(%rcx), %rax
	movl	8(%rax), %eax
.L2702:
	cmpl	$1, %eax
	jle	.L2704
	movq	%rcx, %rsi
	leaq	-128(%rbp), %rdi
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	movq	-200(%rbp), %rcx
	movq	(%rcx), %rax
	movzwl	16(%rax), %eax
	cmpl	$321, %eax
	je	.L2828
	cmpl	$329, %eax
	je	.L2829
.L2718:
	movq	-160(%rbp), %rcx
.L2704:
	movq	-184(%rbp), %r14
	movq	(%r14), %rax
	movzwl	16(%rax), %eax
	movl	%eax, %edx
	andl	$-3, %edx
	cmpw	$325, %dx
	jne	.L2719
.L2813:
	movl	-136(%rbp), %r15d
	jmp	.L2720
	.p2align 4,,10
	.p2align 3
.L2820:
	cmpb	$0, -80(%rbp)
	je	.L2685
	movq	-88(%rbp), %rax
	cmpq	$1, %rax
	je	.L2692
	cmpq	$2, %rax
	je	.L2693
	cmpq	$4, %rax
	je	.L2694
	cmpq	$8, %rax
	je	.L2830
	cmpq	$3, %rax
	je	.L2697
	cmpq	$5, %rax
	je	.L2698
	cmpq	$9, %rax
	jne	.L2685
	movl	$3, -136(%rbp)
	movq	-160(%rbp), %rcx
	movl	$3, %r15d
	movb	$1, -132(%rbp)
	movq	-184(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L2696:
	movq	%r14, %rdi
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE24OwnedByAddressingOperandEPNS1_4NodeE
	movq	-200(%rbp), %rcx
	testb	%al, %al
	je	.L2722
	movzbl	23(%r14), %eax
	movq	32(%r14), %r9
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2723
	movq	16(%r9), %r9
.L2723:
	movq	(%rcx), %rdx
	movzbl	-132(%rbp), %r8d
	movzwl	16(%rdx), %eax
	cmpw	$327, %ax
	je	.L2831
.L2727:
	cmpw	$325, %ax
	je	.L2832
	cmpb	$0, -144(%rbp)
	je	.L2737
	movq	%r14, %rsi
	xorl	%edi, %edi
	xorl	%r14d, %r14d
	jmp	.L2734
	.p2align 4,,10
	.p2align 3
.L2824:
	cmpw	$24, %ax
	jne	.L2764
	movq	48(%rdx), %rax
	jmp	.L2765
	.p2align 4,,10
	.p2align 3
.L2825:
	movq	%r9, %r14
.L2766:
	andl	$2, %r12d
	jne	.L2780
	testl	%r15d, %r15d
	jne	.L2817
	movq	%rcx, %rax
	movq	%r9, %rcx
	jmp	.L2768
	.p2align 4,,10
	.p2align 3
.L2780:
	movq	%r9, %rsi
	jmp	.L2767
	.p2align 4,,10
	.p2align 3
.L2819:
	cmpb	$0, -80(%rbp)
	je	.L2685
	movq	-88(%rbp), %rax
	cmpq	$3, %rax
	ja	.L2685
	movl	%eax, -136(%rbp)
	movq	-160(%rbp), %rcx
	movl	%eax, %r15d
	movb	$0, -132(%rbp)
	jmp	.L2689
.L2759:
	movq	%r14, %rax
	movq	%rcx, %r14
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L2747:
	andl	$2, %r12d
	je	.L2783
	movq	%rcx, %rsi
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	jmp	.L2767
	.p2align 4,,10
	.p2align 3
.L2822:
	movq	%r14, %rdi
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE24OwnedByAddressingOperandEPNS1_4NodeE
	movq	-200(%rbp), %rcx
	testb	%al, %al
	je	.L2738
	leaq	-128(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES7_EELS6_325ELS6_327ELS6_329ELS6_321EEC1EPNS1_4NodeE
	cmpb	$0, -80(%rbp)
	movq	-200(%rbp), %rcx
	je	.L2739
	movl	-72(%rbp), %r15d
	movq	-120(%rbp), %r9
	movq	-96(%rbp), %rdx
	cmpl	$-1, %r15d
	je	.L2815
	movq	24(%r9), %rsi
	testq	%rsi, %rsi
	je	.L2741
	movl	16(%rsi), %edi
	movl	%edi, %eax
	shrl	%eax
	andl	$1, %edi
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rsi,%rax,8), %rax
	jne	.L2742
	movq	(%rax), %rax
.L2742:
	cmpq	%r14, %rax
	je	.L2743
.L2815:
	movq	%rcx, %r14
	xorl	%r15d, %r15d
	movq	%rdx, %rcx
	movl	$1, %edi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	jmp	.L2730
	.p2align 4,,10
	.p2align 3
.L2829:
	movzbl	-80(%rbp), %eax
	testb	%al, %al
	je	.L2718
	movq	-88(%rbp), %rdx
	cmpq	$1, %rdx
	je	.L2711
	cmpq	$2, %rdx
	je	.L2712
	cmpq	$4, %rdx
	je	.L2713
	cmpq	$8, %rdx
	je	.L2714
	cmpq	$3, %rdx
	je	.L2812
	cmpq	$5, %rdx
	je	.L2810
	cmpq	$9, %rdx
	jne	.L2718
.L2811:
	movl	$3, %edx
	.p2align 4,,10
	.p2align 3
.L2708:
	movq	%r15, %rdi
	movl	%edx, -136(%rbp)
	movb	%al, -132(%rbp)
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_E10SwapInputsEv
	movl	-136(%rbp), %r15d
	movq	-160(%rbp), %rcx
	jmp	.L2689
	.p2align 4,,10
	.p2align 3
.L2832:
	movq	%rcx, %rdi
	movq	%r9, -216(%rbp)
	movb	%r8b, -201(%rbp)
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE24OwnedByAddressingOperandEPNS1_4NodeE
	movq	-200(%rbp), %rcx
	movzbl	-201(%rbp), %r8d
	testb	%al, %al
	movq	-216(%rbp), %r9
	jne	.L2833
.L2728:
	cmpb	$0, -144(%rbp)
	jne	.L2764
.L2737:
	movq	%r14, %rsi
	xorl	%edi, %edi
	movq	%rcx, %r14
	xorl	%ecx, %ecx
	testb	%r8b, %r8b
	jne	.L2817
	jmp	.L2766
	.p2align 4,,10
	.p2align 3
.L2823:
	movq	%r14, %rdi
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE24OwnedByAddressingOperandEPNS1_4NodeE
	movq	-200(%rbp), %rcx
	testb	%al, %al
	je	.L2738
	leaq	-128(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES7_EELS6_325ELS6_327ELS6_329ELS6_321EEC1EPNS1_4NodeE
	movl	-72(%rbp), %r15d
	movq	-120(%rbp), %r9
	movq	-96(%rbp), %rax
	movq	-200(%rbp), %rcx
	cmpl	$-1, %r15d
	je	.L2748
	movq	24(%r9), %rdx
	movzbl	-80(%rbp), %r8d
	testq	%rdx, %rdx
	je	.L2749
	movl	16(%rdx), %r10d
	movl	%r10d, %esi
	shrl	%esi
	andl	$1, %r10d
	leaq	3(%rsi,%rsi,2), %rsi
	leaq	(%rdx,%rsi,8), %rdi
	jne	.L2750
	movq	(%rdi), %rdi
.L2750:
	cmpq	%r14, %rdi
	je	.L2834
.L2749:
	testb	%r8b, %r8b
	je	.L2835
	xorl	%r15d, %r15d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
.L2753:
	movq	%rcx, %r14
	xorl	%edi, %edi
	movq	%rax, %rcx
	jmp	.L2730
	.p2align 4,,10
	.p2align 3
.L2719:
	movq	(%rcx), %rdx
	movzwl	16(%rdx), %edx
	andl	$-3, %edx
	cmpw	$325, %dx
	je	.L2836
	movl	-136(%rbp), %r15d
	cmpl	$-1, %r15d
	je	.L2726
	jmp	.L2696
	.p2align 4,,10
	.p2align 3
.L2783:
	xorl	%eax, %eax
	xorl	%edi, %edi
.L2768:
	movq	%rcx, %rsi
	xorl	%r15d, %r15d
	movq	%rax, %rcx
	jmp	.L2767
	.p2align 4,,10
	.p2align 3
.L2831:
	movq	%rcx, %rdi
	movq	%r9, -216(%rbp)
	movb	%r8b, -201(%rbp)
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE24OwnedByAddressingOperandEPNS1_4NodeE
	movq	-200(%rbp), %rcx
	movzbl	-201(%rbp), %r8d
	testb	%al, %al
	movq	-216(%rbp), %r9
	je	.L2728
	movq	%rcx, %rsi
	leaq	-128(%rbp), %rdi
	call	_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES7_EELS6_325ELS6_327ELS6_329ELS6_321EEC1EPNS1_4NodeE
	cmpb	$0, -80(%rbp)
	movq	-200(%rbp), %rcx
	movzbl	-201(%rbp), %r8d
	movq	-216(%rbp), %r9
	je	.L2729
	movq	%r14, %rsi
	movq	-96(%rbp), %rcx
	movq	-120(%rbp), %r14
	movl	$1, %edi
	jmp	.L2730
	.p2align 4,,10
	.p2align 3
.L2828:
	cmpb	$0, -80(%rbp)
	je	.L2718
	movq	-88(%rbp), %rdx
	xorl	%eax, %eax
	cmpq	$3, %rdx
	jbe	.L2708
	jmp	.L2718
	.p2align 4,,10
	.p2align 3
.L2836:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_E10SwapInputsEv
	movq	-184(%rbp), %r14
	movq	-160(%rbp), %rcx
	jmp	.L2813
	.p2align 4,,10
	.p2align 3
.L2739:
	movq	(%r14), %rax
	movzwl	16(%rax), %eax
	jmp	.L2726
	.p2align 4,,10
	.p2align 3
.L2692:
	movl	$0, -136(%rbp)
	movq	-160(%rbp), %rcx
	xorl	%r15d, %r15d
	movb	$0, -132(%rbp)
	movq	-184(%rbp), %r14
	jmp	.L2696
.L2693:
	movl	$1, -136(%rbp)
	movq	-160(%rbp), %rcx
	movl	$1, %r15d
	movb	$0, -132(%rbp)
	movq	-184(%rbp), %r14
	jmp	.L2696
.L2835:
	cmpb	$0, -144(%rbp)
	je	.L2759
	movq	24(%r14), %rsi
	testq	%rsi, %rsi
	je	.L2776
	movl	16(%rsi), %edi
	movl	%edi, %edx
	shrl	%edx
	andl	$1, %edi
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	jne	.L2760
	movq	(%rdx), %rdx
.L2760:
	cmpq	%rdx, %r13
	je	.L2837
.L2778:
	movq	%rcx, %rax
	xorl	%r15d, %r15d
	movq	%r14, %rcx
	xorl	%esi, %esi
	xorl	%r9d, %r9d
	jmp	.L2753
.L2694:
	movl	$2, -136(%rbp)
	movq	-160(%rbp), %rcx
	movl	$2, %r15d
	movb	$0, -132(%rbp)
	movq	-184(%rbp), %r14
	jmp	.L2696
.L2748:
	movzbl	-80(%rbp), %r8d
	jmp	.L2749
.L2833:
	movq	%rcx, %rsi
	leaq	-128(%rbp), %rdi
	movq	%r9, -216(%rbp)
	movb	%r8b, -201(%rbp)
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal8compiler10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES7_EELS6_325ELS6_327ELS6_329ELS6_321EEC1EPNS1_4NodeE
	cmpb	$0, -80(%rbp)
	movq	-200(%rbp), %rcx
	movzbl	-201(%rbp), %r8d
	movq	-216(%rbp), %r9
	je	.L2737
	movq	%r14, %rsi
	movq	-96(%rbp), %rcx
	movq	-120(%rbp), %r14
	xorl	%edi, %edi
	jmp	.L2730
.L2729:
	movq	(%rcx), %rdx
	movzwl	16(%rdx), %eax
	jmp	.L2727
.L2830:
	movl	$3, -136(%rbp)
	movq	-160(%rbp), %rcx
	movl	$3, %r15d
	movb	$0, -132(%rbp)
	movq	-184(%rbp), %r14
	jmp	.L2696
.L2711:
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L2708
.L2741:
	movq	%rcx, %r14
	xorl	%r15d, %r15d
	movq	%rdx, %rcx
	movl	$1, %edi
	xorl	%r8d, %r8d
	jmp	.L2730
.L2712:
	xorl	%eax, %eax
.L2812:
	movl	$1, %edx
	jmp	.L2708
.L2697:
	movl	$1, -136(%rbp)
	movq	-160(%rbp), %rcx
	movl	$1, %r15d
	movb	$1, -132(%rbp)
	movq	-184(%rbp), %r14
	jmp	.L2696
.L2743:
	cmpq	$0, (%rsi)
	jne	.L2815
	movzbl	23(%r9), %eax
	movq	32(%r9), %rsi
	leaq	32(%r9), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2745
	leaq	16(%rsi), %rdi
.L2745:
	movq	%r9, %rsi
	movq	%rcx, %r14
	movq	(%rdi), %r9
	movzbl	-68(%rbp), %r8d
	movq	%rdx, %rcx
	movl	$1, %edi
	jmp	.L2730
.L2834:
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.L2749
	testb	%r8b, %r8b
	je	.L2838
	movzbl	23(%r9), %edx
	movq	32(%r9), %rsi
	leaq	32(%r9), %rdi
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L2755
	leaq	16(%rsi), %rdi
.L2755:
	movq	%r9, %rsi
	movzbl	-68(%rbp), %r8d
	movq	(%rdi), %r9
	jmp	.L2753
.L2713:
	xorl	%eax, %eax
.L2810:
	movl	$2, %edx
	jmp	.L2708
.L2698:
	movl	$2, -136(%rbp)
	movq	-160(%rbp), %rcx
	movl	$2, %r15d
	movb	$1, -132(%rbp)
	movq	-184(%rbp), %r14
	jmp	.L2696
.L2776:
	movq	%rcx, %rax
	xorl	%r9d, %r9d
	movq	%r14, %rcx
	xorl	%r15d, %r15d
	jmp	.L2753
.L2714:
	xorl	%eax, %eax
	jmp	.L2811
.L2837:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.L2778
	movq	%rax, %rdx
	xorl	%r15d, %r15d
	movq	%rcx, %rax
	movq	%rdx, %rcx
	jmp	.L2753
.L2826:
	call	__stack_chk_fail@PLT
.L2838:
	cmpb	$0, -144(%rbp)
	je	.L2759
	movq	24(%r14), %r10
	testq	%r10, %r10
	je	.L2773
	movl	16(%r10), %edi
	movl	%edi, %edx
	shrl	%edx
	addq	$1, %rdx
	imulq	$24, %rdx, %rdx
	addq	%r10, %rdx
	andb	$1, %dil
	jne	.L2757
	movq	(%rdx), %rdx
.L2757:
	cmpq	%rdx, %r13
	jne	.L2776
	cmpq	$0, (%r10)
	jne	.L2776
	movzbl	23(%r9), %edx
	movq	32(%r9), %rsi
	leaq	32(%r9), %rdi
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L2758
	leaq	16(%rsi), %rdi
.L2758:
	movq	%rax, %rdx
	movq	%r9, %rsi
	movq	%rcx, %rax
	movzbl	-68(%rbp), %r8d
	movq	(%rdi), %r9
	movq	%rdx, %rcx
	jmp	.L2753
.L2764:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2773:
	movq	%rcx, %rax
	xorl	%r9d, %r9d
	movq	%rdi, %rcx
	xorl	%r15d, %r15d
	jmp	.L2753
	.cfi_endproc
.LFE19579:
	.size	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE10InitializeEPNS1_4NodeENS_4base5FlagsINS1_13AddressOptionEhEE, .-_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE10InitializeEPNS1_4NodeENS_4base5FlagsINS1_13AddressOptionEhEE
	.section	.text._ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm,"axG",@progbits,_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm
	.type	_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm, @function
_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm:
.LFB17366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	32(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	32(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movq	%rsi, -112(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2840
	movq	%rdx, -104(%rbp)
	xorl	%ecx, %ecx
	movq	$0, -96(%rbp)
	movq	(%rdx), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$27, %ax
	sete	-88(%rbp)
	je	.L2882
.L2842:
	leaq	8(%r14), %rax
.L2845:
	movq	(%rax), %rax
	movq	$0, -72(%rbp)
	movb	$0, -64(%rbp)
	movq	%rax, -80(%rbp)
	movq	(%rax), %rdx
	movzwl	16(%rdx), %eax
	cmpl	$23, %eax
	je	.L2883
	cmpl	$24, %eax
	je	.L2884
.L2848:
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %rdi
	movl	$3, %edx
	movq	%rbx, %rsi
	movb	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movl	$0, -96(%rbp)
	movl	$0, -72(%rbp)
	movups	%xmm0, -88(%rbp)
	call	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE10InitializeEPNS1_4NodeENS_4base5FlagsINS1_13AddressOptionEhEE
	movq	-80(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L2850
	movq	(%rcx), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L2851
	ja	.L2852
	cmpw	$23, %ax
	je	.L2850
	cmpw	$24, %ax
	jne	.L2854
	movq	48(%rdx), %rax
	movl	$4294967294, %edx
	addq	$2147483647, %rax
	cmpq	%rdx, %rax
	setbe	%al
	testb	%al, %al
	je	.L2854
	.p2align 4,,10
	.p2align 3
.L2850:
	pushq	%r12
	movq	%rcx, %r8
	movl	-72(%rbp), %r9d
	pushq	%r13
	movq	-88(%rbp), %rcx
.L2881:
	movl	-96(%rbp), %edx
	movq	-104(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPm
	popq	%rdx
	popq	%rcx
.L2839:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L2885
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2852:
	.cfi_restore_state
	cmpw	$32, %ax
	je	.L2850
.L2854:
	cmpq	$0, -88(%rbp)
	jne	.L2857
	movl	-72(%rbp), %esi
	testl	%esi, %esi
	jne	.L2857
	pushq	%r12
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	%r13
	jmp	.L2881
	.p2align 4,,10
	.p2align 3
.L2882:
	movq	48(%rdx), %rax
	movl	$1, %ecx
	movq	%rax, -96(%rbp)
	jmp	.L2842
	.p2align 4,,10
	.p2align 3
.L2884:
	movq	48(%rdx), %rax
	movb	$1, -64(%rbp)
	movq	%rax, -72(%rbp)
.L2847:
	testb	%cl, %cl
	je	.L2848
	movq	(%r15), %rdi
	leaq	-96(%rbp), %rsi
	movq	%rsi, -120(%rbp)
	call	_ZNK2v88internal8compiler19InstructionSelector33CanAddressRelativeToRootsRegisterERKNS0_17ExternalReferenceE@PLT
	movq	-120(%rbp), %rsi
	testb	%al, %al
	je	.L2848
	movq	-72(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	(%r15), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movq	-120(%rbp), %rsi
	movl	$4294967295, %edx
	addq	%rax, %rsi
	movl	$2147483648, %eax
	addq	%rsi, %rax
	cmpq	%rdx, %rax
	ja	.L2848
	movq	(%r12), %rbx
	movq	%r15, %rdi
	leaq	1(%rbx), %rax
	movq	%rax, (%r12)
	call	_ZN2v88internal8compiler16OperandGenerator13TempImmediateEi
	movq	%rax, 0(%r13,%rbx,8)
	movl	$19, %eax
	jmp	.L2839
	.p2align 4,,10
	.p2align 3
.L2857:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2886
.L2859:
	movq	(%r12), %rdx
	movq	%r15, %rdi
	addq	$8, %r14
	leaq	1(%rdx), %rax
	movq	%rdx, -120(%rbp)
	movq	%rax, (%r12)
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	movq	-120(%rbp), %rdx
	movq	%rax, 0(%r13,%rdx,8)
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2887
.L2862:
	movq	(%r12), %rbx
	movq	(%r14), %rsi
	movq	%r15, %rdi
	leaq	1(%rbx), %rax
	movq	%rax, (%r12)
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	movq	%rax, 0(%r13,%rbx,8)
	movl	$3, %eax
	jmp	.L2839
	.p2align 4,,10
	.p2align 3
.L2840:
	leaq	16(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	$0, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %edx
	cmpw	$27, %dx
	sete	-88(%rbp)
	je	.L2843
	xorl	%ecx, %ecx
.L2844:
	addq	$8, %rax
	jmp	.L2845
	.p2align 4,,10
	.p2align 3
.L2883:
	movslq	44(%rdx), %rax
	movb	$1, -64(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L2847
	.p2align 4,,10
	.p2align 3
.L2851:
	cmpq	$0, 48(%rdx)
	sete	%al
	testb	%al, %al
	jne	.L2850
	jmp	.L2854
	.p2align 4,,10
	.p2align 3
.L2843:
	movq	48(%rcx), %rdx
	movl	$1, %ecx
	movq	%rdx, -96(%rbp)
	jmp	.L2844
	.p2align 4,,10
	.p2align 3
.L2887:
	movq	32(%rbx), %r14
	addq	$24, %r14
	jmp	.L2862
	.p2align 4,,10
	.p2align 3
.L2886:
	movq	16(%rsi), %rsi
	jmp	.L2859
.L2885:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17366:
	.size	_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm, .-_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeES4_i.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"poisoning_level_ != PoisoningMitigationLevel::kDontPoison"
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeES4_i.str1.1,"aMS",@progbits,1
.LC4:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeES4_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeES4_i
	.type	_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeES4_i, @function
_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeES4_i:
.LFB17373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-104(%rbp), %rdi
	movq	%r14, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	salq	$3, %rbx
	pxor	%xmm0, %xmm0
	movq	%r15, %rsi
	leaq	-96(%rbp), %rcx
	leaq	-104(%rbp), %rdi
	movaps	%xmm0, -80(%rbp)
	movabsq	$927712935937, %rax
	orq	%rax, %rbx
	movq	$0, -64(%rbp)
	movq	%rbx, -88(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdx
	movq	$0, -96(%rbp)
	call	_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm
	movq	(%r14), %rdx
	sall	$9, %eax
	movzwl	16(%rdx), %edx
	orl	%r12d, %eax
	cmpl	$502, %edx
	je	.L2895
	movl	%eax, %esi
	cmpl	$430, %edx
	je	.L2896
.L2890:
	pushq	$0
	movq	-96(%rbp), %r8
	movl	$1, %edx
	leaq	-88(%rbp), %rcx
	pushq	$0
	movq	%rbx, %r9
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2897
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2896:
	.cfi_restore_state
	orl	$8388608, %eax
	cmpl	$1, 364(%r13)
	movl	%eax, %esi
	jne	.L2890
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2895:
	orl	$4194304, %eax
	movl	%eax, %esi
	jmp	.L2890
.L2897:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17373:
	.size	_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeES4_i, .-_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeES4_i
	.section	.text._ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeE:
.LFB17375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	movzbl	%ah, %ecx
	cmpb	$14, %al
	ja	.L2899
	leaq	.L2901(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L2901:
	.long	.L2904-.L2901
	.long	.L2908-.L2901
	.long	.L2908-.L2901
	.long	.L2907-.L2901
	.long	.L2906-.L2901
	.long	.L2905-.L2901
	.long	.L2905-.L2901
	.long	.L2905-.L2901
	.long	.L2905-.L2901
	.long	.L2904-.L2901
	.long	.L2904-.L2901
	.long	.L2904-.L2901
	.long	.L2903-.L2901
	.long	.L2909-.L2901
	.long	.L2900-.L2901
	.section	.text._ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L2905:
	movl	$223, %ecx
.L2902:
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeES4_i
	.p2align 4,,10
	.p2align 3
.L2908:
	.cfi_restore_state
	subl	$2, %ecx
	andl	$253, %ecx
	setne	%cl
	movzbl	%cl, %ecx
	addl	$204, %ecx
	jmp	.L2902
	.p2align 4,,10
	.p2align 3
.L2907:
	subl	$2, %ecx
	andl	$253, %ecx
	setne	%cl
	movzbl	%cl, %ecx
	addl	$209, %ecx
	jmp	.L2902
	.p2align 4,,10
	.p2align 3
.L2906:
	movl	$214, %ecx
	jmp	.L2902
	.p2align 4,,10
	.p2align 3
.L2900:
	movl	$226, %ecx
	jmp	.L2902
	.p2align 4,,10
	.p2align 3
.L2909:
	movl	$224, %ecx
	jmp	.L2902
	.p2align 4,,10
	.p2align 3
.L2903:
	movl	$225, %ecx
	jmp	.L2902
.L2899:
	movl	$17, %ecx
	jmp	.L2902
	.p2align 4,,10
	.p2align 3
.L2904:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17375:
	.size	_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector29VisitChangeCompressedToTaggedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector29VisitChangeCompressedToTaggedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector29VisitChangeCompressedToTaggedEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector29VisitChangeCompressedToTaggedEPNS1_4NodeE:
.LFB17444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2914
	movq	16(%r12), %r12
.L2914:
	movq	(%r12), %rax
	movzwl	16(%rax), %eax
	subl	$429, %eax
	cmpl	$1, %eax
	jbe	.L2920
.L2915:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$34359738369, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$222, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.p2align 4,,10
	.p2align 3
.L2920:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L2915
	popq	%rbx
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	movl	$218, %ecx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeES4_i
	.cfi_endproc
.LFE17444:
	.size	_ZN2v88internal8compiler19InstructionSelector29VisitChangeCompressedToTaggedEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector29VisitChangeCompressedToTaggedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector43VisitChangeCompressedPointerToTaggedPointerEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector43VisitChangeCompressedPointerToTaggedPointerEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector43VisitChangeCompressedPointerToTaggedPointerEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector43VisitChangeCompressedPointerToTaggedPointerEPNS1_4NodeE:
.LFB17445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2922
	movq	16(%r12), %r12
.L2922:
	movq	(%r12), %rax
	movzwl	16(%rax), %eax
	subl	$429, %eax
	cmpl	$1, %eax
	jbe	.L2928
.L2923:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$34359738369, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$221, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.p2align 4,,10
	.p2align 3
.L2928:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L2923
	popq	%rbx
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	movl	$217, %ecx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeES4_i
	.cfi_endproc
.LFE17445:
	.size	_ZN2v88internal8compiler19InstructionSelector43VisitChangeCompressedPointerToTaggedPointerEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector43VisitChangeCompressedPointerToTaggedPointerEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector41VisitChangeCompressedSignedToTaggedSignedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector41VisitChangeCompressedSignedToTaggedSignedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector41VisitChangeCompressedSignedToTaggedSignedEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector41VisitChangeCompressedSignedToTaggedSignedEPNS1_4NodeE:
.LFB17446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2930
	movq	16(%r12), %r12
.L2930:
	movq	(%r12), %rax
	movzwl	16(%rax), %eax
	subl	$429, %eax
	cmpl	$1, %eax
	jbe	.L2936
.L2931:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r12d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$34359738369, %rcx
	salq	$3, %r12
	orq	%rcx, %r12
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	salq	$3, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$220, %esi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	.p2align 4,,10
	.p2align 3
.L2936:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L2931
	popq	%rbx
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	movl	$216, %ecx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeES4_i
	.cfi_endproc
.LFE17446:
	.size	_ZN2v88internal8compiler19InstructionSelector41VisitChangeCompressedSignedToTaggedSignedEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector41VisitChangeCompressedSignedToTaggedSignedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitWord64AtomicLoadEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitWord64AtomicLoadEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitWord64AtomicLoadEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitWord64AtomicLoadEPNS1_4NodeE:
.LFB21828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	movzbl	%ah, %ecx
	cmpb	$14, %al
	ja	.L2938
	leaq	.L2940(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector21VisitWord64AtomicLoadEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L2940:
	.long	.L2943-.L2940
	.long	.L2947-.L2940
	.long	.L2947-.L2940
	.long	.L2946-.L2940
	.long	.L2945-.L2940
	.long	.L2944-.L2940
	.long	.L2944-.L2940
	.long	.L2944-.L2940
	.long	.L2944-.L2940
	.long	.L2943-.L2940
	.long	.L2943-.L2940
	.long	.L2943-.L2940
	.long	.L2942-.L2940
	.long	.L2948-.L2940
	.long	.L2939-.L2940
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitWord64AtomicLoadEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L2944:
	movl	$223, %ecx
.L2941:
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeES4_i
	.p2align 4,,10
	.p2align 3
.L2947:
	.cfi_restore_state
	subl	$2, %ecx
	andl	$253, %ecx
	setne	%cl
	movzbl	%cl, %ecx
	addl	$204, %ecx
	jmp	.L2941
	.p2align 4,,10
	.p2align 3
.L2946:
	subl	$2, %ecx
	andl	$253, %ecx
	setne	%cl
	movzbl	%cl, %ecx
	addl	$209, %ecx
	jmp	.L2941
	.p2align 4,,10
	.p2align 3
.L2945:
	movl	$214, %ecx
	jmp	.L2941
	.p2align 4,,10
	.p2align 3
.L2939:
	movl	$226, %ecx
	jmp	.L2941
	.p2align 4,,10
	.p2align 3
.L2948:
	movl	$224, %ecx
	jmp	.L2941
	.p2align 4,,10
	.p2align 3
.L2942:
	movl	$225, %ecx
	jmp	.L2941
.L2938:
	movl	$17, %ecx
	jmp	.L2941
	.p2align 4,,10
	.p2align 3
.L2943:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21828:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitWord64AtomicLoadEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitWord64AtomicLoadEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitPoisonedLoadEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitPoisonedLoadEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitPoisonedLoadEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitPoisonedLoadEPNS1_4NodeE:
.LFB17376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	movzbl	%ah, %ecx
	cmpb	$14, %al
	ja	.L2953
	leaq	.L2955(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector17VisitPoisonedLoadEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L2955:
	.long	.L2958-.L2955
	.long	.L2962-.L2955
	.long	.L2962-.L2955
	.long	.L2961-.L2955
	.long	.L2960-.L2955
	.long	.L2959-.L2955
	.long	.L2959-.L2955
	.long	.L2959-.L2955
	.long	.L2959-.L2955
	.long	.L2958-.L2955
	.long	.L2958-.L2955
	.long	.L2958-.L2955
	.long	.L2957-.L2955
	.long	.L2963-.L2955
	.long	.L2954-.L2955
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitPoisonedLoadEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L2959:
	movl	$223, %ecx
.L2956:
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeES4_i
	.p2align 4,,10
	.p2align 3
.L2962:
	.cfi_restore_state
	subl	$2, %ecx
	andl	$253, %ecx
	setne	%cl
	movzbl	%cl, %ecx
	addl	$204, %ecx
	jmp	.L2956
	.p2align 4,,10
	.p2align 3
.L2961:
	subl	$2, %ecx
	andl	$253, %ecx
	setne	%cl
	movzbl	%cl, %ecx
	addl	$209, %ecx
	jmp	.L2956
	.p2align 4,,10
	.p2align 3
.L2960:
	movl	$214, %ecx
	jmp	.L2956
	.p2align 4,,10
	.p2align 3
.L2954:
	movl	$226, %ecx
	jmp	.L2956
	.p2align 4,,10
	.p2align 3
.L2963:
	movl	$224, %ecx
	jmp	.L2956
	.p2align 4,,10
	.p2align 3
.L2957:
	movl	$225, %ecx
	jmp	.L2956
.L2953:
	movl	$17, %ecx
	jmp	.L2956
	.p2align 4,,10
	.p2align 3
.L2958:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17376:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitPoisonedLoadEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitPoisonedLoadEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector18VisitProtectedLoadEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector18VisitProtectedLoadEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector18VisitProtectedLoadEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector18VisitProtectedLoadEPNS1_4NodeE:
.LFB21830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	movzbl	%ah, %ecx
	cmpb	$14, %al
	ja	.L2968
	leaq	.L2970(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector18VisitProtectedLoadEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L2970:
	.long	.L2973-.L2970
	.long	.L2977-.L2970
	.long	.L2977-.L2970
	.long	.L2976-.L2970
	.long	.L2975-.L2970
	.long	.L2974-.L2970
	.long	.L2974-.L2970
	.long	.L2974-.L2970
	.long	.L2974-.L2970
	.long	.L2973-.L2970
	.long	.L2973-.L2970
	.long	.L2973-.L2970
	.long	.L2972-.L2970
	.long	.L2978-.L2970
	.long	.L2969-.L2970
	.section	.text._ZN2v88internal8compiler19InstructionSelector18VisitProtectedLoadEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L2974:
	movl	$223, %ecx
.L2971:
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeES4_i
	.p2align 4,,10
	.p2align 3
.L2977:
	.cfi_restore_state
	subl	$2, %ecx
	andl	$253, %ecx
	setne	%cl
	movzbl	%cl, %ecx
	addl	$204, %ecx
	jmp	.L2971
	.p2align 4,,10
	.p2align 3
.L2976:
	subl	$2, %ecx
	andl	$253, %ecx
	setne	%cl
	movzbl	%cl, %ecx
	addl	$209, %ecx
	jmp	.L2971
	.p2align 4,,10
	.p2align 3
.L2975:
	movl	$214, %ecx
	jmp	.L2971
	.p2align 4,,10
	.p2align 3
.L2969:
	movl	$226, %ecx
	jmp	.L2971
	.p2align 4,,10
	.p2align 3
.L2978:
	movl	$224, %ecx
	jmp	.L2971
	.p2align 4,,10
	.p2align 3
.L2972:
	movl	$225, %ecx
	jmp	.L2971
.L2968:
	movl	$17, %ecx
	jmp	.L2971
	.p2align 4,,10
	.p2align 3
.L2973:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21830:
	.size	_ZN2v88internal8compiler19InstructionSelector18VisitProtectedLoadEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector18VisitProtectedLoadEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitWord32AtomicLoadEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitWord32AtomicLoadEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitWord32AtomicLoadEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitWord32AtomicLoadEPNS1_4NodeE:
.LFB17562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	movzbl	%ah, %ecx
	cmpb	$14, %al
	ja	.L2983
	leaq	.L2985(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector21VisitWord32AtomicLoadEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L2985:
	.long	.L2988-.L2985
	.long	.L2992-.L2985
	.long	.L2992-.L2985
	.long	.L2991-.L2985
	.long	.L2990-.L2985
	.long	.L2989-.L2985
	.long	.L2989-.L2985
	.long	.L2989-.L2985
	.long	.L2989-.L2985
	.long	.L2988-.L2985
	.long	.L2988-.L2985
	.long	.L2988-.L2985
	.long	.L2987-.L2985
	.long	.L2993-.L2985
	.long	.L2984-.L2985
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitWord32AtomicLoadEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L2989:
	movl	$223, %ecx
.L2986:
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeES4_i
	.p2align 4,,10
	.p2align 3
.L2992:
	.cfi_restore_state
	subl	$2, %ecx
	andl	$253, %ecx
	setne	%cl
	movzbl	%cl, %ecx
	addl	$204, %ecx
	jmp	.L2986
	.p2align 4,,10
	.p2align 3
.L2991:
	subl	$2, %ecx
	andl	$253, %ecx
	setne	%cl
	movzbl	%cl, %ecx
	addl	$209, %ecx
	jmp	.L2986
	.p2align 4,,10
	.p2align 3
.L2990:
	movl	$214, %ecx
	jmp	.L2986
	.p2align 4,,10
	.p2align 3
.L2984:
	movl	$226, %ecx
	jmp	.L2986
	.p2align 4,,10
	.p2align 3
.L2993:
	movl	$224, %ecx
	jmp	.L2986
	.p2align 4,,10
	.p2align 3
.L2987:
	movl	$225, %ecx
	jmp	.L2986
.L2983:
	movl	$17, %ecx
	jmp	.L2986
	.p2align 4,,10
	.p2align 3
.L2988:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17562:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitWord32AtomicLoadEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitWord32AtomicLoadEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector28VisitStackPointerGreaterThanEPNS1_4NodeEPNS1_17FlagsContinuationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector28VisitStackPointerGreaterThanEPNS1_4NodeEPNS1_17FlagsContinuationE
	.type	_ZN2v88internal8compiler19InstructionSelector28VisitStackPointerGreaterThanEPNS1_4NodeEPNS1_17FlagsContinuationE, @function
_ZN2v88internal8compiler19InstructionSelector28VisitStackPointerGreaterThanEPNS1_4NodeEPNS1_17FlagsContinuationE:
.LFB17390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	32(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2998
	movq	16(%r15), %r15
.L2998:
	movq	40(%r14), %rax
	movq	%r12, %rdi
	movq	136(%rax), %rax
	movq	(%rax), %rax
	movq	56(%rax), %rsi
	call	_ZNK2v88internal8compiler19InstructionSelector14GetEffectLevelEPNS1_4NodeE@PLT
	movq	%r12, -96(%rbp)
	movq	%r12, %rdi
	movl	%eax, %ebx
	movq	(%r15), %rax
	cmpw	$429, 16(%rax)
	je	.L3009
.L2999:
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-96(%rbp), %rdi
	movq	%r15, %rsi
	movl	%eax, %r13d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%r13d, %edx
	movq	%r14, %rcx
	movl	$30, %esi
	salq	$3, %rdx
	movq	%r12, %rdi
	movabsq	$377957122049, %r13
	orq	%r13, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandEPNS1_17FlagsContinuationE@PLT
.L2997:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3010
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3009:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_@PLT
	testb	%al, %al
	jne	.L3000
.L3008:
	movq	-96(%rbp), %rdi
	jmp	.L2999
	.p2align 4,,10
	.p2align 3
.L3000:
	movq	-96(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler19InstructionSelector14GetEffectLevelEPNS1_4NodeE@PLT
	cmpl	%eax, %ebx
	jne	.L3008
	movq	(%r15), %rdi
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	subl	$5, %eax
	cmpb	$3, %al
	ja	.L3008
	leaq	-80(%rbp), %rbx
	pxor	%xmm0, %xmm0
	leaq	-88(%rbp), %rcx
	movq	%r15, %rsi
	leaq	-96(%rbp), %rdi
	movq	%rbx, %rdx
	movaps	%xmm0, -80(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -64(%rbp)
	call	_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm
	subq	$8, %rsp
	movq	-88(%rbp), %r8
	xorl	%edx, %edx
	pushq	%r14
	sall	$9, %eax
	movq	%rbx, %r9
	xorl	%ecx, %ecx
	movl	%eax, %esi
	movq	%r12, %rdi
	orl	$30, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L2997
.L3010:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17390:
	.size	_ZN2v88internal8compiler19InstructionSelector28VisitStackPointerGreaterThanEPNS1_4NodeEPNS1_17FlagsContinuationE, .-_ZN2v88internal8compiler19InstructionSelector28VisitStackPointerGreaterThanEPNS1_4NodeEPNS1_17FlagsContinuationE
	.section	.text._ZN2v88internal8compiler19InstructionSelector23VisitChangeInt32ToInt64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector23VisitChangeInt32ToInt64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector23VisitChangeInt32ToInt64EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector23VisitChangeInt32ToInt64EPNS1_4NodeE:
.LFB17438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$72, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movq	%rdi, -104(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3012
	movq	0(%r13), %rax
	cmpw	$429, 16(%rax)
	je	.L3013
.L3025:
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-104(%rbp), %rdi
	movq	%r13, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r13d
	movq	-104(%rbp), %rdi
	movq	%r14, %rsi
	movabsq	$34359738369, %rcx
	salq	$3, %r13
	orq	%rcx, %r13
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-104(%rbp), %rdi
	movq	%r14, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	salq	$3, %rdx
	movq	%r13, %rcx
	movl	$215, %esi
	movq	%r12, %rdi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
.L3011:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3033
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3012:
	.cfi_restore_state
	movq	16(%r13), %r13
	movq	0(%r13), %rax
	cmpw	$429, 16(%rax)
	jne	.L3025
.L3013:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_@PLT
	testb	%al, %al
	jne	.L3034
	movzbl	23(%r14), %eax
	movq	32(%r14), %r13
	movq	-104(%rbp), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3025
	movq	16(%r13), %r13
	jmp	.L3025
	.p2align 4,,10
	.p2align 3
.L3034:
	movq	0(%r13), %rdi
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	movzbl	%ah, %edx
	cmpb	$3, %al
	je	.L3017
	ja	.L3018
	subl	$1, %eax
	cmpb	$1, %al
	ja	.L3019
	subl	$2, %edx
	xorl	%r13d, %r13d
	andl	$253, %edx
	setne	%r13b
	addl	$206, %r13d
.L3022:
	leaq	-104(%rbp), %r15
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler16OperandGenerator16DefineAsRegisterEPNS1_4NodeE
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	movzbl	23(%r14), %eax
	movq	$0, -64(%rbp)
	andl	$15, %eax
	movaps	%xmm0, -80(%rbp)
	cmpl	$15, %eax
	je	.L3035
.L3023:
	movq	(%rbx), %rsi
	leaq	-80(%rbp), %r14
	leaq	-96(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm
	pushq	$0
	movq	-96(%rbp), %r8
	movl	$1, %edx
	pushq	$0
	sall	$9, %eax
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	movl	%eax, %esi
	movq	%r12, %rdi
	orl	%r13d, %esi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	popq	%rdx
	jmp	.L3011
	.p2align 4,,10
	.p2align 3
.L3018:
	cmpb	$4, %al
	jne	.L3019
	subl	$2, %edx
	xorl	%r13d, %r13d
	andl	$253, %edx
	sete	%r13b
	addl	$214, %r13d
	jmp	.L3022
	.p2align 4,,10
	.p2align 3
.L3035:
	movq	32(%r14), %rbx
	addq	$16, %rbx
	jmp	.L3023
	.p2align 4,,10
	.p2align 3
.L3017:
	subl	$2, %edx
	xorl	%r13d, %r13d
	andl	$253, %edx
	setne	%r13b
	addl	$211, %r13d
	jmp	.L3022
.L3033:
	call	__stack_chk_fail@PLT
.L3019:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17438:
	.size	_ZN2v88internal8compiler19InstructionSelector23VisitChangeInt32ToInt64EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector23VisitChangeInt32ToInt64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector10VisitStoreEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector10VisitStoreEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector10VisitStoreEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector10VisitStoreEPNS1_4NodeE:
.LFB17378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	32(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%r12), %eax
	movq	%rdi, -144(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3037
	movq	40(%r12), %r15
	leaq	48(%r12), %rax
.L3038:
	movq	(%r12), %rdi
	movq	(%rax), %r14
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal8compiler21StoreRepresentationOfEPKNS1_8OperatorE@PLT
	movzbl	1(%rax), %ebx
	testb	%bl, %bl
	je	.L3039
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movl	%eax, %r12d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %r12
	movabsq	$927712935937, %rax
	orq	%rax, %r12
	movq	%r12, -128(%rbp)
	movq	(%r15), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L3040
	ja	.L3041
	cmpw	$23, %ax
	je	.L3042
	cmpw	$24, %ax
	jne	.L3044
	movq	48(%rdx), %rax
	movl	$4294967294, %edx
	addq	$2147483647, %rax
	cmpq	%rdx, %rax
	setbe	%al
	testb	%al, %al
	jne	.L3042
	.p2align 4,,10
	.p2align 3
.L3044:
	movq	-144(%rbp), %rdi
	movq	%r15, %rsi
	movl	$1536, %r12d
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-144(%rbp), %rdi
	movq	%r15, %rsi
	movabsq	$927712935937, %rdx
	movl	%eax, %eax
	salq	$3, %rax
	orq	%rdx, %rax
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movq	-152(%rbp), %rax
.L3046:
	movq	-144(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-144(%rbp), %rdi
	movq	%r14, %rsi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	salq	$3, %r15
	movabsq	$927712935937, %rax
	orq	%rax, %r15
	leal	-2(%rbx), %eax
	movq	%r15, -112(%rbp)
	cmpb	$3, %al
	ja	.L3047
	movq	-144(%rbp), %rax
	movabsq	$377957122049, %r14
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movl	%eax, %eax
	salq	$3, %rax
	orq	%r14, %rax
	movq	%rax, -96(%rbp)
	movq	-144(%rbp), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movzbl	%bl, %esi
	xorl	%ecx, %ecx
	leaq	-128(%rbp), %r9
	movl	%eax, %eax
	subl	$2, %esi
	movl	$3, %r8d
	xorl	%edx, %edx
	salq	$3, %rax
	sall	$22, %esi
	movq	%r13, %rdi
	orq	%r14, %rax
	orl	%r12d, %esi
	movq	%rax, -88(%rbp)
	leaq	-96(%rbp), %rax
	orl	$27, %esi
	pushq	%rax
	pushq	$2
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rcx
	popq	%rsi
.L3036:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3090
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3041:
	.cfi_restore_state
	cmpw	$32, %ax
	jne	.L3044
.L3042:
	leaq	-144(%rbp), %rdi
	movq	%r15, %rsi
	movl	$1024, %r12d
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	jmp	.L3046
	.p2align 4,,10
	.p2align 3
.L3039:
	movzbl	(%rax), %ebx
	cmpb	$14, %bl
	ja	.L3047
	leaq	.L3050(%rip), %rcx
	movzbl	%bl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector10VisitStoreEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L3050:
	.long	.L3047-.L3050
	.long	.L3069-.L3050
	.long	.L3069-.L3050
	.long	.L3055-.L3050
	.long	.L3070-.L3050
	.long	.L3053-.L3050
	.long	.L3053-.L3050
	.long	.L3053-.L3050
	.long	.L3053-.L3050
	.long	.L3047-.L3050
	.long	.L3047-.L3050
	.long	.L3047-.L3050
	.long	.L3052-.L3050
	.long	.L3051-.L3050
	.long	.L3049-.L3050
	.section	.text._ZN2v88internal8compiler19InstructionSelector10VisitStoreEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L3037:
	leaq	16(%rsi), %rax
	movq	8(%rax), %r15
	movq	16(%rsi), %rsi
	addq	$16, %rax
	jmp	.L3038
	.p2align 4,,10
	.p2align 3
.L3053:
	movl	$223, %r10d
.L3056:
	leaq	-96(%rbp), %r9
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	subl	$1, %ebx
	leaq	-144(%rbp), %r8
	movq	%r9, %rdx
	leaq	-136(%rbp), %rcx
	movl	%r10d, -164(%rbp)
	movq	%r8, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	%r9, -160(%rbp)
	movq	%r8, -152(%rbp)
	movq	$0, -136(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm
	movl	-164(%rbp), %r10d
	movq	-152(%rbp), %r8
	sall	$9, %eax
	movq	-160(%rbp), %r9
	movl	%eax, %r15d
	orl	%r10d, %r15d
	cmpb	$13, %bl
	ja	.L3047
.L3067:
	movq	(%r14), %rdx
	movzbl	%bl, %ebx
	leaq	CSWTCH.731(%rip), %rcx
	cmpl	$2, (%rcx,%rbx,4)
	movzwl	16(%rdx), %eax
	jg	.L3057
	cmpw	$473, %ax
	je	.L3091
.L3057:
	cmpw	$28, %ax
	je	.L3060
	ja	.L3061
	cmpw	$23, %ax
	je	.L3062
	cmpw	$24, %ax
	jne	.L3064
	movq	48(%rdx), %rax
	movl	$4294967294, %edx
	addq	$2147483647, %rax
	cmpq	%rdx, %rax
	setbe	%al
.L3065:
	testb	%al, %al
	je	.L3064
.L3062:
	movq	%r14, %rsi
	movq	%r8, %rdi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	movq	-152(%rbp), %r9
.L3066:
	movq	-136(%rbp), %rdx
	pushq	$0
	xorl	%ecx, %ecx
	movl	%r15d, %esi
	pushq	$0
	movq	%r13, %rdi
	leaq	1(%rdx), %r8
	movq	%rax, -96(%rbp,%rdx,8)
	xorl	%edx, %edx
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	popq	%rdx
	jmp	.L3036
	.p2align 4,,10
	.p2align 3
.L3040:
	cmpq	$0, 48(%rdx)
	sete	%al
	testb	%al, %al
	je	.L3044
	jmp	.L3042
	.p2align 4,,10
	.p2align 3
.L3069:
	movl	$208, %r10d
	jmp	.L3056
	.p2align 4,,10
	.p2align 3
.L3055:
	movl	$213, %r15d
.L3054:
	leaq	-96(%rbp), %r9
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	subl	$1, %ebx
	leaq	-144(%rbp), %r8
	movq	%r9, %rdx
	movaps	%xmm0, -96(%rbp)
	leaq	-136(%rbp), %rcx
	movq	%r8, %rdi
	movaps	%xmm0, -80(%rbp)
	movq	%r9, -160(%rbp)
	movq	%r8, -152(%rbp)
	movq	$0, -136(%rbp)
	call	_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %r9
	sall	$9, %eax
	orl	%eax, %r15d
	jmp	.L3067
	.p2align 4,,10
	.p2align 3
.L3051:
	movl	$224, %r10d
	jmp	.L3056
	.p2align 4,,10
	.p2align 3
.L3052:
	movl	$225, %r15d
	jmp	.L3054
	.p2align 4,,10
	.p2align 3
.L3061:
	cmpw	$32, %ax
	je	.L3062
.L3064:
	movq	%r14, %rsi
	movq	%r8, %rdi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	movq	-152(%rbp), %r9
	jmp	.L3066
	.p2align 4,,10
	.p2align 3
.L3060:
	cmpq	$0, 48(%rdx)
	sete	%al
	jmp	.L3065
	.p2align 4,,10
	.p2align 3
.L3091:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r9, -160(%rbp)
	movq	%r8, -152(%rbp)
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_@PLT
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %r9
	testb	%al, %al
	je	.L3059
	movzbl	23(%r14), %eax
	movq	32(%r14), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3059
	movq	16(%r14), %r14
	.p2align 4,,10
	.p2align 3
.L3059:
	movq	(%r14), %rdx
	movzwl	16(%rdx), %eax
	jmp	.L3057
	.p2align 4,,10
	.p2align 3
.L3047:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3070:
	movl	$214, %r15d
	jmp	.L3054
	.p2align 4,,10
	.p2align 3
.L3049:
	movl	$226, %r15d
	jmp	.L3054
.L3090:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17378:
	.size	_ZN2v88internal8compiler19InstructionSelector10VisitStoreEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector10VisitStoreEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector19VisitProtectedStoreEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector19VisitProtectedStoreEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector19VisitProtectedStoreEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector19VisitProtectedStoreEPNS1_4NodeE:
.LFB17379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movq	%rdi, -112(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3093
	leaq	48(%rsi), %rax
.L3094:
	movq	0(%r13), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21StoreRepresentationOfEPKNS1_8OperatorE@PLT
	cmpb	$14, (%rax)
	ja	.L3095
	movzbl	(%rax), %eax
	leaq	.L3097(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector19VisitProtectedStoreEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L3097:
	.long	.L3095-.L3097
	.long	.L3112-.L3097
	.long	.L3112-.L3097
	.long	.L3102-.L3097
	.long	.L3101-.L3097
	.long	.L3100-.L3097
	.long	.L3100-.L3097
	.long	.L3100-.L3097
	.long	.L3100-.L3097
	.long	.L3095-.L3097
	.long	.L3095-.L3097
	.long	.L3095-.L3097
	.long	.L3099-.L3097
	.long	.L3098-.L3097
	.long	.L3096-.L3097
	.section	.text._ZN2v88internal8compiler19InstructionSelector19VisitProtectedStoreEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L3093:
	movq	32(%rsi), %rax
	addq	$32, %rax
	jmp	.L3094
	.p2align 4,,10
	.p2align 3
.L3100:
	movl	$223, %ebx
	.p2align 4,,10
	.p2align 3
.L3103:
	leaq	-96(%rbp), %r9
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %rdi
	movq	%r13, %rsi
	movq	%r9, %rdx
	leaq	-104(%rbp), %rcx
	movq	%r9, -128(%rbp)
	movq	%rdi, -120(%rbp)
	movq	$0, -104(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm
	movq	(%r15), %rdx
	movq	-120(%rbp), %rdi
	sall	$9, %eax
	movq	-128(%rbp), %r9
	movl	%eax, %r12d
	movzwl	16(%rdx), %eax
	orl	%ebx, %r12d
	orl	$4194304, %r12d
	cmpw	$28, %ax
	je	.L3104
	ja	.L3105
	cmpw	$23, %ax
	je	.L3106
	cmpw	$24, %ax
	jne	.L3108
	movq	48(%rdx), %rax
	movl	$4294967294, %edx
	addq	$2147483647, %rax
	cmpq	%rdx, %rax
	setbe	%al
.L3109:
	testb	%al, %al
	je	.L3108
.L3106:
	movq	%r15, %rsi
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	movq	-120(%rbp), %r9
	movq	%rax, %rbx
.L3110:
	pushq	$0
	movq	-104(%rbp), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	pushq	$0
	movl	%r12d, %esi
	movq	%r14, %rdi
	leaq	1(%rax), %r8
	movq	%rbx, -96(%rbp,%rax,8)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3123
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3112:
	.cfi_restore_state
	movl	$208, %ebx
	jmp	.L3103
	.p2align 4,,10
	.p2align 3
.L3102:
	movl	$213, %ebx
	jmp	.L3103
	.p2align 4,,10
	.p2align 3
.L3101:
	movl	$214, %ebx
	jmp	.L3103
	.p2align 4,,10
	.p2align 3
.L3099:
	movl	$225, %ebx
	jmp	.L3103
	.p2align 4,,10
	.p2align 3
.L3096:
	movl	$226, %ebx
	jmp	.L3103
	.p2align 4,,10
	.p2align 3
.L3098:
	movl	$224, %ebx
	jmp	.L3103
	.p2align 4,,10
	.p2align 3
.L3105:
	cmpw	$32, %ax
	je	.L3106
.L3108:
	movq	-112(%rbp), %rdi
	movq	%r15, %rsi
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-112(%rbp), %rdi
	movq	%r15, %rsi
	movl	%eax, %ebx
	movabsq	$377957122049, %rax
	salq	$3, %rbx
	orq	%rax, %rbx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movq	-120(%rbp), %r9
	jmp	.L3110
	.p2align 4,,10
	.p2align 3
.L3104:
	cmpq	$0, 48(%rdx)
	sete	%al
	jmp	.L3109
	.p2align 4,,10
	.p2align 3
.L3095:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3123:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17379:
	.size	_ZN2v88internal8compiler19InstructionSelector19VisitProtectedStoreEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector19VisitProtectedStoreEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20EmitPrepareArgumentsEPNS0_10ZoneVectorINS1_13PushParameterEEEPKNS1_14CallDescriptorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20EmitPrepareArgumentsEPNS0_10ZoneVectorINS1_13PushParameterEEEPKNS1_14CallDescriptorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector20EmitPrepareArgumentsEPNS0_10ZoneVectorINS1_13PushParameterEEEPKNS1_14CallDescriptorEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector20EmitPrepareArgumentsEPNS0_10ZoneVectorINS1_13PushParameterEEEPKNS1_14CallDescriptorEPNS1_4NodeE:
.LFB17521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%rcx, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 8(%rdx)
	movq	%rdi, -120(%rbp)
	je	.L3180
	movq	%rcx, %rsi
	call	_ZNK2v88internal8compiler19InstructionSelector14GetEffectLevelEPNS1_4NodeE@PLT
	movq	16(%r13), %r14
	movq	8(%r13), %r13
	movl	%eax, -148(%rbp)
	cmpq	%r14, %r13
	je	.L3124
	leaq	-120(%rbp), %rax
	movq	%r14, %r15
	movq	%rax, -136(%rbp)
	jmp	.L3146
	.p2align 4,,10
	.p2align 3
.L3181:
	cmpw	$23, %ax
	je	.L3139
	cmpw	$24, %ax
	jne	.L3141
	movq	48(%rdx), %rax
	movl	$4294967294, %edi
	addq	$2147483647, %rax
	cmpq	%rdi, %rax
	setbe	%al
.L3142:
	testb	%al, %al
	je	.L3141
.L3139:
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	movq	%rax, %rcx
.L3178:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	$235, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
.L3136:
	subq	$16, %r15
	cmpq	%r15, %r13
	je	.L3124
.L3146:
	movq	-16(%r15), %r14
	testq	%r14, %r14
	je	.L3136
	movq	(%r14), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L3137
	jbe	.L3181
	cmpw	$32, %ax
	je	.L3139
.L3141:
	testb	$8, 37(%rbx)
	jne	.L3143
	movq	16(%rbx), %r12
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler19InstructionSequence17GetRepresentationEi@PLT
	cmpb	$11, %al
	jbe	.L3182
.L3143:
	movq	-120(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-120(%rbp), %rdi
	movq	%r14, %rsi
	movl	%eax, %r12d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%r12d, %ecx
	movabsq	$377957122049, %rax
	salq	$3, %rcx
.L3179:
	orq	%rax, %rcx
	jmp	.L3178
	.p2align 4,,10
	.p2align 3
.L3137:
	cmpq	$0, 48(%rdx)
	sete	%al
	jmp	.L3142
	.p2align 4,,10
	.p2align 3
.L3124:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3183
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3182:
	.cfi_restore_state
	movl	-148(%rbp), %r8d
	movq	-144(%rbp), %rdx
	movq	%r14, %rcx
	movl	$235, %esi
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i
	testb	%al, %al
	jne	.L3184
	movq	-120(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-120(%rbp), %rdi
	movq	%r14, %rsi
	movl	%eax, %r12d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%r12d, %ecx
	movabsq	$103079215105, %rax
	salq	$3, %rcx
	jmp	.L3179
	.p2align 4,,10
	.p2align 3
.L3180:
	movq	24(%rdx), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%r15d, %r15d
	leaq	-120(%rbp), %r14
	movq	8(%rax), %rsi
	pushq	$0
	pushq	$0
	sall	$22, %esi
	orl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	movq	8(%r13), %rdx
	movq	16(%r13), %rcx
	popq	%rsi
	popq	%rdi
	cmpq	%rcx, %rdx
	jne	.L3126
	jmp	.L3124
	.p2align 4,,10
	.p2align 3
.L3185:
	cmpw	$23, %ax
	je	.L3130
	cmpw	$24, %ax
	jne	.L3132
	movq	48(%rdx), %rax
	movl	$4294967294, %edi
	addq	$2147483647, %rax
	cmpq	%rdi, %rax
	setbe	%al
.L3133:
	testb	%al, %al
	je	.L3132
.L3130:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	movq	%rax, %rcx
.L3134:
	movl	%r15d, %esi
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	sall	$22, %esi
	movq	%rbx, %rdi
	orb	$-20, %sil
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	movq	8(%r13), %rdx
	movq	16(%r13), %rcx
.L3127:
	movq	%rcx, %rax
	addq	$1, %r15
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%r15, %rax
	jbe	.L3124
.L3126:
	movq	%r15, %rax
	salq	$4, %rax
	movq	(%rdx,%rax), %rsi
	testq	%rsi, %rsi
	je	.L3127
	movq	(%rsi), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L3128
	jbe	.L3185
	cmpw	$32, %ax
	je	.L3130
.L3132:
	movq	-120(%rbp), %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-136(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movl	%eax, %r12d
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%r12d, %ecx
	movabsq	$377957122049, %rax
	salq	$3, %rcx
	orq	%rax, %rcx
	jmp	.L3134
	.p2align 4,,10
	.p2align 3
.L3128:
	cmpq	$0, 48(%rdx)
	sete	%al
	jmp	.L3133
	.p2align 4,,10
	.p2align 3
.L3184:
	leaq	-96(%rbp), %r12
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %rcx
	movq	%r14, %rsi
	movq	-136(%rbp), %rdi
	movq	%r12, %rdx
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -112(%rbp)
	call	_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm
	pushq	$0
	movq	-112(%rbp), %r8
	xorl	%edx, %edx
	pushq	$0
	sall	$9, %eax
	leaq	-104(%rbp), %rcx
	movq	%r12, %r9
	movl	%eax, %esi
	movq	%rbx, %rdi
	orb	$-21, %sil
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	popq	%rdx
	jmp	.L3136
.L3183:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17521:
	.size	_ZN2v88internal8compiler19InstructionSelector20EmitPrepareArgumentsEPNS0_10ZoneVectorINS1_13PushParameterEEEPKNS1_14CallDescriptorEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector20EmitPrepareArgumentsEPNS0_10ZoneVectorINS1_13PushParameterEEEPKNS1_14CallDescriptorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE, @function
_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE:
.LFB17382:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movl	%edx, -212(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -208(%rbp)
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	movq	-168(%rbp), %r9
	movq	-152(%rbp), %r14
	pxor	%xmm0, %xmm0
	movq	$0, -200(%rbp)
	movq	$0, -136(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpq	%r14, %r9
	je	.L3226
	movq	(%r14), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L3189
	ja	.L3190
	cmpw	$23, %ax
	je	.L3191
	cmpw	$24, %ax
	jne	.L3193
	movq	48(%rdx), %rax
	movl	$4294967294, %edx
	addq	$2147483647, %rax
	cmpq	%rdx, %rax
	setbe	%al
	testb	%al, %al
	je	.L3193
	.p2align 4,,10
	.p2align 3
.L3191:
	movq	-208(%rbp), %rdi
	movq	%r9, %rsi
	movq	%r9, -224(%rbp)
	movq	$1, -200(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-224(%rbp), %r9
	movq	-208(%rbp), %rdi
	movl	%eax, %ebx
	movq	%r9, %rsi
	salq	$3, %rbx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	leaq	-208(%rbp), %rdi
	movq	%r14, %rsi
	movabsq	$377957122049, %rax
	orq	%rax, %rbx
	movq	%rbx, -128(%rbp)
	movq	-200(%rbp), %rbx
	leaq	1(%rbx), %rax
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	leaq	-128(%rbp), %r9
	movq	%rax, -128(%rbp,%rbx,8)
.L3188:
	movl	(%r12), %eax
	movq	-208(%rbp), %rdi
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L3227
.L3202:
	movq	%r15, %rsi
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-208(%rbp), %rdi
	movq	%r15, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	salq	$3, %rbx
	movq	%r13, %rdi
	pushq	%r12
	movq	-224(%rbp), %r9
	movabsq	$1065151889409, %rax
	movl	$1, %edx
	movq	-200(%rbp), %r8
	movl	-212(%rbp), %esi
	orq	%rax, %rbx
	leaq	-136(%rbp), %rcx
	movq	%rbx, -136(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3228
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3190:
	.cfi_restore_state
	cmpw	$32, %ax
	je	.L3191
.L3193:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r9, -224(%rbp)
	call	_ZNK2v88internal8compiler19InstructionSelector14GetEffectLevelEPNS1_4NodeE@PLT
	movq	-224(%rbp), %r9
	movl	%eax, %r8d
	movl	(%r12), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L3229
.L3195:
	movq	(%r15), %rax
	leaq	-208(%rbp), %rbx
	testb	$1, 18(%rax)
	jne	.L3230
.L3196:
	movl	-212(%rbp), %esi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%r14, %rcx
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i
	movq	-200(%rbp), %rdx
	movq	-224(%rbp), %r9
	movq	%rbx, %rdi
	testb	%al, %al
	leaq	1(%rdx), %rax
	movq	%rdx, -224(%rbp)
	movq	%r9, %rsi
	movq	%rax, -200(%rbp)
	je	.L3201
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	leaq	-128(%rbp), %r9
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	-224(%rbp), %rdx
	leaq	-200(%rbp), %rcx
	movq	%r9, -224(%rbp)
	movq	%rax, -128(%rbp,%rdx,8)
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm
	movq	-224(%rbp), %r9
	sall	$9, %eax
	orl	%eax, -212(%rbp)
	jmp	.L3188
	.p2align 4,,10
	.p2align 3
.L3227:
	movq	-200(%rbp), %rcx
	movq	40(%r12), %rdx
	leaq	1(%rcx), %rax
	movq	%rax, -200(%rbp)
	movq	16(%rdi), %rax
	movslq	4(%rdx), %rdx
	movb	$19, -188(%rbp)
	movl	$7, -192(%rbp)
	movq	%rdx, -184(%rbp)
	movq	160(%rax), %rsi
	movq	%rsi, %rbx
	subq	152(%rax), %rbx
	sarq	$4, %rbx
	cmpq	168(%rax), %rsi
	je	.L3203
	movl	$7, (%rsi)
	movb	$19, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rax)
.L3204:
	salq	$32, %rbx
	movq	48(%r12), %rdx
	orq	$11, %rbx
	movq	%rbx, -128(%rbp,%rcx,8)
	movq	-200(%rbp), %rcx
	leaq	1(%rcx), %rax
	movq	%rax, -200(%rbp)
	movq	-208(%rbp), %rax
	movslq	4(%rdx), %rdx
	movq	16(%rax), %rax
	movb	$19, -188(%rbp)
	movl	$7, -192(%rbp)
	movq	%rdx, -184(%rbp)
	movq	160(%rax), %rsi
	movq	%rsi, %rbx
	subq	152(%rax), %rbx
	sarq	$4, %rbx
	cmpq	168(%rax), %rsi
	je	.L3205
	movl	$7, (%rsi)
	movb	$19, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rax)
.L3206:
	salq	$32, %rbx
	movq	-208(%rbp), %rdi
	orq	$11, %rbx
	movq	%rbx, -128(%rbp,%rcx,8)
	jmp	.L3202
	.p2align 4,,10
	.p2align 3
.L3201:
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	movq	-224(%rbp), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rax, -128(%rbp,%rdx,8)
	movq	-200(%rbp), %rdx
	leaq	1(%rdx), %rax
	movq	%rdx, -224(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler16OperandGenerator3UseEPNS1_4NodeE
	movq	-224(%rbp), %rdx
	leaq	-128(%rbp), %r9
	movq	%rax, -128(%rbp,%rdx,8)
	jmp	.L3188
	.p2align 4,,10
	.p2align 3
.L3189:
	cmpq	$0, 48(%rdx)
	sete	%al
	testb	%al, %al
	je	.L3193
	jmp	.L3191
	.p2align 4,,10
	.p2align 3
.L3226:
	movq	%r9, %rsi
	leaq	-208(%rbp), %rdi
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	movq	-200(%rbp), %rdx
	leaq	-128(%rbp), %r9
	leaq	2(%rdx), %rcx
	movq	%rax, -128(%rbp,%rdx,8)
	movq	%rcx, -200(%rbp)
	movq	%rax, -120(%rbp,%rdx,8)
	jmp	.L3188
	.p2align 4,,10
	.p2align 3
.L3230:
	movq	-208(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r9, -240(%rbp)
	movl	%r8d, -232(%rbp)
	movq	%rdi, -224(%rbp)
	call	_ZNK2v88internal8compiler19InstructionSelector9IsDefinedEPNS1_4NodeE@PLT
	movq	-224(%rbp), %rdi
	movl	-232(%rbp), %r8d
	testb	%al, %al
	movq	-240(%rbp), %r9
	je	.L3197
.L3200:
	movq	-208(%rbp), %rbx
	movq	%r9, %rsi
	movl	%r8d, -232(%rbp)
	movq	%r9, -224(%rbp)
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector9IsDefinedEPNS1_4NodeE@PLT
	movq	-224(%rbp), %r9
	movl	-232(%rbp), %r8d
	testb	%al, %al
	je	.L3231
.L3198:
	movl	-212(%rbp), %esi
	leaq	-208(%rbp), %rbx
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%r9, -232(%rbp)
	movl	%r8d, -224(%rbp)
	call	_ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i
	movl	-224(%rbp), %r8d
	movq	-232(%rbp), %r9
	testb	%al, %al
	jne	.L3196
	movq	%r9, %rax
	movq	%r14, %r9
	movq	%rax, %r14
	jmp	.L3196
	.p2align 4,,10
	.p2align 3
.L3229:
	movq	40(%r12), %rax
	movq	%r13, %rdi
	movq	136(%rax), %rax
	movq	(%rax), %rax
	movq	56(%rax), %rsi
	call	_ZNK2v88internal8compiler19InstructionSelector14GetEffectLevelEPNS1_4NodeE@PLT
	movq	-224(%rbp), %r9
	movl	%eax, %r8d
	jmp	.L3195
.L3197:
	movq	%r14, %rsi
	movq	%r9, -232(%rbp)
	movl	%r8d, -224(%rbp)
	call	_ZNK2v88internal8compiler19InstructionSelector6IsUsedEPNS1_4NodeE@PLT
	movl	-224(%rbp), %r8d
	movq	-232(%rbp), %r9
	testb	%al, %al
	jne	.L3196
	jmp	.L3200
.L3203:
	leaq	-192(%rbp), %rdx
	leaq	144(%rax), %rdi
	movq	%r9, -232(%rbp)
	movq	%rcx, -224(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-232(%rbp), %r9
	movq	-224(%rbp), %rcx
	jmp	.L3204
.L3205:
	leaq	-192(%rbp), %rdx
	leaq	144(%rax), %rdi
	movq	%r9, -232(%rbp)
	movq	%rcx, -224(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-232(%rbp), %r9
	movq	-224(%rbp), %rcx
	jmp	.L3206
.L3231:
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movl	%r8d, -232(%rbp)
	movq	%r9, -224(%rbp)
	call	_ZNK2v88internal8compiler19InstructionSelector6IsUsedEPNS1_4NodeE@PLT
	movq	-224(%rbp), %r9
	movl	-232(%rbp), %r8d
	testb	%al, %al
	je	.L3198
	movq	%r9, %rax
	leaq	-208(%rbp), %rbx
	movq	%r14, %r9
	movq	%rax, %r14
	jmp	.L3196
.L3228:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17382:
	.size	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE, .-_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitWord32AndEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitWord32AndEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitWord32AndEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitWord32AndEPNS1_4NodeE:
.LFB17384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L3233
	movq	(%r14), %rdx
	leaq	32(%rsi), %rax
	cmpw	$23, 16(%rdx)
	je	.L3236
.L3252:
	xorl	%ebx, %ebx
	xorl	%edx, %edx
.L3237:
	movq	8(%rax), %r15
	addq	$8, %rax
	movq	(%r15), %rax
	cmpw	$23, 16(%rax)
	jne	.L3239
	movl	44(%rax), %ebx
.L3240:
	cmpl	$255, %ebx
	jne	.L3242
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movabsq	$34359738369, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	salq	$3, %rdx
	movq	%r14, %rcx
	movl	$205, %esi
	movq	%r13, %rdi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	jmp	.L3232
	.p2align 4,,10
	.p2align 3
.L3239:
	movq	(%r12), %rax
	testb	$1, 18(%rax)
	je	.L3241
	testb	%dl, %dl
	jne	.L3253
.L3241:
	leaq	-128(%rbp), %rcx
	movl	$98, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$0, -128(%rbp)
	movq	$0, -112(%rbp)
	movl	$-1, -104(%rbp)
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
.L3232:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3254
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3233:
	.cfi_restore_state
	leaq	16(%r14), %rax
	movq	16(%r14), %r14
	movq	(%r14), %rdx
	cmpw	$23, 16(%rdx)
	jne	.L3252
.L3236:
	movl	44(%rdx), %ebx
	movl	$1, %edx
	jmp	.L3237
	.p2align 4,,10
	.p2align 3
.L3242:
	cmpl	$65535, %ebx
	jne	.L3241
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movabsq	$34359738369, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	salq	$3, %rdx
	movq	%r14, %rcx
	movl	$210, %esi
	movq	%r13, %rdi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	jmp	.L3232
	.p2align 4,,10
	.p2align 3
.L3253:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r15, %r14
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	jmp	.L3240
.L3254:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17384:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitWord32AndEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitWord32AndEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitWord64AndEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitWord64AndEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitWord64AndEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitWord64AndEPNS1_4NodeE:
.LFB17385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$97, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rcx
	movl	$0, -80(%rbp)
	movq	$0, -64(%rbp)
	movl	$-1, -56(%rbp)
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3258
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3258:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17385:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitWord64AndEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitWord64AndEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitWord32OrEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitWord32OrEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitWord32OrEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitWord32OrEPNS1_4NodeE:
.LFB17386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$108, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rcx
	movl	$0, -80(%rbp)
	movq	$0, -64(%rbp)
	movl	$-1, -56(%rbp)
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3262
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3262:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17386:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitWord32OrEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitWord32OrEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitWord64OrEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitWord64OrEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitWord64OrEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitWord64OrEPNS1_4NodeE:
.LFB17387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$107, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rcx
	movl	$0, -80(%rbp)
	movq	$0, -64(%rbp)
	movl	$-1, -56(%rbp)
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3266
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3266:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17387:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitWord64OrEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitWord64OrEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitWord32XorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitWord32XorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitWord32XorEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitWord32XorEPNS1_4NodeE:
.LFB17388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L3268
	movq	(%r14), %rdx
	leaq	32(%rsi), %rax
	cmpw	$23, 16(%rdx)
	je	.L3271
.L3286:
	xorl	%r15d, %r15d
	xorl	%edx, %edx
.L3272:
	movq	8(%rax), %rbx
	addq	$8, %rax
	movq	(%rbx), %rax
	cmpw	$23, 16(%rax)
	jne	.L3274
	movl	44(%rax), %r15d
.L3275:
	cmpl	$-1, %r15d
	jne	.L3276
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	salq	$3, %rdx
	movq	%r14, %rcx
	movl	$122, %esi
	movq	%r13, %rdi
	movabsq	$1065151889409, %rbx
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	jmp	.L3267
	.p2align 4,,10
	.p2align 3
.L3274:
	movq	(%r12), %rax
	testb	$1, 18(%rax)
	je	.L3276
	testb	%dl, %dl
	jne	.L3287
.L3276:
	leaq	-128(%rbp), %rcx
	movl	$110, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$0, -128(%rbp)
	movq	$0, -112(%rbp)
	movl	$-1, -104(%rbp)
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
.L3267:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3288
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3268:
	.cfi_restore_state
	leaq	16(%r14), %rax
	movq	16(%r14), %r14
	movq	(%r14), %rdx
	cmpw	$23, 16(%rdx)
	jne	.L3286
.L3271:
	movl	44(%rdx), %r15d
	movl	$1, %edx
	jmp	.L3272
	.p2align 4,,10
	.p2align 3
.L3287:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rbx, %r14
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	jmp	.L3275
.L3288:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17388:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitWord32XorEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitWord32XorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitWord64XorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitWord64XorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitWord64XorEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitWord64XorEPNS1_4NodeE:
.LFB17389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	leaq	-176(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$144, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherImLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -128(%rbp)
	je	.L3290
	cmpq	$-1, -136(%rbp)
	je	.L3294
.L3290:
	leaq	-112(%rbp), %rcx
	movl	$109, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$0, -112(%rbp)
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
.L3289:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3295
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3294:
	.cfi_restore_state
	movq	-168(%rbp), %r14
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	salq	$3, %rdx
	movq	%r14, %rcx
	movl	$121, %esi
	movq	%r13, %rdi
	movabsq	$1065151889409, %rbx
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	jmp	.L3289
.L3295:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17389:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitWord64XorEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitWord64XorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitInt32AddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitInt32AddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitInt32AddEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitInt32AddEPNS1_4NodeE:
.LFB17410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	leaq	-144(%rbp), %rdi
	.cfi_offset 12, -32
	movq	%rsi, %r12
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	$0, -128(%rbp)
	movl	$0, -104(%rbp)
	movups	%xmm0, -120(%rbp)
	movzbl	18(%rax), %edx
	movb	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	andl	$1, %edx
	orl	$2, %edx
	call	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELS7_306ELS7_308ELS7_310ELS7_302EEEE10InitializeEPNS1_4NodeENS_4base5FlagsINS1_13AddressOptionEhEE
	cmpb	$0, -144(%rbp)
	je	.L3297
	movq	-112(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L3298
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$28, %ax
	je	.L3299
	ja	.L3300
	cmpw	$23, %ax
	je	.L3298
	cmpw	$24, %ax
	jne	.L3297
	movq	48(%rcx), %rax
	movl	$4294967294, %ecx
	addq	$2147483647, %rax
	cmpq	%rcx, %rax
	setbe	%al
	testb	%al, %al
	jne	.L3298
	.p2align 4,,10
	.p2align 3
.L3297:
	leaq	-96(%rbp), %rcx
	movl	$96, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$0, -96(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
.L3296:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3318
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3300:
	.cfi_restore_state
	cmpw	$32, %ax
	jne	.L3297
.L3298:
	movl	-104(%rbp), %eax
	movq	-120(%rbp), %r9
	movl	$231, %esi
	movq	%r13, %rdi
	movl	-128(%rbp), %r8d
	movq	-136(%rbp), %rcx
	pushq	%rax
	pushq	%rdx
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_17EmitLeaEPNS1_19InstructionSelectorEiPNS1_4NodeES6_iS6_S6_NS1_16DisplacementModeE
	popq	%rax
	popq	%rdx
	jmp	.L3296
	.p2align 4,,10
	.p2align 3
.L3299:
	cmpq	$0, 48(%rcx)
	sete	%al
	testb	%al, %al
	je	.L3297
	jmp	.L3298
.L3318:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17410:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitInt32AddEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitInt32AddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitInt64AddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitInt64AddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitInt64AddEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitInt64AddEPNS1_4NodeE:
.LFB17411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	leaq	-144(%rbp), %rdi
	.cfi_offset 12, -32
	movq	%rsi, %r12
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	$0, -128(%rbp)
	movl	$0, -104(%rbp)
	movups	%xmm0, -120(%rbp)
	movzbl	18(%rax), %edx
	movb	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	andl	$1, %edx
	orl	$2, %edx
	call	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE10InitializeEPNS1_4NodeENS_4base5FlagsINS1_13AddressOptionEhEE
	cmpb	$0, -144(%rbp)
	je	.L3320
	movq	-112(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L3321
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$28, %ax
	je	.L3322
	ja	.L3323
	cmpw	$23, %ax
	je	.L3321
	cmpw	$24, %ax
	jne	.L3320
	movq	48(%rcx), %rax
	movl	$4294967294, %ecx
	addq	$2147483647, %rax
	cmpq	%rcx, %rax
	setbe	%al
	testb	%al, %al
	jne	.L3321
	.p2align 4,,10
	.p2align 3
.L3320:
	leaq	-96(%rbp), %rcx
	movl	$95, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$0, -96(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
.L3319:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3341
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3323:
	.cfi_restore_state
	cmpw	$32, %ax
	jne	.L3320
.L3321:
	movl	-104(%rbp), %eax
	movq	-120(%rbp), %r9
	movl	$232, %esi
	movq	%r13, %rdi
	movl	-128(%rbp), %r8d
	movq	-136(%rbp), %rcx
	pushq	%rax
	pushq	%rdx
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_17EmitLeaEPNS1_19InstructionSelectorEiPNS1_4NodeES6_iS6_S6_NS1_16DisplacementModeE
	popq	%rax
	popq	%rdx
	jmp	.L3319
	.p2align 4,,10
	.p2align 3
.L3322:
	cmpq	$0, 48(%rcx)
	sete	%al
	testb	%al, %al
	je	.L3320
	jmp	.L3321
.L3341:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17411:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitInt64AddEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitInt64AddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitInt32SubEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitInt32SubEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitInt32SubEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitInt32SubEPNS1_4NodeE:
.LFB17413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	32(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movq	%rdi, -184(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3343
	leaq	40(%rsi), %rax
.L3344:
	movq	(%rdx), %rcx
	cmpw	$473, 16(%rcx)
	jne	.L3345
	movq	(%rax), %rax
	movq	(%rax), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$28, %ax
	je	.L3346
	ja	.L3347
	cmpw	$23, %ax
	je	.L3348
	cmpw	$24, %ax
	jne	.L3345
	movq	48(%rcx), %rax
	movl	$4294967294, %esi
	addq	$2147483647, %rax
	cmpq	%rsi, %rax
	setbe	%al
	testb	%al, %al
	jne	.L3350
	.p2align 4,,10
	.p2align 3
.L3345:
	leaq	-176(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -156(%rbp)
	je	.L3353
	movl	-160(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L3394
.L3353:
	cmpb	$0, -140(%rbp)
	je	.L3365
	movl	-144(%rbp), %esi
	testl	%esi, %esi
	je	.L3395
	movq	-152(%rbp), %rax
	movq	(%rax), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L3367
	ja	.L3368
	cmpw	$23, %ax
	je	.L3369
	cmpw	$24, %ax
	jne	.L3365
	movq	48(%rdx), %rax
	movl	$4294967294, %edx
	addq	$2147483647, %rax
	cmpq	%rdx, %rax
	setbe	%al
.L3371:
	testb	%al, %al
	je	.L3365
.L3369:
	cmpl	$-2147483648, %esi
	je	.L3372
	negl	%esi
.L3372:
	leaq	-128(%rbp), %rdx
	movq	-184(%rbp), %rax
	movq	%rdx, %rdi
	movq	%rdx, -200(%rbp)
	movq	16(%rax), %r14
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-128(%rbp), %eax
	movq	-200(%rbp), %rdx
	testl	%eax, %eax
	jne	.L3373
	cmpb	$19, -124(%rbp)
	je	.L3396
.L3373:
	movq	160(%r14), %rsi
	movq	%rsi, %rbx
	subq	152(%r14), %rbx
	sarq	$4, %rbx
	cmpq	168(%r14), %rsi
	je	.L3375
	movzbl	-124(%rbp), %ecx
	movq	-120(%rbp), %rdx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%r14)
.L3376:
	salq	$32, %rbx
	orq	$11, %rbx
.L3374:
	movq	-168(%rbp), %rsi
	leaq	-184(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler16OperandGenerator16DefineAsRegisterEPNS1_4NodeE
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	pushq	$0
	movq	%rax, %rdx
	movq	%r14, %rcx
	movl	$1255, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	jmp	.L3342
	.p2align 4,,10
	.p2align 3
.L3347:
	cmpw	$32, %ax
	jne	.L3345
.L3350:
	movl	48(%rcx), %r15d
.L3354:
	movzbl	23(%rdx), %eax
	movq	32(%rdx), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3355
	movq	16(%rsi), %rsi
.L3355:
	leaq	-184(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	movq	%rax, -200(%rbp)
	testl	%r15d, %r15d
	je	.L3397
	cmpl	$-2147483648, %r15d
	je	.L3358
	negl	%r15d
.L3358:
	movq	-184(%rbp), %rax
	leaq	-128(%rbp), %rdx
	movl	%r15d, %esi
	movq	%rdx, %rdi
	movq	%rdx, -208(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -216(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-128(%rbp), %eax
	movq	-208(%rbp), %rdx
	movq	-216(%rbp), %rcx
	testl	%eax, %eax
	jne	.L3359
	cmpb	$19, -124(%rbp)
	je	.L3398
.L3359:
	movq	160(%rcx), %rsi
	movq	%rsi, %rbx
	subq	152(%rcx), %rbx
	sarq	$4, %rbx
	cmpq	168(%rcx), %rsi
	je	.L3361
	movzbl	-124(%rbp), %edi
	movq	-120(%rbp), %rdx
	movl	%eax, (%rsi)
	movb	%dil, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rcx)
.L3362:
	salq	$32, %rbx
	orq	$11, %rbx
.L3360:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler16OperandGenerator16DefineAsRegisterEPNS1_4NodeE
	subq	$8, %rsp
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	pushq	$0
	movq	-200(%rbp), %rcx
	movq	%rax, %rdx
	movq	%rbx, %r8
	movl	$1255, %esi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rsi
	popq	%rdi
	jmp	.L3342
	.p2align 4,,10
	.p2align 3
.L3368:
	cmpw	$32, %ax
	je	.L3369
.L3365:
	leaq	-128(%rbp), %rcx
	movl	$112, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$0, -128(%rbp)
	movq	$0, -112(%rbp)
	movl	$-1, -104(%rbp)
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
.L3342:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3399
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3343:
	.cfi_restore_state
	leaq	16(%rdx), %rax
	movq	16(%rdx), %rdx
	addq	$8, %rax
	jmp	.L3344
	.p2align 4,,10
	.p2align 3
.L3394:
	movq	-152(%rbp), %rsi
	leaq	-184(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler16OperandGenerator17DefineSameAsFirstEPNS1_4NodeE
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rdx
	movl	$124, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	jmp	.L3342
	.p2align 4,,10
	.p2align 3
.L3346:
	cmpq	$0, 48(%rcx)
	sete	%al
	testb	%al, %al
	je	.L3345
	jmp	.L3350
	.p2align 4,,10
	.p2align 3
.L3348:
	movl	44(%rcx), %r15d
	jmp	.L3354
	.p2align 4,,10
	.p2align 3
.L3395:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE@PLT
	jmp	.L3342
	.p2align 4,,10
	.p2align 3
.L3367:
	cmpq	$0, 48(%rdx)
	sete	%al
	jmp	.L3371
	.p2align 4,,10
	.p2align 3
.L3397:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler16OperandGenerator16DefineAsRegisterEPNS1_4NodeE
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$214, %esi
	movq	-200(%rbp), %rcx
	movq	%rax, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	jmp	.L3342
	.p2align 4,,10
	.p2align 3
.L3396:
	movslq	-120(%rbp), %rbx
	salq	$32, %rbx
	orq	$3, %rbx
	jmp	.L3374
	.p2align 4,,10
	.p2align 3
.L3398:
	movslq	-120(%rbp), %rbx
	salq	$32, %rbx
	orq	$3, %rbx
	jmp	.L3360
	.p2align 4,,10
	.p2align 3
.L3375:
	leaq	144(%r14), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L3376
	.p2align 4,,10
	.p2align 3
.L3361:
	leaq	144(%rcx), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L3362
.L3399:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17413:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitInt32SubEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitInt32SubEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitInt64SubEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitInt64SubEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitInt64SubEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitInt64SubEPNS1_4NodeE:
.LFB17415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-192(%rbp), %rdi
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -168(%rbp)
	je	.L3401
	cmpq	$0, -176(%rbp)
	je	.L3422
.L3401:
	cmpb	$0, -144(%rbp)
	je	.L3403
	movq	-160(%rbp), %rax
	movq	(%rax), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L3404
	ja	.L3405
	cmpw	$23, %ax
	je	.L3406
	cmpw	$24, %ax
	jne	.L3403
	movq	48(%rdx), %rax
	movl	$4294967294, %edx
	addq	$2147483647, %rax
	cmpq	%rdx, %rax
	setbe	%al
	testb	%al, %al
	jne	.L3406
	.p2align 4,,10
	.p2align 3
.L3403:
	leaq	-128(%rbp), %rcx
	movl	$111, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$0, -128(%rbp)
	movq	$0, -112(%rbp)
	movl	$-1, -104(%rbp)
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
.L3400:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3423
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3405:
	.cfi_restore_state
	cmpw	$32, %ax
	jne	.L3403
.L3406:
	movl	-152(%rbp), %esi
	leaq	-128(%rbp), %r14
	movq	16(%r12), %rbx
	movq	%r14, %rdi
	negl	%esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-128(%rbp), %eax
	testl	%eax, %eax
	jne	.L3409
	cmpb	$19, -124(%rbp)
	je	.L3424
.L3409:
	movq	160(%rbx), %rsi
	movq	%rsi, %r15
	subq	152(%rbx), %r15
	sarq	$4, %r15
	cmpq	168(%rbx), %rsi
	je	.L3411
	movzbl	-124(%rbp), %ecx
	movq	-120(%rbp), %rdx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rbx)
.L3412:
	salq	$32, %r15
	orq	$11, %r15
.L3410:
	movq	-184(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	subq	$8, %rsp
	movl	%ebx, %edx
	xorl	%r9d, %r9d
	pushq	$0
	salq	$3, %rdx
	movq	%r15, %r8
	movq	%r14, %rcx
	movabsq	$927712935937, %rbx
	movl	$1256, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rax
	popq	%rdx
	jmp	.L3400
	.p2align 4,,10
	.p2align 3
.L3422:
	movq	-160(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$377957122049, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	salq	$3, %rdx
	movq	%r14, %rcx
	movl	$123, %esi
	movq	%r12, %rdi
	movabsq	$1065151889409, %rbx
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
	jmp	.L3400
	.p2align 4,,10
	.p2align 3
.L3404:
	cmpq	$0, 48(%rdx)
	sete	%al
	testb	%al, %al
	je	.L3403
	jmp	.L3406
	.p2align 4,,10
	.p2align 3
.L3424:
	movslq	-120(%rbp), %r15
	salq	$32, %r15
	orq	$3, %r15
	jmp	.L3410
	.p2align 4,,10
	.p2align 3
.L3411:
	leaq	144(%rbx), %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L3412
.L3423:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17415:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitInt64SubEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitInt64SubEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitInt64AddWithOverflowEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitInt64AddWithOverflowEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitInt64AddWithOverflowEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitInt64AddWithOverflowEPNS1_4NodeE:
.LFB17412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movl	$1, %esi
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties14FindProjectionEPNS1_4NodeEm@PLT
	testq	%rax, %rax
	je	.L3426
	movabsq	$85899345925, %rdx
	movq	$0, -80(%rbp)
	movq	%rdx, -96(%rbp)
	movl	$-1, -72(%rbp)
	movq	%rax, -64(%rbp)
.L3430:
	leaq	-96(%rbp), %rcx
	movl	$95, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3431
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3426:
	.cfi_restore_state
	movl	$0, -96(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	jmp	.L3430
.L3431:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17412:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitInt64AddWithOverflowEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitInt64AddWithOverflowEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitInt64SubWithOverflowEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitInt64SubWithOverflowEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitInt64SubWithOverflowEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitInt64SubWithOverflowEPNS1_4NodeE:
.LFB17416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movl	$1, %esi
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties14FindProjectionEPNS1_4NodeEm@PLT
	testq	%rax, %rax
	je	.L3433
	movabsq	$85899345925, %rdx
	movq	$0, -80(%rbp)
	movq	%rdx, -96(%rbp)
	movl	$-1, -72(%rbp)
	movq	%rax, -64(%rbp)
.L3437:
	leaq	-96(%rbp), %rcx
	movl	$111, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3438
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3433:
	.cfi_restore_state
	movl	$0, -96(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	jmp	.L3437
.L3438:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17416:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitInt64SubWithOverflowEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitInt64SubWithOverflowEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitInt32MulWithOverflowEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitInt32MulWithOverflowEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitInt32MulWithOverflowEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitInt32MulWithOverflowEPNS1_4NodeE:
.LFB17422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movl	$1, %esi
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties14FindProjectionEPNS1_4NodeEm@PLT
	testq	%rax, %rax
	je	.L3440
	movabsq	$85899345925, %rdx
	movq	$0, -80(%rbp)
	movq	%rdx, -96(%rbp)
	movl	$-1, -72(%rbp)
	movq	%rax, -64(%rbp)
.L3444:
	leaq	-96(%rbp), %rcx
	movl	$114, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3445
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3440:
	.cfi_restore_state
	movl	$0, -96(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	jmp	.L3444
.L3445:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17422:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitInt32MulWithOverflowEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitInt32MulWithOverflowEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitInt32AddWithOverflowEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitInt32AddWithOverflowEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitInt32AddWithOverflowEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitInt32AddWithOverflowEPNS1_4NodeE:
.LFB17546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movl	$1, %esi
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties14FindProjectionEPNS1_4NodeEm@PLT
	testq	%rax, %rax
	je	.L3447
	movabsq	$85899345925, %rdx
	movq	$0, -80(%rbp)
	movq	%rdx, -96(%rbp)
	movl	$-1, -72(%rbp)
	movq	%rax, -64(%rbp)
.L3451:
	leaq	-96(%rbp), %rcx
	movl	$96, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3452
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3447:
	.cfi_restore_state
	movl	$0, -96(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	jmp	.L3451
.L3452:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17546:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitInt32AddWithOverflowEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitInt32AddWithOverflowEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitInt32SubWithOverflowEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitInt32SubWithOverflowEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitInt32SubWithOverflowEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitInt32SubWithOverflowEPNS1_4NodeE:
.LFB17547:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movl	$1, %esi
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties14FindProjectionEPNS1_4NodeEm@PLT
	testq	%rax, %rax
	je	.L3454
	movabsq	$85899345925, %rdx
	movq	$0, -80(%rbp)
	movq	%rdx, -96(%rbp)
	movl	$-1, -72(%rbp)
	movq	%rax, -64(%rbp)
.L3458:
	leaq	-96(%rbp), %rcx
	movl	$112, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3459
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3454:
	.cfi_restore_state
	movl	$0, -96(%rbp)
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	jmp	.L3458
.L3459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17547:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitInt32SubWithOverflowEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitInt32SubWithOverflowEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_116VisitCompareZeroEPNS1_19InstructionSelectorEPNS1_4NodeES6_iPNS1_17FlagsContinuationE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_116VisitCompareZeroEPNS1_19InstructionSelectorEPNS1_4NodeES6_iPNS1_17FlagsContinuationE, @function
_ZN2v88internal8compiler12_GLOBAL__N_116VisitCompareZeroEPNS1_19InstructionSelectorEPNS1_4NodeES6_iPNS1_17FlagsContinuationE:
.LFB17532:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	movq	%rdi, -128(%rbp)
	subl	$1, %eax
	cmpl	$1, %eax
	ja	.L3462
	cmpl	$1, 4(%r8)
	ja	.L3462
	movq	(%rdx), %rax
	movzwl	16(%rax), %eax
	subw	$299, %ax
	cmpw	$28, %ax
	ja	.L3462
	leaq	.L3465(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_116VisitCompareZeroEPNS1_19InstructionSelectorEPNS1_4NodeES6_iPNS1_17FlagsContinuationE,"a",@progbits
	.align 4
	.align 4
.L3465:
	.long	.L3476-.L3465
	.long	.L3475-.L3465
	.long	.L3462-.L3465
	.long	.L3474-.L3465
	.long	.L3473-.L3465
	.long	.L3462-.L3465
	.long	.L3462-.L3465
	.long	.L3472-.L3465
	.long	.L3462-.L3465
	.long	.L3471-.L3465
	.long	.L3462-.L3465
	.long	.L3462-.L3465
	.long	.L3462-.L3465
	.long	.L3462-.L3465
	.long	.L3462-.L3465
	.long	.L3462-.L3465
	.long	.L3462-.L3465
	.long	.L3462-.L3465
	.long	.L3462-.L3465
	.long	.L3470-.L3465
	.long	.L3469-.L3465
	.long	.L3462-.L3465
	.long	.L3468-.L3465
	.long	.L3467-.L3465
	.long	.L3462-.L3465
	.long	.L3462-.L3465
	.long	.L3466-.L3465
	.long	.L3462-.L3465
	.long	.L3464-.L3465
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_116VisitCompareZeroEPNS1_19InstructionSelectorEPNS1_4NodeES6_iPNS1_17FlagsContinuationE
	.p2align 4,,10
	.p2align 3
.L3468:
	movq	%r12, %rdx
	call	_ZNK2v88internal8compiler19InstructionSelector27IsOnlyUserOfNodeInSameBlockEPNS1_4NodeES4_@PLT
	movq	%r14, %rcx
	movl	$125, %edx
	testb	%al, %al
	je	.L3462
.L3532:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_117TryVisitWordShiftINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELi64EEEbPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeEPNS1_17FlagsContinuationE
	testb	%al, %al
	jne	.L3460
	.p2align 4,,10
	.p2align 3
.L3462:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector14GetEffectLevelEPNS1_4NodeE@PLT
	movl	%eax, %r8d
	movl	(%r14), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L3535
	movq	(%r12), %rdi
	cmpw	$429, 16(%rdi)
	je	.L3536
.L3480:
	leaq	-128(%rbp), %rdi
	movq	%rbx, %rdx
	movl	%r15d, %esi
	movq	%r12, %rcx
	call	_ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i
	leaq	-112(%rbp), %r8
	movl	$0, %esi
	testb	%al, %al
	movq	-128(%rbp), %rax
	movq	%r8, %rdi
	movq	16(%rax), %rbx
	movq	%r8, -136(%rbp)
	jne	.L3537
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-112(%rbp), %eax
	movq	-136(%rbp), %r8
	testl	%eax, %eax
	jne	.L3488
	cmpb	$19, -108(%rbp)
	je	.L3538
.L3488:
	movq	160(%rbx), %rsi
	movq	%rsi, %rcx
	subq	152(%rbx), %rcx
	sarq	$4, %rcx
	cmpq	168(%rbx), %rsi
	je	.L3490
	movzbl	-108(%rbp), %edi
	movq	-104(%rbp), %rdx
	movl	%eax, (%rsi)
	movb	%dil, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rbx)
.L3491:
	salq	$32, %rcx
	orq	$11, %rcx
.L3489:
	movq	-128(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rcx, -136(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-128(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	movq	%r14, %r8
	movl	%r15d, %esi
	salq	$3, %rdx
	movq	-136(%rbp), %rcx
	movq	%r13, %rdi
	movabsq	$34359738369, %rbx
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandES3_PNS1_17FlagsContinuationE@PLT
.L3460:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3539
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3537:
	.cfi_restore_state
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-112(%rbp), %edx
	movq	-136(%rbp), %r8
	testl	%edx, %edx
	jne	.L3484
	cmpb	$19, -108(%rbp)
	je	.L3540
.L3484:
	movq	160(%rbx), %rsi
	movq	%rsi, %rax
	subq	152(%rbx), %rax
	sarq	$4, %rax
	cmpq	168(%rbx), %rsi
	je	.L3486
	movzbl	-108(%rbp), %edi
	movq	-104(%rbp), %rcx
	movl	%edx, (%rsi)
	movb	%dil, 4(%rsi)
	movq	%rcx, 8(%rsi)
	addq	$16, 160(%rbx)
.L3487:
	salq	$32, %rax
	movq	%rax, %rbx
	orq	$11, %rbx
.L3485:
	leaq	-96(%rbp), %r9
	pxor	%xmm0, %xmm0
	movq	%r8, %rcx
	movq	%r12, %rsi
	movq	%r9, %rdx
	leaq	-120(%rbp), %rdi
	movq	%r9, -136(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%r13, -120(%rbp)
	movq	$0, -112(%rbp)
	call	_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm
	subq	$8, %rsp
	movq	-112(%rbp), %rdx
	xorl	%ecx, %ecx
	pushq	%r14
	sall	$9, %eax
	movq	-136(%rbp), %r9
	movq	%r13, %rdi
	movl	%eax, %esi
	leaq	1(%rdx), %r8
	movq	%rbx, -96(%rbp,%rdx,8)
	xorl	%edx, %edx
	orl	%r15d, %esi
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L3460
	.p2align 4,,10
	.p2align 3
.L3535:
	movq	40(%r14), %rax
	movq	%r13, %rdi
	movq	136(%rax), %rax
	movq	(%rax), %rax
	movq	56(%rax), %rsi
	call	_ZNK2v88internal8compiler19InstructionSelector14GetEffectLevelEPNS1_4NodeE@PLT
	movq	(%r12), %rdi
	movl	%eax, %r8d
	cmpw	$429, 16(%rdi)
	jne	.L3480
.L3536:
	movl	%r8d, -136(%rbp)
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	movl	-136(%rbp), %r8d
	cmpb	$2, %al
	je	.L3481
	cmpb	$3, %al
	jne	.L3480
	cmpl	$100, %r15d
	sete	%r15b
	movzbl	%r15b, %r15d
	leal	99(%r15,%r15), %r15d
	jmp	.L3480
	.p2align 4,,10
	.p2align 3
.L3538:
	movslq	-104(%rbp), %rcx
	salq	$32, %rcx
	orq	$3, %rcx
	jmp	.L3489
	.p2align 4,,10
	.p2align 3
.L3540:
	movslq	-104(%rbp), %rbx
	salq	$32, %rbx
	orq	$3, %rbx
	jmp	.L3485
	.p2align 4,,10
	.p2align 3
.L3481:
	cmpl	$100, %r15d
	sete	%r15b
	movzbl	%r15b, %r15d
	leal	99(%r15,%r15,2), %r15d
	jmp	.L3480
	.p2align 4,,10
	.p2align 3
.L3486:
	movq	%r8, %rdx
	leaq	144(%rbx), %rdi
	movq	%rax, -144(%rbp)
	movq	%r8, -136(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %r8
	jmp	.L3487
	.p2align 4,,10
	.p2align 3
.L3490:
	leaq	144(%rbx), %rdi
	movq	%r8, %rdx
	movq	%rcx, -136(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-136(%rbp), %rcx
	jmp	.L3491
	.p2align 4,,10
	.p2align 3
.L3464:
	movq	%r12, %rdx
	call	_ZNK2v88internal8compiler19InstructionSelector27IsOnlyUserOfNodeInSameBlockEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L3462
	movq	%r14, %rcx
	movl	$111, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	jmp	.L3460
	.p2align 4,,10
	.p2align 3
.L3476:
	movq	%r12, %rdx
	call	_ZNK2v88internal8compiler19InstructionSelector27IsOnlyUserOfNodeInSameBlockEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L3462
	movq	%r14, %rcx
	movl	$98, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	jmp	.L3460
	.p2align 4,,10
	.p2align 3
.L3475:
	movq	%r12, %rdx
	call	_ZNK2v88internal8compiler19InstructionSelector27IsOnlyUserOfNodeInSameBlockEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L3462
	movq	%r14, %rcx
	movl	$108, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	jmp	.L3460
	.p2align 4,,10
	.p2align 3
.L3474:
	movq	%r12, %rdx
	call	_ZNK2v88internal8compiler19InstructionSelector27IsOnlyUserOfNodeInSameBlockEPNS1_4NodeES4_@PLT
	movq	%r14, %rcx
	movl	$126, %edx
	testb	%al, %al
	je	.L3462
.L3534:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_117TryVisitWordShiftINS1_12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES8_EELi32EEEbPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeEPNS1_17FlagsContinuationE
	testb	%al, %al
	je	.L3462
	jmp	.L3460
	.p2align 4,,10
	.p2align 3
.L3473:
	movq	%r12, %rdx
	call	_ZNK2v88internal8compiler19InstructionSelector27IsOnlyUserOfNodeInSameBlockEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L3462
	movq	%r14, %rcx
	movl	$128, %edx
	jmp	.L3534
	.p2align 4,,10
	.p2align 3
.L3472:
	movq	%r12, %rdx
	call	_ZNK2v88internal8compiler19InstructionSelector27IsOnlyUserOfNodeInSameBlockEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L3462
	movq	%r14, %rcx
	movl	$96, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	jmp	.L3460
	.p2align 4,,10
	.p2align 3
.L3471:
	movq	%r12, %rdx
	call	_ZNK2v88internal8compiler19InstructionSelector27IsOnlyUserOfNodeInSameBlockEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L3462
	movq	%r14, %rcx
	movl	$112, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	jmp	.L3460
	.p2align 4,,10
	.p2align 3
.L3470:
	movq	%r12, %rdx
	call	_ZNK2v88internal8compiler19InstructionSelector27IsOnlyUserOfNodeInSameBlockEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L3462
	movq	%r14, %rcx
	movl	$97, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	jmp	.L3460
	.p2align 4,,10
	.p2align 3
.L3469:
	movq	%r12, %rdx
	call	_ZNK2v88internal8compiler19InstructionSelector27IsOnlyUserOfNodeInSameBlockEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L3462
	movq	%r14, %rcx
	movl	$107, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	jmp	.L3460
	.p2align 4,,10
	.p2align 3
.L3467:
	movq	%r12, %rdx
	call	_ZNK2v88internal8compiler19InstructionSelector27IsOnlyUserOfNodeInSameBlockEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L3462
	movq	%r14, %rcx
	movl	$127, %edx
	jmp	.L3532
	.p2align 4,,10
	.p2align 3
.L3466:
	movq	%r12, %rdx
	call	_ZNK2v88internal8compiler19InstructionSelector27IsOnlyUserOfNodeInSameBlockEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L3462
	movq	%r14, %rcx
	movl	$95, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	jmp	.L3460
.L3539:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17532:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_116VisitCompareZeroEPNS1_19InstructionSelectorEPNS1_4NodeES6_iPNS1_17FlagsContinuationE, .-_ZN2v88internal8compiler12_GLOBAL__N_116VisitCompareZeroEPNS1_19InstructionSelectorEPNS1_4NodeES6_iPNS1_17FlagsContinuationE
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_116VisitWordCompareEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"MachineSemantic::kInt32 == left_type.semantic()"
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_116VisitWordCompareEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWordCompareEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE, @function
_ZN2v88internal8compiler12_GLOBAL__N_116VisitWordCompareEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE:
.LFB17530:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -144(%rbp)
	movq	32(%rsi), %r12
	movq	%rcx, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movq	%rdi, -120(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3542
	leaq	40(%rsi), %rax
.L3543:
	movq	(%rax), %r15
	leal	-100(%r13), %eax
	movl	%r13d, -152(%rbp)
	andl	$-5, %eax
	movl	%eax, -156(%rbp)
	jne	.L3544
	movq	(%r12), %rax
	cmpw	$473, 16(%rax)
	je	.L3642
.L3545:
	movq	(%r15), %rax
	cmpw	$473, 16(%rax)
	je	.L3643
.L3544:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_120MachineTypeForNarrowEPNS1_4NodeES4_
	movq	%r12, %rsi
	movq	%r15, %rdi
	movb	%al, -148(%rbp)
	movzbl	%ah, %ebx
	call	_ZN2v88internal8compiler12_GLOBAL__N_120MachineTypeForNarrowEPNS1_4NodeES4_
	movzbl	%ah, %edx
	cmpb	%bl, %dl
	jne	.L3548
	cmpb	%al, -148(%rbp)
	je	.L3644
.L3548:
	movq	-144(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler19InstructionSelector14GetEffectLevelEPNS1_4NodeE@PLT
	movl	%eax, %r8d
	movq	-136(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -148(%rbp)
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L3645
	movq	(%r15), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L3566
.L3651:
	ja	.L3567
	cmpw	$23, %ax
	je	.L3568
	cmpw	$24, %ax
	jne	.L3570
	movq	48(%rdx), %rax
	movl	$4294967294, %edx
	addq	$2147483647, %rax
	cmpq	%rdx, %rax
	setbe	%al
	testb	%al, %al
	jne	.L3568
.L3570:
	movq	(%r12), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L3572
	.p2align 4,,10
	.p2align 3
.L3650:
	ja	.L3573
	cmpw	$23, %ax
	je	.L3574
	cmpw	$24, %ax
	jne	.L3568
	movq	48(%rdx), %rax
	movl	$4294967294, %edx
	addq	$2147483647, %rax
	cmpq	%rdx, %rax
	setbe	%al
	testb	%al, %al
	jne	.L3646
	.p2align 4,,10
	.p2align 3
.L3568:
	leaq	-120(%rbp), %rbx
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	movl	%r8d, -148(%rbp)
	call	_ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i
	movl	-148(%rbp), %r8d
	testb	%al, %al
	jne	.L3647
.L3579:
	movq	(%r15), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L3581
	ja	.L3582
	cmpw	$23, %ax
	je	.L3583
	cmpw	$24, %ax
	jne	.L3585
	movq	48(%rdx), %rax
	movl	$4294967294, %edx
	addq	$2147483647, %rax
	cmpq	%rdx, %rax
	setbe	%al
	testb	%al, %al
	jne	.L3583
	.p2align 4,,10
	.p2align 3
.L3585:
	movq	%r12, %rcx
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i
	testb	%al, %al
	jne	.L3648
	movq	(%r14), %rax
	movq	-136(%rbp), %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	-144(%rbp), %rdi
	movl	%r13d, %esi
	movzbl	18(%rax), %r9d
	andl	$1, %r9d
	call	_ZN2v88internal8compiler12_GLOBAL__N_112VisitCompareEPNS1_19InstructionSelectorEiPNS1_4NodeES6_PNS1_17FlagsContinuationEb
.L3541:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3649
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3567:
	.cfi_restore_state
	cmpw	$32, %ax
	je	.L3568
	movq	(%r12), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	jne	.L3650
.L3572:
	cmpq	$0, 48(%rdx)
	sete	%al
	testb	%al, %al
	je	.L3568
	jmp	.L3646
	.p2align 4,,10
	.p2align 3
.L3582:
	cmpw	$32, %ax
	jne	.L3585
.L3583:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	movq	%r12, %rcx
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i
	movq	%r15, %rsi
	movq	%rbx, %rdi
	testb	%al, %al
	je	.L3587
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	leaq	-96(%rbp), %r13
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	leaq	-104(%rbp), %rcx
	leaq	-112(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rax, %rbx
	movq	-144(%rbp), %r15
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%r15, -112(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm
	movq	-104(%rbp), %rdx
	subq	$8, %rsp
	xorl	%ecx, %ecx
	pushq	-136(%rbp)
	sall	$9, %eax
	movq	%r13, %r9
	movq	%r15, %rdi
	movl	-152(%rbp), %esi
	leaq	1(%rdx), %r8
	movq	%rbx, -96(%rbp,%rdx,8)
	xorl	%edx, %edx
	movq	%r8, -104(%rbp)
	orl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L3541
	.p2align 4,,10
	.p2align 3
.L3581:
	cmpq	$0, 48(%rdx)
	sete	%al
	testb	%al, %al
	je	.L3585
	jmp	.L3583
	.p2align 4,,10
	.p2align 3
.L3573:
	cmpw	$32, %ax
	jne	.L3568
.L3574:
	movq	(%r14), %rax
	leaq	-120(%rbp), %rbx
	testb	$1, 18(%rax)
	je	.L3578
	movq	%r12, %rax
	leaq	-120(%rbp), %rbx
	movq	%r15, %r12
	movq	%rax, %r15
	jmp	.L3583
	.p2align 4,,10
	.p2align 3
.L3542:
	leaq	16(%r12), %rax
	movq	16(%r12), %r12
	addq	$8, %rax
	jmp	.L3543
	.p2align 4,,10
	.p2align 3
.L3645:
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rdi
	movq	40(%rax), %rax
	movq	136(%rax), %rax
	movq	(%rax), %rax
	movq	56(%rax), %rsi
	call	_ZNK2v88internal8compiler19InstructionSelector14GetEffectLevelEPNS1_4NodeE@PLT
	movq	(%r15), %rdx
	movl	%eax, %r8d
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	jne	.L3651
.L3566:
	cmpq	$0, 48(%rdx)
	sete	%al
	testb	%al, %al
	jne	.L3568
	jmp	.L3570
	.p2align 4,,10
	.p2align 3
.L3587:
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler16OperandGenerator3UseEPNS1_4NodeE
	movq	-136(%rbp), %r8
	movq	%r14, %rcx
	movl	%r13d, %esi
	movq	-144(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandES3_PNS1_17FlagsContinuationE@PLT
	jmp	.L3541
	.p2align 4,,10
	.p2align 3
.L3647:
	movq	%r12, %rcx
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler19X64OperandGenerator18CanBeMemoryOperandEiPNS1_4NodeES4_i
	movl	-148(%rbp), %r8d
	testb	%al, %al
	jne	.L3579
	movq	(%r14), %rax
	testb	$1, 18(%rax)
	je	.L3578
.L3652:
	movq	%r12, %rax
	movq	%r15, %r12
	movq	%rax, %r15
	jmp	.L3579
	.p2align 4,,10
	.p2align 3
.L3646:
	movq	(%r14), %rax
	leaq	-120(%rbp), %rbx
	testb	$1, 18(%rax)
	jne	.L3652
	jmp	.L3578
	.p2align 4,,10
	.p2align 3
.L3644:
	movzbl	-148(%rbp), %eax
	cmpb	$2, %al
	ja	.L3549
	testb	%al, %al
	je	.L3548
	cmpl	$104, %r13d
	je	.L3591
	movl	-156(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L3548
	cmpb	$3, %bl
	je	.L3653
	movl	$102, -152(%rbp)
	movl	$102, %r13d
	cmpb	$2, %bl
	je	.L3548
.L3564:
	leaq	.LC5(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3642:
	movq	-144(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L3545
	movzbl	23(%r12), %eax
	movq	32(%r12), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3545
	movq	16(%r12), %r12
	jmp	.L3545
	.p2align 4,,10
	.p2align 3
.L3643:
	movq	-144(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L3544
	movzbl	23(%r15), %eax
	movq	32(%r15), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3544
	movq	16(%r15), %r15
	jmp	.L3544
	.p2align 4,,10
	.p2align 3
.L3578:
	movq	-136(%rbp), %rax
	movl	%r8d, -148(%rbp)
	movl	4(%rax), %edi
	call	_ZN2v88internal8compiler21CommuteFlagsConditionENS1_14FlagsConditionE@PLT
	movq	-136(%rbp), %rcx
	movl	-148(%rbp), %r8d
	movl	%eax, 4(%rcx)
	movq	%r12, %rax
	movq	%r15, %r12
	movq	%rax, %r15
	jmp	.L3579
	.p2align 4,,10
	.p2align 3
.L3648:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	leaq	-96(%rbp), %r13
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	pxor	%xmm0, %xmm0
	leaq	-104(%rbp), %rcx
	movq	%r13, %rdx
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, %rbx
	movaps	%xmm0, -96(%rbp)
	movq	-144(%rbp), %r14
	movaps	%xmm0, -80(%rbp)
	movq	$0, -104(%rbp)
	movq	%r14, -112(%rbp)
	call	_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm
	movq	-104(%rbp), %rdx
	subq	$8, %rsp
	movq	%r13, %r9
	pushq	-136(%rbp)
	sall	$9, %eax
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movl	-152(%rbp), %esi
	leaq	1(%rdx), %r8
	movq	%rbx, -96(%rbp,%rdx,8)
	xorl	%edx, %edx
	movq	%r8, -104(%rbp)
	orl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L3541
	.p2align 4,,10
	.p2align 3
.L3549:
	cmpb	$3, -148(%rbp)
	jne	.L3548
	cmpl	$104, %r13d
	je	.L3593
	movl	-156(%rbp), %edi
	testl	%edi, %edi
	jne	.L3548
	cmpb	$3, %bl
	je	.L3654
	cmpb	$2, %bl
	jne	.L3564
.L3639:
	movl	$101, -152(%rbp)
	movl	$101, %r13d
	jmp	.L3548
.L3593:
	movl	$105, -152(%rbp)
	movl	$105, %r13d
	jmp	.L3548
.L3591:
	movl	$106, -152(%rbp)
	movl	$106, %r13d
	jmp	.L3548
.L3653:
	movq	-136(%rbp), %rax
	movl	4(%rax), %eax
	cmpl	$4, %eax
	je	.L3553
	ja	.L3554
	cmpl	$2, %eax
	je	.L3555
	cmpl	$3, %eax
	jne	.L3638
	movq	-136(%rbp), %rax
	movl	$102, %r13d
	movl	$102, -152(%rbp)
	movl	$7, 4(%rax)
	jmp	.L3548
.L3654:
	movq	-136(%rbp), %rax
	movl	4(%rax), %eax
	cmpl	$4, %eax
	je	.L3559
	ja	.L3560
	cmpl	$2, %eax
	je	.L3561
	cmpl	$3, %eax
	jne	.L3639
	movq	-136(%rbp), %rax
	movl	$101, %r13d
	movl	$101, -152(%rbp)
	movl	$7, 4(%rax)
	jmp	.L3548
.L3554:
	cmpl	$5, %eax
	jne	.L3638
	movq	-136(%rbp), %rax
	movl	$102, %r13d
	movl	$102, -152(%rbp)
	movl	$9, 4(%rax)
	jmp	.L3548
.L3560:
	cmpl	$5, %eax
	jne	.L3639
	movq	-136(%rbp), %rax
	movl	$101, %r13d
	movl	$101, -152(%rbp)
	movl	$9, 4(%rax)
	jmp	.L3548
.L3553:
	movq	-136(%rbp), %rax
	movl	$8, 4(%rax)
.L3638:
	movl	$102, -152(%rbp)
	movl	$102, %r13d
	jmp	.L3548
.L3649:
	call	__stack_chk_fail@PLT
.L3555:
	movq	-136(%rbp), %rax
	movl	$102, %r13d
	movl	$102, -152(%rbp)
	movl	$6, 4(%rax)
	jmp	.L3548
.L3559:
	movq	-136(%rbp), %rax
	movl	$101, %r13d
	movl	$101, -152(%rbp)
	movl	$8, 4(%rax)
	jmp	.L3548
.L3561:
	movq	-136(%rbp), %rax
	movl	$101, %r13d
	movl	$101, -152(%rbp)
	movl	$6, 4(%rax)
	jmp	.L3548
	.cfi_endproc
.LFE17530:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWordCompareEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE, .-_ZN2v88internal8compiler12_GLOBAL__N_116VisitWordCompareEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_118VisitWord64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_118VisitWord64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE, @function
_ZN2v88internal8compiler12_GLOBAL__N_118VisitWord64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE:
.LFB17531:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -88(%rbp)
	call	_ZNK2v88internal8compiler19InstructionSelector19CanUseRootsRegisterEv@PLT
	testb	%al, %al
	je	.L3656
	movzbl	23(%r13), %esi
	movq	16(%r12), %rdx
	movq	32(%r13), %r15
	andl	$15, %esi
	movq	(%rdx), %rbx
	leaq	32(%r13), %rdx
	cmpl	$15, %esi
	je	.L3657
	movq	(%r15), %rdi
	movzwl	16(%rdi), %esi
	cmpw	$30, %si
	sete	%cl
	je	.L3658
.L3705:
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L3661:
	movq	8(%rdx), %r9
	addq	$8, %rdx
	movq	(%r9), %rdx
	cmpw	$30, 16(%rdx)
	jne	.L3663
	movq	48(%rdx), %rdi
	leaq	4856(%rbx), %rdx
	leaq	56(%rbx), %rax
	cmpq	%rdi, %rdx
	jbe	.L3664
	cmpq	%rdi, %rax
	ja	.L3664
.L3665:
	subq	%rax, %rdi
	movq	0(%r13), %rax
	movq	%rdi, %rbx
	shrq	$3, %rbx
	testb	$1, 18(%rax)
	je	.L3701
.L3668:
	leaq	-88(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	movl	%ebx, %edi
	movq	%rax, %r13
	jmp	.L3700
	.p2align 4,,10
	.p2align 3
.L3663:
	movq	0(%r13), %rdx
	testb	$1, 18(%rdx)
	je	.L3664
	testb	%al, %al
	jne	.L3702
.L3664:
	testb	%cl, %cl
	jne	.L3669
.L3656:
	movq	%r14, %rcx
	movl	$99, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWordCompareEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
.L3655:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3703
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3658:
	.cfi_restore_state
	movq	48(%rdi), %r8
	jmp	.L3661
	.p2align 4,,10
	.p2align 3
.L3669:
	leaq	56(%rbx), %r15
	cmpq	%r8, %r15
	ja	.L3656
	addq	$4856, %rbx
	movq	%r8, -104(%rbp)
	cmpq	%r8, %rbx
	jbe	.L3656
	leaq	-88(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	movq	-104(%rbp), %r8
	movq	%rax, %r13
	movq	%r8, %rdi
	subq	%r15, %rdi
	shrq	$3, %rdi
.L3700:
	call	_ZN2v88internal18TurboAssemblerBase30RootRegisterOffsetForRootIndexENS0_9RootIndexE@PLT
	leaq	-80(%rbp), %rdx
	movl	%eax, %esi
	movq	-88(%rbp), %rax
	movq	%rdx, %rdi
	movq	16(%rax), %r15
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	movq	-104(%rbp), %rdx
	testl	%eax, %eax
	jne	.L3675
	cmpb	$19, -76(%rbp)
	je	.L3704
.L3675:
	movq	160(%r15), %rsi
	movq	%rsi, %rbx
	subq	152(%r15), %rbx
	sarq	$4, %rbx
	cmpq	168(%r15), %rsi
	je	.L3677
	movzbl	-76(%rbp), %ecx
	movq	-72(%rbp), %rdx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%r15)
.L3678:
	salq	$32, %rbx
	movq	%rbx, %rdx
	orq	$11, %rdx
.L3676:
	movq	%r14, %r8
	movq	%r13, %rcx
	movl	$9827, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandES3_PNS1_17FlagsContinuationE@PLT
	jmp	.L3655
	.p2align 4,,10
	.p2align 3
.L3657:
	leaq	16(%r15), %rdx
	movq	16(%r15), %r15
	movq	(%r15), %rdi
	movzwl	16(%rdi), %esi
	cmpw	$30, %si
	sete	%cl
	je	.L3658
	jmp	.L3705
	.p2align 4,,10
	.p2align 3
.L3702:
	movq	%r9, %rsi
	movq	%r13, %rdi
	movq	%r8, -104(%rbp)
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	movq	-104(%rbp), %r8
	leaq	56(%rbx), %rax
	addq	$4856, %rbx
	cmpq	%r8, %rbx
	jbe	.L3656
	cmpq	%r8, %rax
	ja	.L3656
	movq	-112(%rbp), %r9
	movq	%r8, %rdi
	movq	%r9, %r15
	jmp	.L3665
	.p2align 4,,10
	.p2align 3
.L3704:
	movslq	-72(%rbp), %rdx
	salq	$32, %rdx
	orq	$3, %rdx
	jmp	.L3676
	.p2align 4,,10
	.p2align 3
.L3701:
	movl	4(%r14), %edi
	call	_ZN2v88internal8compiler21CommuteFlagsConditionENS1_14FlagsConditionE@PLT
	movl	%eax, 4(%r14)
	jmp	.L3668
	.p2align 4,,10
	.p2align 3
.L3677:
	leaq	144(%r15), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L3678
.L3703:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17531:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_118VisitWord64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE, .-_ZN2v88internal8compiler12_GLOBAL__N_118VisitWord64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
	.section	.text._ZN2v88internal8compiler19InstructionSelector18VisitInt64LessThanEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector18VisitInt64LessThanEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector18VisitInt64LessThanEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector18VisitInt64LessThanEPNS1_4NodeE:
.LFB17548:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rdx
	movq	$0, -64(%rbp)
	movabsq	$8589934597, %rax
	movq	%rax, -80(%rbp)
	movl	$-1, -56(%rbp)
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_118VisitWord64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3709
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3709:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17548:
	.size	_ZN2v88internal8compiler19InstructionSelector18VisitInt64LessThanEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector18VisitInt64LessThanEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitInt64LessThanOrEqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitInt64LessThanOrEqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitInt64LessThanOrEqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitInt64LessThanOrEqualEPNS1_4NodeE:
.LFB17549:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rdx
	movq	$0, -64(%rbp)
	movabsq	$17179869189, %rax
	movq	%rax, -80(%rbp)
	movl	$-1, -56(%rbp)
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_118VisitWord64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3713
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3713:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17549:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitInt64LessThanOrEqualEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitInt64LessThanOrEqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector19VisitUint64LessThanEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector19VisitUint64LessThanEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector19VisitUint64LessThanEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector19VisitUint64LessThanEPNS1_4NodeE:
.LFB17550:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rdx
	movq	$0, -64(%rbp)
	movabsq	$25769803781, %rax
	movq	%rax, -80(%rbp)
	movl	$-1, -56(%rbp)
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_118VisitWord64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3717
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3717:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17550:
	.size	_ZN2v88internal8compiler19InstructionSelector19VisitUint64LessThanEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector19VisitUint64LessThanEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector26VisitUint64LessThanOrEqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector26VisitUint64LessThanOrEqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector26VisitUint64LessThanOrEqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector26VisitUint64LessThanOrEqualEPNS1_4NodeE:
.LFB17551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rdx
	movq	$0, -64(%rbp)
	movabsq	$34359738373, %rax
	movq	%rax, -80(%rbp)
	movl	$-1, -56(%rbp)
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_118VisitWord64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3721
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3721:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17551:
	.size	_ZN2v88internal8compiler19InstructionSelector26VisitUint64LessThanOrEqualEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector26VisitUint64LessThanOrEqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector18VisitInt32LessThanEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector18VisitInt32LessThanEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector18VisitInt32LessThanEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector18VisitInt32LessThanEPNS1_4NodeE:
.LFB17541:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$100, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rcx
	movq	$0, -64(%rbp)
	movabsq	$8589934597, %rax
	movq	%rax, -80(%rbp)
	movl	$-1, -56(%rbp)
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWordCompareEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3725
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3725:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17541:
	.size	_ZN2v88internal8compiler19InstructionSelector18VisitInt32LessThanEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector18VisitInt32LessThanEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitInt32LessThanOrEqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitInt32LessThanOrEqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitInt32LessThanOrEqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitInt32LessThanOrEqualEPNS1_4NodeE:
.LFB17542:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$100, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rcx
	movq	$0, -64(%rbp)
	movabsq	$17179869189, %rax
	movq	%rax, -80(%rbp)
	movl	$-1, -56(%rbp)
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWordCompareEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3729
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3729:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17542:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitInt32LessThanOrEqualEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitInt32LessThanOrEqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector19VisitUint32LessThanEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector19VisitUint32LessThanEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector19VisitUint32LessThanEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector19VisitUint32LessThanEPNS1_4NodeE:
.LFB17543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$100, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rcx
	movq	$0, -64(%rbp)
	movabsq	$25769803781, %rax
	movq	%rax, -80(%rbp)
	movl	$-1, -56(%rbp)
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWordCompareEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3733
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3733:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17543:
	.size	_ZN2v88internal8compiler19InstructionSelector19VisitUint32LessThanEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector19VisitUint32LessThanEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector26VisitUint32LessThanOrEqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector26VisitUint32LessThanOrEqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector26VisitUint32LessThanOrEqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector26VisitUint32LessThanOrEqualEPNS1_4NodeE:
.LFB17544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$100, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rcx
	movq	$0, -64(%rbp)
	movabsq	$34359738373, %rax
	movq	%rax, -80(%rbp)
	movl	$-1, -56(%rbp)
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWordCompareEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3737
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3737:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17544:
	.size	_ZN2v88internal8compiler19InstructionSelector26VisitUint32LessThanOrEqualEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector26VisitUint32LessThanOrEqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitWord64EqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitWord64EqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitWord64EqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitWord64EqualEPNS1_4NodeE:
.LFB17545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	leaq	-176(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$5, -112(%rbp)
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -128(%rbp)
	je	.L3741
	cmpq	$0, -136(%rbp)
	je	.L3749
.L3741:
	leaq	-112(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_118VisitWord64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
.L3738:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3750
	addq	$152, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3749:
	.cfi_restore_state
	movq	-168(%rbp), %r14
	movq	-176(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r14, %rdx
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L3741
	movq	(%r14), %rax
	movzwl	16(%rax), %eax
	cmpw	$318, %ax
	je	.L3740
	cmpw	$327, %ax
	jne	.L3741
	leaq	-112(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_118VisitWord64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
	jmp	.L3738
	.p2align 4,,10
	.p2align 3
.L3740:
	leaq	-112(%rbp), %rcx
	movl	$103, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWordCompareEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	jmp	.L3738
.L3750:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17545:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitWord64EqualEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitWord64EqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20VisitWordCompareZeroEPNS1_4NodeES4_PNS1_17FlagsContinuationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20VisitWordCompareZeroEPNS1_4NodeES4_PNS1_17FlagsContinuationE
	.type	_ZN2v88internal8compiler19InstructionSelector20VisitWordCompareZeroEPNS1_4NodeES4_PNS1_17FlagsContinuationE, @function
_ZN2v88internal8compiler19InstructionSelector20VisitWordCompareZeroEPNS1_4NodeES4_PNS1_17FlagsContinuationE:
.LFB17538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	cmpw	$334, 16(%rax)
	jne	.L3752
	leaq	-112(%rbp), %rbx
.L3754:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L3752
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIiLNS1_8IrOpcode5ValueE23EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -76(%rbp)
	je	.L3752
	movl	-80(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L3865
.L3752:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L3780
	movq	(%r15), %rdi
	movzwl	16(%rdi), %eax
	cmpw	$349, %ax
	ja	.L3756
	cmpw	$298, %ax
	jbe	.L3866
	subw	$299, %ax
	cmpw	$50, %ax
	ja	.L3780
	leaq	.L3761(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector20VisitWordCompareZeroEPNS1_4NodeES4_PNS1_17FlagsContinuationE,"a",@progbits
	.align 4
	.align 4
.L3761:
	.long	.L3778-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3777-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3780-.L3761
	.long	.L3776-.L3761
	.long	.L3775-.L3761
	.long	.L3774-.L3761
	.long	.L3773-.L3761
	.long	.L3772-.L3761
	.long	.L3771-.L3761
	.long	.L3770-.L3761
	.long	.L3769-.L3761
	.long	.L3768-.L3761
	.long	.L3767-.L3761
	.long	.L3766-.L3761
	.long	.L3765-.L3761
	.long	.L3764-.L3761
	.long	.L3763-.L3761
	.long	.L3762-.L3761
	.long	.L3760-.L3761
	.section	.text._ZN2v88internal8compiler19InstructionSelector20VisitWordCompareZeroEPNS1_4NodeES4_PNS1_17FlagsContinuationE
	.p2align 4,,10
	.p2align 3
.L3756:
	cmpw	$511, %ax
	jne	.L3780
	movl	4(%r14), %eax
	testl	%eax, %eax
	je	.L3846
	movl	$9, 4(%r14)
.L3847:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector28VisitStackPointerGreaterThanEPNS1_4NodeEPNS1_17FlagsContinuationE
	jmp	.L3751
	.p2align 4,,10
	.p2align 3
.L3866:
	cmpw	$55, %ax
	jne	.L3780
	call	_ZN2v88internal8compiler17ProjectionIndexOfEPKNS1_8OperatorE@PLT
	cmpq	$1, %rax
	jne	.L3780
	movzbl	23(%r15), %eax
	movq	32(%r15), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3827
	movq	16(%rbx), %rbx
.L3827:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14FindProjectionEPNS1_4NodeEm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3835
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector9IsDefinedEPNS1_4NodeE@PLT
	testb	%al, %al
	je	.L3780
.L3835:
	movq	(%rbx), %rax
	movzwl	16(%rax), %eax
	subw	$307, %ax
	cmpw	$21, %ax
	ja	.L3780
	leaq	.L3830(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector20VisitWordCompareZeroEPNS1_4NodeES4_PNS1_17FlagsContinuationE
	.align 4
	.align 4
.L3830:
	.long	.L3834-.L3830
	.long	.L3780-.L3830
	.long	.L3833-.L3830
	.long	.L3780-.L3830
	.long	.L3832-.L3830
	.long	.L3780-.L3830
	.long	.L3780-.L3830
	.long	.L3780-.L3830
	.long	.L3780-.L3830
	.long	.L3780-.L3830
	.long	.L3780-.L3830
	.long	.L3780-.L3830
	.long	.L3780-.L3830
	.long	.L3780-.L3830
	.long	.L3780-.L3830
	.long	.L3780-.L3830
	.long	.L3780-.L3830
	.long	.L3780-.L3830
	.long	.L3780-.L3830
	.long	.L3831-.L3830
	.long	.L3780-.L3830
	.long	.L3829-.L3830
	.section	.text._ZN2v88internal8compiler19InstructionSelector20VisitWordCompareZeroEPNS1_4NodeES4_PNS1_17FlagsContinuationE
.L3780:
	movq	%r14, %r8
	movl	$100, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116VisitCompareZeroEPNS1_19InstructionSelectorEPNS1_4NodeES6_iPNS1_17FlagsContinuationE
.L3751:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3867
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3776:
	.cfi_restore_state
	movl	4(%r14), %r8d
	testl	%r8d, %r8d
	je	.L3781
	movl	$0, 4(%r14)
.L3777:
	movq	%r14, %rcx
	movl	$100, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWordCompareEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	jmp	.L3751
	.p2align 4,,10
	.p2align 3
.L3865:
	movq	-104(%rbp), %rax
	xorl	$1, 4(%r14)
	movq	%r15, %r13
	movq	(%rax), %rdx
	movq	%rax, %r15
	cmpw	$334, 16(%rdx)
	je	.L3754
	jmp	.L3752
.L3770:
	movl	4(%r14), %eax
	testl	%eax, %eax
	je	.L3798
	movl	$2, 4(%r14)
	.p2align 4,,10
	.p2align 3
.L3805:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_118VisitWord64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
	jmp	.L3751
.L3760:
	movl	4(%r14), %r9d
	testl	%r9d, %r9d
	je	.L3825
	movl	$7, 4(%r14)
	.p2align 4,,10
	.p2align 3
.L3826:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119VisitFloat64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
	jmp	.L3751
.L3774:
	movl	4(%r14), %edi
	testl	%edi, %edi
	je	.L3784
	movl	$2, 4(%r14)
	jmp	.L3777
.L3766:
	movl	4(%r14), %r13d
	testl	%r13d, %r13d
	je	.L3806
	movl	$18, 4(%r14)
	.p2align 4,,10
	.p2align 3
.L3811:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119VisitFloat32CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
	jmp	.L3751
.L3778:
	movq	%r14, %rcx
	movl	$104, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWordCompareEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	jmp	.L3751
.L3764:
	movl	4(%r14), %r11d
	testl	%r11d, %r11d
	je	.L3810
	movl	$7, 4(%r14)
	jmp	.L3811
.L3765:
	movl	4(%r14), %ebx
	testl	%ebx, %ebx
	je	.L3808
	movl	$9, 4(%r14)
	jmp	.L3811
.L3772:
	movl	4(%r14), %ecx
	testl	%ecx, %ecx
	je	.L3788
	movl	$6, 4(%r14)
	jmp	.L3777
.L3768:
	movl	4(%r14), %eax
	testl	%eax, %eax
	je	.L3802
	movl	$6, 4(%r14)
	jmp	.L3805
.L3762:
	leaq	-112(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_12FloatMatcherIdLNS1_8IrOpcode5ValueE26EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -88(%rbp)
	movl	4(%r14), %eax
	je	.L3815
	pxor	%xmm0, %xmm0
	ucomisd	-96(%rbp), %xmm0
	jp	.L3815
	jne	.L3815
	movq	-80(%rbp), %rdx
	movq	(%rdx), %rcx
	cmpw	$372, 16(%rcx)
	je	.L3817
	.p2align 4,,10
	.p2align 3
.L3815:
	testl	%eax, %eax
	je	.L3818
	movl	$9, 4(%r14)
	jmp	.L3826
.L3763:
	movl	4(%r14), %r10d
	testl	%r10d, %r10d
	je	.L3812
	movl	$18, 4(%r14)
	jmp	.L3826
.L3771:
	movl	4(%r14), %edx
	testl	%edx, %edx
	je	.L3790
	movl	$8, 4(%r14)
	jmp	.L3777
.L3767:
	movl	4(%r14), %eax
	testl	%eax, %eax
	je	.L3804
	movl	$8, 4(%r14)
	jmp	.L3805
.L3775:
	movl	4(%r14), %eax
	testl	%eax, %eax
	je	.L3792
	movl	$0, 4(%r14)
.L3793:
	leaq	-112(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -64(%rbp)
	je	.L3805
	cmpq	$0, -72(%rbp)
	jne	.L3805
	movq	-112(%rbp), %r15
	movq	-104(%rbp), %r13
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L3795
	movq	0(%r13), %rax
	movzwl	16(%rax), %eax
	cmpw	$318, %ax
	je	.L3796
	cmpw	$327, %ax
	jne	.L3795
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_118VisitWord64CompareEPNS1_19InstructionSelectorEPNS1_4NodeEPNS1_17FlagsContinuationE
	jmp	.L3751
.L3769:
	movl	4(%r14), %eax
	testl	%eax, %eax
	je	.L3800
	movl	$4, 4(%r14)
	jmp	.L3805
.L3773:
	movl	4(%r14), %esi
	testl	%esi, %esi
	je	.L3786
	movl	$4, 4(%r14)
	jmp	.L3777
.L3829:
	movl	4(%r14), %edx
	testl	%edx, %edx
	je	.L3844
	movl	$20, 4(%r14)
.L3845:
	movq	%r14, %rcx
	movl	$111, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	jmp	.L3751
.L3831:
	movl	4(%r14), %ecx
	testl	%ecx, %ecx
	je	.L3842
	movl	$20, 4(%r14)
.L3843:
	movq	%r14, %rcx
	movl	$95, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	jmp	.L3751
.L3832:
	movl	4(%r14), %esi
	testl	%esi, %esi
	je	.L3840
	movl	$20, 4(%r14)
.L3841:
	movq	%r14, %rcx
	movl	$114, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	jmp	.L3751
.L3834:
	movl	4(%r14), %r8d
	testl	%r8d, %r8d
	je	.L3836
	movl	$20, 4(%r14)
.L3837:
	movq	%r14, %rcx
	movl	$96, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	jmp	.L3751
.L3833:
	movl	4(%r14), %edi
	testl	%edi, %edi
	je	.L3838
	movl	$20, 4(%r14)
.L3839:
	movq	%r14, %rcx
	movl	$112, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerL10VisitBinopEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	jmp	.L3751
	.p2align 4,,10
	.p2align 3
.L3781:
	movl	$1, 4(%r14)
	jmp	.L3777
	.p2align 4,,10
	.p2align 3
.L3786:
	movl	$5, 4(%r14)
	jmp	.L3777
	.p2align 4,,10
	.p2align 3
.L3846:
	movl	$8, 4(%r14)
	jmp	.L3847
	.p2align 4,,10
	.p2align 3
.L3790:
	movl	$9, 4(%r14)
	jmp	.L3777
	.p2align 4,,10
	.p2align 3
.L3810:
	movl	$6, 4(%r14)
	jmp	.L3811
	.p2align 4,,10
	.p2align 3
.L3825:
	movl	$6, 4(%r14)
	jmp	.L3826
	.p2align 4,,10
	.p2align 3
.L3788:
	movl	$7, 4(%r14)
	jmp	.L3777
	.p2align 4,,10
	.p2align 3
.L3792:
	movl	$1, 4(%r14)
	jmp	.L3793
	.p2align 4,,10
	.p2align 3
.L3798:
	movl	$3, 4(%r14)
	jmp	.L3805
	.p2align 4,,10
	.p2align 3
.L3802:
	movl	$7, 4(%r14)
	jmp	.L3805
	.p2align 4,,10
	.p2align 3
.L3806:
	movl	$19, 4(%r14)
	jmp	.L3811
	.p2align 4,,10
	.p2align 3
.L3804:
	movl	$9, 4(%r14)
	jmp	.L3805
	.p2align 4,,10
	.p2align 3
.L3784:
	movl	$3, 4(%r14)
	jmp	.L3777
	.p2align 4,,10
	.p2align 3
.L3812:
	movl	$19, 4(%r14)
	jmp	.L3826
	.p2align 4,,10
	.p2align 3
.L3808:
	movl	$8, 4(%r14)
	jmp	.L3811
	.p2align 4,,10
	.p2align 3
.L3800:
	movl	$5, 4(%r14)
	jmp	.L3805
	.p2align 4,,10
	.p2align 3
.L3818:
	movl	$8, 4(%r14)
	jmp	.L3826
.L3817:
	testl	%eax, %eax
	je	.L3821
	movl	$1, 4(%r14)
.L3821:
	movl	36(%r12), %eax
	movq	32(%rdx), %rcx
	shrl	$5, %eax
	andl	$1, %eax
	cmpb	$1, %al
	movzbl	23(%rdx), %eax
	sbbl	%esi, %esi
	andl	$-40, %esi
	andl	$15, %eax
	addl	$195, %esi
	cmpl	$15, %eax
	jne	.L3823
	movq	16(%rcx), %rcx
.L3823:
	movq	-104(%rbp), %rdx
	xorl	%r9d, %r9d
	movq	%r14, %r8
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_112VisitCompareEPNS1_19InstructionSelectorEiPNS1_4NodeES6_PNS1_17FlagsContinuationEb
	jmp	.L3751
.L3795:
	movq	%r14, %r8
	movl	$99, %ecx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116VisitCompareZeroEPNS1_19InstructionSelectorEPNS1_4NodeES6_iPNS1_17FlagsContinuationE
	jmp	.L3751
.L3796:
	movq	%r14, %rcx
	movl	$103, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWordCompareEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
	jmp	.L3751
.L3844:
	movl	$21, 4(%r14)
	jmp	.L3845
.L3838:
	movl	$21, 4(%r14)
	jmp	.L3839
.L3840:
	movl	$21, 4(%r14)
	jmp	.L3841
.L3842:
	movl	$21, 4(%r14)
	jmp	.L3843
.L3836:
	movl	$21, 4(%r14)
	jmp	.L3837
.L3867:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17538:
	.size	_ZN2v88internal8compiler19InstructionSelector20VisitWordCompareZeroEPNS1_4NodeES4_PNS1_17FlagsContinuationE, .-_ZN2v88internal8compiler19InstructionSelector20VisitWordCompareZeroEPNS1_4NodeES4_PNS1_17FlagsContinuationE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitWord32EqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitWord32EqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitWord32EqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitWord32EqualEPNS1_4NodeE:
.LFB17540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movzbl	23(%rsi), %edx
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$5, -128(%rbp)
	andl	$15, %edx
	movq	$0, -112(%rbp)
	movl	$-1, -104(%rbp)
	movq	%rsi, -96(%rbp)
	cmpl	$15, %edx
	je	.L3869
	movq	0(%r13), %rdx
	leaq	32(%rsi), %rax
	cmpw	$23, 16(%rdx)
	je	.L3872
.L3887:
	xorl	%r15d, %r15d
	xorl	%edx, %edx
.L3873:
	movq	8(%rax), %rbx
	addq	$8, %rax
	movq	(%rbx), %rax
	cmpw	$23, 16(%rax)
	jne	.L3875
	movl	44(%rax), %r15d
.L3876:
	testl	%r15d, %r15d
	je	.L3888
.L3877:
	leaq	-128(%rbp), %rcx
	movl	$100, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWordCompareEPNS1_19InstructionSelectorEPNS1_4NodeEiPNS1_17FlagsContinuationE
.L3868:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3889
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3875:
	.cfi_restore_state
	movq	(%r12), %rax
	testb	$1, 18(%rax)
	je	.L3877
	testb	%dl, %dl
	je	.L3877
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rbx, %r13
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_.constprop.1
	jmp	.L3876
	.p2align 4,,10
	.p2align 3
.L3869:
	leaq	16(%r13), %rax
	movq	16(%r13), %r13
	movq	0(%r13), %rdx
	cmpw	$23, 16(%rdx)
	jne	.L3887
.L3872:
	movl	44(%rdx), %r15d
	movl	$1, %edx
	jmp	.L3873
	.p2align 4,,10
	.p2align 3
.L3888:
	leaq	-128(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20VisitWordCompareZeroEPNS1_4NodeES4_PNS1_17FlagsContinuationE
	jmp	.L3868
.L3889:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17540:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitWord32EqualEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitWord32EqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_131TryMatchLoadWord64AndShiftRightEPNS1_19InstructionSelectorEPNS1_4NodeEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_131TryMatchLoadWord64AndShiftRightEPNS1_19InstructionSelectorEPNS1_4NodeEi, @function
_ZN2v88internal8compiler12_GLOBAL__N_131TryMatchLoadWord64AndShiftRightEPNS1_19InstructionSelectorEPNS1_4NodeEi:
.LFB17400:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -208(%rbp)
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_@PLT
	movl	%eax, %r12d
	testb	%al, %al
	je	.L3890
	movq	-136(%rbp), %rsi
	xorl	%r12d, %r12d
	movq	(%rsi), %rax
	cmpw	$429, 16(%rax)
	je	.L3943
.L3890:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3944
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3943:
	.cfi_restore_state
	movzbl	-96(%rbp), %r12d
	testb	%r12b, %r12b
	je	.L3890
	xorl	%r12d, %r12d
	cmpq	$32, -104(%rbp)
	jne	.L3890
	pxor	%xmm0, %xmm0
	leaq	-192(%rbp), %rdi
	movl	$3, %edx
	movb	$0, -192(%rbp)
	movq	$0, -184(%rbp)
	movl	$0, -176(%rbp)
	movl	$0, -152(%rbp)
	movups	%xmm0, -168(%rbp)
	call	_ZN2v88internal8compiler35BaseWithIndexAndDisplacementMatcherINS1_10AddMatcherINS1_12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES8_EELS7_325ELS7_327ELS7_329ELS7_321EEEE10InitializeEPNS1_4NodeENS_4base5FlagsINS1_13AddressOptionEhEE
	movzbl	-192(%rbp), %r12d
	testb	%r12b, %r12b
	je	.L3919
	movq	-160(%rbp), %rax
	testq	%rax, %rax
	je	.L3893
	movq	(%rax), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$28, %ax
	je	.L3894
	ja	.L3895
	cmpw	$23, %ax
	je	.L3893
	cmpw	$24, %ax
	jne	.L3919
	movq	48(%rdx), %rax
	movl	$4294967294, %edx
	addq	$2147483647, %rax
	cmpq	%rdx, %rax
	setbe	%al
.L3897:
	testb	%al, %al
	je	.L3919
	.p2align 4,,10
	.p2align 3
.L3893:
	leaq	-80(%rbp), %r9
	movq	-136(%rbp), %rsi
	pxor	%xmm0, %xmm0
	leaq	-208(%rbp), %rdi
	movq	%r9, %rdx
	leaq	-200(%rbp), %rcx
	movq	%r9, -224(%rbp)
	movq	%rdi, -216(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm
	movq	-160(%rbp), %rdx
	movq	-216(%rbp), %rdi
	movq	-224(%rbp), %r9
	movl	%eax, %ebx
	testq	%rdx, %rdx
	je	.L3945
	movq	-200(%rbp), %rax
	leaq	-8(%r9,%rax,8), %rcx
	movl	(%rcx), %eax
	andl	$7, %eax
	cmpl	$3, %eax
	je	.L3946
.L3919:
	xorl	%r12d, %r12d
	jmp	.L3890
.L3945:
	cmpl	$14, %ebx
	ja	.L3900
	leaq	.L3902(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_131TryMatchLoadWord64AndShiftRightEPNS1_19InstructionSelectorEPNS1_4NodeEi,"a",@progbits
	.align 4
	.align 4
.L3902:
	.long	.L3900-.L3902
	.long	.L3910-.L3902
	.long	.L3900-.L3902
	.long	.L3909-.L3902
	.long	.L3918-.L3902
	.long	.L3907-.L3902
	.long	.L3906-.L3902
	.long	.L3900-.L3902
	.long	.L3900-.L3902
	.long	.L3900-.L3902
	.long	.L3900-.L3902
	.long	.L3905-.L3902
	.long	.L3904-.L3902
	.long	.L3903-.L3902
	.long	.L3901-.L3902
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_131TryMatchLoadWord64AndShiftRightEPNS1_19InstructionSelectorEPNS1_4NodeEi
.L3918:
	movl	$8, %ebx
.L3908:
	movq	-200(%rbp), %rax
	movabsq	$17179869187, %rcx
	leaq	1(%rax), %rdx
	movq	%rcx, -80(%rbp,%rax,8)
	movq	%rdx, -200(%rbp)
.L3911:
	movq	%r15, %rsi
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal8compiler16OperandGenerator16DefineAsRegisterEPNS1_4NodeE
	pushq	$0
	movl	$1, %edx
	leaq	-88(%rbp), %rcx
	movq	%rax, -88(%rbp)
	movl	%ebx, %eax
	movq	%r13, %rdi
	movq	-216(%rbp), %r9
	pushq	$0
	sall	$9, %eax
	movq	-200(%rbp), %r8
	orl	%r14d, %eax
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	popq	%rdx
	jmp	.L3890
.L3909:
	movl	$7, %ebx
	jmp	.L3908
.L3910:
	movl	$2, %ebx
	jmp	.L3908
.L3901:
	movl	$18, %ebx
	jmp	.L3908
.L3903:
	movl	$17, %ebx
	jmp	.L3908
.L3904:
	movl	$16, %ebx
	jmp	.L3908
.L3905:
	movl	$15, %ebx
	jmp	.L3908
.L3906:
	movl	$10, %ebx
	jmp	.L3908
.L3907:
	movl	$9, %ebx
	jmp	.L3908
.L3900:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3895:
	cmpw	$32, %ax
	je	.L3893
	jmp	.L3919
.L3946:
	movq	(%rdx), %rax
	cmpw	$23, 16(%rax)
	je	.L3947
	movl	48(%rax), %eax
.L3914:
	addl	$4, %eax
	salq	$32, %rax
	orq	$3, %rax
	movq	%rax, (%rcx)
	jmp	.L3911
.L3894:
	cmpq	$0, 48(%rdx)
	sete	%al
	jmp	.L3897
.L3947:
	movl	44(%rax), %eax
	jmp	.L3914
.L3944:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17400:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_131TryMatchLoadWord64AndShiftRightEPNS1_19InstructionSelectorEPNS1_4NodeEi, .-_ZN2v88internal8compiler12_GLOBAL__N_131TryMatchLoadWord64AndShiftRightEPNS1_19InstructionSelectorEPNS1_4NodeEi
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitTruncateInt64ToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitTruncateInt64ToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitTruncateInt64ToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitTruncateInt64ToInt32EPNS1_4NodeE:
.LFB17500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$120, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movq	%rdi, -160(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3949
	movq	16(%r14), %r14
.L3949:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L3953
	movq	(%r14), %rax
	movzwl	16(%rax), %eax
	cmpw	$323, %ax
	jbe	.L3982
	cmpw	$429, %ax
	jne	.L3953
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_@PLT
	testb	%al, %al
	je	.L3953
	movq	(%r14), %rdi
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	movzbl	%ah, %edx
	cmpb	$11, %al
	ja	.L3961
	leaq	.L3963(%rip), %rcx
	movzbl	%al, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector25VisitTruncateInt64ToInt32EPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L3963:
	.long	.L3961-.L3963
	.long	.L3965-.L3963
	.long	.L3965-.L3963
	.long	.L3964-.L3963
	.long	.L3969-.L3963
	.long	.L3969-.L3963
	.long	.L3969-.L3963
	.long	.L3961-.L3963
	.long	.L3969-.L3963
	.long	.L3969-.L3963
	.long	.L3961-.L3963
	.long	.L3969-.L3963
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitTruncateInt64ToInt32EPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L3982:
	cmpw	$321, %ax
	ja	.L3983
.L3953:
	movq	-160(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-160(%rbp), %rdi
	movq	%r14, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE@PLT
	movl	%ebx, %r14d
	movq	-160(%rbp), %rdi
	movq	%r12, %rsi
	movabsq	$34359738369, %rcx
	salq	$3, %r14
	orq	%rcx, %r14
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE@PLT
	movq	-160(%rbp), %rdi
	movq	%r12, %rsi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE@PLT
	movl	%ebx, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	salq	$3, %rdx
	movq	%r14, %rcx
	movl	$214, %esi
	movq	%r13, %rdi
	movabsq	$927712935937, %rbx
	orq	%rbx, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_@PLT
.L3948:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3984
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3983:
	.cfi_restore_state
	leaq	-144(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12BinopMatcherINS1_10IntMatcherIlLNS1_8IrOpcode5ValueE24EEES6_EC1EPNS1_4NodeE
	cmpb	$0, -96(%rbp)
	je	.L3953
	cmpq	$32, -104(%rbp)
	jne	.L3953
	movzbl	23(%r14), %eax
	movq	32(%r14), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3956
	movq	16(%rcx), %rcx
.L3956:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector20CanCoverTransitivelyEPNS1_4NodeES4_S4_@PLT
	testb	%al, %al
	je	.L3959
	movl	$214, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_131TryMatchLoadWord64AndShiftRightEPNS1_19InstructionSelectorEPNS1_4NodeEi
	testb	%al, %al
	jne	.L3985
.L3959:
	leaq	-160(%rbp), %r15
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler16OperandGenerator13TempImmediateEi
	movq	-136(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler16OperandGenerator17DefineSameAsFirstEPNS1_4NodeE
	subq	$8, %rsp
	movq	%r14, %rcx
	xorl	%r9d, %r9d
	pushq	$0
	movl	$127, %esi
	movq	%rax, %rdx
	movq	%rbx, %r8
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L3948
.L3969:
	movl	$214, %r14d
.L3962:
	leaq	-152(%rbp), %r15
	movq	%r12, %rsi
	movq	%r13, -152(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler16OperandGenerator16DefineAsRegisterEPNS1_4NodeE
	pxor	%xmm0, %xmm0
	movq	$0, -144(%rbp)
	movq	%rax, -88(%rbp)
	movzbl	23(%r12), %eax
	movq	$0, -64(%rbp)
	andl	$15, %eax
	movaps	%xmm0, -80(%rbp)
	cmpl	$15, %eax
	jne	.L3966
	movq	32(%r12), %rbx
	addq	$16, %rbx
.L3966:
	movq	(%rbx), %rsi
	leaq	-80(%rbp), %r12
	leaq	-144(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler19X64OperandGenerator32GetEffectiveAddressMemoryOperandEPNS1_4NodeEPNS1_18InstructionOperandEPm
	pushq	$0
	movl	$1, %edx
	leaq	-88(%rbp), %rcx
	pushq	$0
	sall	$9, %eax
	movq	-144(%rbp), %r8
	movq	%r12, %r9
	movl	%eax, %esi
	movq	%r13, %rdi
	orl	%r14d, %esi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	popq	%rdx
	jmp	.L3948
.L3965:
	subl	$2, %edx
	xorl	%r14d, %r14d
	andl	$253, %edx
	setne	%r14b
	addl	$204, %r14d
	jmp	.L3962
.L3964:
	subl	$2, %edx
	xorl	%r14d, %r14d
	andl	$253, %edx
	setne	%r14b
	addl	$209, %r14d
	jmp	.L3962
.L3961:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3985:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE@PLT
	jmp	.L3948
.L3984:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17500:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitTruncateInt64ToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitTruncateInt64ToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitWord64ShrEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitWord64ShrEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitWord64ShrEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitWord64ShrEPNS1_4NodeE:
.LFB17401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$214, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler12_GLOBAL__N_131TryMatchLoadWord64AndShiftRightEPNS1_19InstructionSelectorEPNS1_4NodeEi
	testb	%al, %al
	je	.L3989
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3989:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$127, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWord64ShiftEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17401:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitWord64ShrEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitWord64ShrEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitWord64SarEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitWord64SarEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitWord64SarEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitWord64SarEPNS1_4NodeE:
.LFB17403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$215, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler12_GLOBAL__N_131TryMatchLoadWord64AndShiftRightEPNS1_19InstructionSelectorEPNS1_4NodeEi
	testb	%al, %al
	je	.L3993
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3993:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$129, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12_GLOBAL__N_116VisitWord64ShiftEPNS1_19InstructionSelectorEPNS1_4NodeENS1_10ArchOpcodeE
	.cfi_endproc
.LFE17403:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitWord64SarEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitWord64SarEPNS1_4NodeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler19InstructionSelector14VisitStackSlotEPNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler19InstructionSelector14VisitStackSlotEPNS1_4NodeE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler19InstructionSelector14VisitStackSlotEPNS1_4NodeE:
.LFB21676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21676:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler19InstructionSelector14VisitStackSlotEPNS1_4NodeE, .-_GLOBAL__sub_I__ZN2v88internal8compiler19InstructionSelector14VisitStackSlotEPNS1_4NodeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler19InstructionSelector14VisitStackSlotEPNS1_4NodeE
	.section	.rodata.CSWTCH.731,"a"
	.align 32
	.type	CSWTCH.731, @object
	.size	CSWTCH.731, 56
CSWTCH.731:
	.long	0
	.long	0
	.long	1
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	2
	.long	3
	.long	4
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_1L13arch_shufflesE,"a"
	.align 32
	.type	_ZN2v88internal8compiler12_GLOBAL__N_1L13arch_shufflesE, @object
	.size	_ZN2v88internal8compiler12_GLOBAL__N_1L13arch_shufflesE, 408
_ZN2v88internal8compiler12_GLOBAL__N_1L13arch_shufflesE:
	.string	""
	.ascii	"\001\002\003\004\005\006\007\020\021\022\023\024\025\026\027"
	.long	400
	.byte	1
	.byte	0
	.zero	2
	.ascii	"\b\t\n\013\f\r\016\017\030\031\032\033\034\035\036\037"
	.long	396
	.byte	1
	.byte	0
	.zero	2
	.string	""
	.ascii	"\001\002\003\020\021\022\023\004\005\006\007\024\025\026\027"
	.long	401
	.byte	1
	.byte	0
	.zero	2
	.ascii	"\b\t\n\013\030\031\032\033\f\r\016\017\034\035\036\037"
	.long	397
	.byte	1
	.byte	0
	.zero	2
	.string	""
	.ascii	"\001\020\021\002\003\022\023\004\005\024\025\006\007\026\027"
	.long	402
	.byte	1
	.byte	0
	.zero	2
	.ascii	"\b\t\030\031\n\013\032\033\f\r\034\035\016\017\036\037"
	.long	398
	.byte	1
	.byte	0
	.zero	2
	.string	""
	.ascii	"\020\001\021\002\022\003\023\004\024\005\025\006\026\007\027"
	.long	403
	.byte	1
	.byte	0
	.zero	2
	.ascii	"\b\030\t\031\n\032\013\033\f\034\r\035\016\036\017\037"
	.long	399
	.byte	1
	.byte	0
	.zero	2
	.string	""
	.ascii	"\001\004\005\b\t\f\r\020\021\024\025\030\031\034\035"
	.long	393
	.byte	1
	.byte	0
	.zero	2
	.ascii	"\002\003\006\007\n\013\016\017\022\023\026\027\032\033\036\037"
	.long	392
	.byte	1
	.byte	1
	.zero	2
	.string	""
	.ascii	"\002\004\006\b\n\f\016\020\022\024\026\030\032\034\036"
	.long	395
	.byte	1
	.byte	1
	.zero	2
	.ascii	"\001\003\005\007\t\013\r\017\021\023\025\027\031\033\035\037"
	.long	394
	.byte	1
	.byte	1
	.zero	2
	.string	""
	.ascii	"\020\002\022\004\024\006\026\b\030\n\032\f\034\016\036"
	.long	404
	.byte	1
	.byte	1
	.zero	2
	.ascii	"\001\021\003\023\005\025\007\027\t\031\013\033\r\035\017\037"
	.long	405
	.byte	1
	.byte	1
	.zero	2
	.string	"\007\006\005\004\003\002\001"
	.ascii	"\017\016\r\f\013\n\t\b"
	.long	406
	.byte	1
	.byte	1
	.zero	2
	.string	"\003\002\001"
	.ascii	"\007\006\005\004\013\n\t\b\017\016\r\f"
	.long	407
	.byte	1
	.byte	1
	.zero	2
	.string	"\001"
	.ascii	"\003\002\005\004\007\006\t\b\013\n\r\f\017\016"
	.long	408
	.byte	1
	.byte	1
	.zero	2
	.weak	_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE9kMn_modes
	.section	.rodata._ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE9kMn_modes,"aG",@progbits,_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE9kMn_modes,comdat
	.align 16
	.type	_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE9kMn_modes, @gnu_unique_object
	.size	_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE9kMn_modes, 16
_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE9kMn_modes:
	.long	1
	.long	3
	.long	13
	.long	14
	.weak	_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE10kMnI_modes
	.section	.rodata._ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE10kMnI_modes,"aG",@progbits,_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE10kMnI_modes,comdat
	.align 16
	.type	_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE10kMnI_modes, @gnu_unique_object
	.size	_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE10kMnI_modes, 16
_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE10kMnI_modes:
	.long	2
	.long	16
	.long	17
	.long	18
	.weak	_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE10kMRn_modes
	.section	.rodata._ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE10kMRn_modes,"aG",@progbits,_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE10kMRn_modes,comdat
	.align 16
	.type	_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE10kMRn_modes, @gnu_unique_object
	.size	_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE10kMRn_modes, 16
_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE10kMRn_modes:
	.long	3
	.long	4
	.long	5
	.long	6
	.weak	_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE11kMRnI_modes
	.section	.rodata._ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE11kMRnI_modes,"aG",@progbits,_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE11kMRnI_modes,comdat
	.align 16
	.type	_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE11kMRnI_modes, @gnu_unique_object
	.size	_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE11kMRnI_modes, 16
_ZZN2v88internal8compiler19X64OperandGenerator27GenerateMemoryOperandInputsEPNS1_4NodeEiS4_S4_NS1_16DisplacementModeEPNS1_18InstructionOperandEPmE11kMRnI_modes:
	.long	7
	.long	8
	.long	9
	.long	10
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
