	.file	"regexp-bytecode-generator.cc"
	.text
	.section	.text._ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv,"axG",@progbits,_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv
	.type	_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv, @function
_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv:
.LFB7977:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7977:
	.size	_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv, .-_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator17stack_limit_slackEv,"axG",@progbits,_ZN2v88internal23RegExpBytecodeGenerator17stack_limit_slackEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23RegExpBytecodeGenerator17stack_limit_slackEv
	.type	_ZN2v88internal23RegExpBytecodeGenerator17stack_limit_slackEv, @function
_ZN2v88internal23RegExpBytecodeGenerator17stack_limit_slackEv:
.LFB7987:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7987:
	.size	_ZN2v88internal23RegExpBytecodeGenerator17stack_limit_slackEv, .-_ZN2v88internal23RegExpBytecodeGenerator17stack_limit_slackEv
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator16CanReadUnalignedEv,"axG",@progbits,_ZN2v88internal23RegExpBytecodeGenerator16CanReadUnalignedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23RegExpBytecodeGenerator16CanReadUnalignedEv
	.type	_ZN2v88internal23RegExpBytecodeGenerator16CanReadUnalignedEv, @function
_ZN2v88internal23RegExpBytecodeGenerator16CanReadUnalignedEv:
.LFB7988:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7988:
	.size	_ZN2v88internal23RegExpBytecodeGenerator16CanReadUnalignedEv, .-_ZN2v88internal23RegExpBytecodeGenerator16CanReadUnalignedEv
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator14ImplementationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator14ImplementationEv
	.type	_ZN2v88internal23RegExpBytecodeGenerator14ImplementationEv, @function
_ZN2v88internal23RegExpBytecodeGenerator14ImplementationEv:
.LFB19263:
	.cfi_startproc
	endbr64
	movl	$8, %eax
	ret
	.cfi_endproc
.LFE19263:
	.size	_ZN2v88internal23RegExpBytecodeGenerator14ImplementationEv, .-_ZN2v88internal23RegExpBytecodeGenerator14ImplementationEv
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator4BindEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator4BindEPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator4BindEPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator4BindEPNS0_5LabelE:
.LFB19264:
	.cfi_startproc
	endbr64
	movl	$-1, 68(%rdi)
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L7
	subl	$1, %eax
	je	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	cltq
	addq	32(%rdi), %rax
	movl	48(%rdi), %ecx
	movq	%rax, %rdx
	movl	(%rax), %eax
	movl	%ecx, (%rdx)
	testl	%eax, %eax
	jne	.L8
.L7:
	movl	48(%rdi), %eax
	notl	%eax
	movl	%eax, (%rsi)
	ret
	.cfi_endproc
.LFE19264:
	.size	_ZN2v88internal23RegExpBytecodeGenerator4BindEPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator4BindEPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGeneratorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGeneratorD2Ev
	.type	_ZN2v88internal23RegExpBytecodeGeneratorD2Ev, @function
_ZN2v88internal23RegExpBytecodeGeneratorD2Ev:
.LFB19260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal23RegExpBytecodeGeneratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movl	52(%rdi), %eax
	testl	%eax, %eax
	jle	.L14
	movl	$0, 52(%rdi)
.L14:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L15
	call	_ZdaPv@PLT
.L15:
	movq	$0, 32(%r12)
	movq	%r12, %rdi
	movq	$0, 40(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal20RegExpMacroAssemblerD2Ev@PLT
	.cfi_endproc
.LFE19260:
	.size	_ZN2v88internal23RegExpBytecodeGeneratorD2Ev, .-_ZN2v88internal23RegExpBytecodeGeneratorD2Ev
	.globl	_ZN2v88internal23RegExpBytecodeGeneratorD1Ev
	.set	_ZN2v88internal23RegExpBytecodeGeneratorD1Ev,_ZN2v88internal23RegExpBytecodeGeneratorD2Ev
	.section	.text._ZN2v88internal23RegExpBytecodeGeneratorD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGeneratorD0Ev
	.type	_ZN2v88internal23RegExpBytecodeGeneratorD0Ev, @function
_ZN2v88internal23RegExpBytecodeGeneratorD0Ev:
.LFB19262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal23RegExpBytecodeGeneratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movl	52(%rdi), %eax
	testl	%eax, %eax
	jle	.L21
	movl	$0, 52(%rdi)
.L21:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L22
	call	_ZdaPv@PLT
.L22:
	movq	$0, 32(%r12)
	movq	%r12, %rdi
	movq	$0, 40(%r12)
	call	_ZN2v88internal20RegExpMacroAssemblerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE19262:
	.size	_ZN2v88internal23RegExpBytecodeGeneratorD0Ev, .-_ZN2v88internal23RegExpBytecodeGeneratorD0Ev
	.section	.rodata._ZN2v88internal23RegExpBytecodeGenerator12PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NewArray"
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator12PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator12PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE
	.type	_ZN2v88internal23RegExpBytecodeGenerator12PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE, @function
_ZN2v88internal23RegExpBytecodeGenerator12PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE:
.LFB19267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$8, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	orl	$3, %r12d
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	movq	40(%rdi), %r13
	movq	32(%rdi), %r14
	leal	3(%rax), %edx
	cmpl	%r13d, %edx
	jge	.L31
.L28:
	movl	%r12d, (%r14,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	leal	(%r13,%r13), %r15d
	leaq	_ZSt7nothrow(%rip), %rsi
	movslq	%r15d, %r15
	movq	%r15, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L32
.L29:
	movq	%rdi, 32(%rbx)
	movq	%r14, %rsi
	movslq	%r13d, %rdx
	movq	%r15, 40(%rbx)
	call	memcpy@PLT
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movq	32(%rbx), %r14
	movslq	48(%rbx), %rax
	jmp	.L28
.L32:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	movq	%r15, %rdi
	leaq	_ZSt7nothrow(%rip), %rsi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L29
	leaq	.LC0(%rip), %rsi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE19267:
	.size	_ZN2v88internal23RegExpBytecodeGenerator12PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE, .-_ZN2v88internal23RegExpBytecodeGenerator12PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator25SetCurrentPositionFromEndEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator25SetCurrentPositionFromEndEi
	.type	_ZN2v88internal23RegExpBytecodeGenerator25SetCurrentPositionFromEndEi, @function
_ZN2v88internal23RegExpBytecodeGenerator25SetCurrentPositionFromEndEi:
.LFB19273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$8, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	orl	$51, %r12d
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	movq	40(%rdi), %r13
	movq	32(%rdi), %r14
	leal	3(%rax), %edx
	cmpl	%r13d, %edx
	jge	.L37
.L34:
	movl	%r12d, (%r14,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	leal	(%r13,%r13), %r15d
	leaq	_ZSt7nothrow(%rip), %rsi
	movslq	%r15d, %r15
	movq	%r15, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L38
.L35:
	movq	%rdi, 32(%rbx)
	movq	%r14, %rsi
	movslq	%r13d, %rdx
	movq	%r15, 40(%rbx)
	call	memcpy@PLT
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movq	32(%rbx), %r14
	movslq	48(%rbx), %rax
	jmp	.L34
.L38:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	movq	%r15, %rdi
	leaq	_ZSt7nothrow(%rip), %rsi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L35
	leaq	.LC0(%rip), %rsi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE19273:
	.size	_ZN2v88internal23RegExpBytecodeGenerator25SetCurrentPositionFromEndEi, .-_ZN2v88internal23RegExpBytecodeGenerator25SetCurrentPositionFromEndEi
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator11PopRegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator11PopRegisterEi
	.type	_ZN2v88internal23RegExpBytecodeGenerator11PopRegisterEi, @function
_ZN2v88internal23RegExpBytecodeGenerator11PopRegisterEi:
.LFB19266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$8, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	orl	$12, %r12d
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	movq	40(%rdi), %r13
	movq	32(%rdi), %r14
	leal	3(%rax), %edx
	cmpl	%r13d, %edx
	jge	.L43
.L40:
	movl	%r12d, (%r14,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	leal	(%r13,%r13), %r15d
	leaq	_ZSt7nothrow(%rip), %rsi
	movslq	%r15d, %r15
	movq	%r15, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L44
.L41:
	movq	%rdi, 32(%rbx)
	movq	%r14, %rsi
	movslq	%r13d, %rdx
	movq	%r15, 40(%rbx)
	call	memcpy@PLT
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movq	32(%rbx), %r14
	movslq	48(%rbx), %rax
	jmp	.L40
.L44:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	movq	%r15, %rdi
	leaq	_ZSt7nothrow(%rip), %rsi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L41
	leaq	.LC0(%rip), %rsi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE19266:
	.size	_ZN2v88internal23RegExpBytecodeGenerator11PopRegisterEi, .-_ZN2v88internal23RegExpBytecodeGenerator11PopRegisterEi
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator28ReadStackPointerFromRegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator28ReadStackPointerFromRegisterEi
	.type	_ZN2v88internal23RegExpBytecodeGenerator28ReadStackPointerFromRegisterEi, @function
_ZN2v88internal23RegExpBytecodeGenerator28ReadStackPointerFromRegisterEi:
.LFB19272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$8, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	orl	$7, %r12d
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	movq	40(%rdi), %r13
	movq	32(%rdi), %r14
	leal	3(%rax), %edx
	cmpl	%r13d, %edx
	jge	.L49
.L46:
	movl	%r12d, (%r14,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	leal	(%r13,%r13), %r15d
	leaq	_ZSt7nothrow(%rip), %rsi
	movslq	%r15d, %r15
	movq	%r15, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L50
.L47:
	movq	%rdi, 32(%rbx)
	movq	%r14, %rsi
	movslq	%r13d, %rdx
	movq	%r15, 40(%rbx)
	call	memcpy@PLT
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movq	32(%rbx), %r14
	movslq	48(%rbx), %rax
	jmp	.L46
.L50:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	movq	%r15, %rdi
	leaq	_ZSt7nothrow(%rip), %rsi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L47
	leaq	.LC0(%rip), %rsi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE19272:
	.size	_ZN2v88internal23RegExpBytecodeGenerator28ReadStackPointerFromRegisterEi, .-_ZN2v88internal23RegExpBytecodeGenerator28ReadStackPointerFromRegisterEi
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator31ReadCurrentPositionFromRegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator31ReadCurrentPositionFromRegisterEi
	.type	_ZN2v88internal23RegExpBytecodeGenerator31ReadCurrentPositionFromRegisterEi, @function
_ZN2v88internal23RegExpBytecodeGenerator31ReadCurrentPositionFromRegisterEi:
.LFB19270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$8, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	orl	$5, %r12d
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	movq	40(%rdi), %r13
	movq	32(%rdi), %r14
	leal	3(%rax), %edx
	cmpl	%r13d, %edx
	jge	.L55
.L52:
	movl	%r12d, (%r14,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	leal	(%r13,%r13), %r15d
	leaq	_ZSt7nothrow(%rip), %rsi
	movslq	%r15d, %r15
	movq	%r15, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L56
.L53:
	movq	%rdi, 32(%rbx)
	movq	%r14, %rsi
	movslq	%r13d, %rdx
	movq	%r15, 40(%rbx)
	call	memcpy@PLT
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movq	32(%rbx), %r14
	movslq	48(%rbx), %rax
	jmp	.L52
.L56:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	movq	%r15, %rdi
	leaq	_ZSt7nothrow(%rip), %rsi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L53
	leaq	.LC0(%rip), %rsi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE19270:
	.size	_ZN2v88internal23RegExpBytecodeGenerator31ReadCurrentPositionFromRegisterEi, .-_ZN2v88internal23RegExpBytecodeGenerator31ReadCurrentPositionFromRegisterEi
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator27WriteStackPointerToRegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator27WriteStackPointerToRegisterEi
	.type	_ZN2v88internal23RegExpBytecodeGenerator27WriteStackPointerToRegisterEi, @function
_ZN2v88internal23RegExpBytecodeGenerator27WriteStackPointerToRegisterEi:
.LFB19271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$8, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	orl	$6, %r12d
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	movq	40(%rdi), %r13
	movq	32(%rdi), %r14
	leal	3(%rax), %edx
	cmpl	%r13d, %edx
	jge	.L61
.L58:
	movl	%r12d, (%r14,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	leal	(%r13,%r13), %r15d
	leaq	_ZSt7nothrow(%rip), %rsi
	movslq	%r15d, %r15
	movq	%r15, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L62
.L59:
	movq	%rdi, 32(%rbx)
	movq	%r14, %rsi
	movslq	%r13d, %rdx
	movq	%r15, 40(%rbx)
	call	memcpy@PLT
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movq	32(%rbx), %r14
	movslq	48(%rbx), %rax
	jmp	.L58
.L62:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	movq	%r15, %rdi
	leaq	_ZSt7nothrow(%rip), %rsi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L59
	leaq	.LC0(%rip), %rsi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE19271:
	.size	_ZN2v88internal23RegExpBytecodeGenerator27WriteStackPointerToRegisterEi, .-_ZN2v88internal23RegExpBytecodeGenerator27WriteStackPointerToRegisterEi
	.section	.text._ZN2v88internal23RegExpBytecodeGeneratorC2EPNS0_7IsolateEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGeneratorC2EPNS0_7IsolateEPNS0_4ZoneE
	.type	_ZN2v88internal23RegExpBytecodeGeneratorC2EPNS0_7IsolateEPNS0_4ZoneE, @function
_ZN2v88internal23RegExpBytecodeGeneratorC2EPNS0_7IsolateEPNS0_4ZoneE:
.LFB19257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v88internal20RegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE@PLT
	leaq	16+_ZTVN2v88internal23RegExpBytecodeGeneratorE(%rip), %rax
	movl	$1024, %edi
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%rax, (%rbx)
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L66
.L64:
	movq	%r12, 72(%rbx)
	movq	%rax, 32(%rbx)
	movq	$1024, 40(%rbx)
	movq	$0, 48(%rbx)
	movl	$0, 56(%rbx)
	movl	$-1, 68(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L66:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$1024, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L64
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE19257:
	.size	_ZN2v88internal23RegExpBytecodeGeneratorC2EPNS0_7IsolateEPNS0_4ZoneE, .-_ZN2v88internal23RegExpBytecodeGeneratorC2EPNS0_7IsolateEPNS0_4ZoneE
	.globl	_ZN2v88internal23RegExpBytecodeGeneratorC1EPNS0_7IsolateEPNS0_4ZoneE
	.set	_ZN2v88internal23RegExpBytecodeGeneratorC1EPNS0_7IsolateEPNS0_4ZoneE,_ZN2v88internal23RegExpBytecodeGeneratorC2EPNS0_7IsolateEPNS0_4ZoneE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator6lengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator6lengthEv
	.type	_ZN2v88internal23RegExpBytecodeGenerator6lengthEv, @function
_ZN2v88internal23RegExpBytecodeGenerator6lengthEv:
.LFB19304:
	.cfi_startproc
	endbr64
	movl	48(%rdi), %eax
	ret
	.cfi_endproc
.LFE19304:
	.size	_ZN2v88internal23RegExpBytecodeGenerator6lengthEv, .-_ZN2v88internal23RegExpBytecodeGenerator6lengthEv
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator4CopyEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator4CopyEPh
	.type	_ZN2v88internal23RegExpBytecodeGenerator4CopyEPh, @function
_ZN2v88internal23RegExpBytecodeGenerator4CopyEPh:
.LFB19305:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	movslq	48(%rdi), %rdx
	movq	32(%rdi), %rsi
	movq	%r8, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE19305:
	.size	_ZN2v88internal23RegExpBytecodeGenerator4CopyEPh, .-_ZN2v88internal23RegExpBytecodeGenerator4CopyEPh
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator6ExpandEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	.type	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv, @function
_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv:
.LFB19306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	40(%rdi), %r14
	movq	%rdi, %rbx
	movq	32(%rdi), %r13
	leal	(%r14,%r14), %r12d
	movslq	%r12d, %r12
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L73
	movq	%rax, %rdi
.L70:
	movq	%rdi, 32(%rbx)
	movslq	%r14d, %rdx
	movq	%r13, %rsi
	movq	%r12, 40(%rbx)
	call	memcpy@PLT
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdaPv@PLT
.L73:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	movq	%r12, %rdi
	leaq	_ZSt7nothrow(%rip), %rsi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L70
	leaq	.LC0(%rip), %rsi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE19306:
	.size	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv, .-_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator18PopCurrentPositionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator18PopCurrentPositionEv
	.type	_ZN2v88internal23RegExpBytecodeGenerator18PopCurrentPositionEv, @function
_ZN2v88internal23RegExpBytecodeGenerator18PopCurrentPositionEv:
.LFB19276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L77
.L75:
	movq	32(%rbx), %rdx
	movl	$10, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L75
	.cfi_endproc
.LFE19276:
	.size	_ZN2v88internal23RegExpBytecodeGenerator18PopCurrentPositionEv, .-_ZN2v88internal23RegExpBytecodeGenerator18PopCurrentPositionEv
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator19PushCurrentPositionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator19PushCurrentPositionEv
	.type	_ZN2v88internal23RegExpBytecodeGenerator19PushCurrentPositionEv, @function
_ZN2v88internal23RegExpBytecodeGenerator19PushCurrentPositionEv:
.LFB19277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L81
.L79:
	movq	32(%rbx), %rdx
	movl	$1, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L79
	.cfi_endproc
.LFE19277:
	.size	_ZN2v88internal23RegExpBytecodeGenerator19PushCurrentPositionEv, .-_ZN2v88internal23RegExpBytecodeGenerator19PushCurrentPositionEv
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator9BacktrackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator9BacktrackEv
	.type	_ZN2v88internal23RegExpBytecodeGenerator9BacktrackEv, @function
_ZN2v88internal23RegExpBytecodeGenerator9BacktrackEv:
.LFB19278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L85
.L83:
	movq	32(%rbx), %rdx
	movl	$11, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L83
	.cfi_endproc
.LFE19278:
	.size	_ZN2v88internal23RegExpBytecodeGenerator9BacktrackEv, .-_ZN2v88internal23RegExpBytecodeGenerator9BacktrackEv
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator7SucceedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator7SucceedEv
	.type	_ZN2v88internal23RegExpBytecodeGenerator7SucceedEv, @function
_ZN2v88internal23RegExpBytecodeGenerator7SucceedEv:
.LFB19281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L89
.L87:
	movq	32(%rbx), %rdx
	movl	$14, (%rdx,%rax)
	xorl	%eax, %eax
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L87
	.cfi_endproc
.LFE19281:
	.size	_ZN2v88internal23RegExpBytecodeGenerator7SucceedEv, .-_ZN2v88internal23RegExpBytecodeGenerator7SucceedEv
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator4FailEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator4FailEv
	.type	_ZN2v88internal23RegExpBytecodeGenerator4FailEv, @function
_ZN2v88internal23RegExpBytecodeGenerator4FailEv:
.LFB19282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L93
.L91:
	movq	32(%rbx), %rdx
	movl	$13, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L91
	.cfi_endproc
.LFE19282:
	.size	_ZN2v88internal23RegExpBytecodeGenerator4FailEv, .-_ZN2v88internal23RegExpBytecodeGenerator4FailEv
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator7GetCodeENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator7GetCodeENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal23RegExpBytecodeGenerator7GetCodeENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal23RegExpBytecodeGenerator7GetCodeENS0_6HandleINS0_6StringEEE:
.LFB19303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal23RegExpBytecodeGenerator4BindEPNS0_5LabelE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L95
	movl	52(%rdi), %eax
	movl	$-1, 68(%rdi)
	testl	%eax, %eax
	jle	.L98
	subl	$1, %eax
	je	.L98
	.p2align 4,,10
	.p2align 3
.L99:
	cltq
	addq	32(%rbx), %rax
	movl	48(%rbx), %ecx
	movq	%rax, %rdx
	movl	(%rax), %eax
	movl	%ecx, (%rdx)
	testl	%eax, %eax
	jne	.L99
.L98:
	movslq	48(%rbx), %rax
	movl	%eax, %edx
	notl	%edx
	movl	%edx, 52(%rbx)
.L97:
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L108
.L100:
	movq	32(%rbx), %rdx
	movl	$11, (%rdx,%rax)
	movl	48(%rbx), %eax
	xorl	%edx, %edx
	movq	72(%rbx), %rdi
	leal	4(%rax), %esi
	movl	%esi, 48(%rbx)
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movslq	48(%rbx), %rdx
	movq	32(%rbx), %rsi
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	15(%rax), %rdi
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	52(%rdi), %rsi
	call	*%rax
	movslq	48(%rbx), %rax
	jmp	.L97
	.cfi_endproc
.LFE19303:
	.size	_ZN2v88internal23RegExpBytecodeGenerator7GetCodeENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal23RegExpBytecodeGenerator7GetCodeENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator14ClearRegistersEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator14ClearRegistersEii
	.type	_ZN2v88internal23RegExpBytecodeGenerator14ClearRegistersEii, @function
_ZN2v88internal23RegExpBytecodeGenerator14ClearRegistersEii:
.LFB19269:
	.cfi_startproc
	endbr64
	cmpl	%edx, %esi
	jg	.L119
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leal	1(%rdx), %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZN2v88internal23RegExpBytecodeGenerator11SetRegisterEii(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L112:
	movq	32(%rbx), %rdx
	movl	%r15d, (%rdx,%rax)
	movl	48(%rbx), %edx
	leal	4(%rdx), %eax
	addl	$7, %edx
	movl	%eax, 48(%rbx)
	cmpl	40(%rbx), %edx
	jge	.L122
.L113:
	movq	32(%rbx), %rdx
	cltq
	addl	$1, %r12d
	movl	$-1, (%rdx,%rax)
	addl	$4, 48(%rbx)
	cmpl	%r14d, %r12d
	je	.L123
.L116:
	movq	(%rbx), %rax
	movq	336(%rax), %rax
	cmpq	%r13, %rax
	jne	.L111
	movslq	48(%rbx), %rax
	movl	%r12d, %r15d
	sall	$8, %r15d
	orl	$8, %r15d
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jl	.L112
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L122:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L111:
	movl	%r12d, %esi
	addl	$1, %r12d
	movl	$-1, %edx
	movq	%rbx, %rdi
	call	*%rax
	cmpl	%r14d, %r12d
	jne	.L116
.L123:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE19269:
	.size	_ZN2v88internal23RegExpBytecodeGenerator14ClearRegistersEii, .-_ZN2v88internal23RegExpBytecodeGenerator14ClearRegistersEii
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator11SetRegisterEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator11SetRegisterEii
	.type	_ZN2v88internal23RegExpBytecodeGenerator11SetRegisterEii, @function
_ZN2v88internal23RegExpBytecodeGenerator11SetRegisterEii:
.LFB19274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$8, %esi
	orl	$8, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L128
.L125:
	movq	32(%rbx), %rdx
	movl	%r12d, (%rdx,%rax)
	movl	48(%rbx), %ecx
	leal	4(%rcx), %eax
	addl	$7, %ecx
	movl	%eax, 48(%rbx)
	cmpl	40(%rbx), %ecx
	jge	.L129
.L126:
	movq	32(%rbx), %rdx
	cltq
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L126
	.cfi_endproc
.LFE19274:
	.size	_ZN2v88internal23RegExpBytecodeGenerator11SetRegisterEii, .-_ZN2v88internal23RegExpBytecodeGenerator11SetRegisterEii
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator15AdvanceRegisterEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator15AdvanceRegisterEii
	.type	_ZN2v88internal23RegExpBytecodeGenerator15AdvanceRegisterEii, @function
_ZN2v88internal23RegExpBytecodeGenerator15AdvanceRegisterEii:
.LFB19275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$8, %esi
	orl	$9, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L134
.L131:
	movq	32(%rbx), %rdx
	movl	%r12d, (%rdx,%rax)
	movl	48(%rbx), %ecx
	leal	4(%rcx), %eax
	addl	$7, %ecx
	movl	%eax, 48(%rbx)
	cmpl	40(%rbx), %ecx
	jge	.L135
.L132:
	movq	32(%rbx), %rdx
	cltq
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L132
	.cfi_endproc
.LFE19275:
	.size	_ZN2v88internal23RegExpBytecodeGenerator15AdvanceRegisterEii, .-_ZN2v88internal23RegExpBytecodeGenerator15AdvanceRegisterEii
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator30WriteCurrentPositionToRegisterEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator30WriteCurrentPositionToRegisterEii
	.type	_ZN2v88internal23RegExpBytecodeGenerator30WriteCurrentPositionToRegisterEii, @function
_ZN2v88internal23RegExpBytecodeGenerator30WriteCurrentPositionToRegisterEii:
.LFB19268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$8, %esi
	orl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L140
.L137:
	movq	32(%rbx), %rdx
	movl	%r12d, (%rdx,%rax)
	movl	48(%rbx), %ecx
	leal	4(%rcx), %eax
	addl	$7, %ecx
	movl	%eax, 48(%rbx)
	cmpl	40(%rbx), %ecx
	jge	.L141
.L138:
	movq	32(%rbx), %rdx
	cltq
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L141:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L138
	.cfi_endproc
.LFE19268:
	.size	_ZN2v88internal23RegExpBytecodeGenerator30WriteCurrentPositionToRegisterEii, .-_ZN2v88internal23RegExpBytecodeGenerator30WriteCurrentPositionToRegisterEii
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator22AdvanceCurrentPositionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator22AdvanceCurrentPositionEi
	.type	_ZN2v88internal23RegExpBytecodeGenerator22AdvanceCurrentPositionEi, @function
_ZN2v88internal23RegExpBytecodeGenerator22AdvanceCurrentPositionEi:
.LFB19283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	48(%rdi), %rax
	movl	%esi, 64(%rdi)
	sall	$8, %esi
	orl	$15, %esi
	movl	%eax, 60(%rdi)
	leal	3(%rax), %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	40(%rdi), %edx
	jge	.L145
.L143:
	movq	32(%rbx), %rdx
	movl	%r12d, (%rdx,%rax)
	movl	48(%rbx), %eax
	addl	$4, %eax
	movl	%eax, 48(%rbx)
	movl	%eax, 68(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L143
	.cfi_endproc
.LFE19283:
	.size	_ZN2v88internal23RegExpBytecodeGenerator22AdvanceCurrentPositionEi, .-_ZN2v88internal23RegExpBytecodeGenerator22AdvanceCurrentPositionEi
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator15IfRegisterEqPosEiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator15IfRegisterEqPosEiPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator15IfRegisterEqPosEiPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator15IfRegisterEqPosEiPNS0_5LabelE:
.LFB19302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$8, %esi
	orl	$46, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L156
.L147:
	movq	32(%rbx), %rdx
	testq	%r12, %r12
	movl	%r13d, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmove	%rdx, %r12
	leal	4(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L157
	leal	-1(%rdx), %r13d
	movl	$0, %edx
	cmove	%edx, %r13d
	addl	$5, %eax
	movl	%eax, (%r12)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L158
.L153:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L156:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L157:
	notl	%edx
	addl	$7, %eax
	movl	%edx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L159
.L150:
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %ecx
	jmp	.L150
	.cfi_endproc
.LFE19302:
	.size	_ZN2v88internal23RegExpBytecodeGenerator15IfRegisterEqPosEiPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator15IfRegisterEqPosEiPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator12CheckAtStartEiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator12CheckAtStartEiPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator12CheckAtStartEiPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator12CheckAtStartEiPNS0_5LabelE:
.LFB19289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$8, %esi
	orl	$47, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L170
.L161:
	movq	32(%rbx), %rdx
	testq	%r12, %r12
	movl	%r13d, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmove	%rdx, %r12
	leal	4(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L171
	leal	-1(%rdx), %r13d
	movl	$0, %edx
	cmove	%edx, %r13d
	addl	$5, %eax
	movl	%eax, (%r12)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L172
.L167:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L170:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L171:
	notl	%edx
	addl	$7, %eax
	movl	%edx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L173
.L164:
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %ecx
	jmp	.L164
	.cfi_endproc
.LFE19289:
	.size	_ZN2v88internal23RegExpBytecodeGenerator12CheckAtStartEiPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator12CheckAtStartEiPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator16CheckCharacterLTEtPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator16CheckCharacterLTEtPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator16CheckCharacterLTEtPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator16CheckCharacterLTEtPNS0_5LabelE:
.LFB19286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movzwl	%si, %r12d
	pushq	%rbx
	sall	$8, %r12d
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	orl	$35, %r12d
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L184
.L175:
	movq	32(%rbx), %rdx
	testq	%r13, %r13
	movl	%r12d, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmove	%rdx, %r13
	leal	4(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movl	0(%r13), %edx
	testl	%edx, %edx
	js	.L185
	leal	-1(%rdx), %r12d
	movl	$0, %edx
	cmove	%edx, %r12d
	addl	$5, %eax
	movl	%eax, 0(%r13)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L186
.L181:
	movq	32(%rbx), %rdx
	movl	%r12d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L184:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L185:
	notl	%edx
	addl	$7, %eax
	movl	%edx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L187
.L178:
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %ecx
	jmp	.L178
	.cfi_endproc
.LFE19286:
	.size	_ZN2v88internal23RegExpBytecodeGenerator16CheckCharacterLTEtPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator16CheckCharacterLTEtPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator16CheckCharacterGTEtPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator16CheckCharacterGTEtPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator16CheckCharacterGTEtPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator16CheckCharacterGTEtPNS0_5LabelE:
.LFB19287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movzwl	%si, %r12d
	pushq	%rbx
	sall	$8, %r12d
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	orl	$36, %r12d
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L198
.L189:
	movq	32(%rbx), %rdx
	testq	%r13, %r13
	movl	%r12d, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmove	%rdx, %r13
	leal	4(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movl	0(%r13), %edx
	testl	%edx, %edx
	js	.L199
	leal	-1(%rdx), %r12d
	movl	$0, %edx
	cmove	%edx, %r12d
	addl	$5, %eax
	movl	%eax, 0(%r13)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L200
.L195:
	movq	32(%rbx), %rdx
	movl	%r12d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L198:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L199:
	notl	%edx
	addl	$7, %eax
	movl	%edx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L201
.L192:
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %ecx
	jmp	.L192
	.cfi_endproc
.LFE19287:
	.size	_ZN2v88internal23RegExpBytecodeGenerator16CheckCharacterGTEtPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator16CheckCharacterGTEtPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator15CheckNotAtStartEiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator15CheckNotAtStartEiPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator15CheckNotAtStartEiPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator15CheckNotAtStartEiPNS0_5LabelE:
.LFB19290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$8, %esi
	orl	$48, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L212
.L203:
	movq	32(%rbx), %rdx
	testq	%r12, %r12
	movl	%r13d, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmove	%rdx, %r12
	leal	4(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L213
	leal	-1(%rdx), %r13d
	movl	$0, %edx
	cmove	%edx, %r13d
	addl	$5, %eax
	movl	%eax, (%r12)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L214
.L209:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L212:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L213:
	notl	%edx
	addl	$7, %eax
	movl	%edx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L215
.L206:
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %ecx
	jmp	.L206
	.cfi_endproc
.LFE19290:
	.size	_ZN2v88internal23RegExpBytecodeGenerator15CheckNotAtStartEiPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator15CheckNotAtStartEiPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator21CheckNotBackReferenceEibPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator21CheckNotBackReferenceEibPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator21CheckNotBackReferenceEibPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator21CheckNotBackReferenceEibPNS0_5LabelE:
.LFB19298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$1, %dl
	movslq	48(%rdi), %rax
	sbbl	%r13d, %r13d
	sall	$8, %esi
	andl	$-3, %r13d
	leal	3(%rax), %edx
	addl	$40, %r13d
	orl	%esi, %r13d
	cmpl	40(%rdi), %edx
	jge	.L228
.L218:
	movq	32(%rbx), %rdx
	testq	%r12, %r12
	movl	%r13d, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmove	%rdx, %r12
	leal	4(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L229
	leal	-1(%rdx), %r13d
	movl	$0, %edx
	cmove	%edx, %r13d
	addl	$5, %eax
	movl	%eax, (%r12)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L230
.L224:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L228:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L229:
	notl	%edx
	addl	$7, %eax
	movl	%edx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L231
.L221:
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %ecx
	jmp	.L221
	.cfi_endproc
.LFE19298:
	.size	_ZN2v88internal23RegExpBytecodeGenerator21CheckNotBackReferenceEibPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator21CheckNotBackReferenceEibPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator12IfRegisterLTEiiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator12IfRegisterLTEiiPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator12IfRegisterLTEiiPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator12IfRegisterLTEiiPNS0_5LabelE:
.LFB19300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$8, %esi
	orl	$44, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	48(%rdi), %rax
	movq	%rdi, %rbx
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L243
.L233:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	movl	48(%rbx), %ecx
	leal	4(%rcx), %eax
	addl	$7, %ecx
	movl	%eax, 48(%rbx)
	cmpl	40(%rbx), %ecx
	jge	.L244
.L234:
	movq	32(%rbx), %rdx
	cltq
	testq	%r12, %r12
	movl	%r14d, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmove	%rdx, %r12
	leal	4(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L245
	leal	-1(%rdx), %r13d
	movl	$0, %edx
	cmove	%edx, %r13d
	addl	$5, %eax
	movl	%eax, (%r12)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L246
.L240:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L244:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L243:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L245:
	notl	%edx
	addl	$7, %eax
	movl	%edx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L247
.L237:
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx)
	addl	$4, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %ecx
	jmp	.L237
	.cfi_endproc
.LFE19300:
	.size	_ZN2v88internal23RegExpBytecodeGenerator12IfRegisterLTEiiPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator12IfRegisterLTEiiPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator12IfRegisterGEEiiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator12IfRegisterGEEiiPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator12IfRegisterGEEiiPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator12IfRegisterGEEiiPNS0_5LabelE:
.LFB19301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$8, %esi
	orl	$45, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	48(%rdi), %rax
	movq	%rdi, %rbx
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L259
.L249:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	movl	48(%rbx), %ecx
	leal	4(%rcx), %eax
	addl	$7, %ecx
	movl	%eax, 48(%rbx)
	cmpl	40(%rbx), %ecx
	jge	.L260
.L250:
	movq	32(%rbx), %rdx
	cltq
	testq	%r12, %r12
	movl	%r14d, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmove	%rdx, %r12
	leal	4(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L261
	leal	-1(%rdx), %r13d
	movl	$0, %edx
	cmove	%edx, %r13d
	addl	$5, %eax
	movl	%eax, (%r12)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L262
.L256:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L260:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L259:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L261:
	notl	%edx
	addl	$7, %eax
	movl	%edx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L263
.L253:
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx)
	addl	$4, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %ecx
	jmp	.L253
	.cfi_endproc
.LFE19301:
	.size	_ZN2v88internal23RegExpBytecodeGenerator12IfRegisterGEEiiPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator12IfRegisterGEEiiPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator31CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator31CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator31CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator31CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE:
.LFB19299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testb	%cl, %cl
	setne	%r12b
	testb	%dl, %dl
	je	.L265
	addl	$41, %r12d
.L266:
	movslq	48(%rbx), %rax
	sall	$8, %esi
	orl	%esi, %r12d
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L278
.L267:
	movq	32(%rbx), %rdx
	testq	%r13, %r13
	movl	%r12d, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmove	%rdx, %r13
	leal	4(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movl	0(%r13), %edx
	testl	%edx, %edx
	js	.L279
	leal	-1(%rdx), %r12d
	movl	$0, %edx
	cmove	%edx, %r12d
	addl	$5, %eax
	movl	%eax, 0(%r13)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L280
.L273:
	movq	32(%rbx), %rdx
	movl	%r12d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	addl	$38, %r12d
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L280:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L278:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L279:
	notl	%edx
	addl	$7, %eax
	movl	%edx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L281
.L270:
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %ecx
	jmp	.L270
	.cfi_endproc
.LFE19299:
	.size	_ZN2v88internal23RegExpBytecodeGenerator31CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator31CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator30CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator30CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator30CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator30CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE:
.LFB19294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movzwl	%si, %r12d
	pushq	%rbx
	sall	$8, %r12d
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	orl	$31, %r12d
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L294
.L283:
	movq	32(%rbx), %rdx
	movl	%r12d, (%rdx,%rax)
	movl	48(%rbx), %edx
	leal	4(%rdx), %eax
	addl	$5, %edx
	movl	%eax, 48(%rbx)
	cmpl	40(%rbx), %edx
	jge	.L295
.L284:
	movq	32(%rbx), %rdx
	cltq
	movw	%r15w, (%rdx,%rax)
	movl	48(%rbx), %edx
	leal	2(%rdx), %eax
	addl	$3, %edx
	movl	%eax, 48(%rbx)
	cmpl	40(%rbx), %edx
	jge	.L296
.L285:
	movq	32(%rbx), %rdx
	cltq
	testq	%r13, %r13
	movw	%r14w, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmove	%rdx, %r13
	leal	2(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movl	0(%r13), %edx
	testl	%edx, %edx
	js	.L297
	leal	-1(%rdx), %r12d
	movl	$0, %edx
	cmove	%edx, %r12d
	addl	$3, %eax
	movl	%eax, 0(%r13)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L298
.L291:
	movq	32(%rbx), %rdx
	movl	%r12d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L296:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L295:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L294:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L297:
	notl	%edx
	addl	$5, %eax
	movl	%edx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L299
.L288:
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %ecx
	jmp	.L288
	.cfi_endproc
.LFE19294:
	.size	_ZN2v88internal23RegExpBytecodeGenerator30CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator30CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator15CheckGreedyLoopEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator15CheckGreedyLoopEPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator15CheckGreedyLoopEPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator15CheckGreedyLoopEPNS0_5LabelE:
.LFB19284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L310
.L301:
	movq	32(%rbx), %rdx
	testq	%r12, %r12
	movl	$49, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmove	%rdx, %r12
	leal	4(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L311
	leal	-1(%rdx), %r13d
	movl	$0, %edx
	cmove	%edx, %r13d
	addl	$5, %eax
	movl	%eax, (%r12)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L312
.L307:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L310:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L311:
	notl	%edx
	addl	$7, %eax
	movl	%edx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L313
.L304:
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %ecx
	jmp	.L304
	.cfi_endproc
.LFE19284:
	.size	_ZN2v88internal23RegExpBytecodeGenerator15CheckGreedyLoopEPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator15CheckGreedyLoopEPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator13PushBacktrackEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator13PushBacktrackEPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator13PushBacktrackEPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator13PushBacktrackEPNS0_5LabelE:
.LFB19280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L324
.L315:
	movq	32(%rbx), %rdx
	testq	%r12, %r12
	movl	$2, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmove	%rdx, %r12
	leal	4(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L325
	leal	-1(%rdx), %r13d
	movl	$0, %edx
	cmove	%edx, %r13d
	addl	$5, %eax
	movl	%eax, (%r12)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L326
.L321:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L324:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L325:
	notl	%edx
	addl	$7, %eax
	movl	%edx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L327
.L318:
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %ecx
	jmp	.L318
	.cfi_endproc
.LFE19280:
	.size	_ZN2v88internal23RegExpBytecodeGenerator13PushBacktrackEPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator13PushBacktrackEPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator24CheckCharacterNotInRangeEttPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator24CheckCharacterNotInRangeEttPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator24CheckCharacterNotInRangeEttPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator24CheckCharacterNotInRangeEttPNS0_5LabelE:
.LFB19296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	48(%rdi), %rax
	movq	%rdi, %rbx
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L340
.L329:
	movq	32(%rbx), %rdx
	movl	$33, (%rdx,%rax)
	movl	48(%rbx), %ecx
	leal	4(%rcx), %eax
	addl	$5, %ecx
	movl	%eax, 48(%rbx)
	cmpl	40(%rbx), %ecx
	jge	.L341
.L330:
	movq	32(%rbx), %rdx
	cltq
	movw	%r14w, (%rdx,%rax)
	movl	48(%rbx), %ecx
	leal	2(%rcx), %eax
	addl	$3, %ecx
	movl	%eax, 48(%rbx)
	cmpl	40(%rbx), %ecx
	jge	.L342
.L331:
	movq	32(%rbx), %rdx
	cltq
	testq	%r12, %r12
	movw	%r13w, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmove	%rdx, %r12
	leal	2(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L343
	leal	-1(%rdx), %r13d
	movl	$0, %edx
	cmove	%edx, %r13d
	addl	$3, %eax
	movl	%eax, (%r12)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L344
.L337:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L342:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L341:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L340:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L343:
	notl	%edx
	addl	$5, %eax
	movl	%edx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L345
.L334:
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx)
	addl	$4, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L345:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %ecx
	jmp	.L334
	.cfi_endproc
.LFE19296:
	.size	_ZN2v88internal23RegExpBytecodeGenerator24CheckCharacterNotInRangeEttPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator24CheckCharacterNotInRangeEttPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator21CheckCharacterInRangeEttPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator21CheckCharacterInRangeEttPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator21CheckCharacterInRangeEttPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator21CheckCharacterInRangeEttPNS0_5LabelE:
.LFB19295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	48(%rdi), %rax
	movq	%rdi, %rbx
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L358
.L347:
	movq	32(%rbx), %rdx
	movl	$32, (%rdx,%rax)
	movl	48(%rbx), %ecx
	leal	4(%rcx), %eax
	addl	$5, %ecx
	movl	%eax, 48(%rbx)
	cmpl	40(%rbx), %ecx
	jge	.L359
.L348:
	movq	32(%rbx), %rdx
	cltq
	movw	%r14w, (%rdx,%rax)
	movl	48(%rbx), %ecx
	leal	2(%rcx), %eax
	addl	$3, %ecx
	movl	%eax, 48(%rbx)
	cmpl	40(%rbx), %ecx
	jge	.L360
.L349:
	movq	32(%rbx), %rdx
	cltq
	testq	%r12, %r12
	movw	%r13w, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmove	%rdx, %r12
	leal	2(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L361
	leal	-1(%rdx), %r13d
	movl	$0, %edx
	cmove	%edx, %r13d
	addl	$3, %eax
	movl	%eax, (%r12)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L362
.L355:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L360:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L359:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L358:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L361:
	notl	%edx
	addl	$5, %eax
	movl	%edx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L363
.L352:
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx)
	addl	$4, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %ecx
	jmp	.L352
	.cfi_endproc
.LFE19295:
	.size	_ZN2v88internal23RegExpBytecodeGenerator21CheckCharacterInRangeEttPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator21CheckCharacterInRangeEttPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator14CheckCharacterEjPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator14CheckCharacterEjPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator14CheckCharacterEjPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator14CheckCharacterEjPNS0_5LabelE:
.LFB19288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	movq	40(%rdi), %rdx
	leal	3(%rax), %ecx
	cmpl	$8388607, %esi
	jbe	.L365
	cmpl	%edx, %ecx
	jge	.L378
.L366:
	movq	32(%rbx), %rdx
	movl	$23, (%rdx,%rax)
	movl	48(%rbx), %edx
	leal	4(%rdx), %eax
	addl	$7, %edx
	movl	%eax, 48(%rbx)
	cmpl	40(%rbx), %edx
	jge	.L379
.L367:
	cltq
.L369:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	movl	48(%rbx), %eax
	leaq	52(%rbx), %rdx
	addl	$4, %eax
	testq	%r12, %r12
	cmove	%rdx, %r12
	movl	%eax, 48(%rbx)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L380
	leal	-1(%rdx), %r13d
	movl	$0, %edx
	cmove	%edx, %r13d
	addl	$1, %eax
	movl	%eax, (%r12)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L381
.L375:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L381:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L379:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L378:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L365:
	sall	$8, %r13d
	orl	$24, %r13d
	cmpl	%edx, %ecx
	jl	.L369
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L380:
	notl	%edx
	movl	%edx, %r12d
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L382
.L372:
	movq	32(%rbx), %rdx
	cltq
	movl	%r12d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L382:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L372
	.cfi_endproc
.LFE19288:
	.size	_ZN2v88internal23RegExpBytecodeGenerator14CheckCharacterEjPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator14CheckCharacterEjPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator17CheckNotCharacterEjPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator17CheckNotCharacterEjPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator17CheckNotCharacterEjPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator17CheckNotCharacterEjPNS0_5LabelE:
.LFB19291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	movq	40(%rdi), %rdx
	leal	3(%rax), %ecx
	cmpl	$8388607, %esi
	jbe	.L384
	cmpl	%edx, %ecx
	jge	.L397
.L385:
	movq	32(%rbx), %rdx
	movl	$25, (%rdx,%rax)
	movl	48(%rbx), %edx
	leal	4(%rdx), %eax
	addl	$7, %edx
	movl	%eax, 48(%rbx)
	cmpl	40(%rbx), %edx
	jge	.L398
.L386:
	cltq
.L388:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	movl	48(%rbx), %eax
	leaq	52(%rbx), %rdx
	addl	$4, %eax
	testq	%r12, %r12
	cmove	%rdx, %r12
	movl	%eax, 48(%rbx)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L399
	leal	-1(%rdx), %r13d
	movl	$0, %edx
	cmove	%edx, %r13d
	addl	$1, %eax
	movl	%eax, (%r12)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L400
.L394:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L398:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L397:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L384:
	sall	$8, %r13d
	orl	$26, %r13d
	cmpl	%edx, %ecx
	jl	.L388
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L399:
	notl	%edx
	movl	%edx, %r12d
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L401
.L391:
	movq	32(%rbx), %rdx
	cltq
	movl	%r12d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L391
	.cfi_endproc
.LFE19291:
	.size	_ZN2v88internal23RegExpBytecodeGenerator17CheckNotCharacterEjPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator17CheckNotCharacterEjPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator25CheckNotCharacterAfterAndEjjPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator25CheckNotCharacterAfterAndEjjPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator25CheckNotCharacterAfterAndEjjPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator25CheckNotCharacterAfterAndEjjPNS0_5LabelE:
.LFB19293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	48(%rdi), %rax
	movq	%rdi, %rbx
	movq	40(%rdi), %rdx
	leal	3(%rax), %ecx
	cmpl	$8388607, %esi
	jbe	.L403
	cmpl	%edx, %ecx
	jge	.L417
.L404:
	movq	32(%rbx), %rdx
	movl	$29, (%rdx,%rax)
	movl	48(%rbx), %edx
	leal	4(%rdx), %eax
	addl	$7, %edx
	movl	%eax, 48(%rbx)
	cmpl	40(%rbx), %edx
	jge	.L418
.L405:
	cltq
.L407:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	movl	48(%rbx), %eax
	addl	$4, %eax
	movl	%eax, 48(%rbx)
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L419
.L408:
	movq	32(%rbx), %rdx
	cltq
	testq	%r12, %r12
	movl	%r14d, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmove	%rdx, %r12
	leal	4(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L420
	leal	-1(%rdx), %r13d
	movl	$0, %edx
	cmove	%edx, %r13d
	addl	$5, %eax
	movl	%eax, (%r12)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L421
.L414:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L418:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L417:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L403:
	sall	$8, %r13d
	orl	$30, %r13d
	cmpl	%edx, %ecx
	jl	.L407
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L419:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L420:
	notl	%edx
	addl	$7, %eax
	movl	%edx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L422
.L411:
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx)
	addl	$4, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %ecx
	jmp	.L411
	.cfi_endproc
.LFE19293:
	.size	_ZN2v88internal23RegExpBytecodeGenerator25CheckNotCharacterAfterAndEjjPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator25CheckNotCharacterAfterAndEjjPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator22CheckCharacterAfterAndEjjPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator22CheckCharacterAfterAndEjjPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator22CheckCharacterAfterAndEjjPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator22CheckCharacterAfterAndEjjPNS0_5LabelE:
.LFB19292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	48(%rdi), %rax
	movq	%rdi, %rbx
	movq	40(%rdi), %rdx
	leal	3(%rax), %ecx
	cmpl	$8388607, %esi
	jbe	.L424
	cmpl	%edx, %ecx
	jge	.L438
.L425:
	movq	32(%rbx), %rdx
	movl	$27, (%rdx,%rax)
	movl	48(%rbx), %edx
	leal	4(%rdx), %eax
	addl	$7, %edx
	movl	%eax, 48(%rbx)
	cmpl	40(%rbx), %edx
	jge	.L439
.L426:
	cltq
.L428:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	movl	48(%rbx), %eax
	addl	$4, %eax
	movl	%eax, 48(%rbx)
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L440
.L429:
	movq	32(%rbx), %rdx
	cltq
	testq	%r12, %r12
	movl	%r14d, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmove	%rdx, %r12
	leal	4(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L441
	leal	-1(%rdx), %r13d
	movl	$0, %edx
	cmove	%edx, %r13d
	addl	$5, %eax
	movl	%eax, (%r12)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L442
.L435:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L439:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L438:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L424:
	sall	$8, %r13d
	orl	$28, %r13d
	cmpl	%edx, %ecx
	jl	.L428
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L440:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L441:
	notl	%edx
	addl	$7, %eax
	movl	%edx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L443
.L432:
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx)
	addl	$4, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %ecx
	jmp	.L432
	.cfi_endproc
.LFE19292:
	.size	_ZN2v88internal23RegExpBytecodeGenerator22CheckCharacterAfterAndEjjPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator22CheckCharacterAfterAndEjjPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator4GoToEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator4GoToEPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator4GoToEPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator4GoToEPNS0_5LabelE:
.LFB19279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	movq	40(%rdi), %rdx
	cmpl	%eax, 68(%rdi)
	je	.L463
	leal	3(%rax), %ecx
	cmpl	%edx, %ecx
	jge	.L464
.L454:
	movq	32(%rbx), %rdx
	testq	%r12, %r12
	movl	$16, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmove	%rdx, %r12
	leal	4(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L465
	leal	-1(%rdx), %r13d
	movl	$0, %edx
	cmove	%edx, %r13d
	addl	$5, %eax
	movl	%eax, (%r12)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L466
.L459:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore_state
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L465:
	notl	%edx
	addl	$7, %eax
	movl	%edx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L467
.L457:
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	movslq	60(%rdi), %rax
	movl	64(%rdi), %r13d
	leal	3(%rax), %ecx
	sall	$8, %r13d
	movl	%eax, 48(%rdi)
	orl	$50, %r13d
	cmpl	%edx, %ecx
	jge	.L468
.L446:
	movq	32(%rbx), %rdx
	testq	%r12, %r12
	movl	%r13d, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmove	%rdx, %r12
	leal	4(%rax), %ecx
	movl	%ecx, 48(%rbx)
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L469
	leal	-1(%rdx), %r13d
	movl	$0, %edx
	cmove	%edx, %r13d
	addl	$5, %eax
	movl	%eax, (%r12)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L470
.L452:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
	addl	$4, 48(%rbx)
.L450:
	movl	$-1, 68(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L467:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %ecx
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L468:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L469:
	notl	%edx
	addl	$7, %eax
	movl	%edx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L471
.L449:
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx)
	addl	$4, 48(%rbx)
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L471:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %ecx
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L470:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L452
	.cfi_endproc
.LFE19279:
	.size	_ZN2v88internal23RegExpBytecodeGenerator4GoToEPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator4GoToEPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator15CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator15CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE
	.type	_ZN2v88internal23RegExpBytecodeGenerator15CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE, @function
_ZN2v88internal23RegExpBytecodeGenerator15CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE:
.LFB19297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movslq	48(%rdi), %rax
	movq	%rdx, %rbx
	leal	3(%rax), %edx
	cmpl	40(%rdi), %edx
	jge	.L501
.L473:
	movq	32(%r13), %rdx
	testq	%rbx, %rbx
	movl	$34, (%rdx,%rax)
	leaq	52(%r13), %rdx
	movl	48(%r13), %eax
	cmove	%rdx, %rbx
	leal	4(%rax), %ecx
	movl	%ecx, 48(%r13)
	movl	(%rbx), %edx
	testl	%edx, %edx
	js	.L502
	leal	-1(%rdx), %r12d
	movl	$0, %edx
	cmove	%edx, %r12d
	addl	$5, %eax
	movl	%eax, (%rbx)
	movslq	48(%r13), %rax
	leal	3(%rax), %edx
	cmpl	40(%r13), %edx
	jge	.L503
.L479:
	movq	32(%r13), %rdx
	movl	%r12d, (%rdx,%rax)
	movl	48(%r13), %eax
	addl	$4, %eax
	movl	%eax, 48(%r13)
.L477:
	movl	$16, %ebx
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L487:
	cltq
.L500:
	movq	32(%r13), %rdx
	addq	$8, %rbx
	movb	%r12b, (%rdx,%rax)
	movl	48(%r13), %eax
	addl	$1, %eax
	movl	%eax, 48(%r13)
	cmpq	$144, %rbx
	je	.L504
.L490:
	movq	(%r14), %rcx
	xorl	%r12d, %r12d
	cmpb	$0, -1(%rbx,%rcx)
	leaq	-1(%rcx), %rdx
	setne	%r12b
	cmpb	$0, (%rbx,%rcx)
	je	.L480
	orl	$2, %r12d
.L480:
	cmpb	$0, 2(%rdx,%rbx)
	je	.L481
	orl	$4, %r12d
.L481:
	cmpb	$0, 3(%rdx,%rbx)
	je	.L482
	orl	$8, %r12d
.L482:
	cmpb	$0, 4(%rdx,%rbx)
	je	.L483
	orl	$16, %r12d
.L483:
	cmpb	$0, 5(%rdx,%rbx)
	je	.L484
	orl	$32, %r12d
.L484:
	cmpb	$0, 6(%rdx,%rbx)
	je	.L485
	orl	$64, %r12d
.L485:
	cmpb	$0, 7(%rdx,%rbx)
	je	.L486
	orb	$-128, %r12b
.L486:
	cmpl	40(%r13), %eax
	jne	.L487
	movq	%r13, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%r13), %rax
	jmp	.L500
.L504:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L503:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%r13), %rax
	jmp	.L479
.L501:
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%r13), %rax
	jmp	.L473
.L502:
	notl	%edx
	addl	$7, %eax
	movl	%edx, %ebx
	cmpl	40(%r13), %eax
	jge	.L505
.L476:
	movq	32(%r13), %rax
	movslq	%ecx, %rcx
	movl	%ebx, (%rax,%rcx)
	movl	48(%r13), %eax
	addl	$4, %eax
	movl	%eax, 48(%r13)
	jmp	.L477
.L505:
	movq	%r13, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%r13), %ecx
	jmp	.L476
	.cfi_endproc
.LFE19297:
	.size	_ZN2v88internal23RegExpBytecodeGenerator15CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE, .-_ZN2v88internal23RegExpBytecodeGenerator15CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE
	.section	.text._ZN2v88internal23RegExpBytecodeGenerator24LoadCurrentCharacterImplEiPNS0_5LabelEbii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23RegExpBytecodeGenerator24LoadCurrentCharacterImplEiPNS0_5LabelEbii
	.type	_ZN2v88internal23RegExpBytecodeGenerator24LoadCurrentCharacterImplEiPNS0_5LabelEbii, @function
_ZN2v88internal23RegExpBytecodeGenerator24LoadCurrentCharacterImplEiPNS0_5LabelEbii:
.LFB19285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	48(%rdi), %rax
	movq	40(%rdi), %rsi
	leal	3(%rax), %edx
	movl	%esi, %edi
	cmpl	%r8d, %r9d
	jle	.L507
	testb	%cl, %cl
	jne	.L536
.L512:
	cmpl	$4, %r15d
	je	.L527
.L542:
	xorl	%esi, %esi
	cmpl	$2, %r15d
	sete	%sil
	xorl	%r13d, %r13d
	leal	18(%rsi,%rsi), %esi
.L515:
	sall	$8, %r12d
	orl	%esi, %r12d
	cmpl	%edi, %edx
	jge	.L537
.L516:
	movq	32(%rbx), %rdx
	cltq
	movl	%r12d, (%rdx,%rax)
	movl	48(%rbx), %eax
	leal	4(%rax), %edx
	movl	%edx, 48(%rbx)
	testb	%r13b, %r13b
	jne	.L538
.L506:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L536:
	.cfi_restore_state
	leal	(%r9,%r12), %r13d
	sall	$8, %r13d
	orl	$52, %r13d
	cmpl	%esi, %edx
	jge	.L539
.L508:
	movq	32(%rbx), %rdx
	testq	%r14, %r14
	movl	%r13d, (%rdx,%rax)
	leaq	52(%rbx), %rdx
	movl	48(%rbx), %eax
	cmovne	%r14, %rdx
	leal	4(%rax), %esi
	movl	%esi, 48(%rbx)
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	js	.L540
	leal	-1(%rcx), %r13d
	movl	$0, %ecx
	cmove	%ecx, %r13d
	addl	$5, %eax
	movl	%eax, (%rdx)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L541
.L514:
	movq	32(%rbx), %rdx
	movl	%r13d, (%rdx,%rax)
.L535:
	movl	48(%rbx), %edx
	movl	40(%rbx), %edi
	leal	4(%rdx), %eax
	addl	$7, %edx
	movl	%eax, 48(%rbx)
	cmpl	$4, %r15d
	jne	.L542
.L527:
	movl	$22, %esi
	sall	$8, %r12d
	xorl	%r13d, %r13d
	orl	%esi, %r12d
	cmpl	%edi, %edx
	jl	.L516
.L537:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %eax
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L507:
	testb	%cl, %cl
	je	.L512
	movl	%ecx, %r13d
	movl	$21, %esi
	cmpl	$4, %r8d
	je	.L515
	xorl	%esi, %esi
	cmpl	$2, %r8d
	sete	%sil
	leal	17(%rsi,%rsi), %esi
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L538:
	leaq	52(%rbx), %rcx
	testq	%r14, %r14
	cmove	%rcx, %r14
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	js	.L543
	movl	$0, %edx
	leal	-1(%rcx), %r12d
	cmove	%edx, %r12d
	addl	$5, %eax
	movl	%eax, (%r14)
	movslq	48(%rbx), %rax
	leal	3(%rax), %edx
	cmpl	40(%rbx), %edx
	jge	.L544
.L522:
	movq	32(%rbx), %rdx
	movl	%r12d, (%rdx,%rax)
	addl	$4, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L544:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L541:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L539:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movslq	48(%rbx), %rax
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L543:
	notl	%ecx
	addl	$7, %eax
	movl	%ecx, %r12d
	cmpl	40(%rbx), %eax
	jge	.L545
.L520:
	movq	32(%rbx), %rax
	movslq	%edx, %rdx
	movl	%r12d, (%rax,%rdx)
	addl	$4, 48(%rbx)
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L540:
	notl	%ecx
	addl	$7, %eax
	movl	%ecx, %r13d
	cmpl	40(%rbx), %eax
	jge	.L546
.L511:
	movq	32(%rbx), %rax
	movslq	%esi, %rsi
	movl	%r13d, (%rax,%rsi)
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L545:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %edx
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L546:
	movq	%rbx, %rdi
	call	_ZN2v88internal23RegExpBytecodeGenerator6ExpandEv
	movl	48(%rbx), %esi
	jmp	.L511
	.cfi_endproc
.LFE19285:
	.size	_ZN2v88internal23RegExpBytecodeGenerator24LoadCurrentCharacterImplEiPNS0_5LabelEbii, .-_ZN2v88internal23RegExpBytecodeGenerator24LoadCurrentCharacterImplEiPNS0_5LabelEbii
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal23RegExpBytecodeGeneratorC2EPNS0_7IsolateEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal23RegExpBytecodeGeneratorC2EPNS0_7IsolateEPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal23RegExpBytecodeGeneratorC2EPNS0_7IsolateEPNS0_4ZoneE:
.LFB23543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23543:
	.size	_GLOBAL__sub_I__ZN2v88internal23RegExpBytecodeGeneratorC2EPNS0_7IsolateEPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal23RegExpBytecodeGeneratorC2EPNS0_7IsolateEPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal23RegExpBytecodeGeneratorC2EPNS0_7IsolateEPNS0_4ZoneE
	.weak	_ZTVN2v88internal23RegExpBytecodeGeneratorE
	.section	.data.rel.ro._ZTVN2v88internal23RegExpBytecodeGeneratorE,"awG",@progbits,_ZTVN2v88internal23RegExpBytecodeGeneratorE,comdat
	.align 8
	.type	_ZTVN2v88internal23RegExpBytecodeGeneratorE, @object
	.size	_ZTVN2v88internal23RegExpBytecodeGeneratorE, 392
_ZTVN2v88internal23RegExpBytecodeGeneratorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23RegExpBytecodeGeneratorD1Ev
	.quad	_ZN2v88internal23RegExpBytecodeGeneratorD0Ev
	.quad	_ZN2v88internal20RegExpMacroAssembler21AbortedCodeGenerationEv
	.quad	_ZN2v88internal23RegExpBytecodeGenerator17stack_limit_slackEv
	.quad	_ZN2v88internal23RegExpBytecodeGenerator16CanReadUnalignedEv
	.quad	_ZN2v88internal23RegExpBytecodeGenerator22AdvanceCurrentPositionEi
	.quad	_ZN2v88internal23RegExpBytecodeGenerator15AdvanceRegisterEii
	.quad	_ZN2v88internal23RegExpBytecodeGenerator9BacktrackEv
	.quad	_ZN2v88internal23RegExpBytecodeGenerator4BindEPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator14CheckCharacterEjPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator22CheckCharacterAfterAndEjjPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator16CheckCharacterGTEtPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator16CheckCharacterLTEtPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator15CheckGreedyLoopEPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator12CheckAtStartEiPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator15CheckNotAtStartEiPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator21CheckNotBackReferenceEibPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator31CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator17CheckNotCharacterEjPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator25CheckNotCharacterAfterAndEjjPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator30CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator21CheckCharacterInRangeEttPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator24CheckCharacterNotInRangeEttPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator15CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE
	.quad	_ZN2v88internal20RegExpMacroAssembler13CheckPositionEiPNS0_5LabelE
	.quad	_ZN2v88internal20RegExpMacroAssembler26CheckSpecialCharacterClassEtPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator4FailEv
	.quad	_ZN2v88internal23RegExpBytecodeGenerator7GetCodeENS0_6HandleINS0_6StringEEE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator4GoToEPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator12IfRegisterGEEiiPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator12IfRegisterLTEiiPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator15IfRegisterEqPosEiPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator14ImplementationEv
	.quad	_ZN2v88internal23RegExpBytecodeGenerator24LoadCurrentCharacterImplEiPNS0_5LabelEbii
	.quad	_ZN2v88internal23RegExpBytecodeGenerator18PopCurrentPositionEv
	.quad	_ZN2v88internal23RegExpBytecodeGenerator11PopRegisterEi
	.quad	_ZN2v88internal23RegExpBytecodeGenerator13PushBacktrackEPNS0_5LabelE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator19PushCurrentPositionEv
	.quad	_ZN2v88internal23RegExpBytecodeGenerator12PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE
	.quad	_ZN2v88internal23RegExpBytecodeGenerator31ReadCurrentPositionFromRegisterEi
	.quad	_ZN2v88internal23RegExpBytecodeGenerator28ReadStackPointerFromRegisterEi
	.quad	_ZN2v88internal23RegExpBytecodeGenerator25SetCurrentPositionFromEndEi
	.quad	_ZN2v88internal23RegExpBytecodeGenerator11SetRegisterEii
	.quad	_ZN2v88internal23RegExpBytecodeGenerator7SucceedEv
	.quad	_ZN2v88internal23RegExpBytecodeGenerator30WriteCurrentPositionToRegisterEii
	.quad	_ZN2v88internal23RegExpBytecodeGenerator14ClearRegistersEii
	.quad	_ZN2v88internal23RegExpBytecodeGenerator27WriteStackPointerToRegisterEi
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
