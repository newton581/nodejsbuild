	.file	"stack-frame-info.cc"
	.text
	.section	.text._ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE:
.LFB17940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	%rax, %rdx
	movslq	19(%rax), %r13
	movq	7(%rax), %r14
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L2
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3:
	movl	%r13d, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory17NewStackFrameInfoENS0_6HandleINS0_10FrameArrayEEEi@PLT
	movq	(%r12), %r14
	movq	(%rax), %r13
	leaq	23(%r14), %rsi
	movq	%r13, 23(%r14)
	testb	$1, %r13b
	je	.L12
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L27
	testb	$24, %al
	je	.L12
.L32:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L28
	.p2align 4,,10
	.p2align 3
.L12:
	movq	(%r12), %r14
	movq	88(%rbx), %r13
	movq	%r13, 7(%r14)
	leaq	7(%r14), %r15
	testb	$1, %r13b
	je	.L11
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L29
	testb	$24, %al
	je	.L11
.L31:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L30
.L11:
	movabsq	$-4294967296, %rdx
	movq	(%r12), %rax
	movq	%rdx, 15(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L31
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L32
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L2:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L33
.L4:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r14, (%rsi)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4
	.cfi_endproc
.LFE17940:
	.size	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE:
.LFB17939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rsi, -37504(%rdx)
	je	.L40
.L35:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L36
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L41
.L38:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	%rdi, %rbx
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L38
	.cfi_endproc
.LFE17939:
	.size	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_118AppendFileLocationEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEEPNS0_24IncrementalStringBuilderE.isra.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	", "
.LC1:
	.string	"<anonymous>"
	.section	.text._ZN2v88internal12_GLOBAL__N_118AppendFileLocationEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEEPNS0_24IncrementalStringBuilderE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118AppendFileLocationEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEEPNS0_24IncrementalStringBuilderE.isra.0, @function
_ZN2v88internal12_GLOBAL__N_118AppendFileLocationEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEEPNS0_24IncrementalStringBuilderE.isra.0:
.LFB21763:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	(%rax), %rax
	movq	47(%rax), %rsi
	movq	(%r12), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r14
	movq	3520(%r14), %rdi
	subq	$37592, %r14
	testq	%rdi, %rdi
	je	.L43
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	testb	$1, %sil
	jne	.L46
.L49:
	movq	%r12, %rdi
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	(%rax), %rax
	testb	$1, 107(%rax)
	jne	.L175
.L50:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L176
.L62:
	movl	8(%rbx), %r10d
	testl	%r10d, %r10d
	jne	.L177
	movl	$60, %eax
	leaq	.LC1(%rip), %r13
.L162:
	movl	20(%rbx), %edx
.L66:
	movq	32(%rbx), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %r13
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%rbx)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%rbx), %edx
	cmpl	16(%rbx), %edx
	je	.L178
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L66
.L65:
	movq	%r12, %rdi
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	(%rax), %rax
	movslq	11(%rax), %r13
	cmpq	$-1, %r13
	je	.L42
	testq	%r13, %r13
	je	.L42
	movl	20(%rbx), %eax
	movl	8(%rbx), %r9d
	movq	32(%rbx), %rdx
	leal	1(%rax), %ecx
	testl	%r9d, %r9d
	movq	(%rdx), %rdx
	movl	%ecx, 20(%rbx)
	je	.L179
	leal	16(%rax,%rax), %eax
	movl	$58, %r8d
	cltq
	movw	%r8w, -1(%rdx,%rax)
	movl	16(%rbx), %eax
	cmpl	%eax, 20(%rbx)
	je	.L77
.L78:
	leaq	-160(%rbp), %r14
	movl	%r13d, %edi
	movl	$100, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal12IntToCStringEiNS0_6VectorIcEE@PLT
	movl	8(%rbx), %edi
	movq	%rax, %r13
	testl	%edi, %edi
	jne	.L168
.L165:
	movzbl	0(%r13), %eax
	testb	%al, %al
	je	.L81
	movl	20(%rbx), %edx
.L83:
	movq	32(%rbx), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %r13
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%rbx)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%rbx), %edx
	cmpl	16(%rbx), %edx
	je	.L180
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L83
.L81:
	movq	%r12, %rdi
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	(%rax), %rax
	movslq	19(%rax), %r12
	cmpq	$-1, %r12
	je	.L42
	testq	%r12, %r12
	je	.L42
	movl	20(%rbx), %eax
	movl	8(%rbx), %esi
	movq	32(%rbx), %rdx
	leal	1(%rax), %ecx
	testl	%esi, %esi
	movq	(%rdx), %rdx
	movl	%ecx, 20(%rbx)
	jne	.L90
	addl	$16, %eax
	cltq
	movb	$58, -1(%rdx,%rax)
	movl	16(%rbx), %eax
	cmpl	%eax, 20(%rbx)
	je	.L91
.L92:
	movl	%r12d, %edi
	movq	%r14, %rsi
	movl	$100, %edx
	call	_ZN2v88internal12IntToCStringEiNS0_6VectorIcEE@PLT
	movq	%rax, %r12
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jne	.L174
.L171:
	movzbl	(%r12), %eax
	testb	%al, %al
	je	.L42
	movl	20(%rbx), %edx
.L97:
	movq	32(%rbx), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %r12
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%rbx)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%rbx), %edx
	cmpl	16(%rbx), %edx
	je	.L181
	movzbl	(%r12), %eax
	testb	%al, %al
	jne	.L97
	.p2align 4,,10
	.p2align 3
.L42:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L182
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movl	20(%rbx), %edx
	movl	$60, %eax
	leaq	.LC1(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L67:
	movq	32(%rbx), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %r13
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%rbx)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%rbx), %edx
	cmpl	16(%rbx), %edx
	je	.L183
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L67
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L183:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	0(%r13), %eax
	testb	%al, %al
	je	.L65
	movl	20(%rbx), %edx
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L162
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L43:
	movq	41088(%r14), %r13
	cmpq	41096(%r14), %r13
	je	.L184
.L45:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, 0(%r13)
	testb	$1, %sil
	je	.L49
.L46:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L49
	movq	0(%r13), %rax
	testb	$1, %al
	je	.L62
	.p2align 4,,10
	.p2align 3
.L176:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L62
	movl	11(%rax), %r11d
	testl	%r11d, %r11d
	jle	.L62
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L175:
	movq	%r12, %rdi
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	(%rax), %rax
	movq	79(%rax), %r15
	movq	(%r12), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r14
	movq	3520(%r14), %rdi
	subq	$37592, %r14
	testq	%rdi, %rdi
	je	.L51
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L52:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movl	8(%rbx), %r14d
	testl	%r14d, %r14d
	je	.L103
	movl	20(%rbx), %edx
	movl	$44, %eax
	leaq	.LC0(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L55:
	movq	32(%rbx), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %r14
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%rbx)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%rbx), %edx
	cmpl	16(%rbx), %edx
	je	.L185
	movzbl	(%r14), %eax
	testb	%al, %al
	jne	.L55
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L186:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
.L168:
	movzbl	0(%r13), %eax
	testb	%al, %al
	je	.L81
	movl	20(%rbx), %edx
.L82:
	movq	32(%rbx), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %r13
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%rbx)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%rbx), %edx
	cmpl	16(%rbx), %edx
	je	.L186
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L82
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L179:
	addl	$16, %eax
	cltq
	movb	$58, -1(%rdx,%rax)
	movl	16(%rbx), %eax
	cmpl	%eax, 20(%rbx)
	jne	.L78
.L77:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L51:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L187
.L53:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r15, (%rsi)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L188:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
.L174:
	movzbl	(%r12), %eax
	testb	%al, %al
	je	.L42
	movl	20(%rbx), %edx
.L96:
	movq	32(%rbx), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %r12
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%rbx)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%rbx), %edx
	cmpl	16(%rbx), %edx
	je	.L188
	movzbl	(%r12), %eax
	testb	%al, %al
	jne	.L96
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L180:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r14), %eax
	testb	%al, %al
	je	.L50
	movl	20(%rbx), %edx
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L103:
	movl	$44, %eax
	leaq	.LC0(%rip), %r14
.L161:
	movl	20(%rbx), %edx
.L54:
	movq	32(%rbx), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %r14
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%rbx)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%rbx), %edx
	cmpl	16(%rbx), %edx
	je	.L189
	movzbl	(%r14), %eax
	testb	%al, %al
	jne	.L54
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L189:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%r14), %eax
	testb	%al, %al
	jne	.L161
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L181:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L184:
	movq	%r14, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L90:
	leal	16(%rax,%rax), %eax
	movl	$58, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	16(%rbx), %eax
	cmpl	%eax, 20(%rbx)
	jne	.L92
.L91:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L92
.L187:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L53
.L182:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21763:
	.size	_ZN2v88internal12_GLOBAL__N_118AppendFileLocationEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEEPNS0_24IncrementalStringBuilderE.isra.0, .-_ZN2v88internal12_GLOBAL__N_118AppendFileLocationEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEEPNS0_24IncrementalStringBuilderE.isra.0
	.section	.text._ZN2v88internal15StackTraceFrame17GetFunctionOffsetENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame17GetFunctionOffsetENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame17GetFunctionOffsetENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame17GetFunctionOffsetENS0_6HandleIS1_EE:
.LFB21767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L196
.L191:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L192
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movslq	27(%rsi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L197
.L194:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	movslq	27(%rsi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	movq	%rdi, %rbx
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L197:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L194
	.cfi_endproc
.LFE21767:
	.size	_ZN2v88internal15StackTraceFrame17GetFunctionOffsetENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame17GetFunctionOffsetENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame18GetPromiseAllIndexENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame18GetPromiseAllIndexENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame18GetPromiseAllIndexENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame18GetPromiseAllIndexENS0_6HandleIS1_EE:
.LFB17921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L204
.L199:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L200
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movslq	27(%rsi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L205
.L202:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	movslq	27(%rsi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	movq	%rdi, %rbx
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L205:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L202
	.cfi_endproc
.LFE17921:
	.size	_ZN2v88internal15StackTraceFrame18GetPromiseAllIndexENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame18GetPromiseAllIndexENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame6IsEvalENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame6IsEvalENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame6IsEvalENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame6IsEvalENS0_6HandleIS1_EE:
.LFB17931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L212
.L207:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L208
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movslq	107(%rsi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L213
.L210:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	movslq	107(%rsi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	movq	%rdi, %rbx
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L213:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L210
	.cfi_endproc
.LFE17931:
	.size	_ZN2v88internal15StackTraceFrame6IsEvalENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame6IsEvalENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame16IsUserJavaScriptENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame16IsUserJavaScriptENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame16IsUserJavaScriptENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame16IsUserJavaScriptENS0_6HandleIS1_EE:
.LFB17935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L220
.L215:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L216
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L217:
	movslq	107(%rsi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sarl	$4, %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L221
.L218:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L220:
	movq	%rdi, %rbx
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L221:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L218
	.cfi_endproc
.LFE17935:
	.size	_ZN2v88internal15StackTraceFrame16IsUserJavaScriptENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame16IsUserJavaScriptENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame12IsPromiseAllENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame12IsPromiseAllENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame12IsPromiseAllENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame12IsPromiseAllENS0_6HandleIS1_EE:
.LFB17938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L228
.L223:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L224
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movslq	107(%rsi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	shrb	$7, %al
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L229
.L226:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	movslq	107(%rsi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	shrb	$7, %al
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	%rdi, %rbx
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L229:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L226
	.cfi_endproc
.LFE17938:
	.size	_ZN2v88internal15StackTraceFrame12IsPromiseAllENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame12IsPromiseAllENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame7IsAsyncENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame7IsAsyncENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame7IsAsyncENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame7IsAsyncENS0_6HandleIS1_EE:
.LFB17937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L236
.L231:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L232
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L233:
	movslq	107(%rsi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sarl	$6, %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L237
.L234:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%rdi, %rbx
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L237:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L234
	.cfi_endproc
.LFE17937:
	.size	_ZN2v88internal15StackTraceFrame7IsAsyncENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame7IsAsyncENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame11IsAsmJsWasmENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame11IsAsmJsWasmENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame11IsAsmJsWasmENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame11IsAsmJsWasmENS0_6HandleIS1_EE:
.LFB17934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L244
.L239:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L240
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L241:
	movslq	107(%rsi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sarl	$3, %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L245
.L242:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L244:
	movq	%rdi, %rbx
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L245:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L242
	.cfi_endproc
.LFE17934:
	.size	_ZN2v88internal15StackTraceFrame11IsAsmJsWasmENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame11IsAsmJsWasmENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame13IsConstructorENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame13IsConstructorENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame13IsConstructorENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame13IsConstructorENS0_6HandleIS1_EE:
.LFB17932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L252
.L247:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L248
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movslq	107(%rsi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sarl	%eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L253
.L250:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	movslq	107(%rsi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sarl	%eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	movq	%rdi, %rbx
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L253:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L250
	.cfi_endproc
.LFE17932:
	.size	_ZN2v88internal15StackTraceFrame13IsConstructorENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame13IsConstructorENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame6IsWasmENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame6IsWasmENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame6IsWasmENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame6IsWasmENS0_6HandleIS1_EE:
.LFB17933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L260
.L255:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L256
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L257:
	movslq	107(%rsi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sarl	$2, %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L261
.L258:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L260:
	movq	%rdi, %rbx
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L261:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L258
	.cfi_endproc
.LFE17933:
	.size	_ZN2v88internal15StackTraceFrame6IsWasmENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame6IsWasmENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame10IsToplevelENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame10IsToplevelENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame10IsToplevelENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame10IsToplevelENS0_6HandleIS1_EE:
.LFB17936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L268
.L263:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L264
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L265:
	movslq	107(%rsi), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sarl	$5, %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L269
.L266:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L268:
	movq	%rdi, %rbx
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L269:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L266
	.cfi_endproc
.LFE17936:
	.size	_ZN2v88internal15StackTraceFrame10IsToplevelENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame10IsToplevelENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame13GetLineNumberENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame13GetLineNumberENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame13GetLineNumberENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame13GetLineNumberENS0_6HandleIS1_EE:
.LFB17916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L277
.L271:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L272
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L273:
	movslq	11(%rsi), %rax
	movl	$0, %edx
	cmpq	$-1, %rax
	cmove	%rdx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L278
.L274:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%rdi, %rbx
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L278:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L274
	.cfi_endproc
.LFE17916:
	.size	_ZN2v88internal15StackTraceFrame13GetLineNumberENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame13GetLineNumberENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame15GetColumnNumberENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame15GetColumnNumberENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame15GetColumnNumberENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame15GetColumnNumberENS0_6HandleIS1_EE:
.LFB17918:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L286
.L280:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L281
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L282:
	movslq	19(%rsi), %rax
	movl	$0, %edx
	cmpq	$-1, %rax
	cmove	%rdx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L287
.L283:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L286:
	movq	%rdi, %rbx
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L287:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L283
	.cfi_endproc
.LFE17918:
	.size	_ZN2v88internal15StackTraceFrame15GetColumnNumberENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame15GetColumnNumberENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame11GetScriptIdENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame11GetScriptIdENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame11GetScriptIdENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame11GetScriptIdENS0_6HandleIS1_EE:
.LFB17920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L295
.L289:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L290
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L291:
	movslq	35(%rsi), %rax
	movl	$0, %edx
	cmpq	$-1, %rax
	cmove	%rdx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L296
.L292:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L295:
	movq	%rdi, %rbx
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L296:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L292
	.cfi_endproc
.LFE17920:
	.size	_ZN2v88internal15StackTraceFrame11GetScriptIdENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame11GetScriptIdENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame23GetOneBasedColumnNumberENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame23GetOneBasedColumnNumberENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame23GetOneBasedColumnNumberENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame23GetOneBasedColumnNumberENS0_6HandleIS1_EE:
.LFB17919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	(%rax), %rax
	movslq	19(%rax), %rbx
	cmpq	$-1, %rbx
	je	.L304
	movl	%ebx, %r13d
	notl	%ebx
	shrl	$31, %ebx
.L298:
	movq	(%r12), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L311
.L299:
	andq	$-262144, %rax
	movq	24(%rax), %r12
	movq	3520(%r12), %rdi
	subq	$37592, %r12
	testq	%rdi, %rdi
	je	.L300
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L301:
	testb	$4, 107(%rsi)
	je	.L297
	cmpb	$1, %bl
	sbbl	$-1, %r13d
.L297:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L312
.L302:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L304:
	movl	$1, %ebx
	xorl	%r13d, %r13d
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L311:
	movq	%r12, %rdi
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%r12), %rax
	movq	23(%rax), %rsi
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L312:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L302
	.cfi_endproc
.LFE17919:
	.size	_ZN2v88internal15StackTraceFrame23GetOneBasedColumnNumberENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame23GetOneBasedColumnNumberENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame21GetOneBasedLineNumberENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame21GetOneBasedLineNumberENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame21GetOneBasedLineNumberENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame21GetOneBasedLineNumberENS0_6HandleIS1_EE:
.LFB17917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	(%rax), %rax
	movslq	11(%rax), %rbx
	cmpq	$-1, %rbx
	je	.L320
	movl	%ebx, %r13d
	notl	%ebx
	shrl	$31, %ebx
.L314:
	movq	(%r12), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L327
.L315:
	andq	$-262144, %rax
	movq	24(%rax), %r12
	movq	3520(%r12), %rdi
	subq	$37592, %r12
	testq	%rdi, %rdi
	je	.L316
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L317:
	testb	$4, 107(%rsi)
	je	.L313
	cmpb	$1, %bl
	sbbl	$-1, %r13d
.L313:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L328
.L318:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L320:
	movl	$1, %ebx
	xorl	%r13d, %r13d
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L327:
	movq	%r12, %rdi
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%r12), %rax
	movq	23(%rax), %rsi
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L328:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L318
	.cfi_endproc
.LFE17917:
	.size	_ZN2v88internal15StackTraceFrame21GetOneBasedLineNumberENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame21GetOneBasedLineNumberENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame11GetTypeNameENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame11GetTypeNameENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame11GetTypeNameENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame11GetTypeNameENS0_6HandleIS1_EE:
.LFB17927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L338
.L330:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L331
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L332:
	movq	(%r12), %rax
	movq	71(%rsi), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L334
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L339
.L333:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L334:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L340
.L336:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%r12), %rax
	movq	23(%rax), %rsi
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L339:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L340:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L336
	.cfi_endproc
.LFE17927:
	.size	_ZN2v88internal15StackTraceFrame11GetTypeNameENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame11GetTypeNameENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame13GetEvalOriginENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame13GetEvalOriginENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame13GetEvalOriginENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame13GetEvalOriginENS0_6HandleIS1_EE:
.LFB17928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L350
.L342:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L343
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L344:
	movq	(%r12), %rax
	movq	79(%rsi), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L346
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L351
.L345:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L346:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L352
.L348:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%r12), %rax
	movq	23(%rax), %rsi
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L351:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L352:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L348
	.cfi_endproc
.LFE17928:
	.size	_ZN2v88internal15StackTraceFrame13GetEvalOriginENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame13GetEvalOriginENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame17GetWasmModuleNameENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame17GetWasmModuleNameENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame17GetWasmModuleNameENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame17GetWasmModuleNameENS0_6HandleIS1_EE:
.LFB17929:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L362
.L354:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L355
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L356:
	movq	(%r12), %rax
	movq	87(%rsi), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L358
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L355:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L363
.L357:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L358:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L364
.L360:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%r12), %rax
	movq	23(%rax), %rsi
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L363:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L364:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L360
	.cfi_endproc
.LFE17929:
	.size	_ZN2v88internal15StackTraceFrame17GetWasmModuleNameENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame17GetWasmModuleNameENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame11GetFileNameENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame11GetFileNameENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame11GetFileNameENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame11GetFileNameENS0_6HandleIS1_EE:
.LFB17923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L374
.L366:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L367
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L368:
	movq	(%r12), %rax
	movq	39(%rsi), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L370
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L375
.L369:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L370:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L376
.L372:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%r12), %rax
	movq	23(%rax), %rsi
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L375:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L376:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L372
	.cfi_endproc
.LFE17923:
	.size	_ZN2v88internal15StackTraceFrame11GetFileNameENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame11GetFileNameENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame15GetFunctionNameENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame15GetFunctionNameENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame15GetFunctionNameENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame15GetFunctionNameENS0_6HandleIS1_EE:
.LFB17925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L386
.L378:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L379
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L380:
	movq	(%r12), %rax
	movq	55(%rsi), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L382
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L387
.L381:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L382:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L388
.L384:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_restore_state
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%r12), %rax
	movq	23(%rax), %rsi
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L387:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L388:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L384
	.cfi_endproc
.LFE17925:
	.size	_ZN2v88internal15StackTraceFrame15GetFunctionNameENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame15GetFunctionNameENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame24GetScriptNameOrSourceUrlENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame24GetScriptNameOrSourceUrlENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame24GetScriptNameOrSourceUrlENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame24GetScriptNameOrSourceUrlENS0_6HandleIS1_EE:
.LFB17924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L398
.L390:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L391
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L392:
	movq	(%r12), %rax
	movq	47(%rsi), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L394
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L391:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L399
.L393:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L394:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L400
.L396:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	.cfi_restore_state
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%r12), %rax
	movq	23(%rax), %rsi
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L399:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L400:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L396
	.cfi_endproc
.LFE17924:
	.size	_ZN2v88internal15StackTraceFrame24GetScriptNameOrSourceUrlENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame24GetScriptNameOrSourceUrlENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame15GetWasmInstanceENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame15GetWasmInstanceENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame15GetWasmInstanceENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame15GetWasmInstanceENS0_6HandleIS1_EE:
.LFB17930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L410
.L402:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L403
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L404:
	movq	(%r12), %rax
	movq	95(%rsi), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L406
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L411
.L405:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L406:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L412
.L408:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore_state
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%r12), %rax
	movq	23(%rax), %rsi
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L411:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L412:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L408
	.cfi_endproc
.LFE17930:
	.size	_ZN2v88internal15StackTraceFrame15GetWasmInstanceENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame15GetWasmInstanceENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal15StackTraceFrame13GetMethodNameENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15StackTraceFrame13GetMethodNameENS0_6HandleIS1_EE
	.type	_ZN2v88internal15StackTraceFrame13GetMethodNameENS0_6HandleIS1_EE, @function
_ZN2v88internal15StackTraceFrame13GetMethodNameENS0_6HandleIS1_EE:
.LFB17926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	23(%rax), %rsi
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L422
.L414:
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L415
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L416:
	movq	(%r12), %rax
	movq	63(%rsi), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L418
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L423
.L417:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L418:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L424
.L420:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	call	_ZN2v88internal15StackTraceFrame19InitializeFrameInfoENS0_6HandleIS1_EE
	movq	(%r12), %rax
	movq	23(%rax), %rsi
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L423:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L420
	.cfi_endproc
.LFE17926:
	.size	_ZN2v88internal15StackTraceFrame13GetMethodNameENS0_6HandleIS1_EE, .-_ZN2v88internal15StackTraceFrame13GetMethodNameENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal27GetFrameArrayFromStackTraceEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27GetFrameArrayFromStackTraceEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEE
	.type	_ZN2v88internal27GetFrameArrayFromStackTraceEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEE, @function
_ZN2v88internal27GetFrameArrayFromStackTraceEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEE:
.LFB17941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	(%rsi), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jne	.L426
	addq	$24, %rsp
	xorl	%edx, %edx
	xorl	%esi, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory13NewFrameArrayEiNS0_14AllocationTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	movq	15(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L427
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L428:
	movq	41112(%r12), %rdi
	movq	7(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L430
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L427:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L434
.L429:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L430:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L435
.L432:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L435:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L434:
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L429
	.cfi_endproc
.LFE17941:
	.size	_ZN2v88internal27GetFrameArrayFromStackTraceEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEE, .-_ZN2v88internal27GetFrameArrayFromStackTraceEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEE
	.section	.rodata._ZN2v88internal24SerializeStackTraceFrameEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEERNS0_24IncrementalStringBuilderE.str1.1,"aMS",@progbits,1
.LC2:
	.string	" ("
.LC3:
	.string	"wasm-function["
.LC4:
	.string	"]:"
.LC5:
	.string	"async "
.LC6:
	.string	"Promise.all (index "
.LC7:
	.string	" [as "
.LC8:
	.string	"new "
.LC9:
	.string	"0x%x"
	.section	.text._ZN2v88internal24SerializeStackTraceFrameEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEERNS0_24IncrementalStringBuilderE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24SerializeStackTraceFrameEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEERNS0_24IncrementalStringBuilderE
	.type	_ZN2v88internal24SerializeStackTraceFrameEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEERNS0_24IncrementalStringBuilderE, @function
_ZN2v88internal24SerializeStackTraceFrameEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEERNS0_24IncrementalStringBuilderE:
.LFB17954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	%r12, %rdi
	movq	(%rax), %rax
	testb	$8, 107(%rax)
	je	.L437
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	(%rax), %rax
	movq	55(%rax), %rsi
	movq	(%r12), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L438
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	testb	$1, %sil
	jne	.L923
.L442:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118AppendFileLocationEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEEPNS0_24IncrementalStringBuilderE.isra.0
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L924
	.p2align 4,,10
	.p2align 3
.L436:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L925
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore_state
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L926
.L440:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	testb	$1, %sil
	je	.L442
.L923:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L442
	movq	0(%r13), %rax
	movl	11(%rax), %r9d
	testl	%r9d, %r9d
	jle	.L442
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movl	8(%r15), %r8d
	testl	%r8d, %r8d
	je	.L711
	movl	20(%r15), %edx
	movl	$32, %ecx
	leaq	.LC2(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L446:
	movq	32(%r15), %rax
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rax), %rax
	movl	%esi, 20(%r15)
	movw	%cx, -1(%rax,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L927
	movzbl	(%rbx), %ecx
	testb	%cl, %cl
	jne	.L446
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L437:
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	%r12, %rdi
	movq	(%rax), %rax
	movslq	107(%rax), %r14
	sarl	$2, %r14d
	movl	%r14d, %eax
	andl	$1, %eax
	movb	%al, -264(%rbp)
	je	.L462
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	(%rax), %rax
	movq	87(%rax), %rsi
	movq	(%r12), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L463
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L464:
	movq	%r12, %rdi
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	(%rax), %rax
	movq	55(%rax), %rsi
	movq	(%r12), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L466
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L467:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L928
.L706:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L929
.L473:
	movl	8(%r15), %esi
	testl	%esi, %esi
	jne	.L930
	movl	20(%r15), %edx
	movq	32(%r15), %rax
	leal	1(%rdx), %ecx
	movq	(%rax), %rax
	addl	$16, %edx
	movslq	%edx, %rdx
	movl	%ecx, 20(%r15)
	movb	$46, -1(%rax,%rdx)
	movl	16(%r15), %eax
	cmpl	%eax, 20(%r15)
	je	.L480
.L471:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
.L477:
	movl	8(%r15), %eax
	testl	%eax, %eax
	je	.L712
	movl	20(%r15), %edx
	movl	$32, %eax
	leaq	.LC2(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L482:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L931
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L482
.L472:
	movq	%r12, %rdi
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	(%rax), %rax
	movslq	11(%rax), %r13
	movl	$0, %eax
	cmpq	$-1, %r13
	cmove	%rax, %r13
	movl	8(%r15), %eax
	testl	%eax, %eax
	je	.L713
	movl	20(%r15), %edx
	movl	$119, %eax
	leaq	.LC3(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L490:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L932
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L490
.L491:
	leaq	-160(%rbp), %rbx
	movl	%r13d, %edi
	movl	$100, %edx
	movq	%rbx, %rsi
	call	_ZN2v88internal12IntToCStringEiNS0_6VectorIcEE@PLT
	movl	8(%r15), %r14d
	movq	%rax, %r13
	testl	%r14d, %r14d
	je	.L496
	movzbl	(%rax), %ecx
	movl	20(%r15), %edx
	testb	%cl, %cl
	je	.L509
	.p2align 4,,10
	.p2align 3
.L498:
	movq	32(%r15), %rax
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %r13
	movslq	%edx, %rdx
	movq	(%rax), %rax
	movl	%esi, 20(%r15)
	movw	%cx, -1(%rax,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L933
	movzbl	0(%r13), %ecx
	testb	%cl, %cl
	jne	.L498
.L505:
	movl	8(%r15), %eax
	testl	%eax, %eax
	jne	.L509
.L508:
	movl	$93, %eax
	leaq	.LC4(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L511:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %r13
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L934
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L511
.L510:
	movq	%r12, %rdi
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	leaq	.LC9(%rip), %rdx
	movq	%rbx, %rdi
	movl	$16, %esi
	movq	(%rax), %rax
	movslq	19(%rax), %rcx
	movl	$0, %eax
	cmpq	$-1, %rcx
	cmove	%rax, %rcx
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movl	8(%r15), %r11d
	testl	%r11d, %r11d
	je	.L518
	movzbl	-160(%rbp), %eax
	testb	%al, %al
	je	.L519
.L899:
	movl	20(%r15), %edx
.L520:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L935
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L520
.L523:
	cmpb	$0, -264(%rbp)
	je	.L436
	movl	8(%r15), %r10d
	testl	%r10d, %r10d
	je	.L529
.L709:
	movq	32(%r15), %rax
	movq	(%rax), %rdx
	movl	20(%r15), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 20(%r15)
	.p2align 4,,10
	.p2align 3
.L893:
	leal	16(%rax,%rax), %eax
	movl	$41, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	16(%r15), %eax
	cmpl	%eax, 20(%r15)
	jne	.L436
.L705:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L462:
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	(%rax), %rax
	movq	55(%rax), %rsi
	movq	(%r12), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L532
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L533:
	movq	%r12, %rdi
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	103(%rax), %rax
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	%r12, %rdi
	movq	(%rax), %rax
	movslq	107(%rax), %rbx
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	%r12, %rdi
	movq	(%rax), %rax
	sarl	$6, %ebx
	andl	$1, %ebx
	movslq	107(%rax), %rax
	shrb	$7, %al
	movb	%al, -264(%rbp)
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	(%rax), %rax
	movq	103(%rax), %rax
	movq	%rax, -280(%rbp)
	testb	%bl, %bl
	je	.L535
	movl	8(%r15), %r9d
	testl	%r9d, %r9d
	je	.L714
	movl	20(%r15), %edx
	movl	$97, %eax
	leaq	.LC5(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L537:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L936
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L537
	.p2align 4,,10
	.p2align 3
.L535:
	cmpb	$0, -264(%rbp)
	jne	.L937
	movq	-280(%rbp), %rax
	sarq	$33, %rax
	movq	%rax, %rdx
	movq	-272(%rbp), %rax
	sarq	$37, %rax
	orl	%edx, %eax
	testb	$1, %al
	je	.L938
	andl	$1, %edx
	je	.L673
	movl	8(%r15), %r11d
	testl	%r11d, %r11d
	jne	.L939
	movl	$110, %eax
	leaq	.LC8(%rip), %rbx
.L904:
	movl	20(%r15), %edx
.L674:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L940
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L674
.L676:
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L941
.L682:
	movl	8(%r15), %r9d
	movl	20(%r15), %edx
	movl	$60, %eax
	leaq	.LC1(%rip), %rbx
	testl	%r9d, %r9d
	jne	.L686
	movl	$60, %eax
	leaq	.LC1(%rip), %rbx
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L907:
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L672
.L685:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	jne	.L907
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	20(%r15), %edx
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L463:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L942
.L465:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L532:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L943
.L534:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L930:
	movl	20(%r15), %edx
	movq	32(%r15), %rax
	leal	1(%rdx), %ecx
	movq	(%rax), %rax
	leal	16(%rdx,%rdx), %edx
	movl	%ecx, 20(%r15)
	movslq	%edx, %rdx
	movl	$46, %ecx
	movw	%cx, -1(%rax,%rdx)
	movl	16(%r15), %eax
	cmpl	%eax, 20(%r15)
	jne	.L471
.L480:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L934:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	0(%r13), %eax
	testb	%al, %al
	je	.L510
	movl	20(%r15), %edx
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L933:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	0(%r13), %ecx
	movl	20(%r15), %edx
	testb	%cl, %cl
	jne	.L498
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L935:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L899
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L932:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L491
	movl	20(%r15), %edx
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L931:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L472
	movl	20(%r15), %edx
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L924:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L436
	movl	11(%rax), %edi
	testl	%edi, %edi
	jle	.L436
.L922:
	movq	32(%r15), %rax
	movl	8(%r15), %esi
	movq	(%rax), %rdx
	movl	20(%r15), %eax
	testl	%esi, %esi
	leal	1(%rax), %ecx
	movl	%ecx, 20(%r15)
	jne	.L893
.L916:
	addl	$16, %eax
	cltq
	movb	$41, -1(%rdx,%rax)
	movl	16(%r15), %eax
	cmpl	%eax, 20(%r15)
	jne	.L436
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L927:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %ecx
	testb	%cl, %cl
	je	.L442
	movl	20(%r15), %edx
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L509:
	movl	$93, %eax
	leaq	.LC4(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L514:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %r13
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L944
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L514
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L944:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	0(%r13), %eax
	testb	%al, %al
	je	.L510
	movl	20(%r15), %edx
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L496:
	movzbl	(%rax), %ecx
	movl	20(%r15), %edx
	testb	%cl, %cl
	jne	.L500
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L914:
	movzbl	0(%r13), %ecx
	testb	%cl, %cl
	je	.L505
.L500:
	movq	32(%r15), %rax
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %r13
	movslq	%edx, %rdx
	movq	(%rax), %rax
	movl	%esi, 20(%r15)
	movb	%cl, -1(%rax,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	jne	.L914
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	20(%r15), %edx
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L518:
	movzbl	-160(%rbp), %eax
	testb	%al, %al
	je	.L521
.L898:
	movl	20(%r15), %edx
.L522:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L945
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L522
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L945:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L898
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L713:
	movl	$119, %eax
	leaq	.LC3(%rip), %rbx
.L897:
	movl	20(%r15), %edx
.L489:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L946
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L489
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L946:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L897
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L712:
	movl	$32, %eax
	leaq	.LC2(%rip), %rbx
.L896:
	movl	20(%r15), %edx
.L481:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L947
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L481
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L947:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L896
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L466:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L948
.L468:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L936:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L535
	movl	20(%r15), %edx
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L937:
	movl	8(%r15), %r8d
	testl	%r8d, %r8d
	jne	.L949
	movl	$80, %eax
	leaq	.LC6(%rip), %rbx
.L901:
	movl	20(%r15), %edx
.L543:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L950
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L543
.L545:
	movq	%r12, %rdi
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	leaq	-160(%rbp), %rsi
	movl	$100, %edx
	movq	(%rax), %rax
	movslq	27(%rax), %rdi
	call	_ZN2v88internal12IntToCStringEiNS0_6VectorIcEE@PLT
	movl	8(%r15), %edi
	movq	%rax, %rbx
	testl	%edi, %edi
	je	.L550
	movzbl	(%rax), %edx
	movl	20(%r15), %eax
	testb	%dl, %dl
	je	.L563
	.p2align 4,,10
	.p2align 3
.L552:
	movq	32(%r15), %rcx
	addq	$1, %rbx
	movq	(%rcx), %rsi
	leal	1(%rax), %ecx
	movl	%ecx, 20(%r15)
	leal	16(%rax,%rax), %ecx
	movslq	%ecx, %rcx
	movw	%dx, -1(%rsi,%rcx)
	movl	20(%r15), %eax
	cmpl	16(%r15), %eax
	je	.L951
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L552
.L559:
	movl	8(%r15), %edx
	testl	%edx, %edx
	jne	.L563
.L562:
	movq	32(%r15), %rdx
	leal	1(%rax), %ecx
	movq	(%rdx), %rdx
	movl	%ecx, 20(%r15)
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L928:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37488(%rdx), %rax
	jne	.L706
	movq	(%r14), %rax
	testb	$1, %al
	je	.L471
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37488(%rdx), %rax
	jne	.L471
	movb	$0, -264(%rbp)
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L711:
	movl	$32, %eax
	leaq	.LC2(%rip), %rbx
.L895:
	movl	20(%r15), %edx
.L445:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L952
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L445
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L952:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L895
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L929:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37488(%rdx), %rax
	je	.L477
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L714:
	movl	$97, %eax
	leaq	.LC5(%rip), %rbx
.L900:
	movl	20(%r15), %edx
.L536:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L953
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L536
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L953:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L900
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L521:
	cmpb	$0, -264(%rbp)
	je	.L436
	.p2align 4,,10
	.p2align 3
.L529:
	movq	32(%r15), %rax
	movq	(%rax), %rdx
	movl	20(%r15), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 20(%r15)
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L938:
	movq	%r12, %rdi
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	(%rax), %rax
	movq	71(%rax), %rsi
	movq	(%r12), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L569
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -264(%rbp)
.L570:
	movq	%r12, %rdi
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	(%rax), %rax
	movq	63(%rax), %rsi
	movq	(%r12), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L572
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L573:
	movq	%r12, %rdi
	call	_ZN2v88internal15StackTraceFrame12GetFrameInfoENS0_6HandleIS1_EE
	movq	(%rax), %rax
	movq	55(%rax), %rsi
	movq	(%r12), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	movq	3520(%rdx), %rdi
	subq	$37592, %rdx
	testq	%rdi, %rdi
	je	.L575
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L576:
	testb	$1, %sil
	jne	.L578
	movq	-264(%rbp), %rax
	movq	(%rax), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
.L579:
	testb	%al, %al
	je	.L954
.L656:
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L955
.L663:
	movl	8(%r15), %ebx
	movl	20(%r15), %edx
	testl	%ebx, %ebx
	je	.L717
	movl	$60, %eax
	leaq	.LC1(%rip), %rbx
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L909:
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L672
.L667:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	jne	.L909
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	20(%r15), %edx
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L949:
	movl	20(%r15), %edx
	movl	$80, %eax
	leaq	.LC6(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L544:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L956
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L544
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L926:
	movq	%rbx, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L939:
	movl	20(%r15), %edx
	movl	$110, %eax
	leaq	.LC8(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L675:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L957
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L675
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L958:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	20(%r15), %edx
.L905:
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L672
.L686:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	jne	.L905
	jmp	.L958
	.p2align 4,,10
	.p2align 3
.L569:
	movq	41088(%rbx), %rax
	movq	%rax, -264(%rbp)
	cmpq	41096(%rbx), %rax
	je	.L959
.L571:
	movq	-264(%rbp), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L563:
	movq	32(%r15), %rdx
	leal	1(%rax), %ecx
	movq	(%rdx), %rdx
	movl	%ecx, 20(%r15)
	jmp	.L893
.L580:
	movq	(%rbx), %rcx
	andl	$1, %eax
	movl	11(%rcx), %esi
	testl	%esi, %esi
	jle	.L579
	testb	%al, %al
	je	.L960
.L583:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L592
.L912:
	movl	20(%r15), %edx
	.p2align 4,,10
	.p2align 3
.L672:
	movl	8(%r15), %edi
	movl	$32, %eax
	leaq	.LC2(%rip), %rbx
	testl	%edi, %edi
	jne	.L696
	movl	$32, %eax
	leaq	.LC2(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L695:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L961
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L695
.L697:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118AppendFileLocationEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEEPNS0_24IncrementalStringBuilderE.isra.0
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L962:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L697
	movl	20(%r15), %edx
	.p2align 4,,10
	.p2align 3
.L696:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L962
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L696
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L961:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L697
	movl	20(%r15), %edx
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L951:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %edx
	movl	20(%r15), %eax
	testb	%dl, %dl
	jne	.L552
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L950:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L901
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L956:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L545
	movl	20(%r15), %edx
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L940:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L904
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L957:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L676
	movl	20(%r15), %edx
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L550:
	movzbl	(%rax), %edx
	movl	20(%r15), %eax
	testb	%dl, %dl
	jne	.L554
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L915:
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	je	.L559
.L554:
	movq	32(%r15), %rcx
	leal	1(%rax), %esi
	addl	$16, %eax
	addq	$1, %rbx
	cltq
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movb	%dl, -1(%rcx,%rax)
	movl	20(%r15), %eax
	cmpl	16(%r15), %eax
	jne	.L915
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	20(%r15), %eax
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L673:
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L963
.L692:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_118AppendFileLocationEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEEPNS0_24IncrementalStringBuilderE.isra.0
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L575:
	movq	41088(%rdx), %rbx
	cmpq	41096(%rdx), %rbx
	je	.L964
.L577:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rbx)
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L572:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L965
.L574:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L941:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L682
	movl	11(%rax), %r10d
	testl	%r10d, %r10d
	jle	.L682
.L908:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movl	20(%r15), %edx
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L948:
	movq	%rbx, %rdi
	movq	%rsi, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L943:
	movq	%rbx, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L942:
	movq	%rbx, %rdi
	movq	%rsi, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L578:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	movq	-264(%rbp), %rax
	movq	(%rax), %rdx
	movq	%rdx, %rax
	notq	%rax
	jbe	.L580
	andl	$1, %eax
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L519:
	cmpb	$0, -264(%rbp)
	jne	.L709
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L963:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L692
	movl	11(%rax), %r8d
	testl	%r8d, %r8d
	jg	.L908
	jmp	.L692
.L717:
	movl	$60, %eax
	leaq	.LC1(%rip), %rbx
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L911:
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L672
.L666:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	jne	.L911
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	20(%r15), %edx
	jmp	.L911
.L955:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L663
	movl	11(%rax), %r13d
	testl	%r13d, %r13d
	jg	.L908
	jmp	.L663
.L954:
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	ja	.L656
	movl	11(%rdx), %edi
	testl	%edi, %edi
	jle	.L656
	movq	-264(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	32(%r15), %rax
	movl	8(%r15), %esi
	movq	(%rax), %rdx
	movl	20(%r15), %eax
	testl	%esi, %esi
	leal	1(%rax), %ecx
	movl	%ecx, 20(%r15)
	jne	.L659
	addl	$16, %eax
	cltq
	movb	$46, -1(%rdx,%rax)
	movl	16(%r15), %eax
	cmpl	%eax, 20(%r15)
	jne	.L656
.L660:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L656
.L960:
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	ja	.L583
	movl	11(%rdx), %eax
	testl	%eax, %eax
	jle	.L583
	cmpl	11(%rcx), %eax
	jg	.L587
	movq	-264(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i@PLT
	testl	%eax, %eax
	je	.L583
.L587:
	movq	-264(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movl	8(%r15), %eax
	testl	%eax, %eax
	movq	32(%r15), %rax
	movq	(%rax), %rdx
	movl	20(%r15), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 20(%r15)
	jne	.L589
	addl	$16, %eax
	cltq
	movb	$46, -1(%rdx,%rax)
	movl	16(%r15), %eax
	cmpl	%eax, 20(%r15)
	jne	.L583
.L591:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L592:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L912
	movl	11(%rax), %r10d
	testl	%r10d, %r10d
	jle	.L912
	cmpq	%r14, %rbx
	je	.L912
	movq	(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L912
	movq	-1(%rdx), %rcx
	movzwl	11(%rcx), %ecx
	andl	$65504, %ecx
	jne	.L600
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L599
.L600:
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	jne	.L912
	movq	(%rbx), %rdx
.L599:
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	ja	.L603
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	je	.L966
.L603:
	movq	(%rbx), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L610
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L610
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L613
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L610:
	movq	%rbx, %rdx
	leaq	-256(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal16FlatStringReaderC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L616
	movq	%r14, %rdx
.L617:
	movq	(%rdx), %rax
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L624
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpw	$5, %cx
	jne	.L624
	movq	41112(%r13), %rdi
	movq	15(%rax), %rbx
	testq	%rdi, %rdi
	je	.L627
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L624:
	leaq	-208(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal16FlatStringReaderC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movl	-172(%rbp), %edx
	movslq	-220(%rbp), %rdi
	leal	-1(%rdx), %ecx
	leal	-1(%rdi), %eax
	testl	%edx, %edx
	js	.L630
	testl	%eax, %eax
	js	.L631
	cmpb	$0, -224(%rbp)
	movq	-216(%rbp), %rsi
	movzbl	-176(%rbp), %r10d
	movq	-168(%rbp), %r9
	je	.L632
	testb	%r10b, %r10b
	je	.L633
	movslq	%edx, %r10
	cltq
	xorl	%ecx, %ecx
	subq	%rdi, %r10
	addq	%r9, %r10
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L645:
	movzbl	(%r10,%rax), %r9d
	cmpl	%r9d, %edi
	jne	.L631
.L646:
	addl	$1, %ecx
	cmpl	%ecx, %edx
	jl	.L630
	subq	$1, %rax
	testl	%eax, %eax
	js	.L631
.L634:
	movzbl	(%rsi,%rax), %edi
	cmpl	%ecx, %edx
	jne	.L645
	cmpl	$46, %edi
	je	.L646
.L631:
	movq	-200(%rbp), %rax
	movq	-192(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	movq	-248(%rbp), %rax
	movq	-240(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	movl	8(%r15), %eax
	testl	%eax, %eax
	je	.L721
	movl	$32, %eax
	leaq	.LC7(%rip), %rbx
.L903:
	movl	20(%r15), %edx
.L708:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	leal	16(%rdx,%rdx), %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movw	%ax, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L967
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L708
.L648:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	32(%r15), %rax
	movl	8(%r15), %r9d
	movq	(%rax), %rdx
	movl	20(%r15), %eax
	testl	%r9d, %r9d
	leal	1(%rax), %ecx
	movl	%ecx, 20(%r15)
	jne	.L653
	addl	$16, %eax
	cltq
	movb	$93, -1(%rdx,%rax)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	jne	.L672
.L913:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movl	20(%r15), %edx
	jmp	.L672
.L659:
	leal	16(%rax,%rax), %eax
	movl	$46, %ecx
	cltq
	movw	%cx, -1(%rdx,%rax)
	movl	16(%r15), %eax
	cmpl	%eax, 20(%r15)
	jne	.L656
	jmp	.L660
.L964:
	movq	%rdx, %rdi
	movq	%rsi, -280(%rbp)
	movq	%rdx, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L577
.L959:
	movq	%rbx, %rdi
	movq	%rsi, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	%rax, -264(%rbp)
	jmp	.L571
.L965:
	movq	%rbx, %rdi
	movq	%rsi, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L574
.L967:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L903
	jmp	.L648
.L721:
	movl	$32, %eax
	leaq	.LC7(%rip), %rbx
.L902:
	movl	20(%r15), %edx
.L707:
	movq	32(%r15), %rcx
	leal	1(%rdx), %esi
	addl	$16, %edx
	addq	$1, %rbx
	movslq	%edx, %rdx
	movq	(%rcx), %rcx
	movl	%esi, 20(%r15)
	movb	%al, -1(%rcx,%rdx)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	je	.L968
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L707
	jmp	.L648
.L968:
	movq	%r15, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6ExtendEv@PLT
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L902
	jmp	.L648
.L653:
	leal	16(%rax,%rax), %eax
	movl	$93, %r8d
	cltq
	movw	%r8w, -1(%rdx,%rax)
	movl	20(%r15), %edx
	cmpl	16(%r15), %edx
	jne	.L672
	jmp	.L913
.L632:
	cltq
	testb	%r10b, %r10b
	jne	.L638
	movslq	%edx, %rcx
	subq	%rdi, %rcx
	leaq	(%r9,%rcx,2), %r9
	xorl	%ecx, %ecx
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L639:
	cmpw	%di, (%r9,%rax,2)
	jne	.L631
.L640:
	addl	$1, %ecx
	cmpl	%ecx, %edx
	jl	.L630
	subq	$1, %rax
	testl	%eax, %eax
	js	.L631
.L641:
	movzwl	(%rsi,%rax,2), %edi
	cmpl	%ecx, %edx
	jne	.L639
	cmpw	$46, %di
	je	.L640
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L616:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %ecx
	movq	%r14, %rdx
	andl	$7, %ecx
	cmpw	$1, %cx
	jne	.L617
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	subw	$1, %dx
	jne	.L619
	movq	23(%rax), %rdx
	cmpl	$0, 11(%rdx)
	je	.L619
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	jmp	.L624
.L589:
	leal	16(%rax,%rax), %eax
	movl	$46, %r11d
	cltq
	movw	%r11w, -1(%rdx,%rax)
	movl	16(%r15), %eax
	cmpl	%eax, 20(%r15)
	jne	.L583
	jmp	.L591
.L638:
	movslq	%edx, %r10
	xorl	%ecx, %ecx
	subq	%rdi, %r10
	addq	%r10, %r9
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L969:
	movzbl	(%r9,%rax), %r10d
	cmpl	%edi, %r10d
	jne	.L631
.L643:
	addl	$1, %ecx
	cmpl	%ecx, %edx
	jl	.L630
	subq	$1, %rax
	testl	%eax, %eax
	js	.L631
.L644:
	movzwl	(%rsi,%rax,2), %edi
	cmpl	%ecx, %edx
	jne	.L969
	cmpl	$46, %edi
	je	.L643
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L633:
	movslq	%edi, %r10
	movslq	%edx, %r11
	movslq	%ecx, %rcx
	xorl	%eax, %eax
	subq	%r11, %r10
	addq	%rsi, %r10
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L970:
	cmpw	(%r9,%rcx,2), %si
	jne	.L631
.L636:
	addl	$1, %eax
	cmpl	%eax, %edx
	jl	.L630
	subq	$1, %rcx
	cmpl	%eax, %edi
	je	.L631
.L637:
	movzbl	(%r10,%rcx), %esi
	cmpl	%eax, %edx
	jne	.L970
	cmpb	$46, %sil
	je	.L636
	jmp	.L631
.L630:
	movq	-200(%rbp), %rax
	movq	-192(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	movq	-248(%rbp), %rax
	movq	-240(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	jmp	.L912
.L966:
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	subw	$1, %ax
	jne	.L605
	movq	23(%rdx), %rax
	cmpl	$0, 11(%rax)
	je	.L605
	movq	%rbx, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	jmp	.L610
.L619:
	movq	41112(%r13), %rdi
	movq	15(%rax), %rbx
	testq	%rdi, %rdi
	je	.L621
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
	jmp	.L617
.L627:
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L971
.L629:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%rbx, (%rdx)
	jmp	.L624
.L613:
	movq	41088(%r13), %rbx
	cmpq	41096(%r13), %rbx
	je	.L972
.L615:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rbx)
	jmp	.L610
.L605:
	movq	41112(%r13), %rdi
	movq	15(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L607
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
	jmp	.L603
.L925:
	call	__stack_chk_fail@PLT
.L621:
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L973
.L623:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%rbx, (%rdx)
	jmp	.L617
.L607:
	movq	41088(%r13), %rbx
	cmpq	41096(%r13), %rbx
	je	.L974
.L609:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rbx)
	jmp	.L603
.L971:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L629
.L972:
	movq	%r13, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L615
.L973:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L623
.L974:
	movq	%r13, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L609
	.cfi_endproc
.LFE17954:
	.size	_ZN2v88internal24SerializeStackTraceFrameEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEERNS0_24IncrementalStringBuilderE, .-_ZN2v88internal24SerializeStackTraceFrameEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEERNS0_24IncrementalStringBuilderE
	.section	.text._ZN2v88internal24SerializeStackTraceFrameEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24SerializeStackTraceFrameEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEE
	.type	_ZN2v88internal24SerializeStackTraceFrameEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEE, @function
_ZN2v88internal24SerializeStackTraceFrameEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEE:
.LFB17955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	movq	%rdi, %rsi
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24SerializeStackTraceFrameEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEERNS0_24IncrementalStringBuilderE
	movq	%r13, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L978
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L978:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17955:
	.size	_ZN2v88internal24SerializeStackTraceFrameEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEE, .-_ZN2v88internal24SerializeStackTraceFrameEPNS0_7IsolateENS0_6HandleINS0_15StackTraceFrameEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal15StackTraceFrame13GetLineNumberENS0_6HandleIS1_EE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal15StackTraceFrame13GetLineNumberENS0_6HandleIS1_EE, @function
_GLOBAL__sub_I__ZN2v88internal15StackTraceFrame13GetLineNumberENS0_6HandleIS1_EE:
.LFB21699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21699:
	.size	_GLOBAL__sub_I__ZN2v88internal15StackTraceFrame13GetLineNumberENS0_6HandleIS1_EE, .-_GLOBAL__sub_I__ZN2v88internal15StackTraceFrame13GetLineNumberENS0_6HandleIS1_EE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal15StackTraceFrame13GetLineNumberENS0_6HandleIS1_EE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
