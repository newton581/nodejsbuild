	.file	"utils.cc"
	.text
	.section	.rodata._ZN2v88internal12_GLOBAL__N_117ReadCharsFromFileEP8_IO_FILEPbbPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot create std::vector larger than max_size()"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_117ReadCharsFromFileEP8_IO_FILEPbbPKc.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Cannot read from file %s.\n"
	.section	.text._ZN2v88internal12_GLOBAL__N_117ReadCharsFromFileEP8_IO_FILEPbbPKc,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117ReadCharsFromFileEP8_IO_FILEPbbPKc, @function
_ZN2v88internal12_GLOBAL__N_117ReadCharsFromFileEP8_IO_FILEPbbPKc:
.LFB5074:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$40, %rsp
	movq	%rdx, -64(%rbp)
	testq	%rsi, %rsi
	je	.L5
	movq	%rsi, %r14
	movl	$2, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	fseek@PLT
	testl	%eax, %eax
	je	.L21
.L5:
	testb	%bl, %bl
	jne	.L22
.L4:
	movq	-64(%rbp), %rax
	pxor	%xmm0, %xmm0
	movb	$0, (%rax)
	movq	$0, 16(%r12)
	movups	%xmm0, (%r12)
.L1:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movq	%r13, %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%r14, %rdi
	call	ftell@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	rewind@PLT
	testq	%r13, %r13
	js	.L23
	je	.L13
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	call	_Znwm@PLT
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%rax, -56(%rbp)
	movq	%rax, %rdi
	leaq	(%rax,%r13), %rax
	movq	%rax, -80(%rbp)
	call	memset@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L10:
	movq	-56(%rbp), %rax
	movq	%r13, %r15
	movq	%r14, %rcx
	movl	$1, %esi
	subq	%rbx, %r15
	movq	%r15, %rdx
	leaq	(%rax,%rbx), %rdi
	call	fread@PLT
	movq	%rax, %rdx
	cmpq	%rax, %r15
	je	.L12
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	ferror@PLT
	movq	-72(%rbp), %rdx
	testl	%eax, %eax
	jne	.L24
.L12:
	addq	%rdx, %rbx
	cmpq	%rbx, %r13
	jle	.L7
.L8:
	movq	%r14, %rdi
	call	feof@PLT
	testl	%eax, %eax
	je	.L10
.L7:
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %xmm0
	movb	$1, (%rax)
	movq	-80(%rbp), %rax
	movq	%rax, %xmm1
	movq	%rax, 16(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%r14, %rdi
	call	fclose@PLT
	movq	-64(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-56(%rbp), %rdi
	movb	$0, (%rax)
	movq	$0, 16(%r12)
	movups	%xmm0, (%r12)
	call	_ZdlPv@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L13:
	movq	$0, -56(%rbp)
	movq	$0, -80(%rbp)
	jmp	.L7
.L23:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5074:
	.size	_ZN2v88internal12_GLOBAL__N_117ReadCharsFromFileEP8_IO_FILEPbbPKc, .-_ZN2v88internal12_GLOBAL__N_117ReadCharsFromFileEP8_IO_FILEPbbPKc
	.section	.rodata._ZN2v88internal19SimpleStringBuilderC2Ei.str1.1,"aMS",@progbits,1
.LC2:
	.string	"NewArray"
	.section	.text._ZN2v88internal19SimpleStringBuilderC2Ei,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SimpleStringBuilderC2Ei
	.type	_ZN2v88internal19SimpleStringBuilderC2Ei, @function
_ZN2v88internal19SimpleStringBuilderC2Ei:
.LFB5054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movslq	%esi, %r12
	leaq	_ZSt7nothrow(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L28
.L26:
	movq	%r12, 8(%rbx)
	movq	%rax, (%rbx)
	movl	$0, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L28:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L26
	leaq	.LC2(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE5054:
	.size	_ZN2v88internal19SimpleStringBuilderC2Ei, .-_ZN2v88internal19SimpleStringBuilderC2Ei
	.globl	_ZN2v88internal19SimpleStringBuilderC1Ei
	.set	_ZN2v88internal19SimpleStringBuilderC1Ei,_ZN2v88internal19SimpleStringBuilderC2Ei
	.section	.text._ZN2v88internal19SimpleStringBuilder9AddStringEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SimpleStringBuilder9AddStringEPKc
	.type	_ZN2v88internal19SimpleStringBuilder9AddStringEPKc, @function
_ZN2v88internal19SimpleStringBuilder9AddStringEPKc:
.LFB5056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	strlen@PLT
	movslq	16(%rbx), %rdi
	movq	%r13, %rsi
	addq	(%rbx), %rdi
	movslq	%eax, %rdx
	movq	%rax, %r12
	call	memcpy@PLT
	addl	%r12d, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5056:
	.size	_ZN2v88internal19SimpleStringBuilder9AddStringEPKc, .-_ZN2v88internal19SimpleStringBuilder9AddStringEPKc
	.section	.text._ZN2v88internal19SimpleStringBuilder12AddSubstringEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SimpleStringBuilder12AddSubstringEPKci
	.type	_ZN2v88internal19SimpleStringBuilder12AddSubstringEPKci, @function
_ZN2v88internal19SimpleStringBuilder12AddSubstringEPKci:
.LFB5057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movslq	16(%rdi), %rdi
	addq	(%rbx), %rdi
	call	memcpy@PLT
	addl	%r12d, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5057:
	.size	_ZN2v88internal19SimpleStringBuilder12AddSubstringEPKci, .-_ZN2v88internal19SimpleStringBuilder12AddSubstringEPKci
	.section	.text._ZN2v88internal19SimpleStringBuilder10AddPaddingEci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SimpleStringBuilder10AddPaddingEci
	.type	_ZN2v88internal19SimpleStringBuilder10AddPaddingEci, @function
_ZN2v88internal19SimpleStringBuilder10AddPaddingEci:
.LFB5058:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jle	.L33
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L35:
	movslq	16(%rdi), %rax
	addl	$1, %ecx
	leal	1(%rax), %r8d
	movl	%r8d, 16(%rdi)
	movq	(%rdi), %r8
	movb	%sil, (%r8,%rax)
	cmpl	%ecx, %edx
	jne	.L35
.L33:
	ret
	.cfi_endproc
.LFE5058:
	.size	_ZN2v88internal19SimpleStringBuilder10AddPaddingEci, .-_ZN2v88internal19SimpleStringBuilder10AddPaddingEci
	.section	.text._ZN2v88internal19SimpleStringBuilder17AddDecimalIntegerEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SimpleStringBuilder17AddDecimalIntegerEi
	.type	_ZN2v88internal19SimpleStringBuilder17AddDecimalIntegerEi, @function
_ZN2v88internal19SimpleStringBuilder17AddDecimalIntegerEi:
.LFB5059:
	.cfi_startproc
	endbr64
	movslq	16(%rdi), %rax
	movq	(%rdi), %r8
	testl	%esi, %esi
	jns	.L39
	leal	1(%rax), %edx
	negl	%esi
	movl	%edx, 16(%rdi)
	movb	$45, (%r8,%rax)
	movslq	16(%rdi), %rax
	movq	(%rdi), %r8
.L39:
	cmpl	$9, %esi
	jbe	.L40
	cmpl	$99, %esi
	jbe	.L44
	cmpl	$999, %esi
	jbe	.L45
	cmpl	$9999, %esi
	jbe	.L46
	cmpl	$99999, %esi
	jbe	.L47
	cmpl	$999999, %esi
	jbe	.L48
	cmpl	$9999999, %esi
	jbe	.L49
	cmpl	$99999999, %esi
	jbe	.L50
	cmpl	$999999999, %esi
	jbe	.L51
	movl	%esi, %r9d
	movl	$3435973837, %r10d
	leal	10(%rax), %edx
	addl	$9, %eax
	imulq	%r10, %r9
	movl	%edx, 16(%rdi)
	movslq	%eax, %rdx
	shrq	$35, %r9
	leal	(%r9,%r9,4), %ecx
	addl	%ecx, %ecx
	subl	%ecx, %esi
	addl	$48, %esi
	movb	%sil, (%r8,%rdx)
	movl	16(%rdi), %eax
	movl	%r9d, %edx
	movq	(%rdi), %r8
	leal	-2(%rax), %ecx
	movl	%r9d, %eax
	imulq	%r10, %rax
	movslq	%ecx, %rcx
	shrq	$35, %rax
	leal	(%rax,%rax,4), %esi
	addl	%esi, %esi
	subl	%esi, %edx
	addl	$48, %edx
	movb	%dl, (%r8,%rcx)
	movl	$10, %ecx
.L43:
	movl	16(%rdi), %esi
	movl	%eax, %edx
	movq	(%rdi), %r10
	leal	-3(%rsi), %r8d
	movl	$3435973837, %esi
	imulq	%rsi, %rdx
	movslq	%r8d, %r8
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %r9d
	addl	%r9d, %r9d
	subl	%r9d, %eax
	addl	$48, %eax
	movb	%al, (%r10,%r8)
	cmpl	$3, %ecx
	je	.L37
	movl	16(%rdi), %eax
	movq	(%rdi), %r10
	leal	-4(%rax), %r8d
	movl	%edx, %eax
	imulq	%rsi, %rax
	movslq	%r8d, %r8
	shrq	$35, %rax
	leal	(%rax,%rax,4), %r9d
	addl	%r9d, %r9d
	subl	%r9d, %edx
	addl	$48, %edx
	movb	%dl, (%r10,%r8)
	cmpl	$4, %ecx
	je	.L37
	movl	16(%rdi), %edx
	movq	(%rdi), %r10
	leal	-5(%rdx), %r8d
	movl	%eax, %edx
	imulq	%rsi, %rdx
	movslq	%r8d, %r8
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %r9d
	addl	%r9d, %r9d
	subl	%r9d, %eax
	addl	$48, %eax
	movb	%al, (%r10,%r8)
	cmpl	$5, %ecx
	je	.L37
	movl	16(%rdi), %eax
	movq	(%rdi), %r10
	leal	-6(%rax), %r8d
	movl	%edx, %eax
	imulq	%rsi, %rax
	movslq	%r8d, %r8
	shrq	$35, %rax
	leal	(%rax,%rax,4), %r9d
	addl	%r9d, %r9d
	subl	%r9d, %edx
	addl	$48, %edx
	movb	%dl, (%r10,%r8)
	cmpl	$6, %ecx
	je	.L37
	movl	16(%rdi), %edx
	movq	(%rdi), %r10
	leal	-7(%rdx), %r8d
	movl	%eax, %edx
	imulq	%rsi, %rdx
	movslq	%r8d, %r8
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %r9d
	addl	%r9d, %r9d
	subl	%r9d, %eax
	addl	$48, %eax
	movb	%al, (%r10,%r8)
	cmpl	$7, %ecx
	je	.L37
	movl	16(%rdi), %eax
	movq	(%rdi), %r10
	leal	-8(%rax), %r8d
	movl	%edx, %eax
	imulq	%rsi, %rax
	movslq	%r8d, %r8
	shrq	$35, %rax
	leal	(%rax,%rax,4), %r9d
	addl	%r9d, %r9d
	subl	%r9d, %edx
	addl	$48, %edx
	movb	%dl, (%r10,%r8)
	cmpl	$8, %ecx
	je	.L37
	movl	16(%rdi), %edx
	movq	(%rdi), %r9
	leal	-9(%rdx), %r8d
	movl	%eax, %edx
	imulq	%rdx, %rsi
	movslq	%r8d, %r8
	shrq	$35, %rsi
	leal	(%rsi,%rsi,4), %edx
	addl	%edx, %edx
	subl	%edx, %eax
	addl	$48, %eax
	movb	%al, (%r9,%r8)
	cmpl	$9, %ecx
	je	.L37
	movl	16(%rdi), %eax
	movq	(%rdi), %rdx
	addl	$48, %esi
	subl	$10, %eax
	cltq
	movb	%sil, (%rdx,%rax)
	ret
.L40:
	leal	1(%rax), %edx
	addl	$48, %esi
	movl	%edx, 16(%rdi)
	movb	%sil, (%r8,%rax)
.L37:
	ret
.L44:
	movl	$2, %ecx
	.p2align 4,,10
	.p2align 3
.L41:
	movl	%esi, %r9d
	movl	$3435973837, %r11d
	addl	%ecx, %eax
	imulq	%r11, %r9
	movl	%eax, 16(%rdi)
	subl	$1, %eax
	movslq	%eax, %rdx
	shrq	$35, %r9
	leal	(%r9,%r9,4), %r10d
	addl	%r10d, %r10d
	subl	%r10d, %esi
	addl	$48, %esi
	movb	%sil, (%r8,%rdx)
	movl	16(%rdi), %eax
	movq	(%rdi), %r8
	leal	-2(%rax), %edx
	movl	%r9d, %eax
	imulq	%r11, %rax
	movslq	%edx, %rdx
	shrq	$35, %rax
	leal	(%rax,%rax,4), %esi
	addl	%esi, %esi
	subl	%esi, %r9d
	addl	$48, %r9d
	movb	%r9b, (%r8,%rdx)
	cmpl	$2, %ecx
	jne	.L43
	ret
.L45:
	movl	$3, %ecx
	jmp	.L41
.L46:
	movl	$4, %ecx
	jmp	.L41
.L47:
	movl	$5, %ecx
	jmp	.L41
.L48:
	movl	$6, %ecx
	jmp	.L41
.L49:
	movl	$7, %ecx
	jmp	.L41
.L50:
	movl	$8, %ecx
	jmp	.L41
.L51:
	movl	$9, %ecx
	jmp	.L41
	.cfi_endproc
.LFE5059:
	.size	_ZN2v88internal19SimpleStringBuilder17AddDecimalIntegerEi, .-_ZN2v88internal19SimpleStringBuilder17AddDecimalIntegerEi
	.section	.text._ZN2v88internal19SimpleStringBuilder8FinalizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SimpleStringBuilder8FinalizeEv
	.type	_ZN2v88internal19SimpleStringBuilder8FinalizeEv, @function
_ZN2v88internal19SimpleStringBuilder8FinalizeEv:
.LFB5060:
	.cfi_startproc
	endbr64
	movslq	16(%rdi), %rax
	movq	(%rdi), %rdx
	cmpl	8(%rdi), %eax
	je	.L81
.L77:
	movb	$0, (%rdx,%rax)
	movq	(%rdi), %rax
	movl	$-1, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	leal	-1(%rax), %ecx
	movl	%ecx, 16(%rdi)
	cmpl	$3, %ecx
	jle	.L80
	subl	$4, %eax
	cltq
	movb	$46, (%rdx,%rax)
	movslq	16(%rdi), %rax
	movq	(%rdi), %rdx
	cmpl	$2, %eax
	jle	.L77
	subl	$2, %eax
	cltq
	movb	$46, (%rdx,%rax)
	movslq	16(%rdi), %rax
	movq	(%rdi), %rdx
	cmpl	$1, %eax
	jle	.L77
	subl	$1, %eax
	cltq
	movb	$46, (%rdx,%rax)
	movslq	16(%rdi), %rax
	movq	(%rdi), %rdx
	movb	$0, (%rdx,%rax)
	movq	(%rdi), %rax
	movl	$-1, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	movslq	%ecx, %rax
	jmp	.L77
	.cfi_endproc
.LFE5060:
	.size	_ZN2v88internal19SimpleStringBuilder8FinalizeEv, .-_ZN2v88internal19SimpleStringBuilder8FinalizeEv
	.section	.rodata._ZN2v88internallsERSoNS0_12FeedbackSlotE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"#"
	.section	.text._ZN2v88internallsERSoNS0_12FeedbackSlotE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoNS0_12FeedbackSlotE
	.type	_ZN2v88internallsERSoNS0_12FeedbackSlotE, @function
_ZN2v88internallsERSoNS0_12FeedbackSlotE:
.LFB5061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	leaq	.LC3(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSolsEi@PLT
	.cfi_endproc
.LFE5061:
	.size	_ZN2v88internallsERSoNS0_12FeedbackSlotE, .-_ZN2v88internallsERSoNS0_12FeedbackSlotE
	.section	.text._ZN2v88internal10hash_valueENS0_9BailoutIdE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal10hash_valueENS0_9BailoutIdE
	.type	_ZN2v88internal10hash_valueENS0_9BailoutIdE, @function
_ZN2v88internal10hash_valueENS0_9BailoutIdE:
.LFB5062:
	.cfi_startproc
	endbr64
	jmp	_ZN2v84base10hash_valueEj@PLT
	.cfi_endproc
.LFE5062:
	.size	_ZN2v88internal10hash_valueENS0_9BailoutIdE, .-_ZN2v88internal10hash_valueENS0_9BailoutIdE
	.section	.text._ZN2v88internallsERSoNS0_9BailoutIdE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoNS0_9BailoutIdE
	.type	_ZN2v88internallsERSoNS0_9BailoutIdE, @function
_ZN2v88internallsERSoNS0_9BailoutIdE:
.LFB5063:
	.cfi_startproc
	endbr64
	jmp	_ZNSolsEi@PLT
	.cfi_endproc
.LFE5063:
	.size	_ZN2v88internallsERSoNS0_9BailoutIdE, .-_ZN2v88internallsERSoNS0_9BailoutIdE
	.section	.text._ZN2v88internal6PrintFEPKcz,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6PrintFEPKcz
	.type	_ZN2v88internal6PrintFEPKcz, @function
_ZN2v88internal6PrintFEPKcz:
.LFB5064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rsi, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L87
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L87:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	leaq	-208(%rbp), %rsi
	movl	$8, -208(%rbp)
	movq	%rax, -200(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$48, -204(%rbp)
	call	_ZN2v84base2OS6VPrintEPKcP13__va_list_tag@PLT
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L90
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L90:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5064:
	.size	_ZN2v88internal6PrintFEPKcz, .-_ZN2v88internal6PrintFEPKcz
	.section	.text._ZN2v88internal6PrintFEP8_IO_FILEPKcz,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6PrintFEP8_IO_FILEPKcz
	.type	_ZN2v88internal6PrintFEP8_IO_FILEPKcz, @function
_ZN2v88internal6PrintFEP8_IO_FILEPKcz:
.LFB5065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rdx, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L92
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L92:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	leaq	-208(%rbp), %rdx
	movl	$16, -208(%rbp)
	movq	%rax, -200(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$48, -204(%rbp)
	call	_ZN2v84base2OS7VFPrintEP8_IO_FILEPKcP13__va_list_tag@PLT
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L95
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L95:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5065:
	.size	_ZN2v88internal6PrintFEP8_IO_FILEPKcz, .-_ZN2v88internal6PrintFEP8_IO_FILEPKcz
	.section	.rodata._ZN2v88internal8PrintPIDEPKcz.str1.1,"aMS",@progbits,1
.LC4:
	.string	"[%d] "
	.section	.text._ZN2v88internal8PrintPIDEPKcz,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8PrintPIDEPKcz
	.type	_ZN2v88internal8PrintPIDEPKcz, @function
_ZN2v88internal8PrintPIDEPKcz:
.LFB5066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$216, %rsp
	movq	%rsi, -184(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L97
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L97:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base2OS19GetCurrentProcessIdEv@PLT
	leaq	.LC4(%rip), %rdi
	movl	%eax, %esi
	xorl	%eax, %eax
	call	_ZN2v84base2OS5PrintEPKcz@PLT
	leaq	16(%rbp), %rax
	leaq	-224(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, -208(%rbp)
	movl	$8, -224(%rbp)
	movl	$48, -220(%rbp)
	call	_ZN2v84base2OS6VPrintEPKcP13__va_list_tag@PLT
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L100
	addq	$216, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L100:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5066:
	.size	_ZN2v88internal8PrintPIDEPKcz, .-_ZN2v88internal8PrintPIDEPKcz
	.section	.rodata._ZN2v88internal12PrintIsolateEPvPKcz.str1.1,"aMS",@progbits,1
.LC5:
	.string	"[%d:%p] "
	.section	.text._ZN2v88internal12PrintIsolateEPvPKcz,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12PrintIsolateEPvPKcz
	.type	_ZN2v88internal12PrintIsolateEPvPKcz, @function
_ZN2v88internal12PrintIsolateEPvPKcz:
.LFB5067:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$208, %rsp
	movq	%rdx, -176(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L102
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L102:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base2OS19GetCurrentProcessIdEv@PLT
	movq	%r13, %rdx
	leaq	.LC5(%rip), %rdi
	movl	%eax, %esi
	xorl	%eax, %eax
	call	_ZN2v84base2OS5PrintEPKcz@PLT
	leaq	16(%rbp), %rax
	leaq	-224(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, -208(%rbp)
	movl	$16, -224(%rbp)
	movl	$48, -220(%rbp)
	call	_ZN2v84base2OS6VPrintEPKcP13__va_list_tag@PLT
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	addq	$208, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L105:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5067:
	.size	_ZN2v88internal12PrintIsolateEPvPKcz, .-_ZN2v88internal12PrintIsolateEPvPKcz
	.section	.text._ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz
	.type	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz, @function
_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz:
.LFB5068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L107
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L107:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	leaq	-208(%rbp), %rcx
	movl	$24, -208(%rbp)
	movq	%rax, -200(%rbp)
	leaq	-176(%rbp), %rax
	movl	$48, -204(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v84base2OS9VSNPrintFEPciPKcP13__va_list_tag@PLT
	movq	-184(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L110
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L110:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5068:
	.size	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz, .-_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz
	.section	.text._ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag
	.type	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag, @function
_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag:
.LFB5069:
	.cfi_startproc
	endbr64
	jmp	_ZN2v84base2OS9VSNPrintFEPciPKcP13__va_list_tag@PLT
	.cfi_endproc
.LFE5069:
	.size	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag, .-_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag
	.section	.text._ZN2v88internal7StrNCpyENS0_6VectorIcEEPKcm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal7StrNCpyENS0_6VectorIcEEPKcm
	.type	_ZN2v88internal7StrNCpyENS0_6VectorIcEEPKcm, @function
_ZN2v88internal7StrNCpyENS0_6VectorIcEEPKcm:
.LFB5071:
	.cfi_startproc
	endbr64
	jmp	_ZN2v84base2OS7StrNCpyEPciPKcm@PLT
	.cfi_endproc
.LFE5071:
	.size	_ZN2v88internal7StrNCpyENS0_6VectorIcEEPKcm, .-_ZN2v88internal7StrNCpyENS0_6VectorIcEEPKcm
	.section	.text._ZN2v88internal5FlushEP8_IO_FILE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal5FlushEP8_IO_FILE
	.type	_ZN2v88internal5FlushEP8_IO_FILE, @function
_ZN2v88internal5FlushEP8_IO_FILE:
.LFB5072:
	.cfi_startproc
	endbr64
	jmp	fflush@PLT
	.cfi_endproc
.LFE5072:
	.size	_ZN2v88internal5FlushEP8_IO_FILE, .-_ZN2v88internal5FlushEP8_IO_FILE
	.section	.text._ZN2v88internal8ReadLineEPKc,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8ReadLineEPKc
	.type	_ZN2v88internal8ReadLineEPKc, @function
_ZN2v88internal8ReadLineEPKc:
.LFB5073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r15
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	stdout(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	fputs@PLT
	movq	stdout(%rip), %rdi
	call	fflush@PLT
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L156:
	cmpb	$92, -322(%rbp,%rbx)
	movzbl	-321(%rbp,%rbx), %edx
	leaq	-1(%rbx), %rax
	je	.L149
.L120:
	cmpb	$10, %dl
	setne	%r13b
.L121:
	testq	%r14, %r14
	je	.L150
.L122:
	leaq	1(%rbx,%r12), %r8
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r8, %rdi
	movq	%r8, -328(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L151
.L124:
	movq	%rcx, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	%rcx, -328(%rbp)
	call	memcpy@PLT
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movq	-328(%rbp), %rcx
	movq	%rcx, %r14
.L123:
	leaq	(%r14,%r12), %rdx
	cmpl	$8, %ebx
	jnb	.L126
	testb	$4, %bl
	jne	.L152
	testl	%ebx, %ebx
	je	.L127
	movzbl	(%r15), %eax
	movb	%al, (%rdx)
	testb	$2, %bl
	jne	.L153
.L127:
	addq	%rbx, %r12
	testb	%r13b, %r13b
	je	.L154
.L130:
	movq	stdin(%rip), %rdx
	movl	$256, %esi
	movq	%r15, %rdi
	call	fgets@PLT
	testq	%rax, %rax
	je	.L155
	movq	%r15, %rbx
.L117:
	movl	(%rbx), %edx
	addq	$4, %rbx
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L117
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rbx), %rdx
	cmove	%rdx, %rbx
	movl	%eax, %esi
	addb	%al, %sil
	sbbq	$3, %rbx
	subq	%r15, %rbx
	cmpq	$1, %rbx
	ja	.L156
	testq	%rbx, %rbx
	jne	.L157
	movl	$1, %r13d
	testq	%r14, %r14
	jne	.L122
.L150:
	leaq	1(%rbx), %r8
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r8, %rdi
	movq	%r8, -328(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L123
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	movq	-328(%rbp), %r8
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r8, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L123
.L125:
	leaq	.LC2(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.p2align 4,,10
	.p2align 3
.L126:
	movq	(%r15), %rax
	leaq	8(%rdx), %rdi
	movq	%r15, %rsi
	addq	%rbx, %r12
	andq	$-8, %rdi
	movq	%rax, (%rdx)
	movl	%ebx, %eax
	movq	-8(%r15,%rax), %rcx
	movq	%rcx, -8(%rdx,%rax)
	subq	%rdi, %rdx
	leal	(%rbx,%rdx), %ecx
	subq	%rdx, %rsi
	shrl	$3, %ecx
	rep movsq
	testb	%r13b, %r13b
	jne	.L130
.L154:
	movb	$0, (%r14,%r12)
.L114:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L158
	addq	$296, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	cmpb	$10, %dl
	jne	.L120
	movb	$10, -322(%rbp,%rbx)
	movl	$1, %r13d
	movb	$0, -321(%rbp,%rbx)
	movq	%rax, %rbx
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L155:
	testq	%r14, %r14
	je	.L114
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	_ZdaPv@PLT
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L152:
	movl	(%r15), %eax
	movl	%eax, (%rdx)
	movl	%ebx, %eax
	movl	-4(%r15,%rax), %ecx
	movl	%ecx, -4(%rdx,%rax)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L153:
	movl	%ebx, %eax
	movzwl	-2(%r15,%rax), %ecx
	movw	%cx, -2(%rdx,%rax)
	jmp	.L127
.L151:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	movq	-328(%rbp), %r8
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r8, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L124
	jmp	.L125
.L157:
	movzbl	-320(%rbp), %edx
	jmp	.L120
.L158:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5073:
	.size	_ZN2v88internal8ReadLineEPKc, .-_ZN2v88internal8ReadLineEPKc
	.section	.rodata._ZN2v88internal8ReadFileB5cxx11EPKcPbb.str1.1,"aMS",@progbits,1
.LC6:
	.string	"rb"
	.section	.text._ZN2v88internal8ReadFileB5cxx11EPKcPbb,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8ReadFileB5cxx11EPKcPbb
	.type	_ZN2v88internal8ReadFileB5cxx11EPKcPbb, @function
_ZN2v88internal8ReadFileB5cxx11EPKcPbb:
.LFB5092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	.LC6(%rip), %rsi
	movq	%rbx, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	leaq	-80(%rbp), %rdi
	movzbl	%r12b, %ecx
	movq	%rbx, %r8
	movq	%rax, %r14
	movq	%r15, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal12_GLOBAL__N_117ReadCharsFromFileEP8_IO_FILEPbbPKc
	testq	%r14, %r14
	je	.L160
	movq	%r14, %rdi
	call	fclose@PLT
.L160:
	movq	-72(%rbp), %rbx
	movq	-80(%rbp), %r12
	leaq	16(%r13), %rax
	movq	%rax, 0(%r13)
	cmpq	%r12, %rbx
	je	.L186
	movq	%rbx, %rdx
	subq	%r12, %rdx
	movq	%rdx, -88(%rbp)
	cmpq	$15, %rdx
	ja	.L187
	leaq	15(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$30, %rdx
	jbe	.L164
.L189:
	leaq	-1(%rbx), %rdx
	subq	%r12, %rdx
	cmpq	$14, %rdx
	jbe	.L164
	movq	%rbx, %rcx
	xorl	%edx, %edx
	subq	%r12, %rcx
	movq	%rcx, %rsi
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L165:
	movdqu	(%r12,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L165
	movq	%rcx, %rdx
	andq	$-16, %rdx
	addq	%rdx, %r12
	addq	%rdx, %rax
	cmpq	%rdx, %rcx
	je	.L168
	movzbl	(%r12), %edx
	movb	%dl, (%rax)
	leaq	1(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L168
	movzbl	1(%r12), %edx
	movb	%dl, 1(%rax)
	leaq	2(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L168
	movzbl	2(%r12), %edx
	movb	%dl, 2(%rax)
	leaq	3(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L168
	movzbl	3(%r12), %edx
	movb	%dl, 3(%rax)
	leaq	4(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L168
	movzbl	4(%r12), %edx
	movb	%dl, 4(%rax)
	leaq	5(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L168
	movzbl	5(%r12), %edx
	movb	%dl, 5(%rax)
	leaq	6(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L168
	movzbl	6(%r12), %edx
	movb	%dl, 6(%rax)
	leaq	7(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L168
	movzbl	7(%r12), %edx
	movb	%dl, 7(%rax)
	leaq	8(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L168
	movzbl	8(%r12), %edx
	movb	%dl, 8(%rax)
	leaq	9(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L168
	movzbl	9(%r12), %edx
	movb	%dl, 9(%rax)
	leaq	10(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L168
	movzbl	10(%r12), %edx
	movb	%dl, 10(%rax)
	leaq	11(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L168
	movzbl	11(%r12), %edx
	movb	%dl, 11(%rax)
	leaq	12(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L168
	movzbl	12(%r12), %edx
	movb	%dl, 12(%rax)
	leaq	13(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L168
	movzbl	13(%r12), %edx
	movb	%dl, 13(%rax)
	leaq	14(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L168
	movzbl	14(%r12), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L168:
	movq	-88(%rbp), %rax
	movq	0(%r13), %rdx
	movq	%rax, 8(%r13)
	movb	$0, (%rdx,%rax)
	movq	-80(%rbp), %r12
.L162:
	testq	%r12, %r12
	je	.L159
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L159:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L188
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movq	$0, 8(%r13)
	movb	$0, 16(%r13)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L187:
	xorl	%edx, %edx
	leaq	-88(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-88(%rbp), %rdx
	movq	%rax, 0(%r13)
	movq	%rdx, 16(%r13)
	leaq	15(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$30, %rdx
	ja	.L189
	.p2align 4,,10
	.p2align 3
.L164:
	subq	%r12, %rbx
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L167:
	movzbl	(%r12,%rcx), %esi
	movb	%sil, (%rax,%rcx)
	addq	$1, %rcx
	cmpq	%rdx, %rcx
	jne	.L167
	jmp	.L168
.L188:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5092:
	.size	_ZN2v88internal8ReadFileB5cxx11EPKcPbb, .-_ZN2v88internal8ReadFileB5cxx11EPKcPbb
	.section	.rodata._ZN2v88internal8ReadFileB5cxx11EP8_IO_FILEPbb.str1.1,"aMS",@progbits,1
.LC7:
	.string	""
	.section	.text._ZN2v88internal8ReadFileB5cxx11EP8_IO_FILEPbb,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8ReadFileB5cxx11EP8_IO_FILEPbb
	.type	_ZN2v88internal8ReadFileB5cxx11EP8_IO_FILEPbb, @function
_ZN2v88internal8ReadFileB5cxx11EP8_IO_FILEPbb:
.LFB5093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%cl, %ecx
	leaq	.LC7(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	leaq	-64(%rbp), %rdi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12_GLOBAL__N_117ReadCharsFromFileEP8_IO_FILEPbbPKc
	movq	-56(%rbp), %rbx
	movq	-64(%rbp), %r12
	leaq	16(%r13), %rax
	movq	%rax, 0(%r13)
	cmpq	%r12, %rbx
	je	.L213
	movq	%rbx, %rdx
	subq	%r12, %rdx
	movq	%rdx, -72(%rbp)
	cmpq	$15, %rdx
	ja	.L214
	leaq	-1(%rbx), %rdx
	subq	%r12, %rdx
	cmpq	$14, %rdx
	jbe	.L194
.L216:
	leaq	15(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$30, %rdx
	jbe	.L194
	movq	%rbx, %rcx
	xorl	%edx, %edx
	subq	%r12, %rcx
	movq	%rcx, %rsi
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L195:
	movdqu	(%r12,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L195
	movq	%rcx, %rdx
	andq	$-16, %rdx
	addq	%rdx, %r12
	addq	%rdx, %rax
	cmpq	%rdx, %rcx
	je	.L198
	movzbl	(%r12), %edx
	movb	%dl, (%rax)
	leaq	1(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L198
	movzbl	1(%r12), %edx
	movb	%dl, 1(%rax)
	leaq	2(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L198
	movzbl	2(%r12), %edx
	movb	%dl, 2(%rax)
	leaq	3(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L198
	movzbl	3(%r12), %edx
	movb	%dl, 3(%rax)
	leaq	4(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L198
	movzbl	4(%r12), %edx
	movb	%dl, 4(%rax)
	leaq	5(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L198
	movzbl	5(%r12), %edx
	movb	%dl, 5(%rax)
	leaq	6(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L198
	movzbl	6(%r12), %edx
	movb	%dl, 6(%rax)
	leaq	7(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L198
	movzbl	7(%r12), %edx
	movb	%dl, 7(%rax)
	leaq	8(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L198
	movzbl	8(%r12), %edx
	movb	%dl, 8(%rax)
	leaq	9(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L198
	movzbl	9(%r12), %edx
	movb	%dl, 9(%rax)
	leaq	10(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L198
	movzbl	10(%r12), %edx
	movb	%dl, 10(%rax)
	leaq	11(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L198
	movzbl	11(%r12), %edx
	movb	%dl, 11(%rax)
	leaq	12(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L198
	movzbl	12(%r12), %edx
	movb	%dl, 12(%rax)
	leaq	13(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L198
	movzbl	13(%r12), %edx
	movb	%dl, 13(%rax)
	leaq	14(%r12), %rdx
	cmpq	%rdx, %rbx
	je	.L198
	movzbl	14(%r12), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L198:
	movq	-72(%rbp), %rax
	movq	0(%r13), %rdx
	movq	%rax, 8(%r13)
	movb	$0, (%rdx,%rax)
	movq	-64(%rbp), %r12
.L192:
	testq	%r12, %r12
	je	.L190
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L190:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L215
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movq	$0, 8(%r13)
	movb	$0, 16(%r13)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L214:
	xorl	%edx, %edx
	leaq	-72(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, 0(%r13)
	movq	%rdx, 16(%r13)
	leaq	-1(%rbx), %rdx
	subq	%r12, %rdx
	cmpq	$14, %rdx
	ja	.L216
	.p2align 4,,10
	.p2align 3
.L194:
	subq	%r12, %rbx
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L197:
	movzbl	(%r12,%rcx), %esi
	movb	%sil, (%rax,%rcx)
	addq	$1, %rcx
	cmpq	%rdx, %rcx
	jne	.L197
	jmp	.L198
.L215:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5093:
	.size	_ZN2v88internal8ReadFileB5cxx11EP8_IO_FILEPbb, .-_ZN2v88internal8ReadFileB5cxx11EP8_IO_FILEPbb
	.section	.text._ZN2v88internal16WriteCharsToFileEPKciP8_IO_FILE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal16WriteCharsToFileEPKciP8_IO_FILE
	.type	_ZN2v88internal16WriteCharsToFileEPKciP8_IO_FILE, @function
_ZN2v88internal16WriteCharsToFileEPKciP8_IO_FILE:
.LFB5094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L220
	movq	%rdi, %r14
	movl	%esi, %ebx
	movq	%rdx, %r12
	xorl	%r13d, %r13d
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L223:
	addl	%eax, %r13d
	cltq
	addq	%rax, %r14
	cmpl	%r13d, %ebx
	jle	.L217
.L219:
	movl	%ebx, %edx
	movq	%r12, %rcx
	movl	$1, %esi
	movq	%r14, %rdi
	subl	%r13d, %edx
	movslq	%edx, %rdx
	call	fwrite@PLT
	testl	%eax, %eax
	jne	.L223
.L217:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L220:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L217
	.cfi_endproc
.LFE5094:
	.size	_ZN2v88internal16WriteCharsToFileEPKciP8_IO_FILE, .-_ZN2v88internal16WriteCharsToFileEPKciP8_IO_FILE
	.section	.rodata._ZN2v88internal11AppendCharsEPKcS2_ib.str1.1,"aMS",@progbits,1
.LC8:
	.string	"ab"
	.section	.rodata._ZN2v88internal11AppendCharsEPKcS2_ib.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"Cannot open file %s for writing.\n"
	.section	.text._ZN2v88internal11AppendCharsEPKcS2_ib,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal11AppendCharsEPKcS2_ib
	.type	_ZN2v88internal11AppendCharsEPKcS2_ib, @function
_ZN2v88internal11AppendCharsEPKcS2_ib:
.LFB5095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	.LC8(%rip), %rsi
	subq	$24, %rsp
	movl	%ecx, -52(%rbp)
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	testq	%rax, %rax
	je	.L233
	movq	%rax, %r14
	xorl	%r13d, %r13d
	testl	%r12d, %r12d
	jg	.L228
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L234:
	addl	%eax, %r13d
	cltq
	addq	%rax, %rbx
	cmpl	%r13d, %r12d
	jle	.L227
.L228:
	movl	%r12d, %edx
	movq	%r14, %rcx
	movl	$1, %esi
	movq	%rbx, %rdi
	subl	%r13d, %edx
	movslq	%edx, %rdx
	call	fwrite@PLT
	testl	%eax, %eax
	jne	.L234
.L227:
	movq	%r14, %rdi
	call	fclose@PLT
.L224:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	xorl	%r13d, %r13d
	cmpb	$0, -52(%rbp)
	je	.L224
	movq	%r15, %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	jmp	.L224
	.cfi_endproc
.LFE5095:
	.size	_ZN2v88internal11AppendCharsEPKcS2_ib, .-_ZN2v88internal11AppendCharsEPKcS2_ib
	.section	.rodata._ZN2v88internal10WriteCharsEPKcS2_ib.str1.1,"aMS",@progbits,1
.LC10:
	.string	"wb"
	.section	.text._ZN2v88internal10WriteCharsEPKcS2_ib,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal10WriteCharsEPKcS2_ib
	.type	_ZN2v88internal10WriteCharsEPKcS2_ib, @function
_ZN2v88internal10WriteCharsEPKcS2_ib:
.LFB5096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	.LC10(%rip), %rsi
	subq	$24, %rsp
	movl	%ecx, -52(%rbp)
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	testq	%rax, %rax
	je	.L244
	movq	%rax, %r14
	xorl	%r13d, %r13d
	testl	%r12d, %r12d
	jg	.L239
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L245:
	addl	%eax, %r13d
	cltq
	addq	%rax, %rbx
	cmpl	%r13d, %r12d
	jle	.L238
.L239:
	movl	%r12d, %edx
	movq	%r14, %rcx
	movl	$1, %esi
	movq	%rbx, %rdi
	subl	%r13d, %edx
	movslq	%edx, %rdx
	call	fwrite@PLT
	testl	%eax, %eax
	jne	.L245
.L238:
	movq	%r14, %rdi
	call	fclose@PLT
.L235:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	xorl	%r13d, %r13d
	cmpb	$0, -52(%rbp)
	je	.L235
	movq	%r15, %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	jmp	.L235
	.cfi_endproc
.LFE5096:
	.size	_ZN2v88internal10WriteCharsEPKcS2_ib, .-_ZN2v88internal10WriteCharsEPKcS2_ib
	.section	.text._ZN2v88internal10WriteBytesEPKcPKhib,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal10WriteBytesEPKcPKhib
	.type	_ZN2v88internal10WriteBytesEPKcPKhib, @function
_ZN2v88internal10WriteBytesEPKcPKhib:
.LFB5097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	.LC10(%rip), %rsi
	subq	$24, %rsp
	movl	%ecx, -52(%rbp)
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	testq	%rax, %rax
	je	.L255
	movq	%rax, %r14
	xorl	%r13d, %r13d
	testl	%r12d, %r12d
	jg	.L250
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L256:
	addl	%eax, %r13d
	cltq
	addq	%rax, %rbx
	cmpl	%r13d, %r12d
	jle	.L249
.L250:
	movl	%r12d, %edx
	movq	%r14, %rcx
	movl	$1, %esi
	movq	%rbx, %rdi
	subl	%r13d, %edx
	movslq	%edx, %rdx
	call	fwrite@PLT
	testl	%eax, %eax
	jne	.L256
.L249:
	movq	%r14, %rdi
	call	fclose@PLT
.L246:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	xorl	%r13d, %r13d
	cmpb	$0, -52(%rbp)
	je	.L246
	movq	%r15, %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	jmp	.L246
	.cfi_endproc
.LFE5097:
	.size	_ZN2v88internal10WriteBytesEPKcPKhib, .-_ZN2v88internal10WriteBytesEPKcPKhib
	.section	.text._ZN2v88internal13StringBuilder12AddFormattedEPKcz,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13StringBuilder12AddFormattedEPKcz
	.type	_ZN2v88internal13StringBuilder12AddFormattedEPKcz, @function
_ZN2v88internal13StringBuilder12AddFormattedEPKcz:
.LFB5098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%rdx, -176(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L258
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L258:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	movslq	16(%rbx), %rdi
	movq	8(%rbx), %rsi
	leaq	16(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	%r10, %rdx
	leaq	-192(%rbp), %rax
	leaq	-224(%rbp), %rcx
	subq	%rdi, %rsi
	addq	(%rbx), %rdi
	movl	$16, -224(%rbp)
	movl	$48, -220(%rbp)
	movq	%rax, -208(%rbp)
	call	_ZN2v84base2OS9VSNPrintFEPciPKcP13__va_list_tag@PLT
	testl	%eax, %eax
	js	.L265
	movq	8(%rbx), %rdx
	movl	16(%rbx), %ecx
	movl	%edx, %esi
	subl	%ecx, %esi
	addl	%eax, %ecx
	cmpl	%esi, %eax
	movl	%ecx, %eax
	cmovge	%edx, %eax
.L262:
	movl	%eax, 16(%rbx)
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L266
	addq	$216, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	movl	8(%rbx), %eax
	jmp	.L262
.L266:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5098:
	.size	_ZN2v88internal13StringBuilder12AddFormattedEPKcz, .-_ZN2v88internal13StringBuilder12AddFormattedEPKcz
	.section	.text._ZN2v88internal13StringBuilder16AddFormattedListEPKcP13__va_list_tag,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13StringBuilder16AddFormattedListEPKcP13__va_list_tag
	.type	_ZN2v88internal13StringBuilder16AddFormattedListEPKcP13__va_list_tag, @function
_ZN2v88internal13StringBuilder16AddFormattedListEPKcP13__va_list_tag:
.LFB5099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rdx, %rcx
	movq	%r8, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	16(%rdi), %rdi
	movq	8(%rbx), %rsi
	subq	%rdi, %rsi
	addq	(%rbx), %rdi
	call	_ZN2v84base2OS9VSNPrintFEPciPKcP13__va_list_tag@PLT
	testl	%eax, %eax
	js	.L273
	movq	8(%rbx), %rdx
	movl	16(%rbx), %ecx
	movl	%edx, %esi
	subl	%ecx, %esi
	addl	%eax, %ecx
	cmpl	%eax, %esi
	movl	%ecx, %eax
	cmovle	%edx, %eax
	movl	%eax, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	movl	8(%rbx), %eax
	movl	%eax, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5099:
	.size	_ZN2v88internal13StringBuilder16AddFormattedListEPKcP13__va_list_tag, .-_ZN2v88internal13StringBuilder16AddFormattedListEPKcP13__va_list_tag
	.section	.text._ZN2v88internal15DoubleToBooleanEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal15DoubleToBooleanEd
	.type	_ZN2v88internal15DoubleToBooleanEd, @function
_ZN2v88internal15DoubleToBooleanEd:
.LFB5100:
	.cfi_startproc
	endbr64
	movq	%xmm0, %rdx
	movq	%xmm0, %rax
	shrq	$48, %rdx
	andw	$32752, %dx
	cmpw	$32752, %dx
	je	.L278
	movl	$1, %r8d
	testw	%dx, %dx
	jne	.L274
	movq	%xmm0, %rdx
	shrq	$32, %rdx
	andl	$1048575, %edx
	orl	%eax, %edx
	setne	%r8b
.L274:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	movq	%xmm0, %rdx
	shrq	$32, %rdx
	andl	$1048575, %edx
	orl	%eax, %edx
	sete	%r8b
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE5100:
	.size	_ZN2v88internal15DoubleToBooleanEd, .-_ZN2v88internal15DoubleToBooleanEd
	.section	.text._ZN2v88internal23GetCurrentStackPositionEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23GetCurrentStackPositionEv
	.type	_ZN2v88internal23GetCurrentStackPositionEv, @function
_ZN2v88internal23GetCurrentStackPositionEv:
.LFB5101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rbp, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5101:
	.size	_ZN2v88internal23GetCurrentStackPositionEv, .-_ZN2v88internal23GetCurrentStackPositionEv
	.section	.text._ZN2v88internal12PassesFilterENS0_6VectorIKcEES3_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12PassesFilterENS0_6VectorIKcEES3_
	.type	_ZN2v88internal12PassesFilterENS0_6VectorIKcEES3_, @function
_ZN2v88internal12PassesFilterENS0_6VectorIKcEES3_:
.LFB5102:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	jne	.L282
	testq	%rsi, %rsi
	sete	%r8b
.L281:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	cmpb	$45, (%rdx)
	je	.L299
	movq	%rdx, %rax
	leaq	(%rdx,%rcx), %r9
	xorl	%r8d, %r8d
	movl	$1, %r11d
	cmpq	%r9, %rax
	je	.L300
.L285:
	movzbl	(%rax), %r10d
	cmpb	$42, %r10b
	je	.L293
	cmpb	$126, %r10b
	je	.L281
	testb	%r11b, %r11b
	leaq	-1(%rcx), %r10
	cmovne	%rcx, %r10
	cmpb	$42, -1(%rdx,%rcx)
	sete	%dl
	movzbl	%dl, %edx
	subq	%rdx, %r10
	cmpq	%rsi, %r10
	ja	.L281
	movq	%rdi, %rdx
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L301:
	addq	$1, %rax
	addq	$1, %rdx
	cmpq	%rax, %r9
	je	.L290
.L289:
	movzbl	(%rax), %ecx
	cmpb	(%rdx), %cl
	je	.L301
	cmpq	%rax, %r9
	je	.L290
	cmpb	$42, %cl
	cmove	%r11d, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	leaq	1(%rdx), %rax
	leaq	(%rdx,%rcx), %r9
	movl	$1, %r8d
	xorl	%r11d, %r11d
	cmpq	%r9, %rax
	jne	.L285
.L300:
	testq	%rsi, %rsi
	setne	%r8b
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	movl	%r11d, %r8d
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L290:
	addq	%rdi, %rsi
	cmpq	%rsi, %rdx
	cmove	%r11d, %r8d
	jmp	.L281
	.cfi_endproc
.LFE5102:
	.size	_ZN2v88internal12PassesFilterENS0_6VectorIKcEES3_, .-_ZN2v88internal12PassesFilterENS0_6VectorIKcEES3_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal19SimpleStringBuilderC2Ei,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal19SimpleStringBuilderC2Ei, @function
_GLOBAL__sub_I__ZN2v88internal19SimpleStringBuilderC2Ei:
.LFB5982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5982:
	.size	_GLOBAL__sub_I__ZN2v88internal19SimpleStringBuilderC2Ei, .-_GLOBAL__sub_I__ZN2v88internal19SimpleStringBuilderC2Ei
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal19SimpleStringBuilderC2Ei
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
