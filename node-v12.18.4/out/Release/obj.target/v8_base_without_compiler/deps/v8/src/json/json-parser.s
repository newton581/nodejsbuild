	.file	"json-parser.cc"
	.text
	.section	.text._ZN2v88internal10JsonParserIhE22UpdatePointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv,"axG",@progbits,_ZN2v88internal10JsonParserIhE22UpdatePointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv,comdat
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE22UpdatePointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv
	.type	_ZN2v88internal10JsonParserIhE22UpdatePointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv, @function
_ZN2v88internal10JsonParserIhE22UpdatePointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv:
.LFB19819:
	.cfi_startproc
	endbr64
	movq	40(%rcx), %rax
	movq	64(%rcx), %rdx
	movq	(%rax), %rax
	addq	$15, %rax
	cmpq	%rdx, %rax
	je	.L1
	movdqu	48(%rcx), %xmm1
	movq	%rdx, %xmm0
	movq	%rax, 64(%rcx)
	punpcklqdq	%xmm0, %xmm0
	psubq	%xmm0, %xmm1
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	paddq	%xmm1, %xmm0
	movups	%xmm0, 48(%rcx)
.L1:
	ret
	.cfi_endproc
.LFE19819:
	.size	_ZN2v88internal10JsonParserIhE22UpdatePointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv, .-_ZN2v88internal10JsonParserIhE22UpdatePointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv
	.section	.text._ZN2v88internal10JsonParserItE22UpdatePointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv,"axG",@progbits,_ZN2v88internal10JsonParserItE22UpdatePointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv,comdat
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE22UpdatePointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv
	.type	_ZN2v88internal10JsonParserItE22UpdatePointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv, @function
_ZN2v88internal10JsonParserItE22UpdatePointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv:
.LFB19879:
	.cfi_startproc
	endbr64
	movq	40(%rcx), %rax
	movq	64(%rcx), %rdx
	movq	(%rax), %rax
	addq	$15, %rax
	cmpq	%rdx, %rax
	je	.L4
	movdqu	48(%rcx), %xmm1
	movq	%rdx, %xmm0
	movq	%rax, 64(%rcx)
	punpcklqdq	%xmm0, %xmm0
	psubq	%xmm0, %xmm1
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	paddq	%xmm1, %xmm0
	movups	%xmm0, 48(%rcx)
.L4:
	ret
	.cfi_endproc
.LFE19879:
	.size	_ZN2v88internal10JsonParserItE22UpdatePointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv, .-_ZN2v88internal10JsonParserItE22UpdatePointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv
	.section	.text._ZN2v88internal12_GLOBAL__N_17MatchesItEEbRKNS0_6VectorIKT_EENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_17MatchesItEEbRKNS0_6VectorIKT_EENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal12_GLOBAL__N_17MatchesItEEbRKNS0_6VectorIKT_EENS0_6HandleINS0_6StringEEE:
.LFB20986:
	.cfi_startproc
	movq	(%rsi), %rdx
	movq	8(%rdi), %rcx
	xorl	%eax, %eax
	cmpl	%ecx, 11(%rdx)
	je	.L26
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	-1(%rdx), %rax
	testb	$8, 11(%rax)
	movq	-1(%rdx), %rsi
	leaq	15(%rdx), %rax
	movzwl	11(%rsi), %esi
	je	.L8
	andl	$7, %esi
	cmpw	$2, %si
	jne	.L9
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	8(%rbx), %rcx
.L9:
	movq	(%rbx), %rdx
	movslq	%ecx, %rcx
	leaq	(%rdx,%rcx,2), %rsi
	cmpq	%rsi, %rdx
	jb	.L11
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L27:
	addq	$2, %rdx
	addq	$1, %rax
	cmpq	%rdx, %rsi
	jbe	.L18
.L11:
	movzbl	(%rax), %ecx
	cmpw	%cx, (%rdx)
	je	.L27
.L19:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	andl	$7, %esi
	cmpw	$2, %si
	jne	.L12
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	8(%rbx), %rcx
.L12:
	movq	(%rbx), %rdx
	movslq	%ecx, %rcx
	leaq	(%rdx,%rcx,2), %rcx
	cmpq	%rcx, %rdx
	jb	.L14
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L28:
	addq	$2, %rdx
	addq	$2, %rax
	cmpq	%rdx, %rcx
	jbe	.L18
.L14:
	movzwl	(%rax), %ebx
	cmpw	%bx, (%rdx)
	je	.L28
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L18:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20986:
	.size	_ZN2v88internal12_GLOBAL__N_17MatchesItEEbRKNS0_6VectorIKT_EENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal12_GLOBAL__N_17MatchesItEEbRKNS0_6VectorIKT_EENS0_6HandleINS0_6StringEEE
	.section	.rodata._ZNSt6vectorIN2v88internal12JsonPropertyESaIS2_EE17_M_default_appendEm.part.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
.LC1:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIN2v88internal12JsonPropertyESaIS2_EE17_M_default_appendEm.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal12JsonPropertyESaIS2_EE17_M_default_appendEm.part.0, @function
_ZNSt6vectorIN2v88internal12JsonPropertyESaIS2_EE17_M_default_appendEm.part.0:
.LFB22544:
	.cfi_startproc
	movabsq	$-6148914691236517205, %r8
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %rcx
	movabsq	$384307168202282325, %r9
	movq	16(%rdi), %rdx
	movq	%r9, %r10
	movq	%rcx, %rax
	subq	(%rdi), %rax
	subq	%rcx, %rdx
	sarq	$3, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	sarq	$3, %rdx
	imulq	%r8, %rax
	imulq	%r8, %rdx
	subq	%rax, %r10
	cmpq	%rsi, %rdx
	jb	.L30
	andb	$-16, 8(%rcx)
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	$0, (%rcx)
	movq	$0, 16(%rcx)
	call	_Z8V8_FatalPKcz@PLT
.L30:
	cmpq	%rsi, %r10
	jb	.L35
	cmpq	%rsi, %rax
	cmovnb	%rax, %rsi
	addq	%rsi, %rax
	cmpq	%r9, %rax
	cmova	%r9, %rax
	imulq	$24, %rax, %rdi
	call	_Znwm@PLT
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L35:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22544:
	.size	_ZNSt6vectorIN2v88internal12JsonPropertyESaIS2_EE17_M_default_appendEm.part.0, .-_ZNSt6vectorIN2v88internal12JsonPropertyESaIS2_EE17_M_default_appendEm.part.0
	.section	.text._ZN2v88internal12_GLOBAL__N_17MatchesIhEEbRKNS0_6VectorIKT_EENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_17MatchesIhEEbRKNS0_6VectorIKT_EENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal12_GLOBAL__N_17MatchesIhEEbRKNS0_6VectorIKT_EENS0_6HandleINS0_6StringEEE:
.LFB20890:
	.cfi_startproc
	movq	(%rsi), %rcx
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	cmpl	%edx, 11(%rcx)
	je	.L51
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	-1(%rcx), %rax
	testb	$8, 11(%rax)
	jne	.L52
	movq	-1(%rcx), %rsi
	leaq	15(%rcx), %rax
	movzwl	11(%rsi), %esi
	andl	$7, %esi
	cmpw	$2, %si
	jne	.L40
	movq	15(%rcx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	8(%rbx), %rdx
.L40:
	movq	(%rbx), %r8
	movslq	%edx, %rcx
	leaq	(%r8,%rcx), %rdx
	cmpq	%rdx, %r8
	jnb	.L44
	xorl	%edx, %edx
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L53:
	addq	$1, %rdx
	cmpq	%rdx, %rcx
	je	.L44
.L42:
	movzbl	(%r8,%rdx), %edi
	movzwl	(%rax,%rdx,2), %esi
	cmpl	%esi, %edi
	je	.L53
	xorl	%eax, %eax
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L52:
	movq	-1(%rcx), %rax
	leaq	15(%rcx), %r8
	movzwl	11(%rax), %esi
	andl	$7, %esi
	cmpw	$2, %si
	jne	.L39
	movq	15(%rcx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	8(%rbx), %rdx
	movq	%rax, %r8
.L39:
	movq	(%rbx), %rdi
	movslq	%edx, %rdx
	movq	%r8, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	sete	%al
.L36:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L36
	.cfi_endproc
.LFE20890:
	.size	_ZN2v88internal12_GLOBAL__N_17MatchesIhEEbRKNS0_6VectorIKT_EENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal12_GLOBAL__N_17MatchesIhEEbRKNS0_6VectorIKT_EENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal10JsonParserIhEC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE,"axG",@progbits,_ZN2v88internal10JsonParserIhEC5EPNS0_7IsolateENS0_6HandleINS0_6StringEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhEC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal10JsonParserIhEC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal10JsonParserIhEC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE:
.LFB19752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	1176(%rsi), %rax
	movq	%rsi, (%rdi)
	movq	15(%rax), %rax
	movq	%rax, 8(%rdi)
	movq	12464(%rsi), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L55
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L56:
	movq	$0, 40(%rbx)
	movq	%rax, %xmm0
	movq	%r15, %xmm1
	movq	(%r15), %rax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rbx)
	movslq	11(%rax), %r13
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L83
.L59:
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	jbe	.L84
.L66:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L85
.L74:
	movq	%r15, 40(%rbx)
	xorl	%r14d, %r14d
.L64:
	movq	(%r15), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	jne	.L80
	movq	40(%rbx), %rax
	movq	(%rax), %rax
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$0, 17(%rbx)
	movq	%rax, 64(%rbx)
.L81:
	addq	%r14, %rax
	movq	%rax, 48(%rbx)
	addq	%r13, %rax
	movq	%rax, 56(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L66
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L69
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L69
	movq	%r15, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L85:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L74
	movq	(%r15), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L77
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L80:
	leaq	37592(%r12), %rdi
	movq	%rbx, %rcx
	movl	$15, %edx
	leaq	_ZN2v88internal10JsonParserIhE22UpdatePointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv(%rip), %rsi
	call	_ZN2v88internal4Heap21AddGCEpilogueCallbackEPFvPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPvES4_S6_@PLT
	movq	40(%rbx), %rax
	movq	(%rax), %rax
	movb	$1, 17(%rbx)
	addq	$15, %rax
	movq	%rax, 64(%rbx)
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L83:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$3, %dx
	jne	.L59
	movq	15(%rax), %rsi
	movslq	27(%rax), %r14
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L60
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L86
.L60:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L62:
	movq	%r15, 40(%rbx)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L55:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L87
.L57:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L69:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L71
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L77:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L88
.L79:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L61:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L89
.L63:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L71:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L90
.L73:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L86:
	movq	15(%rsi), %rsi
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L63
.L90:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L73
	.cfi_endproc
.LFE19752:
	.size	_ZN2v88internal10JsonParserIhEC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal10JsonParserIhEC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.weak	_ZN2v88internal10JsonParserIhEC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.set	_ZN2v88internal10JsonParserIhEC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE,_ZN2v88internal10JsonParserIhEC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal10JsonParserIhED2Ev,"axG",@progbits,_ZN2v88internal10JsonParserIhED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhED2Ev
	.type	_ZN2v88internal10JsonParserIhED2Ev, @function
_ZN2v88internal10JsonParserIhED2Ev:
.LFB19755:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	%rdi, %rdx
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	jne	.L93
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	movq	(%rdi), %rax
	leaq	_ZN2v88internal10JsonParserIhE22UpdatePointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv(%rip), %rsi
	leaq	37592(%rax), %rdi
	jmp	_ZN2v88internal4Heap24RemoveGCEpilogueCallbackEPFvPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPvES6_@PLT
	.cfi_endproc
.LFE19755:
	.size	_ZN2v88internal10JsonParserIhED2Ev, .-_ZN2v88internal10JsonParserIhED2Ev
	.weak	_ZN2v88internal10JsonParserIhED1Ev
	.set	_ZN2v88internal10JsonParserIhED1Ev,_ZN2v88internal10JsonParserIhED2Ev
	.section	.text._ZN2v88internal10JsonParserIhE7advanceEv,"axG",@progbits,_ZN2v88internal10JsonParserIhE7advanceEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE7advanceEv
	.type	_ZN2v88internal10JsonParserIhE7advanceEv, @function
_ZN2v88internal10JsonParserIhE7advanceEv:
.LFB19758:
	.cfi_startproc
	endbr64
	addq	$1, 48(%rdi)
	ret
	.cfi_endproc
.LFE19758:
	.size	_ZN2v88internal10JsonParserIhE7advanceEv, .-_ZN2v88internal10JsonParserIhE7advanceEv
	.section	.text._ZN2v88internal10JsonParserIhE16CurrentCharacterEv,"axG",@progbits,_ZN2v88internal10JsonParserIhE16CurrentCharacterEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE16CurrentCharacterEv
	.type	_ZN2v88internal10JsonParserIhE16CurrentCharacterEv, @function
_ZN2v88internal10JsonParserIhE16CurrentCharacterEv:
.LFB19759:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	cmpq	56(%rdi), %rax
	je	.L97
	movzbl	(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE19759:
	.size	_ZN2v88internal10JsonParserIhE16CurrentCharacterEv, .-_ZN2v88internal10JsonParserIhE16CurrentCharacterEv
	.section	.text._ZN2v88internal10JsonParserIhE13NextCharacterEv,"axG",@progbits,_ZN2v88internal10JsonParserIhE13NextCharacterEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE13NextCharacterEv
	.type	_ZN2v88internal10JsonParserIhE13NextCharacterEv, @function
_ZN2v88internal10JsonParserIhE13NextCharacterEv:
.LFB19760:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdx
	leaq	1(%rdx), %rax
	movq	%rax, 48(%rdi)
	cmpq	%rax, 56(%rdi)
	je	.L100
	movzbl	1(%rdx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE19760:
	.size	_ZN2v88internal10JsonParserIhE13NextCharacterEv, .-_ZN2v88internal10JsonParserIhE13NextCharacterEv
	.section	.text._ZNK2v88internal10JsonParserIhE4peekEv,"axG",@progbits,_ZNK2v88internal10JsonParserIhE4peekEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10JsonParserIhE4peekEv
	.type	_ZNK2v88internal10JsonParserIhE4peekEv, @function
_ZNK2v88internal10JsonParserIhE4peekEv:
.LFB19765:
	.cfi_startproc
	endbr64
	movzbl	16(%rdi), %eax
	ret
	.cfi_endproc
.LFE19765:
	.size	_ZNK2v88internal10JsonParserIhE4peekEv, .-_ZNK2v88internal10JsonParserIhE4peekEv
	.section	.text._ZN2v88internal10JsonParserIhE7ConsumeENS0_9JsonTokenE,"axG",@progbits,_ZN2v88internal10JsonParserIhE7ConsumeENS0_9JsonTokenE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE7ConsumeENS0_9JsonTokenE
	.type	_ZN2v88internal10JsonParserIhE7ConsumeENS0_9JsonTokenE, @function
_ZN2v88internal10JsonParserIhE7ConsumeENS0_9JsonTokenE:
.LFB19766:
	.cfi_startproc
	endbr64
	addq	$1, 48(%rdi)
	ret
	.cfi_endproc
.LFE19766:
	.size	_ZN2v88internal10JsonParserIhE7ConsumeENS0_9JsonTokenE, .-_ZN2v88internal10JsonParserIhE7ConsumeENS0_9JsonTokenE
	.section	.text._ZN2v88internal10JsonParserIhE20ScanUnicodeCharacterEv,"axG",@progbits,_ZN2v88internal10JsonParserIhE20ScanUnicodeCharacterEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE20ScanUnicodeCharacterEv
	.type	_ZN2v88internal10JsonParserIhE20ScanUnicodeCharacterEv, @function
_ZN2v88internal10JsonParserIhE20ScanUnicodeCharacterEv:
.LFB19776:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rcx
	movq	56(%rdi), %rsi
	leaq	1(%rcx), %rax
	movq	%rax, 48(%rdi)
	cmpq	%rax, %rsi
	je	.L120
	movzbl	1(%rcx), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L105
	orl	$32, %eax
	leal	-49(%rax), %edx
	cmpl	$5, %edx
	ja	.L120
	subl	$39, %eax
	js	.L120
.L105:
	leaq	2(%rcx), %rdx
	movq	%rdx, 48(%rdi)
	cmpq	%rdx, %rsi
	je	.L120
	movzbl	2(%rcx), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L106
	orl	$32, %edx
	leal	-49(%rdx), %r8d
	cmpl	$5, %r8d
	ja	.L120
	subl	$39, %edx
	js	.L120
.L106:
	sall	$4, %eax
	addl	%edx, %eax
	leaq	3(%rcx), %rdx
	movq	%rdx, 48(%rdi)
	cmpq	%rdx, %rsi
	je	.L120
	movzbl	3(%rcx), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L107
	orl	$32, %edx
	leal	-49(%rdx), %r8d
	cmpl	$5, %r8d
	ja	.L120
	subl	$39, %edx
	js	.L120
.L107:
	sall	$4, %eax
	addl	%edx, %eax
	leaq	4(%rcx), %rdx
	movq	%rdx, 48(%rdi)
	cmpq	%rsi, %rdx
	je	.L120
	movzbl	4(%rcx), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L108
	orl	$32, %edx
	leal	-49(%rdx), %ecx
	cmpl	$5, %ecx
	ja	.L120
	subl	$39, %edx
	js	.L120
.L108:
	sall	$4, %eax
	addl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE19776:
	.size	_ZN2v88internal10JsonParserIhE20ScanUnicodeCharacterEv, .-_ZN2v88internal10JsonParserIhE20ScanUnicodeCharacterEv
	.section	.text._ZN2v88internal10JsonParserIhE14BuildJsonArrayERKNS2_16JsonContinuationERKSt6vectorINS0_6HandleINS0_6ObjectEEESaIS9_EE,"axG",@progbits,_ZN2v88internal10JsonParserIhE14BuildJsonArrayERKNS2_16JsonContinuationERKSt6vectorINS0_6HandleINS0_6ObjectEEESaIS9_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE14BuildJsonArrayERKNS2_16JsonContinuationERKSt6vectorINS0_6HandleINS0_6ObjectEEESaIS9_EE
	.type	_ZN2v88internal10JsonParserIhE14BuildJsonArrayERKNS2_16JsonContinuationERKSt6vectorINS0_6HandleINS0_6ObjectEEESaIS9_EE, @function
_ZN2v88internal10JsonParserIhE14BuildJsonArrayERKNS2_16JsonContinuationERKSt6vectorINS0_6HandleINS0_6ObjectEEESaIS9_EE:
.LFB19813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movl	24(%rsi), %eax
	movq	8(%rdx), %rcx
	movq	(%rdx), %rsi
	shrl	$2, %eax
	subq	%rsi, %rcx
	movl	%eax, %r13d
	sarq	$3, %rcx
	movl	%ecx, %r14d
	subl	%eax, %r14d
	cmpq	%rcx, %r13
	jnb	.L122
	movq	%r13, %rax
	xorl	%r12d, %r12d
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L123:
	addq	$1, %rax
	cmpq	%rcx, %rax
	jnb	.L161
.L125:
	movq	(%rsi,%rax,8), %rdx
	movq	(%rdx), %rdx
	testb	$1, %dl
	je	.L123
	movq	-1(%rdx), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L124
	addq	$1, %rax
	movl	$4, %r12d
	cmpq	%rcx, %rax
	jb	.L125
.L161:
	movq	(%rdi), %rdi
	movzbl	%r12b, %esi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%r14d, %ecx
	movl	%r14d, %edx
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	movq	(%rax), %rax
	cmpb	$4, %r12b
	je	.L162
	movq	15(%rax), %rdi
	xorl	%ecx, %ecx
	testb	%r12b, %r12b
	jne	.L144
.L135:
	testl	%r14d, %r14d
	jle	.L151
	leal	-1(%r14), %eax
	salq	$3, %r13
	leaq	15(%rdi), %r12
	movq	%rdi, %r9
	leaq	23(%rdi,%rax,8), %r8
	subq	%rdi, %r13
	andq	$-262144, %r9
	.p2align 4,,10
	.p2align 3
.L143:
	movq	(%rbx), %rax
	addq	%r13, %rax
	movq	-15(%r12,%rax), %rax
	movq	(%rax), %r14
	movq	%r14, (%r12)
	testl	%ecx, %ecx
	je	.L137
	movq	%r14, %rax
	notq	%rax
	andl	$1, %eax
	cmpl	$4, %ecx
	je	.L163
	testb	%al, %al
	jne	.L137
.L154:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L137
	testb	$24, 8(%r9)
	jne	.L137
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movl	%ecx, -60(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r8
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L137:
	addq	$8, %r12
	cmpq	%r12, %r8
	jne	.L143
.L151:
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	movq	15(%rax), %rdx
	testl	%r14d, %r14d
	jle	.L151
	leaq	0(,%r13,8), %rax
	leal	-1(%r14), %ecx
	movabsq	$9221120237041090560, %rsi
	leaq	1(%r13,%rcx), %rcx
	subq	%rax, %rdx
	salq	$3, %rcx
	leaq	15(%rdx), %rdi
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L164:
	sarq	$32, %rdx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
.L130:
	ucomisd	%xmm0, %xmm0
	movq	%xmm0, %rdx
	cmovp	%rsi, %rdx
	movq	%rdx, (%rdi,%rax)
	addq	$8, %rax
	cmpq	%rcx, %rax
	je	.L151
.L134:
	movq	(%rbx), %rdx
	movq	(%rdx,%rax), %rdx
	movq	(%rdx), %rdx
	testb	$1, %dl
	je	.L164
	movsd	7(%rdx), %xmm0
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L163:
	testb	%al, %al
	jne	.L137
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L154
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movl	%ecx, -60(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rdi
	movl	-60(%rbp), %ecx
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r9
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L124:
	movq	(%rdi), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%r14d, %ecx
	movl	%r14d, %edx
	movl	$2, %esi
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	15(%rax), %rdi
.L144:
	movq	%rdi, %rax
	movl	$4, %ecx
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testl	$262144, %eax
	jne	.L135
	xorl	%ecx, %ecx
	testb	$24, %al
	sete	%cl
	sall	$2, %ecx
	jmp	.L135
.L122:
	movq	(%rdi), %rdi
	movl	%r14d, %ecx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%r14d, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	xorl	%ecx, %ecx
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	15(%rax), %rdi
	jmp	.L135
	.cfi_endproc
.LFE19813:
	.size	_ZN2v88internal10JsonParserIhE14BuildJsonArrayERKNS2_16JsonContinuationERKSt6vectorINS0_6HandleINS0_6ObjectEEESaIS9_EE, .-_ZN2v88internal10JsonParserIhE14BuildJsonArrayERKNS2_16JsonContinuationERKSt6vectorINS0_6HandleINS0_6ObjectEEESaIS9_EE
	.section	.text._ZN2v88internal10JsonParserIhE21ReportUnexpectedTokenENS0_9JsonTokenE,"axG",@progbits,_ZN2v88internal10JsonParserIhE21ReportUnexpectedTokenENS0_9JsonTokenE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE21ReportUnexpectedTokenENS0_9JsonTokenE
	.type	_ZN2v88internal10JsonParserIhE21ReportUnexpectedTokenENS0_9JsonTokenE, @function
_ZN2v88internal10JsonParserIhE21ReportUnexpectedTokenENS0_9JsonTokenE:
.LFB19815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12480(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L184
.L165:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L185
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movq	32(%rdi), %rax
	movq	%rdi, %rbx
	movl	%esi, %edx
	movq	(%rax), %rax
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	jbe	.L168
.L170:
	xorl	%eax, %eax
.L169:
	movq	48(%rbx), %r13
	subq	64(%rbx), %r13
	subl	%eax, %r13d
	movq	41112(%r12), %rdi
	movq	%r13, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L171
	movl	%edx, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-104(%rbp), %edx
	movq	%rax, %r14
.L172:
	movq	$0, -104(%rbp)
	movl	$267, %r15d
	cmpb	$1, %dl
	je	.L174
	movl	$264, %r15d
	cmpb	$13, %dl
	je	.L174
	movl	$266, %r15d
	testb	%dl, %dl
	je	.L174
	movq	48(%rbx), %rax
	movq	%r12, %rdi
	movq	%r14, -104(%rbp)
	movl	$265, %r15d
	movzbl	(%rax), %esi
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt@PLT
	movq	%rax, %r14
.L174:
	movq	32(%rbx), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewScriptENS0_6HandleINS0_6StringEEENS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal7Isolate32NeedsSourcePositionsForProfilingEv@PLT
	movq	-112(%rbp), %rsi
	testb	%al, %al
	jne	.L186
.L175:
	movq	(%rbx), %rax
	movq	%rsi, -112(%rbp)
	movq	41472(%rax), %rdi
	call	_ZN2v88internal5Debug14OnCompileErrorENS0_6HandleINS0_6ScriptEEE@PLT
	leaq	-96(%rbp), %r10
	movq	-112(%rbp), %rsi
	movl	%r13d, %edx
	movq	%r10, %rdi
	leal	1(%r13), %ecx
	movq	%r10, -112(%rbp)
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii@PLT
	movq	-104(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	-112(%rbp), %r10
	movq	(%rbx), %rdi
	movq	(%rax), %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	56(%rbx), %rax
	movq	%rax, 48(%rbx)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L168:
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpw	$3, %cx
	jne	.L170
	movl	27(%rax), %eax
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L171:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L187
.L173:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L186:
	movq	%rsi, %rdi
	call	_ZN2v88internal6Script12InitLineEndsENS0_6HandleIS1_EE@PLT
	movq	-112(%rbp), %rsi
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L187:
	movq	%r12, %rdi
	movl	%edx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-112(%rbp), %edx
	movq	-104(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L173
.L185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19815:
	.size	_ZN2v88internal10JsonParserIhE21ReportUnexpectedTokenENS0_9JsonTokenE, .-_ZN2v88internal10JsonParserIhE21ReportUnexpectedTokenENS0_9JsonTokenE
	.section	.text._ZN2v88internal10JsonParserIhE6ExpectENS0_9JsonTokenE,"axG",@progbits,_ZN2v88internal10JsonParserIhE6ExpectENS0_9JsonTokenE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE6ExpectENS0_9JsonTokenE
	.type	_ZN2v88internal10JsonParserIhE6ExpectENS0_9JsonTokenE, @function
_ZN2v88internal10JsonParserIhE6ExpectENS0_9JsonTokenE:
.LFB19767:
	.cfi_startproc
	endbr64
	movl	%esi, %r8d
	movzbl	16(%rdi), %esi
	cmpb	%r8b, %sil
	jne	.L189
	addq	$1, 48(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	jmp	_ZN2v88internal10JsonParserIhE21ReportUnexpectedTokenENS0_9JsonTokenE
	.cfi_endproc
.LFE19767:
	.size	_ZN2v88internal10JsonParserIhE6ExpectENS0_9JsonTokenE, .-_ZN2v88internal10JsonParserIhE6ExpectENS0_9JsonTokenE
	.section	.text._ZN2v88internal10JsonParserIhE25ReportUnexpectedCharacterEi,"axG",@progbits,_ZN2v88internal10JsonParserIhE25ReportUnexpectedCharacterEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE25ReportUnexpectedCharacterEi
	.type	_ZN2v88internal10JsonParserIhE25ReportUnexpectedCharacterEi, @function
_ZN2v88internal10JsonParserIhE25ReportUnexpectedCharacterEi:
.LFB19814:
	.cfi_startproc
	endbr64
	movl	$13, %r8d
	cmpl	$-1, %esi
	je	.L193
	movl	$12, %r8d
	cmpl	$255, %esi
	jg	.L193
	movslq	%esi, %rsi
	leaq	_ZN2v88internal12_GLOBAL__N_1L20one_char_json_tokensE(%rip), %rax
	movzbl	(%rax,%rsi), %r8d
.L193:
	movl	%r8d, %esi
	jmp	_ZN2v88internal10JsonParserIhE21ReportUnexpectedTokenENS0_9JsonTokenE
	.cfi_endproc
.LFE19814:
	.size	_ZN2v88internal10JsonParserIhE25ReportUnexpectedCharacterEi, .-_ZN2v88internal10JsonParserIhE25ReportUnexpectedCharacterEi
	.section	.text._ZN2v88internal10JsonParserIhE7isolateEv,"axG",@progbits,_ZN2v88internal10JsonParserIhE7isolateEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE7isolateEv
	.type	_ZN2v88internal10JsonParserIhE7isolateEv, @function
_ZN2v88internal10JsonParserIhE7isolateEv:
.LFB19816:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE19816:
	.size	_ZN2v88internal10JsonParserIhE7isolateEv, .-_ZN2v88internal10JsonParserIhE7isolateEv
	.section	.text._ZN2v88internal10JsonParserIhE7factoryEv,"axG",@progbits,_ZN2v88internal10JsonParserIhE7factoryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE7factoryEv
	.type	_ZN2v88internal10JsonParserIhE7factoryEv, @function
_ZN2v88internal10JsonParserIhE7factoryEv:
.LFB19817:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE19817:
	.size	_ZN2v88internal10JsonParserIhE7factoryEv, .-_ZN2v88internal10JsonParserIhE7factoryEv
	.section	.text._ZN2v88internal10JsonParserIhE18object_constructorEv,"axG",@progbits,_ZN2v88internal10JsonParserIhE18object_constructorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE18object_constructorEv
	.type	_ZN2v88internal10JsonParserIhE18object_constructorEv, @function
_ZN2v88internal10JsonParserIhE18object_constructorEv:
.LFB19818:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE19818:
	.size	_ZN2v88internal10JsonParserIhE18object_constructorEv, .-_ZN2v88internal10JsonParserIhE18object_constructorEv
	.section	.text._ZN2v88internal10JsonParserIhE14UpdatePointersEv,"axG",@progbits,_ZN2v88internal10JsonParserIhE14UpdatePointersEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE14UpdatePointersEv
	.type	_ZN2v88internal10JsonParserIhE14UpdatePointersEv, @function
_ZN2v88internal10JsonParserIhE14UpdatePointersEv:
.LFB19820:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	64(%rdi), %rdx
	movq	(%rax), %rax
	addq	$15, %rax
	cmpq	%rax, %rdx
	je	.L200
	movdqu	48(%rdi), %xmm1
	movq	%rdx, %xmm0
	movq	%rax, 64(%rdi)
	punpcklqdq	%xmm0, %xmm0
	psubq	%xmm0, %xmm1
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	paddq	%xmm1, %xmm0
	movups	%xmm0, 48(%rdi)
.L200:
	ret
	.cfi_endproc
.LFE19820:
	.size	_ZN2v88internal10JsonParserIhE14UpdatePointersEv, .-_ZN2v88internal10JsonParserIhE14UpdatePointersEv
	.section	.text._ZNK2v88internal10JsonParserIhE9is_at_endEv,"axG",@progbits,_ZNK2v88internal10JsonParserIhE9is_at_endEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10JsonParserIhE9is_at_endEv
	.type	_ZNK2v88internal10JsonParserIhE9is_at_endEv, @function
_ZNK2v88internal10JsonParserIhE9is_at_endEv:
.LFB19821:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	cmpq	%rax, 48(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE19821:
	.size	_ZNK2v88internal10JsonParserIhE9is_at_endEv, .-_ZNK2v88internal10JsonParserIhE9is_at_endEv
	.section	.text._ZNK2v88internal10JsonParserIhE8positionEv,"axG",@progbits,_ZNK2v88internal10JsonParserIhE8positionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10JsonParserIhE8positionEv
	.type	_ZNK2v88internal10JsonParserIhE8positionEv, @function
_ZNK2v88internal10JsonParserIhE8positionEv:
.LFB19822:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	subq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE19822:
	.size	_ZNK2v88internal10JsonParserIhE8positionEv, .-_ZNK2v88internal10JsonParserIhE8positionEv
	.section	.text._ZN2v88internal10JsonParserIhE16JsonContinuationC2EPNS0_7IsolateENS3_4TypeEm,"axG",@progbits,_ZN2v88internal10JsonParserIhE16JsonContinuationC5EPNS0_7IsolateENS3_4TypeEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE16JsonContinuationC2EPNS0_7IsolateENS3_4TypeEm
	.type	_ZN2v88internal10JsonParserIhE16JsonContinuationC2EPNS0_7IsolateENS3_4TypeEm, @function
_ZN2v88internal10JsonParserIhE16JsonContinuationC2EPNS0_7IsolateENS3_4TypeEm:
.LFB19824:
	.cfi_startproc
	endbr64
	movq	41088(%rsi), %rax
	salq	$2, %rcx
	andl	$3, %edx
	movq	%rsi, (%rdi)
	movl	%ecx, %ecx
	addl	$1, 41104(%rsi)
	movq	%rax, 8(%rdi)
	movq	41096(%rsi), %rax
	orq	%rdx, %rcx
	movq	%rcx, 24(%rdi)
	movq	%rax, 16(%rdi)
	movl	$0, 32(%rdi)
	ret
	.cfi_endproc
.LFE19824:
	.size	_ZN2v88internal10JsonParserIhE16JsonContinuationC2EPNS0_7IsolateENS3_4TypeEm, .-_ZN2v88internal10JsonParserIhE16JsonContinuationC2EPNS0_7IsolateENS3_4TypeEm
	.weak	_ZN2v88internal10JsonParserIhE16JsonContinuationC1EPNS0_7IsolateENS3_4TypeEm
	.set	_ZN2v88internal10JsonParserIhE16JsonContinuationC1EPNS0_7IsolateENS3_4TypeEm,_ZN2v88internal10JsonParserIhE16JsonContinuationC2EPNS0_7IsolateENS3_4TypeEm
	.section	.text._ZNK2v88internal10JsonParserIhE16JsonContinuation4typeEv,"axG",@progbits,_ZNK2v88internal10JsonParserIhE16JsonContinuation4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10JsonParserIhE16JsonContinuation4typeEv
	.type	_ZNK2v88internal10JsonParserIhE16JsonContinuation4typeEv, @function
_ZNK2v88internal10JsonParserIhE16JsonContinuation4typeEv:
.LFB19826:
	.cfi_startproc
	endbr64
	movzbl	24(%rdi), %eax
	andl	$3, %eax
	ret
	.cfi_endproc
.LFE19826:
	.size	_ZNK2v88internal10JsonParserIhE16JsonContinuation4typeEv, .-_ZNK2v88internal10JsonParserIhE16JsonContinuation4typeEv
	.section	.text._ZN2v88internal10JsonParserIhE16JsonContinuation8set_typeENS3_4TypeE,"axG",@progbits,_ZN2v88internal10JsonParserIhE16JsonContinuation8set_typeENS3_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE16JsonContinuation8set_typeENS3_4TypeE
	.type	_ZN2v88internal10JsonParserIhE16JsonContinuation8set_typeENS3_4TypeE, @function
_ZN2v88internal10JsonParserIhE16JsonContinuation8set_typeENS3_4TypeE:
.LFB19827:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	movzbl	24(%rdi), %esi
	andl	$3, %eax
	andl	$-4, %esi
	orl	%eax, %esi
	movb	%sil, 24(%rdi)
	ret
	.cfi_endproc
.LFE19827:
	.size	_ZN2v88internal10JsonParserIhE16JsonContinuation8set_typeENS3_4TypeE, .-_ZN2v88internal10JsonParserIhE16JsonContinuation8set_typeENS3_4TypeE
	.section	.text._ZN2v88internal10JsonParserItEC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE,"axG",@progbits,_ZN2v88internal10JsonParserItEC5EPNS0_7IsolateENS0_6HandleINS0_6StringEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItEC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal10JsonParserItEC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal10JsonParserItEC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE:
.LFB19830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	1176(%rsi), %rax
	movq	%rsi, (%rdi)
	movq	15(%rax), %rax
	movq	%rax, 8(%rdi)
	movq	12464(%rsi), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L208
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L209:
	movq	$0, 40(%rbx)
	movq	%rax, %xmm0
	movq	%r15, %xmm1
	movq	(%r15), %rax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rbx)
	movslq	11(%rax), %r14
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L236
.L212:
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	jbe	.L237
.L219:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L238
.L227:
	movq	%r15, 40(%rbx)
	xorl	%r13d, %r13d
.L217:
	movq	(%r15), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	jne	.L233
	movq	40(%rbx), %rax
	movq	(%rax), %rax
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$0, 17(%rbx)
	movq	%rax, 64(%rbx)
.L234:
	addq	%rax, %r13
	leaq	0(%r13,%r14,2), %rax
	movq	%r13, 48(%rbx)
	movq	%rax, 56(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L219
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L222
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L222
	movq	%r15, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L238:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L227
	movq	(%r15), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L230
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L233:
	leaq	37592(%r12), %rdi
	movq	%rbx, %rcx
	movl	$15, %edx
	leaq	_ZN2v88internal10JsonParserItE22UpdatePointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv(%rip), %rsi
	call	_ZN2v88internal4Heap21AddGCEpilogueCallbackEPFvPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPvES4_S6_@PLT
	movq	40(%rbx), %rax
	movq	(%rax), %rax
	movb	$1, 17(%rbx)
	addq	$15, %rax
	movq	%rax, 64(%rbx)
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L236:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$3, %dx
	jne	.L212
	movq	15(%rax), %rsi
	movslq	27(%rax), %r13
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L213
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L239
.L213:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L214
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L215:
	movq	%r15, 40(%rbx)
	addq	%r13, %r13
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L208:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L240
.L210:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L222:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L224
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L230:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L241
.L232:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L240:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L214:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L242
.L216:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L224:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L243
.L226:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L239:
	movq	15(%rsi), %rsi
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L241:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L242:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L216
.L243:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L226
	.cfi_endproc
.LFE19830:
	.size	_ZN2v88internal10JsonParserItEC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal10JsonParserItEC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.weak	_ZN2v88internal10JsonParserItEC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.set	_ZN2v88internal10JsonParserItEC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE,_ZN2v88internal10JsonParserItEC2EPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal10JsonParserItED2Ev,"axG",@progbits,_ZN2v88internal10JsonParserItED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItED2Ev
	.type	_ZN2v88internal10JsonParserItED2Ev, @function
_ZN2v88internal10JsonParserItED2Ev:
.LFB19833:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	%rdi, %rdx
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	jne	.L246
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	movq	(%rdi), %rax
	leaq	_ZN2v88internal10JsonParserItE22UpdatePointersCallbackEPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPv(%rip), %rsi
	leaq	37592(%rax), %rdi
	jmp	_ZN2v88internal4Heap24RemoveGCEpilogueCallbackEPFvPNS_7IsolateENS_6GCTypeENS_15GCCallbackFlagsEPvES6_@PLT
	.cfi_endproc
.LFE19833:
	.size	_ZN2v88internal10JsonParserItED2Ev, .-_ZN2v88internal10JsonParserItED2Ev
	.weak	_ZN2v88internal10JsonParserItED1Ev
	.set	_ZN2v88internal10JsonParserItED1Ev,_ZN2v88internal10JsonParserItED2Ev
	.section	.text._ZN2v88internal10JsonParserItE7advanceEv,"axG",@progbits,_ZN2v88internal10JsonParserItE7advanceEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE7advanceEv
	.type	_ZN2v88internal10JsonParserItE7advanceEv, @function
_ZN2v88internal10JsonParserItE7advanceEv:
.LFB19836:
	.cfi_startproc
	endbr64
	addq	$2, 48(%rdi)
	ret
	.cfi_endproc
.LFE19836:
	.size	_ZN2v88internal10JsonParserItE7advanceEv, .-_ZN2v88internal10JsonParserItE7advanceEv
	.section	.text._ZN2v88internal10JsonParserItE16CurrentCharacterEv,"axG",@progbits,_ZN2v88internal10JsonParserItE16CurrentCharacterEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE16CurrentCharacterEv
	.type	_ZN2v88internal10JsonParserItE16CurrentCharacterEv, @function
_ZN2v88internal10JsonParserItE16CurrentCharacterEv:
.LFB19837:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	cmpq	56(%rdi), %rax
	je	.L250
	movzwl	(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE19837:
	.size	_ZN2v88internal10JsonParserItE16CurrentCharacterEv, .-_ZN2v88internal10JsonParserItE16CurrentCharacterEv
	.section	.text._ZN2v88internal10JsonParserItE13NextCharacterEv,"axG",@progbits,_ZN2v88internal10JsonParserItE13NextCharacterEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE13NextCharacterEv
	.type	_ZN2v88internal10JsonParserItE13NextCharacterEv, @function
_ZN2v88internal10JsonParserItE13NextCharacterEv:
.LFB19838:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdx
	leaq	2(%rdx), %rax
	movq	%rax, 48(%rdi)
	cmpq	%rax, 56(%rdi)
	je	.L253
	movzwl	2(%rdx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE19838:
	.size	_ZN2v88internal10JsonParserItE13NextCharacterEv, .-_ZN2v88internal10JsonParserItE13NextCharacterEv
	.section	.text._ZNK2v88internal10JsonParserItE4peekEv,"axG",@progbits,_ZNK2v88internal10JsonParserItE4peekEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10JsonParserItE4peekEv
	.type	_ZNK2v88internal10JsonParserItE4peekEv, @function
_ZNK2v88internal10JsonParserItE4peekEv:
.LFB19843:
	.cfi_startproc
	endbr64
	movzbl	16(%rdi), %eax
	ret
	.cfi_endproc
.LFE19843:
	.size	_ZNK2v88internal10JsonParserItE4peekEv, .-_ZNK2v88internal10JsonParserItE4peekEv
	.section	.text._ZN2v88internal10JsonParserItE7ConsumeENS0_9JsonTokenE,"axG",@progbits,_ZN2v88internal10JsonParserItE7ConsumeENS0_9JsonTokenE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE7ConsumeENS0_9JsonTokenE
	.type	_ZN2v88internal10JsonParserItE7ConsumeENS0_9JsonTokenE, @function
_ZN2v88internal10JsonParserItE7ConsumeENS0_9JsonTokenE:
.LFB19844:
	.cfi_startproc
	endbr64
	addq	$2, 48(%rdi)
	ret
	.cfi_endproc
.LFE19844:
	.size	_ZN2v88internal10JsonParserItE7ConsumeENS0_9JsonTokenE, .-_ZN2v88internal10JsonParserItE7ConsumeENS0_9JsonTokenE
	.section	.text._ZN2v88internal10JsonParserItE20ScanUnicodeCharacterEv,"axG",@progbits,_ZN2v88internal10JsonParserItE20ScanUnicodeCharacterEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE20ScanUnicodeCharacterEv
	.type	_ZN2v88internal10JsonParserItE20ScanUnicodeCharacterEv, @function
_ZN2v88internal10JsonParserItE20ScanUnicodeCharacterEv:
.LFB19854:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rcx
	movq	56(%rdi), %rsi
	leaq	2(%rcx), %rax
	movq	%rax, 48(%rdi)
	cmpq	%rax, %rsi
	je	.L273
	movzwl	2(%rcx), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L258
	orl	$32, %eax
	leal	-49(%rax), %edx
	cmpl	$5, %edx
	ja	.L273
	subl	$39, %eax
	js	.L273
.L258:
	leaq	4(%rcx), %rdx
	movq	%rdx, 48(%rdi)
	cmpq	%rdx, %rsi
	je	.L273
	movzwl	4(%rcx), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L259
	orl	$32, %edx
	leal	-49(%rdx), %r8d
	cmpl	$5, %r8d
	ja	.L273
	subl	$39, %edx
	js	.L273
.L259:
	sall	$4, %eax
	addl	%edx, %eax
	leaq	6(%rcx), %rdx
	movq	%rdx, 48(%rdi)
	cmpq	%rdx, %rsi
	je	.L273
	movzwl	6(%rcx), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L260
	orl	$32, %edx
	leal	-49(%rdx), %r8d
	cmpl	$5, %r8d
	ja	.L273
	subl	$39, %edx
	js	.L273
.L260:
	sall	$4, %eax
	addl	%edx, %eax
	leaq	8(%rcx), %rdx
	movq	%rdx, 48(%rdi)
	cmpq	%rsi, %rdx
	je	.L273
	movzwl	8(%rcx), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L261
	orl	$32, %edx
	leal	-49(%rdx), %ecx
	cmpl	$5, %ecx
	ja	.L273
	subl	$39, %edx
	js	.L273
.L261:
	sall	$4, %eax
	addl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE19854:
	.size	_ZN2v88internal10JsonParserItE20ScanUnicodeCharacterEv, .-_ZN2v88internal10JsonParserItE20ScanUnicodeCharacterEv
	.section	.text._ZN2v88internal10JsonParserItE14BuildJsonArrayERKNS2_16JsonContinuationERKSt6vectorINS0_6HandleINS0_6ObjectEEESaIS9_EE,"axG",@progbits,_ZN2v88internal10JsonParserItE14BuildJsonArrayERKNS2_16JsonContinuationERKSt6vectorINS0_6HandleINS0_6ObjectEEESaIS9_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE14BuildJsonArrayERKNS2_16JsonContinuationERKSt6vectorINS0_6HandleINS0_6ObjectEEESaIS9_EE
	.type	_ZN2v88internal10JsonParserItE14BuildJsonArrayERKNS2_16JsonContinuationERKSt6vectorINS0_6HandleINS0_6ObjectEEESaIS9_EE, @function
_ZN2v88internal10JsonParserItE14BuildJsonArrayERKNS2_16JsonContinuationERKSt6vectorINS0_6HandleINS0_6ObjectEEESaIS9_EE:
.LFB19873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movl	24(%rsi), %eax
	movq	8(%rdx), %rcx
	movq	(%rdx), %rsi
	shrl	$2, %eax
	subq	%rsi, %rcx
	movl	%eax, %r13d
	sarq	$3, %rcx
	movl	%ecx, %r14d
	subl	%eax, %r14d
	cmpq	%rcx, %r13
	jnb	.L275
	movq	%r13, %rax
	xorl	%r12d, %r12d
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L276:
	addq	$1, %rax
	cmpq	%rcx, %rax
	jnb	.L314
.L278:
	movq	(%rsi,%rax,8), %rdx
	movq	(%rdx), %rdx
	testb	$1, %dl
	je	.L276
	movq	-1(%rdx), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L277
	addq	$1, %rax
	movl	$4, %r12d
	cmpq	%rcx, %rax
	jb	.L278
.L314:
	movq	(%rdi), %rdi
	movzbl	%r12b, %esi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%r14d, %ecx
	movl	%r14d, %edx
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	movq	(%rax), %rax
	cmpb	$4, %r12b
	je	.L315
	movq	15(%rax), %rdi
	xorl	%ecx, %ecx
	testb	%r12b, %r12b
	jne	.L297
.L288:
	testl	%r14d, %r14d
	jle	.L304
	leal	-1(%r14), %eax
	salq	$3, %r13
	leaq	15(%rdi), %r12
	movq	%rdi, %r9
	leaq	23(%rdi,%rax,8), %r8
	subq	%rdi, %r13
	andq	$-262144, %r9
	.p2align 4,,10
	.p2align 3
.L296:
	movq	(%rbx), %rax
	addq	%r13, %rax
	movq	-15(%r12,%rax), %rax
	movq	(%rax), %r14
	movq	%r14, (%r12)
	testl	%ecx, %ecx
	je	.L290
	movq	%r14, %rax
	notq	%rax
	andl	$1, %eax
	cmpl	$4, %ecx
	je	.L316
	testb	%al, %al
	jne	.L290
.L307:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L290
	testb	$24, 8(%r9)
	jne	.L290
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movl	%ecx, -60(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r8
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L290:
	addq	$8, %r12
	cmpq	%r12, %r8
	jne	.L296
.L304:
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	movq	15(%rax), %rdx
	testl	%r14d, %r14d
	jle	.L304
	leaq	0(,%r13,8), %rax
	leal	-1(%r14), %ecx
	movabsq	$9221120237041090560, %rsi
	leaq	1(%r13,%rcx), %rcx
	subq	%rax, %rdx
	salq	$3, %rcx
	leaq	15(%rdx), %rdi
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L317:
	sarq	$32, %rdx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
.L283:
	ucomisd	%xmm0, %xmm0
	movq	%xmm0, %rdx
	cmovp	%rsi, %rdx
	movq	%rdx, (%rdi,%rax)
	addq	$8, %rax
	cmpq	%rcx, %rax
	je	.L304
.L287:
	movq	(%rbx), %rdx
	movq	(%rdx,%rax), %rdx
	movq	(%rdx), %rdx
	testb	$1, %dl
	je	.L317
	movsd	7(%rdx), %xmm0
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L316:
	testb	%al, %al
	jne	.L290
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L307
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movl	%ecx, -60(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rdi
	movl	-60(%rbp), %ecx
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r9
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L277:
	movq	(%rdi), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%r14d, %ecx
	movl	%r14d, %edx
	movl	$2, %esi
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	15(%rax), %rdi
.L297:
	movq	%rdi, %rax
	movl	$4, %ecx
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testl	$262144, %eax
	jne	.L288
	xorl	%ecx, %ecx
	testb	$24, %al
	sete	%cl
	sall	$2, %ecx
	jmp	.L288
.L275:
	movq	(%rdi), %rdi
	movl	%r14d, %ecx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%r14d, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	xorl	%ecx, %ecx
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	15(%rax), %rdi
	jmp	.L288
	.cfi_endproc
.LFE19873:
	.size	_ZN2v88internal10JsonParserItE14BuildJsonArrayERKNS2_16JsonContinuationERKSt6vectorINS0_6HandleINS0_6ObjectEEESaIS9_EE, .-_ZN2v88internal10JsonParserItE14BuildJsonArrayERKNS2_16JsonContinuationERKSt6vectorINS0_6HandleINS0_6ObjectEEESaIS9_EE
	.section	.text._ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE,"axG",@progbits,_ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE
	.type	_ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE, @function
_ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE:
.LFB19875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12480(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L337
.L318:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L338
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	movq	32(%rdi), %rax
	movq	%rdi, %rbx
	movl	%esi, %edx
	movq	(%rax), %rax
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	jbe	.L321
.L323:
	xorl	%eax, %eax
.L322:
	movq	48(%rbx), %r13
	subq	64(%rbx), %r13
	sarq	%r13
	movq	41112(%r12), %rdi
	subl	%eax, %r13d
	movq	%r13, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L324
	movl	%edx, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-104(%rbp), %edx
	movq	%rax, %r14
.L325:
	movq	$0, -104(%rbp)
	movl	$267, %r15d
	cmpb	$1, %dl
	je	.L327
	movl	$264, %r15d
	cmpb	$13, %dl
	je	.L327
	movl	$266, %r15d
	testb	%dl, %dl
	je	.L327
	movq	48(%rbx), %rax
	movq	%r12, %rdi
	movq	%r14, -104(%rbp)
	movl	$265, %r15d
	movzwl	(%rax), %esi
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt@PLT
	movq	%rax, %r14
.L327:
	movq	32(%rbx), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewScriptENS0_6HandleINS0_6StringEEENS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal7Isolate32NeedsSourcePositionsForProfilingEv@PLT
	movq	-112(%rbp), %rsi
	testb	%al, %al
	jne	.L339
.L328:
	movq	(%rbx), %rax
	movq	%rsi, -112(%rbp)
	movq	41472(%rax), %rdi
	call	_ZN2v88internal5Debug14OnCompileErrorENS0_6HandleINS0_6ScriptEEE@PLT
	leaq	-96(%rbp), %r10
	movq	-112(%rbp), %rsi
	movl	%r13d, %edx
	movq	%r10, %rdi
	leal	1(%r13), %ecx
	movq	%r10, -112(%rbp)
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii@PLT
	movq	-104(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	-112(%rbp), %r10
	movq	(%rbx), %rdi
	movq	(%rax), %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	56(%rbx), %rax
	movq	%rax, 48(%rbx)
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L321:
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpw	$3, %cx
	jne	.L323
	movl	27(%rax), %eax
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L324:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L340
.L326:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L339:
	movq	%rsi, %rdi
	call	_ZN2v88internal6Script12InitLineEndsENS0_6HandleIS1_EE@PLT
	movq	-112(%rbp), %rsi
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L340:
	movq	%r12, %rdi
	movl	%edx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-112(%rbp), %edx
	movq	-104(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L326
.L338:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19875:
	.size	_ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE, .-_ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE
	.section	.text._ZN2v88internal10JsonParserItE6ExpectENS0_9JsonTokenE,"axG",@progbits,_ZN2v88internal10JsonParserItE6ExpectENS0_9JsonTokenE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE6ExpectENS0_9JsonTokenE
	.type	_ZN2v88internal10JsonParserItE6ExpectENS0_9JsonTokenE, @function
_ZN2v88internal10JsonParserItE6ExpectENS0_9JsonTokenE:
.LFB19845:
	.cfi_startproc
	endbr64
	movl	%esi, %r8d
	movzbl	16(%rdi), %esi
	cmpb	%r8b, %sil
	jne	.L342
	addq	$2, 48(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	jmp	_ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE
	.cfi_endproc
.LFE19845:
	.size	_ZN2v88internal10JsonParserItE6ExpectENS0_9JsonTokenE, .-_ZN2v88internal10JsonParserItE6ExpectENS0_9JsonTokenE
	.section	.text._ZN2v88internal10JsonParserItE25ReportUnexpectedCharacterEi,"axG",@progbits,_ZN2v88internal10JsonParserItE25ReportUnexpectedCharacterEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE25ReportUnexpectedCharacterEi
	.type	_ZN2v88internal10JsonParserItE25ReportUnexpectedCharacterEi, @function
_ZN2v88internal10JsonParserItE25ReportUnexpectedCharacterEi:
.LFB19874:
	.cfi_startproc
	endbr64
	movl	$13, %r8d
	cmpl	$-1, %esi
	je	.L346
	movl	$12, %r8d
	cmpl	$255, %esi
	jg	.L346
	movslq	%esi, %rsi
	leaq	_ZN2v88internal12_GLOBAL__N_1L20one_char_json_tokensE(%rip), %rax
	movzbl	(%rax,%rsi), %r8d
.L346:
	movl	%r8d, %esi
	jmp	_ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE
	.cfi_endproc
.LFE19874:
	.size	_ZN2v88internal10JsonParserItE25ReportUnexpectedCharacterEi, .-_ZN2v88internal10JsonParserItE25ReportUnexpectedCharacterEi
	.section	.text._ZN2v88internal10JsonParserItE7isolateEv,"axG",@progbits,_ZN2v88internal10JsonParserItE7isolateEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE7isolateEv
	.type	_ZN2v88internal10JsonParserItE7isolateEv, @function
_ZN2v88internal10JsonParserItE7isolateEv:
.LFB19876:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE19876:
	.size	_ZN2v88internal10JsonParserItE7isolateEv, .-_ZN2v88internal10JsonParserItE7isolateEv
	.section	.text._ZN2v88internal10JsonParserItE7factoryEv,"axG",@progbits,_ZN2v88internal10JsonParserItE7factoryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE7factoryEv
	.type	_ZN2v88internal10JsonParserItE7factoryEv, @function
_ZN2v88internal10JsonParserItE7factoryEv:
.LFB19877:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE19877:
	.size	_ZN2v88internal10JsonParserItE7factoryEv, .-_ZN2v88internal10JsonParserItE7factoryEv
	.section	.text._ZN2v88internal10JsonParserItE18object_constructorEv,"axG",@progbits,_ZN2v88internal10JsonParserItE18object_constructorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE18object_constructorEv
	.type	_ZN2v88internal10JsonParserItE18object_constructorEv, @function
_ZN2v88internal10JsonParserItE18object_constructorEv:
.LFB19878:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE19878:
	.size	_ZN2v88internal10JsonParserItE18object_constructorEv, .-_ZN2v88internal10JsonParserItE18object_constructorEv
	.section	.text._ZN2v88internal10JsonParserItE14UpdatePointersEv,"axG",@progbits,_ZN2v88internal10JsonParserItE14UpdatePointersEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE14UpdatePointersEv
	.type	_ZN2v88internal10JsonParserItE14UpdatePointersEv, @function
_ZN2v88internal10JsonParserItE14UpdatePointersEv:
.LFB19880:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	64(%rdi), %rdx
	movq	(%rax), %rax
	addq	$15, %rax
	cmpq	%rax, %rdx
	je	.L353
	movdqu	48(%rdi), %xmm1
	movq	%rdx, %xmm0
	movq	%rax, 64(%rdi)
	punpcklqdq	%xmm0, %xmm0
	psubq	%xmm0, %xmm1
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	paddq	%xmm1, %xmm0
	movups	%xmm0, 48(%rdi)
.L353:
	ret
	.cfi_endproc
.LFE19880:
	.size	_ZN2v88internal10JsonParserItE14UpdatePointersEv, .-_ZN2v88internal10JsonParserItE14UpdatePointersEv
	.section	.text._ZNK2v88internal10JsonParserItE9is_at_endEv,"axG",@progbits,_ZNK2v88internal10JsonParserItE9is_at_endEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10JsonParserItE9is_at_endEv
	.type	_ZNK2v88internal10JsonParserItE9is_at_endEv, @function
_ZNK2v88internal10JsonParserItE9is_at_endEv:
.LFB19881:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	cmpq	%rax, 48(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE19881:
	.size	_ZNK2v88internal10JsonParserItE9is_at_endEv, .-_ZNK2v88internal10JsonParserItE9is_at_endEv
	.section	.text._ZNK2v88internal10JsonParserItE8positionEv,"axG",@progbits,_ZNK2v88internal10JsonParserItE8positionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10JsonParserItE8positionEv
	.type	_ZNK2v88internal10JsonParserItE8positionEv, @function
_ZNK2v88internal10JsonParserItE8positionEv:
.LFB19882:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	subq	64(%rdi), %rax
	sarq	%rax
	ret
	.cfi_endproc
.LFE19882:
	.size	_ZNK2v88internal10JsonParserItE8positionEv, .-_ZNK2v88internal10JsonParserItE8positionEv
	.section	.text._ZN2v88internal10JsonParserItE16JsonContinuationC2EPNS0_7IsolateENS3_4TypeEm,"axG",@progbits,_ZN2v88internal10JsonParserItE16JsonContinuationC5EPNS0_7IsolateENS3_4TypeEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE16JsonContinuationC2EPNS0_7IsolateENS3_4TypeEm
	.type	_ZN2v88internal10JsonParserItE16JsonContinuationC2EPNS0_7IsolateENS3_4TypeEm, @function
_ZN2v88internal10JsonParserItE16JsonContinuationC2EPNS0_7IsolateENS3_4TypeEm:
.LFB19884:
	.cfi_startproc
	endbr64
	movq	41088(%rsi), %rax
	salq	$2, %rcx
	andl	$3, %edx
	movq	%rsi, (%rdi)
	movl	%ecx, %ecx
	addl	$1, 41104(%rsi)
	movq	%rax, 8(%rdi)
	movq	41096(%rsi), %rax
	orq	%rdx, %rcx
	movq	%rcx, 24(%rdi)
	movq	%rax, 16(%rdi)
	movl	$0, 32(%rdi)
	ret
	.cfi_endproc
.LFE19884:
	.size	_ZN2v88internal10JsonParserItE16JsonContinuationC2EPNS0_7IsolateENS3_4TypeEm, .-_ZN2v88internal10JsonParserItE16JsonContinuationC2EPNS0_7IsolateENS3_4TypeEm
	.weak	_ZN2v88internal10JsonParserItE16JsonContinuationC1EPNS0_7IsolateENS3_4TypeEm
	.set	_ZN2v88internal10JsonParserItE16JsonContinuationC1EPNS0_7IsolateENS3_4TypeEm,_ZN2v88internal10JsonParserItE16JsonContinuationC2EPNS0_7IsolateENS3_4TypeEm
	.section	.text._ZNK2v88internal10JsonParserItE16JsonContinuation4typeEv,"axG",@progbits,_ZNK2v88internal10JsonParserItE16JsonContinuation4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10JsonParserItE16JsonContinuation4typeEv
	.type	_ZNK2v88internal10JsonParserItE16JsonContinuation4typeEv, @function
_ZNK2v88internal10JsonParserItE16JsonContinuation4typeEv:
.LFB19886:
	.cfi_startproc
	endbr64
	movzbl	24(%rdi), %eax
	andl	$3, %eax
	ret
	.cfi_endproc
.LFE19886:
	.size	_ZNK2v88internal10JsonParserItE16JsonContinuation4typeEv, .-_ZNK2v88internal10JsonParserItE16JsonContinuation4typeEv
	.section	.text._ZN2v88internal10JsonParserItE16JsonContinuation8set_typeENS3_4TypeE,"axG",@progbits,_ZN2v88internal10JsonParserItE16JsonContinuation8set_typeENS3_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE16JsonContinuation8set_typeENS3_4TypeE
	.type	_ZN2v88internal10JsonParserItE16JsonContinuation8set_typeENS3_4TypeE, @function
_ZN2v88internal10JsonParserItE16JsonContinuation8set_typeENS3_4TypeE:
.LFB19887:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	movzbl	24(%rdi), %esi
	andl	$3, %eax
	andl	$-4, %esi
	orl	%eax, %esi
	movb	%sil, 24(%rdi)
	ret
	.cfi_endproc
.LFE19887:
	.size	_ZN2v88internal10JsonParserItE16JsonContinuation8set_typeENS3_4TypeE, .-_ZN2v88internal10JsonParserItE16JsonContinuation8set_typeENS3_4TypeE
	.section	.text._ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_,"axG",@progbits,_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_
	.type	_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_, @function
_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_:
.LFB19749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdx
	movq	(%rsi), %r14
	movq	16(%rdi), %rax
	subl	$1, 41104(%rbx)
	movq	%rdx, 41088(%rbx)
	cmpq	41096(%rbx), %rax
	je	.L361
	movq	%rax, 41096(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L361:
	movq	(%r12), %r13
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L362
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L363:
	movq	41088(%rbx), %rdx
	movq	%rdx, 8(%r12)
	movq	41096(%rbx), %rdx
	movq	%rdx, 16(%r12)
	addl	$1, 41104(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L366
.L364:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r14, (%rax)
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L366:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L364
	.cfi_endproc
.LFE19749:
	.size	_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_, .-_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_
	.section	.text._ZN2v88internal21JsonParseInternalizer23InternalizeJsonPropertyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21JsonParseInternalizer23InternalizeJsonPropertyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEE
	.type	_ZN2v88internal21JsonParseInternalizer23InternalizeJsonPropertyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEE, @function
_ZN2v88internal21JsonParseInternalizer23InternalizeJsonPropertyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEE:
.LFB17869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$296, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	41088(%rax), %rdx
	addl	$1, 41104(%rax)
	movq	%rax, -272(%rbp)
	movq	(%rdi), %r14
	movq	%rdx, -264(%rbp)
	movq	41096(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, -256(%rbp)
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L441
.L369:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L376
.L378:
	movl	$-1, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %r15
.L377:
	movq	(%rbx), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L379
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L379:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	(%rbx), %rax
	movq	%r14, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%rbx, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L442
.L380:
	leaq	-240(%rbp), %r14
	movq	%rax, -208(%rbp)
	movq	%r14, %rdi
	movq	$0, -200(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r15, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L375:
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L440
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L443
.L385:
	movq	8(%r12), %rsi
	movq	(%r12), %rdi
	leaq	-80(%rbp), %r8
	movq	%r13, %rdx
	movq	%r14, %xmm1
	movq	%rbx, %xmm0
	movl	$2, %ecx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L440
	leaq	-272(%rbp), %rdi
	call	_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_
	movq	%rax, %r12
.L382:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L412
	movq	-264(%rbp), %rdx
	subl	$1, 41104(%rdi)
	movq	-256(%rbp), %rax
	movq	%rdx, 41088(%rdi)
	cmpq	41096(%rdi), %rax
	je	.L412
	movq	%rax, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L412:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L444
	addq	$296, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L385
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	je	.L391
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jne	.L392
	movq	%r14, %rdi
	call	_ZN2v88internal7JSProxy7IsArrayENS0_6HandleIS1_EE@PLT
	testb	%al, %al
	jne	.L445
	.p2align 4,,10
	.p2align 3
.L440:
	xorl	%r12d, %r12d
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L441:
	movq	%rax, -160(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L370
	testb	$2, %al
	jne	.L369
.L370:
	leaq	-160(%rbp), %r8
	leaq	-276(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -296(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L369
	movq	0(%r13), %rax
	movl	-276(%rbp), %r15d
	movq	-296(%rbp), %r8
	testb	$1, %al
	jne	.L372
.L374:
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r8, -296(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-296(%rbp), %r8
.L373:
	movabsq	$824633720832, %rcx
	movq	%r8, %rdi
	movq	%r14, -136(%rbp)
	leaq	-240(%rbp), %r14
	movl	$3, -160(%rbp)
	movq	%rcx, -148(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%r13, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -96(%rbp)
	movl	%r15d, -88(%rbp)
	movl	$-1, -84(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%rbx, -128(%rbp)
	movdqa	-112(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm6
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm2, -240(%rbp)
	movaps	%xmm3, -224(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm6, -176(%rbp)
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L376:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L378
	movq	%r13, %r15
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L372:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L374
	movq	%r13, %rax
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L442:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L380
.L445:
	shrw	$8, %ax
	jne	.L391
	.p2align 4,,10
	.p2align 3
.L392:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$18, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal14KeyAccumulator7GetKeysENS0_6HandleINS0_10JSReceiverEEENS0_17KeyCollectionModeENS0_14PropertyFilterENS0_17GetKeysConversionEbb@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L440
	movq	(%r15), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	jle	.L385
	movq	$0, -296(%rbp)
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L447:
	movq	%r9, %rsi
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-304(%rbp), %r8
	movq	%rax, %rdx
.L404:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal21JsonParseInternalizer15RecurseAndApplyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEE
	movq	-304(%rbp), %r8
	testb	%al, %al
	je	.L446
	movq	-312(%rbp), %rax
	subl	$1, 41104(%r8)
	movq	%rax, 41088(%r8)
	movq	-320(%rbp), %rax
	cmpq	41096(%r8), %rax
	je	.L408
	movq	%rax, 41096(%r8)
	movq	%r8, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L408:
	movq	(%r15), %rax
	addq	$1, -296(%rbp)
	movq	-296(%rbp), %rcx
	cmpl	%ecx, 11(%rax)
	jle	.L385
.L410:
	movq	(%r12), %r8
	movq	-296(%rbp), %rcx
	movq	41088(%r8), %rax
	addl	$1, 41104(%r8)
	movq	(%r12), %rsi
	movq	%rax, -312(%rbp)
	movq	41096(%r8), %rax
	movq	%rax, -320(%rbp)
	movq	(%r15), %rax
	movq	15(%rax,%rcx,8), %r9
	movq	41112(%rsi), %rdi
	testq	%rdi, %rdi
	jne	.L447
	movq	41088(%rsi), %rdx
	cmpq	41096(%rsi), %rdx
	je	.L448
.L405:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rsi)
	movq	%r9, (%rdx)
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L391:
	movq	(%r12), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal6Object22GetLengthFromArrayLikeEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE@PLT
	testq	%rax, %rax
	je	.L440
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L393
	sarq	$32, %rax
	pxor	%xmm7, %xmm7
	cvtsi2sdl	%eax, %xmm7
	movsd	%xmm7, -320(%rbp)
.L394:
	movsd	.LC3(%rip), %xmm7
	pxor	%xmm0, %xmm0
	movsd	%xmm7, -328(%rbp)
	movsd	-320(%rbp), %xmm7
	comisd	%xmm0, %xmm7
	jbe	.L385
	.p2align 4,,10
	.p2align 3
.L402:
	movq	(%r12), %r15
	xorl	%esi, %esi
	movsd	%xmm0, -296(%rbp)
	addl	$1, 41104(%r15)
	movq	41096(%r15), %rcx
	movq	41088(%r15), %rax
	movq	(%r12), %rdi
	movq	%rcx, -304(%rbp)
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%r12), %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21JsonParseInternalizer15RecurseAndApplyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEE
	movsd	-296(%rbp), %xmm0
	movq	-304(%rbp), %rcx
	testb	%al, %al
	je	.L449
	movq	-312(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %rcx
	je	.L399
	movq	%rcx, 41096(%r15)
	movq	%r15, %rdi
	movsd	%xmm0, -296(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movsd	-296(%rbp), %xmm0
	addsd	-328(%rbp), %xmm0
	movsd	-320(%rbp), %xmm7
	comisd	%xmm0, %xmm7
	ja	.L402
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L399:
	addsd	.LC3(%rip), %xmm0
	movsd	-320(%rbp), %xmm7
	comisd	%xmm0, %xmm7
	ja	.L402
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L448:
	movq	%rsi, %rdi
	movq	%r9, -336(%rbp)
	movq	%r8, -328(%rbp)
	movq	%rsi, -304(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-336(%rbp), %r9
	movq	-328(%rbp), %r8
	movq	-304(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L393:
	movsd	7(%rax), %xmm7
	movsd	%xmm7, -320(%rbp)
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L446:
	movq	-312(%rbp), %rax
	subl	$1, 41104(%r8)
	xorl	%r12d, %r12d
	movq	%rax, 41088(%r8)
	movq	-320(%rbp), %rax
	cmpq	41096(%r8), %rax
	je	.L382
	movq	%rax, 41096(%r8)
	movq	%r8, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L449:
	movq	-312(%rbp), %rax
	subl	$1, 41104(%r15)
	xorl	%r12d, %r12d
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %rcx
	je	.L382
	movq	%rcx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L382
.L444:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17869:
	.size	_ZN2v88internal21JsonParseInternalizer23InternalizeJsonPropertyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEE, .-_ZN2v88internal21JsonParseInternalizer23InternalizeJsonPropertyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEE
	.section	.text._ZN2v88internal21JsonParseInternalizer11InternalizeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21JsonParseInternalizer11InternalizeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_
	.type	_ZN2v88internal21JsonParseInternalizer11InternalizeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_, @function
_ZN2v88internal21JsonParseInternalizer11InternalizeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_:
.LFB17868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdi), %rax
	movq	%rdi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	movq	39(%rax), %rax
	movq	879(%rax), %r13
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L451
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L452:
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	128(%r12), %r15
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rax, %r13
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	leaq	-64(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal21JsonParseInternalizer23InternalizeJsonPropertyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L456
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L451:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L457
.L453:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L457:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L453
.L456:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17868:
	.size	_ZN2v88internal21JsonParseInternalizer11InternalizeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_, .-_ZN2v88internal21JsonParseInternalizer11InternalizeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_
	.section	.text._ZN2v88internal21JsonParseInternalizer15RecurseAndApplyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21JsonParseInternalizer15RecurseAndApplyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEE
	.type	_ZN2v88internal21JsonParseInternalizer15RecurseAndApplyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEE, @function
_ZN2v88internal21JsonParseInternalizer15RecurseAndApplyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEE:
.LFB17870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r14), %rax
	jb	.L466
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal21JsonParseInternalizer23InternalizeJsonPropertyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEE
	testq	%rax, %rax
	je	.L458
	movq	(%rbx), %rdi
	movq	(%rax), %rcx
	cmpq	%rcx, 88(%rdi)
	je	.L467
	leaq	-80(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	orb	$63, -80(%rbp)
	movabsq	$4294967297, %r8
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE@PLT
.L458:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L468
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	xorl	%eax, %eax
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L467:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver23DeletePropertyOrElementENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_12LanguageModeE@PLT
	jmp	.L458
.L468:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17870:
	.size	_ZN2v88internal21JsonParseInternalizer15RecurseAndApplyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEE, .-_ZN2v88internal21JsonParseInternalizer15RecurseAndApplyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6StringEEE
	.section	.rodata._ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE12emplace_backIJRS4_EEEvDpOT_.str1.1,"aMS",@progbits,1
.LC4:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE12emplace_backIJRS4_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE12emplace_backIJRS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE12emplace_backIJRS4_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE12emplace_backIJRS4_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE12emplace_backIJRS4_EEEvDpOT_:
.LFB20962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L470
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	addq	$8, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rcx
	movq	(%rdi), %r14
	movq	%r12, %rdx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L496
	testq	%rax, %rax
	je	.L481
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L497
.L473:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	8(%rax), %rax
.L474:
	movq	(%rsi), %rcx
	movq	%rcx, 0(%r13,%rdx)
	cmpq	%r14, %r12
	je	.L475
	leaq	-8(%r12), %rsi
	leaq	15(%r13), %rax
	subq	%r14, %rsi
	subq	%r14, %rax
	movq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L484
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L484
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L477:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L477
	movq	%rcx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r14,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%rdi, %rcx
	je	.L479
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L479:
	leaq	16(%r13,%rsi), %rax
.L475:
	testq	%r14, %r14
	je	.L480
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L480:
	movq	%r13, %xmm0
	movq	%rax, %xmm2
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L498
	movl	$8, %eax
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L481:
	movl	$8, %r15d
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L484:
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L476:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L476
	jmp	.L479
.L496:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L498:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$3, %r15
	jmp	.L473
	.cfi_endproc
.LFE20962:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE12emplace_backIJRS4_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE12emplace_backIJRS4_EEEvDpOT_
	.section	.text._ZN2v88internal10JsonParserIhE12DecodeStringIhEEvPT_ii,"axG",@progbits,_ZN2v88internal10JsonParserIhE12DecodeStringIhEEvPT_ii,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE12DecodeStringIhEEvPT_ii
	.type	_ZN2v88internal10JsonParserIhE12DecodeStringIhEEvPT_ii, @function
_ZN2v88internal10JsonParserIhE12DecodeStringIhEEvPT_ii:
.LFB21550:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rax
	movslq	%ecx, %r8
	movq	%rsi, %rdx
	leaq	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE(%rip), %r9
	movl	$-1, %r10d
	movl	$-16, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	addq	64(%rdi), %rax
	leaq	.L510(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L525:
	movq	%rdx, %rcx
	movq	%r8, %rbx
	subq	%rsi, %rcx
	subq	%rcx, %rbx
	movq	%rbx, %rcx
	leaq	(%rax,%rbx), %r12
	sarq	$2, %rbx
	testq	%rbx, %rbx
	jle	.L500
	leaq	(%rdx,%rbx,4), %rbx
.L505:
	movzbl	(%rax), %ecx
	cmpb	$92, %cl
	je	.L501
	movb	%cl, (%rdx)
	movzbl	1(%rax), %ecx
	cmpb	$92, %cl
	je	.L555
	movb	%cl, 1(%rdx)
	movzbl	2(%rax), %ecx
	cmpb	$92, %cl
	je	.L556
	movb	%cl, 2(%rdx)
	movzbl	3(%rax), %ecx
	cmpb	$92, %cl
	je	.L557
	addq	$4, %rdx
	addq	$4, %rax
	movb	%cl, -1(%rdx)
	cmpq	%rbx, %rdx
	jne	.L505
	movq	%r12, %rcx
	subq	%rax, %rcx
.L500:
	cmpq	$2, %rcx
	je	.L526
	cmpq	$3, %rcx
	je	.L527
	cmpq	$1, %rcx
	je	.L528
.L499:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L555:
	.cfi_restore_state
	addq	$1, %rdx
	addq	$1, %rax
.L501:
	cmpq	%rax, %r12
	je	.L499
	movzbl	1(%rax), %ecx
	leaq	1(%rax), %r12
	movq	%rcx, %rbx
	movzbl	(%r9,%rcx), %ecx
	andl	$7, %ecx
	movslq	(%rdi,%rcx,4), %rcx
	addq	%rdi, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internal10JsonParserIhE12DecodeStringIhEEvPT_ii,"aG",@progbits,_ZN2v88internal10JsonParserIhE12DecodeStringIhEEvPT_ii,comdat
	.align 4
	.align 4
.L510:
	.long	.L508-.L510
	.long	.L516-.L510
	.long	.L515-.L510
	.long	.L514-.L510
	.long	.L513-.L510
	.long	.L512-.L510
	.long	.L511-.L510
	.long	.L509-.L510
	.section	.text._ZN2v88internal10JsonParserIhE12DecodeStringIhEEvPT_ii,"axG",@progbits,_ZN2v88internal10JsonParserIhE12DecodeStringIhEEvPT_ii,comdat
	.p2align 4,,10
	.p2align 3
.L509:
	movzbl	2(%rax), %ecx
	subl	$48, %ecx
	cmpl	$9, %ecx
	jbe	.L558
	orl	$32, %ecx
	leal	-49(%rcx), %ebx
	subl	$39, %ecx
	sall	$4, %ecx
	cmpl	$6, %ebx
	cmovnb	%r11d, %ecx
	movl	%ecx, %r12d
.L518:
	movzbl	3(%rax), %ebx
	leal	-48(%rbx), %ecx
	cmpl	$9, %ecx
	jbe	.L519
	orl	$32, %ecx
	leal	-49(%rcx), %ebx
	subl	$39, %ecx
	cmpl	$6, %ebx
	cmovnb	%r10d, %ecx
.L519:
	movzbl	4(%rax), %ebx
	addl	%r12d, %ecx
	sall	$4, %ecx
	subl	$48, %ebx
	cmpl	$9, %ebx
	jbe	.L520
	orl	$32, %ebx
	leal	-49(%rbx), %r12d
	subl	$39, %ebx
	cmpl	$6, %r12d
	cmovnb	%r10d, %ebx
.L520:
	leaq	5(%rax), %r12
	movzbl	5(%rax), %eax
	addl	%ebx, %ecx
	sall	$4, %ecx
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L521
	orl	$32, %eax
	leal	-49(%rax), %ebx
	subl	$39, %eax
	cmpl	$6, %ebx
	cmovnb	%r10d, %eax
.L521:
	addl	%ecx, %eax
	cmpl	$65535, %eax
	jg	.L559
	movb	%al, (%rdx)
	addq	$1, %rdx
	.p2align 4,,10
	.p2align 3
.L524:
	leaq	1(%r12), %rax
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L511:
	movb	$13, (%rdx)
	addq	$1, %rdx
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L512:
	movb	$12, (%rdx)
	addq	$1, %rdx
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L513:
	movb	$10, (%rdx)
	addq	$1, %rdx
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L514:
	movb	$9, (%rdx)
	addq	$1, %rdx
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L515:
	movb	$8, (%rdx)
	addq	$1, %rdx
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L516:
	movb	%bl, (%rdx)
	addq	$1, %rdx
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L556:
	addq	$2, %rdx
	addq	$2, %rax
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L557:
	addq	$3, %rdx
	addq	$3, %rax
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L559:
	leal	-65536(%rax), %ecx
	movb	%al, 1(%rdx)
	addq	$2, %rdx
	shrl	$10, %ecx
	movb	%cl, -2(%rdx)
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L558:
	sall	$4, %ecx
	movl	%ecx, %r12d
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L527:
	movzbl	(%rax), %ecx
	cmpb	$92, %cl
	je	.L501
	movb	%cl, (%rdx)
	addq	$1, %rax
	addq	$1, %rdx
.L526:
	movzbl	(%rax), %ecx
	cmpb	$92, %cl
	je	.L501
	movb	%cl, (%rdx)
	addq	$1, %rax
	addq	$1, %rdx
.L528:
	movzbl	(%rax), %ecx
	cmpb	$92, %cl
	je	.L501
	movb	%cl, (%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L508:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21550:
	.size	_ZN2v88internal10JsonParserIhE12DecodeStringIhEEvPT_ii, .-_ZN2v88internal10JsonParserIhE12DecodeStringIhEEvPT_ii
	.section	.text._ZN2v88internal10JsonParserIhE12DecodeStringItEEvPT_ii,"axG",@progbits,_ZN2v88internal10JsonParserIhE12DecodeStringItEEvPT_ii,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE12DecodeStringItEEvPT_ii
	.type	_ZN2v88internal10JsonParserIhE12DecodeStringItEEvPT_ii, @function
_ZN2v88internal10JsonParserIhE12DecodeStringItEEvPT_ii:
.LFB21560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rax
	movslq	%ecx, %r9
	movq	%rsi, %rdx
	leaq	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE(%rip), %r10
	leaq	.L571(%rip), %r8
	movl	$-1, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	$-16, %ebx
	addq	64(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L586:
	movq	%rdx, %rcx
	movq	%r9, %rdi
	subq	%rsi, %rcx
	sarq	%rcx
	subq	%rcx, %rdi
	movq	%rdi, %rcx
	leaq	(%rax,%rdi), %rdi
	movq	%rcx, %r12
	sarq	$2, %r12
	testq	%r12, %r12
	jle	.L561
	leaq	(%rdx,%r12,8), %r12
.L566:
	movzbl	(%rax), %ecx
	cmpb	$92, %cl
	je	.L562
	movw	%cx, (%rdx)
	movzbl	1(%rax), %ecx
	cmpb	$92, %cl
	je	.L616
	movw	%cx, 2(%rdx)
	movzbl	2(%rax), %ecx
	cmpb	$92, %cl
	je	.L617
	movw	%cx, 4(%rdx)
	movzbl	3(%rax), %ecx
	cmpb	$92, %cl
	je	.L618
	addq	$8, %rdx
	addq	$4, %rax
	movw	%cx, -2(%rdx)
	cmpq	%rdx, %r12
	jne	.L566
	movq	%rdi, %rcx
	subq	%rax, %rcx
.L561:
	cmpq	$2, %rcx
	je	.L587
	cmpq	$3, %rcx
	je	.L588
	cmpq	$1, %rcx
	je	.L589
.L560:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L616:
	.cfi_restore_state
	addq	$2, %rdx
	addq	$1, %rax
.L562:
	cmpq	%rax, %rdi
	je	.L560
	movzbl	1(%rax), %ecx
	leaq	1(%rax), %rdi
	movq	%rcx, %r12
	movzbl	(%r10,%rcx), %ecx
	andl	$7, %ecx
	movslq	(%r8,%rcx,4), %rcx
	addq	%r8, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internal10JsonParserIhE12DecodeStringItEEvPT_ii,"aG",@progbits,_ZN2v88internal10JsonParserIhE12DecodeStringItEEvPT_ii,comdat
	.align 4
	.align 4
.L571:
	.long	.L569-.L571
	.long	.L577-.L571
	.long	.L576-.L571
	.long	.L575-.L571
	.long	.L574-.L571
	.long	.L573-.L571
	.long	.L572-.L571
	.long	.L570-.L571
	.section	.text._ZN2v88internal10JsonParserIhE12DecodeStringItEEvPT_ii,"axG",@progbits,_ZN2v88internal10JsonParserIhE12DecodeStringItEEvPT_ii,comdat
	.p2align 4,,10
	.p2align 3
.L570:
	movzbl	2(%rax), %ecx
	subl	$48, %ecx
	cmpl	$9, %ecx
	jbe	.L619
	orl	$32, %ecx
	leal	-49(%rcx), %edi
	subl	$39, %ecx
	sall	$4, %ecx
	cmpl	$6, %edi
	cmovnb	%ebx, %ecx
	movl	%ecx, %r12d
.L579:
	movzbl	3(%rax), %edi
	leal	-48(%rdi), %ecx
	cmpl	$9, %ecx
	jbe	.L580
	orl	$32, %ecx
	leal	-49(%rcx), %edi
	subl	$39, %ecx
	cmpl	$6, %edi
	cmovnb	%r11d, %ecx
.L580:
	movzbl	4(%rax), %edi
	addl	%r12d, %ecx
	sall	$4, %ecx
	subl	$48, %edi
	cmpl	$9, %edi
	jbe	.L581
	orl	$32, %edi
	leal	-49(%rdi), %r12d
	subl	$39, %edi
	cmpl	$6, %r12d
	cmovnb	%r11d, %edi
.L581:
	addl	%edi, %ecx
	leaq	5(%rax), %rdi
	movzbl	5(%rax), %eax
	sall	$4, %ecx
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L582
	orl	$32, %eax
	leal	-49(%rax), %r12d
	subl	$39, %eax
	cmpl	$6, %r12d
	cmovnb	%r11d, %eax
.L582:
	addl	%ecx, %eax
	cmpl	$65535, %eax
	jg	.L620
	movw	%ax, (%rdx)
	addq	$2, %rdx
	.p2align 4,,10
	.p2align 3
.L585:
	leaq	1(%rdi), %rax
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L572:
	movl	$13, %eax
	addq	$2, %rdx
	movw	%ax, -2(%rdx)
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L573:
	movl	$12, %ecx
	addq	$2, %rdx
	movw	%cx, -2(%rdx)
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L574:
	movl	$10, %r12d
	addq	$2, %rdx
	movw	%r12w, -2(%rdx)
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L575:
	movl	$9, %eax
	addq	$2, %rdx
	movw	%ax, -2(%rdx)
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L576:
	movl	$8, %eax
	addq	$2, %rdx
	movw	%ax, -2(%rdx)
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L577:
	movzbl	%r12b, %eax
	addq	$2, %rdx
	movw	%ax, -2(%rdx)
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L617:
	addq	$4, %rdx
	addq	$2, %rax
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L618:
	addq	$6, %rdx
	addq	$3, %rax
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L620:
	leal	-65536(%rax), %ecx
	andw	$1023, %ax
	addq	$4, %rdx
	shrl	$10, %ecx
	subw	$9216, %ax
	andw	$1023, %cx
	movw	%ax, -2(%rdx)
	subw	$10240, %cx
	movw	%cx, -4(%rdx)
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L619:
	sall	$4, %ecx
	movl	%ecx, %r12d
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L588:
	movzbl	(%rax), %ecx
	cmpb	$92, %cl
	je	.L562
	movw	%cx, (%rdx)
	addq	$1, %rax
	addq	$2, %rdx
.L587:
	movzbl	(%rax), %ecx
	cmpb	$92, %cl
	je	.L562
	movw	%cx, (%rdx)
	addq	$1, %rax
	addq	$2, %rdx
.L589:
	movzbl	(%rax), %ecx
	cmpb	$92, %cl
	je	.L562
	movw	%cx, (%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L569:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21560:
	.size	_ZN2v88internal10JsonParserIhE12DecodeStringItEEvPT_ii, .-_ZN2v88internal10JsonParserIhE12DecodeStringItEEvPT_ii
	.section	.rodata._ZN2v88internal10JsonParserIhE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"(location_) != nullptr"
.LC6:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal10JsonParserIhE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE,"axG",@progbits,_ZN2v88internal10JsonParserIhE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal10JsonParserIhE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal10JsonParserIhE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE:
.LFB19777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movslq	4(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jne	.L622
	movq	(%rdi), %rax
	subq	$-128, %rax
.L623:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L692
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L622:
	.cfi_restore_state
	movzbl	8(%rbx), %r8d
	movq	%rdx, %r13
	testb	$2, %r8b
	jne	.L693
.L624:
	andl	$1, %r8d
	movq	(%r14), %rdi
	jne	.L694
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L646
	movq	(%rax), %rax
	movl	4(%rbx), %ecx
	movl	(%rbx), %r8d
	leaq	15(%rax), %r15
	testb	$4, 8(%rbx)
	jne	.L633
	movq	64(%r14), %rsi
	movslq	%ecx, %rdx
	movslq	%r8d, %rcx
	leaq	(%rsi,%rcx), %r8
	cmpq	$7, %rdx
	ja	.L695
	addq	%r15, %rdx
	cmpq	%rdx, %r15
	jnb	.L657
	leaq	16(%rsi,%rcx), %rcx
	movq	%rax, %rdi
	leaq	16(%rax), %r10
	negq	%rdi
	cmpq	%rcx, %r15
	leaq	31(%rax), %rcx
	setnb	%sil
	cmpq	%rcx, %r8
	setnb	%cl
	orb	%cl, %sil
	je	.L691
	movq	%rdx, %rcx
	subq	%rax, %rcx
	subq	$16, %rcx
	cmpq	$14, %rcx
	seta	%sil
	cmpq	%r10, %rdx
	setnb	%cl
	testb	%cl, %sil
	je	.L691
	movq	%rdx, %rbx
	movq	%r15, %rcx
	subq	%rax, %rbx
	cmpq	%r10, %rdx
	movl	$1, %eax
	leaq	-15(%rbx), %r9
	cmovb	%rax, %r9
	xorl	%eax, %eax
	addq	%r8, %rdi
	leaq	-16(%r9), %rsi
	shrq	$4, %rsi
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L639:
	movdqu	-15(%rcx,%rdi), %xmm3
	addq	$1, %rax
	addq	$16, %rcx
	movups	%xmm3, -16(%rcx)
	cmpq	%rax, %rsi
	ja	.L639
	salq	$4, %rsi
	addq	%rsi, %r15
	addq	%rsi, %r8
	cmpq	%rsi, %r9
	je	.L657
	movzbl	(%r8), %eax
	movb	%al, (%r15)
	leaq	1(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L657
	movzbl	1(%r8), %eax
	movb	%al, 1(%r15)
	leaq	2(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L657
	movzbl	2(%r8), %eax
	movb	%al, 2(%r15)
	leaq	3(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L657
	movzbl	3(%r8), %eax
	movb	%al, 3(%r15)
	leaq	4(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L657
	movzbl	4(%r8), %eax
	movb	%al, 4(%r15)
	leaq	5(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L657
	movzbl	5(%r8), %eax
	movb	%al, 5(%r15)
	leaq	6(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L657
	movzbl	6(%r8), %eax
	movb	%al, 6(%r15)
	leaq	7(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L657
	movzbl	7(%r8), %eax
	movb	%al, 7(%r15)
	leaq	8(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L657
	movzbl	8(%r8), %eax
	movb	%al, 8(%r15)
	leaq	9(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L657
	movzbl	9(%r8), %eax
	movb	%al, 9(%r15)
	leaq	10(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L657
	movzbl	10(%r8), %eax
	movb	%al, 10(%r15)
	leaq	11(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L657
	movzbl	11(%r8), %eax
	movb	%al, 11(%r15)
	leaq	12(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L657
	movzbl	12(%r8), %eax
	movb	%al, 12(%r15)
	leaq	13(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L657
	movzbl	13(%r8), %eax
	movb	%al, 13(%r15)
	leaq	14(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L657
	movzbl	14(%r8), %eax
	movb	%al, 14(%r15)
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L693:
	testb	$4, %r8b
	jne	.L624
	movslq	(%rbx), %rdx
	testq	%r13, %r13
	je	.L626
	addq	64(%rdi), %rdx
	movq	%rsi, -72(%rbp)
	leaq	-80(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_17MatchesIhEEbRKNS0_6VectorIKT_EENS0_6HandleINS0_6StringEEE
	testb	%al, %al
	je	.L629
	movq	%r13, %rax
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L695:
	movq	%r8, %rsi
	movq	%r15, %rdi
	call	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L657:
	movq	%r12, %rax
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L633:
	movl	%r8d, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE12DecodeStringIhEEvPT_ii
	testb	$2, 8(%rbx)
	je	.L657
	movslq	4(%rbx), %rax
	movq	%r15, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rax, %rcx
	testq	%r13, %r13
	je	.L644
	leaq	-80(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_17MatchesIhEEbRKNS0_6VectorIKT_EENS0_6HandleINS0_6StringEEE
	testb	%al, %al
	jne	.L660
	movl	4(%rbx), %ecx
.L644:
	movq	(%r14), %rdi
	movq	%r12, %rsi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17InternalizeStringINS0_16SeqOneByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib@PLT
	movq	%rax, %r12
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L629:
	movslq	(%rbx), %rdx
	movslq	4(%rbx), %rsi
	movzbl	8(%rbx), %r8d
.L626:
	andl	$1, %r8d
	cmpb	$0, 17(%r14)
	movq	(%r14), %rdi
	je	.L631
	movq	40(%r14), %r9
	movl	%esi, %ecx
	movq	%r9, %rsi
	call	_ZN2v88internal7Factory17InternalizeStringINS0_16SeqOneByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib@PLT
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L631:
	addq	64(%r14), %rdx
	movq	%rsi, -72(%rbp)
	leaq	-80(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movl	%r8d, %edx
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L694:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L646
	movq	(%rax), %rax
	movslq	4(%rbx), %r8
	movslq	(%rbx), %rdx
	leaq	15(%rax), %r15
	testb	$4, 8(%rbx)
	jne	.L647
	movq	64(%r14), %rdi
	leaq	(%r15,%r8,2), %rsi
	leaq	(%rdi,%rdx), %rcx
	cmpq	%rsi, %r15
	jnb	.L657
	movq	%rsi, %rbx
	leaq	16(%rax), %r9
	movl	$1, %r8d
	movl	$2, %r11d
	subq	%rax, %rbx
	leaq	-16(%rbx), %r10
	movq	%r10, %rax
	shrq	%rax
	addq	$1, %rax
	cmpq	%rsi, %r9
	cmovbe	%rax, %r8
	addq	%rax, %rax
	cmpq	%rsi, %r9
	cmova	%r11, %rax
	addq	%r15, %rax
	cmpq	%rax, %rcx
	setnb	%r11b
	addq	%r8, %rdx
	addq	%rdi, %rdx
	cmpq	%rdx, %r15
	setnb	%al
	orb	%al, %r11b
	je	.L677
	cmpq	%rsi, %r9
	setbe	%dl
	cmpq	$29, %r10
	seta	%al
	testb	%al, %dl
	je	.L677
	movq	%r8, %rdi
	movq	%rcx, %rdx
	pxor	%xmm1, %xmm1
	movq	%r15, %rax
	andq	$-16, %rdi
	addq	%rcx, %rdi
	.p2align 4,,10
	.p2align 3
.L652:
	movdqu	(%rdx), %xmm0
	addq	$16, %rdx
	addq	$32, %rax
	movdqa	%xmm0, %xmm2
	punpckhbw	%xmm1, %xmm0
	punpcklbw	%xmm1, %xmm2
	movups	%xmm0, -16(%rax)
	movups	%xmm2, -32(%rax)
	cmpq	%rdi, %rdx
	jne	.L652
	movq	%r8, %rdx
	andq	$-16, %rdx
	leaq	(%r15,%rdx,2), %rax
	addq	%rdx, %rcx
	cmpq	%rdx, %r8
	je	.L657
	movzbl	(%rcx), %edx
	movw	%dx, (%rax)
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rsi
	jbe	.L657
	movzbl	1(%rcx), %edx
	movw	%dx, 2(%rax)
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rsi
	jbe	.L657
	movzbl	2(%rcx), %edx
	movw	%dx, 4(%rax)
	leaq	6(%rax), %rdx
	cmpq	%rdx, %rsi
	jbe	.L657
	movzbl	3(%rcx), %edx
	movw	%dx, 6(%rax)
	leaq	8(%rax), %rdx
	cmpq	%rdx, %rsi
	jbe	.L657
	movzbl	4(%rcx), %edx
	movw	%dx, 8(%rax)
	leaq	10(%rax), %rdx
	cmpq	%rdx, %rsi
	jbe	.L657
	movzbl	5(%rcx), %edx
	movw	%dx, 10(%rax)
	leaq	12(%rax), %rdx
	cmpq	%rdx, %rsi
	jbe	.L657
	movzbl	6(%rcx), %edx
	movw	%dx, 12(%rax)
	leaq	14(%rax), %rdx
	cmpq	%rdx, %rsi
	jbe	.L657
	movzbl	7(%rcx), %edx
	movw	%dx, 14(%rax)
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rsi
	jbe	.L657
	movzbl	8(%rcx), %edx
	movw	%dx, 16(%rax)
	leaq	18(%rax), %rdx
	cmpq	%rdx, %rsi
	jbe	.L657
	movzbl	9(%rcx), %edx
	movw	%dx, 18(%rax)
	leaq	20(%rax), %rdx
	cmpq	%rdx, %rsi
	jbe	.L657
	movzbl	10(%rcx), %edx
	movw	%dx, 20(%rax)
	leaq	22(%rax), %rdx
	cmpq	%rdx, %rsi
	jbe	.L657
	movzbl	11(%rcx), %edx
	movw	%dx, 22(%rax)
	leaq	24(%rax), %rdx
	cmpq	%rdx, %rsi
	jbe	.L657
	movzbl	12(%rcx), %edx
	movw	%dx, 24(%rax)
	leaq	26(%rax), %rdx
	cmpq	%rdx, %rsi
	jbe	.L657
	movzbl	13(%rcx), %edx
	movw	%dx, 26(%rax)
	leaq	28(%rax), %rdx
	cmpq	%rdx, %rsi
	jbe	.L657
	movzbl	14(%rcx), %edx
	movw	%dx, 28(%rax)
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L677:
	movzbl	(%rcx), %edx
	movq	%r15, %rax
	addq	$2, %r15
	addq	$1, %rcx
	movw	%dx, (%rax)
	cmpq	%r15, %rsi
	ja	.L677
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L646:
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L691:
	addq	%r8, %rdi
	.p2align 4,,10
	.p2align 3
.L688:
	movq	%r15, %rax
	movzbl	-15(%r15,%rdi), %ecx
	addq	$1, %r15
	movb	%cl, (%rax)
	cmpq	%rdx, %r15
	jb	.L688
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L660:
	movq	%r13, %r12
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L647:
	movl	%r8d, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE12DecodeStringItEEvPT_ii
	testb	$2, 8(%rbx)
	je	.L657
	movslq	4(%rbx), %rax
	movq	%r15, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rax, %rcx
	testq	%r13, %r13
	je	.L656
	leaq	-80(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_17MatchesItEEbRKNS0_6VectorIKT_EENS0_6HandleINS0_6StringEEE
	testb	%al, %al
	jne	.L660
	movl	4(%rbx), %ecx
.L656:
	movq	(%r14), %rdi
	movq	%r12, %rsi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17InternalizeStringINS0_16SeqTwoByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib@PLT
	movq	%rax, %r12
	jmp	.L657
.L692:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19777:
	.size	_ZN2v88internal10JsonParserIhE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal10JsonParserIhE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE
	.section	.rodata._ZN2v88internal10JsonParserIhE15BuildJsonObjectERKNS2_16JsonContinuationERKSt6vectorINS0_12JsonPropertyESaIS7_EENS0_6HandleINS0_3MapEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"(!IsSmi() && (*layout_word_index < length())) || (IsSmi() && (*layout_word_index < 1))"
	.section	.text._ZN2v88internal10JsonParserIhE15BuildJsonObjectERKNS2_16JsonContinuationERKSt6vectorINS0_12JsonPropertyESaIS7_EENS0_6HandleINS0_3MapEEE,"axG",@progbits,_ZN2v88internal10JsonParserIhE15BuildJsonObjectERKNS2_16JsonContinuationERKSt6vectorINS0_12JsonPropertyESaIS7_EENS0_6HandleINS0_3MapEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE15BuildJsonObjectERKNS2_16JsonContinuationERKSt6vectorINS0_12JsonPropertyESaIS7_EENS0_6HandleINS0_3MapEEE
	.type	_ZN2v88internal10JsonParserIhE15BuildJsonObjectERKNS2_16JsonContinuationERKSt6vectorINS0_12JsonPropertyESaIS7_EENS0_6HandleINS0_3MapEEE, @function
_ZN2v88internal10JsonParserIhE15BuildJsonObjectERKNS2_16JsonContinuationERKSt6vectorINS0_12JsonPropertyESaIS7_EENS0_6HandleINS0_3MapEEE:
.LFB19812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -160(%rbp)
	movl	24(%r12), %edx
	movq	%rcx, -184(%rbp)
	movq	(%r15), %r13
	shrl	$2, %edx
	movl	%edx, %edi
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movq	%rdi, -208(%rbp)
	movq	%rax, %rdi
	movq	8(%rax), %rax
	subq	(%rdi), %rax
	sarq	$3, %rax
	imull	$-1431655765, %eax, %eax
	subl	%edx, %eax
	movl	%eax, -176(%rbp)
	movl	%eax, %r14d
	movq	12464(%r13), %rax
	subl	32(%r12), %r14d
	movq	39(%rax), %r8
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L697
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L698:
	movq	%r13, %rdi
	movl	%r14d, %edx
	call	_ZN2v88internal7Factory25ObjectLiteralMapFromCacheENS0_6HandleINS0_13NativeContextEEEi@PLT
	movl	32(%r12), %edi
	movq	%rax, -216(%rbp)
	testl	%edi, %edi
	jne	.L1010
	movq	(%r15), %r9
	movq	%rax, %r14
	leaq	288(%r9), %rax
	movq	%rax, -240(%rbp)
.L707:
	cmpq	$0, -184(%rbp)
	je	.L719
	movq	-184(%rbp), %rax
	movq	(%r14), %rsi
	movq	(%rax), %rcx
	movzbl	14(%rsi), %eax
	movzbl	14(%rcx), %edx
	shrl	$3, %eax
	shrl	$3, %edx
	cmpb	%al, %dl
	je	.L1011
.L719:
	movl	-176(%rbp), %r11d
	testl	%r11d, %r11d
	jle	.L900
	movl	$0, -200(%rbp)
.L718:
	movq	-208(%rbp), %rax
	movq	%r14, %r13
	movq	%r9, %rdi
	movl	$0, -168(%rbp)
	movl	$0, -152(%rbp)
	leaq	(%rax,%rax,2), %r12
	salq	$3, %r12
.L798:
	movq	-160(%rbp), %rax
	movq	(%rax), %r14
	addq	%r12, %r14
	testb	$8, 8(%r14)
	jne	.L721
	movl	-168(%rbp), %eax
	cmpl	%eax, -200(%rbp)
	jle	.L722
	leal	1(%rax), %r9d
	movq	-184(%rbp), %rax
	leal	(%r9,%r9,2), %ebx
	movl	%r9d, %r11d
	movq	(%rax), %rax
	sall	$3, %ebx
	movslq	%ebx, %rbx
	movq	39(%rax), %rax
	movq	-1(%rbx,%rax), %rsi
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	je	.L723
	movq	%r8, %rdi
	movl	%r9d, -224(%rbp)
	movl	%r9d, -192(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-192(%rbp), %r11d
	movl	-224(%rbp), %r9d
	movq	%rax, %rdx
.L724:
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	%r9d, -232(%rbp)
	movl	%r11d, -224(%rbp)
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal10JsonParserIhE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE
	movq	-192(%rbp), %rdx
	movl	-224(%rbp), %r11d
	movl	-232(%rbp), %r9d
	movq	%rax, %r8
	cmpq	%rdx, %rax
	je	.L1012
	testq	%rax, %rax
	je	.L995
	testq	%rdx, %rdx
	je	.L995
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	je	.L1013
.L995:
	movl	-168(%rbp), %r9d
	movq	(%r15), %rbx
	testl	%r9d, %r9d
	jne	.L1014
.L745:
	movq	%r13, -192(%rbp)
	movl	$0, -200(%rbp)
.L868:
	movq	%rbx, %xmm0
	movq	%r13, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	0(%r13), %rax
	movq	$0, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -120(%rbp)
	testb	$1, %al
	je	.L749
	cmpl	$3, %eax
	je	.L749
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L1015
	cmpq	$1, %rdx
	jne	.L817
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L753
	movl	$4, -112(%rbp)
	jmp	.L751
.L1024:
	movq	39(%rsi), %rsi
.L1003:
	movq	41112(%rdx), %rdi
	andq	$-3, %rsi
	testq	%rdi, %rdi
	je	.L740
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r9
.L741:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal10JsonParserIhE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE
	movq	-192(%rbp), %r9
	cmpq	%rbx, %rax
	movq	%rax, %r8
	je	.L1016
	testq	%rax, %rax
	je	.L994
	movq	(%rax), %rax
	cmpq	%rax, (%rbx)
	jne	.L994
	movl	-168(%rbp), %eax
	movq	%r13, -192(%rbp)
	movq	%r9, %r13
	leal	1(%rax), %r11d
	leal	(%r11,%r11,2), %ebx
	sall	$3, %ebx
	movslq	%ebx, %rbx
	.p2align 4,,10
	.p2align 3
.L744:
	movq	0(%r13), %rax
	movq	16(%r14), %rdx
	movq	39(%rax), %rcx
	movq	7(%rbx,%rcx), %rcx
	movq	(%rdx), %rdi
	movzbl	_ZN2v88internal17FLAG_track_fieldsE(%rip), %esi
	movq	%rcx, %r14
	shrq	$38, %rcx
	andl	$7, %ecx
	sarq	$32, %r14
	cmpl	$1, %ecx
	jne	.L757
	testb	%sil, %sil
	jne	.L1017
.L757:
	cmpl	$2, %ecx
	jne	.L898
	cmpb	$0, _ZN2v88internal24FLAG_track_double_fieldsE(%rip)
	jne	.L759
.L898:
	cmpb	$0, _ZN2v88internal29FLAG_track_heap_object_fieldsE(%rip)
	je	.L763
	cmpl	$3, %ecx
	je	.L1018
.L763:
	testb	%sil, %sil
	je	.L767
	testl	%ecx, %ecx
	je	.L1019
.L767:
	cmpl	$3, %ecx
	je	.L773
.L797:
	movl	%r11d, -168(%rbp)
	movq	(%r15), %rdi
.L721:
	addl	$1, -152(%rbp)
	addq	$24, %r12
	movl	-152(%rbp), %eax
	cmpl	%eax, -176(%rbp)
	jne	.L798
	movl	-168(%rbp), %eax
	movq	%r13, %r14
	movq	%rdi, %r9
	cmpl	%eax, -200(%rbp)
	jle	.L884
	testl	%eax, %eax
	jne	.L1020
.L884:
	movq	%r14, -192(%rbp)
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L722:
	movq	0(%r13), %rax
	movq	71(%rax), %rax
	testb	$1, %al
	je	.L729
	cmpl	$3, %eax
	je	.L729
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L1021
	cmpq	$1, %rdx
	je	.L1022
.L817:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1022:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	je	.L729
	movq	-1(%rax), %rax
	.p2align 4,,10
	.p2align 3
.L729:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal10JsonParserIhE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L994
.L874:
	movl	-168(%rbp), %eax
	movq	%r13, -192(%rbp)
	movq	%r8, %r13
	leal	1(%rax), %r11d
	leal	(%r11,%r11,2), %ebx
	sall	$3, %ebx
	movslq	%ebx, %rbx
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L723:
	movq	41088(%rdi), %rdx
	cmpq	%rdx, 41096(%rdi)
	je	.L1023
.L725:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rdi)
	movq	%rsi, (%rdx)
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L1013:
	leal	(%r9,%r9,2), %ebx
	movq	%r13, -192(%rbp)
	movl	%r9d, %r11d
	movq	-184(%rbp), %r13
	sall	$3, %ebx
	movslq	%ebx, %rbx
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L749:
	movl	$1, -112(%rbp)
.L751:
	leaq	-144(%rbp), %rdi
	movl	$1, %edx
	movq	%r8, %rsi
	call	_ZN2v88internal19TransitionsAccessor28FindTransitionToDataPropertyENS0_6HandleINS0_4NameEEENS1_17RequestedLocationE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1005
	movl	-168(%rbp), %eax
	leal	1(%rax), %r11d
	leal	(%r11,%r11,2), %ebx
	sall	$3, %ebx
	movslq	%ebx, %rbx
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L1021:
	andq	$-3, %rax
	movq	39(%rax), %rsi
	movl	15(%rax), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	leal	(%rdx,%rdx,2), %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	7(%rsi,%rdx), %rdx
	movq	%rdx, %rbx
	shrq	$35, %rdx
	shrq	$33, %rbx
	andl	$7, %edx
	movq	%rbx, %r8
	andl	$1, %r8d
	orl	%edx, %r8d
	jne	.L729
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rax,%rsi), %rsi
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L729
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	je	.L734
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L729
.L735:
	movq	0(%r13), %rax
	movq	(%r15), %rdx
	movq	71(%rax), %rsi
	testb	$1, %sil
	je	.L817
	cmpl	$3, %esi
	je	.L817
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	je	.L1003
	cmpq	$1, %rax
	jne	.L817
	movq	-1(%rsi), %rax
	leaq	-1(%rsi), %rdi
	cmpw	$149, 11(%rax)
	je	.L1024
	movq	(%rdi), %rax
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L697:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L1025
.L699:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r8, (%rsi)
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L994:
	movq	%r13, -192(%rbp)
	movq	(%r15), %rbx
	jmp	.L868
.L1020:
	movl	%eax, %edx
	movq	0(%r13), %rax
	leaq	-144(%rbp), %rdi
	movq	%r9, %rsi
	subl	$1, %edx
	movq	%r9, -168(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal3Map14FindFieldOwnerEPNS0_7IsolateEi@PLT
	movq	-168(%rbp), %r9
	movq	%rax, %r12
	movq	41112(%r9), %rdi
	testq	%rdi, %rdi
	je	.L801
	movq	%rax, %rsi
.L1004:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -192(%rbp)
	.p2align 4,,10
	.p2align 3
.L1005:
	movq	(%r15), %r9
.L717:
	movq	-216(%rbp), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L804
	movq	-192(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r9, %rdi
	movl	$2, %edx
	call	_ZN2v88internal7Factory22NewSlowJSObjectFromMapENS0_6HandleINS0_3MapEEEiNS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, -184(%rbp)
.L805:
	movq	(%rax), %r13
	movq	-240(%rbp), %rax
	movq	(%rax), %r12
	leaq	15(%r13), %r14
	movq	%r12, 15(%r13)
	testb	$1, %r12b
	je	.L854
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1026
.L807:
	testb	$24, %al
	je	.L854
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1027
	.p2align 4,,10
	.p2align 3
.L854:
	movq	-184(%rbp), %rax
	movl	$4, %ecx
	movq	(%rax), %rax
	movq	%rax, -168(%rbp)
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testl	$262144, %eax
	jne	.L809
	testb	$24, %al
	sete	%al
	movzbl	%al, %eax
	sall	$2, %eax
	movl	%eax, %ecx
.L809:
	movl	-152(%rbp), %esi
	testl	%esi, %esi
	je	.L844
	movl	-152(%rbp), %eax
	xorl	%r13d, %r13d
	movl	%ecx, %r8d
	movq	-208(%rbp), %rbx
	movq	%r15, -168(%rbp)
	movq	-184(%rbp), %r9
	subl	$1, %eax
	leaq	(%rbx,%rbx,2), %r12
	movq	$8, -200(%rbp)
	movq	-160(%rbp), %r15
	leaq	1(%rbx,%rax), %rax
	salq	$3, %r12
	leaq	(%rax,%rax,2), %r11
	salq	$3, %r11
	.p2align 4,,10
	.p2align 3
.L845:
	movq	(%r15), %rsi
	addq	%r12, %rsi
	testb	$8, 8(%rsi)
	jne	.L843
	movq	-192(%rbp), %rax
	addl	$1, %r13d
	leal	0(%r13,%r13,2), %edx
	movq	(%rax), %rax
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	39(%rax), %rcx
	movq	7(%rdx,%rcx), %rdi
	movq	16(%rsi), %rsi
	movq	(%rsi), %r14
	movq	7(%rdx,%rcx), %rcx
	sarq	$32, %rdi
	movzbl	7(%rax), %edx
	movzbl	8(%rax), %r10d
	movq	%rcx, %rbx
	shrq	$38, %rcx
	shrq	$51, %rbx
	subl	%r10d, %edx
	andl	$7, %ecx
	movq	%rbx, %rsi
	andl	$1023, %esi
	cmpl	%edx, %esi
	setl	%bl
	jl	.L1028
	subl	%edx, %esi
	movl	$16, %r10d
	leal	16(,%rsi,8), %eax
.L814:
	cmpl	$2, %ecx
	jne	.L1029
	movl	$32768, %ecx
.L815:
	movzbl	%bl, %ebx
	movslq	%edx, %rdx
	shrl	$6, %edi
	salq	$17, %rdx
	salq	$14, %rbx
	andl	$7, %edi
	orq	%rdx, %rbx
	movslq	%eax, %rdx
	orq	%rdx, %rbx
	movslq	%r10d, %rdx
	movq	(%r9), %r10
	salq	$27, %rdx
	orq	%rdx, %rbx
	leaq	-1(%r10), %rax
	orq	%rcx, %rbx
	cmpl	$2, %edi
	je	.L1030
.L818:
	andl	$16376, %ebx
	addq	%rax, %rbx
	movq	%r14, (%rbx)
	testl	%r8d, %r8d
	je	.L843
	movq	%r14, %rax
	notq	%rax
	andl	$1, %eax
	cmpl	$4, %r8d
	je	.L1031
	testb	%al, %al
	jne	.L843
.L957:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L843
	movq	%r10, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1032
	.p2align 4,,10
	.p2align 3
.L843:
	addq	$24, %r12
	cmpq	%r12, %r11
	jne	.L845
.L1038:
	movq	-168(%rbp), %r15
.L844:
	movl	-152(%rbp), %esi
	cmpl	%esi, -176(%rbp)
	jle	.L852
	movq	-208(%rbp), %rdi
	movslq	%esi, %rax
	leaq	(%rdi,%rax), %rdx
	leaq	(%rdx,%rdx,2), %rbx
	leaq	1(%rdi,%rax), %rdx
	movl	-176(%rbp), %eax
	salq	$3, %rbx
	subl	$1, %eax
	subl	%esi, %eax
	addq	%rdx, %rax
	leaq	(%rax,%rax,2), %r14
	leaq	0(,%r14,8), %rax
	movq	%rax, -176(%rbp)
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L846:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal10JsonParserIhE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE
	movq	16(%r13), %r13
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	(%rax), %rcx
	movq	%rax, %rsi
	movq	-1(%rcx), %r10
	cmpw	$64, 11(%r10)
	jne	.L848
	movl	11(%rcx), %edx
	notl	%edx
	andl	$1, %edx
.L848:
	movabsq	$824633720832, %rcx
	movl	%edx, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	%rdi, -120(%rbp)
	movq	(%rsi), %rdx
	movq	-1(%rdx), %rdx
	movzwl	11(%rdx), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L1033
.L849:
	movq	%rax, -112(%rbp)
	movq	-184(%rbp), %rax
	leaq	-144(%rbp), %rdi
	movq	%rdi, -168(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	-168(%rbp), %rdi
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS1_20AccessorInfoHandlingE@PLT
	testq	%rax, %rax
	je	.L1034
	movq	-152(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	cmpq	41096(%r14), %r12
	je	.L847
	movq	%r12, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L847:
	addq	$24, %rbx
	cmpq	-176(%rbp), %rbx
	je	.L852
.L853:
	movq	(%r15), %r14
	movq	-160(%rbp), %rcx
	movq	41088(%r14), %rax
	movq	41096(%r14), %r12
	movq	%rax, -152(%rbp)
	movl	41104(%r14), %eax
	leal	1(%rax), %edx
	movl	%edx, 41104(%r14)
	movq	(%rcx), %r13
	addq	%rbx, %r13
	testb	$8, 8(%r13)
	je	.L846
	movl	%eax, 41104(%r14)
	addq	$24, %rbx
	cmpq	-176(%rbp), %rbx
	jne	.L853
.L852:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1035
	movq	-184(%rbp), %rax
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1029:
	.cfi_restore_state
	cmpb	$2, %cl
	jg	.L816
	je	.L817
	xorl	%ecx, %ecx
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L816:
	subl	$3, %ecx
	cmpb	$1, %cl
	ja	.L817
	xorl	%ecx, %ecx
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L1030:
	movq	-1(%r10), %rax
	testb	$64, %bh
	je	.L1006
	movq	47(%rax), %rax
	testq	%rax, %rax
	je	.L1006
	movq	%rax, %rdx
	movl	$32, %edi
	notq	%rdx
	movl	%edx, %esi
	andl	$1, %esi
	je	.L1036
.L822:
	movq	%rbx, %r10
	movl	%ebx, %ecx
	shrq	$30, %r10
	sarl	$3, %ecx
	andl	$15, %r10d
	andl	$2047, %ecx
	subl	%r10d, %ecx
	cmpl	%edi, %ecx
	jnb	.L1006
	testl	%ecx, %ecx
	leal	31(%rcx), %edi
	cmovns	%ecx, %edi
	sarl	$5, %edi
	andl	$1, %edx
	jne	.L823
	cmpl	%edi, 11(%rax)
	jle	.L823
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$27, %edx
	addl	%edx, %ecx
	andl	$31, %ecx
	subl	%edx, %ecx
	movl	$1, %edx
	sall	%cl, %edx
	testb	%sil, %sil
	je	.L1037
.L827:
	sarq	$32, %rax
	testl	%eax, %edx
	sete	%cl
.L829:
	movq	%r14, %rax
	notq	%rax
	movl	%eax, %edx
	andl	$1, %edx
	testb	%cl, %cl
	jne	.L820
	testb	%dl, %dl
	je	.L830
	sarq	$32, %r14
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%r14d, %xmm2
	movq	%xmm2, %rax
.L831:
	movq	(%r9), %rdx
	andl	$16376, %ebx
	addq	$24, %r12
	movq	%rax, -1(%rbx,%rdx)
	cmpq	%r12, %r11
	jne	.L845
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1006:
	movq	%r14, %rax
	notq	%rax
.L820:
	movq	-168(%rbp), %rdi
	movq	(%rdi), %rdx
	movq	256(%rdx), %rdx
	testb	$1, %al
	je	.L832
	movq	-200(%rbp), %rax
	sarq	$32, %r14
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r14d, %xmm0
	movq	%rdx, (%rax)
	leaq	1(%rax), %r14
	testb	$1, %dl
	je	.L833
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L833
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r11, -240(%rbp)
	movl	%r8d, -232(%rbp)
	movq	%r9, -224(%rbp)
	movq	%xmm0, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-240(%rbp), %r11
	movl	-232(%rbp), %r8d
	movq	-216(%rbp), %xmm0
	movq	-224(%rbp), %r9
.L833:
	movq	-200(%rbp), %rax
	movq	%xmm0, 8(%rax)
	movq	(%r9), %r10
	addq	$16, %rax
	movq	%rax, -200(%rbp)
	leaq	-1(%r10), %rax
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L1028:
	movzbl	8(%rax), %r10d
	movzbl	8(%rax), %eax
	addl	%esi, %eax
	sall	$3, %r10d
	sall	$3, %eax
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L1033:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L1010:
	movl	28(%r12), %eax
	leal	1(%rax), %ebx
	movl	%edi, %eax
	sarl	%eax
	addl	%eax, %edi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo32Ej@PLT
	movl	$4, %edx
	cmpl	$4, %eax
	cmovl	%edx, %eax
	leal	(%rax,%rax,8), %eax
	cmpl	%eax, %ebx
	jnb	.L1039
	movl	28(%r12), %esi
	movq	(%r15), %rdi
	xorl	%edx, %edx
	movl	$4, %r14d
	addl	$1, %esi
	call	_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE@PLT
	movq	%rax, -240(%rbp)
	movq	(%rax), %rax
	movq	%rax, -152(%rbp)
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testl	$262144, %eax
	jne	.L708
	xorl	%r14d, %r14d
	testb	$24, %al
	sete	%r14b
	sall	$2, %r14d
.L708:
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	jle	.L1002
	movq	-208(%rbp), %rdi
	subl	$1, %eax
	movq	%r15, %r8
	movq	-240(%rbp), %rcx
	movl	%r14d, %r15d
	leaq	1(%rdi,%rax), %rax
	leaq	(%rdi,%rdi,2), %rbx
	leaq	(%rax,%rax,2), %r13
	salq	$3, %rbx
	salq	$3, %r13
	movq	%r13, %r14
	movq	-160(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L715:
	movq	0(%r13), %rax
	addq	%rbx, %rax
	testb	$8, 8(%rax)
	je	.L709
	movq	16(%rax), %rdx
	movl	(%rax), %eax
	movq	(%rcx), %rdi
	leal	16(,%rax,8), %eax
	movq	(%rdx), %r12
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%r12, (%rsi)
	testl	%r15d, %r15d
	je	.L709
	movq	%r12, %rax
	notq	%rax
	andl	$1, %eax
	cmpl	$4, %r15d
	je	.L1040
	testb	%al, %al
	jne	.L709
.L956:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L709
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L709
	movq	%r12, %rdx
	movq	%r8, -168(%rbp)
	movq	%rcx, -152(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-168(%rbp), %r8
	movq	-152(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L709:
	addq	$24, %rbx
	cmpq	%rbx, %r14
	jne	.L715
	movq	%r8, %r15
.L1002:
	movq	-216(%rbp), %r14
	movq	(%r15), %r9
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	%rdi, %rdx
	notq	%rdx
	andl	$1, %edx
.L758:
	testb	%dl, %dl
	jne	.L797
	cmpb	$0, _ZN2v88internal17FLAG_track_fieldsE(%rip)
	jne	.L774
.L785:
	cmpl	$4, %ecx
	je	.L775
	movl	$4, %edx
	cmpb	$3, %cl
	jg	.L787
.L775:
	movl	$4, %ebx
	testl	%ecx, %ecx
	jne	.L858
.L897:
	movl	$4, %ebx
.L789:
	movq	(%r15), %rsi
	movq	%rdi, -144(%rbp)
	movl	%ebx, %edx
	leaq	-144(%rbp), %rdi
	movl	%r11d, -192(%rbp)
	call	_ZN2v88internal6Object11OptimalTypeEPNS0_7IsolateENS0_14RepresentationE@PLT
	movl	%r14d, %ecx
	movq	(%r15), %rdi
	movl	%ebx, %r8d
	movl	-168(%rbp), %edx
	shrl	$2, %ecx
	movq	%rax, %r9
	movq	%r13, %rsi
	andl	$1, %ecx
	call	_ZN2v88internal3Map15GeneralizeFieldEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS0_14RepresentationENS4_INS0_9FieldTypeEEE@PLT
	movl	-192(%rbp), %r11d
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L1034:
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L804:
	movq	-192(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, -184(%rbp)
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L759:
	testb	$1, %dil
	je	.L797
	movq	-1(%rdi), %rdx
	cmpw	$65, 11(%rdx)
	sete	%dl
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L1018:
	testb	$1, %dil
	je	.L768
.L773:
	movq	39(%rax), %rax
	movl	%r11d, -232(%rbp)
	movl	%ecx, -224(%rbp)
	movq	%rdx, -192(%rbp)
	movq	15(%rbx,%rax), %rdi
	leaq	-144(%rbp), %rbx
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	movq	-192(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rax, -144(%rbp)
	movq	(%rdx), %rsi
	call	_ZNK2v88internal9FieldType11NowContainsENS0_6ObjectE@PLT
	movq	-192(%rbp), %rdx
	movl	-224(%rbp), %ecx
	testb	%al, %al
	movl	-232(%rbp), %r11d
	jne	.L797
	movq	(%rdx), %rax
	movq	(%r15), %rsi
	movl	%ecx, %edx
	movq	%rbx, %rdi
	movl	%r11d, -224(%rbp)
	movl	%ecx, -192(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal6Object11OptimalTypeEPNS0_7IsolateENS0_14RepresentationE@PLT
	movl	%r14d, %edx
	movq	(%r15), %rdi
	movq	%r13, %rsi
	movl	-192(%rbp), %ecx
	shrl	$2, %edx
	movq	%rax, %r9
	andl	$1, %edx
	movl	%ecx, %r8d
	movl	%edx, %ecx
	movl	-168(%rbp), %edx
	call	_ZN2v88internal3Map15GeneralizeFieldEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS0_14RepresentationENS4_INS0_9FieldTypeEEE@PLT
	movl	-224(%rbp), %r11d
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L832:
	movq	%rdx, -1(%r14)
	testq	%rdx, %rdx
	jne	.L1041
.L835:
	movq	(%r9), %r10
	leaq	-1(%r10), %rax
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L1012:
	movq	%r13, -192(%rbp)
	movq	-184(%rbp), %r13
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L1019:
	testb	$1, %dil
	je	.L771
.L772:
	cmpb	$0, _ZN2v88internal24FLAG_track_double_fieldsE(%rip)
	jne	.L779
.L783:
	cmpb	$0, _ZN2v88internal26FLAG_track_computed_fieldsE(%rip)
	jne	.L1042
.L781:
	cmpb	$0, _ZN2v88internal29FLAG_track_heap_object_fieldsE(%rip)
	je	.L785
	cmpl	$3, %ecx
	je	.L899
	testl	%ecx, %ecx
	je	.L883
	movl	$3, %edx
.L787:
	movl	$4, %ebx
	cmpb	%dl, %cl
	jg	.L791
.L1008:
	testl	%ecx, %ecx
	je	.L789
.L858:
	cmpb	$0, _ZN2v88internal40FLAG_modify_field_representation_inplaceE(%rip)
	je	.L790
	andl	$-3, %ecx
	cmpb	$1, %cl
	jne	.L790
	cmpb	$4, %bl
	je	.L789
	.p2align 4,,10
	.p2align 3
.L790:
	movl	-168(%rbp), %edi
	testl	%edi, %edi
	je	.L1005
	movq	(%r15), %rbx
	movl	-168(%rbp), %edx
	leaq	-144(%rbp), %rdi
	movq	%rax, -144(%rbp)
	movq	%rbx, %rsi
	subl	$1, %edx
	call	_ZNK2v88internal3Map14FindFieldOwnerEPNS0_7IsolateEi@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	jne	.L1004
	movq	41088(%rbx), %rax
	movq	%rax, -192(%rbp)
	cmpq	41096(%rbx), %rax
	je	.L1043
.L796:
	movq	-192(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	movq	(%r15), %r9
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L1039:
	movl	32(%r12), %esi
	movq	(%r15), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal9HashTableINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	(%r15), %rdi
	movq	%rax, %r12
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	jle	.L705
	movq	-208(%rbp), %rbx
	subl	$1, %eax
	movq	-160(%rbp), %r14
	movq	%r12, %rsi
	leaq	1(%rbx,%rax), %rax
	leaq	(%rbx,%rbx,2), %r13
	leaq	(%rax,%rax,2), %rbx
	salq	$3, %r13
	salq	$3, %rbx
	.p2align 4,,10
	.p2align 3
.L706:
	movq	(%r14), %rdx
	addq	%r13, %rdx
	testb	$8, 8(%rdx)
	je	.L704
	movl	(%rdx), %r10d
	movq	16(%rdx), %rcx
	movl	$192, %r9d
	xorl	%r8d, %r8d
	movl	%r10d, %edx
	call	_ZN2v88internal16NumberDictionary3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEENS4_INS0_8JSObjectEEENS0_15PropertyDetailsE@PLT
	movq	(%r15), %rdi
	movq	%rax, %rsi
.L704:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L706
	movq	%rsi, %r12
.L705:
	movq	-216(%rbp), %rsi
	movl	$12, %edx
	call	_ZN2v88internal3Map14AsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	%r12, -240(%rbp)
	movq	(%r15), %r9
	movq	%rax, %r14
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L1026:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L768:
	testb	%sil, %sil
	je	.L775
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L1032:
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r10, %rdi
	movq	%r11, -232(%rbp)
	movl	%r8d, -224(%rbp)
	addq	$24, %r12
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-232(%rbp), %r11
	movl	-224(%rbp), %r8d
	movq	-216(%rbp), %r9
	cmpq	%r12, %r11
	jne	.L845
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1036:
	movslq	11(%rax), %rdi
	sall	$3, %edi
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L1023:
	movl	%r9d, -244(%rbp)
	movl	%r9d, -232(%rbp)
	movq	%rsi, -224(%rbp)
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-244(%rbp), %r9d
	movl	-232(%rbp), %r11d
	movq	-224(%rbp), %rsi
	movq	-192(%rbp), %rdi
	movq	%rax, %rdx
	jmp	.L725
.L1027:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L854
.L774:
	testb	$1, %dil
	jne	.L772
	cmpl	$1, %ecx
	je	.L777
	movl	$1, %edx
	testl	%ecx, %ecx
	je	.L771
.L778:
	cmpl	$3, %ecx
	jne	.L787
.L869:
	cmpb	$0, _ZN2v88internal40FLAG_modify_field_representation_inplaceE(%rip)
	jne	.L897
	jmp	.L790
.L1041:
	testb	$1, %dl
	je	.L835
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L835
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r11, -232(%rbp)
	movl	%r8d, -224(%rbp)
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-232(%rbp), %r11
	movl	-224(%rbp), %r8d
	movq	-216(%rbp), %r9
	jmp	.L835
.L859:
	xorl	%edx, %edx
	cmpl	$3, %ecx
	jne	.L787
.L791:
	movl	%ecx, %ebx
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1031:
	testb	%al, %al
	jne	.L843
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L957
	movq	%r10, %rdi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r11, -240(%rbp)
	movl	%r8d, -232(%rbp)
	movq	%r9, -224(%rbp)
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %r10
	movq	-224(%rbp), %r9
	movl	-232(%rbp), %r8d
	movq	-240(%rbp), %r11
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L1011:
	movzbl	7(%rcx), %eax
	movzbl	7(%rsi), %edx
	cmpb	%al, %dl
	jne	.L719
	movl	15(%rcx), %eax
	movl	-176(%rbp), %r10d
	shrl	$10, %eax
	andl	$1023, %eax
	movl	%eax, -200(%rbp)
	testl	%r10d, %r10d
	jg	.L718
	movl	-200(%rbp), %edx
	testl	%edx, %edx
	je	.L900
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	jne	.L900
	movl	$0, -152(%rbp)
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L1015:
	movl	$3, -112(%rbp)
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L1014:
	movq	-184(%rbp), %rax
	leaq	-144(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r8, -192(%rbp)
	movq	(%rax), %rax
	movq	%rax, -144(%rbp)
	movl	-168(%rbp), %eax
	leal	-1(%rax), %edx
	call	_ZNK2v88internal3Map14FindFieldOwnerEPNS0_7IsolateEi@PLT
	movq	41112(%rbx), %rdi
	movq	-192(%rbp), %r8
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L746
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-192(%rbp), %r8
	movq	%rax, %r13
.L747:
	movq	(%r15), %rbx
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L830:
	movq	7(%r14), %rax
	jmp	.L831
.L1025:
	movq	%r13, %rdi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L699
.L1040:
	testb	%al, %al
	jne	.L709
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L956
	movq	%r12, %rdx
	movq	%r8, -200(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%rsi, -168(%rbp)
	movq	%rdi, -152(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-152(%rbp), %rdi
	movq	-168(%rbp), %rsi
	movq	-192(%rbp), %rcx
	movq	-200(%rbp), %r8
	jmp	.L956
.L899:
	testl	%ecx, %ecx
	jne	.L790
.L883:
	movl	$3, %ebx
	jmp	.L789
.L746:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L1044
.L748:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L747
.L753:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -112(%rbp)
	jmp	.L751
.L801:
	movq	41088(%r9), %rax
	movq	%rax, -192(%rbp)
	cmpq	41096(%r9), %rax
	je	.L1045
.L803:
	movq	-192(%rbp), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r9)
	movq	%r12, (%rsi)
	movq	(%r15), %r9
	jmp	.L717
.L777:
	testl	%ecx, %ecx
	je	.L771
	movl	$1, %ebx
	jmp	.L858
.L1044:
	movq	%rbx, %rdi
	movq	%r8, -200(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L740:
	movq	41088(%rdx), %r9
	cmpq	41096(%rdx), %r9
	je	.L1046
.L742:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r9)
	jmp	.L741
.L779:
	movq	-1(%rdi), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L783
	cmpl	$2, %ecx
	je	.L901
	cmpb	$1, %cl
	jg	.L896
.L901:
	testl	%ecx, %ecx
	je	.L790
	movl	$2, %ebx
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L1037:
	leal	0(,%rdi,4), %ecx
	movslq	%ecx, %rcx
	movl	15(%rax,%rcx), %eax
	testl	%eax, %edx
	sete	%cl
	jmp	.L829
.L1042:
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rdi, -37512(%rdx)
	jne	.L781
	testb	%cl, %cl
	jne	.L859
	xorl	%ebx, %ebx
	jmp	.L1008
.L771:
	movl	$1, %ebx
	jmp	.L789
.L1046:
	movq	%rdx, %rdi
	movq	%rsi, -224(%rbp)
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	-192(%rbp), %rdx
	movq	%rax, %r9
	jmp	.L742
.L823:
	cmpl	$31, %ecx
	jg	.L825
	testb	%sil, %sil
	je	.L825
	movl	$1, %edx
	sall	%cl, %edx
	jmp	.L827
.L734:
	movq	41088(%rdi), %rbx
	cmpq	41096(%rdi), %rbx
	je	.L1047
.L736:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%rdi)
	movq	%rsi, (%rbx)
	jmp	.L735
.L1043:
	movq	%rbx, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, -192(%rbp)
	jmp	.L796
.L825:
	leaq	.LC7(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1045:
	movq	%r9, %rdi
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %r9
	movq	%rax, -192(%rbp)
	jmp	.L803
.L1047:
	movq	%rsi, -224(%rbp)
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	-192(%rbp), %rdi
	movq	%rax, %rbx
	jmp	.L736
.L900:
	movq	%r14, -192(%rbp)
	movl	$0, -152(%rbp)
	jmp	.L717
.L1016:
	movq	%r9, %r8
	jmp	.L874
.L896:
	movl	$2, %edx
	jmp	.L778
.L1035:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19812:
	.size	_ZN2v88internal10JsonParserIhE15BuildJsonObjectERKNS2_16JsonContinuationERKSt6vectorINS0_12JsonPropertyESaIS7_EENS0_6HandleINS0_3MapEEE, .-_ZN2v88internal10JsonParserIhE15BuildJsonObjectERKNS2_16JsonContinuationERKSt6vectorINS0_12JsonPropertyESaIS7_EENS0_6HandleINS0_3MapEEE
	.section	.text._ZNSt6vectorIN2v88internal10JsonParserIhE16JsonContinuationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal10JsonParserIhE16JsonContinuationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal10JsonParserIhE16JsonContinuationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal10JsonParserIhE16JsonContinuationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal10JsonParserIhE16JsonContinuationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB21618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r14
	movq	(%rdi), %r12
	movabsq	$-3689348814741910323, %rdi
	movq	%r14, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movabsq	$230584300921369395, %rdi
	cmpq	%rdi, %rax
	je	.L1075
	movq	%rsi, %r8
	subq	%r12, %r8
	testq	%rax, %rax
	je	.L1063
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L1076
.L1050:
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	%rax, %rbx
	leaq	(%rax,%r15), %rax
	movq	-88(%rbp), %rdx
	movq	%rax, -64(%rbp)
	leaq	40(%rbx), %rax
	movq	%rax, -56(%rbp)
.L1062:
	movq	16(%rdx), %rdi
	leaq	(%rbx,%r8), %rax
	movdqu	(%rdx), %xmm3
	movq	$0, (%rdx)
	movq	%rdi, 16(%rax)
	movl	32(%rdx), %edi
	movq	24(%rdx), %rdx
	movups	%xmm3, (%rax)
	movl	%edi, 32(%rax)
	movq	%rdx, 24(%rax)
	cmpq	%r12, %rsi
	je	.L1052
	movq	%r12, %rax
	movq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L1053:
	movdqu	(%rax), %xmm1
	addq	$40, %rax
	addq	$40, %rdx
	movups	%xmm1, -40(%rdx)
	movq	-24(%rax), %rdi
	movq	%rdi, -24(%rdx)
	movl	-16(%rax), %edi
	movq	$0, -40(%rax)
	movl	%edi, -16(%rdx)
	movl	-12(%rax), %edi
	movl	%edi, -12(%rdx)
	movl	-8(%rax), %edi
	movl	%edi, -8(%rdx)
	cmpq	%rax, %rsi
	jne	.L1053
	leaq	-40(%rsi), %rax
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	80(%rbx,%rax,8), %rax
	movq	%rax, -56(%rbp)
.L1052:
	movq	-56(%rbp), %rdx
	movq	%rsi, %rax
	cmpq	%r14, %rsi
	je	.L1059
	.p2align 4,,10
	.p2align 3
.L1058:
	movq	16(%rax), %rdi
	movdqu	(%rax), %xmm2
	addq	$40, %rax
	addq	$40, %rdx
	movq	$0, -40(%rax)
	movq	%rdi, -24(%rdx)
	movl	-16(%rax), %edi
	movups	%xmm2, -40(%rdx)
	movl	%edi, -16(%rdx)
	movl	-12(%rax), %edi
	movl	%edi, -12(%rdx)
	movl	-8(%rax), %edi
	movl	%edi, -8(%rdx)
	cmpq	%rax, %r14
	jne	.L1058
	movq	%r14, %rax
	movq	-56(%rbp), %rcx
	subq	%rsi, %rax
	subq	$40, %rax
	shrq	$3, %rax
	leaq	40(%rcx,%rax,8), %rax
	movq	%rax, -56(%rbp)
.L1059:
	movq	%r12, %r15
	cmpq	%r14, %r12
	je	.L1061
	.p2align 4,,10
	.p2align 3
.L1055:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1060
	movq	8(%r15), %rsi
	movq	16(%r15), %rdx
	subl	$1, 41104(%rdi)
	movq	%rsi, 41088(%rdi)
	cmpq	41096(%rdi), %rdx
	je	.L1060
	movq	%rdx, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1060:
	addq	$40, %r15
	cmpq	%r14, %r15
	jne	.L1055
.L1061:
	testq	%r12, %r12
	je	.L1057
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1057:
	movq	-64(%rbp), %rax
	movq	%rbx, %xmm0
	movhps	-56(%rbp), %xmm0
	movq	%rax, 16(%r13)
	movups	%xmm0, 0(%r13)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1076:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L1051
	movq	$40, -56(%rbp)
	xorl	%ebx, %ebx
	movq	$0, -64(%rbp)
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1063:
	movl	$40, %r15d
	jmp	.L1050
.L1051:
	cmpq	%rdi, %r9
	cmovbe	%r9, %rdi
	imulq	$40, %rdi, %r15
	jmp	.L1050
.L1075:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21618:
	.size	_ZNSt6vectorIN2v88internal10JsonParserIhE16JsonContinuationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal10JsonParserIhE16JsonContinuationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal12JsonPropertyESaIS2_EE17_M_realloc_insertIJNS1_10JsonStringEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal12JsonPropertyESaIS2_EE17_M_realloc_insertIJNS1_10JsonStringEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal12JsonPropertyESaIS2_EE17_M_realloc_insertIJNS1_10JsonStringEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal12JsonPropertyESaIS2_EE17_M_realloc_insertIJNS1_10JsonStringEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal12JsonPropertyESaIS2_EE17_M_realloc_insertIJNS1_10JsonStringEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB21621:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r14
	movabsq	$384307168202282325, %rdi
	movq	%rsi, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rdi, %rax
	je	.L1094
	movq	%r12, %rcx
	subq	%r14, %rcx
	testq	%rax, %rax
	je	.L1086
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L1095
.L1079:
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %rcx
	movq	%rax, %r13
	leaq	(%rax,%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	24(%r13), %rbx
.L1085:
	leaq	0(%r13,%rcx), %rax
	movq	(%rdx), %rcx
	movl	8(%rdx), %edx
	movq	$0, 16(%rax)
	movq	%rcx, (%rax)
	movl	%edx, 8(%rax)
	cmpq	%r14, %r12
	je	.L1081
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L1082:
	movdqu	(%rax), %xmm1
	addq	$24, %rax
	addq	$24, %rdx
	movups	%xmm1, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L1082
	leaq	-24(%r12), %rax
	subq	%r14, %rax
	shrq	$3, %rax
	leaq	48(%r13,%rax,8), %rbx
.L1081:
	cmpq	%rsi, %r12
	je	.L1083
	subq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-24(%rsi), %rax
	movq	%r12, %rsi
	shrq	$3, %rax
	leaq	24(,%rax,8), %rdx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, %rbx
.L1083:
	testq	%r14, %r14
	je	.L1084
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1084:
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rbx, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1095:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L1080
	movq	$0, -56(%rbp)
	movl	$24, %ebx
	xorl	%r13d, %r13d
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1086:
	movl	$24, %ebx
	jmp	.L1079
.L1080:
	cmpq	%rdi, %r8
	cmovbe	%r8, %rdi
	imulq	$24, %rdi, %rbx
	jmp	.L1079
.L1094:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21621:
	.size	_ZNSt6vectorIN2v88internal12JsonPropertyESaIS2_EE17_M_realloc_insertIJNS1_10JsonStringEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal12JsonPropertyESaIS2_EE17_M_realloc_insertIJNS1_10JsonStringEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm, @function
_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm:
.LFB21638:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1135
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%rdi), %rax
	movq	%rcx, %r13
	subq	(%rdi), %r13
	subq	%rcx, %rax
	movq	%r13, %r15
	sarq	$3, %rax
	sarq	$3, %r15
	subq	%r15, %rsi
	cmpq	%rbx, %rax
	jb	.L1098
	cmpq	$1, %rbx
	je	.L1117
	leaq	-2(%rbx), %rdx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	addq	$1, %rdx
	.p2align 4,,10
	.p2align 3
.L1100:
	movq	%rax, %rsi
	addq	$1, %rax
	salq	$4, %rsi
	movups	%xmm0, (%rcx,%rsi)
	cmpq	%rax, %rdx
	ja	.L1100
	leaq	(%rdx,%rdx), %rax
	salq	$4, %rdx
	addq	%rcx, %rdx
	cmpq	%rax, %rbx
	je	.L1101
.L1099:
	movq	$0, (%rdx)
.L1101:
	leaq	(%rcx,%rbx,8), %rax
	movq	%rax, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1135:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1098:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rsi
	jb	.L1138
	cmpq	%rbx, %r15
	movq	%rbx, %rsi
	cmovnb	%r15, %rsi
	addq	%r15, %rsi
	cmpq	%rdx, %rsi
	cmova	%rdx, %rsi
	salq	$3, %rsi
	movq	%rsi, %rdi
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	cmpq	$1, %rbx
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	leaq	(%rax,%r13), %rcx
	je	.L1116
	leaq	-2(%rbx), %rax
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L1107:
	movq	%rdx, %rdi
	addq	$1, %rdx
	salq	$4, %rdi
	movups	%xmm0, (%rcx,%rdi)
	cmpq	%rdx, %rax
	ja	.L1107
	leaq	(%rax,%rax), %rdx
	salq	$4, %rax
	addq	%rax, %rcx
	cmpq	%rdx, %rbx
	je	.L1105
.L1116:
	movq	$0, (%rcx)
.L1105:
	movq	8(%r12), %rcx
	movq	(%r12), %rdi
	cmpq	%rdi, %rcx
	je	.L1115
	leaq	-8(%rcx), %rax
	leaq	15(%rdi), %rdx
	subq	%rdi, %rax
	subq	%r14, %rdx
	shrq	$3, %rax
	cmpq	$30, %rdx
	jbe	.L1119
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rax
	je	.L1119
	addq	$1, %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1113:
	movdqu	(%rdi,%rdx), %xmm1
	movups	%xmm1, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L1113
	movq	%rax, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	leaq	(%rdi,%rdx), %rcx
	addq	%r14, %rdx
	cmpq	%r8, %rax
	je	.L1109
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
.L1109:
	movq	%rsi, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rsi
.L1110:
	addq	%r15, %rbx
	movq	%r14, (%r12)
	leaq	(%r14,%rbx,8), %rax
	addq	%rsi, %r14
	movq	%rax, 8(%r12)
	movq	%r14, 16(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1119:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L1111:
	movq	(%rax), %r8
	addq	$8, %rax
	addq	$8, %rdx
	movq	%r8, -8(%rdx)
	cmpq	%rax, %rcx
	jne	.L1111
.L1115:
	testq	%rdi, %rdi
	je	.L1110
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1117:
	movq	%rcx, %rdx
	jmp	.L1099
.L1138:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21638:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm, .-_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm
	.section	.text._ZN2v88internal10JsonParserItE12DecodeStringIhEEvPT_ii,"axG",@progbits,_ZN2v88internal10JsonParserItE12DecodeStringIhEEvPT_ii,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE12DecodeStringIhEEvPT_ii
	.type	_ZN2v88internal10JsonParserItE12DecodeStringIhEEvPT_ii, @function
_ZN2v88internal10JsonParserItE12DecodeStringIhEEvPT_ii:
.LFB21670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movslq	%ecx, %r8
	leaq	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE(%rip), %r9
	movl	$-1, %r10d
	movl	$-16, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	64(%rdi), %rax
	leaq	.L1150(%rip), %rdi
	leaq	(%rax,%rdx,2), %rax
	movq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L1165:
	movq	%rdx, %rcx
	movq	%r8, %rbx
	subq	%rsi, %rcx
	subq	%rcx, %rbx
	movq	%rbx, %rcx
	addq	%rcx, %rcx
	movq	%rcx, %r12
	leaq	(%rax,%rcx), %rbx
	sarq	$3, %rcx
	sarq	%r12
	testq	%rcx, %rcx
	jle	.L1140
	leaq	(%rdx,%rcx,4), %r12
.L1145:
	movzwl	(%rax), %ecx
	cmpw	$92, %cx
	je	.L1141
	movb	%cl, (%rdx)
	movzwl	2(%rax), %ecx
	cmpw	$92, %cx
	je	.L1195
	movb	%cl, 1(%rdx)
	movzwl	4(%rax), %ecx
	cmpw	$92, %cx
	je	.L1196
	movb	%cl, 2(%rdx)
	movzwl	6(%rax), %ecx
	cmpw	$92, %cx
	je	.L1197
	addq	$4, %rdx
	addq	$8, %rax
	movb	%cl, -1(%rdx)
	cmpq	%r12, %rdx
	jne	.L1145
	movq	%rbx, %r12
	subq	%rax, %r12
	sarq	%r12
.L1140:
	cmpq	$2, %r12
	je	.L1166
	cmpq	$3, %r12
	je	.L1167
	cmpq	$1, %r12
	je	.L1168
.L1139:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1195:
	.cfi_restore_state
	addq	$1, %rdx
	addq	$2, %rax
.L1141:
	cmpq	%rax, %rbx
	je	.L1139
	movzwl	2(%rax), %ecx
	leaq	2(%rax), %r12
	movq	%rcx, %rbx
	movzbl	(%r9,%rcx), %ecx
	andl	$7, %ecx
	movslq	(%rdi,%rcx,4), %rcx
	addq	%rdi, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internal10JsonParserItE12DecodeStringIhEEvPT_ii,"aG",@progbits,_ZN2v88internal10JsonParserItE12DecodeStringIhEEvPT_ii,comdat
	.align 4
	.align 4
.L1150:
	.long	.L1148-.L1150
	.long	.L1156-.L1150
	.long	.L1155-.L1150
	.long	.L1154-.L1150
	.long	.L1153-.L1150
	.long	.L1152-.L1150
	.long	.L1151-.L1150
	.long	.L1149-.L1150
	.section	.text._ZN2v88internal10JsonParserItE12DecodeStringIhEEvPT_ii,"axG",@progbits,_ZN2v88internal10JsonParserItE12DecodeStringIhEEvPT_ii,comdat
	.p2align 4,,10
	.p2align 3
.L1149:
	movzwl	4(%rax), %ecx
	subl	$48, %ecx
	cmpl	$9, %ecx
	jbe	.L1198
	orl	$32, %ecx
	leal	-49(%rcx), %ebx
	subl	$39, %ecx
	sall	$4, %ecx
	cmpl	$6, %ebx
	cmovnb	%r11d, %ecx
	movl	%ecx, %r12d
.L1158:
	movzwl	6(%rax), %ebx
	leal	-48(%rbx), %ecx
	cmpl	$9, %ecx
	jbe	.L1159
	orl	$32, %ecx
	leal	-49(%rcx), %ebx
	subl	$39, %ecx
	cmpl	$6, %ebx
	cmovnb	%r10d, %ecx
.L1159:
	movzwl	8(%rax), %ebx
	addl	%r12d, %ecx
	sall	$4, %ecx
	subl	$48, %ebx
	cmpl	$9, %ebx
	jbe	.L1160
	orl	$32, %ebx
	leal	-49(%rbx), %r12d
	subl	$39, %ebx
	cmpl	$6, %r12d
	cmovnb	%r10d, %ebx
.L1160:
	leaq	10(%rax), %r12
	movzwl	10(%rax), %eax
	addl	%ebx, %ecx
	sall	$4, %ecx
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L1161
	orl	$32, %eax
	leal	-49(%rax), %ebx
	subl	$39, %eax
	cmpl	$6, %ebx
	cmovnb	%r10d, %eax
.L1161:
	addl	%ecx, %eax
	cmpl	$65535, %eax
	jg	.L1199
	movb	%al, (%rdx)
	addq	$1, %rdx
	.p2align 4,,10
	.p2align 3
.L1164:
	leaq	2(%r12), %rax
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1151:
	movb	$13, (%rdx)
	addq	$1, %rdx
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1152:
	movb	$12, (%rdx)
	addq	$1, %rdx
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1153:
	movb	$10, (%rdx)
	addq	$1, %rdx
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1154:
	movb	$9, (%rdx)
	addq	$1, %rdx
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1155:
	movb	$8, (%rdx)
	addq	$1, %rdx
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1156:
	movb	%bl, (%rdx)
	addq	$1, %rdx
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1196:
	addq	$2, %rdx
	addq	$4, %rax
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1197:
	addq	$3, %rdx
	addq	$6, %rax
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1199:
	leal	-65536(%rax), %ecx
	movb	%al, 1(%rdx)
	addq	$2, %rdx
	shrl	$10, %ecx
	movb	%cl, -2(%rdx)
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1198:
	sall	$4, %ecx
	movl	%ecx, %r12d
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1167:
	movzwl	(%rax), %ecx
	cmpw	$92, %cx
	je	.L1141
	movb	%cl, (%rdx)
	addq	$2, %rax
	addq	$1, %rdx
.L1166:
	movzwl	(%rax), %ecx
	cmpw	$92, %cx
	je	.L1141
	movb	%cl, (%rdx)
	addq	$2, %rax
	addq	$1, %rdx
.L1168:
	movzwl	(%rax), %ecx
	cmpw	$92, %cx
	je	.L1141
	movb	%cl, (%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1148:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21670:
	.size	_ZN2v88internal10JsonParserItE12DecodeStringIhEEvPT_ii, .-_ZN2v88internal10JsonParserItE12DecodeStringIhEEvPT_ii
	.section	.text._ZN2v88internal10JsonParserItE12DecodeStringItEEvPT_ii,"axG",@progbits,_ZN2v88internal10JsonParserItE12DecodeStringItEEvPT_ii,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE12DecodeStringItEEvPT_ii
	.type	_ZN2v88internal10JsonParserItE12DecodeStringItEEvPT_ii, @function
_ZN2v88internal10JsonParserItE12DecodeStringItEEvPT_ii:
.LFB21673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movslq	%ecx, %rcx
	leaq	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE(%rip), %r9
	leaq	(%rcx,%rcx), %r8
	movl	$-1, %r10d
	movl	$-16, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	64(%rdi), %rax
	leaq	.L1211(%rip), %rdi
	leaq	(%rax,%rdx,2), %rax
	movq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L1226:
	movq	%rdx, %rcx
	movq	%r8, %rbx
	subq	%rsi, %rcx
	subq	%rcx, %rbx
	movq	%rbx, %rcx
	leaq	(%rax,%rbx), %rbx
	movq	%rcx, %r12
	sarq	$3, %rcx
	sarq	%r12
	testq	%rcx, %rcx
	jle	.L1201
	leaq	(%rdx,%rcx,8), %r12
.L1206:
	movzwl	(%rax), %ecx
	cmpw	$92, %cx
	je	.L1202
	movw	%cx, (%rdx)
	movzwl	2(%rax), %ecx
	cmpw	$92, %cx
	je	.L1256
	movw	%cx, 2(%rdx)
	movzwl	4(%rax), %ecx
	cmpw	$92, %cx
	je	.L1257
	movw	%cx, 4(%rdx)
	movzwl	6(%rax), %ecx
	cmpw	$92, %cx
	je	.L1258
	addq	$8, %rdx
	addq	$8, %rax
	movw	%cx, -2(%rdx)
	cmpq	%rdx, %r12
	jne	.L1206
	movq	%rbx, %r12
	subq	%rax, %r12
	sarq	%r12
.L1201:
	cmpq	$2, %r12
	je	.L1227
	cmpq	$3, %r12
	je	.L1228
	cmpq	$1, %r12
	je	.L1229
.L1200:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1256:
	.cfi_restore_state
	addq	$2, %rdx
	addq	$2, %rax
.L1202:
	cmpq	%rax, %rbx
	je	.L1200
	movzwl	2(%rax), %ecx
	leaq	2(%rax), %r12
	movq	%rcx, %rbx
	movzbl	(%r9,%rcx), %ecx
	andl	$7, %ecx
	movslq	(%rdi,%rcx,4), %rcx
	addq	%rdi, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internal10JsonParserItE12DecodeStringItEEvPT_ii,"aG",@progbits,_ZN2v88internal10JsonParserItE12DecodeStringItEEvPT_ii,comdat
	.align 4
	.align 4
.L1211:
	.long	.L1209-.L1211
	.long	.L1217-.L1211
	.long	.L1216-.L1211
	.long	.L1215-.L1211
	.long	.L1214-.L1211
	.long	.L1213-.L1211
	.long	.L1212-.L1211
	.long	.L1210-.L1211
	.section	.text._ZN2v88internal10JsonParserItE12DecodeStringItEEvPT_ii,"axG",@progbits,_ZN2v88internal10JsonParserItE12DecodeStringItEEvPT_ii,comdat
	.p2align 4,,10
	.p2align 3
.L1210:
	movzwl	4(%rax), %ecx
	subl	$48, %ecx
	cmpl	$9, %ecx
	jbe	.L1259
	orl	$32, %ecx
	leal	-49(%rcx), %ebx
	subl	$39, %ecx
	sall	$4, %ecx
	cmpl	$6, %ebx
	cmovnb	%r11d, %ecx
	movl	%ecx, %r12d
.L1219:
	movzwl	6(%rax), %ebx
	leal	-48(%rbx), %ecx
	cmpl	$9, %ecx
	jbe	.L1220
	orl	$32, %ecx
	leal	-49(%rcx), %ebx
	subl	$39, %ecx
	cmpl	$6, %ebx
	cmovnb	%r10d, %ecx
.L1220:
	movzwl	8(%rax), %ebx
	addl	%r12d, %ecx
	sall	$4, %ecx
	subl	$48, %ebx
	cmpl	$9, %ebx
	jbe	.L1221
	orl	$32, %ebx
	leal	-49(%rbx), %r12d
	subl	$39, %ebx
	cmpl	$6, %r12d
	cmovnb	%r10d, %ebx
.L1221:
	leaq	10(%rax), %r12
	movzwl	10(%rax), %eax
	addl	%ebx, %ecx
	sall	$4, %ecx
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L1222
	orl	$32, %eax
	leal	-49(%rax), %ebx
	subl	$39, %eax
	cmpl	$6, %ebx
	cmovnb	%r10d, %eax
.L1222:
	addl	%ecx, %eax
	cmpl	$65535, %eax
	jg	.L1260
	movw	%ax, (%rdx)
	addq	$2, %rdx
	.p2align 4,,10
	.p2align 3
.L1225:
	leaq	2(%r12), %rax
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1212:
	movl	$13, %eax
	addq	$2, %rdx
	movw	%ax, -2(%rdx)
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1213:
	movl	$12, %ecx
	addq	$2, %rdx
	movw	%cx, -2(%rdx)
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1214:
	movl	$10, %ebx
	addq	$2, %rdx
	movw	%bx, -2(%rdx)
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1215:
	movl	$9, %eax
	addq	$2, %rdx
	movw	%ax, -2(%rdx)
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1216:
	movl	$8, %eax
	addq	$2, %rdx
	movw	%ax, -2(%rdx)
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1217:
	movw	%bx, (%rdx)
	addq	$2, %rdx
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1257:
	addq	$4, %rdx
	addq	$4, %rax
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1258:
	addq	$6, %rdx
	addq	$6, %rax
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1260:
	leal	-65536(%rax), %ecx
	andw	$1023, %ax
	addq	$4, %rdx
	shrl	$10, %ecx
	subw	$9216, %ax
	andw	$1023, %cx
	movw	%ax, -2(%rdx)
	subw	$10240, %cx
	movw	%cx, -4(%rdx)
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1259:
	sall	$4, %ecx
	movl	%ecx, %r12d
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1228:
	movzwl	(%rax), %ecx
	cmpw	$92, %cx
	je	.L1202
	movw	%cx, (%rdx)
	addq	$2, %rax
	addq	$2, %rdx
.L1227:
	movzwl	(%rax), %ecx
	cmpw	$92, %cx
	je	.L1202
	movw	%cx, (%rdx)
	addq	$2, %rax
	addq	$2, %rdx
.L1229:
	movzwl	(%rax), %ecx
	cmpw	$92, %cx
	je	.L1202
	movw	%cx, (%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1209:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21673:
	.size	_ZN2v88internal10JsonParserItE12DecodeStringItEEvPT_ii, .-_ZN2v88internal10JsonParserItE12DecodeStringItEEvPT_ii
	.section	.text._ZN2v88internal10JsonParserItE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE,"axG",@progbits,_ZN2v88internal10JsonParserItE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal10JsonParserItE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal10JsonParserItE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE:
.LFB19855:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movslq	4(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jne	.L1262
	movq	(%rdi), %rax
	subq	$-128, %rax
.L1263:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1334
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1262:
	.cfi_restore_state
	movzbl	8(%rbx), %r8d
	movq	%rdx, %r13
	testb	$2, %r8b
	je	.L1264
	testb	$4, %r8b
	jne	.L1264
	movslq	(%rbx), %rdx
	testq	%r13, %r13
	je	.L1266
	movq	64(%rdi), %rax
	movq	%rsi, -72(%rbp)
	leaq	-80(%rbp), %rdi
	movq	%r13, %rsi
	leaq	(%rax,%rdx,2), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_17MatchesItEEbRKNS0_6VectorIKT_EENS0_6HandleINS0_6StringEEE
	testb	%al, %al
	je	.L1269
	movq	%r13, %rax
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1264:
	andl	$1, %r8d
	movq	(%r14), %rdi
	jne	.L1267
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1285
	movq	(%rax), %rdi
	movslq	4(%rbx), %rcx
	movslq	(%rbx), %rdx
	leaq	15(%rdi), %r15
	movq	%r15, %rsi
	testb	$4, 8(%rbx)
	jne	.L1286
	movq	64(%r14), %r10
	leaq	(%rdx,%rdx), %rax
	leaq	(%rcx,%rcx), %rdx
	leaq	(%r10,%rax), %r8
	cmpq	$3, %rcx
	ja	.L1335
	addq	%r15, %rdx
	cmpq	%rdx, %r15
	jnb	.L1298
	movq	%rdx, %rcx
	leaq	16(%r10,%rax), %rax
	leaq	16(%rdi), %r11
	movq	%rdi, %r9
	subq	%rdi, %rcx
	negq	%r9
	subq	$16, %rcx
	cmpq	%rax, %r15
	leaq	31(%rdi), %rax
	setnb	%r10b
	cmpq	%rax, %r8
	setnb	%al
	orb	%al, %r10b
	je	.L1333
	cmpq	%rdx, %r11
	setbe	%dil
	cmpq	$13, %rcx
	seta	%al
	testb	%al, %dil
	je	.L1333
	shrq	%rcx
	movl	$1, %eax
	leaq	(%r8,%r9), %rdi
	addq	$1, %rcx
	cmpq	%rdx, %r11
	cmova	%rax, %rcx
	movq	%r15, %rax
	movq	%rcx, %rsi
	shrq	$3, %rsi
	salq	$4, %rsi
	addq	%r15, %rsi
	.p2align 4,,10
	.p2align 3
.L1292:
	movdqu	-15(%rax,%rdi), %xmm3
	addq	$16, %rax
	movups	%xmm3, -16(%rax)
	cmpq	%rsi, %rax
	jne	.L1292
	movq	%rcx, %rax
	andq	$-8, %rax
	leaq	(%rax,%rax), %rsi
	addq	%rsi, %r15
	addq	%rsi, %r8
	cmpq	%rax, %rcx
	je	.L1298
	movzwl	(%r8), %eax
	movw	%ax, (%r15)
	leaq	2(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L1298
	movzwl	2(%r8), %eax
	movw	%ax, 2(%r15)
	leaq	4(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L1298
	movzwl	4(%r8), %eax
	movw	%ax, 4(%r15)
	leaq	6(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L1298
	movzwl	6(%r8), %eax
	movw	%ax, 6(%r15)
	leaq	8(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L1298
	movzwl	8(%r8), %eax
	movw	%ax, 8(%r15)
	leaq	10(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L1298
	movzwl	10(%r8), %eax
	movw	%ax, 10(%r15)
	leaq	12(%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L1298
	movzwl	12(%r8), %eax
	movw	%ax, 12(%r15)
	.p2align 4,,10
	.p2align 3
.L1298:
	movq	%r12, %rax
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1267:
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1285
	movq	(%rax), %rax
	movslq	4(%rbx), %rcx
	movslq	(%rbx), %rdx
	leaq	15(%rax), %r12
	testb	$4, 8(%rbx)
	jne	.L1273
	movq	64(%r14), %rdi
	leaq	(%rdx,%rdx), %rsi
	addq	%r12, %rcx
	leaq	(%rdi,%rsi), %r9
	cmpq	%rcx, %r12
	jnb	.L1284
	movq	%rcx, %rdx
	leaq	16(%rax), %r10
	movl	$1, %r8d
	movl	$2, %r11d
	subq	%rax, %rdx
	subq	$15, %rdx
	cmpq	%r10, %rcx
	cmovnb	%rdx, %r8
	addq	%rdx, %rdx
	cmpq	%r10, %rcx
	cmovb	%r11, %rdx
	addq	%rsi, %rdx
	addq	%rdi, %rdx
	cmpq	%rdx, %r12
	leaq	(%r12,%r8), %rdx
	setnb	%sil
	cmpq	%rdx, %r9
	setnb	%dl
	orb	%dl, %sil
	je	.L1276
	movq	%rcx, %rdx
	subq	%rax, %rdx
	subq	$16, %rdx
	cmpq	$14, %rdx
	seta	%sil
	cmpq	%r10, %rcx
	setnb	%dl
	testb	%dl, %sil
	je	.L1276
	leaq	-16(%r8), %rsi
	negq	%rax
	movdqa	.LC8(%rip), %xmm2
	movq	%r12, %rdx
	shrq	$4, %rsi
	leaq	(%r9,%rax,2), %rdi
	xorl	%eax, %eax
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L1278:
	movdqu	-30(%rdi,%rdx,2), %xmm0
	movdqu	-14(%rdi,%rdx,2), %xmm1
	addq	$1, %rax
	addq	$16, %rdx
	pand	%xmm2, %xmm0
	pand	%xmm2, %xmm1
	packuswb	%xmm1, %xmm0
	movups	%xmm0, -16(%rdx)
	cmpq	%rax, %rsi
	ja	.L1278
	movq	%rsi, %rax
	salq	$5, %rsi
	salq	$4, %rax
	addq	%r9, %rsi
	addq	%rax, %r12
	cmpq	%rax, %r8
	je	.L1284
	movzwl	(%rsi), %eax
	movb	%al, (%r12)
	leaq	1(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1284
	movzwl	2(%rsi), %eax
	movb	%al, 1(%r12)
	leaq	2(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1284
	movzwl	4(%rsi), %eax
	movb	%al, 2(%r12)
	leaq	3(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1284
	movzwl	6(%rsi), %eax
	movb	%al, 3(%r12)
	leaq	4(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1284
	movzwl	8(%rsi), %eax
	movb	%al, 4(%r12)
	leaq	5(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1284
	movzwl	10(%rsi), %eax
	movb	%al, 5(%r12)
	leaq	6(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1284
	movzwl	12(%rsi), %eax
	movb	%al, 6(%r12)
	leaq	7(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1284
	movzwl	14(%rsi), %eax
	movb	%al, 7(%r12)
	leaq	8(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1284
	movzwl	16(%rsi), %eax
	movb	%al, 8(%r12)
	leaq	9(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1284
	movzwl	18(%rsi), %eax
	movb	%al, 9(%r12)
	leaq	10(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1284
	movzwl	20(%rsi), %eax
	movb	%al, 10(%r12)
	leaq	11(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1284
	movzwl	22(%rsi), %eax
	movb	%al, 11(%r12)
	leaq	12(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1284
	movzwl	24(%rsi), %eax
	movb	%al, 12(%r12)
	leaq	13(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1284
	movzwl	26(%rsi), %eax
	movb	%al, 13(%r12)
	leaq	14(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1284
	movzwl	28(%rsi), %eax
	movb	%al, 14(%r12)
	.p2align 4,,10
	.p2align 3
.L1284:
	movq	%r15, %rax
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1269:
	movslq	(%rbx), %rdx
	movslq	4(%rbx), %rsi
	movzbl	8(%rbx), %r8d
.L1266:
	andl	$1, %r8d
	cmpb	$0, 17(%r14)
	movq	(%r14), %rdi
	je	.L1271
	movq	40(%r14), %r9
	movl	%esi, %ecx
	movq	%r9, %rsi
	call	_ZN2v88internal7Factory17InternalizeStringINS0_16SeqTwoByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib@PLT
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1286:
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserItE12DecodeStringItEEvPT_ii
	testb	$2, 8(%rbx)
	je	.L1298
	movslq	4(%rbx), %rax
	movq	%r15, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rax, %rcx
	testq	%r13, %r13
	je	.L1297
	leaq	-80(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_17MatchesItEEbRKNS0_6VectorIKT_EENS0_6HandleINS0_6StringEEE
	testb	%al, %al
	jne	.L1301
	movl	4(%rbx), %ecx
.L1297:
	movq	(%r14), %rdi
	movq	%r12, %rsi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17InternalizeStringINS0_16SeqTwoByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib@PLT
	movq	%rax, %r12
	jmp	.L1298
	.p2align 4,,10
	.p2align 3
.L1273:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserItE12DecodeStringIhEEvPT_ii
	testb	$2, 8(%rbx)
	je	.L1284
	movslq	4(%rbx), %rax
	movq	%r12, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rax, %rcx
	testq	%r13, %r13
	je	.L1283
	leaq	-80(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal12_GLOBAL__N_17MatchesIhEEbRKNS0_6VectorIKT_EENS0_6HandleINS0_6StringEEE
	testb	%al, %al
	jne	.L1300
	movl	4(%rbx), %ecx
.L1283:
	movq	(%r14), %rdi
	movq	%r15, %rsi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17InternalizeStringINS0_16SeqOneByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib@PLT
	movq	%rax, %r15
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1271:
	movq	64(%r14), %rax
	movq	%rsi, -72(%rbp)
	leaq	-80(%rbp), %rsi
	leaq	(%rax,%rdx,2), %rax
	movl	%r8d, %edx
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringItEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1285:
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1335:
	movq	%r8, %rsi
	movq	%r15, %rdi
	call	memcpy@PLT
	jmp	.L1298
	.p2align 4,,10
	.p2align 3
.L1276:
	negq	%rax
	leaq	(%r9,%rax,2), %rsi
	.p2align 4,,10
	.p2align 3
.L1282:
	movzwl	-30(%rsi,%r12,2), %edx
	movq	%r12, %rax
	addq	$1, %r12
	movb	%dl, (%rax)
	cmpq	%r12, %rcx
	ja	.L1282
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1333:
	leaq	(%r8,%r9), %rdi
	.p2align 4,,10
	.p2align 3
.L1330:
	movq	%rsi, %rax
	movzwl	-15(%rsi,%rdi), %ecx
	addq	$2, %rsi
	movw	%cx, (%rax)
	cmpq	%rsi, %rdx
	ja	.L1330
	jmp	.L1298
.L1300:
	movq	%r13, %r15
	jmp	.L1284
.L1301:
	movq	%r13, %r12
	jmp	.L1298
.L1334:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19855:
	.size	_ZN2v88internal10JsonParserItE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal10JsonParserItE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal10JsonParserItE15BuildJsonObjectERKNS2_16JsonContinuationERKSt6vectorINS0_12JsonPropertyESaIS7_EENS0_6HandleINS0_3MapEEE,"axG",@progbits,_ZN2v88internal10JsonParserItE15BuildJsonObjectERKNS2_16JsonContinuationERKSt6vectorINS0_12JsonPropertyESaIS7_EENS0_6HandleINS0_3MapEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE15BuildJsonObjectERKNS2_16JsonContinuationERKSt6vectorINS0_12JsonPropertyESaIS7_EENS0_6HandleINS0_3MapEEE
	.type	_ZN2v88internal10JsonParserItE15BuildJsonObjectERKNS2_16JsonContinuationERKSt6vectorINS0_12JsonPropertyESaIS7_EENS0_6HandleINS0_3MapEEE, @function
_ZN2v88internal10JsonParserItE15BuildJsonObjectERKNS2_16JsonContinuationERKSt6vectorINS0_12JsonPropertyESaIS7_EENS0_6HandleINS0_3MapEEE:
.LFB19872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -160(%rbp)
	movl	24(%r12), %edx
	movq	%rcx, -184(%rbp)
	movq	(%r15), %r13
	shrl	$2, %edx
	movl	%edx, %edi
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movq	%rdi, -208(%rbp)
	movq	%rax, %rdi
	movq	8(%rax), %rax
	subq	(%rdi), %rax
	sarq	$3, %rax
	imull	$-1431655765, %eax, %eax
	subl	%edx, %eax
	movl	%eax, -176(%rbp)
	movl	%eax, %r14d
	movq	12464(%r13), %rax
	subl	32(%r12), %r14d
	movq	39(%rax), %r8
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1337
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1338:
	movq	%r13, %rdi
	movl	%r14d, %edx
	call	_ZN2v88internal7Factory25ObjectLiteralMapFromCacheENS0_6HandleINS0_13NativeContextEEEi@PLT
	movl	32(%r12), %edi
	movq	%rax, -216(%rbp)
	testl	%edi, %edi
	jne	.L1650
	movq	(%r15), %r9
	movq	%rax, %r14
	leaq	288(%r9), %rax
	movq	%rax, -240(%rbp)
.L1347:
	cmpq	$0, -184(%rbp)
	je	.L1359
	movq	-184(%rbp), %rax
	movq	(%r14), %rsi
	movq	(%rax), %rcx
	movzbl	14(%rsi), %eax
	movzbl	14(%rcx), %edx
	shrl	$3, %eax
	shrl	$3, %edx
	cmpb	%al, %dl
	je	.L1651
.L1359:
	movl	-176(%rbp), %r11d
	testl	%r11d, %r11d
	jle	.L1540
	movl	$0, -200(%rbp)
.L1358:
	movq	-208(%rbp), %rax
	movq	%r14, %r13
	movq	%r9, %rdi
	movl	$0, -168(%rbp)
	movl	$0, -152(%rbp)
	leaq	(%rax,%rax,2), %r12
	salq	$3, %r12
.L1438:
	movq	-160(%rbp), %rax
	movq	(%rax), %r14
	addq	%r12, %r14
	testb	$8, 8(%r14)
	jne	.L1361
	movl	-168(%rbp), %eax
	cmpl	%eax, -200(%rbp)
	jle	.L1362
	leal	1(%rax), %r9d
	movq	-184(%rbp), %rax
	leal	(%r9,%r9,2), %ebx
	movl	%r9d, %r11d
	movq	(%rax), %rax
	sall	$3, %ebx
	movslq	%ebx, %rbx
	movq	39(%rax), %rax
	movq	-1(%rbx,%rax), %rsi
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	je	.L1363
	movq	%r8, %rdi
	movl	%r9d, -224(%rbp)
	movl	%r9d, -192(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-192(%rbp), %r11d
	movl	-224(%rbp), %r9d
	movq	%rax, %rdx
.L1364:
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	%r9d, -232(%rbp)
	movl	%r11d, -224(%rbp)
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal10JsonParserItE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE
	movq	-192(%rbp), %rdx
	movl	-224(%rbp), %r11d
	movl	-232(%rbp), %r9d
	movq	%rax, %r8
	cmpq	%rdx, %rax
	je	.L1652
	testq	%rax, %rax
	je	.L1635
	testq	%rdx, %rdx
	je	.L1635
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	je	.L1653
.L1635:
	movl	-168(%rbp), %r9d
	movq	(%r15), %rbx
	testl	%r9d, %r9d
	jne	.L1654
.L1385:
	movq	%r13, -192(%rbp)
	movl	$0, -200(%rbp)
.L1508:
	movq	%rbx, %xmm0
	movq	%r13, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	0(%r13), %rax
	movq	$0, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	71(%rax), %rax
	movq	%rax, -120(%rbp)
	testb	$1, %al
	je	.L1389
	cmpl	$3, %eax
	je	.L1389
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L1655
	cmpq	$1, %rdx
	jne	.L1457
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	jne	.L1393
	movl	$4, -112(%rbp)
	jmp	.L1391
.L1664:
	movq	39(%rsi), %rsi
.L1643:
	movq	41112(%rdx), %rdi
	andq	$-3, %rsi
	testq	%rdi, %rdi
	je	.L1380
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r9
.L1381:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal10JsonParserItE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE
	movq	-192(%rbp), %r9
	cmpq	%rbx, %rax
	movq	%rax, %r8
	je	.L1656
	testq	%rax, %rax
	je	.L1634
	movq	(%rax), %rax
	cmpq	%rax, (%rbx)
	jne	.L1634
	movl	-168(%rbp), %eax
	movq	%r13, -192(%rbp)
	movq	%r9, %r13
	leal	1(%rax), %r11d
	leal	(%r11,%r11,2), %ebx
	sall	$3, %ebx
	movslq	%ebx, %rbx
	.p2align 4,,10
	.p2align 3
.L1384:
	movq	0(%r13), %rax
	movq	16(%r14), %rdx
	movq	39(%rax), %rcx
	movq	7(%rbx,%rcx), %rcx
	movq	(%rdx), %rdi
	movzbl	_ZN2v88internal17FLAG_track_fieldsE(%rip), %esi
	movq	%rcx, %r14
	shrq	$38, %rcx
	andl	$7, %ecx
	sarq	$32, %r14
	cmpl	$1, %ecx
	jne	.L1397
	testb	%sil, %sil
	jne	.L1657
.L1397:
	cmpl	$2, %ecx
	jne	.L1538
	cmpb	$0, _ZN2v88internal24FLAG_track_double_fieldsE(%rip)
	jne	.L1399
.L1538:
	cmpb	$0, _ZN2v88internal29FLAG_track_heap_object_fieldsE(%rip)
	je	.L1403
	cmpl	$3, %ecx
	je	.L1658
.L1403:
	testb	%sil, %sil
	je	.L1407
	testl	%ecx, %ecx
	je	.L1659
.L1407:
	cmpl	$3, %ecx
	je	.L1413
.L1437:
	movl	%r11d, -168(%rbp)
	movq	(%r15), %rdi
.L1361:
	addl	$1, -152(%rbp)
	addq	$24, %r12
	movl	-152(%rbp), %eax
	cmpl	%eax, -176(%rbp)
	jne	.L1438
	movl	-168(%rbp), %eax
	movq	%r13, %r14
	movq	%rdi, %r9
	cmpl	%eax, -200(%rbp)
	jle	.L1524
	testl	%eax, %eax
	jne	.L1660
.L1524:
	movq	%r14, -192(%rbp)
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1362:
	movq	0(%r13), %rax
	movq	71(%rax), %rax
	testb	$1, %al
	je	.L1369
	cmpl	$3, %eax
	je	.L1369
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$3, %rdx
	je	.L1661
	cmpq	$1, %rdx
	je	.L1662
.L1457:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1662:
	movq	-1(%rax), %rdx
	cmpw	$149, 11(%rdx)
	je	.L1369
	movq	-1(%rax), %rax
	.p2align 4,,10
	.p2align 3
.L1369:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal10JsonParserItE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L1634
.L1514:
	movl	-168(%rbp), %eax
	movq	%r13, -192(%rbp)
	movq	%r8, %r13
	leal	1(%rax), %r11d
	leal	(%r11,%r11,2), %ebx
	sall	$3, %ebx
	movslq	%ebx, %rbx
	jmp	.L1384
	.p2align 4,,10
	.p2align 3
.L1363:
	movq	41088(%rdi), %rdx
	cmpq	%rdx, 41096(%rdi)
	je	.L1663
.L1365:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rdi)
	movq	%rsi, (%rdx)
	jmp	.L1364
	.p2align 4,,10
	.p2align 3
.L1653:
	leal	(%r9,%r9,2), %ebx
	movq	%r13, -192(%rbp)
	movl	%r9d, %r11d
	movq	-184(%rbp), %r13
	sall	$3, %ebx
	movslq	%ebx, %rbx
	jmp	.L1384
	.p2align 4,,10
	.p2align 3
.L1389:
	movl	$1, -112(%rbp)
.L1391:
	leaq	-144(%rbp), %rdi
	movl	$1, %edx
	movq	%r8, %rsi
	call	_ZN2v88internal19TransitionsAccessor28FindTransitionToDataPropertyENS0_6HandleINS0_4NameEEENS1_17RequestedLocationE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1645
	movl	-168(%rbp), %eax
	leal	1(%rax), %r11d
	leal	(%r11,%r11,2), %ebx
	sall	$3, %ebx
	movslq	%ebx, %rbx
	jmp	.L1384
	.p2align 4,,10
	.p2align 3
.L1661:
	andq	$-3, %rax
	movq	39(%rax), %rsi
	movl	15(%rax), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	leal	(%rdx,%rdx,2), %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	7(%rsi,%rdx), %rdx
	movq	%rdx, %rbx
	shrq	$35, %rdx
	shrq	$33, %rbx
	andl	$7, %edx
	movq	%rbx, %r8
	andl	$1, %r8d
	orl	%edx, %r8d
	jne	.L1369
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rax,%rsi), %rsi
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L1369
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	je	.L1374
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1369
.L1375:
	movq	0(%r13), %rax
	movq	(%r15), %rdx
	movq	71(%rax), %rsi
	testb	$1, %sil
	je	.L1457
	cmpl	$3, %esi
	je	.L1457
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	je	.L1643
	cmpq	$1, %rax
	jne	.L1457
	movq	-1(%rsi), %rax
	leaq	-1(%rsi), %rdi
	cmpw	$149, 11(%rax)
	je	.L1664
	movq	(%rdi), %rax
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1337:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L1665
.L1339:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r8, (%rsi)
	jmp	.L1338
	.p2align 4,,10
	.p2align 3
.L1634:
	movq	%r13, -192(%rbp)
	movq	(%r15), %rbx
	jmp	.L1508
.L1660:
	movl	%eax, %edx
	movq	0(%r13), %rax
	leaq	-144(%rbp), %rdi
	movq	%r9, %rsi
	subl	$1, %edx
	movq	%r9, -168(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal3Map14FindFieldOwnerEPNS0_7IsolateEi@PLT
	movq	-168(%rbp), %r9
	movq	%rax, %r12
	movq	41112(%r9), %rdi
	testq	%rdi, %rdi
	je	.L1441
	movq	%rax, %rsi
.L1644:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -192(%rbp)
	.p2align 4,,10
	.p2align 3
.L1645:
	movq	(%r15), %r9
.L1357:
	movq	-216(%rbp), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L1444
	movq	-192(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r9, %rdi
	movl	$2, %edx
	call	_ZN2v88internal7Factory22NewSlowJSObjectFromMapENS0_6HandleINS0_3MapEEEiNS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, -184(%rbp)
.L1445:
	movq	(%rax), %r13
	movq	-240(%rbp), %rax
	movq	(%rax), %r12
	leaq	15(%r13), %r14
	movq	%r12, 15(%r13)
	testb	$1, %r12b
	je	.L1494
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1666
.L1447:
	testb	$24, %al
	je	.L1494
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1667
	.p2align 4,,10
	.p2align 3
.L1494:
	movq	-184(%rbp), %rax
	movl	$4, %ecx
	movq	(%rax), %rax
	movq	%rax, -168(%rbp)
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testl	$262144, %eax
	jne	.L1449
	testb	$24, %al
	sete	%al
	movzbl	%al, %eax
	sall	$2, %eax
	movl	%eax, %ecx
.L1449:
	movl	-152(%rbp), %esi
	testl	%esi, %esi
	je	.L1484
	movl	-152(%rbp), %eax
	xorl	%r13d, %r13d
	movl	%ecx, %r8d
	movq	-208(%rbp), %rbx
	movq	%r15, -168(%rbp)
	movq	-184(%rbp), %r9
	subl	$1, %eax
	leaq	(%rbx,%rbx,2), %r12
	movq	$8, -200(%rbp)
	movq	-160(%rbp), %r15
	leaq	1(%rbx,%rax), %rax
	salq	$3, %r12
	leaq	(%rax,%rax,2), %r11
	salq	$3, %r11
	.p2align 4,,10
	.p2align 3
.L1485:
	movq	(%r15), %rsi
	addq	%r12, %rsi
	testb	$8, 8(%rsi)
	jne	.L1483
	movq	-192(%rbp), %rax
	addl	$1, %r13d
	leal	0(%r13,%r13,2), %edx
	movq	(%rax), %rax
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	39(%rax), %rcx
	movq	7(%rdx,%rcx), %rdi
	movq	16(%rsi), %rsi
	movq	(%rsi), %r14
	movq	7(%rdx,%rcx), %rcx
	sarq	$32, %rdi
	movzbl	7(%rax), %edx
	movzbl	8(%rax), %r10d
	movq	%rcx, %rbx
	shrq	$38, %rcx
	shrq	$51, %rbx
	subl	%r10d, %edx
	andl	$7, %ecx
	movq	%rbx, %rsi
	andl	$1023, %esi
	cmpl	%edx, %esi
	setl	%bl
	jl	.L1668
	subl	%edx, %esi
	movl	$16, %r10d
	leal	16(,%rsi,8), %eax
.L1454:
	cmpl	$2, %ecx
	jne	.L1669
	movl	$32768, %ecx
.L1455:
	movzbl	%bl, %ebx
	movslq	%edx, %rdx
	shrl	$6, %edi
	salq	$17, %rdx
	salq	$14, %rbx
	andl	$7, %edi
	orq	%rdx, %rbx
	movslq	%eax, %rdx
	orq	%rdx, %rbx
	movslq	%r10d, %rdx
	movq	(%r9), %r10
	salq	$27, %rdx
	orq	%rdx, %rbx
	leaq	-1(%r10), %rax
	orq	%rcx, %rbx
	cmpl	$2, %edi
	je	.L1670
.L1458:
	andl	$16376, %ebx
	addq	%rax, %rbx
	movq	%r14, (%rbx)
	testl	%r8d, %r8d
	je	.L1483
	movq	%r14, %rax
	notq	%rax
	andl	$1, %eax
	cmpl	$4, %r8d
	je	.L1671
	testb	%al, %al
	jne	.L1483
.L1597:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1483
	movq	%r10, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1672
	.p2align 4,,10
	.p2align 3
.L1483:
	addq	$24, %r12
	cmpq	%r12, %r11
	jne	.L1485
.L1678:
	movq	-168(%rbp), %r15
.L1484:
	movl	-152(%rbp), %esi
	cmpl	%esi, -176(%rbp)
	jle	.L1492
	movq	-208(%rbp), %rdi
	movslq	%esi, %rax
	leaq	(%rdi,%rax), %rdx
	leaq	(%rdx,%rdx,2), %rbx
	leaq	1(%rdi,%rax), %rdx
	movl	-176(%rbp), %eax
	salq	$3, %rbx
	subl	$1, %eax
	subl	%esi, %eax
	addq	%rdx, %rax
	leaq	(%rax,%rax,2), %r14
	leaq	0(,%r14,8), %rax
	movq	%rax, -176(%rbp)
	jmp	.L1493
	.p2align 4,,10
	.p2align 3
.L1486:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal10JsonParserItE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE
	movq	16(%r13), %r13
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	(%rax), %rcx
	movq	%rax, %rsi
	movq	-1(%rcx), %r10
	cmpw	$64, 11(%r10)
	jne	.L1488
	movl	11(%rcx), %edx
	notl	%edx
	andl	$1, %edx
.L1488:
	movabsq	$824633720832, %rcx
	movl	%edx, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	%rdi, -120(%rbp)
	movq	(%rsi), %rdx
	movq	-1(%rdx), %rdx
	movzwl	11(%rdx), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L1673
.L1489:
	movq	%rax, -112(%rbp)
	movq	-184(%rbp), %rax
	leaq	-144(%rbp), %rdi
	movq	%rdi, -168(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	-168(%rbp), %rdi
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS1_20AccessorInfoHandlingE@PLT
	testq	%rax, %rax
	je	.L1674
	movq	-152(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	cmpq	41096(%r14), %r12
	je	.L1487
	movq	%r12, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1487:
	addq	$24, %rbx
	cmpq	-176(%rbp), %rbx
	je	.L1492
.L1493:
	movq	(%r15), %r14
	movq	-160(%rbp), %rcx
	movq	41088(%r14), %rax
	movq	41096(%r14), %r12
	movq	%rax, -152(%rbp)
	movl	41104(%r14), %eax
	leal	1(%rax), %edx
	movl	%edx, 41104(%r14)
	movq	(%rcx), %r13
	addq	%rbx, %r13
	testb	$8, 8(%r13)
	je	.L1486
	movl	%eax, 41104(%r14)
	addq	$24, %rbx
	cmpq	-176(%rbp), %rbx
	jne	.L1493
.L1492:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1675
	movq	-184(%rbp), %rax
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1669:
	.cfi_restore_state
	cmpb	$2, %cl
	jg	.L1456
	je	.L1457
	xorl	%ecx, %ecx
	jmp	.L1455
	.p2align 4,,10
	.p2align 3
.L1456:
	subl	$3, %ecx
	cmpb	$1, %cl
	ja	.L1457
	xorl	%ecx, %ecx
	jmp	.L1455
	.p2align 4,,10
	.p2align 3
.L1670:
	movq	-1(%r10), %rax
	testb	$64, %bh
	je	.L1646
	movq	47(%rax), %rax
	testq	%rax, %rax
	je	.L1646
	movq	%rax, %rdx
	movl	$32, %edi
	notq	%rdx
	movl	%edx, %esi
	andl	$1, %esi
	je	.L1676
.L1462:
	movq	%rbx, %r10
	movl	%ebx, %ecx
	shrq	$30, %r10
	sarl	$3, %ecx
	andl	$15, %r10d
	andl	$2047, %ecx
	subl	%r10d, %ecx
	cmpl	%edi, %ecx
	jnb	.L1646
	testl	%ecx, %ecx
	leal	31(%rcx), %edi
	cmovns	%ecx, %edi
	sarl	$5, %edi
	andl	$1, %edx
	jne	.L1463
	cmpl	%edi, 11(%rax)
	jle	.L1463
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$27, %edx
	addl	%edx, %ecx
	andl	$31, %ecx
	subl	%edx, %ecx
	movl	$1, %edx
	sall	%cl, %edx
	testb	%sil, %sil
	je	.L1677
.L1467:
	sarq	$32, %rax
	testl	%eax, %edx
	sete	%cl
.L1469:
	movq	%r14, %rax
	notq	%rax
	movl	%eax, %edx
	andl	$1, %edx
	testb	%cl, %cl
	jne	.L1460
	testb	%dl, %dl
	je	.L1470
	sarq	$32, %r14
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%r14d, %xmm2
	movq	%xmm2, %rax
.L1471:
	movq	(%r9), %rdx
	andl	$16376, %ebx
	addq	$24, %r12
	movq	%rax, -1(%rbx,%rdx)
	cmpq	%r12, %r11
	jne	.L1485
	jmp	.L1678
	.p2align 4,,10
	.p2align 3
.L1646:
	movq	%r14, %rax
	notq	%rax
.L1460:
	movq	-168(%rbp), %rdi
	movq	(%rdi), %rdx
	movq	256(%rdx), %rdx
	testb	$1, %al
	je	.L1472
	movq	-200(%rbp), %rax
	sarq	$32, %r14
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r14d, %xmm0
	movq	%rdx, (%rax)
	leaq	1(%rax), %r14
	testb	$1, %dl
	je	.L1473
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L1473
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r11, -240(%rbp)
	movl	%r8d, -232(%rbp)
	movq	%r9, -224(%rbp)
	movq	%xmm0, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-240(%rbp), %r11
	movl	-232(%rbp), %r8d
	movq	-216(%rbp), %xmm0
	movq	-224(%rbp), %r9
.L1473:
	movq	-200(%rbp), %rax
	movq	%xmm0, 8(%rax)
	movq	(%r9), %r10
	addq	$16, %rax
	movq	%rax, -200(%rbp)
	leaq	-1(%r10), %rax
	jmp	.L1458
	.p2align 4,,10
	.p2align 3
.L1668:
	movzbl	8(%rax), %r10d
	movzbl	8(%rax), %eax
	addl	%esi, %eax
	sall	$3, %r10d
	sall	$3, %eax
	jmp	.L1454
	.p2align 4,,10
	.p2align 3
.L1673:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1650:
	movl	28(%r12), %eax
	leal	1(%rax), %ebx
	movl	%edi, %eax
	sarl	%eax
	addl	%eax, %edi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo32Ej@PLT
	movl	$4, %edx
	cmpl	$4, %eax
	cmovl	%edx, %eax
	leal	(%rax,%rax,8), %eax
	cmpl	%eax, %ebx
	jnb	.L1679
	movl	28(%r12), %esi
	movq	(%r15), %rdi
	xorl	%edx, %edx
	movl	$4, %r14d
	addl	$1, %esi
	call	_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE@PLT
	movq	%rax, -240(%rbp)
	movq	(%rax), %rax
	movq	%rax, -152(%rbp)
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testl	$262144, %eax
	jne	.L1348
	xorl	%r14d, %r14d
	testb	$24, %al
	sete	%r14b
	sall	$2, %r14d
.L1348:
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	jle	.L1642
	movq	-208(%rbp), %rdi
	subl	$1, %eax
	movq	%r15, %r8
	movq	-240(%rbp), %rcx
	movl	%r14d, %r15d
	leaq	1(%rdi,%rax), %rax
	leaq	(%rdi,%rdi,2), %rbx
	leaq	(%rax,%rax,2), %r13
	salq	$3, %rbx
	salq	$3, %r13
	movq	%r13, %r14
	movq	-160(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L1355:
	movq	0(%r13), %rax
	addq	%rbx, %rax
	testb	$8, 8(%rax)
	je	.L1349
	movq	16(%rax), %rdx
	movl	(%rax), %eax
	movq	(%rcx), %rdi
	leal	16(,%rax,8), %eax
	movq	(%rdx), %r12
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%r12, (%rsi)
	testl	%r15d, %r15d
	je	.L1349
	movq	%r12, %rax
	notq	%rax
	andl	$1, %eax
	cmpl	$4, %r15d
	je	.L1680
	testb	%al, %al
	jne	.L1349
.L1596:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1349
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1349
	movq	%r12, %rdx
	movq	%r8, -168(%rbp)
	movq	%rcx, -152(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-168(%rbp), %r8
	movq	-152(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L1349:
	addq	$24, %rbx
	cmpq	%rbx, %r14
	jne	.L1355
	movq	%r8, %r15
.L1642:
	movq	-216(%rbp), %r14
	movq	(%r15), %r9
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1657:
	movq	%rdi, %rdx
	notq	%rdx
	andl	$1, %edx
.L1398:
	testb	%dl, %dl
	jne	.L1437
	cmpb	$0, _ZN2v88internal17FLAG_track_fieldsE(%rip)
	jne	.L1414
.L1425:
	cmpl	$4, %ecx
	je	.L1415
	movl	$4, %edx
	cmpb	$3, %cl
	jg	.L1427
.L1415:
	movl	$4, %ebx
	testl	%ecx, %ecx
	jne	.L1498
.L1537:
	movl	$4, %ebx
.L1429:
	movq	(%r15), %rsi
	movq	%rdi, -144(%rbp)
	movl	%ebx, %edx
	leaq	-144(%rbp), %rdi
	movl	%r11d, -192(%rbp)
	call	_ZN2v88internal6Object11OptimalTypeEPNS0_7IsolateENS0_14RepresentationE@PLT
	movl	%r14d, %ecx
	movq	(%r15), %rdi
	movl	%ebx, %r8d
	movl	-168(%rbp), %edx
	shrl	$2, %ecx
	movq	%rax, %r9
	movq	%r13, %rsi
	andl	$1, %ecx
	call	_ZN2v88internal3Map15GeneralizeFieldEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS0_14RepresentationENS4_INS0_9FieldTypeEEE@PLT
	movl	-192(%rbp), %r11d
	jmp	.L1437
	.p2align 4,,10
	.p2align 3
.L1674:
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	-192(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, -184(%rbp)
	jmp	.L1445
	.p2align 4,,10
	.p2align 3
.L1399:
	testb	$1, %dil
	je	.L1437
	movq	-1(%rdi), %rdx
	cmpw	$65, 11(%rdx)
	sete	%dl
	jmp	.L1398
	.p2align 4,,10
	.p2align 3
.L1658:
	testb	$1, %dil
	je	.L1408
.L1413:
	movq	39(%rax), %rax
	movl	%r11d, -232(%rbp)
	movl	%ecx, -224(%rbp)
	movq	%rdx, -192(%rbp)
	movq	15(%rbx,%rax), %rdi
	leaq	-144(%rbp), %rbx
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	movq	-192(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rax, -144(%rbp)
	movq	(%rdx), %rsi
	call	_ZNK2v88internal9FieldType11NowContainsENS0_6ObjectE@PLT
	movq	-192(%rbp), %rdx
	movl	-224(%rbp), %ecx
	testb	%al, %al
	movl	-232(%rbp), %r11d
	jne	.L1437
	movq	(%rdx), %rax
	movq	(%r15), %rsi
	movl	%ecx, %edx
	movq	%rbx, %rdi
	movl	%r11d, -224(%rbp)
	movl	%ecx, -192(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal6Object11OptimalTypeEPNS0_7IsolateENS0_14RepresentationE@PLT
	movl	%r14d, %edx
	movq	(%r15), %rdi
	movq	%r13, %rsi
	movl	-192(%rbp), %ecx
	shrl	$2, %edx
	movq	%rax, %r9
	andl	$1, %edx
	movl	%ecx, %r8d
	movl	%edx, %ecx
	movl	-168(%rbp), %edx
	call	_ZN2v88internal3Map15GeneralizeFieldEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS0_14RepresentationENS4_INS0_9FieldTypeEEE@PLT
	movl	-224(%rbp), %r11d
	jmp	.L1437
	.p2align 4,,10
	.p2align 3
.L1472:
	movq	%rdx, -1(%r14)
	testq	%rdx, %rdx
	jne	.L1681
.L1475:
	movq	(%r9), %r10
	leaq	-1(%r10), %rax
	jmp	.L1458
	.p2align 4,,10
	.p2align 3
.L1652:
	movq	%r13, -192(%rbp)
	movq	-184(%rbp), %r13
	jmp	.L1384
	.p2align 4,,10
	.p2align 3
.L1659:
	testb	$1, %dil
	je	.L1411
.L1412:
	cmpb	$0, _ZN2v88internal24FLAG_track_double_fieldsE(%rip)
	jne	.L1419
.L1423:
	cmpb	$0, _ZN2v88internal26FLAG_track_computed_fieldsE(%rip)
	jne	.L1682
.L1421:
	cmpb	$0, _ZN2v88internal29FLAG_track_heap_object_fieldsE(%rip)
	je	.L1425
	cmpl	$3, %ecx
	je	.L1539
	testl	%ecx, %ecx
	je	.L1523
	movl	$3, %edx
.L1427:
	movl	$4, %ebx
	cmpb	%dl, %cl
	jg	.L1431
.L1648:
	testl	%ecx, %ecx
	je	.L1429
.L1498:
	cmpb	$0, _ZN2v88internal40FLAG_modify_field_representation_inplaceE(%rip)
	je	.L1430
	andl	$-3, %ecx
	cmpb	$1, %cl
	jne	.L1430
	cmpb	$4, %bl
	je	.L1429
	.p2align 4,,10
	.p2align 3
.L1430:
	movl	-168(%rbp), %edi
	testl	%edi, %edi
	je	.L1645
	movq	(%r15), %rbx
	movl	-168(%rbp), %edx
	leaq	-144(%rbp), %rdi
	movq	%rax, -144(%rbp)
	movq	%rbx, %rsi
	subl	$1, %edx
	call	_ZNK2v88internal3Map14FindFieldOwnerEPNS0_7IsolateEi@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	jne	.L1644
	movq	41088(%rbx), %rax
	movq	%rax, -192(%rbp)
	cmpq	41096(%rbx), %rax
	je	.L1683
.L1436:
	movq	-192(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	movq	(%r15), %r9
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1679:
	movl	32(%r12), %esi
	movq	(%r15), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal9HashTableINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	(%r15), %rdi
	movq	%rax, %r12
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	jle	.L1345
	movq	-208(%rbp), %rbx
	subl	$1, %eax
	movq	-160(%rbp), %r14
	movq	%r12, %rsi
	leaq	1(%rbx,%rax), %rax
	leaq	(%rbx,%rbx,2), %r13
	leaq	(%rax,%rax,2), %rbx
	salq	$3, %r13
	salq	$3, %rbx
	.p2align 4,,10
	.p2align 3
.L1346:
	movq	(%r14), %rdx
	addq	%r13, %rdx
	testb	$8, 8(%rdx)
	je	.L1344
	movl	(%rdx), %r10d
	movq	16(%rdx), %rcx
	movl	$192, %r9d
	xorl	%r8d, %r8d
	movl	%r10d, %edx
	call	_ZN2v88internal16NumberDictionary3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEENS4_INS0_8JSObjectEEENS0_15PropertyDetailsE@PLT
	movq	(%r15), %rdi
	movq	%rax, %rsi
.L1344:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1346
	movq	%rsi, %r12
.L1345:
	movq	-216(%rbp), %rsi
	movl	$12, %edx
	call	_ZN2v88internal3Map14AsElementsKindEPNS0_7IsolateENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	%r12, -240(%rbp)
	movq	(%r15), %r9
	movq	%rax, %r14
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1666:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1408:
	testb	%sil, %sil
	je	.L1415
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1672:
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r10, %rdi
	movq	%r11, -232(%rbp)
	movl	%r8d, -224(%rbp)
	addq	$24, %r12
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-232(%rbp), %r11
	movl	-224(%rbp), %r8d
	movq	-216(%rbp), %r9
	cmpq	%r12, %r11
	jne	.L1485
	jmp	.L1678
	.p2align 4,,10
	.p2align 3
.L1676:
	movslq	11(%rax), %rdi
	sall	$3, %edi
	jmp	.L1462
	.p2align 4,,10
	.p2align 3
.L1663:
	movl	%r9d, -244(%rbp)
	movl	%r9d, -232(%rbp)
	movq	%rsi, -224(%rbp)
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-244(%rbp), %r9d
	movl	-232(%rbp), %r11d
	movq	-224(%rbp), %rsi
	movq	-192(%rbp), %rdi
	movq	%rax, %rdx
	jmp	.L1365
.L1667:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1494
.L1414:
	testb	$1, %dil
	jne	.L1412
	cmpl	$1, %ecx
	je	.L1417
	movl	$1, %edx
	testl	%ecx, %ecx
	je	.L1411
.L1418:
	cmpl	$3, %ecx
	jne	.L1427
.L1509:
	cmpb	$0, _ZN2v88internal40FLAG_modify_field_representation_inplaceE(%rip)
	jne	.L1537
	jmp	.L1430
.L1681:
	testb	$1, %dl
	je	.L1475
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L1475
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r11, -232(%rbp)
	movl	%r8d, -224(%rbp)
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-232(%rbp), %r11
	movl	-224(%rbp), %r8d
	movq	-216(%rbp), %r9
	jmp	.L1475
.L1499:
	xorl	%edx, %edx
	cmpl	$3, %ecx
	jne	.L1427
.L1431:
	movl	%ecx, %ebx
	jmp	.L1648
	.p2align 4,,10
	.p2align 3
.L1671:
	testb	%al, %al
	jne	.L1483
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L1597
	movq	%r10, %rdi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r11, -240(%rbp)
	movl	%r8d, -232(%rbp)
	movq	%r9, -224(%rbp)
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-216(%rbp), %r10
	movq	-224(%rbp), %r9
	movl	-232(%rbp), %r8d
	movq	-240(%rbp), %r11
	jmp	.L1597
	.p2align 4,,10
	.p2align 3
.L1651:
	movzbl	7(%rcx), %eax
	movzbl	7(%rsi), %edx
	cmpb	%al, %dl
	jne	.L1359
	movl	15(%rcx), %eax
	movl	-176(%rbp), %r10d
	shrl	$10, %eax
	andl	$1023, %eax
	movl	%eax, -200(%rbp)
	testl	%r10d, %r10d
	jg	.L1358
	movl	-200(%rbp), %edx
	testl	%edx, %edx
	je	.L1540
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	jne	.L1540
	movl	$0, -152(%rbp)
	jmp	.L1524
	.p2align 4,,10
	.p2align 3
.L1655:
	movl	$3, -112(%rbp)
	jmp	.L1391
	.p2align 4,,10
	.p2align 3
.L1654:
	movq	-184(%rbp), %rax
	leaq	-144(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r8, -192(%rbp)
	movq	(%rax), %rax
	movq	%rax, -144(%rbp)
	movl	-168(%rbp), %eax
	leal	-1(%rax), %edx
	call	_ZNK2v88internal3Map14FindFieldOwnerEPNS0_7IsolateEi@PLT
	movq	41112(%rbx), %rdi
	movq	-192(%rbp), %r8
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1386
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-192(%rbp), %r8
	movq	%rax, %r13
.L1387:
	movq	(%r15), %rbx
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1470:
	movq	7(%r14), %rax
	jmp	.L1471
.L1665:
	movq	%r13, %rdi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1339
.L1680:
	testb	%al, %al
	jne	.L1349
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L1596
	movq	%r12, %rdx
	movq	%r8, -200(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%rsi, -168(%rbp)
	movq	%rdi, -152(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-152(%rbp), %rdi
	movq	-168(%rbp), %rsi
	movq	-192(%rbp), %rcx
	movq	-200(%rbp), %r8
	jmp	.L1596
.L1539:
	testl	%ecx, %ecx
	jne	.L1430
.L1523:
	movl	$3, %ebx
	jmp	.L1429
.L1386:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L1684
.L1388:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L1387
.L1393:
	movq	-1(%rax), %rax
	cmpw	$95, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	%eax, %eax
	movl	%eax, -112(%rbp)
	jmp	.L1391
.L1441:
	movq	41088(%r9), %rax
	movq	%rax, -192(%rbp)
	cmpq	41096(%r9), %rax
	je	.L1685
.L1443:
	movq	-192(%rbp), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r9)
	movq	%r12, (%rsi)
	movq	(%r15), %r9
	jmp	.L1357
.L1417:
	testl	%ecx, %ecx
	je	.L1411
	movl	$1, %ebx
	jmp	.L1498
.L1684:
	movq	%rbx, %rdi
	movq	%r8, -200(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L1388
	.p2align 4,,10
	.p2align 3
.L1380:
	movq	41088(%rdx), %r9
	cmpq	41096(%rdx), %r9
	je	.L1686
.L1382:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r9)
	jmp	.L1381
.L1419:
	movq	-1(%rdi), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1423
	cmpl	$2, %ecx
	je	.L1541
	cmpb	$1, %cl
	jg	.L1536
.L1541:
	testl	%ecx, %ecx
	je	.L1430
	movl	$2, %ebx
	jmp	.L1498
	.p2align 4,,10
	.p2align 3
.L1677:
	leal	0(,%rdi,4), %ecx
	movslq	%ecx, %rcx
	movl	15(%rax,%rcx), %eax
	testl	%eax, %edx
	sete	%cl
	jmp	.L1469
.L1682:
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rdi, -37512(%rdx)
	jne	.L1421
	testb	%cl, %cl
	jne	.L1499
	xorl	%ebx, %ebx
	jmp	.L1648
.L1411:
	movl	$1, %ebx
	jmp	.L1429
.L1686:
	movq	%rdx, %rdi
	movq	%rsi, -224(%rbp)
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	-192(%rbp), %rdx
	movq	%rax, %r9
	jmp	.L1382
.L1463:
	cmpl	$31, %ecx
	jg	.L1465
	testb	%sil, %sil
	je	.L1465
	movl	$1, %edx
	sall	%cl, %edx
	jmp	.L1467
.L1374:
	movq	41088(%rdi), %rbx
	cmpq	41096(%rdi), %rbx
	je	.L1687
.L1376:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%rdi)
	movq	%rsi, (%rbx)
	jmp	.L1375
.L1683:
	movq	%rbx, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, -192(%rbp)
	jmp	.L1436
.L1465:
	leaq	.LC7(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1685:
	movq	%r9, %rdi
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %r9
	movq	%rax, -192(%rbp)
	jmp	.L1443
.L1687:
	movq	%rsi, -224(%rbp)
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	-192(%rbp), %rdi
	movq	%rax, %rbx
	jmp	.L1376
.L1540:
	movq	%r14, -192(%rbp)
	movl	$0, -152(%rbp)
	jmp	.L1357
.L1656:
	movq	%r9, %r8
	jmp	.L1514
.L1536:
	movl	$2, %edx
	jmp	.L1418
.L1675:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19872:
	.size	_ZN2v88internal10JsonParserItE15BuildJsonObjectERKNS2_16JsonContinuationERKSt6vectorINS0_12JsonPropertyESaIS7_EENS0_6HandleINS0_3MapEEE, .-_ZN2v88internal10JsonParserItE15BuildJsonObjectERKNS2_16JsonContinuationERKSt6vectorINS0_12JsonPropertyESaIS7_EENS0_6HandleINS0_3MapEEE
	.section	.text._ZNSt6vectorIN2v88internal10JsonParserItE16JsonContinuationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal10JsonParserItE16JsonContinuationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal10JsonParserItE16JsonContinuationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal10JsonParserItE16JsonContinuationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal10JsonParserItE16JsonContinuationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB21695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r14
	movq	(%rdi), %r12
	movabsq	$-3689348814741910323, %rdi
	movq	%r14, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movabsq	$230584300921369395, %rdi
	cmpq	%rdi, %rax
	je	.L1715
	movq	%rsi, %r8
	subq	%r12, %r8
	testq	%rax, %rax
	je	.L1703
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L1716
.L1690:
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	%rax, %rbx
	leaq	(%rax,%r15), %rax
	movq	-88(%rbp), %rdx
	movq	%rax, -64(%rbp)
	leaq	40(%rbx), %rax
	movq	%rax, -56(%rbp)
.L1702:
	movq	16(%rdx), %rdi
	leaq	(%rbx,%r8), %rax
	movdqu	(%rdx), %xmm3
	movq	$0, (%rdx)
	movq	%rdi, 16(%rax)
	movl	32(%rdx), %edi
	movq	24(%rdx), %rdx
	movups	%xmm3, (%rax)
	movl	%edi, 32(%rax)
	movq	%rdx, 24(%rax)
	cmpq	%r12, %rsi
	je	.L1692
	movq	%r12, %rax
	movq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L1693:
	movdqu	(%rax), %xmm1
	addq	$40, %rax
	addq	$40, %rdx
	movups	%xmm1, -40(%rdx)
	movq	-24(%rax), %rdi
	movq	%rdi, -24(%rdx)
	movl	-16(%rax), %edi
	movq	$0, -40(%rax)
	movl	%edi, -16(%rdx)
	movl	-12(%rax), %edi
	movl	%edi, -12(%rdx)
	movl	-8(%rax), %edi
	movl	%edi, -8(%rdx)
	cmpq	%rax, %rsi
	jne	.L1693
	leaq	-40(%rsi), %rax
	subq	%r12, %rax
	shrq	$3, %rax
	leaq	80(%rbx,%rax,8), %rax
	movq	%rax, -56(%rbp)
.L1692:
	movq	-56(%rbp), %rdx
	movq	%rsi, %rax
	cmpq	%r14, %rsi
	je	.L1699
	.p2align 4,,10
	.p2align 3
.L1698:
	movq	16(%rax), %rdi
	movdqu	(%rax), %xmm2
	addq	$40, %rax
	addq	$40, %rdx
	movq	$0, -40(%rax)
	movq	%rdi, -24(%rdx)
	movl	-16(%rax), %edi
	movups	%xmm2, -40(%rdx)
	movl	%edi, -16(%rdx)
	movl	-12(%rax), %edi
	movl	%edi, -12(%rdx)
	movl	-8(%rax), %edi
	movl	%edi, -8(%rdx)
	cmpq	%rax, %r14
	jne	.L1698
	movq	%r14, %rax
	movq	-56(%rbp), %rcx
	subq	%rsi, %rax
	subq	$40, %rax
	shrq	$3, %rax
	leaq	40(%rcx,%rax,8), %rax
	movq	%rax, -56(%rbp)
.L1699:
	movq	%r12, %r15
	cmpq	%r14, %r12
	je	.L1701
	.p2align 4,,10
	.p2align 3
.L1695:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1700
	movq	8(%r15), %rsi
	movq	16(%r15), %rdx
	subl	$1, 41104(%rdi)
	movq	%rsi, 41088(%rdi)
	cmpq	41096(%rdi), %rdx
	je	.L1700
	movq	%rdx, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1700:
	addq	$40, %r15
	cmpq	%r14, %r15
	jne	.L1695
.L1701:
	testq	%r12, %r12
	je	.L1697
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1697:
	movq	-64(%rbp), %rax
	movq	%rbx, %xmm0
	movhps	-56(%rbp), %xmm0
	movq	%rax, 16(%r13)
	movups	%xmm0, 0(%r13)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1716:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L1691
	movq	$40, -56(%rbp)
	xorl	%ebx, %ebx
	movq	$0, -64(%rbp)
	jmp	.L1702
	.p2align 4,,10
	.p2align 3
.L1703:
	movl	$40, %r15d
	jmp	.L1690
.L1691:
	cmpq	%rdi, %r9
	cmovbe	%r9, %rdi
	imulq	$40, %rdi, %r15
	jmp	.L1690
.L1715:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21695:
	.size	_ZNSt6vectorIN2v88internal10JsonParserItE16JsonContinuationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal10JsonParserItE16JsonContinuationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE19AdvanceToNonDecimalEvEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE19AdvanceToNonDecimalEvEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE19AdvanceToNonDecimalEvEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE19AdvanceToNonDecimalEvEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE19AdvanceToNonDecimalEvEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag:
.LFB21843:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	subq	%rdi, %rdx
	movq	%rdx, %rax
	sarq	$2, %rax
	testq	%rax, %rax
	jle	.L1718
	leaq	(%rdi,%rax,4), %rdx
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L1742:
	movzbl	1(%rdi), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L1738
	movzbl	2(%rdi), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L1739
	movzbl	3(%rdi), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L1740
	addq	$4, %rdi
	cmpq	%rdi, %rdx
	je	.L1741
.L1723:
	movzbl	(%rdi), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L1742
	movq	%rdi, %rax
.L1717:
	ret
	.p2align 4,,10
	.p2align 3
.L1741:
	movq	%rsi, %rdx
	subq	%rdi, %rdx
.L1718:
	cmpq	$2, %rdx
	je	.L1724
	cmpq	$3, %rdx
	je	.L1725
	cmpq	$1, %rdx
	je	.L1726
	movq	%rsi, %rax
	ret
.L1725:
	movzbl	(%rdi), %edx
	movq	%rdi, %rax
	subl	$48, %edx
	cmpl	$9, %edx
	ja	.L1717
	addq	$1, %rdi
.L1724:
	movzbl	(%rdi), %edx
	movq	%rdi, %rax
	subl	$48, %edx
	cmpl	$9, %edx
	ja	.L1717
	addq	$1, %rdi
.L1726:
	movzbl	(%rdi), %eax
	subl	$48, %eax
	cmpl	$10, %eax
	movq	%rdi, %rax
	cmovb	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1738:
	leaq	1(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1739:
	leaq	2(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1740:
	leaq	3(%rdi), %rax
	ret
	.cfi_endproc
.LFE21843:
	.size	_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE19AdvanceToNonDecimalEvEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE19AdvanceToNonDecimalEvEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag
	.section	.text._ZN2v88internal10JsonParserIhE19AdvanceToNonDecimalEv,"axG",@progbits,_ZN2v88internal10JsonParserIhE19AdvanceToNonDecimalEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE19AdvanceToNonDecimalEv
	.type	_ZN2v88internal10JsonParserIhE19AdvanceToNonDecimalEv, @function
_ZN2v88internal10JsonParserIhE19AdvanceToNonDecimalEv:
.LFB19761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	56(%rdi), %rsi
	movq	48(%rdi), %rdi
	call	_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE19AdvanceToNonDecimalEvEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag
	movq	%rax, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19761:
	.size	_ZN2v88internal10JsonParserIhE19AdvanceToNonDecimalEv, .-_ZN2v88internal10JsonParserIhE19AdvanceToNonDecimalEv
	.section	.text._ZN2v88internal10JsonParserIhE15ParseJsonNumberEv,"axG",@progbits,_ZN2v88internal10JsonParserIhE15ParseJsonNumberEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE15ParseJsonNumberEv
	.type	_ZN2v88internal10JsonParserIhE15ParseJsonNumberEv, @function
_ZN2v88internal10JsonParserIhE15ParseJsonNumberEv:
.LFB19778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	$1, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	48(%rdi), %r13
	movzbl	0(%r13), %edx
	movq	%r13, %r12
	cmpl	$45, %edx
	je	.L1817
.L1746:
	cmpl	$48, %edx
	jne	.L1747
	movq	56(%rbx), %rcx
	leaq	1(%r12), %rsi
	movq	%rsi, 48(%rbx)
	cmpq	%rcx, %rsi
	je	.L1748
	movzbl	1(%r12), %r8d
	leaq	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE(%rip), %rdi
	movzbl	%r8b, %eax
	movl	%r8d, %edx
	testb	$16, (%rdi,%rax)
	je	.L1749
	subl	$48, %r8d
	cmpl	$9, %r8d
	jbe	.L1818
.L1751:
	cmpb	$46, %dl
	je	.L1819
.L1773:
	orl	$32, %edx
	cmpb	$101, %dl
	jne	.L1779
	leaq	1(%rsi), %rax
	movq	%rax, 48(%rbx)
	cmpq	%rcx, %rax
	je	.L1784
	movzbl	1(%rsi), %eax
	leal	-43(%rax), %edx
	andl	$253, %edx
	jne	.L1820
	leaq	2(%rsi), %rax
	movq	%rax, 48(%rbx)
	cmpq	%rcx, %rax
	je	.L1784
	movzbl	2(%rsi), %esi
.L1783:
	leal	-48(%rsi), %eax
	cmpl	$9, %eax
	ja	.L1781
	movq	%rbx, %rdi
	call	_ZN2v88internal10JsonParserIhE19AdvanceToNonDecimalEv
	movq	48(%rbx), %rsi
.L1779:
	movsd	.LC9(%rip), %xmm0
	subq	%r13, %rsi
	movq	%r13, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal14StringToDoubleENS0_6VectorIKhEEid@PLT
	movq	(%rbx), %rdi
	addq	$16, %rsp
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L1748:
	.cfi_restore_state
	cmpl	$1, %r14d
	je	.L1816
.L1753:
	movq	%rcx, %rsi
	jmp	.L1779
	.p2align 4,,10
	.p2align 3
.L1821:
	cmpq	%r12, 56(%rbx)
	je	.L1784
	movzbl	(%r12), %esi
	.p2align 4,,10
	.p2align 3
.L1781:
	movq	%rbx, %rdi
	call	_ZN2v88internal10JsonParserIhE25ReportUnexpectedCharacterEi
.L1816:
	movq	(%rbx), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1786
.L1807:
	xorl	%esi, %esi
.L1808:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1804:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1791:
	.cfi_restore_state
	movl	$-1, %r14d
	.p2align 4,,10
	.p2align 3
.L1747:
	movq	%rbx, %rdi
	call	_ZN2v88internal10JsonParserIhE19AdvanceToNonDecimalEv
	movq	48(%rbx), %rsi
	cmpq	%r12, %rsi
	je	.L1821
	movq	56(%rbx), %rcx
	movq	%rsi, %rax
	subq	%r12, %rax
	cmpq	%rcx, %rsi
	je	.L1766
	cmpq	$9, %rax
	jle	.L1822
.L1768:
	movzbl	(%rsi), %edx
	cmpb	$46, %dl
	jne	.L1773
.L1819:
	leaq	1(%rsi), %rax
	movq	%rax, 48(%rbx)
	cmpq	%rcx, %rax
	je	.L1784
	movzbl	1(%rsi), %esi
	leal	-48(%rsi), %eax
	cmpl	$9, %eax
	ja	.L1781
	movq	%rbx, %rdi
	call	_ZN2v88internal10JsonParserIhE19AdvanceToNonDecimalEv
	movq	48(%rbx), %rsi
	movq	56(%rbx), %rcx
	cmpq	%rcx, %rsi
	je	.L1779
	movzbl	(%rsi), %edx
	jmp	.L1773
	.p2align 4,,10
	.p2align 3
.L1817:
	leaq	1(%r13), %r12
	movq	%r12, 48(%rdi)
	cmpq	56(%rdi), %r12
	je	.L1791
	movzbl	1(%r13), %edx
	movl	$-1, %r14d
	jmp	.L1746
	.p2align 4,,10
	.p2align 3
.L1786:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1809
.L1788:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	$0, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1820:
	.cfi_restore_state
	movzbl	%al, %esi
	jmp	.L1783
	.p2align 4,,10
	.p2align 3
.L1749:
	cmpl	$1, %r14d
	jne	.L1751
	jmp	.L1816
	.p2align 4,,10
	.p2align 3
.L1822:
	movzbl	(%rsi), %eax
	leaq	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE(%rip), %rdx
	testb	$16, (%rdx,%rax)
	jne	.L1768
.L1796:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1769:
	movzbl	(%r12), %edx
	leal	(%rax,%rax,4), %ecx
	addq	$1, %r12
	leal	-48(%rdx,%rcx,2), %eax
	cmpq	%r12, %rsi
	jne	.L1769
	imull	%r14d, %eax
	movq	(%rbx), %rbx
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	jne	.L1808
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1823
.L1772:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1804
	.p2align 4,,10
	.p2align 3
.L1784:
	movl	$-1, %esi
	jmp	.L1781
.L1818:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal10JsonParserIhE21ReportUnexpectedTokenENS0_9JsonTokenE
	movq	(%rbx), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L1807
	movq	41088(%rbx), %rax
	cmpq	%rax, 41096(%rbx)
	jne	.L1788
	.p2align 4,,10
	.p2align 3
.L1809:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1788
	.p2align 4,,10
	.p2align 3
.L1766:
	cmpq	$9, %rax
	jle	.L1796
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1823:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L1772
	.cfi_endproc
.LFE19778:
	.size	_ZN2v88internal10JsonParserIhE15ParseJsonNumberEv, .-_ZN2v88internal10JsonParserIhE15ParseJsonNumberEv
	.section	.text._ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE14SkipWhitespaceEvEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE14SkipWhitespaceEvEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE14SkipWhitespaceEvEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE14SkipWhitespaceEvEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE14SkipWhitespaceEvEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag:
.LFB21847:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movq	%rsi, %rdi
	subq	%rax, %rdi
	movq	%rdi, %rcx
	sarq	$2, %rcx
	testq	%rcx, %rcx
	jle	.L1825
	leaq	(%rax,%rcx,4), %r8
	leaq	_ZN2v88internal12_GLOBAL__N_1L20one_char_json_tokensE(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L1833:
	movzbl	(%rax), %edi
	movzbl	(%rcx,%rdi), %edi
	cmpb	$9, %dil
	je	.L1826
.L1852:
	movb	%dil, 16(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L1826:
	movzbl	1(%rax), %edi
	movzbl	(%rcx,%rdi), %edi
	cmpb	$9, %dil
	je	.L1853
	movb	%dil, 16(%rdx)
	addq	$1, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1853:
	movzbl	2(%rax), %edi
	movzbl	(%rcx,%rdi), %edi
	cmpb	$9, %dil
	je	.L1854
	movb	%dil, 16(%rdx)
	addq	$2, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1854:
	movzbl	3(%rax), %edi
	movzbl	(%rcx,%rdi), %edi
	cmpb	$9, %dil
	je	.L1855
	movb	%dil, 16(%rdx)
	addq	$3, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1855:
	addq	$4, %rax
	cmpq	%r8, %rax
	jne	.L1833
	movq	%rsi, %rdi
	subq	%rax, %rdi
.L1825:
	leaq	_ZN2v88internal12_GLOBAL__N_1L20one_char_json_tokensE(%rip), %rcx
	cmpq	$2, %rdi
	je	.L1828
	cmpq	$3, %rdi
	je	.L1829
	leaq	_ZN2v88internal12_GLOBAL__N_1L20one_char_json_tokensE(%rip), %rcx
	cmpq	$1, %rdi
	je	.L1830
.L1841:
	movq	%rsi, %rax
	ret
.L1829:
	movzbl	(%rax), %edi
	leaq	_ZN2v88internal12_GLOBAL__N_1L20one_char_json_tokensE(%rip), %rcx
	movzbl	(%rcx,%rdi), %edi
	cmpb	$9, %dil
	jne	.L1852
	addq	$1, %rax
.L1828:
	movzbl	(%rax), %edi
	movzbl	(%rcx,%rdi), %edi
	cmpb	$9, %dil
	jne	.L1852
	addq	$1, %rax
.L1830:
	movzbl	(%rax), %edi
	movzbl	(%rcx,%rdi), %ecx
	cmpb	$9, %cl
	je	.L1841
	movb	%cl, 16(%rdx)
	ret
	.cfi_endproc
.LFE21847:
	.size	_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE14SkipWhitespaceEvEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE14SkipWhitespaceEvEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag
	.section	.text._ZN2v88internal10JsonParserIhE14SkipWhitespaceEv,"axG",@progbits,_ZN2v88internal10JsonParserIhE14SkipWhitespaceEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE14SkipWhitespaceEv
	.type	_ZN2v88internal10JsonParserIhE14SkipWhitespaceEv, @function
_ZN2v88internal10JsonParserIhE14SkipWhitespaceEv:
.LFB19770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rbx, %rdx
	subq	$8, %rsp
	movb	$13, 16(%rdi)
	movq	56(%rdi), %rsi
	movq	48(%rdi), %rdi
	call	_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE14SkipWhitespaceEvEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag
	movq	%rax, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19770:
	.size	_ZN2v88internal10JsonParserIhE14SkipWhitespaceEv, .-_ZN2v88internal10JsonParserIhE14SkipWhitespaceEv
	.section	.text._ZN2v88internal10JsonParserIhE10ExpectNextENS0_9JsonTokenE,"axG",@progbits,_ZN2v88internal10JsonParserIhE10ExpectNextENS0_9JsonTokenE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE10ExpectNextENS0_9JsonTokenE
	.type	_ZN2v88internal10JsonParserIhE10ExpectNextENS0_9JsonTokenE, @function
_ZN2v88internal10JsonParserIhE10ExpectNextENS0_9JsonTokenE:
.LFB19768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	call	_ZN2v88internal10JsonParserIhE14SkipWhitespaceEv
	movzbl	16(%r12), %esi
	cmpb	%sil, %bl
	jne	.L1859
	popq	%rbx
	addq	$1, 48(%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1859:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10JsonParserIhE21ReportUnexpectedTokenENS0_9JsonTokenE
	.cfi_endproc
.LFE19768:
	.size	_ZN2v88internal10JsonParserIhE10ExpectNextENS0_9JsonTokenE, .-_ZN2v88internal10JsonParserIhE10ExpectNextENS0_9JsonTokenE
	.section	.text._ZN2v88internal10JsonParserIhE5CheckENS0_9JsonTokenE,"axG",@progbits,_ZN2v88internal10JsonParserIhE5CheckENS0_9JsonTokenE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE5CheckENS0_9JsonTokenE
	.type	_ZN2v88internal10JsonParserIhE5CheckENS0_9JsonTokenE, @function
_ZN2v88internal10JsonParserIhE5CheckENS0_9JsonTokenE:
.LFB19769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v88internal10JsonParserIhE14SkipWhitespaceEv
	xorl	%eax, %eax
	cmpb	%r12b, 16(%rbx)
	jne	.L1863
	addq	$1, 48(%rbx)
	movl	$1, %eax
.L1863:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19769:
	.size	_ZN2v88internal10JsonParserIhE5CheckENS0_9JsonTokenE, .-_ZN2v88internal10JsonParserIhE5CheckENS0_9JsonTokenE
	.section	.text._ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE14ScanJsonStringEbEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE14ScanJsonStringEbEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE14ScanJsonStringEbEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE14ScanJsonStringEbEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE14ScanJsonStringEbEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag:
.LFB21851:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	subq	%rdi, %rdx
	movq	%rdx, %rax
	sarq	$2, %rax
	testq	%rax, %rax
	jle	.L1869
	leaq	(%rdi,%rax,4), %rcx
	leaq	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE(%rip), %rax
	jmp	.L1874
	.p2align 4,,10
	.p2align 3
.L1893:
	movzbl	1(%rdi), %edx
	testb	$8, (%rax,%rdx)
	jne	.L1889
	movzbl	2(%rdi), %edx
	testb	$8, (%rax,%rdx)
	jne	.L1890
	movzbl	3(%rdi), %edx
	testb	$8, (%rax,%rdx)
	jne	.L1891
	addq	$4, %rdi
	cmpq	%rdi, %rcx
	je	.L1892
.L1874:
	movzbl	(%rdi), %edx
	testb	$8, (%rax,%rdx)
	je	.L1893
	movq	%rdi, %r8
.L1868:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1892:
	movq	%rsi, %rdx
	subq	%rdi, %rdx
.L1869:
	leaq	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE(%rip), %rax
	cmpq	$2, %rdx
	je	.L1875
	cmpq	$3, %rdx
	je	.L1876
	leaq	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE(%rip), %rax
	movq	%rsi, %r8
	cmpq	$1, %rdx
	jne	.L1868
.L1877:
	movzbl	(%rdi), %edx
	testb	$8, (%rax,%rdx)
	cmove	%rsi, %rdi
	movq	%rdi, %r8
	movq	%r8, %rax
	ret
.L1876:
	movzbl	(%rdi), %edx
	leaq	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE(%rip), %rax
	movq	%rdi, %r8
	testb	$8, (%rax,%rdx)
	jne	.L1868
	addq	$1, %rdi
.L1875:
	movzbl	(%rdi), %edx
	movq	%rdi, %r8
	testb	$8, (%rax,%rdx)
	jne	.L1868
	addq	$1, %rdi
	jmp	.L1877
	.p2align 4,,10
	.p2align 3
.L1889:
	leaq	1(%rdi), %r8
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1890:
	leaq	2(%rdi), %r8
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1891:
	leaq	3(%rdi), %r8
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE21851:
	.size	_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE14ScanJsonStringEbEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE14ScanJsonStringEbEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag
	.section	.text._ZN2v88internal10JsonParserIhE14ScanJsonStringEb,"axG",@progbits,_ZN2v88internal10JsonParserIhE14ScanJsonStringEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE14ScanJsonStringEb
	.type	_ZN2v88internal10JsonParserIhE14ScanJsonStringEb, @function
_ZN2v88internal10JsonParserIhE14ScanJsonStringEb:
.LFB19772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE(%rip), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-72(%rbp), %rbx
	subq	$72, %rsp
	movq	48(%rdi), %rdi
	movq	56(%r15), %r14
	movl	%esi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %rax
	subq	64(%r15), %rax
	movb	$0, -97(%rbp)
	movq	%rax, -112(%rbp)
	movl	%eax, %r12d
	movl	$0, -72(%rbp)
.L1907:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE14ScanJsonStringEbEUlhE_EEET_SB_SB_T0_St26random_access_iterator_tag
	movq	%rax, 48(%r15)
	cmpq	%r14, %rax
	je	.L1900
	movzbl	(%rax), %esi
	cmpb	$34, %sil
	je	.L1911
	cmpb	$92, %sil
	jne	.L1899
	leaq	1(%rax), %rdx
	movq	%rdx, 48(%r15)
	cmpq	%r14, %rdx
	je	.L1900
	movzbl	1(%rax), %eax
	movq	%rax, %rsi
	movzbl	0(%r13,%rax), %eax
	andl	$7, %eax
	cmpb	$7, %al
	jne	.L1912
	movq	%r15, %rdi
	call	_ZN2v88internal10JsonParserIhE20ScanUnicodeCharacterEv
	movl	%eax, %esi
	cmpl	$-1, %eax
	jne	.L1904
	movq	48(%r15), %rax
	cmpq	56(%r15), %rax
	je	.L1899
	movzbl	(%rax), %esi
	jmp	.L1899
	.p2align 4,,10
	.p2align 3
.L1912:
	testb	%al, %al
	jne	.L1913
.L1899:
	movq	%r15, %rdi
	call	_ZN2v88internal10JsonParserIhE25ReportUnexpectedCharacterEi
.L1896:
	andb	$-16, -60(%rbp)
	movq	$0, -68(%rbp)
.L1898:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	-68(%rbp), %rax
	movl	-60(%rbp), %edx
	jne	.L1914
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1904:
	.cfi_restore_state
	orl	%eax, -72(%rbp)
	xorl	%eax, %eax
	cmpl	$65536, %esi
	movq	48(%r15), %rdx
	setl	%al
	movq	56(%r15), %r14
	leal	4(%r12,%rax), %r12d
.L1903:
	leaq	1(%rdx), %rdi
	movb	$1, -97(%rbp)
	movq	%rdi, 48(%r15)
	jmp	.L1907
	.p2align 4,,10
	.p2align 3
.L1913:
	addl	$1, %r12d
	jmp	.L1903
	.p2align 4,,10
	.p2align 3
.L1900:
	movl	$-1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal10JsonParserIhE25ReportUnexpectedCharacterEi
	jmp	.L1896
	.p2align 4,,10
	.p2align 3
.L1911:
	movq	%rax, %rdx
	addq	$1, %rax
	subq	64(%r15), %rdx
	movzbl	-104(%rbp), %esi
	subl	%r12d, %edx
	movq	%rax, 48(%r15)
	cmpl	$255, -72(%rbp)
	movl	-112(%rbp), %eax
	setg	%cl
	cmpl	$10, %edx
	movzbl	-97(%rbp), %r15d
	movl	%edx, -64(%rbp)
	movl	%eax, -68(%rbp)
	setle	%al
	sall	$2, %r15d
	orl	%eax, %esi
	leal	(%rsi,%rsi), %eax
	orl	%ecx, %eax
	orl	%eax, %r15d
	movzbl	-60(%rbp), %eax
	andl	$15, %r15d
	andl	$-16, %eax
	orl	%eax, %r15d
	movb	%r15b, -60(%rbp)
	jmp	.L1898
.L1914:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19772:
	.size	_ZN2v88internal10JsonParserIhE14ScanJsonStringEb, .-_ZN2v88internal10JsonParserIhE14ScanJsonStringEb
	.section	.text._ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE19ScanJsonPropertyKeyEPNS8_16JsonContinuationEEUlhE_EEET_SD_SD_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE19ScanJsonPropertyKeyEPNS8_16JsonContinuationEEUlhE_EEET_SD_SD_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE19ScanJsonPropertyKeyEPNS8_16JsonContinuationEEUlhE_EEET_SD_SD_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE19ScanJsonPropertyKeyEPNS8_16JsonContinuationEEUlhE_EEET_SD_SD_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE19ScanJsonPropertyKeyEPNS8_16JsonContinuationEEUlhE_EEET_SD_SD_T0_St26random_access_iterator_tag:
.LFB21855:
	.cfi_startproc
	endbr64
	movq	%rsi, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subq	%rdi, %rcx
	movq	%rcx, %rax
	sarq	$2, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	testq	%rax, %rax
	jle	.L1916
	leaq	(%rdi,%rax,4), %r9
	movq	%rdi, %r8
	movl	$429496729, %r10d
.L1918:
	movzbl	(%r8), %eax
	leal	-48(%rax), %ecx
	cmpl	$9, %ecx
	ja	.L1915
	subl	$45, %eax
	movl	%r10d, %r11d
	movl	(%rdx), %edi
	sarl	$3, %eax
	subl	%eax, %r11d
	cmpl	%r11d, %edi
	ja	.L1915
	leal	(%rdi,%rdi,4), %eax
	leaq	1(%r8), %r11
	leal	(%rcx,%rax,2), %ecx
	movl	%ecx, (%rdx)
	movzbl	1(%r8), %eax
	leal	-48(%rax), %edi
	cmpl	$9, %edi
	ja	.L1927
	subl	$45, %eax
	movl	%r10d, %ebx
	sarl	$3, %eax
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	ja	.L1927
	leal	(%rcx,%rcx,4), %eax
	leaq	2(%r8), %r11
	leal	(%rdi,%rax,2), %ecx
	movl	%ecx, (%rdx)
	movzbl	2(%r8), %eax
	leal	-48(%rax), %edi
	cmpl	$9, %edi
	ja	.L1927
	subl	$45, %eax
	movl	%r10d, %ebx
	sarl	$3, %eax
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	ja	.L1927
	leal	(%rcx,%rcx,4), %eax
	leaq	3(%r8), %r11
	leal	(%rdi,%rax,2), %ecx
	movl	%ecx, (%rdx)
	movzbl	3(%r8), %eax
	leal	-48(%rax), %edi
	cmpl	$9, %edi
	ja	.L1927
	subl	$45, %eax
	movl	%r10d, %ebx
	sarl	$3, %eax
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	ja	.L1927
	leal	(%rcx,%rcx,4), %eax
	addq	$4, %r8
	leal	(%rdi,%rax,2), %eax
	movl	%eax, (%rdx)
	cmpq	%r8, %r9
	jne	.L1918
	movq	%rsi, %rcx
	movq	%r9, %rdi
	subq	%r9, %rcx
	.p2align 4,,10
	.p2align 3
.L1916:
	cmpq	$2, %rcx
	je	.L1919
	cmpq	$3, %rcx
	je	.L1920
	cmpq	$1, %rcx
	je	.L1921
.L1942:
	movq	%rsi, %r8
.L1915:
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1927:
	.cfi_restore_state
	movq	%r11, %r8
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%r8, %rax
	ret
.L1920:
	.cfi_restore_state
	movzbl	(%rdi), %eax
	movq	%rdi, %r8
	leal	-48(%rax), %ecx
	cmpl	$9, %ecx
	ja	.L1915
	subl	$45, %eax
	movl	$429496729, %r8d
	movl	(%rdx), %r9d
	sarl	$3, %eax
	subl	%eax, %r8d
	movl	%r8d, %eax
	movq	%rdi, %r8
	cmpl	%eax, %r9d
	ja	.L1915
	leal	(%r9,%r9,4), %eax
	addq	$1, %rdi
	leal	(%rcx,%rax,2), %eax
	movl	%eax, (%rdx)
.L1919:
	movzbl	(%rdi), %eax
	movq	%rdi, %r8
	leal	-48(%rax), %ecx
	cmpl	$9, %ecx
	ja	.L1915
	subl	$45, %eax
	movl	$429496729, %r8d
	movl	(%rdx), %r9d
	sarl	$3, %eax
	subl	%eax, %r8d
	movl	%r8d, %eax
	movq	%rdi, %r8
	cmpl	%eax, %r9d
	ja	.L1915
	leal	(%r9,%r9,4), %eax
	addq	$1, %rdi
	leal	(%rcx,%rax,2), %eax
	movl	%eax, (%rdx)
.L1921:
	movzbl	(%rdi), %eax
	movq	%rdi, %r8
	leal	-48(%rax), %r9d
	cmpl	$9, %r9d
	ja	.L1915
	subl	$45, %eax
	movl	$429496729, %ecx
	movl	(%rdx), %edi
	sarl	$3, %eax
	subl	%eax, %ecx
	cmpl	%ecx, %edi
	ja	.L1915
	leal	(%rdi,%rdi,4), %eax
	leal	(%r9,%rax,2), %eax
	movl	%eax, (%rdx)
	jmp	.L1942
	.cfi_endproc
.LFE21855:
	.size	_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE19ScanJsonPropertyKeyEPNS8_16JsonContinuationEEUlhE_EEET_SD_SD_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE19ScanJsonPropertyKeyEPNS8_16JsonContinuationEEUlhE_EEET_SD_SD_T0_St26random_access_iterator_tag
	.section	.text._ZN2v88internal10JsonParserIhE19ScanJsonPropertyKeyEPNS2_16JsonContinuationE,"axG",@progbits,_ZN2v88internal10JsonParserIhE19ScanJsonPropertyKeyEPNS2_16JsonContinuationE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE19ScanJsonPropertyKeyEPNS2_16JsonContinuationE
	.type	_ZN2v88internal10JsonParserIhE19ScanJsonPropertyKeyEPNS2_16JsonContinuationE, @function
_ZN2v88internal10JsonParserIhE19ScanJsonPropertyKeyEPNS2_16JsonContinuationE:
.LFB19774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	48(%rdi), %r14
	movq	56(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	%rdx, %r14
	je	.L1945
	movzbl	(%r14), %eax
	movq	%rsi, %rbx
	cmpl	$92, %eax
	je	.L1956
.L1946:
	leal	-48(%rax), %edx
	cmpl	$9, %edx
	ja	.L1945
	movq	48(%r12), %rdi
	movq	56(%r12), %rsi
	cmpl	$48, %eax
	jne	.L1948
	leaq	1(%rdi), %rax
	movq	%rax, 48(%r12)
	cmpq	%rsi, %rax
	je	.L1945
	cmpb	$34, 1(%rdi)
	je	.L1957
	.p2align 4,,10
	.p2align 3
.L1945:
	movq	%r14, 48(%r12)
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserIhE14ScanJsonStringEb
	movq	%rax, -52(%rbp)
	movl	%edx, -44(%rbp)
.L1949:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	movl	-44(%rbp), %edx
	jne	.L1958
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1948:
	.cfi_restore_state
	movl	%edx, -56(%rbp)
	leaq	-56(%rbp), %r13
.L1952:
	movq	%r13, %rdx
	addq	$1, %rdi
	call	_ZSt9__find_ifIPKhN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserIhE19ScanJsonPropertyKeyEPNS8_16JsonContinuationEEUlhE_EEET_SD_SD_T0_St26random_access_iterator_tag
	movq	56(%r12), %rdx
	movq	%rax, 48(%r12)
	cmpq	%rax, %rdx
	je	.L1945
	movzbl	(%rax), %ecx
	cmpb	$34, %cl
	je	.L1959
	cmpb	$92, %cl
	jne	.L1945
	leaq	1(%rax), %rcx
	movq	%rcx, 48(%r12)
	cmpq	%rcx, %rdx
	je	.L1945
	cmpb	$117, 1(%rax)
	jne	.L1945
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserIhE20ScanUnicodeCharacterEv
	leal	-48(%rax), %edx
	cmpl	$9, %edx
	ja	.L1945
	subl	$45, %eax
	movl	-56(%rbp), %ecx
	sarl	$3, %eax
	cmpl	$1, %eax
	sbbl	%eax, %eax
	notl	%eax
	addl	$429496729, %eax
	cmpl	%eax, %ecx
	ja	.L1945
	leal	(%rcx,%rcx,4), %eax
	movq	56(%r12), %rsi
	movq	48(%r12), %rdi
	leal	(%rdx,%rax,2), %eax
	movl	%eax, -56(%rbp)
	jmp	.L1952
	.p2align 4,,10
	.p2align 3
.L1956:
	leaq	1(%r14), %rax
	movq	%rax, 48(%rdi)
	cmpq	%rax, %rdx
	je	.L1945
	cmpb	$117, 1(%r14)
	jne	.L1945
	call	_ZN2v88internal10JsonParserIhE20ScanUnicodeCharacterEv
	jmp	.L1946
	.p2align 4,,10
	.p2align 3
.L1957:
	addq	$2, %rdi
	movq	$0, -52(%rbp)
	movq	%rdi, 48(%r12)
	addl	$1, 32(%rbx)
.L1955:
	movzbl	-44(%rbp), %eax
	andl	$-16, %eax
	orl	$8, %eax
	movb	%al, -44(%rbp)
	movq	-52(%rbp), %rax
	jmp	.L1949
	.p2align 4,,10
	.p2align 3
.L1959:
	addq	$1, %rax
	movl	$0, -48(%rbp)
	movq	%rax, 48(%r12)
	movl	-56(%rbp), %eax
	addl	$1, 32(%rbx)
	cmpl	%eax, 28(%rbx)
	movl	%eax, %edx
	cmovnb	28(%rbx), %edx
	movl	%eax, -52(%rbp)
	movl	%edx, 28(%rbx)
	jmp	.L1955
.L1958:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19774:
	.size	_ZN2v88internal10JsonParserIhE19ScanJsonPropertyKeyEPNS2_16JsonContinuationE, .-_ZN2v88internal10JsonParserIhE19ScanJsonPropertyKeyEPNS2_16JsonContinuationE
	.section	.rodata._ZN2v88internal10JsonParserIhE14ParseJsonValueEv.str1.1,"aMS",@progbits,1
.LC10:
	.string	"true"
.LC11:
	.string	"null"
	.section	.text._ZN2v88internal10JsonParserIhE14ParseJsonValueEv,"axG",@progbits,_ZN2v88internal10JsonParserIhE14ParseJsonValueEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE14ParseJsonValueEv
	.type	_ZN2v88internal10JsonParserIhE14ParseJsonValueEv, @function
_ZN2v88internal10JsonParserIhE14ParseJsonValueEv:
.LFB19779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$640, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -112(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -144(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -128(%rbp)
	call	_Znwm@PLT
	movq	-184(%rbp), %r13
	movq	-192(%rbp), %r12
	movq	%rax, %rbx
	cmpq	%r12, %r13
	je	.L1961
.L1963:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1962
	movq	8(%r12), %rdx
	movq	16(%r12), %rax
	subl	$1, 41104(%rdi)
	movq	%rdx, 41088(%rdi)
	cmpq	41096(%rdi), %rax
	je	.L1962
	movq	%rax, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1962:
	addq	$40, %r12
	cmpq	%r12, %r13
	jne	.L1963
	movq	-192(%rbp), %r12
.L1961:
	testq	%r12, %r12
	je	.L1964
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1964:
	movq	-160(%rbp), %rdx
	movq	-144(%rbp), %rax
	movq	%rbx, %xmm0
	movabsq	$-6148914691236517205, %rcx
	punpcklqdq	%xmm0, %xmm0
	addq	$640, %rbx
	subq	%rdx, %rax
	movq	%rbx, -176(%rbp)
	sarq	$3, %rax
	movaps	%xmm0, -192(%rbp)
	imulq	%rcx, %rax
	cmpq	$15, %rax
	jbe	.L2177
.L1965:
	movq	-128(%rbp), %rdx
	movq	-112(%rbp), %rax
	subq	%rdx, %rax
	cmpq	$127, %rax
	jbe	.L2178
.L1970:
	movq	(%r14), %rax
	leaq	-96(%rbp), %r12
	movq	$0, -72(%rbp)
	leaq	-204(%rbp), %r13
	movl	$0, -64(%rbp)
	leaq	.L1982(%rip), %rbx
	movq	41088(%rax), %rdx
	movq	%rax, -96(%rbp)
	addl	$1, 41104(%rax)
	movq	%rdx, -88(%rbp)
	movq	41096(%rax), %rdx
	movq	$0, -216(%rbp)
	movq	%rdx, -80(%rbp)
.L1979:
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE14SkipWhitespaceEv
	cmpb	$13, 16(%r14)
	ja	.L1991
	movzbl	16(%r14), %eax
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10JsonParserIhE14ParseJsonValueEv,"aG",@progbits,_ZN2v88internal10JsonParserIhE14ParseJsonValueEv,comdat
	.align 4
	.align 4
.L1982:
	.long	.L1990-.L1982
	.long	.L1989-.L1982
	.long	.L1988-.L1982
	.long	.L1981-.L1982
	.long	.L1987-.L1982
	.long	.L1981-.L1982
	.long	.L1986-.L1982
	.long	.L1985-.L1982
	.long	.L1984-.L1982
	.long	.L1983-.L1982
	.long	.L1981-.L1982
	.long	.L1981-.L1982
	.long	.L1981-.L1982
	.long	.L1981-.L1982
	.section	.text._ZN2v88internal10JsonParserIhE14ParseJsonValueEv,"axG",@progbits,_ZN2v88internal10JsonParserIhE14ParseJsonValueEv,comdat
	.p2align 4,,10
	.p2align 3
.L1981:
	movq	48(%r14), %rax
	cmpq	56(%r14), %rax
	je	.L2103
	movzbl	(%rax), %esi
.L2036:
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE25ReportUnexpectedCharacterEi
	movq	-184(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L2161:
	cmpq	-192(%rbp), %rbx
	je	.L2179
.L2037:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2180
	movq	-88(%rbp), %rdx
	subl	$1, 41104(%rdi)
	movq	-80(%rbp), %rax
	movq	%rdx, 41088(%rdi)
	cmpq	41096(%rdi), %rax
	je	.L2040
	movq	%rax, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2040:
	movq	-32(%rbx), %rax
	movq	%rax, -88(%rbp)
	movq	-24(%rbx), %rax
	movq	%rax, -80(%rbp)
	movzbl	-16(%rbx), %edx
	movq	$0, -40(%rbx)
	movzbl	-72(%rbp), %eax
	andl	$3, %edx
	andl	$-4, %eax
	orl	%edx, %eax
	movb	%al, -72(%rbp)
	movl	-16(%rbx), %edx
	movl	-72(%rbp), %eax
	andl	$-4, %edx
	andl	$3, %eax
	orl	%edx, %eax
	movl	%eax, -72(%rbp)
	movl	-12(%rbx), %eax
	movl	%eax, -68(%rbp)
	movl	-8(%rbx), %eax
	movq	-184(%rbp), %rbx
	movl	%eax, -64(%rbp)
	movq	%rbx, %rax
	subq	$40, %rbx
	movq	%rbx, -184(%rbp)
	movq	-40(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2161
	movq	-24(%rax), %rdx
	movq	-32(%rax), %rax
	subl	$1, 41104(%rdi)
	movq	%rax, 41088(%rdi)
	cmpq	41096(%rdi), %rdx
	je	.L2161
	movq	%rdx, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rbx
	cmpq	-192(%rbp), %rbx
	jne	.L2037
.L2179:
	xorl	%r13d, %r13d
	jmp	.L2038
	.p2align 4,,10
	.p2align 3
.L1988:
	addq	$1, 48(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE14SkipWhitespaceEv
	cmpb	$3, 16(%r14)
	je	.L1992
	movq	-184(%rbp), %rsi
	cmpq	-176(%rbp), %rsi
	je	.L1993
	movabsq	$-6148914691236517205, %rcx
	movq	-96(%rbp), %rax
	movq	%rax, (%rsi)
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-80(%rbp), %rax
	addq	$40, -184(%rbp)
	movq	%rax, 16(%rsi)
	movzbl	24(%rsi), %eax
	movzbl	-72(%rbp), %edx
	andl	$-4, %eax
	andl	$3, %edx
	orl	%edx, %eax
	movb	%al, 24(%rsi)
	movl	-72(%rbp), %edx
	movl	24(%rsi), %eax
	andl	$-4, %edx
	andl	$3, %eax
	orl	%edx, %eax
	movl	%eax, 24(%rsi)
	movl	-68(%rbp), %eax
	movl	%eax, 28(%rsi)
	movl	-64(%rbp), %eax
	movl	%eax, 32(%rsi)
	movq	-152(%rbp), %rax
	movq	(%r14), %rdx
	subq	-160(%rbp), %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movq	41096(%rdx), %rcx
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r15
	movq	%rcx, -232(%rbp)
	andl	$1073741823, %eax
.L1994:
	movq	%rdx, -96(%rbp)
.L1996:
	movq	%r15, %xmm0
	leaq	1(,%rax,4), %rax
	movl	$0, -64(%rbp)
	movhps	-232(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movups	%xmm0, -88(%rbp)
	jmp	.L2174
	.p2align 4,,10
	.p2align 3
.L1989:
	addq	$1, 48(%r14)
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE14ScanJsonStringEb
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	%edx, -196(%rbp)
	xorl	%edx, %edx
	movq	%rax, -204(%rbp)
	call	_ZN2v88internal10JsonParserIhE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE
	movq	%rax, -216(%rbp)
	.p2align 4,,10
	.p2align 3
.L1991:
	movzbl	-72(%rbp), %eax
	andl	$3, %eax
	cmpb	$1, %al
	je	.L2043
.L2182:
	cmpb	$2, %al
	je	.L2044
	testb	%al, %al
	jne	.L1979
	movq	-216(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_
	movq	%rax, %r13
.L2038:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2081
	movq	-88(%rbp), %rdx
	subl	$1, 41104(%rdi)
	movq	-80(%rbp), %rax
	movq	%rdx, 41088(%rdi)
	cmpq	41096(%rdi), %rax
	je	.L2081
	movq	%rax, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2081:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2082
	call	_ZdlPv@PLT
.L2082:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2083
	call	_ZdlPv@PLT
.L2083:
	movq	-184(%rbp), %rbx
	movq	-192(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2084
	.p2align 4,,10
	.p2align 3
.L2086:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2085
	movq	8(%r12), %rdx
	movq	16(%r12), %rax
	subl	$1, 41104(%rdi)
	movq	%rdx, 41088(%rdi)
	cmpq	41096(%rdi), %rax
	je	.L2085
	movq	%rax, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2085:
	addq	$40, %r12
	cmpq	%r12, %rbx
	jne	.L2086
	movq	-192(%rbp), %r12
.L2084:
	testq	%r12, %r12
	je	.L2087
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2087:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2181
	addq	$200, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1990:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE15ParseJsonNumberEv
	movq	%rax, -216(%rbp)
	movzbl	-72(%rbp), %eax
	andl	$3, %eax
	cmpb	$1, %al
	jne	.L2182
	.p2align 4,,10
	.p2align 3
.L2043:
	movq	-152(%rbp), %rax
	movq	-216(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rdx, -8(%rax)
	call	_ZN2v88internal10JsonParserIhE14SkipWhitespaceEv
	cmpb	$11, 16(%r14)
	je	.L2045
	movq	-184(%rbp), %rax
	xorl	%ecx, %ecx
	cmpq	%rax, -192(%rbp)
	je	.L2061
	movzbl	-16(%rax), %edx
	andl	$3, %edx
	cmpb	$2, %dl
	je	.L2183
.L2061:
	leaq	-160(%rbp), %r15
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal10JsonParserIhE15BuildJsonObjectERKNS2_16JsonContinuationERKSt6vectorINS0_12JsonPropertyESaIS7_EENS0_6HandleINS0_3MapEEE
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %rsi
	movabsq	$-6148914691236517205, %rdi
	movq	%rax, -216(%rbp)
	movl	-72(%rbp), %eax
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	shrl	$2, %eax
	sarq	$3, %rdx
	imulq	%rdi, %rdx
	cmpq	%rdx, %rax
	ja	.L2184
	jb	.L2185
.L2066:
	movzbl	16(%r14), %esi
	cmpb	$3, %sil
	jne	.L2076
.L2176:
	addq	$1, 48(%r14)
.L2077:
	movq	-216(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_
	movq	-96(%rbp), %rdi
	movq	-184(%rbp), %r15
	movq	%rax, -216(%rbp)
	testq	%rdi, %rdi
	je	.L2186
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rax
	subl	$1, 41104(%rdi)
	movq	%rdx, 41088(%rdi)
	cmpq	41096(%rdi), %rax
	je	.L2079
	movq	%rax, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2079:
	movq	-32(%r15), %rax
	movq	%rax, -88(%rbp)
	movq	-24(%r15), %rax
	movq	%rax, -80(%rbp)
	movzbl	-16(%r15), %edx
	movq	$0, -40(%r15)
	movzbl	-72(%rbp), %eax
	andl	$3, %edx
	andl	$-4, %eax
	orl	%edx, %eax
	movb	%al, -72(%rbp)
	movl	-16(%r15), %edx
	movl	-72(%rbp), %eax
	andl	$-4, %edx
	andl	$3, %eax
	orl	%edx, %eax
	movl	%eax, -72(%rbp)
	movl	-12(%r15), %eax
	movl	%eax, -68(%rbp)
	movl	-8(%r15), %eax
	movl	%eax, -64(%rbp)
	movq	-184(%rbp), %rax
	leaq	-40(%rax), %rdx
	movq	%rdx, -184(%rbp)
	movq	-40(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1991
	movq	-24(%rax), %rdx
	movq	-32(%rax), %rax
	subl	$1, 41104(%rdi)
	movq	%rax, 41088(%rdi)
	cmpq	41096(%rdi), %rdx
	je	.L1991
	movq	%rdx, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L1991
	.p2align 4,,10
	.p2align 3
.L1984:
	movq	48(%r14), %rax
	movq	56(%r14), %rdx
	subq	%rax, %rdx
	leaq	1(%rax), %rcx
	cmpq	$3, %rdx
	jbe	.L2026
	cmpw	$27765, 1(%rax)
	je	.L2187
.L2027:
	movq	%rcx, 48(%r14)
	movzbl	1(%rax), %esi
	cmpl	$117, %esi
	jne	.L2032
	leaq	2(%rax), %rdx
	movq	%rdx, 48(%r14)
	movzbl	2(%rax), %esi
	cmpl	$108, %esi
	jne	.L2032
	addq	$3, %rax
	movq	%rax, 48(%r14)
.L2099:
	movzbl	(%rax), %esi
	cmpl	$108, %esi
	jne	.L2032
	addq	$1, %rax
	movq	%rax, 48(%r14)
.L2033:
	movl	$13, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE21ReportUnexpectedTokenENS0_9JsonTokenE
	jmp	.L2030
	.p2align 4,,10
	.p2align 3
.L1985:
	movq	48(%r14), %rax
	movq	56(%r14), %rdx
	subq	%rax, %rdx
	leaq	1(%rax), %rcx
	cmpq	$4, %rdx
	jbe	.L2018
	cmpl	$1702063201, 1(%rax)
	jne	.L2019
	addq	$5, %rax
	movq	%rax, 48(%r14)
.L2020:
	movq	(%r14), %rax
	addq	$120, %rax
	movq	%rax, -216(%rbp)
	jmp	.L1991
	.p2align 4,,10
	.p2align 3
.L1986:
	movq	48(%r14), %rax
	movq	56(%r14), %rdx
	subq	%rax, %rdx
	leaq	1(%rax), %rcx
	cmpq	$3, %rdx
	jbe	.L2008
	cmpw	$30066, 1(%rax)
	je	.L2188
.L2009:
	movq	%rcx, 48(%r14)
	movzbl	1(%rax), %esi
	cmpl	$114, %esi
	jne	.L2014
	leaq	2(%rax), %rdx
	movq	%rdx, 48(%r14)
	movzbl	2(%rax), %esi
	cmpl	$117, %esi
	jne	.L2014
	addq	$3, %rax
	movq	%rax, 48(%r14)
.L2097:
	movzbl	(%rax), %esi
	cmpl	$101, %esi
	jne	.L2014
	addq	$1, %rax
	movq	%rax, 48(%r14)
.L2015:
	movl	$13, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE21ReportUnexpectedTokenENS0_9JsonTokenE
	jmp	.L2012
	.p2align 4,,10
	.p2align 3
.L1987:
	addq	$1, 48(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE14SkipWhitespaceEv
	cmpb	$5, 16(%r14)
	je	.L2189
	movq	-184(%rbp), %rsi
	cmpq	-176(%rbp), %rsi
	je	.L2004
	movq	-96(%rbp), %rax
	movq	%rax, (%rsi)
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-80(%rbp), %rax
	addq	$40, -184(%rbp)
	movq	%rax, 16(%rsi)
	movzbl	24(%rsi), %eax
	movzbl	-72(%rbp), %edx
	andl	$-4, %eax
	andl	$3, %edx
	orl	%edx, %eax
	movb	%al, 24(%rsi)
	movl	-72(%rbp), %edx
	movl	24(%rsi), %eax
	andl	$-4, %edx
	andl	$3, %eax
	orl	%edx, %eax
	movl	%eax, 24(%rsi)
	movl	-68(%rbp), %eax
	movl	%eax, 28(%rsi)
	movl	-64(%rbp), %eax
	movl	%eax, 32(%rsi)
	movq	(%r14), %rdx
	movq	-120(%rbp), %rax
	subq	-128(%rbp), %rax
	movq	41096(%rdx), %rcx
	addl	$1, 41104(%rdx)
	sarq	$3, %rax
	movq	41088(%rdx), %r15
	andl	$1073741823, %eax
	movq	%rcx, -232(%rbp)
.L2005:
	movq	%rdx, -96(%rbp)
.L2007:
	movq	%r15, %xmm0
	leaq	2(,%rax,4), %rax
	movl	$0, -64(%rbp)
	movhps	-232(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movups	%xmm0, -88(%rbp)
	jmp	.L1979
	.p2align 4,,10
	.p2align 3
.L2180:
	movq	-40(%rbx), %rax
	movq	%rax, -96(%rbp)
	jmp	.L2040
	.p2align 4,,10
	.p2align 3
.L2044:
	leaq	-128(%rbp), %r15
	leaq	-216(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE12emplace_backIJRS4_EEEvDpOT_
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE14SkipWhitespaceEv
	cmpb	$11, 16(%r14)
	je	.L2159
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE14BuildJsonArrayERKNS2_16JsonContinuationERKSt6vectorINS0_6HandleINS0_6ObjectEEESaIS9_EE
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rcx
	movq	%rax, -216(%rbp)
	movl	-72(%rbp), %esi
	movq	%rdx, %rax
	subq	%rcx, %rax
	shrl	$2, %esi
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L2190
	jnb	.L2075
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdx
	je	.L2075
	movq	%rax, -120(%rbp)
.L2075:
	movzbl	16(%r14), %esi
	cmpb	$5, %sil
	je	.L2176
.L2076:
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE21ReportUnexpectedTokenENS0_9JsonTokenE
	jmp	.L2077
	.p2align 4,,10
	.p2align 3
.L2045:
	addq	$1, 48(%r14)
.L2174:
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE14SkipWhitespaceEv
	movzbl	16(%r14), %esi
	cmpb	$1, %sil
	jne	.L2048
	addq	$1, 48(%r14)
.L2049:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE19ScanJsonPropertyKeyEPNS2_16JsonContinuationE
	movq	-152(%rbp), %rsi
	movq	%rax, -204(%rbp)
	movl	%edx, -196(%rbp)
	cmpq	-144(%rbp), %rsi
	je	.L2050
	movq	%rax, (%rsi)
	movl	-196(%rbp), %eax
	movq	$0, 16(%rsi)
	movl	%eax, 8(%rsi)
	addq	$24, -152(%rbp)
.L2051:
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE14SkipWhitespaceEv
	movzbl	16(%r14), %esi
	cmpb	$10, %sil
	jne	.L2052
.L2159:
	addq	$1, 48(%r14)
	jmp	.L1979
	.p2align 4,,10
	.p2align 3
.L2178:
	movq	-120(%rbp), %r12
	movl	$128, %edi
	subq	%rdx, %r12
	call	_Znwm@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	%rdi, %rcx
	je	.L1978
	leaq	-8(%rcx), %rax
	leaq	15(%rdi), %rdx
	subq	%rdi, %rax
	subq	%rbx, %rdx
	shrq	$3, %rax
	cmpq	$30, %rdx
	jbe	.L2102
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rax
	je	.L2102
	addq	$1, %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1976:
	movdqu	(%rdi,%rdx), %xmm2
	movups	%xmm2, (%rbx,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L1976
	movq	%rax, %rsi
	andq	$-2, %rsi
	leaq	0(,%rsi,8), %rdx
	leaq	(%rdi,%rdx), %rcx
	addq	%rbx, %rdx
	cmpq	%rax, %rsi
	je	.L1972
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
.L1972:
	call	_ZdlPv@PLT
.L1973:
	addq	%rbx, %r12
	movq	%rbx, -128(%rbp)
	subq	$-128, %rbx
	movq	%r12, -120(%rbp)
	movq	%rbx, -112(%rbp)
	jmp	.L1970
	.p2align 4,,10
	.p2align 3
.L2177:
	movq	-152(%rbp), %rbx
	movl	$384, %edi
	subq	%rdx, %rbx
	call	_Znwm@PLT
	movq	-160(%rbp), %r8
	movq	-152(%rbp), %rdi
	movq	%rax, %r12
	movq	%rax, %rcx
	movq	%r8, %rdx
	cmpq	%r8, %rdi
	je	.L1969
	.p2align 4,,10
	.p2align 3
.L1966:
	movdqu	(%rdx), %xmm1
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rdi
	jne	.L1966
.L1969:
	testq	%r8, %r8
	je	.L1968
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L1968:
	addq	%r12, %rbx
	movq	%r12, -160(%rbp)
	addq	$384, %r12
	movq	%rbx, -152(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L1965
	.p2align 4,,10
	.p2align 3
.L2189:
	addq	$1, 48(%r14)
	movq	(%r14), %rdi
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	movq	%rax, -216(%rbp)
	jmp	.L1991
	.p2align 4,,10
	.p2align 3
.L1992:
	addq	$1, 48(%r14)
	movq	24(%r14), %rsi
	xorl	%edx, %edx
	movq	(%r14), %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%rax, -216(%rbp)
	jmp	.L1991
	.p2align 4,,10
	.p2align 3
.L2052:
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE21ReportUnexpectedTokenENS0_9JsonTokenE
	jmp	.L1979
	.p2align 4,,10
	.p2align 3
.L2102:
	movq	%rbx, %rdx
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L1974:
	movq	(%rax), %rsi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rsi, -8(%rdx)
	cmpq	%rax, %rcx
	jne	.L1974
.L1978:
	testq	%rdi, %rdi
	je	.L1973
	jmp	.L1972
	.p2align 4,,10
	.p2align 3
.L2050:
	leaq	-160(%rbp), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIN2v88internal12JsonPropertyESaIS2_EE17_M_realloc_insertIJNS1_10JsonStringEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L2051
.L1993:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorIN2v88internal10JsonParserIhE16JsonContinuationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-152(%rbp), %rax
	subq	-160(%rbp), %rax
	movabsq	$-6148914691236517205, %rcx
	movq	(%r14), %rdx
	sarq	$3, %rax
	movq	-96(%rbp), %rdi
	imulq	%rcx, %rax
	movq	41096(%rdx), %rcx
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r15
	movq	%rcx, -232(%rbp)
	andl	$1073741823, %eax
	testq	%rdi, %rdi
	je	.L1994
	movq	-88(%rbp), %rsi
	subl	$1, 41104(%rdi)
	movq	-80(%rbp), %rdx
	movq	%rsi, 41088(%rdi)
	cmpq	41096(%rdi), %rdx
	je	.L1996
	movq	%rdx, 41096(%rdi)
	movl	%eax, -236(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-236(%rbp), %eax
	jmp	.L1996
.L2004:
	leaq	-192(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorIN2v88internal10JsonParserIhE16JsonContinuationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	(%r14), %rdx
	movq	-120(%rbp), %rax
	movq	-96(%rbp), %rdi
	subq	-128(%rbp), %rax
	movq	41096(%rdx), %rcx
	sarq	$3, %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r15
	andl	$1073741823, %eax
	movq	%rcx, -232(%rbp)
	testq	%rdi, %rdi
	je	.L2005
	movq	-88(%rbp), %rsi
	subl	$1, 41104(%rdi)
	movq	-80(%rbp), %rdx
	movq	%rsi, 41088(%rdi)
	cmpq	41096(%rdi), %rdx
	je	.L2007
	movq	%rdx, 41096(%rdi)
	movl	%eax, -236(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-236(%rbp), %eax
	jmp	.L2007
	.p2align 4,,10
	.p2align 3
.L2187:
	cmpb	$108, 2(%rcx)
	jne	.L2027
	addq	$4, %rax
	movq	%rax, 48(%r14)
.L2030:
	movq	(%r14), %rax
	addq	$104, %rax
	movq	%rax, -216(%rbp)
	jmp	.L1991
	.p2align 4,,10
	.p2align 3
.L2188:
	cmpb	$101, 2(%rcx)
	jne	.L2009
	addq	$4, %rax
	movq	%rax, 48(%r14)
.L2012:
	movq	(%r14), %rax
	addq	$112, %rax
	movq	%rax, -216(%rbp)
	jmp	.L1991
	.p2align 4,,10
	.p2align 3
.L2048:
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE21ReportUnexpectedTokenENS0_9JsonTokenE
	jmp	.L2049
	.p2align 4,,10
	.p2align 3
.L1983:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2185:
	leaq	(%rax,%rax,2), %rax
	leaq	(%rsi,%rax,8), %rax
	cmpq	%rax, %rcx
	je	.L2066
	movq	%rax, -152(%rbp)
	jmp	.L2066
.L2183:
	movq	-120(%rbp), %rsi
	movl	-16(%rax), %eax
	movq	%rsi, %rdx
	subq	-128(%rbp), %rdx
	shrl	$2, %eax
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jnb	.L2061
	movq	-8(%rsi), %rax
	movq	(%rax), %rax
	testb	$1, %al
	je	.L2061
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L2061
	movq	-1(%rax), %r15
	movq	(%r14), %rdx
	movl	15(%r15), %eax
	testl	$1048576, %eax
	jne	.L2061
	cmpw	$1057, 11(%r15)
	je	.L2191
.L2056:
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2062
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
	jmp	.L2061
	.p2align 4,,10
	.p2align 3
.L2014:
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE25ReportUnexpectedCharacterEi
	jmp	.L2012
.L2032:
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE25ReportUnexpectedCharacterEi
	jmp	.L2030
	.p2align 4,,10
	.p2align 3
.L2186:
	movq	-40(%r15), %rax
	movq	%rax, -96(%rbp)
	jmp	.L2079
	.p2align 4,,10
	.p2align 3
.L2008:
	movq	%rcx, 48(%r14)
	leaq	-1(%rdx), %rcx
	cmpq	$1, %rdx
	je	.L2015
	movzbl	1(%rax), %esi
	cmpl	$114, %esi
	jne	.L2014
	leaq	2(%rax), %rdx
	movq	%rdx, 48(%r14)
	cmpq	$1, %rcx
	jbe	.L2015
	movzbl	2(%rax), %esi
	cmpl	$117, %esi
	jne	.L2014
	addq	$3, %rax
	movq	%rax, 48(%r14)
	cmpq	$2, %rcx
	ja	.L2097
	jmp	.L2015
	.p2align 4,,10
	.p2align 3
.L2103:
	movl	$-1, %esi
	jmp	.L2036
	.p2align 4,,10
	.p2align 3
.L2026:
	movq	%rcx, 48(%r14)
	leaq	-1(%rdx), %rcx
	cmpq	$1, %rdx
	je	.L2033
	movzbl	1(%rax), %esi
	cmpl	$117, %esi
	jne	.L2032
	leaq	2(%rax), %rdx
	movq	%rdx, 48(%r14)
	cmpq	$1, %rcx
	jbe	.L2033
	movzbl	2(%rax), %esi
	cmpl	$108, %esi
	jne	.L2032
	addq	$3, %rax
	movq	%rax, 48(%r14)
	cmpq	$2, %rcx
	ja	.L2099
	jmp	.L2033
	.p2align 4,,10
	.p2align 3
.L2018:
	leaq	-1(%rdx), %rdi
	movq	%rcx, 48(%r14)
	movl	$4, %ecx
	cmpq	$4, %rdi
	cmovbe	%rdi, %rcx
	cmpq	$1, %rdx
	je	.L2023
	movzbl	1(%rax), %esi
	cmpl	$97, %esi
	jne	.L2022
	leaq	2(%rax), %rdx
	movq	%rdx, 48(%r14)
	cmpq	$1, %rdi
	jbe	.L2023
	movzbl	2(%rax), %esi
	cmpl	$108, %esi
	jne	.L2022
	addq	$3, %rax
	movq	%rax, 48(%r14)
	cmpq	$2, %rdi
	je	.L2023
.L2098:
	movzbl	(%rax), %esi
	cmpl	$115, %esi
	jne	.L2022
	leaq	1(%rax), %rdx
	movq	%rdx, 48(%r14)
	cmpq	$4, %rcx
	jne	.L2023
	movzbl	1(%rax), %esi
	cmpl	$101, %esi
	jne	.L2022
	addq	$2, %rax
	movq	%rax, 48(%r14)
.L2023:
	movl	$13, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE21ReportUnexpectedTokenENS0_9JsonTokenE
	jmp	.L2020
.L2022:
	movq	%r14, %rdi
	call	_ZN2v88internal10JsonParserIhE25ReportUnexpectedCharacterEi
	jmp	.L2020
.L2019:
	movq	%rcx, 48(%r14)
	movzbl	1(%rax), %esi
	cmpl	$97, %esi
	jne	.L2022
	leaq	2(%rax), %rdx
	movq	%rdx, 48(%r14)
	movzbl	2(%rax), %esi
	cmpl	$108, %esi
	jne	.L2022
	addq	$3, %rax
	movl	$4, %ecx
	movq	%rax, 48(%r14)
	jmp	.L2098
	.p2align 4,,10
	.p2align 3
.L2190:
	subq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm
	jmp	.L2075
.L2181:
	call	__stack_chk_fail@PLT
.L2062:
	movq	41088(%rdx), %rcx
	cmpq	41096(%rdx), %rcx
	je	.L2192
.L2064:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%r15, (%rcx)
	jmp	.L2061
.L2191:
	movl	15(%r15), %eax
	testl	$1047552, %eax
	je	.L2056
	movq	%r15, %rsi
	movq	31(%r15), %rax
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	testb	$1, %al
	je	.L2059
	movq	-1(%rax), %rdi
	cmpq	%rdi, -37456(%rsi)
	je	.L2058
.L2059:
	movq	-37504(%rsi), %rax
.L2058:
	cmpq	%rax, 88(%rdx)
	jne	.L2056
	jmp	.L2061
.L2184:
	subq	%rdx, %rax
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZNSt6vectorIN2v88internal12JsonPropertyESaIS2_EE17_M_default_appendEm.part.0
.L2192:
	movq	%rdx, %rdi
	movq	%rdx, -232(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L2064
	.cfi_endproc
.LFE19779:
	.size	_ZN2v88internal10JsonParserIhE14ParseJsonValueEv, .-_ZN2v88internal10JsonParserIhE14ParseJsonValueEv
	.section	.text._ZN2v88internal10JsonParserIhE9ParseJsonEv,"axG",@progbits,_ZN2v88internal10JsonParserIhE9ParseJsonEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE9ParseJsonEv
	.type	_ZN2v88internal10JsonParserIhE9ParseJsonEv, @function
_ZN2v88internal10JsonParserIhE9ParseJsonEv:
.LFB19757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v88internal10JsonParserIhE14ParseJsonValueEv
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal10JsonParserIhE14SkipWhitespaceEv
	movzbl	16(%rbx), %esi
	cmpb	$13, %sil
	je	.L2194
	movq	%rbx, %rdi
	call	_ZN2v88internal10JsonParserIhE21ReportUnexpectedTokenENS0_9JsonTokenE
.L2195:
	movq	(%rbx), %rax
	popq	%rbx
	movq	12480(%rax), %rdx
	cmpq	%rdx, 96(%rax)
	movl	$0, %eax
	cmove	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2194:
	.cfi_restore_state
	addq	$1, 48(%rbx)
	jmp	.L2195
	.cfi_endproc
.LFE19757:
	.size	_ZN2v88internal10JsonParserIhE9ParseJsonEv, .-_ZN2v88internal10JsonParserIhE9ParseJsonEv
	.section	.text._ZN2v88internal10JsonParserIhE5ParseEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS5_INS0_6ObjectEEE,"axG",@progbits,_ZN2v88internal10JsonParserIhE5ParseEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS5_INS0_6ObjectEEE,comdat
	.p2align 4
	.weak	_ZN2v88internal10JsonParserIhE5ParseEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS5_INS0_6ObjectEEE
	.type	_ZN2v88internal10JsonParserIhE5ParseEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS5_INS0_6ObjectEEE, @function
_ZN2v88internal10JsonParserIhE5ParseEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS5_INS0_6ObjectEEE:
.LFB19750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movq	%rsi, %rdx
	pushq	%r14
	movq	%rdi, %rsi
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	subq	$80, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal10JsonParserIhEC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	movq	%r13, %rdi
	call	_ZN2v88internal10JsonParserIhE9ParseJsonEv
	testq	%rax, %rax
	je	.L2209
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal10JsonParserIhED1Ev
	movq	(%r15), %rax
	testb	$1, %al
	jne	.L2202
.L2203:
	movq	%r12, %rax
.L2201:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2210
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2209:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal10JsonParserIhED1Ev
	xorl	%eax, %eax
	jmp	.L2201
	.p2align 4,,10
	.p2align 3
.L2202:
	movq	-1(%rax), %rax
	testb	$2, 13(%rax)
	je	.L2203
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21JsonParseInternalizer11InternalizeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_
	jmp	.L2201
.L2210:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19750:
	.size	_ZN2v88internal10JsonParserIhE5ParseEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS5_INS0_6ObjectEEE, .-_ZN2v88internal10JsonParserIhE5ParseEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS5_INS0_6ObjectEEE
	.section	.text._ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE19AdvanceToNonDecimalEvEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE19AdvanceToNonDecimalEvEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE19AdvanceToNonDecimalEvEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE19AdvanceToNonDecimalEvEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE19AdvanceToNonDecimalEvEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag:
.LFB21948:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	subq	%rdi, %rax
	movq	%rax, %rdx
	sarq	$3, %rax
	sarq	%rdx
	testq	%rax, %rax
	jle	.L2212
	leaq	(%rdi,%rax,8), %rdx
	jmp	.L2217
	.p2align 4,,10
	.p2align 3
.L2236:
	movzwl	2(%rdi), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L2232
	movzwl	4(%rdi), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L2233
	movzwl	6(%rdi), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L2234
	addq	$8, %rdi
	cmpq	%rdi, %rdx
	je	.L2235
.L2217:
	movzwl	(%rdi), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L2236
	movq	%rdi, %rax
.L2211:
	ret
	.p2align 4,,10
	.p2align 3
.L2235:
	movq	%rsi, %rdx
	subq	%rdi, %rdx
	sarq	%rdx
.L2212:
	cmpq	$2, %rdx
	je	.L2218
	cmpq	$3, %rdx
	je	.L2219
	cmpq	$1, %rdx
	je	.L2220
	movq	%rsi, %rax
	ret
.L2219:
	movzwl	(%rdi), %edx
	movq	%rdi, %rax
	subl	$48, %edx
	cmpl	$9, %edx
	ja	.L2211
	addq	$2, %rdi
.L2218:
	movzwl	(%rdi), %edx
	movq	%rdi, %rax
	subl	$48, %edx
	cmpl	$9, %edx
	ja	.L2211
	addq	$2, %rdi
.L2220:
	movzwl	(%rdi), %eax
	subl	$48, %eax
	cmpl	$10, %eax
	movq	%rdi, %rax
	cmovb	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2232:
	leaq	2(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2233:
	leaq	4(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2234:
	leaq	6(%rdi), %rax
	ret
	.cfi_endproc
.LFE21948:
	.size	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE19AdvanceToNonDecimalEvEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE19AdvanceToNonDecimalEvEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag
	.section	.text._ZN2v88internal10JsonParserItE19AdvanceToNonDecimalEv,"axG",@progbits,_ZN2v88internal10JsonParserItE19AdvanceToNonDecimalEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE19AdvanceToNonDecimalEv
	.type	_ZN2v88internal10JsonParserItE19AdvanceToNonDecimalEv, @function
_ZN2v88internal10JsonParserItE19AdvanceToNonDecimalEv:
.LFB19839:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	56(%rdi), %rsi
	movq	48(%rdi), %rdi
	call	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE19AdvanceToNonDecimalEvEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag
	movq	%rax, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19839:
	.size	_ZN2v88internal10JsonParserItE19AdvanceToNonDecimalEv, .-_ZN2v88internal10JsonParserItE19AdvanceToNonDecimalEv
	.section	.text._ZN2v88internal10JsonParserItE15ParseJsonNumberEv,"axG",@progbits,_ZN2v88internal10JsonParserItE15ParseJsonNumberEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE15ParseJsonNumberEv
	.type	_ZN2v88internal10JsonParserItE15ParseJsonNumberEv, @function
_ZN2v88internal10JsonParserItE15ParseJsonNumberEv:
.LFB19856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	$1, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	48(%rdi), %r13
	movzwl	0(%r13), %edx
	movq	%r13, %r12
	cmpl	$45, %edx
	je	.L2313
.L2240:
	cmpl	$48, %edx
	jne	.L2241
	movq	56(%rbx), %r8
	leaq	2(%r12), %rsi
	movq	%rsi, 48(%rbx)
	cmpq	%r8, %rsi
	je	.L2242
	movzwl	2(%r12), %ecx
	movl	%ecx, %edx
	cmpw	$255, %cx
	ja	.L2243
	movslq	%ecx, %rdi
	leaq	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE(%rip), %rax
	testb	$16, (%rax,%rdi)
	jne	.L2314
.L2243:
	cmpl	$1, %r14d
	je	.L2312
.L2245:
	cmpw	$46, %dx
	je	.L2315
.L2267:
	orl	$32, %edx
	cmpw	$101, %dx
	jne	.L2273
	leaq	2(%rsi), %rax
	movq	%rax, 48(%rbx)
	cmpq	%r8, %rax
	je	.L2278
	movzwl	2(%rsi), %eax
	leal	-43(%rax), %edx
	testw	$-3, %dx
	jne	.L2316
	leaq	4(%rsi), %rax
	movq	%rax, 48(%rbx)
	cmpq	%r8, %rax
	je	.L2278
	movzwl	4(%rsi), %esi
.L2277:
	leal	-48(%rsi), %eax
	cmpl	$9, %eax
	ja	.L2275
	movq	%rbx, %rdi
	call	_ZN2v88internal10JsonParserItE19AdvanceToNonDecimalEv
	movq	48(%rbx), %rsi
.L2273:
	subq	%r13, %rsi
	movsd	.LC9(%rip), %xmm0
	movq	%r13, %rdi
	xorl	%edx, %edx
	sarq	%rsi
	call	_ZN2v88internal14StringToDoubleENS0_6VectorIKtEEid@PLT
	movq	(%rbx), %rdi
	addq	$16, %rsp
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L2284:
	.cfi_restore_state
	movl	$-1, %r14d
	.p2align 4,,10
	.p2align 3
.L2241:
	movq	%rbx, %rdi
	call	_ZN2v88internal10JsonParserItE19AdvanceToNonDecimalEv
	movq	48(%rbx), %rsi
	cmpq	%r12, %rsi
	je	.L2317
	movq	56(%rbx), %r8
	movq	%rsi, %rax
	subq	%r12, %rax
	cmpq	%r8, %rsi
	je	.L2260
	movzwl	(%rsi), %edx
	cmpq	$18, %rax
	jg	.L2245
	cmpw	$255, %dx
	ja	.L2289
	movzwl	%dx, %eax
	leaq	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE(%rip), %rcx
	testb	$16, (%rcx,%rax)
	jne	.L2245
.L2289:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2263:
	movzwl	(%r12), %edx
	leal	(%rax,%rax,4), %ecx
	addq	$2, %r12
	leal	-48(%rdx,%rcx,2), %eax
	cmpq	%r12, %rsi
	jne	.L2263
	imull	%r14d, %eax
	movq	(%rbx), %rbx
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	jne	.L2304
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2318
.L2266:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L2300
	.p2align 4,,10
	.p2align 3
.L2317:
	cmpq	%r12, 56(%rbx)
	je	.L2278
	movzwl	(%r12), %esi
	.p2align 4,,10
	.p2align 3
.L2275:
	movq	%rbx, %rdi
	call	_ZN2v88internal10JsonParserItE25ReportUnexpectedCharacterEi
.L2312:
	movq	(%rbx), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2280
.L2303:
	xorl	%esi, %esi
.L2304:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L2300:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2313:
	.cfi_restore_state
	leaq	2(%r13), %r12
	movq	%r12, 48(%rdi)
	cmpq	56(%rdi), %r12
	je	.L2284
	movzwl	2(%r13), %edx
	movl	$-1, %r14d
	jmp	.L2240
	.p2align 4,,10
	.p2align 3
.L2280:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2305
.L2282:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	$0, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2314:
	.cfi_restore_state
	subl	$48, %ecx
	cmpl	$9, %ecx
	ja	.L2245
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE
	movq	(%rbx), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L2303
	movq	41088(%rbx), %rax
	cmpq	%rax, 41096(%rbx)
	jne	.L2282
	.p2align 4,,10
	.p2align 3
.L2305:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L2282
	.p2align 4,,10
	.p2align 3
.L2316:
	movzwl	%ax, %esi
	jmp	.L2277
	.p2align 4,,10
	.p2align 3
.L2242:
	cmpl	$1, %r14d
	jne	.L2273
	jmp	.L2312
	.p2align 4,,10
	.p2align 3
.L2315:
	leaq	2(%rsi), %rax
	movq	%rax, 48(%rbx)
	cmpq	%r8, %rax
	je	.L2278
	movzwl	2(%rsi), %esi
	leal	-48(%rsi), %eax
	cmpl	$9, %eax
	ja	.L2275
	movq	%rbx, %rdi
	call	_ZN2v88internal10JsonParserItE19AdvanceToNonDecimalEv
	movq	48(%rbx), %rsi
	movq	56(%rbx), %r8
	cmpq	%r8, %rsi
	je	.L2273
	movzwl	(%rsi), %edx
	jmp	.L2267
	.p2align 4,,10
	.p2align 3
.L2278:
	movl	$-1, %esi
	jmp	.L2275
	.p2align 4,,10
	.p2align 3
.L2260:
	cmpq	$18, %rax
	jg	.L2273
	jmp	.L2289
.L2318:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L2266
	.cfi_endproc
.LFE19856:
	.size	_ZN2v88internal10JsonParserItE15ParseJsonNumberEv, .-_ZN2v88internal10JsonParserItE15ParseJsonNumberEv
	.section	.text._ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE14SkipWhitespaceEvEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE14SkipWhitespaceEvEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE14SkipWhitespaceEvEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE14SkipWhitespaceEvEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE14SkipWhitespaceEvEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag:
.LFB21952:
	.cfi_startproc
	endbr64
	movq	%rsi, %rcx
	movq	%rdi, %rax
	subq	%rdi, %rcx
	movq	%rcx, %rdi
	sarq	$3, %rcx
	sarq	%rdi
	testq	%rcx, %rcx
	jle	.L2320
	leaq	(%rax,%rcx,8), %r9
	leaq	_ZN2v88internal12_GLOBAL__N_1L20one_char_json_tokensE(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L2335:
	movzwl	(%rax), %ecx
	cmpw	$255, %cx
	ja	.L2356
	movslq	%ecx, %rcx
	movzbl	(%rdi,%rcx), %ecx
	cmpb	$9, %cl
	je	.L2323
.L2355:
	movb	%cl, 16(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L2323:
	movzwl	2(%rax), %r8d
	leaq	2(%rax), %r10
	movzwl	%r8w, %ecx
	cmpw	$255, %r8w
	ja	.L2351
	movzbl	(%rdi,%rcx), %ecx
	cmpb	$9, %cl
	je	.L2324
.L2357:
	movb	%cl, 16(%rdx)
	movq	%r10, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2324:
	movzwl	4(%rax), %r8d
	leaq	4(%rax), %r10
	movzwl	%r8w, %ecx
	cmpw	$255, %r8w
	ja	.L2351
	movzbl	(%rdi,%rcx), %ecx
	cmpb	$9, %cl
	jne	.L2357
	movzwl	6(%rax), %r8d
	leaq	6(%rax), %r10
	movzwl	%r8w, %ecx
	cmpw	$255, %r8w
	ja	.L2351
	movzbl	(%rdi,%rcx), %ecx
	cmpb	$9, %cl
	jne	.L2357
	addq	$8, %rax
	cmpq	%r9, %rax
	jne	.L2335
	movq	%rsi, %rdi
	subq	%rax, %rdi
	sarq	%rdi
.L2320:
	cmpq	$2, %rdi
	je	.L2327
	cmpq	$3, %rdi
	je	.L2328
	cmpq	$1, %rdi
	je	.L2329
.L2343:
	movq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2351:
	movb	$12, 16(%rdx)
	movq	%r10, %rax
	ret
.L2328:
	movzwl	(%rax), %ecx
	cmpw	$255, %cx
	jbe	.L2330
.L2356:
	movb	$12, 16(%rdx)
	ret
.L2330:
	movslq	%ecx, %rcx
	leaq	_ZN2v88internal12_GLOBAL__N_1L20one_char_json_tokensE(%rip), %rdi
	movzbl	(%rdi,%rcx), %ecx
	cmpb	$9, %cl
	jne	.L2355
	addq	$2, %rax
.L2327:
	movzwl	(%rax), %ecx
	cmpw	$255, %cx
	ja	.L2356
	movslq	%ecx, %rcx
	leaq	_ZN2v88internal12_GLOBAL__N_1L20one_char_json_tokensE(%rip), %rdi
	movzbl	(%rdi,%rcx), %ecx
	cmpb	$9, %cl
	jne	.L2355
	addq	$2, %rax
.L2329:
	movzwl	(%rax), %ecx
	cmpw	$255, %cx
	ja	.L2356
	movslq	%ecx, %rcx
	leaq	_ZN2v88internal12_GLOBAL__N_1L20one_char_json_tokensE(%rip), %rdi
	movzbl	(%rdi,%rcx), %ecx
	cmpb	$9, %cl
	jne	.L2355
	jmp	.L2343
	.cfi_endproc
.LFE21952:
	.size	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE14SkipWhitespaceEvEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE14SkipWhitespaceEvEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag
	.section	.text._ZN2v88internal10JsonParserItE14SkipWhitespaceEv,"axG",@progbits,_ZN2v88internal10JsonParserItE14SkipWhitespaceEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE14SkipWhitespaceEv
	.type	_ZN2v88internal10JsonParserItE14SkipWhitespaceEv, @function
_ZN2v88internal10JsonParserItE14SkipWhitespaceEv:
.LFB19848:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rbx, %rdx
	subq	$8, %rsp
	movb	$13, 16(%rdi)
	movq	56(%rdi), %rsi
	movq	48(%rdi), %rdi
	call	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE14SkipWhitespaceEvEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag
	movq	%rax, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19848:
	.size	_ZN2v88internal10JsonParserItE14SkipWhitespaceEv, .-_ZN2v88internal10JsonParserItE14SkipWhitespaceEv
	.section	.text._ZN2v88internal10JsonParserItE10ExpectNextENS0_9JsonTokenE,"axG",@progbits,_ZN2v88internal10JsonParserItE10ExpectNextENS0_9JsonTokenE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE10ExpectNextENS0_9JsonTokenE
	.type	_ZN2v88internal10JsonParserItE10ExpectNextENS0_9JsonTokenE, @function
_ZN2v88internal10JsonParserItE10ExpectNextENS0_9JsonTokenE:
.LFB19846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	call	_ZN2v88internal10JsonParserItE14SkipWhitespaceEv
	movzbl	16(%r12), %esi
	cmpb	%sil, %bl
	jne	.L2361
	popq	%rbx
	addq	$2, 48(%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2361:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE
	.cfi_endproc
.LFE19846:
	.size	_ZN2v88internal10JsonParserItE10ExpectNextENS0_9JsonTokenE, .-_ZN2v88internal10JsonParserItE10ExpectNextENS0_9JsonTokenE
	.section	.text._ZN2v88internal10JsonParserItE5CheckENS0_9JsonTokenE,"axG",@progbits,_ZN2v88internal10JsonParserItE5CheckENS0_9JsonTokenE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE5CheckENS0_9JsonTokenE
	.type	_ZN2v88internal10JsonParserItE5CheckENS0_9JsonTokenE, @function
_ZN2v88internal10JsonParserItE5CheckENS0_9JsonTokenE:
.LFB19847:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v88internal10JsonParserItE14SkipWhitespaceEv
	xorl	%eax, %eax
	cmpb	%r12b, 16(%rbx)
	jne	.L2365
	addq	$2, 48(%rbx)
	movl	$1, %eax
.L2365:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19847:
	.size	_ZN2v88internal10JsonParserItE5CheckENS0_9JsonTokenE, .-_ZN2v88internal10JsonParserItE5CheckENS0_9JsonTokenE
	.section	.text._ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE14ScanJsonStringEbEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE14ScanJsonStringEbEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE14ScanJsonStringEbEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE14ScanJsonStringEbEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE14ScanJsonStringEbEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag:
.LFB21956:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	subq	%rdi, %rax
	movq	%rax, %rcx
	sarq	$3, %rax
	sarq	%rcx
	testq	%rax, %rax
	jle	.L2371
	leaq	(%rdi,%rax,8), %r9
	leaq	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE(%rip), %rcx
	jmp	.L2381
	.p2align 4,,10
	.p2align 3
.L2372:
	cltq
	testb	$8, (%rcx,%rax)
	jne	.L2390
	movzwl	2(%rdi), %eax
	cmpw	$255, %ax
	ja	.L2411
.L2375:
	cltq
	testb	$8, (%rcx,%rax)
	jne	.L2412
	movzwl	4(%rdi), %eax
	cmpw	$255, %ax
	ja	.L2413
.L2377:
	cltq
	testb	$8, (%rcx,%rax)
	jne	.L2414
	movzwl	6(%rdi), %eax
	cmpw	$255, %ax
	ja	.L2415
.L2379:
	cltq
	testb	$8, (%rcx,%rax)
	jne	.L2416
	addq	$8, %rdi
	cmpq	%rdi, %r9
	je	.L2417
.L2381:
	movzwl	(%rdi), %eax
	cmpw	$255, %ax
	jbe	.L2372
	orl	%eax, (%rdx)
	movzwl	2(%rdi), %eax
	cmpw	$255, %ax
	jbe	.L2375
	.p2align 4,,10
	.p2align 3
.L2411:
	orl	%eax, (%rdx)
	movzwl	4(%rdi), %eax
	cmpw	$255, %ax
	jbe	.L2377
	.p2align 4,,10
	.p2align 3
.L2413:
	orl	%eax, (%rdx)
	movzwl	6(%rdi), %eax
	cmpw	$255, %ax
	jbe	.L2379
	.p2align 4,,10
	.p2align 3
.L2415:
	addq	$8, %rdi
	orl	%eax, (%rdx)
	cmpq	%rdi, %r9
	jne	.L2381
	.p2align 4,,10
	.p2align 3
.L2417:
	movq	%rsi, %rcx
	subq	%rdi, %rcx
	sarq	%rcx
.L2371:
	cmpq	$2, %rcx
	je	.L2382
	cmpq	$3, %rcx
	je	.L2383
	cmpq	$1, %rcx
	je	.L2384
.L2410:
	movq	%rsi, %rax
	ret
.L2420:
	orl	%eax, (%rdx)
.L2386:
	addq	$2, %rdi
.L2382:
	movzwl	(%rdi), %eax
	cmpw	$255, %ax
	ja	.L2418
	movslq	%eax, %rcx
	leaq	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE(%rip), %r8
	movq	%rdi, %rax
	testb	$8, (%r8,%rcx)
	jne	.L2370
.L2388:
	addq	$2, %rdi
.L2384:
	movzwl	(%rdi), %eax
	cmpw	$255, %ax
	ja	.L2419
	cltq
	leaq	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE(%rip), %rdx
	testb	$8, (%rdx,%rax)
	movq	%rsi, %rax
	cmovne	%rdi, %rax
	ret
.L2383:
	movzwl	(%rdi), %eax
	cmpw	$255, %ax
	ja	.L2420
	movslq	%eax, %rcx
	leaq	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE(%rip), %r8
	movq	%rdi, %rax
	testb	$8, (%r8,%rcx)
	je	.L2386
.L2370:
	ret
	.p2align 4,,10
	.p2align 3
.L2390:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2412:
	leaq	2(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2414:
	leaq	4(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2416:
	leaq	6(%rdi), %rax
	ret
.L2419:
	orl	%eax, (%rdx)
	jmp	.L2410
.L2418:
	orl	%eax, (%rdx)
	jmp	.L2388
	.cfi_endproc
.LFE21956:
	.size	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE14ScanJsonStringEbEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE14ScanJsonStringEbEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag
	.section	.text._ZN2v88internal10JsonParserItE14ScanJsonStringEb,"axG",@progbits,_ZN2v88internal10JsonParserItE14ScanJsonStringEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE14ScanJsonStringEb
	.type	_ZN2v88internal10JsonParserItE14ScanJsonStringEb, @function
_ZN2v88internal10JsonParserItE14ScanJsonStringEb:
.LFB19850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE(%rip), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-72(%rbp), %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	%esi, -104(%rbp)
	movq	48(%rdi), %rdi
	movq	56(%r15), %rsi
	movq	%rdi, %rbx
	subq	64(%r15), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -72(%rbp)
	sarq	%rbx
	movb	$0, -97(%rbp)
	movl	%ebx, %r13d
.L2435:
	movq	%r12, %rdx
	call	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE14ScanJsonStringEbEUltE_EEET_SB_SB_T0_St26random_access_iterator_tag
	movq	56(%r15), %rsi
	movq	%rax, 48(%r15)
	cmpq	%rsi, %rax
	je	.L2441
	movzwl	(%rax), %edx
	cmpw	$34, %dx
	je	.L2442
	cmpw	$92, %dx
	jne	.L2426
	leaq	2(%rax), %rdx
	movq	%rdx, 48(%r15)
	cmpq	%rdx, %rsi
	je	.L2437
	movzwl	2(%rax), %r9d
	cmpw	$255, %r9w
	ja	.L2427
	movslq	%r9d, %rax
	movzbl	(%r14,%rax), %eax
	andl	$7, %eax
	cmpb	$7, %al
	jne	.L2443
	movq	%r15, %rdi
	call	_ZN2v88internal10JsonParserItE20ScanUnicodeCharacterEv
	movl	%eax, %esi
	cmpl	$-1, %eax
	jne	.L2432
	movq	48(%r15), %rax
	cmpq	56(%r15), %rax
	je	.L2440
	movzwl	(%rax), %esi
.L2440:
	movq	%r15, %rdi
	call	_ZN2v88internal10JsonParserItE25ReportUnexpectedCharacterEi
.L2423:
	andb	$-16, -60(%rbp)
	movq	$0, -68(%rbp)
.L2425:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	-68(%rbp), %rax
	movl	-60(%rbp), %edx
	jne	.L2444
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2443:
	.cfi_restore_state
	testb	%al, %al
	jne	.L2445
	movl	%r9d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal10JsonParserItE25ReportUnexpectedCharacterEi
	andb	$-16, -60(%rbp)
	movq	$0, -68(%rbp)
	jmp	.L2425
	.p2align 4,,10
	.p2align 3
.L2432:
	orl	%eax, -72(%rbp)
	xorl	%eax, %eax
	cmpl	$65536, %esi
	movq	48(%r15), %rdx
	setl	%al
	movq	56(%r15), %rsi
	leal	4(%r13,%rax), %r13d
.L2431:
	leaq	2(%rdx), %rdi
	movb	$1, -97(%rbp)
	movq	%rdi, 48(%r15)
	jmp	.L2435
	.p2align 4,,10
	.p2align 3
.L2445:
	addl	$1, %r13d
	jmp	.L2431
	.p2align 4,,10
	.p2align 3
.L2441:
	movl	$-1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal10JsonParserItE25ReportUnexpectedCharacterEi
	jmp	.L2423
	.p2align 4,,10
	.p2align 3
.L2437:
	movl	$-1, %r9d
.L2427:
	movl	%r9d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal10JsonParserItE25ReportUnexpectedCharacterEi
	jmp	.L2423
	.p2align 4,,10
	.p2align 3
.L2442:
	movq	%rax, %rdx
	subq	64(%r15), %rdx
	addq	$2, %rax
	movzbl	-104(%rbp), %esi
	sarq	%rdx
	movq	%rax, 48(%r15)
	movzbl	-97(%rbp), %r15d
	subl	%r13d, %edx
	cmpl	$255, -72(%rbp)
	movl	%ebx, -68(%rbp)
	setle	%cl
	cmpl	$10, %edx
	movl	%edx, -64(%rbp)
	setle	%al
	sall	$2, %r15d
	orl	%eax, %esi
	leal	(%rsi,%rsi), %eax
	orl	%ecx, %eax
	orl	%eax, %r15d
	movzbl	-60(%rbp), %eax
	andl	$15, %r15d
	andl	$-16, %eax
	orl	%eax, %r15d
	movb	%r15b, -60(%rbp)
	jmp	.L2425
	.p2align 4,,10
	.p2align 3
.L2426:
	movzwl	%dx, %esi
	jmp	.L2440
.L2444:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19850:
	.size	_ZN2v88internal10JsonParserItE14ScanJsonStringEb, .-_ZN2v88internal10JsonParserItE14ScanJsonStringEb
	.section	.text._ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE19ScanJsonPropertyKeyEPNS8_16JsonContinuationEEUltE_EEET_SD_SD_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE19ScanJsonPropertyKeyEPNS8_16JsonContinuationEEUltE_EEET_SD_SD_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE19ScanJsonPropertyKeyEPNS8_16JsonContinuationEEUltE_EEET_SD_SD_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE19ScanJsonPropertyKeyEPNS8_16JsonContinuationEEUltE_EEET_SD_SD_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE19ScanJsonPropertyKeyEPNS8_16JsonContinuationEEUltE_EEET_SD_SD_T0_St26random_access_iterator_tag:
.LFB21960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	subq	%rdi, %rax
	movq	%rax, %rcx
	sarq	$3, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	sarq	%rcx
	pushq	%rbx
	.cfi_offset 3, -24
	testq	%rax, %rax
	jle	.L2447
	leaq	(%rdi,%rax,8), %r10
	movq	%rdi, %r8
	movl	$429496729, %r9d
.L2449:
	movzwl	(%r8), %eax
	leal	-48(%rax), %ecx
	cmpl	$9, %ecx
	ja	.L2446
	subl	$45, %eax
	movl	%r9d, %r11d
	movl	(%rdx), %edi
	sarl	$3, %eax
	subl	%eax, %r11d
	cmpl	%r11d, %edi
	ja	.L2446
	leal	(%rdi,%rdi,4), %eax
	leaq	2(%r8), %r11
	leal	(%rcx,%rax,2), %ecx
	movl	%ecx, (%rdx)
	movzwl	2(%r8), %eax
	leal	-48(%rax), %edi
	cmpl	$9, %edi
	ja	.L2458
	subl	$45, %eax
	movl	%r9d, %ebx
	sarl	$3, %eax
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	ja	.L2458
	leal	(%rcx,%rcx,4), %eax
	leaq	4(%r8), %r11
	leal	(%rdi,%rax,2), %ecx
	movl	%ecx, (%rdx)
	movzwl	4(%r8), %eax
	leal	-48(%rax), %edi
	cmpl	$9, %edi
	ja	.L2458
	subl	$45, %eax
	movl	%r9d, %ebx
	sarl	$3, %eax
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	ja	.L2458
	leal	(%rcx,%rcx,4), %eax
	leaq	6(%r8), %r11
	leal	(%rdi,%rax,2), %ecx
	movl	%ecx, (%rdx)
	movzwl	6(%r8), %eax
	leal	-48(%rax), %edi
	cmpl	$9, %edi
	ja	.L2458
	subl	$45, %eax
	movl	%r9d, %ebx
	sarl	$3, %eax
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	ja	.L2458
	leal	(%rcx,%rcx,4), %eax
	addq	$8, %r8
	leal	(%rdi,%rax,2), %eax
	movl	%eax, (%rdx)
	cmpq	%r8, %r10
	jne	.L2449
	movq	%rsi, %rcx
	movq	%r10, %rdi
	subq	%r10, %rcx
	sarq	%rcx
	.p2align 4,,10
	.p2align 3
.L2447:
	cmpq	$2, %rcx
	je	.L2450
	cmpq	$3, %rcx
	je	.L2451
	cmpq	$1, %rcx
	je	.L2452
.L2473:
	movq	%rsi, %r8
.L2446:
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2458:
	.cfi_restore_state
	movq	%r11, %r8
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%r8, %rax
	ret
.L2451:
	.cfi_restore_state
	movzwl	(%rdi), %eax
	movq	%rdi, %r8
	leal	-48(%rax), %ecx
	cmpl	$9, %ecx
	ja	.L2446
	subl	$45, %eax
	movl	$429496729, %r8d
	movl	(%rdx), %r9d
	sarl	$3, %eax
	subl	%eax, %r8d
	movl	%r8d, %eax
	movq	%rdi, %r8
	cmpl	%eax, %r9d
	ja	.L2446
	leal	(%r9,%r9,4), %eax
	addq	$2, %rdi
	leal	(%rcx,%rax,2), %eax
	movl	%eax, (%rdx)
.L2450:
	movzwl	(%rdi), %eax
	movq	%rdi, %r8
	leal	-48(%rax), %ecx
	cmpl	$9, %ecx
	ja	.L2446
	subl	$45, %eax
	movl	$429496729, %r8d
	movl	(%rdx), %r9d
	sarl	$3, %eax
	subl	%eax, %r8d
	movl	%r8d, %eax
	movq	%rdi, %r8
	cmpl	%eax, %r9d
	ja	.L2446
	leal	(%r9,%r9,4), %eax
	addq	$2, %rdi
	leal	(%rcx,%rax,2), %eax
	movl	%eax, (%rdx)
.L2452:
	movzwl	(%rdi), %eax
	movq	%rdi, %r8
	leal	-48(%rax), %r9d
	cmpl	$9, %r9d
	ja	.L2446
	subl	$45, %eax
	movl	$429496729, %ecx
	movl	(%rdx), %edi
	sarl	$3, %eax
	subl	%eax, %ecx
	cmpl	%ecx, %edi
	ja	.L2446
	leal	(%rdi,%rdi,4), %eax
	leal	(%r9,%rax,2), %eax
	movl	%eax, (%rdx)
	jmp	.L2473
	.cfi_endproc
.LFE21960:
	.size	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE19ScanJsonPropertyKeyEPNS8_16JsonContinuationEEUltE_EEET_SD_SD_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE19ScanJsonPropertyKeyEPNS8_16JsonContinuationEEUltE_EEET_SD_SD_T0_St26random_access_iterator_tag
	.section	.text._ZN2v88internal10JsonParserItE19ScanJsonPropertyKeyEPNS2_16JsonContinuationE,"axG",@progbits,_ZN2v88internal10JsonParserItE19ScanJsonPropertyKeyEPNS2_16JsonContinuationE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE19ScanJsonPropertyKeyEPNS2_16JsonContinuationE
	.type	_ZN2v88internal10JsonParserItE19ScanJsonPropertyKeyEPNS2_16JsonContinuationE, @function
_ZN2v88internal10JsonParserItE19ScanJsonPropertyKeyEPNS2_16JsonContinuationE:
.LFB19852:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	48(%rdi), %r14
	movq	56(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	%rdx, %r14
	je	.L2476
	movzwl	(%r14), %eax
	movq	%rsi, %rbx
	cmpl	$92, %eax
	je	.L2487
.L2477:
	leal	-48(%rax), %edx
	cmpl	$9, %edx
	ja	.L2476
	movq	48(%r12), %rdi
	movq	56(%r12), %rsi
	cmpl	$48, %eax
	jne	.L2479
	leaq	2(%rdi), %rax
	movq	%rax, 48(%r12)
	cmpq	%rsi, %rax
	je	.L2476
	cmpw	$34, 2(%rdi)
	je	.L2488
	.p2align 4,,10
	.p2align 3
.L2476:
	movq	%r14, 48(%r12)
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE14ScanJsonStringEb
	movq	%rax, -52(%rbp)
	movl	%edx, -44(%rbp)
.L2480:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	movl	-44(%rbp), %edx
	jne	.L2489
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2479:
	.cfi_restore_state
	movl	%edx, -56(%rbp)
	leaq	-56(%rbp), %r13
.L2483:
	movq	%r13, %rdx
	addq	$2, %rdi
	call	_ZSt9__find_ifIPKtN9__gnu_cxx5__ops10_Iter_predIZN2v88internal10JsonParserItE19ScanJsonPropertyKeyEPNS8_16JsonContinuationEEUltE_EEET_SD_SD_T0_St26random_access_iterator_tag
	movq	56(%r12), %rdx
	movq	%rax, 48(%r12)
	cmpq	%rax, %rdx
	je	.L2476
	movzwl	(%rax), %ecx
	cmpw	$34, %cx
	je	.L2490
	cmpw	$92, %cx
	jne	.L2476
	leaq	2(%rax), %rcx
	movq	%rcx, 48(%r12)
	cmpq	%rcx, %rdx
	je	.L2476
	cmpw	$117, 2(%rax)
	jne	.L2476
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE20ScanUnicodeCharacterEv
	leal	-48(%rax), %edx
	cmpl	$9, %edx
	ja	.L2476
	subl	$45, %eax
	movl	-56(%rbp), %ecx
	sarl	$3, %eax
	cmpl	$1, %eax
	sbbl	%eax, %eax
	notl	%eax
	addl	$429496729, %eax
	cmpl	%eax, %ecx
	ja	.L2476
	leal	(%rcx,%rcx,4), %eax
	movq	56(%r12), %rsi
	movq	48(%r12), %rdi
	leal	(%rdx,%rax,2), %eax
	movl	%eax, -56(%rbp)
	jmp	.L2483
	.p2align 4,,10
	.p2align 3
.L2487:
	leaq	2(%r14), %rax
	movq	%rax, 48(%rdi)
	cmpq	%rax, %rdx
	je	.L2476
	cmpw	$117, 2(%r14)
	jne	.L2476
	call	_ZN2v88internal10JsonParserItE20ScanUnicodeCharacterEv
	jmp	.L2477
	.p2align 4,,10
	.p2align 3
.L2488:
	addq	$4, %rdi
	movq	$0, -52(%rbp)
	movq	%rdi, 48(%r12)
	addl	$1, 32(%rbx)
.L2486:
	movzbl	-44(%rbp), %eax
	andl	$-16, %eax
	orl	$8, %eax
	movb	%al, -44(%rbp)
	movq	-52(%rbp), %rax
	jmp	.L2480
	.p2align 4,,10
	.p2align 3
.L2490:
	addq	$2, %rax
	movl	$0, -48(%rbp)
	movq	%rax, 48(%r12)
	movl	-56(%rbp), %eax
	addl	$1, 32(%rbx)
	cmpl	%eax, 28(%rbx)
	movl	%eax, %edx
	cmovnb	28(%rbx), %edx
	movl	%eax, -52(%rbp)
	movl	%edx, 28(%rbx)
	jmp	.L2486
.L2489:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19852:
	.size	_ZN2v88internal10JsonParserItE19ScanJsonPropertyKeyEPNS2_16JsonContinuationE, .-_ZN2v88internal10JsonParserItE19ScanJsonPropertyKeyEPNS2_16JsonContinuationE
	.section	.text._ZN2v88internal10JsonParserItE14ParseJsonValueEv,"axG",@progbits,_ZN2v88internal10JsonParserItE14ParseJsonValueEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE14ParseJsonValueEv
	.type	_ZN2v88internal10JsonParserItE14ParseJsonValueEv, @function
_ZN2v88internal10JsonParserItE14ParseJsonValueEv:
.LFB19857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$640, %edi
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -112(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -144(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -128(%rbp)
	call	_Znwm@PLT
	movq	-184(%rbp), %r14
	movq	-192(%rbp), %r13
	movq	%rax, %rbx
	cmpq	%r13, %r14
	je	.L2492
.L2494:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2493
	movq	8(%r13), %rdx
	movq	16(%r13), %rax
	subl	$1, 41104(%rdi)
	movq	%rdx, 41088(%rdi)
	cmpq	41096(%rdi), %rax
	je	.L2493
	movq	%rax, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2493:
	addq	$40, %r13
	cmpq	%r13, %r14
	jne	.L2494
	movq	-192(%rbp), %r13
.L2492:
	testq	%r13, %r13
	je	.L2495
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2495:
	movq	-160(%rbp), %rdx
	movq	-144(%rbp), %rax
	movq	%rbx, %xmm0
	movabsq	$-6148914691236517205, %rcx
	punpcklqdq	%xmm0, %xmm0
	addq	$640, %rbx
	subq	%rdx, %rax
	movq	%rbx, -176(%rbp)
	sarq	$3, %rax
	movaps	%xmm0, -192(%rbp)
	imulq	%rcx, %rax
	cmpq	$15, %rax
	jbe	.L2702
.L2496:
	movq	-128(%rbp), %rdx
	movq	-112(%rbp), %rax
	subq	%rdx, %rax
	cmpq	$127, %rax
	jbe	.L2703
.L2501:
	movq	(%r12), %rax
	movq	$0, -72(%rbp)
	leaq	-160(%rbp), %r14
	leaq	.L2513(%rip), %rbx
	movl	$0, -64(%rbp)
	movq	41088(%rax), %rdx
	movq	%rax, -96(%rbp)
	addl	$1, 41104(%rax)
	movq	%rdx, -88(%rbp)
	movq	41096(%rax), %rdx
	movq	$0, -216(%rbp)
	movq	%rdx, -80(%rbp)
.L2510:
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE14SkipWhitespaceEv
	cmpb	$13, 16(%r12)
	ja	.L2522
	movzbl	16(%r12), %eax
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10JsonParserItE14ParseJsonValueEv,"aG",@progbits,_ZN2v88internal10JsonParserItE14ParseJsonValueEv,comdat
	.align 4
	.align 4
.L2513:
	.long	.L2521-.L2513
	.long	.L2520-.L2513
	.long	.L2519-.L2513
	.long	.L2512-.L2513
	.long	.L2518-.L2513
	.long	.L2512-.L2513
	.long	.L2517-.L2513
	.long	.L2516-.L2513
	.long	.L2515-.L2513
	.long	.L2514-.L2513
	.long	.L2512-.L2513
	.long	.L2512-.L2513
	.long	.L2512-.L2513
	.long	.L2512-.L2513
	.section	.text._ZN2v88internal10JsonParserItE14ParseJsonValueEv,"axG",@progbits,_ZN2v88internal10JsonParserItE14ParseJsonValueEv,comdat
	.p2align 4,,10
	.p2align 3
.L2512:
	movq	48(%r12), %rax
	cmpq	56(%r12), %rax
	je	.L2631
	movzwl	(%rax), %esi
.L2563:
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE25ReportUnexpectedCharacterEi
	movq	-184(%rbp), %rbx
	cmpq	%rbx, -192(%rbp)
	jne	.L2564
	jmp	.L2569
	.p2align 4,,10
	.p2align 3
.L2566:
	movq	-88(%rbp), %rdx
	subl	$1, 41104(%rdi)
	movq	-80(%rbp), %rax
	movq	%rdx, 41088(%rdi)
	cmpq	41096(%rdi), %rax
	je	.L2567
	movq	%rax, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2567:
	movq	-32(%rbx), %rax
	movq	%rax, -88(%rbp)
	movq	-24(%rbx), %rax
	movq	%rax, -80(%rbp)
	movzbl	-16(%rbx), %edx
	movq	$0, -40(%rbx)
	movzbl	-72(%rbp), %eax
	andl	$3, %edx
	andl	$-4, %eax
	orl	%edx, %eax
	movb	%al, -72(%rbp)
	movl	-16(%rbx), %edx
	movl	-72(%rbp), %eax
	andl	$-4, %edx
	andl	$3, %eax
	orl	%edx, %eax
	movl	%eax, -72(%rbp)
	movl	-12(%rbx), %eax
	movl	%eax, -68(%rbp)
	movl	-8(%rbx), %eax
	movq	-184(%rbp), %rbx
	movl	%eax, -64(%rbp)
	movq	%rbx, %rax
	subq	$40, %rbx
	movq	%rbx, -184(%rbp)
	movq	-40(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2568
	movq	-24(%rax), %rdx
	movq	-32(%rax), %rax
	subl	$1, 41104(%rdi)
	movq	%rax, 41088(%rdi)
	cmpq	41096(%rdi), %rdx
	je	.L2568
	movq	%rdx, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rbx
.L2568:
	cmpq	-192(%rbp), %rbx
	je	.L2569
.L2564:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2566
	movq	-40(%rbx), %rax
	movq	%rax, -96(%rbp)
	jmp	.L2567
	.p2align 4,,10
	.p2align 3
.L2519:
	addq	$2, 48(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE14SkipWhitespaceEv
	cmpb	$3, 16(%r12)
	je	.L2523
	movq	-184(%rbp), %rsi
	cmpq	-176(%rbp), %rsi
	je	.L2524
	movq	-96(%rbp), %rax
	leaq	-96(%rbp), %r13
	movabsq	$-6148914691236517205, %rcx
	movq	%rax, (%rsi)
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-80(%rbp), %rax
	addq	$40, -184(%rbp)
	movq	%rax, 16(%rsi)
	movzbl	24(%rsi), %eax
	movzbl	-72(%rbp), %edx
	andl	$-4, %eax
	andl	$3, %edx
	orl	%edx, %eax
	movb	%al, 24(%rsi)
	movl	-72(%rbp), %edx
	movl	24(%rsi), %eax
	andl	$-4, %edx
	andl	$3, %eax
	orl	%edx, %eax
	movl	%eax, 24(%rsi)
	movl	-68(%rbp), %eax
	movl	%eax, 28(%rsi)
	movl	-64(%rbp), %eax
	movl	%eax, 32(%rsi)
	movq	-152(%rbp), %rax
	movq	(%r12), %rdx
	subq	-160(%rbp), %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movq	41096(%rdx), %rcx
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r15
	movq	%rcx, -232(%rbp)
	andl	$1073741823, %eax
.L2525:
	movq	%rdx, -96(%rbp)
.L2527:
	movq	%r15, %xmm0
	leaq	1(,%rax,4), %rax
	movq	%r12, %rdi
	movl	$0, -64(%rbp)
	movhps	-232(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movups	%xmm0, -88(%rbp)
	call	_ZN2v88internal10JsonParserItE14SkipWhitespaceEv
	movzbl	16(%r12), %esi
	cmpb	$1, %sil
	jne	.L2528
	addq	$2, 48(%r12)
	jmp	.L2696
	.p2align 4,,10
	.p2align 3
.L2520:
	addq	$2, 48(%r12)
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE14ScanJsonStringEb
	leaq	-204(%rbp), %rsi
	movq	%r12, %rdi
	movl	%edx, -196(%rbp)
	xorl	%edx, %edx
	movq	%rax, -204(%rbp)
	call	_ZN2v88internal10JsonParserItE10MakeStringERKNS0_10JsonStringENS0_6HandleINS0_6StringEEE
	movq	%rax, -216(%rbp)
	.p2align 4,,10
	.p2align 3
.L2522:
	movzbl	-72(%rbp), %eax
	andl	$3, %eax
	cmpb	$1, %al
	je	.L2570
.L2705:
	cmpb	$2, %al
	je	.L2571
	testb	%al, %al
	jne	.L2510
	movq	-216(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_
	movq	%rax, %r13
.L2565:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2609
	movq	-88(%rbp), %rdx
	subl	$1, 41104(%rdi)
	movq	-80(%rbp), %rax
	movq	%rdx, 41088(%rdi)
	cmpq	41096(%rdi), %rax
	je	.L2609
	movq	%rax, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2609:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2610
	call	_ZdlPv@PLT
.L2610:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2611
	call	_ZdlPv@PLT
.L2611:
	movq	-184(%rbp), %rbx
	movq	-192(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2612
	.p2align 4,,10
	.p2align 3
.L2614:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2613
	movq	8(%r12), %rdx
	movq	16(%r12), %rax
	subl	$1, 41104(%rdi)
	movq	%rdx, 41088(%rdi)
	cmpq	41096(%rdi), %rax
	je	.L2613
	movq	%rax, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2613:
	addq	$40, %r12
	cmpq	%r12, %rbx
	jne	.L2614
	movq	-192(%rbp), %r12
.L2612:
	testq	%r12, %r12
	je	.L2615
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2615:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2704
	addq	$200, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2521:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE15ParseJsonNumberEv
	movq	%rax, -216(%rbp)
	movzbl	-72(%rbp), %eax
	andl	$3, %eax
	cmpb	$1, %al
	jne	.L2705
	.p2align 4,,10
	.p2align 3
.L2570:
	movq	-152(%rbp), %rax
	movq	-216(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rdx, -8(%rax)
	call	_ZN2v88internal10JsonParserItE14SkipWhitespaceEv
	cmpb	$11, 16(%r12)
	jne	.L2572
	addq	$2, 48(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE14SkipWhitespaceEv
	movzbl	16(%r12), %esi
	cmpb	$1, %sil
	jne	.L2706
	addq	$2, 48(%r12)
.L2577:
	leaq	-96(%rbp), %r13
.L2696:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE19ScanJsonPropertyKeyEPNS2_16JsonContinuationE
	movq	-152(%rbp), %rsi
	movq	%rax, -204(%rbp)
	movl	%edx, -196(%rbp)
	cmpq	-144(%rbp), %rsi
	je	.L2578
	movq	%rax, (%rsi)
	movl	-196(%rbp), %eax
	movq	$0, 16(%rsi)
	movl	%eax, 8(%rsi)
	addq	$24, -152(%rbp)
.L2579:
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE14SkipWhitespaceEv
	movzbl	16(%r12), %esi
	cmpb	$10, %sil
	jne	.L2580
.L2688:
	addq	$2, 48(%r12)
	jmp	.L2510
	.p2align 4,,10
	.p2align 3
.L2515:
	movq	48(%r12), %rax
	movq	56(%r12), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rcx
	sarq	%rcx
	cmpq	$7, %rdx
	jbe	.L2555
	cmpw	$117, 2(%rax)
	jne	.L2556
	cmpw	$108, 4(%rax)
	jne	.L2556
	cmpw	$108, 6(%rax)
	jne	.L2556
	addq	$8, %rax
	movq	%rax, 48(%r12)
.L2557:
	movq	(%r12), %rax
	addq	$104, %rax
	movq	%rax, -216(%rbp)
	jmp	.L2522
	.p2align 4,,10
	.p2align 3
.L2516:
	movq	48(%r12), %rax
	movq	56(%r12), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rcx
	sarq	%rcx
	cmpq	$9, %rdx
	jbe	.L2547
	cmpw	$97, 2(%rax)
	jne	.L2548
	cmpw	$108, 4(%rax)
	jne	.L2548
	cmpw	$115, 6(%rax)
	jne	.L2548
	cmpw	$101, 8(%rax)
	jne	.L2548
	addq	$10, %rax
	movq	%rax, 48(%r12)
.L2549:
	movq	(%r12), %rax
	addq	$120, %rax
	movq	%rax, -216(%rbp)
	jmp	.L2522
	.p2align 4,,10
	.p2align 3
.L2517:
	movq	48(%r12), %rax
	movq	56(%r12), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rcx
	sarq	%rcx
	cmpq	$7, %rdx
	jbe	.L2539
	cmpw	$114, 2(%rax)
	jne	.L2540
	cmpw	$117, 4(%rax)
	jne	.L2540
	cmpw	$101, 6(%rax)
	jne	.L2540
	addq	$8, %rax
	movq	%rax, 48(%r12)
.L2541:
	movq	(%r12), %rax
	addq	$112, %rax
	movq	%rax, -216(%rbp)
	jmp	.L2522
	.p2align 4,,10
	.p2align 3
.L2518:
	addq	$2, 48(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE14SkipWhitespaceEv
	cmpb	$5, 16(%r12)
	je	.L2707
	movq	-184(%rbp), %rsi
	cmpq	-176(%rbp), %rsi
	je	.L2535
	movq	-96(%rbp), %rax
	movq	%rax, (%rsi)
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-80(%rbp), %rax
	addq	$40, -184(%rbp)
	movq	%rax, 16(%rsi)
	movzbl	24(%rsi), %eax
	movzbl	-72(%rbp), %edx
	andl	$-4, %eax
	andl	$3, %edx
	orl	%edx, %eax
	movb	%al, 24(%rsi)
	movl	-72(%rbp), %edx
	movl	24(%rsi), %eax
	andl	$-4, %edx
	andl	$3, %eax
	orl	%edx, %eax
	movl	%eax, 24(%rsi)
	movl	-68(%rbp), %eax
	movl	%eax, 28(%rsi)
	movl	-64(%rbp), %eax
	movl	%eax, 32(%rsi)
	movq	(%r12), %rdx
	movq	-120(%rbp), %rax
	subq	-128(%rbp), %rax
	movq	41096(%rdx), %rcx
	addl	$1, 41104(%rdx)
	sarq	$3, %rax
	movl	%eax, %r13d
	movq	41088(%rdx), %r15
	movq	%rcx, -232(%rbp)
	andl	$1073741823, %r13d
.L2536:
	movq	%rdx, -96(%rbp)
.L2538:
	movq	%r15, %xmm0
	movl	%r13d, %eax
	movl	$0, -64(%rbp)
	movhps	-232(%rbp), %xmm0
	leaq	2(,%rax,4), %rax
	movq	%rax, -72(%rbp)
	movups	%xmm0, -88(%rbp)
	jmp	.L2510
	.p2align 4,,10
	.p2align 3
.L2571:
	leaq	-128(%rbp), %r15
	leaq	-216(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE12emplace_backIJRS4_EEEvDpOT_
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE14SkipWhitespaceEv
	cmpb	$11, 16(%r12)
	je	.L2688
	leaq	-96(%rbp), %r13
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal10JsonParserItE14BuildJsonArrayERKNS2_16JsonContinuationERKSt6vectorINS0_6HandleINS0_6ObjectEEESaIS9_EE
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rcx
	movq	%rax, -216(%rbp)
	movl	-72(%rbp), %esi
	movq	%rdx, %rax
	subq	%rcx, %rax
	shrl	$2, %esi
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L2708
	jnb	.L2603
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdx
	je	.L2603
	movq	%rax, -120(%rbp)
.L2603:
	movzbl	16(%r12), %esi
	cmpb	$5, %sil
	je	.L2701
.L2604:
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE
	jmp	.L2605
	.p2align 4,,10
	.p2align 3
.L2569:
	xorl	%r13d, %r13d
	jmp	.L2565
	.p2align 4,,10
	.p2align 3
.L2703:
	movq	-120(%rbp), %r13
	movl	$128, %edi
	subq	%rdx, %r13
	call	_Znwm@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	%rdi, %rcx
	je	.L2509
	leaq	-8(%rcx), %rax
	leaq	15(%rdi), %rdx
	subq	%rdi, %rax
	subq	%rbx, %rdx
	shrq	$3, %rax
	cmpq	$30, %rdx
	jbe	.L2630
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rax
	je	.L2630
	addq	$1, %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L2507:
	movdqu	(%rdi,%rdx), %xmm2
	movups	%xmm2, (%rbx,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L2507
	movq	%rax, %rsi
	andq	$-2, %rsi
	leaq	0(,%rsi,8), %rdx
	leaq	(%rdi,%rdx), %rcx
	addq	%rbx, %rdx
	cmpq	%rsi, %rax
	je	.L2503
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
.L2503:
	call	_ZdlPv@PLT
.L2504:
	addq	%rbx, %r13
	movq	%rbx, -128(%rbp)
	subq	$-128, %rbx
	movq	%r13, -120(%rbp)
	movq	%rbx, -112(%rbp)
	jmp	.L2501
	.p2align 4,,10
	.p2align 3
.L2702:
	movq	-152(%rbp), %rbx
	movl	$384, %edi
	subq	%rdx, %rbx
	call	_Znwm@PLT
	movq	-160(%rbp), %r8
	movq	-152(%rbp), %rdi
	movq	%rax, %r13
	movq	%rax, %rcx
	movq	%r8, %rdx
	cmpq	%r8, %rdi
	je	.L2500
	.p2align 4,,10
	.p2align 3
.L2497:
	movdqu	(%rdx), %xmm1
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rdi
	jne	.L2497
.L2500:
	testq	%r8, %r8
	je	.L2499
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L2499:
	addq	%r13, %rbx
	movq	%r13, -160(%rbp)
	addq	$384, %r13
	movq	%rbx, -152(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L2496
	.p2align 4,,10
	.p2align 3
.L2572:
	movq	-184(%rbp), %rax
	xorl	%ecx, %ecx
	cmpq	%rax, -192(%rbp)
	je	.L2589
	movzbl	-16(%rax), %edx
	andl	$3, %edx
	cmpb	$2, %dl
	je	.L2709
.L2589:
	leaq	-96(%rbp), %r13
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal10JsonParserItE15BuildJsonObjectERKNS2_16JsonContinuationERKSt6vectorINS0_12JsonPropertyESaIS7_EENS0_6HandleINS0_3MapEEE
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %rsi
	movabsq	$-6148914691236517205, %rdi
	movq	%rax, -216(%rbp)
	movl	-72(%rbp), %eax
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	shrl	$2, %eax
	sarq	$3, %rdx
	imulq	%rdi, %rdx
	cmpq	%rdx, %rax
	ja	.L2710
	jb	.L2711
.L2594:
	movzbl	16(%r12), %esi
	cmpb	$3, %sil
	jne	.L2604
.L2701:
	addq	$2, 48(%r12)
.L2605:
	movq	-216(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope14CloseAndEscapeINS0_6ObjectEEENS0_6HandleIT_EES6_
	movq	-96(%rbp), %rdi
	movq	-184(%rbp), %r15
	movq	%rax, -216(%rbp)
	testq	%rdi, %rdi
	je	.L2712
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rax
	subl	$1, 41104(%rdi)
	movq	%rdx, 41088(%rdi)
	cmpq	41096(%rdi), %rax
	je	.L2607
	movq	%rax, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2607:
	movq	-32(%r15), %rax
	movq	%rax, -88(%rbp)
	movq	-24(%r15), %rax
	movq	%rax, -80(%rbp)
	movzbl	-16(%r15), %edx
	movq	$0, -40(%r15)
	movzbl	-72(%rbp), %eax
	andl	$3, %edx
	andl	$-4, %eax
	orl	%edx, %eax
	movb	%al, -72(%rbp)
	movl	-16(%r15), %edx
	movl	-72(%rbp), %eax
	andl	$-4, %edx
	andl	$3, %eax
	orl	%edx, %eax
	movl	%eax, -72(%rbp)
	movl	-12(%r15), %eax
	movl	%eax, -68(%rbp)
	movl	-8(%r15), %eax
	movl	%eax, -64(%rbp)
	movq	-184(%rbp), %rax
	leaq	-40(%rax), %rdx
	movq	%rdx, -184(%rbp)
	movq	-40(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2522
	movq	-24(%rax), %rdx
	movq	-32(%rax), %rax
	subl	$1, 41104(%rdi)
	movq	%rax, 41088(%rdi)
	cmpq	41096(%rdi), %rdx
	je	.L2522
	movq	%rdx, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L2522
	.p2align 4,,10
	.p2align 3
.L2523:
	addq	$2, 48(%r12)
	movq	24(%r12), %rsi
	xorl	%edx, %edx
	movq	(%r12), %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%rax, -216(%rbp)
	jmp	.L2522
	.p2align 4,,10
	.p2align 3
.L2707:
	addq	$2, 48(%r12)
	movq	(%r12), %rdi
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	movq	%rax, -216(%rbp)
	jmp	.L2522
	.p2align 4,,10
	.p2align 3
.L2580:
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE
	jmp	.L2510
	.p2align 4,,10
	.p2align 3
.L2630:
	movq	%rbx, %rdx
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L2505:
	movq	(%rax), %rsi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rsi, -8(%rdx)
	cmpq	%rcx, %rax
	jne	.L2505
.L2509:
	testq	%rdi, %rdi
	je	.L2504
	jmp	.L2503
	.p2align 4,,10
	.p2align 3
.L2578:
	leaq	-204(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIN2v88internal12JsonPropertyESaIS2_EE17_M_realloc_insertIJNS1_10JsonStringEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L2579
.L2535:
	leaq	-96(%rbp), %r13
	leaq	-192(%rbp), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIN2v88internal10JsonParserItE16JsonContinuationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	(%r12), %rdx
	movq	-120(%rbp), %rax
	subq	-128(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	41096(%rdx), %rcx
	sarq	$3, %rax
	addl	$1, 41104(%rdx)
	movl	%eax, %r13d
	movq	41088(%rdx), %r15
	movq	%rcx, -232(%rbp)
	andl	$1073741823, %r13d
	testq	%rdi, %rdi
	je	.L2536
	movq	-88(%rbp), %rsi
	subl	$1, 41104(%rdi)
	movq	-80(%rbp), %rdx
	movq	%rsi, 41088(%rdi)
	cmpq	41096(%rdi), %rdx
	je	.L2538
	movq	%rdx, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L2538
.L2524:
	leaq	-96(%rbp), %r13
	leaq	-192(%rbp), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIN2v88internal10JsonParserItE16JsonContinuationESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-152(%rbp), %rax
	subq	-160(%rbp), %rax
	movabsq	$-6148914691236517205, %rcx
	movq	(%r12), %rdx
	sarq	$3, %rax
	movq	-96(%rbp), %rdi
	imulq	%rcx, %rax
	movq	41096(%rdx), %rcx
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r15
	movq	%rcx, -232(%rbp)
	andl	$1073741823, %eax
	testq	%rdi, %rdi
	je	.L2525
	movq	-88(%rbp), %rsi
	subl	$1, 41104(%rdi)
	movq	-80(%rbp), %rdx
	movq	%rsi, 41088(%rdi)
	cmpq	41096(%rdi), %rdx
	je	.L2527
	movq	%rdx, 41096(%rdi)
	movl	%eax, -236(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-236(%rbp), %eax
	jmp	.L2527
	.p2align 4,,10
	.p2align 3
.L2548:
	leaq	2(%rax), %rdx
	movq	%rdx, 48(%r12)
	movzwl	2(%rax), %esi
	cmpl	$97, %esi
	jne	.L2551
	leaq	4(%rax), %rcx
	movq	%rcx, 48(%r12)
	movzwl	2(%rdx), %esi
	cmpl	$108, %esi
	jne	.L2551
	addq	$6, %rax
	movl	$4, %edx
	movq	%rax, 48(%r12)
.L2626:
	movzwl	(%rax), %esi
	cmpl	$115, %esi
	jne	.L2551
	leaq	2(%rax), %rcx
	movq	%rcx, 48(%r12)
	cmpq	$4, %rdx
	jne	.L2552
	movzwl	2(%rax), %esi
	cmpl	$101, %esi
	jne	.L2551
	addq	$4, %rax
	movq	%rax, 48(%r12)
.L2552:
	movl	$13, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE
	jmp	.L2549
	.p2align 4,,10
	.p2align 3
.L2706:
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE
	jmp	.L2577
	.p2align 4,,10
	.p2align 3
.L2514:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2711:
	leaq	(%rax,%rax,2), %rax
	leaq	(%rsi,%rax,8), %rax
	cmpq	%rax, %rcx
	je	.L2594
	movq	%rax, -152(%rbp)
	jmp	.L2594
.L2709:
	movq	-120(%rbp), %rsi
	movl	-16(%rax), %eax
	movq	%rsi, %rdx
	subq	-128(%rbp), %rdx
	shrl	$2, %eax
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jnb	.L2589
	movq	-8(%rsi), %rax
	movq	(%rax), %rax
	testb	$1, %al
	je	.L2589
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L2589
	movq	-1(%rax), %r15
	movq	(%r12), %r13
	movl	15(%r15), %eax
	testl	$1048576, %eax
	jne	.L2589
	cmpw	$1057, 11(%r15)
	je	.L2713
.L2584:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2590
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
	jmp	.L2589
	.p2align 4,,10
	.p2align 3
.L2551:
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE25ReportUnexpectedCharacterEi
	jmp	.L2549
	.p2align 4,,10
	.p2align 3
.L2540:
	leaq	2(%rax), %rdx
	movq	%rdx, 48(%r12)
	movzwl	2(%rax), %esi
	cmpl	$114, %esi
	jne	.L2543
	leaq	4(%rax), %rcx
	movq	%rcx, 48(%r12)
	movzwl	2(%rdx), %esi
	cmpl	$117, %esi
	jne	.L2543
	addq	$6, %rax
	movq	%rax, 48(%r12)
.L2625:
	movzwl	(%rax), %esi
	cmpl	$101, %esi
	jne	.L2543
	addq	$2, %rax
	movq	%rax, 48(%r12)
.L2544:
	movl	$13, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE
	jmp	.L2541
	.p2align 4,,10
	.p2align 3
.L2556:
	leaq	2(%rax), %rdx
	movq	%rdx, 48(%r12)
	movzwl	2(%rax), %esi
	cmpl	$117, %esi
	jne	.L2559
	leaq	4(%rax), %rcx
	movq	%rcx, 48(%r12)
	movzwl	2(%rdx), %esi
	cmpl	$108, %esi
	jne	.L2559
	addq	$6, %rax
	movq	%rax, 48(%r12)
.L2627:
	movzwl	(%rax), %esi
	cmpl	$108, %esi
	jne	.L2559
	addq	$2, %rax
	movq	%rax, 48(%r12)
.L2560:
	movl	$13, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE
	jmp	.L2557
	.p2align 4,,10
	.p2align 3
.L2712:
	movq	-40(%r15), %rax
	movq	%rax, -96(%rbp)
	jmp	.L2607
	.p2align 4,,10
	.p2align 3
.L2631:
	movl	$-1, %esi
	jmp	.L2563
.L2543:
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE25ReportUnexpectedCharacterEi
	jmp	.L2541
.L2559:
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE25ReportUnexpectedCharacterEi
	jmp	.L2557
	.p2align 4,,10
	.p2align 3
.L2708:
	subq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEESaIS4_EE17_M_default_appendEm
	jmp	.L2603
.L2528:
	movq	%r12, %rdi
	call	_ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE
	jmp	.L2696
	.p2align 4,,10
	.p2align 3
.L2539:
	leaq	2(%rax), %rdx
	leaq	-1(%rcx), %rdi
	movq	%rdx, 48(%r12)
	cmpq	$1, %rcx
	je	.L2544
	movzwl	2(%rax), %esi
	cmpl	$114, %esi
	jne	.L2543
	leaq	4(%rax), %rcx
	movq	%rcx, 48(%r12)
	cmpq	$1, %rdi
	jbe	.L2544
	movzwl	2(%rdx), %esi
	cmpl	$117, %esi
	jne	.L2543
	addq	$6, %rax
	movq	%rax, 48(%r12)
	cmpq	$2, %rdi
	ja	.L2625
	jmp	.L2544
	.p2align 4,,10
	.p2align 3
.L2555:
	leaq	2(%rax), %rdx
	leaq	-1(%rcx), %rdi
	movq	%rdx, 48(%r12)
	cmpq	$1, %rcx
	je	.L2560
	movzwl	2(%rax), %esi
	cmpl	$117, %esi
	jne	.L2559
	leaq	4(%rax), %rcx
	movq	%rcx, 48(%r12)
	cmpq	$1, %rdi
	jbe	.L2560
	movzwl	2(%rdx), %esi
	cmpl	$108, %esi
	jne	.L2559
	addq	$6, %rax
	movq	%rax, 48(%r12)
	cmpq	$2, %rdi
	ja	.L2627
	jmp	.L2560
	.p2align 4,,10
	.p2align 3
.L2547:
	leaq	2(%rax), %rdi
	movq	%rdi, 48(%r12)
	cmpq	$1, %rcx
	je	.L2552
	subq	$1, %rcx
	movzwl	2(%rax), %esi
	movl	$4, %edx
	cmpq	$4, %rcx
	cmovbe	%rcx, %rdx
	cmpl	$97, %esi
	jne	.L2551
	leaq	4(%rax), %rsi
	movq	%rsi, 48(%r12)
	cmpq	$1, %rcx
	jbe	.L2552
	movzwl	2(%rdi), %esi
	cmpl	$108, %esi
	jne	.L2551
	addq	$6, %rax
	movq	%rax, 48(%r12)
	cmpq	$2, %rcx
	je	.L2552
	jmp	.L2626
.L2704:
	call	__stack_chk_fail@PLT
.L2590:
	movq	41088(%r13), %rcx
	cmpq	41096(%r13), %rcx
	je	.L2714
.L2592:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rcx)
	jmp	.L2589
.L2713:
	movl	15(%r15), %eax
	testl	$1047552, %eax
	je	.L2584
	movq	%r15, %rsi
	movq	31(%r15), %rax
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	testb	$1, %al
	je	.L2587
	movq	-1(%rax), %rdi
	cmpq	%rdi, -37456(%rsi)
	je	.L2586
.L2587:
	movq	-37504(%rsi), %rax
.L2586:
	cmpq	%rax, 88(%r13)
	jne	.L2584
	jmp	.L2589
.L2710:
	subq	%rdx, %rax
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZNSt6vectorIN2v88internal12JsonPropertyESaIS2_EE17_M_default_appendEm.part.0
.L2714:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L2592
	.cfi_endproc
.LFE19857:
	.size	_ZN2v88internal10JsonParserItE14ParseJsonValueEv, .-_ZN2v88internal10JsonParserItE14ParseJsonValueEv
	.section	.text._ZN2v88internal10JsonParserItE9ParseJsonEv,"axG",@progbits,_ZN2v88internal10JsonParserItE9ParseJsonEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE9ParseJsonEv
	.type	_ZN2v88internal10JsonParserItE9ParseJsonEv, @function
_ZN2v88internal10JsonParserItE9ParseJsonEv:
.LFB19835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v88internal10JsonParserItE14ParseJsonValueEv
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal10JsonParserItE14SkipWhitespaceEv
	movzbl	16(%rbx), %esi
	cmpb	$13, %sil
	je	.L2716
	movq	%rbx, %rdi
	call	_ZN2v88internal10JsonParserItE21ReportUnexpectedTokenENS0_9JsonTokenE
.L2717:
	movq	(%rbx), %rax
	popq	%rbx
	movq	12480(%rax), %rdx
	cmpq	%rdx, 96(%rax)
	movl	$0, %eax
	cmove	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2716:
	.cfi_restore_state
	addq	$2, 48(%rbx)
	jmp	.L2717
	.cfi_endproc
.LFE19835:
	.size	_ZN2v88internal10JsonParserItE9ParseJsonEv, .-_ZN2v88internal10JsonParserItE9ParseJsonEv
	.section	.text._ZN2v88internal10JsonParserItE5ParseEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS5_INS0_6ObjectEEE,"axG",@progbits,_ZN2v88internal10JsonParserItE5ParseEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS5_INS0_6ObjectEEE,comdat
	.p2align 4
	.weak	_ZN2v88internal10JsonParserItE5ParseEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS5_INS0_6ObjectEEE
	.type	_ZN2v88internal10JsonParserItE5ParseEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS5_INS0_6ObjectEEE, @function
_ZN2v88internal10JsonParserItE5ParseEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS5_INS0_6ObjectEEE:
.LFB19828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movq	%rsi, %rdx
	pushq	%r14
	movq	%rdi, %rsi
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	subq	$80, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal10JsonParserItEC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE
	movq	%r13, %rdi
	call	_ZN2v88internal10JsonParserItE9ParseJsonEv
	testq	%rax, %rax
	je	.L2731
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal10JsonParserItED1Ev
	movq	(%r15), %rax
	testb	$1, %al
	jne	.L2724
.L2725:
	movq	%r12, %rax
.L2723:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2732
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2731:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal10JsonParserItED1Ev
	xorl	%eax, %eax
	jmp	.L2723
	.p2align 4,,10
	.p2align 3
.L2724:
	movq	-1(%rax), %rax
	testb	$2, 13(%rax)
	je	.L2725
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21JsonParseInternalizer11InternalizeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_
	jmp	.L2723
.L2732:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19828:
	.size	_ZN2v88internal10JsonParserItE5ParseEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS5_INS0_6ObjectEEE, .-_ZN2v88internal10JsonParserItE5ParseEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS5_INS0_6ObjectEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal21JsonParseInternalizer11InternalizeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal21JsonParseInternalizer11InternalizeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_, @function
_GLOBAL__sub_I__ZN2v88internal21JsonParseInternalizer11InternalizeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_:
.LFB22311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22311:
	.size	_GLOBAL__sub_I__ZN2v88internal21JsonParseInternalizer11InternalizeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_, .-_GLOBAL__sub_I__ZN2v88internal21JsonParseInternalizer11InternalizeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal21JsonParseInternalizer11InternalizeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_
	.section	.rodata._ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE,"a"
	.align 32
	.type	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE, @object
	.size	_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE, 256
_ZN2v88internal12_GLOBAL__N_1L25character_json_scan_flagsE:
	.string	"\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b"
	.string	""
	.string	"\t"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\020"
	.string	"\020\020\001\020\020\020\020\020\020\020\020\020\020"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\020"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\t"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\002"
	.string	""
	.string	"\020\005"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\004"
	.string	""
	.string	""
	.string	"\006"
	.string	"\003\007"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata._ZN2v88internal12_GLOBAL__N_1L20one_char_json_tokensE,"a"
	.align 32
	.type	_ZN2v88internal12_GLOBAL__N_1L20one_char_json_tokensE, @object
	.size	_ZN2v88internal12_GLOBAL__N_1L20one_char_json_tokensE, 256
_ZN2v88internal12_GLOBAL__N_1L20one_char_json_tokensE:
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	9
	.byte	9
	.byte	12
	.byte	12
	.byte	9
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	9
	.byte	12
	.byte	1
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	11
	.byte	0
	.byte	12
	.byte	12
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	10
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	4
	.byte	12
	.byte	5
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	7
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	8
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	6
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	2
	.byte	12
	.byte	3
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC3:
	.long	0
	.long	1072693248
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC8:
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.section	.rodata.cst8
	.align 8
.LC9:
	.long	0
	.long	2146959360
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
