	.file	"heap-profiler.cc"
	.text
	.section	.text._ZN2v88internal27HeapObjectAllocationTracker9MoveEventEmmi,"axG",@progbits,_ZN2v88internal27HeapObjectAllocationTracker9MoveEventEmmi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal27HeapObjectAllocationTracker9MoveEventEmmi
	.type	_ZN2v88internal27HeapObjectAllocationTracker9MoveEventEmmi, @function
_ZN2v88internal27HeapObjectAllocationTracker9MoveEventEmmi:
.LFB7522:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7522:
	.size	_ZN2v88internal27HeapObjectAllocationTracker9MoveEventEmmi, .-_ZN2v88internal27HeapObjectAllocationTracker9MoveEventEmmi
	.section	.text._ZN2v88internal12HeapProfiler15AllocationEventEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler15AllocationEventEmi
	.type	_ZN2v88internal12HeapProfiler15AllocationEventEmi, @function
_ZN2v88internal12HeapProfiler15AllocationEventEmi:
.LFB19907:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3
	jmp	_ZN2v88internal17AllocationTracker15AllocationEventEmi@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	ret
	.cfi_endproc
.LFE19907:
	.size	_ZN2v88internal12HeapProfiler15AllocationEventEmi, .-_ZN2v88internal12HeapProfiler15AllocationEventEmi
	.section	.text._ZN2v88internal12HeapProfiler21UpdateObjectSizeEventEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler21UpdateObjectSizeEventEmi
	.type	_ZN2v88internal12HeapProfiler21UpdateObjectSizeEventEmi, @function
_ZN2v88internal12HeapProfiler21UpdateObjectSizeEventEmi:
.LFB19908:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal14HeapObjectsMap16UpdateObjectSizeEmi@PLT
	.cfi_endproc
.LFE19908:
	.size	_ZN2v88internal12HeapProfiler21UpdateObjectSizeEventEmi, .-_ZN2v88internal12HeapProfiler21UpdateObjectSizeEventEmi
	.section	.text._ZN2v88internal12HeapProfilerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfilerD2Ev
	.type	_ZN2v88internal12HeapProfilerD2Ev, @function
_ZN2v88internal12HeapProfilerD2Ev:
.LFB19860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12HeapProfilerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rax, (%rdi)
	movq	112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L7
	call	_ZdlPv@PLT
.L7:
	movq	104(%rbx), %r12
	testq	%r12, %r12
	je	.L8
	movq	%r12, %rdi
	call	_ZN2v88internal20SamplingHeapProfilerD1Ev@PLT
	movl	$304, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L8:
	leaq	64(%rbx), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	48(%rbx), %r12
	testq	%r12, %r12
	je	.L9
	movq	%r12, %rdi
	call	_ZN2v88internal17AllocationTrackerD1Ev@PLT
	movl	$456, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L9:
	movq	40(%rbx), %r12
	testq	%r12, %r12
	je	.L10
	movq	%r12, %rdi
	call	_ZN2v88internal14StringsStorageD1Ev@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L10:
	movq	24(%rbx), %r15
	movq	16(%rbx), %r13
	cmpq	%r13, %r15
	je	.L11
	.p2align 4,,10
	.p2align 3
.L25:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L12
	movq	456(%r12), %rdi
	testq	%rdi, %rdi
	je	.L13
	call	_ZdlPv@PLT
.L13:
	movq	416(%r12), %r14
	testq	%r14, %r14
	je	.L17
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%r14, %rdi
	movq	(%r14), %r14
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L14
.L17:
	movq	408(%r12), %rax
	movq	400(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	400(%r12), %rdi
	leaq	448(%r12), %rax
	movq	$0, 424(%r12)
	movq	$0, 416(%r12)
	cmpq	%rax, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	movq	376(%r12), %rdi
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movq	296(%r12), %rdi
	testq	%rdi, %rdi
	je	.L19
	movq	368(%r12), %rax
	movq	336(%r12), %r14
	addq	$8, %rax
	cmpq	%r14, %rax
	jbe	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%r14), %rdi
	movq	%rax, -56(%rbp)
	addq	$8, %r14
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	cmpq	%r14, %rax
	ja	.L21
	movq	296(%r12), %rdi
.L20:
	call	_ZdlPv@PLT
.L19:
	movq	216(%r12), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	288(%r12), %rax
	movq	256(%r12), %r14
	addq	$8, %rax
	cmpq	%r14, %rax
	jbe	.L23
	.p2align 4,,10
	.p2align 3
.L24:
	movq	(%r14), %rdi
	movq	%rax, -56(%rbp)
	addq	$8, %r14
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	cmpq	%r14, %rax
	ja	.L24
	movq	216(%r12), %rdi
.L23:
	call	_ZdlPv@PLT
.L22:
	movl	$488, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L12:
	addq	$8, %r13
	cmpq	%r13, %r15
	jne	.L25
	movq	16(%rbx), %r13
.L11:
	testq	%r13, %r13
	je	.L26
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L26:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L6
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L28
	call	_ZdlPv@PLT
.L28:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L29
	call	_ZdlPv@PLT
.L29:
	movq	8(%r12), %rdi
	call	free@PLT
	addq	$24, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19860:
	.size	_ZN2v88internal12HeapProfilerD2Ev, .-_ZN2v88internal12HeapProfilerD2Ev
	.globl	_ZN2v88internal12HeapProfilerD1Ev
	.set	_ZN2v88internal12HeapProfilerD1Ev,_ZN2v88internal12HeapProfilerD2Ev
	.section	.text._ZN2v88internal12HeapProfilerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfilerD0Ev
	.type	_ZN2v88internal12HeapProfilerD0Ev, @function
_ZN2v88internal12HeapProfilerD0Ev:
.LFB19862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal12HeapProfilerD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE19862:
	.size	_ZN2v88internal12HeapProfilerD0Ev, .-_ZN2v88internal12HeapProfilerD0Ev
	.section	.text._ZN2v88internal14V8HeapExplorerD0Ev,"axG",@progbits,_ZN2v88internal14V8HeapExplorerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14V8HeapExplorerD0Ev
	.type	_ZN2v88internal14V8HeapExplorerD0Ev, @function
_ZN2v88internal14V8HeapExplorerD0Ev:
.LFB26264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14V8HeapExplorerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, (%rdi)
	movq	232(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L78
	call	_ZdlPv@PLT
.L78:
	movq	184(%r12), %rbx
	testq	%rbx, %rbx
	je	.L79
	.p2align 4,,10
	.p2align 3
.L80:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L80
.L79:
	movq	176(%r12), %rax
	movq	168(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	168(%r12), %rdi
	leaq	216(%r12), %rax
	movq	$0, 192(%r12)
	movq	$0, 184(%r12)
	cmpq	%rax, %rdi
	je	.L81
	call	_ZdlPv@PLT
.L81:
	movq	128(%r12), %rbx
	testq	%rbx, %rbx
	je	.L82
	.p2align 4,,10
	.p2align 3
.L83:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L83
.L82:
	movq	120(%r12), %rax
	movq	112(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	112(%r12), %rdi
	leaq	160(%r12), %rax
	movq	$0, 136(%r12)
	movq	$0, 128(%r12)
	cmpq	%rax, %rdi
	je	.L84
	call	_ZdlPv@PLT
.L84:
	movq	72(%r12), %rbx
	testq	%rbx, %rbx
	je	.L85
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L86
.L85:
	movq	64(%r12), %rax
	movq	56(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	56(%r12), %rdi
	leaq	104(%r12), %rax
	movq	$0, 80(%r12)
	movq	$0, 72(%r12)
	cmpq	%rax, %rdi
	je	.L87
	call	_ZdlPv@PLT
.L87:
	popq	%rbx
	movq	%r12, %rdi
	movl	$272, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26264:
	.size	_ZN2v88internal14V8HeapExplorerD0Ev, .-_ZN2v88internal14V8HeapExplorerD0Ev
	.section	.text._ZN2v88internal12HeapProfilerC2EPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfilerC2EPNS0_4HeapE
	.type	_ZN2v88internal12HeapProfilerC2EPNS0_4HeapE, @function
_ZN2v88internal12HeapProfilerC2EPNS0_4HeapE:
.LFB19857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12HeapProfilerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movl	$88, %edi
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v88internal14HeapObjectsMapC1EPNS0_4HeapE@PLT
	movq	%r12, 8(%rbx)
	movl	$24, %edi
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	call	_Znwm@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v88internal14StringsStorageC1Ev@PLT
	movq	%r12, 40(%rbx)
	leaq	64(%rbx), %rdi
	movq	$0, 48(%rbx)
	movb	$0, 56(%rbx)
	call	_ZN2v84base5MutexC1Ev@PLT
	pxor	%xmm0, %xmm0
	movups	%xmm0, 104(%rbx)
	movups	%xmm0, 120(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19857:
	.size	_ZN2v88internal12HeapProfilerC2EPNS0_4HeapE, .-_ZN2v88internal12HeapProfilerC2EPNS0_4HeapE
	.globl	_ZN2v88internal12HeapProfilerC1EPNS0_4HeapE
	.set	_ZN2v88internal12HeapProfilerC1EPNS0_4HeapE,_ZN2v88internal12HeapProfilerC2EPNS0_4HeapE
	.section	.text._ZN2v88internal12HeapProfiler18DeleteAllSnapshotsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler18DeleteAllSnapshotsEv
	.type	_ZN2v88internal12HeapProfiler18DeleteAllSnapshotsEv, @function
_ZN2v88internal12HeapProfiler18DeleteAllSnapshotsEv:
.LFB19863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r14
	movq	24(%rdi), %r15
	cmpq	%r15, %r14
	je	.L107
	movq	%r14, %r13
	.p2align 4,,10
	.p2align 3
.L121:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L108
	movq	456(%r12), %rdi
	testq	%rdi, %rdi
	je	.L109
	call	_ZdlPv@PLT
.L109:
	movq	416(%r12), %rax
	testq	%rax, %rax
	je	.L113
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	jne	.L110
.L113:
	movq	408(%r12), %rax
	movq	400(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	400(%r12), %rdi
	leaq	448(%r12), %rax
	movq	$0, 424(%r12)
	movq	$0, 416(%r12)
	cmpq	%rax, %rdi
	je	.L111
	call	_ZdlPv@PLT
.L111:
	movq	376(%r12), %rdi
	testq	%rdi, %rdi
	je	.L114
	call	_ZdlPv@PLT
.L114:
	movq	296(%r12), %rdi
	testq	%rdi, %rdi
	je	.L115
	movq	368(%r12), %rax
	leaq	8(%rax), %rdx
	movq	336(%r12), %rax
	cmpq	%rax, %rdx
	jbe	.L116
	.p2align 4,,10
	.p2align 3
.L117:
	movq	(%rax), %rdi
	movq	%rdx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rdx
	addq	$8, %rax
	cmpq	%rax, %rdx
	ja	.L117
	movq	296(%r12), %rdi
.L116:
	call	_ZdlPv@PLT
.L115:
	movq	216(%r12), %rdi
	testq	%rdi, %rdi
	je	.L118
	movq	288(%r12), %rax
	leaq	8(%rax), %rdx
	movq	256(%r12), %rax
	cmpq	%rax, %rdx
	jbe	.L119
	.p2align 4,,10
	.p2align 3
.L120:
	movq	(%rax), %rdi
	movq	%rdx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rdx
	addq	$8, %rax
	cmpq	%rax, %rdx
	ja	.L120
	movq	216(%r12), %rdi
.L119:
	call	_ZdlPv@PLT
.L118:
	movl	$488, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L108:
	addq	$8, %r13
	cmpq	%r13, %r15
	jne	.L121
	movq	%r14, 24(%rbx)
	cmpq	16(%rbx), %r14
	je	.L107
.L106:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	cmpq	$0, 104(%rbx)
	jne	.L106
	cmpq	$0, 48(%rbx)
	jne	.L106
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal14StringsStorageC1Ev@PLT
	movq	40(%rbx), %r13
	movq	%r12, 40(%rbx)
	testq	%r13, %r13
	je	.L106
	movq	%r13, %rdi
	call	_ZN2v88internal14StringsStorageD1Ev@PLT
	addq	$24, %rsp
	movq	%r13, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE19863:
	.size	_ZN2v88internal12HeapProfiler18DeleteAllSnapshotsEv, .-_ZN2v88internal12HeapProfiler18DeleteAllSnapshotsEv
	.section	.text._ZN2v88internal12HeapProfiler24MaybeClearStringsStorageEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler24MaybeClearStringsStorageEv
	.type	_ZN2v88internal12HeapProfiler24MaybeClearStringsStorageEv, @function
_ZN2v88internal12HeapProfiler24MaybeClearStringsStorageEv:
.LFB19864:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	cmpq	%rax, 16(%rdi)
	je	.L162
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 104(%rdi)
	je	.L163
.L151:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	cmpq	$0, 48(%rdi)
	jne	.L151
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal14StringsStorageC1Ev@PLT
	movq	40(%rbx), %r13
	movq	%r12, 40(%rbx)
	testq	%r13, %r13
	je	.L151
	movq	%r13, %rdi
	call	_ZN2v88internal14StringsStorageD1Ev@PLT
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$24, %esi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE19864:
	.size	_ZN2v88internal12HeapProfiler24MaybeClearStringsStorageEv, .-_ZN2v88internal12HeapProfiler24MaybeClearStringsStorageEv
	.section	.text._ZN2v88internal12HeapProfiler14RemoveSnapshotEPNS0_12HeapSnapshotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler14RemoveSnapshotEPNS0_12HeapSnapshotE
	.type	_ZN2v88internal12HeapProfiler14RemoveSnapshotEPNS0_12HeapSnapshotE, @function
_ZN2v88internal12HeapProfiler14RemoveSnapshotEPNS0_12HeapSnapshotE:
.LFB19865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rbx
	movq	%rdx, %rax
	subq	%rbx, %rax
	movq	%rax, %rcx
	sarq	$5, %rax
	sarq	$3, %rcx
	testq	%rax, %rax
	jle	.L165
	salq	$5, %rax
	addq	%rbx, %rax
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L258:
	cmpq	%rsi, 8(%rbx)
	je	.L254
	cmpq	%rsi, 16(%rbx)
	je	.L255
	cmpq	%rsi, 24(%rbx)
	je	.L256
	addq	$32, %rbx
	cmpq	%rbx, %rax
	je	.L257
.L170:
	leaq	8(%rbx), %r13
	cmpq	%rsi, (%rbx)
	jne	.L258
.L166:
	cmpq	%r13, %rdx
	je	.L175
.L259:
	movq	%rdx, %rax
	subq	%r13, %rax
	movq	%rax, %r15
	sarq	$3, %r15
	testq	%rax, %rax
	jle	.L206
	.p2align 4,,10
	.p2align 3
.L190:
	movq	0(%r13), %rax
	movq	$0, 0(%r13)
	movq	(%rbx), %r12
	movq	%rax, (%rbx)
	testq	%r12, %r12
	je	.L177
	movq	456(%r12), %rdi
	testq	%rdi, %rdi
	je	.L178
	call	_ZdlPv@PLT
.L178:
	movq	416(%r12), %rax
	testq	%rax, %rax
	je	.L182
	.p2align 4,,10
	.p2align 3
.L179:
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	jne	.L179
.L182:
	movq	408(%r12), %rax
	movq	400(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	400(%r12), %rdi
	leaq	448(%r12), %rax
	movq	$0, 424(%r12)
	movq	$0, 416(%r12)
	cmpq	%rax, %rdi
	je	.L180
	call	_ZdlPv@PLT
.L180:
	movq	376(%r12), %rdi
	testq	%rdi, %rdi
	je	.L183
	call	_ZdlPv@PLT
.L183:
	movq	296(%r12), %rdi
	testq	%rdi, %rdi
	je	.L184
	movq	368(%r12), %rax
	leaq	8(%rax), %rdx
	movq	336(%r12), %rax
	cmpq	%rax, %rdx
	jbe	.L185
	.p2align 4,,10
	.p2align 3
.L186:
	movq	(%rax), %rdi
	movq	%rdx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rdx
	addq	$8, %rax
	cmpq	%rax, %rdx
	ja	.L186
	movq	296(%r12), %rdi
.L185:
	call	_ZdlPv@PLT
.L184:
	movq	216(%r12), %rdi
	testq	%rdi, %rdi
	je	.L187
	movq	288(%r12), %rax
	leaq	8(%rax), %rdx
	movq	256(%r12), %rax
	cmpq	%rax, %rdx
	jbe	.L188
	.p2align 4,,10
	.p2align 3
.L189:
	movq	(%rax), %rdi
	movq	%rdx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rdx
	addq	$8, %rax
	cmpq	%rax, %rdx
	ja	.L189
	movq	216(%r12), %rdi
.L188:
	call	_ZdlPv@PLT
.L187:
	movl	$488, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L177:
	addq	$8, %r13
	addq	$8, %rbx
	subq	$1, %r15
	jne	.L190
	movq	24(%r14), %r13
.L175:
	leaq	-8(%r13), %rax
	movq	%rax, 24(%r14)
	movq	-8(%r13), %r12
	testq	%r12, %r12
	je	.L164
	movq	456(%r12), %rdi
	testq	%rdi, %rdi
	je	.L192
	call	_ZdlPv@PLT
.L192:
	movq	416(%r12), %rbx
	testq	%rbx, %rbx
	je	.L196
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L193
.L196:
	movq	408(%r12), %rax
	movq	400(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	400(%r12), %rdi
	leaq	448(%r12), %rax
	movq	$0, 424(%r12)
	movq	$0, 416(%r12)
	cmpq	%rax, %rdi
	je	.L194
	call	_ZdlPv@PLT
.L194:
	movq	376(%r12), %rdi
	testq	%rdi, %rdi
	je	.L197
	call	_ZdlPv@PLT
.L197:
	movq	296(%r12), %rdi
	testq	%rdi, %rdi
	je	.L198
	movq	368(%r12), %rax
	movq	336(%r12), %rbx
	leaq	8(%rax), %r13
	cmpq	%rbx, %r13
	jbe	.L199
	.p2align 4,,10
	.p2align 3
.L200:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r13
	ja	.L200
	movq	296(%r12), %rdi
.L199:
	call	_ZdlPv@PLT
.L198:
	movq	216(%r12), %rdi
	testq	%rdi, %rdi
	je	.L201
	movq	288(%r12), %rax
	movq	256(%r12), %rbx
	leaq	8(%rax), %r13
	cmpq	%rbx, %r13
	jbe	.L202
	.p2align 4,,10
	.p2align 3
.L203:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r13
	ja	.L203
	movq	216(%r12), %rdi
.L202:
	call	_ZdlPv@PLT
.L201:
	addq	$24, %rsp
	movq	%r12, %rdi
	movl	$488, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	movq	%rdx, %rcx
	subq	%rbx, %rcx
	sarq	$3, %rcx
.L165:
	cmpq	$2, %rcx
	je	.L204
	cmpq	$3, %rcx
	je	.L172
	cmpq	$1, %rcx
	je	.L173
.L206:
	movq	%rdx, %r13
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L204:
	movq	%rbx, %r13
.L171:
	leaq	8(%r13), %rbx
	cmpq	%rsi, 0(%r13)
	je	.L205
.L173:
	leaq	8(%rbx), %r13
	cmpq	%rsi, (%rbx)
	jne	.L206
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L172:
	leaq	8(%rbx), %r13
	cmpq	%rsi, (%rbx)
	jne	.L171
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L254:
	leaq	16(%rbx), %rax
	movq	%r13, %rbx
	movq	%rax, %r13
	cmpq	%r13, %rdx
	jne	.L259
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L255:
	leaq	24(%rbx), %r13
	addq	$16, %rbx
	cmpq	%r13, %rdx
	jne	.L259
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L256:
	leaq	32(%rbx), %r13
	addq	$24, %rbx
	cmpq	%r13, %rdx
	jne	.L259
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L205:
	movq	%rbx, %rax
	movq	%r13, %rbx
	movq	%rax, %r13
	jmp	.L166
	.cfi_endproc
.LFE19865:
	.size	_ZN2v88internal12HeapProfiler14RemoveSnapshotEPNS0_12HeapSnapshotE, .-_ZN2v88internal12HeapProfiler14RemoveSnapshotEPNS0_12HeapSnapshotE
	.section	.text._ZN2v88internal12HeapProfiler18BuildEmbedderGraphEPNS0_7IsolateEPNS_13EmbedderGraphE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler18BuildEmbedderGraphEPNS0_7IsolateEPNS_13EmbedderGraphE
	.type	_ZN2v88internal12HeapProfiler18BuildEmbedderGraphEPNS0_7IsolateEPNS_13EmbedderGraphE, @function
_ZN2v88internal12HeapProfiler18BuildEmbedderGraphEPNS0_7IsolateEPNS_13EmbedderGraphE:
.LFB19889:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	112(%rdi), %rbx
	movq	120(%rdi), %r14
	cmpq	%r14, %rbx
	je	.L260
	movq	%rsi, %r12
	movq	%rdx, %r13
	.p2align 4,,10
	.p2align 3
.L262:
	movq	8(%rbx), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*(%rbx)
	addq	$16, %rbx
	cmpq	%rbx, %r14
	jne	.L262
.L260:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19889:
	.size	_ZN2v88internal12HeapProfiler18BuildEmbedderGraphEPNS0_7IsolateEPNS_13EmbedderGraphE, .-_ZN2v88internal12HeapProfiler18BuildEmbedderGraphEPNS0_7IsolateEPNS_13EmbedderGraphE
	.section	.text._ZN2v88internal12HeapProfiler25StartSamplingHeapProfilerEmiNS_12HeapProfiler13SamplingFlagsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler25StartSamplingHeapProfilerEmiNS_12HeapProfiler13SamplingFlagsE
	.type	_ZN2v88internal12HeapProfiler25StartSamplingHeapProfilerEmiNS_12HeapProfiler13SamplingFlagsE, @function
_ZN2v88internal12HeapProfiler25StartSamplingHeapProfilerEmiNS_12HeapProfiler13SamplingFlagsE:
.LFB19897:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 104(%rdi)
	je	.L274
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rax
	movq	40(%rdi), %rdx
	movl	$304, %edi
	movq	80(%rax), %rsi
	movq	%rdx, -56(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movl	%r12d, %r9d
	movq	%rax, %rdi
	movl	%r13d, %r8d
	movq	%r14, %rcx
	movq	%rax, %r15
	call	_ZN2v88internal20SamplingHeapProfilerC1EPNS0_4HeapEPNS0_14StringsStorageEmiNS_12HeapProfiler13SamplingFlagsE@PLT
	movq	104(%rbx), %r12
	movl	$1, %eax
	movq	%r15, 104(%rbx)
	testq	%r12, %r12
	je	.L265
	movq	%r12, %rdi
	movb	%al, -56(%rbp)
	call	_ZN2v88internal20SamplingHeapProfilerD1Ev@PLT
	movl	$304, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movzbl	-56(%rbp), %eax
.L265:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19897:
	.size	_ZN2v88internal12HeapProfiler25StartSamplingHeapProfilerEmiNS_12HeapProfiler13SamplingFlagsE, .-_ZN2v88internal12HeapProfiler25StartSamplingHeapProfilerEmiNS_12HeapProfiler13SamplingFlagsE
	.section	.text._ZN2v88internal12HeapProfiler24StopSamplingHeapProfilerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler24StopSamplingHeapProfilerEv
	.type	_ZN2v88internal12HeapProfiler24StopSamplingHeapProfilerEv, @function
_ZN2v88internal12HeapProfiler24StopSamplingHeapProfilerEv:
.LFB19898:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	104(%rdi), %r12
	movq	$0, 104(%rdi)
	testq	%r12, %r12
	je	.L276
	movq	%r12, %rdi
	call	_ZN2v88internal20SamplingHeapProfilerD1Ev@PLT
	movl	$304, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	16(%rbx), %rax
	cmpq	%rax, 24(%rbx)
	je	.L285
.L275:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	cmpq	$0, 104(%rbx)
	jne	.L275
	.p2align 4,,10
	.p2align 3
.L280:
	cmpq	$0, 48(%rbx)
	jne	.L275
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal14StringsStorageC1Ev@PLT
	movq	40(%rbx), %r13
	movq	%r12, 40(%rbx)
	testq	%r13, %r13
	je	.L275
	movq	%r13, %rdi
	call	_ZN2v88internal14StringsStorageD1Ev@PLT
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	movq	16(%rdi), %rax
	cmpq	%rax, 24(%rdi)
	je	.L280
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19898:
	.size	_ZN2v88internal12HeapProfiler24StopSamplingHeapProfilerEv, .-_ZN2v88internal12HeapProfiler24StopSamplingHeapProfilerEv
	.section	.text._ZN2v88internal12HeapProfiler20GetAllocationProfileEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler20GetAllocationProfileEv
	.type	_ZN2v88internal12HeapProfiler20GetAllocationProfileEv, @function
_ZN2v88internal12HeapProfiler20GetAllocationProfileEv:
.LFB19899:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L286
	jmp	_ZN2v88internal20SamplingHeapProfiler20GetAllocationProfileEv@PLT
	.p2align 4,,10
	.p2align 3
.L286:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE19899:
	.size	_ZN2v88internal12HeapProfiler20GetAllocationProfileEv, .-_ZN2v88internal12HeapProfiler20GetAllocationProfileEv
	.section	.text._ZN2v88internal12HeapProfiler24StartHeapObjectsTrackingEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler24StartHeapObjectsTrackingEb
	.type	_ZN2v88internal12HeapProfiler24StartHeapObjectsTrackingEb, @function
_ZN2v88internal12HeapProfiler24StartHeapObjectsTrackingEb:
.LFB19900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	call	_ZN2v88internal14HeapObjectsMap20UpdateHeapObjectsMapEv@PLT
	movb	$1, 56(%rbx)
	testb	%r12b, %r12b
	jne	.L295
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	movq	8(%rbx), %r13
	movq	40(%rbx), %r14
	movl	$456, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%rax, %r12
	call	_ZN2v88internal17AllocationTrackerC1EPNS0_14HeapObjectsMapEPNS0_14StringsStorageE@PLT
	movq	48(%rbx), %r13
	movq	%r12, 48(%rbx)
	testq	%r13, %r13
	je	.L290
	movq	%r13, %rdi
	call	_ZN2v88internal17AllocationTrackerD1Ev@PLT
	movl	$456, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L290:
	movq	8(%rbx), %rax
	movq	%rbx, %rsi
	movq	80(%rax), %rdi
	call	_ZN2v88internal4Heap30AddHeapObjectAllocationTrackerEPNS0_27HeapObjectAllocationTrackerE@PLT
	movq	8(%rbx), %rax
	movl	$5, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	movq	80(%rax), %rax
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	3880(%rax), %rdi
	addq	$48, %rdi
	jmp	_ZN2v88internal19DebugFeatureTracker5TrackENS1_7FeatureE@PLT
	.cfi_endproc
.LFE19900:
	.size	_ZN2v88internal12HeapProfiler24StartHeapObjectsTrackingEb, .-_ZN2v88internal12HeapProfiler24StartHeapObjectsTrackingEb
	.section	.text._ZN2v88internal12HeapProfiler20PushHeapObjectsStatsEPNS_12OutputStreamEPl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler20PushHeapObjectsStatsEPNS_12OutputStreamEPl
	.type	_ZN2v88internal12HeapProfiler20PushHeapObjectsStatsEPNS_12OutputStreamEPl, @function
_ZN2v88internal12HeapProfiler20PushHeapObjectsStatsEPNS_12OutputStreamEPl:
.LFB19901:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal14HeapObjectsMap20PushHeapObjectsStatsEPNS_12OutputStreamEPl@PLT
	.cfi_endproc
.LFE19901:
	.size	_ZN2v88internal12HeapProfiler20PushHeapObjectsStatsEPNS_12OutputStreamEPl, .-_ZN2v88internal12HeapProfiler20PushHeapObjectsStatsEPNS_12OutputStreamEPl
	.section	.text._ZN2v88internal12HeapProfiler23StopHeapObjectsTrackingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler23StopHeapObjectsTrackingEv
	.type	_ZN2v88internal12HeapProfiler23StopHeapObjectsTrackingEv, @function
_ZN2v88internal12HeapProfiler23StopHeapObjectsTrackingEv:
.LFB19902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	_ZN2v88internal14HeapObjectsMap23StopHeapObjectsTrackingEv@PLT
	movq	48(%r12), %r13
	testq	%r13, %r13
	je	.L297
	movq	$0, 48(%r12)
	movq	%r13, %rdi
	call	_ZN2v88internal17AllocationTrackerD1Ev@PLT
	movl	$456, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	16(%r12), %rax
	cmpq	%rax, 24(%r12)
	je	.L306
.L300:
	movq	8(%r12), %rax
	movq	%r12, %rsi
	movq	80(%rax), %rdi
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4Heap33RemoveHeapObjectAllocationTrackerEPNS0_27HeapObjectAllocationTrackerE@PLT
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	cmpq	$0, 104(%r12)
	jne	.L300
	cmpq	$0, 48(%r12)
	jne	.L300
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal14StringsStorageC1Ev@PLT
	movq	40(%r12), %r14
	movq	%r13, 40(%r12)
	testq	%r14, %r14
	je	.L300
	movq	%r14, %rdi
	call	_ZN2v88internal14StringsStorageD1Ev@PLT
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L300
	.cfi_endproc
.LFE19902:
	.size	_ZN2v88internal12HeapProfiler23StopHeapObjectsTrackingEv, .-_ZN2v88internal12HeapProfiler23StopHeapObjectsTrackingEv
	.section	.text._ZN2v88internal12HeapProfiler17GetSnapshotsCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler17GetSnapshotsCountEv
	.type	_ZN2v88internal12HeapProfiler17GetSnapshotsCountEv, @function
_ZN2v88internal12HeapProfiler17GetSnapshotsCountEv:
.LFB19903:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	subq	16(%rdi), %rax
	sarq	$3, %rax
	ret
	.cfi_endproc
.LFE19903:
	.size	_ZN2v88internal12HeapProfiler17GetSnapshotsCountEv, .-_ZN2v88internal12HeapProfiler17GetSnapshotsCountEv
	.section	.rodata._ZN2v88internal12HeapProfiler11GetSnapshotEi.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internal12HeapProfiler11GetSnapshotEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler11GetSnapshotEi
	.type	_ZN2v88internal12HeapProfiler11GetSnapshotEi, @function
_ZN2v88internal12HeapProfiler11GetSnapshotEi:
.LFB19904:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	movslq	%esi, %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L313
	movq	(%rax,%rsi,8), %rax
	ret
.L313:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE19904:
	.size	_ZN2v88internal12HeapProfiler11GetSnapshotEi, .-_ZN2v88internal12HeapProfiler11GetSnapshotEi
	.section	.text._ZN2v88internal12HeapProfiler19GetSnapshotObjectIdENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler19GetSnapshotObjectIdENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal12HeapProfiler19GetSnapshotObjectIdENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal12HeapProfiler19GetSnapshotObjectIdENS0_6HandleINS0_6ObjectEEE:
.LFB19905:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rsi
	testb	$1, %sil
	jne	.L316
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	movq	8(%rdi), %rdi
	subq	$1, %rsi
	jmp	_ZN2v88internal14HeapObjectsMap9FindEntryEm@PLT
	.cfi_endproc
.LFE19905:
	.size	_ZN2v88internal12HeapProfiler19GetSnapshotObjectIdENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal12HeapProfiler19GetSnapshotObjectIdENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal12HeapProfiler15ObjectMoveEventEmmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler15ObjectMoveEventEmmi
	.type	_ZN2v88internal12HeapProfiler15ObjectMoveEventEmmi, @function
_ZN2v88internal12HeapProfiler15ObjectMoveEventEmmi:
.LFB19906:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	64(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%rbx), %rdi
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal14HeapObjectsMap10MoveObjectEmmi@PLT
	testb	%al, %al
	jne	.L318
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L318
	addq	$408, %rdi
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal17AddressToTraceMap10MoveObjectEmmi@PLT
.L318:
	addq	$8, %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE19906:
	.size	_ZN2v88internal12HeapProfiler15ObjectMoveEventEmmi, .-_ZN2v88internal12HeapProfiler15ObjectMoveEventEmmi
	.section	.text._ZN2v88internal12HeapProfiler18FindHeapObjectByIdEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler18FindHeapObjectByIdEj
	.type	_ZN2v88internal12HeapProfiler18FindHeapObjectByIdEj, @function
_ZN2v88internal12HeapProfiler18FindHeapObjectByIdEj:
.LFB19909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%r12, %rdi
	movq	80(%rax), %rsi
	call	_ZN2v88internal26CombinedHeapObjectIteratorC1EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal26CombinedHeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	je	.L324
	movq	%rax, %rbx
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L326:
	movq	8(%r15), %rdi
	leaq	-1(%rbx), %rsi
	call	_ZN2v88internal14HeapObjectsMap9FindEntryEm@PLT
	movq	%r12, %rdi
	cmpl	%r13d, %eax
	cmove	%rbx, %r14
	call	_ZN2v88internal26CombinedHeapObjectIterator4NextEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L326
	testq	%r14, %r14
	je	.L324
	movq	8(%r15), %rax
	movq	80(%rax), %rbx
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L327
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L324:
	xorl	%r13d, %r13d
.L330:
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L340
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L341
.L329:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%r14, 0(%r13)
	jmp	.L330
.L341:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r13
	jmp	.L329
.L340:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19909:
	.size	_ZN2v88internal12HeapProfiler18FindHeapObjectByIdEj, .-_ZN2v88internal12HeapProfiler18FindHeapObjectByIdEj
	.section	.text._ZN2v88internal12HeapProfiler18ClearHeapObjectMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler18ClearHeapObjectMapEv
	.type	_ZN2v88internal12HeapProfiler18ClearHeapObjectMapEv, @function
_ZN2v88internal12HeapProfiler18ClearHeapObjectMapEv:
.LFB19913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movl	$88, %edi
	movq	80(%rax), %r13
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal14HeapObjectsMapC1EPNS0_4HeapE@PLT
	movq	8(%rbx), %r13
	movq	%r12, 8(%rbx)
	testq	%r13, %r13
	je	.L343
	movq	56(%r13), %rdi
	testq	%rdi, %rdi
	je	.L344
	call	_ZdlPv@PLT
.L344:
	movq	32(%r13), %rdi
	testq	%rdi, %rdi
	je	.L345
	call	_ZdlPv@PLT
.L345:
	movq	8(%r13), %rdi
	call	free@PLT
	movl	$88, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L343:
	cmpq	$0, 48(%rbx)
	je	.L357
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_restore_state
	movb	$0, 56(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19913:
	.size	_ZN2v88internal12HeapProfiler18ClearHeapObjectMapEv, .-_ZN2v88internal12HeapProfiler18ClearHeapObjectMapEv
	.section	.text._ZNK2v88internal12HeapProfiler4heapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12HeapProfiler4heapEv
	.type	_ZNK2v88internal12HeapProfiler4heapEv, @function
_ZNK2v88internal12HeapProfiler4heapEv:
.LFB19914:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	80(%rax), %rax
	ret
	.cfi_endproc
.LFE19914:
	.size	_ZNK2v88internal12HeapProfiler4heapEv, .-_ZNK2v88internal12HeapProfiler4heapEv
	.section	.text._ZNK2v88internal12HeapProfiler7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12HeapProfiler7isolateEv
	.type	_ZNK2v88internal12HeapProfiler7isolateEv, @function
_ZNK2v88internal12HeapProfiler7isolateEv:
.LFB19915:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	80(%rax), %rax
	subq	$37592, %rax
	ret
	.cfi_endproc
.LFE19915:
	.size	_ZNK2v88internal12HeapProfiler7isolateEv, .-_ZNK2v88internal12HeapProfiler7isolateEv
	.section	.rodata._ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.type	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, @function
_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_:
.LFB22541:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L374
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L370
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L375
.L362:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L369:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L376
	testq	%r13, %r13
	jg	.L365
	testq	%r9, %r9
	jne	.L368
.L366:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L365
.L368:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L375:
	testq	%rsi, %rsi
	jne	.L363
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L365:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L366
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L370:
	movl	$8, %r14d
	jmp	.L362
.L374:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L363:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L362
	.cfi_endproc
.LFE22541:
	.size	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, .-_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.section	.text._ZN2v88internal12HeapProfiler12QueryObjectsENS0_6HandleINS0_7ContextEEEPNS_5debug20QueryObjectPredicateEPNS_21PersistentValueVectorINS_6ObjectENS_34DefaultPersistentValueVectorTraitsEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler12QueryObjectsENS0_6HandleINS0_7ContextEEEPNS_5debug20QueryObjectPredicateEPNS_21PersistentValueVectorINS_6ObjectENS_34DefaultPersistentValueVectorTraitsEEE
	.type	_ZN2v88internal12HeapProfiler12QueryObjectsENS0_6HandleINS0_7ContextEEEPNS_5debug20QueryObjectPredicateEPNS_21PersistentValueVectorINS_6ObjectENS_34DefaultPersistentValueVectorTraitsEEE, @function
_ZN2v88internal12HeapProfiler12QueryObjectsENS0_6HandleINS0_7ContextEEEPNS_5debug20QueryObjectPredicateEPNS_21PersistentValueVectorINS_6ObjectENS_34DefaultPersistentValueVectorTraitsEEE:
.LFB19916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	$1, %edx
	leaq	-136(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%r12, %rdi
	movq	80(%rax), %rsi
	call	_ZN2v88internal26CombinedHeapObjectIteratorC1EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal26CombinedHeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	je	.L383
	.p2align 4,,10
	.p2align 3
.L378:
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	je	.L406
	movq	%r12, %rdi
	call	_ZN2v88internal26CombinedHeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	jne	.L378
.L383:
	movq	%r12, %rdi
	leaq	-144(%rbp), %r15
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movq	8(%rbx), %rax
	movl	$11, %esi
	movq	80(%rax), %rdi
	call	_ZN2v88internal4Heap26CollectAllAvailableGarbageENS0_23GarbageCollectionReasonE@PLT
	movq	8(%rbx), %rax
	movl	$1, %edx
	movq	%r12, %rdi
	movq	80(%rax), %rsi
	call	_ZN2v88internal26CombinedHeapObjectIteratorC1EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal26CombinedHeapObjectIterator4NextEv@PLT
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	jne	.L379
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L407:
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	(%r14), %rax
	movq	%rsi, -152(%rbp)
	call	*16(%rax)
	movq	-152(%rbp), %rsi
	testb	%al, %al
	je	.L392
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L389
.L405:
	movq	0(%r13), %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
.L389:
	movq	%rax, -136(%rbp)
	movq	16(%r13), %rsi
	cmpq	24(%r13), %rsi
	je	.L390
	movq	%rax, (%rsi)
	addq	$8, 16(%r13)
	.p2align 4,,10
	.p2align 3
.L392:
	movq	%r12, %rdi
	call	_ZN2v88internal26CombinedHeapObjectIterator4NextEv@PLT
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L380
.L379:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L392
	movq	8(%rbx), %rax
	movq	%r15, %rdi
	movq	80(%rax), %rsi
	subq	$37592, %rsi
	call	_ZNK2v88internal10HeapObject10IsExternalEPNS0_7IsolateE@PLT
	testb	%al, %al
	jne	.L392
	movq	8(%rbx), %rax
	movq	-144(%rbp), %r8
	movq	80(%rax), %rdx
	movq	3520(%rdx), %rdi
	subq	$37592, %rdx
	testq	%rdi, %rdi
	jne	.L407
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L408
.L387:
	leaq	8(%rsi), %rax
	movq	%rsi, -152(%rbp)
	movq	%r14, %rdi
	movq	%rax, 41088(%rdx)
	movq	%r8, (%rsi)
	movq	(%r14), %rax
	call	*16(%rax)
	movq	-152(%rbp), %rsi
	testb	%al, %al
	jne	.L405
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L406:
	movq	%rax, -136(%rbp)
	movq	8(%rbx), %rax
	movq	%r15, %rdi
	movq	80(%rax), %rsi
	subq	$37592, %rsi
	call	_ZN2v88internal14FeedbackVector10ClearSlotsEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal26CombinedHeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	jne	.L378
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L380:
	movq	%r12, %rdi
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L409
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L390:
	.cfi_restore_state
	leaq	-136(%rbp), %rdx
	leaq	8(%r13), %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJRKmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L408:
	movq	%rdx, %rdi
	movq	%r8, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %r8
	movq	-152(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L387
.L409:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19916:
	.size	_ZN2v88internal12HeapProfiler12QueryObjectsENS0_6HandleINS0_7ContextEEEPNS_5debug20QueryObjectPredicateEPNS_21PersistentValueVectorINS_6ObjectENS_34DefaultPersistentValueVectorTraitsEEE, .-_ZN2v88internal12HeapProfiler12QueryObjectsENS0_6HandleINS0_7ContextEEEPNS_5debug20QueryObjectPredicateEPNS_21PersistentValueVectorINS_6ObjectENS_34DefaultPersistentValueVectorTraitsEEE
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v88internal12HeapSnapshotESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJRPS3_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v88internal12HeapSnapshotESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJRPS3_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88internal12HeapSnapshotESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJRPS3_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN2v88internal12HeapSnapshotESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJRPS3_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN2v88internal12HeapSnapshotESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJRPS3_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_:
.LFB23907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	8(%rdi), %rax
	movq	%rdi, -104(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rax, -96(%rbp)
	subq	%rcx, %rax
	movq	%rcx, -72(%rbp)
	sarq	$3, %rax
	movabsq	$1152921504606846975, %rcx
	cmpq	%rcx, %rax
	je	.L467
	movq	%rsi, %r14
	movq	%rsi, %r13
	movq	%rdx, %r12
	subq	-72(%rbp), %r14
	testq	%rax, %rax
	je	.L435
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L468
.L412:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rax, -80(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -88(%rbp)
	leaq	8(%rax), %rbx
.L434:
	movq	(%r12), %rax
	movq	-80(%rbp), %r12
	movq	%rax, (%r12,%r14)
	movq	-72(%rbp), %rax
	cmpq	%rax, %r13
	je	.L414
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L428:
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	movq	%rax, (%r12)
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L415
	movq	456(%r15), %rdi
	testq	%rdi, %rdi
	je	.L416
	call	_ZdlPv@PLT
.L416:
	movq	416(%r15), %r14
	testq	%r14, %r14
	je	.L420
	.p2align 4,,10
	.p2align 3
.L417:
	movq	%r14, %rdi
	movq	(%r14), %r14
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L417
.L420:
	movq	408(%r15), %rax
	movq	400(%r15), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	400(%r15), %rdi
	leaq	448(%r15), %rax
	movq	$0, 424(%r15)
	movq	$0, 416(%r15)
	cmpq	%rax, %rdi
	je	.L418
	call	_ZdlPv@PLT
.L418:
	movq	376(%r15), %rdi
	testq	%rdi, %rdi
	je	.L421
	call	_ZdlPv@PLT
.L421:
	movq	296(%r15), %rdi
	testq	%rdi, %rdi
	je	.L422
	movq	368(%r15), %rax
	movq	336(%r15), %r14
	addq	$8, %rax
	movq	%rax, -56(%rbp)
	cmpq	%r14, %rax
	jbe	.L423
	.p2align 4,,10
	.p2align 3
.L424:
	movq	(%r14), %rdi
	addq	$8, %r14
	call	_ZdlPv@PLT
	cmpq	%r14, -56(%rbp)
	ja	.L424
	movq	296(%r15), %rdi
.L423:
	call	_ZdlPv@PLT
.L422:
	movq	216(%r15), %rdi
	testq	%rdi, %rdi
	je	.L425
	movq	288(%r15), %rax
	movq	256(%r15), %r14
	addq	$8, %rax
	movq	%rax, -56(%rbp)
	cmpq	%r14, %rax
	jbe	.L426
	.p2align 4,,10
	.p2align 3
.L427:
	movq	(%r14), %rdi
	addq	$8, %r14
	call	_ZdlPv@PLT
	cmpq	%r14, -56(%rbp)
	ja	.L427
	movq	216(%r15), %rdi
.L426:
	call	_ZdlPv@PLT
.L425:
	movl	$488, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L415:
	addq	$8, %rbx
	addq	$8, %r12
	cmpq	%rbx, %r13
	jne	.L428
	movq	-80(%rbp), %rcx
	movq	%r13, %rax
	subq	-72(%rbp), %rax
	leaq	8(%rcx,%rax), %rbx
.L414:
	movq	-96(%rbp), %rax
	cmpq	%rax, %r13
	je	.L429
	subq	%r13, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L437
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L431:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L431
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r15
	leaq	0(%r13,%r15), %rdi
	leaq	(%rbx,%r15), %rdx
	movq	%rdi, -64(%rbp)
	cmpq	%rcx, %rax
	je	.L432
.L430:
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L432:
	leaq	8(%rbx,%rsi), %rbx
.L429:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L433
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L433:
	movq	-80(%rbp), %xmm0
	movq	-104(%rbp), %rax
	movq	%rbx, %xmm2
	movq	-88(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L468:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L413
	movq	$0, -88(%rbp)
	movl	$8, %ebx
	movq	$0, -80(%rbp)
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L435:
	movl	$8, %ebx
	jmp	.L412
.L437:
	movq	%rbx, %rdx
	jmp	.L430
.L413:
	cmpq	%rcx, %rdx
	cmova	%rcx, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L412
.L467:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23907:
	.size	_ZNSt6vectorISt10unique_ptrIN2v88internal12HeapSnapshotESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJRPS3_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN2v88internal12HeapSnapshotESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJRPS3_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.section	.text._ZN2v88internal12HeapProfiler12TakeSnapshotEPNS_15ActivityControlEPNS_12HeapProfiler18ObjectNameResolverE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler12TakeSnapshotEPNS_15ActivityControlEPNS_12HeapProfiler18ObjectNameResolverE
	.type	_ZN2v88internal12HeapProfiler12TakeSnapshotEPNS_15ActivityControlEPNS_12HeapProfiler18ObjectNameResolverE, @function
_ZN2v88internal12HeapProfiler12TakeSnapshotEPNS_15ActivityControlEPNS_12HeapProfiler18ObjectNameResolverE:
.LFB19890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	leaq	-464(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$488, %edi
	subq	$440, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal12HeapSnapshotC1EPNS0_12HeapProfilerE@PLT
	movq	8(%rbx), %rax
	movq	%r15, %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r12, -472(%rbp)
	movq	80(%rax), %r8
	call	_ZN2v88internal21HeapSnapshotGeneratorC1EPNS0_12HeapSnapshotEPNS_15ActivityControlEPNS_12HeapProfiler18ObjectNameResolverEPNS0_4HeapE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal21HeapSnapshotGenerator16GenerateSnapshotEv@PLT
	testb	%al, %al
	jne	.L470
	movq	-472(%rbp), %r12
	testq	%r12, %r12
	je	.L471
	movq	456(%r12), %rdi
	testq	%rdi, %rdi
	je	.L472
	call	_ZdlPv@PLT
.L472:
	movq	416(%r12), %r13
	testq	%r13, %r13
	je	.L476
	.p2align 4,,10
	.p2align 3
.L473:
	movq	%r13, %rdi
	movq	0(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L473
.L476:
	movq	408(%r12), %rax
	movq	400(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	400(%r12), %rdi
	leaq	448(%r12), %rax
	movq	$0, 424(%r12)
	movq	$0, 416(%r12)
	cmpq	%rax, %rdi
	je	.L474
	call	_ZdlPv@PLT
.L474:
	movq	376(%r12), %rdi
	testq	%rdi, %rdi
	je	.L477
	call	_ZdlPv@PLT
.L477:
	movq	296(%r12), %rdi
	testq	%rdi, %rdi
	je	.L478
	movq	368(%r12), %rax
	movq	336(%r12), %r13
	leaq	8(%rax), %r14
	cmpq	%r13, %r14
	jbe	.L479
	.p2align 4,,10
	.p2align 3
.L480:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %r14
	ja	.L480
	movq	296(%r12), %rdi
.L479:
	call	_ZdlPv@PLT
.L478:
	movq	216(%r12), %rdi
	testq	%rdi, %rdi
	je	.L481
	movq	288(%r12), %rax
	movq	256(%r12), %r13
	leaq	8(%rax), %r14
	cmpq	%r13, %r14
	jbe	.L482
	.p2align 4,,10
	.p2align 3
.L483:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %r14
	ja	.L483
	movq	216(%r12), %rdi
.L482:
	call	_ZdlPv@PLT
.L481:
	movl	$488, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L471:
	movq	$0, -472(%rbp)
.L484:
	movq	-112(%rbp), %r12
	leaq	16+_ZTVN2v88internal21HeapSnapshotGeneratorE(%rip), %rax
	movq	%rax, -464(%rbp)
	testq	%r12, %r12
	je	.L489
	.p2align 4,,10
	.p2align 3
.L486:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L486
.L489:
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-128(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	$0, -112(%rbp)
	cmpq	%rax, %rdi
	je	.L487
	call	_ZdlPv@PLT
.L487:
	movq	-144(%rbp), %r12
	leaq	16+_ZTVN2v88internal14V8HeapExplorerE(%rip), %r14
	testq	%r12, %r12
	je	.L490
	movq	(%r12), %rax
	leaq	_ZN2v88internal14V8HeapExplorerD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L491
	movq	232(%r12), %rdi
	movq	%r14, (%r12)
	testq	%rdi, %rdi
	je	.L492
	call	_ZdlPv@PLT
.L492:
	movq	184(%r12), %r13
	testq	%r13, %r13
	je	.L496
	.p2align 4,,10
	.p2align 3
.L493:
	movq	%r13, %rdi
	movq	0(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L493
.L496:
	movq	176(%r12), %rax
	movq	168(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	168(%r12), %rdi
	leaq	216(%r12), %rax
	movq	$0, 192(%r12)
	movq	$0, 184(%r12)
	cmpq	%rax, %rdi
	je	.L494
	call	_ZdlPv@PLT
.L494:
	movq	128(%r12), %r13
	testq	%r13, %r13
	je	.L500
	.p2align 4,,10
	.p2align 3
.L497:
	movq	%r13, %rdi
	movq	0(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L497
.L500:
	movq	120(%r12), %rax
	movq	112(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	112(%r12), %rdi
	leaq	160(%r12), %rax
	movq	$0, 136(%r12)
	movq	$0, 128(%r12)
	cmpq	%rax, %rdi
	je	.L498
	call	_ZdlPv@PLT
.L498:
	movq	72(%r12), %r13
	testq	%r13, %r13
	je	.L504
	.p2align 4,,10
	.p2align 3
.L501:
	movq	%r13, %rdi
	movq	0(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L501
.L504:
	movq	64(%r12), %rax
	movq	56(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	56(%r12), %rdi
	leaq	104(%r12), %rax
	movq	$0, 80(%r12)
	movq	$0, 72(%r12)
	cmpq	%rax, %rdi
	je	.L502
	call	_ZdlPv@PLT
.L502:
	movl	$272, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L490:
	movq	-208(%rbp), %rdi
	movq	%r14, -440(%rbp)
	testq	%rdi, %rdi
	je	.L505
	call	_ZdlPv@PLT
.L505:
	movq	-256(%rbp), %r12
	testq	%r12, %r12
	je	.L509
	.p2align 4,,10
	.p2align 3
.L506:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L506
.L509:
	movq	-264(%rbp), %rax
	movq	-272(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-272(%rbp), %rdi
	leaq	-224(%rbp), %rax
	movq	$0, -248(%rbp)
	movq	$0, -256(%rbp)
	cmpq	%rax, %rdi
	je	.L507
	call	_ZdlPv@PLT
.L507:
	movq	-312(%rbp), %r12
	testq	%r12, %r12
	je	.L513
	.p2align 4,,10
	.p2align 3
.L510:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L510
.L513:
	movq	-320(%rbp), %rax
	movq	-328(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-328(%rbp), %rdi
	leaq	-280(%rbp), %rax
	movq	$0, -304(%rbp)
	movq	$0, -312(%rbp)
	cmpq	%rax, %rdi
	je	.L511
	call	_ZdlPv@PLT
.L511:
	movq	-368(%rbp), %r12
	testq	%r12, %r12
	je	.L517
	.p2align 4,,10
	.p2align 3
.L514:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L514
.L517:
	movq	-376(%rbp), %rax
	movq	-384(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-384(%rbp), %rdi
	leaq	-336(%rbp), %rax
	movq	$0, -360(%rbp)
	movq	$0, -368(%rbp)
	cmpq	%rax, %rdi
	je	.L515
	call	_ZdlPv@PLT
.L515:
	movq	8(%rbx), %rdi
	call	_ZN2v88internal14HeapObjectsMap17RemoveDeadEntriesEv@PLT
	movq	8(%rbx), %rax
	movb	$1, 56(%rbx)
	movl	$4, %esi
	movq	80(%rax), %rax
	movq	3880(%rax), %rdi
	addq	$48, %rdi
	call	_ZN2v88internal19DebugFeatureTracker5TrackENS1_7FeatureE@PLT
	movq	-472(%rbp), %rax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L586
	addq	$440, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	movq	24(%rbx), %rsi
	cmpq	32(%rbx), %rsi
	je	.L485
	movq	-472(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 24(%rbx)
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L491:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L485:
	leaq	-472(%rbp), %rdx
	leaq	16(%rbx), %rdi
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal12HeapSnapshotESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJRPS3_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	jmp	.L484
.L586:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19890:
	.size	_ZN2v88internal12HeapProfiler12TakeSnapshotEPNS_15ActivityControlEPNS_12HeapProfiler18ObjectNameResolverE, .-_ZN2v88internal12HeapProfiler12TakeSnapshotEPNS_15ActivityControlEPNS_12HeapProfiler18ObjectNameResolverE
	.section	.text._ZNSt6vectorISt4pairIPFvPN2v87IsolateEPNS1_13EmbedderGraphEPvES6_ESaIS9_EE17_M_realloc_insertIJS9_EEEvN9__gnu_cxx17__normal_iteratorIPS9_SB_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIPFvPN2v87IsolateEPNS1_13EmbedderGraphEPvES6_ESaIS9_EE17_M_realloc_insertIJS9_EEEvN9__gnu_cxx17__normal_iteratorIPS9_SB_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIPFvPN2v87IsolateEPNS1_13EmbedderGraphEPvES6_ESaIS9_EE17_M_realloc_insertIJS9_EEEvN9__gnu_cxx17__normal_iteratorIPS9_SB_EEDpOT_
	.type	_ZNSt6vectorISt4pairIPFvPN2v87IsolateEPNS1_13EmbedderGraphEPvES6_ESaIS9_EE17_M_realloc_insertIJS9_EEEvN9__gnu_cxx17__normal_iteratorIPS9_SB_EEDpOT_, @function
_ZNSt6vectorISt4pairIPFvPN2v87IsolateEPNS1_13EmbedderGraphEPvES6_ESaIS9_EE17_M_realloc_insertIJS9_EEEvN9__gnu_cxx17__normal_iteratorIPS9_SB_EEDpOT_:
.LFB24860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$576460752303423487, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$4, %rax
	cmpq	%rdi, %rax
	je	.L606
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L597
	movabsq	$9223372036854775792, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L607
.L589:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	16(%r14), %r8
.L596:
	movq	8(%rdx), %rsi
	movq	(%rdx), %rdx
	addq	%r14, %rcx
	movq	%rdx, (%rcx)
	movq	%rsi, 8(%rcx)
	cmpq	%r15, %rbx
	je	.L591
	movq	%r14, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L592:
	movq	(%rdx), %rdi
	movq	8(%rdx), %rsi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L592
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	16(%r14,%rdx), %r8
.L591:
	cmpq	%r12, %rbx
	je	.L593
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L594:
	movq	8(%rdx), %rsi
	movq	(%rdx), %rdi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%r12, %rdx
	jne	.L594
	subq	%rbx, %rdx
	addq	%rdx, %r8
.L593:
	testq	%r15, %r15
	je	.L595
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L595:
	movq	%r14, %xmm0
	movq	%r8, %xmm1
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L590
	movl	$16, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L597:
	movl	$16, %esi
	jmp	.L589
.L590:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$4, %rax
	movq	%rax, %rsi
	jmp	.L589
.L606:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24860:
	.size	_ZNSt6vectorISt4pairIPFvPN2v87IsolateEPNS1_13EmbedderGraphEPvES6_ESaIS9_EE17_M_realloc_insertIJS9_EEEvN9__gnu_cxx17__normal_iteratorIPS9_SB_EEDpOT_, .-_ZNSt6vectorISt4pairIPFvPN2v87IsolateEPNS1_13EmbedderGraphEPvES6_ESaIS9_EE17_M_realloc_insertIJS9_EEEvN9__gnu_cxx17__normal_iteratorIPS9_SB_EEDpOT_
	.section	.text._ZN2v88internal12HeapProfiler29AddBuildEmbedderGraphCallbackEPFvPNS_7IsolateEPNS_13EmbedderGraphEPvES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler29AddBuildEmbedderGraphCallbackEPFvPNS_7IsolateEPNS_13EmbedderGraphEPvES6_
	.type	_ZN2v88internal12HeapProfiler29AddBuildEmbedderGraphCallbackEPFvPNS_7IsolateEPNS_13EmbedderGraphEPvES6_, @function
_ZN2v88internal12HeapProfiler29AddBuildEmbedderGraphCallbackEPFvPNS_7IsolateEPNS_13EmbedderGraphEPvES6_:
.LFB19869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	120(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -32(%rbp)
	movq	%rdx, -24(%rbp)
	cmpq	128(%rdi), %r8
	je	.L609
	movq	%rsi, (%r8)
	movq	%rdx, 8(%r8)
	addq	$16, 120(%rdi)
.L608:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L613
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L609:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	addq	$112, %rdi
	movq	%r8, %rsi
	call	_ZNSt6vectorISt4pairIPFvPN2v87IsolateEPNS1_13EmbedderGraphEPvES6_ESaIS9_EE17_M_realloc_insertIJS9_EEEvN9__gnu_cxx17__normal_iteratorIPS9_SB_EEDpOT_
	jmp	.L608
.L613:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19869:
	.size	_ZN2v88internal12HeapProfiler29AddBuildEmbedderGraphCallbackEPFvPNS_7IsolateEPNS_13EmbedderGraphEPvES6_, .-_ZN2v88internal12HeapProfiler29AddBuildEmbedderGraphCallbackEPFvPNS_7IsolateEPNS_13EmbedderGraphEPvES6_
	.section	.text._ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPSt4pairIPFvPN2v87IsolateEPNS3_13EmbedderGraphEPvES8_ESt6vectorISB_SaISB_EEEENS0_5__ops16_Iter_equals_valIKSB_EEET_SL_SL_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPSt4pairIPFvPN2v87IsolateEPNS3_13EmbedderGraphEPvES8_ESt6vectorISB_SaISB_EEEENS0_5__ops16_Iter_equals_valIKSB_EEET_SL_SL_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPSt4pairIPFvPN2v87IsolateEPNS3_13EmbedderGraphEPvES8_ESt6vectorISB_SaISB_EEEENS0_5__ops16_Iter_equals_valIKSB_EEET_SL_SL_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPSt4pairIPFvPN2v87IsolateEPNS3_13EmbedderGraphEPvES8_ESt6vectorISB_SaISB_EEEENS0_5__ops16_Iter_equals_valIKSB_EEET_SL_SL_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPSt4pairIPFvPN2v87IsolateEPNS3_13EmbedderGraphEPvES8_ESt6vectorISB_SaISB_EEEENS0_5__ops16_Iter_equals_valIKSB_EEET_SL_SL_T0_St26random_access_iterator_tag:
.LFB24866:
	.cfi_startproc
	endbr64
	movq	%rsi, %rcx
	subq	%rdi, %rcx
	movq	%rcx, %rax
	sarq	$6, %rcx
	sarq	$4, %rax
	testq	%rcx, %rcx
	jle	.L615
	salq	$6, %rcx
	movq	(%rdx), %rax
	addq	%rdi, %rcx
.L626:
	cmpq	(%rdi), %rax
	je	.L641
.L616:
	cmpq	16(%rdi), %rax
	je	.L642
.L618:
	cmpq	32(%rdi), %rax
	je	.L643
.L621:
	cmpq	48(%rdi), %rax
	je	.L644
.L624:
	addq	$64, %rdi
	cmpq	%rdi, %rcx
	jne	.L626
	movq	%rsi, %rax
	subq	%rdi, %rax
	sarq	$4, %rax
.L615:
	cmpq	$2, %rax
	je	.L627
	cmpq	$3, %rax
	je	.L628
	cmpq	$1, %rax
	je	.L645
.L630:
	movq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L641:
	movq	8(%rdx), %r8
	cmpq	%r8, 8(%rdi)
	jne	.L616
.L638:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L642:
	movq	8(%rdx), %r9
	cmpq	%r9, 24(%rdi)
	jne	.L618
	leaq	16(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L643:
	movq	8(%rdx), %r10
	cmpq	%r10, 40(%rdi)
	jne	.L621
	leaq	32(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L644:
	movq	8(%rdx), %r11
	cmpq	%r11, 56(%rdi)
	jne	.L624
	leaq	48(%rdi), %rax
	ret
.L645:
	movq	(%rdx), %rcx
.L636:
	cmpq	%rcx, (%rdi)
	jne	.L630
	movq	8(%rdx), %rax
	cmpq	%rax, 8(%rdi)
	jne	.L630
	jmp	.L638
.L628:
	movq	(%rdx), %rcx
	cmpq	%rcx, (%rdi)
	je	.L646
.L631:
	addq	$16, %rdi
.L633:
	cmpq	%rcx, (%rdi)
	je	.L647
.L634:
	addq	$16, %rdi
	jmp	.L636
.L627:
	movq	(%rdx), %rcx
	jmp	.L633
.L647:
	movq	8(%rdx), %r11
	movq	%rdi, %rax
	cmpq	%r11, 8(%rdi)
	jne	.L634
	ret
.L646:
	movq	8(%rdx), %r11
	movq	%rdi, %rax
	cmpq	%r11, 8(%rdi)
	jne	.L631
	ret
	.cfi_endproc
.LFE24866:
	.size	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPSt4pairIPFvPN2v87IsolateEPNS3_13EmbedderGraphEPvES8_ESt6vectorISB_SaISB_EEEENS0_5__ops16_Iter_equals_valIKSB_EEET_SL_SL_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPSt4pairIPFvPN2v87IsolateEPNS3_13EmbedderGraphEPvES8_ESt6vectorISB_SaISB_EEEENS0_5__ops16_Iter_equals_valIKSB_EEET_SL_SL_T0_St26random_access_iterator_tag
	.section	.text._ZN2v88internal12HeapProfiler32RemoveBuildEmbedderGraphCallbackEPFvPNS_7IsolateEPNS_13EmbedderGraphEPvES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HeapProfiler32RemoveBuildEmbedderGraphCallbackEPFvPNS_7IsolateEPNS_13EmbedderGraphEPvES6_
	.type	_ZN2v88internal12HeapProfiler32RemoveBuildEmbedderGraphCallbackEPFvPNS_7IsolateEPNS_13EmbedderGraphEPvES6_, @function
_ZN2v88internal12HeapProfiler32RemoveBuildEmbedderGraphCallbackEPFvPNS_7IsolateEPNS_13EmbedderGraphEPvES6_:
.LFB19878:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -48(%rbp)
	movq	120(%rdi), %rsi
	movq	112(%rdi), %rdi
	movq	%rdx, -40(%rbp)
	leaq	-48(%rbp), %rdx
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPSt4pairIPFvPN2v87IsolateEPNS3_13EmbedderGraphEPvES8_ESt6vectorISB_SaISB_EEEENS0_5__ops16_Iter_equals_valIKSB_EEET_SL_SL_T0_St26random_access_iterator_tag
	movq	120(%rbx), %rcx
	cmpq	%rax, %rcx
	je	.L648
	movq	%rax, %rdx
	addq	$16, %rax
	cmpq	%rax, %rcx
	je	.L650
	movq	%rcx, %rsi
	subq	%rax, %rsi
	movq	%rsi, %rax
	sarq	$4, %rax
	testq	%rsi, %rsi
	jle	.L653
	.p2align 4,,10
	.p2align 3
.L651:
	movq	16(%rdx), %rcx
	addq	$16, %rdx
	movq	%rcx, -16(%rdx)
	movq	8(%rdx), %rcx
	movq	%rcx, -8(%rdx)
	subq	$1, %rax
	jne	.L651
	movq	120(%rbx), %rax
.L650:
	subq	$16, %rax
	movq	%rax, 120(%rbx)
.L648:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L656
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L653:
	.cfi_restore_state
	movq	%rcx, %rax
	jmp	.L650
.L656:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19878:
	.size	_ZN2v88internal12HeapProfiler32RemoveBuildEmbedderGraphCallbackEPFvPNS_7IsolateEPNS_13EmbedderGraphEPvES6_, .-_ZN2v88internal12HeapProfiler32RemoveBuildEmbedderGraphCallbackEPFvPNS_7IsolateEPNS_13EmbedderGraphEPvES6_
	.section	.text._ZN2v88internal14V8HeapExplorerD2Ev,"axG",@progbits,_ZN2v88internal14V8HeapExplorerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14V8HeapExplorerD2Ev
	.type	_ZN2v88internal14V8HeapExplorerD2Ev, @function
_ZN2v88internal14V8HeapExplorerD2Ev:
.LFB26262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14V8HeapExplorerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	232(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L658
	call	_ZdlPv@PLT
.L658:
	movq	184(%rbx), %r12
	testq	%r12, %r12
	je	.L659
	.p2align 4,,10
	.p2align 3
.L660:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L660
.L659:
	movq	176(%rbx), %rax
	movq	168(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	168(%rbx), %rdi
	leaq	216(%rbx), %rax
	movq	$0, 192(%rbx)
	movq	$0, 184(%rbx)
	cmpq	%rax, %rdi
	je	.L661
	call	_ZdlPv@PLT
.L661:
	movq	128(%rbx), %r12
	testq	%r12, %r12
	je	.L662
	.p2align 4,,10
	.p2align 3
.L663:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L663
.L662:
	movq	120(%rbx), %rax
	movq	112(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	112(%rbx), %rdi
	leaq	160(%rbx), %rax
	movq	$0, 136(%rbx)
	movq	$0, 128(%rbx)
	cmpq	%rax, %rdi
	je	.L664
	call	_ZdlPv@PLT
.L664:
	movq	72(%rbx), %r12
	testq	%r12, %r12
	je	.L665
	.p2align 4,,10
	.p2align 3
.L666:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L666
.L665:
	movq	64(%rbx), %rax
	movq	56(%rbx), %rdi
	xorl	%esi, %esi
	addq	$104, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L657
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L657:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26262:
	.size	_ZN2v88internal14V8HeapExplorerD2Ev, .-_ZN2v88internal14V8HeapExplorerD2Ev
	.weak	_ZN2v88internal14V8HeapExplorerD1Ev
	.set	_ZN2v88internal14V8HeapExplorerD1Ev,_ZN2v88internal14V8HeapExplorerD2Ev
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal12HeapProfilerC2EPNS0_4HeapE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal12HeapProfilerC2EPNS0_4HeapE, @function
_GLOBAL__sub_I__ZN2v88internal12HeapProfilerC2EPNS0_4HeapE:
.LFB26454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26454:
	.size	_GLOBAL__sub_I__ZN2v88internal12HeapProfilerC2EPNS0_4HeapE, .-_GLOBAL__sub_I__ZN2v88internal12HeapProfilerC2EPNS0_4HeapE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal12HeapProfilerC2EPNS0_4HeapE
	.weak	_ZTVN2v88internal12HeapProfilerE
	.section	.data.rel.ro.local._ZTVN2v88internal12HeapProfilerE,"awG",@progbits,_ZTVN2v88internal12HeapProfilerE,comdat
	.align 8
	.type	_ZTVN2v88internal12HeapProfilerE, @object
	.size	_ZTVN2v88internal12HeapProfilerE, 56
_ZTVN2v88internal12HeapProfilerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12HeapProfiler15AllocationEventEmi
	.quad	_ZN2v88internal27HeapObjectAllocationTracker9MoveEventEmmi
	.quad	_ZN2v88internal12HeapProfiler21UpdateObjectSizeEventEmi
	.quad	_ZN2v88internal12HeapProfilerD1Ev
	.quad	_ZN2v88internal12HeapProfilerD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
