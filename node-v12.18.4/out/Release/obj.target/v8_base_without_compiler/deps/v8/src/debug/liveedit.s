	.file	"liveedit.cc"
	.text
	.section	.text._ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii, @function
_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii:
.LFB20304:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	16(%rdi), %eax
	movl	20(%rdi), %r12d
	cmpl	%esi, %eax
	jle	.L2
	movl	%esi, %r13d
	cmpl	%r12d, %edx
	jl	.L11
	subl	%esi, %eax
	leal	0(,%rax,4), %r12d
.L1:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	imull	%edx, %eax
	movq	8(%rdi), %rdx
	movq	%rdi, %rbx
	addl	%esi, %eax
	cltq
	movl	(%rdx,%rax,4), %r12d
	andl	$-4, %r12d
	cmpl	$-4, %r12d
	jne	.L1
	movq	(%rdi), %rdi
	movl	%r14d, %edx
	movq	(%rdi), %rax
	call	*16(%rax)
	leal	1(%r13), %esi
	leal	1(%r14), %r8d
	testb	%al, %al
	je	.L5
	movl	%r8d, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %r12d
	movl	%eax, %ecx
.L6:
	movl	16(%rbx), %edx
	movq	8(%rbx), %rax
	imull	%r14d, %edx
	leal	(%rdx,%r13), %esi
	movslq	%esi, %rsi
	movl	%ecx, (%rax,%rsi,4)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	subl	%edx, %r12d
	sall	$2, %r12d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	movl	%r14d, %edx
	movq	%rbx, %rdi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	-52(%rbp), %r8d
	movl	%r13d, %esi
	movq	%rbx, %rdi
	leal	4(%rax), %r12d
	movl	%eax, %r15d
	movl	%r8d, %edx
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%r12d, %ecx
	orl	$3, %ecx
	cmpl	%eax, %r15d
	je	.L6
	jge	.L8
	movl	%r12d, %ecx
	orl	$1, %ecx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L8:
	leal	4(%rax), %r12d
	movl	%r12d, %ecx
	orl	$2, %ecx
	jmp	.L6
	.cfi_endproc
.LFE20304:
	.size	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii, .-_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	.section	.text._ZN2v88internal12_GLOBAL__N_118TokensCompareInput10GetLength1Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118TokensCompareInput10GetLength1Ev, @function
_ZN2v88internal12_GLOBAL__N_118TokensCompareInput10GetLength1Ev:
.LFB20333:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %eax
	ret
	.cfi_endproc
.LFE20333:
	.size	_ZN2v88internal12_GLOBAL__N_118TokensCompareInput10GetLength1Ev, .-_ZN2v88internal12_GLOBAL__N_118TokensCompareInput10GetLength1Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_118TokensCompareInput10GetLength2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118TokensCompareInput10GetLength2Ev, @function
_ZN2v88internal12_GLOBAL__N_118TokensCompareInput10GetLength2Ev:
.LFB20334:
	.cfi_startproc
	endbr64
	movl	36(%rdi), %eax
	ret
	.cfi_endproc
.LFE20334:
	.size	_ZN2v88internal12_GLOBAL__N_118TokensCompareInput10GetLength2Ev, .-_ZN2v88internal12_GLOBAL__N_118TokensCompareInput10GetLength2Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput10GetLength1Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput10GetLength1Ev, @function
_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput10GetLength1Ev:
.LFB20364:
	.cfi_startproc
	endbr64
	movl	64(%rdi), %eax
	ret
	.cfi_endproc
.LFE20364:
	.size	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput10GetLength1Ev, .-_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput10GetLength1Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput10GetLength2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput10GetLength2Ev, @function
_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput10GetLength2Ev:
.LFB20365:
	.cfi_startproc
	endbr64
	movl	68(%rdi), %eax
	ret
	.cfi_endproc
.LFE20365:
	.size	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput10GetLength2Ev, .-_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput10GetLength2Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput12SetSubrange1Eii,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput12SetSubrange1Eii, @function
_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput12SetSubrange1Eii:
.LFB20367:
	.cfi_startproc
	endbr64
	movl	%esi, 56(%rdi)
	movl	%edx, 64(%rdi)
	ret
	.cfi_endproc
.LFE20367:
	.size	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput12SetSubrange1Eii, .-_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput12SetSubrange1Eii
	.section	.text._ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput12SetSubrange2Eii,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput12SetSubrange2Eii, @function
_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput12SetSubrange2Eii:
.LFB20368:
	.cfi_startproc
	endbr64
	movl	%esi, 60(%rdi)
	movl	%edx, 68(%rdi)
	ret
	.cfi_endproc
.LFE20368:
	.size	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput12SetSubrange2Eii, .-_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput12SetSubrange2Eii
	.section	.text._ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput12SetSubrange1Eii,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput12SetSubrange1Eii, @function
_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput12SetSubrange1Eii:
.LFB20380:
	.cfi_startproc
	endbr64
	movl	%esi, 64(%rdi)
	ret
	.cfi_endproc
.LFE20380:
	.size	_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput12SetSubrange1Eii, .-_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput12SetSubrange1Eii
	.section	.text._ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput12SetSubrange2Eii,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput12SetSubrange2Eii, @function
_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput12SetSubrange2Eii:
.LFB20381:
	.cfi_startproc
	endbr64
	movl	%esi, 68(%rdi)
	ret
	.cfi_endproc
.LFE20381:
	.size	_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput12SetSubrange2Eii, .-_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput12SetSubrange2Eii
	.section	.text._ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_T0_SJ_T1_T2_,"ax",@progbits
	.p2align 4
	.type	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_T0_SJ_T1_T2_, @function
_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_T0_SJ_T1_T2_:
.LFB27114:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	andl	$1, %edi
	subq	$104, %rsp
	movq	%rsi, -104(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%r8, -128(%rbp)
	movq	%r9, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1(%rdx), %rax
	movq	%rdi, -136(%rbp)
	movq	%rax, %r13
	shrq	$63, %r13
	addq	%rax, %r13
	movq	%r13, %rax
	sarq	%rax
	movq	%rax, -112(%rbp)
	cmpq	%rax, %rsi
	jge	.L21
	movq	%rsi, %rdx
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L44:
	movdqu	(%r12), %xmm1
	movq	-96(%rbp), %rdx
	movups	%xmm1, (%rbx,%r13)
	cmpq	-112(%rbp), %rdx
	jge	.L40
.L43:
	movq	%rdx, %r13
.L25:
	leaq	1(%r13), %rax
	salq	$4, %r13
	leaq	(%rax,%rax), %rdx
	salq	$5, %rax
	leaq	-1(%rdx), %r15
	leaq	(%rbx,%rax), %r12
	movq	%rdx, -96(%rbp)
	movq	-88(%rbp), %rax
	movq	%r15, %r14
	movq	%r12, %rdi
	salq	$4, %r14
	addq	%rbx, %r14
	movq	%r14, %rsi
	call	*%rax
	testb	%al, %al
	je	.L44
	movdqu	(%r14), %xmm2
	movups	%xmm2, (%rbx,%r13)
	cmpq	-112(%rbp), %r15
	jge	.L32
	movq	%r15, %r13
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%rdx, %r13
.L23:
	cmpq	$0, -136(%rbp)
	jne	.L26
.L29:
	movq	-144(%rbp), %rdx
	subq	$2, %rdx
	movq	%rdx, %r14
	shrq	$63, %r14
	addq	%rdx, %r14
	sarq	%r14
	cmpq	%r13, %r14
	jne	.L26
	leaq	1(%r13,%r13), %r13
	movq	%r13, %rax
	salq	$4, %rax
	addq	%rbx, %rax
	movdqu	(%rax), %xmm4
	movups	%xmm4, (%r12)
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L26:
	movq	-120(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -72(%rbp)
	leaq	-1(%r13), %rax
	movq	%rax, %r15
	shrq	$63, %r15
	addq	%rax, %r15
	sarq	%r15
	cmpq	-104(%rbp), %r13
	jle	.L27
	leaq	-80(%rbp), %r12
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	-1(%r15), %rsi
	movdqu	(%r14), %xmm0
	movq	%rsi, %rax
	shrq	$63, %rax
	movups	%xmm0, 0(%r13)
	movq	%r15, %r13
	addq	%rsi, %rax
	sarq	%rax
	cmpq	%r15, -104(%rbp)
	jge	.L45
	movq	%rax, %r15
.L28:
	movq	%r15, %rcx
	movq	-88(%rbp), %rax
	movq	%r12, %rsi
	salq	$4, %r13
	salq	$4, %rcx
	addq	%rbx, %r13
	leaq	(%rbx,%rcx), %r14
	movq	%r14, %rdi
	call	*%rax
	testb	%al, %al
	jne	.L46
	movq	%r13, %r12
.L27:
	movdqu	-80(%rbp), %xmm3
	movups	%xmm3, (%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L47
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movq	%r14, %r12
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%r14, %r12
	movq	%r15, %r13
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rsi, %rcx
	salq	$4, %rcx
	cmpq	$0, -136(%rbp)
	leaq	(%rbx,%rcx), %r12
	jne	.L48
	movq	-104(%rbp), %r13
	jmp	.L29
.L48:
	movq	-120(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -72(%rbp)
	jmp	.L27
.L47:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27114:
	.size	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_T0_SJ_T1_T2_, .-_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_T0_SJ_T1_T2_
	.section	.text._ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputD2Ev, @function
_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputD2Ev:
.LFB27352:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE27352:
	.size	_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputD2Ev, .-_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputD2Ev
	.set	_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputD1Ev,_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputD2Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_121LineArrayCompareInputD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInputD2Ev, @function
_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInputD2Ev:
.LFB27356:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE27356:
	.size	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInputD2Ev, .-_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInputD2Ev
	.set	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInputD1Ev,_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInputD2Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_119TokensCompareOutputD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119TokensCompareOutputD2Ev, @function
_ZN2v88internal12_GLOBAL__N_119TokensCompareOutputD2Ev:
.LFB27360:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE27360:
	.size	_ZN2v88internal12_GLOBAL__N_119TokensCompareOutputD2Ev, .-_ZN2v88internal12_GLOBAL__N_119TokensCompareOutputD2Ev
	.set	_ZN2v88internal12_GLOBAL__N_119TokensCompareOutputD1Ev,_ZN2v88internal12_GLOBAL__N_119TokensCompareOutputD2Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_118TokensCompareInputD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118TokensCompareInputD2Ev, @function
_ZN2v88internal12_GLOBAL__N_118TokensCompareInputD2Ev:
.LFB27364:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE27364:
	.size	_ZN2v88internal12_GLOBAL__N_118TokensCompareInputD2Ev, .-_ZN2v88internal12_GLOBAL__N_118TokensCompareInputD2Ev
	.set	_ZN2v88internal12_GLOBAL__N_118TokensCompareInputD1Ev,_ZN2v88internal12_GLOBAL__N_118TokensCompareInputD2Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_121LineArrayCompareInputD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInputD0Ev, @function
_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInputD0Ev:
.LFB27358:
	.cfi_startproc
	endbr64
	movl	$72, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27358:
	.size	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInputD0Ev, .-_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInputD0Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputD0Ev, @function
_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputD0Ev:
.LFB27354:
	.cfi_startproc
	endbr64
	movl	$80, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27354:
	.size	_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputD0Ev, .-_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputD0Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_118TokensCompareInputD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118TokensCompareInputD0Ev, @function
_ZN2v88internal12_GLOBAL__N_118TokensCompareInputD0Ev:
.LFB27366:
	.cfi_startproc
	endbr64
	movl	$40, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27366:
	.size	_ZN2v88internal12_GLOBAL__N_118TokensCompareInputD0Ev, .-_ZN2v88internal12_GLOBAL__N_118TokensCompareInputD0Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_119TokensCompareOutputD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119TokensCompareOutputD0Ev, @function
_ZN2v88internal12_GLOBAL__N_119TokensCompareOutputD0Ev:
.LFB27362:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27362:
	.size	_ZN2v88internal12_GLOBAL__N_119TokensCompareOutputD0Ev, .-_ZN2v88internal12_GLOBAL__N_119TokensCompareOutputD0Ev
	.section	.text._ZN2v88internal6Logger27is_listening_to_code_eventsEv,"axG",@progbits,_ZN2v88internal6Logger27is_listening_to_code_eventsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.type	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, @function
_ZN2v88internal6Logger27is_listening_to_code_eventsEv:
.LFB20025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L57
	cmpq	$0, 80(%rbx)
	setne	%al
.L57:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20025:
	.size	_ZN2v88internal6Logger27is_listening_to_code_eventsEv, .-_ZN2v88internal6Logger27is_listening_to_code_eventsEv
	.section	.text._ZN2v88internal12_GLOBAL__N_119SourcePositionEvent8LessThanERKS2_S4_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119SourcePositionEvent8LessThanERKS2_S4_, @function
_ZN2v88internal12_GLOBAL__N_119SourcePositionEvent8LessThanERKS2_S4_:
.LFB20388:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	cmpl	%eax, (%rdi)
	je	.L61
	setl	%al
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	4(%rdi), %eax
	cmpl	4(%rsi), %eax
	jne	.L71
	testl	%eax, %eax
	je	.L72
	cmpl	$1, %eax
	je	.L73
	movl	8(%rsi), %eax
	cmpl	%eax, 8(%rdi)
.L71:
	setl	%al
.L60:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	8(%rbx), %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, %r13d
	jne	.L74
	movq	8(%r12), %rdx
	movq	8(%rbx), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rdx)
	setl	%al
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L73:
	movq	8(%rdi), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	8(%rbx), %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %r13d
	jne	.L75
	movq	8(%r12), %rdx
	movq	8(%rbx), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rdx)
	setg	%al
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L74:
	movq	8(%r12), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	8(%rbx), %rdi
	movl	%eax, %r12d
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, %r12d
	setg	%al
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L75:
	movq	8(%r12), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	8(%rbx), %rdi
	movl	%eax, %r12d
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %r12d
	setg	%al
	jmp	.L60
	.cfi_endproc
.LFE20388:
	.size	_ZN2v88internal12_GLOBAL__N_119SourcePositionEvent8LessThanERKS2_S4_, .-_ZN2v88internal12_GLOBAL__N_119SourcePositionEvent8LessThanERKS2_S4_
	.section	.text._ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal12_GLOBAL__N_112FunctionDataEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal12_GLOBAL__N_112FunctionDataEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E, @function
_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal12_GLOBAL__N_112FunctionDataEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E:
.LFB24887:
	.cfi_startproc
	testq	%rsi, %rsi
	je	.L95
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L81:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal12_GLOBAL__N_112FunctionDataEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	80(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L78
	call	_ZdlPv@PLT
.L78:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L76
.L80:
	movq	%rbx, %r12
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L80
.L76:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE24887:
	.size	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal12_GLOBAL__N_112FunctionDataEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E, .-_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal12_GLOBAL__N_112FunctionDataEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	.section	.rodata._ZN2v88internal12_GLOBAL__N_117CompareSubstringsENS0_6HandleINS0_6StringEEEiS4_ii.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal12_GLOBAL__N_117CompareSubstringsENS0_6HandleINS0_6StringEEEiS4_ii,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117CompareSubstringsENS0_6HandleINS0_6StringEEEiS4_ii, @function
_ZN2v88internal12_GLOBAL__N_117CompareSubstringsENS0_6HandleINS0_6StringEEEiS4_ii:
.LFB20320:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -72(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L120
	movslq	%esi, %r12
	movslq	%ecx, %r13
	leaq	.L103(%rip), %r15
	leal	(%r8,%r12), %eax
	movq	%r13, %rbx
	movq	%r12, %r14
	movl	%eax, -84(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L121:
	movq	-72(%rbp), %rax
	movq	(%rax), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L101
	movzwl	%ax, %eax
	movslq	(%r15,%rax,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12_GLOBAL__N_117CompareSubstringsENS0_6HandleINS0_6StringEEEiS4_ii,"a",@progbits
	.align 4
	.align 4
.L103:
	.long	.L109-.L103
	.long	.L106-.L103
	.long	.L108-.L103
	.long	.L104-.L103
	.long	.L101-.L103
	.long	.L102-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L107-.L103
	.long	.L106-.L103
	.long	.L105-.L103
	.long	.L104-.L103
	.long	.L101-.L103
	.long	.L102-.L103
	.section	.text._ZN2v88internal12_GLOBAL__N_117CompareSubstringsENS0_6HandleINS0_6StringEEEiS4_ii
	.p2align 4,,10
	.p2align 3
.L102:
	movq	-96(%rbp), %rdi
	movl	%r14d, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10ThinString3GetEi@PLT
	movl	%eax, %r8d
	.p2align 4,,10
	.p2align 3
.L110:
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L101
	leaq	.L112(%rip), %rcx
	movzwl	%dx, %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal12_GLOBAL__N_117CompareSubstringsENS0_6HandleINS0_6StringEEEiS4_ii
	.align 4
	.align 4
.L112:
	.long	.L118-.L112
	.long	.L115-.L112
	.long	.L117-.L112
	.long	.L113-.L112
	.long	.L101-.L112
	.long	.L111-.L112
	.long	.L101-.L112
	.long	.L101-.L112
	.long	.L116-.L112
	.long	.L115-.L112
	.long	.L114-.L112
	.long	.L113-.L112
	.long	.L101-.L112
	.long	.L111-.L112
	.section	.text._ZN2v88internal12_GLOBAL__N_117CompareSubstringsENS0_6HandleINS0_6StringEEEiS4_ii
	.p2align 4,,10
	.p2align 3
.L104:
	movq	-96(%rbp), %rdi
	movl	%r14d, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	movl	%eax, %r8d
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L106:
	movq	-96(%rbp), %rdi
	movl	%r14d, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10ConsString3GetEi@PLT
	movl	%eax, %r8d
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L111:
	movq	-96(%rbp), %rdi
	movl	%ebx, %esi
	movl	%r8d, -88(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal10ThinString3GetEi@PLT
	movl	-88(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L119:
	cmpw	%ax, %r8w
	jne	.L123
.L126:
	addl	$1, %ebx
	addq	$1, %r13
	addl	$1, %r14d
	addq	$1, %r12
	cmpl	%r14d, -84(%rbp)
	jne	.L121
.L120:
	movl	$1, %eax
.L98:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L125
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movq	-96(%rbp), %rdi
	movl	%ebx, %esi
	movl	%r8d, -88(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	movl	-88(%rbp), %r8d
	cmpw	%ax, %r8w
	je	.L126
	.p2align 4,,10
	.p2align 3
.L123:
	xorl	%eax, %eax
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L115:
	movq	-96(%rbp), %rdi
	movl	%ebx, %esi
	movl	%r8d, -88(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal10ConsString3GetEi@PLT
	movl	-88(%rbp), %r8d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L108:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax,%r12,2), %r8d
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L109:
	leal	16(%r14,%r14), %eax
	cltq
	movzwl	-1(%rdx,%rax), %r8d
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L105:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzbl	(%rax,%r12), %r8d
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L107:
	leal	16(%r14), %eax
	cltq
	movzbl	-1(%rdx,%rax), %r8d
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L118:
	leal	16(%rbx,%rbx), %edx
	movslq	%edx, %rdx
	movzwl	-1(%rax,%rdx), %eax
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L114:
	movq	15(%rax), %rdi
	movl	%r8d, -88(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movl	-88(%rbp), %r8d
	movzbl	(%rax,%r13), %eax
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L116:
	leal	16(%rbx), %edx
	movslq	%edx, %rdx
	movzbl	-1(%rax,%rdx), %eax
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L117:
	movq	15(%rax), %rdi
	movl	%r8d, -88(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movl	-88(%rbp), %r8d
	movzwl	(%rax,%r13,2), %eax
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L101:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20320:
	.size	_ZN2v88internal12_GLOBAL__N_117CompareSubstringsENS0_6HandleINS0_6StringEEEiS4_ii, .-_ZN2v88internal12_GLOBAL__N_117CompareSubstringsENS0_6HandleINS0_6StringEEEiS4_ii
	.section	.rodata._ZNSt6vectorIN2v88internal12_GLOBAL__N_119SourcePositionEventESaIS3_EE17_M_realloc_insertIJRPNS1_15FunctionLiteralEbEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal12_GLOBAL__N_119SourcePositionEventESaIS3_EE17_M_realloc_insertIJRPNS1_15FunctionLiteralEbEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal12_GLOBAL__N_119SourcePositionEventESaIS3_EE17_M_realloc_insertIJRPNS1_15FunctionLiteralEbEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal12_GLOBAL__N_119SourcePositionEventESaIS3_EE17_M_realloc_insertIJRPNS1_15FunctionLiteralEbEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB24620:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r8
	movabsq	$576460752303423487, %rdi
	movq	%r13, %rax
	subq	%r8, %rax
	sarq	$4, %rax
	cmpq	%rdi, %rax
	je	.L148
	movq	%rsi, %r12
	subq	%r8, %rsi
	testq	%rax, %rax
	je	.L139
	movabsq	$9223372036854775792, %rbx
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L149
.L129:
	movq	%rbx, %rdi
	movq	%rcx, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rsi
	movq	%rax, %r15
	leaq	(%rax,%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rcx
	movq	%rax, -56(%rbp)
	leaq	16(%r15), %rbx
.L138:
	movq	(%rdx), %rdx
	addq	%r15, %rsi
	cmpb	$0, (%rcx)
	movq	%r8, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, %rdi
	jne	.L150
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %rdx
	movl	%eax, (%rsi)
	movl	$1, %eax
.L132:
	movl	%eax, 4(%rsi)
	movq	%rdx, 8(%rsi)
	cmpq	%r8, %r12
	je	.L133
	movq	%r15, %rdx
	movq	%r8, %rax
	.p2align 4,,10
	.p2align 3
.L134:
	movdqu	(%rax), %xmm1
	addq	$16, %rax
	addq	$16, %rdx
	movups	%xmm1, -16(%rdx)
	cmpq	%rax, %r12
	jne	.L134
	movq	%r12, %rax
	subq	%r8, %rax
	leaq	16(%r15,%rax), %rbx
.L133:
	cmpq	%r13, %r12
	je	.L135
	movq	%r12, %rax
	movq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L136:
	movdqu	(%rax), %xmm2
	addq	$16, %rax
	addq	$16, %rdx
	movups	%xmm2, -16(%rdx)
	cmpq	%r13, %rax
	jne	.L136
	subq	%r12, %rax
	addq	%rax, %rbx
.L135:
	testq	%r8, %r8
	je	.L137
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L137:
	movq	-56(%rbp), %rax
	movq	%r15, %xmm0
	movq	%rbx, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 16(%r14)
	movaps	%xmm0, (%r14)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-80(%rbp), %r8
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L149:
	testq	%r9, %r9
	jne	.L130
	movq	$0, -56(%rbp)
	movl	$16, %ebx
	xorl	%r15d, %r15d
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L139:
	movl	$16, %ebx
	jmp	.L129
.L130:
	cmpq	%rdi, %r9
	movq	%rdi, %rbx
	cmovbe	%r9, %rbx
	salq	$4, %rbx
	jmp	.L129
.L148:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24620:
	.size	_ZNSt6vectorIN2v88internal12_GLOBAL__N_119SourcePositionEventESaIS3_EE17_M_realloc_insertIJRPNS1_15FunctionLiteralEbEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal12_GLOBAL__N_119SourcePositionEventESaIS3_EE17_M_realloc_insertIJRPNS1_15FunctionLiteralEbEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal12_GLOBAL__N_119SourcePositionEventESaIS3_EE17_M_realloc_insertIJRKNS1_17SourceChangeRangeEbEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal12_GLOBAL__N_119SourcePositionEventESaIS3_EE17_M_realloc_insertIJRKNS1_17SourceChangeRangeEbEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal12_GLOBAL__N_119SourcePositionEventESaIS3_EE17_M_realloc_insertIJRKNS1_17SourceChangeRangeEbEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB24626:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$576460752303423487, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$4, %rax
	cmpq	%rdi, %rax
	je	.L172
	movq	%rsi, %r9
	movq	%rsi, %rbx
	subq	%r15, %r9
	testq	%rax, %rax
	je	.L162
	movabsq	$9223372036854775792, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L173
.L153:
	movq	%rsi, %rdi
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	%rax, %r13
	leaq	16(%rax), %r8
	addq	%rax, %rsi
.L161:
	cmpb	$0, (%rcx)
	leaq	0(%r13,%r9), %rax
	movl	4(%rdx), %edi
	movl	(%rdx), %r9d
	jne	.L164
	movl	%edi, %r10d
	movl	$3, %ecx
.L155:
	movl	%ecx, 4(%rax)
	movl	12(%rdx), %ecx
	subl	%r9d, %edi
	subl	8(%rdx), %ecx
	movl	%r10d, (%rax)
	movl	%ecx, %edx
	subl	%edi, %edx
	movl	%edx, 8(%rax)
	cmpq	%r15, %rbx
	je	.L156
	movq	%r13, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L157:
	movdqu	(%rdx), %xmm1
	addq	$16, %rdx
	addq	$16, %rcx
	movups	%xmm1, -16(%rcx)
	cmpq	%rdx, %rbx
	jne	.L157
	movq	%rbx, %rax
	subq	%r15, %rax
	leaq	16(%r13,%rax), %r8
.L156:
	cmpq	%r12, %rbx
	je	.L158
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L159:
	movdqu	(%rdx), %xmm2
	addq	$16, %rdx
	addq	$16, %rcx
	movups	%xmm2, -16(%rcx)
	cmpq	%r12, %rdx
	jne	.L159
	subq	%rbx, %rdx
	addq	%rdx, %r8
.L158:
	testq	%r15, %r15
	je	.L160
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %r8
.L160:
	movq	%r13, %xmm0
	movq	%r8, %xmm3
	movq	%rsi, 16(%r14)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, (%r14)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movl	%r9d, %r10d
	movl	$2, %ecx
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L173:
	testq	%r8, %r8
	jne	.L154
	movl	$16, %r8d
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L162:
	movl	$16, %esi
	jmp	.L153
.L154:
	cmpq	%rdi, %r8
	movq	%rdi, %rsi
	cmovbe	%r8, %rsi
	salq	$4, %rsi
	jmp	.L153
.L172:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24626:
	.size	_ZNSt6vectorIN2v88internal12_GLOBAL__N_119SourcePositionEventESaIS3_EE17_M_realloc_insertIJRKNS1_17SourceChangeRangeEbEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal12_GLOBAL__N_119SourcePositionEventESaIS3_EE17_M_realloc_insertIJRKNS1_17SourceChangeRangeEbEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZN2v88internal12_GLOBAL__N_118TokensCompareInput6EqualsEii,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118TokensCompareInput6EqualsEii, @function
_ZN2v88internal12_GLOBAL__N_118TokensCompareInput6EqualsEii:
.LFB20335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	addl	16(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	(%rax), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L175
	movl	%edx, %ebx
	movzwl	%ax, %eax
	leaq	.L177(%rip), %rdx
	movq	%rdi, %r12
	movslq	(%rdx,%rax,4), %rax
	movl	%esi, %r13d
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12_GLOBAL__N_118TokensCompareInput6EqualsEii,"a",@progbits
	.align 4
	.align 4
.L177:
	.long	.L183-.L177
	.long	.L180-.L177
	.long	.L182-.L177
	.long	.L178-.L177
	.long	.L175-.L177
	.long	.L176-.L177
	.long	.L175-.L177
	.long	.L175-.L177
	.long	.L181-.L177
	.long	.L180-.L177
	.long	.L179-.L177
	.long	.L178-.L177
	.long	.L175-.L177
	.long	.L176-.L177
	.section	.text._ZN2v88internal12_GLOBAL__N_118TokensCompareInput6EqualsEii
	.p2align 4,,10
	.p2align 3
.L176:
	leaq	-48(%rbp), %rdi
	movq	%rcx, -48(%rbp)
	call	_ZN2v88internal10ThinString3GetEi@PLT
	movl	%eax, %r13d
	.p2align 4,,10
	.p2align 3
.L184:
	movq	24(%r12), %rax
	addl	32(%r12), %ebx
	movq	(%rax), %rdi
	movq	-1(%rdi), %rax
	movzwl	11(%rax), %ecx
	andl	$15, %ecx
	cmpw	$13, %cx
	ja	.L175
	leaq	.L186(%rip), %rax
	movzwl	%cx, %ecx
	movslq	(%rax,%rcx,4), %rcx
	addq	%rax, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internal12_GLOBAL__N_118TokensCompareInput6EqualsEii
	.align 4
	.align 4
.L186:
	.long	.L192-.L186
	.long	.L189-.L186
	.long	.L191-.L186
	.long	.L187-.L186
	.long	.L175-.L186
	.long	.L185-.L186
	.long	.L175-.L186
	.long	.L175-.L186
	.long	.L190-.L186
	.long	.L189-.L186
	.long	.L188-.L186
	.long	.L187-.L186
	.long	.L175-.L186
	.long	.L185-.L186
	.section	.text._ZN2v88internal12_GLOBAL__N_118TokensCompareInput6EqualsEii
	.p2align 4,,10
	.p2align 3
.L180:
	leaq	-48(%rbp), %rdi
	movq	%rcx, -48(%rbp)
	call	_ZN2v88internal10ConsString3GetEi@PLT
	movl	%eax, %r13d
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L178:
	leaq	-48(%rbp), %rdi
	movq	%rcx, -48(%rbp)
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	movl	%eax, %r13d
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%rdi, -48(%rbp)
	movl	%ebx, %esi
	leaq	-48(%rbp), %rdi
	call	_ZN2v88internal10ThinString3GetEi@PLT
	.p2align 4,,10
	.p2align 3
.L193:
	cmpw	%ax, %r13w
	sete	%al
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L196
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	%rdi, -48(%rbp)
	movl	%ebx, %esi
	leaq	-48(%rbp), %rdi
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L189:
	movq	%rdi, -48(%rbp)
	movl	%ebx, %esi
	leaq	-48(%rbp), %rdi
	call	_ZN2v88internal10ConsString3GetEi@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L182:
	movq	15(%rcx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r13d, %rsi
	movzwl	(%rax,%rsi,2), %r13d
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L179:
	movq	15(%rcx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r13d, %rsi
	movzbl	(%rax,%rsi), %r13d
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L183:
	leal	16(%rsi,%rsi), %eax
	cltq
	movzwl	-1(%rcx,%rax), %r13d
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L181:
	leal	16(%rsi), %esi
	movslq	%esi, %rsi
	movzbl	-1(%rcx,%rsi), %r13d
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L192:
	leal	16(%rbx,%rbx), %eax
	cltq
	movzwl	-1(%rdi,%rax), %eax
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L188:
	movq	15(%rdi), %rdi
	movslq	%ebx, %rbx
	movq	(%rdi), %rax
	call	*48(%rax)
	movzbl	(%rax,%rbx), %eax
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L190:
	leal	16(%rbx), %ebx
	movslq	%ebx, %rbx
	movzbl	-1(%rdi,%rbx), %eax
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L191:
	movq	15(%rdi), %rdi
	movslq	%ebx, %rbx
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax,%rbx,2), %eax
	jmp	.L193
.L175:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L196:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20335:
	.size	_ZN2v88internal12_GLOBAL__N_118TokensCompareInput6EqualsEii, .-_ZN2v88internal12_GLOBAL__N_118TokensCompareInput6EqualsEii
	.section	.text._ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput6EqualsEii,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput6EqualsEii, @function
_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput6EqualsEii:
.LFB20366:
	.cfi_startproc
	endbr64
	addl	60(%rdi), %edx
	movl	%esi, %eax
	addl	56(%rdi), %eax
	je	.L207
	movq	24(%rdi), %r8
	leal	-1(%rax), %esi
	movq	(%r8), %rcx
	cmpl	%esi, 11(%rcx)
	je	.L209
	leal	8(,%rax,8), %esi
	movslq	%esi, %rsi
	movq	-1(%rcx,%rsi), %rsi
	sarq	$32, %rsi
	addl	$1, %esi
	testl	%edx, %edx
	jne	.L210
.L208:
	movq	40(%rdi), %r9
	xorl	%ecx, %ecx
.L200:
	movq	(%r8), %r8
	cmpl	%eax, 11(%r8)
	je	.L211
.L202:
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%r8,%rax), %r8
	movq	(%r9), %rax
	sarq	$32, %r8
	addl	$1, %r8d
	cmpl	%edx, 11(%rax)
	je	.L212
.L204:
	leal	16(,%rdx,8), %edx
	subl	%esi, %r8d
	movslq	%edx, %rdx
	movq	-1(%rax,%rdx), %rax
	sarq	$32, %rax
	addl	$1, %eax
	subl	%ecx, %eax
	cmpl	%eax, %r8d
	je	.L213
.L206:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	movq	24(%rdi), %r8
	xorl	%esi, %esi
.L198:
	testl	%edx, %edx
	je	.L208
.L210:
	movq	40(%rdi), %r9
	leal	-1(%rdx), %r10d
	movq	(%r9), %rcx
	cmpl	%r10d, 11(%rcx)
	je	.L214
	leal	8(,%rdx,8), %r10d
	movslq	%r10d, %r10
	movq	-1(%rcx,%r10), %rcx
	movq	(%r8), %r8
	sarq	$32, %rcx
	addl	$1, %ecx
	cmpl	%eax, 11(%r8)
	jne	.L202
.L211:
	movq	(%r9), %rax
	movl	32(%rdi), %r8d
	cmpl	%edx, 11(%rax)
	jne	.L204
.L212:
	movl	48(%rdi), %eax
	subl	%esi, %r8d
	subl	%ecx, %eax
	cmpl	%eax, %r8d
	jne	.L206
.L213:
	movq	16(%rdi), %rdx
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal12_GLOBAL__N_117CompareSubstringsENS0_6HandleINS0_6StringEEEiS4_ii
	.p2align 4,,10
	.p2align 3
.L214:
	movl	48(%rdi), %ecx
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L209:
	movl	32(%rdi), %esi
	jmp	.L198
	.cfi_endproc
.LFE20366:
	.size	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput6EqualsEii, .-_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput6EqualsEii
	.section	.text._ZN2v88internal12_GLOBAL__N_115FunctionDataMapD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_115FunctionDataMapD2Ev, @function
_ZN2v88internal12_GLOBAL__N_115FunctionDataMapD2Ev:
.LFB27348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_115FunctionDataMapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	24(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L215
	leaq	8(%rdi), %r13
.L220:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal12_GLOBAL__N_112FunctionDataEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	80(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L217
	call	_ZdlPv@PLT
.L217:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L218
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L215
.L219:
	movq	%rbx, %r12
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L218:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L219
.L215:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27348:
	.size	_ZN2v88internal12_GLOBAL__N_115FunctionDataMapD2Ev, .-_ZN2v88internal12_GLOBAL__N_115FunctionDataMapD2Ev
	.set	_ZN2v88internal12_GLOBAL__N_115FunctionDataMapD1Ev,_ZN2v88internal12_GLOBAL__N_115FunctionDataMapD2Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_115FunctionDataMapD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_115FunctionDataMapD0Ev, @function
_ZN2v88internal12_GLOBAL__N_115FunctionDataMapD0Ev:
.LFB27350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_115FunctionDataMapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L235
	leaq	8(%rdi), %r14
.L239:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal12_GLOBAL__N_112FunctionDataEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	80(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L236
	call	_ZdlPv@PLT
.L236:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L237
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L235
.L238:
	movq	%rbx, %r12
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L237:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L238
.L235:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$56, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27350:
	.size	_ZN2v88internal12_GLOBAL__N_115FunctionDataMapD0Ev, .-_ZN2v88internal12_GLOBAL__N_115FunctionDataMapD0Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_115FunctionDataMap11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_115FunctionDataMap11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE, @function
_ZN2v88internal12_GLOBAL__N_115FunctionDataMap11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE:
.LFB20558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1504(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1560, %rsp
	movq	%rdi, -1592(%rbp)
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateEPNS0_14ThreadLocalTopE@PLT
	cmpq	$0, -88(%rbp)
	je	.L253
	movq	%r14, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L253
	leaq	-1536(%rbp), %rax
	movq	%rax, -1568(%rbp)
	leaq	16(%rbx), %rax
	movq	%rax, -1560(%rbp)
	.p2align 4,,10
	.p2align 3
.L273:
	movq	-1568(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	$0, -1520(%rbp)
	movaps	%xmm0, -1536(%rbp)
	call	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_6HandleINS0_18SharedFunctionInfoEEESaIS5_EE@PLT
	movq	-1536(%rbp), %rdi
	movq	-1528(%rbp), %rbx
	cmpq	%rdi, %rbx
	je	.L256
	movq	%rdi, %r15
	leaq	-1552(%rbp), %r12
	leaq	-1544(%rbp), %r13
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L259:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L291
.L270:
	movq	(%r15), %rax
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -1552(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-1552(%rbp), %rsi
	movq	31(%rsi), %rcx
	testb	$1, %cl
	je	.L259
	movq	-1(%rcx), %r8
	leaq	-1(%rcx), %rdi
	cmpw	$86, 11(%r8)
	je	.L292
.L258:
	movq	(%rdi), %rcx
	cmpw	$96, 11(%rcx)
	jne	.L259
	cmpl	$-1, %eax
	je	.L259
	movq	31(%rsi), %rax
	testb	$1, %al
	jne	.L293
.L261:
	movslq	67(%rax), %r9
	movq	-1552(%rbp), %rax
	movq	%r13, %rdi
	movq	%r9, -1584(%rbp)
	movl	%r9d, -1572(%rbp)
	movq	%rax, -1544(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-1544(%rbp), %rcx
	movl	$-1, %edx
	movl	47(%rcx), %ecx
	andl	$268435456, %ecx
	cmovne	%edx, %eax
	movq	-1592(%rbp), %rdx
	movq	24(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L259
	movq	-1560(%rbp), %rsi
	movl	-1572(%rbp), %r8d
	movq	-1584(%rbp), %r9
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L294:
	jne	.L267
	cmpl	%eax, 36(%rcx)
	jl	.L266
.L267:
	movq	%rcx, %rsi
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L265
.L264:
	cmpl	32(%rcx), %r8d
	jle	.L294
.L266:
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L264
.L265:
	cmpq	%rsi, -1560(%rbp)
	je	.L259
	cmpl	32(%rsi), %r9d
	jl	.L259
	jne	.L269
	cmpl	%eax, 36(%rsi)
	jg	.L259
.L269:
	addq	$8, %r15
	movl	$4, 104(%rsi)
	cmpq	%r15, %rbx
	jne	.L270
.L291:
	movq	-1536(%rbp), %rdi
.L256:
	testq	%rdi, %rdi
	je	.L271
	call	_ZdlPv@PLT
.L271:
	movq	%r14, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L273
.L253:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L295
	addq	$1560, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_restore_state
	movq	23(%rcx), %rdi
	testb	$1, %dil
	je	.L259
	subq	$1, %rdi
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L293:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L261
	movq	23(%rax), %rax
	jmp	.L261
.L295:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20558:
	.size	_ZN2v88internal12_GLOBAL__N_115FunctionDataMap11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE, .-_ZN2v88internal12_GLOBAL__N_115FunctionDataMap11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE
	.section	.text._ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_NS2_12_GLOBAL__N_111ChangeStateEESaIS9_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_NS2_12_GLOBAL__N_111ChangeStateEESaIS9_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_, @function
_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_NS2_12_GLOBAL__N_111ChangeStateEESaIS9_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_:
.LFB24730:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %r14
	movq	8(%rdi), %rdi
	movq	%r14, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r15
	testq	%rax, %rax
	je	.L297
	movq	(%rax), %rcx
	movq	%rdx, %rsi
	movq	8(%rcx), %r8
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L332:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L297
	movq	8(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %rsi
	jne	.L297
.L299:
	cmpq	%r8, %r14
	jne	.L332
	addq	$24, %rsp
	leaq	16(%rcx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	24(%r12), %rdx
	movq	8(%r12), %rsi
	leaq	32(%r12), %rdi
	movq	$0, (%rax)
	movq	%rax, %rbx
	movq	0(%r13), %rax
	movl	$1, %ecx
	movl	$0, 16(%rbx)
	movq	%rax, 8(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r13
	testb	%al, %al
	jne	.L300
	movq	(%r12), %r8
.L301:
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L310
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	(%rax), %rax
	movq	%rbx, (%rax)
.L311:
	addq	$1, 24(%r12)
	addq	$24, %rsp
	leaq	16(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L333
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L334
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r12), %r10
	movq	%rax, %r8
.L303:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L305
	xorl	%edi, %edi
	leaq	16(%r12), %r9
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L307:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L308:
	testq	%rsi, %rsi
	je	.L305
.L306:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r13
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L307
	movq	16(%r12), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L314
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L305:
	movq	(%r12), %rdi
	cmpq	%r10, %rdi
	je	.L309
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L309:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r13, 8(%r12)
	divq	%r13
	movq	%r8, (%r12)
	leaq	0(,%rdx,8), %r15
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L310:
	movq	16(%r12), %rdx
	movq	%rbx, 16(%r12)
	movq	%rdx, (%rbx)
	testq	%rdx, %rdx
	je	.L312
	movq	8(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	%rbx, (%r8,%rdx,8)
	movq	(%r12), %rax
	addq	%r15, %rax
.L312:
	leaq	16(%r12), %rdx
	movq	%rdx, (%rax)
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L314:
	movq	%rdx, %rdi
	jmp	.L308
.L333:
	movq	$0, 48(%r12)
	leaq	48(%r12), %r8
	movq	%r8, %r10
	jmp	.L303
.L334:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE24730:
	.size	_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_NS2_12_GLOBAL__N_111ChangeStateEESaIS9_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_, .-_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_NS2_12_GLOBAL__N_111ChangeStateEESaIS9_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	.section	.text._ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_SI_T0_.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_SI_T0_.constprop.0, @function
_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_SI_T0_.constprop.0:
.LFB28173:
	.cfi_startproc
	cmpq	%rsi, %rdi
	je	.L359
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	16(%rdi), %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	cmpq	%r12, %rsi
	jne	.L356
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L368:
	movdqu	(%r12), %xmm0
	cmpq	%r12, %r13
	je	.L345
	movq	%r12, %rdx
	leaq	16(%r13), %rdi
	movq	%r13, %rsi
	movaps	%xmm0, -64(%rbp)
	subq	%r13, %rdx
	call	memmove@PLT
	movdqa	-64(%rbp), %xmm0
.L345:
	addq	$16, %r12
	movups	%xmm0, 0(%r13)
	cmpq	%r12, %r14
	je	.L335
.L356:
	movl	0(%r13), %eax
	cmpl	%eax, (%r12)
	jne	.L362
	movl	4(%r12), %eax
	cmpl	4(%r13), %eax
	jne	.L362
	testl	%eax, %eax
	je	.L366
	cmpl	$1, %eax
	je	.L367
	movl	8(%r13), %eax
	cmpl	%eax, 8(%r12)
	.p2align 4,,10
	.p2align 3
.L362:
	setl	%al
.L338:
	testb	%al, %al
	jne	.L368
	movq	8(%r12), %r8
	movl	(%r12), %edx
	movq	%r12, %rbx
	movl	4(%r12), %ecx
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L347:
	cmpl	-12(%rbx), %ecx
	jne	.L364
	testl	%ecx, %ecx
	je	.L369
	cmpl	$1, %ecx
	je	.L370
	cmpl	%r8d, -8(%rbx)
	setg	%al
.L348:
	subq	$16, %rbx
	testb	%al, %al
	je	.L354
.L371:
	movdqu	(%rbx), %xmm1
	movups	%xmm1, 16(%rbx)
.L355:
	movq	%rbx, %r15
	cmpl	-16(%rbx), %edx
	je	.L347
.L364:
	setl	%al
.L373:
	subq	$16, %rbx
	testb	%al, %al
	jne	.L371
.L354:
	salq	$32, %rcx
	addq	$16, %r12
	movq	%r8, 8(%r15)
	orq	%rdx, %rcx
	movq	%rcx, (%r15)
	cmpq	%r12, %r14
	jne	.L356
.L335:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	.cfi_restore_state
	movq	%r8, %rdi
	movl	%edx, -80(%rbp)
	movl	%ecx, -76(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	-8(%rbx), %rdi
	movl	%eax, -64(%rbp)
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, -64(%rbp)
	movq	-72(%rbp), %r8
	movl	-76(%rbp), %ecx
	movl	-80(%rbp), %edx
	je	.L351
	movq	%r8, %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	-8(%rbx), %rdi
	movl	%eax, -64(%rbp)
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
.L365:
	cmpl	%eax, -64(%rbp)
	movq	-72(%rbp), %r8
	movl	-76(%rbp), %ecx
	movl	-80(%rbp), %edx
	setg	%al
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L370:
	movq	%r8, %rdi
	movl	%edx, -80(%rbp)
	movl	%ecx, -76(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	-8(%rbx), %rdi
	movl	%eax, -64(%rbp)
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, -64(%rbp)
	movq	-72(%rbp), %r8
	movl	-76(%rbp), %ecx
	movl	-80(%rbp), %edx
	jne	.L372
	movq	-8(%rbx), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%r8)
	setg	%al
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L351:
	movq	-8(%rbx), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%r8)
	setl	%al
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L366:
	movq	8(%r12), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	8(%r13), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, %ebx
	jne	.L374
	movq	8(%r12), %rdx
	movq	8(%r13), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rdx)
	setl	%al
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L372:
	movq	%r8, %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	-8(%rbx), %rdi
	movl	%eax, -64(%rbp)
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L374:
	movq	8(%r12), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	8(%r13), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, %ebx
	setg	%al
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L367:
	movq	8(%r12), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	8(%r13), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %ebx
	jne	.L375
	movq	8(%r12), %rdx
	movq	8(%r13), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rdx)
	setg	%al
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L375:
	movq	8(%r12), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	8(%r13), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %ebx
	setg	%al
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE28173:
	.size	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_SI_T0_.constprop.0, .-_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_SI_T0_.constprop.0
	.section	.text._ZN2v88internal12_GLOBAL__N_115UpdatePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEERKSt6vectorINS0_17SourceChangeRangeESaIS8_EE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_115UpdatePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEERKSt6vectorINS0_17SourceChangeRangeESaIS8_EE, @function
_ZN2v88internal12_GLOBAL__N_115UpdatePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEERKSt6vectorINS0_17SourceChangeRangeESaIS8_EE:
.LFB20568:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	8(%rbx), %r8
	movq	(%rbx), %rdi
	movl	%eax, %r14d
	movq	%r8, %rdx
	movq	%rdi, %rcx
	subq	%rdi, %rdx
	sarq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L378:
	testq	%rdx, %rdx
	jle	.L377
.L451:
	movq	%rdx, %rsi
	sarq	%rsi
	movq	%rsi, %rax
	salq	$4, %rax
	addq	%rcx, %rax
	cmpl	4(%rax), %r14d
	jle	.L431
	subq	%rsi, %rdx
	leaq	16(%rax), %rcx
	subq	$1, %rdx
	testq	%rdx, %rdx
	jg	.L451
.L377:
	cmpq	%rcx, %r8
	je	.L380
	cmpl	4(%rcx), %r14d
	je	.L452
.L380:
	cmpq	%rcx, %rdi
	je	.L381
	addl	-4(%rcx), %r14d
	subl	-12(%rcx), %r14d
.L381:
	movq	(%r15), %rax
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	movq	8(%rbx), %r9
	movq	(%rbx), %rsi
	movl	%eax, %edx
	movq	%r9, %rcx
	movq	%rsi, %rdi
	subq	%rsi, %rcx
	sarq	$4, %rcx
	movq	%rcx, %rax
	.p2align 4,,10
	.p2align 3
.L383:
	testq	%rax, %rax
	jle	.L382
.L453:
	movq	%rax, %r10
	sarq	%r10
	movq	%r10, %r8
	salq	$4, %r8
	addq	%rdi, %r8
	cmpl	4(%r8), %edx
	jle	.L432
	subq	%r10, %rax
	leaq	16(%r8), %rdi
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L453
.L382:
	cmpq	%rdi, %r9
	je	.L385
	cmpl	4(%rdi), %edx
	je	.L454
.L385:
	cmpq	%rdi, %rsi
	je	.L386
	addl	-4(%rdi), %edx
	subl	-12(%rdi), %edx
.L386:
	movq	(%r15), %r10
	movq	%r10, -128(%rbp)
	movzwl	45(%r10), %r11d
	cmpl	$65535, %r11d
	movl	%r11d, -200(%rbp)
	je	.L433
	movq	%r12, %rdi
	movl	%edx, -208(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	8(%rbx), %r9
	movl	-200(%rbp), %r11d
	movq	(%rbx), %rsi
	movq	(%r15), %r10
	movq	%r9, %rcx
	subl	%r11d, %eax
	movl	-208(%rbp), %edx
	subq	%rsi, %rcx
	movl	%eax, %r8d
	sarq	$4, %rcx
.L387:
	movq	%rsi, %rdi
	.p2align 4,,10
	.p2align 3
.L389:
	testq	%rcx, %rcx
	jle	.L388
.L455:
	movq	%rcx, %r11
	sarq	%r11
	movq	%r11, %rax
	salq	$4, %rax
	addq	%rdi, %rax
	cmpl	4(%rax), %r8d
	jle	.L434
	subq	%r11, %rcx
	leaq	16(%rax), %rdi
	subq	$1, %rcx
	testq	%rcx, %rcx
	jg	.L455
.L388:
	cmpq	%rdi, %r9
	je	.L391
	cmpl	4(%rdi), %r8d
	je	.L456
.L391:
	cmpq	%rdi, %rsi
	je	.L392
	addl	-4(%rdi), %r8d
	subl	-12(%rdi), %r8d
.L392:
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	%r8d, -200(%rbp)
	movq	%r10, -128(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo11SetPositionEii@PLT
	movq	(%r15), %rax
	movl	%r14d, %edx
	movq	%r12, %rdi
	movl	-200(%rbp), %r8d
	movq	%rax, -128(%rbp)
	movl	%r8d, %esi
	call	_ZN2v88internal18SharedFunctionInfo24SetFunctionTokenPositionEii@PLT
	movq	(%r15), %rax
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L457
.L393:
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L458
.L376:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L459
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	movq	%r11, %rcx
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L431:
	movq	%rsi, %rdx
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L432:
	movq	%r10, %rax
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L456:
	movl	12(%rdi), %r8d
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L454:
	movl	12(%rdi), %edx
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L452:
	movl	12(%rcx), %r14d
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L457:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L393
.L397:
	movq	(%r15), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L460
.L394:
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L461
.L400:
	movq	7(%rax), %rax
	movq	7(%rax), %rsi
.L399:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L401
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L402:
	leaq	-192(%rbp), %r15
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal26SourcePositionTableBuilderC1ENS1_13RecordingModeE@PLT
	movq	(%r14), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L404
.L408:
	andq	$-262144, %rax
	movq	24(%rax), %rax
	leaq	-37592(%rax), %rdx
	cmpq	-37280(%rax), %rsi
	je	.L462
	movq	7(%rsi), %rsi
.L407:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L409
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L410:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_9ByteArrayENS1_15IterationFilterE@PLT
	cmpl	$-1, -104(%rbp)
	je	.L420
	.p2align 4,,10
	.p2align 3
.L412:
	movq	-88(%rbp), %rdx
	movq	8(%rbx), %r11
	movq	(%rbx), %r9
	movq	%rdx, %rax
	movq	%r11, %rcx
	shrq	%rax
	subq	%r9, %rcx
	movq	%r9, %rsi
	andl	$1073741823, %eax
	sarq	$4, %rcx
	leal	-1(%rax), %edi
	.p2align 4,,10
	.p2align 3
.L416:
	testq	%rcx, %rcx
	jle	.L415
.L463:
	movq	%rcx, %r10
	sarq	%r10
	movq	%r10, %r8
	salq	$4, %r8
	addq	%rsi, %r8
	cmpl	4(%r8), %edi
	jle	.L435
	subq	%r10, %rcx
	leaq	16(%r8), %rsi
	subq	$1, %rcx
	testq	%rcx, %rcx
	jg	.L463
.L415:
	cmpq	%rsi, %r11
	je	.L418
	cmpl	4(%rsi), %edi
	je	.L464
.L418:
	cmpq	%rsi, %r9
	je	.L419
	addl	-4(%rsi), %edi
	subl	-12(%rsi), %edi
	leal	1(%rdi), %eax
.L419:
	movzbl	-80(%rbp), %r8d
	cltq
	movq	%rdx, %rcx
	movslq	-96(%rbp), %rsi
	andq	$-2147483647, %rcx
	leaq	(%rax,%rax), %rdx
	movq	%r15, %rdi
	orq	%rcx, %rdx
	movl	%r8d, %ecx
	call	_ZN2v88internal26SourcePositionTableBuilder11AddPositionEmNS0_14SourcePositionEb@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -104(%rbp)
	jne	.L412
.L420:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal26SourcePositionTableBuilder21ToSourcePositionTableEPNS0_7IsolateE@PLT
	movq	(%r14), %r15
	movq	(%rax), %r12
	movq	%rax, %rbx
	leaq	31(%r15), %rsi
	movq	%r12, 31(%r15)
	testb	$1, %r12b
	je	.L429
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -200(%rbp)
	testl	$262144, %eax
	je	.L421
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rsi, -208(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rsi
	movq	8(%rcx), %rax
.L421:
	testb	$24, %al
	je	.L429
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L429
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L429:
	movq	41016(%r13), %r12
	leaq	_ZN2v88internal6Logger27is_listening_to_code_eventsEv(%rip), %rdx
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	136(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L423
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L426
	cmpq	$0, 80(%r12)
	je	.L425
.L426:
	movq	(%r14), %rsi
	movq	(%rbx), %rdx
	movq	%r12, %rdi
	addq	$53, %rsi
	call	_ZN2v88internal6Logger26CodeLinePosInfoRecordEventEmNS0_9ByteArrayE@PLT
.L425:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L376
	call	_ZdlPv@PLT
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L458:
	movq	-1(%rax), %rax
	cmpw	$91, 11(%rax)
	jne	.L376
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L401:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L465
.L403:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L435:
	movq	%r10, %rcx
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L464:
	movl	12(%rsi), %eax
	addl	$1, %eax
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L409:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L466
.L411:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L404:
	movq	-1(%rsi), %rdx
	cmpw	$70, 11(%rdx)
	jne	.L408
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L460:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L394
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L394
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L394
	movq	31(%rdx), %rsi
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L462:
	movq	976(%rdx), %rsi
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L461:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L400
	movq	7(%rax), %rsi
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L423:
	call	*%rax
	testb	%al, %al
	je	.L425
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L466:
	movq	%r13, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L465:
	movq	%r13, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L433:
	movl	$-1, %r8d
	jmp	.L387
.L459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20568:
	.size	_ZN2v88internal12_GLOBAL__N_115UpdatePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEERKSt6vectorINS0_17SourceChangeRangeESaIS8_EE, .-_ZN2v88internal12_GLOBAL__N_115UpdatePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEERKSt6vectorINS0_17SourceChangeRangeESaIS8_EE
	.section	.text._ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_SI_T0_T1_.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_SI_T0_T1_.constprop.0, @function
_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_SI_T0_T1_.constprop.0:
.LFB28179:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rdx, -64(%rbp)
	movq	%rcx, -72(%rbp)
	cmpq	$256, %rbx
	jle	.L467
	movq	%rdi, %r14
	testq	%rdx, %rdx
	je	.L554
	leaq	16(%rdi), %rax
	movq	%rsi, %r12
	movq	%rax, -80(%rbp)
.L471:
	movq	%r12, %rax
	subq	$1, -64(%rbp)
	subq	%r14, %rax
	movq	%rax, %r13
	shrq	$63, %rax
	sarq	$4, %r13
	addq	%rax, %r13
	sarq	%r13
	salq	$4, %r13
	addq	%r14, %r13
	movl	0(%r13), %eax
	cmpl	%eax, 16(%r14)
	jne	.L544
	movl	20(%r14), %eax
	cmpl	4(%r13), %eax
	jne	.L544
	testl	%eax, %eax
	je	.L555
	cmpl	$1, %eax
	je	.L556
	movl	8(%r13), %eax
	cmpl	%eax, 24(%r14)
	.p2align 4,,10
	.p2align 3
.L544:
	setl	%dl
.L477:
	movl	-16(%r12), %eax
	testb	%dl, %dl
	je	.L483
	cmpl	0(%r13), %eax
	jne	.L545
	movl	4(%r13), %eax
	cmpl	-12(%r12), %eax
	je	.L486
.L546:
	setl	%al
.L485:
	testb	%al, %al
	je	.L491
.L565:
	movdqu	(%r14), %xmm0
	movdqu	0(%r13), %xmm4
	movups	%xmm4, (%r14)
	movups	%xmm0, 0(%r13)
.L492:
	movq	-80(%rbp), %r15
	movl	(%r14), %ecx
	movq	%r12, -56(%rbp)
	movl	(%r15), %eax
	movq	%r15, -96(%rbp)
	cmpl	%eax, %ecx
	je	.L518
	.p2align 4,,10
	.p2align 3
.L561:
	setg	%dl
	movl	%ecx, %eax
.L519:
	testb	%dl, %dl
	jne	.L525
	movq	-56(%rbp), %rbx
	subq	$16, %rbx
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L559:
	movl	(%r14), %eax
.L534:
	movq	%rbx, -56(%rbp)
	cmpl	(%rbx), %eax
	jne	.L553
	movl	4(%r14), %eax
	cmpl	4(%rbx), %eax
	jne	.L553
	testl	%eax, %eax
	je	.L557
	cmpl	$1, %eax
	je	.L558
	movl	8(%rbx), %eax
	cmpl	%eax, 8(%r14)
	.p2align 4,,10
	.p2align 3
.L553:
	setl	%al
.L527:
	subq	$16, %rbx
	testb	%al, %al
	jne	.L559
	cmpq	-56(%rbp), %r15
	jnb	.L560
	movq	-56(%rbp), %rax
	movdqu	(%r15), %xmm0
	movdqu	(%rax), %xmm1
	movups	%xmm1, (%r15)
	movups	%xmm0, (%rax)
	movl	(%r14), %eax
	movaps	%xmm1, -96(%rbp)
.L525:
	addq	$16, %r15
	movl	%eax, %ecx
	movl	(%r15), %eax
	movq	%r15, -96(%rbp)
	cmpl	%eax, %ecx
	jne	.L561
.L518:
	movl	4(%r15), %edx
	cmpl	4(%r14), %edx
	jne	.L552
	testl	%edx, %edx
	je	.L562
	cmpl	$1, %edx
	je	.L563
	movl	8(%r14), %esi
	cmpl	%esi, 8(%r15)
.L552:
	setl	%dl
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L558:
	movq	8(%r14), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	8(%rbx), %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %r13d
	je	.L532
	movq	8(%r14), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	8(%rbx), %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %r13d
	setg	%al
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L557:
	movq	8(%r14), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	8(%rbx), %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, %r13d
	je	.L530
	movq	8(%r14), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	8(%rbx), %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, %r13d
	setg	%al
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L532:
	movq	8(%r14), %rcx
	movq	8(%rbx), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rcx)
	setg	%al
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L530:
	movq	8(%r14), %rcx
	movq	8(%rbx), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rcx)
	setl	%al
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L562:
	movq	8(%r15), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	8(%r14), %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, %r13d
	je	.L522
	movq	8(%r15), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	8(%r14), %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, %r13d
	movl	(%r14), %eax
	setg	%dl
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L563:
	movq	8(%r15), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	8(%r14), %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %r13d
	je	.L524
	movq	8(%r15), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	8(%r14), %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %r13d
	movl	(%r14), %eax
	setg	%dl
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L560:
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%r15, %rbx
	movq	%r12, %rsi
	movq	%r15, %rdi
	subq	%r14, %rbx
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_SI_T0_T1_.constprop.0
	cmpq	$256, %rbx
	jle	.L467
	cmpq	$0, -64(%rbp)
	je	.L469
	movq	%r15, %r12
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L522:
	movq	8(%r15), %rdx
	movq	8(%r14), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rdx)
	setl	%dl
	movl	(%r14), %eax
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L524:
	movq	8(%r15), %rdx
	movq	8(%r14), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rdx)
	setg	%dl
	movl	(%r14), %eax
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L483:
	cmpl	16(%r14), %eax
	jne	.L548
	movl	20(%r14), %eax
	cmpl	-12(%r12), %eax
	je	.L503
.L549:
	setl	%al
.L502:
	testb	%al, %al
	je	.L508
.L564:
	movdqu	(%r14), %xmm0
	movdqu	16(%r14), %xmm5
	movups	%xmm0, 16(%r14)
	movups	%xmm5, (%r14)
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L570:
	movq	24(%r14), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	-8(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, %ebx
	je	.L505
	movq	24(%r14), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	-8(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, %ebx
	.p2align 4,,10
	.p2align 3
.L548:
	setg	%al
	testb	%al, %al
	jne	.L564
.L508:
	movl	-16(%r12), %eax
	cmpl	%eax, 0(%r13)
	je	.L509
.L550:
	setl	%al
.L510:
	movdqu	(%r14), %xmm0
	testb	%al, %al
	je	.L516
.L551:
	movdqu	-16(%r12), %xmm6
	movups	%xmm6, (%r14)
	movups	%xmm0, -16(%r12)
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L572:
	movq	8(%r13), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	-8(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, %ebx
	je	.L488
	movq	8(%r13), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	-8(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, %ebx
	.p2align 4,,10
	.p2align 3
.L545:
	setg	%al
	testb	%al, %al
	jne	.L565
.L491:
	movl	-16(%r12), %eax
	cmpl	%eax, 16(%r14)
	je	.L493
.L547:
	setl	%al
.L494:
	movdqu	(%r14), %xmm0
	testb	%al, %al
	jne	.L551
	movdqu	16(%r14), %xmm7
	movups	%xmm0, 16(%r14)
	movups	%xmm7, (%r14)
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L554:
	movq	%rsi, -96(%rbp)
.L469:
	sarq	$4, %rbx
	leaq	-2(%rbx), %r12
	sarq	%r12
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L472:
	subq	$1, %r12
.L474:
	movq	%r12, %rax
	movq	-72(%rbp), %r9
	movq	%rbx, %rdx
	movq	%r12, %rsi
	salq	$4, %rax
	movq	%r14, %rdi
	movq	(%r14,%rax), %rcx
	movq	8(%r14,%rax), %r8
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_T0_SJ_T1_T2_
	testq	%r12, %r12
	jne	.L472
	movq	-96(%rbp), %rbx
	subq	$16, %rbx
	.p2align 4,,10
	.p2align 3
.L473:
	movq	%rbx, %r12
	movdqu	(%r14), %xmm3
	movq	(%rbx), %rcx
	xorl	%esi, %esi
	subq	%r14, %r12
	movq	8(%rbx), %r8
	movq	-72(%rbp), %r9
	movq	%r14, %rdi
	movq	%r12, %rdx
	movups	%xmm3, (%rbx)
	subq	$16, %rbx
	sarq	$4, %rdx
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEElS5_NS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_T0_SJ_T1_T2_
	cmpq	$16, %r12
	jg	.L473
.L467:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L516:
	.cfi_restore_state
	movdqu	0(%r13), %xmm7
	movups	%xmm7, (%r14)
	movups	%xmm0, 0(%r13)
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L509:
	movl	4(%r13), %eax
	cmpl	-12(%r12), %eax
	jne	.L550
	testl	%eax, %eax
	je	.L566
	cmpl	$1, %eax
	je	.L567
	movl	-8(%r12), %eax
	cmpl	%eax, 8(%r13)
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L493:
	movl	20(%r14), %eax
	cmpl	-12(%r12), %eax
	jne	.L547
	testl	%eax, %eax
	je	.L568
	cmpl	$1, %eax
	je	.L569
	movl	-8(%r12), %eax
	cmpl	%eax, 24(%r14)
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L503:
	testl	%eax, %eax
	je	.L570
	cmpl	$1, %eax
	je	.L571
	movl	-8(%r12), %eax
	cmpl	%eax, 24(%r14)
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L486:
	testl	%eax, %eax
	je	.L572
	cmpl	$1, %eax
	je	.L573
	movl	-8(%r12), %eax
	cmpl	%eax, 8(%r13)
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L556:
	movq	24(%r14), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	8(%r13), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %ebx
	jne	.L574
	movq	24(%r14), %rdx
	movq	8(%r13), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rdx)
	setg	%dl
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L555:
	movq	24(%r14), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	8(%r13), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, %ebx
	jne	.L575
	movq	24(%r14), %rdx
	movq	8(%r13), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rdx)
	setl	%dl
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L575:
	movq	24(%r14), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	8(%r13), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, %ebx
	setg	%dl
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L574:
	movq	24(%r14), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	8(%r13), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %ebx
	setg	%dl
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L566:
	movq	8(%r13), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	-8(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, %ebx
	jne	.L576
	movq	8(%r13), %rdx
	movq	-8(%r12), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rdx)
	setl	%al
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L568:
	movq	24(%r14), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	-8(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, %ebx
	jne	.L577
	movq	24(%r14), %rdx
	movq	-8(%r12), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rdx)
	setl	%al
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L573:
	movq	8(%r13), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	-8(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %ebx
	je	.L490
	movq	8(%r13), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	-8(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %ebx
	setg	%al
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L571:
	movq	24(%r14), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	-8(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %ebx
	je	.L507
	movq	24(%r14), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	-8(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %ebx
	setg	%al
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L505:
	movq	24(%r14), %rdx
	movq	-8(%r12), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rdx)
	setl	%al
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L488:
	movq	8(%r13), %rdx
	movq	-8(%r12), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rdx)
	setl	%al
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L507:
	movq	24(%r14), %rdx
	movq	-8(%r12), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rdx)
	setg	%al
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L490:
	movq	8(%r13), %rdx
	movq	-8(%r12), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rdx)
	setg	%al
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L567:
	movq	8(%r13), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	-8(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %ebx
	jne	.L578
	movq	8(%r13), %rdx
	movq	-8(%r12), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rdx)
	setg	%al
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L569:
	movq	24(%r14), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	-8(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %ebx
	jne	.L579
	movq	24(%r14), %rdx
	movq	-8(%r12), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rdx)
	setg	%al
	jmp	.L494
.L577:
	movq	24(%r14), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	-8(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, %ebx
	setg	%al
	jmp	.L494
.L576:
	movq	8(%r13), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	-8(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, %ebx
	setg	%al
	jmp	.L510
.L579:
	movq	24(%r14), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	-8(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %ebx
	setg	%al
	jmp	.L494
.L578:
	movq	8(%r13), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	-8(%r12), %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, %ebx
	setg	%al
	jmp	.L510
	.cfi_endproc
.LFE28179:
	.size	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_SI_T0_T1_.constprop.0, .-_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_SI_T0_T1_.constprop.0
	.section	.rodata._ZN2v88internal12_GLOBAL__N_131CalculateFunctionLiteralChangesERKSt6vectorIPNS0_15FunctionLiteralESaIS4_EERKS2_INS0_17SourceChangeRangeESaIS9_EEPSt13unordered_mapIS4_NS1_21FunctionLiteralChangeESt4hashIS4_ESt8equal_toIS4_ESaISt4pairIKS4_SF_EEE.constprop.0.str1.1,"aMS",@progbits,1
.LC2:
	.string	"vector::reserve"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_131CalculateFunctionLiteralChangesERKSt6vectorIPNS0_15FunctionLiteralESaIS4_EERKS2_INS0_17SourceChangeRangeESaIS9_EEPSt13unordered_mapIS4_NS1_21FunctionLiteralChangeESt4hashIS4_ESt8equal_toIS4_ESaISt4pairIKS4_SF_EEE.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZN2v88internal12_GLOBAL__N_131CalculateFunctionLiteralChangesERKSt6vectorIPNS0_15FunctionLiteralESaIS4_EERKS2_INS0_17SourceChangeRangeESaIS9_EEPSt13unordered_mapIS4_NS1_21FunctionLiteralChangeESt4hashIS4_ESt8equal_toIS4_ESaISt4pairIKS4_SF_EEE.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_131CalculateFunctionLiteralChangesERKSt6vectorIPNS0_15FunctionLiteralESaIS4_EERKS2_INS0_17SourceChangeRangeESaIS9_EEPSt13unordered_mapIS4_NS1_21FunctionLiteralChangeESt4hashIS4_ESt8equal_toIS4_ESaISt4pairIKS4_SF_EEE.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_131CalculateFunctionLiteralChangesERKSt6vectorIPNS0_15FunctionLiteralESaIS4_EERKS2_INS0_17SourceChangeRangeESaIS9_EEPSt13unordered_mapIS4_NS1_21FunctionLiteralChangeESt4hashIS4_ESt8equal_toIS4_ESaISt4pairIKS4_SF_EEE.constprop.0:
.LFB28185:
	.cfi_startproc
	movabsq	$576460752303423487, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -104(%rbp)
	movq	8(%rdi), %r12
	movq	%rdx, -144(%rbp)
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	subq	(%rsi), %rax
	movaps	%xmm0, -80(%rbp)
	sarq	$4, %rax
	movq	$0, -64(%rbp)
	movq	%rax, %rdx
	movq	%r12, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	leaq	(%rax,%rax), %rdx
	cmpq	%rcx, %rdx
	ja	.L707
	xorl	%esi, %esi
	xorl	%r15d, %r15d
	testq	%rdx, %rdx
	jne	.L708
.L582:
	cmpq	%rbx, %r12
	je	.L587
	leaq	-89(%rbp), %r14
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L709:
	movq	%r13, %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	%r13, 8(%r15)
	movl	%eax, (%r15)
	movl	$0, 4(%r15)
	movq	-72(%rbp), %rax
	movb	$0, -89(%rbp)
	leaq	16(%rax), %r13
	movq	%r13, -72(%rbp)
	cmpq	-64(%rbp), %r13
	je	.L590
.L710:
	movq	-88(%rbp), %r15
	addq	$8, %rbx
	movq	%r15, %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	%r15, 8(%r13)
	movl	%eax, 0(%r13)
	movl	$1, 4(%r13)
	movq	-72(%rbp), %rax
	leaq	16(%rax), %r15
	movq	%r15, -72(%rbp)
	cmpq	%rbx, %r12
	je	.L587
.L591:
	movq	-64(%rbp), %rsi
.L593:
	movq	(%rbx), %r13
	movb	$1, -89(%rbp)
	movq	%r13, -88(%rbp)
	cmpq	%r15, %rsi
	jne	.L709
	leaq	-88(%rbp), %rdx
	leaq	-80(%rbp), %rdi
	movq	%r14, %rcx
	call	_ZNSt6vectorIN2v88internal12_GLOBAL__N_119SourcePositionEventESaIS3_EE17_M_realloc_insertIJRPNS1_15FunctionLiteralEbEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movb	$0, -89(%rbp)
	movq	-72(%rbp), %r13
	cmpq	-64(%rbp), %r13
	jne	.L710
.L590:
	leaq	-88(%rbp), %rdx
	leaq	-80(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r13, %rsi
	call	_ZNSt6vectorIN2v88internal12_GLOBAL__N_119SourcePositionEventESaIS3_EE17_M_realloc_insertIJRPNS1_15FunctionLiteralEbEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	addq	$8, %rbx
	movq	-72(%rbp), %r15
	cmpq	%rbx, %r12
	jne	.L591
.L587:
	movq	-104(%rbp), %rax
	movq	(%rax), %rbx
	movq	8(%rax), %r13
	cmpq	%r13, %rbx
	je	.L594
	leaq	-88(%rbp), %r12
	movq	%r15, %rsi
	.p2align 4,,10
	.p2align 3
.L600:
	movb	$1, -88(%rbp)
	cmpq	%rsi, -64(%rbp)
	je	.L595
	movl	(%rbx), %eax
	movl	$2, 4(%rsi)
	movl	%eax, (%rsi)
	addl	12(%rbx), %eax
	subl	8(%rbx), %eax
	subl	4(%rbx), %eax
	movl	%eax, 8(%rsi)
	movq	-72(%rbp), %rax
	movb	$0, -88(%rbp)
	leaq	16(%rax), %rsi
	movq	%rsi, -72(%rbp)
	cmpq	%rsi, -64(%rbp)
	je	.L597
.L719:
	movl	4(%rbx), %edx
	movl	$3, 4(%rsi)
	addq	$16, %rbx
	movl	%edx, (%rsi)
	movl	-4(%rbx), %eax
	subl	-16(%rbx), %edx
	subl	-8(%rbx), %eax
	subl	%edx, %eax
	movl	%eax, 8(%rsi)
	movq	-72(%rbp), %rax
	leaq	16(%rax), %rsi
	movq	%rsi, -72(%rbp)
	cmpq	%rbx, %r13
	jne	.L600
.L704:
	movq	%rsi, %r15
.L594:
	movq	-80(%rbp), %r12
	cmpq	%r15, %r12
	je	.L601
	movq	%r15, %rbx
	movl	$63, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	subq	%r12, %rbx
	leaq	_ZN2v88internal12_GLOBAL__N_119SourcePositionEvent8LessThanERKS2_S4_(%rip), %rcx
	movq	%rbx, %rax
	sarq	$4, %rax
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_SI_T0_T1_.constprop.0
	cmpq	$256, %rbx
	jle	.L602
	leaq	256(%r12), %r13
	leaq	_ZN2v88internal12_GLOBAL__N_119SourcePositionEvent8LessThanERKS2_S4_(%rip), %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_SI_T0_.constprop.0
	cmpq	%r15, %r13
	je	.L601
	movq	%r15, -120(%rbp)
	movq	%r13, %r15
	.p2align 4,,10
	.p2align 3
.L614:
	movq	8(%r15), %rsi
	movl	(%r15), %r14d
	movq	%r15, %rbx
	movl	4(%r15), %r12d
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L607:
	testl	%r12d, %r12d
	je	.L711
	cmpl	$1, %r12d
	je	.L712
	cmpl	%esi, -8(%rbx)
	.p2align 4,,10
	.p2align 3
.L705:
	setg	%al
.L606:
	subq	$16, %rbx
	testb	%al, %al
	je	.L612
.L713:
	movdqu	(%rbx), %xmm1
	movups	%xmm1, 16(%rbx)
.L613:
	movq	%rbx, %r13
	cmpl	%r14d, -16(%rbx)
	jne	.L705
	cmpl	-12(%rbx), %r12d
	je	.L607
	setl	%al
.L726:
	subq	$16, %rbx
	testb	%al, %al
	jne	.L713
.L612:
	salq	$32, %r12
	movq	%rsi, 8(%r13)
	addq	$16, %r15
	orq	%r14, %r12
	movq	%r12, 0(%r13)
	cmpq	%r15, -120(%rbp)
	jne	.L614
.L601:
	movl	$64, %edi
	call	_Znwm@PLT
	movl	$512, %edi
	movq	%rax, %rbx
	leaq	24(%rax), %r13
	movq	%rax, -160(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	%r13, -120(%rbp)
	movq	%rax, 24(%rbx)
	movq	-80(%rbp), %rbx
	leaq	512(%rax), %rdi
	movq	%rax, -128(%rbp)
	movq	%rdi, -152(%rbp)
	movq	%rcx, -104(%rbp)
	cmpq	%rcx, %rbx
	je	.L616
	movq	-144(%rbp), %r14
	movq	%rax, -112(%rbp)
	movq	%rax, %r12
	xorl	%r15d, %r15d
	movq	%rdi, -192(%rbp)
	leaq	16(%r14), %rax
	movq	%r13, -168(%rbp)
	movq	$8, -184(%rbp)
	movl	$0, -132(%rbp)
	movq	%rax, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L654:
	movl	4(%rbx), %eax
	cmpl	$2, %eax
	je	.L617
	ja	.L618
	testl	%eax, %eax
	je	.L714
	cmpq	-112(%rbp), %r12
	je	.L715
	testb	%r15b, %r15b
	je	.L716
	movl	$-1, -20(%r12)
	movq	%r12, %rdx
.L661:
	movl	$40, %edi
	movq	%rdx, -144(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rdx
	movq	8(%r14), %r9
	movq	$0, (%rax)
	movq	%rax, %r8
	movq	-32(%rdx), %r13
	movdqu	-24(%rdx), %xmm3
	movq	%r13, 8(%rax)
	movups	%xmm3, 16(%rax)
	movq	-8(%rdx), %rax
	xorl	%edx, %edx
	movq	%rax, 32(%r8)
	movq	%r13, %rax
	divq	%r9
	movq	(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rdi
	leaq	0(,%rdx,8), %r11
	testq	%rax, %rax
	je	.L624
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L717:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L624
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r9
	cmpq	%rdx, %rdi
	jne	.L624
.L626:
	cmpq	%rsi, %r13
	jne	.L717
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	cmpq	-112(%rbp), %r12
	je	.L640
.L727:
	subq	$32, %r12
	.p2align 4,,10
	.p2align 3
.L621:
	addq	$16, %rbx
	cmpq	%rbx, -104(%rbp)
	jne	.L654
	movq	-168(%rbp), %r13
.L616:
	movq	-120(%rbp), %rbx
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jnb	.L658
	.p2align 4,,10
	.p2align 3
.L655:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L655
.L658:
	movq	-160(%rbp), %rdi
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L580
	call	_ZdlPv@PLT
.L580:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L718
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L595:
	.cfi_restore_state
	leaq	-80(%rbp), %rdi
	movq	%r12, %rcx
	movq	%rbx, %rdx
	call	_ZNSt6vectorIN2v88internal12_GLOBAL__N_119SourcePositionEventESaIS3_EE17_M_realloc_insertIJRKNS1_17SourceChangeRangeEbEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movb	$0, -88(%rbp)
	movq	-72(%rbp), %rsi
	cmpq	%rsi, -64(%rbp)
	jne	.L719
	.p2align 4,,10
	.p2align 3
.L597:
	movq	%rbx, %rdx
	leaq	-80(%rbp), %rdi
	movq	%r12, %rcx
	addq	$16, %rbx
	call	_ZNSt6vectorIN2v88internal12_GLOBAL__N_119SourcePositionEventESaIS3_EE17_M_realloc_insertIJRKNS1_17SourceChangeRangeEbEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	-72(%rbp), %rsi
	cmpq	%rbx, %r13
	jne	.L600
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L618:
	cmpl	$3, %eax
	jne	.L621
	movl	8(%rbx), %ecx
	xorl	%r15d, %r15d
	addl	%ecx, -132(%rbp)
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L617:
	movl	$1, %r15d
	cmpq	-128(%rbp), %r12
	je	.L621
	movq	%r12, %rax
	cmpq	-112(%rbp), %r12
	je	.L720
.L653:
	movb	$1, -16(%rax)
	movl	$1, %r15d
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L714:
	xorl	%ecx, %ecx
	cmpq	-128(%rbp), %r12
	je	.L641
	movq	%r12, %rax
	cmpq	-112(%rbp), %r12
	je	.L721
.L642:
	movq	-32(%rax), %rcx
.L641:
	movq	8(%rbx), %r8
	movl	$-1, %r13d
	testb	%r15b, %r15b
	je	.L722
	movq	-152(%rbp), %rax
	subq	$32, %rax
	cmpq	%rax, %r12
	je	.L644
.L723:
	movq	%r8, (%r12)
	addq	$32, %r12
	movl	%r13d, -24(%r12)
	movl	$-1, -20(%r12)
	movb	$0, -16(%r12)
	movq	%rcx, -8(%r12)
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L722:
	movq	%r8, %rdi
	movq	%rcx, -144(%rbp)
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movl	-132(%rbp), %ecx
	movq	8(%rbx), %r8
	leal	(%rax,%rcx), %r13d
	movq	-152(%rbp), %rax
	movq	-144(%rbp), %rcx
	subq	$32, %rax
	cmpq	%rax, %r12
	jne	.L723
.L644:
	movq	-120(%rbp), %r9
	subq	-168(%rbp), %r9
	movq	%r12, %rax
	movq	%r9, %rdi
	subq	-112(%rbp), %rax
	sarq	$3, %rdi
	sarq	$5, %rax
	leaq	-1(%rdi), %rdx
	salq	$4, %rdx
	leaq	(%rdx,%rax), %rdx
	movq	-192(%rbp), %rax
	subq	-128(%rbp), %rax
	sarq	$5, %rax
	addq	%rdx, %rax
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	je	.L724
	movq	-120(%rbp), %rax
	movq	-184(%rbp), %rdx
	subq	-160(%rbp), %rax
	sarq	$3, %rax
	subq	%rax, %rdx
	cmpq	$1, %rdx
	jbe	.L725
.L647:
	movl	$512, %edi
	movq	%rcx, -144(%rbp)
	movq	%r8, -112(%rbp)
	call	_Znwm@PLT
	movq	-120(%rbp), %rdi
	movq	-112(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	%rax, 8(%rdi)
	movq	%rdi, %rax
	movq	%rcx, 24(%r12)
	addq	$8, %rax
	movq	%r8, (%r12)
	movl	%r13d, 8(%r12)
	movl	$-1, 12(%r12)
	movb	$0, 16(%r12)
	movq	8(%rdi), %r12
	movq	%rax, -120(%rbp)
	leaq	512(%r12), %rcx
	movq	%r12, -112(%rbp)
	movq	%rcx, -152(%rbp)
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L602:
	leaq	_ZN2v88internal12_GLOBAL__N_119SourcePositionEvent8LessThanERKS2_S4_(%rip), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal12_GLOBAL__N_119SourcePositionEventESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIPFbRKS5_SE_EEEEvT_SI_T0_.constprop.0
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L711:
	movq	%rsi, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	-8(%rbx), %rdi
	movl	%eax, -104(%rbp)
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, -104(%rbp)
	movq	-112(%rbp), %rsi
	je	.L609
	movq	%rsi, %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	-8(%rbx), %rdi
	movl	%eax, -104(%rbp)
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	cmpl	%eax, -104(%rbp)
	movq	-112(%rbp), %rsi
	setg	%al
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L712:
	movq	%rsi, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	-8(%rbx), %rdi
	movl	%eax, -104(%rbp)
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, -104(%rbp)
	movq	-112(%rbp), %rsi
	je	.L611
	movq	%rsi, %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	-8(%rbx), %rdi
	movl	%eax, -104(%rbp)
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	cmpl	%eax, -104(%rbp)
	movq	-112(%rbp), %rsi
	setg	%al
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L609:
	movq	-8(%rbx), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rsi)
	setl	%al
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L611:
	movq	-8(%rbx), %rax
	movl	28(%rax), %eax
	cmpl	%eax, 28(%rsi)
	setg	%al
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L624:
	movq	24(%r14), %rdx
	movq	%r9, %rsi
	leaq	32(%r14), %rdi
	movl	$1, %ecx
	movq	%r8, -144(%rbp)
	movq	%r11, -200(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	-144(%rbp), %r8
	testb	%al, %al
	movq	%rdx, %r9
	jne	.L627
	movq	(%r14), %r10
	movq	-200(%rbp), %r11
	leaq	(%r10,%r11), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L637
.L731:
	movq	(%rdx), %rdx
	movq	%rdx, (%r8)
	movq	(%rax), %rax
	movq	%r8, (%rax)
.L638:
	addq	$1, 24(%r14)
	cmpq	-112(%rbp), %r12
	jne	.L727
.L640:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rcx
	movq	-8(%rcx), %rax
	leaq	512(%rax), %rdi
	movq	%rax, -112(%rbp)
	leaq	480(%rax), %r12
	leaq	-8(%rcx), %rax
	movq	%rdi, -152(%rbp)
	movq	%rax, -120(%rbp)
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L715:
	movq	-120(%rbp), %rax
	movq	-8(%rax), %r13
	testb	%r15b, %r15b
	je	.L728
	movl	$-1, 492(%r13)
.L660:
	movq	-120(%rbp), %rax
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L716:
	movq	8(%rbx), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	addl	-132(%rbp), %eax
	movq	%r12, %rdx
	movl	%eax, -20(%r12)
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L721:
	movq	-120(%rbp), %rax
	movq	-8(%rax), %rax
	movq	%rax, -144(%rbp)
	addq	$512, %rax
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L720:
	movq	-120(%rbp), %rax
	movq	-8(%rax), %rax
	movq	%rax, -144(%rbp)
	addq	$512, %rax
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L708:
	salq	$5, %rax
	movq	%rdi, %r14
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_Znwm@PLT
	movq	-80(%rbp), %rdi
	movq	-72(%rbp), %rcx
	movq	%rax, %r15
	movq	%rax, %rdx
	movq	%rdi, %rax
	cmpq	%rdi, %rcx
	je	.L586
	.p2align 4,,10
	.p2align 3
.L583:
	movdqu	(%rax), %xmm2
	addq	$16, %rax
	addq	$16, %rdx
	movups	%xmm2, -16(%rdx)
	cmpq	%rax, %rcx
	jne	.L583
.L586:
	testq	%rdi, %rdi
	je	.L585
	call	_ZdlPv@PLT
.L585:
	movq	%r15, %xmm0
	leaq	(%r15,%rbx), %rsi
	movq	8(%r14), %r12
	movq	(%r14), %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rsi, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L725:
	leaq	2(%rdi), %rdx
	movq	-184(%rbp), %rdi
	leaq	(%rdx,%rdx), %rax
	cmpq	%rax, %rdi
	ja	.L729
	movq	%rcx, -152(%rbp)
	movq	-184(%rbp), %rcx
	movl	$1, %eax
	movq	%rdx, -112(%rbp)
	testq	%rcx, %rcx
	movq	%r9, -192(%rbp)
	cmovne	%rcx, %rax
	movq	%r8, -144(%rbp)
	leaq	2(%rcx,%rax), %rax
	movabsq	$1152921504606846975, %rcx
	cmpq	%rcx, %rax
	ja	.L651
	leaq	0(,%rax,8), %rdi
	movq	%rax, -184(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rdx
	movq	-144(%rbp), %r8
	movq	%rax, %r10
	movq	-184(%rbp), %rax
	movq	-152(%rbp), %rcx
	movq	-192(%rbp), %r9
	subq	%rdx, %rax
	movq	-120(%rbp), %rdx
	shrq	%rax
	leaq	(%r10,%rax,8), %rdi
	movq	-168(%rbp), %rax
	addq	$8, %rdx
	movq	%rdi, -112(%rbp)
	cmpq	%rax, %rdx
	je	.L652
	subq	%rax, %rdx
	movq	%rax, %rsi
	movq	%r10, -168(%rbp)
	movq	%r9, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%r8, -120(%rbp)
	call	memmove@PLT
	movq	-168(%rbp), %r10
	movq	-152(%rbp), %r9
	movq	-144(%rbp), %rcx
	movq	-120(%rbp), %r8
.L652:
	movq	-160(%rbp), %rdi
	movq	%r10, -168(%rbp)
	movq	%r9, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %r10
	movq	-112(%rbp), %rax
	movq	-120(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	%r10, -160(%rbp)
	movq	-152(%rbp), %r9
	movq	%rax, -168(%rbp)
.L650:
	movq	(%rax), %rdi
	addq	%rax, %r9
	movq	%r9, -120(%rbp)
	addq	$512, %rdi
	movq	%rdi, -192(%rbp)
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L627:
	cmpq	$1, %rdx
	je	.L730
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L651
	leaq	0(,%rdx,8), %rdx
	movq	%r8, -208(%rbp)
	movq	%rdx, %rdi
	movq	%r9, -200(%rbp)
	movq	%rdx, -144(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-208(%rbp), %r8
	movq	-200(%rbp), %r9
	movq	%rax, %r10
	leaq	48(%r14), %rax
	movq	%rax, -144(%rbp)
.L630:
	movq	16(%r14), %rsi
	movq	$0, 16(%r14)
	testq	%rsi, %rsi
	je	.L632
	xorl	%r11d, %r11d
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L634:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%rdi), %rax
	movq	%rcx, (%rax)
.L635:
	testq	%rsi, %rsi
	je	.L632
.L633:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r9
	leaq	(%r10,%rdx,8), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L634
	movq	16(%r14), %rax
	movq	%rax, (%rcx)
	movq	-176(%rbp), %rax
	movq	%rcx, 16(%r14)
	movq	%rax, (%rdi)
	cmpq	$0, (%rcx)
	je	.L667
	movq	%rcx, (%r10,%r11,8)
	movq	%rdx, %r11
	testq	%rsi, %rsi
	jne	.L633
	.p2align 4,,10
	.p2align 3
.L632:
	movq	(%r14), %rdi
	cmpq	%rdi, -144(%rbp)
	je	.L636
	movq	%r9, -208(%rbp)
	movq	%r10, -200(%rbp)
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-208(%rbp), %r9
	movq	-200(%rbp), %r10
	movq	-144(%rbp), %r8
.L636:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r9, 8(%r14)
	divq	%r9
	movq	%r10, (%r14)
	leaq	0(,%rdx,8), %r11
	leaq	(%r10,%r11), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L731
.L637:
	movq	16(%r14), %rdx
	movq	%r8, 16(%r14)
	movq	%rdx, (%r8)
	testq	%rdx, %rdx
	je	.L639
	movq	8(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r14)
	movq	%r8, (%r10,%rdx,8)
	movq	(%r14), %rax
	addq	%r11, %rax
.L639:
	movq	-176(%rbp), %rcx
	movq	%rcx, (%rax)
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L728:
	movq	8(%rbx), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	addl	-132(%rbp), %eax
	movl	%eax, 492(%r13)
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L667:
	movq	%rdx, %r11
	jmp	.L635
.L729:
	movq	%rdi, %rax
	movq	-160(%rbp), %rdi
	movq	-168(%rbp), %rsi
	subq	%rdx, %rax
	shrq	%rax
	leaq	(%rdi,%rax,8), %r10
	movq	-120(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	%rsi, %r10
	jnb	.L649
	cmpq	%rax, %rsi
	je	.L672
	movq	%r10, %rdi
	movq	%r9, -144(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%r8, -112(%rbp)
	call	memmove@PLT
	movq	-112(%rbp), %r8
	movq	-120(%rbp), %rcx
	movq	%rax, -168(%rbp)
	movq	-144(%rbp), %r9
	jmp	.L650
.L649:
	cmpq	%rax, %rsi
	je	.L672
	leaq	8(%r9), %rdi
	movq	%rcx, -152(%rbp)
	subq	%rdx, %rdi
	movq	%r8, -144(%rbp)
	addq	%r10, %rdi
	movq	%r9, -120(%rbp)
	movq	%r10, -112(%rbp)
	call	memmove@PLT
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %r9
	movq	-144(%rbp), %r8
	movq	-152(%rbp), %rcx
	movq	%r10, -168(%rbp)
	movq	%r10, %rax
	jmp	.L650
.L672:
	movq	%r10, -168(%rbp)
	movq	%r10, %rax
	jmp	.L650
.L730:
	leaq	48(%r14), %r10
	movq	$0, 48(%r14)
	movq	%r10, -144(%rbp)
	jmp	.L630
.L718:
	call	__stack_chk_fail@PLT
.L651:
	call	_ZSt17__throw_bad_allocv@PLT
.L707:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L724:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE28185:
	.size	_ZN2v88internal12_GLOBAL__N_131CalculateFunctionLiteralChangesERKSt6vectorIPNS0_15FunctionLiteralESaIS4_EERKS2_INS0_17SourceChangeRangeESaIS9_EEPSt13unordered_mapIS4_NS1_21FunctionLiteralChangeESt4hashIS4_ESt8equal_toIS4_ESaISt4pairIKS4_SF_EEE.constprop.0, .-_ZN2v88internal12_GLOBAL__N_131CalculateFunctionLiteralChangesERKSt6vectorIPNS0_15FunctionLiteralESaIS4_EERKS2_INS0_17SourceChangeRangeESaIS9_EEPSt13unordered_mapIS4_NS1_21FunctionLiteralChangeESt4hashIS4_ESt8equal_toIS4_ESaISt4pairIKS4_SF_EEE.constprop.0
	.section	.text._ZN2v88internal8LiveEdit21InitializeThreadLocalEPNS0_5DebugE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8LiveEdit21InitializeThreadLocalEPNS0_5DebugE
	.type	_ZN2v88internal8LiveEdit21InitializeThreadLocalEPNS0_5DebugE, @function
_ZN2v88internal8LiveEdit21InitializeThreadLocalEPNS0_5DebugE:
.LFB20643:
	.cfi_startproc
	endbr64
	movq	$0, 120(%rdi)
	ret
	.cfi_endproc
.LFE20643:
	.size	_ZN2v88internal8LiveEdit21InitializeThreadLocalEPNS0_5DebugE, .-_ZN2v88internal8LiveEdit21InitializeThreadLocalEPNS0_5DebugE
	.section	.text._ZN2v88internal8LiveEdit12RestartFrameEPNS0_15JavaScriptFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8LiveEdit12RestartFrameEPNS0_15JavaScriptFrameE
	.type	_ZN2v88internal8LiveEdit12RestartFrameEPNS0_15JavaScriptFrameE, @function
_ZN2v88internal8LiveEdit12RestartFrameEPNS0_15JavaScriptFrameE:
.LFB20644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1512, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	_ZN2v88internal8LiveEdit22kFrameDropperSupportedE(%rip), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%r13b, %r13b
	jne	.L771
.L733:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L772
	addq	$1512, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L771:
	.cfi_restore_state
	movq	16(%rdi), %rsi
	leaq	-1504(%rbp), %r14
	movq	%rdi, %r12
	movq	%r14, %rdi
	movq	41472(%rsi), %rax
	movq	%rsi, -1552(%rbp)
	movl	72(%rax), %eax
	testl	%eax, %eax
	movl	%eax, -1540(%rbp)
	sete	%bl
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r15
	testq	%r15, %r15
	je	.L770
.L735:
	testb	%bl, %bl
	jne	.L736
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*56(%rax)
	cmpl	%eax, -1540(%rbp)
	je	.L773
	movq	32(%r12), %rax
	cmpq	%rax, 32(%r15)
	je	.L770
.L747:
	movq	%r14, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %r15
	testq	%r15, %r15
	jne	.L735
	.p2align 4,,10
	.p2align 3
.L770:
	xorl	%r13d, %r13d
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L736:
	movq	32(%r12), %rax
	cmpq	%rax, 32(%r15)
	je	.L738
.L739:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	cmpl	$3, %eax
	je	.L770
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	cmpl	$21, %eax
	je	.L770
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	cmpl	$20, %eax
	ja	.L750
	movl	$1150992, %ebx
	movl	%eax, %ecx
	shrq	%cl, %rbx
	notq	%rbx
	andl	$1, %ebx
	jne	.L747
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	leaq	-1536(%rbp), %rsi
	movq	$0, -1520(%rbp)
	movaps	%xmm0, -1536(%rbp)
	call	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_6HandleINS0_18SharedFunctionInfoEEESaIS5_EE@PLT
	movq	-1536(%rbp), %rdi
	movq	-1528(%rbp), %rcx
	cmpq	%rdi, %rcx
	je	.L742
.L744:
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movl	47(%rax), %eax
	andl	$31, %eax
	leal	-9(%rax), %edx
	cmpb	$6, %dl
	jbe	.L743
	cmpb	$1, %al
	jne	.L774
.L743:
	movq	-1536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L770
	call	_ZdlPv@PLT
	xorl	%r13d, %r13d
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L774:
	addq	$8, %rdi
	cmpq	%rdi, %rcx
	jne	.L744
	movq	-1536(%rbp), %rdi
.L742:
	testq	%rdi, %rdi
	je	.L750
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L750:
	movl	%r13d, %ebx
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L773:
	movq	32(%r15), %rax
	cmpq	%rax, 32(%r12)
	jne	.L739
.L738:
	movq	-1552(%rbp), %rax
	movq	%r15, %rsi
	movq	41472(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal5Debug20ScheduleFrameRestartEPNS0_10StackFrameE@PLT
	jmp	.L733
.L772:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20644:
	.size	_ZN2v88internal8LiveEdit12RestartFrameEPNS0_15JavaScriptFrameE, .-_ZN2v88internal8LiveEdit12RestartFrameEPNS0_15JavaScriptFrameE
	.section	.rodata._ZN2v88internal8LiveEdit14CompareStringsEPNS0_7IsolateENS0_6HandleINS0_6StringEEES6_PSt6vectorINS0_17SourceChangeRangeESaIS8_EE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"NewArray"
	.section	.text._ZN2v88internal8LiveEdit14CompareStringsEPNS0_7IsolateENS0_6HandleINS0_6StringEEES6_PSt6vectorINS0_17SourceChangeRangeESaIS8_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8LiveEdit14CompareStringsEPNS0_7IsolateENS0_6HandleINS0_6StringEEES6_PSt6vectorINS0_17SourceChangeRangeESaIS8_EE
	.type	_ZN2v88internal8LiveEdit14CompareStringsEPNS0_7IsolateENS0_6HandleINS0_6StringEEES6_PSt6vectorINS0_17SourceChangeRangeESaIS8_EE, @function
_ZN2v88internal8LiveEdit14CompareStringsEPNS0_7IsolateENS0_6HandleINS0_6StringEEES6_PSt6vectorINS0_17SourceChangeRangeESaIS8_EE:
.LFB20645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, %rsi
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L982
.L777:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L983
.L785:
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	jbe	.L984
.L792:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L985
.L800:
	xorl	%edx, %edx
	movq	%r8, %rdi
	movq	%r15, %rsi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal6String17CalculateLineEndsEPNS0_7IsolateENS0_6HandleIS1_EEb@PLT
	movq	-272(%rbp), %r8
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rax, %r14
	movq	(%r15), %rax
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_121LineArrayCompareInputE(%rip), %rbx
	movq	%r8, %rdi
	movq	%r8, -288(%rbp)
	movl	11(%rax), %r9d
	movl	%r9d, -264(%rbp)
	call	_ZN2v88internal6String17CalculateLineEndsEPNS0_7IsolateENS0_6HandleIS1_EEb@PLT
	movq	(%r12), %rdx
	movl	-264(%rbp), %r9d
	movq	%r12, %xmm1
	movq	-288(%rbp), %r8
	movq	%r15, %xmm0
	movl	11(%rdx), %edx
	movq	%r12, -208(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rbx, -224(%rbp)
	movq	%r15, -216(%rbp)
	movq	%r14, -200(%rbp)
	movl	%r9d, -192(%rbp)
	movq	%rax, -184(%rbp)
	movl	%edx, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	(%r14), %rsi
	movslq	11(%rsi), %rbx
	leal	1(%rbx), %r13d
	movq	%rbx, -264(%rbp)
	movl	%r13d, -160(%rbp)
	movq	(%rax), %rsi
	movslq	11(%rsi), %rdi
	movq	%r8, -136(%rbp)
	movq	%r14, -128(%rbp)
	movl	%edi, %ebx
	movq	%rdi, -272(%rbp)
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputE(%rip), %rdi
	addl	$1, %ebx
	movq	%rdi, -144(%rbp)
	movq	-280(%rbp), %rdi
	cmpl	%ebx, %r13d
	movl	%ebx, %r12d
	movl	%ebx, -156(%rbp)
	cmovle	%r13d, %r12d
	movl	%r9d, -120(%rbp)
	movq	%rax, -112(%rbp)
	movl	%edx, -104(%rbp)
	movq	$0, -80(%rbp)
	movq	%rdi, -72(%rbp)
	movaps	%xmm0, -96(%rbp)
	testl	%r12d, %r12d
	jle	.L986
	xorl	%edi, %edi
	xorl	%edx, %edx
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L806:
	movq	(%r14), %r8
	cmpl	%edi, 11(%r8)
	je	.L987
	leal	16(,%rdi,8), %edi
	movslq	%edi, %rdi
	movq	-1(%r8,%rdi), %r8
	sarq	$32, %r8
	addl	$1, %r8d
.L813:
	movq	(%rax), %rdi
	movl	-176(%rbp), %eax
	cmpl	%edx, 11(%rdi)
	je	.L815
	leal	16(,%rdx,8), %eax
	cltq
	movq	-1(%rdi,%rax), %rax
	sarq	$32, %rax
	addl	$1, %eax
.L815:
	subl	%esi, %r8d
	subl	%ecx, %eax
	cmpl	%eax, %r8d
	je	.L816
.L979:
	movl	%ebx, %r9d
	movl	%r13d, %eax
	subl	%r15d, %r9d
	subl	%r15d, %eax
.L807:
	movl	-272(%rbp), %edi
	cmpl	%edi, -264(%rbp)
	cmovle	%eax, %r9d
	testl	%r9d, %r9d
	jle	.L937
	movl	%r9d, -288(%rbp)
	xorl	%r10d, %r10d
	movl	%ebx, %r14d
	movl	%r13d, %r12d
	movl	%r10d, %ebx
	.p2align 4,,10
	.p2align 3
.L833:
	movl	-164(%rbp), %eax
	movl	%r14d, %r13d
	movl	%r12d, %r9d
	leal	-1(%r14), %r14d
	leal	-1(%r12), %r12d
	addl	%r14d, %eax
	movl	%r12d, %edx
	addl	-168(%rbp), %edx
	je	.L938
	movq	-200(%rbp), %r8
	leal	-1(%rdx), %ecx
	movq	(%r8), %rsi
	cmpl	%ecx, 11(%rsi)
	je	.L988
	leal	8(,%rdx,8), %ecx
	movslq	%ecx, %rcx
	movq	-1(%rsi,%rcx), %rsi
	sarq	$32, %rsi
	addl	$1, %esi
.L821:
	testl	%eax, %eax
	je	.L939
	movq	-184(%rbp), %rdi
	leal	-1(%rax), %ecx
	movq	(%rdi), %r11
	cmpl	%ecx, 11(%r11)
	je	.L989
	leal	8(,%rax,8), %ecx
	movslq	%ecx, %rcx
	movq	-1(%r11,%rcx), %rcx
	sarq	$32, %rcx
	addl	$1, %ecx
.L823:
	movq	(%r8), %r8
	cmpl	%edx, 11(%r8)
	je	.L990
	leal	16(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	-1(%r8,%rdx), %r8
	sarq	$32, %r8
	addl	$1, %r8d
.L826:
	movq	(%rdi), %rdx
	cmpl	%eax, 11(%rdx)
	je	.L991
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	sarq	$32, %rax
	addl	$1, %eax
.L828:
	subl	%esi, %r8d
	subl	%ecx, %eax
	cmpl	%eax, %r8d
	je	.L829
.L980:
	movl	%ebx, %r10d
	movl	%r13d, %ebx
	movl	%r9d, %r13d
	testl	%r10d, %r10d
	setg	%al
.L820:
	testl	%r15d, %r15d
	jg	.L831
	testb	%al, %al
	jne	.L831
	movl	-156(%rbp), %ebx
	movl	-160(%rbp), %r13d
.L832:
	movl	%ebx, -236(%rbp)
	imull	%r13d, %ebx
	leaq	-224(%rbp), %rax
	movq	%rax, -256(%rbp)
	leaq	_ZSt7nothrow(%rip), %rsi
	movabsq	$2305843009213693950, %rax
	movl	%r13d, -240(%rbp)
	movslq	%ebx, %rbx
	cmpq	%rax, %rbx
	leaq	0(,%rbx,4), %r12
	movq	$-1, %rax
	cmova	%rax, %r12
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L992
.L835:
	movl	-240(%rbp), %esi
	movl	-236(%rbp), %edx
	movq	%rax, -248(%rbp)
	movl	%esi, %edi
	movl	%esi, %ecx
	movl	%edx, %r8d
	imull	%edx, %edi
	testl	%edi, %edi
	jle	.L836
	movl	$-4, (%rax)
	cmpl	$1, %edi
	je	.L838
	leal	-2(%rdi), %eax
	leaq	8(,%rax,4), %rcx
	movl	$4, %eax
	.p2align 4,,10
	.p2align 3
.L839:
	movq	-248(%rbp), %rdx
	movl	$-4, (%rdx,%rax)
	addq	$4, %rax
	cmpq	%rcx, %rax
	jne	.L839
	movl	-240(%rbp), %esi
	movl	-236(%rbp), %edx
.L838:
	movl	%esi, %ecx
	movl	%edx, %r8d
.L836:
	testl	%esi, %esi
	jle	.L913
	testl	%edx, %edx
	jle	.L912
	movq	-248(%rbp), %rax
	movl	(%rax), %eax
	andl	$-4, %eax
	cmpl	$-4, %eax
	je	.L993
.L912:
	xorl	%eax, %eax
	movl	$-1, %r14d
	movl	$-1, %ebx
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L995:
	subl	$2, %edx
	cmpl	$1, %edx
	ja	.L918
	testb	%al, %al
	jne	.L919
	movl	%r12d, %r14d
	movl	%r13d, %ebx
	movl	$1, %eax
.L919:
	addl	$1, %r12d
	movl	$1, %edx
.L921:
	movl	%esi, %ecx
	cmpl	%esi, %r13d
	jge	.L994
.L914:
	cmpl	%r8d, %r12d
	jge	.L916
	movl	%esi, %edx
	movq	-248(%rbp), %rcx
	imull	%r12d, %edx
	addl	%r13d, %edx
	movslq	%edx, %rdx
	movl	(%rcx,%rdx,4), %edx
	andl	$3, %edx
	cmpl	$1, %edx
	jne	.L995
	testb	%al, %al
	jne	.L922
	movl	%r12d, %r14d
	movl	%r13d, %ebx
	movl	$1, %eax
.L922:
	addl	$1, %r13d
	movl	$1, %edx
	movl	%esi, %ecx
	cmpl	%esi, %r13d
	jl	.L914
.L994:
	cmpl	%r8d, %r12d
	je	.L925
	testb	%dl, %dl
	je	.L928
	movl	%r8d, %r12d
	movl	%r13d, %ecx
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L987:
	movl	-192(%rbp), %r8d
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L829:
	movq	-208(%rbp), %rdx
	movq	-216(%rbp), %rdi
	movl	%r9d, -280(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_117CompareSubstringsENS0_6HandleINS0_6StringEEEiS4_ii
	movl	-280(%rbp), %r9d
	testb	%al, %al
	je	.L980
	leal	1(%rbx), %eax
	cmpl	%eax, -288(%rbp)
	je	.L996
	movl	%eax, %ebx
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L991:
	movl	-176(%rbp), %eax
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L990:
	movl	-192(%rbp), %r8d
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L989:
	movl	-176(%rbp), %ecx
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L939:
	movq	-184(%rbp), %rdi
	xorl	%ecx, %ecx
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L988:
	movl	-192(%rbp), %esi
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L938:
	movq	-200(%rbp), %r8
	xorl	%esi, %esi
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L816:
	movq	-208(%rbp), %rdx
	movq	-216(%rbp), %rdi
	call	_ZN2v88internal12_GLOBAL__N_117CompareSubstringsENS0_6HandleINS0_6StringEEEiS4_ii
	testb	%al, %al
	je	.L979
	addl	$1, %r15d
	cmpl	%r12d, %r15d
	je	.L979
	movl	-164(%rbp), %edx
	movl	%r15d, %edi
	addl	%r15d, %edx
	addl	-168(%rbp), %edi
	je	.L935
	movq	-200(%rbp), %r14
	movl	-192(%rbp), %esi
	leal	-1(%rdi), %eax
	movq	(%r14), %rcx
	cmpl	%eax, 11(%rcx)
	je	.L808
	leal	8(,%rdi,8), %eax
	cltq
	movq	-1(%rcx,%rax), %rsi
	sarq	$32, %rsi
	addl	$1, %esi
.L808:
	testl	%edx, %edx
	je	.L936
.L998:
	movq	-184(%rbp), %rax
	leal	-1(%rdx), %ecx
	movq	(%rax), %r8
	cmpl	%ecx, 11(%r8)
	je	.L997
	leal	8(,%rdx,8), %ecx
	movslq	%ecx, %rcx
	movq	-1(%r8,%rcx), %rcx
	sarq	$32, %rcx
	addl	$1, %ecx
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L935:
	movq	-200(%rbp), %r14
	xorl	%esi, %esi
	testl	%edx, %edx
	jne	.L998
.L936:
	movq	-184(%rbp), %rax
	xorl	%ecx, %ecx
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L997:
	movl	-176(%rbp), %ecx
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L985:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L800
	movq	(%r12), %rax
	movq	41112(%r8), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L803
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-264(%rbp), %r8
	movq	%rax, %r12
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L983:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L785
	movq	(%r15), %rax
	movq	41112(%r8), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L788
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-264(%rbp), %r8
	movq	%rax, %r15
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L996:
	movl	-264(%rbp), %r13d
	movl	%ebx, %r10d
	subl	%ebx, %r13d
	movl	-272(%rbp), %ebx
	subl	%r10d, %ebx
.L831:
	subl	%r15d, %r13d
	subl	%r15d, %ebx
	movl	%r15d, -80(%rbp)
	movl	%r15d, -168(%rbp)
	movl	%r13d, -160(%rbp)
	movl	%r15d, -164(%rbp)
	movl	%ebx, -156(%rbp)
	movl	%r15d, -76(%rbp)
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L918:
	testb	%al, %al
	je	.L920
	movl	%r13d, %ecx
	movl	%r12d, %r8d
	movl	%ebx, %esi
	movl	%r14d, %edx
	subl	%r14d, %r8d
	subl	%ebx, %ecx
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput8AddChunkEiiii
	movl	-236(%rbp), %r8d
	movl	-240(%rbp), %esi
	xorl	%eax, %eax
.L920:
	addl	$1, %r13d
	addl	$1, %r12d
	xorl	%edx, %edx
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L984:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L792
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L795
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L795
	movq	%r8, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	-264(%rbp), %r8
	movq	%rax, %r12
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L982:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L777
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L780
	movq	23(%rax), %rdx
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	je	.L780
	movq	%r15, %rsi
	xorl	%edx, %edx
	movq	%rdi, -264(%rbp)
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	-264(%rbp), %r8
	movq	%rax, %r15
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L916:
	movl	%r12d, %r8d
	testb	%al, %al
	je	.L924
.L981:
	movl	%r12d, %r8d
	movl	%ebx, %r13d
	movl	%r14d, %r12d
	.p2align 4,,10
	.p2align 3
.L924:
	subl	%r13d, %ecx
	leaq	-144(%rbp), %rdi
	subl	%r12d, %r8d
	movl	%r12d, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput8AddChunkEiiii
.L926:
	movq	-248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L927
	call	_ZdaPv@PLT
.L927:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L999
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L925:
	.cfi_restore_state
	testb	%dl, %dl
	je	.L926
	movl	%r13d, %ecx
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L842:
	movslq	-240(%rbp), %rax
	movl	-236(%rbp), %edx
	movq	-248(%rbp), %rcx
	cmpl	$1, %eax
	jle	.L865
	testl	%edx, %edx
	jle	.L866
	movl	4(%rcx), %r12d
	andl	$-4, %r12d
	cmpl	$-4, %r12d
	je	.L1000
.L867:
	leal	4(%r12), %ebx
.L931:
	leal	0(,%rax,4), %r13d
	cmpl	$1, %edx
	jle	.L890
	movl	(%rcx,%rax,4), %eax
	andl	$-4, %eax
	movl	%eax, %r13d
	cmpl	$-4, %eax
	je	.L1001
.L890:
	cmpl	%r12d, %r13d
	je	.L1002
	leal	4(%r13), %eax
	orl	$1, %ebx
	orl	$2, %eax
	cmpl	%r12d, %r13d
	cmovle	%eax, %ebx
.L845:
	movl	%ebx, (%rcx)
	movl	-240(%rbp), %esi
	movl	-236(%rbp), %r8d
	movl	%esi, %ecx
	testl	%esi, %esi
	jg	.L912
	.p2align 4,,10
	.p2align 3
.L913:
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	testl	%r8d, %r8d
	je	.L926
.L928:
	movl	%r13d, %ecx
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L803:
	movq	41088(%r8), %r12
	cmpq	41096(%r8), %r12
	je	.L1003
.L805:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r8)
	movq	%rsi, (%r12)
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L788:
	movq	41088(%r8), %r15
	cmpq	41096(%r8), %r15
	je	.L1004
.L790:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r8)
	movq	%rsi, (%r15)
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L993:
	movq	-256(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L842
	movl	-240(%rbp), %eax
	movq	-248(%rbp), %rcx
	movl	-236(%rbp), %edx
	cmpl	$1, %eax
	jle	.L843
	cmpl	$1, %edx
	jle	.L844
	addl	$1, %eax
	cltq
	movl	(%rcx,%rax,4), %ebx
	andl	$-4, %ebx
	cmpl	$-4, %ebx
	jne	.L845
	movq	-256(%rbp), %rdi
	movl	$1, %edx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L846
	movl	-240(%rbp), %ecx
	movl	-236(%rbp), %esi
	movq	-248(%rbp), %rdx
	leal	1(%rcx), %eax
	cmpl	$2, %ecx
	jle	.L847
	cmpl	$2, %esi
	jle	.L848
	leal	(%rax,%rax), %ecx
	movslq	%ecx, %rcx
	movl	(%rdx,%rcx,4), %ebx
	andl	$-4, %ebx
	movl	%ebx, %ecx
	cmpl	$-4, %ebx
	je	.L1005
.L849:
	cltq
	movl	%ecx, (%rdx,%rax,4)
	movq	-248(%rbp), %rcx
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L780:
	movq	41112(%r8), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L782
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-264(%rbp), %r8
	movq	(%rax), %rsi
	movq	%rax, %r15
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L795:
	movq	41112(%r8), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L797
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-264(%rbp), %r8
	movq	(%rax), %rsi
	movq	%rax, %r12
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L937:
	xorl	%eax, %eax
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L986:
	movl	%r13d, %eax
	movl	%ebx, %r9d
	xorl	%r15d, %r15d
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L782:
	movq	41088(%r8), %r15
	cmpq	41096(%r8), %r15
	je	.L1006
.L784:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r8)
	movq	%rsi, (%r15)
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L797:
	movq	41088(%r8), %r12
	cmpq	41096(%r8), %r12
	je	.L1007
.L799:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r8)
	movq	%rsi, (%r12)
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L843:
	leal	-4(,%rdx,4), %ebx
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L865:
	leal	0(,%rdx,4), %r12d
.L887:
	leal	4(%r12), %ebx
	leal	-4(,%rdx,4), %r13d
	testl	%eax, %eax
	jle	.L890
	jmp	.L931
.L1006:
	movq	%r8, %rdi
	movq	%rsi, -272(%rbp)
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %r8
	movq	%rax, %r15
	jmp	.L784
.L1007:
	movq	%r8, %rdi
	movq	%rsi, -272(%rbp)
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %r8
	movq	%rax, %r12
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L1002:
	orl	$3, %ebx
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	%r8, %rdi
	movq	%rsi, -272(%rbp)
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %r8
	movq	%rax, %r15
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L1003:
	movq	%r8, %rdi
	movq	%rsi, -272(%rbp)
	movq	%r8, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %r8
	movq	%rax, %r12
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L866:
	leal	-4(,%rax,4), %r12d
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L844:
	leal	-4(,%rax,4), %ebx
	jmp	.L845
.L1001:
	movq	-256(%rbp), %rdi
	movl	$1, %edx
	xorl	%esi, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	-236(%rbp), %edx
	movq	-248(%rbp), %rcx
	testb	%al, %al
	movslq	-240(%rbp), %rax
	je	.L891
	cmpl	$1, %eax
	jle	.L892
	cmpl	$2, %edx
	jle	.L893
	leal	1(%rax,%rax), %edx
	movslq	%edx, %rdx
	movl	(%rcx,%rdx,4), %r13d
	andl	$-4, %r13d
	movl	%r13d, %edx
	cmpl	$-4, %r13d
	je	.L1008
.L894:
	movl	%edx, (%rcx,%rax,4)
	movq	-248(%rbp), %rcx
	jmp	.L890
.L1000:
	movq	-256(%rbp), %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	-240(%rbp), %edx
	movl	-236(%rbp), %ecx
	testb	%al, %al
	movq	-248(%rbp), %rax
	je	.L868
	cmpl	$2, %edx
	jle	.L869
	cmpl	$1, %ecx
	jle	.L870
	addl	$2, %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %r12d
	andl	$-4, %r12d
	movl	%r12d, %edx
	cmpl	$-4, %r12d
	je	.L1009
.L871:
	movl	%edx, 4(%rax)
	movslq	-240(%rbp), %rax
	movl	-236(%rbp), %edx
	movq	-248(%rbp), %rcx
	jmp	.L887
.L846:
	movl	-240(%rbp), %eax
	movl	-236(%rbp), %ecx
	movq	-248(%rbp), %rdx
	cmpl	$2, %eax
	jle	.L852
	cmpl	$1, %ecx
	jle	.L853
	leal	2(%rax), %esi
	movslq	%esi, %rsi
	movl	(%rdx,%rsi,4), %r12d
	andl	$-4, %r12d
	cmpl	$-4, %r12d
	je	.L1010
.L854:
	leal	4(%r12), %ebx
.L929:
	cmpl	$2, %ecx
	jle	.L859
	leal	1(%rax,%rax), %ecx
	movslq	%ecx, %rcx
	movl	(%rdx,%rcx,4), %r13d
	andl	$-4, %r13d
	cmpl	$-4, %r13d
	je	.L1011
.L860:
	movl	%ebx, %ecx
	addl	$1, %eax
	orl	$3, %ecx
	cmpl	%r13d, %r12d
	je	.L849
	jge	.L864
	movl	%ebx, %ecx
	orl	$1, %ecx
	jmp	.L849
.L868:
	cmpl	$2, %edx
	jle	.L874
	testl	%ecx, %ecx
	jle	.L875
	movl	8(%rax), %ebx
	andl	$-4, %ebx
	cmpl	$-4, %ebx
	je	.L1012
.L876:
	leal	4(%rbx), %r12d
.L932:
	cmpl	$1, %ecx
	jle	.L881
	addl	$1, %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %r13d
	andl	$-4, %r13d
	cmpl	$-4, %r13d
	je	.L1013
.L882:
	movl	%r12d, %edx
	orl	$3, %edx
	cmpl	%r13d, %ebx
	je	.L871
	jge	.L886
	movl	%r12d, %edx
	orl	$1, %edx
	jmp	.L871
.L891:
	cmpl	$1, %eax
	jle	.L897
	cmpl	$1, %edx
	jle	.L898
	leal	1(%rax), %esi
	movslq	%esi, %rsi
	movl	(%rcx,%rsi,4), %r14d
	andl	$-4, %r14d
	cmpl	$-4, %r14d
	je	.L1014
.L899:
	leal	4(%r14), %r13d
.L930:
	leal	0(,%rax,4), %r15d
	cmpl	$2, %edx
	jle	.L905
	leal	(%rax,%rax), %edx
	movslq	%edx, %rdx
	movl	(%rcx,%rdx,4), %r15d
	andl	$-4, %r15d
	cmpl	$-4, %r15d
	je	.L1015
.L905:
	movl	%r13d, %edx
	orl	$3, %edx
	cmpl	%r15d, %r14d
	je	.L894
	jge	.L909
	movl	%r13d, %edx
	orl	$1, %edx
	jmp	.L894
.L897:
	leal	-4(,%rdx,4), %r14d
.L902:
	leal	4(%r14), %r13d
	leal	-8(,%rdx,4), %r15d
	testl	%eax, %eax
	jle	.L905
	jmp	.L930
.L869:
	leal	-4(,%rcx,4), %r12d
	movl	%r12d, %edx
	jmp	.L871
.L892:
	leal	-8(,%rdx,4), %r13d
	movl	%r13d, %edx
	jmp	.L894
.L847:
	leal	-8(,%rsi,4), %ebx
	movl	%ebx, %ecx
	jmp	.L849
.L852:
	leal	-4(,%rcx,4), %r12d
.L857:
	leal	4(%r12), %ebx
	leal	-8(,%rcx,4), %r13d
	cmpl	$1, %eax
	jle	.L860
	jmp	.L929
.L874:
	leal	0(,%rcx,4), %ebx
.L879:
	leal	4(%rbx), %r12d
	leal	-4(,%rcx,4), %r13d
	cmpl	$1, %edx
	jle	.L882
	jmp	.L932
.L875:
	leal	-8(,%rdx,4), %ebx
	jmp	.L876
.L881:
	leal	-4(,%rdx,4), %r13d
	jmp	.L882
.L893:
	leal	-4(,%rax,4), %r13d
	movl	%r13d, %edx
	jmp	.L894
.L886:
	leal	4(%r13), %r12d
	movl	%r12d, %edx
	orl	$2, %edx
	jmp	.L871
.L898:
	leal	-4(,%rax,4), %r14d
	jmp	.L899
.L909:
	leal	4(%r15), %r13d
	movl	%r13d, %edx
	orl	$2, %edx
	jmp	.L894
.L848:
	leal	-8(,%rcx,4), %ebx
	movl	%ebx, %ecx
	jmp	.L849
.L853:
	leal	-8(,%rax,4), %r12d
	jmp	.L854
.L859:
	leal	-4(,%rax,4), %r13d
	jmp	.L860
.L870:
	leal	-8(,%rdx,4), %r12d
	movl	%r12d, %edx
	jmp	.L871
.L864:
	leal	4(%r13), %ebx
	movl	%ebx, %ecx
	orl	$2, %ecx
	jmp	.L849
.L1009:
	movq	-256(%rbp), %rdi
	movl	$1, %edx
	movl	$2, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L872
	leaq	-256(%rbp), %rdi
	movl	$2, %edx
	movl	$3, %esi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %r12d
	xorl	%eax, %eax
.L873:
	movl	-240(%rbp), %ebx
	movq	-248(%rbp), %rcx
	orl	%r12d, %eax
	leal	2(%rbx), %edx
	movslq	%edx, %rdx
	movl	%eax, (%rcx,%rdx,4)
	movl	%r12d, %edx
	movq	-248(%rbp), %rax
	jmp	.L871
.L1011:
	movq	-256(%rbp), %rdi
	movl	$2, %edx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L861
	leaq	-256(%rbp), %rdi
	movl	$3, %edx
	movl	$2, %esi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %r13d
	xorl	%eax, %eax
.L862:
	movl	-240(%rbp), %edx
	movq	-248(%rbp), %rcx
	orl	%r13d, %eax
	leal	1(%rdx,%rdx), %edx
	movslq	%edx, %rdx
	movl	%eax, (%rcx,%rdx,4)
	movl	-240(%rbp), %eax
	movq	-248(%rbp), %rdx
	jmp	.L860
.L1015:
	movq	-256(%rbp), %rdi
	movl	$2, %edx
	xorl	%esi, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	leaq	-256(%rbp), %rdi
	testb	%al, %al
	je	.L906
	movl	$3, %edx
	movl	$1, %esi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %r15d
	xorl	%eax, %eax
.L907:
	movl	-240(%rbp), %edi
	movq	-248(%rbp), %rcx
	orl	%r15d, %eax
	leal	(%rdi,%rdi), %edx
	movslq	%edx, %rdx
	movl	%eax, (%rcx,%rdx,4)
	movslq	-240(%rbp), %rax
	movq	-248(%rbp), %rcx
	jmp	.L905
.L1010:
	movq	-256(%rbp), %rdi
	movl	$1, %edx
	movl	$2, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L855
	leaq	-256(%rbp), %rdi
	movl	$2, %edx
	movl	$3, %esi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %r12d
	xorl	%eax, %eax
.L856:
	movl	-240(%rbp), %ebx
	movq	-248(%rbp), %rcx
	orl	%r12d, %eax
	leal	2(%rbx), %edx
	movslq	%edx, %rdx
	movl	%eax, (%rcx,%rdx,4)
	movl	-240(%rbp), %eax
	movl	-236(%rbp), %ecx
	movq	-248(%rbp), %rdx
	jmp	.L857
.L1012:
	movq	-256(%rbp), %rdi
	xorl	%edx, %edx
	movl	$2, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L877
	leaq	-256(%rbp), %rdi
	movl	$1, %edx
	movl	$3, %esi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %ebx
	xorl	%eax, %eax
.L878:
	movq	-248(%rbp), %rdx
	orl	%ebx, %eax
	movl	%eax, 8(%rdx)
	movl	-240(%rbp), %edx
	movl	-236(%rbp), %ecx
	movq	-248(%rbp), %rax
	jmp	.L879
.L992:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L835
	leaq	.LC4(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L1013:
	movq	-256(%rbp), %rdi
	movl	$1, %edx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L883
	leaq	-256(%rbp), %rdi
	movl	$2, %edx
	movl	$2, %esi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %r13d
	xorl	%eax, %eax
.L884:
	movl	-240(%rbp), %edi
	movq	-248(%rbp), %rcx
	orl	%r13d, %eax
	leal	1(%rdi), %edx
	movslq	%edx, %rdx
	movl	%eax, (%rcx,%rdx,4)
	movq	-248(%rbp), %rax
	jmp	.L882
.L1014:
	movq	-256(%rbp), %rdi
	movl	$1, %edx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L900
	leaq	-256(%rbp), %rdi
	movl	$2, %edx
	movl	$2, %esi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %r14d
	xorl	%eax, %eax
.L901:
	movl	-240(%rbp), %edi
	movq	-248(%rbp), %rcx
	orl	%r14d, %eax
	leal	1(%rdi), %edx
	movslq	%edx, %rdx
	movl	%eax, (%rcx,%rdx,4)
	movslq	-240(%rbp), %rax
	movl	-236(%rbp), %edx
	movq	-248(%rbp), %rcx
	jmp	.L902
.L999:
	call	__stack_chk_fail@PLT
.L1008:
	movq	-256(%rbp), %rdi
	movl	$2, %edx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L895
	leaq	-256(%rbp), %rdi
	movl	$3, %edx
	movl	$2, %esi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %r13d
	xorl	%eax, %eax
.L896:
	movl	-240(%rbp), %edx
	movq	-248(%rbp), %rcx
	orl	%r13d, %eax
	leal	1(%rdx,%rdx), %edx
	movslq	%edx, %rdx
	movl	%eax, (%rcx,%rdx,4)
	movl	%r13d, %edx
	movslq	-240(%rbp), %rax
	movq	-248(%rbp), %rcx
	jmp	.L894
.L1005:
	movq	-256(%rbp), %rdi
	movl	$2, %edx
	movl	$2, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L850
	leaq	-256(%rbp), %rdi
	movl	$3, %edx
	movl	$3, %esi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %ebx
	xorl	%eax, %eax
.L851:
	movl	-240(%rbp), %edx
	movq	-248(%rbp), %rcx
	orl	%ebx, %eax
	leal	2(%rdx,%rdx), %edx
	movslq	%edx, %rdx
	movl	%eax, (%rcx,%rdx,4)
	movl	-240(%rbp), %eax
	movl	%ebx, %ecx
	movq	-248(%rbp), %rdx
	addl	$1, %eax
	jmp	.L849
.L855:
	leaq	-256(%rbp), %r13
	movl	$1, %edx
	movl	$3, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	$2, %edx
	movl	$2, %esi
	movq	%r13, %rdi
	movl	%eax, %ebx
	leal	4(%rax), %r12d
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %edx
	cmpl	%eax, %ebx
	je	.L945
	movl	$1, %eax
	jl	.L856
	leal	4(%rdx), %r12d
	movl	$2, %eax
	jmp	.L856
.L850:
	leaq	-256(%rbp), %r13
	movl	$2, %edx
	movl	$3, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	$3, %edx
	movl	$2, %esi
	movq	%r13, %rdi
	movl	%eax, %r12d
	leal	4(%rax), %ebx
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %edx
	cmpl	%eax, %r12d
	je	.L943
	movl	$1, %eax
	jl	.L851
	leal	4(%rdx), %ebx
	movl	$2, %eax
	jmp	.L851
.L861:
	leaq	-256(%rbp), %r15
	movl	$2, %edx
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	$3, %edx
	movl	$1, %esi
	movq	%r15, %rdi
	movl	%eax, %r14d
	leal	4(%rax), %r13d
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %edx
	cmpl	%eax, %r14d
	je	.L947
	movl	$1, %eax
	jl	.L862
	leal	4(%rdx), %r13d
	movl	$2, %eax
	jmp	.L862
.L900:
	leaq	-256(%rbp), %r15
	movl	$1, %edx
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	$2, %edx
	movl	$1, %esi
	movq	%r15, %rdi
	movl	%eax, %r13d
	leal	4(%rax), %r14d
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %edx
	cmpl	%eax, %r13d
	je	.L959
	movl	$1, %eax
	jl	.L901
	leal	4(%rdx), %r14d
	movl	$2, %eax
	jmp	.L901
.L872:
	leaq	-256(%rbp), %r13
	movl	$1, %edx
	movl	$3, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	$2, %edx
	movl	$2, %esi
	movq	%r13, %rdi
	movl	%eax, %ebx
	leal	4(%rax), %r12d
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %edx
	cmpl	%eax, %ebx
	je	.L950
	movl	$1, %eax
	jl	.L873
	leal	4(%rdx), %r12d
	movl	$2, %eax
	jmp	.L873
.L877:
	leaq	-256(%rbp), %r13
	xorl	%edx, %edx
	movl	$3, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	$1, %edx
	movl	$2, %esi
	movq	%r13, %rdi
	movl	%eax, %r12d
	leal	4(%rax), %ebx
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %edx
	cmpl	%eax, %r12d
	je	.L952
	movl	$1, %eax
	jl	.L878
	leal	4(%rdx), %ebx
	movl	$2, %eax
	jmp	.L878
.L906:
	movl	$2, %edx
	movl	$1, %esi
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movq	-272(%rbp), %rdi
	movl	$3, %edx
	xorl	%esi, %esi
	movl	%eax, -264(%rbp)
	leal	4(%rax), %r15d
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	-264(%rbp), %ecx
	movl	%eax, %edx
	cmpl	%eax, %ecx
	je	.L961
	movl	$1, %eax
	jl	.L907
	leal	4(%rdx), %r15d
	movl	$2, %eax
	jmp	.L907
.L895:
	leaq	-256(%rbp), %r15
	movl	$2, %edx
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	$3, %edx
	movl	$1, %esi
	movq	%r15, %rdi
	movl	%eax, %r14d
	leal	4(%rax), %r13d
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %edx
	cmpl	%eax, %r14d
	je	.L957
	movl	$1, %eax
	jl	.L896
	leal	4(%rdx), %r13d
	movl	$2, %eax
	jmp	.L896
.L883:
	leaq	-256(%rbp), %r15
	movl	$1, %edx
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	$2, %edx
	movl	$1, %esi
	movq	%r15, %rdi
	movl	%eax, %r14d
	leal	4(%rax), %r13d
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %edx
	cmpl	%eax, %r14d
	je	.L954
	movl	$1, %eax
	jl	.L884
	leal	4(%rdx), %r13d
	movl	$2, %eax
	jmp	.L884
.L945:
	movl	$3, %eax
	jmp	.L856
.L954:
	movl	$3, %eax
	jmp	.L884
.L947:
	movl	$3, %eax
	jmp	.L862
.L950:
	movl	$3, %eax
	jmp	.L873
.L961:
	movl	$3, %eax
	jmp	.L907
.L943:
	movl	$3, %eax
	jmp	.L851
.L952:
	movl	$3, %eax
	jmp	.L878
.L959:
	movl	$3, %eax
	jmp	.L901
.L957:
	movl	$3, %eax
	jmp	.L896
	.cfi_endproc
.LFE20645:
	.size	_ZN2v88internal8LiveEdit14CompareStringsEPNS0_7IsolateENS0_6HandleINS0_6StringEEES6_PSt6vectorINS0_17SourceChangeRangeESaIS8_EE, .-_ZN2v88internal8LiveEdit14CompareStringsEPNS0_7IsolateENS0_6HandleINS0_6StringEEES6_PSt6vectorINS0_17SourceChangeRangeESaIS8_EE
	.section	.text._ZN2v88internal8LiveEdit17TranslatePositionERKSt6vectorINS0_17SourceChangeRangeESaIS3_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8LiveEdit17TranslatePositionERKSt6vectorINS0_17SourceChangeRangeESaIS3_EEi
	.type	_ZN2v88internal8LiveEdit17TranslatePositionERKSt6vectorINS0_17SourceChangeRangeESaIS3_EEi, @function
_ZN2v88internal8LiveEdit17TranslatePositionERKSt6vectorINS0_17SourceChangeRangeESaIS3_EEi:
.LFB20646:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r9
	movq	(%rdi), %rax
	movq	%r9, %rdx
	movq	%rax, %r8
	subq	%rax, %rdx
	sarq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1018:
	testq	%rdx, %rdx
	jle	.L1017
.L1024:
	movq	%rdx, %rdi
	sarq	%rdi
	movq	%rdi, %rcx
	salq	$4, %rcx
	addq	%r8, %rcx
	cmpl	%esi, 4(%rcx)
	jge	.L1022
	subq	%rdi, %rdx
	leaq	16(%rcx), %r8
	subq	$1, %rdx
	testq	%rdx, %rdx
	jg	.L1024
.L1017:
	cmpq	%r8, %r9
	je	.L1020
	cmpl	%esi, 4(%r8)
	je	.L1025
.L1020:
	cmpq	%r8, %rax
	je	.L1023
	addl	-4(%r8), %esi
	movl	%esi, %eax
	subl	-12(%r8), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1022:
	movq	%rdi, %rdx
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1025:
	movl	12(%r8), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1023:
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE20646:
	.size	_ZN2v88internal8LiveEdit17TranslatePositionERKSt6vectorINS0_17SourceChangeRangeESaIS3_EEi, .-_ZN2v88internal8LiveEdit17TranslatePositionERKSt6vectorINS0_17SourceChangeRangeESaIS3_EEi
	.section	.text._ZNSt6vectorIN2v88internal17SourceChangeRangeESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal17SourceChangeRangeESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal17SourceChangeRangeESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal17SourceChangeRangeESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal17SourceChangeRangeESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB24593:
	.cfi_startproc
	endbr64
	movabsq	$576460752303423487, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$4, %rax
	cmpq	%rcx, %rax
	je	.L1040
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1036
	movabsq	$9223372036854775792, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1041
.L1028:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1035:
	movdqu	(%r15), %xmm1
	subq	%r8, %r13
	leaq	16(%rbx,%rdx), %r15
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	movups	%xmm1, (%rbx,%rdx)
	testq	%rdx, %rdx
	jg	.L1042
	testq	%r13, %r13
	jg	.L1031
	testq	%r9, %r9
	jne	.L1034
.L1032:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1042:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1031
.L1034:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1041:
	testq	%rsi, %rsi
	jne	.L1029
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1032
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1036:
	movl	$16, %r14d
	jmp	.L1028
.L1040:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1029:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$4, %r14
	jmp	.L1028
	.cfi_endproc
.LFE24593:
	.size	_ZNSt6vectorIN2v88internal17SourceChangeRangeESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal17SourceChangeRangeESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput8AddChunkEiiii,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput8AddChunkEiiii, @function
_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput8AddChunkEiiii:
.LFB20379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	addl	68(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	64(%rdi), %esi
	je	.L1044
	movq	16(%rdi), %rax
	leal	-1(%rsi), %r9d
	movq	(%rax), %rax
	cmpl	%r9d, 11(%rax)
	je	.L1206
	leal	8(,%rsi,8), %r9d
	movslq	%r9d, %r9
	movq	-1(%rax,%r9), %rax
	sarq	$32, %rax
	addl	$1, %eax
.L1044:
	xorl	%r10d, %r10d
	testl	%edx, %edx
	je	.L1046
	movq	32(%rdi), %r9
	leal	-1(%rdx), %r10d
	movq	(%r9), %r9
	cmpl	%r10d, 11(%r9)
	je	.L1207
	leal	8(,%rdx,8), %r10d
	movslq	%r10d, %r10
	movq	-1(%r9,%r10), %r10
	sarq	$32, %r10
	addl	$1, %r10d
.L1046:
	addl	%esi, %ecx
	je	.L1048
.L1216:
	movq	16(%rdi), %rsi
	leal	-1(%rcx), %r9d
	movq	(%rsi), %rsi
	cmpl	%r9d, 11(%rsi)
	je	.L1208
	leal	8(,%rcx,8), %ecx
	movslq	%ecx, %rcx
	movq	-1(%rsi,%rcx), %rcx
	sarq	$32, %rcx
	addl	$1, %ecx
.L1048:
	movl	%ecx, %esi
	subl	%eax, %esi
	addl	%r8d, %edx
	je	.L1050
.L1221:
	movq	32(%rdi), %r8
	leal	-1(%rdx), %r9d
	movq	(%r8), %r8
	cmpl	%r9d, 11(%r8)
	je	.L1209
	leal	8(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	-1(%r8,%rdx), %rdx
	sarq	$32, %rdx
	addl	$1, %edx
.L1050:
	movl	%edx, %r8d
	subl	%r10d, %r8d
	cmpl	$799, %esi
	setle	%r15b
	cmpl	$799, %r8d
	setle	%r9b
	andb	%r9b, %r15b
	je	.L1052
	movq	8(%rdi), %r13
	movl	%esi, -112(%rbp)
	movl	%eax, -144(%rbp)
	movq	41088(%r13), %rbx
	addl	$1, 41104(%r13)
	movq	56(%rdi), %rdx
	movq	48(%rdi), %rcx
	movl	%r10d, -140(%rbp)
	movq	%rbx, -184(%rbp)
	movq	41096(%r13), %rbx
	movl	%esi, -76(%rbp)
	imull	%r8d, %esi
	movl	%eax, -80(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -128(%rbp)
	movabsq	$2305843009213693950, %rax
	movslq	%esi, %rsi
	movq	%rbx, -176(%rbp)
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_118TokensCompareInputE(%rip), %rbx
	cmpq	%rax, %rsi
	leaq	0(,%rsi,4), %r12
	movq	$-1, %rax
	movq	%rdx, -72(%rbp)
	cmova	%rax, %r12
	movq	72(%rdi), %rdx
	movq	%rbx, -96(%rbp)
	leaq	_ZSt7nothrow(%rip), %rsi
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_119TokensCompareOutputE(%rip), %rbx
	movq	%rcx, -88(%rbp)
	movq	%r12, %rdi
	movl	%r10d, -64(%rbp)
	movl	%r8d, -60(%rbp)
	movq	%rbx, -160(%rbp)
	movq	%rdx, -152(%rbp)
	movl	%r8d, -108(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L1210
.L1054:
	movl	-112(%rbp), %edi
	movl	-108(%rbp), %edx
	movq	%rax, -120(%rbp)
	movl	%edi, %esi
	movl	%edi, %ecx
	movl	%edx, %r10d
	imull	%edx, %esi
	testl	%esi, %esi
	jle	.L1055
	movl	$-4, (%rax)
	cmpl	$1, %esi
	je	.L1057
	leal	-2(%rsi), %eax
	leaq	8(,%rax,4), %rcx
	movl	$4, %eax
	.p2align 4,,10
	.p2align 3
.L1058:
	movq	-120(%rbp), %rdx
	movl	$-4, (%rdx,%rax)
	addq	$4, %rax
	cmpq	%rax, %rcx
	jne	.L1058
	movl	-112(%rbp), %edi
	movl	-108(%rbp), %edx
.L1057:
	movl	%edi, %ecx
	movl	%edx, %r10d
.L1055:
	testl	%edi, %edi
	jle	.L1126
	testl	%edx, %edx
	jg	.L1211
.L1125:
	xorl	%r8d, %r8d
	movl	$-1, %edx
	movl	$-1, %r14d
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1213:
	subl	$2, %eax
	cmpl	$1, %eax
	ja	.L1131
	testb	%r8b, %r8b
	jne	.L1132
	movl	%ebx, %edx
	movl	%r12d, %r14d
	movl	$1, %r8d
.L1132:
	addl	$1, %ebx
	movl	%r15d, %eax
.L1134:
	movl	%edi, %ecx
	cmpl	%r12d, %edi
	jle	.L1212
.L1127:
	cmpl	%r10d, %ebx
	jge	.L1129
	movl	%ebx, %eax
	movq	-120(%rbp), %rcx
	imull	%edi, %eax
	addl	%r12d, %eax
	cltq
	movl	(%rcx,%rax,4), %eax
	andl	$3, %eax
	cmpl	$1, %eax
	jne	.L1213
	testb	%r8b, %r8b
	jne	.L1135
	movl	%ebx, %edx
	movl	%r12d, %r14d
	movl	$1, %r8d
.L1135:
	addl	$1, %r12d
	movl	%r15d, %eax
	movl	%edi, %ecx
	cmpl	%r12d, %edi
	jg	.L1127
.L1212:
	cmpl	%r10d, %ebx
	je	.L1138
	testb	%al, %al
	je	.L1146
	movl	%r10d, %ebx
	movl	%r12d, %ecx
.L1205:
	movl	%r14d, %r12d
	jmp	.L1137
	.p2align 4,,10
	.p2align 3
.L1052:
	movd	%edx, %xmm2
	movd	%eax, %xmm0
	movd	%ecx, %xmm3
	movq	72(%rdi), %rdi
	movd	%r10d, %xmm1
	punpckldq	%xmm3, %xmm0
	punpckldq	%xmm2, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L1142
	movups	%xmm0, (%rsi)
	addq	$16, 8(%rdi)
.L1043:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1214
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1131:
	.cfi_restore_state
	testb	%r8b, %r8b
	je	.L1133
	movl	%r12d, %ecx
	movl	%ebx, %r8d
	leaq	-160(%rbp), %rdi
	movl	%r14d, %esi
	subl	%edx, %r8d
	subl	%r14d, %ecx
	movl	%edx, -164(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_119TokensCompareOutput8AddChunkEiiii
	movl	-108(%rbp), %r10d
	movl	-112(%rbp), %edi
	xorl	%r8d, %r8d
	movl	-164(%rbp), %edx
.L1133:
	addl	$1, %r12d
	addl	$1, %ebx
	xorl	%eax, %eax
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1129:
	testb	%r8b, %r8b
	jne	.L1205
	movl	%ebx, %edx
.L1137:
	subl	%edx, %ebx
	subl	%r12d, %ecx
	leaq	-160(%rbp), %rdi
	movl	%r12d, %esi
	movl	%ebx, %r8d
	call	_ZN2v88internal12_GLOBAL__N_119TokensCompareOutput8AddChunkEiiii
	jmp	.L1139
	.p2align 4,,10
	.p2align 3
.L1138:
	testb	%al, %al
	jne	.L1215
.L1139:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1140
	call	_ZdaPv@PLT
.L1140:
	movq	-184(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-176(%rbp), %rax
	cmpq	41096(%r13), %rax
	je	.L1043
	movq	%rax, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1206:
	movl	24(%rdi), %eax
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1207:
	movl	40(%rdi), %r10d
	addl	%esi, %ecx
	je	.L1048
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1211:
	movq	-120(%rbp), %rax
	movl	(%rax), %eax
	andl	$-4, %eax
	cmpl	$-4, %eax
	jne	.L1125
	movq	-128(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	jne	.L1217
	movslq	-112(%rbp), %rdx
	movl	-108(%rbp), %esi
	movq	-120(%rbp), %rax
	cmpl	$1, %edx
	jle	.L1082
	testl	%esi, %esi
	jle	.L1083
	movl	4(%rax), %ebx
	andl	$-4, %ebx
	cmpl	$-4, %ebx
	je	.L1218
.L1084:
	leal	4(%rbx), %r14d
.L1149:
	leal	0(,%rdx,4), %r12d
	cmpl	$1, %esi
	jle	.L1105
	movl	(%rax,%rdx,4), %r12d
	andl	$-4, %r12d
	cmpl	$-4, %r12d
	je	.L1219
.L1105:
	cmpl	%ebx, %r12d
	je	.L1220
	movl	%r14d, %ecx
	leal	4(%r12), %edx
	orl	$1, %ecx
	orl	$2, %edx
	cmpl	%ebx, %r12d
	movl	%ecx, %ebx
	cmovle	%edx, %ebx
.L1064:
	movl	%ebx, (%rax)
	movl	-112(%rbp), %edi
	movl	-108(%rbp), %r10d
	movl	%edi, %ecx
	testl	%edi, %edi
	jg	.L1125
	.p2align 4,,10
	.p2align 3
.L1126:
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	testl	%r10d, %r10d
	je	.L1139
.L1146:
	movl	%ebx, %edx
	movl	%r12d, %ecx
	movl	%r10d, %ebx
	jmp	.L1137
	.p2align 4,,10
	.p2align 3
.L1208:
	movl	24(%rdi), %ecx
	movl	%ecx, %esi
	subl	%eax, %esi
	addl	%r8d, %edx
	je	.L1050
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1209:
	movl	40(%rdi), %edx
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1142:
	leaq	-96(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal17SourceChangeRangeESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1217:
	movl	-112(%rbp), %edx
	movq	-120(%rbp), %rax
	movl	-108(%rbp), %ecx
	cmpl	$1, %edx
	jle	.L1062
	cmpl	$1, %ecx
	jle	.L1063
	addl	$1, %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %ebx
	andl	$-4, %ebx
	cmpl	$-4, %ebx
	jne	.L1064
	movq	-128(%rbp), %rdi
	movl	$1, %edx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L1065
	movl	-112(%rbp), %eax
	movl	-108(%rbp), %edx
	cmpl	$2, %eax
	jle	.L1066
	cmpl	$2, %edx
	jle	.L1067
	leal	2(%rax,%rax), %edx
	movq	-120(%rbp), %rax
	xorl	%ecx, %ecx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %ebx
	andl	$-4, %ebx
	cmpl	$-4, %ebx
	je	.L1222
.L1068:
	movl	-112(%rbp), %esi
	orl	%ebx, %ecx
	leal	1(%rsi), %edx
	movslq	%edx, %rdx
	movl	%ecx, (%rax,%rdx,4)
	movq	-120(%rbp), %rax
	jmp	.L1064
.L1220:
	movl	%r14d, %ebx
	orl	$3, %ebx
	jmp	.L1064
.L1082:
	leal	0(,%rsi,4), %ebx
.L1102:
	leal	4(%rbx), %r14d
	leal	-4(,%rsi,4), %r12d
	testl	%edx, %edx
	jle	.L1105
	jmp	.L1149
.L1062:
	leal	-4(,%rcx,4), %ebx
	jmp	.L1064
.L1063:
	leal	-4(,%rdx,4), %ebx
	jmp	.L1064
.L1083:
	leal	-4(,%rdx,4), %ebx
	jmp	.L1084
.L1219:
	movq	-128(%rbp), %rdi
	xorl	%esi, %esi
	movl	$1, %edx
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L1106
	movl	-112(%rbp), %eax
	movl	-108(%rbp), %edx
	cmpl	$1, %eax
	jle	.L1107
	cmpl	$2, %edx
	jle	.L1108
	leal	1(%rax,%rax), %edx
	movq	-120(%rbp), %rax
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %r12d
	xorl	%edx, %edx
	andl	$-4, %r12d
	cmpl	$-4, %r12d
	je	.L1223
.L1109:
	movslq	-112(%rbp), %rsi
	orl	%r12d, %edx
	movl	%edx, (%rax,%rsi,4)
	movq	-120(%rbp), %rax
	jmp	.L1105
.L1218:
	movq	-128(%rbp), %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L1085
	movl	-112(%rbp), %eax
	movl	-108(%rbp), %edx
	cmpl	$2, %eax
	jle	.L1086
	cmpl	$1, %edx
	jle	.L1087
	addl	$2, %eax
	movslq	%eax, %rdx
	movq	-120(%rbp), %rax
	movl	(%rax,%rdx,4), %ebx
	xorl	%edx, %edx
	andl	$-4, %ebx
	cmpl	$-4, %ebx
	je	.L1224
.L1088:
	orl	%ebx, %edx
	movl	%edx, 4(%rax)
	movslq	-112(%rbp), %rdx
	movl	-108(%rbp), %esi
	movq	-120(%rbp), %rax
	jmp	.L1102
.L1065:
	movl	-112(%rbp), %edi
	movl	-108(%rbp), %eax
	cmpl	$2, %edi
	jle	.L1071
	cmpl	$1, %eax
	jle	.L1072
	movq	-120(%rbp), %rax
	leal	2(%rdi), %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %r12d
	andl	$-4, %r12d
	cmpl	$-4, %r12d
	je	.L1225
.L1073:
	leal	4(%r12), %ebx
.L1147:
	cmpl	$2, -108(%rbp)
	jle	.L1078
	leal	1(%rdi,%rdi), %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %r8d
	movl	%r8d, %r14d
	andl	$-4, %r14d
	cmpl	$-4, %r14d
	je	.L1226
.L1079:
	movl	$3, %ecx
	cmpl	%r12d, %r14d
	je	.L1068
	movl	$1, %ecx
	jg	.L1068
	leal	4(%r14), %ebx
	movl	$2, %ecx
	jmp	.L1068
.L1106:
	movl	-112(%rbp), %edi
	movl	-108(%rbp), %eax
	cmpl	$1, %edi
	jle	.L1112
	cmpl	$1, %eax
	jle	.L1113
	movq	-120(%rbp), %rax
	leal	1(%rdi), %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %r8d
	andl	$-4, %r8d
	cmpl	$-4, %r8d
	je	.L1227
.L1114:
	leal	4(%r8), %r12d
.L1148:
	cmpl	$2, -108(%rbp)
	leal	0(,%rdi,4), %r10d
	jle	.L1120
	leal	(%rdi,%rdi), %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %r10d
	andl	$-4, %r10d
	cmpl	$-4, %r10d
	je	.L1228
.L1120:
	movl	$3, %edx
	cmpl	%r10d, %r8d
	je	.L1109
	movl	$1, %edx
	jl	.L1109
	leal	4(%r10), %r12d
	movl	$2, %edx
	jmp	.L1109
.L1085:
	movl	-112(%rbp), %edi
	movl	-108(%rbp), %r12d
	cmpl	$2, %edi
	jle	.L1091
	testl	%r12d, %r12d
	jle	.L1092
	movq	-120(%rbp), %rax
	movl	8(%rax), %r12d
	andl	$-4, %r12d
	cmpl	$-4, %r12d
	je	.L1229
.L1093:
	leal	4(%r12), %ebx
.L1150:
	cmpl	$1, -108(%rbp)
	jle	.L1098
	leal	1(%rdi), %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %ecx
	andl	$-4, %ecx
	movl	%ecx, %r14d
	cmpl	$-4, %ecx
	je	.L1230
.L1099:
	movl	$3, %edx
	cmpl	%r14d, %r12d
	je	.L1088
	movl	$1, %edx
	jl	.L1088
	leal	4(%r14), %ebx
	movl	$2, %edx
	jmp	.L1088
.L1086:
	leal	-4(,%rdx,4), %ebx
	movq	-120(%rbp), %rax
	xorl	%edx, %edx
	jmp	.L1088
.L1098:
	leal	-4(,%rdi,4), %r14d
	jmp	.L1099
.L1092:
	movq	-120(%rbp), %rax
	leal	-8(,%rdi,4), %r12d
	jmp	.L1093
.L1091:
	sall	$2, %r12d
.L1096:
	leal	4(%r12), %ebx
	cmpl	$1, %edi
	jg	.L1231
	movl	-108(%rbp), %eax
	leal	-4(,%rax,4), %r14d
	movq	-120(%rbp), %rax
	jmp	.L1099
.L1112:
	leal	-4(,%rax,4), %r8d
.L1117:
	leal	4(%r8), %r12d
	testl	%edi, %edi
	jg	.L1232
	movl	-108(%rbp), %eax
	leal	-8(,%rax,4), %r10d
	movq	-120(%rbp), %rax
	jmp	.L1120
.L1113:
	movq	-120(%rbp), %rax
	leal	-4(,%rdi,4), %r8d
	jmp	.L1114
.L1108:
	leal	-4(,%rax,4), %r12d
	xorl	%edx, %edx
	movq	-120(%rbp), %rax
	jmp	.L1109
.L1107:
	leal	-8(,%rdx,4), %r12d
	movq	-120(%rbp), %rax
	xorl	%edx, %edx
	jmp	.L1109
.L1078:
	leal	-4(,%rdi,4), %r14d
	jmp	.L1079
.L1072:
	movq	-120(%rbp), %rax
	leal	-8(,%rdi,4), %r12d
	jmp	.L1073
.L1071:
	leal	-4(,%rax,4), %r12d
.L1076:
	leal	4(%r12), %ebx
	cmpl	$1, %edi
	jg	.L1233
	movl	-108(%rbp), %eax
	leal	-8(,%rax,4), %r14d
	movq	-120(%rbp), %rax
	jmp	.L1079
.L1066:
	movq	-120(%rbp), %rax
	leal	-8(,%rdx,4), %ebx
	xorl	%ecx, %ecx
	jmp	.L1068
.L1087:
	leal	-8(,%rax,4), %ebx
	xorl	%edx, %edx
	movq	-120(%rbp), %rax
	jmp	.L1088
.L1067:
	leal	-8(,%rax,4), %ebx
	xorl	%ecx, %ecx
	movq	-120(%rbp), %rax
	jmp	.L1068
.L1214:
	call	__stack_chk_fail@PLT
.L1222:
	movq	-128(%rbp), %rdi
	movl	$2, %edx
	movl	$2, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L1069
	leaq	-128(%rbp), %rdi
	movl	$3, %edx
	movl	$3, %esi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %ebx
	xorl	%eax, %eax
.L1070:
	movl	-112(%rbp), %edx
	movq	-120(%rbp), %rcx
	orl	%ebx, %eax
	leal	2(%rdx,%rdx), %edx
	movslq	%edx, %rdx
	movl	%eax, (%rcx,%rdx,4)
	xorl	%ecx, %ecx
	movq	-120(%rbp), %rax
	jmp	.L1068
.L1233:
	movq	-120(%rbp), %rax
	jmp	.L1147
.L1225:
	movq	-128(%rbp), %rdi
	movl	$1, %edx
	movl	$2, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L1074
	leaq	-128(%rbp), %rdi
	movl	$2, %edx
	movl	$3, %esi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %r12d
	xorl	%eax, %eax
.L1075:
	movl	-112(%rbp), %ebx
	movq	-120(%rbp), %rcx
	orl	%r12d, %eax
	leal	2(%rbx), %edx
	movslq	%edx, %rdx
	movl	%eax, (%rcx,%rdx,4)
	movl	-112(%rbp), %edi
	jmp	.L1076
.L1224:
	movq	-128(%rbp), %rdi
	movl	$1, %edx
	movl	$2, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L1089
	leaq	-128(%rbp), %rdi
	movl	$2, %edx
	movl	$3, %esi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %ebx
	xorl	%eax, %eax
.L1090:
	movl	-112(%rbp), %esi
	movq	-120(%rbp), %rcx
	orl	%ebx, %eax
	leal	2(%rsi), %edx
	movslq	%edx, %rdx
	movl	%eax, (%rcx,%rdx,4)
	xorl	%edx, %edx
	movq	-120(%rbp), %rax
	jmp	.L1088
.L1227:
	movq	-128(%rbp), %rdi
	movl	$1, %edx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	leaq	-128(%rbp), %rdi
	testb	%al, %al
	je	.L1115
	movl	$2, %edx
	movl	$2, %esi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %r8d
	xorl	%eax, %eax
.L1116:
	movl	-112(%rbp), %esi
	orl	%r8d, %eax
	leal	1(%rsi), %edx
	movq	-120(%rbp), %rsi
	movslq	%edx, %rdx
	movl	%eax, (%rsi,%rdx,4)
	movl	-112(%rbp), %edi
	jmp	.L1117
.L1232:
	movq	-120(%rbp), %rax
	jmp	.L1148
.L1210:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L1054
	leaq	.LC4(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L1228:
	movq	-128(%rbp), %rdi
	movl	%r8d, -164(%rbp)
	xorl	%esi, %esi
	movl	$2, %edx
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	-164(%rbp), %r8d
	testb	%al, %al
	je	.L1121
	leaq	-128(%rbp), %rdi
	movl	$3, %edx
	movl	$1, %esi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	-164(%rbp), %r8d
	movl	%eax, %r10d
	xorl	%eax, %eax
.L1122:
	movl	-112(%rbp), %esi
	orl	%r10d, %eax
	leal	(%rsi,%rsi), %edx
	movq	-120(%rbp), %rsi
	movslq	%edx, %rdx
	movl	%eax, (%rsi,%rdx,4)
	movq	-120(%rbp), %rax
	jmp	.L1120
.L1231:
	movq	-120(%rbp), %rax
	jmp	.L1150
.L1223:
	movq	-128(%rbp), %rdi
	movl	$2, %edx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	leaq	-128(%rbp), %rdi
	testb	%al, %al
	je	.L1110
	movl	$3, %edx
	movl	$2, %esi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %r12d
	xorl	%eax, %eax
.L1111:
	movl	-112(%rbp), %edx
	movq	-120(%rbp), %rsi
	orl	%r12d, %eax
	leal	1(%rdx,%rdx), %edx
	movslq	%edx, %rdx
	movl	%eax, (%rsi,%rdx,4)
	xorl	%edx, %edx
	movq	-120(%rbp), %rax
	jmp	.L1109
.L1226:
	movq	-128(%rbp), %rdi
	movl	$2, %edx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	leaq	-128(%rbp), %rdi
	testb	%al, %al
	je	.L1080
	movl	$3, %edx
	movl	$2, %esi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %r14d
	xorl	%eax, %eax
.L1081:
	movl	-112(%rbp), %edx
	movq	-120(%rbp), %rcx
	orl	%r14d, %eax
	leal	1(%rdx,%rdx), %edx
	movslq	%edx, %rdx
	movl	%eax, (%rcx,%rdx,4)
	movq	-120(%rbp), %rax
	jmp	.L1079
.L1230:
	movq	-128(%rbp), %rdi
	movl	$1, %edx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	leaq	-128(%rbp), %rdi
	testb	%al, %al
	je	.L1100
	movl	$2, %edx
	movl	$2, %esi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %r14d
	xorl	%eax, %eax
.L1101:
	movl	-112(%rbp), %esi
	orl	%r14d, %eax
	leal	1(%rsi), %edx
	movq	-120(%rbp), %rsi
	movslq	%edx, %rdx
	movl	%eax, (%rsi,%rdx,4)
	movq	-120(%rbp), %rax
	jmp	.L1099
.L1229:
	movq	-128(%rbp), %rdi
	xorl	%edx, %edx
	movl	$2, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L1094
	leaq	-128(%rbp), %rdi
	movl	$1, %edx
	movl	$3, %esi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %r12d
	xorl	%eax, %eax
.L1095:
	movq	-120(%rbp), %rdx
	orl	%r12d, %eax
	movl	%eax, 8(%rdx)
	movl	-112(%rbp), %edi
	jmp	.L1096
.L1110:
	movl	$2, %edx
	movl	$2, %esi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	$3, %edx
	movl	$1, %esi
	movq	-192(%rbp), %rdi
	movl	%eax, -164(%rbp)
	leal	4(%rax), %r12d
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	-164(%rbp), %r8d
	movl	%eax, %edx
	cmpl	%eax, %r8d
	je	.L1174
	movl	$1, %eax
	jl	.L1111
	leal	4(%rdx), %r12d
	movl	$2, %eax
	jmp	.L1111
.L1094:
	leaq	-128(%rbp), %r14
	xorl	%edx, %edx
	movl	$3, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	$1, %edx
	movl	$2, %esi
	movq	%r14, %rdi
	movl	%eax, %ebx
	leal	4(%rax), %r12d
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %edx
	cmpl	%eax, %ebx
	je	.L1167
	movl	$1, %eax
	jl	.L1095
	leal	4(%rdx), %r12d
	movl	$2, %eax
	jmp	.L1095
.L1069:
	leaq	-128(%rbp), %r14
	movl	$2, %edx
	movl	$3, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	$3, %edx
	movl	$2, %esi
	movq	%r14, %rdi
	movl	%eax, %r12d
	leal	4(%rax), %ebx
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %edx
	cmpl	%eax, %r12d
	je	.L1156
	movl	$1, %eax
	jl	.L1070
	leal	4(%rdx), %ebx
	movl	$2, %eax
	jmp	.L1070
.L1121:
	leaq	-128(%rbp), %rdi
	movl	$2, %edx
	movl	$1, %esi
	movl	%r8d, -168(%rbp)
	movq	%rdi, -200(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movq	-200(%rbp), %rdi
	movl	$3, %edx
	xorl	%esi, %esi
	leal	4(%rax), %r10d
	movl	%eax, -192(%rbp)
	movl	%r10d, -164(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	-192(%rbp), %r11d
	movl	-164(%rbp), %r10d
	movl	-168(%rbp), %r8d
	movl	%eax, %edx
	cmpl	%eax, %r11d
	je	.L1178
	movl	$1, %eax
	jl	.L1122
	leal	4(%rdx), %r10d
	movl	$2, %eax
	jmp	.L1122
.L1089:
	leaq	-128(%rbp), %r14
	movl	$1, %edx
	movl	$3, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	$2, %edx
	movl	$2, %esi
	movq	%r14, %rdi
	movl	%eax, %r12d
	leal	4(%rax), %ebx
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %edx
	cmpl	%eax, %r12d
	je	.L1165
	movl	$1, %eax
	jl	.L1090
	leal	4(%rdx), %ebx
	movl	$2, %eax
	jmp	.L1090
.L1080:
	movl	$2, %edx
	movl	$2, %esi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	$3, %edx
	movl	$1, %esi
	movq	-192(%rbp), %rdi
	movl	%eax, -164(%rbp)
	leal	4(%rax), %r14d
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	-164(%rbp), %ecx
	movl	%eax, %edx
	cmpl	%eax, %ecx
	je	.L1160
	movl	$1, %eax
	jl	.L1081
	leal	4(%rdx), %r14d
	movl	$2, %eax
	jmp	.L1081
.L1074:
	leaq	-128(%rbp), %r14
	movl	$1, %edx
	movl	$3, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	$2, %edx
	movl	$2, %esi
	movq	%r14, %rdi
	movl	%eax, %ebx
	leal	4(%rax), %r12d
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	%eax, %edx
	cmpl	%eax, %ebx
	je	.L1158
	movl	$1, %eax
	jl	.L1075
	leal	4(%rdx), %r12d
	movl	$2, %eax
	jmp	.L1075
.L1100:
	movl	$1, %edx
	movl	$2, %esi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	$2, %edx
	movl	$1, %esi
	movq	-192(%rbp), %rdi
	movl	%eax, -164(%rbp)
	leal	4(%rax), %r14d
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	-164(%rbp), %r8d
	movl	%eax, %edx
	cmpl	%eax, %r8d
	je	.L1169
	movl	$1, %eax
	jl	.L1101
	leal	4(%rdx), %r14d
	movl	$2, %eax
	jmp	.L1101
.L1115:
	movl	$1, %edx
	movl	$2, %esi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	$2, %edx
	movl	$1, %esi
	movq	-192(%rbp), %rdi
	leal	4(%rax), %r8d
	movl	%eax, %r12d
	movl	%r8d, -164(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111Differencer15CompareUpToTailEii
	movl	-164(%rbp), %r8d
	cmpl	%eax, %r12d
	movl	%eax, %edx
	je	.L1176
	movl	$1, %eax
	jl	.L1116
	leal	4(%rdx), %r8d
	movl	$2, %eax
	jmp	.L1116
.L1174:
	movl	$3, %eax
	jmp	.L1111
.L1176:
	movl	$3, %eax
	jmp	.L1116
.L1215:
	movl	%r12d, %ecx
	jmp	.L1205
.L1156:
	movl	$3, %eax
	jmp	.L1070
.L1165:
	movl	$3, %eax
	jmp	.L1090
.L1158:
	movl	$3, %eax
	jmp	.L1075
.L1167:
	movl	$3, %eax
	jmp	.L1095
.L1160:
	movl	$3, %eax
	jmp	.L1081
.L1178:
	movl	$3, %eax
	jmp	.L1122
.L1169:
	movl	$3, %eax
	jmp	.L1101
	.cfi_endproc
.LFE20379:
	.size	_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput8AddChunkEiiii, .-_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput8AddChunkEiiii
	.section	.text._ZN2v88internal12_GLOBAL__N_119TokensCompareOutput8AddChunkEiiii,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119TokensCompareOutput8AddChunkEiiii, @function
_ZN2v88internal12_GLOBAL__N_119TokensCompareOutput8AddChunkEiiii:
.LFB20346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	16(%rdi), %ecx
	addl	20(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %r9
	addl	%edx, %r8d
	movl	%edx, -24(%rbp)
	leal	(%rcx,%rsi), %eax
	addl	%r10d, %esi
	movl	%r8d, -20(%rbp)
	addl	%ecx, %esi
	movl	%eax, -32(%rbp)
	movq	8(%r9), %r10
	movl	%esi, -28(%rbp)
	cmpq	16(%r9), %r10
	je	.L1235
	movd	%eax, %xmm1
	movd	%esi, %xmm4
	movd	%edx, %xmm0
	movd	%r8d, %xmm3
	punpckldq	%xmm4, %xmm1
	punpckldq	%xmm3, %xmm0
	movdqa	%xmm1, %xmm2
	punpcklqdq	%xmm0, %xmm2
	movups	%xmm2, (%r10)
	addq	$16, 8(%r9)
.L1234:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1239
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1235:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	movq	%r10, %rsi
	movq	%r9, %rdi
	call	_ZNSt6vectorIN2v88internal17SourceChangeRangeESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L1234
.L1239:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20346:
	.size	_ZN2v88internal12_GLOBAL__N_119TokensCompareOutput8AddChunkEiiii, .-_ZN2v88internal12_GLOBAL__N_119TokensCompareOutput8AddChunkEiiii
	.section	.text._ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E,"axG",@progbits,_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	.type	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E, @function
_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E:
.LFB24681:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1248
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L1242:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1242
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1248:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE24681:
	.size	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E, .-_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	.section	.text._ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB24753:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1265
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1261
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1266
.L1253:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1260:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1267
	testq	%r13, %r13
	jg	.L1256
	testq	%r9, %r9
	jne	.L1259
.L1257:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1267:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1256
.L1259:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1266:
	testq	%rsi, %rsi
	jne	.L1254
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1256:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1257
	jmp	.L1259
	.p2align 4,,10
	.p2align 3
.L1261:
	movl	$8, %r14d
	jmp	.L1253
.L1265:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1254:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L1253
	.cfi_endproc
.LFE24753:
	.size	_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE10VisitBlockEPNS0_5BlockE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE10VisitBlockEPNS0_5BlockE, @function
_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE10VisitBlockEPNS0_5BlockE:
.LFB26390:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 24(%rsi)
	je	.L1373
	movl	24(%rdi), %esi
	leal	1(%rsi), %eax
	movl	%eax, 24(%rdi)
	movq	24(%r12), %rdx
	movq	96(%rdx), %r14
	leaq	88(%rdx), %rbx
	movzbl	8(%rdi), %edx
	cmpq	%rbx, %r14
	je	.L1271
	testb	%dl, %dl
	je	.L1372
.L1322:
	subl	$1, %eax
	jmp	.L1371
	.p2align 4,,10
	.p2align 3
.L1289:
	movl	24(%r15), %edi
	movq	40(%r13), %rsi
	movq	%r13, -64(%rbp)
	leal	1(%rdi), %edx
	leaq	88(%rsi), %rax
	movl	%edx, 24(%r15)
	movq	96(%rsi), %rcx
	movq	%rcx, -80(%rbp)
	cmpq	%rcx, %rax
	jne	.L1363
	cmpb	$0, 8(%r15)
	movl	%edi, 24(%r15)
	jne	.L1362
.L1364:
	movq	40(%r13), %rax
	cmpb	$0, 131(%rax)
	js	.L1362
	movl	%edx, 24(%r15)
	movl	60(%r13), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L1365
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1426:
	movq	-72(%rbp), %rax
	addq	$1, %rax
	cmpl	%eax, 60(%r13)
	jle	.L1366
.L1365:
	movq	48(%r13), %rdx
	movq	%r15, %rdi
	movq	%rax, -72(%rbp)
	movq	(%rdx,%rax,8), %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r15)
	je	.L1426
.L1366:
	subl	$1, 24(%r15)
.L1362:
	movq	32(%r15), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L1367
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 8(%rdi)
	.p2align 4,,10
	.p2align 3
.L1275:
	cmpb	$0, 8(%r15)
	jne	.L1427
.L1370:
	movq	(%rbx), %rbx
	addq	$16, %rbx
	cmpq	%r14, %rbx
	je	.L1428
.L1372:
	movq	(%rbx), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r15), %rax
	jb	.L1429
	movzbl	4(%r13), %eax
	andl	$63, %eax
	cmpb	$57, %al
	ja	.L1275
	leaq	.L1277(%rip), %rcx
	movzbl	%al, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE10VisitBlockEPNS0_5BlockE,"a",@progbits
	.align 4
	.align 4
.L1277:
	.long	.L1275-.L1277
	.long	.L1343-.L1277
	.long	.L1319-.L1277
	.long	.L1318-.L1277
	.long	.L1317-.L1277
	.long	.L1315-.L1277
	.long	.L1315-.L1277
	.long	.L1314-.L1277
	.long	.L1313-.L1277
	.long	.L1309-.L1277
	.long	.L1275-.L1277
	.long	.L1344-.L1277
	.long	.L1310-.L1277
	.long	.L1275-.L1277
	.long	.L1275-.L1277
	.long	.L1309-.L1277
	.long	.L1308-.L1277
	.long	.L1307-.L1277
	.long	.L1306-.L1277
	.long	.L1275-.L1277
	.long	.L1305-.L1277
	.long	.L1275-.L1277
	.long	.L1304-.L1277
	.long	.L1303-.L1277
	.long	.L1286-.L1277
	.long	.L1278-.L1277
	.long	.L1286-.L1277
	.long	.L1299-.L1277
	.long	.L1298-.L1277
	.long	.L1297-.L1277
	.long	.L1296-.L1277
	.long	.L1295-.L1277
	.long	.L1286-.L1277
	.long	.L1286-.L1277
	.long	.L1284-.L1277
	.long	.L1278-.L1277
	.long	.L1290-.L1277
	.long	.L1275-.L1277
	.long	.L1289-.L1277
	.long	.L1275-.L1277
	.long	.L1278-.L1277
	.long	.L1275-.L1277
	.long	.L1275-.L1277
	.long	.L1278-.L1277
	.long	.L1286-.L1277
	.long	.L1275-.L1277
	.long	.L1285-.L1277
	.long	.L1284-.L1277
	.long	.L1275-.L1277
	.long	.L1278-.L1277
	.long	.L1282-.L1277
	.long	.L1275-.L1277
	.long	.L1278-.L1277
	.long	.L1278-.L1277
	.long	.L1275-.L1277
	.long	.L1278-.L1277
	.long	.L1278-.L1277
	.long	.L1276-.L1277
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE10VisitBlockEPNS0_5BlockE
	.p2align 4,,10
	.p2align 3
.L1429:
	subl	$1, 24(%r15)
	movb	$1, 8(%r15)
.L1268:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1430
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1428:
	.cfi_restore_state
	subl	$1, 24(%r15)
.L1373:
	movl	20(%r12), %eax
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jg	.L1270
	jmp	.L1268
	.p2align 4,,10
	.p2align 3
.L1431:
	addq	$1, %rbx
	cmpl	%ebx, 20(%r12)
	jle	.L1268
.L1270:
	movq	8(%r12), %rax
	movq	%r15, %rdi
	movq	(%rax,%rbx,8), %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r15)
	je	.L1431
	jmp	.L1268
	.p2align 4,,10
	.p2align 3
.L1278:
	addl	$1, 24(%r15)
	movq	8(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r15)
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1286:
	addl	$1, 24(%r15)
	movq	8(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r15)
	jne	.L1322
.L1413:
	movq	16(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r15)
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1315:
	movq	32(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r15)
	je	.L1329
	.p2align 4,,10
	.p2align 3
.L1416:
	movl	24(%r15), %eax
	jmp	.L1322
	.p2align 4,,10
	.p2align 3
.L1309:
	movq	8(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1284:
	addl	$1, 24(%r15)
	movq	8(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r15)
	jne	.L1322
	movq	16(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r15)
	jne	.L1322
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r15)
	jmp	.L1275
.L1307:
	movq	8(%r13), %rsi
.L1422:
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r15)
	jne	.L1416
.L1343:
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	jmp	.L1275
.L1306:
	movq	8(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r15)
	jne	.L1416
.L1344:
	movq	16(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	jmp	.L1275
.L1310:
	movq	8(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r15)
	jne	.L1416
.L1308:
	movq	16(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r15)
	jne	.L1416
	jmp	.L1343
.L1317:
	movq	32(%r13), %rsi
	testq	%rsi, %rsi
	je	.L1324
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r15)
	jne	.L1416
.L1324:
	movq	40(%r13), %rsi
	testq	%rsi, %rsi
	je	.L1325
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r15)
	jne	.L1416
.L1325:
	movq	48(%r13), %rsi
	testq	%rsi, %rsi
	jne	.L1422
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1361:
	movq	(%rax), %rax
	addq	$16, %rax
	cmpq	%rax, -80(%rbp)
	je	.L1432
.L1363:
	movq	(%rax), %rsi
	movq	%r15, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r15)
	movq	-72(%rbp), %rax
	je	.L1361
	jmp	.L1366
.L1299:
	addl	$1, 24(%r15)
	movq	8(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r15)
	movl	%eax, 24(%r15)
	jne	.L1322
	movq	24(%r13), %rdi
	cmpq	%rdi, 32(%r13)
	je	.L1370
	xorl	%r8d, %r8d
	jmp	.L1351
	.p2align 4,,10
	.p2align 3
.L1433:
	movq	-72(%rbp), %r8
	movq	32(%r13), %rdx
	subq	24(%r13), %rdx
	addq	$1, %r8
	sarq	$4, %rdx
	cmpq	%r8, %rdx
	jbe	.L1370
.L1351:
	addl	$1, %eax
	movq	%r15, %rdi
	movq	%r8, -72(%rbp)
	movl	%eax, 24(%r15)
	movq	%r8, %rax
	salq	$4, %rax
	addq	24(%r13), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r15)
	movl	%eax, 24(%r15)
	je	.L1433
	jmp	.L1322
.L1298:
	addl	$1, 24(%r15)
	movq	8(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r15)
	movl	%eax, 24(%r15)
	jne	.L1322
	movl	28(%r13), %r8d
	testl	%r8d, %r8d
	jle	.L1370
	xorl	%edx, %edx
	jmp	.L1352
	.p2align 4,,10
	.p2align 3
.L1434:
	movq	-72(%rbp), %rdx
	addq	$1, %rdx
	cmpl	%edx, 28(%r13)
	jle	.L1370
.L1352:
	movq	16(%r13), %rsi
	addl	$1, %eax
	movq	%r15, %rdi
	movq	%rdx, -72(%rbp)
	movq	(%rsi,%rdx,8), %rsi
	movl	%eax, 24(%r15)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r15)
	movl	%eax, 24(%r15)
	je	.L1434
	jmp	.L1322
.L1297:
	addl	$1, 24(%r15)
	movq	8(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r15)
	movl	%eax, 24(%r15)
	jne	.L1322
	movl	28(%r13), %edi
	testl	%edi, %edi
	jle	.L1370
	xorl	%edx, %edx
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1435:
	movq	-72(%rbp), %rdx
	addq	$1, %rdx
	cmpl	%edx, 28(%r13)
	jle	.L1370
.L1353:
	movq	16(%r13), %rsi
	addl	$1, %eax
	movq	%r15, %rdi
	movq	%rdx, -72(%rbp)
	movq	(%rsi,%rdx,8), %rsi
	movl	%eax, 24(%r15)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r15)
	movl	%eax, 24(%r15)
	je	.L1435
	jmp	.L1322
.L1296:
	movl	36(%r13), %esi
	testl	%esi, %esi
	jle	.L1275
	movl	24(%r15), %eax
	xorl	%edx, %edx
	jmp	.L1354
	.p2align 4,,10
	.p2align 3
.L1436:
	movq	-72(%rbp), %rdx
	addq	$1, %rdx
	cmpl	%edx, 36(%r13)
	jle	.L1370
.L1354:
	movq	24(%r13), %rsi
	addl	$1, %eax
	movq	%r15, %rdi
	movq	%rdx, -72(%rbp)
	movq	(%rsi,%rdx,8), %rsi
	movl	%eax, 24(%r15)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r15)
	movl	%eax, 24(%r15)
	je	.L1436
	jmp	.L1322
.L1313:
	movq	16(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r15)
	jne	.L1416
	movl	36(%r13), %edi
	testl	%edi, %edi
	jle	.L1370
	movq	$0, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L1339:
	movq	24(%r13), %rax
	movq	-88(%rbp), %rcx
	movq	(%rax,%rcx,8), %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L1437
	movq	%r15, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r15)
	movq	-72(%rbp), %rdx
	jne	.L1416
	movl	20(%rdx), %eax
	testl	%eax, %eax
	jle	.L1411
.L1334:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1338:
	movq	8(%rdx), %rsi
	movq	%r15, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	(%rsi,%rax,8), %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r15)
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rdx
	jne	.L1416
	addq	$1, %rax
	cmpl	%eax, 20(%rdx)
	jg	.L1338
.L1411:
	movl	36(%r13), %edi
.L1335:
	addq	$1, -88(%rbp)
	movq	-88(%rbp), %rax
	cmpl	%eax, %edi
	jg	.L1339
	jmp	.L1370
.L1314:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE10VisitBlockEPNS0_5BlockE
	jmp	.L1275
.L1290:
	movq	8(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE10VisitBlockEPNS0_5BlockE
	jmp	.L1275
.L1319:
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r15)
	jne	.L1416
	movq	32(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	jmp	.L1275
.L1318:
	movq	32(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r15)
	jne	.L1416
	jmp	.L1343
.L1295:
	movl	24(%r15), %eax
	cmpq	$0, 32(%r13)
	leal	1(%rax), %edx
	je	.L1355
	movl	%edx, 24(%r15)
	movq	32(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %edx
	cmpb	$0, 8(%r15)
	leal	-1(%rdx), %eax
	movl	%eax, 24(%r15)
	jne	.L1322
.L1355:
	movl	%edx, 24(%r15)
	movq	40(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %edx
	cmpb	$0, 8(%r15)
	leal	-1(%rdx), %eax
	movl	%eax, 24(%r15)
	jne	.L1322
	cmpq	$0, 56(%r13)
	je	.L1356
	movl	%edx, 24(%r15)
	movq	56(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %edx
	cmpb	$0, 8(%r15)
	leal	-1(%rdx), %eax
	movl	%eax, 24(%r15)
	jne	.L1322
.L1356:
	cmpq	$0, 64(%r13)
	je	.L1357
	movl	%edx, 24(%r15)
	movq	64(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r15)
	movl	%eax, 24(%r15)
	jne	.L1322
.L1357:
	movq	48(%r13), %rdi
	movl	12(%rdi), %ecx
	movq	%rdi, -72(%rbp)
	testl	%ecx, %ecx
	jle	.L1275
	xorl	%r13d, %r13d
	jmp	.L1359
	.p2align 4,,10
	.p2align 3
.L1438:
	movq	-72(%rbp), %rdi
	addq	$1, %r13
	cmpl	%r13d, 12(%rdi)
	jle	.L1370
.L1359:
	movq	-72(%rbp), %rdi
	movq	(%rdi), %rsi
	movq	(%rsi,%r13,8), %r8
	leal	1(%rax), %esi
	movq	(%r8), %rax
	andq	$-4, %rax
	movl	4(%rax), %eax
	andl	$63, %eax
	cmpb	$41, %al
	je	.L1358
	movl	%esi, 24(%r15)
	movq	(%r8), %rsi
	movq	%r15, %rdi
	movq	%r8, -80(%rbp)
	andq	$-4, %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %esi
	cmpb	$0, 8(%r15)
	movq	-80(%rbp), %r8
	leal	-1(%rsi), %eax
	movl	%eax, 24(%r15)
	jne	.L1322
.L1358:
	movl	%esi, 24(%r15)
	movq	8(%r8), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r15)
	movl	%eax, 24(%r15)
	je	.L1438
	jmp	.L1322
.L1285:
	addl	$1, 24(%r15)
	jmp	.L1413
.L1305:
	movq	8(%r13), %rax
	xorl	%r13d, %r13d
	movl	12(%rax), %r11d
	movq	%rax, -72(%rbp)
	testl	%r11d, %r11d
	jg	.L1345
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1347:
	movq	-72(%rbp), %rax
	addq	$1, %r13
	cmpl	%r13d, 12(%rax)
	jle	.L1370
.L1345:
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %r8
	movq	(%r8), %rsi
	andq	$-4, %rsi
	movzbl	4(%rsi), %eax
	andl	$63, %eax
	cmpb	$41, %al
	je	.L1346
	movq	%r15, %rdi
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r15)
	movq	-80(%rbp), %r8
	jne	.L1416
.L1346:
	movq	8(%r8), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r15)
	je	.L1347
	jmp	.L1416
.L1304:
	movl	36(%r13), %r10d
	testl	%r10d, %r10d
	jle	.L1275
	movq	$0, -72(%rbp)
	movl	24(%r15), %eax
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1439:
	movq	-80(%rbp), %rdx
	movq	%r15, %rdi
	movq	8(%rdx), %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r15)
	movl	%eax, 24(%r15)
	jne	.L1322
	addq	$1, -72(%rbp)
	movq	-72(%rbp), %rcx
	cmpl	%ecx, 36(%r13)
	jle	.L1370
.L1349:
	movq	24(%r13), %rdx
	movq	-72(%rbp), %rcx
	addl	$1, %eax
	movq	%r15, %rdi
	movq	(%rdx,%rcx,8), %rdx
	movl	%eax, 24(%r15)
	movq	(%rdx), %rsi
	movq	%rdx, -80(%rbp)
	andq	$-4, %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r15)
	je	.L1439
	jmp	.L1322
.L1303:
	movl	36(%r13), %r9d
	testl	%r9d, %r9d
	jle	.L1275
	movl	24(%r15), %eax
	xorl	%edx, %edx
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1440:
	movq	-72(%rbp), %rdx
	addq	$1, %rdx
	cmpl	%edx, 36(%r13)
	jle	.L1370
.L1350:
	movq	24(%r13), %rsi
	addl	$1, %eax
	movq	%r15, %rdi
	movq	%rdx, -72(%rbp)
	movq	(%rsi,%rdx,8), %rsi
	movl	%eax, 24(%r15)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r15)
	movl	%eax, 24(%r15)
	je	.L1440
	jmp	.L1322
.L1282:
	movq	16(%r13), %rax
	movq	(%rax), %rdx
	movslq	12(%rax), %rax
	leaq	(%rdx,%rax,8), %rax
	movq	%rax, -72(%rbp)
	cmpq	%rax, %rdx
	je	.L1275
	movl	24(%r15), %eax
	movq	%rdx, %r13
	jmp	.L1369
	.p2align 4,,10
	.p2align 3
.L1441:
	addq	$8, %r13
	cmpq	%r13, -72(%rbp)
	je	.L1370
.L1369:
	addl	$1, %eax
	movq	0(%r13), %rsi
	movq	%r15, %rdi
	movl	%eax, 24(%r15)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r15), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r15)
	movl	%eax, 24(%r15)
	je	.L1441
	jmp	.L1322
.L1276:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1437:
	movl	20(%rdx), %eax
	testl	%eax, %eax
	jg	.L1334
	jmp	.L1335
.L1329:
	movq	40(%r13), %rsi
	jmp	.L1422
.L1271:
	movl	%esi, 24(%rdi)
	testb	%dl, %dl
	jne	.L1268
	jmp	.L1373
	.p2align 4,,10
	.p2align 3
.L1432:
	movl	24(%r15), %edx
	leal	-1(%rdx), %eax
	movl	%eax, 24(%r15)
	jmp	.L1364
.L1367:
	leaq	-64(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1275
.L1430:
	call	__stack_chk_fail@PLT
.L1427:
	movl	24(%r15), %eax
	subl	$1, %eax
.L1371:
	movl	%eax, 24(%r15)
	jmp	.L1268
	.cfi_endproc
.LFE26390:
	.size	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE10VisitBlockEPNS0_5BlockE, .-_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE10VisitBlockEPNS0_5BlockE
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE, @function
_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE:
.LFB24756:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 8(%rdi)
	je	.L1554
.L1442:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1555
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1554:
	.cfi_restore_state
	movq	%rdi, %r12
	movq	%rsi, %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L1556
	movzbl	4(%r13), %eax
	andl	$63, %eax
	cmpb	$57, %al
	ja	.L1442
	leaq	.L1447(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE,"a",@progbits
	.align 4
	.align 4
.L1447:
	.long	.L1442-.L1447
	.long	.L1549-.L1447
	.long	.L1480-.L1447
	.long	.L1479-.L1447
	.long	.L1478-.L1447
	.long	.L1476-.L1447
	.long	.L1476-.L1447
	.long	.L1475-.L1447
	.long	.L1474-.L1447
	.long	.L1471-.L1447
	.long	.L1442-.L1447
	.long	.L1547-.L1447
	.long	.L1472-.L1447
	.long	.L1442-.L1447
	.long	.L1442-.L1447
	.long	.L1471-.L1447
	.long	.L1470-.L1447
	.long	.L1469-.L1447
	.long	.L1468-.L1447
	.long	.L1442-.L1447
	.long	.L1467-.L1447
	.long	.L1442-.L1447
	.long	.L1466-.L1447
	.long	.L1465-.L1447
	.long	.L1452-.L1447
	.long	.L1448-.L1447
	.long	.L1452-.L1447
	.long	.L1462-.L1447
	.long	.L1461-.L1447
	.long	.L1460-.L1447
	.long	.L1459-.L1447
	.long	.L1458-.L1447
	.long	.L1452-.L1447
	.long	.L1452-.L1447
	.long	.L1450-.L1447
	.long	.L1448-.L1447
	.long	.L1454-.L1447
	.long	.L1442-.L1447
	.long	.L1453-.L1447
	.long	.L1442-.L1447
	.long	.L1448-.L1447
	.long	.L1442-.L1447
	.long	.L1442-.L1447
	.long	.L1448-.L1447
	.long	.L1452-.L1447
	.long	.L1442-.L1447
	.long	.L1451-.L1447
	.long	.L1450-.L1447
	.long	.L1442-.L1447
	.long	.L1448-.L1447
	.long	.L1449-.L1447
	.long	.L1442-.L1447
	.long	.L1448-.L1447
	.long	.L1448-.L1447
	.long	.L1442-.L1447
	.long	.L1448-.L1447
	.long	.L1448-.L1447
	.long	.L1446-.L1447
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L1448:
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1556:
	movb	$1, 8(%r12)
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1452:
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L1442
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1450:
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L1442
	movl	%eax, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L1442
	movl	%eax, 24(%r12)
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1476:
	movq	32(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L1442
	movq	40(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L1442
	.p2align 4,,10
	.p2align 3
.L1549:
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1471:
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	jmp	.L1442
.L1472:
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L1442
.L1470:
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L1442
	jmp	.L1549
.L1468:
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L1442
.L1547:
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	jmp	.L1442
.L1480:
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L1442
	movq	32(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	jmp	.L1442
.L1459:
	movl	36(%r13), %esi
	testl	%esi, %esi
	jle	.L1442
	movl	24(%r12), %eax
	xorl	%ebx, %ebx
	jmp	.L1504
	.p2align 4,,10
	.p2align 3
.L1557:
	addq	$1, %rbx
	cmpl	%ebx, 36(%r13)
	jle	.L1442
.L1504:
	movq	24(%r13), %rdx
	addl	$1, %eax
	movq	%r12, %rdi
	movq	(%rdx,%rbx,8), %rsi
	movl	%eax, 24(%r12)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 24(%r12)
	je	.L1557
	jmp	.L1442
.L1458:
	movl	24(%r12), %eax
	addl	$1, %eax
	cmpq	$0, 32(%r13)
	je	.L1505
	movl	%eax, 24(%r12)
	movq	32(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L1442
.L1505:
	movl	%eax, 24(%r12)
	movq	40(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %edx
	cmpb	$0, 8(%r12)
	leal	-1(%rdx), %eax
	movl	%eax, 24(%r12)
	jne	.L1442
	cmpq	$0, 56(%r13)
	je	.L1506
	movl	%edx, 24(%r12)
	movq	56(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %edx
	cmpb	$0, 8(%r12)
	leal	-1(%rdx), %eax
	movl	%eax, 24(%r12)
	jne	.L1442
.L1506:
	cmpq	$0, 64(%r13)
	je	.L1507
	movl	%edx, 24(%r12)
	movq	64(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 24(%r12)
	jne	.L1442
.L1507:
	movq	48(%r13), %rbx
	movl	12(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L1442
	xorl	%r13d, %r13d
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1558:
	addq	$1, %r13
	cmpl	%r13d, 12(%rbx)
	jle	.L1442
.L1509:
	movq	(%rbx), %rdx
	addl	$1, %eax
	movq	(%rdx,%r13,8), %r14
	movq	(%r14), %rdx
	andq	$-4, %rdx
	movl	4(%rdx), %edx
	andl	$63, %edx
	cmpb	$41, %dl
	je	.L1508
	movl	%eax, 24(%r12)
	movq	(%r14), %rsi
	movq	%r12, %rdi
	andq	$-4, %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L1442
.L1508:
	movl	%eax, 24(%r12)
	movq	8(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 24(%r12)
	je	.L1558
	jmp	.L1442
.L1465:
	movl	36(%r13), %r9d
	testl	%r9d, %r9d
	jle	.L1442
	movl	24(%r12), %eax
	xorl	%ebx, %ebx
	jmp	.L1500
	.p2align 4,,10
	.p2align 3
.L1559:
	addq	$1, %rbx
	cmpl	%ebx, 36(%r13)
	jle	.L1442
.L1500:
	movq	24(%r13), %rdx
	addl	$1, %eax
	movq	%r12, %rdi
	movq	(%rdx,%rbx,8), %rsi
	movl	%eax, 24(%r12)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 24(%r12)
	je	.L1559
	jmp	.L1442
.L1449:
	movq	16(%r13), %rax
	movq	(%rax), %rbx
	movslq	12(%rax), %rax
	leaq	(%rbx,%rax,8), %r13
	cmpq	%r13, %rbx
	je	.L1442
	movl	24(%r12), %eax
	jmp	.L1519
	.p2align 4,,10
	.p2align 3
.L1560:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L1442
.L1519:
	addl	$1, %eax
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	movl	%eax, 24(%r12)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 24(%r12)
	je	.L1560
	jmp	.L1442
.L1461:
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 24(%r12)
	jne	.L1442
	movl	28(%r13), %r8d
	testl	%r8d, %r8d
	jle	.L1442
	xorl	%ebx, %ebx
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1561:
	addq	$1, %rbx
	cmpl	%ebx, 28(%r13)
	jle	.L1442
.L1502:
	movq	16(%r13), %rdx
	addl	$1, %eax
	movq	%r12, %rdi
	movq	(%rdx,%rbx,8), %rsi
	movl	%eax, 24(%r12)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 24(%r12)
	je	.L1561
	jmp	.L1442
.L1460:
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 24(%r12)
	jne	.L1442
	movl	28(%r13), %edi
	testl	%edi, %edi
	jle	.L1442
	xorl	%ebx, %ebx
	jmp	.L1503
	.p2align 4,,10
	.p2align 3
.L1562:
	addq	$1, %rbx
	cmpl	%ebx, 28(%r13)
	jle	.L1442
.L1503:
	movq	16(%r13), %rdx
	addl	$1, %eax
	movq	%r12, %rdi
	movq	(%rdx,%rbx,8), %rsi
	movl	%eax, 24(%r12)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 24(%r12)
	je	.L1562
	jmp	.L1442
.L1462:
	addl	$1, 24(%r12)
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 24(%r12)
	jne	.L1442
	movq	32(%r13), %rcx
	cmpq	%rcx, 24(%r13)
	je	.L1442
	xorl	%ebx, %ebx
	jmp	.L1501
	.p2align 4,,10
	.p2align 3
.L1563:
	movq	32(%r13), %rdx
	subq	24(%r13), %rdx
	addq	$1, %rbx
	sarq	$4, %rdx
	cmpq	%rbx, %rdx
	jbe	.L1442
.L1501:
	addl	$1, %eax
	movq	%r12, %rdi
	movl	%eax, 24(%r12)
	movq	%rbx, %rax
	salq	$4, %rax
	addq	24(%r13), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 24(%r12)
	je	.L1563
	jmp	.L1442
.L1453:
	movq	40(%r13), %rax
	addl	$1, 24(%r12)
	movq	%r13, -64(%rbp)
	movq	96(%rax), %r14
	leaq	88(%rax), %rbx
	cmpq	%r14, %rbx
	jne	.L1513
	jmp	.L1510
	.p2align 4,,10
	.p2align 3
.L1511:
	movq	(%rbx), %rbx
	addq	$16, %rbx
	cmpq	%rbx, %r14
	je	.L1564
.L1513:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	je	.L1511
.L1516:
	subl	$1, 24(%r12)
.L1512:
	movq	32(%r12), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L1517
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 8(%rdi)
	jmp	.L1442
.L1454:
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE10VisitBlockEPNS0_5BlockE
	jmp	.L1442
.L1479:
	movq	32(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L1442
	jmp	.L1549
.L1478:
	movq	32(%r13), %rsi
	testq	%rsi, %rsi
	je	.L1485
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L1442
.L1485:
	movq	40(%r13), %rsi
	testq	%rsi, %rsi
	je	.L1484
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L1442
.L1484:
	movq	48(%r13), %rsi
	testq	%rsi, %rsi
	je	.L1549
.L1553:
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	je	.L1549
	jmp	.L1442
.L1475:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE10VisitBlockEPNS0_5BlockE
	jmp	.L1442
.L1474:
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L1442
	movl	36(%r13), %r14d
	testl	%r14d, %r14d
	jle	.L1442
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L1493:
	movq	24(%r13), %rax
	movq	(%rax,%r15,8), %r14
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	je	.L1565
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L1442
	movl	20(%r14), %eax
	testl	%eax, %eax
	jle	.L1494
.L1489:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1492:
	movq	8(%r14), %rax
	movq	%r12, %rdi
	movq	(%rax,%rbx,8), %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L1442
	addq	$1, %rbx
	cmpl	%ebx, 20(%r14)
	jg	.L1492
.L1494:
	addq	$1, %r15
	cmpl	%r15d, 36(%r13)
	jg	.L1493
	jmp	.L1442
.L1469:
	movq	8(%r13), %rsi
	jmp	.L1553
.L1467:
	movq	8(%r13), %r13
	movl	12(%r13), %r11d
	testl	%r11d, %r11d
	jle	.L1442
	xorl	%ebx, %ebx
	jmp	.L1498
	.p2align 4,,10
	.p2align 3
.L1540:
	addq	$1, %rbx
	cmpl	%ebx, 12(%r13)
	jle	.L1442
.L1498:
	movq	0(%r13), %rax
	movq	(%rax,%rbx,8), %r14
	movq	(%r14), %rsi
	andq	$-4, %rsi
	movzbl	4(%rsi), %eax
	andl	$63, %eax
	cmpb	$41, %al
	je	.L1497
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L1442
.L1497:
	movq	8(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	je	.L1540
	jmp	.L1442
.L1466:
	movl	36(%r13), %r10d
	testl	%r10d, %r10d
	jle	.L1442
	movl	24(%r12), %eax
	xorl	%ebx, %ebx
	jmp	.L1499
	.p2align 4,,10
	.p2align 3
.L1566:
	movl	%eax, 24(%r12)
	movq	8(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 24(%r12)
	jne	.L1442
	addq	$1, %rbx
	cmpl	%ebx, 36(%r13)
	jle	.L1442
.L1499:
	movq	24(%r13), %rdx
	addl	$1, %eax
	movq	%r12, %rdi
	movq	(%rdx,%rbx,8), %r14
	movl	%eax, 24(%r12)
	movq	(%r14), %rsi
	andq	$-4, %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	je	.L1566
	jmp	.L1442
.L1451:
	addl	$1, 24(%r12)
	movq	16(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	subl	$1, 24(%r12)
	jmp	.L1442
.L1446:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1565:
	movl	20(%r14), %ebx
	testl	%ebx, %ebx
	jg	.L1489
	cmpb	$0, 8(%r12)
	je	.L1494
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1564:
	movl	24(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
.L1514:
	movq	40(%r13), %rdx
	cmpb	$0, 131(%rdx)
	js	.L1512
	movl	%eax, 24(%r12)
	movl	60(%r13), %edx
	xorl	%ebx, %ebx
	testl	%edx, %edx
	jg	.L1515
	jmp	.L1516
	.p2align 4,,10
	.p2align 3
.L1567:
	addq	$1, %rbx
	cmpl	%ebx, 60(%r13)
	jle	.L1516
.L1515:
	movq	48(%r13), %rax
	movq	%r12, %rdi
	movq	(%rax,%rbx,8), %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	je	.L1567
	jmp	.L1516
.L1510:
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	je	.L1514
	jmp	.L1512
	.p2align 4,,10
	.p2align 3
.L1517:
	leaq	-64(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1442
.L1555:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24756:
	.size	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE, .-_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
	.section	.text._ZN2v88internal12_GLOBAL__N_111ParseScriptEPNS0_7IsolateEPNS0_9ParseInfoEbPSt6vectorIPNS0_15FunctionLiteralESaIS8_EEPNS_5debug14LiveEditResultE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_111ParseScriptEPNS0_7IsolateEPNS0_9ParseInfoEbPSt6vectorIPNS0_15FunctionLiteralESaIS8_EEPNS_5debug14LiveEditResultE, @function
_ZN2v88internal12_GLOBAL__N_111ParseScriptEPNS0_7IsolateEPNS0_9ParseInfoEbPSt6vectorIPNS0_15FunctionLiteralESaIS8_EEPNS_5debug14LiveEditResultE:
.LFB20508:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%rcx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	orl	$2, 8(%rsi)
	movq	%rdi, %rsi
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	testb	%r15b, %r15b
	je	.L1569
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8Compiler18CompileForLiveEditEPNS0_9ParseInfoEPNS0_7IsolateE@PLT
	testq	%rax, %rax
	je	.L1571
.L1572:
	movq	37528(%r12), %rax
	movl	$1, %r12d
	movq	168(%rbx), %rsi
	leaq	-160(%rbp), %rdi
	movl	$0, -136(%rbp)
	movq	%rax, -160(%rbp)
	movq	-168(%rbp), %rax
	movq	%rsi, -144(%rbp)
	movb	$0, -152(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal19AstTraversalVisitorINS0_12_GLOBAL__N_123CollectFunctionLiteralsEE5VisitEPNS0_7AstNodeE
.L1575:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1578
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1569:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7parsing12ParseProgramEPNS0_9ParseInfoEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE@PLT
	testb	%al, %al
	jne	.L1579
.L1571:
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-160(%rbp), %r12
	call	_ZN2v88internal7Isolate27OptionalRescheduleExceptionEb@PLT
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch7MessageEv@PLT
	movq	%rax, %rdi
	call	_ZNK2v87Message3GetEv@PLT
	movq	%r14, %rdi
	movq	%rax, 16(%r13)
	call	_ZNK2v88TryCatch7MessageEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal15JSMessageObject13GetLineNumberEv@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	%eax, 24(%r13)
	movq	(%rbx), %rax
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal15JSMessageObject15GetColumnNumberEv@PLT
	movl	$1, 0(%r13)
	movl	%eax, 28(%r13)
	jmp	.L1575
	.p2align 4,,10
	.p2align 3
.L1579:
	movq	%rbx, %rdi
	call	_ZN2v88internal8Compiler7AnalyzeEPNS0_9ParseInfoE@PLT
	movq	112(%rbx), %rdi
	movq	%r12, %rsi
	movb	%al, -169(%rbp)
	call	_ZN2v88internal15AstValueFactory11InternalizeEPNS0_7IsolateE@PLT
	movzbl	-169(%rbp), %eax
	testb	%al, %al
	jne	.L1572
	jmp	.L1571
.L1578:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20508:
	.size	_ZN2v88internal12_GLOBAL__N_111ParseScriptEPNS0_7IsolateEPNS0_9ParseInfoEbPSt6vectorIPNS0_15FunctionLiteralESaIS8_EEPNS_5debug14LiveEditResultE, .-_ZN2v88internal12_GLOBAL__N_111ParseScriptEPNS0_7IsolateEPNS0_9ParseInfoEbPSt6vectorIPNS0_15FunctionLiteralESaIS8_EEPNS_5debug14LiveEditResultE
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_10JSFunctionEEESaIS4_EE17_M_realloc_insertIJRS3_RPNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_10JSFunctionEEESaIS4_EE17_M_realloc_insertIJRS3_RPNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_10JSFunctionEEESaIS4_EE17_M_realloc_insertIJRS3_RPNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_10JSFunctionEEESaIS4_EE17_M_realloc_insertIJRS3_RPNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal6HandleINS1_10JSFunctionEEESaIS4_EE17_M_realloc_insertIJRS3_RPNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB24799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movabsq	$1152921504606846975, %rsi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r8
	movq	(%rdi), %r12
	movq	%r8, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L1610
	movq	%r15, %r10
	movq	%rdi, %r13
	subq	%r12, %r10
	testq	%rax, %rax
	je	.L1595
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1611
.L1582:
	movq	%r14, %rdi
	movq	%rcx, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %r10
	movq	%rax, %rbx
	leaq	(%rax,%r14), %rax
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rcx
	movq	%rax, -56(%rbp)
	leaq	8(%rbx), %r14
.L1594:
	movq	(%rcx), %rcx
	movq	(%rdx), %rsi
	addq	%rbx, %r10
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L1584
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
.L1585:
	movq	%rax, (%r10)
	cmpq	%r12, %r15
	je	.L1587
	leaq	-8(%r15), %rcx
	leaq	15(%rbx), %rax
	subq	%r12, %rcx
	subq	%r12, %rax
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rax
	jbe	.L1597
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L1597
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1589:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1589
	movq	%rsi, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%rdi, %rsi
	je	.L1591
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L1591:
	leaq	16(%rbx,%rcx), %r14
.L1587:
	cmpq	%r8, %r15
	je	.L1592
	subq	%r15, %r8
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r8, %rdx
	movq	%r8, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	addq	%r8, %r14
.L1592:
	testq	%r12, %r12
	je	.L1593
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1593:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r14, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r13)
	movups	%xmm0, 0(%r13)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1611:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L1583
	movq	$0, -56(%rbp)
	movl	$8, %r14d
	xorl	%ebx, %ebx
	jmp	.L1594
	.p2align 4,,10
	.p2align 3
.L1584:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L1612
.L1586:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L1585
	.p2align 4,,10
	.p2align 3
.L1595:
	movl	$8, %r14d
	jmp	.L1582
	.p2align 4,,10
	.p2align 3
.L1597:
	movq	%rbx, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L1588:
	movq	(%rax), %rsi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rsi, -8(%rdx)
	cmpq	%rax, %r15
	jne	.L1588
	jmp	.L1591
.L1612:
	movq	%rcx, %rdi
	movq	%r8, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %rcx
	jmp	.L1586
.L1583:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	movq	%rsi, %r14
	salq	$3, %r14
	jmp	.L1582
.L1610:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24799:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_10JSFunctionEEESaIS4_EE17_M_realloc_insertIJRS3_RPNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal6HandleINS1_10JSFunctionEEESaIS4_EE17_M_realloc_insertIJRS3_RPNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_17JSGeneratorObjectEEESaIS4_EE17_M_realloc_insertIJRS3_RPNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_17JSGeneratorObjectEEESaIS4_EE17_M_realloc_insertIJRS3_RPNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_17JSGeneratorObjectEEESaIS4_EE17_M_realloc_insertIJRS3_RPNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_17JSGeneratorObjectEEESaIS4_EE17_M_realloc_insertIJRS3_RPNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal6HandleINS1_17JSGeneratorObjectEEESaIS4_EE17_M_realloc_insertIJRS3_RPNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB24805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movabsq	$1152921504606846975, %rsi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r8
	movq	(%rdi), %r12
	movq	%r8, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L1643
	movq	%r15, %r10
	movq	%rdi, %r13
	subq	%r12, %r10
	testq	%rax, %rax
	je	.L1628
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1644
.L1615:
	movq	%r14, %rdi
	movq	%rcx, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %r10
	movq	%rax, %rbx
	leaq	(%rax,%r14), %rax
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rcx
	movq	%rax, -56(%rbp)
	leaq	8(%rbx), %r14
.L1627:
	movq	(%rcx), %rcx
	movq	(%rdx), %rsi
	addq	%rbx, %r10
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L1617
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
.L1618:
	movq	%rax, (%r10)
	cmpq	%r12, %r15
	je	.L1620
	leaq	-8(%r15), %rcx
	leaq	15(%rbx), %rax
	subq	%r12, %rcx
	subq	%r12, %rax
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rax
	jbe	.L1630
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L1630
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1622:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1622
	movq	%rsi, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%rdi, %rsi
	je	.L1624
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L1624:
	leaq	16(%rbx,%rcx), %r14
.L1620:
	cmpq	%r8, %r15
	je	.L1625
	subq	%r15, %r8
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r8, %rdx
	movq	%r8, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	addq	%r8, %r14
.L1625:
	testq	%r12, %r12
	je	.L1626
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1626:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r14, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r13)
	movups	%xmm0, 0(%r13)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1644:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L1616
	movq	$0, -56(%rbp)
	movl	$8, %r14d
	xorl	%ebx, %ebx
	jmp	.L1627
	.p2align 4,,10
	.p2align 3
.L1617:
	movq	41088(%rcx), %rax
	cmpq	41096(%rcx), %rax
	je	.L1645
.L1619:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rcx)
	movq	%rsi, (%rax)
	jmp	.L1618
	.p2align 4,,10
	.p2align 3
.L1628:
	movl	$8, %r14d
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1630:
	movq	%rbx, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L1621:
	movq	(%rax), %rsi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rsi, -8(%rdx)
	cmpq	%rax, %r15
	jne	.L1621
	jmp	.L1624
.L1645:
	movq	%rcx, %rdi
	movq	%r8, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %rcx
	jmp	.L1619
.L1616:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	movq	%rsi, %r14
	salq	$3, %r14
	jmp	.L1615
.L1643:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24805:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_17JSGeneratorObjectEEESaIS4_EE17_M_realloc_insertIJRS3_RPNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal6HandleINS1_17JSGeneratorObjectEEESaIS4_EE17_M_realloc_insertIJRS3_RPNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZN2v88internal12_GLOBAL__N_115FunctionDataMap4FillEPNS0_7IsolateEPm.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_115FunctionDataMap4FillEPNS0_7IsolateEPm.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_115FunctionDataMap4FillEPNS0_7IsolateEPm.constprop.0:
.LFB28188:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1504(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1576, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -1584(%rbp)
	movq	%r15, %rdi
	movq	%rsi, -1560(%rbp)
	addq	$37592, %rsi
	movq	%rdx, -1576(%rbp)
	movl	$1, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18HeapObjectIteratorC1EPNS0_4HeapENS1_20HeapObjectsFilteringE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %r14
	leaq	16(%r12), %rax
	movq	%rax, -1568(%rbp)
	testq	%r14, %r14
	je	.L1702
	leaq	-1536(%rbp), %r13
	leaq	-1544(%rbp), %rbx
	movq	%r13, -1592(%rbp)
	jmp	.L1647
	.p2align 4,,10
	.p2align 3
.L1650:
	movq	-1(%r14), %rax
	cmpw	$1105, 11(%rax)
	je	.L1804
	movq	(%rdx), %rax
	cmpw	$1068, 11(%rax)
	je	.L1686
	movq	(%rdx), %rax
	cmpw	$1063, 11(%rax)
	je	.L1686
	movq	(%rdx), %rax
	cmpw	$1064, 11(%rax)
	je	.L1686
	.p2align 4,,10
	.p2align 3
.L1653:
	movq	%r15, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1702
.L1647:
	movq	-1(%r14), %rax
	leaq	-1(%r14), %rdx
	cmpw	$160, 11(%rax)
	jne	.L1650
	movq	%rbx, %rdi
	movq	%r14, -1544(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-1544(%rbp), %rcx
	movq	31(%rcx), %rdx
	testb	$1, %dl
	je	.L1653
	movq	-1(%rdx), %rdi
	leaq	-1(%rdx), %rsi
	cmpw	$86, 11(%rdi)
	je	.L1805
.L1652:
	movq	(%rsi), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L1653
	cmpl	$-1, %eax
	je	.L1653
	movq	31(%rcx), %rax
	testb	$1, %al
	jne	.L1806
.L1655:
	movslq	67(%rax), %rsi
	movq	-1592(%rbp), %rdi
	movq	-1544(%rbp), %rax
	movq	%rsi, -1600(%rbp)
	movl	%esi, %r12d
	movq	%rax, -1536(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-1536(%rbp), %rdx
	movl	$-1, %ecx
	movl	47(%rdx), %edx
	andl	$268435456, %edx
	cmove	%eax, %ecx
	movq	-1584(%rbp), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1653
	movq	-1568(%rbp), %r13
	movq	-1600(%rbp), %rsi
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1807:
	jne	.L1661
	cmpl	%ecx, 36(%rax)
	jl	.L1660
.L1661:
	movq	%rax, %r13
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1659
.L1658:
	cmpl	32(%rax), %r12d
	jle	.L1807
.L1660:
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1658
.L1659:
	cmpq	%r13, -1568(%rbp)
	je	.L1653
	cmpl	32(%r13), %esi
	jl	.L1653
	je	.L1808
.L1663:
	movq	-1560(%rbp), %r12
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1809
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1664:
	movq	%rax, 48(%r13)
	movq	%r15, %rdi
	call	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L1647
.L1702:
	movq	%r15, %rdi
	xorl	%ebx, %ebx
	call	_ZN2v88internal18HeapObjectIteratorD1Ev@PLT
	movq	-1560(%rbp), %rsi
	movq	%r15, %rdi
	movq	41472(%rsi), %rax
	movl	72(%rax), %eax
	testl	%eax, %eax
	sete	%bl
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-1584(%rbp), %rax
	movq	-88(%rbp), %r14
	addl	$1, %ebx
	addq	$16, %rax
	movq	%rax, -1592(%rbp)
	testq	%r14, %r14
	je	.L1732
	movq	%r15, -1568(%rbp)
	.p2align 4,,10
	.p2align 3
.L1703:
	movq	(%r14), %rax
	cmpl	$1, %ebx
	je	.L1810
	cmpl	$2, %ebx
	jne	.L1742
	movq	%r14, %rdi
	call	*8(%rax)
	movl	%eax, %ebx
	cmpl	$3, %eax
	je	.L1708
.L1827:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	cmpl	$21, %eax
	je	.L1709
	movq	(%r14), %rax
	movl	$2, %ebx
.L1706:
	movq	%r14, %rdi
	call	*8(%rax)
	cmpl	$20, %eax
	ja	.L1712
	movl	$1150992, %edx
	btq	%rax, %rdx
	jnc	.L1712
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	-1536(%rbp), %rsi
	movq	$0, -1520(%rbp)
	movaps	%xmm0, -1536(%rbp)
	call	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_6HandleINS0_18SharedFunctionInfoEEESaIS5_EE@PLT
	movq	-1536(%rbp), %rdx
	movq	-1528(%rbp), %r12
	cmpq	%r12, %rdx
	je	.L1713
	leaq	-1544(%rbp), %rax
	movq	%rdx, %r15
	leaq	-1552(%rbp), %r13
	movq	%rax, -1600(%rbp)
	jmp	.L1730
	.p2align 4,,10
	.p2align 3
.L1719:
	addq	$8, %r15
	cmpq	%r15, %r12
	je	.L1811
.L1730:
	movq	(%r15), %rcx
	movq	(%rcx), %rax
	cmpl	$2, %ebx
	je	.L1812
.L1714:
	movq	%r13, %rdi
	movq	%rax, -1552(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-1552(%rbp), %rsi
	movq	31(%rsi), %rcx
	testb	$1, %cl
	je	.L1719
	movq	-1(%rcx), %r8
	leaq	-1(%rcx), %rdi
	cmpw	$86, 11(%r8)
	je	.L1813
.L1718:
	movq	(%rdi), %rcx
	cmpw	$96, 11(%rcx)
	jne	.L1719
	cmpl	$-1, %eax
	je	.L1719
	movq	31(%rsi), %rax
	testb	$1, %al
	jne	.L1814
.L1721:
	movslq	67(%rax), %r9
	movq	-1600(%rbp), %rdi
	movq	-1552(%rbp), %rax
	movq	%r9, -1616(%rbp)
	movl	%r9d, -1604(%rbp)
	movq	%rax, -1544(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-1544(%rbp), %rcx
	movl	$-1, %edx
	movl	47(%rcx), %ecx
	andl	$268435456, %ecx
	cmovne	%edx, %eax
	movq	-1584(%rbp), %rdx
	movq	24(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L1719
	movq	-1592(%rbp), %rsi
	movl	-1604(%rbp), %r8d
	movq	-1616(%rbp), %r9
	jmp	.L1724
	.p2align 4,,10
	.p2align 3
.L1815:
	jne	.L1727
	cmpl	%eax, 36(%rcx)
	jl	.L1726
.L1727:
	movq	%rcx, %rsi
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1725
.L1724:
	cmpl	32(%rcx), %r8d
	jle	.L1815
.L1726:
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L1724
.L1725:
	cmpq	%rsi, -1592(%rbp)
	je	.L1719
	cmpl	32(%rsi), %r9d
	jl	.L1719
	je	.L1816
.L1729:
	cmpb	$0, 108(%rsi)
	je	.L1719
	movl	%ebx, 104(%rsi)
	movq	-1576(%rbp), %rdx
	addq	$8, %r15
	movq	32(%r14), %rax
	movq	%rax, (%rdx)
	cmpq	%r15, %r12
	jne	.L1730
.L1811:
	movq	-1536(%rbp), %r12
.L1713:
	testq	%r12, %r12
	je	.L1712
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1712:
	movq	-1568(%rbp), %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %r14
	testq	%r14, %r14
	jne	.L1703
.L1732:
	movq	-1560(%rbp), %rax
	movq	-1584(%rbp), %rsi
	movq	41168(%rax), %rdi
	call	_ZN2v88internal13ThreadManager22IterateArchivedThreadsEPNS0_13ThreadVisitorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1817
	addq	$1576, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1686:
	.cfi_restore_state
	movq	%r14, -1552(%rbp)
	cmpl	$-1, 67(%r14)
	je	.L1653
	movq	23(%r14), %rax
	movq	%rbx, %rdi
	movq	23(%rax), %rax
	movq	%rax, -1544(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-1544(%rbp), %rcx
	movq	31(%rcx), %rdx
	testb	$1, %dl
	je	.L1653
	movq	-1(%rdx), %rdi
	leaq	-1(%rdx), %rsi
	cmpw	$86, 11(%rdi)
	je	.L1818
	movq	(%rsi), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L1653
.L1828:
	cmpl	$-1, %eax
	je	.L1653
	movq	31(%rcx), %rax
	testb	$1, %al
	jne	.L1819
.L1690:
	movslq	67(%rax), %r13
	movq	-1592(%rbp), %rdi
	movq	-1544(%rbp), %rax
	movl	%r13d, %r14d
	movq	%rax, -1536(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-1536(%rbp), %rdx
	movl	47(%rdx), %edx
	andl	$268435456, %edx
	movl	$-1, %edx
	cmove	%eax, %edx
	movq	-1584(%rbp), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1653
	movq	-1568(%rbp), %r12
	jmp	.L1693
	.p2align 4,,10
	.p2align 3
.L1820:
	jne	.L1696
	cmpl	%edx, 36(%rax)
	jl	.L1695
.L1696:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1694
.L1693:
	cmpl	32(%rax), %r14d
	jle	.L1820
.L1695:
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1693
.L1694:
	cmpq	-1568(%rbp), %r12
	je	.L1653
	cmpl	32(%r12), %r13d
	jl	.L1653
	je	.L1821
.L1698:
	movq	88(%r12), %r14
	cmpq	96(%r12), %r14
	je	.L1822
	movq	-1560(%rbp), %r13
	movq	-1552(%rbp), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1699
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1700:
	movq	%rax, (%r14)
	addq	$8, 88(%r12)
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1804:
	movq	%r14, -1552(%rbp)
	movq	23(%r14), %rax
	movq	%rbx, %rdi
	movq	%rax, -1544(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-1544(%rbp), %rcx
	movq	31(%rcx), %rdx
	testb	$1, %dl
	je	.L1653
	movq	-1(%rdx), %rdi
	leaq	-1(%rdx), %rsi
	cmpw	$86, 11(%rdi)
	je	.L1823
	movq	(%rsi), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L1653
.L1829:
	cmpl	$-1, %eax
	je	.L1653
	movq	31(%rcx), %rax
	testb	$1, %al
	jne	.L1824
.L1671:
	movslq	67(%rax), %r13
	movq	-1592(%rbp), %rdi
	movq	-1544(%rbp), %rax
	movl	%r13d, %r14d
	movq	%rax, -1536(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-1536(%rbp), %rdx
	movl	47(%rdx), %edx
	andl	$268435456, %edx
	movl	$-1, %edx
	cmove	%eax, %edx
	movq	-1584(%rbp), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1653
	movq	-1568(%rbp), %r12
	jmp	.L1674
	.p2align 4,,10
	.p2align 3
.L1825:
	jne	.L1677
	cmpl	%edx, 36(%rax)
	jl	.L1676
.L1677:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1675
.L1674:
	cmpl	32(%rax), %r14d
	jle	.L1825
.L1676:
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1674
.L1675:
	cmpq	%r12, -1568(%rbp)
	je	.L1653
	cmpl	32(%r12), %r13d
	jl	.L1653
	jne	.L1679
	cmpl	%edx, 36(%r12)
	jg	.L1653
.L1679:
	movq	64(%r12), %r14
	cmpq	72(%r12), %r14
	je	.L1826
	movq	-1560(%rbp), %r13
	movq	-1552(%rbp), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1680
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1681:
	movq	%rax, (%r14)
	addq	$8, 64(%r12)
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1812:
	movl	47(%rax), %esi
	andl	$31, %esi
	leal	-9(%rsi), %edi
	cmpb	$6, %dil
	jbe	.L1743
	cmpb	$1, %sil
	jne	.L1714
.L1743:
	movq	(%rcx), %rax
	movl	$3, %ebx
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L1810:
	movq	%r14, %rdi
	call	*56(%rax)
	movq	-1560(%rbp), %rdx
	movq	41472(%rdx), %rdx
	cmpl	%eax, 72(%rdx)
	movq	(%r14), %rax
	jne	.L1706
	movq	%r14, %rdi
	call	*8(%rax)
	movl	%eax, %ebx
	cmpl	$3, %eax
	jne	.L1827
.L1708:
	movq	-1568(%rbp), %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %r14
	testq	%r14, %r14
	je	.L1732
	movq	(%r14), %rax
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1742:
	movl	$3, %ebx
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1813:
	movq	23(%rcx), %rdi
	testb	$1, %dil
	je	.L1719
	subq	$1, %rdi
	jmp	.L1718
	.p2align 4,,10
	.p2align 3
.L1709:
	movq	-1568(%rbp), %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %r14
	testq	%r14, %r14
	je	.L1732
	movq	(%r14), %rax
	movl	$3, %ebx
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1818:
	movq	23(%rdx), %rsi
	testb	$1, %sil
	je	.L1653
	movq	-1(%rsi), %rdx
	subq	$1, %rsi
	cmpw	$96, 11(%rdx)
	jne	.L1653
	jmp	.L1828
	.p2align 4,,10
	.p2align 3
.L1823:
	movq	23(%rdx), %rsi
	testb	$1, %sil
	je	.L1653
	movq	-1(%rsi), %rdx
	subq	$1, %rsi
	cmpw	$96, 11(%rdx)
	jne	.L1653
	jmp	.L1829
	.p2align 4,,10
	.p2align 3
.L1814:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L1721
	movq	23(%rax), %rax
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1805:
	movq	23(%rdx), %rsi
	testb	$1, %sil
	je	.L1653
	subq	$1, %rsi
	jmp	.L1652
	.p2align 4,,10
	.p2align 3
.L1816:
	cmpl	%eax, 36(%rsi)
	jg	.L1719
	jmp	.L1729
.L1806:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L1655
	movq	23(%rax), %rax
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1809:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1830
.L1665:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r12)
	movq	%r14, (%rax)
	jmp	.L1664
.L1830:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1665
	.p2align 4,,10
	.p2align 3
.L1699:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L1831
.L1701:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L1700
	.p2align 4,,10
	.p2align 3
.L1822:
	leaq	-1560(%rbp), %rcx
	leaq	-1552(%rbp), %rdx
	movq	%r14, %rsi
	leaq	80(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_17JSGeneratorObjectEEESaIS4_EE17_M_realloc_insertIJRS3_RPNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L1653
.L1831:
	movq	%r13, %rdi
	movq	%rsi, -1600(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1600(%rbp), %rsi
	jmp	.L1701
.L1808:
	cmpl	%ecx, 36(%r13)
	jg	.L1653
	jmp	.L1663
.L1821:
	cmpl	%edx, 36(%r12)
	jg	.L1653
	jmp	.L1698
	.p2align 4,,10
	.p2align 3
.L1680:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L1832
.L1682:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L1681
	.p2align 4,,10
	.p2align 3
.L1826:
	leaq	-1560(%rbp), %rcx
	leaq	-1552(%rbp), %rdx
	movq	%r14, %rsi
	leaq	56(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_10JSFunctionEEESaIS4_EE17_M_realloc_insertIJRS3_RPNS1_7IsolateEEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L1653
.L1832:
	movq	%r13, %rdi
	movq	%rsi, -1600(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1600(%rbp), %rsi
	jmp	.L1682
.L1819:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L1690
	movq	23(%rax), %rax
	jmp	.L1690
.L1824:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L1671
	movq	23(%rax), %rax
	jmp	.L1671
.L1817:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28188:
	.size	_ZN2v88internal12_GLOBAL__N_115FunctionDataMap4FillEPNS0_7IsolateEPm.constprop.0, .-_ZN2v88internal12_GLOBAL__N_115FunctionDataMap4FillEPNS0_7IsolateEPm.constprop.0
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.type	_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, @function
_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E:
.LFB24898:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1841
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L1835:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1835
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1841:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE24898:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, .-_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.section	.text._ZNSt10_HashtableIPN2v88internal15FunctionLiteralESt4pairIKS3_S3_ESaIS6_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal15FunctionLiteralESt4pairIKS3_S3_ESaIS6_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal15FunctionLiteralESt4pairIKS3_S3_ESaIS6_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.type	_ZNSt10_HashtableIPN2v88internal15FunctionLiteralESt4pairIKS3_S3_ESaIS6_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm, @function
_ZNSt10_HashtableIPN2v88internal15FunctionLiteralESt4pairIKS3_S3_ESaIS6_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm:
.LFB25795:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L1845
	movq	(%rbx), %r8
.L1846:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L1855
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L1856:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1845:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L1869
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1870
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L1848:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1850
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L1851
	.p2align 4,,10
	.p2align 3
.L1852:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1853:
	testq	%rsi, %rsi
	je	.L1850
.L1851:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L1852
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L1858
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1851
	.p2align 4,,10
	.p2align 3
.L1850:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L1854
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1854:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L1846
	.p2align 4,,10
	.p2align 3
.L1855:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L1857
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L1857:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L1856
	.p2align 4,,10
	.p2align 3
.L1858:
	movq	%rdx, %rdi
	jmp	.L1853
	.p2align 4,,10
	.p2align 3
.L1869:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L1848
.L1870:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE25795:
	.size	_ZNSt10_HashtableIPN2v88internal15FunctionLiteralESt4pairIKS3_S3_ESaIS6_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm, .-_ZNSt10_HashtableIPN2v88internal15FunctionLiteralESt4pairIKS3_S3_ESaIS6_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.section	.text._ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_S4_ESaIS7_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_,"axG",@progbits,_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_S4_ESaIS7_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_S4_ESaIS7_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	.type	_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_S4_ESaIS7_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_, @function
_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_S4_ESaIS7_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_:
.LFB24737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %r13
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	%r13, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L1872
	movq	(%rax), %rcx
	movq	8(%rcx), %r8
	jmp	.L1874
	.p2align 4,,10
	.p2align 3
.L1884:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1872
	movq	8(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L1872
.L1874:
	cmpq	%r8, %r13
	jne	.L1884
	popq	%rbx
	leaq	16(%rcx), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1872:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movq	(%rbx), %rax
	movl	$1, %r8d
	movq	$0, 16(%rcx)
	movq	%rax, 8(%rcx)
	call	_ZNSt10_HashtableIPN2v88internal15FunctionLiteralESt4pairIKS3_S3_ESaIS6_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	popq	%rbx
	popq	%r12
	addq	$16, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24737:
	.size	_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_S4_ESaIS7_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_, .-_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_S4_ESaIS7_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiESaIS2_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiESaIS2_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiESaIS2_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_
	.type	_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiESaIS2_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_, @function
_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiESaIS2_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_:
.LFB25924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %r15
	je	.L1935
	movl	(%rdx), %r14d
	cmpl	32(%rsi), %r14d
	jge	.L1896
	movq	24(%rdi), %rbx
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rsi, %rbx
	je	.L1888
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	32(%rax), %r14d
	jle	.L1898
	movl	$0, %ebx
	cmpq	$0, 24(%rax)
	movq	%rbx, %rax
	cmovne	%r12, %rdx
	cmovne	%r12, %rax
.L1888:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1896:
	.cfi_restore_state
	jle	.L1907
	movq	32(%rdi), %rdx
	cmpq	%rsi, %rdx
	je	.L1934
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	32(%rax), %r14d
	jge	.L1909
	movl	$0, %ebx
	cmpq	$0, 24(%r12)
	movq	%rbx, %rax
	cmovne	%rdx, %rax
	cmove	%r12, %rdx
	jmp	.L1888
	.p2align 4,,10
	.p2align 3
.L1935:
	cmpq	$0, 40(%rdi)
	je	.L1887
	movq	32(%rdi), %rdx
	movl	(%r14), %eax
	cmpl	%eax, 32(%rdx)
	jl	.L1934
.L1887:
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	je	.L1918
	movl	(%r14), %esi
	jmp	.L1890
	.p2align 4,,10
	.p2align 3
.L1936:
	movq	16(%rbx), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L1891
.L1937:
	movq	%rax, %rbx
.L1890:
	movl	32(%rbx), %edx
	cmpl	%edx, %esi
	jl	.L1936
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L1937
.L1891:
	movq	%rbx, %r12
	testb	%cl, %cl
	jne	.L1889
.L1894:
	xorl	%eax, %eax
	cmpl	%esi, %edx
	cmovl	%rax, %rbx
	cmovge	%rax, %r12
.L1895:
	addq	$8, %rsp
	movq	%rbx, %rax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1907:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rsi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1934:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1898:
	.cfi_restore_state
	movq	16(%r13), %r12
	testq	%r12, %r12
	jne	.L1901
	jmp	.L1938
	.p2align 4,,10
	.p2align 3
.L1939:
	movq	16(%r12), %rax
	movl	$1, %esi
.L1904:
	testq	%rax, %rax
	je	.L1902
	movq	%rax, %r12
.L1901:
	movl	32(%r12), %ecx
	cmpl	%ecx, %r14d
	jl	.L1939
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L1904
	.p2align 4,,10
	.p2align 3
.L1918:
	movq	%r12, %rbx
.L1889:
	cmpq	%rbx, 24(%r13)
	je	.L1920
	movq	%rbx, %rdi
	movq	%rbx, %r12
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	(%r14), %esi
	movl	32(%rax), %edx
	movq	%rax, %rbx
	jmp	.L1894
	.p2align 4,,10
	.p2align 3
.L1902:
	movq	%r12, %rdx
	testb	%sil, %sil
	jne	.L1900
.L1905:
	xorl	%eax, %eax
	cmpl	%ecx, %r14d
	cmovg	%rax, %r12
	cmovle	%rax, %rdx
.L1906:
	movq	%r12, %rax
	jmp	.L1888
.L1938:
	movq	%r15, %r12
.L1900:
	cmpq	%r12, %rbx
	je	.L1924
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r12, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %r12
	jmp	.L1905
	.p2align 4,,10
	.p2align 3
.L1909:
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L1912
	jmp	.L1940
	.p2align 4,,10
	.p2align 3
.L1941:
	movq	16(%rbx), %rax
	movl	$1, %esi
.L1915:
	testq	%rax, %rax
	je	.L1913
	movq	%rax, %rbx
.L1912:
	movl	32(%rbx), %ecx
	cmpl	%ecx, %r14d
	jl	.L1941
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	jmp	.L1915
	.p2align 4,,10
	.p2align 3
.L1913:
	movq	%rbx, %rdx
	testb	%sil, %sil
	jne	.L1911
.L1916:
	xorl	%eax, %eax
	cmpl	%ecx, %r14d
	cmovg	%rax, %rbx
	cmovle	%rax, %rdx
.L1917:
	movq	%rbx, %rax
	jmp	.L1888
	.p2align 4,,10
	.p2align 3
.L1920:
	movq	%rbx, %r12
	xorl	%ebx, %ebx
	jmp	.L1895
.L1940:
	movq	%r15, %rbx
.L1911:
	cmpq	%rbx, 24(%r13)
	je	.L1928
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %rbx
	jmp	.L1916
	.p2align 4,,10
	.p2align 3
.L1924:
	movq	%r12, %rdx
	xorl	%r12d, %r12d
	jmp	.L1906
.L1928:
	movq	%rbx, %rdx
	xorl	%ebx, %ebx
	jmp	.L1917
	.cfi_endproc
.LFE25924:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiESaIS2_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_, .-_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiESaIS2_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_
	.section	.text._ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE24_M_get_insert_unique_posERS2_,"axG",@progbits,_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE24_M_get_insert_unique_posERS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE24_M_get_insert_unique_posERS2_
	.type	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE24_M_get_insert_unique_posERS2_, @function
_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE24_M_get_insert_unique_posERS2_:
.LFB26306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L1958
	movl	(%rsi), %r8d
	jmp	.L1945
	.p2align 4,,10
	.p2align 3
.L1959:
	jne	.L1948
	movl	4(%r12), %eax
	cmpl	%eax, 36(%rbx)
	jg	.L1947
.L1948:
	movq	24(%rbx), %rax
	xorl	%r9d, %r9d
	testq	%rax, %rax
	je	.L1946
.L1960:
	movq	%rax, %rbx
.L1945:
	movl	32(%rbx), %ecx
	cmpl	%r8d, %ecx
	jle	.L1959
.L1947:
	movq	16(%rbx), %rax
	movl	$1, %r9d
	testq	%rax, %rax
	jne	.L1960
.L1946:
	movq	%rbx, %rdx
	testb	%r9b, %r9b
	jne	.L1944
	cmpl	%r8d, %ecx
	jge	.L1961
.L1953:
	xorl	%eax, %eax
.L1952:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1958:
	.cfi_restore_state
	leaq	8(%rdi), %rbx
.L1944:
	xorl	%eax, %eax
	movq	%rbx, %rdx
	cmpq	24(%rdi), %rbx
	je	.L1952
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	(%r12), %r8d
	movq	%rbx, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %rbx
	cmpl	%r8d, %ecx
	jl	.L1953
.L1961:
	jne	.L1954
	movl	4(%r12), %eax
	cmpl	%eax, 36(%rbx)
	jl	.L1953
.L1954:
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26306:
	.size	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE24_M_get_insert_unique_posERS2_, .-_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE24_M_get_insert_unique_posERS2_
	.section	.text._ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS2_,"axG",@progbits,_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS2_
	.type	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS2_, @function
_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS2_:
.LFB25725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	%rax, %rsi
	je	.L1980
	movl	(%rdx), %r13d
	movl	32(%rsi), %eax
	movq	%rsi, %rbx
	cmpl	%r13d, %eax
	jle	.L1981
.L1967:
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rbx, 24(%r14)
	je	.L1966
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	32(%rax), %r13d
	jg	.L1972
	jne	.L1964
	movl	4(%r12), %eax
	cmpl	%eax, 36(%rdx)
	jge	.L1964
	.p2align 4,,10
	.p2align 3
.L1972:
	cmpq	$0, 24(%rdx)
	movl	$0, %eax
	cmovne	%rbx, %rax
	cmovne	%rbx, %rdx
.L1966:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1981:
	.cfi_restore_state
	jne	.L1968
	movl	4(%rdx), %ecx
	cmpl	%ecx, 36(%rsi)
	jg	.L1967
.L1968:
	cmpl	%r13d, %eax
	jge	.L1982
.L1969:
	cmpq	%rbx, 32(%r14)
	je	.L1979
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	32(%rax), %r13d
	jl	.L1975
	jne	.L1964
	movl	36(%rax), %eax
	cmpl	%eax, 4(%r12)
	jge	.L1964
.L1975:
	cmpq	$0, 24(%rbx)
	movl	$0, %eax
	cmovne	%rdx, %rax
	cmove	%rbx, %rdx
	jmp	.L1966
	.p2align 4,,10
	.p2align 3
.L1980:
	cmpq	$0, 40(%rdi)
	je	.L1964
	movq	32(%rdi), %rbx
	movl	32(%rbx), %eax
	cmpl	%eax, (%rdx)
	jg	.L1979
	jne	.L1964
	movl	36(%rbx), %eax
	cmpl	%eax, 4(%rdx)
	jle	.L1964
.L1979:
	movq	%rbx, %rdx
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1964:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rsi
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE24_M_get_insert_unique_posERS2_
	.p2align 4,,10
	.p2align 3
.L1982:
	.cfi_restore_state
	movl	4(%r12), %eax
	cmpl	%eax, 36(%rbx)
	jl	.L1969
	movq	%rbx, %rax
	xorl	%edx, %edx
	jmp	.L1966
	.cfi_endproc
.LFE25725:
	.size	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS2_, .-_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS2_
	.section	.text._ZN2v88internal12_GLOBAL__N_111MapLiteralsERKSt13unordered_mapIPNS0_15FunctionLiteralENS1_21FunctionLiteralChangeESt4hashIS4_ESt8equal_toIS4_ESaISt4pairIKS4_S5_EEERKSt6vectorIS4_SaIS4_EEPS2_IS4_S4_S7_S9_SaISA_ISB_S4_EEESP_.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_111MapLiteralsERKSt13unordered_mapIPNS0_15FunctionLiteralENS1_21FunctionLiteralChangeESt4hashIS4_ESt8equal_toIS4_ESaISt4pairIKS4_S5_EEERKSt6vectorIS4_SaIS4_EEPS2_IS4_S4_S7_S9_SaISA_ISB_S4_EEESP_.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_111MapLiteralsERKSt13unordered_mapIPNS0_15FunctionLiteralENS1_21FunctionLiteralChangeESt4hashIS4_ESt8equal_toIS4_ESaISt4pairIKS4_S5_EEERKSt6vectorIS4_SaIS4_EEPS2_IS4_S4_S7_S9_SaISA_ISB_S4_EEESP_.constprop.0:
.LFB28187:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -336(%rbp)
	movq	(%rsi), %r14
	movq	%rdx, -408(%rbp)
	movq	%rcx, -416(%rbp)
	movq	%r14, %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-280(%rbp), %rax
	movl	$0, -280(%rbp)
	movq	%rax, -312(%rbp)
	movq	%rax, -264(%rbp)
	movq	%rax, -256(%rbp)
	movq	8(%rsi), %rax
	movq	$0, -272(%rbp)
	movq	$0, -248(%rbp)
	movq	%rax, -328(%rbp)
	cmpq	%rax, %r14
	je	.L1999
	.p2align 4,,10
	.p2align 3
.L1998:
	movq	(%r15), %r13
	movl	$-1, %r14d
	movl	$-1, %ebx
	movl	28(%r13), %esi
	testl	%esi, %esi
	jne	.L2157
.L1987:
	movq	-272(%rbp), %rax
	movq	-312(%rbp), %r12
	testq	%rax, %rax
	jne	.L1989
	jmp	.L1988
	.p2align 4,,10
	.p2align 3
.L2158:
	jne	.L1992
	cmpl	%r14d, 36(%rax)
	jl	.L1991
.L1992:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1990
.L1989:
	cmpl	%ebx, 32(%rax)
	jge	.L2158
.L1991:
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1989
.L1990:
	cmpq	-312(%rbp), %r12
	je	.L1988
	cmpl	%ebx, 32(%r12)
	jg	.L1988
	jne	.L1994
	cmpl	%r14d, 36(%r12)
	jle	.L1994
	.p2align 4,,10
	.p2align 3
.L1988:
	movl	$48, %edi
	movq	%r12, -320(%rbp)
	call	_Znwm@PLT
	movq	-320(%rbp), %rsi
	leaq	-288(%rbp), %rdi
	movl	%ebx, 32(%rax)
	leaq	32(%rax), %rdx
	movq	%rax, %r12
	movl	%r14d, 36(%rax)
	movq	$0, 40(%rax)
	call	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS2_
	movq	%rax, %rbx
	testq	%rdx, %rdx
	je	.L1995
	movl	$1, %edi
	testq	%rax, %rax
	jne	.L1996
	cmpq	%rdx, -312(%rbp)
	jne	.L2159
.L1996:
	movq	-312(%rbp), %rcx
	movq	%r12, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -248(%rbp)
.L1994:
	movq	%r13, 40(%r12)
	addq	$8, %r15
	cmpq	%r15, -328(%rbp)
	jne	.L1998
.L1999:
	leaq	-192(%rbp), %rax
	movq	$1, -232(%rbp)
	movq	%rax, -392(%rbp)
	movq	%rax, -240(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -400(%rbp)
	movq	%rax, -176(%rbp)
	movq	-336(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	16(%rax), %r15
	leaq	-96(%rbp), %rax
	movq	$0, -216(%rbp)
	movl	$0x3f800000, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -192(%rbp)
	movq	$1, -168(%rbp)
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movl	$0x3f800000, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	%rax, -384(%rbp)
	testq	%r15, %r15
	je	.L1986
	movq	%r15, -320(%rbp)
	.p2align 4,,10
	.p2align 3
.L1985:
	movq	-320(%rbp), %rax
	movq	8(%rax), %rdx
	movl	28(%rdx), %ecx
	movq	%rdx, -296(%rbp)
	testl	%ecx, %ecx
	je	.L2075
	movl	16(%rax), %ecx
	movl	20(%rax), %esi
.L2000:
	movq	-312(%rbp), %rdi
	movq	-272(%rbp), %rax
	movq	%rdi, -328(%rbp)
	testq	%rax, %rax
	jne	.L2001
	jmp	.L2002
	.p2align 4,,10
	.p2align 3
.L2160:
	jne	.L2005
	cmpl	%esi, 36(%rax)
	jl	.L2004
.L2005:
	movq	%rax, -328(%rbp)
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2003
.L2001:
	cmpl	%ecx, 32(%rax)
	jge	.L2160
.L2004:
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L2001
.L2003:
	movq	-328(%rbp), %rax
	movq	-312(%rbp), %rdi
	cmpq	%rdi, %rax
	je	.L2002
	cmpl	%ecx, 32(%rax)
	jg	.L2002
	jne	.L2008
	cmpl	%esi, 36(%rax)
	jle	.L2008
	.p2align 4,,10
	.p2align 3
.L2002:
	leaq	-176(%rbp), %r12
	leaq	-296(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_NS2_12_GLOBAL__N_111ChangeStateEESaIS9_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	movl	$2, (%rax)
	movq	-320(%rbp), %rax
	cmpq	$0, 32(%rax)
	je	.L2045
	leaq	32(%rax), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_NS2_12_GLOBAL__N_111ChangeStateEESaIS9_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	cmpl	$2, (%rax)
	je	.L2045
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_NS2_12_GLOBAL__N_111ChangeStateEESaIS9_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	movl	$1, (%rax)
.L2045:
	movq	-320(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -320(%rbp)
	testq	%rax, %rax
	jne	.L1985
	movq	-224(%rbp), %r12
	testq	%r12, %r12
	je	.L2156
	movq	-408(%rbp), %r14
	movq	-416(%rbp), %r15
	leaq	-176(%rbp), %rbx
	jmp	.L2052
	.p2align 4,,10
	.p2align 3
.L2161:
	movq	%r14, %rdi
	call	_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_S4_ESaIS7_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	movq	16(%r12), %rdx
	movq	%rdx, (%rax)
.L2055:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L2156
.L2052:
	leaq	8(%r12), %r13
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_NS2_12_GLOBAL__N_111ChangeStateEESaIS9_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	movq	%r13, %rsi
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L2161
	movq	%rbx, %rdi
	call	_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_NS2_12_GLOBAL__N_111ChangeStateEESaIS9_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	cmpl	$1, (%rax)
	jne	.L2055
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_S4_ESaIS7_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	movq	16(%r12), %rdx
	movq	%rdx, (%rax)
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L2052
	.p2align 4,,10
	.p2align 3
.L2156:
	movq	-160(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1986
	.p2align 4,,10
	.p2align 3
.L2056:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2056
.L1986:
	movq	-168(%rbp), %rax
	movq	-176(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-176(%rbp), %rdi
	movq	$0, -152(%rbp)
	movq	$0, -160(%rbp)
	cmpq	-400(%rbp), %rdi
	je	.L2057
	call	_ZdlPv@PLT
.L2057:
	movq	-224(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L2062
	.p2align 4,,10
	.p2align 3
.L2059:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2059
.L2062:
	movq	-232(%rbp), %rax
	movq	-240(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-240(%rbp), %rdi
	movq	$0, -216(%rbp)
	movq	$0, -224(%rbp)
	cmpq	-392(%rbp), %rdi
	je	.L2060
	call	_ZdlPv@PLT
.L2060:
	movq	-272(%rbp), %rbx
	leaq	-288(%rbp), %r12
	testq	%rbx, %rbx
	je	.L1983
.L2063:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_PN2v88internal15FunctionLiteralEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2063
.L1983:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2162
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2008:
	.cfi_restore_state
	movq	40(%rdx), %rax
	movq	8(%rax), %r14
	movq	-328(%rbp), %rax
	movq	40(%rax), %rax
	movq	40(%rax), %rax
	movq	8(%rax), %r13
	testq	%r14, %r14
	je	.L2041
	testq	%r13, %r13
	je	.L2041
	leaq	-64(%rbp), %rax
	movq	%rax, -336(%rbp)
.L2043:
	movq	-336(%rbp), %rax
	movq	64(%r14), %r12
	leaq	56(%r14), %r15
	movq	$1, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -88(%rbp)
	movl	$0x3f800000, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	cmpq	%r12, %r15
	je	.L2009
	leaq	-80(%rbp), %rax
	movq	%r12, -344(%rbp)
	movq	%r15, %r12
	movq	%rax, -352(%rbp)
	jmp	.L2027
	.p2align 4,,10
	.p2align 3
.L2010:
	leaq	24(%r15), %r12
	cmpq	%r12, -344(%rbp)
	je	.L2163
.L2027:
	movq	(%r12), %r15
	movzwl	40(%r15), %eax
	sarl	$7, %eax
	andl	$7, %eax
	cmpb	$3, %al
	jne	.L2010
	movslq	32(%r15), %rax
	movq	-104(%rbp), %rcx
	xorl	%edx, %edx
	movq	%rax, -368(%rbp)
	movq	%rax, %rsi
	divq	%rcx
	movq	-112(%rbp), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	leaq	0(,%rdx,8), %r11
	testq	%rax, %rax
	je	.L2011
	movq	(%rax), %rbx
	movl	8(%rbx), %edi
	jmp	.L2013
	.p2align 4,,10
	.p2align 3
.L2164:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2011
	movslq	8(%rbx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rcx
	cmpq	%rdx, %r8
	jne	.L2011
.L2013:
	cmpl	%edi, %esi
	jne	.L2164
.L2155:
	movq	8(%r15), %rax
	addq	$16, %rbx
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	(%r12), %r15
	leaq	24(%r15), %r12
	cmpq	%r12, -344(%rbp)
	jne	.L2027
.L2163:
	movq	64(%r13), %r8
	leaq	56(%r13), %rax
	movq	-96(%rbp), %rbx
	cmpq	%r8, %rax
	je	.L2028
	movq	-104(%rbp), %rsi
	movq	-112(%rbp), %r10
	jmp	.L2036
	.p2align 4,,10
	.p2align 3
.L2029:
	leaq	24(%rcx), %rax
	cmpq	%r8, %rax
	je	.L2028
.L2036:
	movq	(%rax), %rcx
	movzwl	40(%rcx), %eax
	sarl	$7, %eax
	andl	$7, %eax
	cmpb	$3, %al
	jne	.L2029
	movslq	32(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %r11
	divq	%rsi
	movq	(%r10,%rdx,8), %rax
	movq	%rdx, %r12
	testq	%rax, %rax
	je	.L2030
	movq	(%rax), %rdi
	movl	8(%rdi), %r9d
	jmp	.L2032
	.p2align 4,,10
	.p2align 3
.L2165:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2030
	movslq	8(%rdi), %rax
	xorl	%edx, %edx
	movq	%rax, %r9
	divq	%rsi
	cmpq	%rdx, %r12
	jne	.L2030
.L2032:
	cmpl	%r9d, %r11d
	jne	.L2165
	movq	8(%rcx), %rax
	movq	(%rax), %rdx
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	je	.L2029
	.p2align 4,,10
	.p2align 3
.L2030:
	testq	%rbx, %rbx
	je	.L2033
	.p2align 4,,10
	.p2align 3
.L2034:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2034
	movq	-104(%rbp), %rsi
	movq	-112(%rbp), %r10
.L2033:
	leaq	0(,%rsi,8), %rdx
	movq	%r10, %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	$0, -88(%rbp)
	movq	-112(%rbp), %rdi
	movq	$0, -96(%rbp)
	cmpq	-336(%rbp), %rdi
	je	.L2002
	call	_ZdlPv@PLT
	jmp	.L2002
	.p2align 4,,10
	.p2align 3
.L2028:
	movq	8(%r14), %r14
	movq	8(%r13), %r13
	testq	%rbx, %rbx
	je	.L2037
	.p2align 4,,10
	.p2align 3
.L2038:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2038
.L2037:
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rdi
	leaq	0(,%rax,8), %rdx
.L2039:
	xorl	%esi, %esi
	call	memset@PLT
	movq	$0, -88(%rbp)
	movq	-112(%rbp), %rdi
	movq	$0, -96(%rbp)
	cmpq	-336(%rbp), %rdi
	je	.L2040
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L2041
	testq	%r14, %r14
	jne	.L2043
.L2041:
	cmpq	%r14, %r13
	jne	.L2002
	leaq	-296(%rbp), %r13
	leaq	-240(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_S4_ESaIS7_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	movq	-328(%rbp), %rcx
	movq	40(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	-296(%rbp), %r8
	xorl	%edx, %edx
	movq	-168(%rbp), %rdi
	movq	%r8, %rax
	divq	%rdi
	movq	-176(%rbp), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2047
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2049
	.p2align 4,,10
	.p2align 3
.L2166:
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L2047
.L2049:
	cmpq	%rsi, %r8
	je	.L2045
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L2166
.L2047:
	movq	-320(%rbp), %rax
	leaq	-176(%rbp), %r12
	movq	%r13, %rsi
	movq	%r12, %rdi
	movzbl	24(%rax), %ebx
	call	_ZNSt8__detail9_Map_baseIPN2v88internal15FunctionLiteralESt4pairIKS4_NS2_12_GLOBAL__N_111ChangeStateEESaIS9_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	movl	%ebx, (%rax)
	jmp	.L2045
	.p2align 4,,10
	.p2align 3
.L2011:
	movl	$24, %edi
	movq	%r11, -376(%rbp)
	movl	%esi, -360(%rbp)
	call	_Znwm@PLT
	movl	-360(%rbp), %esi
	movq	-88(%rbp), %rdx
	movl	$1, %ecx
	movq	$0, (%rax)
	movq	-352(%rbp), %rdi
	movq	%rax, %rbx
	movl	%esi, 8(%rax)
	movq	-104(%rbp), %rsi
	movq	$0, 16(%rax)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r8
	testb	%al, %al
	jne	.L2014
	movq	-112(%rbp), %r10
	movq	-376(%rbp), %r11
	leaq	(%r10,%r11), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2024
.L2169:
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	(%rax), %rax
	movq	%rbx, (%rax)
.L2025:
	addq	$1, -88(%rbp)
	jmp	.L2155
	.p2align 4,,10
	.p2align 3
.L2075:
	movl	$-1, %esi
	movl	$-1, %ecx
	jmp	.L2000
	.p2align 4,,10
	.p2align 3
.L2157:
	movq	%r13, %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movl	%eax, %ebx
	jmp	.L1987
	.p2align 4,,10
	.p2align 3
.L2014:
	cmpq	$1, %rdx
	je	.L2167
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2168
	leaq	0(,%rdx,8), %rdx
	movq	%r8, -376(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -360(%rbp)
	call	_Znwm@PLT
	movq	-360(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-376(%rbp), %r8
	movq	%rax, %r10
.L2017:
	movq	-96(%rbp), %rsi
	movq	$0, -96(%rbp)
	testq	%rsi, %rsi
	je	.L2019
	movq	-384(%rbp), %rdi
	xorl	%r11d, %r11d
	jmp	.L2020
	.p2align 4,,10
	.p2align 3
.L2021:
	movq	(%r9), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L2022:
	testq	%rsi, %rsi
	je	.L2019
.L2020:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r8
	leaq	(%r10,%rdx,8), %rax
	movq	(%rax), %r9
	testq	%r9, %r9
	jne	.L2021
	movq	-96(%rbp), %r9
	movq	%r9, (%rcx)
	movq	%rcx, -96(%rbp)
	movq	%rdi, (%rax)
	cmpq	$0, (%rcx)
	je	.L2077
	movq	%rcx, (%r10,%r11,8)
	movq	%rdx, %r11
	testq	%rsi, %rsi
	jne	.L2020
	.p2align 4,,10
	.p2align 3
.L2019:
	movq	-112(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L2023
	movq	%r8, -376(%rbp)
	movq	%r10, -360(%rbp)
	call	_ZdlPv@PLT
	movq	-376(%rbp), %r8
	movq	-360(%rbp), %r10
.L2023:
	movq	-368(%rbp), %rax
	xorl	%edx, %edx
	movq	%r8, -104(%rbp)
	movq	%r10, -112(%rbp)
	divq	%r8
	leaq	0(,%rdx,8), %r11
	leaq	(%r10,%r11), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L2169
.L2024:
	movq	-96(%rbp), %rdx
	movq	%rbx, -96(%rbp)
	movq	%rdx, (%rbx)
	testq	%rdx, %rdx
	je	.L2026
	movslq	8(%rdx), %rax
	xorl	%edx, %edx
	divq	-104(%rbp)
	movq	%rbx, (%r10,%rdx,8)
	movq	-112(%rbp), %rax
	addq	%r11, %rax
.L2026:
	movq	-384(%rbp), %rcx
	movq	%rcx, (%rax)
	jmp	.L2025
	.p2align 4,,10
	.p2align 3
.L1995:
	movq	%r12, %rdi
	movq	%rbx, %r12
	call	_ZdlPv@PLT
	jmp	.L1994
	.p2align 4,,10
	.p2align 3
.L2077:
	movq	%rdx, %r11
	jmp	.L2022
	.p2align 4,,10
	.p2align 3
.L2009:
	movq	64(%r13), %r8
	movq	%rax, %r10
	leaq	56(%r13), %rax
	xorl	%ebx, %ebx
	movl	$1, %esi
	cmpq	%rax, %r8
	jne	.L2036
	movq	8(%r14), %r14
	movq	8(%r13), %r13
	movq	%r10, %rdi
	movl	$8, %edx
	jmp	.L2039
	.p2align 4,,10
	.p2align 3
.L2040:
	testq	%r14, %r14
	je	.L2041
	testq	%r13, %r13
	jne	.L2043
	jmp	.L2041
	.p2align 4,,10
	.p2align 3
.L2159:
	movl	32(%rdx), %eax
	cmpl	%eax, 32(%r12)
	jl	.L1996
	movl	$0, %edi
	jne	.L1996
	xorl	%edi, %edi
	movl	36(%rdx), %eax
	cmpl	%eax, 36(%r12)
	setl	%dil
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L2167:
	movq	$0, -64(%rbp)
	movq	-336(%rbp), %r10
	jmp	.L2017
.L2168:
	call	_ZSt17__throw_bad_allocv@PLT
.L2162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28187:
	.size	_ZN2v88internal12_GLOBAL__N_111MapLiteralsERKSt13unordered_mapIPNS0_15FunctionLiteralENS1_21FunctionLiteralChangeESt4hashIS4_ESt8equal_toIS4_ESaISt4pairIKS4_S5_EEERKSt6vectorIS4_SaIS4_EEPS2_IS4_S4_S7_S9_SaISA_ISB_S4_EEESP_.constprop.0, .-_ZN2v88internal12_GLOBAL__N_111MapLiteralsERKSt13unordered_mapIPNS0_15FunctionLiteralENS1_21FunctionLiteralChangeESt4hashIS4_ESt8equal_toIS4_ESaISt4pairIKS4_S5_EEERKSt6vectorIS4_SaIS4_EEPS2_IS4_S4_S7_S9_SaISA_ISB_S4_EEESP_.constprop.0
	.section	.rodata._ZN2v88internal8LiveEdit11PatchScriptEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6StringEEEbPNS_5debug14LiveEditResultE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"(location_) != nullptr"
.LC7:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8LiveEdit11PatchScriptEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6StringEEEbPNS_5debug14LiveEditResultE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8LiveEdit11PatchScriptEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6StringEEEbPNS_5debug14LiveEditResultE
	.type	_ZN2v88internal8LiveEdit11PatchScriptEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6StringEEEbPNS_5debug14LiveEditResultE, @function
_ZN2v88internal8LiveEdit11PatchScriptEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6StringEEEbPNS_5debug14LiveEditResultE:
.LFB20569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$2472, %rsp
	movq	%rsi, -2400(%rbp)
	movq	41112(%rdi), %rdi
	movl	%ecx, -2456(%rbp)
	movq	%r8, -2432(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movaps	%xmm0, -2352(%rbp)
	movq	$0, -2336(%rbp)
	movq	7(%rax), %r13
	testq	%rdi, %rdi
	je	.L2171
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2172:
	leaq	-2352(%rbp), %rax
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%rax, %rcx
	movq	%rax, -2440(%rbp)
	call	_ZN2v88internal8LiveEdit14CompareStringsEPNS0_7IsolateENS0_6HandleINS0_6StringEEES6_PSt6vectorINS0_17SourceChangeRangeESaIS8_EE
	movq	-2352(%rbp), %rdi
	cmpq	%rdi, -2344(%rbp)
	je	.L2833
	movq	-2400(%rbp), %r14
	leaq	-1952(%rbp), %r15
	movq	%rbx, %rsi
	leaq	-2320(%rbp), %r13
	movq	%r15, %rdi
	movq	%r15, -2448(%rbp)
	movq	%r14, %rdx
	call	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE@PLT
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r13, %rcx
	movq	-2432(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	$0, -2304(%rbp)
	movaps	%xmm0, -2320(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111ParseScriptEPNS0_7IsolateEPNS0_9ParseInfoEbPSt6vectorIPNS0_15FunctionLiteralESaIS8_EEPNS_5debug14LiveEditResultE
	testb	%al, %al
	jne	.L2834
.L2176:
	movq	-2320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2468
.L2838:
	call	_ZdlPv@PLT
.L2468:
	movq	-2448(%rbp), %rdi
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	movq	-2352(%rbp), %rdi
.L2175:
	testq	%rdi, %rdi
	je	.L2170
	call	_ZdlPv@PLT
.L2170:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2835
	addq	$2472, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2834:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory11CloneScriptENS0_6HandleINS0_6ScriptEEE@PLT
	movq	(%r12), %r12
	movq	(%rax), %r14
	movq	%rax, -2408(%rbp)
	movq	%r12, 7(%r14)
	leaq	7(%r14), %r15
	testb	$1, %r12b
	je	.L2481
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -2392(%rbp)
	testl	$262144, %eax
	jne	.L2836
	testb	$24, %al
	je	.L2481
.L2871:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2481
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2481:
	leaq	-1728(%rbp), %rax
	movq	-2408(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	movq	%rax, -2480(%rbp)
	leaq	-2288(%rbp), %r14
	movaps	%xmm0, -2288(%rbp)
	movq	$0, -2272(%rbp)
	call	_ZN2v88internal9ParseInfoC1EPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE@PLT
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%r15, %rsi
	movq	-2432(%rbp), %r8
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111ParseScriptEPNS0_7IsolateEPNS0_9ParseInfoEbPSt6vectorIPNS0_15FunctionLiteralESaIS8_EEPNS_5debug14LiveEditResultE
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L2837
.L2180:
	movq	-2480(%rbp), %rdi
	call	_ZN2v88internal9ParseInfoD1Ev@PLT
	movq	-2288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2176
	call	_ZdlPv@PLT
	movq	-2320(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2838
	jmp	.L2468
	.p2align 4,,10
	.p2align 3
.L2171:
	movq	41088(%rbx), %rsi
	cmpq	%rsi, 41096(%rbx)
	je	.L2839
.L2173:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r13, (%rsi)
	jmp	.L2172
	.p2align 4,,10
	.p2align 3
.L2833:
	movq	-2432(%rbp), %rax
	movl	$0, (%rax)
	jmp	.L2175
	.p2align 4,,10
	.p2align 3
.L2837:
	movss	.LC5(%rip), %xmm0
	movq	-2440(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-2208(%rbp), %r15
	leaq	-2160(%rbp), %rax
	movq	%r15, %rdx
	movq	$1, -2200(%rbp)
	movq	%rax, -2496(%rbp)
	movq	%rax, -2208(%rbp)
	movss	%xmm0, -2176(%rbp)
	movq	$0, -2192(%rbp)
	movq	$0, -2184(%rbp)
	movq	$0, -2168(%rbp)
	movq	$0, -2160(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_131CalculateFunctionLiteralChangesERKSt6vectorIPNS0_15FunctionLiteralESaIS4_EERKS2_INS0_17SourceChangeRangeESaIS9_EEPSt13unordered_mapIS4_NS1_21FunctionLiteralChangeESt4hashIS4_ESt8equal_toIS4_ESaISt4pairIKS4_SF_EEE.constprop.0
	movss	.LC5(%rip), %xmm0
	movq	%r14, %rsi
	leaq	-2096(%rbp), %rax
	movq	%rax, -2504(%rbp)
	leaq	-2144(%rbp), %rcx
	movq	%r15, %rdi
	leaq	-2080(%rbp), %rdx
	movq	%rax, -2144(%rbp)
	leaq	-2032(%rbp), %rax
	movq	%rax, -2512(%rbp)
	movq	%rax, -2080(%rbp)
	movq	$1, -2136(%rbp)
	movq	$0, -2128(%rbp)
	movq	$0, -2120(%rbp)
	movq	$0, -2104(%rbp)
	movq	$0, -2096(%rbp)
	movq	$1, -2072(%rbp)
	movq	$0, -2064(%rbp)
	movq	$0, -2056(%rbp)
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movss	%xmm0, -2112(%rbp)
	movss	%xmm0, -2048(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_111MapLiteralsERKSt13unordered_mapIPNS0_15FunctionLiteralENS1_21FunctionLiteralChangeESt4hashIS4_ESt8equal_toIS4_ESaISt4pairIKS4_S5_EEERKSt6vectorIS4_SaIS4_EEPS2_IS4_S4_S7_S9_SaISA_ISB_S4_EEESP_.constprop.0
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_115FunctionDataMapE(%rip), %rax
	movq	-2128(%rbp), %r13
	movl	$0, -2000(%rbp)
	movq	%rax, -2016(%rbp)
	leaq	-2000(%rbp), %rax
	movq	$0, -1992(%rbp)
	movq	%rax, -2392(%rbp)
	movq	%rax, -1984(%rbp)
	movq	%rax, -1976(%rbp)
	movq	$0, -1968(%rbp)
	testq	%r13, %r13
	je	.L2210
	movq	%rbx, -2464(%rbp)
	.p2align 4,,10
	.p2align 3
.L2181:
	movq	-2400(%rbp), %rax
	movq	8(%r13), %rbx
	movq	(%rax), %rax
	movq	%rbx, %rdi
	movslq	67(%rax), %r15
	movl	%r15d, -2416(%rbp)
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movl	28(%rbx), %r14d
	movl	$112, %edi
	testl	%r14d, %r14d
	movl	$-1, %r14d
	cmovne	%eax, %r14d
	call	_Znwm@PLT
	movq	-1992(%rbp), %rdx
	movl	-2416(%rbp), %r9d
	movl	%r15d, 32(%rax)
	movq	%rax, %rsi
	testq	%rdx, %rdx
	movl	%r14d, 36(%rax)
	movq	%rbx, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	$0, 72(%rax)
	movq	$0, 80(%rax)
	movq	$0, 88(%rax)
	movq	$0, 96(%rax)
	movl	$0, 104(%rax)
	movb	$1, 108(%rax)
	jne	.L2186
	jmp	.L2840
	.p2align 4,,10
	.p2align 3
.L2841:
	jne	.L2189
	cmpl	36(%rdx), %r14d
	jl	.L2188
.L2189:
	movq	24(%rdx), %rax
	xorl	%r10d, %r10d
	testq	%rax, %rax
	je	.L2187
.L2842:
	movq	%rax, %rdx
.L2186:
	movl	32(%rdx), %edi
	cmpl	%edi, %r9d
	jge	.L2841
.L2188:
	movq	16(%rdx), %rax
	movl	%r12d, %r10d
	testq	%rax, %rax
	jne	.L2842
.L2187:
	testb	%r10b, %r10b
	jne	.L2843
	movq	%rdx, %r9
	cmpl	%edi, %r15d
	jg	.L2495
	cmpl	%edi, %r15d
	jne	.L2194
.L2873:
	cmpl	36(%rdx), %r14d
	jle	.L2194
	movq	%r9, %rdx
.L2193:
	testq	%rdx, %rdx
	je	.L2194
.L2495:
	movl	$1, %edi
	cmpq	-2392(%rbp), %rdx
	jne	.L2844
.L2195:
	movq	-2392(%rbp), %rcx
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -1968(%rbp)
.L2196:
	movq	-2408(%rbp), %rax
	movq	16(%r13), %r14
	movl	$-1, %r15d
	movq	(%rax), %rax
	movq	%r14, %rdi
	movslq	67(%rax), %r8
	movq	%r8, -2424(%rbp)
	movl	%r8d, -2416(%rbp)
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movl	28(%r14), %r11d
	movl	$112, %edi
	testl	%r11d, %r11d
	cmovne	%eax, %r15d
	call	_Znwm@PLT
	movq	-2424(%rbp), %r8
	movl	-2416(%rbp), %r9d
	movq	%r14, 40(%rax)
	movq	-1992(%rbp), %r14
	movq	%rax, %rsi
	movl	%r8d, 32(%rax)
	testq	%r14, %r14
	movl	%r15d, 36(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	$0, 72(%rax)
	movq	$0, 80(%rax)
	movq	$0, 88(%rax)
	movq	$0, 96(%rax)
	movl	$0, 104(%rax)
	movb	$0, 108(%rax)
	jne	.L2199
	jmp	.L2845
	.p2align 4,,10
	.p2align 3
.L2846:
	jne	.L2202
	cmpl	36(%r14), %r15d
	jl	.L2201
.L2202:
	movq	24(%r14), %rax
	xorl	%r10d, %r10d
	testq	%rax, %rax
	je	.L2200
.L2847:
	movq	%rax, %r14
.L2199:
	movl	32(%r14), %edi
	cmpl	%edi, %r9d
	jge	.L2846
.L2201:
	movq	16(%r14), %rax
	movl	%r12d, %r10d
	testq	%rax, %rax
	jne	.L2847
.L2200:
	testb	%r10b, %r10b
	jne	.L2848
	movq	%r14, %rax
	cmpl	%edi, %r8d
	jg	.L2492
	cmpl	%edi, %r8d
	jne	.L2207
.L2874:
	cmpl	36(%rax), %r15d
	jle	.L2207
.L2206:
	testq	%r14, %r14
	je	.L2207
.L2492:
	movl	$1, %edi
	cmpq	-2392(%rbp), %r14
	jne	.L2849
.L2208:
	movq	-2392(%rbp), %rcx
	movq	%r14, %rdx
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -1968(%rbp)
.L2209:
	movq	0(%r13), %r13
	testq	%r13, %r13
	jne	.L2181
	movq	-2464(%rbp), %rbx
.L2210:
	movq	-2064(%rbp), %r13
	testq	%r13, %r13
	je	.L2183
	movq	%rbx, -2464(%rbp)
	.p2align 4,,10
	.p2align 3
.L2182:
	movq	-2400(%rbp), %rax
	movq	8(%r13), %r15
	movl	$-1, %ebx
	movq	(%rax), %rax
	movq	%r15, %rdi
	movslq	67(%rax), %r14
	movl	%r14d, -2416(%rbp)
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movl	28(%r15), %r10d
	movl	$112, %edi
	testl	%r10d, %r10d
	cmovne	%eax, %ebx
	call	_Znwm@PLT
	movq	-1992(%rbp), %rdx
	movl	-2416(%rbp), %r9d
	movl	%r14d, 32(%rax)
	movq	%rax, %rsi
	testq	%rdx, %rdx
	movl	%ebx, 36(%rax)
	movq	%r15, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	$0, 72(%rax)
	movq	$0, 80(%rax)
	movq	$0, 88(%rax)
	movq	$0, 96(%rax)
	movl	$0, 104(%rax)
	movb	$0, 108(%rax)
	jne	.L2215
	jmp	.L2850
	.p2align 4,,10
	.p2align 3
.L2851:
	jne	.L2218
	cmpl	36(%rdx), %ebx
	jl	.L2217
.L2218:
	movq	24(%rdx), %rax
	xorl	%r10d, %r10d
	testq	%rax, %rax
	je	.L2216
.L2852:
	movq	%rax, %rdx
.L2215:
	movl	32(%rdx), %edi
	cmpl	%edi, %r9d
	jge	.L2851
.L2217:
	movq	16(%rdx), %rax
	movl	%r12d, %r10d
	testq	%rax, %rax
	jne	.L2852
.L2216:
	testb	%r10b, %r10b
	jne	.L2853
	movq	%rdx, %r9
	cmpl	%edi, %r14d
	jg	.L2489
	cmpl	%edi, %r14d
	jne	.L2223
.L2872:
	cmpl	36(%rdx), %ebx
	jle	.L2223
	movq	%r9, %rdx
.L2222:
	testq	%rdx, %rdx
	je	.L2223
.L2489:
	movl	$1, %edi
	cmpq	-2392(%rbp), %rdx
	jne	.L2854
.L2224:
	movq	-2392(%rbp), %rcx
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -1968(%rbp)
.L2225:
	movq	0(%r13), %r13
	testq	%r13, %r13
	jne	.L2182
	movq	-2464(%rbp), %rbx
.L2183:
	leaq	-2384(%rbp), %rdx
	movq	%rbx, %rsi
	leaq	-2016(%rbp), %rdi
	movq	$0, -2384(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_115FunctionDataMap4FillEPNS0_7IsolateEPm.constprop.0
	movq	-2128(%rbp), %r15
	testq	%r15, %r15
	je	.L2212
	movq	%rbx, -2416(%rbp)
	movq	%r15, %rbx
	movq	-2400(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L2211:
	movq	8(%rbx), %r12
	movq	(%r15), %rax
	movq	%r12, %rdi
	movslq	67(%rax), %r13
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movl	28(%r12), %r9d
	movl	$-1, %ecx
	movq	-1992(%rbp), %r12
	movl	%r13d, %r14d
	testl	%r9d, %r9d
	cmove	%ecx, %eax
	testq	%r12, %r12
	je	.L2229
	movq	-2392(%rbp), %rdx
	jmp	.L2230
	.p2align 4,,10
	.p2align 3
.L2855:
	jne	.L2233
	cmpl	36(%r12), %eax
	jg	.L2232
.L2233:
	movq	%r12, %rdx
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L2231
.L2230:
	cmpl	32(%r12), %r14d
	jle	.L2855
.L2232:
	movq	24(%r12), %r12
	testq	%r12, %r12
	jne	.L2230
.L2231:
	cmpq	-2392(%rbp), %rdx
	je	.L2229
	cmpl	32(%rdx), %r13d
	jl	.L2229
	jne	.L2235
	cmpl	36(%rdx), %eax
	jl	.L2229
.L2235:
	leaq	40(%rdx), %r12
.L2229:
	movq	-2408(%rbp), %rax
	movq	16(%rbx), %r13
	movq	(%rax), %rax
	movq	%r13, %rdi
	movl	67(%rax), %r14d
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movl	28(%r13), %r8d
	movl	$-1, %ecx
	movq	-1992(%rbp), %rsi
	testl	%r8d, %r8d
	cmove	%ecx, %eax
	testq	%rsi, %rsi
	je	.L2237
	movq	%rsi, %rdx
	jmp	.L2238
	.p2align 4,,10
	.p2align 3
.L2856:
	jne	.L2240
	cmpl	36(%rdx), %eax
	jg	.L2239
.L2240:
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L2237
.L2238:
	cmpl	32(%rdx), %r14d
	jle	.L2856
.L2239:
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2238
.L2237:
	cmpq	$0, 8(%r12)
	je	.L2242
	cmpb	$0, 68(%r12)
	je	.L2857
	movl	64(%r12), %eax
	cmpl	$1, %eax
	je	.L2508
	cmpl	$3, %eax
	je	.L2509
	movq	40(%r12), %rcx
	cmpq	%rcx, 48(%r12)
	jne	.L2510
	cmpl	$4, %eax
	je	.L2858
.L2242:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2211
	movq	-2416(%rbp), %rbx
.L2212:
	movq	-2384(%rbp), %r12
	testq	%r12, %r12
	jne	.L2859
.L2227:
	cmpb	$0, -2456(%rbp)
	jne	.L2860
	leaq	-2248(%rbp), %rax
	leaq	-2368(%rbp), %rcx
	movl	$0, -2248(%rbp)
	movq	%rax, -2456(%rbp)
	movq	%rax, -2232(%rbp)
	movq	%rax, -2224(%rbp)
	movq	-2064(%rbp), %rax
	movq	$0, -2240(%rbp)
	movq	$0, -2216(%rbp)
	movq	%rax, %r13
	movq	%rcx, -2488(%rbp)
	testq	%rax, %rax
	je	.L2365
	.p2align 4,,10
	.p2align 3
.L2271:
	movq	-2400(%rbp), %rax
	movq	8(%r13), %r14
	movq	(%rax), %rax
	movq	%r14, %rdi
	movslq	67(%rax), %rcx
	movq	%rcx, -2416(%rbp)
	movl	%ecx, %r12d
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movl	28(%r14), %edi
	movl	$-1, %ecx
	movq	-1992(%rbp), %rdx
	testl	%edi, %edi
	cmove	%ecx, %eax
	testq	%rdx, %rdx
	je	.L2331
	movq	-2392(%rbp), %r15
	movq	-2416(%rbp), %rcx
	jmp	.L2276
	.p2align 4,,10
	.p2align 3
.L2861:
	jne	.L2279
	cmpl	36(%rdx), %eax
	jg	.L2278
.L2279:
	movq	%rdx, %r15
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L2277
.L2276:
	cmpl	32(%rdx), %r12d
	jle	.L2861
.L2278:
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2276
.L2277:
	cmpq	-2392(%rbp), %r15
	je	.L2331
	cmpl	32(%r15), %ecx
	jl	.L2331
	jne	.L2281
	cmpl	36(%r15), %eax
	jge	.L2281
	.p2align 4,,10
	.p2align 3
.L2331:
	movq	0(%r13), %r13
	testq	%r13, %r13
	jne	.L2271
.L2365:
	movq	-2128(%rbp), %r12
	testq	%r12, %r12
	je	.L2273
	.p2align 4,,10
	.p2align 3
.L2272:
	movq	-2408(%rbp), %rax
	movq	16(%r12), %r14
	movq	(%rax), %rax
	movq	%r14, %rdi
	movslq	67(%rax), %r15
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movl	28(%r14), %esi
	movl	$-1, %edx
	movl	%r15d, %r13d
	testl	%esi, %esi
	cmove	%edx, %eax
	movq	-1992(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2385
	movq	-2392(%rbp), %rcx
	jmp	.L2370
	.p2align 4,,10
	.p2align 3
.L2862:
	jne	.L2373
	cmpl	36(%rdx), %eax
	jg	.L2372
.L2373:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L2371
.L2370:
	cmpl	32(%rdx), %r13d
	jle	.L2862
.L2372:
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2370
.L2371:
	cmpq	-2392(%rbp), %rcx
	je	.L2385
	cmpl	32(%rcx), %r15d
	jl	.L2385
	jne	.L2375
	cmpl	36(%rcx), %eax
	jl	.L2385
.L2375:
	movq	48(%rcx), %rax
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2863
	movq	-2400(%rbp), %rax
	movq	8(%r12), %r15
	movq	(%rax), %rax
	movq	%r15, %rdi
	movslq	67(%rax), %rcx
	movq	%rcx, -2416(%rbp)
	movl	%ecx, %r14d
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movl	28(%r15), %ecx
	movl	$-1, %edx
	testl	%ecx, %ecx
	cmove	%edx, %eax
	movq	-1992(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2385
	movq	-2392(%rbp), %r15
	movq	-2416(%rbp), %rcx
	jmp	.L2378
	.p2align 4,,10
	.p2align 3
.L2864:
	jne	.L2381
	cmpl	36(%rdx), %eax
	jg	.L2380
.L2381:
	movq	%rdx, %r15
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L2379
.L2378:
	cmpl	32(%rdx), %r14d
	jle	.L2864
.L2380:
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2378
.L2379:
	cmpq	-2392(%rbp), %r15
	je	.L2385
	cmpl	32(%r15), %ecx
	jl	.L2385
	jne	.L2383
	cmpl	36(%r15), %eax
	jl	.L2385
.L2383:
	movq	48(%r15), %r14
	testq	%r14, %r14
	je	.L2385
	movq	41472(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal5Debug18DeoptimizeFunctionENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movq	40952(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal16CompilationCache6RemoveENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movq	64(%r15), %rax
	movq	56(%r15), %r14
	leaq	-1504(%rbp), %r15
	movq	%rax, -2416(%rbp)
	cmpq	%r14, %rax
	je	.L2385
	movq	%r12, -2488(%rbp)
	movq	%r14, %r12
	.p2align 4,,10
	.p2align 3
.L2399:
	movq	(%r12), %rax
	movq	0(%r13), %rdx
	movq	(%rax), %r14
	movq	%rdx, 23(%r14)
	leaq	23(%r14), %rsi
	testb	$1, %dl
	je	.L2477
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -2424(%rbp)
	testl	$262144, %eax
	je	.L2387
	movq	%r14, %rdi
	movq	%rdx, -2464(%rbp)
	movq	%rsi, -2440(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-2424(%rbp), %r8
	movq	-2464(%rbp), %rdx
	movq	-2440(%rbp), %rsi
	movq	8(%r8), %rax
.L2387:
	testb	$24, %al
	je	.L2477
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2477
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2477:
	movq	(%r12), %rax
	movq	%r15, %rdi
	movq	(%rax), %r14
	movq	23(%r14), %rax
	movq	%rax, -1504(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo7GetCodeEv@PLT
	leaq	47(%r14), %rsi
	movq	%rax, 47(%r14)
	movq	%rax, %rdx
	testb	$1, %al
	je	.L2389
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2389
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L2389:
	movq	(%r12), %rax
	movq	4480(%rbx), %rdx
	movq	(%rax), %rdi
	movq	%rdx, 39(%rdi)
	leaq	39(%rdi), %rsi
	testb	$1, %dl
	je	.L2476
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -2424(%rbp)
	testl	$262144, %eax
	je	.L2392
	movq	%rdx, -2472(%rbp)
	movq	%rsi, -2464(%rbp)
	movq	%rdi, -2440(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-2424(%rbp), %r8
	movq	-2472(%rbp), %rdx
	movq	-2464(%rbp), %rsi
	movq	-2440(%rbp), %rdi
	movq	8(%r8), %rax
.L2392:
	testb	$24, %al
	je	.L2476
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2476
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2476:
	movq	(%r12), %rax
	movq	(%rax), %rax
	movq	47(%rax), %rdx
	cmpl	$67, 59(%rdx)
	je	.L2395
	movabsq	$287762808832, %rcx
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2395
	testb	$1, %al
	jne	.L2865
.L2397:
	movq	(%r12), %rdi
	call	_ZN2v88internal10JSFunction20EnsureFeedbackVectorENS0_6HandleIS1_EE@PLT
.L2395:
	addq	$8, %r12
	cmpq	%r12, -2416(%rbp)
	jne	.L2399
	movq	-2488(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L2385:
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L2272
.L2273:
	movq	-2408(%rbp), %rax
	leaq	-2368(%rbp), %r12
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIteratorC1EPNS0_7IsolateENS0_6ScriptE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIterator4NextEv@PLT
	leaq	-1504(%rbp), %rcx
	movq	%rcx, -2392(%rbp)
	testq	%rax, %rax
	je	.L2367
	movq	%rbx, -2416(%rbp)
	jmp	.L2366
	.p2align 4,,10
	.p2align 3
.L2407:
	movq	(%rdx), %rcx
	testb	$1, %cl
	jne	.L2866
.L2404:
	movq	%r12, %rdi
	call	_ZN2v88internal18SharedFunctionInfo14ScriptIterator4NextEv@PLT
	testq	%rax, %rax
	je	.L2867
.L2366:
	movq	7(%rax), %rcx
	leaq	7(%rax), %rdx
	testb	$1, %cl
	je	.L2407
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L2407
.L2406:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L2868
.L2408:
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L2869
.L2410:
	movq	(%rdx), %rax
	movq	7(%rax), %rax
.L2409:
	movq	15(%rax), %r15
	movq	7(%r15), %rdx
	leaq	7(%r15), %r13
	movq	%rdx, %rax
	sarq	$32, %rax
	testq	%rax, %rax
	jle	.L2404
	leaq	15(%r15), %r14
	xorl	%ebx, %ebx
	jmp	.L2421
	.p2align 4,,10
	.p2align 3
.L2420:
	movq	%rdx, %rax
	addl	$1, %ebx
	addq	$8, %r14
	sarq	$32, %rax
	cmpl	%eax, %ebx
	jge	.L2404
.L2421:
	movq	(%r14), %rax
	testb	$1, %al
	je	.L2420
	movq	-1(%rax), %rax
	cmpw	$160, 11(%rax)
	jne	.L2420
	movq	(%r14), %rax
	movq	-2392(%rbp), %rdi
	movq	%rax, -1504(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-2240(%rbp), %rdx
	movq	-2456(%rbp), %rcx
	testq	%rdx, %rdx
	jne	.L2412
	jmp	.L2825
	.p2align 4,,10
	.p2align 3
.L2870:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
.L2415:
	testq	%rdx, %rdx
	je	.L2413
.L2412:
	cmpl	32(%rdx), %eax
	jle	.L2870
	movq	24(%rdx), %rdx
	jmp	.L2415
	.p2align 4,,10
	.p2align 3
.L2836:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-2392(%rbp), %rcx
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L2871
	jmp	.L2481
	.p2align 4,,10
	.p2align 3
.L2853:
	cmpq	-1984(%rbp), %rdx
	je	.L2489
.L2491:
	movq	%rdx, %rdi
	movq	%rsi, -2424(%rbp)
	movq	%rdx, -2416(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-2416(%rbp), %rdx
	movq	-2424(%rbp), %rsi
	movl	32(%rax), %edi
	cmpl	%edi, %r14d
	jg	.L2222
	movq	%rdx, %r9
	movq	%rax, %rdx
	cmpl	%edi, %r14d
	je	.L2872
.L2223:
	movq	%rsi, %rdi
	call	_ZdlPv@PLT
	jmp	.L2225
	.p2align 4,,10
	.p2align 3
.L2843:
	cmpq	-1984(%rbp), %rdx
	je	.L2495
.L2497:
	movq	%rdx, %rdi
	movq	%rsi, -2424(%rbp)
	movq	%rdx, -2416(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-2416(%rbp), %rdx
	movq	-2424(%rbp), %rsi
	movl	32(%rax), %edi
	cmpl	%edi, %r15d
	jg	.L2193
	movq	%rdx, %r9
	movq	%rax, %rdx
	cmpl	%edi, %r15d
	je	.L2873
.L2194:
	movq	%rsi, %rdi
	call	_ZdlPv@PLT
	jmp	.L2196
	.p2align 4,,10
	.p2align 3
.L2848:
	cmpq	-1984(%rbp), %r14
	je	.L2492
.L2494:
	movq	%r14, %rdi
	movq	%r8, -2424(%rbp)
	movq	%rsi, -2416(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-2424(%rbp), %r8
	movq	-2416(%rbp), %rsi
	movl	32(%rax), %edi
	cmpl	%edi, %r8d
	jg	.L2206
	cmpl	%edi, %r8d
	je	.L2874
.L2207:
	movq	%rsi, %rdi
	call	_ZdlPv@PLT
	jmp	.L2209
	.p2align 4,,10
	.p2align 3
.L2849:
	cmpl	32(%r14), %r8d
	jl	.L2208
	movl	$0, %edi
	jne	.L2208
	xorl	%edi, %edi
	cmpl	36(%r14), %r15d
	setl	%dil
	jmp	.L2208
	.p2align 4,,10
	.p2align 3
.L2854:
	cmpl	32(%rdx), %r14d
	jl	.L2224
	movl	$0, %edi
	jne	.L2224
	xorl	%edi, %edi
	cmpl	36(%rdx), %ebx
	setl	%dil
	jmp	.L2224
	.p2align 4,,10
	.p2align 3
.L2844:
	cmpl	32(%rdx), %r15d
	jl	.L2195
	movl	$0, %edi
	jne	.L2195
	xorl	%edi, %edi
	cmpl	36(%rdx), %r14d
	setl	%dil
	jmp	.L2195
	.p2align 4,,10
	.p2align 3
.L2850:
	movq	-2392(%rbp), %rax
	cmpq	%rax, -1984(%rbp)
	je	.L2514
	movq	%rax, %rdx
	jmp	.L2491
	.p2align 4,,10
	.p2align 3
.L2845:
	movq	-2392(%rbp), %rax
	movq	%rax, %r14
	cmpq	%rax, -1984(%rbp)
	jne	.L2494
	movl	$1, %edi
	jmp	.L2208
	.p2align 4,,10
	.p2align 3
.L2840:
	movq	-2392(%rbp), %rax
	movq	%rax, %rdx
	cmpq	%rax, -1984(%rbp)
	jne	.L2497
	movl	$1, %edi
	jmp	.L2195
	.p2align 4,,10
	.p2align 3
.L2281:
	movq	48(%r15), %r12
	testq	%r12, %r12
	je	.L2331
	movq	40952(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal16CompilationCache6RemoveENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movq	41472(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal5Debug18DeoptimizeFunctionENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movq	(%r12), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L2875
.L2284:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-2440(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_115UpdatePositionsEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEERKSt6vectorINS0_17SourceChangeRangeESaIS8_EE
	movq	-2408(%rbp), %rax
	movq	(%r12), %r14
	movq	(%rax), %rdx
	movq	31(%r14), %rdi
	leaq	31(%r14), %rsi
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	testb	$1, %dil
	jne	.L2876
.L2289:
	movq	%rdx, 31(%r14)
	testb	%al, %al
	jne	.L2293
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -2416(%rbp)
	testl	$262144, %eax
	je	.L2295
	movq	%r14, %rdi
	movq	%rdx, -2464(%rbp)
	movq	%rsi, -2424(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-2416(%rbp), %rcx
	movq	-2464(%rbp), %rdx
	movq	-2424(%rbp), %rsi
	movq	8(%rcx), %rax
.L2295:
	testb	$24, %al
	je	.L2293
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2877
	.p2align 4,,10
	.p2align 3
.L2293:
	movq	16(%r13), %rax
	movl	28(%rax), %edx
	movq	(%r12), %rax
	movl	%edx, 51(%rax)
	movq	-2408(%rbp), %rax
	movq	(%r12), %rcx
	movq	(%rax), %rax
	movq	%rcx, %rdx
	movq	87(%rax), %rdi
	movq	16(%r13), %rax
	orq	$2, %rdx
	movl	28(%rax), %eax
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L2473
	cmpl	$3, %edx
	je	.L2473
	movq	%rcx, %rdx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	andq	$-3, %rdx
	movq	%rcx, %r14
	testl	$262144, %eax
	jne	.L2878
.L2298:
	testb	$24, %al
	je	.L2473
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2879
	.p2align 4,,10
	.p2align 3
.L2473:
	movq	16(%r13), %r8
	movq	%r8, %rdi
	movq	%r8, -2416(%rbp)
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	-2456(%rbp), %r14
	movq	-2416(%rbp), %r8
	movl	%eax, %edx
	movq	-2240(%rbp), %rax
	testq	%rax, %rax
	jne	.L2301
	jmp	.L2300
	.p2align 4,,10
	.p2align 3
.L2880:
	movq	%rax, %r14
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2302
.L2301:
	cmpl	32(%rax), %edx
	jle	.L2880
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L2301
.L2302:
	cmpq	-2456(%rbp), %r14
	je	.L2300
	cmpl	32(%r14), %edx
	jge	.L2305
.L2300:
	movl	$40, %edi
	movq	%r8, -2424(%rbp)
	movl	%edx, -2464(%rbp)
	movq	%r14, -2416(%rbp)
	call	_Znwm@PLT
	movl	-2464(%rbp), %edx
	movq	-2416(%rbp), %rsi
	leaq	-2256(%rbp), %rdi
	movl	$0, 36(%rax)
	movq	%rax, %r14
	movl	%edx, 32(%rax)
	leaq	32(%rax), %rdx
	call	_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiESaIS2_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS2_ERS1_
	movq	-2424(%rbp), %r8
	testq	%rdx, %rdx
	je	.L2306
	testq	%rax, %rax
	jne	.L2513
	cmpq	%rdx, -2456(%rbp)
	jne	.L2881
.L2513:
	movl	$1, %edi
.L2307:
	movq	-2456(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r8, -2416(%rbp)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -2216(%rbp)
	movq	-2416(%rbp), %r8
.L2305:
	movl	28(%r8), %eax
	movl	%eax, 36(%r14)
	movq	(%r12), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L2882
.L2311:
	movq	64(%r15), %r14
	movq	56(%r15), %r15
	cmpq	%r15, %r14
	je	.L2327
	movq	%r12, -2464(%rbp)
	movq	%r13, -2472(%rbp)
	.p2align 4,,10
	.p2align 3
.L2326:
	movq	(%r15), %rax
	movq	4480(%rbx), %rdx
	movq	(%rax), %r12
	movq	%rdx, 39(%r12)
	leaq	39(%r12), %r13
	testb	$1, %dl
	je	.L2470
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -2416(%rbp)
	testl	$262144, %eax
	je	.L2319
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rdx, -2424(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-2416(%rbp), %rcx
	movq	-2424(%rbp), %rdx
	movq	8(%rcx), %rax
.L2319:
	testb	$24, %al
	je	.L2470
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2470
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2470:
	movq	(%r15), %rax
	movq	(%rax), %rax
	movq	47(%rax), %rdx
	cmpl	$67, 59(%rdx)
	je	.L2322
	movabsq	$287762808832, %rcx
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2322
	testb	$1, %al
	jne	.L2883
.L2324:
	movq	(%r15), %rdi
	call	_ZN2v88internal10JSFunction20EnsureFeedbackVectorENS0_6HandleIS1_EE@PLT
.L2322:
	addq	$8, %r15
	cmpq	%r15, %r14
	jne	.L2326
	movq	-2464(%rbp), %r12
	movq	-2472(%rbp), %r13
.L2327:
	movq	(%r12), %rax
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L2884
.L2316:
	movq	7(%rax), %rax
	testb	$1, %al
	je	.L2331
	movq	-1(%rax), %rax
	cmpw	$91, 11(%rax)
	jne	.L2331
.L2330:
	movq	(%r12), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L2885
.L2332:
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L2886
.L2334:
	movq	7(%rax), %rax
	movq	7(%rax), %rax
.L2333:
	movq	15(%rax), %r15
	xorl	%r14d, %r14d
	movq	7(%r15), %rdx
	leaq	7(%r15), %r11
	leaq	15(%r15), %r12
	movq	%rdx, %rax
	sarq	$32, %rax
	testq	%rax, %rax
	jle	.L2331
	movq	%r15, -2416(%rbp)
	movq	%r11, %r15
	jmp	.L2363
	.p2align 4,,10
	.p2align 3
.L2337:
	movq	%rdx, %rax
	addl	$1, %r14d
	addq	$8, %r12
	sarq	$32, %rax
	cmpl	%eax, %r14d
	jge	.L2331
.L2363:
	movq	(%r12), %rax
	testb	$1, %al
	je	.L2337
	movq	-1(%rax), %rax
	cmpw	$160, 11(%rax)
	jne	.L2337
	movq	(%r12), %rax
	movq	-2488(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-2368(%rbp), %rcx
	movq	31(%rcx), %rdx
	testb	$1, %dl
	jne	.L2767
.L2824:
	movq	(%r15), %rdx
	jmp	.L2337
	.p2align 4,,10
	.p2align 3
.L2883:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L2322
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L2324
	jmp	.L2322
	.p2align 4,,10
	.p2align 3
.L2865:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L2395
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L2397
	jmp	.L2395
	.p2align 4,,10
	.p2align 3
.L2876:
	movq	-1(%rdi), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L2289
	movq	%rdx, 23(%rdi)
	leaq	23(%rdi), %rsi
	testb	%al, %al
	jne	.L2293
	movq	%rdx, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L2291
	movq	%rdx, -2464(%rbp)
	movq	%rsi, -2424(%rbp)
	movq	%rdi, -2416(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-2464(%rbp), %rdx
	movq	-2424(%rbp), %rsi
	movq	-2416(%rbp), %rdi
.L2291:
	testb	$24, %al
	je	.L2293
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2293
	jmp	.L2823
	.p2align 4,,10
	.p2align 3
.L2884:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L2316
	jmp	.L2330
	.p2align 4,,10
	.p2align 3
.L2882:
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L2311
	movq	(%r12), %rax
	leaq	-1504(%rbp), %rcx
	movq	7(%rax), %r14
	movq	%rax, -2424(%rbp)
	movq	%r14, %rdx
	movq	%r14, %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %r10
	movl	$32, %edx
	movq	%r10, %rdi
	movq	%r10, -2416(%rbp)
	call	_ZN2v88internal4Heap24NotifyObjectLayoutChangeENS0_10HeapObjectEiRKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	-2424(%rbp), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36912(%rax), %rdx
	movq	%rdx, -1(%r14)
	testq	%rdx, %rdx
	movq	-2416(%rbp), %r10
	jne	.L2887
.L2312:
	leaq	23(%r14), %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r10, %rdi
	movl	$8, %edx
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	jmp	.L2311
	.p2align 4,,10
	.p2align 3
.L2875:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L2284
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2285
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L2286:
	movq	41472(%rbx), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal5Debug27RemoveBreakInfoAndMaybeFreeENS0_6HandleINS0_9DebugInfoEEE@PLT
	jmp	.L2284
	.p2align 4,,10
	.p2align 3
.L2839:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2173
	.p2align 4,,10
	.p2align 3
.L2285:
	movq	41088(%rbx), %r8
	cmpq	%r8, 41096(%rbx)
	je	.L2888
.L2287:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r8)
	jmp	.L2286
	.p2align 4,,10
	.p2align 3
.L2306:
	movq	%r14, %rdi
	movq	%rax, -2424(%rbp)
	movq	%r8, -2416(%rbp)
	call	_ZdlPv@PLT
	movq	-2424(%rbp), %rax
	movq	-2416(%rbp), %r8
	movq	%rax, %r14
	jmp	.L2305
	.p2align 4,,10
	.p2align 3
.L2878:
	movq	%rdx, -2464(%rbp)
	movq	%rsi, -2424(%rbp)
	movq	%rdi, -2416(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-2464(%rbp), %rdx
	movq	-2424(%rbp), %rsi
	movq	-2416(%rbp), %rdi
	jmp	.L2298
	.p2align 4,,10
	.p2align 3
.L2877:
	movq	%r14, %rdi
.L2823:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2293
	.p2align 4,,10
	.p2align 3
.L2879:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2473
.L2881:
	xorl	%edi, %edi
	movl	32(%rdx), %eax
	cmpl	%eax, 32(%r14)
	setl	%dil
	jmp	.L2307
.L2767:
	movq	-1(%rdx), %rdi
	leaq	-1(%rdx), %rsi
	cmpw	$86, 11(%rdi)
	je	.L2889
.L2340:
	movq	(%rsi), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L2824
	cmpl	$-1, %eax
	je	.L2824
	movq	31(%rcx), %rax
	testb	$1, %al
	jne	.L2890
.L2342:
	movslq	67(%rax), %r8
	movq	-2368(%rbp), %rax
	leaq	-1504(%rbp), %rdi
	movq	%r8, -2464(%rbp)
	movl	%r8d, -2424(%rbp)
	movq	%rax, -1504(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-1504(%rbp), %rdx
	movl	$-1, %esi
	movl	47(%rdx), %edx
	movl	-2424(%rbp), %ecx
	movq	-2464(%rbp), %r8
	andl	$268435456, %edx
	movq	-2392(%rbp), %rdx
	cmove	%eax, %esi
	movq	-1992(%rbp), %rax
	testq	%rax, %rax
	jne	.L2345
	jmp	.L2824
	.p2align 4,,10
	.p2align 3
.L2891:
	jne	.L2348
	cmpl	%esi, 36(%rax)
	jl	.L2347
.L2348:
	movq	%rax, %rdx
	movq	16(%rax), %rax
.L2349:
	testq	%rax, %rax
	je	.L2346
.L2345:
	cmpl	32(%rax), %ecx
	jle	.L2891
.L2347:
	movq	24(%rax), %rax
	jmp	.L2349
	.p2align 4,,10
	.p2align 3
.L2413:
	cmpq	-2456(%rbp), %rcx
	je	.L2825
	cmpl	32(%rcx), %eax
	jl	.L2825
	movq	-2408(%rbp), %rax
	movq	(%rax), %rdx
	movl	36(%rcx), %eax
	movq	87(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	movq	%rax, %rdx
	andq	$-3, %rdx
	cmpq	%rdx, -1504(%rbp)
	je	.L2825
	movq	%rdx, (%r14)
	testb	$1, %dl
	je	.L2825
	andq	$-262144, %rax
	movq	8(%rax), %rcx
	movq	%rax, -2424(%rbp)
	testl	$262144, %ecx
	je	.L2418
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rdx, -2440(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-2424(%rbp), %rax
	movq	-2440(%rbp), %rdx
	movq	8(%rax), %rcx
.L2418:
	andl	$24, %ecx
	je	.L2825
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2825
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2825:
	movq	0(%r13), %rdx
	jmp	.L2420
	.p2align 4,,10
	.p2align 3
.L2866:
	movq	-1(%rcx), %rcx
	cmpw	$91, 11(%rcx)
	jne	.L2404
	jmp	.L2406
	.p2align 4,,10
	.p2align 3
.L2859:
	leaq	-1504(%rbp), %r13
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2246
	jmp	.L2248
	.p2align 4,,10
	.p2align 3
.L2892:
	movq	%r13, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2248
.L2246:
	cmpq	32(%rdi), %r12
	jne	.L2892
.L2247:
	cmpb	$0, _ZN2v88internal8LiveEdit22kFrameDropperSupportedE(%rip)
	jne	.L2249
	movq	-2432(%rbp), %rax
	movq	-1992(%rbp), %r13
	movl	$7, (%rax)
	.p2align 4,,10
	.p2align 3
.L2245:
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_115FunctionDataMapE(%rip), %rax
	movq	%rax, -2016(%rbp)
	testq	%r13, %r13
	je	.L2454
.L2449:
	leaq	-2008(%rbp), %r12
.L2456:
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal12_GLOBAL__N_112FunctionDataEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	80(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L2452
	call	_ZdlPv@PLT
.L2452:
	movq	56(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2453
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L2454
.L2455:
	movq	%rbx, %r13
	jmp	.L2456
	.p2align 4,,10
	.p2align 3
.L2453:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2455
.L2454:
	movq	-2064(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L2450
	.p2align 4,,10
	.p2align 3
.L2451:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2451
.L2450:
	movq	-2072(%rbp), %rax
	movq	-2080(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-2080(%rbp), %rdi
	movq	$0, -2056(%rbp)
	movq	$0, -2064(%rbp)
	cmpq	-2512(%rbp), %rdi
	je	.L2457
	call	_ZdlPv@PLT
.L2457:
	movq	-2128(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L2462
	.p2align 4,,10
	.p2align 3
.L2459:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2459
.L2462:
	movq	-2136(%rbp), %rax
	movq	-2144(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-2144(%rbp), %rdi
	movq	$0, -2120(%rbp)
	movq	$0, -2128(%rbp)
	cmpq	-2504(%rbp), %rdi
	je	.L2460
	call	_ZdlPv@PLT
.L2460:
	movq	-2192(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L2466
	.p2align 4,,10
	.p2align 3
.L2463:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2463
.L2466:
	movq	-2200(%rbp), %rax
	movq	-2208(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-2208(%rbp), %rdi
	movq	$0, -2184(%rbp)
	movq	$0, -2192(%rbp)
	cmpq	-2496(%rbp), %rdi
	je	.L2180
	call	_ZdlPv@PLT
	jmp	.L2180
	.p2align 4,,10
	.p2align 3
.L2863:
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2860:
	movq	-2432(%rbp), %rax
	movq	-1992(%rbp), %r13
	movl	$0, (%rax)
	jmp	.L2245
	.p2align 4,,10
	.p2align 3
.L2508:
	movq	%rsi, %r13
	movl	$3, %eax
.L2244:
	movq	-2432(%rbp), %rcx
	movl	%eax, (%rcx)
	jmp	.L2245
	.p2align 4,,10
	.p2align 3
.L2509:
	movq	%rsi, %r13
	movl	$4, %eax
	jmp	.L2244
	.p2align 4,,10
	.p2align 3
.L2510:
	movq	%rsi, %r13
	movl	$2, %eax
	jmp	.L2244
	.p2align 4,,10
	.p2align 3
.L2887:
	testb	$1, %dl
	je	.L2312
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2312
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-2416(%rbp), %r10
	jmp	.L2312
	.p2align 4,,10
	.p2align 3
.L2858:
	movq	%rsi, %r13
	movl	$5, %eax
	jmp	.L2244
	.p2align 4,,10
	.p2align 3
.L2867:
	movq	-2416(%rbp), %rbx
.L2367:
	cmpq	$0, -2384(%rbp)
	jne	.L2893
.L2402:
	movq	-2408(%rbp), %rbx
	movq	-2400(%rbp), %rax
	leaq	-2256(%rbp), %r12
	movq	(%rbx), %rdx
	movq	(%rax), %rcx
	movslq	67(%rdx), %rdx
	movslq	67(%rcx), %rax
	salq	$32, %rdx
	salq	$32, %rax
	movq	%rdx, 63(%rcx)
	movq	(%rbx), %rdx
	movq	%rax, 63(%rdx)
	movq	-2432(%rbp), %rax
	movq	%rbx, 8(%rax)
	movq	-2240(%rbp), %rbx
	movl	$0, (%rax)
	testq	%rbx, %rbx
	je	.L2429
.L2426:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2426
.L2429:
	movq	-1992(%rbp), %r13
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_115FunctionDataMapE(%rip), %rax
	leaq	-2008(%rbp), %r12
	movq	%rax, -2016(%rbp)
	testq	%r13, %r13
	je	.L2427
.L2428:
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal12_GLOBAL__N_112FunctionDataEESt10_Select1stIS7_ESt4lessIS1_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	80(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L2432
	call	_ZdlPv@PLT
.L2432:
	movq	56(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2433
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L2427
.L2434:
	movq	%rbx, %r13
	jmp	.L2428
	.p2align 4,,10
	.p2align 3
.L2433:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2434
.L2427:
	movq	-2064(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L2430
	.p2align 4,,10
	.p2align 3
.L2431:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2431
.L2430:
	movq	-2072(%rbp), %rax
	movq	-2080(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-2080(%rbp), %rdi
	movq	$0, -2056(%rbp)
	movq	$0, -2064(%rbp)
	cmpq	-2512(%rbp), %rdi
	je	.L2435
	call	_ZdlPv@PLT
.L2435:
	movq	-2128(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L2440
	.p2align 4,,10
	.p2align 3
.L2437:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2437
.L2440:
	movq	-2136(%rbp), %rax
	movq	-2144(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-2144(%rbp), %rdi
	movq	$0, -2120(%rbp)
	movq	$0, -2128(%rbp)
	cmpq	-2504(%rbp), %rdi
	je	.L2438
	call	_ZdlPv@PLT
.L2438:
	movq	-2192(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L2466
	.p2align 4,,10
	.p2align 3
.L2441:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2441
	jmp	.L2466
.L2885:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L2332
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L2332
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L2332
	movq	31(%rdx), %rax
	jmp	.L2333
.L2868:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L2408
	movq	39(%rax), %rcx
	testb	$1, %cl
	je	.L2408
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L2408
	movq	31(%rax), %rax
	jmp	.L2409
	.p2align 4,,10
	.p2align 3
.L2886:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L2334
	movq	7(%rax), %rax
	jmp	.L2333
.L2248:
	xorl	%edi, %edi
	jmp	.L2247
.L2249:
	leaq	-2256(%rbp), %r12
	pxor	%xmm0, %xmm0
	movq	$0, -2240(%rbp)
	movq	%r12, %rsi
	movaps	%xmm0, -2256(%rbp)
	call	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_6HandleINS0_18SharedFunctionInfoEEESaIS5_EE@PLT
	movq	-2256(%rbp), %r12
	movq	-2248(%rbp), %r15
	cmpq	%r15, %r12
	je	.L2250
	leaq	-2368(%rbp), %rax
	movq	%rbx, -2424(%rbp)
	leaq	-2376(%rbp), %r14
	movq	%rax, -2416(%rbp)
	jmp	.L2268
	.p2align 4,,10
	.p2align 3
.L2253:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L2894
.L2268:
	movq	(%r12), %rax
	movq	%r14, %rdi
	movq	(%rax), %rax
	movq	%rax, -2376(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-2376(%rbp), %rcx
	movq	31(%rcx), %rdx
	testb	$1, %dl
	je	.L2253
	movq	-1(%rdx), %rdi
	leaq	-1(%rdx), %rsi
	cmpw	$86, 11(%rdi)
	je	.L2895
.L2252:
	movq	(%rsi), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L2253
	cmpl	$-1, %eax
	je	.L2253
	movq	31(%rcx), %rax
	testb	$1, %al
	jne	.L2896
.L2255:
	movslq	67(%rax), %r13
	movq	-2416(%rbp), %rdi
	movq	-2376(%rbp), %rax
	movl	%r13d, %ebx
	movq	%rax, -2368(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-2368(%rbp), %rdx
	movl	$-1, %esi
	movl	47(%rdx), %edx
	movq	-1992(%rbp), %r10
	andl	$268435456, %edx
	cmove	%eax, %esi
	testq	%r10, %r10
	je	.L2253
	movq	-2392(%rbp), %rdx
	movq	%r10, %rax
	jmp	.L2258
	.p2align 4,,10
	.p2align 3
.L2897:
	jne	.L2261
	cmpl	%esi, 36(%rax)
	jl	.L2260
.L2261:
	movq	%rax, %rdx
	movq	16(%rax), %rax
.L2262:
	testq	%rax, %rax
	je	.L2259
.L2258:
	cmpl	32(%rax), %ebx
	jle	.L2897
.L2260:
	movq	24(%rax), %rax
	jmp	.L2262
.L2895:
	movq	23(%rdx), %rsi
	testb	$1, %sil
	je	.L2253
	subq	$1, %rsi
	jmp	.L2252
	.p2align 4,,10
	.p2align 3
.L2869:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L2410
	movq	(%rdx), %rax
	jmp	.L2409
.L2259:
	cmpq	-2392(%rbp), %rdx
	je	.L2253
	cmpl	32(%rdx), %r13d
	jl	.L2253
	jne	.L2263
	cmpl	%esi, 36(%rdx)
	jg	.L2253
.L2263:
	movq	40(%rdx), %r8
	movq	-2136(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	movq	-2144(%rbp), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2253
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2898:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2253
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L2253
.L2265:
	cmpq	%rsi, %r8
	jne	.L2898
	movq	16(%rcx), %rax
	movq	40(%rax), %rax
	cmpq	$0, 192(%rax)
	je	.L2253
	movq	-2432(%rbp), %rax
	movq	-2256(%rbp), %rdi
	movq	%r10, %r13
	movl	$6, (%rax)
	testq	%rdi, %rdi
	je	.L2899
	call	_ZdlPv@PLT
	movq	-1992(%rbp), %r13
	jmp	.L2245
.L2894:
	movq	-2424(%rbp), %rbx
.L2250:
	movq	-2256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2227
	call	_ZdlPv@PLT
	jmp	.L2227
.L2888:
	movq	%rbx, %rdi
	movq	%rsi, -2416(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-2416(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L2287
.L2893:
	leaq	-1504(%rbp), %r13
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	testq	%rsi, %rsi
	jne	.L2422
	jmp	.L2402
	.p2align 4,,10
	.p2align 3
.L2424:
	movq	%r13, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L2402
.L2422:
	movq	32(%rsi), %rax
	cmpq	%rax, -2384(%rbp)
	jne	.L2424
	movq	41472(%rbx), %rdi
	call	_ZN2v88internal5Debug20ScheduleFrameRestartEPNS0_10StackFrameE@PLT
	movq	-2432(%rbp), %rax
	movb	$1, 4(%rax)
	jmp	.L2402
.L2889:
	movq	23(%rdx), %rsi
	testb	$1, %sil
	je	.L2824
	subq	$1, %rsi
	jmp	.L2340
.L2896:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L2255
	movq	23(%rax), %rax
	jmp	.L2255
.L2346:
	cmpq	-2392(%rbp), %rdx
	je	.L2824
	cmpl	32(%rdx), %r8d
	jl	.L2824
	jne	.L2350
	cmpl	%esi, 36(%rdx)
	jg	.L2824
.L2350:
	movq	40(%rdx), %r8
	movq	-2136(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	movq	-2144(%rbp), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2824
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2352
	.p2align 4,,10
	.p2align 3
.L2900:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2824
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L2824
.L2352:
	cmpq	%rsi, %r8
	jne	.L2900
	movq	-2408(%rbp), %rax
	movq	16(%rcx), %rcx
	movq	(%rax), %rax
	movq	%rcx, %rdi
	movq	%rcx, -2424(%rbp)
	movslq	67(%rax), %r8
	movq	%r8, -2472(%rbp)
	movl	%r8d, -2464(%rbp)
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	-2424(%rbp), %rcx
	movl	$-1, %esi
	movq	-2472(%rbp), %r8
	movl	28(%rcx), %edx
	movq	-2392(%rbp), %rcx
	testl	%edx, %edx
	movl	-2464(%rbp), %edx
	cmovne	%eax, %esi
	movq	-1992(%rbp), %rax
	testq	%rax, %rax
	jne	.L2354
	jmp	.L2824
	.p2align 4,,10
	.p2align 3
.L2901:
	jne	.L2357
	cmpl	36(%rax), %esi
	jg	.L2356
.L2357:
	movq	%rax, %rcx
	movq	16(%rax), %rax
.L2358:
	testq	%rax, %rax
	je	.L2355
.L2354:
	cmpl	32(%rax), %edx
	jle	.L2901
.L2356:
	movq	24(%rax), %rax
	jmp	.L2358
.L2355:
	cmpq	-2392(%rbp), %rcx
	je	.L2824
	cmpl	32(%rcx), %r8d
	jl	.L2824
	jne	.L2359
	cmpl	36(%rcx), %esi
	jl	.L2824
.L2359:
	movq	48(%rcx), %rax
	testq	%rax, %rax
	je	.L2824
	movq	(%rax), %rdx
	movq	%rdx, (%r12)
	testb	$1, %dl
	je	.L2824
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -2424(%rbp)
	testl	$262144, %eax
	je	.L2361
	movq	-2416(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdx, -2464(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-2424(%rbp), %rcx
	movq	-2464(%rbp), %rdx
	movq	8(%rcx), %rax
.L2361:
	testb	$24, %al
	je	.L2824
	movq	-2416(%rbp), %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2824
	movq	-2416(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2824
.L2890:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L2342
	movq	23(%rax), %rax
	jmp	.L2342
.L2899:
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_115FunctionDataMapE(%rip), %rax
	movq	%rax, -2016(%rbp)
	jmp	.L2449
.L2857:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2835:
	call	__stack_chk_fail@PLT
.L2514:
	movq	-2392(%rbp), %rdx
	movl	$1, %edi
	jmp	.L2224
	.cfi_endproc
.LFE20569:
	.size	_ZN2v88internal8LiveEdit11PatchScriptEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6StringEEEbPNS_5debug14LiveEditResultE, .-_ZN2v88internal8LiveEdit11PatchScriptEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6StringEEEbPNS_5debug14LiveEditResultE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8LiveEdit11PatchScriptEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6StringEEEbPNS_5debug14LiveEditResultE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8LiveEdit11PatchScriptEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6StringEEEbPNS_5debug14LiveEditResultE, @function
_GLOBAL__sub_I__ZN2v88internal8LiveEdit11PatchScriptEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6StringEEEbPNS_5debug14LiveEditResultE:
.LFB27460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27460:
	.size	_GLOBAL__sub_I__ZN2v88internal8LiveEdit11PatchScriptEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6StringEEEbPNS_5debug14LiveEditResultE, .-_GLOBAL__sub_I__ZN2v88internal8LiveEdit11PatchScriptEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6StringEEEbPNS_5debug14LiveEditResultE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8LiveEdit11PatchScriptEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEENS4_INS0_6StringEEEbPNS_5debug14LiveEditResultE
	.section	.data.rel.ro.local._ZTVN2v88internal12_GLOBAL__N_118TokensCompareInputE,"aw"
	.align 8
	.type	_ZTVN2v88internal12_GLOBAL__N_118TokensCompareInputE, @object
	.size	_ZTVN2v88internal12_GLOBAL__N_118TokensCompareInputE, 56
_ZTVN2v88internal12_GLOBAL__N_118TokensCompareInputE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12_GLOBAL__N_118TokensCompareInput10GetLength1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_118TokensCompareInput10GetLength2Ev
	.quad	_ZN2v88internal12_GLOBAL__N_118TokensCompareInput6EqualsEii
	.quad	_ZN2v88internal12_GLOBAL__N_118TokensCompareInputD1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_118TokensCompareInputD0Ev
	.section	.data.rel.ro.local._ZTVN2v88internal12_GLOBAL__N_119TokensCompareOutputE,"aw"
	.align 8
	.type	_ZTVN2v88internal12_GLOBAL__N_119TokensCompareOutputE, @object
	.size	_ZTVN2v88internal12_GLOBAL__N_119TokensCompareOutputE, 40
_ZTVN2v88internal12_GLOBAL__N_119TokensCompareOutputE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12_GLOBAL__N_119TokensCompareOutput8AddChunkEiiii
	.quad	_ZN2v88internal12_GLOBAL__N_119TokensCompareOutputD1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_119TokensCompareOutputD0Ev
	.section	.data.rel.ro.local._ZTVN2v88internal12_GLOBAL__N_121LineArrayCompareInputE,"aw"
	.align 8
	.type	_ZTVN2v88internal12_GLOBAL__N_121LineArrayCompareInputE, @object
	.size	_ZTVN2v88internal12_GLOBAL__N_121LineArrayCompareInputE, 72
_ZTVN2v88internal12_GLOBAL__N_121LineArrayCompareInputE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput10GetLength1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput10GetLength2Ev
	.quad	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput6EqualsEii
	.quad	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInputD1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInputD0Ev
	.quad	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput12SetSubrange1Eii
	.quad	_ZN2v88internal12_GLOBAL__N_121LineArrayCompareInput12SetSubrange2Eii
	.section	.data.rel.ro.local._ZTVN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputE,"aw"
	.align 8
	.type	_ZTVN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputE, @object
	.size	_ZTVN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputE, 56
_ZTVN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput8AddChunkEiiii
	.quad	_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputD1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutputD0Ev
	.quad	_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput12SetSubrange1Eii
	.quad	_ZN2v88internal12_GLOBAL__N_132TokenizingLineArrayCompareOutput12SetSubrange2Eii
	.section	.data.rel.ro.local._ZTVN2v88internal12_GLOBAL__N_115FunctionDataMapE,"aw"
	.align 8
	.type	_ZTVN2v88internal12_GLOBAL__N_115FunctionDataMapE, @object
	.size	_ZTVN2v88internal12_GLOBAL__N_115FunctionDataMapE, 40
_ZTVN2v88internal12_GLOBAL__N_115FunctionDataMapE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12_GLOBAL__N_115FunctionDataMap11VisitThreadEPNS0_7IsolateEPNS0_14ThreadLocalTopE
	.quad	_ZN2v88internal12_GLOBAL__N_115FunctionDataMapD1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_115FunctionDataMapD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC5:
	.long	1065353216
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
