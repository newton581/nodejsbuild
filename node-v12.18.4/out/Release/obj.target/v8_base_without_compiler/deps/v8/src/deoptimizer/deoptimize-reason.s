	.file	"deoptimize-reason.cc"
	.text
	.section	.rodata._ZN2v88internallsERSoNS0_16DeoptimizeReasonE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ArrayBufferWasDetached"
.LC1:
	.string	"BigIntTooBig"
.LC2:
	.string	"CowArrayElementsChanged"
.LC3:
	.string	"CouldNotGrowElements"
.LC4:
	.string	"DeoptimizeNow"
.LC5:
	.string	"DivisionByZero"
.LC6:
	.string	"Hole"
.LC7:
	.string	"InstanceMigrationFailed"
	.section	.rodata._ZN2v88internallsERSoNS0_16DeoptimizeReasonE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"InsufficientTypeFeedbackForCall"
	.align 8
.LC9:
	.string	"InsufficientTypeFeedbackForConstruct"
	.align 8
.LC10:
	.string	"InsufficientTypeFeedbackForForIn"
	.align 8
.LC11:
	.string	"InsufficientTypeFeedbackForBinaryOperation"
	.align 8
.LC12:
	.string	"InsufficientTypeFeedbackForCompareOperation"
	.align 8
.LC13:
	.string	"InsufficientTypeFeedbackForGenericNamedAccess"
	.align 8
.LC14:
	.string	"InsufficientTypeFeedbackForGenericKeyedAccess"
	.align 8
.LC15:
	.string	"InsufficientTypeFeedbackForUnaryOperation"
	.section	.rodata._ZN2v88internallsERSoNS0_16DeoptimizeReasonE.str1.1
.LC16:
	.string	"LostPrecision"
.LC17:
	.string	"LostPrecisionOrNaN"
.LC18:
	.string	"MinusZero"
.LC19:
	.string	"NaN"
.LC20:
	.string	"NoCache"
.LC21:
	.string	"NotAHeapNumber"
.LC22:
	.string	"NotAJavaScriptObject"
	.section	.rodata._ZN2v88internallsERSoNS0_16DeoptimizeReasonE.str1.8
	.align 8
.LC23:
	.string	"NotAJavaScriptObjectOrNullOrUndefined"
	.section	.rodata._ZN2v88internallsERSoNS0_16DeoptimizeReasonE.str1.1
.LC24:
	.string	"NotANumberOrOddball"
.LC25:
	.string	"NotASmi"
.LC26:
	.string	"NotAString"
.LC27:
	.string	"NotASymbol"
.LC28:
	.string	"OutOfBounds"
.LC29:
	.string	"Overflow"
.LC30:
	.string	"ReceiverNotAGlobalProxy"
.LC31:
	.string	"Smi"
.LC32:
	.string	"Unknown"
.LC33:
	.string	"ValueMismatch"
.LC34:
	.string	"WrongCallTarget"
.LC35:
	.string	"WrongEnumIndices"
.LC36:
	.string	"WrongInstanceType"
.LC37:
	.string	"WrongMap"
.LC38:
	.string	"WrongName"
.LC39:
	.string	"WrongValue"
.LC40:
	.string	"NoInitialElement"
.LC41:
	.string	"unreachable code"
	.section	.text._ZN2v88internallsERSoNS0_16DeoptimizeReasonE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoNS0_16DeoptimizeReasonE
	.type	_ZN2v88internallsERSoNS0_16DeoptimizeReasonE, @function
_ZN2v88internallsERSoNS0_16DeoptimizeReasonE:
.LFB3140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpb	$40, %sil
	ja	.L2
	leaq	.L4(%rip), %rcx
	movzbl	%sil, %edx
	movq	%rdi, %r12
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internallsERSoNS0_16DeoptimizeReasonE,"a",@progbits
	.align 4
	.align 4
.L4:
	.long	.L44-.L4
	.long	.L43-.L4
	.long	.L42-.L4
	.long	.L41-.L4
	.long	.L40-.L4
	.long	.L39-.L4
	.long	.L38-.L4
	.long	.L37-.L4
	.long	.L36-.L4
	.long	.L35-.L4
	.long	.L34-.L4
	.long	.L33-.L4
	.long	.L32-.L4
	.long	.L31-.L4
	.long	.L30-.L4
	.long	.L29-.L4
	.long	.L28-.L4
	.long	.L27-.L4
	.long	.L26-.L4
	.long	.L25-.L4
	.long	.L24-.L4
	.long	.L23-.L4
	.long	.L22-.L4
	.long	.L21-.L4
	.long	.L20-.L4
	.long	.L19-.L4
	.long	.L18-.L4
	.long	.L17-.L4
	.long	.L16-.L4
	.long	.L15-.L4
	.long	.L14-.L4
	.long	.L13-.L4
	.long	.L12-.L4
	.long	.L11-.L4
	.long	.L10-.L4
	.long	.L9-.L4
	.long	.L8-.L4
	.long	.L7-.L4
	.long	.L6-.L4
	.long	.L5-.L4
	.long	.L3-.L4
	.section	.text._ZN2v88internallsERSoNS0_16DeoptimizeReasonE
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$10, %edx
	leaq	.LC39(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L45:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	$9, %edx
	leaq	.LC38(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$8, %edx
	leaq	.LC37(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$17, %edx
	leaq	.LC36(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$16, %edx
	leaq	.LC35(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$15, %edx
	leaq	.LC34(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$13, %edx
	leaq	.LC33(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$7, %edx
	leaq	.LC32(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$3, %edx
	leaq	.LC31(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$23, %edx
	leaq	.LC30(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$8, %edx
	leaq	.LC29(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$11, %edx
	leaq	.LC28(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$10, %edx
	leaq	.LC27(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$10, %edx
	leaq	.LC26(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$7, %edx
	leaq	.LC25(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$19, %edx
	leaq	.LC24(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$37, %edx
	leaq	.LC23(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$20, %edx
	leaq	.LC22(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$14, %edx
	leaq	.LC21(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$7, %edx
	leaq	.LC20(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$3, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$9, %edx
	leaq	.LC18(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$18, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$13, %edx
	leaq	.LC16(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$41, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$45, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$45, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$43, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$42, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$32, %edx
	leaq	.LC10(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$36, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$31, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$23, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$4, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$14, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$13, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$20, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$23, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L43:
	movl	$12, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$22, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$16, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L45
.L2:
	leaq	.LC41(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE3140:
	.size	_ZN2v88internallsERSoNS0_16DeoptimizeReasonE, .-_ZN2v88internallsERSoNS0_16DeoptimizeReasonE
	.section	.text._ZN2v88internal10hash_valueENS0_16DeoptimizeReasonE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal10hash_valueENS0_16DeoptimizeReasonE
	.type	_ZN2v88internal10hash_valueENS0_16DeoptimizeReasonE, @function
_ZN2v88internal10hash_valueENS0_16DeoptimizeReasonE:
.LFB3141:
	.cfi_startproc
	endbr64
	movzbl	%dil, %eax
	ret
	.cfi_endproc
.LFE3141:
	.size	_ZN2v88internal10hash_valueENS0_16DeoptimizeReasonE, .-_ZN2v88internal10hash_valueENS0_16DeoptimizeReasonE
	.section	.text._ZN2v88internal24DeoptimizeReasonToStringENS0_16DeoptimizeReasonE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24DeoptimizeReasonToStringENS0_16DeoptimizeReasonE
	.type	_ZN2v88internal24DeoptimizeReasonToStringENS0_16DeoptimizeReasonE, @function
_ZN2v88internal24DeoptimizeReasonToStringENS0_16DeoptimizeReasonE:
.LFB3142:
	.cfi_startproc
	endbr64
	movzbl	%dil, %edi
	leaq	_ZZN2v88internal24DeoptimizeReasonToStringENS0_16DeoptimizeReasonEE24kDeoptimizeReasonStrings(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	ret
	.cfi_endproc
.LFE3142:
	.size	_ZN2v88internal24DeoptimizeReasonToStringENS0_16DeoptimizeReasonE, .-_ZN2v88internal24DeoptimizeReasonToStringENS0_16DeoptimizeReasonE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC42:
	.string	"array buffer was detached"
.LC43:
	.string	"BigInt too big"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC44:
	.string	"copy-on-write array's elements changed"
	.section	.rodata.str1.1
.LC45:
	.string	"failed to grow elements store"
.LC46:
	.string	"%_DeoptimizeNow"
.LC47:
	.string	"division by zero"
.LC48:
	.string	"hole"
.LC49:
	.string	"instance migration failed"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"Insufficient type feedback for call"
	.align 8
.LC51:
	.string	"Insufficient type feedback for construct"
	.align 8
.LC52:
	.string	"Insufficient type feedback for for-in"
	.align 8
.LC53:
	.string	"Insufficient type feedback for binary operation"
	.align 8
.LC54:
	.string	"Insufficient type feedback for compare operation"
	.align 8
.LC55:
	.string	"Insufficient type feedback for generic named access"
	.align 8
.LC56:
	.string	"Insufficient type feedback for generic keyed access"
	.align 8
.LC57:
	.string	"Insufficient type feedback for unary operation"
	.section	.rodata.str1.1
.LC58:
	.string	"lost precision"
.LC59:
	.string	"lost precision or NaN"
.LC60:
	.string	"minus zero"
.LC61:
	.string	"no cache"
.LC62:
	.string	"not a heap number"
.LC63:
	.string	"not a JavaScript object"
	.section	.rodata.str1.8
	.align 8
.LC64:
	.string	"not a JavaScript object, Null or Undefined"
	.section	.rodata.str1.1
.LC65:
	.string	"not a Number or Oddball"
.LC66:
	.string	"not a Smi"
.LC67:
	.string	"not a String"
.LC68:
	.string	"not a Symbol"
.LC69:
	.string	"out of bounds"
.LC70:
	.string	"overflow"
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"receiver was not a global proxy"
	.section	.rodata.str1.1
.LC72:
	.string	"(unknown)"
.LC73:
	.string	"value mismatch"
.LC74:
	.string	"wrong call target"
.LC75:
	.string	"wrong enum indices"
.LC76:
	.string	"wrong instance type"
.LC77:
	.string	"wrong map"
.LC78:
	.string	"wrong name"
.LC79:
	.string	"wrong value"
.LC80:
	.string	"no initial element"
	.section	.data.rel.ro.local._ZZN2v88internal24DeoptimizeReasonToStringENS0_16DeoptimizeReasonEE24kDeoptimizeReasonStrings,"aw"
	.align 32
	.type	_ZZN2v88internal24DeoptimizeReasonToStringENS0_16DeoptimizeReasonEE24kDeoptimizeReasonStrings, @object
	.size	_ZZN2v88internal24DeoptimizeReasonToStringENS0_16DeoptimizeReasonEE24kDeoptimizeReasonStrings, 328
_ZZN2v88internal24DeoptimizeReasonToStringENS0_16DeoptimizeReasonEE24kDeoptimizeReasonStrings:
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.quad	.LC19
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.quad	.LC71
	.quad	.LC31
	.quad	.LC72
	.quad	.LC73
	.quad	.LC74
	.quad	.LC75
	.quad	.LC76
	.quad	.LC77
	.quad	.LC78
	.quad	.LC79
	.quad	.LC80
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
