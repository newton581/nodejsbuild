	.file	"template-objects.cc"
	.text
	.section	.text._ZN2v88internal20CachedTemplateObject3NewEPNS0_7IsolateEiNS0_6HandleINS0_7JSArrayEEENS4_INS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20CachedTemplateObject3NewEPNS0_7IsolateEiNS0_6HandleINS0_7JSArrayEEENS4_INS0_10HeapObjectEEE
	.type	_ZN2v88internal20CachedTemplateObject3NewEPNS0_7IsolateEiNS0_6HandleINS0_7JSArrayEEENS4_INS0_10HeapObjectEEE, @function
_ZN2v88internal20CachedTemplateObject3NewEPNS0_7IsolateEiNS0_6HandleINS0_7JSArrayEEENS4_INS0_10HeapObjectEEE:
.LFB17864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$1, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	movl	$103, %esi
	salq	$32, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	%rbx, 7(%rax)
	movq	(%r12), %r15
	movq	0(%r13), %r13
	movq	%r13, 15(%r15)
	testb	$1, %r13b
	je	.L9
	movq	%r13, %rbx
	leaq	15(%r15), %rsi
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L24
	testb	$24, %al
	je	.L9
.L29:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L25
	.p2align 4,,10
	.p2align 3
.L9:
	movq	(%r12), %r15
	movq	(%r14), %r13
	movq	%r13, 23(%r15)
	leaq	23(%r15), %r14
	testb	$1, %r13b
	je	.L8
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L26
	testb	$24, %al
	je	.L8
.L28:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L27
.L8:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L28
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L29
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L8
	.cfi_endproc
.LFE17864:
	.size	_ZN2v88internal20CachedTemplateObject3NewEPNS0_7IsolateEiNS0_6HandleINS0_7JSArrayEEENS4_INS0_10HeapObjectEEE, .-_ZN2v88internal20CachedTemplateObject3NewEPNS0_7IsolateEiNS0_6HandleINS0_7JSArrayEEENS4_INS0_10HeapObjectEEE
	.section	.text._ZN2v88internal25TemplateObjectDescription17GetTemplateObjectEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_IS1_EENS4_INS0_18SharedFunctionInfoEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25TemplateObjectDescription17GetTemplateObjectEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_IS1_EENS4_INS0_18SharedFunctionInfoEEEi
	.type	_ZN2v88internal25TemplateObjectDescription17GetTemplateObjectEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_IS1_EENS4_INS0_18SharedFunctionInfoEEEi, @function
_ZN2v88internal25TemplateObjectDescription17GetTemplateObjectEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_IS1_EENS4_INS0_18SharedFunctionInfoEEEi:
.LFB17863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -144(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	1519(%rax), %rdx
	cmpq	%rdx, 88(%rdi)
	je	.L68
	movq	1519(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L33
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L32:
	movq	0(%r13), %rax
	movq	%rax, -96(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo4HashEv@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	movl	%eax, %ebx
	movq	(%r14), %rax
	movl	%ebx, %edx
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal19ObjectHashTableBaseINS0_18EphemeronHashTableENS0_23EphemeronHashTableShapeEE6LookupENS0_6HandleINS0_6ObjectEEEi@PLT
	testb	$1, %al
	jne	.L69
	.p2align 4,,10
	.p2align 3
.L35:
	cmpl	%r12d, 11(%rax)
	je	.L70
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L35
.L69:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37496(%rdx)
	jne	.L35
	movq	-120(%rbp), %rax
	movq	41112(%r15), %rdi
	movq	(%rax), %rax
	movq	7(%rax), %r8
	testq	%rdi, %rdi
	je	.L71
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r8
	movq	%rax, %rsi
.L44:
	movslq	11(%r8), %rcx
	movq	%r15, %rdi
	movl	$1, %r8d
	movl	$2, %edx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	41112(%r15), %rdi
	movq	%rax, -136(%rbp)
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	15(%rax), %r8
	testq	%rdi, %rdi
	je	.L46
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r8
	movq	%rax, %rsi
.L47:
	movslq	11(%r8), %rcx
	movl	$2, %edx
	movq	%r15, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	-136(%rbp), %rdi
	xorl	%edx, %edx
	movl	$5, %esi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal10JSReceiver17SetIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesENS0_11ShouldThrowE@PLT
	testb	%al, %al
	je	.L72
.L49:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rcx
	movq	%r15, %rdi
	leaq	3144(%r15), %rdx
	movq	-120(%rbp), %rsi
	movl	$1, %r8d
	movq	$0, -80(%rbp)
	movq	%rax, -88(%rbp)
	movzbl	-96(%rbp), %eax
	movq	$0, -72(%rbp)
	andl	$-64, %eax
	movq	$0, -64(%rbp)
	orl	$42, %eax
	movb	%al, -96(%rbp)
	call	_ZN2v88internal7JSArray17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L73
.L50:
	movq	-120(%rbp), %rdi
	xorl	%edx, %edx
	movl	$5, %esi
	call	_ZN2v88internal10JSReceiver17SetIntegrityLevelENS0_6HandleIS1_EENS0_18PropertyAttributesENS0_11ShouldThrowE@PLT
	testb	%al, %al
	je	.L74
.L51:
	movq	(%r14), %rax
	leaq	-104(%rbp), %rdi
	movq	%r13, %rsi
	movl	%ebx, %edx
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal19ObjectHashTableBaseINS0_18EphemeronHashTableENS0_23EphemeronHashTableShapeEE6LookupENS0_6HandleINS0_6ObjectEEEi@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L52
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L53:
	movq	-120(%rbp), %rdx
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal20CachedTemplateObject3NewEPNS0_7IsolateEiNS0_6HandleINS0_7JSArrayEEENS4_INS0_10HeapObjectEEE
	movl	%ebx, %r8d
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rax, %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal19ObjectHashTableBaseINS0_18EphemeronHashTableENS0_23EphemeronHashTableShapeEE3PutEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_6ObjectEEESA_i@PLT
	movq	-144(%rbp), %rbx
	movq	(%rax), %r12
	movq	(%rbx), %r13
	movq	%r12, 1519(%r13)
	leaq	1519(%r13), %r14
	testb	$1, %r12b
	je	.L58
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L56
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L56:
	testb	$24, %al
	je	.L58
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L75
	.p2align 4,,10
	.p2align 3
.L58:
	movq	-120(%rbp), %rax
.L42:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L76
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L77
.L34:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L52:
	movq	41088(%r15), %rcx
	cmpq	41096(%r15), %rcx
	je	.L78
.L54:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L46:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L79
.L48:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r8, (%rsi)
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L71:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L80
.L45:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r8, (%rsi)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L68:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal9HashTableINS0_18EphemeronHashTableENS0_23EphemeronHashTableShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	%rax, %r14
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L70:
	movq	41112(%r15), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L39
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L39:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L81
.L41:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L72:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L74:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L73:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L80:
	movq	%r15, %rdi
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%r15, %rdi
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L78:
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L77:
	movq	%r15, %rdi
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L34
.L81:
	movq	%r15, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	jmp	.L41
.L76:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17863:
	.size	_ZN2v88internal25TemplateObjectDescription17GetTemplateObjectEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_IS1_EENS4_INS0_18SharedFunctionInfoEEEi, .-_ZN2v88internal25TemplateObjectDescription17GetTemplateObjectEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_IS1_EENS4_INS0_18SharedFunctionInfoEEEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal25TemplateObjectDescription17GetTemplateObjectEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_IS1_EENS4_INS0_18SharedFunctionInfoEEEi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal25TemplateObjectDescription17GetTemplateObjectEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_IS1_EENS4_INS0_18SharedFunctionInfoEEEi, @function
_GLOBAL__sub_I__ZN2v88internal25TemplateObjectDescription17GetTemplateObjectEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_IS1_EENS4_INS0_18SharedFunctionInfoEEEi:
.LFB21611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21611:
	.size	_GLOBAL__sub_I__ZN2v88internal25TemplateObjectDescription17GetTemplateObjectEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_IS1_EENS4_INS0_18SharedFunctionInfoEEEi, .-_GLOBAL__sub_I__ZN2v88internal25TemplateObjectDescription17GetTemplateObjectEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_IS1_EENS4_INS0_18SharedFunctionInfoEEEi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal25TemplateObjectDescription17GetTemplateObjectEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEENS4_IS1_EENS4_INS0_18SharedFunctionInfoEEEi
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
