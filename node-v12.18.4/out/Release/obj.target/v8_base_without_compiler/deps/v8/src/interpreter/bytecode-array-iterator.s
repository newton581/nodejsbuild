	.file	"bytecode-array-iterator.cc"
	.text
	.section	.text._ZN2v88internal11interpreter21BytecodeArrayIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter21BytecodeArrayIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EE
	.type	_ZN2v88internal11interpreter21BytecodeArrayIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EE, @function
_ZN2v88internal11interpreter21BytecodeArrayIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EE:
.LFB17793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	leaq	-16(%rbp), %rsi
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal11interpreter21BytecodeArrayAccessorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEi@PLT
	movq	-16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1
	movq	(%rdi), %rax
	call	*72(%rax)
.L1:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L9
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17793:
	.size	_ZN2v88internal11interpreter21BytecodeArrayIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EE, .-_ZN2v88internal11interpreter21BytecodeArrayIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EE
	.globl	_ZN2v88internal11interpreter21BytecodeArrayIteratorC1ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EE
	.set	_ZN2v88internal11interpreter21BytecodeArrayIteratorC1ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EE,_ZN2v88internal11interpreter21BytecodeArrayIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EE
	.section	.text._ZN2v88internal11interpreter21BytecodeArrayIteratorC2ENS0_6HandleINS0_13BytecodeArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter21BytecodeArrayIteratorC2ENS0_6HandleINS0_13BytecodeArrayEEE
	.type	_ZN2v88internal11interpreter21BytecodeArrayIteratorC2ENS0_6HandleINS0_13BytecodeArrayEEE, @function
_ZN2v88internal11interpreter21BytecodeArrayIteratorC2ENS0_6HandleINS0_13BytecodeArrayEEE:
.LFB17796:
	.cfi_startproc
	endbr64
	xorl	%edx, %edx
	jmp	_ZN2v88internal11interpreter21BytecodeArrayAccessorC2ENS0_6HandleINS0_13BytecodeArrayEEEi@PLT
	.cfi_endproc
.LFE17796:
	.size	_ZN2v88internal11interpreter21BytecodeArrayIteratorC2ENS0_6HandleINS0_13BytecodeArrayEEE, .-_ZN2v88internal11interpreter21BytecodeArrayIteratorC2ENS0_6HandleINS0_13BytecodeArrayEEE
	.globl	_ZN2v88internal11interpreter21BytecodeArrayIteratorC1ENS0_6HandleINS0_13BytecodeArrayEEE
	.set	_ZN2v88internal11interpreter21BytecodeArrayIteratorC1ENS0_6HandleINS0_13BytecodeArrayEEE,_ZN2v88internal11interpreter21BytecodeArrayIteratorC2ENS0_6HandleINS0_13BytecodeArrayEEE
	.section	.text._ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv
	.type	_ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv, @function
_ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv:
.LFB17798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	8(%rdi), %ebx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor21current_bytecode_sizeEv@PLT
	movq	%r12, %rdi
	leal	(%rax,%rbx), %esi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter21BytecodeArrayAccessor9SetOffsetEi@PLT
	.cfi_endproc
.LFE17798:
	.size	_ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv, .-_ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv
	.section	.text._ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv
	.type	_ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv, @function
_ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv:
.LFB17799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	8(%rdi), %ebx
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	call	*(%rax)
	cmpl	%ebx, %eax
	setle	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17799:
	.size	_ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv, .-_ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter21BytecodeArrayIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter21BytecodeArrayIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EE, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter21BytecodeArrayIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EE:
.LFB21512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21512:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter21BytecodeArrayIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EE, .-_GLOBAL__sub_I__ZN2v88internal11interpreter21BytecodeArrayIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter21BytecodeArrayIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
