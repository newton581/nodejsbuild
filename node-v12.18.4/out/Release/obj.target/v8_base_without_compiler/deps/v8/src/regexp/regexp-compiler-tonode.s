	.file	"regexp-compiler-tonode.cc"
	.text
	.section	.text._ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_
	.type	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_, @function
_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_:
.LFB9993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	(%r12), %rdi
	movq	%rax, %rbx
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	8(%rbx), %rcx
	movq	8(%rax), %rdx
	movl	$-1, %eax
	movzwl	(%rdx), %edx
	cmpw	%dx, (%rcx)
	jb	.L1
	seta	%al
	movzbl	%al, %eax
.L1:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9993:
	.size	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_, .-_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB11674:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE11674:
	.size	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB11675:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L9
	cmpl	$3, %edx
	je	.L10
	cmpl	$1, %edx
	je	.L14
.L10:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE11675:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZN2v84base16LazyInstanceImplINS_8internal13SpecialAddSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv,"axG",@progbits,_ZN2v84base16LazyInstanceImplINS_8internal13SpecialAddSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv,comdat
	.p2align 4
	.weak	_ZN2v84base16LazyInstanceImplINS_8internal13SpecialAddSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv
	.type	_ZN2v84base16LazyInstanceImplINS_8internal13SpecialAddSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS_8internal13SpecialAddSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv:
.LFB11384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18BuildSpecialAddSetEv@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L18
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L18:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11384:
	.size	_ZN2v84base16LazyInstanceImplINS_8internal13SpecialAddSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS_8internal13SpecialAddSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv
	.section	.text._ZN2v84base16LazyInstanceImplINS_8internal9IgnoreSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv,"axG",@progbits,_ZN2v84base16LazyInstanceImplINS_8internal9IgnoreSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv,comdat
	.p2align 4
	.weak	_ZN2v84base16LazyInstanceImplINS_8internal9IgnoreSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv
	.type	_ZN2v84base16LazyInstanceImplINS_8internal9IgnoreSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS_8internal9IgnoreSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv:
.LFB11385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal14BuildIgnoreSetEv@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L22
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L22:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11385:
	.size	_ZN2v84base16LazyInstanceImplINS_8internal9IgnoreSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS_8internal9IgnoreSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv
	.section	.text._ZN2v88internal16RegExpLookaround7Builder8ForMatchEPNS0_10RegExpNodeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal16RegExpLookaround7Builder8ForMatchEPNS0_10RegExpNodeE.part.0, @function
_ZN2v88internal16RegExpLookaround7Builder8ForMatchEPNS0_10RegExpNodeE.part.0:
.LFB11823:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movq	48(%r15), %r13
	movq	16(%r13), %r12
	movq	24(%r13), %rax
	subq	%r12, %rax
	cmpq	$71, %rax
	jbe	.L41
	leaq	72(%r12), %rax
	movq	%rax, 16(%r13)
.L25:
	leaq	16+_ZTVN2v88internal10ChoiceNodeE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r13, 48(%r12)
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movups	%xmm0, 32(%r12)
	movq	24(%r13), %rdx
	movq	16(%r13), %rbx
	movq	%rdx, %rax
	subq	%rbx, %rax
	cmpq	$15, %rax
	jbe	.L42
	leaq	16(%rbx), %rax
	subq	%rax, %rdx
	movq	%rax, 16(%r13)
	cmpq	$31, %rdx
	jbe	.L43
.L28:
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%r13)
.L29:
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	movq	$2, 8(%rbx)
	movw	%ax, 64(%r12)
	leaq	16+_ZTVN2v88internal28NegativeLookaroundChoiceNodeE(%rip), %rax
	movq	%rbx, 56(%r12)
	movq	%rax, (%r12)
	movslq	12(%rbx), %rax
	movl	8(%rbx), %edx
	cmpl	%edx, %eax
	jge	.L30
	leal	1(%rax), %edx
	salq	$4, %rax
	addq	(%rbx), %rax
	movl	%edx, 12(%rbx)
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	56(%r12), %rbx
	movslq	12(%rbx), %rax
	movl	8(%rbx), %edx
	cmpl	%edx, %eax
	jge	.L35
.L46:
	leal	1(%rax), %edx
	salq	$4, %rax
	addq	(%rbx), %rax
	movl	%edx, 12(%rbx)
	movq	%r15, (%rax)
	movq	$0, 8(%rax)
.L36:
	movl	28(%r14), %esi
	movl	24(%r14), %edi
	addq	$24, %rsp
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10ActionNode13BeginSubmatchEiiPNS0_10RegExpNodeE@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	48(%r12), %r8
	leal	1(%rdx,%rdx), %r13d
	movslq	%r13d, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	salq	$4, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L44
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L33:
	movslq	12(%rbx), %rdx
	movq	%rdx, %rsi
	salq	$4, %rdx
	testl	%esi, %esi
	jg	.L45
.L34:
	movq	%rdi, (%rbx)
	addl	$1, %esi
	addq	%rdx, %rdi
	movl	%r13d, 8(%rbx)
	movl	%esi, 12(%rbx)
	movq	%rcx, (%rdi)
	movq	$0, 8(%rdi)
	movq	56(%r12), %rbx
	movslq	12(%rbx), %rax
	movl	8(%rbx), %edx
	cmpl	%edx, %eax
	jl	.L46
.L35:
	movq	48(%r12), %rdi
	leal	1(%rdx,%rdx), %r13d
	movslq	%r13d, %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	salq	$4, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L47
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L38:
	movslq	12(%rbx), %rdx
	movq	%rdx, %rsi
	salq	$4, %rdx
	testl	%esi, %esi
	jg	.L48
.L39:
	movq	%rcx, (%rbx)
	addl	$1, %esi
	addq	%rdx, %rcx
	movl	%r13d, 8(%rbx)
	movl	%esi, 12(%rbx)
	movq	%r15, (%rcx)
	movq	$0, 8(%rcx)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L48:
	movq	(%rbx), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%rbx), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rsi
	salq	$4, %rdx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L45:
	movq	(%rbx), %rsi
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movslq	12(%rbx), %rdx
	movq	-56(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rdx, %rsi
	salq	$4, %rdx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$16, %esi
	movq	%r13, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r13), %rdx
	movq	-56(%rbp), %rcx
	movq	%rax, %rbx
	movq	16(%r13), %rax
	subq	%rax, %rdx
	cmpq	$31, %rdx
	ja	.L28
	.p2align 4,,10
	.p2align 3
.L43:
	movl	$32, %esi
	movq	%r13, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rsi, -56(%rbp)
	movq	%r13, %rdi
	movl	$72, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r12
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L47:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%r8, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %rdi
	jmp	.L33
	.cfi_endproc
.LFE11823:
	.size	_ZN2v88internal16RegExpLookaround7Builder8ForMatchEPNS0_10RegExpNodeE.part.0, .-_ZN2v88internal16RegExpLookaround7Builder8ForMatchEPNS0_10RegExpNodeE.part.0
	.section	.text._ZN2v88internal12_GLOBAL__N_141MatchAndNegativeLookaroundInReadDirectionEPNS0_14RegExpCompilerEPNS0_8ZoneListINS0_14CharacterRangeEEES7_PNS0_10RegExpNodeEbNS_4base5FlagsINS0_8JSRegExp4FlagEiEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_141MatchAndNegativeLookaroundInReadDirectionEPNS0_14RegExpCompilerEPNS0_8ZoneListINS0_14CharacterRangeEEES7_PNS0_10RegExpNodeEbNS_4base5FlagsINS0_8JSRegExp4FlagEiEE, @function
_ZN2v88internal12_GLOBAL__N_141MatchAndNegativeLookaroundInReadDirectionEPNS0_14RegExpCompilerEPNS0_8ZoneListINS0_14CharacterRangeEEES7_PNS0_10RegExpNodeEbNS_4base5FlagsINS0_8JSRegExp4FlagEiEE:
.LFB9984:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$72, %rsp
	movl	12(%rdi), %edx
	movq	1096(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %edx
	je	.L62
	movl	16(%rdi), %r13d
	cmpl	$-1, %r13d
	je	.L63
.L53:
	movq	48(%rcx), %rdi
	movl	%edx, -72(%rbp)
	movl	%r13d, -68(%rbp)
	movq	%rcx, -80(%rbp)
	movq	24(%rdi), %rax
	movq	16(%rdi), %rcx
	movb	$0, -96(%rbp)
	subq	%rcx, %rax
	cmpq	$79, %rax
	jbe	.L64
	leaq	80(%rcx), %rax
	movq	%rax, 16(%rdi)
.L57:
	leaq	16+_ZTVN2v88internal23NegativeSubmatchSuccessE(%rip), %rax
	movq	%rdi, 48(%rcx)
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	$0, 8(%rcx)
	movq	%r15, %rdi
	movq	%rax, (%rcx)
	movl	%edx, 60(%rcx)
	movl	%r13d, 64(%rcx)
	movzbl	%r8b, %r13d
	movl	%ebx, %r8d
	movq	$0, 16(%rcx)
	movl	%r13d, %edx
	movq	$0, 24(%rcx)
	movl	$2, 56(%rcx)
	movq	$0, 68(%rcx)
	movups	%xmm0, 32(%rcx)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal8TextNode24CreateForCharacterRangesEPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEEbPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE@PLT
	cmpb	$0, -96(%rbp)
	movq	%rax, %rdx
	je	.L58
	movl	-68(%rbp), %esi
	movl	-72(%rbp), %edi
	call	_ZN2v88internal10ActionNode13BeginSubmatchEiiPNS0_10RegExpNodeE@PLT
	movq	%rax, %rcx
.L59:
	movl	%ebx, %r8d
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8TextNode24CreateForCharacterRangesEPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEEbPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE@PLT
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L65
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	leaq	-96(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal16RegExpLookaround7Builder8ForMatchEPNS0_10RegExpNodeE.part.0
	movq	%rax, %rcx
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L62:
	movl	8(%rdi), %edx
	cmpl	$65534, %edx
	jg	.L66
	leal	1(%rdx), %eax
	movl	%eax, 8(%rdi)
.L52:
	movl	16(%rdi), %r13d
	movl	%edx, 12(%rdi)
	cmpl	$-1, %r13d
	jne	.L53
.L63:
	movl	8(%rdi), %r13d
	cmpl	$65534, %r13d
	jg	.L67
	leal	1(%r13), %eax
	movl	%eax, 8(%rdi)
.L55:
	movl	%r13d, 16(%rdi)
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L66:
	movb	$1, 49(%rdi)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L67:
	movb	$1, 49(%rdi)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$80, %esi
	movl	%r8d, -112(%rbp)
	movl	%edx, -108(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %rdi
	movl	-108(%rbp), %edx
	movl	-112(%rbp), %r8d
	movq	%rax, %rcx
	jmp	.L57
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9984:
	.size	_ZN2v88internal12_GLOBAL__N_141MatchAndNegativeLookaroundInReadDirectionEPNS0_14RegExpCompilerEPNS0_8ZoneListINS0_14CharacterRangeEEES7_PNS0_10RegExpNodeEbNS_4base5FlagsINS0_8JSRegExp4FlagEiEE, .-_ZN2v88internal12_GLOBAL__N_141MatchAndNegativeLookaroundInReadDirectionEPNS0_14RegExpCompilerEPNS0_8ZoneListINS0_14CharacterRangeEEES7_PNS0_10RegExpNodeEbNS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	.section	.text._ZN2v88internal12_GLOBAL__N_146NegativeLookaroundAgainstReadDirectionAndMatchEPNS0_14RegExpCompilerEPNS0_8ZoneListINS0_14CharacterRangeEEES7_PNS0_10RegExpNodeEbNS_4base5FlagsINS0_8JSRegExp4FlagEiEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_146NegativeLookaroundAgainstReadDirectionAndMatchEPNS0_14RegExpCompilerEPNS0_8ZoneListINS0_14CharacterRangeEEES7_PNS0_10RegExpNodeEbNS_4base5FlagsINS0_8JSRegExp4FlagEiEE, @function
_ZN2v88internal12_GLOBAL__N_146NegativeLookaroundAgainstReadDirectionAndMatchEPNS0_14RegExpCompilerEPNS0_8ZoneListINS0_14CharacterRangeEEES7_PNS0_10RegExpNodeEbNS_4base5FlagsINS0_8JSRegExp4FlagEiEE:
.LFB9983:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r9d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdx, %rsi
	movzbl	%r8b, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	movl	%r9d, %r8d
	subq	$72, %rsp
	movq	1096(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	_ZN2v88internal8TextNode24CreateForCharacterRangesEPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEEbPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE@PLT
	movl	12(%r12), %r8d
	cmpl	$-1, %r8d
	je	.L81
	movl	16(%r12), %edx
	cmpl	$-1, %edx
	je	.L82
.L72:
	movq	48(%rax), %r12
	movq	%rax, -80(%rbp)
	movl	%r8d, -72(%rbp)
	movl	%edx, -68(%rbp)
	movq	16(%r12), %rcx
	movq	24(%r12), %rax
	movb	$0, -96(%rbp)
	subq	%rcx, %rax
	cmpq	$79, %rax
	jbe	.L83
	leaq	80(%rcx), %rax
	movq	%rax, 16(%r12)
.L76:
	leaq	16+_ZTVN2v88internal23NegativeSubmatchSuccessE(%rip), %rax
	movl	%edx, 64(%rcx)
	xorl	$1, %ebx
	movq	%r13, %rsi
	movq	%rax, (%rcx)
	pxor	%xmm0, %xmm0
	movzbl	%bl, %edx
	movq	%r14, %rdi
	movq	$0, 8(%rcx)
	movl	%r8d, 60(%rcx)
	movl	%r15d, %r8d
	movq	%r12, 48(%rcx)
	movq	$0, 16(%rcx)
	movq	$0, 24(%rcx)
	movl	$2, 56(%rcx)
	movq	$0, 68(%rcx)
	movups	%xmm0, 32(%rcx)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal8TextNode24CreateForCharacterRangesEPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEEbPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE@PLT
	cmpb	$0, -96(%rbp)
	movq	%rax, %rdx
	je	.L77
	movl	-68(%rbp), %esi
	movl	-72(%rbp), %edi
	call	_ZN2v88internal10ActionNode13BeginSubmatchEiiPNS0_10RegExpNodeE@PLT
.L68:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L84
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	leaq	-96(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal16RegExpLookaround7Builder8ForMatchEPNS0_10RegExpNodeE.part.0
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L81:
	movl	8(%r12), %r8d
	cmpl	$65534, %r8d
	jg	.L85
	leal	1(%r8), %edx
	movl	%edx, 8(%r12)
.L71:
	movl	16(%r12), %edx
	movl	%r8d, 12(%r12)
	cmpl	$-1, %edx
	jne	.L72
.L82:
	movl	8(%r12), %edx
	cmpl	$65534, %edx
	jg	.L86
	leal	1(%rdx), %ecx
	movl	%ecx, 8(%r12)
.L74:
	movl	%edx, 16(%r12)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L85:
	movb	$1, 49(%r12)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L86:
	movb	$1, 49(%r12)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$80, %esi
	movq	%r12, %rdi
	movl	%r8d, -104(%rbp)
	movl	%edx, -100(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-100(%rbp), %edx
	movl	-104(%rbp), %r8d
	movq	%rax, %rcx
	jmp	.L76
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9983:
	.size	_ZN2v88internal12_GLOBAL__N_146NegativeLookaroundAgainstReadDirectionAndMatchEPNS0_14RegExpCompilerEPNS0_8ZoneListINS0_14CharacterRangeEEES7_PNS0_10RegExpNodeEbNS_4base5FlagsINS0_8JSRegExp4FlagEiEE, .-_ZN2v88internal12_GLOBAL__N_146NegativeLookaroundAgainstReadDirectionAndMatchEPNS0_14RegExpCompilerEPNS0_8ZoneListINS0_14CharacterRangeEEES7_PNS0_10RegExpNodeEbNS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	.section	.text._ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE,"axG",@progbits,_ZN2v88internal20RegExpCharacterClassC5EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	.type	_ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE, @function
_ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE:
.LFB8074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal20RegExpCharacterClassE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	movq	%rdx, 8(%rdi)
	movw	%ax, 16(%rdi)
	movl	%ecx, 24(%rdi)
	movl	12(%rdx), %ecx
	movl	%r8d, 28(%rdi)
	testl	%ecx, %ecx
	je	.L95
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movl	8(%rdx), %eax
	movq	%rdx, %r12
	testl	%eax, %eax
	jle	.L89
	movq	(%rdx), %rax
	movl	$1, 12(%rdx)
	movabsq	$4785070309113856, %rcx
	movq	%rcx, (%rax)
.L90:
	xorl	$1, 28(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movq	%rsi, %rdi
	leal	1(%rax,%rax), %r13d
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	movslq	%r13d, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L96
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L92:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L93
	movq	(%r12), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
.L93:
	addl	$1, %eax
	movq	%rcx, (%r12)
	movl	%eax, 12(%r12)
	movabsq	$4785070309113856, %rax
	movl	%r13d, 8(%r12)
	movq	%rax, (%rcx,%rdx)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L96:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L92
	.cfi_endproc
.LFE8074:
	.size	_ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE, .-_ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	.weak	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	.set	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE,_ZN2v88internal20RegExpCharacterClassC2EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	.section	.text._ZN2v88internal10RegExpAtom6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpAtom6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.type	_ZN2v88internal10RegExpAtom6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, @function
_ZN2v88internal10RegExpAtom6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE:
.LFB9965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	1096(%rsi), %r14
	movq	%rdx, -56(%rbp)
	movq	24(%r14), %rdx
	movq	16(%r14), %rbx
	movq	%rdx, %rax
	subq	%rbx, %rax
	cmpq	$15, %rax
	jbe	.L110
	leaq	16(%rbx), %rax
	subq	%rax, %rdx
	movq	%rax, 16(%r14)
	cmpq	$15, %rdx
	jbe	.L111
.L100:
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
.L101:
	movq	%rax, (%rbx)
	movq	%r15, %rdi
	movq	$1, 8(%rbx)
	movq	1096(%r13), %r12
	call	_ZN2v88internal11TextElement4AtomEPNS0_10RegExpAtomE@PLT
	movq	%rax, %r15
	movq	%rdx, %r14
	movslq	12(%rbx), %rax
	movl	8(%rbx), %edx
	cmpl	%edx, %eax
	jge	.L102
	leal	1(%rax), %edx
	salq	$4, %rax
	addq	(%rbx), %rax
	movl	%edx, 12(%rbx)
	movq	%r15, (%rax)
	movq	%r14, 8(%rax)
.L103:
	movq	1096(%r13), %rdi
	movzbl	52(%r13), %r14d
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$79, %rdx
	jbe	.L112
	leaq	80(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L108:
	movq	-56(%rbp), %rcx
	pxor	%xmm1, %xmm1
	movq	48(%rcx), %xmm0
	leaq	16+_ZTVN2v88internal8TextNodeE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movhps	-56(%rbp), %xmm0
	movq	$0, 24(%rax)
	movq	%rcx, (%rax)
	movq	%rbx, 64(%rax)
	movb	%r14b, 72(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm0, 48(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movq	16(%r12), %rcx
	movq	24(%r12), %rax
	leal	1(%rdx,%rdx), %r9d
	movslq	%r9d, %rsi
	salq	$4, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L113
	addq	%rcx, %rsi
	movq	%rsi, 16(%r12)
.L105:
	movslq	12(%rbx), %rdx
	movq	%rdx, %rsi
	salq	$4, %rdx
	testl	%esi, %esi
	jg	.L114
.L106:
	movq	%rcx, (%rbx)
	addl	$1, %esi
	addq	%rdx, %rcx
	movl	%r9d, 8(%rbx)
	movl	%esi, 12(%rbx)
	movq	%r15, (%rcx)
	movq	%r14, 8(%rcx)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L114:
	movq	(%rbx), %rsi
	movq	%rcx, %rdi
	movl	%r9d, -60(%rbp)
	call	memcpy@PLT
	movslq	12(%rbx), %rdx
	movl	-60(%rbp), %r9d
	movq	%rax, %rcx
	movq	%rdx, %rsi
	salq	$4, %rdx
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L112:
	movl	$80, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r14), %rdx
	movq	%rax, %rbx
	movq	16(%r14), %rax
	subq	%rax, %rdx
	cmpq	$15, %rdx
	ja	.L100
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%r12, %rdi
	movl	%r9d, -60(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-60(%rbp), %r9d
	movq	%rax, %rcx
	jmp	.L105
	.cfi_endproc
.LFE9965:
	.size	_ZN2v88internal10RegExpAtom6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, .-_ZN2v88internal10RegExpAtom6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal10RegExpText6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpText6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.type	_ZN2v88internal10RegExpText6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, @function
_ZN2v88internal10RegExpText6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE:
.LFB9966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	1096(%rsi), %rdi
	movzbl	52(%rsi), %r13d
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$79, %rdx
	jbe	.L119
	leaq	80(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L117:
	movq	48(%rbx), %xmm0
	movq	%rbx, %xmm2
	pxor	%xmm1, %xmm1
	leaq	16+_ZTVN2v88internal8TextNodeE(%rip), %rcx
	movq	$0, 8(%rax)
	punpcklqdq	%xmm2, %xmm0
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rcx, (%rax)
	movq	%r12, 64(%rax)
	movb	%r13b, 72(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm0, 48(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	movl	$80, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L117
	.cfi_endproc
.LFE9966:
	.size	_ZN2v88internal10RegExpText6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, .-_ZN2v88internal10RegExpText6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal19RegExpBackReference6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19RegExpBackReference6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.type	_ZN2v88internal19RegExpBackReference6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, @function
_ZN2v88internal19RegExpBackReference6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE:
.LFB10008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movl	24(%rdi), %r15d
	movq	1096(%rsi), %rdi
	movzbl	52(%rsi), %r13d
	movl	16(%rax), %ebx
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	addl	%ebx, %ebx
	subq	%rax, %rdx
	leal	1(%rbx), %r14d
	cmpq	$79, %rdx
	jbe	.L124
	leaq	80(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L122:
	movq	48(%r12), %xmm0
	movq	%r12, %xmm2
	pxor	%xmm1, %xmm1
	leaq	16+_ZTVN2v88internal17BackReferenceNodeE(%rip), %rcx
	movq	$0, 8(%rax)
	punpcklqdq	%xmm2, %xmm0
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rcx, (%rax)
	movl	%ebx, 64(%rax)
	movl	%r14d, 68(%rax)
	movl	%r15d, 72(%rax)
	movb	%r13b, 76(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm0, 48(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movl	$80, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L122
	.cfi_endproc
.LFE10008:
	.size	_ZN2v88internal19RegExpBackReference6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, .-_ZN2v88internal19RegExpBackReference6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal11RegExpEmpty6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpEmpty6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.type	_ZN2v88internal11RegExpEmpty6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, @function
_ZN2v88internal11RegExpEmpty6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE:
.LFB10009:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	ret
	.cfi_endproc
.LFE10009:
	.size	_ZN2v88internal11RegExpEmpty6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, .-_ZN2v88internal11RegExpEmpty6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal16RegExpLookaround7BuilderC2EbPNS0_10RegExpNodeEiiii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RegExpLookaround7BuilderC2EbPNS0_10RegExpNodeEiiii
	.type	_ZN2v88internal16RegExpLookaround7BuilderC2EbPNS0_10RegExpNodeEiiii, @function
_ZN2v88internal16RegExpLookaround7BuilderC2EbPNS0_10RegExpNodeEiiii:
.LFB10011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movb	%sil, (%rdi)
	movq	%rdx, 16(%rdi)
	movl	%ecx, 24(%rdi)
	movl	%r8d, 28(%rdi)
	testb	%sil, %sil
	je	.L127
	movl	16(%rbp), %ecx
	movq	%rdx, %r8
	movl	%r13d, %esi
	movl	%r12d, %edi
	movl	%r9d, %edx
	call	_ZN2v88internal10ActionNode23PositiveSubmatchSuccessEiiiiPNS0_10RegExpNodeE@PLT
	movq	%rax, 8(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movq	48(%rdx), %r14
	movq	16(%r14), %rax
	movq	24(%r14), %rcx
	subq	%rax, %rcx
	cmpq	$79, %rcx
	jbe	.L132
	leaq	80(%rax), %rcx
	movq	%rcx, 16(%r14)
.L130:
	movd	16(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movq	%r14, 48(%rax)
	movd	%r13d, %xmm3
	movups	%xmm0, 32(%rax)
	movd	%r9d, %xmm1
	movd	%r12d, %xmm0
	leaq	16+_ZTVN2v88internal23NegativeSubmatchSuccessE(%rip), %rdi
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	movq	$0, 8(%rax)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movl	$2, 56(%rax)
	movq	%rdi, (%rax)
	movups	%xmm0, 60(%rax)
	movq	%rax, 8(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	movl	$80, %esi
	movq	%r14, %rdi
	movl	%r9d, -36(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-36(%rbp), %r9d
	jmp	.L130
	.cfi_endproc
.LFE10011:
	.size	_ZN2v88internal16RegExpLookaround7BuilderC2EbPNS0_10RegExpNodeEiiii, .-_ZN2v88internal16RegExpLookaround7BuilderC2EbPNS0_10RegExpNodeEiiii
	.globl	_ZN2v88internal16RegExpLookaround7BuilderC1EbPNS0_10RegExpNodeEiiii
	.set	_ZN2v88internal16RegExpLookaround7BuilderC1EbPNS0_10RegExpNodeEiiii,_ZN2v88internal16RegExpLookaround7BuilderC2EbPNS0_10RegExpNodeEiiii
	.section	.text._ZN2v88internal16RegExpLookaround7Builder8ForMatchEPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RegExpLookaround7Builder8ForMatchEPNS0_10RegExpNodeE
	.type	_ZN2v88internal16RegExpLookaround7Builder8ForMatchEPNS0_10RegExpNodeE, @function
_ZN2v88internal16RegExpLookaround7Builder8ForMatchEPNS0_10RegExpNodeE:
.LFB10013:
	.cfi_startproc
	endbr64
	cmpb	$0, (%rdi)
	movq	%rsi, %rdx
	je	.L134
	movl	28(%rdi), %esi
	movl	24(%rdi), %edi
	jmp	_ZN2v88internal10ActionNode13BeginSubmatchEiiPNS0_10RegExpNodeE@PLT
	.p2align 4,,10
	.p2align 3
.L134:
	jmp	_ZN2v88internal16RegExpLookaround7Builder8ForMatchEPNS0_10RegExpNodeE.part.0
	.cfi_endproc
.LFE10013:
	.size	_ZN2v88internal16RegExpLookaround7Builder8ForMatchEPNS0_10RegExpNodeE, .-_ZN2v88internal16RegExpLookaround7Builder8ForMatchEPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal16RegExpLookaround6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RegExpLookaround6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.type	_ZN2v88internal16RegExpLookaround6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, @function
_ZN2v88internal16RegExpLookaround6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE:
.LFB10014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	8(%rsi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$65534, %r13d
	jg	.L146
	leal	1(%r13), %r15d
	je	.L148
	leal	2(%r13), %eax
	movl	%eax, 8(%rsi)
.L138:
	movl	20(%r14), %eax
	cmpl	$1, 28(%r14)
	movq	%r8, -80(%rbp)
	movzbl	52(%r12), %ebx
	movl	%r13d, -72(%rbp)
	leal	(%rax,%rax), %r9d
	movl	24(%r14), %eax
	sete	52(%r12)
	movl	%r15d, -68(%rbp)
	leal	2(%rax,%rax), %ecx
	movzbl	16(%r14), %eax
	movb	%al, -96(%rbp)
	testb	%al, %al
	je	.L139
	movl	%r9d, %edx
	movl	%r15d, %esi
	movl	%r13d, %edi
	call	_ZN2v88internal10ActionNode23PositiveSubmatchSuccessEiiiiPNS0_10RegExpNodeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rdx
.L140:
	movq	8(%r14), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	cmpb	$0, -96(%rbp)
	movq	%rax, %rdx
	je	.L143
	movl	-68(%rbp), %esi
	movl	-72(%rbp), %edi
	call	_ZN2v88internal10ActionNode13BeginSubmatchEiiPNS0_10RegExpNodeE@PLT
.L144:
	movb	%bl, 52(%r12)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L149
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	48(%r8), %rdi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	$79, %rax
	jbe	.L150
	leaq	80(%rdx), %rax
	movq	%rax, 16(%rdi)
.L142:
	pxor	%xmm0, %xmm0
	movd	%r9d, %xmm1
	movd	%ecx, %xmm2
	movq	%rdi, 48(%rdx)
	movups	%xmm0, 32(%rdx)
	movd	%r15d, %xmm3
	movd	%r13d, %xmm0
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	leaq	16+_ZTVN2v88internal23NegativeSubmatchSuccessE(%rip), %rax
	movq	$0, 8(%rdx)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 16(%rdx)
	movq	$0, 24(%rdx)
	movl	$2, 56(%rdx)
	movq	%rax, (%rdx)
	movups	%xmm0, 60(%rdx)
	movq	%rdx, -88(%rbp)
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L143:
	leaq	-96(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal16RegExpLookaround7Builder8ForMatchEPNS0_10RegExpNodeE.part.0
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L146:
	movl	%r13d, %r15d
.L136:
	movb	$1, 49(%r12)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L150:
	movl	$80, %esi
	movl	%ecx, -112(%rbp)
	movl	%r9d, -108(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %rdi
	movl	-108(%rbp), %r9d
	movl	-112(%rbp), %ecx
	movq	%rax, %rdx
	jmp	.L142
.L149:
	call	__stack_chk_fail@PLT
.L148:
	movl	$65535, 8(%rsi)
	jmp	.L136
	.cfi_endproc
.LFE10014:
	.size	_ZN2v88internal16RegExpLookaround6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, .-_ZN2v88internal16RegExpLookaround6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal13RegExpCapture6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpCapture6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.type	_ZN2v88internal13RegExpCapture6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, @function
_ZN2v88internal13RegExpCapture6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE:
.LFB10015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	movl	16(%rdi), %r12d
	movq	8(%rdi), %r14
	addl	%r12d, %r12d
	cmpb	$0, 52(%rsi)
	leal	1(%r12), %edi
	je	.L152
	movl	%r12d, %eax
	movl	%edi, %r12d
	movl	%eax, %edi
.L152:
	movl	$1, %esi
	call	_ZN2v88internal10ActionNode13StorePositionEibPNS0_10RegExpNodeE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	(%r14), %rax
	call	*24(%rax)
	addq	$8, %rsp
	movl	%r12d, %edi
	movl	$1, %esi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10ActionNode13StorePositionEibPNS0_10RegExpNodeE@PLT
	.cfi_endproc
.LFE10015:
	.size	_ZN2v88internal13RegExpCapture6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, .-_ZN2v88internal13RegExpCapture6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal13RegExpCapture6ToNodeEPNS0_10RegExpTreeEiPNS0_14RegExpCompilerEPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpCapture6ToNodeEPNS0_10RegExpTreeEiPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.type	_ZN2v88internal13RegExpCapture6ToNodeEPNS0_10RegExpTreeEiPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, @function
_ZN2v88internal13RegExpCapture6ToNodeEPNS0_10RegExpTreeEiPNS0_14RegExpCompilerEPNS0_10RegExpNodeE:
.LFB10016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	movq	%rcx, %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leal	(%rsi,%rsi), %r12d
	leal	1(%r12), %edi
	subq	$8, %rsp
	cmpb	$0, 52(%r14)
	je	.L155
	movl	%r12d, %eax
	movl	%edi, %r12d
	movl	%eax, %edi
.L155:
	movl	$1, %esi
	call	_ZN2v88internal10ActionNode13StorePositionEibPNS0_10RegExpNodeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	0(%r13), %rax
	call	*24(%rax)
	addq	$8, %rsp
	movl	%r12d, %edi
	movl	$1, %esi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10ActionNode13StorePositionEibPNS0_10RegExpNodeE@PLT
	.cfi_endproc
.LFE10016:
	.size	_ZN2v88internal13RegExpCapture6ToNodeEPNS0_10RegExpTreeEiPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, .-_ZN2v88internal13RegExpCapture6ToNodeEPNS0_10RegExpTreeEiPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal17RegExpAlternative6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpAlternative6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.type	_ZN2v88internal17RegExpAlternative6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, @function
_ZN2v88internal17RegExpAlternative6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE:
.LFB10023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r14
	movq	1096(%rsi), %rax
	movq	%rdx, -96(%rbp)
	movl	12(%r14), %r13d
	movq	%rax, -56(%rbp)
	testl	%r13d, %r13d
	jle	.L158
	movq	%rsi, -112(%rbp)
	xorl	%ebx, %ebx
	movl	$-1, %r12d
	movq	%r14, %r15
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L159:
	call	*128(%rax)
	movl	%eax, %r14d
	testb	%al, %al
	jne	.L160
	subl	%r12d, %r13d
	movl	%r13d, -80(%rbp)
	cmpl	$1, %r13d
	jg	.L219
.L161:
	movl	$-1, %r12d
.L160:
	movl	12(%r15), %r13d
	addq	$1, %rbx
	cmpl	%ebx, %r13d
	jle	.L220
.L174:
	movq	(%r15), %rax
	movl	%ebx, %r13d
	movq	(%rax,%rbx,8), %rdi
	movq	(%rdi), %rax
	cmpl	$-1, %r12d
	jne	.L159
	call	*128(%rax)
	movl	%ebx, %r12d
	testb	%al, %al
	je	.L161
	movl	12(%r15), %r13d
	addq	$1, %rbx
	cmpl	%ebx, %r13d
	jg	.L174
	.p2align 4,,10
	.p2align 3
.L220:
	movq	%r15, %r14
	movq	-112(%rbp), %r15
	cmpl	$-1, %r12d
	je	.L175
	movl	%r13d, %eax
	subl	%r12d, %eax
	cmpl	$1, %eax
	jg	.L221
.L175:
	cmpb	$0, 52(%r15)
	je	.L191
.L231:
	movq	-96(%rbp), %rdx
	testl	%r13d, %r13d
	jle	.L157
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L193:
	movq	(%r14), %rax
	movq	%r15, %rsi
	movq	(%rax,%rbx,8), %rdi
	addq	$1, %rbx
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %rdx
	cmpl	%ebx, 12(%r14)
	jg	.L193
.L157:
	addq	$104, %rsp
	movq	%rdx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movslq	%r12d, %rsi
	leaq	0(,%rsi,8), %rax
	movq	%rsi, -88(%rbp)
	movq	%rax, -104(%rbp)
	movq	(%r15), %rax
	movq	(%rax,%rsi,8), %rdi
	movq	(%rdi), %rax
	call	*120(%rax)
	movl	%ebx, -132(%rbp)
	movl	12(%rax), %ecx
	cmpl	%ebx, %r12d
	jge	.L161
	movq	-88(%rbp), %rsi
	leal	-1(%r13), %eax
	movq	-104(%rbp), %r10
	xorl	%r13d, %r13d
	movl	%r12d, -120(%rbp)
	movl	%r13d, %r12d
	movq	%r15, %r13
	leaq	1(%rsi,%rax), %rax
	movq	%rbx, -128(%rbp)
	movq	%r10, %r15
	movl	%ecx, %ebx
	leaq	0(,%rax,8), %rsi
	movq	%rsi, -64(%rbp)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L223:
	sall	%cl, %edx
	movl	$1, %r14d
.L163:
	orl	%edx, %r12d
	addq	$8, %r15
	cmpq	%r15, -64(%rbp)
	je	.L222
.L166:
	movq	0(%r13), %rax
	movq	(%rax,%r15), %rdi
	movq	(%rdi), %rax
	call	*120(%rax)
	movl	$1, %edx
	cmpl	12(%rax), %ebx
	movl	8(%rax), %ecx
	jne	.L223
	sall	%cl, %edx
	testl	%r12d, %edx
	je	.L163
	testb	%r14b, %r14b
	jne	.L163
	movq	-56(%rbp), %rsi
	movq	16(%rsi), %rax
	movq	24(%rsi), %rsi
	movq	%rsi, %rcx
	movq	%rsi, -72(%rbp)
	subq	%rax, %rcx
	cmpq	$7, %rcx
	jbe	.L224
	movq	-56(%rbp), %rsi
	leaq	8(%rax), %rcx
	movq	%rcx, 16(%rsi)
.L165:
	leaq	16+_ZTVN2v88internal11RegExpEmptyE(%rip), %rcx
	orl	%edx, %r12d
	movq	%rcx, (%rax)
	movq	0(%r13), %rcx
	movq	%rax, (%rcx,%r15)
	addq	$8, %r15
	cmpq	%r15, -64(%rbp)
	jne	.L166
	.p2align 4,,10
	.p2align 3
.L222:
	movq	%r13, %r15
	movl	%r12d, %r13d
	movq	-128(%rbp), %rbx
	movl	-120(%rbp), %r12d
	movl	%r13d, %r11d
	andl	$48, %r11d
	cmpl	$48, %r11d
	jne	.L161
	movq	-56(%rbp), %rax
	movq	16(%rax), %rdx
	movq	24(%rax), %rax
	movq	%rax, -64(%rbp)
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L225
	movq	-56(%rbp), %rcx
	leaq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
	movq	%rcx, %rax
.L168:
	movq	$0, (%rdx)
	movq	$0, 8(%rdx)
	movq	16(%rax), %r14
	movq	24(%rax), %rax
	movq	%rax, -64(%rbp)
	subq	%r14, %rax
	cmpq	$31, %rax
	jbe	.L226
	movq	-56(%rbp), %rcx
	leaq	32(%r14), %rax
	movq	%rax, 16(%rcx)
	movq	%rcx, %r13
.L170:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	movq	(%r15), %rax
	movq	-88(%rbp), %rcx
	movq	%r14, (%rax,%rcx,8)
	movq	24(%r13), %rax
	movq	16(%r13), %rcx
	movq	%rax, -64(%rbp)
	subq	%rcx, %rax
	cmpq	$7, %rax
	jbe	.L227
	movq	-56(%rbp), %rsi
	leaq	8(%rcx), %rax
	movq	%rax, 16(%rsi)
.L172:
	leaq	16+_ZTVN2v88internal11RegExpEmptyE(%rip), %rax
	addl	$1, %r12d
	movq	%rax, (%rcx)
	cmpl	%r12d, -132(%rbp)
	jle	.L161
	movl	-80(%rbp), %esi
	movq	-104(%rbp), %rax
	leal	-2(%rsi), %edx
	movq	-88(%rbp), %rsi
	addq	$8, %rax
	leaq	2(%rsi,%rdx), %rsi
	salq	$3, %rsi
	.p2align 4,,10
	.p2align 3
.L173:
	movq	(%r15), %rdx
	movq	%rcx, (%rdx,%rax)
	addq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L173
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L158:
	cmpb	$0, 52(%rsi)
	jne	.L157
	.p2align 4,,10
	.p2align 3
.L191:
	movl	%r13d, %eax
	subl	$1, %eax
	js	.L198
	movslq	%eax, %r12
	movslq	%r13d, %r10
	movl	%eax, %eax
	movq	-96(%rbp), %rdx
	subq	%rax, %r10
	salq	$3, %r12
	leaq	-16(,%r10,8), %rbx
	.p2align 4,,10
	.p2align 3
.L194:
	movq	(%r14), %rax
	movq	%r15, %rsi
	movq	(%rax,%r12), %rdi
	subq	$8, %r12
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %rdx
	cmpq	%r12, %rbx
	jne	.L194
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L221:
	movslq	%r12d, %rcx
	leaq	0(,%rcx,8), %rax
	movq	%rcx, -80(%rbp)
	movq	%rax, -88(%rbp)
	movq	(%r14), %rax
	movq	(%rax,%rcx,8), %rdi
	movq	(%rdi), %rax
	call	*120(%rax)
	movl	12(%rax), %eax
	movl	%eax, %esi
	cmpl	%r12d, %r13d
	jle	.L217
	leal	-1(%r13), %eax
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdx
	xorl	%ebx, %ebx
	subl	%r12d, %eax
	movl	%r13d, -104(%rbp)
	movl	%esi, %r13d
	leaq	1(%rcx,%rax), %rax
	movl	%r12d, -112(%rbp)
	movq	%rdx, %r12
	salq	$3, %rax
	movq	%r15, -120(%rbp)
	movl	%ebx, %r15d
	movb	$0, -64(%rbp)
	movq	%rax, %rbx
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L229:
	movb	$1, -64(%rbp)
	sall	%cl, %eax
	movl	%eax, %ecx
.L178:
	addq	$8, %r12
	orl	%ecx, %r15d
	cmpq	%r12, %rbx
	je	.L228
.L181:
	movq	(%r14), %rax
	movq	(%rax,%r12), %rdi
	movq	(%rdi), %rax
	call	*120(%rax)
	cmpl	12(%rax), %r13d
	movl	8(%rax), %ecx
	movl	$1, %eax
	jne	.L229
	sall	%cl, %eax
	movl	%eax, %ecx
	testl	%r15d, %eax
	je	.L178
	cmpb	$0, -64(%rbp)
	jne	.L178
	movq	-56(%rbp), %rsi
	movq	16(%rsi), %rax
	movq	24(%rsi), %rsi
	movq	%rsi, -72(%rbp)
	subq	%rax, %rsi
	cmpq	$7, %rsi
	jbe	.L230
	movq	-56(%rbp), %rdx
	leaq	8(%rax), %rsi
	movq	%rsi, 16(%rdx)
.L180:
	leaq	16+_ZTVN2v88internal11RegExpEmptyE(%rip), %rsi
	orl	%ecx, %r15d
	movq	%rsi, (%rax)
	movq	(%r14), %rsi
	movq	%rax, (%rsi,%r12)
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L181
	.p2align 4,,10
	.p2align 3
.L228:
	movl	%r15d, %ebx
	movl	-104(%rbp), %r13d
	movl	-112(%rbp), %r12d
	andl	$48, %ebx
	movq	-120(%rbp), %r15
	cmpl	$48, %ebx
	je	.L182
.L217:
	cmpb	$0, 52(%r15)
	movl	12(%r14), %r13d
	je	.L191
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L182:
	movq	-56(%rbp), %rax
	movq	16(%rax), %rdx
	movq	24(%rax), %rax
	movq	%rax, -64(%rbp)
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L232
	movq	-56(%rbp), %rcx
	leaq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
	movq	%rcx, %rax
.L184:
	movq	$0, (%rdx)
	movq	$0, 8(%rdx)
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rax, -64(%rbp)
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L233
	movq	-56(%rbp), %rcx
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rcx)
	movq	%rcx, %rsi
.L186:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	call	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	movq	(%r14), %rax
	movq	-80(%rbp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	movq	-56(%rbp), %rax
	movq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rax, -64(%rbp)
	subq	%rcx, %rax
	cmpq	$7, %rax
	jbe	.L234
	movq	-56(%rbp), %rsi
	leaq	8(%rcx), %rax
	movq	%rax, 16(%rsi)
.L188:
	leaq	16+_ZTVN2v88internal11RegExpEmptyE(%rip), %rax
	movq	%rax, (%rcx)
	leal	1(%r12), %eax
	cmpl	%eax, %r13d
	jle	.L217
	movq	-80(%rbp), %rsi
	leal	-2(%r13), %r10d
	movq	-88(%rbp), %rax
	subl	%r12d, %r10d
	leaq	2(%rsi,%r10), %rsi
	addq	$8, %rax
	salq	$3, %rsi
	.p2align 4,,10
	.p2align 3
.L190:
	movq	(%r14), %rdx
	movq	%rcx, (%rdx,%rax)
	addq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L190
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L224:
	movq	-56(%rbp), %rdi
	movl	$8, %esi
	movl	%edx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-72(%rbp), %edx
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L198:
	movq	-96(%rbp), %rdx
	jmp	.L157
.L232:
	movq	-56(%rbp), %rdi
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	jmp	.L184
.L230:
	movq	-56(%rbp), %rdi
	movl	$8, %esi
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-72(%rbp), %ecx
	jmp	.L180
.L225:
	movq	-56(%rbp), %rdi
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	jmp	.L168
.L227:
	movq	-56(%rbp), %rdi
	movl	$8, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L172
.L226:
	movq	-56(%rbp), %rdi
	movl	$32, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %r13
	movq	%rax, %r14
	jmp	.L170
.L234:
	movq	-56(%rbp), %rdi
	movl	$8, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L188
.L233:
	movq	-56(%rbp), %rdi
	movl	$32, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L186
	.cfi_endproc
.LFE10023:
	.size	_ZN2v88internal17RegExpAlternative6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, .-_ZN2v88internal17RegExpAlternative6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal14CharacterRange13GetWordBoundsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CharacterRange13GetWordBoundsEv
	.type	_ZN2v88internal14CharacterRange13GetWordBoundsEv, @function
_ZN2v88internal14CharacterRange13GetWordBoundsEv:
.LFB10028:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88internal25regexp_compiler_constantsL11kWordRangesE(%rip), %rax
	movl	$8, %edx
	ret
	.cfi_endproc
.LFE10028:
	.size	_ZN2v88internal14CharacterRange13GetWordBoundsEv, .-_ZN2v88internal14CharacterRange13GetWordBoundsEv
	.section	.text._ZN2v88internal14CharacterRange11IsCanonicalEPNS0_8ZoneListIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CharacterRange11IsCanonicalEPNS0_8ZoneListIS1_EE
	.type	_ZN2v88internal14CharacterRange11IsCanonicalEPNS0_8ZoneListIS1_EE, @function
_ZN2v88internal14CharacterRange11IsCanonicalEPNS0_8ZoneListIS1_EE:
.LFB10040:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %esi
	movl	$1, %eax
	cmpl	$1, %esi
	jle	.L236
	movq	(%rdi), %rdx
	subl	$2, %esi
	movl	4(%rdx), %ecx
	leaq	8(%rdx), %rax
	leaq	16(%rdx,%rsi,8), %rsi
	.p2align 4,,10
	.p2align 3
.L238:
	movl	%ecx, %edx
	movl	4(%rax), %ecx
	addl	$1, %edx
	cmpl	(%rax), %edx
	jge	.L240
	addq	$8, %rax
	cmpq	%rsi, %rax
	jne	.L238
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	xorl	%eax, %eax
.L236:
	ret
	.cfi_endproc
.LFE10040:
	.size	_ZN2v88internal14CharacterRange11IsCanonicalEPNS0_8ZoneListIS1_EE, .-_ZN2v88internal14CharacterRange11IsCanonicalEPNS0_8ZoneListIS1_EE
	.section	.text._ZN2v88internal14CharacterRange12CanonicalizeEPNS0_8ZoneListIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CharacterRange12CanonicalizeEPNS0_8ZoneListIS1_EE
	.type	_ZN2v88internal14CharacterRange12CanonicalizeEPNS0_8ZoneListIS1_EE, @function
_ZN2v88internal14CharacterRange12CanonicalizeEPNS0_8ZoneListIS1_EE:
.LFB10045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	12(%rdi), %r9d
	movl	%r9d, -44(%rbp)
	cmpl	$1, %r9d
	jle	.L242
	movq	(%rdi), %rdx
	movl	$1, %eax
	movl	4(%rdx), %r8d
	leaq	8(%rdx), %rcx
	.p2align 4,,10
	.p2align 3
.L245:
	movl	%r8d, %esi
	movl	4(%rcx), %r8d
	addl	$1, %esi
	cmpl	(%rcx), %esi
	jge	.L244
	addl	$1, %eax
	addq	$8, %rcx
	cmpl	%r9d, %eax
	jne	.L245
.L242:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	cmpl	%eax, -44(%rbp)
	je	.L242
	movslq	%eax, %r12
	movl	%eax, %r13d
	salq	$3, %r12
	.p2align 4,,10
	.p2align 3
.L265:
	leaq	(%rdx,%r12), %rcx
	movl	%eax, %r9d
	movl	(%rcx), %esi
	movl	4(%rcx), %r10d
	subl	$1, %r9d
	js	.L266
	leal	1(%r10), %ebx
	movslq	%r9d, %r9
	movslq	%eax, %rcx
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L272:
	addl	$1, %r11d
	cmpl	%r11d, %esi
	jg	.L271
	subq	$1, %r9
	testl	%r9d, %r9d
	js	.L270
.L248:
	movl	4(%rdx,%r9,8), %r11d
	movl	%r9d, %r8d
	cmpl	%ebx, (%rdx,%r9,8)
	jle	.L272
	movslq	%r9d, %rcx
	subq	$1, %r9
	testl	%r9d, %r9d
	jns	.L248
.L270:
	xorl	%r8d, %r8d
	xorl	%ebx, %ebx
	cmpl	%r8d, %ecx
	je	.L273
.L249:
	leaq	(%rdx,%rbx), %r9
	leal	1(%r8), %r14d
	cmpl	%esi, (%r9)
	cmovle	(%r9), %esi
	cmpl	%r14d, %ecx
	je	.L274
	leal	-1(%rcx), %r15d
	movslq	%r15d, %r15
	leaq	0(,%r15,8), %r11
	cmpl	%r10d, 4(%rdx,%r15,8)
	cmovge	4(%rdx,%r11), %r10d
	cmpl	%ecx, %eax
	jg	.L275
.L256:
	subl	%r8d, %ecx
	movl	%esi, (%r9)
	movl	%r10d, 4(%r9)
	subl	%ecx, %eax
	addl	$1, %eax
.L254:
	addl	$1, %r13d
	addq	$8, %r12
	cmpl	%r13d, -44(%rbp)
	jle	.L264
.L277:
	movq	(%rdi), %rdx
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L271:
	addl	$1, %r8d
	movslq	%r8d, %rbx
	salq	$3, %rbx
	cmpl	%r8d, %ecx
	jne	.L249
.L273:
	cmpl	%ecx, %eax
	jg	.L276
.L250:
	addq	%rbx, %rdx
	addl	$1, %eax
	addl	$1, %r13d
	addq	$8, %r12
	movl	%esi, (%rdx)
	movl	%r10d, 4(%rdx)
	cmpl	%r13d, -44(%rbp)
	jg	.L277
.L264:
	movl	%eax, 12(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	movl	%eax, %r15d
	subl	%ecx, %r15d
	cmpl	%r14d, %ecx
	jge	.L257
	leal	-1(%r15), %r9d
	addl	%r9d, %r14d
	addl	%ecx, %r9d
	movslq	%r9d, %r9
	movslq	%r14d, %r14
	movq	(%rdx,%r9,8), %r9
	movq	%r9, (%rdx,%r14,8)
	cmpl	$1, %r15d
	je	.L262
	movslq	%ecx, %rdx
	leal	-2(%r15), %r11d
	movslq	%r15d, %r15
	movslq	%r11d, %r9
	addq	%rdx, %r15
	movl	%r11d, %r11d
	subq	%r11, %r15
	movslq	%r8d, %r11
	addq	%rdx, %r9
	subq	%rdx, %r11
	salq	$3, %r9
	leaq	-24(,%r15,8), %r15
	leaq	8(,%r11,8), %r14
	.p2align 4,,10
	.p2align 3
.L260:
	movq	(%rdi), %rdx
	leaq	(%rdx,%r14), %r11
	movq	(%rdx,%r9), %rdx
	movq	%rdx, (%r11,%r9)
	subq	$8, %r9
	cmpq	%r9, %r15
	jne	.L260
.L262:
	addq	(%rdi), %rbx
	movq	%rbx, %r9
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L274:
	cmpl	%r10d, 4(%r9)
	cmovge	4(%r9), %r10d
	movl	%esi, (%r9)
	movl	%r10d, 4(%r9)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L257:
	movslq	%ecx, %r9
	movslq	%r14d, %r14
	movq	%r9, -56(%rbp)
	movq	8(%rdx,%r11), %r9
	movq	%r9, (%rdx,%r14,8)
	cmpl	$1, %r15d
	je	.L262
	leal	-2(%r15), %edx
	movslq	%ecx, %r15
	addq	$16, %r11
	leaq	2(%r15,%rdx), %r14
	movslq	%r8d, %rdx
	subq	%r15, %rdx
	salq	$3, %r14
	leaq	8(,%rdx,8), %r15
	.p2align 4,,10
	.p2align 3
.L263:
	movq	(%rdi), %rdx
	leaq	(%rdx,%r15), %r9
	movq	(%rdx,%r11), %rdx
	movq	%rdx, (%r9,%r11)
	addq	$8, %r11
	cmpq	%r11, %r14
	jne	.L263
	addq	(%rdi), %rbx
	movq	%rbx, %r9
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L276:
	leal	-1(%rax), %r9d
	movl	%eax, %r8d
	movslq	%eax, %r11
	movslq	%r9d, %r9
	subl	%ecx, %r8d
	movq	(%rdx,%r9,8), %r9
	movq	%r9, (%rdx,%r11,8)
	cmpl	$1, %r8d
	je	.L252
	leal	-2(%r8), %r9d
	movslq	%r8d, %r8
	movslq	%r9d, %rdx
	movl	%r9d, %r9d
	addq	%rcx, %rdx
	addq	%r8, %rcx
	subq	%r9, %rcx
	salq	$3, %rdx
	leaq	-24(,%rcx,8), %r9
.L253:
	movq	(%rdi), %rcx
	movq	(%rcx,%rdx), %r8
	movq	%r8, 8(%rcx,%rdx)
	subq	$8, %rdx
	cmpq	%r9, %rdx
	jne	.L253
.L252:
	movq	(%rdi), %rdx
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L266:
	movslq	%eax, %rcx
	jmp	.L270
	.cfi_endproc
.LFE10045:
	.size	_ZN2v88internal14CharacterRange12CanonicalizeEPNS0_8ZoneListIS1_EE, .-_ZN2v88internal14CharacterRange12CanonicalizeEPNS0_8ZoneListIS1_EE
	.section	.text._ZN2v88internal12CharacterSet12CanonicalizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12CharacterSet12CanonicalizeEv
	.type	_ZN2v88internal12CharacterSet12CanonicalizeEv, @function
_ZN2v88internal12CharacterSet12CanonicalizeEv:
.LFB10044:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L278
	jmp	_ZN2v88internal14CharacterRange12CanonicalizeEPNS0_8ZoneListIS1_EE
	.p2align 4,,10
	.p2align 3
.L278:
	ret
	.cfi_endproc
.LFE10044:
	.size	_ZN2v88internal12CharacterSet12CanonicalizeEv, .-_ZN2v88internal12CharacterSet12CanonicalizeEv
	.section	.text._ZN2v88internal12_GLOBAL__N_119ToCanonicalZoneListEPKNS_4base11SmallVectorINS0_14CharacterRangeELm8EEEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119ToCanonicalZoneListEPKNS_4base11SmallVectorINS0_14CharacterRangeELm8EEEPNS0_4ZoneE, @function
_ZN2v88internal12_GLOBAL__N_119ToCanonicalZoneListEPKNS_4base11SmallVectorINS0_14CharacterRangeELm8EEEPNS0_4ZoneE:
.LFB9977:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	8(%rdi), %rbx
	cmpq	%rbx, %rax
	je	.L294
	subq	%rax, %rbx
	movq	16(%rsi), %r12
	movq	24(%rsi), %rax
	movq	%rdi, %r14
	sarq	$3, %rbx
	movq	%rsi, %r15
	subq	%r12, %rax
	movl	%ebx, %ecx
	cmpq	$15, %rax
	jbe	.L297
	leaq	16(%r12), %rax
	movq	%rax, 16(%rsi)
.L283:
	movq	%r12, %r13
	xorl	%eax, %eax
	testl	%ebx, %ebx
	jg	.L298
.L284:
	movq	%rax, (%r12)
	movl	%ebx, 8(%r12)
	movl	$0, 12(%r12)
	movq	(%r14), %rdx
	cmpq	8(%r14), %rdx
	je	.L287
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L299:
	leal	1(%rax), %esi
	movq	(%r12), %rcx
	movl	%esi, 12(%r12)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx,%rax,8)
.L289:
	movq	(%r14), %rdx
	movq	8(%r14), %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L287
	movslq	12(%r12), %rax
	movl	8(%r12), %ecx
.L293:
	leaq	(%rdx,%rbx,8), %rdx
	cmpl	%ecx, %eax
	jl	.L299
	leal	1(%rcx,%rcx), %r8d
	movq	16(%r15), %rdi
	movq	24(%r15), %rax
	movslq	%r8d, %rsi
	movq	(%rdx), %rcx
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L300
	addq	%rdi, %rsi
	movq	%rsi, 16(%r15)
.L291:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L292
	movq	(%r12), %rsi
	movl	%r8d, -60(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movl	-60(%rbp), %r8d
	movq	-56(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
.L292:
	addl	$1, %eax
	movq	%rdi, (%r12)
	movl	%r8d, 8(%r12)
	movl	%eax, 12(%r12)
	movq	%rcx, (%rdi,%rdx)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L287:
	movq	%r12, %rdi
	call	_ZN2v88internal14CharacterRange12CanonicalizeEPNS0_8ZoneListIS1_EE
.L280:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	movq	16(%r15), %rax
	movq	24(%r15), %rdx
	movslq	%ebx, %rsi
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L301
	addq	%rax, %rsi
	movq	%rsi, 16(%r15)
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L300:
	movq	%r15, %rdi
	movl	%r8d, -60(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movl	-60(%rbp), %r8d
	movq	%rax, %rdi
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L294:
	xorl	%r13d, %r13d
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L297:
	movl	$16, %esi
	movq	%r15, %rdi
	movl	%ebx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %ecx
	movq	%rax, %r12
	jmp	.L283
.L301:
	movq	%r15, %rdi
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %ecx
	jmp	.L284
	.cfi_endproc
.LFE9977:
	.size	_ZN2v88internal12_GLOBAL__N_119ToCanonicalZoneListEPKNS_4base11SmallVectorINS0_14CharacterRangeELm8EEEPNS0_4ZoneE, .-_ZN2v88internal12_GLOBAL__N_119ToCanonicalZoneListEPKNS_4base11SmallVectorINS0_14CharacterRangeELm8EEEPNS0_4ZoneE
	.section	.text._ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	.type	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE, @function
_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE:
.LFB10554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	12(%rdi), %rax
	movl	8(%rdi), %ecx
	cmpl	%ecx, %eax
	jge	.L303
	leal	1(%rax), %ecx
	movq	(%rdi), %rdx
	movl	%ecx, 12(%rdi)
	movq	(%rsi), %rcx
	movq	%rcx, (%rdx,%rax,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	leal	1(%rcx,%rcx), %r12d
	movq	24(%rdx), %rax
	movq	16(%rdx), %rcx
	movq	(%rsi), %r13
	movslq	%r12d, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L309
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdx)
.L306:
	movslq	12(%rbx), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L310
.L307:
	addl	$1, %eax
	movl	%r12d, 8(%rbx)
	movq	%rcx, (%rbx)
	movl	%eax, 12(%rbx)
	movq	%r13, (%rcx,%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore_state
	movq	(%rbx), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%rbx), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L309:
	movq	%rdx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L306
	.cfi_endproc
.LFE10554:
	.size	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE, .-_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	.section	.text._ZN2v88internal17RegExpDisjunction30FixSingleCharacterDisjunctionsEPNS0_14RegExpCompilerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpDisjunction30FixSingleCharacterDisjunctionsEPNS0_14RegExpCompilerE
	.type	_ZN2v88internal17RegExpDisjunction30FixSingleCharacterDisjunctionsEPNS0_14RegExpCompilerE, @function
_ZN2v88internal17RegExpDisjunction30FixSingleCharacterDisjunctionsEPNS0_14RegExpCompilerE:
.LFB10000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	1096(%rsi), %rax
	movq	%rax, -104(%rbp)
	movl	12(%r15), %eax
	movl	%eax, -68(%rbp)
	testl	%eax, %eax
	jle	.L337
	leaq	-64(%rbp), %rax
	movl	$0, -72(%rbp)
	xorl	%r14d, %r14d
	movq	%rax, -136(%rbp)
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L314:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*152(%rax)
	cmpl	$1, 16(%rax)
	je	.L316
	movq	(%r15), %rax
	movslq	-72(%rbp), %rdx
	movl	-80(%rbp), %r14d
	movq	(%rax,%r13,8), %rcx
	movq	%rcx, (%rax,%rdx,8)
	leal	1(%rdx), %eax
	movl	%eax, -72(%rbp)
.L315:
	cmpl	%r14d, -68(%rbp)
	jle	.L312
.L313:
	movslq	%r14d, %r13
	leaq	0(,%r13,8), %rax
	movq	%rax, -96(%rbp)
	movq	(%r15), %rax
	movq	(%rax,%r13,8), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*160(%rax)
	leal	1(%r14), %ecx
	movl	%ecx, -80(%rbp)
	testb	%al, %al
	jne	.L314
	movq	(%r15), %rax
	movslq	-72(%rbp), %rdx
	movl	%ecx, %r14d
	movq	(%rax,%r13,8), %rcx
	movq	%rcx, (%rax,%rdx,8)
	leal	1(%rdx), %eax
	movl	%eax, -72(%rbp)
	cmpl	%r14d, -68(%rbp)
	jg	.L313
.L312:
	movl	-72(%rbp), %eax
	movl	%eax, 12(%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L353
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	movl	24(%rax), %edx
	movq	8(%rax), %rax
	leal	1(%r14), %ebx
	movzwl	(%rax), %eax
	movl	%edx, -84(%rbp)
	andw	$-1024, %ax
	cmpw	$-9216, %ax
	sete	-85(%rbp)
	movzbl	-85(%rbp), %edx
	cmpl	%ebx, -68(%rbp)
	jle	.L317
	leaq	0(,%r13,8), %rax
	movq	%r13, -112(%rbp)
	movl	%edx, %r13d
	addq	$8, %rax
	movl	%r14d, -116(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rax, %r12
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L355:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*152(%rax)
	cmpl	$1, 16(%rax)
	jne	.L318
	movl	-84(%rbp), %esi
	cmpl	%esi, 24(%rax)
	jne	.L318
	movq	8(%rax), %rax
	movzwl	(%rax), %eax
	andw	$-1024, %ax
	cmpw	$-9216, %ax
	sete	%al
	addl	$1, %ebx
	addq	$8, %r12
	orl	%eax, %r13d
	cmpl	-68(%rbp), %ebx
	je	.L354
.L320:
	movq	(%r15), %rax
	movq	(%rax,%r12), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*160(%rax)
	testb	%al, %al
	jne	.L355
.L318:
	movb	%r13b, -85(%rbp)
	movl	-116(%rbp), %r14d
	movq	-112(%rbp), %r13
	cmpl	-80(%rbp), %ebx
	jg	.L319
	cmpl	%r14d, %ebx
	jg	.L356
	movl	%ebx, %r14d
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L354:
	movb	%r13b, -85(%rbp)
	movl	-116(%rbp), %r14d
	movq	-112(%rbp), %r13
.L319:
	movl	%ebx, %eax
	subl	%r14d, %eax
	movl	%eax, %r14d
	movq	-104(%rbp), %rax
	movq	16(%rax), %r12
	movq	24(%rax), %rax
	movq	%rax, %rdx
	subq	%r12, %rdx
	cmpq	$15, %rdx
	jbe	.L357
	movq	-104(%rbp), %rcx
	leaq	16(%r12), %rdx
	movq	%rdx, 16(%rcx)
	movq	%rcx, %rdx
.L323:
	movq	16(%rdx), %rdx
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L358
	movq	-104(%rbp), %rcx
	leaq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
.L325:
	movq	%rdx, (%r12)
	movq	$2, 8(%r12)
	testl	%r14d, %r14d
	jle	.L330
	leal	-1(%r14), %eax
	movl	%ebx, -112(%rbp)
	movq	-104(%rbp), %r14
	leaq	1(%r13,%rax), %r13
	movq	-136(%rbp), %rbx
	movq	-96(%rbp), %rax
	salq	$3, %r13
	movq	%r13, -80(%rbp)
	movq	-128(%rbp), %r13
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L359:
	addq	$8, %r13
.L331:
	movq	(%r15), %rdx
	movq	(%rdx,%rax), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rax
	movzwl	(%rax), %eax
	movl	%eax, -64(%rbp)
	movl	%eax, -60(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	movq	%r13, %rax
	cmpq	%r13, -80(%rbp)
	jne	.L359
	movl	-112(%rbp), %ebx
.L330:
	testb	$16, -84(%rbp)
	je	.L329
	cmpb	$0, -85(%rbp)
	movl	$2, %r8d
	jne	.L327
.L329:
	xorl	%r8d, %r8d
.L327:
	movq	-104(%rbp), %rax
	movq	16(%rax), %r13
	movq	24(%rax), %rax
	movq	%rax, -80(%rbp)
	subq	%r13, %rax
	cmpq	$31, %rax
	jbe	.L360
	movq	-104(%rbp), %rdx
	leaq	32(%r13), %rax
	movq	%rax, 16(%rdx)
	movq	%rdx, %rsi
.L333:
	movl	-84(%rbp), %ecx
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	%ebx, %r14d
	call	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	movslq	-72(%rbp), %rdx
	movq	(%r15), %rax
	movq	%r13, (%rax,%rdx,8)
	leal	1(%rdx), %eax
	movl	%eax, -72(%rbp)
	jmp	.L315
.L317:
	movq	(%r15), %rax
	movslq	-72(%rbp), %rdx
	leal	1(%r14), %ebx
	movq	(%rax,%r13,8), %rcx
	movq	%rcx, (%rax,%rdx,8)
.L334:
	movl	-72(%rbp), %eax
	subl	%r14d, %eax
	movl	%ebx, %r14d
	addl	%ebx, %eax
	movl	%eax, -72(%rbp)
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L356:
	movq	(%r15), %rax
	movslq	-72(%rbp), %rdx
	movq	(%rax,%r13,8), %rcx
	movq	%rcx, (%rax,%rdx,8)
	jmp	.L334
.L337:
	movl	$0, -72(%rbp)
	jmp	.L312
.L360:
	movq	-104(%rbp), %rdi
	movl	$32, %esi
	movl	%r8d, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-80(%rbp), %r8d
	movq	-104(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L333
.L357:
	movq	-104(%rbp), %rdi
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %rdx
	movq	%rax, %r12
	movq	-104(%rbp), %rax
	movq	24(%rax), %rax
	jmp	.L323
.L358:
	movq	-104(%rbp), %rdi
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L325
.L353:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10000:
	.size	_ZN2v88internal17RegExpDisjunction30FixSingleCharacterDisjunctionsEPNS0_14RegExpCompilerE, .-_ZN2v88internal17RegExpDisjunction30FixSingleCharacterDisjunctionsEPNS0_14RegExpCompilerE
	.section	.text._ZN2v88internal14CharacterRange6NegateEPNS0_8ZoneListIS1_EES4_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CharacterRange6NegateEPNS0_8ZoneListIS1_EES4_PNS0_4ZoneE
	.type	_ZN2v88internal14CharacterRange6NegateEPNS0_8ZoneListIS1_EES4_PNS0_4ZoneE, @function
_ZN2v88internal14CharacterRange6NegateEPNS0_8ZoneListIS1_EES4_PNS0_4ZoneE:
.LFB10046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	12(%rdi), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L373
	movq	(%rdi), %rax
	movq	%rdi, %r8
	movl	(%rax), %ebx
	movl	4(%rax), %ecx
	testl	%ebx, %ebx
	je	.L379
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
.L363:
	movslq	%r13d, %rax
	leaq	8(,%rax,8), %r14
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L380:
	leal	1(%rax), %edx
	addl	$1, %r13d
	movl	%edx, 12(%r12)
	movq	(%r12), %rdx
	leaq	(%rdx,%rax,8), %rax
	movl	%r15d, (%rax)
	leal	1(%rcx), %r15d
	movl	%ebx, 4(%rax)
	cmpl	%r13d, %r10d
	jle	.L364
.L382:
	movq	(%r8), %rax
	addq	%r14, %rax
	addq	$8, %r14
	movl	4(%rax), %ecx
	movl	(%rax), %ebx
.L370:
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	subl	$1, %ebx
	cmpl	%edx, %eax
	jl	.L380
	movq	16(%r9), %rdi
	movq	24(%r9), %rax
	leal	1(%rdx,%rdx), %r11d
	movslq	%r11d, %rsi
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L381
	addq	%rdi, %rsi
	movq	%rsi, 16(%r9)
.L368:
	movslq	12(%r12), %rdx
	movq	%rdx, %rsi
	salq	$3, %rdx
	testl	%esi, %esi
	jle	.L369
	movq	(%r12), %rsi
	movq	%r9, -96(%rbp)
	movq	%r8, -88(%rbp)
	movl	%ecx, -80(%rbp)
	movl	%r11d, -76(%rbp)
	movl	%r10d, -72(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r8
	movl	-80(%rbp), %ecx
	movq	%rax, %rdi
	movl	-76(%rbp), %r11d
	movl	-72(%rbp), %r10d
	movq	%rdx, %rsi
	salq	$3, %rdx
.L369:
	movq	%rdi, (%r12)
	addl	$1, %esi
	addq	%rdx, %rdi
	addl	$1, %r13d
	movl	%r11d, 8(%r12)
	movl	%esi, 12(%r12)
	movl	%r15d, (%rdi)
	leal	1(%rcx), %r15d
	movl	%ebx, 4(%rdi)
	cmpl	%r13d, %r10d
	jg	.L382
.L364:
	cmpl	$1114110, %r15d
	jle	.L362
.L361:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L383
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	leal	1(%rcx), %r15d
	cmpl	$1, %r10d
	je	.L364
	movl	8(%rax), %ebx
	movl	12(%rax), %ecx
	movl	$1, %r13d
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L373:
	xorl	%r15d, %r15d
.L362:
	leaq	-64(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movl	%r15d, -64(%rbp)
	movl	$1114111, -60(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L381:
	movq	%r9, %rdi
	movq	%r8, -96(%rbp)
	movl	%ecx, -88(%rbp)
	movl	%r11d, -80(%rbp)
	movl	%r10d, -76(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %r9
	movl	-76(%rbp), %r10d
	movl	-80(%rbp), %r11d
	movl	-88(%rbp), %ecx
	movq	%rax, %rdi
	movq	-96(%rbp), %r8
	jmp	.L368
.L383:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10046:
	.size	_ZN2v88internal14CharacterRange6NegateEPNS0_8ZoneListIS1_EES4_PNS0_4ZoneE, .-_ZN2v88internal14CharacterRange6NegateEPNS0_8ZoneListIS1_EES4_PNS0_4ZoneE
	.section	.rodata._ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEPNS0_4ZoneE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEPNS0_4ZoneE
	.type	_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEPNS0_4ZoneE, @function
_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEPNS0_4ZoneE:
.LFB10027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$119, %dil
	jg	.L385
	movq	%rsi, %r12
	movq	%rdx, %r13
	cmpb	$67, %dil
	jle	.L469
	subl	$68, %edi
	cmpb	$51, %dil
	ja	.L385
	leaq	.L390(%rip), %rdx
	movzbl	%dil, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEPNS0_4ZoneE,"a",@progbits
	.align 4
	.align 4
.L390:
	.long	.L396-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L395-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L394-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L393-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L392-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L391-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L385-.L390
	.long	.L389-.L390
	.section	.text._ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEPNS0_4ZoneE
	.p2align 4,,10
	.p2align 3
.L469:
	cmpb	$42, %dil
	je	.L387
	cmpb	$46, %dil
	jne	.L385
	movslq	12(%rsi), %rax
	movl	8(%rsi), %edx
	cmpl	%eax, %edx
	jle	.L470
	leal	1(%rax), %ecx
	movq	(%rsi), %rdx
	movl	%ecx, 12(%rsi)
	movabsq	$38654705664, %rcx
	movq	%rcx, (%rdx,%rax,8)
.L444:
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%eax, %edx
	jle	.L471
	leal	1(%rax), %ecx
	movq	(%r12), %rdx
	movl	%ecx, 12(%r12)
	movabsq	$51539607563, %rcx
	movq	%rcx, (%rdx,%rax,8)
.L451:
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jge	.L454
	leal	1(%rax), %ecx
	movq	(%r12), %rdx
	movl	%ecx, 12(%r12)
	movabsq	$35351875813390, %rcx
	movq	%rcx, (%rdx,%rax,8)
.L455:
	movabsq	$4785070309122090, %rax
	jmp	.L468
.L389:
	leaq	_ZN2v88internal25regexp_compiler_constantsL11kWordRangesE(%rip), %rbx
	leaq	-64(%rbp), %r14
	leaq	32(%rbx), %r15
	.p2align 4,,10
	.p2align 3
.L413:
	movl	(%rbx), %edx
	movl	4(%rbx), %eax
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$8, %rbx
	movl	%edx, -64(%rbp)
	subl	$1, %eax
	movq	%r13, %rdx
	movl	%eax, -60(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	cmpq	%r15, %rbx
	jne	.L413
	jmp	.L384
.L396:
	movslq	12(%rsi), %rax
	movl	8(%rsi), %edx
	cmpl	%edx, %eax
	jge	.L472
	leal	1(%rax), %ecx
	movq	(%rsi), %rdx
	movl	%ecx, 12(%rsi)
	movabsq	$201863462912, %rcx
	movq	%rcx, (%rdx,%rax,8)
.L402:
	movabsq	$4785070309113914, %rax
.L468:
	leaq	-64(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
.L384:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L473
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L394:
	.cfi_restore_state
	movslq	12(%rsi), %rax
	movl	8(%rsi), %edx
	cmpl	%edx, %eax
	jge	.L474
	leal	1(%rax), %ecx
	movq	(%rsi), %rdx
	movl	%ecx, 12(%rsi)
	movabsq	$201863462912, %rcx
	movq	%rcx, (%rdx,%rax,8)
.L418:
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jge	.L475
	leal	1(%rax), %ecx
	movq	(%r12), %rdx
	movl	%ecx, 12(%r12)
	movabsq	$274877907002, %rcx
	movq	%rcx, (%rdx,%rax,8)
.L425:
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jge	.L476
	leal	1(%rax), %ecx
	movq	(%r12), %rdx
	movl	%ecx, 12(%r12)
	movabsq	$403726925915, %rcx
	movq	%rcx, (%rdx,%rax,8)
.L432:
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jge	.L435
	leal	1(%rax), %ecx
	movq	(%r12), %rdx
	movl	%ecx, 12(%r12)
	movabsq	$412316860512, %rcx
	movq	%rcx, (%rdx,%rax,8)
.L436:
	movabsq	$4785070309113979, %rax
	jmp	.L468
.L393:
	movabsq	$244813135920, %rax
	jmp	.L468
.L395:
	leaq	_ZN2v88internal25regexp_compiler_constantsL12kSpaceRangesE(%rip), %rbx
	xorl	%eax, %eax
	leaq	80(%rbx), %r15
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L478:
	leal	1(%rax), %edx
	addq	$8, %rbx
	movl	%edx, 12(%r12)
	movq	(%r12), %rdx
	leaq	(%rdx,%rax,8), %rax
	movl	%ecx, (%rax)
	movl	%r14d, 4(%rax)
	movl	-4(%rbx), %edx
	movl	%edx, %eax
	cmpq	%rbx, %r15
	je	.L477
.L412:
	movl	(%rbx), %ecx
	movl	8(%r12), %edx
	leal	-1(%rcx), %r14d
	movzwl	%ax, %ecx
	movslq	12(%r12), %rax
	cmpl	%edx, %eax
	jl	.L478
	movq	16(%r13), %rdi
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %r8d
	movslq	%r8d, %rsi
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L479
	addq	%rdi, %rsi
	movq	%rsi, 16(%r13)
.L410:
	movslq	12(%r12), %rdx
	movq	%rdx, %rsi
	salq	$3, %rdx
	testl	%esi, %esi
	jle	.L411
	movq	(%r12), %rsi
	movl	%r8d, -72(%rbp)
	movl	%ecx, -68(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movl	-72(%rbp), %r8d
	movl	-68(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rsi
	salq	$3, %rdx
.L411:
	movq	%rdi, (%r12)
	addl	$1, %esi
	addq	%rdx, %rdi
	addq	$8, %rbx
	movl	%r8d, 8(%r12)
	movl	%esi, 12(%r12)
	movl	%ecx, (%rdi)
	movl	%r14d, 4(%rdi)
	movl	-4(%rbx), %edx
	movl	%edx, %eax
	cmpq	%rbx, %r15
	jne	.L412
.L477:
	andl	$65535, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1114111, -60(%rbp)
	movl	%edx, -64(%rbp)
	movq	%r13, %rdx
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	jmp	.L384
.L392:
	leaq	-64(%rbp), %r14
	movq	%r13, %rdx
	movq	%r12, %rdi
	movabsq	$42949672970, %rax
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$55834574861, %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movabsq	$35360465756200, %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	jmp	.L384
.L391:
	leaq	_ZN2v88internal25regexp_compiler_constantsL12kSpaceRangesE(%rip), %rbx
	leaq	-64(%rbp), %r14
	leaq	80(%rbx), %r15
	.p2align 4,,10
	.p2align 3
.L406:
	movl	(%rbx), %edx
	movl	4(%rbx), %eax
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$8, %rbx
	movl	%edx, -64(%rbp)
	subl	$1, %eax
	movq	%r13, %rdx
	movl	%eax, -60(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	cmpq	%rbx, %r15
	jne	.L406
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L387:
	movabsq	$4785070309113856, %rax
	jmp	.L468
.L385:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L470:
	movq	16(%r13), %rcx
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %ebx
	movslq	%ebx, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rsi, %rax
	jb	.L480
	addq	%rcx, %rsi
	movq	%rsi, 16(%r13)
.L446:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L445
	movq	(%r12), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
.L445:
	addl	$1, %eax
	movq	%rcx, (%r12)
	movl	%eax, 12(%r12)
	movabsq	$38654705664, %rax
	movl	%ebx, 8(%r12)
	movq	%rax, (%rcx,%rdx)
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L454:
	movq	16(%r13), %rcx
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %ebx
	movslq	%ebx, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L481
	addq	%rcx, %rsi
	movq	%rsi, 16(%r13)
.L457:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L458
	movq	(%r12), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
.L458:
	addl	$1, %eax
	movq	%rcx, (%r12)
	movl	%eax, 12(%r12)
	movabsq	$35351875813390, %rax
	movl	%ebx, 8(%r12)
	movq	%rax, (%rcx,%rdx)
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L435:
	movq	16(%r13), %rcx
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %ebx
	movslq	%ebx, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L482
	addq	%rcx, %rsi
	movq	%rsi, 16(%r13)
.L438:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L439
	movq	(%r12), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
.L439:
	addl	$1, %eax
	movq	%rcx, (%r12)
	movl	%eax, 12(%r12)
	movabsq	$412316860512, %rax
	movl	%ebx, 8(%r12)
	movq	%rax, (%rcx,%rdx)
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L476:
	movq	16(%r13), %rcx
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %ebx
	movslq	%ebx, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L483
	addq	%rcx, %rsi
	movq	%rsi, 16(%r13)
.L434:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L433
	movq	(%r12), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
.L433:
	addl	$1, %eax
	movq	%rcx, (%r12)
	movl	%eax, 12(%r12)
	movabsq	$403726925915, %rax
	movl	%ebx, 8(%r12)
	movq	%rax, (%rcx,%rdx)
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L471:
	movq	16(%r13), %rcx
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %ebx
	movslq	%ebx, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rsi, %rax
	jb	.L484
	addq	%rcx, %rsi
	movq	%rsi, 16(%r13)
.L453:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L452
	movq	(%r12), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
.L452:
	addl	$1, %eax
	movq	%rcx, (%r12)
	movl	%eax, 12(%r12)
	movabsq	$51539607563, %rax
	movl	%ebx, 8(%r12)
	movq	%rax, (%rcx,%rdx)
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L475:
	movq	16(%r13), %rcx
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %ebx
	movslq	%ebx, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L485
	addq	%rcx, %rsi
	movq	%rsi, 16(%r13)
.L427:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L426
	movq	(%r12), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
.L426:
	addl	$1, %eax
	movq	%rcx, (%r12)
	movl	%eax, 12(%r12)
	movabsq	$274877907002, %rax
	movl	%ebx, 8(%r12)
	movq	%rax, (%rcx,%rdx)
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L474:
	movq	16(%r13), %rcx
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %ebx
	movslq	%ebx, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rsi, %rax
	jb	.L486
	addq	%rcx, %rsi
	movq	%rsi, 16(%r13)
.L420:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L419
	movq	(%r12), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
.L419:
	addl	$1, %eax
	movq	%rcx, (%r12)
	movl	%eax, 12(%r12)
	movabsq	$201863462912, %rax
	movl	%ebx, 8(%r12)
	movq	%rax, (%rcx,%rdx)
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L472:
	movq	16(%r13), %rcx
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %ebx
	movslq	%ebx, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L487
	addq	%rcx, %rsi
	movq	%rsi, 16(%r13)
.L404:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jle	.L403
	movq	(%r12), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
.L403:
	addl	$1, %eax
	movq	%rcx, (%r12)
	movl	%eax, 12(%r12)
	movabsq	$201863462912, %rax
	movl	%ebx, 8(%r12)
	movq	%rax, (%rcx,%rdx)
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L479:
	movq	%r13, %rdi
	movl	%r8d, -72(%rbp)
	movl	%ecx, -68(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-68(%rbp), %ecx
	movl	-72(%rbp), %r8d
	movq	%rax, %rdi
	jmp	.L410
.L484:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L453
.L487:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L404
.L486:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L420
.L485:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L427
.L483:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L434
.L482:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L438
.L481:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L457
.L480:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L446
.L473:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10027:
	.size	_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEPNS0_4ZoneE, .-_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEPNS0_4ZoneE
	.section	.text._ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	.type	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE, @function
_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE:
.LFB10576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movslq	12(%rdi), %rax
	movq	%rdi, %rbx
	movl	8(%rdi), %ecx
	cmpl	%ecx, %eax
	jge	.L489
	leal	1(%rax), %edx
	salq	$4, %rax
	addq	(%rdi), %rax
	movl	%edx, 12(%rdi)
	movdqu	(%rsi), %xmm1
	movups	%xmm1, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L489:
	.cfi_restore_state
	leal	1(%rcx,%rcx), %r14d
	movq	24(%rdx), %rax
	movq	16(%rdx), %rcx
	movq	(%rsi), %r12
	movq	8(%rsi), %r13
	movslq	%r14d, %rsi
	salq	$4, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L495
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdx)
.L492:
	movslq	12(%rbx), %rdx
	movq	%rdx, %rax
	salq	$4, %rdx
	testl	%eax, %eax
	jg	.L496
.L493:
	movq	%r12, %xmm0
	movq	%r13, %xmm2
	addl	$1, %eax
	movq	%rcx, (%rbx)
	punpcklqdq	%xmm2, %xmm0
	movl	%r14d, 8(%rbx)
	movl	%eax, 12(%rbx)
	movups	%xmm0, (%rcx,%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	movq	(%rbx), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%rbx), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$4, %rdx
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L495:
	movq	%rdx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L492
	.cfi_endproc
.LFE10576:
	.size	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE, .-_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	.section	.text._ZN2v88internal16RegExpQuantifier6ToNodeEiibPNS0_10RegExpTreeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeEb.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal16RegExpQuantifier6ToNodeEiibPNS0_10RegExpTreeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeEb.part.0, @function
_ZN2v88internal16RegExpQuantifier6ToNodeEiibPNS0_10RegExpTreeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeEb.part.0:
.LFB11824:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$136, %rsp
	movl	16(%rbp), %eax
	movl	%edi, -104(%rbp)
	movq	%r12, %rdi
	movl	%esi, -112(%rbp)
	movl	%edx, -160(%rbp)
	movl	%eax, %r13d
	movq	%r9, -144(%rbp)
	movl	%eax, -120(%rbp)
	movq	(%r12), %rax
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	*56(%rax)
	movq	%r12, %rdi
	movl	%eax, -116(%rbp)
	movq	(%r12), %rax
	call	*72(%rax)
	movq	1096(%rbx), %r15
	movq	%rax, -136(%rbp)
	movl	-116(%rbp), %eax
	testl	%eax, %eax
	jne	.L498
	movl	-104(%rbp), %r14d
	cmpl	$2147483647, -112(%rbp)
	setne	%r13b
	movl	8(%rbx), %ecx
	testl	%r14d, %r14d
	setg	%al
	movl	%ecx, -152(%rbp)
	orl	%eax, %r13d
	movb	%r13b, -108(%rbp)
	cmpl	$65534, %ecx
	jg	.L586
	movl	%ecx, %eax
	movl	$-1, %r14d
	addl	$1, %eax
	cmpb	$0, -108(%rbp)
	movl	%eax, 8(%rbx)
	jne	.L527
	.p2align 4,,10
	.p2align 3
.L501:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*56(%rax)
	movq	16(%r15), %r13
	movzbl	52(%rbx), %ecx
	testl	%eax, %eax
	movq	24(%r15), %rax
	sete	%r11b
	subq	%r13, %rax
	cmpq	$95, %rax
	jbe	.L587
	leaq	96(%r13), %rax
	movq	%rax, 16(%r15)
.L530:
	leaq	16+_ZTVN2v88internal10ChoiceNodeE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r15, 48(%r13)
	movq	%rax, 0(%r13)
	movq	$0, 8(%r13)
	movq	$0, 16(%r13)
	movq	$0, 24(%r13)
	movups	%xmm0, 32(%r13)
	movq	24(%r15), %rsi
	movq	16(%r15), %rdx
	movq	%r13, -128(%rbp)
	movq	%rsi, %rax
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L588
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r15)
.L532:
	subq	%rax, %rsi
	cmpq	$31, %rsi
	jbe	.L589
	leaq	32(%rax), %rsi
	movq	%rsi, 16(%r15)
.L534:
	movq	%rax, (%rdx)
	leaq	16+_ZTVN2v88internal14LoopChoiceNodeE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	$2, 8(%rdx)
	cmpb	$0, -120(%rbp)
	movq	%rax, 0(%r13)
	movl	-104(%rbp), %eax
	movq	%rdx, 56(%r13)
	movw	%si, 64(%r13)
	movb	%r11b, 88(%r13)
	movb	%cl, 89(%r13)
	movb	$0, 90(%r13)
	movl	%eax, 92(%r13)
	movups	%xmm0, 72(%r13)
	je	.L535
	cmpb	$0, 52(%rbx)
	jne	.L535
	movb	$1, 64(%r13)
.L535:
	cmpb	$0, -108(%rbp)
	movq	%r13, %rdx
	jne	.L590
.L536:
	movl	-116(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L591
	movq	(%r12), %rax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	*24(%rax)
.L547:
	movq	-136(%rbp), %rdi
	cmpl	$-1, %edi
	je	.L538
	movq	%rax, %rsi
	call	_ZN2v88internal10ActionNode13ClearCapturesENS0_8IntervalEPNS0_10RegExpNodeE@PLT
.L538:
	cmpl	$2147483647, -112(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	je	.L539
	movq	16(%r15), %rsi
	movq	24(%r15), %rax
	subq	%rsi, %rax
	cmpq	$15, %rax
	jbe	.L592
	leaq	16(%rsi), %rax
	movq	%rax, 16(%r15)
.L541:
	movl	-112(%rbp), %eax
	movl	%r14d, (%rsi)
	leaq	-96(%rbp), %rdi
	movq	%r15, %rdx
	movl	$0, 4(%rsi)
	movl	%eax, 8(%rsi)
	call	_ZN2v88internal18GuardedAlternative8AddGuardEPNS0_5GuardEPNS0_4ZoneE@PLT
.L539:
	movq	-144(%rbp), %rax
	movl	-104(%rbp), %edx
	movq	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
	testl	%edx, %edx
	jg	.L593
.L542:
	cmpb	$0, -160(%rbp)
	je	.L545
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal14LoopChoiceNode18AddLoopAlternativeENS0_18GuardedAlternativeE@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal14LoopChoiceNode22AddContinueAlternativeENS0_18GuardedAlternativeE@PLT
.L546:
	cmpb	$0, -108(%rbp)
	je	.L497
	movq	%r13, %rdx
	xorl	%esi, %esi
	movl	%r14d, %edi
	call	_ZN2v88internal10ActionNode18SetRegisterForLoopEiiPNS0_10RegExpNodeE@PLT
	movq	%rax, -128(%rbp)
.L497:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L594
	movq	-128(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	.cfi_restore_state
	cmpb	$0, 51(%rbx)
	je	.L503
	cmpl	$-1, -136(%rbp)
	je	.L595
.L503:
	movl	-104(%rbp), %r10d
	movl	$-1, %r14d
	movl	$-1, -152(%rbp)
	testl	%r10d, %r10d
	setg	%r13b
	cmpl	$2147483647, -112(%rbp)
	setne	%al
	orl	%eax, %r13d
	movb	%r13b, -108(%rbp)
	cmpb	$0, -108(%rbp)
	je	.L501
.L527:
	movl	8(%rbx), %r14d
	cmpl	$65534, %r14d
	jg	.L500
	leal	1(%r14), %eax
	movb	$1, -108(%rbp)
	movl	%eax, 8(%rbx)
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L545:
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal14LoopChoiceNode22AddContinueAlternativeENS0_18GuardedAlternativeE@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal14LoopChoiceNode18AddLoopAlternativeENS0_18GuardedAlternativeE@PLT
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L593:
	movq	1096(%rbx), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$15, %rax
	jbe	.L596
	leaq	16(%rsi), %rax
	movq	%rax, 16(%rdi)
.L544:
	movl	-104(%rbp), %eax
	movl	%r14d, (%rsi)
	leaq	-80(%rbp), %rdi
	movq	%r15, %rdx
	movl	$1, 4(%rsi)
	movl	%eax, 8(%rsi)
	call	_ZN2v88internal18GuardedAlternative8AddGuardEPNS0_5GuardEPNS0_4ZoneE@PLT
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L591:
	movq	%rdx, %rcx
	movl	-152(%rbp), %edi
	movl	-104(%rbp), %edx
	movl	%r14d, %esi
	call	_ZN2v88internal10ActionNode15EmptyMatchCheckEiiiPNS0_10RegExpNodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	(%r12), %rax
	call	*24(%rax)
	movl	-152(%rbp), %edi
	xorl	%esi, %esi
	movq	%rax, %rdx
	call	_ZN2v88internal10ActionNode13StorePositionEibPNS0_10RegExpNodeE@PLT
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L590:
	movq	%r13, %rsi
	movl	%r14d, %edi
	call	_ZN2v88internal10ActionNode17IncrementRegisterEiPNS0_10RegExpNodeE@PLT
	movq	%rax, %rdx
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L586:
	movb	$1, 49(%rbx)
	movl	$-1, %r14d
	testb	%r13b, %r13b
	je	.L501
	movl	%ecx, %r14d
.L500:
	movb	$1, 49(%rbx)
	movb	$1, -108(%rbp)
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L588:
	movl	$16, %esi
	movq	%r15, %rdi
	movb	%cl, -162(%rbp)
	movb	%r11b, -161(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r15), %rsi
	movzbl	-161(%rbp), %r11d
	movq	%rax, %rdx
	movzbl	-162(%rbp), %ecx
	movq	16(%r15), %rax
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L587:
	movl	$96, %esi
	movq	%r15, %rdi
	movb	%cl, -161(%rbp)
	movb	%r11b, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-128(%rbp), %r11d
	movzbl	-161(%rbp), %ecx
	movq	%rax, %r13
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L589:
	movl	$32, %esi
	movq	%r15, %rdi
	movq	%rdx, -176(%rbp)
	movb	%cl, -162(%rbp)
	movb	%r11b, -161(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-161(%rbp), %r11d
	movzbl	-162(%rbp), %ecx
	movq	-176(%rbp), %rdx
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L595:
	movl	56(%rbx), %eax
	movl	-104(%rbp), %ecx
	movl	%eax, -108(%rbp)
	leal	-1(%rcx), %edx
	cmpl	$6, %eax
	jg	.L504
	xorl	%eax, %eax
	cmpl	%ecx, -112(%rbp)
	setne	%al
	addl	%ecx, %eax
	cmpl	$6, %eax
	jle	.L505
	movl	$7, 56(%rbx)
.L504:
	cmpl	$2, %edx
	jbe	.L508
	movl	-108(%rbp), %eax
	cmpl	$3, -112(%rbp)
	movl	%eax, 56(%rbx)
	jg	.L503
	movl	-104(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L503
	cmpl	$6, -108(%rbp)
	jg	.L508
.L549:
	movl	-112(%rbp), %ecx
	movl	-108(%rbp), %eax
	imull	%ecx, %eax
	movl	%eax, 56(%rbx)
	cmpl	$6, %eax
	jg	.L508
	testl	%ecx, %ecx
	jle	.L554
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %rax
	movb	%r14b, -116(%rbp)
	movq	-144(%rbp), %r11
	movq	%rbx, -104(%rbp)
	movq	%r15, %r14
	movq	%r12, %rbx
	movl	%ecx, %r15d
	movb	%r13b, -136(%rbp)
	movq	%rax, %r13
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L601:
	movq	(%rbx), %rax
	movq	%r11, %rdx
	movq	-104(%rbp), %rsi
	movq	%rbx, %rdi
	call	*24(%rax)
	movq	$0, -72(%rbp)
	movq	%r13, %rsi
	movq	%rax, -80(%rbp)
	movq	48(%r12), %rdx
	movq	56(%r12), %rdi
	call	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	movq	-144(%rbp), %rax
.L585:
	movq	%rax, -80(%rbp)
	movq	%r13, %rsi
	movq	$0, -72(%rbp)
	movq	48(%r12), %rdx
	movq	56(%r12), %rdi
	call	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	cmpb	$0, -136(%rbp)
	je	.L525
	movq	-104(%rbp), %rax
	cmpb	$0, 52(%rax)
	jne	.L525
	movb	$1, 64(%r12)
.L525:
	addl	$1, %r15d
	cmpl	%r15d, -112(%rbp)
	je	.L597
	movq	%r12, %r11
.L526:
	movq	16(%r14), %r12
	movq	24(%r14), %rax
	subq	%r12, %rax
	cmpq	$71, %rax
	jbe	.L598
	leaq	72(%r12), %rax
	movq	%rax, 16(%r14)
.L518:
	leaq	16+_ZTVN2v88internal10ChoiceNodeE(%rip), %rax
	pxor	%xmm1, %xmm1
	movq	%r14, 48(%r12)
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movups	%xmm1, 32(%r12)
	movq	24(%r14), %rdx
	movq	16(%r14), %r9
	movq	%r12, -128(%rbp)
	movq	%rdx, %rax
	subq	%r9, %rax
	cmpq	$15, %rax
	jbe	.L599
	leaq	16(%r9), %rax
	movq	%rax, 16(%r14)
.L520:
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L600
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%r14)
.L522:
	xorl	%edi, %edi
	cmpb	$0, -116(%rbp)
	movq	%rax, (%r9)
	movq	$2, 8(%r9)
	movq	%r9, 56(%r12)
	movw	%di, 64(%r12)
	jne	.L601
	movq	-144(%rbp), %rax
	movq	%r13, %rsi
	movq	%r9, %rdi
	movq	%r11, -152(%rbp)
	movq	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	48(%r12), %rdx
	call	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	movq	(%rbx), %rax
	movq	-104(%rbp), %rsi
	movq	%rbx, %rdi
	movq	-152(%rbp), %r11
	movq	%r11, %rdx
	call	*24(%rax)
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L508:
	movl	$-1, -152(%rbp)
	movl	-108(%rbp), %eax
	movl	%eax, 56(%rbx)
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L592:
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L596:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L505:
	imull	-108(%rbp), %eax
	movl	%eax, 56(%rbx)
	cmpl	$2, %edx
	ja	.L506
	cmpl	$6, %eax
	jg	.L508
	movl	-112(%rbp), %eax
	cmpl	$2147483647, %eax
	je	.L511
	movq	-144(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movl	-104(%rbp), %ecx
	subl	%ecx, %eax
	movl	%eax, -112(%rbp)
	je	.L512
.L511:
	subq	$8, %rsp
	movq	-144(%rbp), %r9
	movl	-112(%rbp), %esi
	xorl	%edi, %edi
	pushq	$1
	movzbl	-160(%rbp), %edx
	movq	%rbx, %r8
	movq	%r12, %rcx
	call	_ZN2v88internal16RegExpQuantifier6ToNodeEiibPNS0_10RegExpTreeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeEb.part.0
	popq	%r8
	popq	%r9
	movq	%rax, -128(%rbp)
.L512:
	movq	-128(%rbp), %rdx
	movl	-104(%rbp), %r14d
	xorl	%r13d, %r13d
.L513:
	movq	(%r12), %rax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addl	$1, %r13d
	call	*24(%rax)
	movq	%rax, %rdx
	cmpl	%r13d, %r14d
	jne	.L513
	movq	%rax, -128(%rbp)
	movl	-108(%rbp), %eax
	movl	%eax, 56(%rbx)
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L597:
	movq	-104(%rbp), %rbx
.L516:
	movl	-108(%rbp), %eax
	movl	%eax, 56(%rbx)
	jmp	.L497
.L600:
	movl	$32, %esi
	movq	%r14, %rdi
	movq	%r11, -160(%rbp)
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %r11
	jmp	.L522
.L599:
	movl	$16, %esi
	movq	%r14, %rdi
	movq	%r11, -152(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r14), %rdx
	movq	-152(%rbp), %r11
	movq	%rax, %r9
	movq	16(%r14), %rax
	jmp	.L520
.L598:
	movl	$72, %esi
	movq	%r14, %rdi
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-128(%rbp), %r11
	movq	%rax, %r12
	jmp	.L518
.L506:
	movl	-108(%rbp), %eax
	cmpl	$3, -112(%rbp)
	movl	%eax, 56(%rbx)
	jg	.L503
	movl	-104(%rbp), %eax
	testl	%eax, %eax
	je	.L549
	jmp	.L503
.L554:
	movq	-144(%rbp), %rax
	movq	%rax, -128(%rbp)
	jmp	.L516
.L594:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11824:
	.size	_ZN2v88internal16RegExpQuantifier6ToNodeEiibPNS0_10RegExpTreeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeEb.part.0, .-_ZN2v88internal16RegExpQuantifier6ToNodeEiibPNS0_10RegExpTreeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeEb.part.0
	.section	.text._ZN2v88internal16RegExpQuantifier6ToNodeEiibPNS0_10RegExpTreeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RegExpQuantifier6ToNodeEiibPNS0_10RegExpTreeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeEb
	.type	_ZN2v88internal16RegExpQuantifier6ToNodeEiibPNS0_10RegExpTreeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeEb, @function
_ZN2v88internal16RegExpQuantifier6ToNodeEiibPNS0_10RegExpTreeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeEb:
.LFB10054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	16(%rbp), %eax
	testl	%esi, %esi
	je	.L603
	movzbl	%al, %eax
	movzbl	%dl, %edx
	movl	%eax, 16(%rbp)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal16RegExpQuantifier6ToNodeEiibPNS0_10RegExpTreeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeEb.part.0
	.p2align 4,,10
	.p2align 3
.L603:
	.cfi_restore_state
	movq	%r9, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10054:
	.size	_ZN2v88internal16RegExpQuantifier6ToNodeEiibPNS0_10RegExpTreeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeEb, .-_ZN2v88internal16RegExpQuantifier6ToNodeEiibPNS0_10RegExpTreeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeEb
	.section	.text._ZN2v88internal16RegExpQuantifier6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RegExpQuantifier6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.type	_ZN2v88internal16RegExpQuantifier6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, @function
_ZN2v88internal16RegExpQuantifier6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE:
.LFB10005:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	movl	20(%rdi), %esi
	movq	%rdx, %rax
	testl	%esi, %esi
	je	.L610
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	32(%rdi), %r9d
	xorl	%edx, %edx
	movq	8(%rdi), %rcx
	movl	16(%rdi), %edi
	testl	%r9d, %r9d
	movq	%rax, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	sete	%dl
	subq	$8, %rsp
	pushq	$0
	call	_ZN2v88internal16RegExpQuantifier6ToNodeEiibPNS0_10RegExpTreeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeEb.part.0
	popq	%r10
	popq	%r11
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L610:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE10005:
	.size	_ZN2v88internal16RegExpQuantifier6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, .-_ZN2v88internal16RegExpQuantifier6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal8ZoneListIPNS0_10RegExpTreeEE3AddERKS3_PNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8ZoneListIPNS0_10RegExpTreeEE3AddERKS3_PNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8ZoneListIPNS0_10RegExpTreeEE3AddERKS3_PNS0_4ZoneE
	.type	_ZN2v88internal8ZoneListIPNS0_10RegExpTreeEE3AddERKS3_PNS0_4ZoneE, @function
_ZN2v88internal8ZoneListIPNS0_10RegExpTreeEE3AddERKS3_PNS0_4ZoneE:
.LFB10794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	12(%rdi), %rax
	movl	8(%rdi), %ecx
	cmpl	%ecx, %eax
	jge	.L614
	leal	1(%rax), %ecx
	movq	(%rdi), %rdx
	movl	%ecx, 12(%rdi)
	movq	(%rsi), %rcx
	movq	%rcx, (%rdx,%rax,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	.cfi_restore_state
	leal	1(%rcx,%rcx), %r13d
	movq	24(%rdx), %rax
	movq	16(%rdx), %rcx
	movq	(%rsi), %r12
	movslq	%r13d, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L620
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdx)
.L617:
	movslq	12(%rbx), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L621
.L618:
	addl	$1, %eax
	movl	%r13d, 8(%rbx)
	movq	%rcx, (%rbx)
	movl	%eax, 12(%rbx)
	movq	%r12, (%rcx,%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L621:
	.cfi_restore_state
	movq	(%rbx), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%rbx), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L620:
	movq	%rdx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L617
	.cfi_endproc
.LFE10794:
	.size	_ZN2v88internal8ZoneListIPNS0_10RegExpTreeEE3AddERKS3_PNS0_4ZoneE, .-_ZN2v88internal8ZoneListIPNS0_10RegExpTreeEE3AddERKS3_PNS0_4ZoneE
	.section	.text._ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEm,"axG",@progbits,_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEm
	.type	_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEm, @function
_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEm:
.LFB11370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rdi), %r12
	movq	16(%rdi), %rdi
	subq	%rax, %r12
	subq	%rax, %rdi
	sarq	$3, %rdi
	addq	%rdi, %rdi
	cmpq	%rsi, %rdi
	cmovb	%rsi, %rdi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	leaq	0(,%rax,8), %r14
	movq	%r14, %rdi
	call	malloc@PLT
	movq	(%rbx), %r15
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	movq	%r15, %rsi
	call	memcpy@PLT
	leaq	24(%rbx), %rax
	cmpq	%rax, %r15
	je	.L623
	movq	%r15, %rdi
	call	free@PLT
.L623:
	leaq	0(%r13,%r12), %rax
	movq	%r13, (%rbx)
	addq	%r14, %r13
	movq	%r13, 16(%rbx)
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11370:
	.size	_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEm, .-_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEm
	.section	.text._ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEv,"axG",@progbits,_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEv
	.type	_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEv, @function
_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEv:
.LFB11096:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEm
	.cfi_endproc
.LFE11096:
	.size	_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEv, .-_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEv
	.section	.text._ZN2v88internal20UnicodeRangeSplitter8AddRangeENS0_14CharacterRangeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20UnicodeRangeSplitter8AddRangeENS0_14CharacterRangeE
	.type	_ZN2v88internal20UnicodeRangeSplitter8AddRangeENS0_14CharacterRangeE, @function
_ZN2v88internal20UnicodeRangeSplitter8AddRangeENS0_14CharacterRangeE:
.LFB9976:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	sarq	$32, %rbx
	subq	$40, %rsp
	testl	%ebx, %ebx
	js	.L626
	testl	%esi, %esi
	movl	$0, %r15d
	movq	%rdi, %r12
	movq	%rsi, %r13
	cmovns	%esi, %r15d
	movl	$55295, %r14d
	cmpl	$55295, %ebx
	leaq	88(%rdi), %r10
	cmovle	%ebx, %r14d
	leaq	176(%rdi), %r9
	leaq	264(%rdi), %r8
	cmpl	%r15d, %r14d
	jl	.L628
	movq	8(%rdi), %rax
	cmpq	%rax, 16(%rdi)
	je	.L629
.L630:
	movl	%r15d, (%rax)
	addq	$8, %rax
	movl	%r14d, -4(%rax)
	movq	%rax, 8(%r12)
.L628:
	cmpl	$55295, %ebx
	jle	.L626
	cmpl	$55296, %r13d
	movl	$55296, %r14d
	movl	$56319, %r15d
	cmovge	%r13d, %r14d
	cmpl	$56319, %ebx
	cmovle	%ebx, %r15d
	cmpl	%r15d, %r14d
	jg	.L631
	movq	96(%r12), %rax
	cmpq	104(%r12), %rax
	je	.L632
.L633:
	movl	%r14d, (%rax)
	addq	$8, %rax
	movl	%r15d, -4(%rax)
	movq	%rax, 96(%r12)
.L631:
	cmpl	$56319, %ebx
	jle	.L626
	cmpl	$56320, %r13d
	movl	$56320, %r14d
	movl	$57343, %r15d
	cmovge	%r13d, %r14d
	cmpl	$57343, %ebx
	cmovle	%ebx, %r15d
	cmpl	%r15d, %r14d
	jg	.L634
	movq	184(%r12), %rax
	cmpq	192(%r12), %rax
	je	.L635
.L636:
	movl	%r14d, (%rax)
	addq	$8, %rax
	movl	%r15d, -4(%rax)
	movq	%rax, 184(%r12)
.L634:
	cmpl	$57343, %ebx
	jle	.L626
	cmpl	$57344, %r13d
	movl	$57344, %r14d
	movl	$65535, %r15d
	cmovge	%r13d, %r14d
	cmpl	$65535, %ebx
	cmovle	%ebx, %r15d
	cmpl	%r15d, %r14d
	jg	.L637
	movq	8(%r12), %rax
	cmpq	16(%r12), %rax
	je	.L638
.L639:
	movl	%r14d, (%rax)
	addq	$8, %rax
	movl	%r15d, -4(%rax)
	movq	%rax, 8(%r12)
.L637:
	cmpl	$65535, %ebx
	jle	.L626
	cmpl	$65536, %r13d
	movl	$65536, %esi
	movl	$1114111, %eax
	cmovl	%esi, %r13d
	cmpl	$1114111, %ebx
	cmovg	%eax, %ebx
	cmpl	%r13d, %ebx
	jl	.L626
	movq	272(%r12), %rax
	cmpq	280(%r12), %rax
	je	.L643
.L641:
	movl	%r13d, (%rax)
	addq	$8, %rax
	movl	%ebx, -4(%rax)
	movq	%rax, 272(%r12)
.L626:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L638:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEv
	movq	-56(%rbp), %r8
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L643:
	movq	%r8, %rdi
	call	_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEv
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L629:
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEv
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L632:
	movq	%r10, %rdi
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEv
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L635:
	movq	%r9, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEv
	movq	-56(%rbp), %r8
	jmp	.L636
	.cfi_endproc
.LFE9976:
	.size	_ZN2v88internal20UnicodeRangeSplitter8AddRangeENS0_14CharacterRangeE, .-_ZN2v88internal20UnicodeRangeSplitter8AddRangeENS0_14CharacterRangeE
	.section	.text._ZN2v88internal20UnicodeRangeSplitterC2EPNS0_8ZoneListINS0_14CharacterRangeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20UnicodeRangeSplitterC2EPNS0_8ZoneListINS0_14CharacterRangeEEE
	.type	_ZN2v88internal20UnicodeRangeSplitterC2EPNS0_8ZoneListINS0_14CharacterRangeEEE, @function
_ZN2v88internal20UnicodeRangeSplitterC2EPNS0_8ZoneListINS0_14CharacterRangeEEE:
.LFB9974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24(%rdi), %rax
	movq	%rdi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movq	%rax, 8(%rdi)
	leaq	88(%rdi), %rax
	movq	%rax, 16(%rdi)
	movq	%rax, -56(%rbp)
	leaq	112(%rdi), %rax
	movq	%rax, 88(%rdi)
	movq	%rax, 96(%rdi)
	leaq	176(%rdi), %rax
	movq	%rax, 104(%rdi)
	movq	%rax, -64(%rbp)
	leaq	200(%rdi), %rax
	movq	%rax, 176(%rdi)
	movq	%rax, 184(%rdi)
	leaq	264(%rdi), %rax
	movq	%rax, 192(%rdi)
	movq	%rax, -72(%rbp)
	leaq	288(%rdi), %rax
	movq	%rax, 264(%rdi)
	movq	%rax, 272(%rdi)
	leaq	352(%rdi), %rax
	movq	%rax, 280(%rdi)
	movl	12(%rsi), %edi
	testl	%edi, %edi
	jle	.L644
	movq	%rsi, %r13
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L684:
	cmpl	$55295, %r12d
	jg	.L648
	movq	8(%rdx), %rax
	movl	$55295, %r10d
	cmpq	16(%rdx), %rax
	je	.L649
.L650:
	movl	%r8d, (%rax)
	addq	$8, %rax
	movl	%r10d, -4(%rax)
	movq	%rax, 8(%rdx)
.L648:
	cmpl	$55295, %ebx
	jle	.L683
	cmpl	$55296, %r12d
	movl	$55296, %r8d
	cmovge	%r12d, %r8d
	cmpl	$56318, %ebx
	jle	.L652
	cmpl	$56319, %r12d
	jg	.L653
	movq	96(%rdx), %rax
	movl	$56319, %r10d
	cmpq	104(%rdx), %rax
	je	.L654
.L655:
	movl	%r8d, (%rax)
	addq	$8, %rax
	movl	%r10d, -4(%rax)
	movq	%rax, 96(%rdx)
.L653:
	cmpl	$56319, %ebx
	jle	.L683
	cmpl	$56320, %r12d
	movl	$56320, %r8d
	cmovge	%r12d, %r8d
	cmpl	$57342, %ebx
	jle	.L657
	cmpl	$57343, %r12d
	jg	.L658
	movq	184(%rdx), %rax
	movl	$57343, %r10d
	cmpq	192(%rdx), %rax
	je	.L659
.L660:
	movl	%r8d, (%rax)
	addq	$8, %rax
	movl	%r10d, -4(%rax)
	movq	%rax, 184(%rdx)
.L658:
	cmpl	$57343, %ebx
	jle	.L683
	cmpl	$57344, %r12d
	movl	$57344, %r8d
	cmovge	%r12d, %r8d
	cmpl	$65534, %ebx
	jle	.L662
	cmpl	$65535, %r12d
	jg	.L663
	movq	8(%rdx), %rax
	movl	$65535, %r10d
	cmpq	16(%rdx), %rax
	je	.L664
.L665:
	movl	%r8d, (%rax)
	addq	$8, %rax
	movl	%r10d, -4(%rax)
	movq	%rax, 8(%rdx)
.L663:
	cmpl	$65535, %ebx
	jle	.L683
	cmpl	$65536, %r12d
	movl	$65536, %eax
	cmovl	%eax, %r12d
	cmpl	$1114111, %ebx
	movl	$1114111, %eax
	cmovge	%eax, %ebx
	cmpl	%ebx, %r12d
	jg	.L683
	movq	272(%rdx), %rax
	cmpq	280(%rdx), %rax
	je	.L669
.L670:
	movl	%r12d, (%rax)
	addq	$8, %rax
	movl	%ebx, -4(%rax)
	movq	%rax, 272(%rdx)
.L683:
	movl	12(%r13), %edi
.L646:
	addq	$1, %r14
	cmpl	%r14d, %edi
	jle	.L644
.L671:
	movq	0(%r13), %rax
	leaq	(%rax,%r14,8), %rax
	movl	4(%rax), %ebx
	movl	(%rax), %r12d
	testl	%ebx, %ebx
	js	.L646
	testl	%r12d, %r12d
	movl	%r15d, %r8d
	cmovns	%r12d, %r8d
	cmpl	$55294, %ebx
	jg	.L684
	cmpl	%ebx, %r8d
	jg	.L646
	movq	8(%rdx), %rax
	movl	%ebx, %r10d
	cmpq	16(%rdx), %rax
	jne	.L650
.L649:
	movq	%rdx, %rdi
	movl	%r10d, -96(%rbp)
	movl	%r8d, -84(%rbp)
	movq	%rdx, -80(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEv
	movq	-80(%rbp), %rdx
	movl	-84(%rbp), %r8d
	movl	-96(%rbp), %r10d
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L652:
	cmpl	%ebx, %r8d
	jg	.L683
	movq	96(%rdx), %rax
	movl	%ebx, %r10d
	cmpq	104(%rdx), %rax
	jne	.L655
.L654:
	movq	-56(%rbp), %rdi
	movq	%rdx, -96(%rbp)
	movl	%r10d, -84(%rbp)
	movl	%r8d, -80(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEv
	movl	-80(%rbp), %r8d
	movl	-84(%rbp), %r10d
	movq	-96(%rbp), %rdx
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L657:
	cmpl	%ebx, %r8d
	jg	.L683
	movq	184(%rdx), %rax
	movl	%ebx, %r10d
	cmpq	192(%rdx), %rax
	jne	.L660
.L659:
	movq	-64(%rbp), %rdi
	movq	%rdx, -96(%rbp)
	movl	%r10d, -84(%rbp)
	movl	%r8d, -80(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEv
	movl	-80(%rbp), %r8d
	movl	-84(%rbp), %r10d
	movq	-96(%rbp), %rdx
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L662:
	cmpl	%ebx, %r8d
	jg	.L683
	movq	8(%rdx), %rax
	movl	%ebx, %r10d
	cmpq	16(%rdx), %rax
	jne	.L665
.L664:
	movq	%rdx, %rdi
	movl	%r10d, -96(%rbp)
	movl	%r8d, -84(%rbp)
	movq	%rdx, -80(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEv
	movq	-80(%rbp), %rdx
	movl	-84(%rbp), %r8d
	movl	-96(%rbp), %r10d
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L644:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L669:
	.cfi_restore_state
	movq	-72(%rbp), %rdi
	movq	%rdx, -80(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal14CharacterRangeELm8EE4GrowEv
	movq	-80(%rbp), %rdx
	jmp	.L670
	.cfi_endproc
.LFE9974:
	.size	_ZN2v88internal20UnicodeRangeSplitterC2EPNS0_8ZoneListINS0_14CharacterRangeEEE, .-_ZN2v88internal20UnicodeRangeSplitterC2EPNS0_8ZoneListINS0_14CharacterRangeEEE
	.globl	_ZN2v88internal20UnicodeRangeSplitterC1EPNS0_8ZoneListINS0_14CharacterRangeEEE
	.set	_ZN2v88internal20UnicodeRangeSplitterC1EPNS0_8ZoneListINS0_14CharacterRangeEEE,_ZN2v88internal20UnicodeRangeSplitterC2EPNS0_8ZoneListINS0_14CharacterRangeEEE
	.section	.text._ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_,"axG",@progbits,_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_,comdat
	.p2align 4
	.weak	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	.type	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_, @function
_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_:
.LFB11595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L685
	leaq	8(%rdi), %rbx
	movq	%rdi, %r13
	cmpq	%rbx, %rsi
	je	.L685
	movq	%rsi, -80(%rbp)
	movq	%rdx, %r12
	leaq	-64(%rbp), %r15
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L696:
	movq	(%rbx), %r14
	cmpq	%rbx, %r13
	je	.L689
	movq	%rbx, %rdx
	leaq	8(%r13), %rdi
	movq	%r13, %rsi
	subq	%r13, %rdx
	call	memmove@PLT
.L689:
	movq	%r14, 0(%r13)
	addq	$8, %rbx
	cmpq	-80(%rbp), %rbx
	je	.L685
.L693:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*%r12
	testl	%eax, %eax
	js	.L696
	movq	(%rbx), %rax
	leaq	-8(%rbx), %r14
	movq	%rax, -64(%rbp)
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L697:
	movq	(%r14), %rax
	subq	$8, %r14
	movq	%rax, 16(%r14)
.L692:
	leaq	8(%r14), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -72(%rbp)
	call	*%r12
	testl	%eax, %eax
	js	.L697
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rcx
	addq	$8, %rbx
	movq	%rax, (%rcx)
	cmpq	-80(%rbp), %rbx
	jne	.L693
.L685:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L698
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L698:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11595:
	.size	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_, .-_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	.section	.text._ZSt24__merge_sort_with_bufferIPPN2v88internal10RegExpTreeES4_N9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_,"axG",@progbits,_ZSt24__merge_sort_with_bufferIPPN2v88internal10RegExpTreeES4_N9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_,comdat
	.p2align 4
	.weak	_ZSt24__merge_sort_with_bufferIPPN2v88internal10RegExpTreeES4_N9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_
	.type	_ZSt24__merge_sort_with_bufferIPPN2v88internal10RegExpTreeES4_N9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_, @function
_ZSt24__merge_sort_with_bufferIPPN2v88internal10RegExpTreeES4_N9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_:
.LFB11599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	%rdi, %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	subq	$88, %rsp
	movq	%rdi, -120(%rbp)
	movq	%rax, -104(%rbp)
	leaq	(%rdx,%rbx), %rax
	movq	%rsi, -56(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	$48, %rbx
	jle	.L700
	movq	%rdi, %r12
	movq	%rsi, %r13
	movq	%rcx, %r14
	.p2align 4,,10
	.p2align 3
.L701:
	movq	%r12, %rdi
	addq	$56, %r12
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	%r13, %rax
	subq	%r12, %rax
	cmpq	$48, %rax
	jg	.L701
	movq	-80(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	cmpq	$56, %rbx
	jle	.L699
	movl	$7, %ebx
	.p2align 4,,10
	.p2align 3
.L740:
	leaq	(%rbx,%rbx), %rax
	movq	%rax, -72(%rbp)
	cmpq	%rax, -104(%rbp)
	jl	.L745
	movq	%rbx, %rax
	movq	-120(%rbp), %rsi
	movq	-128(%rbp), %r13
	movq	%rbx, -112(%rbp)
	salq	$4, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	%rsi, %r14
	leaq	(%rsi,%rbx,8), %r15
	leaq	(%r14,%rax), %rbx
	cmpq	%r15, %rbx
	je	.L704
	.p2align 4,,10
	.p2align 3
.L767:
	movq	%r15, %r12
	.p2align 4,,10
	.p2align 3
.L708:
	movq	-80(%rbp), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	js	.L765
	movq	(%r14), %rax
	addq	$8, %r14
	addq	$8, %r13
	movq	%rax, -8(%r13)
	cmpq	%r15, %r14
	je	.L706
	cmpq	%r12, %rbx
	jne	.L708
.L706:
	movq	%r15, %rdx
	subq	%r14, %rdx
	cmpq	%r14, %r15
	je	.L709
.L743:
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %rdx
.L709:
	movq	%rbx, %r14
	addq	%rdx, %r13
	subq	%r12, %r14
	cmpq	%r12, %rbx
	je	.L710
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	addq	%r14, %r13
	call	memmove@PLT
	movq	-56(%rbp), %rax
	addq	-88(%rbp), %r15
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	%rax, -72(%rbp)
	jg	.L766
.L712:
	movq	-88(%rbp), %rax
	movq	%rbx, %r14
	leaq	(%r14,%rax), %rbx
	cmpq	%r15, %rbx
	jne	.L767
.L704:
	movq	%r15, %rdx
	movq	%r15, %r12
	subq	%r14, %rdx
	cmpq	%r14, %r15
	jne	.L743
	addq	%rdx, %r13
	xorl	%r14d, %r14d
	movq	%r15, %r12
	.p2align 4,,10
	.p2align 3
.L710:
	movq	-56(%rbp), %rax
	addq	%r14, %r13
	addq	-88(%rbp), %r15
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rax, -72(%rbp)
	jle	.L712
	movq	-112(%rbp), %rbx
	movq	%r12, %r14
.L703:
	cmpq	%rax, %rbx
	cmovle	%rbx, %rax
	leaq	(%r14,%rax,8), %r12
	movq	%r12, %r15
	cmpq	%r14, %r12
	je	.L714
	cmpq	%r12, -56(%rbp)
	je	.L714
	.p2align 4,,10
	.p2align 3
.L718:
	movq	-80(%rbp), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	js	.L768
	movq	(%r14), %rax
	addq	$8, %r14
	addq	$8, %r13
	movq	%rax, -8(%r13)
	cmpq	%r14, %r12
	je	.L714
	cmpq	%r15, -56(%rbp)
	jne	.L718
.L714:
	movq	%r12, %rcx
	subq	%r14, %rcx
	cmpq	%r12, %r14
	je	.L719
	movq	%rcx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rcx, -88(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %rcx
.L719:
	movq	-56(%rbp), %rax
	cmpq	%r15, %rax
	je	.L720
	movq	%rax, %rdx
	leaq	0(%r13,%rcx), %rdi
	movq	%r15, %rsi
	subq	%r15, %rdx
	call	memmove@PLT
.L720:
	salq	$2, %rbx
	cmpq	%rbx, -104(%rbp)
	jl	.L748
	leaq	0(,%rbx,8), %rax
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %r13
	movq	%rbx, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	-72(%rbp), %rax
	movq	%rsi, %r14
	leaq	(%rsi,%rax,8), %r12
	movq	-88(%rbp), %rax
	leaq	(%r14,%rax), %rbx
	cmpq	%r12, %rbx
	je	.L722
	.p2align 4,,10
	.p2align 3
.L771:
	movq	%r12, %r15
	.p2align 4,,10
	.p2align 3
.L726:
	movq	-80(%rbp), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	js	.L769
	movq	(%r14), %rax
	addq	$8, %r14
	addq	$8, %r13
	movq	%rax, -8(%r13)
	cmpq	%r12, %r14
	je	.L724
	cmpq	%r15, %rbx
	jne	.L726
.L724:
	movq	%r12, %rdx
	subq	%r14, %rdx
	cmpq	%r14, %r12
	je	.L727
.L741:
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rdx, -112(%rbp)
	call	memmove@PLT
	movq	-112(%rbp), %rdx
.L727:
	movq	%rbx, %r14
	addq	%rdx, %r13
	subq	%r15, %r14
	cmpq	%r15, %rbx
	je	.L728
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	addq	%r14, %r13
	call	memmove@PLT
	movq	-64(%rbp), %rax
	addq	-88(%rbp), %r12
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	%rax, -96(%rbp)
	jg	.L770
.L730:
	movq	-88(%rbp), %rax
	movq	%rbx, %r14
	leaq	(%r14,%rax), %rbx
	cmpq	%r12, %rbx
	jne	.L771
.L722:
	movq	%r12, %rdx
	movq	%r12, %r15
	subq	%r14, %rdx
	cmpq	%r14, %r12
	jne	.L741
	addq	%rdx, %r13
	xorl	%r14d, %r14d
	movq	%r12, %r15
	.p2align 4,,10
	.p2align 3
.L728:
	movq	-64(%rbp), %rax
	addq	%r14, %r13
	addq	-88(%rbp), %r12
	subq	%r15, %rax
	sarq	$3, %rax
	cmpq	%rax, -96(%rbp)
	jle	.L730
	movq	-96(%rbp), %rbx
	movq	%r15, %r14
.L721:
	movq	-72(%rbp), %rcx
	cmpq	%rax, %rcx
	movq	%rcx, %r12
	cmovg	%rax, %r12
	leaq	(%r14,%r12,8), %r15
	movq	%r15, %r12
	cmpq	%r14, %r15
	je	.L732
	cmpq	%r15, -64(%rbp)
	je	.L732
	.p2align 4,,10
	.p2align 3
.L736:
	movq	-80(%rbp), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	js	.L772
	movq	(%r14), %rax
	addq	$8, %r14
	addq	$8, %r13
	movq	%rax, -8(%r13)
	cmpq	%r14, %r15
	je	.L732
	cmpq	%r12, -64(%rbp)
	jne	.L736
.L732:
	movq	%r15, %rdx
	subq	%r14, %rdx
	cmpq	%r14, %r15
	je	.L737
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rdx, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %rdx
.L737:
	movq	-64(%rbp), %rax
	cmpq	%r12, %rax
	je	.L738
	movq	%rax, %r8
	leaq	0(%r13,%rdx), %rdi
	movq	%r12, %rsi
	subq	%r12, %r8
	movq	%r8, %rdx
	call	memmove@PLT
.L738:
	cmpq	%rbx, -104(%rbp)
	jg	.L740
.L699:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L765:
	.cfi_restore_state
	movq	(%r12), %rax
	addq	$8, %r12
	addq	$8, %r13
	movq	%rax, -8(%r13)
	cmpq	%r12, %rbx
	je	.L706
	cmpq	%r15, %r14
	jne	.L708
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L769:
	movq	(%r15), %rax
	addq	$8, %r15
	addq	$8, %r13
	movq	%rax, -8(%r13)
	cmpq	%r15, %rbx
	je	.L724
	cmpq	%r12, %r14
	jne	.L726
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L768:
	movq	(%r15), %rax
	addq	$8, %r13
	addq	$8, %r15
	movq	%rax, -8(%r13)
	cmpq	%r15, -56(%rbp)
	je	.L714
	cmpq	%r14, %r12
	jne	.L718
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L772:
	movq	(%r12), %rax
	addq	$8, %r13
	addq	$8, %r12
	movq	%rax, -8(%r13)
	cmpq	%r12, -64(%rbp)
	je	.L732
	cmpq	%r14, %r15
	jne	.L736
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L770:
	movq	%rbx, %r14
	movq	-96(%rbp), %rbx
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L766:
	movq	%rbx, %r14
	movq	-112(%rbp), %rbx
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L748:
	movq	-120(%rbp), %r13
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %r14
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L745:
	movq	-128(%rbp), %r13
	movq	-104(%rbp), %rax
	movq	-120(%rbp), %r14
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L700:
	addq	$88, %rsp
	movq	%rcx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	.cfi_endproc
.LFE11599:
	.size	_ZSt24__merge_sort_with_bufferIPPN2v88internal10RegExpTreeES4_N9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_, .-_ZSt24__merge_sort_with_bufferIPPN2v88internal10RegExpTreeES4_N9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_
	.section	.text._ZNSt3_V28__rotateIPPN2v88internal10RegExpTreeEEET_S6_S6_S6_St26random_access_iterator_tag,"axG",@progbits,_ZNSt3_V28__rotateIPPN2v88internal10RegExpTreeEEET_S6_S6_S6_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZNSt3_V28__rotateIPPN2v88internal10RegExpTreeEEET_S6_S6_S6_St26random_access_iterator_tag
	.type	_ZNSt3_V28__rotateIPPN2v88internal10RegExpTreeEEET_S6_S6_S6_St26random_access_iterator_tag, @function
_ZNSt3_V28__rotateIPPN2v88internal10RegExpTreeEEET_S6_S6_S6_St26random_access_iterator_tag:
.LFB11701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	%rsi, %rdi
	je	.L773
	movq	%rdi, %rbx
	cmpq	%rdx, %rsi
	je	.L801
	movq	%rdx, %rax
	movq	%rsi, %rcx
	subq	%rsi, %r12
	subq	%rdi, %rax
	subq	%rdi, %rcx
	addq	%rdi, %r12
	sarq	$3, %rax
	sarq	$3, %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rcx
	je	.L833
	.p2align 4,,10
	.p2align 3
.L781:
	movq	%rax, %rsi
	subq	%rcx, %rsi
	cmpq	%rcx, %rsi
	jle	.L782
.L835:
	cmpq	$1, %rcx
	je	.L834
	leaq	0(,%rcx,8), %rdx
	leaq	(%rbx,%rdx), %rdi
	testq	%rsi, %rsi
	jle	.L785
	addq	$16, %rdx
	testq	%rdx, %rdx
	leaq	16(%rbx), %rdx
	setle	%r8b
	cmpq	%rdx, %rdi
	setnb	%dl
	orb	%dl, %r8b
	je	.L803
	leaq	-1(%rsi), %rdx
	cmpq	$1, %rdx
	jbe	.L803
	movq	%rsi, %r9
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	shrq	%r9
	.p2align 4,,10
	.p2align 3
.L788:
	movdqu	(%rbx,%rdx), %xmm0
	movdqu	(%rdi,%rdx), %xmm1
	addq	$1, %r8
	movups	%xmm1, (%rbx,%rdx)
	movups	%xmm0, (%rdi,%rdx)
	addq	$16, %rdx
	cmpq	%r8, %r9
	jne	.L788
	movq	%rsi, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rdx
	leaq	(%rbx,%rdx), %r8
	addq	%rdx, %rdi
	cmpq	%r9, %rsi
	je	.L790
	movq	(%r8), %rdx
	movq	(%rdi), %r9
	movq	%r9, (%r8)
	movq	%rdx, (%rdi)
.L790:
	leaq	(%rbx,%rsi,8), %rbx
.L785:
	cqto
	idivq	%rcx
	testq	%rdx, %rdx
	je	.L773
	movq	%rcx, %rax
	subq	%rdx, %rcx
	movq	%rax, %rsi
	subq	%rcx, %rsi
	cmpq	%rcx, %rsi
	jg	.L835
.L782:
	leaq	0(,%rax,8), %rdx
	cmpq	$1, %rsi
	je	.L836
	leaq	(%rbx,%rdx), %r11
	leaq	0(,%rsi,8), %rdi
	movq	%r11, %r10
	subq	%rdi, %r10
	testq	%rcx, %rcx
	jle	.L804
	movq	%rdx, %r8
	subq	%rdi, %r8
	leaq	-16(%rdx), %rdi
	leaq	-16(%r8), %r9
	cmpq	%rdi, %r8
	setle	%r8b
	cmpq	%r9, %rdx
	setle	%dl
	orb	%dl, %r8b
	je	.L805
	leaq	-1(%rcx), %rdx
	cmpq	$1, %rdx
	jbe	.L805
	movq	%rcx, %r13
	addq	%rbx, %r9
	xorl	%edx, %edx
	addq	%rdi, %rbx
	shrq	%r13
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L797:
	movdqu	(%r9,%rdx), %xmm0
	movdqu	(%rbx,%rdx), %xmm2
	addq	$1, %r8
	movups	%xmm2, (%r9,%rdx)
	movups	%xmm0, (%rbx,%rdx)
	subq	$16, %rdx
	cmpq	%r8, %r13
	jne	.L797
	movq	%rcx, %r8
	andq	$-2, %r8
	movq	%r8, %rdx
	negq	%rdx
	salq	$3, %rdx
	leaq	(%r10,%rdx), %rdi
	addq	%r11, %rdx
	cmpq	%r8, %rcx
	je	.L799
	movq	-8(%rdi), %r8
	movq	-8(%rdx), %r9
	movq	%r9, -8(%rdi)
	movq	%r8, -8(%rdx)
.L799:
	negq	%rcx
	leaq	(%r10,%rcx,8), %rbx
.L794:
	cqto
	idivq	%rsi
	movq	%rdx, %rcx
	testq	%rdx, %rdx
	je	.L773
	movq	%rsi, %rax
	jmp	.L781
.L833:
	leaq	-8(%rsi), %rdi
	leaq	15(%rbx), %rax
	subq	%rbx, %rdi
	subq	%rsi, %rax
	movq	%rdi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L802
	movabsq	$2305843009213693950, %rax
	testq	%rax, %rcx
	je	.L802
	addq	$1, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L778:
	movdqu	(%rbx,%rax), %xmm0
	movdqu	(%rsi,%rax), %xmm3
	movups	%xmm3, (%rbx,%rax)
	movups	%xmm0, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L778
	movq	%rcx, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rax
	leaq	(%rbx,%rax), %rdx
	addq	%rax, %rsi
	cmpq	%r8, %rcx
	je	.L780
	movq	(%rdx), %rax
	movq	(%rsi), %rcx
	movq	%rcx, (%rdx)
	movq	%rax, (%rsi)
.L780:
	leaq	8(%rbx,%rdi), %r12
.L773:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L803:
	.cfi_restore_state
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L786:
	movq	(%rbx,%rdx,8), %r8
	movq	(%rdi,%rdx,8), %r9
	movq	%r9, (%rbx,%rdx,8)
	movq	%r8, (%rdi,%rdx,8)
	addq	$1, %rdx
	cmpq	%rdx, %rsi
	jne	.L786
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L805:
	movq	%r10, %rdx
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L795:
	movq	-8(%rdx), %r8
	movq	-8(%r11), %r9
	addq	$1, %rdi
	subq	$8, %rdx
	subq	$8, %r11
	movq	%r9, (%rdx)
	movq	%r8, (%r11)
	cmpq	%rdi, %rcx
	jne	.L795
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L804:
	movq	%r10, %rbx
	jmp	.L794
.L801:
	movq	%rdi, %r12
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L834:
	.cfi_restore_state
	leaq	0(,%rax,8), %r13
	leaq	8(%rbx), %rsi
	movq	(%rbx), %r14
	leaq	(%rbx,%r13), %rax
	leaq	-8(%r13), %rdx
	cmpq	%rax, %rsi
	je	.L784
	movq	%rbx, %rdi
	call	memmove@PLT
.L784:
	movq	%r14, -8(%rbx,%r13)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L836:
	.cfi_restore_state
	movq	-8(%rbx,%rdx), %r13
	subq	$8, %rdx
	je	.L793
	leaq	8(%rbx), %rdi
	movq	%rbx, %rsi
	call	memmove@PLT
.L793:
	movq	%r13, (%rbx)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L802:
	.cfi_restore_state
	movq	%rsi, %rdx
	movq	%rbx, %rax
.L776:
	movq	(%rdx), %r8
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$8, %rdx
	movq	%r8, -8(%rax)
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rsi
	jne	.L776
	jmp	.L780
	.cfi_endproc
.LFE11701:
	.size	_ZNSt3_V28__rotateIPPN2v88internal10RegExpTreeEEET_S6_S6_S6_St26random_access_iterator_tag, .-_ZNSt3_V28__rotateIPPN2v88internal10RegExpTreeEEET_S6_S6_S6_St26random_access_iterator_tag
	.section	.text._ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_,"axG",@progbits,_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_,comdat
	.p2align 4
	.weak	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	.type	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_, @function
_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_:
.LFB11597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%r8, -56(%rbp)
	movq	%r9, -64(%rbp)
	testq	%r8, %r8
	je	.L837
	movq	%rcx, %rax
	testq	%rcx, %rcx
	je	.L837
	addq	%r8, %rax
	movq	%rcx, %rbx
	cmpq	$2, %rax
	je	.L872
	cmpq	%rbx, -56(%rbp)
	jge	.L844
.L874:
	movq	%rbx, %rax
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %r15
	shrq	$63, %rax
	addq	%rbx, %rax
	sarq	%rax
	movq	%rax, -88(%rbp)
	movq	%rax, %r12
	leaq	(%rsi,%rax,8), %r14
	movq	-112(%rbp), %rax
	subq	%r15, %rax
	movq	%rax, %rdx
	sarq	$3, %rdx
	testq	%rax, %rax
	jle	.L859
	movq	%r12, -96(%rbp)
	movq	%r14, %r12
	movq	%r15, %r14
	movq	%rbx, -104(%rbp)
	movq	%rdx, %rbx
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L848:
	testq	%r13, %r13
	je	.L869
.L849:
	movq	%r13, %rbx
.L846:
	movq	%rbx, %r13
	movq	-64(%rbp), %rax
	movq	%r12, %rsi
	sarq	%r13
	leaq	(%r14,%r13,8), %r15
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	jns	.L848
	subq	%r13, %rbx
	leaq	8(%r15), %r14
	leaq	-1(%rbx), %r13
	testq	%r13, %r13
	jg	.L849
.L869:
	movq	%r14, %r15
	movq	-104(%rbp), %rbx
	movq	%r12, %r14
	movq	-96(%rbp), %r12
	movq	%r15, %r13
	subq	-80(%rbp), %r13
	sarq	$3, %r13
	subq	%r13, -56(%rbp)
.L845:
	subq	-88(%rbp), %rbx
.L850:
	movq	-80(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZNSt3_V28__rotateIPPN2v88internal10RegExpTreeEEET_S6_S6_S6_St26random_access_iterator_tag
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %rdi
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%rax, -80(%rbp)
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	testq	%rbx, %rbx
	je	.L837
	cmpq	$0, -56(%rbp)
	movq	-80(%rbp), %rax
	je	.L837
	movq	-56(%rbp), %rsi
	leaq	(%rsi,%rbx), %rdx
	cmpq	$2, %rdx
	je	.L873
	movq	%r15, -80(%rbp)
	movq	%rax, -72(%rbp)
	cmpq	%rbx, -56(%rbp)
	jl	.L874
.L844:
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %r14
	movq	%rcx, %rax
	shrq	$63, %rax
	addq	%rcx, %rax
	movq	-80(%rbp), %rcx
	sarq	%rax
	movq	%rax, -88(%rbp)
	movq	%rax, %r13
	leaq	(%rcx,%rax,8), %r15
	movq	%rcx, %rax
	subq	%r14, %rax
	movq	%rax, %rdx
	sarq	$3, %rdx
	testq	%rax, %rax
	jle	.L860
	movq	%r13, -96(%rbp)
	movq	%r14, %r13
	movq	%r15, %r14
	movq	%rbx, -104(%rbp)
	movq	%rdx, %rbx
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L875:
	subq	%r12, %rbx
	leaq	8(%r15), %r13
	leaq	-1(%rbx), %r12
	testq	%r12, %r12
	jle	.L870
.L855:
	movq	%r12, %rbx
.L852:
	movq	%rbx, %r12
	movq	-64(%rbp), %rax
	movq	%r14, %rdi
	sarq	%r12
	leaq	0(%r13,%r12,8), %r15
	movq	%r15, %rsi
	call	*%rax
	testl	%eax, %eax
	jns	.L875
	testq	%r12, %r12
	jne	.L855
.L870:
	movq	%r14, %r15
	movq	%r13, %r14
	movq	-104(%rbp), %rbx
	movq	-96(%rbp), %r13
	movq	%r14, %r12
	subq	-72(%rbp), %r12
	sarq	$3, %r12
	subq	%r12, %rbx
.L851:
	movq	-88(%rbp), %rcx
	subq	%rcx, -56(%rbp)
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L873:
	movq	%rax, %r13
	movq	%r15, %r12
.L841:
	movq	-64(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jns	.L837
	movq	0(%r13), %rax
	movq	(%r12), %rdx
	movq	%rdx, 0(%r13)
	movq	%rax, (%r12)
.L837:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L859:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L860:
	xorl	%r12d, %r12d
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L872:
	movq	-80(%rbp), %r12
	movq	-72(%rbp), %r13
	jmp	.L841
	.cfi_endproc
.LFE11597:
	.size	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_, .-_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	.section	.text._ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_,"axG",@progbits,_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_,comdat
	.p2align 4
	.weak	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	.type	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_, @function
_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_:
.LFB11496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	subq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	$112, %rcx
	jle	.L879
	sarq	$4, %rcx
	leaq	0(,%rcx,8), %rbx
	leaq	(%rdi,%rbx), %r15
	sarq	$3, %rbx
	movq	%r15, %rsi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	%r13, %r8
	addq	$8, %rsp
	movq	%rbx, %rcx
	subq	%r15, %r8
	popq	%rbx
	movq	%r14, %r9
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	sarq	$3, %r8
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	.p2align 4,,10
	.p2align 3
.L879:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	.cfi_endproc
.LFE11496:
	.size	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_, .-_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	.section	.text._ZSt16__merge_adaptiveIPPN2v88internal10RegExpTreeElS4_N9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_SJ_T2_,"axG",@progbits,_ZSt16__merge_adaptiveIPPN2v88internal10RegExpTreeElS4_N9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_SJ_T2_,comdat
	.p2align 4
	.weak	_ZSt16__merge_adaptiveIPPN2v88internal10RegExpTreeElS4_N9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_SJ_T2_
	.type	_ZSt16__merge_adaptiveIPPN2v88internal10RegExpTreeElS4_N9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_SJ_T2_, @function
_ZSt16__merge_adaptiveIPPN2v88internal10RegExpTreeElS4_N9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_SJ_T2_:
.LFB11600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -80(%rbp)
	movq	24(%rbp), %rax
	movq	16(%rbp), %rdi
	movq	%rsi, -72(%rbp)
	movq	%rax, -56(%rbp)
	movq	%r8, %rax
	cmpq	%r8, %rdi
	movq	%rdx, -88(%rbp)
	cmovle	%rdi, %rax
	movq	%rdi, -64(%rbp)
	cmpq	%rax, %rcx
	jle	.L921
	cmpq	%r8, %rdi
	jge	.L957
	movq	%r8, %rbx
	movq	%rcx, %r12
	movq	%r9, -96(%rbp)
	cmpq	%r12, %rbx
	jge	.L899
.L960:
	movq	%r12, %r14
	movq	-88(%rbp), %rax
	movq	-72(%rbp), %r13
	shrq	$63, %r14
	movq	-80(%rbp), %rcx
	addq	%r12, %r14
	subq	%r13, %rax
	sarq	%r14
	movq	%r14, -112(%rbp)
	leaq	(%rcx,%r14,8), %r15
	movq	%r14, -104(%rbp)
	movq	%rax, %r14
	sarq	$3, %r14
	testq	%rax, %rax
	jle	.L924
	movq	%r12, -120(%rbp)
	movq	%r15, %r12
	movq	%rbx, -128(%rbp)
	movq	%r14, %rbx
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L903:
	testq	%r14, %r14
	je	.L951
.L904:
	movq	%r14, %rbx
.L901:
	movq	%rbx, %r14
	movq	-56(%rbp), %rax
	movq	%r12, %rsi
	sarq	%r14
	leaq	0(%r13,%r14,8), %r15
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	jns	.L903
	subq	%r14, %rbx
	leaq	8(%r15), %r13
	leaq	-1(%rbx), %r14
	testq	%r14, %r14
	jg	.L904
.L951:
	movq	-128(%rbp), %rbx
	movq	%r13, %r8
	subq	-72(%rbp), %r8
	movq	%r12, %r15
	sarq	$3, %r8
	movq	-120(%rbp), %r12
	subq	%r8, %rbx
.L900:
	subq	-112(%rbp), %r12
	cmpq	%r12, %r8
	jge	.L911
.L962:
	cmpq	-64(%rbp), %r8
	jg	.L911
	movq	%r15, %r14
	testq	%r8, %r8
	je	.L912
	movq	-72(%rbp), %rsi
	movq	%r13, %r14
	subq	%rsi, %r14
	cmpq	%rsi, %r13
	je	.L913
	movq	-96(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r8, -112(%rbp)
	call	memmove@PLT
	movq	-112(%rbp), %r8
.L913:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L914
	subq	%r15, %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r8, -72(%rbp)
	movq	%rax, %rdx
	subq	%rax, %rdi
	call	memmove@PLT
	movq	-72(%rbp), %r8
.L914:
	testq	%r14, %r14
	je	.L915
	movq	-96(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%r8, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %r8
.L915:
	addq	%r15, %r14
.L912:
	pushq	-56(%rbp)
	movq	-96(%rbp), %r9
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	-104(%rbp), %rcx
	pushq	-64(%rbp)
	movq	-80(%rbp), %rdi
	call	_ZSt16__merge_adaptiveIPPN2v88internal10RegExpTreeElS4_N9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_SJ_T2_
	popq	%rax
	movq	-64(%rbp), %rax
	popq	%rdx
	cmpq	%rbx, %rax
	cmovg	%rbx, %rax
	cmpq	%r12, %rax
	jge	.L958
	cmpq	%rbx, -64(%rbp)
	jge	.L959
	movq	%r13, -72(%rbp)
	movq	%r14, -80(%rbp)
	cmpq	%r12, %rbx
	jl	.L960
.L899:
	movq	%rbx, %r14
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %r15
	shrq	$63, %r14
	addq	%rbx, %r14
	sarq	%r14
	leaq	(%rax,%r14,8), %r13
	subq	%r15, %rax
	movq	%r14, -112(%rbp)
	movq	%r14, %r8
	movq	%rax, %r14
	sarq	$3, %r14
	testq	%rax, %rax
	jle	.L925
	movq	%r12, -120(%rbp)
	movq	%r14, %r12
	movq	%rbx, -128(%rbp)
	movq	%r15, %rbx
	movq	%r8, -104(%rbp)
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L961:
	movq	%r12, %rdx
	leaq	8(%r15), %rbx
	subq	%r14, %rdx
	leaq	-1(%rdx), %r14
	testq	%r14, %r14
	jle	.L952
.L910:
	movq	%r14, %r12
.L907:
	movq	%r12, %r14
	movq	-56(%rbp), %rax
	movq	%r13, %rdi
	sarq	%r14
	leaq	(%rbx,%r14,8), %r15
	movq	%r15, %rsi
	call	*%rax
	testl	%eax, %eax
	jns	.L961
	testq	%r14, %r14
	jne	.L910
.L952:
	movq	%rbx, %r15
	movq	-104(%rbp), %r8
	movq	-120(%rbp), %r12
	movq	%r15, %rax
	subq	-80(%rbp), %rax
	movq	-128(%rbp), %rbx
	sarq	$3, %rax
	movq	%rax, -104(%rbp)
	subq	%rax, %r12
.L906:
	subq	-112(%rbp), %rbx
	cmpq	%r12, %r8
	jl	.L962
.L911:
	cmpq	%r12, -64(%rbp)
	jl	.L916
	movq	%r13, %r14
	testq	%r12, %r12
	je	.L912
	movq	-72(%rbp), %rax
	movq	%rax, %r9
	subq	%r15, %r9
	cmpq	%rax, %r15
	je	.L917
	movq	-96(%rbp), %rdi
	movq	%r9, %rdx
	movq	%r15, %rsi
	movq	%r8, -120(%rbp)
	movq	%r9, -112(%rbp)
	call	memmove@PLT
	movq	-120(%rbp), %r8
	movq	-112(%rbp), %r9
.L917:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r13
	je	.L918
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r9, -112(%rbp)
	subq	%rax, %rdx
	movq	%r8, -72(%rbp)
	call	memmove@PLT
	movq	-112(%rbp), %r9
	movq	-72(%rbp), %r8
.L918:
	movq	%r13, %r14
	subq	%r9, %r14
	testq	%r9, %r9
	je	.L912
	movq	-96(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %r8
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L916:
	movq	-72(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%r8, -112(%rbp)
	call	_ZNSt3_V28__rotateIPPN2v88internal10RegExpTreeEEET_S6_S6_S6_St26random_access_iterator_tag
	movq	-112(%rbp), %r8
	movq	%rax, %r14
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L958:
	movq	-96(%rbp), %r12
.L881:
	movq	%r13, %rbx
	subq	%r14, %rbx
	cmpq	%r14, %r13
	je	.L884
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	memmove@PLT
.L884:
	addq	%r12, %rbx
	cmpq	%r12, %rbx
	jne	.L954
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L886:
	movq	(%r12), %rax
	addq	$8, %r12
	addq	$8, %r14
	movq	%rax, -8(%r14)
	cmpq	%r12, %rbx
	je	.L880
.L954:
	cmpq	%r13, -88(%rbp)
	je	.L885
.L889:
	movq	-56(%rbp), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*%rax
	testl	%eax, %eax
	jns	.L886
	movq	0(%r13), %rax
	addq	$8, %r14
	addq	$8, %r13
	movq	%rax, -8(%r14)
	cmpq	%r13, -88(%rbp)
	je	.L885
	cmpq	%r12, %rbx
	jne	.L889
.L880:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L959:
	.cfi_restore_state
	movq	-96(%rbp), %r12
.L882:
	movq	-88(%rbp), %rax
	movq	%rax, %r15
	subq	%r13, %r15
	cmpq	%r13, %rax
	je	.L892
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	memmove@PLT
.L892:
	leaq	(%r12,%r15), %rbx
	cmpq	%r13, %r14
	je	.L963
	cmpq	%r12, %rbx
	je	.L880
	movq	-88(%rbp), %r15
	subq	$8, %r13
	subq	$8, %rbx
	.p2align 4,,10
	.p2align 3
.L895:
	movq	-56(%rbp), %rax
	movq	%r13, %rsi
	movq	%rbx, %rdi
	subq	$8, %r15
	call	*%rax
	testl	%eax, %eax
	js	.L964
	movq	(%rbx), %rax
	movq	%rax, (%r15)
	cmpq	%rbx, %r12
	je	.L880
	subq	$8, %rbx
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L964:
	movq	0(%r13), %rax
	movq	%rax, (%r15)
	cmpq	%r14, %r13
	je	.L965
	subq	$8, %r13
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L885:
	cmpq	%r12, %rbx
	je	.L880
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	subq	%r12, %rdx
.L955:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L924:
	.cfi_restore_state
	xorl	%r8d, %r8d
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L925:
	movq	$0, -104(%rbp)
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L965:
	leaq	8(%rbx), %rdx
	cmpq	%rdx, %r12
	je	.L880
	subq	%r12, %rdx
	movq	%r15, %rdi
	subq	%rdx, %rdi
.L956:
	movq	%r12, %rsi
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L921:
	movq	-72(%rbp), %r13
	movq	-80(%rbp), %r14
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L957:
	movq	-72(%rbp), %r13
	movq	-80(%rbp), %r14
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L963:
	cmpq	%r12, %rbx
	je	.L880
	movq	-88(%rbp), %rdi
	movq	%r15, %rdx
	subq	%r15, %rdi
	jmp	.L956
	.cfi_endproc
.LFE11600:
	.size	_ZSt16__merge_adaptiveIPPN2v88internal10RegExpTreeElS4_N9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_SJ_T2_, .-_ZSt16__merge_adaptiveIPPN2v88internal10RegExpTreeElS4_N9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_SJ_T2_
	.section	.text._ZSt22__stable_sort_adaptiveIPPN2v88internal10RegExpTreeES4_lN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_T2_,"axG",@progbits,_ZSt22__stable_sort_adaptiveIPPN2v88internal10RegExpTreeES4_lN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt22__stable_sort_adaptiveIPPN2v88internal10RegExpTreeES4_lN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_T2_
	.type	_ZSt22__stable_sort_adaptiveIPPN2v88internal10RegExpTreeES4_lN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_T2_, @function
_ZSt22__stable_sort_adaptiveIPPN2v88internal10RegExpTreeES4_lN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_T2_:
.LFB11498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	1(%rax), %rcx
	movq	%rcx, %rax
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$63, %rax
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	addq	%rcx, %rax
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	sarq	%rax
	leaq	0(,%rax,8), %r9
	subq	$24, %rsp
	leaq	(%rdi,%r9), %r10
	movq	%r9, -64(%rbp)
	cmpq	%r14, %rax
	jle	.L967
	movq	%r10, %rsi
	movq	%r14, %rcx
	movq	%r10, -56(%rbp)
	call	_ZSt22__stable_sort_adaptiveIPPN2v88internal10RegExpTreeES4_lN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_T2_
	movq	-56(%rbp), %r10
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	_ZSt22__stable_sort_adaptiveIPPN2v88internal10RegExpTreeES4_lN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_T2_
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r9
.L968:
	pushq	%r15
	movq	%r13, %r8
	movq	%r9, %rcx
	movq	%r13, %rdx
	pushq	%r14
	subq	%r10, %r8
	movq	%rbx, %r9
	movq	%r12, %rdi
	sarq	$3, %rcx
	sarq	$3, %r8
	movq	%r10, %rsi
	call	_ZSt16__merge_adaptiveIPPN2v88internal10RegExpTreeElS4_N9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_SJ_T2_
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L967:
	.cfi_restore_state
	movq	%r8, %rcx
	movq	%r10, %rsi
	movq	%r10, -56(%rbp)
	call	_ZSt24__merge_sort_with_bufferIPPN2v88internal10RegExpTreeES4_N9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_
	movq	-56(%rbp), %r10
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	_ZSt24__merge_sort_with_bufferIPPN2v88internal10RegExpTreeES4_N9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r10
	jmp	.L968
	.cfi_endproc
.LFE11498:
	.size	_ZSt22__stable_sort_adaptiveIPPN2v88internal10RegExpTreeES4_lN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_T2_, .-_ZSt22__stable_sort_adaptiveIPPN2v88internal10RegExpTreeES4_lN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_T2_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10RegExpAtom6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10RegExpAtom6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, @function
_GLOBAL__sub_I__ZN2v88internal10RegExpAtom6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE:
.LFB11754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE11754:
	.size	_GLOBAL__sub_I__ZN2v88internal10RegExpAtom6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, .-_GLOBAL__sub_I__ZN2v88internal10RegExpAtom6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10RegExpAtom6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal17BuildAsciiAToZSetEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17BuildAsciiAToZSetEv
	.type	_ZN2v88internal17BuildAsciiAToZSetEv, @function
_ZN2v88internal17BuildAsciiAToZSetEv:
.LFB10035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$122, %edx
	movl	$97, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
	movq	%r12, %rdi
	movl	$90, %edx
	movl	$65, %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10035:
	.size	_ZN2v88internal17BuildAsciiAToZSetEv, .-_ZN2v88internal17BuildAsciiAToZSetEv
	.section	.text._ZN2v84base16LazyInstanceImplINS_8internal12AsciiAToZSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv,"axG",@progbits,_ZN2v84base16LazyInstanceImplINS_8internal12AsciiAToZSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv,comdat
	.p2align 4
	.weak	_ZN2v84base16LazyInstanceImplINS_8internal12AsciiAToZSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv
	.type	_ZN2v84base16LazyInstanceImplINS_8internal12AsciiAToZSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS_8internal12AsciiAToZSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv:
.LFB11382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$122, %edx
	movl	$97, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
	movq	%r12, %rdi
	movl	$90, %edx
	movl	$65, %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	.cfi_endproc
.LFE11382:
	.size	_ZN2v84base16LazyInstanceImplINS_8internal12AsciiAToZSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS_8internal12AsciiAToZSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv
	.section	.text._ZN2v88internal12_GLOBAL__N_125AddUnicodeCaseEquivalentsEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_125AddUnicodeCaseEquivalentsEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneE, @function
_ZN2v88internal12_GLOBAL__N_125AddUnicodeCaseEquivalentsEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneE:
.LFB9988:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 12(%rdi)
	jne	.L977
	movq	(%rdi), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	je	.L993
.L977:
	leaq	-256(%rbp), %r13
	xorl	%ebx, %ebx
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movl	12(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L981
	.p2align 4,,10
	.p2align 3
.L979:
	movq	(%r12), %rax
	movq	%r13, %rdi
	leaq	(%rax,%rbx,8), %rax
	addq	$1, %rbx
	movl	4(%rax), %edx
	movl	(%rax), %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	cmpl	%ebx, 12(%r12)
	jg	.L979
.L981:
	movq	$0, (%r12)
	movl	$2, %esi
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	movq	$0, 8(%r12)
	call	_ZN6icu_6710UnicodeSet9closeOverEi@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet16removeAllStringsEv@PLT
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L994:
	leal	1(%rax), %edx
	movl	%edx, 12(%r12)
	movq	(%r12), %rdx
	leaq	(%rdx,%rax,8), %rax
	movl	%ecx, (%rax)
	movl	%r14d, 4(%rax)
.L984:
	addl	$1, %ebx
.L980:
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	cmpl	%ebx, %eax
	jle	.L982
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	%ebx, %esi
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movl	8(%r12), %edx
	movl	%eax, %ecx
	movslq	12(%r12), %rax
	cmpl	%edx, %eax
	jl	.L994
	movq	16(%r15), %rdi
	movq	24(%r15), %rax
	leal	1(%rdx,%rdx), %r8d
	movslq	%r8d, %rsi
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L995
	addq	%rdi, %rsi
	movq	%rsi, 16(%r15)
.L986:
	movslq	12(%r12), %rdx
	movq	%rdx, %rsi
	salq	$3, %rdx
	testl	%esi, %esi
	jle	.L987
	movq	(%r12), %rsi
	movl	%r8d, -264(%rbp)
	movl	%ecx, -260(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movl	-264(%rbp), %r8d
	movl	-260(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rsi
	salq	$3, %rdx
.L987:
	movq	%rdi, (%r12)
	addl	$1, %esi
	addq	%rdx, %rdi
	movl	%r8d, 8(%r12)
	movl	%esi, 12(%r12)
	movl	%ecx, (%rdi)
	movl	%r14d, 4(%rdi)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L982:
	movq	%r12, %rdi
	call	_ZN2v88internal14CharacterRange12CanonicalizeEPNS0_8ZoneListIS1_EE
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
.L976:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L996
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L993:
	.cfi_restore_state
	cmpl	$1114110, 4(%rax)
	jg	.L976
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L995:
	movq	%r15, %rdi
	movl	%r8d, -264(%rbp)
	movl	%ecx, -260(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-260(%rbp), %ecx
	movl	-264(%rbp), %r8d
	movq	%rax, %rdi
	jmp	.L986
.L996:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9988:
	.size	_ZN2v88internal12_GLOBAL__N_125AddUnicodeCaseEquivalentsEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneE, .-_ZN2v88internal12_GLOBAL__N_125AddUnicodeCaseEquivalentsEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneE
	.section	.text._ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEbPNS0_4ZoneE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEbPNS0_4ZoneE.part.0, @function
_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEbPNS0_4ZoneE.part.0:
.LFB11839:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	16(%rdx), %r12
	movq	24(%rdx), %rdx
	movq	%rdx, %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L1039
	leaq	16(%r12), %rax
	movq	%rax, 16(%r13)
.L999:
	subq	%rax, %rdx
	movq	%r12, %r14
	cmpq	$15, %rdx
	jbe	.L1040
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
.L1001:
	movabsq	$4294967298, %rcx
	movq	%rax, (%r12)
	movabsq	$244813135920, %rdi
	movq	%rcx, 8(%r12)
	movq	%rdi, (%rax)
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jge	.L1041
	leal	1(%rax), %ecx
	movq	(%r12), %rdx
	movl	%ecx, 12(%r12)
	movabsq	$386547056705, %rcx
	movq	%rcx, (%rdx,%rax,8)
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jge	.L1042
.L1009:
	movq	(%r12), %rdx
	leal	1(%rax), %ecx
	movabsq	$408021893215, %rdi
	movl	%ecx, 12(%r12)
	movq	%rdi, (%rdx,%rax,8)
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jge	.L1016
.L1047:
	leal	1(%rax), %ecx
	movq	(%r12), %rdx
	movl	%ecx, 12(%r12)
	movabsq	$523986010209, %rcx
	movq	%rcx, (%rdx,%rax,8)
.L1017:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_125AddUnicodeCaseEquivalentsEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneE
	cmpb	$87, %r15b
	je	.L1043
.L1021:
	movl	12(%r14), %r12d
	movq	(%r14), %r15
	movl	12(%rbx), %r14d
	addl	%r12d, %r14d
	cmpl	8(%rbx), %r14d
	jg	.L1044
.L1026:
	leal	-1(%r12), %ecx
	xorl	%eax, %eax
	testl	%r12d, %r12d
	jle	.L1032
	.p2align 4,,10
	.p2align 3
.L1031:
	movl	12(%rbx), %edx
	movq	(%r15,%rax,8), %rdi
	movq	(%rbx), %rsi
	addl	%eax, %edx
	movslq	%edx, %rdx
	movq	%rdi, (%rsi,%rdx,8)
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	%rdx, %rcx
	jne	.L1031
.L1032:
	movl	%r14d, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1041:
	.cfi_restore_state
	movq	16(%r13), %rdi
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %ecx
	movslq	%ecx, %rsi
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1045
	addq	%rdi, %rsi
	movq	%rsi, 16(%r13)
.L1008:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L1005
.L1007:
	addl	$1, %eax
	movl	%ecx, 8(%r12)
	movl	%eax, 12(%r12)
	movabsq	$386547056705, %rax
	movq	%rdi, (%r12)
	movq	%rax, (%rdi,%rdx)
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jl	.L1009
.L1042:
	movq	16(%r13), %rdi
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %ecx
	movslq	%ecx, %rsi
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1046
	addq	%rdi, %rsi
	movq	%rsi, 16(%r13)
.L1015:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L1012
.L1014:
	addl	$1, %eax
	movl	%ecx, 8(%r12)
	movl	%eax, 12(%r12)
	movabsq	$408021893215, %rax
	movq	%rdi, (%r12)
	movq	%rax, (%rdi,%rdx)
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jl	.L1047
.L1016:
	movq	16(%r13), %rdi
	movq	24(%r13), %rax
	leal	1(%rdx,%rdx), %ecx
	movslq	%ecx, %rsi
	salq	$3, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1048
	addq	%rdi, %rsi
	movq	%rsi, 16(%r13)
.L1019:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$3, %rdx
	testl	%eax, %eax
	jg	.L1049
.L1020:
	addl	$1, %eax
	movq	%rdi, (%r12)
	movl	%eax, 12(%r12)
	movabsq	$523986010209, %rax
	movl	%ecx, 8(%r12)
	movq	%rax, (%rdi,%rdx)
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	16(%r13), %rcx
	movq	24(%r13), %rax
	movslq	%r14d, %rsi
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1050
	addq	%rcx, %rsi
	movq	%rsi, 16(%r13)
.L1028:
	movslq	12(%rbx), %rdx
	testl	%edx, %edx
	jg	.L1051
	movq	%rcx, (%rbx)
	movl	%r14d, 8(%rbx)
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1043:
	movq	24(%r13), %rdx
	movq	16(%r13), %r14
	movq	%rdx, %rax
	subq	%r14, %rax
	cmpq	$15, %rax
	jbe	.L1052
	leaq	16(%r14), %rax
	movq	%rax, 16(%r13)
.L1023:
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L1053
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
.L1025:
	movq	%rax, (%r14)
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	$2, 8(%r14)
	call	_ZN2v88internal14CharacterRange6NegateEPNS0_8ZoneListIS1_EES4_PNS0_4ZoneE
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1012:
	movq	(%r12), %rsi
	movl	%ecx, -52(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1049:
	movq	(%r12), %rsi
	movl	%ecx, -52(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1051:
	movq	(%rbx), %rsi
	movq	%rcx, %rdi
	salq	$3, %rdx
	call	memcpy@PLT
	movl	%r14d, 8(%rbx)
	movq	%rax, %rcx
	movq	%rcx, (%rbx)
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1005:
	movq	(%r12), %rsi
	movl	%ecx, -52(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rdx, %rax
	salq	$3, %rdx
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1039:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r13), %rdx
	movq	%rax, %r12
	movq	16(%r13), %rax
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1040:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1052:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r13), %rdx
	movq	%rax, %r14
	movq	16(%r13), %rax
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1050:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1048:
	movq	%r13, %rdi
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1046:
	movq	%r13, %rdi
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	%r13, %rdi
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1053:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1025
	.cfi_endproc
.LFE11839:
	.size	_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEbPNS0_4ZoneE.part.0, .-_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEbPNS0_4ZoneE.part.0
	.section	.text._ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEbPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEbPNS0_4ZoneE
	.type	_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEbPNS0_4ZoneE, @function
_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEbPNS0_4ZoneE:
.LFB10026:
	.cfi_startproc
	endbr64
	movl	%edx, %r8d
	movsbl	%dil, %r9d
	movq	%rcx, %rdx
	testb	%r8b, %r8b
	je	.L1055
	andl	$-33, %edi
	cmpb	$87, %dil
	je	.L1056
.L1055:
	movl	%r9d, %edi
	jmp	_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEPNS0_4ZoneE
	.p2align 4,,10
	.p2align 3
.L1056:
	movl	%r9d, %edi
	jmp	_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEbPNS0_4ZoneE.part.0
	.cfi_endproc
.LFE10026:
	.size	_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEbPNS0_4ZoneE, .-_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEbPNS0_4ZoneE
	.section	.text._ZN2v88internal12_GLOBAL__N_129BoundaryAssertionAsLookaroundEPNS0_14RegExpCompilerEPNS0_10RegExpNodeENS0_15RegExpAssertion13AssertionTypeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_129BoundaryAssertionAsLookaroundEPNS0_14RegExpCompilerEPNS0_10RegExpNodeENS0_15RegExpAssertion13AssertionTypeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE, @function
_ZN2v88internal12_GLOBAL__N_129BoundaryAssertionAsLookaroundEPNS0_14RegExpCompilerEPNS0_10RegExpNodeENS0_15RegExpAssertion13AssertionTypeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE:
.LFB10006:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	1096(%rdi), %rbx
	movq	%rsi, -176(%rbp)
	movl	%ecx, -156(%rbp)
	movq	24(%rbx), %rdx
	movq	16(%rbx), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	subq	%r13, %rax
	cmpq	$15, %rax
	jbe	.L1098
	leaq	16(%r13), %rax
	movq	%rax, 16(%rbx)
.L1059:
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L1099
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
.L1061:
	movq	%rax, 0(%r13)
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movl	$119, %edi
	movq	$2, 8(%r13)
	call	_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEbPNS0_4ZoneE.part.0
	movl	12(%r12), %eax
	movl	%eax, -148(%rbp)
	cmpl	$-1, %eax
	je	.L1100
	movl	16(%r12), %eax
	movl	%eax, -160(%rbp)
	cmpl	$-1, %eax
	je	.L1101
.L1065:
	movq	16(%rbx), %r12
	movq	24(%rbx), %rax
	subq	%r12, %rax
	cmpq	$71, %rax
	jbe	.L1102
	leaq	72(%r12), %rax
	movq	%rax, 16(%rbx)
.L1069:
	leaq	16+_ZTVN2v88internal10ChoiceNodeE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rbx, 48(%r12)
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movups	%xmm0, 32(%r12)
	movq	24(%rbx), %rdx
	movq	16(%rbx), %r15
	movq	%rdx, %rax
	subq	%r15, %rax
	cmpq	$15, %rax
	jbe	.L1103
	leaq	16(%r15), %rax
	movq	%rax, 16(%rbx)
.L1071:
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1104
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rbx)
.L1073:
	movq	%rax, (%r15)
	xorl	%eax, %eax
	cmpl	$4, %r14d
	movq	$2, 8(%r15)
	movw	%ax, 64(%r12)
	leaq	-144(%rbp), %rax
	movq	%r15, 56(%r12)
	sete	-149(%rbp)
	xorl	%r15d, %r15d
	movq	%rax, -184(%rbp)
	movq	%rbx, -168(%rbp)
	movl	-148(%rbp), %ebx
	movl	%r15d, -148(%rbp)
	movl	-160(%rbp), %r15d
.L1087:
	movl	-148(%rbp), %esi
	movzbl	-149(%rbp), %eax
	movl	%ebx, -104(%rbp)
	movq	-176(%rbp), %r8
	movl	%r15d, -100(%rbp)
	movl	%esi, %r14d
	xorl	$1, %r14d
	movq	%r8, -112(%rbp)
	xorl	%r14d, %eax
	movb	%r14b, -128(%rbp)
	movb	%al, -160(%rbp)
	cmpl	$1, %esi
	je	.L1074
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r15d, %esi
	movl	%ebx, %edi
	call	_ZN2v88internal10ActionNode23PositiveSubmatchSuccessEiiiiPNS0_10RegExpNodeE@PLT
	movq	%rax, -120(%rbp)
	movq	%rax, %rcx
.L1075:
	movl	-156(%rbp), %r8d
	movq	-168(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8TextNode24CreateForCharacterRangesEPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEEbPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE@PLT
	cmpb	$0, -128(%rbp)
	movq	%rax, %rdx
	je	.L1078
	movl	-100(%rbp), %esi
	movl	-104(%rbp), %edi
	call	_ZN2v88internal10ActionNode13BeginSubmatchEiiPNS0_10RegExpNodeE@PLT
	movq	%rax, %r8
.L1079:
	movzbl	-160(%rbp), %eax
	movq	%r8, -80(%rbp)
	movl	%ebx, -72(%rbp)
	movb	%al, -96(%rbp)
	movl	%r15d, -68(%rbp)
	cmpb	-149(%rbp), %r14b
	je	.L1080
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r15d, %esi
	movl	%ebx, %edi
	call	_ZN2v88internal10ActionNode23PositiveSubmatchSuccessEiiiiPNS0_10RegExpNodeE@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rcx
.L1081:
	movl	-156(%rbp), %r8d
	movq	-168(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8TextNode24CreateForCharacterRangesEPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEEbPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE@PLT
	cmpb	$0, -96(%rbp)
	movq	%rax, %rdx
	je	.L1084
	movl	-68(%rbp), %esi
	movl	-72(%rbp), %edi
	call	_ZN2v88internal10ActionNode13BeginSubmatchEiiPNS0_10RegExpNodeE@PLT
.L1097:
	movq	48(%r12), %rdx
	movq	56(%r12), %rdi
	movq	%rax, -144(%rbp)
	movq	-184(%rbp), %rsi
	movq	$0, -136(%rbp)
	call	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	cmpl	$1, -148(%rbp)
	je	.L1057
	movl	$1, -148(%rbp)
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1074:
	movq	48(%r8), %rdx
	movq	16(%rdx), %rcx
	movq	24(%rdx), %rax
	subq	%rcx, %rax
	cmpq	$79, %rax
	jbe	.L1105
	leaq	80(%rcx), %rax
	movq	%rax, 16(%rdx)
.L1077:
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal23NegativeSubmatchSuccessE(%rip), %rax
	movq	%rdx, 48(%rcx)
	movq	$0, 8(%rcx)
	movq	$0, 16(%rcx)
	movq	$0, 24(%rcx)
	movl	$2, 56(%rcx)
	movq	%rax, (%rcx)
	movl	%ebx, 60(%rcx)
	movl	%r15d, 64(%rcx)
	movq	$0, 68(%rcx)
	movups	%xmm0, 32(%rcx)
	movq	%rcx, -120(%rbp)
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1084:
	leaq	-96(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal16RegExpLookaround7Builder8ForMatchEPNS0_10RegExpNodeE.part.0
	jmp	.L1097
	.p2align 4,,10
	.p2align 3
.L1080:
	movq	48(%r8), %r14
	movq	16(%r14), %rcx
	movq	24(%r14), %rax
	subq	%rcx, %rax
	cmpq	$79, %rax
	jbe	.L1106
	leaq	80(%rcx), %rax
	movq	%rax, 16(%r14)
.L1083:
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal23NegativeSubmatchSuccessE(%rip), %rax
	movq	%r14, 48(%rcx)
	movq	$0, 8(%rcx)
	movq	$0, 16(%rcx)
	movq	$0, 24(%rcx)
	movl	$2, 56(%rcx)
	movq	%rax, (%rcx)
	movl	%ebx, 60(%rcx)
	movl	%r15d, 64(%rcx)
	movq	$0, 68(%rcx)
	movups	%xmm0, 32(%rcx)
	movq	%rcx, -88(%rbp)
	jmp	.L1081
	.p2align 4,,10
	.p2align 3
.L1078:
	leaq	-128(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal16RegExpLookaround7Builder8ForMatchEPNS0_10RegExpNodeE.part.0
	movq	%rax, %r8
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1057:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1107
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1100:
	.cfi_restore_state
	movl	8(%r12), %eax
	movl	%eax, -148(%rbp)
	cmpl	$65534, %eax
	jg	.L1108
	addl	$1, %eax
	movl	%eax, 8(%r12)
.L1064:
	movl	-148(%rbp), %eax
	movl	%eax, 12(%r12)
	movl	16(%r12), %eax
	movl	%eax, -160(%rbp)
	cmpl	$-1, %eax
	jne	.L1065
.L1101:
	movl	8(%r12), %eax
	movl	%eax, -160(%rbp)
	cmpl	$65534, %eax
	jg	.L1109
	addl	$1, %eax
	movl	%eax, 8(%r12)
.L1067:
	movl	-160(%rbp), %eax
	movl	%eax, 16(%r12)
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1108:
	movb	$1, 49(%r12)
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1109:
	movb	$1, 49(%r12)
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1099:
	movl	$16, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1104:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1102:
	movl	$72, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1105:
	movq	%rdx, %rdi
	movl	$80, %esi
	movq	%rdx, -192(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-192(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1106:
	movl	$80, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1098:
	movl	$16, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%rbx), %rdx
	movq	%rax, %r13
	movq	16(%rbx), %rax
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1103:
	movl	$16, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%rbx), %rdx
	movq	%rax, %r15
	movq	16(%rbx), %rax
	jmp	.L1071
.L1107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10006:
	.size	_ZN2v88internal12_GLOBAL__N_129BoundaryAssertionAsLookaroundEPNS0_14RegExpCompilerEPNS0_10RegExpNodeENS0_15RegExpAssertion13AssertionTypeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE, .-_ZN2v88internal12_GLOBAL__N_129BoundaryAssertionAsLookaroundEPNS0_14RegExpCompilerEPNS0_10RegExpNodeENS0_15RegExpAssertion13AssertionTypeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	.section	.text._ZN2v88internal15RegExpAssertion6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15RegExpAssertion6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.type	_ZN2v88internal15RegExpAssertion6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, @function
_ZN2v88internal15RegExpAssertion6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE:
.LFB10007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$5, 8(%rdi)
	ja	.L1111
	movl	8(%rdi), %eax
	movq	%rdx, %r13
	leaq	.L1113(%rip), %rdx
	movq	%rsi, %r8
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal15RegExpAssertion6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE,"a",@progbits
	.align 4
	.align 4
.L1113:
	.long	.L1118-.L1113
	.long	.L1117-.L1113
	.long	.L1116-.L1113
	.long	.L1115-.L1113
	.long	.L1114-.L1113
	.long	.L1112-.L1113
	.section	.text._ZN2v88internal15RegExpAssertion6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.p2align 4,,10
	.p2align 3
.L1114:
	movl	12(%rdi), %ecx
	movl	%ecx, %eax
	andl	$18, %eax
	cmpl	$18, %eax
	je	.L1163
	movq	48(%r13), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$71, %rax
	jbe	.L1164
	leaq	72(%r12), %rax
	movq	%rax, 16(%rdi)
.L1126:
	movq	48(%r13), %xmm0
	leaq	16+_ZTVN2v88internal13AssertionNodeE(%rip), %rax
	movq	%r13, %xmm7
	pxor	%xmm1, %xmm1
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	punpcklqdq	%xmm7, %xmm0
	movq	$0, 24(%r12)
	movq	%rax, (%r12)
	movl	$2, 64(%r12)
	movups	%xmm1, 32(%r12)
	movups	%xmm0, 48(%r12)
	.p2align 4,,10
	.p2align 3
.L1110:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1165
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1112:
	.cfi_restore_state
	movl	12(%rdi), %ecx
	movl	%ecx, %eax
	andl	$18, %eax
	cmpl	$18, %eax
	je	.L1166
	movq	48(%r13), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$71, %rax
	jbe	.L1167
	leaq	72(%r12), %rax
	movq	%rax, 16(%rdi)
.L1129:
	movq	48(%r13), %xmm0
	movq	%r13, %xmm7
	pxor	%xmm1, %xmm1
	leaq	16+_ZTVN2v88internal13AssertionNodeE(%rip), %rax
	movq	$0, 8(%r12)
	punpcklqdq	%xmm7, %xmm0
	movq	%rax, (%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movl	$3, 64(%r12)
	movups	%xmm1, 32(%r12)
	movups	%xmm0, 48(%r12)
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1118:
	movq	48(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$71, %rdx
	jbe	.L1168
	leaq	72(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1120:
	movq	48(%r13), %xmm0
	movq	%r13, %xmm2
	pxor	%xmm1, %xmm1
	movq	%rax, %r12
	movq	$0, 8(%rax)
	punpcklqdq	%xmm2, %xmm0
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm0, 48(%rax)
	leaq	16+_ZTVN2v88internal13AssertionNodeE(%rip), %rax
	movq	%rax, (%r12)
	movl	$4, 64(%r12)
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1117:
	movq	48(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$71, %rdx
	jbe	.L1169
	leaq	72(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1123:
	movq	48(%r13), %xmm0
	movq	%r13, %xmm3
	pxor	%xmm1, %xmm1
	movq	%rax, %r12
	movq	$0, 8(%rax)
	punpcklqdq	%xmm3, %xmm0
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm0, 48(%rax)
	leaq	16+_ZTVN2v88internal13AssertionNodeE(%rip), %rax
	movq	%rax, (%r12)
	movl	$1, 64(%r12)
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1116:
	movl	8(%rsi), %r14d
	movq	1096(%rsi), %r15
	cmpl	$65534, %r14d
	jg	.L1161
	leal	1(%r14), %eax
	movl	%eax, -84(%rbp)
	je	.L1170
	leal	2(%r14), %eax
	movl	%eax, 8(%rsi)
.L1134:
	movq	16(%r15), %rbx
	movq	24(%r15), %rax
	subq	%rbx, %rax
	cmpq	$71, %rax
	jbe	.L1171
	leaq	72(%rbx), %rax
	movq	%rax, 16(%r15)
.L1136:
	leaq	16+_ZTVN2v88internal10ChoiceNodeE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r15, 48(%rbx)
	movq	%rbx, %r12
	movq	%rax, (%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movups	%xmm0, 32(%rbx)
	movq	24(%r15), %rdx
	movq	16(%r15), %rcx
	movq	%rdx, %rax
	subq	%rcx, %rax
	cmpq	$15, %rax
	jbe	.L1172
	leaq	16(%rcx), %rax
	movq	%rax, 16(%r15)
.L1138:
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1173
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%r15)
.L1140:
	xorl	%edx, %edx
	movq	%rax, (%rcx)
	movq	$2, 8(%rcx)
	movq	%rcx, 56(%rbx)
	movw	%dx, 64(%rbx)
	movq	24(%r15), %rdx
	movq	16(%r15), %r8
	movq	%rdx, %rax
	subq	%r8, %rax
	cmpq	$15, %rax
	jbe	.L1174
	leaq	16(%r8), %rax
	movq	%rax, 16(%r15)
.L1142:
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L1175
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%r15)
.L1144:
	movq	%rax, (%r8)
	movq	%r15, %rdx
	movq	%r8, %rsi
	movl	$110, %edi
	movq	$3, 8(%r8)
	call	_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEPNS0_4ZoneE
	movq	16(%r15), %r9
	movq	24(%r15), %rax
	subq	%r9, %rax
	cmpq	$31, %rax
	jbe	.L1176
	leaq	32(%r9), %rax
	movq	%rax, 16(%r15)
.L1146:
	leaq	16+_ZTVN2v88internal20RegExpCharacterClassE(%rip), %rax
	movl	-84(%rbp), %esi
	xorl	%edx, %edx
	movq	%r13, %r8
	movq	%rax, (%r9)
	movl	$110, %eax
	movl	$-1, %ecx
	movl	%r14d, %edi
	movq	$0, 8(%r9)
	movw	%ax, 16(%r9)
	movq	$0, 24(%r9)
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal10ActionNode23PositiveSubmatchSuccessEiiiiPNS0_10RegExpNodeE@PLT
	movq	16(%r15), %r8
	movq	-96(%rbp), %r9
	movq	%rax, %rdx
	movq	24(%r15), %rax
	subq	%r8, %rax
	cmpq	$79, %rax
	jbe	.L1177
	leaq	80(%r8), %rax
	movq	%rax, 16(%r15)
.L1148:
	movq	48(%rdx), %rdi
	pxor	%xmm0, %xmm0
	movq	%rdx, %xmm5
	leaq	16+_ZTVN2v88internal8TextNodeE(%rip), %rax
	movups	%xmm0, 32(%r8)
	movq	%rdi, %xmm0
	movq	%rax, (%r8)
	punpcklqdq	%xmm5, %xmm0
	movq	$0, 8(%r8)
	movq	$0, 16(%r8)
	movq	$0, 24(%r8)
	movups	%xmm0, 48(%r8)
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rcx
	movq	%rdx, %rax
	subq	%rcx, %rax
	cmpq	$15, %rax
	jbe	.L1178
	leaq	16(%rcx), %rax
	movq	%rax, 16(%rdi)
.L1150:
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L1179
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1152:
	movq	%rax, (%rcx)
	movq	%r9, %rdi
	movq	$1, 8(%rcx)
	movq	48(%r8), %r15
	movq	%rcx, 64(%r8)
	movb	$0, 72(%r8)
	movq	%rcx, -104(%rbp)
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal11TextElement9CharClassEPNS0_20RegExpCharacterClassE@PLT
	movq	-104(%rbp), %rcx
	movq	-96(%rbp), %r8
	movq	%rax, %r11
	movq	%rdx, %r9
	movslq	12(%rcx), %rax
	movl	8(%rcx), %edx
	cmpl	%edx, %eax
	jge	.L1153
	leal	1(%rax), %edx
	salq	$4, %rax
	addq	(%rcx), %rax
	movl	%edx, 12(%rcx)
	movq	%r11, (%rax)
	movq	%r9, 8(%rax)
.L1154:
	movl	-84(%rbp), %esi
	movq	%r8, %rdx
	movl	%r14d, %edi
	leaq	-80(%rbp), %r14
	call	_ZN2v88internal10ActionNode13BeginSubmatchEiiPNS0_10RegExpNodeE@PLT
	movq	$0, -72(%rbp)
	movq	%r14, %rsi
	movq	%rax, -80(%rbp)
	movq	48(%rbx), %rdx
	movq	56(%rbx), %rdi
	call	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	movq	48(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$71, %rdx
	jbe	.L1180
	leaq	72(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1159:
	movq	48(%r13), %xmm0
	movq	%r13, %xmm6
	pxor	%xmm1, %xmm1
	movq	%r14, %rsi
	leaq	16+_ZTVN2v88internal13AssertionNodeE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	punpcklqdq	%xmm6, %xmm0
	movq	$0, 24(%rax)
	movq	%rcx, (%rax)
	movl	$0, 64(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm0, 48(%rax)
	movq	%rax, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	48(%rbx), %rdx
	movq	56(%rbx), %rdi
	call	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1115:
	movq	48(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$71, %rdx
	jbe	.L1181
	leaq	72(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1131:
	movq	48(%r13), %xmm0
	movq	%r13, %xmm4
	pxor	%xmm1, %xmm1
	movq	%rax, %r12
	movq	$0, 8(%rax)
	punpcklqdq	%xmm4, %xmm0
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm0, 48(%rax)
	leaq	16+_ZTVN2v88internal13AssertionNodeE(%rip), %rax
	movq	%rax, (%r12)
	movl	$0, 64(%r12)
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1161:
	movl	%r14d, -84(%rbp)
.L1132:
	movb	$1, 49(%r8)
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1153:
	movq	16(%r15), %rdi
	movq	24(%r15), %rax
	leal	1(%rdx,%rdx), %r10d
	movslq	%r10d, %rsi
	salq	$4, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1182
	addq	%rdi, %rsi
	movq	%rsi, 16(%r15)
.L1156:
	movslq	12(%rcx), %rdx
	movq	%rdx, %rax
	salq	$4, %rdx
	testl	%eax, %eax
	jle	.L1157
	movq	(%rcx), %rsi
	movq	%r9, -128(%rbp)
	movq	%r11, -120(%rbp)
	movl	%r10d, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rcx, -96(%rbp)
	call	memcpy@PLT
	movq	-96(%rbp), %rcx
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %r11
	movl	-112(%rbp), %r10d
	movq	%rax, %rdi
	movslq	12(%rcx), %rdx
	movq	-104(%rbp), %r8
	movq	%rdx, %rax
	salq	$4, %rdx
.L1157:
	movq	%rdi, (%rcx)
	addl	$1, %eax
	addq	%rdx, %rdi
	movl	%r10d, 8(%rcx)
	movl	%eax, 12(%rcx)
	movq	%r11, (%rdi)
	movq	%r9, 8(%rdi)
	jmp	.L1154
.L1182:
	movq	%r15, %rdi
	movq	%r9, -128(%rbp)
	movq	%r11, -120(%rbp)
	movl	%r10d, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %rcx
	movl	-112(%rbp), %r10d
	movq	-120(%rbp), %r11
	movq	%rax, %rdi
	movq	-128(%rbp), %r9
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1166:
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal12_GLOBAL__N_129BoundaryAssertionAsLookaroundEPNS0_14RegExpCompilerEPNS0_10RegExpNodeENS0_15RegExpAssertion13AssertionTypeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	movq	%rax, %r12
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1163:
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal12_GLOBAL__N_129BoundaryAssertionAsLookaroundEPNS0_14RegExpCompilerEPNS0_10RegExpNodeENS0_15RegExpAssertion13AssertionTypeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	movq	%rax, %r12
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1169:
	movl	$72, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1123
	.p2align 4,,10
	.p2align 3
.L1168:
	movl	$72, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1181:
	movl	$72, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1180:
	movl	$72, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1179:
	movl	$16, %esi
	movq	%rcx, -112(%rbp)
	movq	%r9, -104(%rbp)
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %rcx
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1178:
	movl	$16, %esi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rdi
	movq	-104(%rbp), %r8
	movq	%rax, %rcx
	movq	-112(%rbp), %r9
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1177:
	movl	$80, %esi
	movq	%r15, %rdi
	movq	%r9, -104(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %r9
	movq	%rax, %r8
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1176:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r9
	jmp	.L1146
	.p2align 4,,10
	.p2align 3
.L1175:
	movl	$24, %esi
	movq	%r15, %rdi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %r8
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1174:
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r15), %rdx
	movq	%rax, %r8
	movq	16(%r15), %rax
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1173:
	movl	$32, %esi
	movq	%r15, %rdi
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rcx
	jmp	.L1140
	.p2align 4,,10
	.p2align 3
.L1172:
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r15), %rdx
	movq	%rax, %rcx
	movq	16(%r15), %rax
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1171:
	movl	$72, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1167:
	movl	$72, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1164:
	movl	$72, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1126
.L1111:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1165:
	call	__stack_chk_fail@PLT
.L1170:
	movl	$65535, 8(%rsi)
	jmp	.L1132
	.cfi_endproc
.LFE10007:
	.size	_ZN2v88internal15RegExpAssertion6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, .-_ZN2v88internal15RegExpAssertion6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE
	.type	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE, @function
_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE:
.LFB10041:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L1193
	ret
	.p2align 4,,10
	.p2align 3
.L1193:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	24(%rsi), %rcx
	movq	16(%rsi), %r12
	movq	%rcx, %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L1194
	leaq	16(%r12), %rax
	movq	%rax, 16(%rsi)
.L1186:
	subq	%rax, %rcx
	cmpq	$15, %rcx
	jbe	.L1195
	leaq	16(%rax), %rcx
	movq	%rcx, 16(%rdx)
.L1188:
	movq	%rax, (%r12)
	movq	%r12, %rsi
	movq	$2, 8(%r12)
	movsbl	8(%rbx), %edi
	movq	%r12, (%rbx)
	call	_ZN2v88internal14CharacterRange14AddClassEscapeEcPNS0_8ZoneListIS1_EEPNS0_4ZoneE
	movq	(%rbx), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1194:
	.cfi_restore_state
	movq	%rdx, %rdi
	movl	$16, %esi
	movq	%rdx, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-24(%rbp), %rdx
	movq	%rax, %r12
	movq	24(%rdx), %rcx
	movq	16(%rdx), %rax
	jmp	.L1186
	.p2align 4,,10
	.p2align 3
.L1195:
	movq	%rdx, %rdi
	movl	$16, %esi
	movq	%rdx, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-24(%rbp), %rdx
	jmp	.L1188
	.cfi_endproc
.LFE10041:
	.size	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE, .-_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE
	.section	.text._ZN2v88internal20RegExpCharacterClass11is_standardEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20RegExpCharacterClass11is_standardEPNS0_4ZoneE
	.type	_ZN2v88internal20RegExpCharacterClass11is_standardEPNS0_4ZoneE, @function
_ZN2v88internal20RegExpCharacterClass11is_standardEPNS0_4ZoneE:
.LFB9969:
	.cfi_startproc
	endbr64
	testb	$1, 28(%rdi)
	jne	.L1209
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpw	$0, 16(%rdi)
	je	.L1210
.L1196:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1210:
	.cfi_restore_state
	leaq	8(%rdi), %r13
	movq	%rsi, %r12
	movq	%r13, %rdi
	call	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE
	movl	12(%rax), %ecx
	leal	(%rcx,%rcx), %edx
	cmpl	$20, %edx
	jne	.L1199
	movq	(%rax), %rax
	cmpl	$9, (%rax)
	movl	4(%rax), %edx
	jne	.L1199
	cmpl	$13, %edx
	jne	.L1199
	cmpl	$32, 8(%rax)
	movl	12(%rax), %edx
	jne	.L1199
	cmpl	$32, %edx
	jne	.L1199
	cmpl	$160, 16(%rax)
	movl	20(%rax), %edx
	jne	.L1199
	cmpl	$160, %edx
	jne	.L1199
	cmpl	$5760, 24(%rax)
	movl	28(%rax), %edx
	jne	.L1199
	cmpl	$5760, %edx
	jne	.L1199
	cmpl	$8192, 32(%rax)
	movl	36(%rax), %edx
	jne	.L1199
	cmpl	$8202, %edx
	jne	.L1199
	cmpl	$8232, 40(%rax)
	movl	44(%rax), %edx
	jne	.L1199
	cmpl	$8233, %edx
	jne	.L1199
	cmpl	$8239, 48(%rax)
	movl	52(%rax), %edx
	jne	.L1199
	cmpl	$8239, %edx
	jne	.L1199
	cmpl	$8287, 56(%rax)
	movl	60(%rax), %edx
	jne	.L1199
	cmpl	$8287, %edx
	jne	.L1199
	cmpl	$12288, 64(%rax)
	movl	68(%rax), %edx
	jne	.L1199
	cmpl	$12288, %edx
	jne	.L1199
	cmpl	$65279, 72(%rax)
	movl	76(%rax), %edx
	jne	.L1199
	cmpl	$65279, %edx
	jne	.L1199
	movl	$115, %r11d
	movl	$1, %eax
	movw	%r11w, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1199:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE
	cmpl	$11, 12(%rax)
	jne	.L1200
	movq	(%rax), %rax
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	jne	.L1200
	cmpl	$8, 4(%rax)
	jne	.L1200
	cmpl	$14, 8(%rax)
	movl	12(%rax), %edx
	jne	.L1200
	cmpl	$31, %edx
	jne	.L1200
	cmpl	$33, 16(%rax)
	movl	20(%rax), %edx
	jne	.L1200
	cmpl	$159, %edx
	jne	.L1200
	cmpl	$161, 24(%rax)
	movl	28(%rax), %edx
	jne	.L1200
	cmpl	$5759, %edx
	jne	.L1200
	cmpl	$5761, 32(%rax)
	movl	36(%rax), %edx
	jne	.L1200
	cmpl	$8191, %edx
	jne	.L1200
	cmpl	$8203, 40(%rax)
	movl	44(%rax), %edx
	jne	.L1200
	cmpl	$8231, %edx
	jne	.L1200
	cmpl	$8234, 48(%rax)
	movl	52(%rax), %edx
	jne	.L1200
	cmpl	$8238, %edx
	jne	.L1200
	cmpl	$8240, 56(%rax)
	movl	60(%rax), %edx
	jne	.L1200
	cmpl	$8286, %edx
	jne	.L1200
	cmpl	$8288, 64(%rax)
	movl	68(%rax), %edx
	jne	.L1200
	cmpl	$12287, %edx
	jne	.L1200
	cmpl	$12289, 72(%rax)
	movl	76(%rax), %edx
	jne	.L1200
	cmpl	$65278, %edx
	jne	.L1200
	cmpl	$65280, 80(%rax)
	movl	84(%rax), %edx
	jne	.L1200
	cmpl	$1114111, %edx
	jne	.L1200
	movl	$83, %r9d
	movl	$1, %eax
	movw	%r9w, 16(%rbx)
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1200:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE
	cmpl	$4, 12(%rax)
	jne	.L1201
	movq	(%rax), %rax
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jne	.L1201
	cmpl	$9, 4(%rax)
	jne	.L1201
	cmpl	$11, 8(%rax)
	movl	12(%rax), %edx
	jne	.L1201
	cmpl	$12, %edx
	jne	.L1201
	cmpl	$14, 16(%rax)
	movl	20(%rax), %edx
	jne	.L1201
	cmpl	$8231, %edx
	jne	.L1201
	cmpl	$8234, 24(%rax)
	movl	28(%rax), %edx
	jne	.L1201
	cmpl	$1114111, %edx
	jne	.L1201
	movl	$46, %edi
	movl	$1, %eax
	movw	%di, 16(%rbx)
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1209:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1201:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE
	movl	12(%rax), %ecx
	leal	(%rcx,%rcx), %edx
	cmpl	$6, %edx
	jne	.L1202
	movq	(%rax), %rax
	cmpl	$10, (%rax)
	movl	4(%rax), %edx
	jne	.L1202
	cmpl	$10, %edx
	jne	.L1202
	cmpl	$13, 8(%rax)
	movl	12(%rax), %edx
	jne	.L1202
	cmpl	$13, %edx
	jne	.L1202
	cmpl	$8232, 16(%rax)
	movl	20(%rax), %edx
	jne	.L1202
	cmpl	$8233, %edx
	jne	.L1202
	movl	$110, %esi
	movl	$1, %eax
	movw	%si, 16(%rbx)
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1202:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE
	movl	12(%rax), %ecx
	leal	(%rcx,%rcx), %edx
	cmpl	$8, %edx
	jne	.L1203
	movq	(%rax), %rax
	cmpl	$48, (%rax)
	movl	4(%rax), %edx
	jne	.L1203
	cmpl	$57, %edx
	jne	.L1203
	cmpl	$65, 8(%rax)
	movl	12(%rax), %edx
	jne	.L1203
	cmpl	$90, %edx
	jne	.L1203
	cmpl	$95, 16(%rax)
	movl	20(%rax), %edx
	jne	.L1203
	cmpl	$95, %edx
	jne	.L1203
	cmpl	$97, 24(%rax)
	movl	28(%rax), %edx
	jne	.L1203
	cmpl	$122, %edx
	jne	.L1203
	movl	$119, %ecx
	movl	$1, %eax
	movw	%cx, 16(%rbx)
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1203:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE
	cmpl	$5, 12(%rax)
	jne	.L1204
	movq	(%rax), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L1204
	cmpl	$47, 4(%rax)
	jne	.L1204
	cmpl	$58, 8(%rax)
	movl	12(%rax), %edx
	jne	.L1204
	cmpl	$64, %edx
	jne	.L1204
	cmpl	$91, 16(%rax)
	movl	20(%rax), %edx
	jne	.L1204
	cmpl	$94, %edx
	jne	.L1204
	cmpl	$96, 24(%rax)
	movl	28(%rax), %edx
	jne	.L1204
	cmpl	$96, %edx
	jne	.L1204
	cmpl	$123, 32(%rax)
	movl	36(%rax), %edx
	jne	.L1204
	cmpl	$1114111, %edx
	jne	.L1204
	movl	$87, %eax
	movw	%ax, 16(%rbx)
	movl	$1, %eax
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1204:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9969:
	.size	_ZN2v88internal20RegExpCharacterClass11is_standardEPNS0_4ZoneE, .-_ZN2v88internal20RegExpCharacterClass11is_standardEPNS0_4ZoneE
	.section	.text._ZN2v88internal20RegExpCharacterClass6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20RegExpCharacterClass6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.type	_ZN2v88internal20RegExpCharacterClass6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, @function
_ZN2v88internal20RegExpCharacterClass6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE:
.LFB9989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -440(%rbp)
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1212
	call	_ZN2v88internal14CharacterRange12CanonicalizeEPNS0_8ZoneListIS1_EE
.L1212:
	movq	1096(%r12), %r14
	leaq	8(%r13), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE
	movq	%rax, %rbx
	movl	24(%r13), %eax
	movl	%eax, %edx
	andl	$18, %edx
	cmpl	$18, %edx
	je	.L1315
.L1213:
	testb	$16, %al
	je	.L1214
	cmpb	$0, 48(%r12)
	je	.L1316
.L1214:
	movq	16(%r14), %rbx
	movq	24(%r14), %rax
	movzbl	52(%r12), %ecx
	subq	%rbx, %rax
	cmpq	$79, %rax
	jbe	.L1317
	leaq	80(%rbx), %rax
	movq	%rax, 16(%r14)
.L1217:
	movq	-440(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%rbx, %r15
	movq	48(%rax), %r14
	movups	%xmm0, 32(%rbx)
	leaq	16+_ZTVN2v88internal8TextNodeE(%rip), %rax
	movq	%rax, (%rbx)
	movq	%r14, %xmm0
	movq	$0, 8(%rbx)
	movhps	-440(%rbp), %xmm0
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movups	%xmm0, 48(%rbx)
	movq	24(%r14), %rdx
	movq	16(%r14), %r12
	movq	%rdx, %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L1318
	leaq	16(%r12), %rax
	movq	%rax, 16(%r14)
.L1219:
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L1319
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
.L1221:
	movq	%rax, (%r12)
	movq	%r13, %rdi
	movq	$1, 8(%r12)
	movq	48(%rbx), %r14
	movq	%r12, 64(%rbx)
	movb	%cl, 72(%rbx)
	call	_ZN2v88internal11TextElement9CharClassEPNS0_20RegExpCharacterClassE@PLT
	movq	%rax, %r13
	movq	%rdx, %rbx
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jge	.L1222
.L1314:
	leal	1(%rax), %edx
	salq	$4, %rax
	addq	(%r12), %rax
	movl	%edx, 12(%r12)
	movq	%r13, (%rax)
	movq	%rbx, 8(%rax)
.L1211:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1320
	addq	$472, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1316:
	.cfi_restore_state
	movl	28(%r13), %eax
	testb	$2, %al
	jne	.L1214
	testb	$1, %al
	jne	.L1321
.L1228:
	movl	12(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L1322
	cmpw	$42, 16(%r13)
	jne	.L1247
	movq	1096(%r12), %r12
	movabsq	$281470681743360, %rax
	movq	%rax, -432(%rbp)
	movq	24(%r12), %rax
	movq	16(%r12), %r13
	movq	%rax, %rdx
	subq	%r13, %rdx
	cmpq	$15, %rdx
	jbe	.L1323
	leaq	16(%r13), %rdx
	movq	%rdx, 16(%r12)
.L1249:
	subq	%rdx, %rax
	cmpq	$7, %rax
	jbe	.L1324
	leaq	8(%rdx), %rax
	movq	%rax, 16(%r12)
.L1251:
	movq	%rdx, 0(%r13)
	leaq	-432(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	$1, 8(%r13)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	-440(%rbp), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8TextNode24CreateForCharacterRangesEPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEEbPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE@PLT
	movq	%rax, %r15
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1222:
	movq	16(%r14), %rdi
	movq	24(%r14), %rax
	leal	1(%rdx,%rdx), %r8d
	movslq	%r8d, %rsi
	salq	$4, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1325
	addq	%rdi, %rsi
	movq	%rsi, 16(%r14)
.L1225:
	movslq	12(%r12), %rdx
	movq	%rdx, %rcx
	salq	$4, %rdx
	testl	%ecx, %ecx
	jg	.L1326
.L1226:
	addq	%rdi, %rdx
	addl	$1, %ecx
	movq	%rdi, (%r12)
	movl	%r8d, 8(%r12)
	movl	%ecx, 12(%r12)
	movq	%r13, (%rdx)
	movq	%rbx, 8(%rdx)
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1315:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_125AddUnicodeCaseEquivalentsEPNS0_8ZoneListINS0_14CharacterRangeEEEPNS0_4ZoneE
	movl	24(%r13), %eax
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1326:
	movq	(%r12), %rsi
	movl	%r8d, -440(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movl	-440(%rbp), %r8d
	movq	%rax, %rdi
	movq	%rdx, %rcx
	salq	$4, %rdx
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1317:
	movl	$80, %esi
	movq	%r14, %rdi
	movb	%cl, -448(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-448(%rbp), %ecx
	movq	%rax, %rbx
	jmp	.L1217
	.p2align 4,,10
	.p2align 3
.L1318:
	movl	$16, %esi
	movq	%r14, %rdi
	movb	%cl, -440(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	24(%r14), %rdx
	movzbl	-440(%rbp), %ecx
	movq	%rax, %r12
	movq	16(%r14), %rax
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1319:
	movl	$16, %esi
	movq	%r14, %rdi
	movb	%cl, -440(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-440(%rbp), %ecx
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1247:
	movq	16(%r14), %rax
	movq	%rax, %rcx
	movq	%rax, -480(%rbp)
	movq	24(%r14), %rax
	subq	%rcx, %rax
	cmpq	$71, %rax
	jbe	.L1327
	movq	%rcx, %rax
	addq	$72, %rax
	movq	%rax, 16(%r14)
.L1253:
	movq	-480(%rbp), %rax
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal10ChoiceNodeE(%rip), %rcx
	movq	%r14, 48(%rax)
	movq	%rax, %r15
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rcx, (%rax)
	movups	%xmm0, 32(%rax)
	movq	24(%r14), %rax
	movq	16(%r14), %r13
	movq	%rax, %rdx
	subq	%r13, %rdx
	cmpq	$15, %rdx
	jbe	.L1328
	leaq	16(%r13), %rdx
	movq	%rdx, 16(%r14)
.L1255:
	subq	%rdx, %rax
	cmpq	$31, %rax
	jbe	.L1329
	leaq	32(%rdx), %rax
	movq	%rax, 16(%r14)
.L1257:
	movq	-480(%rbp), %r14
	movq	%rdx, 0(%r13)
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	$2, 8(%r13)
	movq	%r13, 56(%r14)
	leaq	-416(%rbp), %r13
	movw	%dx, 64(%r14)
	movq	%r13, %rdi
	call	_ZN2v88internal20UnicodeRangeSplitterC1EPNS0_8ZoneListINS0_14CharacterRangeEEE
	movq	1096(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_119ToCanonicalZoneListEPKNS_4base11SmallVectorINS0_14CharacterRangeELm8EEEPNS0_4ZoneE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1259
	movzbl	52(%r12), %edx
	movq	1096(%r12), %rdi
	xorl	%r8d, %r8d
	movq	-440(%rbp), %rcx
	call	_ZN2v88internal8TextNode24CreateForCharacterRangesEPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEEbPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE@PLT
	leaq	-432(%rbp), %rsi
	movq	$0, -424(%rbp)
	movq	%rax, -432(%rbp)
	movq	48(%r14), %rdx
	movq	56(%r14), %rdi
	call	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
.L1259:
	movq	1096(%r12), %rsi
	leaq	-152(%rbp), %rdi
	call	_ZN2v88internal12_GLOBAL__N_119ToCanonicalZoneListEPKNS_4base11SmallVectorINS0_14CharacterRangeELm8EEEPNS0_4ZoneE
	movq	%rax, -448(%rbp)
	testq	%rax, %rax
	je	.L1264
	movq	%rax, %rdi
	movq	1096(%r12), %rax
	movq	%rax, -464(%rbp)
	call	_ZN2v88internal14CharacterRange12CanonicalizeEPNS0_8ZoneListIS1_EE
	leaq	-432(%rbp), %rax
	xorl	%r11d, %r11d
	movq	%rax, -472(%rbp)
	movl	12(%rdi), %eax
	testl	%eax, %eax
	jle	.L1264
	movq	%r15, -504(%rbp)
	movq	-480(%rbp), %r15
	movq	%r12, -456(%rbp)
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1266:
	movq	-448(%rbp), %rax
	addq	$1, %r11
	cmpl	%r11d, 12(%rax)
	jle	.L1330
.L1263:
	movq	-448(%rbp), %rax
	movq	(%rax), %rax
	leaq	(%rax,%r11,8), %rax
	movl	(%rax), %edx
	movl	4(%rax), %ebx
	leal	-65536(%rdx), %r13d
	leal	-65536(%rbx), %r12d
	andw	$1023, %dx
	andw	$1023, %bx
	shrl	$10, %r13d
	shrl	$10, %r12d
	subw	$9216, %dx
	subw	$9216, %bx
	andw	$1023, %r13w
	andw	$1023, %r12w
	leal	-10240(%r13), %r14d
	leal	-10240(%r12), %r10d
	cmpw	%r12w, %r13w
	je	.L1331
	cmpw	$-9216, %dx
	je	.L1267
	movq	-456(%rbp), %rax
	movzwl	%r14w, %esi
	movzwl	%dx, %edx
	xorl	%r9d, %r9d
	movq	%rsi, %r14
	movq	-440(%rbp), %r8
	movq	-464(%rbp), %rdi
	movq	%r11, -496(%rbp)
	movzbl	52(%rax), %ecx
	salq	$32, %r14
	movl	%r10d, -488(%rbp)
	movabsq	$246286309654528, %rax
	orq	%r14, %rsi
	orq	%rax, %rdx
	leal	-10239(%r13), %r14d
	call	_ZN2v88internal8TextNode22CreateForSurrogatePairEPNS0_4ZoneENS0_14CharacterRangeES4_bPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE@PLT
	movq	-472(%rbp), %rsi
	movq	$0, -424(%rbp)
	movq	%rax, -432(%rbp)
	movq	48(%r15), %rdx
	movq	56(%r15), %rdi
	call	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	movq	-496(%rbp), %r11
	movl	-488(%rbp), %r10d
.L1267:
	cmpw	$-8193, %bx
	je	.L1268
	movq	-456(%rbp), %rax
	movzwl	%r10w, %esi
	movzwl	%bx, %edx
	movq	-440(%rbp), %r8
	movq	%rsi, %r10
	movq	-464(%rbp), %rdi
	salq	$32, %rdx
	xorl	%r9d, %r9d
	salq	$32, %r10
	movzbl	52(%rax), %ecx
	orb	$-36, %dh
	movq	%r11, -488(%rbp)
	orq	%r10, %rsi
	call	_ZN2v88internal8TextNode22CreateForSurrogatePairEPNS0_4ZoneENS0_14CharacterRangeES4_bPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE@PLT
	movq	-472(%rbp), %rsi
	movq	$0, -424(%rbp)
	movq	%rax, -432(%rbp)
	movq	48(%r15), %rdx
	movq	56(%r15), %rdi
	call	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	movq	-488(%rbp), %r11
	leal	-10241(%r12), %r10d
.L1268:
	cmpw	%r10w, %r14w
	ja	.L1266
	movzwl	%r10w, %esi
	movq	-456(%rbp), %rax
	xorl	%r9d, %r9d
	movq	%r11, -488(%rbp)
	salq	$32, %rsi
	movq	-440(%rbp), %r8
	movabsq	$246286309710848, %rdx
	movq	%rsi, %r10
	movzbl	52(%rax), %ecx
	movzwl	%r14w, %esi
	orq	%r10, %rsi
.L1313:
	movq	-464(%rbp), %rdi
	call	_ZN2v88internal8TextNode22CreateForSurrogatePairEPNS0_4ZoneENS0_14CharacterRangeES4_bPNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE@PLT
	movq	-472(%rbp), %rsi
	movq	$0, -424(%rbp)
	movq	%rax, -432(%rbp)
	movq	48(%r15), %rdx
	movq	56(%r15), %rdi
	call	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	movq	-488(%rbp), %r11
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1330:
	movq	-504(%rbp), %r15
	movq	-456(%rbp), %r12
.L1264:
	movq	1096(%r12), %rsi
	leaq	-328(%rbp), %rdi
	call	_ZN2v88internal12_GLOBAL__N_119ToCanonicalZoneListEPKNS_4base11SmallVectorINS0_14CharacterRangeELm8EEEPNS0_4ZoneE
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1270
	movq	1096(%r12), %rdx
	movabsq	$246286309710848, %rax
	movq	%rax, -432(%rbp)
	movq	24(%rdx), %rax
	movq	16(%rdx), %r13
	movq	%rax, %rcx
	subq	%r13, %rcx
	cmpq	$15, %rcx
	jbe	.L1332
	leaq	16(%r13), %rcx
	movq	%rcx, 16(%rdx)
.L1272:
	subq	%rcx, %rax
	cmpq	$7, %rax
	jbe	.L1333
	leaq	8(%rcx), %rax
	movq	%rax, 16(%rdx)
.L1274:
	movq	%rcx, 0(%r13)
	leaq	-432(%rbp), %r14
	movq	%r13, %rdi
	movq	$1, 8(%r13)
	movq	%r14, %rsi
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	xorl	%r9d, %r9d
	cmpb	$0, 52(%r12)
	je	.L1275
	movq	-440(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal12_GLOBAL__N_146NegativeLookaroundAgainstReadDirectionAndMatchEPNS0_14RegExpCompilerEPNS0_8ZoneListINS0_14CharacterRangeEEES7_PNS0_10RegExpNodeEbNS_4base5FlagsINS0_8JSRegExp4FlagEiEE
.L1276:
	movq	%rax, -432(%rbp)
	movq	-480(%rbp), %rax
	movq	%r14, %rsi
	movq	$0, -424(%rbp)
	movq	48(%rax), %rdx
	movq	56(%rax), %rdi
	call	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
.L1270:
	movq	1096(%r12), %rsi
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal12_GLOBAL__N_119ToCanonicalZoneListEPKNS_4base11SmallVectorINS0_14CharacterRangeELm8EEEPNS0_4ZoneE
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1278
	movq	1096(%r12), %rdx
	movabsq	$241888263198720, %rax
	movq	%rax, -432(%rbp)
	movq	24(%rdx), %rax
	movq	16(%rdx), %r13
	movq	%rax, %rcx
	subq	%r13, %rcx
	cmpq	$15, %rcx
	jbe	.L1334
	leaq	16(%r13), %rcx
	movq	%rcx, 16(%rdx)
.L1280:
	subq	%rcx, %rax
	cmpq	$7, %rax
	jbe	.L1335
	leaq	8(%rcx), %rax
	movq	%rax, 16(%rdx)
.L1282:
	movq	%rcx, 0(%r13)
	leaq	-432(%rbp), %r14
	movq	%r13, %rdi
	movq	$1, 8(%r13)
	movq	%r14, %rsi
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	xorl	%r9d, %r9d
	cmpb	$0, 52(%r12)
	je	.L1283
	movq	-440(%rbp), %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal12_GLOBAL__N_141MatchAndNegativeLookaroundInReadDirectionEPNS0_14RegExpCompilerEPNS0_8ZoneListINS0_14CharacterRangeEEES7_PNS0_10RegExpNodeEbNS_4base5FlagsINS0_8JSRegExp4FlagEiEE
.L1284:
	movq	%rax, -432(%rbp)
	movq	-480(%rbp), %rax
	movq	%r14, %rsi
	movq	$0, -424(%rbp)
	movq	48(%rax), %rdx
	movq	56(%rax), %rdi
	call	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
.L1278:
	movq	-152(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1285
	call	free@PLT
.L1285:
	movq	-240(%rbp), %rdi
	leaq	-216(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1286
	call	free@PLT
.L1286:
	movq	-328(%rbp), %rdi
	leaq	-304(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1287
	call	free@PLT
.L1287:
	movq	-416(%rbp), %rdi
	leaq	-392(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1211
	call	free@PLT
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1322:
	movq	16(%r14), %r13
	movq	24(%r14), %rax
	subq	%r13, %rax
	cmpq	$31, %rax
	jbe	.L1336
	leaq	32(%r13), %rax
	movq	%rax, 16(%r14)
.L1235:
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal20RegExpCharacterClassC1EPNS0_4ZoneEPNS0_8ZoneListINS0_14CharacterRangeEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS9_INS1_4FlagEiEE
	movq	16(%r14), %rbx
	movq	24(%r14), %rax
	movzbl	52(%r12), %ecx
	subq	%rbx, %rax
	cmpq	$79, %rax
	jbe	.L1337
	leaq	80(%rbx), %rax
	movq	%rax, 16(%r14)
.L1237:
	movq	-440(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%rbx, %r15
	movq	48(%rax), %r14
	movups	%xmm0, 32(%rbx)
	leaq	16+_ZTVN2v88internal8TextNodeE(%rip), %rax
	movq	$0, 8(%rbx)
	movq	%r14, %xmm0
	movq	$0, 16(%rbx)
	movhps	-440(%rbp), %xmm0
	movq	$0, 24(%rbx)
	movq	%rax, (%rbx)
	movups	%xmm0, 48(%rbx)
	movq	24(%r14), %rax
	movq	16(%r14), %r12
	movq	%rax, %rdx
	subq	%r12, %rdx
	cmpq	$15, %rdx
	jbe	.L1338
	leaq	16(%r12), %rdx
	movq	%rdx, 16(%r14)
.L1239:
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L1339
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r14)
.L1241:
	movq	%rdx, (%r12)
	movq	%r13, %rdi
	movq	$1, 8(%r12)
	movq	48(%rbx), %r14
	movq	%r12, 64(%rbx)
	movb	%cl, 72(%rbx)
	call	_ZN2v88internal11TextElement9CharClassEPNS0_20RegExpCharacterClassE@PLT
	movq	%rax, %r13
	movq	%rdx, %rbx
	movslq	12(%r12), %rax
	movl	8(%r12), %edx
	cmpl	%edx, %eax
	jl	.L1314
	movq	16(%r14), %rcx
	movq	24(%r14), %rax
	leal	1(%rdx,%rdx), %r8d
	movslq	%r8d, %rsi
	salq	$4, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1340
	addq	%rcx, %rsi
	movq	%rsi, 16(%r14)
.L1245:
	movslq	12(%r12), %rdx
	movq	%rdx, %rax
	salq	$4, %rdx
	testl	%eax, %eax
	jle	.L1246
	movq	(%r12), %rsi
	movq	%rcx, %rdi
	movl	%r8d, -440(%rbp)
	call	memcpy@PLT
	movslq	12(%r12), %rdx
	movl	-440(%rbp), %r8d
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$4, %rdx
.L1246:
	movq	%rcx, (%r12)
	addl	$1, %eax
	addq	%rdx, %rcx
	movl	%r8d, 8(%r12)
	movl	%eax, 12(%r12)
	movq	%r13, (%rcx)
	movq	%rbx, 8(%rcx)
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1321:
	movq	24(%r14), %rax
	movq	16(%r14), %r15
	movq	%rax, %rdx
	subq	%r15, %rdx
	cmpq	$15, %rdx
	jbe	.L1341
	leaq	16(%r15), %rdx
	movq	%rdx, 16(%r14)
.L1230:
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L1342
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r14)
.L1232:
	movq	%rdx, (%r15)
	movq	%rbx, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	$2, 8(%r15)
	movq	%r15, %rbx
	call	_ZN2v88internal14CharacterRange6NegateEPNS0_8ZoneListIS1_EES4_PNS0_4ZoneE
	jmp	.L1228
.L1336:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L1235
.L1341:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r14), %rdx
	movq	%rax, %r15
	movq	24(%r14), %rax
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1331:
	movzwl	%r14w, %esi
	movzwl	%bx, %ebx
	movzwl	%dx, %edx
	xorl	%r9d, %r9d
	movq	%rsi, %r14
	movq	-456(%rbp), %rax
	salq	$32, %rbx
	movq	%r11, -488(%rbp)
	salq	$32, %r14
	movq	-440(%rbp), %r8
	orq	%rbx, %rdx
	movzbl	52(%rax), %ecx
	orq	%r14, %rsi
	jmp	.L1313
	.p2align 4,,10
	.p2align 3
.L1325:
	movq	%r14, %rdi
	movl	%r8d, -440(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-440(%rbp), %r8d
	movq	%rax, %rdi
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1275:
	movq	-440(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_141MatchAndNegativeLookaroundInReadDirectionEPNS0_14RegExpCompilerEPNS0_8ZoneListINS0_14CharacterRangeEEES7_PNS0_10RegExpNodeEbNS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1283:
	movq	-440(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_146NegativeLookaroundAgainstReadDirectionAndMatchEPNS0_14RegExpCompilerEPNS0_8ZoneListINS0_14CharacterRangeEEES7_PNS0_10RegExpNodeEbNS_4base5FlagsINS0_8JSRegExp4FlagEiEE
	jmp	.L1284
.L1328:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r14), %rdx
	movq	%rax, %r13
	movq	24(%r14), %rax
	jmp	.L1255
.L1327:
	movl	$72, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, -480(%rbp)
	jmp	.L1253
.L1329:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1257
.L1324:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1251
.L1323:
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %r13
	movq	24(%r12), %rax
	jmp	.L1249
.L1338:
	movl	$16, %esi
	movq	%r14, %rdi
	movb	%cl, -440(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-440(%rbp), %ecx
	movq	16(%r14), %rdx
	movq	%rax, %r12
	movq	24(%r14), %rax
	jmp	.L1239
.L1337:
	movl	$80, %esi
	movq	%r14, %rdi
	movb	%cl, -448(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-448(%rbp), %ecx
	movq	%rax, %rbx
	jmp	.L1237
.L1339:
	movl	$16, %esi
	movq	%r14, %rdi
	movb	%cl, -440(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-440(%rbp), %ecx
	movq	%rax, %rdx
	jmp	.L1241
.L1342:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1232
.L1335:
	movq	%rdx, %rdi
	movl	$8, %esi
	movq	%rdx, -448(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-448(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L1282
.L1334:
	movq	%rdx, %rdi
	movl	$16, %esi
	movq	%rdx, -448(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-448(%rbp), %rdx
	movq	%rax, %r13
	movq	16(%rdx), %rcx
	movq	24(%rdx), %rax
	jmp	.L1280
.L1333:
	movq	%rdx, %rdi
	movl	$8, %esi
	movq	%rdx, -448(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-448(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L1274
.L1332:
	movq	%rdx, %rdi
	movl	$16, %esi
	movq	%rdx, -448(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-448(%rbp), %rdx
	movq	%rax, %r13
	movq	16(%rdx), %rcx
	movq	24(%rdx), %rax
	jmp	.L1272
.L1340:
	movq	%r14, %rdi
	movl	%r8d, -440(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-440(%rbp), %r8d
	movq	%rax, %rcx
	jmp	.L1245
.L1320:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9989:
	.size	_ZN2v88internal20RegExpCharacterClass6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, .-_ZN2v88internal20RegExpCharacterClass6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	.type	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_, @function
_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_:
.LFB9994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$152, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	(%rbx), %rdi
	movq	%rax, %r12
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	%rax, %rbx
	movq	8(%r12), %rax
	leaq	-176(%rbp), %r12
	movq	%r12, %rdi
	movzwl	(%rax), %esi
	call	_ZN6icu_6713UnicodeStringC1Ei@PLT
	movq	8(%rbx), %rax
	movq	%r13, %rdi
	movzwl	(%rax), %esi
	call	_ZN6icu_6713UnicodeStringC1Ei@PLT
	movzwl	-104(%rbp), %esi
	testw	%si, %si
	js	.L1344
	movswl	%si, %ecx
	sarl	$5, %ecx
.L1345:
	movzwl	-168(%rbp), %eax
	testw	%ax, %ax
	js	.L1346
	movswl	%ax, %edx
	sarl	$5, %edx
.L1347:
	testb	$1, %sil
	je	.L1348
	notl	%eax
	andl	$1, %eax
	movl	%eax, %ebx
.L1349:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movsbl	%bl, %eax
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1356
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1348:
	.cfi_restore_state
	testl	%ecx, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	cmovle	%ecx, %r8d
	js	.L1350
	movl	%ecx, %r9d
	subl	%r8d, %r9d
	cmpl	%ecx, %r9d
	cmovg	%ecx, %r9d
.L1350:
	andl	$2, %esi
	leaq	-102(%rbp), %rcx
	cmove	-88(%rbp), %rcx
	subq	$8, %rsp
	xorl	%esi, %esi
	movq	%r12, %rdi
	pushq	$0
	call	_ZNK6icu_6713UnicodeString13doCaseCompareEiiPKDsiij@PLT
	movl	%eax, %ebx
	popq	%rax
	popq	%rdx
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1346:
	movl	-164(%rbp), %edx
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1344:
	movl	-100(%rbp), %ecx
	jmp	.L1345
.L1356:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9994:
	.size	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_, .-_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	.section	.text._ZN2v88internal17RegExpDisjunction20SortConsecutiveAtomsEPNS0_14RegExpCompilerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpDisjunction20SortConsecutiveAtomsEPNS0_14RegExpCompilerE
	.type	_ZN2v88internal17RegExpDisjunction20SortConsecutiveAtomsEPNS0_14RegExpCompilerE, @function
_ZN2v88internal17RegExpDisjunction20SortConsecutiveAtomsEPNS0_14RegExpCompilerE:
.LFB9995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	12(%r15), %eax
	movl	%eax, -68(%rbp)
	testl	%eax, %eax
	jle	.L1652
	movb	$0, -69(%rbp)
	xorl	%r14d, %r14d
	movq	%r15, -104(%rbp)
	movl	%r14d, %r15d
	.p2align 4,,10
	.p2align 3
.L1650:
	movl	-68(%rbp), %r14d
	movq	-104(%rbp), %r12
	movslq	%r15d, %rbx
	.p2align 4,,10
	.p2align 3
.L1360:
	movq	(%r12), %rax
	leaq	0(,%rbx,8), %r13
	movq	(%rax,%rbx,8), %rdi
	movq	(%rdi), %rax
	call	*160(%rax)
	movl	%r15d, %edx
	addl	$1, %r15d
	testb	%al, %al
	jne	.L1694
	addq	$1, %rbx
	cmpl	%r15d, %r14d
	jg	.L1360
	je	.L1357
	movl	%edx, %eax
	movslq	%r15d, %rbx
	movl	%r15d, -88(%rbp)
	addl	$2, %eax
	leaq	0(,%rbx,8), %r13
	movl	%eax, %r15d
.L1359:
	movq	-104(%rbp), %rax
	movslq	%r15d, %r12
	salq	$3, %r12
	movq	(%rax), %rax
	movq	(%rax,%r13), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movl	24(%rax), %eax
	movl	%eax, -80(%rbp)
	cmpl	%r15d, -68(%rbp)
	jle	.L1708
	movq	%rbx, -112(%rbp)
	movq	-104(%rbp), %rbx
	movq	%r13, -96(%rbp)
	jmp	.L1365
	.p2align 4,,10
	.p2align 3
.L1363:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*152(%rax)
	movl	-80(%rbp), %ecx
	cmpl	%ecx, 24(%rax)
	jne	.L1705
	addq	$8, %r12
	cmpl	%r15d, -68(%rbp)
	je	.L1709
.L1365:
	movq	(%rbx), %rax
	movl	%r15d, %r13d
	addl	$1, %r15d
	movq	(%rax,%r12), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*160(%rax)
	testb	%al, %al
	jne	.L1363
.L1705:
	movl	%r13d, %eax
	subl	-88(%rbp), %eax
	movq	-112(%rbp), %rbx
	movl	%eax, -112(%rbp)
	movq	-96(%rbp), %r13
	movslq	%eax, %rdx
.L1362:
	movq	-104(%rbp), %rax
	addq	%rdx, %rbx
	salq	$3, %rbx
	movq	(%rax), %rax
	leaq	(%rax,%rbx), %rsi
	subq	%r13, %rbx
	addq	%r13, %rax
	movq	%rbx, %r12
	movq	%rsi, -96(%rbp)
	movq	%rax, -88(%rbp)
	sarq	$3, %r12
	testb	$2, -80(%rbp)
	je	.L1366
	testq	%rbx, %rbx
	jle	.L1367
	movq	%r12, %r14
.L1368:
	leaq	0(,%r14,8), %rdi
	leaq	_ZSt7nothrow(%rip), %rsi
	call	_ZnwmRKSt9nothrow_t@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1370
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r8
.L1706:
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	call	_ZSt22__stable_sort_adaptiveIPPN2v88internal10RegExpTreeES4_lN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_T1_T2_
.L1509:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	movzbl	-69(%rbp), %esi
	cmpl	$2, -112(%rbp)
	movl	$1, %eax
	cmovge	%eax, %esi
	movb	%sil, -69(%rbp)
	cmpl	%r15d, -68(%rbp)
	jg	.L1650
.L1357:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1710
	movzbl	-69(%rbp), %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1694:
	.cfi_restore_state
	movl	%edx, -88(%rbp)
	jmp	.L1359
	.p2align 4,,10
	.p2align 3
.L1366:
	testq	%rbx, %rbx
	jle	.L1505
	movq	%r12, %r14
.L1506:
	leaq	0(,%r14,8), %rdi
	leaq	_ZSt7nothrow(%rip), %rsi
	call	_ZnwmRKSt9nothrow_t@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1508
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r8
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1709:
	movl	-68(%rbp), %eax
	movq	-112(%rbp), %rbx
	movq	-96(%rbp), %r13
	leal	1(%rax), %r15d
	subl	-88(%rbp), %eax
	movl	%eax, -112(%rbp)
	movslq	%eax, %rdx
	jmp	.L1362
.L1367:
	movq	-88(%rbp), %r14
	movq	-96(%rbp), %rax
	cmpq	%rax, %r14
	je	.L1512
	leaq	8(%r14), %rbx
	cmpq	%rax, %rbx
	je	.L1512
	movl	%r15d, -80(%rbp)
	leaq	-64(%rbp), %r13
	jmp	.L1381
	.p2align 4,,10
	.p2align 3
.L1711:
	movq	(%rbx), %r12
	cmpq	%r14, %rbx
	je	.L1377
	movq	%rbx, %rdx
	movl	$8, %eax
	movq	%r14, %rsi
	subq	%r14, %rdx
	leaq	(%r14,%rax), %rdi
	call	memmove@PLT
.L1377:
	movq	%r12, (%r14)
	addq	$8, %rbx
	cmpq	-96(%rbp), %rbx
	je	.L1707
.L1381:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	js	.L1711
	movq	(%rbx), %rax
	leaq	-8(%rbx), %r15
	movq	%rax, -64(%rbp)
	jmp	.L1380
	.p2align 4,,10
	.p2align 3
.L1712:
	movq	(%r15), %rax
	subq	$8, %r15
	movq	%rax, 16(%r15)
.L1380:
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	8(%r15), %r12
	call	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	js	.L1712
	movq	-64(%rbp), %rax
	addq	$8, %rbx
	movq	%rax, (%r12)
	cmpq	-96(%rbp), %rbx
	jne	.L1381
.L1707:
	movl	-80(%rbp), %r15d
.L1512:
	xorl	%r13d, %r13d
	jmp	.L1509
.L1505:
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rcx
	cmpq	%rcx, %rax
	je	.L1512
	leaq	8(%rax), %rbx
	cmpq	%rcx, %rbx
	je	.L1512
	movl	%r15d, -80(%rbp)
	jmp	.L1519
	.p2align 4,,10
	.p2align 3
.L1714:
	movq	%r15, (%r12)
.L1516:
	addq	$8, %rbx
	cmpq	-96(%rbp), %rbx
	je	.L1707
.L1519:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	%rax, %r12
	movq	-88(%rbp), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	8(%r12), %rdx
	movq	8(%rax), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, (%rdx)
	jb	.L1713
	movq	(%rbx), %r15
	movq	%rbx, %r14
	.p2align 4,,10
	.p2align 3
.L1518:
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r14, %r12
	subq	$8, %r14
	call	*152(%rax)
	movq	(%r14), %rdi
	movq	%rax, %r13
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	8(%r13), %rdx
	movq	8(%rax), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, (%rdx)
	jnb	.L1714
	movq	(%r14), %rax
	movq	%rax, 8(%r14)
	jmp	.L1518
	.p2align 4,,10
	.p2align 3
.L1713:
	movq	-88(%rbp), %rsi
	movq	(%rbx), %r12
	cmpq	%rsi, %rbx
	je	.L1515
	movq	%rbx, %rdx
	movl	$8, %eax
	subq	%rsi, %rdx
	leaq	(%rsi,%rax), %rdi
	call	memmove@PLT
.L1515:
	movq	-88(%rbp), %rax
	movq	%r12, (%rax)
	jmp	.L1516
.L1708:
	movl	$1, -112(%rbp)
	addl	$1, %r15d
	movl	$1, %edx
	jmp	.L1362
.L1652:
	movb	$0, -69(%rbp)
	jmp	.L1357
.L1370:
	sarq	%r14
	jne	.L1368
	cmpq	$112, %rbx
	jle	.L1367
	movq	-88(%rbp), %rcx
	sarq	%r12
	leaq	0(,%r12,8), %rax
	addq	%rax, %rcx
	movq	%rax, -120(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rax, %rcx
	sarq	$3, %rcx
	movq	%rcx, -144(%rbp)
	movq	%rcx, -128(%rbp)
	cmpq	$112, %rax
	jbe	.L1715
	sarq	$4, %rax
	movq	-88(%rbp), %r14
	salq	$3, %rax
	movq	%rax, %rcx
	movq	%rax, -136(%rbp)
	addq	%rax, %r14
	sarq	$3, %rcx
	movq	%rcx, -160(%rbp)
	cmpq	$112, %rax
	jbe	.L1716
	movq	%rax, %rbx
	movq	-88(%rbp), %rax
	sarq	$4, %rbx
	salq	$3, %rbx
	leaq	(%rax,%rbx), %r12
	movq	%rbx, %rax
	sarq	$3, %rax
	movq	%rax, -152(%rbp)
	cmpq	$112, %rbx
	jbe	.L1717
	movq	%rbx, %rax
	movq	-88(%rbp), %rcx
	sarq	$4, %rax
	salq	$3, %rax
	leaq	(%rcx,%rax), %r13
	movq	%rax, %rcx
	sarq	$3, %rcx
	movq	%rcx, -168(%rbp)
	cmpq	$112, %rax
	jbe	.L1718
	movq	%rax, %rcx
	movq	%rax, -200(%rbp)
	movq	-88(%rbp), %rax
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	sarq	$4, %rcx
	salq	$3, %rcx
	movq	%rax, %rdi
	leaq	(%rax,%rcx), %r10
	movq	%rcx, -192(%rbp)
	movq	%r10, %rsi
	movq	%r10, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-200(%rbp), %rax
	movq	-88(%rbp), %rdi
	movq	%r13, %rdx
	movq	-192(%rbp), %rcx
	movq	-184(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%rax, %r8
	movq	%rax, -176(%rbp)
	subq	%rcx, %r8
	movq	%r10, %rsi
	sarq	$3, %rcx
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-176(%rbp), %rax
.L1403:
	movq	%rbx, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	$112, %rdx
	jle	.L1719
	sarq	$4, %rdx
	movq	%r13, %rdi
	movq	%rax, -192(%rbp)
	leaq	0(,%rdx,8), %rcx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	leaq	0(%r13,%rcx), %r10
	movq	%rcx, -184(%rbp)
	movq	%r10, %rsi
	movq	%r10, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	%r12, %r8
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	-176(%rbp), %r10
	movq	-184(%rbp), %rcx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	subq	%r10, %r8
	sarq	$3, %rcx
	movq	%r10, %rsi
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-192(%rbp), %rax
.L1405:
	movq	-168(%rbp), %rcx
	movq	%rax, %r8
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	-88(%rbp), %rdi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1401:
	movq	-136(%rbp), %rax
	subq	%rbx, %rax
	movq	%rax, %rbx
	sarq	$3, %rbx
	cmpq	$112, %rax
	jle	.L1720
	sarq	$4, %rax
	salq	$3, %rax
	movq	%rax, %rcx
	leaq	(%r12,%rax), %r13
	sarq	$3, %rcx
	movq	%rcx, -168(%rbp)
	cmpq	$112, %rax
	jbe	.L1721
	movq	%rax, %rcx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	sarq	$4, %rcx
	salq	$3, %rcx
	leaq	(%r12,%rcx), %r10
	movq	%rcx, -184(%rbp)
	movq	%r10, %rsi
	movq	%r10, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-184(%rbp), %rcx
	movq	%r13, %rdx
	movq	-192(%rbp), %rax
	movq	-176(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%r12, %rdi
	subq	%rcx, %rax
	sarq	$3, %rcx
	sarq	$3, %rax
	movq	%r10, %rsi
	movq	%rax, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1409:
	movq	%r14, %rdx
	subq	%r13, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	$112, %rdx
	jle	.L1722
	sarq	$4, %rdx
	movq	%r13, %rdi
	movq	%rax, -192(%rbp)
	leaq	0(,%rdx,8), %rcx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	leaq	0(%r13,%rcx), %r10
	movq	%rcx, -184(%rbp)
	movq	%r10, %rsi
	movq	%r10, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r14, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	%r14, %r8
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	-176(%rbp), %r10
	movq	-184(%rbp), %rcx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	subq	%r10, %r8
	sarq	$3, %rcx
	movq	%r10, %rsi
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-192(%rbp), %rax
.L1411:
	movq	-168(%rbp), %rcx
	movq	%rax, %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%r12, %rdi
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1407:
	movq	-152(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	-88(%rbp), %rdi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1393:
	movq	-120(%rbp), %rbx
	subq	-136(%rbp), %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	movq	%rax, -120(%rbp)
	cmpq	$112, %rbx
	jle	.L1723
	sarq	$4, %rbx
	salq	$3, %rbx
	movq	%rbx, %rax
	leaq	(%r14,%rbx), %r12
	sarq	$3, %rax
	movq	%rax, -136(%rbp)
	cmpq	$112, %rbx
	jbe	.L1724
	movq	%rbx, %rax
	sarq	$4, %rax
	salq	$3, %rax
	movq	%rax, %rcx
	leaq	(%r14,%rax), %r13
	sarq	$3, %rcx
	movq	%rcx, -152(%rbp)
	cmpq	$112, %rax
	jbe	.L1725
	movq	%rax, %rcx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r14, %rdi
	movq	%rax, -192(%rbp)
	sarq	$4, %rcx
	salq	$3, %rcx
	leaq	(%r14,%rcx), %r10
	movq	%rcx, -184(%rbp)
	movq	%r10, %rsi
	movq	%r10, -168(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-168(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-192(%rbp), %rax
	movq	%r13, %rdx
	movq	-184(%rbp), %rcx
	movq	-176(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%r14, %rdi
	movq	%rax, %r8
	movq	%rax, -168(%rbp)
	subq	%rcx, %r8
	movq	%r10, %rsi
	sarq	$3, %rcx
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-168(%rbp), %rax
.L1425:
	subq	%rax, %rbx
	movq	%rbx, %rax
	sarq	$3, %rbx
	cmpq	$112, %rax
	jle	.L1726
	sarq	$4, %rax
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rdi
	salq	$3, %rax
	leaq	0(%r13,%rax), %r10
	movq	%rax, -176(%rbp)
	movq	%r10, %rsi
	movq	%r10, -168(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-168(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	%r12, %r8
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	-168(%rbp), %r10
	movq	-176(%rbp), %rax
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	subq	%r10, %r8
	sarq	$3, %rax
	movq	%r10, %rsi
	movq	%rax, %rcx
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1427:
	movq	-152(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r12, %rdx
	movq	%r13, %rsi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%r14, %rdi
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1423:
	movq	-80(%rbp), %rax
	subq	%r12, %rax
	movq	%rax, %rbx
	sarq	$3, %rbx
	cmpq	$112, %rax
	jle	.L1727
	sarq	$4, %rax
	salq	$3, %rax
	movq	%rax, %rcx
	leaq	(%r12,%rax), %r13
	sarq	$3, %rcx
	movq	%rcx, -152(%rbp)
	cmpq	$112, %rax
	jbe	.L1728
	movq	%rax, %rcx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	sarq	$4, %rcx
	salq	$3, %rcx
	leaq	(%r12,%rcx), %r10
	movq	%rcx, -176(%rbp)
	movq	%r10, %rsi
	movq	%r10, -168(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-168(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %rcx
	movq	%r13, %rdx
	movq	-184(%rbp), %rax
	movq	-168(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%r12, %rdi
	subq	%rcx, %rax
	sarq	$3, %rcx
	sarq	$3, %rax
	movq	%r10, %rsi
	movq	%rax, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1431:
	movq	-80(%rbp), %rdx
	subq	%r13, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	$112, %rdx
	jle	.L1729
	sarq	$4, %rdx
	movq	%r13, %rdi
	movq	%rax, -184(%rbp)
	leaq	0(,%rdx,8), %rcx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	leaq	0(%r13,%rcx), %r10
	movq	%rcx, -176(%rbp)
	movq	%r10, %rsi
	movq	%r10, -168(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-168(%rbp), %r10
	movq	-80(%rbp), %rsi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-80(%rbp), %rdx
	movq	%r13, %rdi
	movq	-168(%rbp), %r10
	movq	-176(%rbp), %rcx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%rdx, %r8
	movq	%r10, %rsi
	subq	%r10, %r8
	sarq	$3, %rcx
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-184(%rbp), %rax
.L1433:
	movq	-152(%rbp), %rcx
	movq	%rax, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-80(%rbp), %rdx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1429:
	movq	-136(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-80(%rbp), %rdx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1414:
	movq	-120(%rbp), %r8
	movq	-160(%rbp), %rcx
	movq	%r14, %rsi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rdi
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1384:
	movq	-96(%rbp), %rax
	subq	-80(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -136(%rbp)
	sarq	$3, %rsi
	movq	%rsi, -152(%rbp)
	movq	%rsi, -120(%rbp)
	cmpq	$112, %rax
	jle	.L1730
	movq	%rax, %r14
	movq	-80(%rbp), %rax
	sarq	$4, %r14
	leaq	0(,%r14,8), %rbx
	leaq	(%rax,%rbx), %r14
	movq	%rbx, %rax
	sarq	$3, %rax
	movq	%rax, -160(%rbp)
	cmpq	$112, %rbx
	jbe	.L1731
	movq	%rbx, %rax
	sarq	$4, %rax
	leaq	0(,%rax,8), %r13
	movq	-80(%rbp), %rax
	leaq	(%rax,%r13), %r12
	movq	%r13, %rax
	sarq	$3, %rax
	movq	%rax, -136(%rbp)
	cmpq	$112, %r13
	jbe	.L1732
	movq	%r13, %r11
	movq	-80(%rbp), %rax
	sarq	$4, %r11
	salq	$3, %r11
	leaq	(%rax,%r11), %r10
	movq	%r11, %rax
	sarq	$3, %rax
	movq	%rax, -168(%rbp)
	cmpq	$112, %r11
	jbe	.L1733
	movq	%r11, %rcx
	movq	-80(%rbp), %rdi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r11, -208(%rbp)
	sarq	$4, %rcx
	movq	%r10, -176(%rbp)
	salq	$3, %rcx
	leaq	(%rdi,%rcx), %rax
	movq	%rcx, -200(%rbp)
	movq	%rax, %rsi
	movq	%rax, -184(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-184(%rbp), %rax
	movq	-176(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r10, %rsi
	movq	%rax, %rdi
	movq	%rax, -192(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-208(%rbp), %r11
	movq	-80(%rbp), %rdi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	-200(%rbp), %rcx
	movq	-176(%rbp), %r10
	movq	%r11, %r8
	movq	-192(%rbp), %rax
	movq	%r11, -184(%rbp)
	subq	%rcx, %r8
	movq	%r10, %rdx
	sarq	$3, %rcx
	sarq	$3, %r8
	movq	%rax, %rsi
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-184(%rbp), %r11
	movq	-176(%rbp), %r10
.L1458:
	movq	%r13, %rdx
	subq	%r11, %rdx
	movq	%rdx, %r11
	sarq	$3, %r11
	cmpq	$112, %rdx
	jle	.L1734
	movq	%rdx, %rcx
	movq	%r10, %rdi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r11, -200(%rbp)
	sarq	$4, %rcx
	movq	%r10, -184(%rbp)
	salq	$3, %rcx
	leaq	(%r10,%rcx), %rax
	movq	%rcx, -192(%rbp)
	movq	%rax, %rsi
	movq	%rax, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %rax
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %rax
	movq	%r12, %r8
	movq	%r12, %rdx
	movq	-184(%rbp), %r10
	movq	-192(%rbp), %rcx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	subq	%rax, %r8
	movq	%rax, %rsi
	movq	%r10, %rdi
	sarq	$3, %rcx
	movq	%r10, -176(%rbp)
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-200(%rbp), %r11
	movq	-176(%rbp), %r10
.L1460:
	movq	-168(%rbp), %rcx
	movq	%r11, %r8
	movq	%r12, %rdx
	movq	%r10, %rsi
	movq	-80(%rbp), %rdi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1456:
	subq	%r13, %rbx
	movq	%rbx, %r13
	sarq	$3, %r13
	cmpq	$112, %rbx
	jle	.L1735
	sarq	$4, %rbx
	leaq	0(,%rbx,8), %rax
	movq	%rax, %rcx
	leaq	(%r12,%rax), %rbx
	sarq	$3, %rcx
	movq	%rcx, -168(%rbp)
	cmpq	$112, %rax
	jbe	.L1736
	movq	%rax, %rcx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	sarq	$4, %rcx
	salq	$3, %rcx
	leaq	(%r12,%rcx), %r10
	movq	%rcx, -184(%rbp)
	movq	%r10, %rsi
	movq	%r10, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-184(%rbp), %rcx
	movq	%rbx, %rdx
	movq	-192(%rbp), %rax
	movq	-176(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%r12, %rdi
	subq	%rcx, %rax
	sarq	$3, %rcx
	sarq	$3, %rax
	movq	%r10, %rsi
	movq	%rax, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1464:
	movq	%r14, %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	$112, %rdx
	jle	.L1737
	sarq	$4, %rdx
	movq	%rbx, %rdi
	movq	%rax, -192(%rbp)
	leaq	0(,%rdx,8), %rcx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	leaq	(%rbx,%rcx), %r10
	movq	%rcx, -184(%rbp)
	movq	%r10, %rsi
	movq	%r10, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r14, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	%r14, %r8
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movq	-176(%rbp), %r10
	movq	-184(%rbp), %rcx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	subq	%r10, %r8
	sarq	$3, %rcx
	movq	%r10, %rsi
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-192(%rbp), %rax
.L1466:
	movq	-168(%rbp), %rcx
	movq	%rax, %r8
	movq	%r14, %rdx
	movq	%rbx, %rsi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%r12, %rdi
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1462:
	movq	-136(%rbp), %rcx
	movq	%r13, %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	-80(%rbp), %rdi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1448:
	movq	-96(%rbp), %rbx
	subq	%r14, %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	movq	%rax, -136(%rbp)
	cmpq	$112, %rbx
	jle	.L1738
	sarq	$4, %rbx
	salq	$3, %rbx
	movq	%rbx, %rax
	leaq	(%r14,%rbx), %r12
	sarq	$3, %rax
	movq	%rax, -168(%rbp)
	cmpq	$112, %rbx
	jbe	.L1739
	movq	%rbx, %rax
	sarq	$4, %rax
	salq	$3, %rax
	movq	%rax, %rcx
	leaq	(%r14,%rax), %r13
	sarq	$3, %rcx
	movq	%rcx, -176(%rbp)
	cmpq	$112, %rax
	jbe	.L1740
	movq	%rax, %rcx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r14, %rdi
	movq	%rax, -208(%rbp)
	sarq	$4, %rcx
	salq	$3, %rcx
	leaq	(%r14,%rcx), %r10
	movq	%rcx, -200(%rbp)
	movq	%r10, %rsi
	movq	%r10, -184(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-184(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -192(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-208(%rbp), %rax
	movq	%r13, %rdx
	movq	-200(%rbp), %rcx
	movq	-192(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%r14, %rdi
	movq	%rax, %r8
	movq	%rax, -184(%rbp)
	subq	%rcx, %r8
	movq	%r10, %rsi
	sarq	$3, %rcx
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-184(%rbp), %rax
.L1480:
	subq	%rax, %rbx
	movq	%rbx, %rax
	sarq	$3, %rbx
	cmpq	$112, %rax
	jle	.L1741
	sarq	$4, %rax
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rdi
	salq	$3, %rax
	leaq	0(%r13,%rax), %r10
	movq	%rax, -192(%rbp)
	movq	%r10, %rsi
	movq	%r10, -184(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-184(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	%r12, %r8
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	-184(%rbp), %r10
	movq	-192(%rbp), %rax
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	subq	%r10, %r8
	sarq	$3, %rax
	movq	%r10, %rsi
	movq	%rax, %rcx
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1482:
	movq	-176(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r12, %rdx
	movq	%r13, %rsi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%r14, %rdi
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1478:
	movq	-96(%rbp), %rax
	subq	%r12, %rax
	movq	%rax, %rbx
	sarq	$3, %rbx
	cmpq	$112, %rax
	jle	.L1742
	sarq	$4, %rax
	salq	$3, %rax
	movq	%rax, %rcx
	leaq	(%r12,%rax), %r13
	sarq	$3, %rcx
	movq	%rcx, -176(%rbp)
	cmpq	$112, %rax
	jbe	.L1743
	movq	%rax, %rcx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	sarq	$4, %rcx
	salq	$3, %rcx
	leaq	(%r12,%rcx), %r10
	movq	%rcx, -192(%rbp)
	movq	%r10, %rsi
	movq	%r10, -184(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-184(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-192(%rbp), %rcx
	movq	%r13, %rdx
	movq	-200(%rbp), %rax
	movq	-184(%rbp), %r10
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%r12, %rdi
	subq	%rcx, %rax
	sarq	$3, %rcx
	sarq	$3, %rax
	movq	%r10, %rsi
	movq	%rax, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1486:
	movq	-96(%rbp), %rdx
	subq	%r13, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	$112, %rdx
	jle	.L1744
	sarq	$4, %rdx
	movq	%r13, %rdi
	movq	%rax, -200(%rbp)
	leaq	0(,%rdx,8), %rcx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	leaq	0(%r13,%rcx), %r10
	movq	%rcx, -192(%rbp)
	movq	%r10, %rsi
	movq	%r10, -184(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-184(%rbp), %r10
	movq	-96(%rbp), %rsi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movq	-184(%rbp), %r10
	movq	-192(%rbp), %rcx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%rdx, %r8
	movq	%r10, %rsi
	subq	%r10, %r8
	sarq	$3, %rcx
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-200(%rbp), %rax
.L1488:
	movq	-176(%rbp), %rcx
	movq	%rax, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-96(%rbp), %rdx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1484:
	movq	-168(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-96(%rbp), %rdx
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1469:
	movq	-136(%rbp), %r8
	movq	-160(%rbp), %rcx
	movq	%r14, %rsi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	-96(%rbp), %rdx
	movq	-80(%rbp), %rdi
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1437:
	movq	-152(%rbp), %rax
	addq	-144(%rbp), %rax
	cmpq	$2, %rax
	je	.L1745
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %rax
	movl	%r15d, -160(%rbp)
	cmpq	%rax, %rcx
	jle	.L1491
.L1747:
	movq	%rcx, %rax
	movq	-80(%rbp), %rbx
	shrq	$63, %rax
	addq	%rcx, %rax
	movq	-88(%rbp), %rcx
	sarq	%rax
	movq	%rax, -152(%rbp)
	leaq	(%rcx,%rax,8), %r12
	movq	%rax, -136(%rbp)
	movq	-96(%rbp), %rax
	subq	%rbx, %rax
	movq	%rax, %r14
	sarq	$3, %r14
	testq	%rax, %rax
	jg	.L1493
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1495:
	testq	%r13, %r13
	je	.L1494
.L1496:
	movq	%r13, %r14
.L1493:
	movq	%r14, %r13
	movq	%r12, %rsi
	sarq	%r13
	leaq	(%rbx,%r13,8), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	jns	.L1495
	subq	%r13, %r14
	leaq	8(%r15), %rbx
	leaq	-1(%r14), %r13
	testq	%r13, %r13
	jg	.L1496
.L1494:
	movq	%rbx, %r15
	subq	-80(%rbp), %r15
	movq	%r15, %rax
	sarq	$3, %rax
	subq	%rax, -120(%rbp)
	movq	%rax, -144(%rbp)
.L1492:
	movq	-152(%rbp), %rcx
	subq	%rcx, -128(%rbp)
.L1497:
	movq	-80(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZNSt3_V28__rotateIPPN2v88internal10RegExpTreeEEET_S6_S6_S6_St26random_access_iterator_tag
	movq	-144(%rbp), %r8
	movq	-88(%rbp), %rdi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	-136(%rbp), %rcx
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%rax, %r13
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	cmpq	$0, -128(%rbp)
	je	.L1703
	cmpq	$0, -120(%rbp)
	je	.L1703
	movq	-128(%rbp), %rax
	addq	-120(%rbp), %rax
	cmpq	$2, %rax
	je	.L1746
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %rax
	movq	%r13, -88(%rbp)
	movq	%rbx, -80(%rbp)
	cmpq	%rax, %rcx
	jg	.L1747
.L1491:
	movq	%rax, %rcx
	shrq	$63, %rax
	movq	-88(%rbp), %r12
	addq	%rcx, %rax
	movq	-80(%rbp), %rcx
	sarq	%rax
	movq	%rax, -152(%rbp)
	leaq	(%rcx,%rax,8), %rbx
	movq	%rax, -144(%rbp)
	movq	%rcx, %rax
	subq	%r12, %rax
	movq	%rax, %r14
	sarq	$3, %r14
	testq	%rax, %rax
	jle	.L1656
	.p2align 4,,10
	.p2align 3
.L1499:
	movq	%r14, %r13
	movq	%rbx, %rdi
	sarq	%r13
	leaq	(%r12,%r13,8), %r15
	movq	%r15, %rsi
	call	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	js	.L1501
	subq	%r13, %r14
	leaq	8(%r15), %r12
	subq	$1, %r14
	testq	%r14, %r14
	jg	.L1499
.L1500:
	movq	%r12, %r14
	subq	-88(%rbp), %r14
	movq	%r14, %rax
	sarq	$3, %rax
	subq	%rax, -128(%rbp)
	movq	%rax, -136(%rbp)
.L1498:
	movq	-152(%rbp), %rcx
	subq	%rcx, -120(%rbp)
	jmp	.L1497
	.p2align 4,,10
	.p2align 3
.L1501:
	testq	%r13, %r13
	je	.L1500
	movq	%r13, %r14
	jmp	.L1499
.L1703:
	movl	-160(%rbp), %r15d
	xorl	%r13d, %r13d
	jmp	.L1509
.L1746:
	movl	-160(%rbp), %r15d
.L1489:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	jns	.L1512
	movq	0(%r13), %rax
	movq	(%rbx), %rdx
	movq	%rdx, 0(%r13)
	xorl	%r13d, %r13d
	movq	%rax, (%rbx)
	jmp	.L1509
.L1715:
	movq	-88(%rbp), %r14
	leaq	-64(%rbp), %r13
	leaq	8(%r14), %rbx
	cmpq	%rbx, -80(%rbp)
	je	.L1384
	movl	%r15d, -120(%rbp)
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1749:
	movq	(%rbx), %r12
	cmpq	%r14, %rbx
	je	.L1386
	movq	%rbx, %rdx
	movl	$8, %eax
	movq	%r14, %rsi
	subq	%r14, %rdx
	leaq	(%r14,%rax), %rdi
	call	memmove@PLT
.L1386:
	movq	%r12, (%r14)
	addq	$8, %rbx
	cmpq	%rbx, -80(%rbp)
	je	.L1748
.L1383:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	js	.L1749
	movq	(%rbx), %rax
	leaq	-8(%rbx), %r15
	movq	%rax, -64(%rbp)
	jmp	.L1389
	.p2align 4,,10
	.p2align 3
.L1750:
	movq	(%r15), %rax
	subq	$8, %r15
	movq	%rax, 16(%r15)
.L1389:
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	8(%r15), %r12
	call	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	js	.L1750
	movq	-64(%rbp), %rax
	addq	$8, %rbx
	movq	%rax, (%r12)
	cmpq	%rbx, -80(%rbp)
	jne	.L1383
.L1748:
	movl	-120(%rbp), %r15d
	jmp	.L1384
.L1730:
	movq	-96(%rbp), %rcx
	cmpq	%rcx, -80(%rbp)
	je	.L1438
	movq	-80(%rbp), %r14
	leaq	8(%r14), %rbx
	cmpq	-96(%rbp), %rbx
	je	.L1438
	movl	%r15d, -160(%rbp)
	leaq	-64(%rbp), %r13
	jmp	.L1444
	.p2align 4,,10
	.p2align 3
.L1752:
	movq	(%rbx), %r12
	cmpq	%rbx, %r14
	je	.L1440
	movq	%rbx, %rdx
	movl	$8, %eax
	movq	%r14, %rsi
	subq	%r14, %rdx
	leaq	(%r14,%rax), %rdi
	call	memmove@PLT
.L1440:
	movq	%r12, (%r14)
	addq	$8, %rbx
	cmpq	-96(%rbp), %rbx
	je	.L1751
.L1444:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	js	.L1752
	movq	(%rbx), %rax
	leaq	-8(%rbx), %r15
	movq	%rax, -64(%rbp)
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1753:
	movq	(%r15), %rax
	subq	$8, %r15
	movq	%rax, 16(%r15)
.L1443:
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	8(%r15), %r12
	call	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	js	.L1753
	movq	-64(%rbp), %rax
	addq	$8, %rbx
	movq	%rax, (%r12)
	cmpq	-96(%rbp), %rbx
	jne	.L1444
.L1751:
	movl	-160(%rbp), %r15d
.L1438:
	cmpq	$0, -136(%rbp)
	jne	.L1437
	jmp	.L1512
.L1731:
	movq	-80(%rbp), %rax
	leaq	-64(%rbp), %r13
	leaq	8(%rax), %rbx
	cmpq	%rbx, %r14
	je	.L1448
	movl	%r15d, -168(%rbp)
	movq	%rax, %r15
	movq	%r14, -136(%rbp)
	jmp	.L1447
.L1755:
	movq	(%rbx), %r12
	cmpq	%rbx, %r15
	je	.L1450
	movq	%rbx, %rdx
	movl	$8, %eax
	movq	%r15, %rsi
	subq	%r15, %rdx
	leaq	(%r15,%rax), %rdi
	call	memmove@PLT
.L1450:
	movq	%r12, (%r15)
	addq	$8, %rbx
	cmpq	%rbx, -136(%rbp)
	je	.L1754
.L1447:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	js	.L1755
	movq	(%rbx), %rax
	leaq	-8(%rbx), %r14
	movq	%rax, -64(%rbp)
	jmp	.L1453
	.p2align 4,,10
	.p2align 3
.L1756:
	movq	(%r14), %rax
	subq	$8, %r14
	movq	%rax, 16(%r14)
.L1453:
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	8(%r14), %r12
	call	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	js	.L1756
	movq	-64(%rbp), %rax
	addq	$8, %rbx
	movq	%rax, (%r12)
	cmpq	%rbx, -136(%rbp)
	jne	.L1447
.L1754:
	movq	-136(%rbp), %r14
	movl	-168(%rbp), %r15d
	jmp	.L1448
.L1738:
	movq	-96(%rbp), %rax
	cmpq	%rax, %r14
	je	.L1469
	leaq	8(%r14), %rbx
	cmpq	%rax, %rbx
	je	.L1469
	movl	%r15d, -168(%rbp)
	leaq	-64(%rbp), %r13
	jmp	.L1476
.L1758:
	movq	(%rbx), %r12
	cmpq	%rbx, %r14
	je	.L1472
	movq	%rbx, %rdx
	movl	$8, %eax
	movq	%r14, %rsi
	subq	%r14, %rdx
	leaq	(%r14,%rax), %rdi
	call	memmove@PLT
.L1472:
	movq	%r12, (%r14)
	addq	$8, %rbx
	cmpq	-96(%rbp), %rbx
	je	.L1757
.L1476:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	js	.L1758
	movq	(%rbx), %rax
	leaq	-8(%rbx), %r15
	movq	%rax, -64(%rbp)
	jmp	.L1475
	.p2align 4,,10
	.p2align 3
.L1759:
	movq	(%r15), %rax
	subq	$8, %r15
	movq	%rax, 16(%r15)
.L1475:
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	8(%r15), %r12
	call	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	js	.L1759
	movq	-64(%rbp), %rax
	addq	$8, %rbx
	movq	%rax, (%r12)
	cmpq	-96(%rbp), %rbx
	jne	.L1476
.L1757:
	movl	-168(%rbp), %r15d
	jmp	.L1469
.L1716:
	movq	-88(%rbp), %rax
	leaq	-64(%rbp), %r13
	leaq	8(%rax), %rbx
	cmpq	%rbx, %r14
	je	.L1393
	movl	%r15d, -168(%rbp)
	movq	%rax, %r15
	movq	%r14, -152(%rbp)
	jmp	.L1392
.L1761:
	movq	(%rbx), %r12
	cmpq	%r15, %rbx
	je	.L1395
	movq	%rbx, %rdx
	movl	$8, %eax
	movq	%r15, %rsi
	subq	%r15, %rdx
	leaq	(%r15,%rax), %rdi
	call	memmove@PLT
.L1395:
	movq	%r12, (%r15)
	addq	$8, %rbx
	cmpq	%rbx, -152(%rbp)
	je	.L1760
.L1392:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	js	.L1761
	movq	(%rbx), %rax
	leaq	-8(%rbx), %r14
	movq	%rax, -64(%rbp)
	jmp	.L1398
	.p2align 4,,10
	.p2align 3
.L1762:
	movq	(%r14), %rax
	subq	$8, %r14
	movq	%rax, 16(%r14)
.L1398:
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	8(%r14), %r12
	call	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	js	.L1762
	movq	-64(%rbp), %rax
	addq	$8, %rbx
	movq	%rax, (%r12)
	cmpq	%rbx, -152(%rbp)
	jne	.L1392
.L1760:
	movq	-152(%rbp), %r14
	movl	-168(%rbp), %r15d
	jmp	.L1393
.L1723:
	movq	-80(%rbp), %rax
	cmpq	%r14, %rax
	je	.L1414
	leaq	8(%r14), %rbx
	cmpq	%rbx, %rax
	je	.L1414
	movl	%r15d, -136(%rbp)
	leaq	-64(%rbp), %r13
	jmp	.L1421
.L1764:
	movq	(%rbx), %r12
	cmpq	%rbx, %r14
	je	.L1417
	movq	%rbx, %rdx
	movl	$8, %eax
	movq	%r14, %rsi
	subq	%r14, %rdx
	leaq	(%r14,%rax), %rdi
	call	memmove@PLT
.L1417:
	movq	%r12, (%r14)
	addq	$8, %rbx
	cmpq	%rbx, -80(%rbp)
	je	.L1763
.L1421:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	js	.L1764
	movq	(%rbx), %rax
	leaq	-8(%rbx), %r15
	movq	%rax, -64(%rbp)
	jmp	.L1420
	.p2align 4,,10
	.p2align 3
.L1765:
	movq	(%r15), %rax
	subq	$8, %r15
	movq	%rax, 16(%r15)
.L1420:
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	8(%r15), %r12
	call	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	js	.L1765
	movq	-64(%rbp), %rax
	addq	$8, %rbx
	movq	%rax, (%r12)
	cmpq	%rbx, -80(%rbp)
	jne	.L1421
.L1763:
	movl	-136(%rbp), %r15d
	jmp	.L1414
.L1656:
	movq	$0, -136(%rbp)
	jmp	.L1498
.L1655:
	movq	$0, -144(%rbp)
	jmp	.L1492
.L1739:
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1478
.L1717:
	movq	-88(%rbp), %rdi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1401
.L1720:
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1407
.L1727:
	movq	-80(%rbp), %rsi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1429
.L1724:
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1423
.L1742:
	movq	-96(%rbp), %rsi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1484
.L1735:
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1462
.L1732:
	movq	-80(%rbp), %rdi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1456
.L1733:
	movq	-80(%rbp), %rdi
	movq	%r10, %rsi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r11, -184(%rbp)
	movq	%r10, -176(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %r10
	movq	-184(%rbp), %r11
	jmp	.L1458
.L1743:
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1486
.L1734:
	movq	%r10, %rdi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%r11, -184(%rbp)
	movq	%r10, -176(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %r10
	movq	-184(%rbp), %r11
	jmp	.L1460
.L1722:
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -176(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %rax
	jmp	.L1411
.L1721:
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1409
.L1737:
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rax, -176(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %rax
	jmp	.L1466
.L1736:
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1464
.L1745:
	movq	-88(%rbp), %r13
	movq	-80(%rbp), %rbx
	jmp	.L1489
.L1744:
	movq	-96(%rbp), %rsi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rdi
	movq	%rax, -184(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-184(%rbp), %rax
	jmp	.L1488
.L1729:
	movq	-80(%rbp), %rsi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rdi
	movq	%rax, -168(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-168(%rbp), %rax
	jmp	.L1433
.L1728:
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1431
.L1726:
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1427
.L1725:
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -168(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-168(%rbp), %rax
	jmp	.L1425
.L1741:
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1482
.L1740:
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -184(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-184(%rbp), %rax
	jmp	.L1480
.L1719:
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -176(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %rax
	jmp	.L1405
.L1718:
	movq	-88(%rbp), %rdi
	leaq	_ZN2v88internal30CompareFirstCharCaseInsensitveEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rsi
	movq	%rax, -176(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %rax
	jmp	.L1403
.L1508:
	sarq	%r14
	jne	.L1506
	cmpq	$112, %rbx
	jle	.L1505
	movq	-88(%rbp), %rcx
	sarq	%r12
	leaq	0(,%r12,8), %rax
	addq	%rax, %rcx
	movq	%rax, -120(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rax, %rcx
	sarq	$3, %rcx
	movq	%rcx, -144(%rbp)
	movq	%rcx, -128(%rbp)
	cmpq	$112, %rax
	jbe	.L1766
	sarq	$4, %rax
	movq	-88(%rbp), %r13
	salq	$3, %rax
	movq	%rax, %rdx
	movq	%rax, -136(%rbp)
	addq	%rax, %r13
	sarq	$3, %rdx
	movq	%rdx, -160(%rbp)
	cmpq	$112, %rax
	jbe	.L1767
	movq	%rax, %rbx
	movq	-88(%rbp), %rax
	sarq	$4, %rbx
	salq	$3, %rbx
	leaq	(%rax,%rbx), %r12
	movq	%rbx, %rax
	sarq	$3, %rax
	movq	%rax, -152(%rbp)
	cmpq	$112, %rbx
	jbe	.L1768
	movq	%rbx, %rax
	movq	-88(%rbp), %rcx
	sarq	$4, %rax
	salq	$3, %rax
	leaq	(%rcx,%rax), %r11
	movq	%rax, %rcx
	sarq	$3, %rcx
	movq	%rcx, -168(%rbp)
	cmpq	$112, %rax
	jbe	.L1769
	movq	%rax, %rcx
	movq	-88(%rbp), %r14
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%rax, -208(%rbp)
	sarq	$4, %rcx
	movq	%r11, -176(%rbp)
	salq	$3, %rcx
	movq	%r14, %rdi
	leaq	(%r14,%rcx), %r10
	movq	%rcx, -200(%rbp)
	movq	%r10, %rsi
	movq	%r10, -184(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-184(%rbp), %r10
	movq	-176(%rbp), %r11
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r11, %rsi
	movq	%r10, %rdi
	movq	%r10, -192(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-208(%rbp), %rax
	movq	%r14, %rdi
	movq	-200(%rbp), %rcx
	movq	-176(%rbp), %r11
	movq	-192(%rbp), %r10
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%rax, %r8
	movq	%rax, -184(%rbp)
	subq	%rcx, %r8
	movq	%r11, %rdx
	sarq	$3, %rcx
	movq	%r10, %rsi
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-184(%rbp), %rax
	movq	-176(%rbp), %r11
.L1541:
	movq	%rbx, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	$112, %rdx
	jle	.L1770
	sarq	$4, %rdx
	movq	%r11, %rdi
	movq	%rax, -200(%rbp)
	leaq	0(,%rdx,8), %rcx
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r11, -184(%rbp)
	leaq	(%r11,%rcx), %r10
	movq	%rcx, -192(%rbp)
	movq	%r10, %rsi
	movq	%r10, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %r10
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %r10
	movq	%r12, %r8
	movq	%r12, %rdx
	movq	-184(%rbp), %r11
	movq	-192(%rbp), %rcx
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	subq	%r10, %r8
	movq	%r10, %rsi
	movq	%r11, %rdi
	sarq	$3, %rcx
	movq	%r11, -176(%rbp)
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-200(%rbp), %rax
	movq	-176(%rbp), %r11
.L1543:
	movq	-168(%rbp), %rcx
	movq	%rax, %r8
	movq	%r12, %rdx
	movq	%r11, %rsi
	movq	-88(%rbp), %rdi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1539:
	movq	-136(%rbp), %rax
	subq	%rbx, %rax
	movq	%rax, %r14
	sarq	$3, %r14
	cmpq	$112, %rax
	jle	.L1771
	sarq	$4, %rax
	salq	$3, %rax
	movq	%rax, %rcx
	leaq	(%r12,%rax), %rbx
	sarq	$3, %rcx
	movq	%rcx, -168(%rbp)
	cmpq	$112, %rax
	jbe	.L1772
	movq	%rax, %rcx
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	sarq	$4, %rcx
	salq	$3, %rcx
	leaq	(%r12,%rcx), %r11
	movq	%rcx, -184(%rbp)
	movq	%r11, %rsi
	movq	%r11, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %r11
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r11, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-184(%rbp), %rcx
	movq	%rbx, %rdx
	movq	-192(%rbp), %rax
	movq	-176(%rbp), %r11
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%r12, %rdi
	subq	%rcx, %rax
	sarq	$3, %rcx
	sarq	$3, %rax
	movq	%r11, %rsi
	movq	%rax, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1547:
	movq	%r13, %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	$112, %rdx
	jle	.L1773
	sarq	$4, %rdx
	movq	%rbx, %rdi
	movq	%rax, -192(%rbp)
	leaq	0(,%rdx,8), %rcx
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	leaq	(%rbx,%rcx), %r11
	movq	%rcx, -184(%rbp)
	movq	%r11, %rsi
	movq	%r11, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %r11
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rsi
	movq	%r11, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	%r13, %r8
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	-176(%rbp), %r11
	movq	-184(%rbp), %rcx
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	subq	%r11, %r8
	sarq	$3, %rcx
	movq	%r11, %rsi
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-192(%rbp), %rax
.L1549:
	movq	-168(%rbp), %rcx
	movq	%rax, %r8
	movq	%r13, %rdx
	movq	%rbx, %rsi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%r12, %rdi
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1545:
	movq	-152(%rbp), %rcx
	movq	%r14, %r8
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	-88(%rbp), %rdi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1531:
	movq	-120(%rbp), %rbx
	subq	-136(%rbp), %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	movq	%rax, -136(%rbp)
	cmpq	$112, %rbx
	jle	.L1774
	sarq	$4, %rbx
	salq	$3, %rbx
	movq	%rbx, %rax
	leaq	0(%r13,%rbx), %r12
	sarq	$3, %rax
	movq	%rax, -120(%rbp)
	cmpq	$112, %rbx
	jbe	.L1775
	movq	%rbx, %rax
	sarq	$4, %rax
	salq	$3, %rax
	movq	%rax, %rcx
	leaq	0(%r13,%rax), %r11
	sarq	$3, %rcx
	movq	%rcx, %r14
	cmpq	$112, %rax
	jbe	.L1776
	movq	%rax, %rcx
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rdi
	movq	%rax, -192(%rbp)
	sarq	$4, %rcx
	movq	%r11, -152(%rbp)
	salq	$3, %rcx
	leaq	0(%r13,%rcx), %r10
	movq	%rcx, -184(%rbp)
	movq	%r10, %rsi
	movq	%r10, -168(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-168(%rbp), %r10
	movq	-152(%rbp), %r11
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r11, %rsi
	movq	%r10, %rdi
	movq	%r10, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-192(%rbp), %rax
	movq	%r13, %rdi
	movq	-184(%rbp), %rcx
	movq	-152(%rbp), %r11
	movq	-176(%rbp), %r10
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%rax, %r8
	movq	%rax, -168(%rbp)
	subq	%rcx, %r8
	movq	%r11, %rdx
	sarq	$3, %rcx
	movq	%r10, %rsi
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-168(%rbp), %rax
	movq	-152(%rbp), %r11
.L1563:
	subq	%rax, %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$112, %rbx
	jle	.L1777
	sarq	$4, %rbx
	movq	%r11, %rdi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%rax, -176(%rbp)
	leaq	0(,%rbx,8), %rcx
	movq	%r11, -152(%rbp)
	leaq	(%r11,%rcx), %rbx
	movq	%rcx, -168(%rbp)
	movq	%rbx, %rsi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	%r12, %r8
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	-152(%rbp), %r11
	movq	-168(%rbp), %rcx
	subq	%rbx, %r8
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	sarq	$3, %r8
	movq	%r11, %rdi
	sarq	$3, %rcx
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-176(%rbp), %rax
	movq	-152(%rbp), %r11
.L1565:
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%rax, %r8
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r11, %rsi
	movq	%r13, %rdi
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1561:
	movq	-80(%rbp), %rax
	subq	%r12, %rax
	movq	%rax, %r14
	sarq	$3, %r14
	cmpq	$112, %rax
	jle	.L1778
	sarq	$4, %rax
	salq	$3, %rax
	movq	%rax, %rcx
	leaq	(%r12,%rax), %rbx
	sarq	$3, %rcx
	movq	%rcx, -152(%rbp)
	cmpq	$112, %rax
	jbe	.L1779
	movq	%rax, %rcx
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	sarq	$4, %rcx
	salq	$3, %rcx
	leaq	(%r12,%rcx), %r11
	movq	%rcx, -176(%rbp)
	movq	%r11, %rsi
	movq	%r11, -168(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-168(%rbp), %r11
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r11, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %rcx
	movq	%rbx, %rdx
	movq	-184(%rbp), %rax
	movq	-168(%rbp), %r11
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%r12, %rdi
	subq	%rcx, %rax
	sarq	$3, %rcx
	sarq	$3, %rax
	movq	%r11, %rsi
	movq	%rax, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1569:
	movq	-80(%rbp), %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	$112, %rdx
	jle	.L1780
	sarq	$4, %rdx
	movq	%rbx, %rdi
	movq	%rax, -184(%rbp)
	leaq	0(,%rdx,8), %rcx
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	leaq	(%rbx,%rcx), %r11
	movq	%rcx, -176(%rbp)
	movq	%r11, %rsi
	movq	%r11, -168(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-168(%rbp), %r11
	movq	-80(%rbp), %rsi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r11, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-80(%rbp), %rdx
	movq	%rbx, %rdi
	movq	-168(%rbp), %r11
	movq	-176(%rbp), %rcx
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%rdx, %r8
	movq	%r11, %rsi
	subq	%r11, %r8
	sarq	$3, %rcx
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-184(%rbp), %rax
.L1571:
	movq	-152(%rbp), %rcx
	movq	%rax, %r8
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-80(%rbp), %rdx
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1567:
	movq	-120(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	%r14, %r8
	movq	%r12, %rsi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%r13, %rdi
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1552:
	movq	-136(%rbp), %r8
	movq	-160(%rbp), %rcx
	movq	%r13, %rsi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rdi
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1522:
	movq	-96(%rbp), %rax
	subq	-80(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -136(%rbp)
	sarq	$3, %rsi
	movq	%rsi, -152(%rbp)
	movq	%rsi, -120(%rbp)
	cmpq	$112, %rax
	jle	.L1781
	movq	%rax, %r13
	sarq	$4, %r13
	leaq	0(,%r13,8), %rax
	movq	-80(%rbp), %r13
	movq	%rax, %rcx
	movq	%rax, -160(%rbp)
	sarq	$3, %rcx
	addq	%rax, %r13
	movq	%rcx, -176(%rbp)
	cmpq	$112, %rax
	jbe	.L1782
	movq	-80(%rbp), %rcx
	sarq	$4, %rax
	salq	$3, %rax
	addq	%rax, %rcx
	movq	%rax, -168(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%rax, %rcx
	sarq	$3, %rcx
	movq	%rcx, -184(%rbp)
	cmpq	$112, %rax
	jbe	.L1783
	movq	%rax, %rbx
	movq	-80(%rbp), %rax
	sarq	$4, %rbx
	salq	$3, %rbx
	movq	%rbx, %r14
	leaq	(%rax,%rbx), %r12
	sarq	$3, %r14
	cmpq	$112, %rbx
	jbe	.L1784
	movq	%rax, %rcx
	movq	%rbx, %rax
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	sarq	$4, %rax
	movq	%rcx, %rdi
	salq	$3, %rax
	leaq	(%rcx,%rax), %r11
	movq	%rax, -200(%rbp)
	movq	%r11, %rsi
	movq	%r11, -192(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-192(%rbp), %r11
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	%rbx, %r8
	movq	-80(%rbp), %rdi
	movq	%r12, %rdx
	movq	-200(%rbp), %rax
	movq	-192(%rbp), %r11
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	subq	%rax, %r8
	sarq	$3, %rax
	movq	%r11, %rsi
	movq	%rax, %rcx
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1603:
	movq	-168(%rbp), %rax
	subq	%rbx, %rax
	movq	%rax, %rbx
	sarq	$3, %rbx
	cmpq	$112, %rax
	jle	.L1785
	sarq	$4, %rax
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rdi
	salq	$3, %rax
	leaq	(%r12,%rax), %r11
	movq	%rax, -200(%rbp)
	movq	%r11, %rsi
	movq	%r11, -192(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-192(%rbp), %r11
	movq	-136(%rbp), %rsi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r11, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-136(%rbp), %rdx
	movq	%r12, %rdi
	movq	-192(%rbp), %r11
	movq	-200(%rbp), %rax
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%rdx, %r8
	movq	%r11, %rsi
	subq	%r11, %r8
	sarq	$3, %rax
	movq	%rax, %rcx
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1605:
	movq	-136(%rbp), %rdx
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	-80(%rbp), %rdi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1595:
	movq	-160(%rbp), %rax
	subq	-168(%rbp), %rax
	movq	%rax, %rbx
	sarq	$3, %rbx
	cmpq	$112, %rax
	jle	.L1786
	sarq	$4, %rax
	movq	-136(%rbp), %rcx
	salq	$3, %rax
	movq	%rax, %r14
	leaq	(%rcx,%rax), %r12
	sarq	$3, %r14
	cmpq	$112, %rax
	jbe	.L1787
	movq	%rax, %rcx
	movq	%rax, -192(%rbp)
	movq	-136(%rbp), %rax
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	sarq	$4, %rcx
	salq	$3, %rcx
	movq	%rax, %rdi
	leaq	(%rax,%rcx), %r11
	movq	%rcx, -168(%rbp)
	movq	%r11, %rsi
	movq	%r11, -160(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-160(%rbp), %r11
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-168(%rbp), %rcx
	movq	%r12, %rdx
	movq	-192(%rbp), %rax
	movq	-160(%rbp), %r11
	movq	-136(%rbp), %rdi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	subq	%rcx, %rax
	sarq	$3, %rcx
	movq	%rax, %r8
	movq	%r11, %rsi
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1609:
	movq	%r13, %rax
	subq	%r12, %rax
	movq	%rax, %r11
	sarq	$3, %r11
	cmpq	$112, %rax
	jle	.L1788
	sarq	$4, %rax
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rdi
	movq	%r11, -192(%rbp)
	leaq	0(,%rax,8), %rcx
	leaq	(%r12,%rcx), %r10
	movq	%rcx, -168(%rbp)
	movq	%r10, %rsi
	movq	%r10, -160(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-160(%rbp), %r10
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	%r13, %r8
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	-160(%rbp), %r10
	movq	-168(%rbp), %rcx
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	subq	%r10, %r8
	sarq	$3, %rcx
	movq	%r10, %rsi
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-192(%rbp), %r11
.L1611:
	movq	-136(%rbp), %rdi
	movq	%r11, %r8
	movq	%r14, %rcx
	movq	%r13, %rdx
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%r12, %rsi
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1607:
	movq	-184(%rbp), %rcx
	movq	-80(%rbp), %rdi
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	-136(%rbp), %rsi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1586:
	movq	-96(%rbp), %rbx
	subq	%r13, %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	movq	%rax, -160(%rbp)
	cmpq	$112, %rbx
	jle	.L1789
	sarq	$4, %rbx
	salq	$3, %rbx
	movq	%rbx, %rax
	leaq	0(%r13,%rbx), %r12
	sarq	$3, %rax
	movq	%rax, -136(%rbp)
	cmpq	$112, %rbx
	jbe	.L1790
	movq	%rbx, %rax
	sarq	$4, %rax
	salq	$3, %rax
	movq	%rax, %rcx
	leaq	0(%r13,%rax), %r11
	sarq	$3, %rcx
	movq	%rcx, %r14
	cmpq	$112, %rax
	jbe	.L1791
	movq	%rax, %rcx
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rdi
	movq	%rax, -208(%rbp)
	sarq	$4, %rcx
	movq	%r11, -168(%rbp)
	salq	$3, %rcx
	leaq	0(%r13,%rcx), %r10
	movq	%rcx, -200(%rbp)
	movq	%r10, %rsi
	movq	%r10, -184(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-184(%rbp), %r10
	movq	-168(%rbp), %r11
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r11, %rsi
	movq	%r10, %rdi
	movq	%r10, -192(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-208(%rbp), %rax
	movq	%r13, %rdi
	movq	-200(%rbp), %rcx
	movq	-168(%rbp), %r11
	movq	-192(%rbp), %r10
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%rax, %r8
	movq	%rax, -184(%rbp)
	subq	%rcx, %r8
	movq	%r11, %rdx
	sarq	$3, %rcx
	movq	%r10, %rsi
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-184(%rbp), %rax
	movq	-168(%rbp), %r11
.L1625:
	subq	%rax, %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$112, %rbx
	jle	.L1792
	sarq	$4, %rbx
	movq	%r11, %rdi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%rax, -192(%rbp)
	leaq	0(,%rbx,8), %rcx
	movq	%r11, -168(%rbp)
	leaq	(%r11,%rcx), %rbx
	movq	%rcx, -184(%rbp)
	movq	%rbx, %rsi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	%r12, %r8
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	-168(%rbp), %r11
	movq	-184(%rbp), %rcx
	subq	%rbx, %r8
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	sarq	$3, %r8
	movq	%r11, %rdi
	sarq	$3, %rcx
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-192(%rbp), %rax
	movq	-168(%rbp), %r11
.L1627:
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%rax, %r8
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r11, %rsi
	movq	%r13, %rdi
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1623:
	movq	-96(%rbp), %rax
	subq	%r12, %rax
	movq	%rax, %r14
	sarq	$3, %r14
	cmpq	$112, %rax
	jle	.L1793
	sarq	$4, %rax
	salq	$3, %rax
	movq	%rax, %rcx
	leaq	(%r12,%rax), %rbx
	sarq	$3, %rcx
	movq	%rcx, -168(%rbp)
	cmpq	$112, %rax
	jbe	.L1794
	movq	%rax, %rcx
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	sarq	$4, %rcx
	salq	$3, %rcx
	leaq	(%r12,%rcx), %r11
	movq	%rcx, -192(%rbp)
	movq	%r11, %rsi
	movq	%r11, -184(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-184(%rbp), %r11
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r11, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-192(%rbp), %rcx
	movq	%rbx, %rdx
	movq	-200(%rbp), %rax
	movq	-184(%rbp), %r11
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%r12, %rdi
	subq	%rcx, %rax
	sarq	$3, %rcx
	sarq	$3, %rax
	movq	%r11, %rsi
	movq	%rax, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1631:
	movq	-96(%rbp), %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	$112, %rdx
	jle	.L1795
	sarq	$4, %rdx
	movq	%rbx, %rdi
	movq	%rax, -200(%rbp)
	leaq	0(,%rdx,8), %rcx
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	leaq	(%rbx,%rcx), %r11
	movq	%rcx, -192(%rbp)
	movq	%r11, %rsi
	movq	%r11, -184(%rbp)
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-184(%rbp), %r11
	movq	-96(%rbp), %rsi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r11, %rdi
	call	_ZSt21__inplace_stable_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-96(%rbp), %rdx
	movq	%rbx, %rdi
	movq	-184(%rbp), %r11
	movq	-192(%rbp), %rcx
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	%rdx, %r8
	movq	%r11, %rsi
	subq	%r11, %r8
	sarq	$3, %rcx
	sarq	$3, %r8
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	movq	-200(%rbp), %rax
.L1633:
	movq	-168(%rbp), %rcx
	movq	%rax, %r8
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-96(%rbp), %rdx
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1629:
	movq	-136(%rbp), %rcx
	movq	%r14, %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	-96(%rbp), %rdx
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1614:
	movq	-160(%rbp), %r8
	movq	-176(%rbp), %rcx
	movq	%r13, %rsi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	-96(%rbp), %rdx
	movq	-80(%rbp), %rdi
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
.L1575:
	movq	-144(%rbp), %rax
	addq	-152(%rbp), %rax
	cmpq	$2, %rax
	je	.L1796
	movl	%r15d, -160(%rbp)
	movq	-128(%rbp), %rsi
	cmpq	%rsi, -120(%rbp)
	jge	.L1636
.L1798:
	movq	%rsi, %rax
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rbx
	shrq	$63, %rax
	addq	%rsi, %rax
	sarq	%rax
	movq	%rax, -152(%rbp)
	leaq	(%rdx,%rax,8), %r12
	movq	%rax, -136(%rbp)
	movq	-96(%rbp), %rax
	subq	%rbx, %rax
	movq	%rax, %r15
	sarq	$3, %r15
	testq	%rax, %rax
	jg	.L1638
	jmp	.L1659
	.p2align 4,,10
	.p2align 3
.L1640:
	testq	%r13, %r13
	je	.L1639
.L1641:
	movq	%r13, %r15
.L1638:
	movq	%r15, %r13
	movq	%r12, %rsi
	sarq	%r13
	leaq	(%rbx,%r13,8), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	jns	.L1640
	subq	%r13, %r15
	leaq	8(%r14), %rbx
	leaq	-1(%r15), %r13
	testq	%r13, %r13
	jg	.L1641
.L1639:
	movq	%rbx, %r14
	subq	-80(%rbp), %r14
	movq	%r14, %rax
	sarq	$3, %rax
	subq	%rax, -120(%rbp)
	movq	%rax, -144(%rbp)
.L1637:
	movq	-152(%rbp), %rcx
	subq	%rcx, -128(%rbp)
.L1642:
	movq	-80(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZNSt3_V28__rotateIPPN2v88internal10RegExpTreeEEET_S6_S6_S6_St26random_access_iterator_tag
	movq	-144(%rbp), %r8
	movq	-88(%rbp), %rdi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %r9
	movq	-136(%rbp), %rcx
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%rax, %r13
	call	_ZSt22__merge_without_bufferIPPN2v88internal10RegExpTreeElN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_SF_T0_SJ_T1_
	cmpq	$0, -128(%rbp)
	je	.L1703
	cmpq	$0, -120(%rbp)
	je	.L1703
	movq	-128(%rbp), %rax
	addq	-120(%rbp), %rax
	cmpq	$2, %rax
	je	.L1797
	movq	%r13, -88(%rbp)
	movq	-128(%rbp), %rsi
	movq	%rbx, -80(%rbp)
	cmpq	%rsi, -120(%rbp)
	jl	.L1798
.L1636:
	movq	-120(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	-88(%rbp), %r12
	movq	%rcx, %rax
	shrq	$63, %rax
	addq	%rcx, %rax
	sarq	%rax
	movq	%rax, -152(%rbp)
	leaq	(%rsi,%rax,8), %rbx
	movq	%rax, -144(%rbp)
	movq	%rsi, %rax
	subq	%r12, %rax
	movq	%rax, %r15
	sarq	$3, %r15
	testq	%rax, %rax
	jle	.L1660
	.p2align 4,,10
	.p2align 3
.L1644:
	movq	%r15, %r13
	movq	%rbx, %rdi
	sarq	%r13
	leaq	(%r12,%r13,8), %r14
	movq	%r14, %rsi
	call	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	js	.L1646
	subq	%r13, %r15
	leaq	8(%r14), %r12
	subq	$1, %r15
	testq	%r15, %r15
	jg	.L1644
.L1645:
	movq	%r12, %r15
	subq	-88(%rbp), %r15
	movq	%r15, %rax
	sarq	$3, %rax
	subq	%rax, -128(%rbp)
	movq	%rax, -136(%rbp)
.L1643:
	movq	-152(%rbp), %rdx
	subq	%rdx, -120(%rbp)
	jmp	.L1642
	.p2align 4,,10
	.p2align 3
.L1646:
	testq	%r13, %r13
	je	.L1645
	movq	%r13, %r15
	jmp	.L1644
.L1797:
	movl	-160(%rbp), %r15d
	movq	%r13, %r14
.L1634:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	(%r14), %rdi
	movq	%rax, %r13
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	8(%r13), %rdx
	movq	8(%rax), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, (%rdx)
	jnb	.L1512
	movq	(%r14), %rax
	movq	(%rbx), %rdx
	xorl	%r13d, %r13d
	movq	%rdx, (%r14)
	movq	%rax, (%rbx)
	jmp	.L1509
.L1766:
	movq	-88(%rbp), %rax
	leaq	8(%rax), %rbx
	cmpq	%rbx, -80(%rbp)
	je	.L1522
	movl	%r15d, -120(%rbp)
	jmp	.L1521
	.p2align 4,,10
	.p2align 3
.L1801:
	movq	%r15, (%r12)
	addq	$8, %rbx
	cmpq	%rbx, -80(%rbp)
	je	.L1799
.L1521:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	%rax, %r12
	movq	-88(%rbp), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	8(%r12), %rdx
	movq	8(%rax), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, (%rdx)
	jb	.L1800
	movq	(%rbx), %r15
	movq	%rbx, %r14
	.p2align 4,,10
	.p2align 3
.L1527:
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r14, %r12
	subq	$8, %r14
	call	*152(%rax)
	movq	(%r14), %rdi
	movq	%rax, %r13
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	8(%r13), %rdx
	movq	8(%rax), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, (%rdx)
	jnb	.L1801
	movq	(%r14), %rax
	movq	%rax, 8(%r14)
	jmp	.L1527
.L1800:
	movq	-88(%rbp), %rsi
	movq	(%rbx), %r12
	cmpq	%rsi, %rbx
	je	.L1524
	movq	%rbx, %rdx
	movl	$8, %eax
	subq	%rsi, %rdx
	leaq	(%rsi,%rax), %rdi
	call	memmove@PLT
.L1524:
	movq	-88(%rbp), %rax
	addq	$8, %rbx
	movq	%r12, (%rax)
	cmpq	%rbx, -80(%rbp)
	jne	.L1521
.L1799:
	movl	-120(%rbp), %r15d
	jmp	.L1522
.L1781:
	movq	-96(%rbp), %rcx
	cmpq	%rcx, -80(%rbp)
	je	.L1576
	movq	-80(%rbp), %rax
	leaq	8(%rax), %rbx
	cmpq	-96(%rbp), %rbx
	je	.L1576
	movl	%r15d, -160(%rbp)
	jmp	.L1582
	.p2align 4,,10
	.p2align 3
.L1804:
	movq	%r15, (%r12)
	addq	$8, %rbx
	cmpq	-96(%rbp), %rbx
	je	.L1802
.L1582:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	%rax, %r12
	movq	-80(%rbp), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	8(%r12), %rdx
	movq	8(%rax), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, (%rdx)
	jb	.L1803
	movq	(%rbx), %r15
	movq	%rbx, %r14
	.p2align 4,,10
	.p2align 3
.L1581:
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r14, %r12
	subq	$8, %r14
	call	*152(%rax)
	movq	(%r14), %rdi
	movq	%rax, %r13
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	8(%r13), %rdx
	movq	8(%rax), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, (%rdx)
	jnb	.L1804
	movq	(%r14), %rax
	movq	%rax, 8(%r14)
	jmp	.L1581
.L1803:
	movq	-80(%rbp), %rsi
	movq	(%rbx), %r12
	cmpq	%rbx, %rsi
	je	.L1578
	movq	%rbx, %rdx
	movl	$8, %eax
	subq	%rsi, %rdx
	leaq	(%rsi,%rax), %rdi
	call	memmove@PLT
.L1578:
	movq	-80(%rbp), %rax
	addq	$8, %rbx
	movq	%r12, (%rax)
	cmpq	-96(%rbp), %rbx
	jne	.L1582
.L1802:
	movl	-160(%rbp), %r15d
.L1576:
	cmpq	$0, -136(%rbp)
	jne	.L1575
	jmp	.L1512
.L1767:
	movq	-88(%rbp), %rax
	leaq	8(%rax), %rbx
	cmpq	%rbx, %r13
	je	.L1531
	movq	%r13, -152(%rbp)
	movl	%r15d, -168(%rbp)
	jmp	.L1530
	.p2align 4,,10
	.p2align 3
.L1807:
	movq	%r15, (%r12)
	addq	$8, %rbx
	cmpq	%rbx, -152(%rbp)
	je	.L1805
.L1530:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	%rax, %r12
	movq	-88(%rbp), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	8(%r12), %rdx
	movq	8(%rax), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, (%rdx)
	jb	.L1806
	movq	(%rbx), %r15
	movq	%rbx, %r13
	.p2align 4,,10
	.p2align 3
.L1536:
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r13, %r12
	subq	$8, %r13
	call	*152(%rax)
	movq	0(%r13), %rdi
	movq	%rax, %r14
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	8(%r14), %rdx
	movq	8(%rax), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, (%rdx)
	jnb	.L1807
	movq	0(%r13), %rax
	movq	%rax, 8(%r13)
	jmp	.L1536
.L1806:
	movq	-88(%rbp), %rsi
	movq	(%rbx), %r12
	cmpq	%rsi, %rbx
	je	.L1533
	movq	%rbx, %rdx
	movl	$8, %eax
	subq	%rsi, %rdx
	leaq	(%rsi,%rax), %rdi
	call	memmove@PLT
.L1533:
	movq	-88(%rbp), %rax
	addq	$8, %rbx
	movq	%r12, (%rax)
	cmpq	%rbx, -152(%rbp)
	jne	.L1530
.L1805:
	movq	-152(%rbp), %r13
	movl	-168(%rbp), %r15d
	jmp	.L1531
.L1774:
	movq	-80(%rbp), %rax
	cmpq	%r13, %rax
	je	.L1552
	leaq	8(%r13), %rbx
	cmpq	%rbx, %rax
	je	.L1552
	movq	%r13, -120(%rbp)
	movl	%r15d, -152(%rbp)
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1810:
	movq	%r15, (%r12)
	addq	$8, %rbx
	cmpq	%rbx, -80(%rbp)
	je	.L1808
.L1559:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	%rax, %r12
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	8(%r12), %rdx
	movq	8(%rax), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, (%rdx)
	jb	.L1809
	movq	(%rbx), %r15
	movq	%rbx, %r14
	.p2align 4,,10
	.p2align 3
.L1558:
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r14, %r12
	subq	$8, %r14
	call	*152(%rax)
	movq	(%r14), %rdi
	movq	%rax, %r13
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	8(%r13), %rdx
	movq	8(%rax), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, (%rdx)
	jnb	.L1810
	movq	(%r14), %rax
	movq	%rax, 8(%r14)
	jmp	.L1558
.L1809:
	movq	-120(%rbp), %rsi
	movq	(%rbx), %r12
	cmpq	%rbx, %rsi
	je	.L1555
	movq	%rbx, %rdx
	movl	$8, %eax
	subq	%rsi, %rdx
	leaq	(%rsi,%rax), %rdi
	call	memmove@PLT
.L1555:
	movq	-120(%rbp), %rax
	addq	$8, %rbx
	movq	%r12, (%rax)
	cmpq	%rbx, -80(%rbp)
	jne	.L1559
.L1808:
	movq	-120(%rbp), %r13
	movl	-152(%rbp), %r15d
	jmp	.L1552
.L1789:
	movq	-96(%rbp), %rax
	cmpq	%rax, %r13
	je	.L1614
	leaq	8(%r13), %rbx
	cmpq	%rax, %rbx
	je	.L1614
	movq	%r13, -136(%rbp)
	movl	%r15d, -168(%rbp)
	jmp	.L1621
	.p2align 4,,10
	.p2align 3
.L1813:
	movq	%r15, (%r12)
	addq	$8, %rbx
	cmpq	-96(%rbp), %rbx
	je	.L1811
.L1621:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	%rax, %r12
	movq	-136(%rbp), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	8(%r12), %rdx
	movq	8(%rax), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, (%rdx)
	jb	.L1812
	movq	(%rbx), %r15
	movq	%rbx, %r14
	.p2align 4,,10
	.p2align 3
.L1620:
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r14, %r12
	subq	$8, %r14
	call	*152(%rax)
	movq	(%r14), %rdi
	movq	%rax, %r13
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	8(%r13), %rdx
	movq	8(%rax), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, (%rdx)
	jnb	.L1813
	movq	(%r14), %rax
	movq	%rax, 8(%r14)
	jmp	.L1620
.L1812:
	movq	-136(%rbp), %rsi
	movq	(%rbx), %r12
	cmpq	%rbx, %rsi
	je	.L1617
	movq	%rbx, %rdx
	movl	$8, %eax
	subq	%rsi, %rdx
	leaq	(%rsi,%rax), %rdi
	call	memmove@PLT
.L1617:
	movq	-136(%rbp), %rax
	addq	$8, %rbx
	movq	%r12, (%rax)
	cmpq	-96(%rbp), %rbx
	jne	.L1621
.L1811:
	movq	-136(%rbp), %r13
	movl	-168(%rbp), %r15d
	jmp	.L1614
.L1782:
	movq	-80(%rbp), %rax
	leaq	8(%rax), %rbx
	cmpq	%rbx, %r13
	je	.L1586
	movq	%r13, -136(%rbp)
	movl	%r15d, -160(%rbp)
	jmp	.L1585
	.p2align 4,,10
	.p2align 3
.L1816:
	movq	%r15, (%r12)
	addq	$8, %rbx
	cmpq	%rbx, -136(%rbp)
	je	.L1814
.L1585:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	%rax, %r12
	movq	-80(%rbp), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	8(%r12), %rdx
	movq	8(%rax), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, (%rdx)
	jb	.L1815
	movq	(%rbx), %r15
	movq	%rbx, %r13
	.p2align 4,,10
	.p2align 3
.L1591:
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r13, %r12
	subq	$8, %r13
	call	*152(%rax)
	movq	0(%r13), %rdi
	movq	%rax, %r14
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	8(%r14), %rdx
	movq	8(%rax), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, (%rdx)
	jnb	.L1816
	movq	0(%r13), %rax
	movq	%rax, 8(%r13)
	jmp	.L1591
.L1815:
	movq	-80(%rbp), %rsi
	movq	(%rbx), %r12
	cmpq	%rbx, %rsi
	je	.L1588
	movq	%rbx, %rdx
	movl	$8, %eax
	subq	%rsi, %rdx
	leaq	(%rsi,%rax), %rdi
	call	memmove@PLT
.L1588:
	movq	-80(%rbp), %rax
	addq	$8, %rbx
	movq	%r12, (%rax)
	cmpq	%rbx, -136(%rbp)
	jne	.L1585
.L1814:
	movq	-136(%rbp), %r13
	movl	-160(%rbp), %r15d
	jmp	.L1586
.L1660:
	movq	$0, -136(%rbp)
	jmp	.L1643
.L1659:
	movq	$0, -144(%rbp)
	jmp	.L1637
.L1783:
	movq	-80(%rbp), %rax
	leaq	8(%rax), %rbx
	cmpq	%rbx, -136(%rbp)
	je	.L1595
	movq	%r13, -192(%rbp)
	movl	%r15d, -200(%rbp)
	jmp	.L1594
.L1818:
	movq	(%rbx), %r12
	movq	%r14, %rsi
	cmpq	%rbx, %r14
	je	.L1597
	movq	%rbx, %rdx
	movl	$8, %eax
	subq	%r14, %rdx
	leaq	(%r14,%rax), %rdi
	call	memmove@PLT
.L1597:
	movq	-80(%rbp), %rax
	movq	%r12, (%rax)
.L1598:
	addq	$8, %rbx
	cmpq	%rbx, -136(%rbp)
	je	.L1817
.L1594:
	movq	-80(%rbp), %r14
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_
	testl	%eax, %eax
	js	.L1818
	movq	(%rbx), %r15
	movq	%rbx, %r13
	.p2align 4,,10
	.p2align 3
.L1600:
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r13, %r12
	subq	$8, %r13
	call	*152(%rax)
	movq	0(%r13), %rdi
	movq	%rax, %r14
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	8(%r14), %rdx
	movq	8(%rax), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, (%rdx)
	jb	.L1599
	movq	%r15, (%r12)
	jmp	.L1598
	.p2align 4,,10
	.p2align 3
.L1599:
	movq	0(%r13), %rax
	movq	%rax, 8(%r13)
	jmp	.L1600
.L1778:
	movq	-80(%rbp), %rsi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1567
.L1771:
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1545
.L1768:
	movq	-88(%rbp), %rdi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1539
.L1786:
	movq	-136(%rbp), %rdi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rsi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1607
.L1775:
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1561
.L1790:
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1623
.L1793:
	movq	-96(%rbp), %rsi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1629
.L1817:
	movq	-192(%rbp), %r13
	movl	-200(%rbp), %r15d
	jmp	.L1595
.L1795:
	movq	-96(%rbp), %rsi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%rbx, %rdi
	movq	%rax, -184(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-184(%rbp), %rax
	jmp	.L1633
.L1794:
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1631
.L1792:
	movq	%r11, %rdi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%rax, -184(%rbp)
	movq	%r11, -168(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-168(%rbp), %r11
	movq	-184(%rbp), %rax
	jmp	.L1627
.L1780:
	movq	-80(%rbp), %rsi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%rbx, %rdi
	movq	%rax, -168(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-168(%rbp), %rax
	jmp	.L1571
.L1777:
	movq	%r11, %rdi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%rax, -168(%rbp)
	movq	%r11, -152(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-152(%rbp), %r11
	movq	-168(%rbp), %rax
	jmp	.L1565
.L1776:
	movq	%r11, %rsi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rdi
	movq	%rax, -168(%rbp)
	movq	%r11, -152(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-152(%rbp), %r11
	movq	-168(%rbp), %rax
	jmp	.L1563
.L1796:
	movq	-88(%rbp), %r14
	movq	-80(%rbp), %rbx
	jmp	.L1634
.L1773:
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, -176(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %rax
	jmp	.L1549
.L1772:
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1547
.L1769:
	movq	-88(%rbp), %rdi
	movq	%r11, %rsi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%rax, -184(%rbp)
	movq	%r11, -176(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %r11
	movq	-184(%rbp), %rax
	jmp	.L1541
.L1787:
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%rcx, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1609
.L1785:
	movq	-136(%rbp), %rsi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1605
.L1784:
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1603
.L1788:
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r11, -160(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-160(%rbp), %r11
	jmp	.L1611
.L1791:
	movq	%r11, %rsi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r13, %rdi
	movq	%rax, -184(%rbp)
	movq	%r11, -168(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-168(%rbp), %r11
	movq	-184(%rbp), %rax
	jmp	.L1625
.L1779:
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	jmp	.L1569
.L1770:
	movq	%r11, %rdi
	leaq	_ZN2v88internal16CompareFirstCharEPKPNS0_10RegExpTreeES4_(%rip), %rdx
	movq	%r12, %rsi
	movq	%rax, -184(%rbp)
	movq	%r11, -176(%rbp)
	call	_ZSt16__insertion_sortIPPN2v88internal10RegExpTreeEN9__gnu_cxx5__ops15_Iter_comp_iterIZNS1_8ZoneListIS3_E10StableSortIPFiPKS3_SC_EEEvT_mmEUlRSB_SG_E_EEEvSF_SF_T0_
	movq	-176(%rbp), %r11
	movq	-184(%rbp), %rax
	jmp	.L1543
.L1710:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9995:
	.size	_ZN2v88internal17RegExpDisjunction20SortConsecutiveAtomsEPNS0_14RegExpCompilerE, .-_ZN2v88internal17RegExpDisjunction20SortConsecutiveAtomsEPNS0_14RegExpCompilerE
	.section	.text._ZN2v88internal17RegExpDisjunction27RationalizeConsecutiveAtomsEPNS0_14RegExpCompilerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpDisjunction27RationalizeConsecutiveAtomsEPNS0_14RegExpCompilerE
	.type	_ZN2v88internal17RegExpDisjunction27RationalizeConsecutiveAtomsEPNS0_14RegExpCompilerE, @function
_ZN2v88internal17RegExpDisjunction27RationalizeConsecutiveAtomsEPNS0_14RegExpCompilerE:
.LFB9996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r15
	movq	1096(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	12(%r15), %eax
	movl	%eax, -220(%rbp)
	testl	%eax, %eax
	jle	.L1878
	leaq	-192(%rbp), %rax
	movl	$0, -232(%rbp)
	movl	$0, -224(%rbp)
	movq	%rax, -248(%rbp)
	jmp	.L1821
	.p2align 4,,10
	.p2align 3
.L1910:
	movq	(%r15), %rax
	movslq	-224(%rbp), %rdx
	movl	%edi, -232(%rbp)
	movq	(%rax,%rbx,8), %rcx
	movl	-232(%rbp), %ebx
	movq	%rcx, (%rax,%rdx,8)
	leal	1(%rdx), %eax
	movl	%eax, -224(%rbp)
	cmpl	%ebx, -220(%rbp)
	jle	.L1820
.L1821:
	movslq	-232(%rbp), %rbx
	leaq	0(,%rbx,8), %rax
	movq	%rbx, -272(%rbp)
	movq	%rax, -256(%rbp)
	movq	(%r15), %rax
	movq	(%rax,%rbx,8), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*160(%rax)
	leal	1(%rbx), %edi
	movl	%edi, -264(%rbp)
	testb	%al, %al
	je	.L1910
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*152(%rax)
	movq	-248(%rbp), %rdi
	movl	24(%rax), %r14d
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movzwl	(%rax), %esi
	movl	%r14d, -216(%rbp)
	call	_ZN6icu_6713UnicodeStringC1Ei@PLT
	movl	16(%rbx), %ebx
	movl	-264(%rbp), %eax
	cmpl	%eax, -220(%rbp)
	jle	.L1879
	movq	-256(%rbp), %rsi
	andl	$2, %r14d
	movl	%eax, -212(%rbp)
	movl	%r14d, -240(%rbp)
	leaq	-128(%rbp), %r14
	leaq	8(%rsi), %rdi
	movq	%r13, -280(%rbp)
	movq	%rdi, -296(%rbp)
	movq	%rdi, %r12
	jmp	.L1836
	.p2align 4,,10
	.p2align 3
.L1911:
	movzbl	-184(%rbp), %eax
	andl	$1, %eax
.L1829:
	testb	%al, %al
	je	.L1834
.L1846:
	movq	16(%r13), %rax
	movq	%r14, %rdi
	cmpl	%eax, %ebx
	cmovg	%eax, %ebx
	addl	$1, -212(%rbp)
	addq	$8, %r12
	movl	-212(%rbp), %r13d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpl	-220(%rbp), %r13d
	je	.L1905
.L1836:
	movq	(%r15), %rax
	movq	(%rax,%r12), %r13
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*160(%rax)
	testb	%al, %al
	je	.L1905
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*152(%rax)
	movq	%rax, %r13
	movl	-216(%rbp), %eax
	cmpl	24(%r13), %eax
	jne	.L1905
	movq	8(%r13), %rax
	movq	%r14, %rdi
	movzwl	(%rax), %esi
	call	_ZN6icu_6713UnicodeStringC1Ei@PLT
	movswl	-120(%rbp), %edx
	testb	$1, %dl
	jne	.L1911
	testw	%dx, %dx
	js	.L1830
	sarl	$5, %edx
.L1831:
	movzwl	-184(%rbp), %ecx
	testw	%cx, %cx
	js	.L1832
	movswl	%cx, %eax
	sarl	$5, %eax
.L1833:
	cmpl	%edx, %eax
	jne	.L1834
	andl	$1, %ecx
	je	.L1912
.L1834:
	movl	-240(%rbp), %esi
	testl	%esi, %esi
	je	.L1837
	movzwl	-120(%rbp), %esi
	testw	%si, %si
	js	.L1838
	movswl	%si, %ecx
	sarl	$5, %ecx
.L1839:
	movzwl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L1840
	movswl	%ax, %edx
	sarl	$5, %edx
.L1841:
	testb	$1, %sil
	je	.L1842
	notl	%eax
	andl	$1, %eax
.L1843:
	testb	%al, %al
	je	.L1846
.L1837:
	movq	%r14, %rdi
	movq	-280(%rbp), %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1835:
	movl	-232(%rbp), %edi
	movl	-212(%rbp), %r14d
	leal	2(%rdi), %eax
	cmpl	%r14d, %eax
	jge	.L1824
	movq	(%r15), %rax
	movq	-272(%rbp), %rsi
	subl	%edi, %r14d
	movl	%r14d, -232(%rbp)
	movq	(%rax,%rsi,8), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	%rax, -240(%rbp)
	cmpl	$1, %r14d
	jle	.L1847
	cmpl	$1, %ebx
	jle	.L1847
	movq	-296(%rbp), %rsi
	movl	$1, %r12d
	movl	%ebx, %eax
	movq	%r13, -264(%rbp)
	movq	%r15, %rbx
	movl	%r12d, %r13d
	movl	%eax, %r15d
	movq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L1851:
	movq	(%rbx), %rax
	movq	(%rax,%r12), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	-240(%rbp), %rdi
	movq	8(%rax), %rdx
	movl	$1, %eax
	movq	8(%rdi), %r14
	.p2align 4,,10
	.p2align 3
.L1849:
	movzwl	(%rdx,%rax,2), %ecx
	cmpw	%cx, (%r14,%rax,2)
	jne	.L1883
	addq	$1, %rax
	cmpl	%eax, %r15d
	jg	.L1849
.L1848:
	addl	$1, %r13d
	addq	$8, %r12
	cmpl	%r13d, -232(%rbp)
	jle	.L1906
	cmpl	$1, %r15d
	jg	.L1851
.L1906:
	movl	%r15d, %eax
	movq	-264(%rbp), %r13
	movq	%rbx, %r15
	movl	%eax, %ebx
.L1850:
	movslq	%ebx, %rax
	movq	16(%r13), %r12
	movq	%rax, -264(%rbp)
	movq	24(%r13), %rax
	subq	%r12, %rax
	cmpq	$31, %rax
	jbe	.L1913
	leaq	32(%r12), %rax
	movq	%rax, 16(%r13)
.L1853:
	leaq	16+_ZTVN2v88internal10RegExpAtomE(%rip), %rax
	movq	%r14, 8(%r12)
	movq	%rax, (%r12)
	movslq	%ebx, %rax
	movq	%rax, 16(%r12)
	movl	-216(%rbp), %eax
	movl	%eax, 24(%r12)
	movq	16(%r13), %rax
	movq	%rax, %rsi
	movq	%rax, -288(%rbp)
	movq	24(%r13), %rax
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	$15, %rdx
	jbe	.L1914
	leaq	16(%rsi), %rdx
	subq	%rdx, %rax
	movq	%rdx, 16(%r13)
	cmpq	$15, %rax
	jbe	.L1915
.L1856:
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r13)
.L1857:
	movq	-288(%rbp), %rdi
	leaq	-200(%rbp), %r14
	movq	%r14, %rsi
	movq	%rdx, (%rdi)
	movq	%r13, %rdx
	movq	$2, 8(%rdi)
	movq	%r12, -200(%rbp)
	call	_ZN2v88internal8ZoneListIPNS0_10RegExpTreeEE3AddERKS3_PNS0_4ZoneE
	movq	16(%r13), %r12
	movq	24(%r13), %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L1916
	leaq	16(%r12), %rax
	movq	%rax, 16(%r13)
.L1859:
	movl	-232(%rbp), %eax
	testl	%eax, %eax
	jg	.L1917
	movq	$0, (%r12)
	movl	%eax, 8(%r12)
	movl	$0, 12(%r12)
	.p2align 4,,10
	.p2align 3
.L1876:
	movq	16(%r13), %rbx
	movq	24(%r13), %rax
	subq	%rbx, %rax
	cmpq	$23, %rax
	jbe	.L1918
	leaq	24(%rbx), %rax
	movq	%rax, 16(%r13)
.L1871:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal17RegExpDisjunctionC1EPNS0_8ZoneListIPNS0_10RegExpTreeEEE@PLT
	movq	-288(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, -200(%rbp)
	call	_ZN2v88internal8ZoneListIPNS0_10RegExpTreeEE3AddERKS3_PNS0_4ZoneE
	movq	16(%r13), %rbx
	movq	24(%r13), %rax
	subq	%rbx, %rax
	cmpq	$23, %rax
	jbe	.L1919
	leaq	24(%rbx), %rax
	movq	%rax, 16(%r13)
.L1873:
	movq	-288(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17RegExpAlternativeC1EPNS0_8ZoneListIPNS0_10RegExpTreeEEE@PLT
	movslq	-224(%rbp), %rdx
	movq	(%r15), %rax
	movq	%rbx, (%rax,%rdx,8)
	movl	-212(%rbp), %eax
	movl	%eax, -232(%rbp)
	leal	1(%rdx), %eax
	movl	%eax, -224(%rbp)
.L1874:
	movq	-248(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-232(%rbp), %ebx
	cmpl	%ebx, -220(%rbp)
	jg	.L1821
.L1820:
	movl	-224(%rbp), %eax
	movl	%eax, 12(%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1920
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1842:
	.cfi_restore_state
	testl	%ecx, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	cmovle	%ecx, %r8d
	js	.L1844
	movl	%ecx, %r9d
	subl	%r8d, %r9d
	cmpl	%ecx, %r9d
	cmovg	%ecx, %r9d
.L1844:
	andl	$2, %esi
	leaq	-118(%rbp), %rcx
	cmove	-104(%rbp), %rcx
	movq	-248(%rbp), %rdi
	subq	$8, %rsp
	xorl	%esi, %esi
	pushq	$0
	call	_ZNK6icu_6713UnicodeString13doCaseCompareEiiPKDsiij@PLT
	popq	%rdx
	popq	%rcx
	jmp	.L1843
	.p2align 4,,10
	.p2align 3
.L1840:
	movl	-180(%rbp), %edx
	jmp	.L1841
	.p2align 4,,10
	.p2align 3
.L1838:
	movl	-116(%rbp), %ecx
	jmp	.L1839
	.p2align 4,,10
	.p2align 3
.L1832:
	movl	-180(%rbp), %eax
	jmp	.L1833
	.p2align 4,,10
	.p2align 3
.L1830:
	movl	-116(%rbp), %edx
	jmp	.L1831
	.p2align 4,,10
	.p2align 3
.L1883:
	movl	%eax, %r15d
	jmp	.L1848
	.p2align 4,,10
	.p2align 3
.L1905:
	movq	-280(%rbp), %r13
	jmp	.L1835
	.p2align 4,,10
	.p2align 3
.L1912:
	movq	-248(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	jmp	.L1829
.L1879:
	movl	%eax, -212(%rbp)
	.p2align 4,,10
	.p2align 3
.L1824:
	movl	-212(%rbp), %ebx
	movl	-232(%rbp), %eax
	cmpl	%eax, %ebx
	jle	.L1884
	movq	(%r15), %rax
	movq	-272(%rbp), %rcx
	movslq	-224(%rbp), %rdx
	movq	(%rax,%rcx,8), %rcx
	leaq	0(,%rdx,8), %rsi
	movq	%rcx, (%rax,%rdx,8)
	movl	-264(%rbp), %eax
	cmpl	%eax, %ebx
	jle	.L1875
	movq	(%r15), %rax
	movq	-256(%rbp), %rbx
	movq	8(%rax,%rbx), %rdx
	movq	%rdx, 8(%rax,%rsi)
.L1875:
	movl	-212(%rbp), %ebx
	movl	-224(%rbp), %eax
	subl	-232(%rbp), %eax
	addl	%ebx, %eax
	movl	%ebx, -232(%rbp)
	movl	%eax, -224(%rbp)
	jmp	.L1874
.L1917:
	movslq	%eax, %rsi
	movq	24(%r13), %rdx
	movq	16(%r13), %rax
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1921
	addq	%rax, %rsi
	movq	%rsi, 16(%r13)
.L1862:
	movq	%rax, (%r12)
	movl	-232(%rbp), %eax
	movslq	%ebx, %rdi
	movq	-272(%rbp), %rsi
	leaq	(%rdi,%rdi), %rcx
	movl	%ebx, -240(%rbp)
	movl	%eax, 8(%r12)
	subl	$1, %eax
	movq	-296(%rbp), %rbx
	leaq	1(%rsi,%rax), %rax
	movq	%rcx, -280(%rbp)
	movq	%r12, %rcx
	movl	$0, 12(%r12)
	salq	$3, %rax
	movq	%r13, %r12
	movq	%rcx, %r13
	movq	%rax, -232(%rbp)
	movq	-256(%rbp), %rax
	jmp	.L1869
	.p2align 4,,10
	.p2align 3
.L1863:
	movq	-280(%rbp), %r8
	movq	24(%r12), %rsi
	movslq	%edx, %rdx
	addq	8(%rax), %r8
	movq	16(%r12), %rax
	subq	-264(%rbp), %rdx
	subq	%rax, %rsi
	cmpq	$31, %rsi
	jbe	.L1922
	leaq	32(%rax), %rsi
	movq	%rsi, 16(%r12)
.L1868:
	movl	-216(%rbp), %esi
	leaq	16+_ZTVN2v88internal10RegExpAtomE(%rip), %rdi
	movq	%r8, 8(%rax)
	movq	%rdi, (%rax)
	movq	%rdx, 16(%rax)
	movl	%esi, 24(%rax)
.L1909:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8ZoneListIPNS0_10RegExpTreeEE3AddERKS3_PNS0_4ZoneE
	movq	%rbx, %rax
	cmpq	%rbx, -232(%rbp)
	je	.L1907
	addq	$8, %rbx
.L1869:
	movq	(%r15), %rdx
	movq	(%rdx,%rax), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	16(%rax), %rdx
	cmpl	-240(%rbp), %edx
	jne	.L1863
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L1923
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%r12)
.L1865:
	leaq	16+_ZTVN2v88internal11RegExpEmptyE(%rip), %rsi
	movq	%rsi, (%rax)
	jmp	.L1909
	.p2align 4,,10
	.p2align 3
.L1907:
	movq	%r13, %rax
	movq	%r12, %r13
	movq	%rax, %r12
	jmp	.L1876
.L1922:
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%r8, -272(%rbp)
	movq	%rdx, -256(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-256(%rbp), %rdx
	movq	-272(%rbp), %r8
	jmp	.L1868
.L1884:
	movl	%ebx, -232(%rbp)
	jmp	.L1874
.L1923:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1865
.L1847:
	movq	-240(%rbp), %rax
	movq	8(%rax), %r14
	jmp	.L1850
.L1914:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	16(%r13), %rdx
	movq	%rax, -288(%rbp)
	movq	24(%r13), %rax
	subq	%rdx, %rax
	cmpq	$15, %rax
	ja	.L1856
.L1915:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1857
.L1916:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1859
.L1918:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1871
.L1913:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1853
.L1919:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1873
.L1878:
	movl	$0, -224(%rbp)
	jmp	.L1820
.L1921:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1862
.L1920:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9996:
	.size	_ZN2v88internal17RegExpDisjunction27RationalizeConsecutiveAtomsEPNS0_14RegExpCompilerE, .-_ZN2v88internal17RegExpDisjunction27RationalizeConsecutiveAtomsEPNS0_14RegExpCompilerE
	.section	.text._ZN2v88internal17RegExpDisjunction6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpDisjunction6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.type	_ZN2v88internal17RegExpDisjunction6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, @function
_ZN2v88internal17RegExpDisjunction6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE:
.LFB10004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	%rdx, -56(%rbp)
	movl	12(%rbx), %edx
	cmpl	$2, %edx
	jle	.L1925
	movq	%rdi, %r14
	call	_ZN2v88internal17RegExpDisjunction20SortConsecutiveAtomsEPNS0_14RegExpCompilerE
	testb	%al, %al
	jne	.L1946
.L1926:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17RegExpDisjunction30FixSingleCharacterDisjunctionsEPNS0_14RegExpCompilerE
	movl	12(%rbx), %edx
	cmpl	$1, %edx
	je	.L1947
.L1925:
	movq	1096(%r12), %r14
	movq	16(%r14), %r13
	movq	24(%r14), %rax
	subq	%r13, %rax
	cmpq	$71, %rax
	jbe	.L1948
	leaq	72(%r13), %rax
	movq	%rax, 16(%r14)
.L1928:
	leaq	16+_ZTVN2v88internal10ChoiceNodeE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, 48(%r13)
	movq	$0, 8(%r13)
	movq	$0, 16(%r13)
	movq	$0, 24(%r13)
	movq	%rax, 0(%r13)
	movups	%xmm0, 32(%r13)
	movq	16(%r14), %r15
	movq	24(%r14), %rax
	subq	%r15, %rax
	cmpq	$15, %rax
	jbe	.L1949
	leaq	16(%r15), %rax
	movq	%rax, 16(%r14)
.L1930:
	testl	%edx, %edx
	jg	.L1950
	xorl	%eax, %eax
	movq	$0, (%r15)
	movl	%edx, 8(%r15)
	movl	$0, 12(%r15)
	movq	%r15, 56(%r13)
	movw	%ax, 64(%r13)
.L1924:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1950:
	.cfi_restore_state
	movq	16(%r14), %rax
	movq	24(%r14), %rcx
	movslq	%edx, %rsi
	salq	$4, %rsi
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L1951
	addq	%rax, %rsi
	movq	%rsi, 16(%r14)
.L1933:
	movq	%rax, (%r15)
	xorl	%ecx, %ecx
	leal	-1(%rdx), %eax
	movl	%edx, 8(%r15)
	leaq	8(,%rax,8), %r14
	movl	$0, 12(%r15)
	movq	%r15, 56(%r13)
	xorl	%r15d, %r15d
	movw	%cx, 64(%r13)
	jmp	.L1939
	.p2align 4,,10
	.p2align 3
.L1952:
	leal	1(%rax), %edx
	salq	$4, %rax
	addq	(%r9), %rax
	movl	%edx, 12(%r9)
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
.L1935:
	addq	$8, %r15
	cmpq	%r15, %r14
	je	.L1924
.L1939:
	movq	(%rbx), %rax
	movq	-56(%rbp), %rdx
	movq	%r12, %rsi
	movq	(%rax,%r15), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	56(%r13), %r9
	movq	%rax, %rcx
	movl	8(%r9), %edx
	movslq	12(%r9), %rax
	cmpl	%edx, %eax
	jl	.L1952
	movq	48(%r13), %r11
	leal	1(%rdx,%rdx), %r10d
	movslq	%r10d, %rsi
	movq	16(%r11), %rdi
	movq	24(%r11), %rax
	salq	$4, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1953
	addq	%rdi, %rsi
	movq	%rsi, 16(%r11)
.L1937:
	movslq	12(%r9), %rdx
	movq	%rdx, %rsi
	salq	$4, %rdx
	testl	%esi, %esi
	jle	.L1938
	movq	(%r9), %rsi
	movl	%r10d, -76(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	movl	-76(%rbp), %r10d
	movq	-72(%rbp), %rcx
	movq	%rax, %rdi
	movslq	12(%r9), %rdx
	movq	%rdx, %rsi
	salq	$4, %rdx
.L1938:
	movq	%rdi, (%r9)
	addl	$1, %esi
	addq	%rdx, %rdi
	movl	%r10d, 8(%r9)
	movl	%esi, 12(%r9)
	movq	%rcx, (%rdi)
	movq	$0, 8(%rdi)
	jmp	.L1935
	.p2align 4,,10
	.p2align 3
.L1953:
	movq	%r11, %rdi
	movl	%r10d, -76(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r9
	movl	-76(%rbp), %r10d
	movq	%rax, %rdi
	jmp	.L1937
	.p2align 4,,10
	.p2align 3
.L1947:
	movq	(%rbx), %rax
	movq	-56(%rbp), %rdx
	movq	%r12, %rsi
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1946:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17RegExpDisjunction27RationalizeConsecutiveAtomsEPNS0_14RegExpCompilerE
	jmp	.L1926
	.p2align 4,,10
	.p2align 3
.L1949:
	movl	$16, %esi
	movq	%r14, %rdi
	movl	%edx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-64(%rbp), %edx
	movq	%rax, %r15
	jmp	.L1930
	.p2align 4,,10
	.p2align 3
.L1948:
	movl	$72, %esi
	movq	%r14, %rdi
	movl	%edx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-64(%rbp), %edx
	movq	%rax, %r13
	jmp	.L1928
.L1951:
	movq	%r14, %rdi
	movl	%edx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-64(%rbp), %edx
	jmp	.L1933
	.cfi_endproc
.LFE10004:
	.size	_ZN2v88internal17RegExpDisjunction6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, .-_ZN2v88internal17RegExpDisjunction6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal14CharacterRange18AddCaseEquivalentsEPNS0_7IsolateEPNS0_4ZoneEPNS0_8ZoneListIS1_EEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14CharacterRange18AddCaseEquivalentsEPNS0_7IsolateEPNS0_4ZoneEPNS0_8ZoneListIS1_EEb
	.type	_ZN2v88internal14CharacterRange18AddCaseEquivalentsEPNS0_7IsolateEPNS0_4ZoneEPNS0_8ZoneListIS1_EEb, @function
_ZN2v88internal14CharacterRange18AddCaseEquivalentsEPNS0_7IsolateEPNS0_4ZoneEPNS0_8ZoneListIS1_EEb:
.LFB10039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1088(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	subq	$1128, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -1152(%rbp)
	movq	%rdx, -1128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal14CharacterRange12CanonicalizeEPNS0_8ZoneListIS1_EE
	movl	12(%rdi), %ebx
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	testl	%ebx, %ebx
	jle	.L1955
	leal	-1(%rbx), %eax
	testb	%r12b, %r12b
	je	.L2030
	leaq	8(,%rax,8), %rax
	xorl	%ebx, %ebx
	movq	%rax, -1112(%rbp)
.L1965:
	movq	-1128(%rbp), %rcx
	movq	(%rcx), %rax
	addq	%rbx, %rax
	movl	(%rax), %r12d
	cmpl	$65535, %r12d
	jg	.L1961
	movl	4(%rax), %r15d
	movl	$65535, %r13d
	cmpl	$65535, %r15d
	cmovle	%r15d, %r13d
	cmpl	$55295, %r12d
	jle	.L1995
	cmpl	$57343, %r15d
	jle	.L1961
.L1995:
	movq	%r15, %rax
	movl	%r12d, %edi
	salq	$32, %rax
	orq	%rax, %rdi
	call	_ZN2v88internal30RangeContainsLatin1EquivalentsENS0_14CharacterRangeE@PLT
	testb	%al, %al
	jne	.L1964
	cmpl	$255, %r12d
	jg	.L1961
	cmpl	$256, %r15d
	movl	$255, %eax
	cmovge	%eax, %r13d
.L1964:
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
.L1961:
	addq	$8, %rbx
	cmpq	-1112(%rbp), %rbx
	jne	.L1965
.L1955:
	leaq	-880(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -1160(%rbp)
	call	_ZN6icu_6710UnicodeSetC1ERKS0_@PLT
	leaq	-672(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -1136(%rbp)
	call	_ZN6icu_6710UnicodeSetC1ERKS0_@PLT
	movzbl	_ZN2v88internalL16ascii_a_to_z_setE(%rip), %eax
	cmpb	$2, %al
	jne	.L2031
.L1966:
	movq	-1136(%rbp), %rbx
	leaq	8+_ZN2v88internalL16ascii_a_to_z_setE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSet9retainAllERKS0_@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet9removeAllERKS0_@PLT
	leaq	-464(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -1144(%rbp)
	call	_ZN6icu_6710UnicodeSetC1ERKS0_@PLT
	movzbl	_ZN2v88internalL15special_add_setE(%rip), %eax
	cmpb	$2, %al
	jne	.L2032
.L1968:
	movq	-1144(%rbp), %rbx
	leaq	8+_ZN2v88internalL15special_add_setE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSet9retainAllERKS0_@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet9removeAllERKS0_@PLT
	movzbl	_ZN2v88internalL10ignore_setE(%rip), %eax
	cmpb	$2, %al
	jne	.L2033
.L1970:
	leaq	8+_ZN2v88internalL10ignore_setE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet9removeAllERKS0_@PLT
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet9closeOverEi@PLT
	movzbl	_ZN2v88internalL16ascii_a_to_z_setE(%rip), %eax
	cmpb	$2, %al
	jne	.L2034
.L1972:
	leaq	8+_ZN2v88internalL16ascii_a_to_z_setE(%rip), %rsi
	movq	%r14, %rdi
	xorl	%ebx, %ebx
	call	_ZN6icu_6710UnicodeSet9removeAllERKS0_@PLT
	movq	-1136(%rbp), %r13
	jmp	.L1977
.L2035:
	andl	$95, %eax
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	%eax, %edx
	andl	$95, %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
.L1976:
	addl	$1, %ebx
.L1977:
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	cmpl	%ebx, %eax
	jle	.L1993
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movl	%ebx, %esi
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	testb	$32, %r12b
	jne	.L2035
	orl	$32, %eax
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	%eax, %edx
	orl	$32, %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	jmp	.L1976
.L1993:
	movl	$0, -1120(%rbp)
	leaq	-256(%rbp), %r13
.L1974:
	movq	-1144(%rbp), %r15
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	movl	-1120(%rbp), %r12d
	cmpl	%r12d, %eax
	jle	.L1978
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	%r12d, %esi
	movq	%r15, %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movl	%eax, -1112(%rbp)
	cmpl	%eax, %ebx
	jl	.L1979
	leal	1(%rbx), %eax
	movl	%eax, -1116(%rbp)
	.p2align 4,,10
	.p2align 3
.L1986:
	movl	-1112(%rbp), %edi
	call	u_isupper_67@PLT
	testb	%al, %al
	je	.L2036
.L1980:
	movl	-1112(%rbp), %edx
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	movl	%edx, %esi
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
	movq	%r13, %rdi
	movl	$2, %esi
	call	_ZN6icu_6710UnicodeSet9closeOverEi@PLT
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	cmpl	%r12d, %eax
	jle	.L1981
	.p2align 4,,10
	.p2align 3
.L2037:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	%r12d, %esi
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movl	%eax, %ebx
	cmpl	%eax, %r15d
	jl	.L1982
	addl	$1, %r15d
	jmp	.L1984
	.p2align 4,,10
	.p2align 3
.L1983:
	addl	$1, %ebx
	cmpl	%r15d, %ebx
	je	.L1982
.L1984:
	movl	%ebx, %edi
	call	u_isupper_67@PLT
	testb	%al, %al
	jne	.L1983
	movl	%ebx, %esi
	movq	%r14, %rdi
	addl	$1, %ebx
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	cmpl	%r15d, %ebx
	jne	.L1984
.L1982:
	movq	%r13, %rdi
	addl	$1, %r12d
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	cmpl	%r12d, %eax
	jg	.L2037
.L1981:
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	addl	$1, -1112(%rbp)
	movl	-1112(%rbp), %eax
	cmpl	-1116(%rbp), %eax
	jne	.L1986
.L1979:
	addl	$1, -1120(%rbp)
	jmp	.L1974
.L2036:
	movl	-1112(%rbp), %edi
	call	u_toupper_67@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	jmp	.L1980
.L1978:
	movq	-1160(%rbp), %rsi
	movq	%r14, %rdi
	xorl	%ebx, %ebx
	leaq	-1096(%rbp), %r12
	call	_ZN6icu_6710UnicodeSet9removeAllERKS0_@PLT
	movq	-1128(%rbp), %r15
	jmp	.L1990
.L1988:
	movq	-1152(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	%eax, -1092(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
.L1989:
	addl	$1, %ebx
.L1990:
	movq	%r14, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	cmpl	%ebx, %eax
	jle	.L1987
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movl	%ebx, %esi
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	%r13d, -1096(%rbp)
	cmpl	%eax, %r13d
	jne	.L1988
	movq	-1152(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	%r13d, -1092(%rbp)
	call	_ZN2v88internal8ZoneListINS0_14CharacterRangeEE3AddERKS2_PNS0_4ZoneE
	jmp	.L1989
.L1987:
	movq	-1144(%rbp), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-1136(%rbp), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-1160(%rbp), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2038
	addq	$1128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2030:
	.cfi_restore_state
	leaq	8(,%rax,8), %r12
	xorl	%r13d, %r13d
	movl	$65535, %ebx
.L1959:
	movq	-1128(%rbp), %rcx
	movq	(%rcx), %rax
	addq	%r13, %rax
	movl	(%rax), %esi
	cmpl	$65535, %esi
	jg	.L1957
	movl	4(%rax), %eax
	movl	%ebx, %edx
	cmpl	$65535, %eax
	cmovle	%eax, %edx
	cmpl	$57343, %eax
	jg	.L1994
	cmpl	$55295, %esi
	jg	.L1957
.L1994:
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
.L1957:
	addq	$8, %r13
	cmpq	%r12, %r13
	jne	.L1959
	jmp	.L1955
.L2034:
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal12AsciiAToZSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -256(%rbp)
	leaq	8+_ZN2v88internalL16ascii_a_to_z_setE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-256(%rbp), %r12
	movq	%rax, -248(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZN2v88internalL16ascii_a_to_z_setE(%rip), %rdi
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-240(%rbp), %rax
	testq	%rax, %rax
	je	.L1972
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1972
.L2033:
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal9IgnoreSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -256(%rbp)
	leaq	8+_ZN2v88internalL10ignore_setE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-256(%rbp), %r12
	movq	%rax, -248(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZN2v88internalL10ignore_setE(%rip), %rdi
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-240(%rbp), %rax
	testq	%rax, %rax
	je	.L1970
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1970
.L2032:
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal13SpecialAddSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -256(%rbp)
	leaq	8+_ZN2v88internalL15special_add_setE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-256(%rbp), %r12
	movq	%rax, -248(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZN2v88internalL15special_add_setE(%rip), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-240(%rbp), %rax
	testq	%rax, %rax
	je	.L1968
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1968
.L2031:
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal12AsciiAToZSetENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -256(%rbp)
	leaq	8+_ZN2v88internalL16ascii_a_to_z_setE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-256(%rbp), %r12
	movq	%rax, -248(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZN2v88internalL16ascii_a_to_z_setE(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-240(%rbp), %rax
	testq	%rax, %rax
	je	.L1966
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1966
.L2038:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10039:
	.size	_ZN2v88internal14CharacterRange18AddCaseEquivalentsEPNS0_7IsolateEPNS0_4ZoneEPNS0_8ZoneListIS1_EEb, .-_ZN2v88internal14CharacterRange18AddCaseEquivalentsEPNS0_7IsolateEPNS0_4ZoneEPNS0_8ZoneListIS1_EEb
	.section	.bss._ZN2v88internalL16ascii_a_to_z_setE,"aw",@nobits
	.align 32
	.type	_ZN2v88internalL16ascii_a_to_z_setE, @object
	.size	_ZN2v88internalL16ascii_a_to_z_setE, 208
_ZN2v88internalL16ascii_a_to_z_setE:
	.zero	208
	.section	.bss._ZN2v88internalL15special_add_setE,"aw",@nobits
	.align 32
	.type	_ZN2v88internalL15special_add_setE, @object
	.size	_ZN2v88internalL15special_add_setE, 208
_ZN2v88internalL15special_add_setE:
	.zero	208
	.section	.bss._ZN2v88internalL10ignore_setE,"aw",@nobits
	.align 32
	.type	_ZN2v88internalL10ignore_setE, @object
	.size	_ZN2v88internalL10ignore_setE, 208
_ZN2v88internalL10ignore_setE:
	.zero	208
	.section	.rodata._ZN2v88internal25regexp_compiler_constantsL11kWordRangesE,"a"
	.align 32
	.type	_ZN2v88internal25regexp_compiler_constantsL11kWordRangesE, @object
	.size	_ZN2v88internal25regexp_compiler_constantsL11kWordRangesE, 36
_ZN2v88internal25regexp_compiler_constantsL11kWordRangesE:
	.long	48
	.long	58
	.long	65
	.long	91
	.long	95
	.long	96
	.long	97
	.long	123
	.long	1114112
	.section	.rodata._ZN2v88internal25regexp_compiler_constantsL12kSpaceRangesE,"a"
	.align 32
	.type	_ZN2v88internal25regexp_compiler_constantsL12kSpaceRangesE, @object
	.size	_ZN2v88internal25regexp_compiler_constantsL12kSpaceRangesE, 84
_ZN2v88internal25regexp_compiler_constantsL12kSpaceRangesE:
	.long	9
	.long	14
	.long	32
	.long	33
	.long	160
	.long	161
	.long	5760
	.long	5761
	.long	8192
	.long	8203
	.long	8232
	.long	8234
	.long	8239
	.long	8240
	.long	8287
	.long	8288
	.long	12288
	.long	12289
	.long	65279
	.long	65280
	.long	1114112
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
