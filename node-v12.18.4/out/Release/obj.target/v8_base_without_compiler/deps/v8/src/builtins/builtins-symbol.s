	.file	"builtins-symbol.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L14
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL28Builtin_Impl_Stats_SymbolForEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internalL28Builtin_Impl_Stats_SymbolForEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"V8.Builtin_SymbolFor"
	.section	.text._ZN2v88internalL28Builtin_Impl_Stats_SymbolForEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Builtin_Impl_Stats_SymbolForEiPmPNS0_7IsolateE, @function
_ZN2v88internalL28Builtin_Impl_Stats_SymbolForEiPmPNS0_7IsolateE:
.LFB20043:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L54
.L16:
	movq	_ZZN2v88internalL28Builtin_Impl_Stats_SymbolForEiPmPNS0_7IsolateEE27trace_event_unique_atomic37(%rip), %r13
	testq	%r13, %r13
	je	.L55
.L18:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L56
.L20:
	addl	$1, 41104(%r12)
	leaq	-8(%rbx), %rsi
	leaq	88(%r12), %rax
	cmpl	$5, %r15d
	cmovle	%rax, %rsi
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L26
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L57
.L28:
	movq	%rsi, %rdx
	xorl	%ecx, %ecx
	movl	$575, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate9SymbolForENS0_9RootIndexENS0_6HandleINS0_6StringEEEb@PLT
	movq	(%rax), %r15
.L30:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L33
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L33:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L58
.L15:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L59
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L28
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L28
	.p2align 4,,10
	.p2align 3
.L57:
	movq	312(%r12), %r15
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L56:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L60
.L21:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	leaq	.LC1(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L55:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L61
.L19:
	movq	%r13, _ZZN2v88internalL28Builtin_Impl_Stats_SymbolForEiPmPNS0_7IsolateEE27trace_event_unique_atomic37(%rip)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L58:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L54:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$841, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L61:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L60:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L21
.L59:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20043:
	.size	_ZN2v88internalL28Builtin_Impl_Stats_SymbolForEiPmPNS0_7IsolateE, .-_ZN2v88internalL28Builtin_Impl_Stats_SymbolForEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Builtin_Impl_Stats_SymbolConstructorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"V8.Builtin_SymbolConstructor"
	.section	.text._ZN2v88internalL36Builtin_Impl_Stats_SymbolConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_Stats_SymbolConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_Stats_SymbolConstructorEiPmPNS0_7IsolateE:
.LFB20040:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L112
.L63:
	movq	_ZZN2v88internalL36Builtin_Impl_Stats_SymbolConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic18(%rip), %r13
	testq	%r13, %r13
	je	.L113
.L65:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L114
.L67:
	leal	-8(,%r14,8), %eax
	movq	%rbx, %rcx
	addl	$1, 41104(%r12)
	movq	41088(%r12), %r15
	cltq
	movq	41096(%r12), %r13
	subq	%rax, %rcx
	movq	%rcx, %rax
	movq	88(%r12), %rcx
	cmpq	%rcx, (%rax)
	je	.L71
	leaq	3376(%r12), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$90, %esi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
.L72:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L85
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L85:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L115
.L62:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L116
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE@PLT
	leaq	-8(%rbx), %rsi
	cmpl	$5, %r14d
	movq	%rax, %rcx
	leaq	88(%r12), %rax
	cmovle	%rax, %rsi
	movq	(%rsi), %rax
	cmpq	88(%r12), %rax
	jne	.L117
.L75:
	movq	(%rcx), %r14
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L114:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L118
.L68:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L69
	movq	(%rdi), %rax
	call	*8(%rax)
.L69:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L70
	movq	(%rdi), %rax
	call	*8(%rax)
.L70:
	leaq	.LC2(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L113:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L119
.L66:
	movq	%r13, _ZZN2v88internalL36Builtin_Impl_Stats_SymbolConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic18(%rip)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L117:
	testb	$1, %al
	jne	.L76
.L79:
	movq	%r12, %rdi
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-168(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L120
.L78:
	movq	(%rcx), %rbx
	movq	(%rsi), %r14
	movq	%r14, 15(%rbx)
	leaq	15(%rbx), %rsi
	testb	$1, %r14b
	je	.L75
	movq	%r14, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -168(%rbp)
	testl	$262144, %eax
	jne	.L121
.L81:
	testb	$24, %al
	je	.L75
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L75
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-168(%rbp), %rcx
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L115:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L112:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$840, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L119:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L118:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC2(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L76:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L79
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movq	%rcx, -184(%rbp)
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-168(%rbp), %r8
	movq	-184(%rbp), %rcx
	movq	-176(%rbp), %rsi
	movq	8(%r8), %rax
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L120:
	movq	312(%r12), %r14
	jmp	.L72
.L116:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20040:
	.size	_ZN2v88internalL36Builtin_Impl_Stats_SymbolConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_Stats_SymbolConstructorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Builtin_Impl_Stats_SymbolKeyForEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"V8.Builtin_SymbolKeyFor"
	.section	.text._ZN2v88internalL31Builtin_Impl_Stats_SymbolKeyForEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Builtin_Impl_Stats_SymbolKeyForEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Builtin_Impl_Stats_SymbolKeyForEiPmPNS0_7IsolateE:
.LFB20046:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L160
.L123:
	movq	_ZZN2v88internalL31Builtin_Impl_Stats_SymbolKeyForEiPmPNS0_7IsolateEE27trace_event_unique_atomic47(%rip), %r13
	testq	%r13, %r13
	je	.L161
.L125:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L162
.L127:
	addl	$1, 41104(%r12)
	leaq	-8(%rbx), %rdx
	leaq	88(%r12), %rsi
	cmpl	$5, %r14d
	cmovle	%rsi, %rdx
	movq	41088(%r12), %r9
	movq	41096(%r12), %r15
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L133
.L135:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$169, %esi
	movq	%r12, %rdi
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	-168(%rbp), %r9
	movq	%rax, %r13
.L134:
	subl	$1, 41104(%r12)
	movq	%r9, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L140
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L140:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L163
.L122:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L164
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L135
	testb	$4, 11(%rax)
	je	.L136
	movq	15(%rax), %r13
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L162:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L165
.L128:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L129
	movq	(%rdi), %rax
	call	*8(%rax)
.L129:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L130
	movq	(%rdi), %rax
	call	*8(%rax)
.L130:
	leaq	.LC3(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L161:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L166
.L126:
	movq	%r13, _ZZN2v88internalL31Builtin_Impl_Stats_SymbolKeyForEiPmPNS0_7IsolateEE27trace_event_unique_atomic47(%rip)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L163:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L160:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$842, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L166:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L165:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC3(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L136:
	movq	88(%r12), %r13
	jmp	.L134
.L164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20046:
	.size	_ZN2v88internalL31Builtin_Impl_Stats_SymbolKeyForEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Builtin_Impl_Stats_SymbolKeyForEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Builtin_SymbolConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Builtin_SymbolConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Builtin_SymbolConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Builtin_SymbolConstructorEiPmPNS0_7IsolateE:
.LFB20041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L191
	leal	-8(,%rdi,8), %eax
	movq	%rsi, %rcx
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	cltq
	movq	41096(%rdx), %rbx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movq	88(%rdx), %rcx
	cmpq	%rcx, (%rax)
	je	.L169
	leaq	3376(%rdx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$90, %esi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L170:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L167
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L167:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	movl	$1, %esi
	movq	%rdx, %rdi
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE@PLT
	leaq	-8(%r13), %rsi
	cmpl	$5, %r15d
	movq	%rax, %rcx
	leaq	88(%r12), %rax
	cmovle	%rax, %rsi
	movq	(%rsi), %rax
	cmpq	88(%r12), %rax
	jne	.L192
.L173:
	movq	(%rcx), %r13
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L192:
	testb	$1, %al
	jne	.L174
.L177:
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-56(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L193
.L176:
	movq	(%rcx), %r15
	movq	(%rsi), %r13
	movq	%r13, 15(%r15)
	leaq	15(%r15), %rsi
	testb	$1, %r13b
	je	.L173
	movq	%r13, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -56(%rbp)
	testl	$262144, %eax
	jne	.L194
.L179:
	testb	$24, %al
	je	.L173
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L173
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L191:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL36Builtin_Impl_Stats_SymbolConstructorEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rcx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r8
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%r8), %rax
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L174:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L177
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L193:
	movq	312(%r12), %r13
	jmp	.L170
	.cfi_endproc
.LFE20041:
	.size	_ZN2v88internal25Builtin_SymbolConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal25Builtin_SymbolConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal17Builtin_SymbolForEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17Builtin_SymbolForEiPmPNS0_7IsolateE
	.type	_ZN2v88internal17Builtin_SymbolForEiPmPNS0_7IsolateE, @function
_ZN2v88internal17Builtin_SymbolForEiPmPNS0_7IsolateE:
.LFB20044:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L208
	addl	$1, 41104(%rdx)
	leaq	88(%rdx), %rax
	subq	$8, %rsi
	cmpl	$5, %edi
	cmovle	%rax, %rsi
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L199
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L209
.L201:
	movq	%rsi, %rdx
	xorl	%ecx, %ecx
	movl	$575, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate9SymbolForENS0_9RootIndexENS0_6HandleINS0_6StringEEEb@PLT
	movq	(%rax), %r14
.L203:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L195
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L195:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L201
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L201
	.p2align 4,,10
	.p2align 3
.L209:
	movq	312(%r12), %r14
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L208:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL28Builtin_Impl_Stats_SymbolForEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20044:
	.size	_ZN2v88internal17Builtin_SymbolForEiPmPNS0_7IsolateE, .-_ZN2v88internal17Builtin_SymbolForEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Builtin_SymbolKeyForEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Builtin_SymbolKeyForEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Builtin_SymbolKeyForEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Builtin_SymbolKeyForEiPmPNS0_7IsolateE:
.LFB20047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L222
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	subq	$8, %rsi
	leaq	88(%rdx), %rdx
	cmpl	$5, %edi
	movq	41008(%rdx), %rbx
	cmovg	%rsi, %rdx
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L214
.L216:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$169, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L215:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L210
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L210:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L216
	testb	$4, 11(%rax)
	je	.L217
	movq	15(%rax), %r13
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L222:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Builtin_Impl_Stats_SymbolKeyForEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	movq	88(%r12), %r13
	jmp	.L215
	.cfi_endproc
.LFE20047:
	.size	_ZN2v88internal20Builtin_SymbolKeyForEiPmPNS0_7IsolateE, .-_ZN2v88internal20Builtin_SymbolKeyForEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal25Builtin_SymbolConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal25Builtin_SymbolConstructorEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal25Builtin_SymbolConstructorEiPmPNS0_7IsolateE:
.LFB24982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24982:
	.size	_GLOBAL__sub_I__ZN2v88internal25Builtin_SymbolConstructorEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal25Builtin_SymbolConstructorEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal25Builtin_SymbolConstructorEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL31Builtin_Impl_Stats_SymbolKeyForEiPmPNS0_7IsolateEE27trace_event_unique_atomic47,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Builtin_Impl_Stats_SymbolKeyForEiPmPNS0_7IsolateEE27trace_event_unique_atomic47, @object
	.size	_ZZN2v88internalL31Builtin_Impl_Stats_SymbolKeyForEiPmPNS0_7IsolateEE27trace_event_unique_atomic47, 8
_ZZN2v88internalL31Builtin_Impl_Stats_SymbolKeyForEiPmPNS0_7IsolateEE27trace_event_unique_atomic47:
	.zero	8
	.section	.bss._ZZN2v88internalL28Builtin_Impl_Stats_SymbolForEiPmPNS0_7IsolateEE27trace_event_unique_atomic37,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Builtin_Impl_Stats_SymbolForEiPmPNS0_7IsolateEE27trace_event_unique_atomic37, @object
	.size	_ZZN2v88internalL28Builtin_Impl_Stats_SymbolForEiPmPNS0_7IsolateEE27trace_event_unique_atomic37, 8
_ZZN2v88internalL28Builtin_Impl_Stats_SymbolForEiPmPNS0_7IsolateEE27trace_event_unique_atomic37:
	.zero	8
	.section	.bss._ZZN2v88internalL36Builtin_Impl_Stats_SymbolConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic18,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Builtin_Impl_Stats_SymbolConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic18, @object
	.size	_ZZN2v88internalL36Builtin_Impl_Stats_SymbolConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic18, 8
_ZZN2v88internalL36Builtin_Impl_Stats_SymbolConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic18:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
