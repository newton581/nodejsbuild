	.file	"snapshot-common.cc"
	.text
	.section	.text._ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB7689:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rcx), %r8
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7689:
	.size	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.text._ZNK2v88internal12SnapshotData7PayloadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12SnapshotData7PayloadEv
	.type	_ZNK2v88internal12SnapshotData7PayloadEv, @function
_ZNK2v88internal12SnapshotData7PayloadEv:
.LFB18868:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	4(%rax), %edx
	movl	8(%rax), %r8d
	leal	19(,%rdx,4), %edx
	andl	$-8, %edx
	addq	%rdx, %rax
	movq	%r8, %rdx
	ret
	.cfi_endproc
.LFE18868:
	.size	_ZNK2v88internal12SnapshotData7PayloadEv, .-_ZNK2v88internal12SnapshotData7PayloadEv
	.section	.text._ZN2v88internal14SerializedDataD2Ev,"axG",@progbits,_ZN2v88internal14SerializedDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14SerializedDataD2Ev
	.type	_ZN2v88internal14SerializedDataD2Ev, @function
_ZN2v88internal14SerializedDataD2Ev:
.LFB8870:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14SerializedDataE(%rip), %rax
	cmpb	$0, 20(%rdi)
	movq	%rax, (%rdi)
	je	.L4
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	ret
	.cfi_endproc
.LFE8870:
	.size	_ZN2v88internal14SerializedDataD2Ev, .-_ZN2v88internal14SerializedDataD2Ev
	.weak	_ZN2v88internal14SerializedDataD1Ev
	.set	_ZN2v88internal14SerializedDataD1Ev,_ZN2v88internal14SerializedDataD2Ev
	.section	.text._ZN2v88internal14SerializedDataD0Ev,"axG",@progbits,_ZN2v88internal14SerializedDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14SerializedDataD0Ev
	.type	_ZN2v88internal14SerializedDataD0Ev, @function
_ZN2v88internal14SerializedDataD0Ev:
.LFB8872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14SerializedDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 20(%rdi)
	movq	%rax, (%rdi)
	je	.L10
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	_ZdaPv@PLT
.L10:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8872:
	.size	_ZN2v88internal14SerializedDataD0Ev, .-_ZN2v88internal14SerializedDataD0Ev
	.section	.text._ZN2v88internal12SnapshotDataD2Ev,"axG",@progbits,_ZN2v88internal12SnapshotDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12SnapshotDataD2Ev
	.type	_ZN2v88internal12SnapshotDataD2Ev, @function
_ZN2v88internal12SnapshotDataD2Ev:
.LFB23655:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14SerializedDataE(%rip), %rax
	cmpb	$0, 20(%rdi)
	movq	%rax, (%rdi)
	je	.L15
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L15
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	ret
	.cfi_endproc
.LFE23655:
	.size	_ZN2v88internal12SnapshotDataD2Ev, .-_ZN2v88internal12SnapshotDataD2Ev
	.weak	_ZN2v88internal12SnapshotDataD1Ev
	.set	_ZN2v88internal12SnapshotDataD1Ev,_ZN2v88internal12SnapshotDataD2Ev
	.section	.text._ZN2v88internal12SnapshotDataD0Ev,"axG",@progbits,_ZN2v88internal12SnapshotDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12SnapshotDataD0Ev
	.type	_ZN2v88internal12SnapshotDataD0Ev, @function
_ZN2v88internal12SnapshotDataD0Ev:
.LFB23657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14SerializedDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 20(%rdi)
	movq	%rax, (%rdi)
	je	.L21
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L21
	call	_ZdaPv@PLT
.L21:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23657:
	.size	_ZN2v88internal12SnapshotDataD0Ev, .-_ZN2v88internal12SnapshotDataD0Ev
	.section	.text._ZN2v88internal19StartupDeserializerD2Ev,"axG",@progbits,_ZN2v88internal19StartupDeserializerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19StartupDeserializerD2Ev
	.type	_ZN2v88internal19StartupDeserializerD2Ev, @function
_ZN2v88internal19StartupDeserializerD2Ev:
.LFB23659:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal19StartupDeserializerE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal12DeserializerD2Ev@PLT
	.cfi_endproc
.LFE23659:
	.size	_ZN2v88internal19StartupDeserializerD2Ev, .-_ZN2v88internal19StartupDeserializerD2Ev
	.weak	_ZN2v88internal19StartupDeserializerD1Ev
	.set	_ZN2v88internal19StartupDeserializerD1Ev,_ZN2v88internal19StartupDeserializerD2Ev
	.section	.text._ZN2v88internal19StartupDeserializerD0Ev,"axG",@progbits,_ZN2v88internal19StartupDeserializerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19StartupDeserializerD0Ev
	.type	_ZN2v88internal19StartupDeserializerD0Ev, @function
_ZN2v88internal19StartupDeserializerD0Ev:
.LFB23661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal19StartupDeserializerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal12DeserializerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$624, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23661:
	.size	_ZN2v88internal19StartupDeserializerD0Ev, .-_ZN2v88internal19StartupDeserializerD0Ev
	.section	.text._ZN2v88internal20ReadOnlyDeserializerD2Ev,"axG",@progbits,_ZN2v88internal20ReadOnlyDeserializerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20ReadOnlyDeserializerD2Ev
	.type	_ZN2v88internal20ReadOnlyDeserializerD2Ev, @function
_ZN2v88internal20ReadOnlyDeserializerD2Ev:
.LFB23663:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal20ReadOnlyDeserializerE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal12DeserializerD2Ev@PLT
	.cfi_endproc
.LFE23663:
	.size	_ZN2v88internal20ReadOnlyDeserializerD2Ev, .-_ZN2v88internal20ReadOnlyDeserializerD2Ev
	.weak	_ZN2v88internal20ReadOnlyDeserializerD1Ev
	.set	_ZN2v88internal20ReadOnlyDeserializerD1Ev,_ZN2v88internal20ReadOnlyDeserializerD2Ev
	.section	.text._ZN2v88internal20ReadOnlyDeserializerD0Ev,"axG",@progbits,_ZN2v88internal20ReadOnlyDeserializerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20ReadOnlyDeserializerD0Ev
	.type	_ZN2v88internal20ReadOnlyDeserializerD0Ev, @function
_ZN2v88internal20ReadOnlyDeserializerD0Ev:
.LFB23665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal20ReadOnlyDeserializerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal12DeserializerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$624, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23665:
	.size	_ZN2v88internal20ReadOnlyDeserializerD0Ev, .-_ZN2v88internal20ReadOnlyDeserializerD0Ev
	.section	.rodata._ZN2v88internal12_GLOBAL__N_112RunExtraCodeEPNS_7IsolateENS_5LocalINS_7ContextEEEPKcS8_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"!try_catch.HasCaught()"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal12_GLOBAL__N_112RunExtraCodeEPNS_7IsolateENS_5LocalINS_7ContextEEEPKcS8_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_112RunExtraCodeEPNS_7IsolateENS_5LocalINS_7ContextEEEPKcS8_, @function
_ZN2v88internal12_GLOBAL__N_112RunExtraCodeEPNS_7IsolateENS_5LocalINS_7ContextEEEPKcS8_:
.LFB18869:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context5EnterEv@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	je	.L60
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r8, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L61
.L41:
	movq	%rax, %xmm1
	movq	%rbx, %xmm0
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	punpcklqdq	%xmm1, %xmm0
	leaq	-128(%rbp), %rsi
	xorl	%edx, %edx
	movl	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movq	$0, -72(%rbp)
	movaps	%xmm0, -112(%rbp)
	movups	%xmm0, -88(%rbp)
	call	_ZN2v814ScriptCompiler7CompileENS_5LocalINS_7ContextEEEPNS0_6SourceENS0_14CompileOptionsENS0_13NoCacheReasonE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L35
	movq	%r12, %rsi
	call	_ZN2v86Script3RunENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L35
	movq	%r14, %rdi
	movl	$1, %r15d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L62
.L37:
	movq	-72(%rbp), %r13
	testq	%r13, %r13
	je	.L33
	movq	%r13, %rdi
	call	_ZN2v814ScriptCompiler10CachedDataD1Ev@PLT
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L33:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L63
	addq	$152, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	xorl	%r15d, %r15d
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L60:
	xorl	%r15d, %r15d
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%rax, -184(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-184(%rbp), %rax
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L63:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18869:
	.size	_ZN2v88internal12_GLOBAL__N_112RunExtraCodeEPNS_7IsolateENS_5LocalINS_7ContextEEEPKcS8_, .-_ZN2v88internal12_GLOBAL__N_112RunExtraCodeEPNS_7IsolateENS_5LocalINS_7ContextEEEPKcS8_
	.section	.rodata._ZN2v88internal8Snapshot18HasContextSnapshotEPNS0_7IsolateEm.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"kNumberOfContextsOffset < data->raw_size"
	.section	.text._ZN2v88internal8Snapshot18HasContextSnapshotEPNS0_7IsolateEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Snapshot18HasContextSnapshotEPNS0_7IsolateEm
	.type	_ZN2v88internal8Snapshot18HasContextSnapshotEPNS0_7IsolateEm, @function
_ZN2v88internal8Snapshot18HasContextSnapshotEPNS0_7IsolateEm:
.LFB18847:
	.cfi_startproc
	endbr64
	movq	41792(%rdi), %rax
	testq	%rax, %rax
	je	.L68
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L68
	movl	8(%rax), %eax
	testl	%eax, %eax
	jle	.L72
	movl	(%rdx), %eax
	cmpq	%rsi, %rax
	seta	%al
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18847:
	.size	_ZN2v88internal8Snapshot18HasContextSnapshotEPNS0_7IsolateEm, .-_ZN2v88internal8Snapshot18HasContextSnapshotEPNS0_7IsolateEm
	.section	.text._ZN2v88internal8Snapshot18ExtractNumContextsEPKNS_11StartupDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Snapshot18ExtractNumContextsEPKNS_11StartupDataE
	.type	_ZN2v88internal8Snapshot18ExtractNumContextsEPKNS_11StartupDataE, @function
_ZN2v88internal8Snapshot18ExtractNumContextsEPKNS_11StartupDataE:
.LFB18855:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L78
	movq	(%rdi), %rax
	movl	(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18855:
	.size	_ZN2v88internal8Snapshot18ExtractNumContextsEPKNS_11StartupDataE, .-_ZN2v88internal8Snapshot18ExtractNumContextsEPKNS_11StartupDataE
	.section	.rodata._ZN2v88internal8Snapshot14VerifyChecksumEPKNS_11StartupDataE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"[Verifying snapshot checksum took %0.3f ms]\n"
	.section	.text._ZN2v88internal8Snapshot14VerifyChecksumEPKNS_11StartupDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Snapshot14VerifyChecksumEPKNS_11StartupDataE
	.type	_ZN2v88internal8Snapshot14VerifyChecksumEPKNS_11StartupDataE, @function
_ZN2v88internal8Snapshot14VerifyChecksumEPKNS_11StartupDataE:
.LFB18856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	_ZN2v88internal28FLAG_profile_deserializationE(%rip), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%r8b, %r8b
	jne	.L94
.L80:
	movq	(%r14), %rax
	movl	8(%r14), %ecx
	movl	8(%rax), %ebx
	movl	12(%rax), %r12d
	leal	-9(%rcx), %edx
	addq	$16, %rax
	subl	$16, %ecx
	cmovns	%ecx, %edx
	sarl	$3, %edx
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,8), %rsi
	cmpq	%rax, %rsi
	jbe	.L86
	xorl	%ecx, %ecx
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L82:
	addq	$8, %rax
	addq	-8(%rax), %rdx
	addq	%rdx, %rcx
	cmpq	%rax, %rsi
	ja	.L82
	movq	%rdx, %r14
	movq	%rcx, %r15
	shrq	$32, %r14
	shrq	$32, %r15
	xorl	%edx, %r14d
	xorl	%ecx, %r15d
.L81:
	testb	%r8b, %r8b
	jne	.L95
.L83:
	cmpl	%r15d, %r12d
	sete	%al
	cmpl	%r14d, %ebx
	sete	%dl
	andl	%edx, %eax
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L96
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movzbl	_ZN2v88internal28FLAG_profile_deserializationE(%rip), %r8d
	movq	%rax, %r13
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L95:
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-64(%rbp), %rdi
	subq	%r13, %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	leaq	.LC3(%rip), %rdi
	movl	$1, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L86:
	xorl	%r15d, %r15d
	movl	$1, %r14d
	jmp	.L81
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18856:
	.size	_ZN2v88internal8Snapshot14VerifyChecksumEPKNS_11StartupDataE, .-_ZN2v88internal8Snapshot14VerifyChecksumEPKNS_11StartupDataE
	.section	.rodata._ZN2v88internal8Snapshot20ExtractContextOffsetEPKNS_11StartupDataEj.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"context_offset < static_cast<uint32_t>(data->raw_size)"
	.section	.text._ZN2v88internal8Snapshot20ExtractContextOffsetEPKNS_11StartupDataEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Snapshot20ExtractContextOffsetEPKNS_11StartupDataEj
	.type	_ZN2v88internal8Snapshot20ExtractContextOffsetEPKNS_11StartupDataEj, @function
_ZN2v88internal8Snapshot20ExtractContextOffsetEPKNS_11StartupDataEj:
.LFB18857:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	leal	84(,%rsi,4), %eax
	movl	(%rdx,%rax), %eax
	cmpl	%eax, 8(%rdi)
	jbe	.L102
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18857:
	.size	_ZN2v88internal8Snapshot20ExtractContextOffsetEPKNS_11StartupDataEj, .-_ZN2v88internal8Snapshot20ExtractContextOffsetEPKNS_11StartupDataEj
	.section	.rodata._ZN2v88internal8Snapshot20ExtractRehashabilityEPKNS_11StartupDataE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"kRehashabilityOffset < static_cast<uint32_t>(data->raw_size)"
	.align 8
.LC6:
	.string	"rehashability != 0 implies rehashability == 1"
	.section	.text._ZN2v88internal8Snapshot20ExtractRehashabilityEPKNS_11StartupDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Snapshot20ExtractRehashabilityEPKNS_11StartupDataE
	.type	_ZN2v88internal8Snapshot20ExtractRehashabilityEPKNS_11StartupDataE, @function
_ZN2v88internal8Snapshot20ExtractRehashabilityEPKNS_11StartupDataE:
.LFB18858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpl	$4, 8(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jbe	.L107
	movq	(%rdi), %rax
	movl	4(%rax), %eax
	cmpl	$1, %eax
	ja	.L108
	andl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L108:
	leaq	.LC6(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18858:
	.size	_ZN2v88internal8Snapshot20ExtractRehashabilityEPKNS_11StartupDataE, .-_ZN2v88internal8Snapshot20ExtractRehashabilityEPKNS_11StartupDataE
	.section	.rodata._ZN2v88internal8Snapshot18ExtractStartupDataEPKNS_11StartupDataE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"start_offset < end_offset"
	.section	.rodata._ZN2v88internal8Snapshot18ExtractStartupDataEPKNS_11StartupDataE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"end_offset < snapshot->raw_size"
	.section	.text._ZN2v88internal8Snapshot18ExtractStartupDataEPKNS_11StartupDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Snapshot18ExtractStartupDataEPKNS_11StartupDataE
	.type	_ZN2v88internal8Snapshot18ExtractStartupDataEPKNS_11StartupDataE, @function
_ZN2v88internal8Snapshot18ExtractStartupDataEPKNS_11StartupDataE:
.LFB18860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	8(%rdi), %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%esi, %esi
	jle	.L114
	movq	(%rdi), %rax
	movl	(%rax), %ecx
	movl	80(%rax), %edx
	leal	91(,%rcx,4), %ecx
	andl	$-8, %ecx
	cmpl	%edx, %ecx
	jnb	.L115
	cmpl	%esi, %edx
	jnb	.L116
	movl	%ecx, %esi
	subl	%ecx, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L115:
	leaq	.LC7(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L116:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18860:
	.size	_ZN2v88internal8Snapshot18ExtractStartupDataEPKNS_11StartupDataE, .-_ZN2v88internal8Snapshot18ExtractStartupDataEPKNS_11StartupDataE
	.section	.text._ZN2v88internal8Snapshot19ExtractReadOnlyDataEPKNS_11StartupDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Snapshot19ExtractReadOnlyDataEPKNS_11StartupDataE
	.type	_ZN2v88internal8Snapshot19ExtractReadOnlyDataEPKNS_11StartupDataE, @function
_ZN2v88internal8Snapshot19ExtractReadOnlyDataEPKNS_11StartupDataE:
.LFB18861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movl	84(%rax), %edx
	movl	80(%rax), %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	%edx, %ecx
	jnb	.L123
	movl	8(%rdi), %esi
	testl	%esi, %esi
	jle	.L121
	cmpl	%esi, %edx
	jnb	.L121
	movl	%ecx, %esi
	subl	%ecx, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	leaq	.LC7(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L121:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18861:
	.size	_ZN2v88internal8Snapshot19ExtractReadOnlyDataEPKNS_11StartupDataE, .-_ZN2v88internal8Snapshot19ExtractReadOnlyDataEPKNS_11StartupDataE
	.section	.rodata._ZN2v88internal8Snapshot18ExtractContextDataEPKNS_11StartupDataEj.str1.1,"aMS",@progbits,1
.LC9:
	.string	"index < num_contexts"
	.section	.text._ZN2v88internal8Snapshot18ExtractContextDataEPKNS_11StartupDataEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Snapshot18ExtractContextDataEPKNS_11StartupDataEj
	.type	_ZN2v88internal8Snapshot18ExtractContextDataEPKNS_11StartupDataEj, @function
_ZN2v88internal8Snapshot18ExtractContextDataEPKNS_11StartupDataEj:
.LFB18862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	8(%rdi), %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%ecx, %ecx
	jle	.L132
	movq	(%rdi), %rax
	movl	(%rax), %edx
	cmpl	%edx, %esi
	jnb	.L133
	leal	84(,%rsi,4), %edi
	movl	(%rdi,%rax), %edi
	cmpl	%ecx, %edi
	jnb	.L129
	subl	$1, %edx
	cmpl	%esi, %edx
	je	.L130
	leal	88(,%rsi,4), %edx
	movl	(%rdx,%rax), %edx
	cmpl	%edx, %ecx
	jbe	.L129
	movl	%edi, %ecx
	subl	%edi, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	%rcx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movl	%ecx, %edx
	movl	%edi, %ecx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	%rcx, %rax
	subl	%edi, %edx
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L132:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L133:
	leaq	.LC9(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18862:
	.size	_ZN2v88internal8Snapshot18ExtractContextDataEPKNS_11StartupDataEj, .-_ZN2v88internal8Snapshot18ExtractContextDataEPKNS_11StartupDataEj
	.section	.rodata._ZN2v88internal8Snapshot22NewContextFromSnapshotEPNS0_7IsolateENS0_6HandleINS0_13JSGlobalProxyEEEmNS_33DeserializeInternalFieldsCallbackE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"[Deserializing context #%zu (%d bytes) took %0.3f ms]\n"
	.section	.text._ZN2v88internal8Snapshot22NewContextFromSnapshotEPNS0_7IsolateENS0_6HandleINS0_13JSGlobalProxyEEEmNS_33DeserializeInternalFieldsCallbackE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Snapshot22NewContextFromSnapshotEPNS0_7IsolateENS0_6HandleINS0_13JSGlobalProxyEEEmNS_33DeserializeInternalFieldsCallbackE
	.type	_ZN2v88internal8Snapshot22NewContextFromSnapshotEPNS0_7IsolateENS0_6HandleINS0_13JSGlobalProxyEEEmNS_33DeserializeInternalFieldsCallbackE, @function
_ZN2v88internal8Snapshot22NewContextFromSnapshotEPNS0_7IsolateENS0_6HandleINS0_13JSGlobalProxyEEEmNS_33DeserializeInternalFieldsCallbackE:
.LFB18849:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41792(%rdi), %rax
	testq	%rax, %rax
	je	.L135
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L135
	pxor	%xmm0, %xmm0
	movq	%rdi, %r12
	movq	%rsi, %r14
	movq	%rdx, %r13
	movq	$0, -64(%rbp)
	movq	%r8, %rbx
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L160
.L136:
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	jne	.L137
	movq	$0, -160(%rbp)
.L138:
	movq	41792(%r12), %rdi
	cmpl	$4, 8(%rdi)
	jbe	.L161
	movq	(%rdi), %rax
	movl	4(%rax), %r15d
	cmpl	$1, %r15d
	ja	.L162
	movl	%r13d, %esi
	movq	%rcx, -152(%rbp)
	call	_ZN2v88internal8Snapshot18ExtractContextDataEPKNS_11StartupDataEj
	movq	%r12, %rdi
	leaq	-128(%rbp), %rsi
	movq	%rbx, %r9
	movq	-152(%rbp), %rcx
	movq	%rdx, -168(%rbp)
	movq	%rax, -120(%rbp)
	leaq	16+_ZTVN2v88internal12SnapshotDataE(%rip), %rax
	movl	%edx, -112(%rbp)
	movq	%rcx, %r8
	movl	%r15d, %edx
	movq	%r14, %rcx
	movb	$0, -108(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal19PartialDeserializer18DeserializeContextEPNS0_7IsolateEPKNS0_12SnapshotDataEbNS0_6HandleINS0_13JSGlobalProxyEEENS_33DeserializeInternalFieldsCallbackE@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L143
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	jne	.L163
.L144:
	movq	%r12, %rax
.L143:
	leaq	16+_ZTVN2v88internal14SerializedDataE(%rip), %rbx
	cmpb	$0, -108(%rbp)
	movq	%rbx, -128(%rbp)
	je	.L145
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L145
	movq	%rax, -152(%rbp)
	call	_ZdaPv@PLT
	movq	-152(%rbp), %rax
.L145:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L139
	leaq	-88(%rbp), %rsi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-152(%rbp), %rax
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L135:
	xorl	%eax, %eax
.L139:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L164
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-136(%rbp), %rdi
	subq	-160(%rbp), %rax
	movq	%rax, -136(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	movl	-168(%rbp), %edx
	movq	%r13, %rsi
	leaq	.LC10(%rip), %rdi
	movl	$1, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L137:
	movq	%rcx, -152(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-152(%rbp), %rcx
	movq	%rax, -160(%rbp)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L161:
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L160:
	movq	40960(%rdi), %rax
	leaq	-88(%rbp), %rsi
	movl	$141, %edx
	movq	%rcx, -152(%rbp)
	leaq	23240(%rax), %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	-152(%rbp), %rcx
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L162:
	leaq	.LC6(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18849:
	.size	_ZN2v88internal8Snapshot22NewContextFromSnapshotEPNS0_7IsolateENS0_6HandleINS0_13JSGlobalProxyEEEmNS_33DeserializeInternalFieldsCallbackE, .-_ZN2v88internal8Snapshot22NewContextFromSnapshotEPNS0_7IsolateENS0_6HandleINS0_13JSGlobalProxyEEEmNS_33DeserializeInternalFieldsCallbackE
	.section	.rodata._ZN2v88internal8Snapshot12CheckVersionEPKNS_11StartupDataE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"kVersionStringOffset + kVersionStringLength < static_cast<uint32_t>(data->raw_size)"
	.align 8
.LC12:
	.string	"Version mismatch between V8 binary and snapshot.\n#   V8 binary version: %.*s\n#    Snapshot version: %.*s\n# The snapshot consists of %d bytes and contains %d context(s)."
	.section	.text._ZN2v88internal8Snapshot12CheckVersionEPKNS_11StartupDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Snapshot12CheckVersionEPKNS_11StartupDataE
	.type	_ZN2v88internal8Snapshot12CheckVersionEPKNS_11StartupDataE, @function
_ZN2v88internal8Snapshot12CheckVersionEPKNS_11StartupDataE:
.LFB18863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$80, 8(%rdi)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	jbe	.L171
	leaq	-112(%rbp), %r12
	movq	%rdi, %rbx
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Version9GetStringENS0_6VectorIcEE@PLT
	movq	(%rbx), %r14
	movl	$64, %edx
	movq	%r12, %rdi
	leaq	16(%r14), %r13
	movq	%r13, %rsi
	call	strncmp@PLT
	testl	%eax, %eax
	je	.L165
	movl	8(%rbx), %r9d
	testl	%r9d, %r9d
	jle	.L172
	movl	(%r14), %eax
	subq	$8, %rsp
	movq	%r13, %r8
	movl	$64, %ecx
	movq	%r12, %rdx
	movl	$64, %esi
	leaq	.LC12(%rip), %rdi
	pushq	%rax
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L165:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L173
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	leaq	.LC11(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L172:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L173:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18863:
	.size	_ZN2v88internal8Snapshot12CheckVersionEPKNS_11StartupDataE, .-_ZN2v88internal8Snapshot12CheckVersionEPKNS_11StartupDataE
	.section	.text._ZN2v88internal12SnapshotDataC2EPKNS0_10SerializerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12SnapshotDataC2EPKNS0_10SerializerE
	.type	_ZN2v88internal12SnapshotDataC2EPKNS0_10SerializerE, @function
_ZN2v88internal12SnapshotDataC2EPKNS0_10SerializerE:
.LFB18865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	addq	$216, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal12SnapshotDataE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	$0, 16(%rdi)
	movb	$0, 20(%rdi)
	leaq	-80(%rbp), %rdi
	call	_ZNK2v88internal19SerializerAllocator18EncodeReservationsEv@PLT
	movq	-72(%rbp), %r14
	subq	-80(%rbp), %r14
	movq	%rbx, %rdi
	leal	19(%r14), %r13d
	movq	88(%r12), %rsi
	subq	80(%r12), %rsi
	andl	$-8, %r13d
	addl	%r13d, %esi
	call	_ZN2v88internal14SerializedData12AllocateDataEj@PLT
	movq	8(%rbx), %rdi
	movq	%r13, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movq	8(%rbx), %rax
	movl	$-1059191884, (%rax)
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$2, %rax
	movl	%eax, 4(%rdx)
	movq	8(%rbx), %rdx
	movq	88(%r12), %rax
	subq	80(%r12), %rax
	movl	%eax, 8(%rdx)
	movq	%r14, %rdx
	movq	8(%rbx), %rdi
	andl	$4294967295, %edx
	jne	.L204
.L175:
	movq	80(%r12), %rsi
	movq	88(%r12), %rdx
	subq	%rsi, %rdx
	jne	.L205
.L182:
	movq	-80(%rbp), %r15
	testq	%r15, %r15
	jne	.L178
.L174:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L205:
	addq	%r13, %rdi
.L181:
	cmpq	$7, %rdx
	jbe	.L207
	call	memcpy@PLT
	movq	-80(%rbp), %r15
	testq	%r15, %r15
	je	.L174
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L204:
	movq	-80(%rbp), %r15
	leaq	12(%rdi), %r8
	testl	$4294967288, %r14d
	jne	.L176
	xorl	%eax, %eax
.L177:
	movzbl	(%r15,%rax), %ecx
	movb	%cl, 12(%rdi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L177
	movq	8(%rbx), %rdi
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L176:
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	memcpy@PLT
	movq	80(%r12), %rsi
	movq	88(%r12), %rdx
	subq	%rsi, %rdx
	je	.L178
	movq	8(%rbx), %rdi
	addq	%r13, %rdi
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L207:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L183:
	movzbl	(%rsi,%rax), %ecx
	movb	%cl, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L183
	jmp	.L182
.L206:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18865:
	.size	_ZN2v88internal12SnapshotDataC2EPKNS0_10SerializerE, .-_ZN2v88internal12SnapshotDataC2EPKNS0_10SerializerE
	.globl	_ZN2v88internal12SnapshotDataC1EPKNS0_10SerializerE
	.set	_ZN2v88internal12SnapshotDataC1EPKNS0_10SerializerE,_ZN2v88internal12SnapshotDataC2EPKNS0_10SerializerE
	.section	.text._ZNK2v88internal12SnapshotData12ReservationsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12SnapshotData12ReservationsEv
	.type	_ZNK2v88internal12SnapshotData12ReservationsEv, @function
_ZNK2v88internal12SnapshotData12ReservationsEv:
.LFB18867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rsi), %rax
	movl	4(%rax), %ebx
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	leaq	0(,%rbx,4), %r14
	testq	%rbx, %rbx
	je	.L209
	movq	%r14, %rdi
	call	_Znwm@PLT
	leaq	(%rax,%r14), %rcx
	movq	%rax, (%r12)
	movq	%rax, %rdi
	leaq	-1(%rbx), %rax
	movq	%rcx, 16(%r12)
	cmpq	$2, %rax
	jbe	.L214
	movq	%rbx, %rax
	movq	%rdi, %rdx
	pxor	%xmm0, %xmm0
	shrq	$2, %rax
	salq	$4, %rax
	addq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L211:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L211
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	andq	$-4, %rdx
	andl	$3, %esi
	leaq	(%rdi,%rdx,4), %rax
	cmpq	%rdx, %rbx
	je	.L213
.L210:
	movl	$0, (%rax)
	cmpq	$1, %rsi
	je	.L213
	movl	$0, 4(%rax)
	cmpq	$2, %rsi
	je	.L213
	movl	$0, 8(%rax)
.L213:
	movq	%rcx, 8(%r12)
	movq	8(%r13), %rsi
	movq	%r14, %rdx
	addq	$12, %rsi
	call	memcpy@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L214:
	movq	%rdi, %rax
	movq	%rbx, %rsi
	jmp	.L210
	.cfi_endproc
.LFE18867:
	.size	_ZNK2v88internal12SnapshotData12ReservationsEv, .-_ZNK2v88internal12SnapshotData12ReservationsEv
	.section	.rodata._ZN2v88internal22ProfileDeserializationEPKNS0_12SnapshotDataES3_RKSt6vectorIPS1_SaIS5_EE.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"Deserialization will reserve:\n"
	.section	.rodata._ZN2v88internal22ProfileDeserializationEPKNS0_12SnapshotDataES3_RKSt6vectorIPS1_SaIS5_EE.part.0.str1.1,"aMS",@progbits,1
.LC14:
	.string	"%10d bytes per isolate\n"
.LC15:
	.string	"%10d bytes per context #%zu\n"
	.section	.text._ZN2v88internal22ProfileDeserializationEPKNS0_12SnapshotDataES3_RKSt6vectorIPS1_SaIS5_EE.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal22ProfileDeserializationEPKNS0_12SnapshotDataES3_RKSt6vectorIPS1_SaIS5_EE.part.0, @function
_ZN2v88internal22ProfileDeserializationEPKNS0_12SnapshotDataES3_RKSt6vectorIPS1_SaIS5_EE.part.0:
.LFB23751:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	.LC13(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal12SnapshotData12ReservationsEv
	movq	-64(%rbp), %rdi
	movq	-56(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L238
	movq	%rdi, %rax
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L226:
	movl	(%rax), %edx
	addq	$4, %rax
	andl	$2147483647, %edx
	addl	%edx, %r12d
	cmpq	%rax, %rcx
	jne	.L226
.L225:
	testq	%rdi, %rdi
	je	.L227
	call	_ZdlPv@PLT
.L227:
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal12SnapshotData12ReservationsEv
	movq	-64(%rbp), %rdi
	movq	-56(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L228
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L229:
	movl	(%rax), %edx
	addq	$4, %rax
	andl	$2147483647, %edx
	addl	%edx, %r12d
	cmpq	%rax, %rcx
	jne	.L229
.L228:
	testq	%rdi, %rdi
	je	.L230
	call	_ZdlPv@PLT
.L230:
	xorl	%eax, %eax
	movl	%r12d, %esi
	leaq	.LC14(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	je	.L224
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L236:
	movq	(%rax,%r14,8), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal12SnapshotData12ReservationsEv
	movq	-64(%rbp), %rdi
	movq	-56(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L239
	movq	%rdi, %rax
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L233:
	movl	(%rax), %edx
	addq	$4, %rax
	andl	$2147483647, %edx
	addl	%edx, %r12d
	cmpq	%rax, %rcx
	jne	.L233
.L232:
	testq	%rdi, %rdi
	je	.L234
	call	_ZdlPv@PLT
	movq	%r14, %rdx
	xorl	%eax, %eax
	movl	%r12d, %esi
	leaq	.LC15(%rip), %rdi
	addq	$1, %r14
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%rbx), %rax
	movq	8(%rbx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r14
	jb	.L236
.L224:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L250
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movq	%r14, %rdx
	xorl	%eax, %eax
	movl	%r12d, %esi
	addq	$1, %r14
	leaq	.LC15(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%rbx), %rax
	movq	8(%rbx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%r14, %rdx
	ja	.L236
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L239:
	xorl	%r12d, %r12d
	jmp	.L232
.L238:
	xorl	%r12d, %r12d
	jmp	.L225
.L250:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23751:
	.size	_ZN2v88internal22ProfileDeserializationEPKNS0_12SnapshotDataES3_RKSt6vectorIPS1_SaIS5_EE.part.0, .-_ZN2v88internal22ProfileDeserializationEPKNS0_12SnapshotDataES3_RKSt6vectorIPS1_SaIS5_EE.part.0
	.section	.text._ZN2v88internal22ProfileDeserializationEPKNS0_12SnapshotDataES3_RKSt6vectorIPS1_SaIS5_EE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22ProfileDeserializationEPKNS0_12SnapshotDataES3_RKSt6vectorIPS1_SaIS5_EE
	.type	_ZN2v88internal22ProfileDeserializationEPKNS0_12SnapshotDataES3_RKSt6vectorIPS1_SaIS5_EE, @function
_ZN2v88internal22ProfileDeserializationEPKNS0_12SnapshotDataES3_RKSt6vectorIPS1_SaIS5_EE:
.LFB18853:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	jne	.L253
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	jmp	_ZN2v88internal22ProfileDeserializationEPKNS0_12SnapshotDataES3_RKSt6vectorIPS1_SaIS5_EE.part.0
	.cfi_endproc
.LFE18853:
	.size	_ZN2v88internal22ProfileDeserializationEPKNS0_12SnapshotDataES3_RKSt6vectorIPS1_SaIS5_EE, .-_ZN2v88internal22ProfileDeserializationEPKNS0_12SnapshotDataES3_RKSt6vectorIPS1_SaIS5_EE
	.section	.rodata._ZN2v88internal8Snapshot18CreateSnapshotBlobEPKNS0_12SnapshotDataES4_RKSt6vectorIPS2_SaIS6_EEb.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"Snapshot blob consists of:\n%10d bytes in %d chunks for startup\n"
	.section	.rodata._ZN2v88internal8Snapshot18CreateSnapshotBlobEPKNS0_12SnapshotDataES4_RKSt6vectorIPS2_SaIS6_EEb.str1.1,"aMS",@progbits,1
.LC17:
	.string	"%10d bytes for read-only\n"
	.section	.rodata._ZN2v88internal8Snapshot18CreateSnapshotBlobEPKNS0_12SnapshotDataES4_RKSt6vectorIPS2_SaIS6_EEb.str1.8
	.align 8
.LC18:
	.string	"%10d bytes in %d chunks for context #%d\n"
	.section	.text._ZN2v88internal8Snapshot18CreateSnapshotBlobEPKNS0_12SnapshotDataES4_RKSt6vectorIPS2_SaIS6_EEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Snapshot18CreateSnapshotBlobEPKNS0_12SnapshotDataES4_RKSt6vectorIPS2_SaIS6_EEb
	.type	_ZN2v88internal8Snapshot18CreateSnapshotBlobEPKNS0_12SnapshotDataES4_RKSt6vectorIPS2_SaIS6_EEb, @function
_ZN2v88internal8Snapshot18CreateSnapshotBlobEPKNS0_12SnapshotDataES4_RKSt6vectorIPS2_SaIS6_EEb:
.LFB18854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %r11
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	8(%rdx), %rcx
	movl	16(%rsi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	addl	16(%rdi), %r13d
	movq	%rcx, %r15
	subq	%rax, %r15
	sarq	$3, %r15
	leal	91(,%r15,4), %ebx
	andl	$-8, %ebx
	addl	%ebx, %r13d
	cmpq	%rcx, %rax
	je	.L255
	.p2align 4,,10
	.p2align 3
.L256:
	movq	(%rax), %rdx
	addq	$8, %rax
	addl	16(%rdx), %r13d
	cmpq	%rax, %rcx
	jne	.L256
.L255:
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	jne	.L297
.L257:
	movl	%r13d, %eax
	movq	%r9, -104(%rbp)
	movzbl	%r12b, %r12d
	movq	%rax, %rdi
	movq	%r11, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%rax, -120(%rbp)
	call	_Znam@PLT
	movl	%ebx, %edx
	xorl	%esi, %esi
	movq	%rax, %r14
	movq	%rax, %rdi
	movq	%rdx, -112(%rbp)
	call	memset@PLT
	movl	%r12d, 4(%r14)
	pxor	%xmm0, %xmm0
	leaq	16(%r14), %r12
	movl	%r15d, (%r14)
	movq	%r12, %rdi
	movl	$64, %esi
	movups	%xmm0, 16(%r14)
	movups	%xmm0, 16(%r12)
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	call	_ZN2v88internal7Version9GetStringENS0_6VectorIcEE@PLT
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %r11
	movq	-104(%rbp), %r9
	movl	16(%r8), %eax
	testq	%rax, %rax
	movq	%rax, %r10
	jne	.L298
.L259:
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	jne	.L299
.L263:
	addl	%r10d, %ebx
	movl	%ebx, 80(%r14)
	movl	16(%r11), %edx
	movq	%rdx, %r8
	testq	%rdx, %rdx
	jne	.L300
.L266:
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	jne	.L301
.L270:
	leal	-1(%r15), %eax
	leal	(%rbx,%r8), %r10d
	xorl	%ebx, %ebx
	movq	%rax, -88(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -104(%rbp)
	testl	%r15d, %r15d
	je	.L281
	movq	%r12, -136(%rbp)
	movl	%r10d, %r15d
	movl	%r13d, -124(%rbp)
	movq	%r14, %r13
	movq	%r9, %r14
	.p2align 4,,10
	.p2align 3
.L282:
	leal	84(,%rbx,4), %eax
	movl	%ebx, %ecx
	movl	%r15d, 0(%r13,%rax)
	movq	(%r14), %rax
	movq	(%rax,%rbx,8), %r10
	movl	16(%r10), %edx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L275
	movl	%r15d, %edi
	movq	8(%r10), %rsi
	addq	%r13, %rdi
	cmpq	$7, %rdx
	ja	.L276
	xorl	%eax, %eax
.L278:
	movzbl	(%rsi,%rax), %r8d
	movb	%r8b, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L278
	.p2align 4,,10
	.p2align 3
.L275:
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	jne	.L302
.L279:
	addl	%r12d, %r15d
	leaq	1(%rbx), %rax
	cmpq	-88(%rbp), %rbx
	je	.L295
.L303:
	movq	%rax, %rbx
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L302:
	movq	-104(%rbp), %rdi
	movq	%r10, %rsi
	movl	%ecx, -96(%rbp)
	call	_ZNK2v88internal12SnapshotData12ReservationsEv
	movq	-72(%rbp), %rdx
	movl	-96(%rbp), %ecx
	movl	%r12d, %esi
	subq	-80(%rbp), %rdx
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	sarq	$2, %rdx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L279
	call	_ZdlPv@PLT
	addl	%r12d, %r15d
	leaq	1(%rbx), %rax
	cmpq	-88(%rbp), %rbx
	jne	.L303
	.p2align 4,,10
	.p2align 3
.L295:
	movq	%r13, %r14
	movq	-136(%rbp), %r12
	movl	-124(%rbp), %r13d
.L281:
	leal	-9(%r13), %eax
	subl	$16, %r13d
	cmovs	%eax, %r13d
	sarl	$3, %r13d
	movslq	%r13d, %r13
	leaq	(%r12,%r13,8), %rcx
	cmpq	%rcx, %r12
	jnb	.L284
	xorl	%edx, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L273:
	addq	$8, %r12
	addq	-8(%r12), %rax
	addq	%rax, %rdx
	cmpq	%r12, %rcx
	ja	.L273
	movq	%rax, %rcx
	shrq	$32, %rcx
	xorl	%ecx, %eax
	movq	%rdx, %rcx
	shrq	$32, %rcx
	xorl	%ecx, %edx
.L272:
	movl	%edx, 12(%r14)
	movq	-120(%rbp), %rdx
	movl	%eax, 8(%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L304
	addq	$104, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	movq	%r10, -96(%rbp)
	movl	%ebx, -112(%rbp)
	call	memcpy@PLT
	movl	-112(%rbp), %ecx
	movq	-96(%rbp), %r10
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L298:
	movq	-112(%rbp), %rdx
	movq	8(%r8), %rsi
	leaq	(%r14,%rdx), %rdi
	cmpq	$7, %rax
	ja	.L260
	xorl	%edx, %edx
.L262:
	movzbl	(%rsi,%rdx), %ecx
	movb	%cl, (%rdi,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L262
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L300:
	movl	%ebx, %edi
	movq	8(%r11), %rsi
	addq	%r14, %rdi
	cmpq	$7, %rdx
	ja	.L267
	xorl	%eax, %eax
.L269:
	movzbl	(%rsi,%rax), %ecx
	movb	%cl, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L269
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L299:
	movq	%r8, %rsi
	leaq	-80(%rbp), %rdi
	movq	%r9, -104(%rbp)
	movq	%r11, -96(%rbp)
	movl	%r10d, -88(%rbp)
	call	_ZNK2v88internal12SnapshotData12ReservationsEv
	movl	-88(%rbp), %r10d
	movq	-72(%rbp), %rdx
	xorl	%eax, %eax
	subq	-80(%rbp), %rdx
	leaq	.LC16(%rip), %rdi
	sarq	$2, %rdx
	movl	%r10d, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-80(%rbp), %rdi
	movl	-88(%rbp), %r10d
	movq	-96(%rbp), %r11
	movq	-104(%rbp), %r9
	testq	%rdi, %rdi
	je	.L263
	call	_ZdlPv@PLT
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %r11
	movl	-88(%rbp), %r10d
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L297:
	movq	%r9, %rdx
	movq	%r8, %rsi
	movq	%r11, %rdi
	movq	%r9, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	_ZN2v88internal22ProfileDeserializationEPKNS0_12SnapshotDataES3_RKSt6vectorIPS1_SaIS5_EE.part.0
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %r11
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L301:
	movl	%r8d, %esi
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	movq	%r9, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-96(%rbp), %r9
	movl	-88(%rbp), %r8d
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L267:
	movq	%r9, -96(%rbp)
	movl	%edx, -88(%rbp)
	call	memcpy@PLT
	movq	-96(%rbp), %r9
	movl	-88(%rbp), %r8d
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L260:
	movq	%rax, %rdx
	movq	%r9, -112(%rbp)
	movq	%r11, -104(%rbp)
	movq	%r8, -96(%rbp)
	movl	%eax, -88(%rbp)
	call	memcpy@PLT
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %r11
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %r10d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L284:
	xorl	%edx, %edx
	movl	$1, %eax
	jmp	.L272
.L304:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18854:
	.size	_ZN2v88internal8Snapshot18CreateSnapshotBlobEPKNS0_12SnapshotDataES4_RKSt6vectorIPS2_SaIS6_EEb, .-_ZN2v88internal8Snapshot18CreateSnapshotBlobEPKNS0_12SnapshotDataES4_RKSt6vectorIPS2_SaIS6_EEb
	.section	.rodata._ZN2v88internal30CreateSnapshotDataBlobInternalENS_15SnapshotCreator20FunctionCodeHandlingEPKcPNS_7IsolateE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"<embedded>"
	.section	.text._ZN2v88internal30CreateSnapshotDataBlobInternalENS_15SnapshotCreator20FunctionCodeHandlingEPKcPNS_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30CreateSnapshotDataBlobInternalENS_15SnapshotCreator20FunctionCodeHandlingEPKcPNS_7IsolateE
	.type	_ZN2v88internal30CreateSnapshotDataBlobInternalENS_15SnapshotCreator20FunctionCodeHandlingEPKcPNS_7IsolateE, @function
_ZN2v88internal30CreateSnapshotDataBlobInternalENS_15SnapshotCreator20FunctionCodeHandlingEPKcPNS_7IsolateE:
.LFB18870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L315
.L306:
	leaq	-88(%rbp), %r15
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-80(%rbp), %rbx
	call	_ZN2v815SnapshotCreatorC1EPNS_7IsolateEPKlPNS_11StartupDataE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%edx, %edx
	xorl	%esi, %esi
	pushq	$0
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L307
	leaq	.LC19(%rip), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_112RunExtraCodeEPNS_7IsolateENS_5LocalINS_7ContextEEEPKcS8_
	movq	-104(%rbp), %rsi
	testb	%al, %al
	je	.L316
.L307:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	call	_ZN2v815SnapshotCreator17SetDefaultContextENS_5LocalINS_7ContextEEENS_31SerializeInternalFieldsCallbackE@PLT
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN2v815SnapshotCreator10CreateBlobENS0_20FunctionCodeHandlingE@PLT
	movq	%rax, %r12
	movq	%rdx, %r13
.L308:
	movq	%r15, %rdi
	call	_ZN2v815SnapshotCreatorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L317
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L315:
	call	_ZN2v87Isolate8AllocateEv@PLT
	movq	%rax, %r12
	jmp	.L306
.L317:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18870:
	.size	_ZN2v88internal30CreateSnapshotDataBlobInternalENS_15SnapshotCreator20FunctionCodeHandlingEPKcPNS_7IsolateE, .-_ZN2v88internal30CreateSnapshotDataBlobInternalENS_15SnapshotCreator20FunctionCodeHandlingEPKcPNS_7IsolateE
	.section	.rodata._ZN2v88internal30WarmUpSnapshotDataBlobInternalENS_11StartupDataEPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"cold_snapshot_blob.raw_size > 0 && cold_snapshot_blob.data != nullptr"
	.section	.rodata._ZN2v88internal30WarmUpSnapshotDataBlobInternalENS_11StartupDataEPKc.str1.1,"aMS",@progbits,1
.LC21:
	.string	"(warmup_source) != nullptr"
.LC22:
	.string	"<warm-up>"
	.section	.text._ZN2v88internal30WarmUpSnapshotDataBlobInternalENS_11StartupDataEPKc,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30WarmUpSnapshotDataBlobInternalENS_11StartupDataEPKc
	.type	_ZN2v88internal30WarmUpSnapshotDataBlobInternalENS_11StartupDataEPKc, @function
_ZN2v88internal30WarmUpSnapshotDataBlobInternalENS_11StartupDataEPKc:
.LFB18871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$64, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jle	.L319
	testq	%rdi, %rdi
	je	.L319
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L327
	leaq	-72(%rbp), %r14
	leaq	-96(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	-64(%rbp), %r15
	call	_ZN2v815SnapshotCreatorC1EPKlPNS_11StartupDataE@PLT
	movq	%r14, %rdi
	call	_ZN2v815SnapshotCreator10GetIsolateEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	leaq	.LC22(%rip), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal12_GLOBAL__N_112RunExtraCodeEPNS_7IsolateENS_5LocalINS_7ContextEEEPKcS8_
	popq	%rcx
	popq	%rsi
	testb	%al, %al
	je	.L328
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v87Isolate27ContextDisposedNotificationEb@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v815SnapshotCreator17SetDefaultContextENS_5LocalINS_7ContextEEENS_31SerializeInternalFieldsCallbackE@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v815SnapshotCreator10CreateBlobENS0_20FunctionCodeHandlingE@PLT
	movq	%rax, %r12
	movq	%rdx, %r13
	popq	%rax
	popq	%rdx
.L323:
	movq	%r14, %rdi
	call	_ZN2v815SnapshotCreatorD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L329
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	movq	%r13, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L319:
	leaq	.LC20(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L327:
	leaq	.LC21(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L329:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18871:
	.size	_ZN2v88internal30WarmUpSnapshotDataBlobInternalENS_11StartupDataEPKc, .-_ZN2v88internal30WarmUpSnapshotDataBlobInternalENS_11StartupDataEPKc
	.section	.rodata._ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_.str1.1,"aMS",@progbits,1
.LC23:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	.type	_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_, @function
_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_:
.LFB23284:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L344
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L340
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L345
.L332:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L339:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L346
	testq	%r13, %r13
	jg	.L335
	testq	%r9, %r9
	jne	.L338
.L336:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L335
.L338:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L345:
	testq	%rsi, %rsi
	jne	.L333
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L335:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L336
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L340:
	movl	$8, %r14d
	jmp	.L332
.L344:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L333:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L332
	.cfi_endproc
.LFE23284:
	.size	_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_, .-_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	.section	.rodata._ZN2v88internal8Snapshot10InitializeEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"VerifyChecksum(blob)"
	.section	.rodata._ZN2v88internal8Snapshot10InitializeEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"[Deserializing isolate (%d bytes) took %0.3f ms]\n"
	.section	.text._ZN2v88internal8Snapshot10InitializeEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Snapshot10InitializeEPNS0_7IsolateE
	.type	_ZN2v88internal8Snapshot10InitializeEPNS0_7IsolateE, @function
_ZN2v88internal8Snapshot10InitializeEPNS0_7IsolateE:
.LFB18848:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1432, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41792(%rdi), %rax
	testq	%rax, %rax
	je	.L370
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	je	.L370
	pxor	%xmm0, %xmm0
	movq	%rdi, %r12
	movq	$0, -1328(%rbp)
	movaps	%xmm0, -1360(%rbp)
	movaps	%xmm0, -1344(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L392
.L349:
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	movq	$0, -1464(%rbp)
	jne	.L393
.L350:
	movq	41792(%r12), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8Snapshot12CheckVersionEPKNS_11StartupDataE
	movq	%rbx, %rdi
	call	_ZN2v88internal8Snapshot14VerifyChecksumEPKNS_11StartupDataE
	testb	%al, %al
	je	.L394
	movq	%rbx, %rdi
	call	_ZN2v88internal8Snapshot18ExtractStartupDataEPKNS_11StartupDataE
	leaq	16+_ZTVN2v88internal12SnapshotDataE(%rip), %rdi
	movb	$0, -1436(%rbp)
	movq	%rax, -1448(%rbp)
	movl	%edx, -1440(%rbp)
	movq	%rdi, -1456(%rbp)
	movq	%rdx, -1472(%rbp)
	movq	(%rbx), %rdx
	movl	84(%rdx), %ecx
	movl	80(%rdx), %esi
	cmpl	%esi, %ecx
	jbe	.L395
	movl	8(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L372
	cmpl	%r8d, %ecx
	jnb	.L372
	pxor	%xmm0, %xmm0
	movl	%esi, %r8d
	subl	%esi, %ecx
	movq	%rdi, -1424(%rbp)
	movups	%xmm0, -1304(%rbp)
	addq	%r8, %rdx
	leaq	16+_ZTVN2v88internal12DeserializerE(%rip), %r15
	leaq	-1392(%rbp), %r13
	movups	%xmm0, -1288(%rbp)
	leaq	-1456(%rbp), %rsi
	movq	%r13, %rdi
	movups	%xmm0, -1272(%rbp)
	movups	%xmm0, -1256(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1232(%rbp)
	movaps	%xmm0, -1216(%rbp)
	movq	%rdx, -1416(%rbp)
	movl	%ecx, -1408(%rbp)
	movb	$0, -1404(%rbp)
	movl	$0, -1240(%rbp)
	movq	%r15, -1312(%rbp)
	movl	4(%rax), %edx
	movl	8(%rax), %ecx
	movl	$0, -1188(%rbp)
	leal	19(,%rdx,4), %edx
	andl	$-8, %edx
	movl	%ecx, -1192(%rbp)
	addq	%rax, %rdx
	movq	%rdx, -1200(%rbp)
	movl	(%rax), %eax
	xorl	%edx, %edx
	movups	%xmm0, -1176(%rbp)
	movl	%eax, -1184(%rbp)
	movups	%xmm0, -1160(%rbp)
	movups	%xmm0, -1144(%rbp)
	movups	%xmm0, -1128(%rbp)
	movups	%xmm0, -1112(%rbp)
	movups	%xmm0, -1096(%rbp)
	movups	%xmm0, -1080(%rbp)
	movups	%xmm0, -1064(%rbp)
	movups	%xmm0, -1048(%rbp)
	movups	%xmm0, -1032(%rbp)
	movups	%xmm0, -1016(%rbp)
	movups	%xmm0, -1000(%rbp)
	movups	%xmm0, -984(%rbp)
	movups	%xmm0, -968(%rbp)
	movw	%dx, -720(%rbp)
	movups	%xmm0, -952(%rbp)
	movups	%xmm0, -936(%rbp)
	movups	%xmm0, -920(%rbp)
	movups	%xmm0, -904(%rbp)
	movups	%xmm0, -888(%rbp)
	movups	%xmm0, -872(%rbp)
	movups	%xmm0, -856(%rbp)
	movups	%xmm0, -776(%rbp)
	movups	%xmm0, -760(%rbp)
	movups	%xmm0, -744(%rbp)
	movups	%xmm0, -712(%rbp)
	movl	$0, -792(%rbp)
	movb	$0, -788(%rbp)
	movl	$0, -784(%rbp)
	movq	$0, -696(%rbp)
	call	_ZNK2v88internal12SnapshotData12ReservationsEv
	leaq	-984(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal21DeserializerAllocator17DecodeReservationERKSt6vectorINS0_14SerializedData11ReservationESaIS4_EE@PLT
	movq	-1392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L355
	call	_ZdlPv@PLT
.L355:
	movq	$0, -1392(%rbp)
	movq	-1000(%rbp), %rsi
	cmpq	-992(%rbp), %rsi
	je	.L356
	movq	$0, (%rsi)
	addq	$8, -1000(%rbp)
.L357:
	movq	-1416(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	leaq	16+_ZTVN2v88internal19StartupDeserializerE(%rip), %r14
	movups	%xmm0, -680(%rbp)
	leaq	-1424(%rbp), %rsi
	movups	%xmm0, -664(%rbp)
	movups	%xmm0, -648(%rbp)
	movups	%xmm0, -632(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -608(%rbp)
	movaps	%xmm0, -592(%rbp)
	movq	%r14, -1312(%rbp)
	movl	$0, -616(%rbp)
	movq	%r15, -688(%rbp)
	movl	4(%rdx), %eax
	movl	8(%rdx), %ecx
	movl	$0, -564(%rbp)
	leal	19(,%rax,4), %eax
	andl	$-8, %eax
	movl	%ecx, -568(%rbp)
	addq	%rdx, %rax
	movq	%rax, -576(%rbp)
	movl	(%rdx), %eax
	movups	%xmm0, -552(%rbp)
	movl	%eax, -560(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -536(%rbp)
	movups	%xmm0, -520(%rbp)
	movups	%xmm0, -504(%rbp)
	movups	%xmm0, -488(%rbp)
	movups	%xmm0, -472(%rbp)
	movups	%xmm0, -456(%rbp)
	movups	%xmm0, -440(%rbp)
	movups	%xmm0, -424(%rbp)
	movups	%xmm0, -408(%rbp)
	movups	%xmm0, -392(%rbp)
	movups	%xmm0, -376(%rbp)
	movups	%xmm0, -360(%rbp)
	movups	%xmm0, -344(%rbp)
	movups	%xmm0, -328(%rbp)
	movups	%xmm0, -312(%rbp)
	movw	%ax, -96(%rbp)
	movups	%xmm0, -296(%rbp)
	movups	%xmm0, -280(%rbp)
	movups	%xmm0, -264(%rbp)
	movups	%xmm0, -248(%rbp)
	movups	%xmm0, -232(%rbp)
	movups	%xmm0, -152(%rbp)
	movups	%xmm0, -136(%rbp)
	movups	%xmm0, -120(%rbp)
	movups	%xmm0, -88(%rbp)
	movl	$0, -168(%rbp)
	movb	$0, -164(%rbp)
	movl	$0, -160(%rbp)
	movq	$0, -72(%rbp)
	call	_ZNK2v88internal12SnapshotData12ReservationsEv
	leaq	-360(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal21DeserializerAllocator17DecodeReservationERKSt6vectorINS0_14SerializedData11ReservationESaIS4_EE@PLT
	movq	-1392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L358
	call	_ZdlPv@PLT
.L358:
	movq	$0, -1392(%rbp)
	movq	-376(%rbp), %rsi
	cmpq	-368(%rbp), %rsi
	je	.L359
	movq	$0, (%rsi)
	addq	$8, -376(%rbp)
.L360:
	leaq	16+_ZTVN2v88internal20ReadOnlyDeserializerE(%rip), %rcx
	cmpl	$4, 8(%rbx)
	movq	%rcx, -688(%rbp)
	jbe	.L363
	movq	(%rbx), %rax
	movl	4(%rax), %eax
	cmpl	$1, %eax
	ja	.L364
	movb	%al, -719(%rbp)
	andb	$1, -719(%rbp)
	cmpl	$4, 8(%rbx)
	jbe	.L363
	movq	(%rbx), %rax
	movl	4(%rax), %eax
	cmpl	$1, %eax
	ja	.L364
	leaq	-1312(%rbp), %r15
	leaq	-688(%rbp), %rbx
	movq	%r12, %rdi
	movb	%al, -95(%rbp)
	movq	%r15, %rdx
	movq	%rbx, %rsi
	andb	$1, -95(%rbp)
	call	_ZN2v88internal7Isolate16InitWithSnapshotEPNS0_20ReadOnlyDeserializerEPNS0_19StartupDeserializerE@PLT
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	leaq	16+_ZTVN2v88internal20ReadOnlyDeserializerE(%rip), %rcx
	movl	%eax, %r12d
	jne	.L396
.L365:
	movq	%rbx, %rdi
	movq	%rcx, -688(%rbp)
	leaq	16+_ZTVN2v88internal14SerializedDataE(%rip), %rbx
	call	_ZN2v88internal12DeserializerD2Ev@PLT
	movq	%r15, %rdi
	movq	%r14, -1312(%rbp)
	call	_ZN2v88internal12DeserializerD2Ev@PLT
	cmpb	$0, -1404(%rbp)
	movq	%rbx, -1424(%rbp)
	je	.L366
	movq	-1416(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L366
	call	_ZdaPv@PLT
.L366:
	cmpb	$0, -1436(%rbp)
	movq	%rbx, -1456(%rbp)
	je	.L367
	movq	-1448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L367
	call	_ZdaPv@PLT
.L367:
	movq	-1360(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L348
	leaq	-1352(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L370:
	xorl	%r12d, %r12d
.L348:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L397
	addq	$1432, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rax, -1464(%rbp)
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L396:
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r13, %rdi
	subq	-1464(%rbp), %rax
	movq	%rax, -1392(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	movl	-1472(%rbp), %esi
	movl	$1, %eax
	leaq	.LC25(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	16+_ZTVN2v88internal20ReadOnlyDeserializerE(%rip), %rcx
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L363:
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L356:
	leaq	-1008(%rbp), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L359:
	leaq	-384(%rbp), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L364:
	leaq	.LC6(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L394:
	leaq	.LC24(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L392:
	movq	40960(%rdi), %rax
	leaq	-1352(%rbp), %rsi
	movl	$142, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -1360(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L395:
	leaq	.LC7(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L372:
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L397:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18848:
	.size	_ZN2v88internal8Snapshot10InitializeEPNS0_7IsolateE, .-_ZN2v88internal8Snapshot10InitializeEPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Snapshot18HasContextSnapshotEPNS0_7IsolateEm,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Snapshot18HasContextSnapshotEPNS0_7IsolateEm, @function
_GLOBAL__sub_I__ZN2v88internal8Snapshot18HasContextSnapshotEPNS0_7IsolateEm:
.LFB23695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23695:
	.size	_GLOBAL__sub_I__ZN2v88internal8Snapshot18HasContextSnapshotEPNS0_7IsolateEm, .-_GLOBAL__sub_I__ZN2v88internal8Snapshot18HasContextSnapshotEPNS0_7IsolateEm
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Snapshot18HasContextSnapshotEPNS0_7IsolateEm
	.weak	_ZTVN2v88internal14SerializedDataE
	.section	.data.rel.ro.local._ZTVN2v88internal14SerializedDataE,"awG",@progbits,_ZTVN2v88internal14SerializedDataE,comdat
	.align 8
	.type	_ZTVN2v88internal14SerializedDataE, @object
	.size	_ZTVN2v88internal14SerializedDataE, 32
_ZTVN2v88internal14SerializedDataE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14SerializedDataD1Ev
	.quad	_ZN2v88internal14SerializedDataD0Ev
	.weak	_ZTVN2v88internal20ReadOnlyDeserializerE
	.section	.data.rel.ro._ZTVN2v88internal20ReadOnlyDeserializerE,"awG",@progbits,_ZTVN2v88internal20ReadOnlyDeserializerE,comdat
	.align 8
	.type	_ZTVN2v88internal20ReadOnlyDeserializerE, @object
	.size	_ZTVN2v88internal20ReadOnlyDeserializerE, 56
_ZTVN2v88internal20ReadOnlyDeserializerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal20ReadOnlyDeserializerD1Ev
	.quad	_ZN2v88internal20ReadOnlyDeserializerD0Ev
	.quad	_ZN2v88internal12Deserializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.quad	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.quad	_ZN2v88internal12Deserializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.weak	_ZTVN2v88internal19StartupDeserializerE
	.section	.data.rel.ro._ZTVN2v88internal19StartupDeserializerE,"awG",@progbits,_ZTVN2v88internal19StartupDeserializerE,comdat
	.align 8
	.type	_ZTVN2v88internal19StartupDeserializerE, @object
	.size	_ZTVN2v88internal19StartupDeserializerE, 56
_ZTVN2v88internal19StartupDeserializerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal19StartupDeserializerD1Ev
	.quad	_ZN2v88internal19StartupDeserializerD0Ev
	.quad	_ZN2v88internal12Deserializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.quad	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.quad	_ZN2v88internal12Deserializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.weak	_ZTVN2v88internal12SnapshotDataE
	.section	.data.rel.ro.local._ZTVN2v88internal12SnapshotDataE,"awG",@progbits,_ZTVN2v88internal12SnapshotDataE,comdat
	.align 8
	.type	_ZTVN2v88internal12SnapshotDataE, @object
	.size	_ZTVN2v88internal12SnapshotDataE, 40
_ZTVN2v88internal12SnapshotDataE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12SnapshotDataD1Ev
	.quad	_ZN2v88internal12SnapshotDataD0Ev
	.quad	_ZNK2v88internal12SnapshotData7PayloadEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
