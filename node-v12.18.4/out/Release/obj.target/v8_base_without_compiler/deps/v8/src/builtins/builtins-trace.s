	.file	"builtins-trace.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4860:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4860:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4861:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4861:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4863:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4863:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZNK2v88internal12_GLOBAL__N_114JsonTraceValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal12_GLOBAL__N_114JsonTraceValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZNK2v88internal12_GLOBAL__N_114JsonTraceValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB20289:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	movq	16(%rdi), %rdx
	movq	8(%rdi), %rsi
	movq	%r8, %rdi
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	.cfi_endproc
.LFE20289:
	.size	_ZNK2v88internal12_GLOBAL__N_114JsonTraceValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZNK2v88internal12_GLOBAL__N_114JsonTraceValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_114JsonTraceValueD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_114JsonTraceValueD2Ev, @function
_ZN2v88internal12_GLOBAL__N_114JsonTraceValueD2Ev:
.LFB23995:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_114JsonTraceValueE(%rip), %rax
	addq	$24, %rdi
	movq	%rax, -24(%rdi)
	cmpq	%rdi, %r8
	je	.L6
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	ret
	.cfi_endproc
.LFE23995:
	.size	_ZN2v88internal12_GLOBAL__N_114JsonTraceValueD2Ev, .-_ZN2v88internal12_GLOBAL__N_114JsonTraceValueD2Ev
	.set	_ZN2v88internal12_GLOBAL__N_114JsonTraceValueD1Ev,_ZN2v88internal12_GLOBAL__N_114JsonTraceValueD2Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_114JsonTraceValueD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_114JsonTraceValueD0Ev, @function
_ZN2v88internal12_GLOBAL__N_114JsonTraceValueD0Ev:
.LFB23997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_114JsonTraceValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23997:
	.size	_ZN2v88internal12_GLOBAL__N_114JsonTraceValueD0Ev, .-_ZN2v88internal12_GLOBAL__N_114JsonTraceValueD0Ev
	.section	.text._ZN2v88internalL18Builtin_Impl_TraceENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL18Builtin_Impl_TraceENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL18Builtin_Impl_TraceENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20296:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 3, -56
	movq	41096(%rdx), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%rax, -328(%rbp)
	cmpl	$5, %edi
	jg	.L12
	leaq	88(%rdx), %r15
	movq	%r15, %rbx
.L13:
	movq	%rbx, -336(%rbp)
	movq	%rbx, %rax
.L188:
	movq	%rax, -344(%rbp)
.L189:
	movq	%rax, -360(%rbp)
.L21:
	movq	(%rbx), %rax
	leaq	-168(%rbp), %r14
	movq	$0, -64(%rbp)
	movq	%r14, -176(%rbp)
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L22
	movq	%rax, %rsi
	movq	%rbx, %r10
.L23:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L32
.L190:
	movq	(%r10), %rsi
.L31:
	movq	-1(%rsi), %rax
	testb	$8, 11(%rax)
	je	.L38
	movq	(%r10), %rsi
	movslq	11(%rsi), %r11
	cmpl	$99, %r11d
	jg	.L192
	movq	-176(%rbp), %rdi
	testl	%r11d, %r11d
	jg	.L41
.L42:
	movb	$0, (%rdi,%r11)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r10
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L193
.L46:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L47
	movl	$1, %esi
	movq	%r10, -352(%rbp)
	call	_ZdlPvm@PLT
	movq	-352(%rbp), %r10
.L47:
	cmpb	$0, (%r10)
	jne	.L48
	movq	120(%r12), %r14
.L49:
	subl	$1, 41104(%r12)
	movq	-328(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L132
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L132:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L194
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	leaq	-8(%rsi), %r15
	cmpl	$6, %edi
	je	.L195
	leaq	-16(%rsi), %rbx
	cmpl	$7, %edi
	je	.L196
	leaq	-24(%rsi), %rax
	movq	%rax, -336(%rbp)
	cmpl	$8, %edi
	je	.L197
	leaq	-32(%rsi), %rax
	movq	%rax, -344(%rbp)
	cmpl	$9, %edi
	je	.L198
	leaq	-40(%rsi), %rax
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L32:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L190
	movq	(%r10), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L35
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L192:
	leal	1(%r11), %edi
	movq	%r11, -368(%rbp)
	movslq	%edi, %rdi
	movq	%r10, -352(%rbp)
	call	_Znam@PLT
	movq	-64(%rbp), %r8
	movq	-352(%rbp), %r10
	movq	%rax, -64(%rbp)
	movq	-368(%rbp), %r11
	movq	%rax, %rdi
	testq	%r8, %r8
	je	.L40
	movq	%r8, %rdi
	movl	$1, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %rdi
	movq	-368(%rbp), %r11
	movq	-352(%rbp), %r10
.L40:
	movq	%rdi, -176(%rbp)
	movq	(%r10), %rsi
.L41:
	movq	%r11, %rdx
	addq	$15, %rsi
	movq	%r11, -352(%rbp)
	call	memcpy@PLT
	movq	-176(%rbp), %rdi
	movq	-352(%rbp), %r11
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L48:
	movq	(%r15), %rax
	testb	$1, %al
	je	.L50
	movq	-1(%rax), %rax
	cmpw	$65, 11(%rax)
	jne	.L199
.L50:
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L200
.L51:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$364, %esi
.L191:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L22:
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	movq	%rbx, %r10
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L23
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L26
	movq	23(%rax), %rdx
	movl	11(%rdx), %ecx
	testl	%ecx, %ecx
	je	.L26
	movq	%rbx, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rsi
	movq	%rax, %r10
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L38:
	movq	%r10, %rdi
	movq	%r12, %rsi
	movq	%r10, -352(%rbp)
	call	_ZNK2v86String10Utf8LengthEPNS_7IsolateE@PLT
	movq	-352(%rbp), %r10
	cmpl	$99, %eax
	jg	.L201
	movq	-176(%rbp), %rdi
	movslq	%eax, %r11
	testl	%eax, %eax
	jle	.L42
.L45:
	movq	%rdi, %rdx
	xorl	%r9d, %r9d
	movq	%r10, %rdi
	xorl	%r8d, %r8d
	movl	$-1, %ecx
	movq	%r12, %rsi
	movq	%r11, -352(%rbp)
	call	_ZNK2v86String9WriteUtf8EPNS_7IsolateEPciPii@PLT
	movq	-176(%rbp), %rdi
	movq	-352(%rbp), %r11
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L35:
	movq	41088(%r12), %r10
	cmpq	41096(%r12), %r10
	je	.L202
.L37:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r10)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L199:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$367, %esi
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L193:
	movq	-176(%rbp), %rsi
	call	*%rax
	movq	%rax, %r10
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L200:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L51
	movq	-336(%rbp), %rax
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L203
.L53:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$365, %esi
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L201:
	leal	1(%rax), %edi
	movq	%r10, -368(%rbp)
	movslq	%edi, %rdi
	movl	%eax, -352(%rbp)
	call	_Znam@PLT
	movq	-64(%rbp), %r8
	movl	-352(%rbp), %edx
	movq	%rax, -64(%rbp)
	movq	-368(%rbp), %r10
	movq	%rax, %rdi
	testq	%r8, %r8
	je	.L44
	movq	%r8, %rdi
	movl	$1, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %rdi
	movq	-368(%rbp), %r10
	movl	-352(%rbp), %edx
.L44:
	movq	%rdi, -176(%rbp)
	movslq	%edx, %r11
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L26:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L28
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r10
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L28:
	movq	41088(%r12), %r10
	cmpq	41096(%r12), %r10
	je	.L204
.L30:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r10)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L203:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L53
	movq	-344(%rbp), %rcx
	movl	$0, -352(%rbp)
	movl	$1, -344(%rbp)
	movq	(%rcx), %rdx
	cmpq	%rdx, 104(%r12)
	jne	.L205
.L55:
	movl	11(%rax), %edx
	testl	%edx, %edx
	jne	.L67
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$366, %esi
	jmp	.L191
.L204:
	movq	%r12, %rdi
	movq	%rsi, -352(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-352(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L202:
	movq	%r12, %rdi
	movq	%rsi, -352(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-352(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L37
.L67:
	leaq	-296(%rbp), %rdx
	movq	$0, -192(%rbp)
	movq	%rdx, -304(%rbp)
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L69
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L206
.L69:
	movq	-336(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L76
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L76
	movq	41112(%r12), %rdi
	movq	15(%rax), %rbx
	testq	%rdi, %rdi
	je	.L79
	movq	%rbx, %rsi
	movq	%r10, -368(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-368(%rbp), %r10
	movq	%rax, -336(%rbp)
.L76:
	movq	-336(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	testb	$8, 11(%rdx)
	je	.L82
	movslq	11(%rax), %rbx
	movq	%rbx, %rdx
	cmpl	$99, %ebx
	jg	.L207
	movq	-304(%rbp), %rdi
	testl	%ebx, %ebx
	jg	.L85
.L86:
	movq	-360(%rbp), %rsi
	movb	$0, (%rdi,%rbx)
	xorl	%ebx, %ebx
	movq	(%rsi), %rax
	cmpq	%rax, 88(%r12)
	je	.L90
	leaq	88(%r12), %rdx
	movq	%r12, %rdi
	movq	%r10, -336(%rbp)
	movq	%rdx, %rcx
	call	_ZN2v88internal13JsonStringifyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	-336(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L208
	movl	$40, %edi
	movq	%r10, -368(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_114JsonTraceValueE(%rip), %rcx
	movq	%r14, -176(%rbp)
	movq	%rcx, (%rax)
	leaq	8(%rax), %rcx
	movq	%rax, -336(%rbp)
	movq	%rcx, -360(%rbp)
	movq	%rax, %rcx
	leaq	24(%rax), %rax
	movq	%rax, 8(%rcx)
	movq	$0, 16(%rcx)
	movb	$0, 24(%rcx)
	movq	$0, -64(%rbp)
	movq	(%rbx), %rax
	movq	-1(%rax), %rdx
	movq	-368(%rbp), %r10
	cmpw	$63, 11(%rdx)
	ja	.L94
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L209
.L94:
	movq	(%rbx), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L101
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L101
	movq	41112(%r12), %rdi
	movq	15(%rax), %r14
	testq	%rdi, %rdi
	je	.L104
	movq	%r14, %rsi
	movq	%r10, -368(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-368(%rbp), %r10
	movq	%rax, %rbx
.L101:
	movq	(%rbx), %rax
	movq	-1(%rax), %rdx
	testb	$8, 11(%rdx)
	je	.L107
	movl	11(%rax), %r14d
	cmpl	$99, %r14d
	jg	.L210
	movslq	%r14d, %rdx
	testl	%r14d, %r14d
	jg	.L110
.L111:
	movq	-176(%rbp), %rax
	movq	%r10, -368(%rbp)
	movb	$0, (%rax,%rdx)
	movq	-176(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	-360(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r14, %rcx
	movq	%rax, %r8
	movq	-336(%rbp), %rax
	movq	16(%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-64(%rbp), %rdi
	movq	-368(%rbp), %r10
	testq	%rdi, %rdi
	je	.L115
	movl	$1, %esi
	movq	%r10, -360(%rbp)
	call	_ZdlPvm@PLT
	movq	-360(%rbp), %r10
.L115:
	movq	-336(%rbp), %rax
	movb	$8, -313(%rbp)
	movl	$1, %ebx
	movq	%rax, -312(%rbp)
.L90:
	movq	-304(%rbp), %rax
	movq	%rax, -360(%rbp)
	movq	(%r15), %rax
	testb	$1, %al
	jne	.L116
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L117:
	movsd	.LC1(%rip), %xmm2
	movapd	%xmm0, %xmm1
	andpd	.LC0(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.L118
	movsd	.LC2(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L118
	comisd	.LC3(%rip), %xmm0
	jb	.L118
	cvttsd2sil	%xmm0, %r14d
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%r14d, %xmm1
	ucomisd	%xmm1, %xmm0
	jnp	.L211
.L118:
	movabsq	$9218868437227405312, %rdx
	movq	%xmm0, %rax
	testq	%rdx, %rax
	je	.L143
	movq	%xmm0, %rdx
	xorl	%r14d, %r14d
	shrq	$52, %rdx
	andl	$2047, %edx
	movl	%edx, %ecx
	subl	$1075, %ecx
	js	.L212
	cmpl	$31, %ecx
	jg	.L121
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %rsi
	andq	%rax, %rdx
	addq	%rsi, %rdx
	salq	%cl, %rdx
	movl	%edx, %esi
.L124:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%esi, %eax
	movl	%eax, %r14d
.L121:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -176(%rbp)
	cmpl	$1, %ebx
	jne	.L126
	cmpb	$8, -313(%rbp)
	je	.L213
.L126:
	movq	%r10, -336(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	-336(%rbp), %r10
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L214
.L127:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L128
	movq	(%rdi), %rax
	call	*8(%rax)
.L128:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L129
	movq	(%rdi), %rax
	call	*8(%rax)
.L129:
	movq	112(%r12), %r14
.L92:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L49
	movl	$1, %esi
	call	_ZdlPvm@PLT
	jmp	.L49
.L205:
	cmpq	%rdx, 88(%r12)
	je	.L55
	testb	$1, %dl
	jne	.L215
	sarq	$32, %rdx
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
.L59:
	movsd	.LC1(%rip), %xmm2
	movapd	%xmm0, %xmm1
	andpd	.LC0(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.L60
	movsd	.LC2(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L60
	comisd	.LC3(%rip), %xmm0
	jb	.L60
	cvttsd2sil	%xmm0, %ecx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movl	%ecx, -352(%rbp)
	ucomisd	%xmm1, %xmm0
	jnp	.L216
.L60:
	movabsq	$9218868437227405312, %rcx
	movq	%xmm0, %rdx
	testq	%rcx, %rdx
	je	.L140
	movq	%xmm0, %rdi
	shrq	$52, %rdi
	andl	$2047, %edi
	movl	%edi, %ecx
	subl	$1075, %ecx
	js	.L217
	cmpl	$31, %ecx
	jg	.L140
	movabsq	$4503599627370495, %rsi
	movl	$1, %edi
	andq	%rdx, %rsi
	salq	$52, %rdi
	addq	%rdi, %rsi
	salq	%cl, %rsi
	movl	%esi, %ecx
.L65:
	movl	$3, -344(%rbp)
	sarq	$63, %rdx
	orl	$1, %edx
	imull	%ecx, %edx
	movl	%edx, -352(%rbp)
	jmp	.L55
.L116:
	movsd	7(%rax), %xmm0
	jmp	.L117
.L212:
	cmpl	$-52, %ecx
	jl	.L121
	movabsq	$4503599627370495, %rsi
	movabsq	$4503599627370496, %rcx
	andq	%rax, %rsi
	addq	%rcx, %rsi
	movl	$1075, %ecx
	subl	%edx, %ecx
	shrq	%cl, %rsi
	jmp	.L124
.L82:
	movq	-336(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r10, -368(%rbp)
	call	_ZNK2v86String10Utf8LengthEPNS_7IsolateE@PLT
	movq	-368(%rbp), %r10
	cmpl	$99, %eax
	jg	.L218
	movq	-304(%rbp), %rdx
	movslq	%eax, %r11
	movq	%r11, %rbx
	movq	%rdx, %rdi
	testl	%eax, %eax
	jle	.L86
.L89:
	movq	-336(%rbp), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	orl	$-1, %ecx
	movq	%r12, %rsi
	movq	%r11, -376(%rbp)
	movq	%r10, -368(%rbp)
	call	_ZNK2v86String9WriteUtf8EPNS_7IsolateEPciPii@PLT
	movq	-376(%rbp), %r11
	movq	-304(%rbp), %rdi
	movq	-368(%rbp), %r10
	movq	%r11, %rbx
	jmp	.L86
.L207:
	addl	$1, %edx
	movq	%r10, -368(%rbp)
	movslq	%edx, %rdi
	call	_Znam@PLT
	movq	-192(%rbp), %rdi
	movq	-368(%rbp), %r10
	movq	%rax, -192(%rbp)
	testq	%rdi, %rdi
	je	.L84
	movl	$1, %esi
	call	_ZdlPvm@PLT
	movq	-192(%rbp), %rax
	movq	-368(%rbp), %r10
.L84:
	movq	%rax, -304(%rbp)
	movq	%rax, %rdi
	movq	-336(%rbp), %rax
	movq	(%rax), %rax
.L85:
	leaq	15(%rax), %rsi
	movq	%rbx, %rdx
	movq	%r10, -336(%rbp)
	call	memcpy@PLT
	movq	-304(%rbp), %rdi
	movq	-336(%rbp), %r10
	jmp	.L86
.L214:
	movl	-344(%rbp), %ecx
	subq	$8, %rsp
	movsbl	%r14b, %esi
	leaq	-176(%rbp), %rdx
	movslq	-352(%rbp), %r9
	xorl	%r8d, %r8d
	pushq	%rcx
	leaq	_ZZN2v88internalL18Builtin_Impl_TraceENS0_16BuiltinArgumentsEPNS0_7IsolateEE8arg_name(%rip), %rcx
	pushq	%rdx
	leaq	-312(%rbp), %rdx
	pushq	%rdx
	leaq	-313(%rbp), %rdx
	pushq	%rdx
	movq	%r10, %rdx
	pushq	%rcx
	movq	-360(%rbp), %rcx
	pushq	%rbx
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L127
.L206:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	subw	$1, %dx
	jne	.L71
	movq	23(%rax), %rdx
	cmpl	$0, 11(%rdx)
	je	.L71
	movq	-336(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r10, -368(%rbp)
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	-368(%rbp), %r10
	movq	%rax, -336(%rbp)
	jmp	.L76
.L215:
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L219
	movsd	7(%rdx), %xmm0
	jmp	.L59
.L107:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r10, -368(%rbp)
	call	_ZNK2v86String10Utf8LengthEPNS_7IsolateE@PLT
	movq	-368(%rbp), %r10
	cmpl	$99, %eax
	movl	%eax, %r14d
	jg	.L220
	movslq	%eax, %rdx
	testl	%eax, %eax
	jle	.L111
.L114:
	movq	-176(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	orl	$-1, %ecx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r10, -368(%rbp)
	call	_ZNK2v86String9WriteUtf8EPNS_7IsolateEPciPii@PLT
	movq	-368(%rbp), %r10
	movslq	%r14d, %rdx
	jmp	.L111
.L210:
	leal	1(%r14), %edi
	movq	%r10, -368(%rbp)
	movslq	%edi, %rdi
	call	_Znam@PLT
	movq	-64(%rbp), %rdi
	movq	-368(%rbp), %r10
	movq	%rax, -64(%rbp)
	testq	%rdi, %rdi
	je	.L109
	movl	$1, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %rax
	movq	-368(%rbp), %r10
.L109:
	movq	%rax, -176(%rbp)
	movq	(%rbx), %rax
.L110:
	movq	-176(%rbp), %rdi
	movslq	%r14d, %rdx
	leaq	15(%rax), %rsi
	movq	%r10, -376(%rbp)
	movq	%rdx, -368(%rbp)
	call	memcpy@PLT
	movq	-368(%rbp), %rdx
	movq	-376(%rbp), %r10
	jmp	.L111
.L79:
	movq	41088(%r12), %rax
	movq	%rax, -336(%rbp)
	cmpq	41096(%r12), %rax
	je	.L221
.L81:
	movq	-336(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rbx, (%rcx)
	jmp	.L76
.L218:
	leal	1(%rax), %edi
	movq	%r10, -376(%rbp)
	movslq	%edi, %rdi
	movl	%eax, -368(%rbp)
	call	_Znam@PLT
	movq	-192(%rbp), %rdi
	movl	-368(%rbp), %ecx
	movq	%rax, -192(%rbp)
	movq	-376(%rbp), %r10
	testq	%rdi, %rdi
	je	.L88
	movl	$1, %esi
	movl	%ecx, -376(%rbp)
	movq	%r10, -368(%rbp)
	call	_ZdlPvm@PLT
	movq	-192(%rbp), %rax
	movl	-376(%rbp), %ecx
	movq	-368(%rbp), %r10
.L88:
	movq	%rax, -304(%rbp)
	movq	%rax, %rdx
	movslq	%ecx, %r11
	jmp	.L89
.L217:
	cmpl	$-52, %ecx
	jl	.L140
	movabsq	$4503599627370495, %rcx
	movl	$1, %esi
	andq	%rdx, %rcx
	salq	$52, %rsi
	addq	%rcx, %rsi
	movl	$1075, %ecx
	subl	%edi, %ecx
	shrq	%cl, %rsi
	movq	%rsi, %rcx
	jmp	.L65
.L213:
	movq	-312(%rbp), %rax
	movq	%rax, -176(%rbp)
	jmp	.L126
.L71:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rbx
	testq	%rdi, %rdi
	je	.L73
	movq	%rbx, %rsi
	movq	%r10, -368(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-368(%rbp), %r10
	movq	%rax, -336(%rbp)
	jmp	.L69
.L140:
	movl	$0, -352(%rbp)
	movl	$3, -344(%rbp)
	jmp	.L55
.L208:
	movq	312(%r12), %r14
	jmp	.L92
.L209:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	subw	$1, %dx
	jne	.L96
	movq	23(%rax), %rdx
	cmpl	$0, 11(%rdx)
	je	.L96
	movq	%rbx, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r10, -368(%rbp)
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	-368(%rbp), %r10
	movq	%rax, %rbx
	jmp	.L101
.L194:
	call	__stack_chk_fail@PLT
.L219:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$368, %esi
	jmp	.L191
.L73:
	movq	41088(%r12), %rax
	movq	%rax, -336(%rbp)
	cmpq	41096(%r12), %rax
	je	.L222
.L75:
	movq	-336(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rbx, (%rcx)
	jmp	.L69
.L96:
	movq	41112(%r12), %rdi
	movq	15(%rax), %r14
	testq	%rdi, %rdi
	je	.L98
	movq	%r14, %rsi
	movq	%r10, -368(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-368(%rbp), %r10
	movq	%rax, %rbx
	jmp	.L94
.L220:
	leal	1(%rax), %edi
	movslq	%edi, %rdi
	call	_Znam@PLT
	movq	-64(%rbp), %rdi
	movq	-368(%rbp), %r10
	movq	%rax, -64(%rbp)
	testq	%rdi, %rdi
	je	.L113
	movl	$1, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %rax
	movq	-368(%rbp), %r10
.L113:
	movq	%rax, -176(%rbp)
	jmp	.L114
.L211:
	je	.L121
	jmp	.L118
.L104:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L223
.L106:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rbx)
	jmp	.L101
.L221:
	movq	%r12, %rdi
	movq	%r10, -368(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-368(%rbp), %r10
	movq	%rax, -336(%rbp)
	jmp	.L81
.L98:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L224
.L100:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rbx)
	jmp	.L94
.L143:
	xorl	%r14d, %r14d
	jmp	.L121
.L216:
	movl	$3, -344(%rbp)
	je	.L55
	jmp	.L60
.L224:
	movq	%r12, %rdi
	movq	%r10, -368(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-368(%rbp), %r10
	movq	%rax, %rbx
	jmp	.L100
.L198:
	leaq	88(%rdx), %rax
	movq	%rax, -360(%rbp)
	jmp	.L21
.L197:
	leaq	88(%rdx), %rax
	jmp	.L188
.L223:
	movq	%r12, %rdi
	movq	%r10, -368(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-368(%rbp), %r10
	movq	%rax, %rbx
	jmp	.L106
.L222:
	movq	%r12, %rdi
	movq	%r10, -368(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-368(%rbp), %r10
	movq	%rax, -336(%rbp)
	jmp	.L75
.L195:
	leaq	88(%rdx), %rbx
	jmp	.L13
.L196:
	leaq	88(%rdx), %rax
	movq	%rax, -336(%rbp)
	jmp	.L188
	.cfi_endproc
.LFE20296:
	.size	_ZN2v88internalL18Builtin_Impl_TraceENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL18Builtin_Impl_TraceENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB18192:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L231
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L234
.L225:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L225
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE18192:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL41Builtin_Impl_Stats_IsTraceCategoryEnabledEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC5:
	.string	"V8.Builtin_IsTraceCategoryEnabled"
	.section	.text._ZN2v88internalL41Builtin_Impl_Stats_IsTraceCategoryEnabledEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_Stats_IsTraceCategoryEnabledEiPmPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_Stats_IsTraceCategoryEnabledEiPmPNS0_7IsolateE:
.LFB20291:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -224(%rbp)
	movq	$0, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L310
.L236:
	movq	_ZZN2v88internalL41Builtin_Impl_Stats_IsTraceCategoryEnabledEiPmPNS0_7IsolateEE28trace_event_unique_atomic100(%rip), %rbx
	testq	%rbx, %rbx
	je	.L311
.L238:
	movq	$0, -256(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L312
.L240:
	addl	$1, 41104(%r12)
	leaq	88(%r12), %rax
	subq	$8, %r13
	cmpl	$5, %r15d
	cmovle	%rax, %r13
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L246
.L248:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$364, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L247:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L279
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L279:
	leaq	-256(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L313
.L235:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L314
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L248
	leaq	-168(%rbp), %rdx
	movq	$0, -64(%rbp)
	leaq	37592(%r12), %r15
	movq	%rax, %rsi
	movq	%rdx, -176(%rbp)
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L250
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L315
.L250:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L309
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L261
.L309:
	movq	0(%r13), %rsi
.L258:
	movq	-1(%rsi), %rax
	testb	$8, 11(%rax)
	je	.L265
	movq	0(%r13), %rsi
	movslq	11(%rsi), %r10
	movq	%r10, %rax
	cmpl	$99, %r10d
	jg	.L316
	movq	-176(%rbp), %rdi
	testl	%r10d, %r10d
	jg	.L268
.L269:
	movb	$0, (%rdi,%r10)
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L317
.L273:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L274
	movl	$1, %esi
	call	_ZdlPvm@PLT
.L274:
	subq	$37592, %r15
	cmpb	$0, 0(%r13)
	je	.L275
	movq	112(%r15), %r13
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L312:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -176(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L318
.L241:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L242
	movq	(%rdi), %rax
	call	*8(%rax)
.L242:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L243
	movq	(%rdi), %rax
	call	*8(%rax)
.L243:
	leaq	.LC5(%rip), %rax
	movq	%rbx, -248(%rbp)
	movq	%rax, -240(%rbp)
	leaq	-248(%rbp), %rax
	movq	%r14, -232(%rbp)
	movq	%rax, -256(%rbp)
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L311:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L319
.L239:
	movq	%rbx, _ZZN2v88internalL41Builtin_Impl_Stats_IsTraceCategoryEnabledEiPmPNS0_7IsolateEE28trace_event_unique_atomic100(%rip)
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L313:
	leaq	-216(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L310:
	movq	40960(%rdx), %rax
	leaq	-216(%rbp), %rsi
	movl	$851, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -224(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L275:
	movq	120(%r15), %r13
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L319:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L318:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$88, %esi
	leaq	-176(%rbp), %rdx
	pushq	$0
	leaq	.LC5(%rip), %rcx
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L265:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK2v86String10Utf8LengthEPNS_7IsolateE@PLT
	cmpl	$99, %eax
	jg	.L320
	movq	-176(%rbp), %rdi
	movslq	%eax, %r10
	testl	%eax, %eax
	jle	.L269
.L272:
	movq	%rdi, %rdx
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movl	$-1, %ecx
	movq	%r12, %rsi
	movq	%r10, -264(%rbp)
	call	_ZNK2v86String9WriteUtf8EPNS_7IsolateEPciPii@PLT
	movq	-176(%rbp), %rdi
	movq	-264(%rbp), %r10
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L261:
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L262
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L320:
	leal	1(%rax), %edi
	movl	%eax, -264(%rbp)
	movslq	%edi, %rdi
	call	_Znam@PLT
	movq	-64(%rbp), %r8
	movl	-264(%rbp), %edx
	movq	%rax, -64(%rbp)
	movq	%rax, %rdi
	testq	%r8, %r8
	je	.L271
	movq	%r8, %rdi
	movl	$1, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %rdi
	movl	-264(%rbp), %edx
.L271:
	movq	%rdi, -176(%rbp)
	movslq	%edx, %r10
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L316:
	addl	$1, %eax
	movq	%r10, -264(%rbp)
	movslq	%eax, %rdi
	call	_Znam@PLT
	movq	-64(%rbp), %r8
	movq	-264(%rbp), %r10
	movq	%rax, -64(%rbp)
	movq	%rax, %rdi
	testq	%r8, %r8
	je	.L267
	movq	%r8, %rdi
	movl	$1, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %rdi
	movq	-264(%rbp), %r10
.L267:
	movq	%rdi, -176(%rbp)
	movq	0(%r13), %rsi
.L268:
	movq	%r10, %rdx
	addq	$15, %rsi
	movq	%r10, -264(%rbp)
	call	memcpy@PLT
	movq	-176(%rbp), %rdi
	movq	-264(%rbp), %r10
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L317:
	movq	-176(%rbp), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L315:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L253
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L253
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L262:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L321
.L264:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L253:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L255
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	jmp	.L250
.L255:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L322
.L257:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L250
.L321:
	movq	%r12, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L264
.L322:
	movq	%r12, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L257
.L314:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20291:
	.size	_ZN2v88internalL41Builtin_Impl_Stats_IsTraceCategoryEnabledEiPmPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_Stats_IsTraceCategoryEnabledEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL24Builtin_Impl_Stats_TraceEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"V8.Builtin_Trace"
	.section	.text._ZN2v88internalL24Builtin_Impl_Stats_TraceEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Builtin_Impl_Stats_TraceEiPmPNS0_7IsolateE, @function
_ZN2v88internalL24Builtin_Impl_Stats_TraceEiPmPNS0_7IsolateE:
.LFB20294:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L352
.L324:
	movq	_ZZN2v88internalL24Builtin_Impl_Stats_TraceEiPmPNS0_7IsolateEE28trace_event_unique_atomic112(%rip), %rbx
	testq	%rbx, %rbx
	je	.L353
.L326:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L354
.L328:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL18Builtin_Impl_TraceENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L355
.L332:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L356
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L357
.L327:
	movq	%rbx, _ZZN2v88internalL24Builtin_Impl_Stats_TraceEiPmPNS0_7IsolateEE28trace_event_unique_atomic112(%rip)
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L354:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L358
.L329:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L330
	movq	(%rdi), %rax
	call	*8(%rax)
.L330:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L331
	movq	(%rdi), %rax
	call	*8(%rax)
.L331:
	leaq	.LC6(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L355:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L352:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$852, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L358:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L357:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L327
.L356:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20294:
	.size	_ZN2v88internalL24Builtin_Impl_Stats_TraceEiPmPNS0_7IsolateE, .-_ZN2v88internalL24Builtin_Impl_Stats_TraceEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Builtin_IsTraceCategoryEnabledEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Builtin_IsTraceCategoryEnabledEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Builtin_IsTraceCategoryEnabledEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Builtin_IsTraceCategoryEnabledEiPmPNS0_7IsolateE:
.LFB20292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L410
	addl	$1, 41104(%rdx)
	subq	$8, %rsi
	leaq	88(%rdx), %rax
	cmpl	$5, %edi
	cmovg	%rsi, %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%rax, %r13
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L364
.L366:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$364, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L365:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L359
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L359:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L411
	addq	$152, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L366
	leaq	-168(%rbp), %rdx
	movq	$0, -64(%rbp)
	leaq	37592(%r12), %r15
	movq	%rax, %rsi
	movq	%rdx, -176(%rbp)
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L368
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L412
.L368:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L409
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L379
.L409:
	movq	0(%r13), %rsi
.L376:
	movq	-1(%rsi), %rax
	testb	$8, 11(%rax)
	je	.L383
	movq	0(%r13), %rsi
	movslq	11(%rsi), %r10
	movq	%r10, %rax
	cmpl	$99, %r10d
	jg	.L413
	movq	-176(%rbp), %rdi
	testl	%r10d, %r10d
	jg	.L386
.L387:
	movb	$0, (%rdi,%r10)
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L414
.L391:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L392
	movl	$1, %esi
	call	_ZdlPvm@PLT
.L392:
	subq	$37592, %r15
	cmpb	$0, 0(%r13)
	je	.L393
	movq	112(%r15), %r13
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L410:
	call	_ZN2v88internalL41Builtin_Impl_Stats_IsTraceCategoryEnabledEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L393:
	movq	120(%r15), %r13
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L379:
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L380
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L412:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L371
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L371
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L383:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK2v86String10Utf8LengthEPNS_7IsolateE@PLT
	cmpl	$99, %eax
	jg	.L415
	movq	-176(%rbp), %rdi
	movslq	%eax, %r10
	testl	%eax, %eax
	jle	.L387
.L390:
	movq	%rdi, %rdx
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movl	$-1, %ecx
	movq	%r12, %rsi
	movq	%r10, -184(%rbp)
	call	_ZNK2v86String9WriteUtf8EPNS_7IsolateEPciPii@PLT
	movq	-176(%rbp), %rdi
	movq	-184(%rbp), %r10
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L413:
	addl	$1, %eax
	movq	%r10, -184(%rbp)
	movslq	%eax, %rdi
	call	_Znam@PLT
	movq	-64(%rbp), %r8
	movq	-184(%rbp), %r10
	movq	%rax, -64(%rbp)
	movq	%rax, %rdi
	testq	%r8, %r8
	je	.L385
	movq	%r8, %rdi
	movl	$1, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %rdi
	movq	-184(%rbp), %r10
.L385:
	movq	%rdi, -176(%rbp)
	movq	0(%r13), %rsi
.L386:
	movq	%r10, %rdx
	addq	$15, %rsi
	movq	%r10, -184(%rbp)
	call	memcpy@PLT
	movq	-176(%rbp), %rdi
	movq	-184(%rbp), %r10
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L414:
	movq	-176(%rbp), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L415:
	leal	1(%rax), %edi
	movl	%eax, -184(%rbp)
	movslq	%edi, %rdi
	call	_Znam@PLT
	movq	-64(%rbp), %r8
	movl	-184(%rbp), %edx
	movq	%rax, -64(%rbp)
	movq	%rax, %rdi
	testq	%r8, %r8
	je	.L389
	movq	%r8, %rdi
	movl	$1, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %rdi
	movl	-184(%rbp), %edx
.L389:
	movq	%rdi, -176(%rbp)
	movslq	%edx, %r10
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L371:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L373
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L380:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L416
.L382:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L376
.L373:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L417
.L375:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L368
.L416:
	movq	%r12, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L382
.L417:
	movq	%r12, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L375
.L411:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20292:
	.size	_ZN2v88internal30Builtin_IsTraceCategoryEnabledEiPmPNS0_7IsolateE, .-_ZN2v88internal30Builtin_IsTraceCategoryEnabledEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal13Builtin_TraceEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal13Builtin_TraceEiPmPNS0_7IsolateE
	.type	_ZN2v88internal13Builtin_TraceEiPmPNS0_7IsolateE, @function
_ZN2v88internal13Builtin_TraceEiPmPNS0_7IsolateE:
.LFB20295:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L422
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL18Builtin_Impl_TraceENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore 6
	jmp	_ZN2v88internalL24Builtin_Impl_Stats_TraceEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20295:
	.size	_ZN2v88internal13Builtin_TraceEiPmPNS0_7IsolateE, .-_ZN2v88internal13Builtin_TraceEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal30Builtin_IsTraceCategoryEnabledEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal30Builtin_IsTraceCategoryEnabledEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal30Builtin_IsTraceCategoryEnabledEiPmPNS0_7IsolateE:
.LFB25698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25698:
	.size	_GLOBAL__sub_I__ZN2v88internal30Builtin_IsTraceCategoryEnabledEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal30Builtin_IsTraceCategoryEnabledEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal30Builtin_IsTraceCategoryEnabledEiPmPNS0_7IsolateE
	.section	.data.rel.ro.local._ZTVN2v88internal12_GLOBAL__N_114JsonTraceValueE,"aw"
	.align 8
	.type	_ZTVN2v88internal12_GLOBAL__N_114JsonTraceValueE, @object
	.size	_ZTVN2v88internal12_GLOBAL__N_114JsonTraceValueE, 40
_ZTVN2v88internal12_GLOBAL__N_114JsonTraceValueE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12_GLOBAL__N_114JsonTraceValueD1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_114JsonTraceValueD0Ev
	.quad	_ZNK2v88internal12_GLOBAL__N_114JsonTraceValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC7:
	.string	"data"
	.section	.data.rel.local._ZZN2v88internalL18Builtin_Impl_TraceENS0_16BuiltinArgumentsEPNS0_7IsolateEE8arg_name,"aw"
	.align 8
	.type	_ZZN2v88internalL18Builtin_Impl_TraceENS0_16BuiltinArgumentsEPNS0_7IsolateEE8arg_name, @object
	.size	_ZZN2v88internalL18Builtin_Impl_TraceENS0_16BuiltinArgumentsEPNS0_7IsolateEE8arg_name, 8
_ZZN2v88internalL18Builtin_Impl_TraceENS0_16BuiltinArgumentsEPNS0_7IsolateEE8arg_name:
	.quad	.LC7
	.section	.bss._ZZN2v88internalL24Builtin_Impl_Stats_TraceEiPmPNS0_7IsolateEE28trace_event_unique_atomic112,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL24Builtin_Impl_Stats_TraceEiPmPNS0_7IsolateEE28trace_event_unique_atomic112, @object
	.size	_ZZN2v88internalL24Builtin_Impl_Stats_TraceEiPmPNS0_7IsolateEE28trace_event_unique_atomic112, 8
_ZZN2v88internalL24Builtin_Impl_Stats_TraceEiPmPNS0_7IsolateEE28trace_event_unique_atomic112:
	.zero	8
	.section	.bss._ZZN2v88internalL41Builtin_Impl_Stats_IsTraceCategoryEnabledEiPmPNS0_7IsolateEE28trace_event_unique_atomic100,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Builtin_Impl_Stats_IsTraceCategoryEnabledEiPmPNS0_7IsolateEE28trace_event_unique_atomic100, @object
	.size	_ZZN2v88internalL41Builtin_Impl_Stats_IsTraceCategoryEnabledEiPmPNS0_7IsolateEE28trace_event_unique_atomic100, 8
_ZZN2v88internalL41Builtin_Impl_Stats_IsTraceCategoryEnabledEiPmPNS0_7IsolateEE28trace_event_unique_atomic100:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
	.align 8
.LC2:
	.long	4290772992
	.long	1105199103
	.align 8
.LC3:
	.long	0
	.long	-1042284544
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
