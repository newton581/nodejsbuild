	.file	"handles.cc"
	.text
	.section	.text._ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEE11DeleteArrayEPv,"axG",@progbits,_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEE11DeleteArrayEPv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEE11DeleteArrayEPv
	.type	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEE11DeleteArrayEPv, @function
_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEE11DeleteArrayEPv:
.LFB21731:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21731:
	.size	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEE11DeleteArrayEPv, .-_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEE11DeleteArrayEPv
	.section	.text._ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEE15NewPointerArrayEm,"axG",@progbits,_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEE15NewPointerArrayEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEE15NewPointerArrayEm
	.type	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEE15NewPointerArrayEm, @function
_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEE15NewPointerArrayEm:
.LFB21730:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	%rax, %rsi
	ja	.L5
	addq	%r8, %rsi
	movq	%r8, %rax
	movq	%rsi, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	jmp	_ZN2v88internal4Zone9NewExpandEm@PLT
	.cfi_endproc
.LFE21730:
	.size	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEE15NewPointerArrayEm, .-_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEE15NewPointerArrayEm
	.section	.text._ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED2Ev,"axG",@progbits,_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED2Ev
	.type	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED2Ev, @function
_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED2Ev:
.LFB19849:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal15IdentityMapBase5ClearEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15IdentityMapBaseD2Ev@PLT
	.cfi_endproc
.LFE19849:
	.size	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED2Ev, .-_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED2Ev
	.weak	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED1Ev
	.set	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED1Ev,_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED2Ev
	.section	.text._ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED0Ev,"axG",@progbits,_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED0Ev
	.type	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED0Ev, @function
_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED0Ev:
.LFB19851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal15IdentityMapBase5ClearEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal15IdentityMapBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE19851:
	.size	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED0Ev, .-_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED0Ev
	.section	.text._ZN2v88internal11HandleScope15NumberOfHandlesEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11HandleScope15NumberOfHandlesEPNS0_7IsolateE
	.type	_ZN2v88internal11HandleScope15NumberOfHandlesEPNS0_7IsolateE, @function
_ZN2v88internal11HandleScope15NumberOfHandlesEPNS0_7IsolateE:
.LFB17941:
	.cfi_startproc
	endbr64
	movq	41120(%rdi), %rcx
	movq	24(%rcx), %rdx
	movl	%edx, %eax
	testl	%edx, %edx
	je	.L10
	movq	8(%rcx), %rcx
	movq	41088(%rdi), %rax
	subq	-8(%rcx,%rdx,8), %rax
	subl	$1, %edx
	imull	$1022, %edx, %edx
	sarq	$3, %rax
	addl	%edx, %eax
.L10:
	ret
	.cfi_endproc
.LFE17941:
	.size	_ZN2v88internal11HandleScope15NumberOfHandlesEPNS0_7IsolateE, .-_ZN2v88internal11HandleScope15NumberOfHandlesEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Cannot create a handle without a HandleScope"
	.align 8
.LC1:
	.string	"v8::HandleScope::CreateHandle()"
	.section	.rodata._ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"NewArray"
	.section	.text._ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE
	.type	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE, @function
_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE:
.LFB17942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	41088(%rdi), %r12
	movl	41108(%rdi), %eax
	cmpl	%eax, 41104(%rdi)
	je	.L30
	movq	41120(%rdi), %r13
	movq	41096(%rdi), %rax
	movq	%rdi, %rbx
	movq	24(%r13), %rdx
	testq	%rdx, %rdx
	je	.L15
	movq	8(%r13), %rcx
	movq	-8(%rcx,%rdx,8), %rdx
	addq	$8176, %rdx
	cmpq	%rax, %rdx
	je	.L15
	movq	%rdx, 41096(%rbx)
	movq	%rdx, %rax
.L15:
	cmpq	%rax, %r12
	je	.L31
.L12:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%r12d, %r12d
	call	_ZN2v85Utils16ReportApiFailureEPKcS2_@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L31:
	movq	104(%r13), %r12
	testq	%r12, %r12
	je	.L32
.L16:
	movq	$0, 104(%r13)
	movq	24(%r13), %rdx
	cmpq	16(%r13), %rdx
	je	.L17
	movq	8(%r13), %r15
	salq	$3, %rdx
.L18:
	leaq	8176(%r12), %rax
	movq	%r12, (%r15,%rdx)
	addq	$1, 24(%r13)
	movq	%rax, 41096(%rbx)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L17:
	movq	_ZN2v88internal20DetachableVectorBase16kMinimumCapacityE(%rip), %r14
	addq	%rdx, %rdx
	movabsq	$1152921504606846975, %rax
	cmpq	%r14, %rdx
	cmovnb	%rdx, %r14
	cmpq	%rax, %r14
	leaq	0(,%r14,8), %rdi
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_Znam@PLT
	movq	24(%r13), %rdx
	movq	8(%r13), %r8
	movq	%rax, %r15
	salq	$3, %rdx
	je	.L20
	movq	%r8, %rsi
	movq	%rax, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L21:
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	24(%r13), %rax
	leaq	0(,%rax,8), %rdx
.L22:
	movq	%r15, 8(%r13)
	movq	%r14, 16(%r13)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L20:
	testq	%r8, %r8
	je	.L22
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L32:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$8176, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L16
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$8176, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L16
	leaq	.LC2(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE17942:
	.size	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE, .-_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE
	.section	.text._ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE
	.type	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE, @function
_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE:
.LFB17943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	41120(%rdi), %rbx
	movq	41096(%rdi), %r13
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L49
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L50:
	call	_ZdaPv@PLT
	movq	24(%rbx), %rax
	movq	%r12, 104(%rbx)
	testq	%rax, %rax
	je	.L33
.L49:
	movq	8(%rbx), %rdx
.L39:
	subq	$1, %rax
	movq	(%rdx,%rax,8), %r12
	leaq	8176(%r12), %rcx
	cmpq	%r13, %rcx
	jb	.L40
	cmpq	%r13, %r12
	jbe	.L33
.L40:
	movq	104(%rbx), %rdi
	movq	%rax, 24(%rbx)
	testq	%rdi, %rdi
	jne	.L50
	movq	%r12, 104(%rbx)
	testq	%rax, %rax
	jne	.L39
.L33:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17943:
	.size	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE, .-_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE
	.section	.text._ZN2v88internal11HandleScope21current_level_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11HandleScope21current_level_addressEPNS0_7IsolateE
	.type	_ZN2v88internal11HandleScope21current_level_addressEPNS0_7IsolateE, @function
_ZN2v88internal11HandleScope21current_level_addressEPNS0_7IsolateE:
.LFB17944:
	.cfi_startproc
	endbr64
	leaq	41104(%rdi), %rax
	ret
	.cfi_endproc
.LFE17944:
	.size	_ZN2v88internal11HandleScope21current_level_addressEPNS0_7IsolateE, .-_ZN2v88internal11HandleScope21current_level_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal11HandleScope20current_next_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11HandleScope20current_next_addressEPNS0_7IsolateE
	.type	_ZN2v88internal11HandleScope20current_next_addressEPNS0_7IsolateE, @function
_ZN2v88internal11HandleScope20current_next_addressEPNS0_7IsolateE:
.LFB17945:
	.cfi_startproc
	endbr64
	leaq	41088(%rdi), %rax
	ret
	.cfi_endproc
.LFE17945:
	.size	_ZN2v88internal11HandleScope20current_next_addressEPNS0_7IsolateE, .-_ZN2v88internal11HandleScope20current_next_addressEPNS0_7IsolateE
	.section	.text._ZN2v88internal11HandleScope21current_limit_addressEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11HandleScope21current_limit_addressEPNS0_7IsolateE
	.type	_ZN2v88internal11HandleScope21current_limit_addressEPNS0_7IsolateE, @function
_ZN2v88internal11HandleScope21current_limit_addressEPNS0_7IsolateE:
.LFB17946:
	.cfi_startproc
	endbr64
	leaq	41096(%rdi), %rax
	ret
	.cfi_endproc
.LFE17946:
	.size	_ZN2v88internal11HandleScope21current_limit_addressEPNS0_7IsolateE, .-_ZN2v88internal11HandleScope21current_limit_addressEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal20CanonicalHandleScopeC2EPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../deps/v8/src/handles/handles.cc:130"
	.section	.text._ZN2v88internal20CanonicalHandleScopeC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20CanonicalHandleScopeC2EPNS0_7IsolateE
	.type	_ZN2v88internal20CanonicalHandleScopeC2EPNS0_7IsolateE, @function
_ZN2v88internal20CanonicalHandleScopeC2EPNS0_7IsolateE:
.LFB17948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rsi, (%rdi)
	movq	41136(%rsi), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	(%rbx), %r14
	movl	$8, %edi
	movq	41112(%r14), %rax
	movq	%rax, 96(%rbx)
	movq	%rbx, 41112(%r14)
	call	_Znwm@PLT
	movq	%r12, %rsi
	addq	$37592, %r12
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v88internal12RootIndexMapC1EPNS0_7IsolateE@PLT
	movq	%r13, 72(%rbx)
	movl	$72, %edi
	call	_Znwm@PLT
	movdqa	.LC4(%rip), %xmm0
	leaq	16+_ZTVN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEEE(%rip), %rcx
	movq	%r12, 16(%rax)
	movups	%xmm0, 24(%rax)
	pxor	%xmm0, %xmm0
	movb	$0, 56(%rax)
	movq	%rcx, (%rax)
	movq	%r15, 64(%rax)
	movups	%xmm0, 40(%rax)
	movq	%rax, 80(%rbx)
	movl	41104(%r14), %eax
	movl	%eax, 88(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17948:
	.size	_ZN2v88internal20CanonicalHandleScopeC2EPNS0_7IsolateE, .-_ZN2v88internal20CanonicalHandleScopeC2EPNS0_7IsolateE
	.globl	_ZN2v88internal20CanonicalHandleScopeC1EPNS0_7IsolateE
	.set	_ZN2v88internal20CanonicalHandleScopeC1EPNS0_7IsolateE,_ZN2v88internal20CanonicalHandleScopeC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal20CanonicalHandleScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20CanonicalHandleScopeD2Ev
	.type	_ZN2v88internal20CanonicalHandleScopeD2Ev, @function
_ZN2v88internal20CanonicalHandleScopeD2Ev:
.LFB17951:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L57
	movl	$8, %esi
	call	_ZdlPvm@PLT
.L57:
	movq	80(%rbx), %r12
	testq	%r12, %r12
	je	.L58
	movq	(%r12), %rax
	leaq	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L59
	leaq	16+_ZTVN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN2v88internal15IdentityMapBase5ClearEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal15IdentityMapBaseD2Ev@PLT
	movl	$72, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L58:
	movq	(%rbx), %rax
	movq	96(%rbx), %rdx
	leaq	8(%rbx), %rdi
	movq	%rdx, 41112(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4ZoneD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L58
	.cfi_endproc
.LFE17951:
	.size	_ZN2v88internal20CanonicalHandleScopeD2Ev, .-_ZN2v88internal20CanonicalHandleScopeD2Ev
	.globl	_ZN2v88internal20CanonicalHandleScopeD1Ev
	.set	_ZN2v88internal20CanonicalHandleScopeD1Ev,_ZN2v88internal20CanonicalHandleScopeD2Ev
	.section	.text._ZN2v88internal20CanonicalHandleScope6LookupEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20CanonicalHandleScope6LookupEm
	.type	_ZN2v88internal20CanonicalHandleScope6LookupEm, @function
_ZN2v88internal20CanonicalHandleScope6LookupEm:
.LFB17953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	88(%rdi), %eax
	cmpl	%eax, 41104(%r12)
	je	.L68
	movq	41088(%r12), %rax
	cmpq	%rax, 41096(%r12)
	je	.L80
.L69:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%r13, (%rax)
.L67:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L81
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	%rsi, %rax
	movq	%rdi, %rbx
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L82
.L71:
	movq	80(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal15IdentityMapBase8GetEntryEm@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L67
	movq	(%rbx), %rbx
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L83
.L74:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r13, (%rax)
	movq	%rax, (%r12)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L82:
	movq	72(%rdi), %rdi
	leaq	-42(%rbp), %rdx
	call	_ZNK2v88internal12RootIndexMap6LookupEmPNS0_9RootIndexE@PLT
	testb	%al, %al
	je	.L71
	movq	(%rbx), %rax
	movzwl	-42(%rbp), %edx
	leaq	56(%rax,%rdx,8), %rax
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L80:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L83:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE
	jmp	.L74
.L81:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17953:
	.size	_ZN2v88internal20CanonicalHandleScope6LookupEm, .-_ZN2v88internal20CanonicalHandleScope6LookupEm
	.section	.text._ZN2v88internal19DeferredHandleScopeC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19DeferredHandleScopeC2EPNS0_7IsolateE
	.type	_ZN2v88internal19DeferredHandleScopeC2EPNS0_7IsolateE, @function
_ZN2v88internal19DeferredHandleScopeC2EPNS0_7IsolateE:
.LFB17955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	41120(%rsi), %rdi
	movq	%rdi, 16(%r15)
	call	_ZN2v88internal22HandleScopeImplementer18BeginDeferredScopeEv@PLT
	movq	16(%r15), %r12
	movq	104(%r12), %rbx
	movq	(%r12), %r14
	movq	%r12, %r13
	testq	%rbx, %rbx
	je	.L101
.L85:
	movq	$0, 104(%r12)
	leaq	8176(%rbx), %rax
	movq	24(%r13), %rdx
	movq	%rax, -56(%rbp)
	cmpq	16(%r13), %rdx
	je	.L88
	movq	8(%r13), %r12
	salq	$3, %rdx
.L89:
	movq	%rbx, (%r12,%rdx)
	addq	$1, 24(%r13)
	movdqu	41088(%r14), %xmm0
	addl	$1, 41104(%r14)
	shufpd	$1, %xmm0, %xmm0
	movups	%xmm0, (%r15)
	movq	%rbx, %xmm0
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, 41088(%r14)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movq	_ZN2v88internal20DetachableVectorBase16kMinimumCapacityE(%rip), %rax
	addq	%rdx, %rdx
	cmpq	%rax, %rdx
	cmovnb	%rdx, %rax
	movq	%rax, %rcx
	movq	%rax, -64(%rbp)
	leaq	0(,%rax,8), %rdi
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_Znam@PLT
	movq	24(%r13), %rdx
	movq	8(%r13), %r9
	movq	%rax, %r12
	salq	$3, %rdx
	je	.L91
	movq	%r9, %rsi
	movq	%rax, %rdi
	movq	%r9, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %r9
.L92:
	movq	%r9, %rdi
	call	_ZdaPv@PLT
	movq	24(%r13), %rax
	leaq	0(,%rax,8), %rdx
.L93:
	movq	-64(%rbp), %rax
	movq	%r12, 8(%r13)
	movq	%rax, 16(%r13)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L91:
	testq	%r9, %r9
	je	.L93
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L101:
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$8176, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L86
.L100:
	movq	16(%r15), %r13
	jmp	.L85
.L86:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$8176, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L100
	leaq	.LC2(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE17955:
	.size	_ZN2v88internal19DeferredHandleScopeC2EPNS0_7IsolateE, .-_ZN2v88internal19DeferredHandleScopeC2EPNS0_7IsolateE
	.globl	_ZN2v88internal19DeferredHandleScopeC1EPNS0_7IsolateE
	.set	_ZN2v88internal19DeferredHandleScopeC1EPNS0_7IsolateE,_ZN2v88internal19DeferredHandleScopeC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal19DeferredHandleScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19DeferredHandleScopeD2Ev
	.type	_ZN2v88internal19DeferredHandleScopeD2Ev, @function
_ZN2v88internal19DeferredHandleScopeD2Ev:
.LFB17958:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	subl	$1, 41104(%rax)
	ret
	.cfi_endproc
.LFE17958:
	.size	_ZN2v88internal19DeferredHandleScopeD2Ev, .-_ZN2v88internal19DeferredHandleScopeD2Ev
	.globl	_ZN2v88internal19DeferredHandleScopeD1Ev
	.set	_ZN2v88internal19DeferredHandleScopeD1Ev,_ZN2v88internal19DeferredHandleScopeD2Ev
	.section	.text._ZN2v88internal19DeferredHandleScope6DetachEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19DeferredHandleScope6DetachEv
	.type	_ZN2v88internal19DeferredHandleScope6DetachEv, @function
_ZN2v88internal19DeferredHandleScope6DetachEv:
.LFB17960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rbx), %rsi
	movq	16(%rdi), %rdi
	call	_ZN2v88internal22HandleScopeImplementer6DetachEPm@PLT
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rcx
	movq	(%rdx), %rdx
	movq	%rcx, 41088(%rdx)
	movq	(%rbx), %rcx
	movq	%rcx, 41096(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17960:
	.size	_ZN2v88internal19DeferredHandleScope6DetachEv, .-_ZN2v88internal19DeferredHandleScope6DetachEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11HandleScope15NumberOfHandlesEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11HandleScope15NumberOfHandlesEPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal11HandleScope15NumberOfHandlesEPNS0_7IsolateE:
.LFB21732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21732:
	.size	_GLOBAL__sub_I__ZN2v88internal11HandleScope15NumberOfHandlesEPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal11HandleScope15NumberOfHandlesEPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11HandleScope15NumberOfHandlesEPNS0_7IsolateE
	.weak	_ZTVN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEEE
	.section	.data.rel.ro.local._ZTVN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEEE,"awG",@progbits,_ZTVN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEEE,comdat
	.align 8
	.type	_ZTVN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEEE, @object
	.size	_ZTVN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEEE, 48
_ZTVN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED1Ev
	.quad	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEED0Ev
	.quad	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEE15NewPointerArrayEm
	.quad	_ZN2v88internal11IdentityMapIPmNS0_20ZoneAllocationPolicyEE11DeleteArrayEPv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	-1
	.long	0
	.long	0
	.long	0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
