	.file	"marking.cc"
	.text
	.section	.text._ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE17AllBitsSetInRangeEjj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE17AllBitsSetInRangeEjj
	.type	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE17AllBitsSetInRangeEjj, @function
_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE17AllBitsSetInRangeEjj:
.LFB5038:
	.cfi_startproc
	endbr64
	xorl	%r10d, %r10d
	cmpl	%edx, %esi
	jnb	.L1
	subl	$1, %edx
	movl	$1, %r8d
	movl	%esi, %ecx
	movl	%esi, %eax
	movl	%r8d, %r9d
	movl	%edx, %esi
	shrl	$5, %eax
	sall	%cl, %r9d
	shrl	$5, %esi
	movl	%edx, %ecx
	sall	%cl, %r8d
	cmpl	%esi, %eax
	je	.L3
	movl	%eax, %edx
	negl	%r9d
	movl	(%rdi,%rdx,4), %ecx
	andl	%r9d, %ecx
	cmpl	%r9d, %ecx
	jne	.L1
	addl	$1, %eax
	cmpl	%eax, %esi
	ja	.L5
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L11:
	addq	$1, %rax
	cmpl	%eax, %esi
	jbe	.L4
.L5:
	cmpl	$-1, (%rdi,%rax,4)
	je	.L11
	xorl	%r10d, %r10d
.L1:
	movl	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	%r8d, %edx
	movl	(%rdi,%rax,4), %ecx
	subl	%r9d, %edx
	orl	%edx, %r8d
	andl	%r8d, %ecx
	cmpl	%r8d, %ecx
	sete	%r10b
	movl	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	leal	-1(%r8), %eax
	orl	%eax, %r8d
	movl	(%rdi,%rsi,4), %eax
	andl	%r8d, %eax
	cmpl	%r8d, %eax
	sete	%r10b
	movl	%r10d, %eax
	ret
	.cfi_endproc
.LFE5038:
	.size	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE17AllBitsSetInRangeEjj, .-_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE17AllBitsSetInRangeEjj
	.section	.text._ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE19AllBitsClearInRangeEjj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE19AllBitsClearInRangeEjj
	.type	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE19AllBitsClearInRangeEjj, @function
_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE19AllBitsClearInRangeEjj:
.LFB5039:
	.cfi_startproc
	endbr64
	movl	$1, %r8d
	cmpl	%edx, %esi
	jnb	.L12
	subl	$1, %edx
	movl	$1, %r9d
	movl	%esi, %ecx
	movl	%esi, %eax
	movl	%r9d, %r8d
	movl	%edx, %esi
	shrl	$5, %eax
	sall	%cl, %r8d
	shrl	$5, %esi
	movl	%edx, %ecx
	sall	%cl, %r9d
	cmpl	%esi, %eax
	je	.L14
	movl	%r8d, %edx
	movl	%eax, %ecx
	xorl	%r8d, %r8d
	negl	%edx
	testl	%edx, (%rdi,%rcx,4)
	jne	.L12
	addl	$1, %eax
	cmpl	%eax, %esi
	ja	.L16
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L21:
	addq	$1, %rax
	cmpl	%eax, %esi
	jbe	.L15
.L16:
	movl	(%rdi,%rax,4), %edx
	testl	%edx, %edx
	je	.L21
	xorl	%r8d, %r8d
.L12:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	%r9d, %edx
	subl	%r8d, %edx
	orl	%edx, %r9d
	testl	%r9d, (%rdi,%rax,4)
	sete	%r8b
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	leal	-1(%r9), %eax
	orl	%eax, %r9d
	testl	%r9d, (%rdi,%rsi,4)
	sete	%r8b
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE5039:
	.size	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE19AllBitsClearInRangeEjj, .-_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE19AllBitsClearInRangeEjj
	.section	.rodata._ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE5PrintEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%d: %dx%d\n"
.LC1:
	.string	"%d: "
.LC2:
	.string	"1"
.LC3:
	.string	"0"
.LC4:
	.string	"\n"
	.section	.text._ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE5PrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE5PrintEv
	.type	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE5PrintEv, @function
_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE5PrintEv:
.LFB5047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC3(%rip), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -64(%rbp)
	movl	$0, -52(%rbp)
	movl	$0, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L30:
	movq	-64(%rbp), %rax
	movl	(%rax,%r12,4), %r13d
	cmpl	-52(%rbp), %r13d
	je	.L49
	testl	%ecx, %ecx
	jne	.L50
.L25:
	leal	-1(%r13), %eax
	cmpl	$-3, %eax
	ja	.L32
	movl	%r12d, %esi
	xorl	%eax, %eax
	movl	$32, %ebx
	movl	$1, %r14d
	leaq	.LC1(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	.LC2(%rip), %rdi
	testl	%r14d, %r13d
	jne	.L48
	movq	%r15, %rdi
.L48:
	xorl	%eax, %eax
	addl	%r14d, %r14d
	call	_ZN2v88internal6PrintFEPKcz@PLT
	subl	$1, %ebx
	jne	.L29
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	xorl	%ecx, %ecx
.L24:
	addq	$1, %r12
	cmpq	$1024, %r12
	jne	.L30
	testl	%ecx, %ecx
	jne	.L51
.L31:
	addq	$24, %rsp
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6PrintFEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	addl	$1, %ecx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L32:
	movl	%r13d, -52(%rbp)
	xorl	%ecx, %ecx
	movl	%r12d, -56(%rbp)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L50:
	movl	-52(%rbp), %esi
	sall	$5, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rdi
	testl	%esi, %esi
	movl	-56(%rbp), %esi
	setne	%dl
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L25
.L51:
	movl	-52(%rbp), %eax
	sall	$5, %ecx
	xorl	%edx, %edx
	movl	-56(%rbp), %esi
	leaq	.LC0(%rip), %rdi
	testl	%eax, %eax
	setne	%dl
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L31
	.cfi_endproc
.LFE5047:
	.size	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE5PrintEv, .-_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE5PrintEv
	.section	.text._ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE7IsCleanEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE7IsCleanEv
	.type	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE7IsCleanEv, @function
_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE7IsCleanEv:
.LFB5048:
	.cfi_startproc
	endbr64
	leaq	4096(%rdi), %rax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L58:
	addq	$4, %rdi
	cmpq	%rax, %rdi
	je	.L57
.L54:
	movl	(%rdi), %edx
	testl	%edx, %edx
	je	.L58
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5048:
	.size	_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE7IsCleanEv, .-_ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE7IsCleanEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE17AllBitsSetInRangeEjj,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE17AllBitsSetInRangeEjj, @function
_GLOBAL__sub_I__ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE17AllBitsSetInRangeEjj:
.LFB5824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5824:
	.size	_GLOBAL__sub_I__ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE17AllBitsSetInRangeEjj, .-_GLOBAL__sub_I__ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE17AllBitsSetInRangeEjj
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal16ConcurrentBitmapILNS0_10AccessModeE1EE17AllBitsSetInRangeEjj
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
