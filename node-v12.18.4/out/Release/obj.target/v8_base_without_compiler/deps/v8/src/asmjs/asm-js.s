	.file	"asm-js.cc"
	.text
	.section	.text._ZN2v88internal14CompilationJobD2Ev,"axG",@progbits,_ZN2v88internal14CompilationJobD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CompilationJobD2Ev
	.type	_ZN2v88internal14CompilationJobD2Ev, @function
_ZN2v88internal14CompilationJobD2Ev:
.LFB10770:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10770:
	.size	_ZN2v88internal14CompilationJobD2Ev, .-_ZN2v88internal14CompilationJobD2Ev
	.weak	_ZN2v88internal14CompilationJobD1Ev
	.set	_ZN2v88internal14CompilationJobD1Ev,_ZN2v88internal14CompilationJobD2Ev
	.section	.text._ZN2v88internal19AsmJsCompilationJobD2Ev,"axG",@progbits,_ZN2v88internal19AsmJsCompilationJobD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19AsmJsCompilationJobD2Ev
	.type	_ZN2v88internal19AsmJsCompilationJobD2Ev, @function
_ZN2v88internal19AsmJsCompilationJobD2Ev:
.LFB23747:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal19AsmJsCompilationJobE(%rip), %rax
	addq	$72, %rdi
	movq	%rax, -72(%rdi)
	jmp	_ZN2v88internal4ZoneD1Ev@PLT
	.cfi_endproc
.LFE23747:
	.size	_ZN2v88internal19AsmJsCompilationJobD2Ev, .-_ZN2v88internal19AsmJsCompilationJobD2Ev
	.weak	_ZN2v88internal19AsmJsCompilationJobD1Ev
	.set	_ZN2v88internal19AsmJsCompilationJobD1Ev,_ZN2v88internal19AsmJsCompilationJobD2Ev
	.section	.text._ZN2v88internal14CompilationJobD0Ev,"axG",@progbits,_ZN2v88internal14CompilationJobD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CompilationJobD0Ev
	.type	_ZN2v88internal14CompilationJobD0Ev, @function
_ZN2v88internal14CompilationJobD0Ev:
.LFB10772:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10772:
	.size	_ZN2v88internal14CompilationJobD0Ev, .-_ZN2v88internal14CompilationJobD0Ev
	.section	.text._ZN2v88internal19AsmJsCompilationJobD0Ev,"axG",@progbits,_ZN2v88internal19AsmJsCompilationJobD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19AsmJsCompilationJobD0Ev
	.type	_ZN2v88internal19AsmJsCompilationJobD0Ev, @function
_ZN2v88internal19AsmJsCompilationJobD0Ev:
.LFB23749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal19AsmJsCompilationJobE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	72(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -72(%rdi)
	call	_ZN2v88internal4ZoneD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$296, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23749:
	.size	_ZN2v88internal19AsmJsCompilationJobD0Ev, .-_ZN2v88internal19AsmJsCompilationJobD0Ev
	.section	.rodata._ZN2v88internal19AsmJsCompilationJob14ExecuteJobImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/v8/src/asmjs/asm-js.cc:235"
	.align 8
.LC1:
	.string	"[asm.js translation successful: time=%0.3fms, translate_zone=%zuKB, compile_zone+=%zuKB]\n"
	.section	.text._ZN2v88internal19AsmJsCompilationJob14ExecuteJobImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AsmJsCompilationJob14ExecuteJobImplEv
	.type	_ZN2v88internal19AsmJsCompilationJob14ExecuteJobImplEv, @function
_ZN2v88internal19AsmJsCompilationJob14ExecuteJobImplEv:
.LFB20175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$888, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L8
	movq	16(%rax), %rcx
	leaq	-24(%rcx), %rbx
	subq	%rdx, %rbx
.L8:
	movq	(%rax), %rax
	leaq	-880(%rbp), %r12
	movq	%rax, -920(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	64(%r15), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%r12, %rdi
	movq	%rax, -912(%rbp)
	movq	40(%r15), %rax
	movq	8(%rax), %r13
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	32(%r15), %rax
	movq	96(%rax), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*32(%rax)
	movq	40(%r15), %rax
	movq	16(%rax), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	movq	32(%r14), %rcx
	movq	8(%r14), %rsi
	cltq
	cmpq	%rcx, %rax
	jb	.L10
	movq	24(%r14), %rdx
	subq	%rsi, %rdx
	sarq	%rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %rax
	jnb	.L10
	subq	%rcx, %rax
	leaq	(%rsi,%rax,2), %rax
	movq	%rax, 16(%r14)
.L11:
	movq	24(%r15), %rdx
	leaq	-816(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%rdi, -904(%rbp)
	call	_ZN2v88internal4wasm11AsmJsParserC1EPNS0_4ZoneEmPNS0_20Utf16CharacterStreamE@PLT
	movq	-904(%rbp), %rdi
	call	_ZN2v88internal4wasm11AsmJsParser3RunEv@PLT
	testb	%al, %al
	jne	.L12
	cmpb	$0, _ZN2v88internal26FLAG_suppress_asm_messagesE(%rip)
	movl	$1, %r13d
	je	.L81
.L13:
	movq	-80(%rbp), %rax
	leaq	-80(%rbp), %rdx
	cmpq	%rdx, %rax
	je	.L24
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L25
.L24:
	movq	-584(%rbp), %r14
	testq	%r14, %r14
	jne	.L26
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L82:
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L30
.L31:
	movq	%rbx, %r14
.L26:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	movq	(%r14), %rbx
	cmpq	%rax, %rdi
	jne	.L82
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L31
.L30:
	movq	-592(%rbp), %rax
	movq	-600(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-600(%rbp), %rdi
	leaq	-552(%rbp), %rax
	movq	$0, -576(%rbp)
	movq	$0, -584(%rbp)
	cmpq	%rax, %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	movq	-640(%rbp), %r14
	testq	%r14, %r14
	jne	.L32
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L83:
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L36
.L37:
	movq	%rbx, %r14
.L32:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	movq	(%r14), %rbx
	cmpq	%rax, %rdi
	jne	.L83
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L37
.L36:
	movq	-648(%rbp), %rax
	movq	-656(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-656(%rbp), %rdi
	leaq	-608(%rbp), %rax
	movq	$0, -632(%rbp)
	movq	$0, -640(%rbp)
	cmpq	%rax, %rdi
	je	.L33
	call	_ZdlPv@PLT
.L33:
	movq	-696(%rbp), %r14
	testq	%r14, %r14
	jne	.L38
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L84:
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L42
.L43:
	movq	%rbx, %r14
.L38:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	movq	(%r14), %rbx
	cmpq	%rax, %rdi
	jne	.L84
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L43
.L42:
	movq	-704(%rbp), %rax
	movq	-712(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-712(%rbp), %rdi
	leaq	-664(%rbp), %rax
	movq	$0, -688(%rbp)
	movq	$0, -696(%rbp)
	cmpq	%rax, %rdi
	je	.L39
	call	_ZdlPv@PLT
.L39:
	movq	-752(%rbp), %rdi
	leaq	-736(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L44
	call	_ZdlPv@PLT
.L44:
	movq	%r12, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$888, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	16(%r13), %r14
	movq	24(%r13), %rax
	subq	%r14, %rax
	cmpq	$31, %rax
	jbe	.L86
	leaq	32(%r14), %rax
	movq	%rax, 16(%r13)
.L15:
	movq	%r13, (%r14)
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	subq	%rax, %rdx
	cmpq	$1023, %rdx
	jbe	.L87
	leaq	1024(%rax), %rdx
	movq	%rdx, 16(%r13)
.L17:
	movq	%rax, 8(%r14)
	movq	%r14, %rsi
	movq	%rax, 16(%r14)
	movq	%rdx, 24(%r14)
	movq	-520(%rbp), %rdi
	movq	%r14, 232(%r15)
	call	_ZNK2v88internal4wasm17WasmModuleBuilder7WriteToEPNS1_10ZoneBufferE@PLT
	movq	16(%r13), %r14
	movq	24(%r13), %rax
	subq	%r14, %rax
	cmpq	$31, %rax
	jbe	.L88
	leaq	32(%r14), %rax
	movq	%rax, 16(%r13)
.L19:
	movq	%r13, (%r14)
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	subq	%rax, %rdx
	cmpq	$1023, %rdx
	jbe	.L89
	leaq	1024(%rax), %rdx
	movq	%rdx, 16(%r13)
.L21:
	movq	%rax, 8(%r14)
	movq	%r14, %rsi
	movq	%rax, 16(%r14)
	movq	%rdx, 24(%r14)
	movq	-520(%rbp), %rdi
	movq	%r14, 240(%r15)
	xorl	%r14d, %r14d
	call	_ZNK2v88internal4wasm17WasmModuleBuilder21WriteAsmJsOffsetTableEPNS1_10ZoneBufferE@PLT
	movq	-488(%rbp), %rax
	movq	%rax, 248(%r15)
	movq	40(%r15), %rax
	movq	8(%rax), %rax
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L22
	movq	16(%rax), %rcx
	leaq	-24(%rcx), %r14
	subq	%rdx, %r14
.L22:
	movq	(%rax), %rax
	movq	-840(%rbp), %rdx
	movq	%rax, -928(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L23
	movq	-864(%rbp), %rax
	subq	$24, %rax
	subq	%rdx, %rax
.L23:
	addq	-880(%rbp), %rax
	movq	%rax, 288(%r15)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-912(%rbp), %r13
	leaq	-888(%rbp), %rdi
	movq	%rdi, -904(%rbp)
	subq	%r13, %rax
	movq	%rax, -888(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	movsd	%xmm0, 256(%r15)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-904(%rbp), %rdi
	subq	%r13, %rax
	movq	%rax, -888(%rbp)
	call	_ZNK2v84base9TimeDelta14InMicrosecondsEv@PLT
	movq	%rax, 280(%r15)
	movq	40(%r15), %rax
	movq	16(%rax), %rdi
	call	_ZNK2v88internal15FunctionLiteral12end_positionEv@PLT
	movl	%eax, %r13d
	movq	40(%r15), %rax
	movq	16(%rax), %rdi
	call	_ZNK2v88internal15FunctionLiteral14start_positionEv@PLT
	subl	%eax, %r13d
	movl	%r13d, 272(%r15)
	xorl	%r13d, %r13d
	cmpb	$0, _ZN2v88internal21FLAG_trace_asm_parserE(%rip)
	je	.L13
	movq	-928(%rbp), %rdx
	subq	-920(%rbp), %rdx
	leaq	.LC1(%rip), %rdi
	movl	$1, %eax
	movq	288(%r15), %rsi
	subq	%rbx, %rdx
	movsd	256(%r15), %xmm0
	addq	%r14, %rdx
	shrq	$10, %rdx
	shrq	$10, %rsi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L81:
	movl	-264(%rbp), %esi
	movq	32(%r15), %rdi
	movl	$354, %ecx
	movq	-272(%rbp), %r8
	addq	$176, %rdi
	movl	%esi, %edx
	call	_ZN2v88internal30PendingCompilationErrorHandler15ReportWarningAtEiiNS0_15MessageTemplateEPKc@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L10:
	cmpb	$0, 48(%r14)
	movq	%rax, 32(%r14)
	movq	%rsi, 16(%r14)
	jne	.L11
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	jmp	.L11
.L89:
	movl	$1024, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	leaq	1024(%rax), %rdx
	jmp	.L21
.L86:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L15
.L87:
	movl	$1024, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	leaq	1024(%rax), %rdx
	jmp	.L17
.L88:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L19
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20175:
	.size	_ZN2v88internal19AsmJsCompilationJob14ExecuteJobImplEv, .-_ZN2v88internal19AsmJsCompilationJob14ExecuteJobImplEv
	.section	.text._ZN2v88internal12_GLOBAL__N_126ReportInstantiationFailureENS0_6HandleINS0_6ScriptEEEiPKc,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_126ReportInstantiationFailureENS0_6HandleINS0_6ScriptEEEiPKc, @function
_ZN2v88internal12_GLOBAL__N_126ReportInstantiationFailureENS0_6HandleINS0_6ScriptEEEiPKc:
.LFB20158:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal26FLAG_suppress_asm_messagesE(%rip)
	jne	.L90
	movq	%rdi, %r13
	movq	%rdx, %rdi
	movl	%esi, %r12d
	movq	%rdx, %rbx
	call	strlen@PLT
	movq	%rbx, -112(%rbp)
	leaq	-96(%rbp), %r15
	movl	%r12d, %ecx
	movq	%rax, -104(%rbp)
	movq	0(%r13), %rax
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	andq	$-262144, %rax
	movq	24(%rax), %r14
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii@PLT
	leaq	-112(%rbp), %rsi
	subq	$37592, %r14
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r15, %rdx
	movl	$357, %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	xorl	%r8d, %r8d
	call	_ZN2v88internal14MessageHandler17MakeMessageObjectEPNS0_7IsolateENS0_15MessageTemplateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS8_INS0_10FixedArrayEEE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movabsq	$68719476736, %rcx
	movq	%rax, %rdx
	movq	(%rax), %rax
	movq	%rcx, 87(%rax)
	call	_ZN2v88internal14MessageHandler13ReportMessageEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_15JSMessageObjectEEE@PLT
.L90:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L94
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L94:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20158:
	.size	_ZN2v88internal12_GLOBAL__N_126ReportInstantiationFailureENS0_6HandleINS0_6ScriptEEEiPKc, .-_ZN2v88internal12_GLOBAL__N_126ReportInstantiationFailureENS0_6HandleINS0_6ScriptEEEiPKc
	.section	.rodata._ZN2v88internal19AsmJsCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"AsmJs::Compile"
.LC3:
	.string	"(location_) != nullptr"
.LC4:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal19AsmJsCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"success, asm->wasm: %0.3f ms, compile: %0.3f ms, %zu bytes"
	.section	.rodata._ZN2v88internal19AsmJsCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE.str1.1
.LC6:
	.string	"-1 != length"
	.section	.text._ZN2v88internal19AsmJsCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AsmJsCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	.type	_ZN2v88internal19AsmJsCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE, @function
_ZN2v88internal19AsmJsCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE:
.LFB20227:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	248(%rbx), %r14
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	leaq	.LC2(%rip), %rsi
	movq	(%rax), %rdx
	movq	%r14, 7(%rdx)
	leaq	-200(%rbp), %rdx
	movq	45752(%r12), %rdi
	leaq	-240(%rbp), %r14
	movq	%rdx, -216(%rbp)
	movq	0(%r13), %rdx
	leaq	-272(%rbp), %r13
	movl	$0, -224(%rbp)
	movq	$0, -208(%rbp)
	movb	$0, -200(%rbp)
	movq	%rsi, -232(%rbp)
	movq	%r12, -240(%rbp)
	movl	47(%rdx), %edx
	movq	240(%rbx), %rcx
	shrl	$6, %edx
	movq	8(%rcx), %r8
	movq	16(%rcx), %r9
	andl	$1, %edx
	movq	232(%rbx), %rcx
	subq	%r8, %r9
	movq	8(%rcx), %rsi
	movq	16(%rcx), %rcx
	pushq	%rdx
	movq	%r14, %rdx
	pushq	%rax
	subq	%rsi, %rcx
	movq	%rsi, -272(%rbp)
	movq	%r12, %rsi
	movslq	%ecx, %rcx
	movq	%rcx, -264(%rbp)
	movq	%r13, %rcx
	call	_ZN2v88internal4wasm10WasmEngine26SyncCompileTranslatedAsmJsEPNS0_7IsolateEPNS1_12ErrorThrowerERKNS1_15ModuleWireBytesENS0_6VectorIKhEENS0_6HandleINS0_10HeapNumberEEENS0_12LanguageModeE@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L106
	movq	%rax, %r15
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r13, %rdi
	subq	-296(%rbp), %rax
	movq	%rax, -272(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	movq	40(%rbx), %rax
	movsd	%xmm0, 264(%rbx)
	movq	%r15, 48(%rax)
	movq	40960(%r12), %r12
	movl	280(%rbx), %esi
	leaq	2936(%r12), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movl	288(%rbx), %esi
	leaq	1528(%r12), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movl	272(%rbx), %esi
	leaq	1608(%r12), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	280(%rbx), %rcx
	xorl	%esi, %esi
	testq	%rcx, %rcx
	jne	.L107
.L97:
	leaq	1648(%r12), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	cmpb	$0, _ZN2v88internal26FLAG_suppress_asm_messagesE(%rip)
	jne	.L100
	cmpb	$0, _ZN2v88internal19FLAG_trace_asm_timeE(%rip)
	jne	.L108
.L100:
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm12ErrorThrowerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L109
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movslq	272(%rbx), %rax
	cqto
	idivq	%rcx
	movq	%rax, %rsi
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L108:
	movq	232(%rbx), %rax
	movsd	256(%rbx), %xmm0
	leaq	-160(%rbp), %rdi
	movl	$100, %esi
	movsd	264(%rbx), %xmm1
	movq	8(%rax), %rdx
	movq	16(%rax), %rcx
	movq	40(%rbx), %rax
	subq	%rdx, %rcx
	leaq	.LC5(%rip), %rdx
	movq	16(%rax), %rax
	movl	(%rax), %r8d
	movq	32(%rbx), %rax
	movq	%rdi, -176(%rbp)
	movq	$100, -168(%rbp)
	movq	80(%rax), %r15
	movl	$2, %eax
	movl	%r8d, -296(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movl	-296(%rbp), %r8d
	cmpl	$-1, %eax
	je	.L110
	movq	-176(%rbp), %rdx
	cltq
	movl	%r8d, %ecx
	movq	%r15, %rsi
	movq	%rax, -168(%rbp)
	movq	%r13, %rdi
	movq	%rdx, -288(%rbp)
	movl	%r8d, %edx
	movq	%rax, -280(%rbp)
	movq	(%r15), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r12
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii@PLT
	leaq	-288(%rbp), %rsi
	subq	$37592, %r12
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rcx
	movl	$355, %esi
	call	_ZN2v88internal14MessageHandler17MakeMessageObjectEPNS0_7IsolateENS0_15MessageTemplateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS8_INS0_10FixedArrayEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$17179869184, %rcx
	movq	%rax, %rdx
	movq	(%rax), %rax
	movq	%rcx, 87(%rax)
	call	_ZN2v88internal14MessageHandler13ReportMessageEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_15JSMessageObjectEEE@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L106:
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L110:
	leaq	.LC6(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L109:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20227:
	.size	_ZN2v88internal19AsmJsCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE, .-_ZN2v88internal19AsmJsCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"Math"
	.section	.text._ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE, @function
_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE:
.LFB20138:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-128(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%r13, %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rax
	movq	$4, -120(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	%rax, %rsi
	movq	(%r12), %rax
	movq	(%rsi), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$2, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L112
	xorl	%eax, %eax
	testb	$1, 11(%rdx)
	sete	%al
	addl	%eax, %eax
.L112:
	movl	%eax, -128(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -116(%rbp)
	movq	%rdi, -104(%rbp)
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L130
.L113:
	movq	%r13, %rdi
	movq	%rsi, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r12, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%r12, -64(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -124(%rbp)
	jne	.L114
	movq	-104(%rbp), %rax
	movq	88(%rax), %rdx
	leaq	88(%rax), %r12
	testb	$1, %dl
	jne	.L116
.L118:
	leaq	88(%rbx), %rax
.L117:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L131
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	%rax, %r12
	movq	(%r12), %rdx
	testb	$1, %dl
	je	.L118
.L116:
	movq	-1(%rdx), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L118
	movq	(%r14), %rcx
	andq	$-262144, %rdx
	movq	%r14, %rax
	movq	24(%rdx), %rdi
	movl	$2, %edx
	movq	-1(%rcx), %rsi
	subq	$37592, %rdi
	cmpw	$64, 11(%rsi)
	jne	.L119
	xorl	%edx, %edx
	testb	$1, 11(%rcx)
	sete	%dl
	addl	%edx, %edx
.L119:
	movabsq	$824633720832, %rbx
	movl	%edx, -128(%rbp)
	movq	(%r14), %rdx
	movq	%rbx, -116(%rbp)
	movq	%rdi, -104(%rbp)
	movq	-1(%rdx), %rdx
	movzwl	11(%rdx), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L132
.L120:
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r12, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%r12, -64(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -124(%rbp)
	jne	.L121
	movq	-104(%rbp), %rax
	addq	$88, %rax
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L130:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L132:
	movq	%r14, %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L120
.L131:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20138:
	.size	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE, .-_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	.section	.text._ZN2v88internal10JSReceiver15GetDataPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE,"axG",@progbits,_ZN2v88internal10JSReceiver15GetDataPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10JSReceiver15GetDataPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE
	.type	_ZN2v88internal10JSReceiver15GetDataPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE, @function
_ZN2v88internal10JSReceiver15GetDataPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE:
.LFB14422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	(%rdi), %rdx
	movq	(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	movq	-1(%rcx), %r8
	movl	$2, %edx
	subq	$37592, %rdi
	cmpw	$64, 11(%r8)
	jne	.L134
	xorl	%edx, %edx
	testb	$1, 11(%rcx)
	sete	%dl
	addl	%edx, %edx
.L134:
	movabsq	$824633720832, %rcx
	movl	%edx, -112(%rbp)
	movq	(%rsi), %rdx
	movq	%rcx, -100(%rbp)
	movq	%rdi, -88(%rbp)
	movq	-1(%rdx), %rdx
	movzwl	11(%rdx), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L143
.L135:
	leaq	-112(%rbp), %r12
	movq	%rax, -80(%rbp)
	movq	%r12, %rdi
	movq	$0, -72(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	%rbx, -48(%rbp)
	movq	$-1, -40(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -108(%rbp)
	jne	.L136
	movq	-88(%rbp), %rax
	addq	$88, %rax
.L137:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L144
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L143:
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L135
.L144:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14422:
	.size	_ZN2v88internal10JSReceiver15GetDataPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE, .-_ZN2v88internal10JSReceiver15GetDataPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE
	.section	.text._ZN2v88internal19AsmJsCompilationJob16RecordHistogramsEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AsmJsCompilationJob16RecordHistogramsEPNS0_7IsolateE
	.type	_ZN2v88internal19AsmJsCompilationJob16RecordHistogramsEPNS0_7IsolateE, @function
_ZN2v88internal19AsmJsCompilationJob16RecordHistogramsEPNS0_7IsolateE:
.LFB20228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	40960(%rsi), %r12
	movl	280(%rbx), %esi
	leaq	2936(%r12), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movl	288(%rbx), %esi
	leaq	1528(%r12), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movl	272(%rbx), %esi
	leaq	1608(%r12), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	280(%rbx), %rcx
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.L146
	movslq	272(%rbx), %rax
	cqto
	idivq	%rcx
	movq	%rax, %rsi
.L146:
	popq	%rbx
	leaq	1648(%r12), %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Histogram9AddSampleEi@PLT
	.cfi_endproc
.LFE20228:
	.size	_ZN2v88internal19AsmJsCompilationJob16RecordHistogramsEPNS0_7IsolateE, .-_ZN2v88internal19AsmJsCompilationJob16RecordHistogramsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal5AsmJs17NewCompilationJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"../deps/v8/src/asmjs/asm-js.cc:194"
	.section	.text._ZN2v88internal5AsmJs17NewCompilationJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5AsmJs17NewCompilationJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorE
	.type	_ZN2v88internal5AsmJs17NewCompilationJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorE, @function
_ZN2v88internal5AsmJs17NewCompilationJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorE:
.LFB20229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$296, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -64(%rbp)
	call	_Znwm@PLT
	movq	32(%r12), %r15
	leaq	16+_ZTVN2v88internal14CompilationJobE(%rip), %rcx
	movq	%rax, %rbx
	leaq	136(%rax), %rax
	movq	%rcx, (%rbx)
	movq	%rax, -56(%rbp)
	movl	$1, 8(%rbx)
	movq	$0, 16(%rbx)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r12, %xmm0
	movq	%r15, 24(%rbx)
	leaq	72(%rbx), %r15
	movhps	-56(%rbp), %xmm0
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, 16(%rbx)
	leaq	16+_ZTVN2v88internal19AsmJsCompilationJobE(%rip), %rdx
	movups	%xmm0, 32(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rdx, (%rbx)
	leaq	.LC8(%rip), %rdx
	movups	%xmm0, 48(%rbx)
	movq	%r14, 64(%rbx)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	-64(%rbp), %r8
	movq	%r12, %rdx
	movq	%r15, %rsi
	leaq	136(%rbx), %rdi
	movq	%r8, %rcx
	call	_ZN2v88internal26UnoptimizedCompilationInfoC1EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, 0(%r13)
	movq	%r13, %rax
	movups	%xmm0, 232(%rbx)
	pxor	%xmm0, %xmm0
	movq	$0, 248(%rbx)
	movl	$0, 272(%rbx)
	movq	$0, 280(%rbx)
	movq	$0, 288(%rbx)
	movups	%xmm0, 256(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20229:
	.size	_ZN2v88internal5AsmJs17NewCompilationJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorE, .-_ZN2v88internal5AsmJs17NewCompilationJobEPNS0_9ParseInfoEPNS0_15FunctionLiteralEPNS0_19AccountingAllocatorE
	.section	.rodata._ZN2v88internal5AsmJs18InstantiateAsmWasmEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_11AsmWasmDataEEENS4_INS0_10JSReceiverEEESA_NS4_INS0_13JSArrayBufferEEE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"Requires standard library"
.LC12:
	.string	"min"
.LC13:
	.string	"max"
.LC14:
	.string	"abs"
.LC15:
	.string	"fround"
.LC16:
	.string	"acos"
.LC17:
	.string	"asin"
.LC18:
	.string	"atan"
.LC19:
	.string	"cos"
.LC20:
	.string	"sin"
.LC21:
	.string	"tan"
.LC22:
	.string	"exp"
.LC23:
	.string	"log"
.LC24:
	.string	"atan2"
.LC25:
	.string	"pow"
.LC26:
	.string	"imul"
.LC27:
	.string	"clz32"
.LC28:
	.string	"ceil"
.LC29:
	.string	"floor"
.LC30:
	.string	"sqrt"
.LC31:
	.string	"E"
.LC33:
	.string	"LN10"
.LC35:
	.string	"LN2"
.LC37:
	.string	"LOG2E"
.LC39:
	.string	"LOG10E"
.LC41:
	.string	"PI"
.LC43:
	.string	"SQRT1_2"
.LC45:
	.string	"SQRT2"
.LC47:
	.string	"Int8Array"
.LC48:
	.string	"Uint8Array"
.LC49:
	.string	"Int16Array"
.LC50:
	.string	"Uint16Array"
.LC51:
	.string	"Int32Array"
.LC52:
	.string	"Uint32Array"
.LC53:
	.string	"Float32Array"
.LC54:
	.string	"Float64Array"
.LC55:
	.string	"Unexpected stdlib member"
.LC56:
	.string	"Requires heap buffer"
.LC57:
	.string	"Invalid heap size"
.LC58:
	.string	"AsmJs::Instantiate"
.LC59:
	.string	"NewArray"
.LC60:
	.string	"Internal wasm failure"
.LC61:
	.string	"success, %0.3f ms"
.LC62:
	.string	"__single_function__"
.LC63:
	.string	"exports"
.LC64:
	.string	"Internal wasm failure: %s"
	.section	.text._ZN2v88internal5AsmJs18InstantiateAsmWasmEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_11AsmWasmDataEEENS4_INS0_10JSReceiverEEESA_NS4_INS0_13JSArrayBufferEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal5AsmJs18InstantiateAsmWasmEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_11AsmWasmDataEEENS4_INS0_10JSReceiverEEESA_NS4_INS0_13JSArrayBufferEEE
	.type	_ZN2v88internal5AsmJs18InstantiateAsmWasmEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_11AsmWasmDataEEENS4_INS0_10JSReceiverEEESA_NS4_INS0_13JSArrayBufferEEE, @function
_ZN2v88internal5AsmJs18InstantiateAsmWasmEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_11AsmWasmDataEEENS4_INS0_10JSReceiverEEESA_NS4_INS0_13JSArrayBufferEEE:
.LFB20233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$344, %rsp
	movq	%rcx, -312(%rbp)
	movq	%r8, -360(%rbp)
	movq	%r9, -344(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%r9, -336(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	41112(%r14), %rdi
	movq	%rax, -368(%rbp)
	movq	(%rbx), %rax
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L154
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
	movq	0(%r13), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L624
.L157:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L158
	movq	%r8, -320(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-320(%rbp), %r8
	movq	%rax, %r12
.L159:
	movq	45752(%r14), %r15
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r8, -320(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm10WasmEngine23FinalizeTranslatedAsmJsEPNS0_7IsolateENS0_6HandleINS0_11AsmWasmDataEEENS5_INS0_6ScriptEEE@PLT
	movq	%rax, -352(%rbp)
	movq	0(%r13), %rax
	leaq	-272(%rbp), %r13
	movq	%r13, %rdi
	movq	%rax, -272(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-320(%rbp), %r8
	movl	%eax, -328(%rbp)
	movq	(%r8), %rax
	movq	7(%rax), %rcx
	testq	%rcx, %rcx
	je	.L161
	cmpq	$0, -312(%rbp)
	je	.L625
	testb	$1, %cl
	jne	.L626
.L164:
	testb	$2, %cl
	jne	.L627
.L176:
	testb	$4, %cl
	jne	.L628
.L183:
	testb	$8, %cl
	jne	.L629
.L188:
	testb	$16, %cl
	jne	.L630
.L193:
	testb	$32, %cl
	jne	.L631
.L198:
	testb	$64, %cl
	jne	.L632
.L203:
	testb	$-128, %cl
	jne	.L633
.L208:
	testb	$1, %ch
	jne	.L634
.L213:
	testb	$2, %ch
	jne	.L635
.L218:
	testb	$4, %ch
	jne	.L636
.L223:
	testb	$8, %ch
	jne	.L637
.L228:
	testb	$16, %ch
	jne	.L638
.L233:
	testb	$32, %ch
	jne	.L639
.L238:
	testb	$64, %ch
	jne	.L640
.L243:
	testb	$-128, %ch
	jne	.L641
.L248:
	testl	$65536, %ecx
	jne	.L642
.L253:
	testl	$131072, %ecx
	jne	.L643
.L258:
	testl	$262144, %ecx
	jne	.L644
.L263:
	testl	$524288, %ecx
	jne	.L645
.L268:
	testl	$1048576, %ecx
	jne	.L646
.L273:
	testl	$2097152, %ecx
	jne	.L647
.L278:
	testl	$4194304, %ecx
	jne	.L648
.L283:
	testl	$8388608, %ecx
	jne	.L649
.L288:
	testl	$16777216, %ecx
	jne	.L650
.L293:
	testl	$33554432, %ecx
	jne	.L651
.L298:
	testl	$67108864, %ecx
	jne	.L652
.L303:
	testl	$134217728, %ecx
	jne	.L653
.L308:
	testl	$268435456, %ecx
	jne	.L654
.L313:
	xorl	%eax, %eax
	testl	$536870912, %ecx
	jne	.L655
.L318:
	testl	$1073741824, %ecx
	jne	.L656
.L328:
	testl	$2147483648, %ecx
	jne	.L657
.L338:
	btq	$32, %rcx
	jc	.L658
.L348:
	btq	$33, %rcx
	jc	.L659
.L358:
	btq	$34, %rcx
	jc	.L660
.L368:
	btq	$35, %rcx
	jc	.L661
	btq	$36, %rcx
	jc	.L392
	testb	%al, %al
	je	.L161
.L391:
	cmpq	$0, -344(%rbp)
	je	.L662
	movq	-344(%rbp), %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal4wasm17WasmMemoryTracker25MarkWasmMemoryNotGrowableENS0_6HandleINS0_13JSArrayBufferEEE@PLT
	movq	(%rbx), %rax
	movq	23(%rax), %rbx
	cmpq	$4095, %rbx
	jbe	.L404
	call	_ZN2v88internal4wasm13max_mem_pagesEv@PLT
	movl	%eax, %eax
	salq	$16, %rax
	cmpq	%rax, %rbx
	ja	.L404
	cmpq	$16777215, %rbx
	ja	.L405
	leal	-1(%rbx), %eax
	testl	%ebx, %eax
	je	.L406
	.p2align 4,,10
	.p2align 3
.L404:
	movl	-328(%rbp), %esi
	movq	%r12, %rdi
	leaq	.LC57(%rip), %rdx
	xorl	%r12d, %r12d
	call	_ZN2v88internal12_GLOBAL__N_126ReportInstantiationFailureENS0_6HandleINS0_6ScriptEEEiPKc
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L161:
	movq	$0, -336(%rbp)
.L406:
	leaq	.LC58(%rip), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	-336(%rbp), %r9
	movq	%rax, -184(%rbp)
	movq	-360(%rbp), %r8
	leaq	-192(%rbp), %rax
	movq	-352(%rbp), %rcx
	movq	%rax, -312(%rbp)
	movq	%rax, %rdx
	leaq	-152(%rbp), %rax
	movq	%r14, -192(%rbp)
	movl	$0, -176(%rbp)
	movq	%rax, -168(%rbp)
	movq	$0, -160(%rbp)
	movb	$0, -152(%rbp)
	call	_ZN2v88internal4wasm10WasmEngine15SyncInstantiateEPNS0_7IsolateEPNS1_12ErrorThrowerENS0_6HandleINS0_16WasmModuleObjectEEENS0_11MaybeHandleINS0_10JSReceiverEEENSA_INS0_13JSArrayBufferEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L663
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	leaq	-296(%rbp), %rdi
	subq	-368(%rbp), %rax
	movq	%rax, -296(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	cmpb	$0, _ZN2v88internal26FLAG_suppress_asm_messagesE(%rip)
	jne	.L414
	cmpb	$0, _ZN2v88internal19FLAG_trace_asm_timeE(%rip)
	je	.L414
	leaq	-112(%rbp), %rdi
	movl	$50, %esi
	leaq	.LC61(%rip), %rdx
	movl	$1, %eax
	movq	%rdi, -128(%rbp)
	movq	$50, -120(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	cmpl	$-1, %eax
	je	.L664
	movq	-128(%rbp), %rdx
	cltq
	movl	-328(%rbp), %ecx
	movq	%r12, %rsi
	movq	%rax, -120(%rbp)
	movq	%r13, %rdi
	movq	%rdx, -288(%rbp)
	movl	%ecx, %edx
	movq	%rax, -280(%rbp)
	movq	(%r12), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r15
	call	_ZN2v88internal15MessageLocationC1ENS0_6HandleINS0_6ScriptEEEii@PLT
	leaq	-288(%rbp), %rsi
	subq	$37592, %r15
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%r13, %rdx
	movl	$356, %esi
	movq	%r15, %rdi
	movq	%rax, %rcx
	xorl	%r8d, %r8d
	call	_ZN2v88internal14MessageHandler17MakeMessageObjectEPNS0_7IsolateENS0_15MessageTemplateEPKNS0_15MessageLocationENS0_6HandleINS0_6ObjectEEENS8_INS0_10FixedArrayEEE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movabsq	$17179869184, %rcx
	movq	%rax, %rdx
	movq	(%rax), %rax
	movq	%rcx, 87(%rax)
	call	_ZN2v88internal14MessageHandler13ReportMessageEPNS0_7IsolateEPKNS0_15MessageLocationENS0_6HandleINS0_15JSMessageObjectEEE@PLT
.L414:
	leaq	.LC62(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	$19, -264(%rbp)
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%rax, %r12
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L416
.L418:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %r15
.L417:
	movq	(%r12), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	je	.L665
.L419:
	movl	%eax, -272(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -260(%rbp)
	movq	%r14, -248(%rbp)
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L666
.L420:
	movq	%r13, %rdi
	movq	%r12, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	%rbx, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	%r15, -208(%rbp)
	movq	$-1, -200(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -268(%rbp)
	jne	.L421
	movq	-248(%rbp), %rax
	leaq	88(%rax), %r12
.L422:
	movq	(%r12), %rax
	cmpq	%rax, 88(%r14)
	je	.L423
.L411:
	movq	-312(%rbp), %rdi
	call	_ZN2v88internal4wasm12ErrorThrowerD1Ev@PLT
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L663:
	movq	96(%r14), %rax
	cmpq	12480(%r14), %rax
	je	.L408
	movq	%rax, 12480(%r14)
.L408:
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	je	.L409
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$100, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L667
.L410:
	movq	-168(%rbp), %rcx
	movq	%r13, %rdi
	movl	$100, %esi
	xorl	%eax, %eax
	leaq	.LC64(%rip), %rdx
	movq	%r13, -272(%rbp)
	movq	$100, -264(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movl	-328(%rbp), %esi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal12_GLOBAL__N_126ReportInstantiationFailureENS0_6HandleINS0_6ScriptEEEiPKc
	movq	%r13, %rdi
	call	_ZdaPv@PLT
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L158:
	movq	41088(%r14), %r12
	cmpq	%r12, 41096(%r14)
	je	.L668
.L160:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r12)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L154:
	movq	41088(%r14), %r8
	cmpq	41096(%r14), %r8
	je	.L669
.L156:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r8)
	movq	0(%r13), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	je	.L157
.L624:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L157
	movq	23(%rsi), %rsi
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L625:
	movl	-328(%rbp), %esi
	movq	%r12, %rdi
	leaq	.LC9(%rip), %rdx
	xorl	%r12d, %r12d
	call	_ZN2v88internal12_GLOBAL__N_126ReportInstantiationFailureENS0_6HandleINS0_6ScriptEEEiPKc
	.p2align 4,,10
	.p2align 3
.L163:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L670
	addq	$344, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L422
.L423:
	leaq	.LC63(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	$7, -264(%rbp)
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	%rax, %r12
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L424
.L426:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %r15
.L425:
	movq	(%r12), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L427
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L427:
	movl	%eax, -272(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -260(%rbp)
	movq	%r14, -248(%rbp)
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L671
.L428:
	movq	%r13, %rdi
	movq	%r12, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	%rbx, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	%r15, -208(%rbp)
	movq	$-1, -200(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -268(%rbp)
	jne	.L429
	movq	-248(%rbp), %rax
	leaq	88(%rax), %r12
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L409:
	movl	-328(%rbp), %esi
	leaq	.LC60(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_126ReportInstantiationFailureENS0_6HandleINS0_6ScriptEEEiPKc
.L431:
	movq	-312(%rbp), %rdi
	xorl	%r12d, %r12d
	call	_ZN2v88internal4wasm12ErrorThrower5ResetEv@PLT
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L627:
	movq	-312(%rbp), %rax
	leaq	2880(%r14), %rsi
	movq	(%rax), %rax
	movq	%rax, -320(%rbp)
	movq	%rax, %rdx
	movq	2880(%r14), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	movq	-1(%rax), %r8
	movl	$2, %edx
	subq	$37592, %rdi
	cmpw	$64, 11(%r8)
	jne	.L177
	xorl	%edx, %edx
	testb	$1, 11(%rax)
	sete	%dl
	addl	%edx, %edx
.L177:
	movabsq	$824633720832, %rax
	movl	%edx, -272(%rbp)
	movq	%rax, -260(%rbp)
	movq	2880(%r14), %rax
	movq	%rdi, -248(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L672
.L178:
	movq	-312(%rbp), %rax
	movq	%r13, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rsi, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	%rax, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	%rax, -208(%rbp)
	movq	$-1, -200(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -268(%rbp)
	movq	-320(%rbp), %rcx
	jne	.L179
	movq	-248(%rbp), %rax
	addq	$88, %rax
.L180:
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L673
	.p2align 4,,10
	.p2align 3
.L175:
	movl	-328(%rbp), %esi
	movq	%r12, %rdi
	leaq	.LC55(%rip), %rdx
	xorl	%r12d, %r12d
	call	_ZN2v88internal12_GLOBAL__N_126ReportInstantiationFailureENS0_6HandleINS0_6ScriptEEEiPKc
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L665:
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L416:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L418
	movq	%rbx, %r15
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L628:
	xorl	%edx, %edx
	leaq	.LC12(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$3, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andl	$1, %edx
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$414, %rax
	jne	.L175
	andq	$-5, %rcx
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L626:
	movq	-312(%rbp), %rax
	leaq	2696(%r14), %rsi
	movq	(%rax), %rax
	movq	%rax, -320(%rbp)
	movq	%rax, %rdx
	movq	2696(%r14), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	movq	-1(%rax), %r8
	movl	$2, %edx
	subq	$37592, %rdi
	cmpw	$64, 11(%r8)
	jne	.L165
	xorl	%edx, %edx
	testb	$1, 11(%rax)
	sete	%dl
	addl	%edx, %edx
.L165:
	movabsq	$824633720832, %rax
	movl	%edx, -272(%rbp)
	movq	%rax, -260(%rbp)
	movq	2696(%r14), %rax
	movq	%rdi, -248(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L674
.L166:
	movq	-312(%rbp), %rax
	movq	%r13, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rsi, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	%rax, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	%rax, -208(%rbp)
	movq	$-1, -200(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -268(%rbp)
	movq	-320(%rbp), %rcx
	jne	.L167
	movq	-248(%rbp), %rax
	addq	$88, %rax
.L168:
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L675
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L173:
	andpd	.LC10(%rip), %xmm0
	ucomisd	.LC11(%rip), %xmm0
	jbe	.L175
	andq	$-2, %rcx
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L668:
	movq	%r14, %rdi
	movq	%rsi, -328(%rbp)
	movq	%r8, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-328(%rbp), %rsi
	movq	-320(%rbp), %r8
	movq	%rax, %r12
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L669:
	movq	%r14, %rdi
	movq	%rsi, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L675:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L175
	movsd	7(%rax), %xmm0
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L167:
	movq	%r13, %rdi
	movq	%rcx, -320(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-320(%rbp), %rcx
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L429:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r12
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L666:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r12
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L673:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L175
	movsd	7(%rax), %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.L175
	andq	$-3, %rcx
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L631:
	xorl	%edx, %edx
	leaq	.LC15(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$6, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andl	$1, %edx
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$833, %rax
	jne	.L175
	andq	$-33, %rcx
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L662:
	movl	-328(%rbp), %esi
	movq	%r12, %rdi
	leaq	.LC56(%rip), %rdx
	xorl	%r12d, %r12d
	call	_ZN2v88internal12_GLOBAL__N_126ReportInstantiationFailureENS0_6HandleINS0_6ScriptEEEiPKc
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L405:
	testl	$16777215, %ebx
	je	.L406
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L179:
	movq	%r13, %rdi
	movq	%rcx, -320(%rbp)
	call	_ZN2v88internal10JSReceiver15GetDataPropertyEPNS0_14LookupIteratorE@PLT
	movq	-320(%rbp), %rcx
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L629:
	xorl	%edx, %edx
	leaq	.LC13(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$3, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andl	$1, %edx
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$413, %rax
	jne	.L175
	andq	$-9, %rcx
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L424:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L426
	movq	%rbx, %r15
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L630:
	xorl	%edx, %edx
	leaq	.LC14(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$3, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andl	$1, %edx
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$409, %rax
	jne	.L175
	andq	$-17, %rcx
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L674:
	movq	%rcx, -320(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-320(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L671:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r12
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L672:
	movq	%rcx, -320(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-320(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L632:
	xorl	%edx, %edx
	leaq	.LC16(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$4, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andl	$1, %edx
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$820, %rax
	jne	.L175
	andq	$-65, %rcx
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L633:
	xorl	%edx, %edx
	leaq	.LC17(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$4, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andl	$1, %edx
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$822, %rax
	jne	.L175
	andb	$127, %cl
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L664:
	leaq	.LC6(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L634:
	xorl	%edx, %edx
	leaq	.LC18(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$4, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andb	$1, %dl
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$824, %rax
	jne	.L175
	andb	$-2, %ch
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L635:
	xorl	%edx, %edx
	leaq	.LC19(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$3, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andb	$1, %dl
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$829, %rax
	jne	.L175
	andb	$-3, %ch
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L636:
	xorl	%edx, %edx
	leaq	.LC20(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$3, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andb	$1, %dl
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$838, %rax
	jne	.L175
	andb	$-5, %ch
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L637:
	xorl	%edx, %edx
	leaq	.LC21(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$3, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andb	$1, %dl
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$842, %rax
	jne	.L175
	andb	$-9, %ch
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L638:
	xorl	%edx, %edx
	leaq	.LC22(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$3, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andb	$1, %dl
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$831, %rax
	jne	.L175
	andb	$-17, %ch
	jmp	.L233
.L639:
	xorl	%edx, %edx
	leaq	.LC23(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$3, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andb	$1, %dl
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$834, %rax
	jne	.L175
	andb	$-33, %ch
	jmp	.L238
.L640:
	xorl	%edx, %edx
	leaq	.LC24(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$5, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andb	$1, %dl
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$825, %rax
	jne	.L175
	andb	$-65, %ch
	jmp	.L243
.L670:
	call	__stack_chk_fail@PLT
.L641:
	xorl	%edx, %edx
	leaq	.LC25(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$3, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andb	$1, %dl
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$415, %rax
	jne	.L175
	andb	$127, %ch
	jmp	.L248
.L642:
	xorl	%edx, %edx
	leaq	.LC26(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$4, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andb	$1, %dl
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$412, %rax
	jne	.L175
	andq	$-65537, %rcx
	jmp	.L253
.L643:
	xorl	%edx, %edx
	leaq	.LC27(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$5, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andb	$1, %dl
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$828, %rax
	jne	.L175
	andq	$-131073, %rcx
	jmp	.L258
.L646:
	xorl	%edx, %edx
	leaq	.LC30(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$4, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andb	$1, %dl
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$841, %rax
	jne	.L175
	andq	$-1048577, %rcx
	jmp	.L273
.L645:
	xorl	%edx, %edx
	leaq	.LC29(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$5, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andb	$1, %dl
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$411, %rax
	jne	.L175
	andq	$-524289, %rcx
	jmp	.L268
.L644:
	xorl	%edx, %edx
	leaq	.LC28(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$4, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L175
	movq	23(%rax), %rax
	movq	7(%rax), %rdx
	andb	$1, %dl
	jne	.L175
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$410, %rax
	jne	.L175
	andq	$-262145, %rcx
	jmp	.L263
.L647:
	xorl	%edx, %edx
	leaq	.LC31(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$1, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L676
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L282:
	ucomisd	.LC32(%rip), %xmm0
	jp	.L175
	jne	.L175
	andq	$-2097153, %rcx
	jmp	.L278
.L655:
	xorl	%edx, %edx
	leaq	.LC47(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$9, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	%rax, %rdx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L175
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	607(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L322
	movq	%rdx, -376(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-376(%rbp), %rdx
	movq	-320(%rbp), %rcx
	cmpq	%rax, %rdx
	je	.L327
	testq	%rax, %rax
	je	.L175
.L326:
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	jne	.L175
.L327:
	andq	$-536870913, %rcx
	movl	$1, %eax
	jmp	.L318
.L322:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L677
.L325:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%r14)
	movq	%rsi, (%rax)
	cmpq	%rax, %rdx
	jne	.L326
	jmp	.L327
.L659:
	xorl	%edx, %edx
	leaq	.LC51(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$10, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	%rax, %rdx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L175
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	599(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L362
	movq	%rdx, -376(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-376(%rbp), %rdx
	movq	-320(%rbp), %rcx
	cmpq	%rax, %rdx
	je	.L367
	testq	%rax, %rax
	je	.L175
.L366:
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	jne	.L175
.L367:
	btrq	$33, %rcx
	movl	$1, %eax
	jmp	.L358
.L362:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L678
.L365:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%r14)
	movq	%rsi, (%rax)
	cmpq	%rax, %rdx
	jne	.L366
	jmp	.L367
.L651:
	xorl	%edx, %edx
	leaq	.LC39(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$6, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L679
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L302:
	ucomisd	.LC40(%rip), %xmm0
	jp	.L175
	jne	.L175
	andq	$-33554433, %rcx
	jmp	.L298
.L661:
	xorl	%edx, %edx
	leaq	.LC53(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$12, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	%rax, %rdx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L175
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	399(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L382
	movq	%rdx, -376(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-376(%rbp), %rdx
	movq	-320(%rbp), %rcx
	cmpq	%rax, %rdx
	je	.L387
	testq	%rax, %rax
	je	.L175
.L386:
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	jne	.L175
.L387:
	btq	$36, %rcx
	jnc	.L391
.L392:
	leaq	.LC54(%rip), %rax
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -272(%rbp)
	movq	$12, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE
	movq	%rax, %rbx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L175
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	407(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L396
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	cmpq	%rax, %rbx
	je	.L391
	testq	%rax, %rax
	je	.L175
.L400:
	movq	(%rax), %rax
	cmpq	%rax, (%rbx)
	je	.L391
	jmp	.L175
.L382:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L680
.L385:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%r14)
	movq	%rsi, (%rax)
	cmpq	%rax, %rdx
	jne	.L386
	jmp	.L387
.L396:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L681
.L399:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	cmpq	%rax, %rbx
	jne	.L400
	jmp	.L391
.L680:
	movq	%r14, %rdi
	movq	%rsi, -384(%rbp)
	movq	%rdx, -376(%rbp)
	movq	%rcx, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-384(%rbp), %rsi
	movq	-376(%rbp), %rdx
	movq	-320(%rbp), %rcx
	jmp	.L385
.L681:
	movq	%r14, %rdi
	movq	%rsi, -312(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %rsi
	jmp	.L399
.L653:
	xorl	%edx, %edx
	leaq	.LC43(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$7, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L682
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L312:
	ucomisd	.LC44(%rip), %xmm0
	jp	.L175
	jne	.L175
	andq	$-134217729, %rcx
	jmp	.L308
.L652:
	xorl	%edx, %edx
	leaq	.LC41(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$2, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L683
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L307:
	ucomisd	.LC42(%rip), %xmm0
	jp	.L175
	jne	.L175
	andq	$-67108865, %rcx
	jmp	.L303
.L682:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L175
	movsd	7(%rax), %xmm0
	jmp	.L312
.L683:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L175
	movsd	7(%rax), %xmm0
	jmp	.L307
.L654:
	xorl	%edx, %edx
	leaq	.LC45(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$5, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L684
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L317:
	ucomisd	.LC46(%rip), %xmm0
	jp	.L175
	jne	.L175
	andq	$-268435457, %rcx
	jmp	.L313
.L676:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L175
	movsd	7(%rax), %xmm0
	jmp	.L282
.L684:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L175
	movsd	7(%rax), %xmm0
	jmp	.L317
.L649:
	xorl	%edx, %edx
	leaq	.LC35(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$3, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L685
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L292:
	ucomisd	.LC36(%rip), %xmm0
	jp	.L175
	jne	.L175
	andq	$-8388609, %rcx
	jmp	.L288
.L648:
	xorl	%edx, %edx
	leaq	.LC33(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$4, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L686
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L287:
	ucomisd	.LC34(%rip), %xmm0
	jp	.L175
	jne	.L175
	andq	$-4194305, %rcx
	jmp	.L283
.L685:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L175
	movsd	7(%rax), %xmm0
	jmp	.L292
.L686:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L175
	movsd	7(%rax), %xmm0
	jmp	.L287
.L650:
	xorl	%edx, %edx
	leaq	.LC37(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$5, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_116StdlibMathMemberEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L687
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L297:
	ucomisd	.LC38(%rip), %xmm0
	jp	.L175
	jne	.L175
	andq	$-16777217, %rcx
	jmp	.L293
.L678:
	movq	%r14, %rdi
	movq	%rsi, -384(%rbp)
	movq	%rdx, -376(%rbp)
	movq	%rcx, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-384(%rbp), %rsi
	movq	-376(%rbp), %rdx
	movq	-320(%rbp), %rcx
	jmp	.L365
.L687:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L175
	movsd	7(%rax), %xmm0
	jmp	.L297
.L657:
	xorl	%edx, %edx
	leaq	.LC49(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$10, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	%rax, %rdx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L175
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	591(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L342
	movq	%rdx, -376(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-376(%rbp), %rdx
	movq	-320(%rbp), %rcx
	cmpq	%rax, %rdx
	je	.L347
	testq	%rax, %rax
	je	.L175
.L346:
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	jne	.L175
.L347:
	btrq	$31, %rcx
	movl	$1, %eax
	jmp	.L338
.L342:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L688
.L345:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%r14)
	movq	%rsi, (%rax)
	cmpq	%rax, %rdx
	jne	.L346
	jmp	.L347
.L656:
	xorl	%edx, %edx
	leaq	.LC48(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$10, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	%rax, %rdx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L175
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1559(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L332
	movq	%rdx, -376(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-376(%rbp), %rdx
	movq	-320(%rbp), %rcx
	cmpq	%rax, %rdx
	je	.L337
	testq	%rax, %rax
	je	.L175
.L336:
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	jne	.L175
.L337:
	andq	$-1073741825, %rcx
	movl	$1, %eax
	jmp	.L328
.L332:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L689
.L335:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%r14)
	movq	%rsi, (%rax)
	cmpq	%rax, %rdx
	jne	.L336
	jmp	.L337
.L688:
	movq	%r14, %rdi
	movq	%rsi, -384(%rbp)
	movq	%rdx, -376(%rbp)
	movq	%rcx, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-384(%rbp), %rsi
	movq	-376(%rbp), %rdx
	movq	-320(%rbp), %rcx
	jmp	.L345
.L689:
	movq	%r14, %rdi
	movq	%rsi, -384(%rbp)
	movq	%rdx, -376(%rbp)
	movq	%rcx, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-384(%rbp), %rsi
	movq	-376(%rbp), %rdx
	movq	-320(%rbp), %rcx
	jmp	.L335
.L667:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$100, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L410
	leaq	.LC59(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L658:
	xorl	%edx, %edx
	leaq	.LC50(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$11, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	%rax, %rdx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L175
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1543(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L352
	movq	%rdx, -376(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-376(%rbp), %rdx
	movq	-320(%rbp), %rcx
	cmpq	%rax, %rdx
	je	.L357
	testq	%rax, %rax
	je	.L175
.L356:
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	jne	.L175
.L357:
	btrq	$32, %rcx
	movl	$1, %eax
	jmp	.L348
.L352:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L690
.L355:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%r14)
	movq	%rsi, (%rax)
	cmpq	%rax, %rdx
	jne	.L356
	jmp	.L357
.L677:
	movq	%r14, %rdi
	movq	%rsi, -384(%rbp)
	movq	%rdx, -376(%rbp)
	movq	%rcx, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-384(%rbp), %rsi
	movq	-376(%rbp), %rdx
	movq	-320(%rbp), %rcx
	jmp	.L325
.L690:
	movq	%r14, %rdi
	movq	%rsi, -384(%rbp)
	movq	%rdx, -376(%rbp)
	movq	%rcx, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-384(%rbp), %rsi
	movq	-376(%rbp), %rdx
	movq	-320(%rbp), %rcx
	jmp	.L355
.L660:
	xorl	%edx, %edx
	leaq	.LC52(%rip), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -320(%rbp)
	movq	%rax, -272(%rbp)
	movq	$11, -264(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb@PLT
	movq	-312(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal10JSReceiver15GetDataPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEE
	movq	-320(%rbp), %rcx
	movq	%rax, %rdx
	movq	(%rax), %rax
	testb	$1, %al
	je	.L175
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L175
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1551(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L372
	movq	%rdx, -376(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-376(%rbp), %rdx
	movq	-320(%rbp), %rcx
	cmpq	%rax, %rdx
	je	.L377
	testq	%rax, %rax
	je	.L175
.L376:
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	jne	.L175
.L377:
	btrq	$34, %rcx
	movl	$1, %eax
	jmp	.L368
.L372:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L691
.L375:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%r14)
	movq	%rsi, (%rax)
	cmpq	%rax, %rdx
	jne	.L376
	jmp	.L377
.L679:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L175
	movsd	7(%rax), %xmm0
	jmp	.L302
.L691:
	movq	%r14, %rdi
	movq	%rsi, -384(%rbp)
	movq	%rdx, -376(%rbp)
	movq	%rcx, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-384(%rbp), %rsi
	movq	-376(%rbp), %rdx
	movq	-320(%rbp), %rcx
	jmp	.L375
	.cfi_endproc
.LFE20233:
	.size	_ZN2v88internal5AsmJs18InstantiateAsmWasmEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_11AsmWasmDataEEENS4_INS0_10JSReceiverEEESA_NS4_INS0_13JSArrayBufferEEE, .-_ZN2v88internal5AsmJs18InstantiateAsmWasmEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_11AsmWasmDataEEENS4_INS0_10JSReceiverEEESA_NS4_INS0_13JSArrayBufferEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal5AsmJs19kSingleFunctionNameE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal5AsmJs19kSingleFunctionNameE, @function
_GLOBAL__sub_I__ZN2v88internal5AsmJs19kSingleFunctionNameE:
.LFB25456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25456:
	.size	_GLOBAL__sub_I__ZN2v88internal5AsmJs19kSingleFunctionNameE, .-_GLOBAL__sub_I__ZN2v88internal5AsmJs19kSingleFunctionNameE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal5AsmJs19kSingleFunctionNameE
	.weak	_ZTVN2v88internal14CompilationJobE
	.section	.data.rel.ro.local._ZTVN2v88internal14CompilationJobE,"awG",@progbits,_ZTVN2v88internal14CompilationJobE,comdat
	.align 8
	.type	_ZTVN2v88internal14CompilationJobE, @object
	.size	_ZTVN2v88internal14CompilationJobE, 32
_ZTVN2v88internal14CompilationJobE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14CompilationJobD1Ev
	.quad	_ZN2v88internal14CompilationJobD0Ev
	.weak	_ZTVN2v88internal19AsmJsCompilationJobE
	.section	.data.rel.ro.local._ZTVN2v88internal19AsmJsCompilationJobE,"awG",@progbits,_ZTVN2v88internal19AsmJsCompilationJobE,comdat
	.align 8
	.type	_ZTVN2v88internal19AsmJsCompilationJobE, @object
	.size	_ZTVN2v88internal19AsmJsCompilationJobE, 48
_ZTVN2v88internal19AsmJsCompilationJobE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal19AsmJsCompilationJobD1Ev
	.quad	_ZN2v88internal19AsmJsCompilationJobD0Ev
	.quad	_ZN2v88internal19AsmJsCompilationJob14ExecuteJobImplEv
	.quad	_ZN2v88internal19AsmJsCompilationJob15FinalizeJobImplENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateE
	.globl	_ZN2v88internal5AsmJs19kSingleFunctionNameE
	.section	.data.rel.ro.local._ZN2v88internal5AsmJs19kSingleFunctionNameE,"aw"
	.align 8
	.type	_ZN2v88internal5AsmJs19kSingleFunctionNameE, @object
	.size	_ZN2v88internal5AsmJs19kSingleFunctionNameE, 8
_ZN2v88internal5AsmJs19kSingleFunctionNameE:
	.quad	.LC62
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC10:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC11:
	.long	4294967295
	.long	2146435071
	.align 8
.LC32:
	.long	2333366121
	.long	1074118410
	.align 8
.LC34:
	.long	3149223190
	.long	1073900465
	.align 8
.LC36:
	.long	4277811695
	.long	1072049730
	.align 8
.LC38:
	.long	1697350398
	.long	1073157447
	.align 8
.LC40:
	.long	354870542
	.long	1071369083
	.align 8
.LC42:
	.long	1413754136
	.long	1074340347
	.align 8
.LC44:
	.long	1719614413
	.long	1072079006
	.align 8
.LC46:
	.long	1719614413
	.long	1073127582
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
