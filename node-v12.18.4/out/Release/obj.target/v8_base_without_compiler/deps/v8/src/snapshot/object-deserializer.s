	.file	"object-deserializer.cc"
	.text
	.section	.text._ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB7245:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rcx), %r8
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7245:
	.size	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.text._ZN2v88internal18ObjectDeserializerD2Ev,"axG",@progbits,_ZN2v88internal18ObjectDeserializerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18ObjectDeserializerD2Ev
	.type	_ZN2v88internal18ObjectDeserializerD2Ev, @function
_ZN2v88internal18ObjectDeserializerD2Ev:
.LFB26995:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal18ObjectDeserializerE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal12DeserializerD2Ev@PLT
	.cfi_endproc
.LFE26995:
	.size	_ZN2v88internal18ObjectDeserializerD2Ev, .-_ZN2v88internal18ObjectDeserializerD2Ev
	.weak	_ZN2v88internal18ObjectDeserializerD1Ev
	.set	_ZN2v88internal18ObjectDeserializerD1Ev,_ZN2v88internal18ObjectDeserializerD2Ev
	.section	.text._ZN2v88internal18ObjectDeserializerD0Ev,"axG",@progbits,_ZN2v88internal18ObjectDeserializerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal18ObjectDeserializerD0Ev
	.type	_ZN2v88internal18ObjectDeserializerD0Ev, @function
_ZN2v88internal18ObjectDeserializerD0Ev:
.LFB26997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal18ObjectDeserializerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal12DeserializerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$624, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26997:
	.size	_ZN2v88internal18ObjectDeserializerD0Ev, .-_ZN2v88internal18ObjectDeserializerD0Ev
	.section	.text._ZN2v88internal18ObjectDeserializer11FlushICacheEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ObjectDeserializer11FlushICacheEv
	.type	_ZN2v88internal18ObjectDeserializer11FlushICacheEv, @function
_ZN2v88internal18ObjectDeserializer11FlushICacheEv:
.LFB21096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	184(%rdi), %r12
	movq	192(%rdi), %r13
	cmpq	%r13, %r12
	je	.L6
	.p2align 4,,10
	.p2align 3
.L8:
	movq	(%r12), %rbx
	addq	$8, %r12
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_WriteBarrierForCodeSlowENS0_4CodeE@PLT
	movslq	39(%rbx), %rsi
	leaq	63(%rbx), %rdi
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	cmpq	%r12, %r13
	jne	.L8
.L6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21096:
	.size	_ZN2v88internal18ObjectDeserializer11FlushICacheEv, .-_ZN2v88internal18ObjectDeserializer11FlushICacheEv
	.section	.rodata._ZN2v88internal18ObjectDeserializer26CommitPostProcessedObjectsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"new_internalized_strings().size() <= kMaxInt"
	.section	.rodata._ZN2v88internal18ObjectDeserializer26CommitPostProcessedObjectsEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal18ObjectDeserializer26CommitPostProcessedObjectsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ObjectDeserializer26CommitPostProcessedObjectsEv
	.type	_ZN2v88internal18ObjectDeserializer26CommitPostProcessedObjectsEv, @function
_ZN2v88internal18ObjectDeserializer26CommitPostProcessedObjectsEv:
.LFB21097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	264(%rdi), %rsi
	subq	256(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	sarq	$3, %rsi
	cmpq	$2147483647, %rsi
	jbe	.L27
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%rdi, %r14
	movq	80(%rdi), %rdi
	leaq	-80(%rbp), %rbx
	call	_ZN2v88internal11StringTable32EnsureCapacityForDeserializationEPNS0_7IsolateEi@PLT
	movq	264(%r14), %r13
	movq	256(%r14), %r12
	cmpq	%r13, %r12
	je	.L17
	.p2align 4,,10
	.p2align 3
.L16:
	movq	(%r12), %rax
	movq	%rbx, %rdi
	addq	$8, %r12
	movq	(%rax), %rsi
	call	_ZN2v88internal23StringTableInsertionKeyC1ENS0_6StringE@PLT
	movq	80(%r14), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal11StringTable14AddKeyNoResizeEPNS0_7IsolateEPNS0_14StringTableKeyE@PLT
	cmpq	%r12, %r13
	jne	.L16
.L17:
	movq	80(%r14), %rcx
	movq	288(%r14), %rdx
	leaq	-80(%rbp), %rbx
	movq	280(%r14), %r13
	movq	%rcx, -88(%rbp)
	leaq	37592(%rcx), %rax
	leaq	4680(%rcx), %r15
	movq	%rdx, -96(%rbp)
	cmpq	%rdx, %r13
	jne	.L19
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L28:
	movq	80(%r14), %rax
	addq	$37592, %rax
.L19:
	movslq	-32788(%rax), %r10
	movq	0(%r13), %r12
	leaq	-37592(%rax), %rdi
	movabsq	$4294967296, %rax
	leal	1(%r10), %edx
	movq	(%r12), %rsi
	salq	$32, %rdx
	cmpq	$2147483647, %r10
	cmovne	%rdx, %rax
	addq	$8, %r13
	movq	%rax, 4800(%rdi)
	movq	%r14, %rdi
	movq	%rax, 63(%rsi)
	movq	(%r12), %rsi
	call	_ZN2v88internal12Deserializer15LogScriptEventsENS0_6ScriptE@PLT
	movq	80(%r14), %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movl	$0, -80(%rbp)
	movq	%r12, -72(%rbp)
	call	_ZN2v88internal13WeakArrayList8AddToEndEPNS0_7IsolateENS0_6HandleIS1_EERKNS0_17MaybeObjectHandleE@PLT
	movq	-88(%rbp), %rcx
	movq	(%rax), %rax
	movq	%rax, 4680(%rcx)
	cmpq	%r13, -96(%rbp)
	jne	.L28
.L11:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L29
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L29:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21097:
	.size	_ZN2v88internal18ObjectDeserializer26CommitPostProcessedObjectsEv, .-_ZN2v88internal18ObjectDeserializer26CommitPostProcessedObjectsEv
	.section	.text._ZN2v88internal18ObjectDeserializer19LinkAllocationSitesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ObjectDeserializer19LinkAllocationSitesEv
	.type	_ZN2v88internal18ObjectDeserializer19LinkAllocationSitesEv, @function
_ZN2v88internal18ObjectDeserializer19LinkAllocationSitesEv:
.LFB21098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	80(%rdi), %rcx
	movq	168(%rdi), %r15
	movq	160(%rdi), %r12
	leaq	37592(%rcx), %rax
	movq	%rax, -64(%rbp)
	cmpq	%r15, %r12
	jne	.L41
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%r13, 39(%rbx)
	testb	$1, %r13b
	je	.L37
.L63:
	movq	%r13, %r9
	andq	$-262144, %r9
	movq	8(%r9), %rax
	movq	%r9, -56(%rbp)
	testl	$262144, %eax
	je	.L39
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rcx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	8(%r9), %rax
.L39:
	testb	$24, %al
	je	.L37
	testb	$24, 8(%r14)
	jne	.L37
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%rbx, 39128(%rcx)
.L32:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L30
.L41:
	movq	(%r12), %rbx
	movq	%rbx, %r14
	andq	$-262144, %r14
	movq	24(%r14), %rdx
	movq	-1(%rbx), %rax
	cmpq	%rax, -33296(%rdx)
	jne	.L32
	movq	39128(%rcx), %r13
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	leaq	39(%rbx), %rsi
	jne	.L33
	movq	-64(%rbp), %rax
	movq	-37504(%rax), %r13
	movq	%r13, 39(%rbx)
	testb	$1, %r13b
	jne	.L63
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L30:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21098:
	.size	_ZN2v88internal18ObjectDeserializer19LinkAllocationSitesEv, .-_ZN2v88internal18ObjectDeserializer19LinkAllocationSitesEv
	.section	.text._ZN2v88internal18ObjectDeserializer11DeserializeEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ObjectDeserializer11DeserializeEPNS0_7IsolateE
	.type	_ZN2v88internal18ObjectDeserializer11DeserializeEPNS0_7IsolateE, @function
_ZN2v88internal18ObjectDeserializer11DeserializeEPNS0_7IsolateE:
.LFB21095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	leaq	328(%r12), %r13
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12Deserializer10InitializeEPNS0_7IsolateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal21DeserializerAllocator12ReserveSpaceEv@PLT
	testb	%al, %al
	jne	.L65
	xorl	%eax, %eax
.L66:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L84
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movq	41088(%r15), %rax
	leaq	-64(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	addl	$1, 41104(%r15)
	leaq	-56(%rbp), %r8
	movl	$19, %esi
	movq	%rax, -80(%rbp)
	movq	41096(%r15), %rax
	movq	$0, -64(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal12Deserializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal12Deserializer26DeserializeDeferredObjectsEv@PLT
	movq	192(%r12), %rax
	movq	184(%r12), %r14
	movq	%rax, -72(%rbp)
	cmpq	%rax, %r14
	je	.L71
	.p2align 4,,10
	.p2align 3
.L70:
	movq	(%r14), %rbx
	addq	$8, %r14
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_WriteBarrierForCodeSlowENS0_4CodeE@PLT
	movslq	39(%rbx), %rsi
	leaq	63(%rbx), %rdi
	call	_ZN2v88internal21FlushInstructionCacheEPvm@PLT
	cmpq	%r14, -72(%rbp)
	jne	.L70
.L71:
	movq	%r12, %rdi
	call	_ZN2v88internal18ObjectDeserializer19LinkAllocationSitesEv
	movq	%r12, %rdi
	call	_ZN2v88internal12Deserializer15LogNewMapEventsEv@PLT
	movq	41112(%r15), %rdi
	movq	-64(%rbp), %rsi
	testq	%rdi, %rdi
	je	.L85
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L72:
	movq	%r12, %rdi
	call	_ZN2v88internal12Deserializer6RehashEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal21DeserializerAllocator45RegisterDeserializedObjectsForBlackAllocationEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal18ObjectDeserializer26CommitPostProcessedObjectsEv
	movq	-80(%rbp), %rax
	movq	(%rbx), %r12
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-88(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L74
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L74:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L75
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L85:
	movq	41088(%r15), %rbx
	cmpq	41096(%r15), %rbx
	je	.L86
.L73:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L75:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L87
.L77:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%r12, (%rax)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L77
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21095:
	.size	_ZN2v88internal18ObjectDeserializer11DeserializeEPNS0_7IsolateE, .-_ZN2v88internal18ObjectDeserializer11DeserializeEPNS0_7IsolateE
	.section	.rodata._ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_.str1.1,"aMS",@progbits,1
.LC2:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	.type	_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_, @function
_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_:
.LFB26325:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L102
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L98
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L103
.L90:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L97:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L104
	testq	%r13, %r13
	jg	.L93
	testq	%r9, %r9
	jne	.L96
.L94:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L93
.L96:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L103:
	testq	%rsi, %rsi
	jne	.L91
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L94
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L98:
	movl	$8, %r14d
	jmp	.L90
.L102:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L91:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L90
	.cfi_endproc
.LFE26325:
	.size	_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_, .-_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	.section	.text._ZN2v88internal18ObjectDeserializer29DeserializeSharedFunctionInfoEPNS0_7IsolateEPKNS0_18SerializedCodeDataENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ObjectDeserializer29DeserializeSharedFunctionInfoEPNS0_7IsolateEPKNS0_18SerializedCodeDataENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal18ObjectDeserializer29DeserializeSharedFunctionInfoEPNS0_7IsolateEPKNS0_18SerializedCodeDataENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal18ObjectDeserializer29DeserializeSharedFunctionInfoEPNS0_7IsolateEPKNS0_18SerializedCodeDataENS0_6HandleINS0_6StringEEE:
.LFB21091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-720(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$712, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -680(%rbp)
	leaq	16+_ZTVN2v88internal12DeserializerE(%rip), %rax
	movups	%xmm0, -664(%rbp)
	movups	%xmm0, -648(%rbp)
	movups	%xmm0, -632(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -608(%rbp)
	movaps	%xmm0, -592(%rbp)
	movl	$0, -616(%rbp)
	movq	%rax, -688(%rbp)
	call	_ZNK2v88internal18SerializedCodeData7PayloadEv@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -576(%rbp)
	movq	8(%r12), %rax
	movl	%edx, -568(%rbp)
	movl	$0, -564(%rbp)
	movl	(%rax), %eax
	movups	%xmm0, -552(%rbp)
	movl	%eax, -560(%rbp)
	movl	$1, %eax
	movups	%xmm0, -536(%rbp)
	movups	%xmm0, -520(%rbp)
	movups	%xmm0, -504(%rbp)
	movups	%xmm0, -488(%rbp)
	movups	%xmm0, -472(%rbp)
	movups	%xmm0, -456(%rbp)
	movups	%xmm0, -440(%rbp)
	movups	%xmm0, -424(%rbp)
	movups	%xmm0, -408(%rbp)
	movups	%xmm0, -392(%rbp)
	movups	%xmm0, -376(%rbp)
	movups	%xmm0, -360(%rbp)
	movups	%xmm0, -344(%rbp)
	movups	%xmm0, -328(%rbp)
	movups	%xmm0, -312(%rbp)
	movups	%xmm0, -296(%rbp)
	movups	%xmm0, -280(%rbp)
	movups	%xmm0, -264(%rbp)
	movups	%xmm0, -248(%rbp)
	movups	%xmm0, -232(%rbp)
	movups	%xmm0, -152(%rbp)
	movups	%xmm0, -136(%rbp)
	movl	$0, -168(%rbp)
	movb	$0, -164(%rbp)
	movl	$0, -160(%rbp)
	movups	%xmm0, -120(%rbp)
	movw	%ax, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movq	$0, -72(%rbp)
	call	_ZNK2v88internal18SerializedCodeData12ReservationsEv@PLT
	leaq	-360(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal21DeserializerAllocator17DecodeReservationERKSt6vectorINS0_14SerializedData11ReservationESaIS4_EE@PLT
	movq	-720(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L106
	call	_ZdlPv@PLT
.L106:
	movq	$0, -720(%rbp)
	movq	-376(%rbp), %rsi
	cmpq	-368(%rbp), %rsi
	je	.L107
	movq	$0, (%rsi)
	addq	$8, -376(%rbp)
.L108:
	leaq	16+_ZTVN2v88internal18ObjectDeserializerE(%rip), %r14
	movq	-592(%rbp), %r12
	movq	%r14, -688(%rbp)
	cmpq	-584(%rbp), %r12
	je	.L109
	movq	%rbx, (%r12)
	addq	$8, -592(%rbp)
.L110:
	leaq	-688(%rbp), %r15
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18ObjectDeserializer11DeserializeEPNS0_7IsolateE
	movq	%r15, %rdi
	movq	%r14, -688(%rbp)
	movq	%rax, %r12
	call	_ZN2v88internal12DeserializerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L142
	addq	$712, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	leaq	-384(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L109:
	movq	-600(%rbp), %r8
	movq	%r12, %rdx
	movabsq	$1152921504606846975, %rsi
	subq	%r8, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L143
	testq	%rax, %rax
	je	.L123
	movabsq	$9223372036854775800, %rcx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L144
.L112:
	movq	%rcx, %rdi
	movq	%rdx, -744(%rbp)
	movq	%r8, -736(%rbp)
	movq	%rcx, -728(%rbp)
	call	_Znwm@PLT
	movq	-728(%rbp), %rcx
	movq	-736(%rbp), %r8
	movq	-744(%rbp), %rdx
	movq	%rax, %r15
	addq	%rax, %rcx
	leaq	8(%rax), %rax
.L113:
	movq	%rbx, (%r15,%rdx)
	cmpq	%r8, %r12
	je	.L114
	leaq	-8(%r12), %rsi
	leaq	15(%r15), %rax
	subq	%r8, %rsi
	subq	%r8, %rax
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L126
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L126
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L116:
	movdqu	(%r8,%rdx), %xmm1
	movups	%xmm1, (%r15,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L116
	movq	%rdi, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rdx
	leaq	(%r8,%rdx), %rax
	addq	%r15, %rdx
	cmpq	%r9, %rdi
	je	.L118
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L118:
	leaq	16(%r15,%rsi), %rax
.L114:
	testq	%r8, %r8
	je	.L119
	movq	%r8, %rdi
	movq	%rax, -736(%rbp)
	movq	%rcx, -728(%rbp)
	call	_ZdlPv@PLT
	movq	-736(%rbp), %rax
	movq	-728(%rbp), %rcx
.L119:
	movq	%r15, %xmm0
	movq	%rax, %xmm2
	movq	%rcx, -584(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -600(%rbp)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L144:
	testq	%rdi, %rdi
	jne	.L145
	movl	$8, %eax
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L123:
	movl	$8, %ecx
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L126:
	movq	%r15, %rdx
	movq	%r8, %rax
	.p2align 4,,10
	.p2align 3
.L115:
	movq	(%rax), %rdi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rdi, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L115
	jmp	.L118
.L142:
	call	__stack_chk_fail@PLT
.L143:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L145:
	cmpq	%rsi, %rdi
	movq	%rsi, %rcx
	cmovbe	%rdi, %rcx
	salq	$3, %rcx
	jmp	.L112
	.cfi_endproc
.LFE21091:
	.size	_ZN2v88internal18ObjectDeserializer29DeserializeSharedFunctionInfoEPNS0_7IsolateEPKNS0_18SerializedCodeDataENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal18ObjectDeserializer29DeserializeSharedFunctionInfoEPNS0_7IsolateEPKNS0_18SerializedCodeDataENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal18ObjectDeserializerC2EPKNS0_18SerializedCodeDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ObjectDeserializerC2EPKNS0_18SerializedCodeDataE
	.type	_ZN2v88internal18ObjectDeserializerC2EPKNS0_18SerializedCodeDataE, @function
_ZN2v88internal18ObjectDeserializerC2EPKNS0_18SerializedCodeDataE:
.LFB21089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	leaq	-64(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal12DeserializerE(%rip), %rax
	movl	$0, 72(%rdi)
	movq	%rax, (%rdi)
	movups	%xmm0, 8(%rdi)
	movups	%xmm0, 24(%rdi)
	movups	%xmm0, 40(%rdi)
	movups	%xmm0, 56(%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 96(%rdi)
	movq	%rsi, %rdi
	call	_ZNK2v88internal18SerializedCodeData7PayloadEv@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%edx, 120(%rbx)
	movq	%rax, 112(%rbx)
	movl	$0, 124(%rbx)
	movq	8(%r12), %rax
	movl	(%rax), %eax
	movups	%xmm0, 136(%rbx)
	movups	%xmm0, 152(%rbx)
	movl	%eax, 128(%rbx)
	movl	$1, %eax
	movups	%xmm0, 168(%rbx)
	movups	%xmm0, 184(%rbx)
	movups	%xmm0, 200(%rbx)
	movups	%xmm0, 216(%rbx)
	movups	%xmm0, 232(%rbx)
	movups	%xmm0, 248(%rbx)
	movups	%xmm0, 264(%rbx)
	movups	%xmm0, 280(%rbx)
	movups	%xmm0, 296(%rbx)
	movups	%xmm0, 312(%rbx)
	movups	%xmm0, 328(%rbx)
	movups	%xmm0, 344(%rbx)
	movups	%xmm0, 360(%rbx)
	movups	%xmm0, 376(%rbx)
	movups	%xmm0, 392(%rbx)
	movups	%xmm0, 408(%rbx)
	movups	%xmm0, 424(%rbx)
	movups	%xmm0, 440(%rbx)
	movups	%xmm0, 456(%rbx)
	movups	%xmm0, 536(%rbx)
	movups	%xmm0, 552(%rbx)
	movl	$0, 520(%rbx)
	movb	$0, 524(%rbx)
	movl	$0, 528(%rbx)
	movups	%xmm0, 568(%rbx)
	movw	%ax, 592(%rbx)
	movq	$0, 616(%rbx)
	movups	%xmm0, 600(%rbx)
	call	_ZNK2v88internal18SerializedCodeData12ReservationsEv@PLT
	leaq	328(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal21DeserializerAllocator17DecodeReservationERKSt6vectorINS0_14SerializedData11ReservationESaIS4_EE@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L147
	call	_ZdlPv@PLT
.L147:
	movq	$0, -64(%rbp)
	movq	312(%rbx), %rsi
	cmpq	320(%rbx), %rsi
	je	.L148
	movq	$0, (%rsi)
	addq	$8, 312(%rbx)
.L149:
	leaq	16+_ZTVN2v88internal18ObjectDeserializerE(%rip), %rax
	movq	%rax, (%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L155
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	leaq	304(%rbx), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIPhSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	jmp	.L149
.L155:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21089:
	.size	_ZN2v88internal18ObjectDeserializerC2EPKNS0_18SerializedCodeDataE, .-_ZN2v88internal18ObjectDeserializerC2EPKNS0_18SerializedCodeDataE
	.globl	_ZN2v88internal18ObjectDeserializerC1EPKNS0_18SerializedCodeDataE
	.set	_ZN2v88internal18ObjectDeserializerC1EPKNS0_18SerializedCodeDataE,_ZN2v88internal18ObjectDeserializerC2EPKNS0_18SerializedCodeDataE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal18ObjectDeserializerC2EPKNS0_18SerializedCodeDataE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal18ObjectDeserializerC2EPKNS0_18SerializedCodeDataE, @function
_GLOBAL__sub_I__ZN2v88internal18ObjectDeserializerC2EPKNS0_18SerializedCodeDataE:
.LFB27027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27027:
	.size	_GLOBAL__sub_I__ZN2v88internal18ObjectDeserializerC2EPKNS0_18SerializedCodeDataE, .-_GLOBAL__sub_I__ZN2v88internal18ObjectDeserializerC2EPKNS0_18SerializedCodeDataE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal18ObjectDeserializerC2EPKNS0_18SerializedCodeDataE
	.weak	_ZTVN2v88internal18ObjectDeserializerE
	.section	.data.rel.ro._ZTVN2v88internal18ObjectDeserializerE,"awG",@progbits,_ZTVN2v88internal18ObjectDeserializerE,comdat
	.align 8
	.type	_ZTVN2v88internal18ObjectDeserializerE, @object
	.size	_ZTVN2v88internal18ObjectDeserializerE, 56
_ZTVN2v88internal18ObjectDeserializerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal18ObjectDeserializerD1Ev
	.quad	_ZN2v88internal18ObjectDeserializerD0Ev
	.quad	_ZN2v88internal12Deserializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.quad	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.quad	_ZN2v88internal12Deserializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
