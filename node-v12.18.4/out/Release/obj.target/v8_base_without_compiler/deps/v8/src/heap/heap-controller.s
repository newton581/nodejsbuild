	.file	"heap-controller.cc"
	.text
	.section	.text._ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE33MinimumAllocationLimitGrowingStepENS0_4Heap15HeapGrowingModeE,"axG",@progbits,_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE33MinimumAllocationLimitGrowingStepENS0_4Heap15HeapGrowingModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE33MinimumAllocationLimitGrowingStepENS0_4Heap15HeapGrowingModeE
	.type	_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE33MinimumAllocationLimitGrowingStepENS0_4Heap15HeapGrowingModeE, @function
_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE33MinimumAllocationLimitGrowingStepENS0_4Heap15HeapGrowingModeE:
.LFB20171:
	.cfi_startproc
	endbr64
	cmpl	$1, %edi
	movl	$8388608, %edx
	movl	$2097152, %eax
	cmovne	%rdx, %rax
	ret
	.cfi_endproc
.LFE20171:
	.size	_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE33MinimumAllocationLimitGrowingStepENS0_4Heap15HeapGrowingModeE, .-_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE33MinimumAllocationLimitGrowingStepENS0_4Heap15HeapGrowingModeE
	.section	.rodata._ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE24CalculateAllocationLimitEPNS0_4HeapEmmmmdNS4_15HeapGrowingModeE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"1.0 < factor"
.LC5:
	.string	"Check failed: %s."
.LC6:
	.string	"0 < current_size"
	.section	.rodata._ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE24CalculateAllocationLimitEPNS0_4HeapEmmmmdNS4_15HeapGrowingModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"[%s] Limit: old size: %zu KB, new limit: %zu KB (%.1f)\n"
	.section	.text._ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE24CalculateAllocationLimitEPNS0_4HeapEmmmmdNS4_15HeapGrowingModeE,"axG",@progbits,_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE24CalculateAllocationLimitEPNS0_4HeapEmmmmdNS4_15HeapGrowingModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE24CalculateAllocationLimitEPNS0_4HeapEmmmmdNS4_15HeapGrowingModeE
	.type	_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE24CalculateAllocationLimitEPNS0_4HeapEmmmmdNS4_15HeapGrowingModeE, @function
_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE24CalculateAllocationLimitEPNS0_4HeapEmmmmdNS4_15HeapGrowingModeE:
.LFB20173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	%r9d, %edi
	subq	$24, %rsp
	movl	_ZN2v88internal25FLAG_heap_growing_percentE(%rip), %eax
	cmpl	$1, %r9d
	jle	.L24
	cmpl	$2, %r9d
	jne	.L8
	movsd	.LC1(%rip), %xmm0
	testl	%eax, %eax
	jle	.L13
.L20:
	movsd	.LC3(%rip), %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	.LC2(%rip), %xmm0
	addsd	%xmm1, %xmm0
.L12:
	comisd	%xmm1, %xmm0
	jbe	.L25
.L13:
	testq	%r13, %r13
	je	.L26
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE33MinimumAllocationLimitGrowingStepENS0_4Heap15HeapGrowingModeE
	testq	%r13, %r13
	movsd	-56(%rbp), %xmm0
	js	.L15
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%r13, %xmm1
.L16:
	mulsd	%xmm0, %xmm1
	movsd	.LC7(%rip), %xmm2
	comisd	%xmm2, %xmm1
	jnb	.L17
	cvttsd2siq	%xmm1, %r9
.L18:
	addq	%r13, %rax
	movq	%r14, %rdx
	cmpq	%rax, %r9
	cmovb	%rax, %r9
	leaq	(%r9,%r15), %r8
	cmpq	%r14, %r8
	cmovnb	%r8, %rdx
	addq	%r13, %r12
	shrq	%r12
	cmpq	%r12, %rdx
	cmovbe	%rdx, %r12
	cmpb	$0, _ZN2v88internal21FLAG_trace_gc_verboseE(%rip)
	jne	.L27
.L5:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	testl	%r9d, %r9d
	jns	.L28
.L8:
	movsd	.LC3(%rip), %xmm1
	testl	%eax, %eax
	jg	.L20
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%r13, %rdi
	movq	%r13, %rdx
	pxor	%xmm1, %xmm1
	shrq	%rdi
	andl	$1, %edx
	orq	%rdx, %rdi
	cvtsi2sdq	%rdi, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L17:
	subsd	%xmm2, %xmm1
	cvttsd2siq	%xmm1, %r9
	btcq	$63, %r9
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L28:
	movsd	.LC0(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L8
	movapd	%xmm1, %xmm0
	testl	%eax, %eax
	jg	.L20
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	_ZN2v88internal11V8HeapTrait5kNameE(%rip), %rdx
	leaq	-37592(%rbx), %rdi
	shrq	$10, %rcx
	shrq	$10, %r8
	movl	$1, %eax
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L25:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20173:
	.size	_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE24CalculateAllocationLimitEPNS0_4HeapEmmmmdNS4_15HeapGrowingModeE, .-_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE24CalculateAllocationLimitEPNS0_4HeapEmmmmdNS4_15HeapGrowingModeE
	.section	.text._ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE16MaxGrowingFactorEm,"axG",@progbits,_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE16MaxGrowingFactorEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE16MaxGrowingFactorEm
	.type	_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE16MaxGrowingFactorEm, @function
_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE16MaxGrowingFactorEm:
.LFB20175:
	.cfi_startproc
	endbr64
	movsd	.LC9(%rip), %xmm0
	cmpq	$2147483647, %rdi
	ja	.L29
	cmpq	$268435456, %rdi
	movl	$268435456, %eax
	pxor	%xmm0, %xmm0
	cmovb	%rax, %rdi
	subq	$268435456, %rdi
	cvtsi2sdq	%rdi, %xmm0
	mulsd	.LC10(%rip), %xmm0
	divsd	.LC11(%rip), %xmm0
	addsd	.LC0(%rip), %xmm0
.L29:
	ret
	.cfi_endproc
.LFE20175:
	.size	_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE16MaxGrowingFactorEm, .-_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE16MaxGrowingFactorEm
	.section	.text._ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE20DynamicGrowingFactorEddd,"axG",@progbits,_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE20DynamicGrowingFactorEddd,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE20DynamicGrowingFactorEddd
	.type	_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE20DynamicGrowingFactorEddd, @function
_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE20DynamicGrowingFactorEddd:
.LFB20176:
	.cfi_startproc
	endbr64
	pxor	%xmm3, %xmm3
	movl	$0, %edx
	ucomisd	%xmm3, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L37
	ucomisd	%xmm3, %xmm1
	setnp	%dl
	cmove	%edx, %eax
	testb	%al, %al
	jne	.L37
	divsd	%xmm1, %xmm0
	movapd	%xmm2, %xmm3
	mulsd	.LC13(%rip), %xmm0
	movapd	%xmm0, %xmm1
	subsd	.LC14(%rip), %xmm1
	mulsd	%xmm1, %xmm3
	comisd	%xmm0, %xmm3
	jbe	.L34
	divsd	%xmm1, %xmm0
	minsd	%xmm2, %xmm0
	movapd	%xmm0, %xmm2
.L34:
	movsd	.LC1(%rip), %xmm0
	maxsd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movapd	%xmm2, %xmm0
	ret
	.cfi_endproc
.LFE20176:
	.size	_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE20DynamicGrowingFactorEddd, .-_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE20DynamicGrowingFactorEddd
	.section	.rodata._ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE13GrowingFactorEPNS0_4HeapEmdd.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"[%s] factor %.1f based on mu=%.3f, speed_ratio=%.f (gc=%.f, mutator=%.f)\n"
	.section	.text._ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE13GrowingFactorEPNS0_4HeapEmdd,"axG",@progbits,_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE13GrowingFactorEPNS0_4HeapEmdd,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE13GrowingFactorEPNS0_4HeapEmdd
	.type	_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE13GrowingFactorEPNS0_4HeapEmdd, @function
_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE13GrowingFactorEPNS0_4HeapEmdd:
.LFB20172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$24, %rsp
	movsd	%xmm1, -32(%rbp)
	movsd	%xmm0, -24(%rbp)
	call	_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE16MaxGrowingFactorEm
	movsd	-32(%rbp), %xmm1
	movapd	%xmm0, %xmm2
	movsd	-24(%rbp), %xmm0
	call	_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE20DynamicGrowingFactorEddd
	cmpb	$0, _ZN2v88internal21FLAG_trace_gc_verboseE(%rip)
	jne	.L44
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movsd	-24(%rbp), %xmm3
	movsd	-32(%rbp), %xmm4
	leaq	-37592(%rbx), %rdi
	leaq	.LC15(%rip), %rsi
	movsd	.LC14(%rip), %xmm1
	movl	$5, %eax
	movsd	%xmm0, -24(%rbp)
	movq	_ZN2v88internal11V8HeapTrait5kNameE(%rip), %rdx
	movapd	%xmm3, %xmm2
	divsd	%xmm4, %xmm2
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movsd	-24(%rbp), %xmm0
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20172:
	.size	_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE13GrowingFactorEPNS0_4HeapEmdd, .-_ZN2v88internal16MemoryControllerINS0_11V8HeapTraitEE13GrowingFactorEPNS0_4HeapEmdd
	.section	.text._ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE33MinimumAllocationLimitGrowingStepENS0_4Heap15HeapGrowingModeE,"axG",@progbits,_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE33MinimumAllocationLimitGrowingStepENS0_4Heap15HeapGrowingModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE33MinimumAllocationLimitGrowingStepENS0_4Heap15HeapGrowingModeE
	.type	_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE33MinimumAllocationLimitGrowingStepENS0_4Heap15HeapGrowingModeE, @function
_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE33MinimumAllocationLimitGrowingStepENS0_4Heap15HeapGrowingModeE:
.LFB20177:
	.cfi_startproc
	endbr64
	cmpl	$1, %edi
	movl	$8388608, %edx
	movl	$2097152, %eax
	cmovne	%rdx, %rax
	ret
	.cfi_endproc
.LFE20177:
	.size	_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE33MinimumAllocationLimitGrowingStepENS0_4Heap15HeapGrowingModeE, .-_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE33MinimumAllocationLimitGrowingStepENS0_4Heap15HeapGrowingModeE
	.section	.text._ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE24CalculateAllocationLimitEPNS0_4HeapEmmmmdNS4_15HeapGrowingModeE,"axG",@progbits,_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE24CalculateAllocationLimitEPNS0_4HeapEmmmmdNS4_15HeapGrowingModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE24CalculateAllocationLimitEPNS0_4HeapEmmmmdNS4_15HeapGrowingModeE
	.type	_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE24CalculateAllocationLimitEPNS0_4HeapEmmmmdNS4_15HeapGrowingModeE, @function
_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE24CalculateAllocationLimitEPNS0_4HeapEmmmmdNS4_15HeapGrowingModeE:
.LFB20179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	%r9d, %edi
	subq	$24, %rsp
	movl	_ZN2v88internal25FLAG_heap_growing_percentE(%rip), %eax
	cmpl	$1, %r9d
	jle	.L67
	cmpl	$2, %r9d
	jne	.L51
	movsd	.LC1(%rip), %xmm0
	testl	%eax, %eax
	jle	.L56
.L63:
	movsd	.LC3(%rip), %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	.LC2(%rip), %xmm0
	addsd	%xmm1, %xmm0
.L55:
	comisd	%xmm1, %xmm0
	jbe	.L68
.L56:
	testq	%r13, %r13
	je	.L69
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE33MinimumAllocationLimitGrowingStepENS0_4Heap15HeapGrowingModeE
	testq	%r13, %r13
	movsd	-56(%rbp), %xmm0
	js	.L58
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%r13, %xmm1
.L59:
	mulsd	%xmm0, %xmm1
	movsd	.LC7(%rip), %xmm2
	comisd	%xmm2, %xmm1
	jnb	.L60
	cvttsd2siq	%xmm1, %r9
.L61:
	addq	%r13, %rax
	movq	%r14, %rdx
	cmpq	%rax, %r9
	cmovb	%rax, %r9
	leaq	(%r9,%r15), %r8
	cmpq	%r14, %r8
	cmovnb	%r8, %rdx
	addq	%r13, %r12
	shrq	%r12
	cmpq	%r12, %rdx
	cmovbe	%rdx, %r12
	cmpb	$0, _ZN2v88internal21FLAG_trace_gc_verboseE(%rip)
	jne	.L70
.L48:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	testl	%r9d, %r9d
	jns	.L71
.L51:
	movsd	.LC3(%rip), %xmm1
	testl	%eax, %eax
	jg	.L63
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L69:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%r13, %rdi
	movq	%r13, %rdx
	pxor	%xmm1, %xmm1
	shrq	%rdi
	andl	$1, %edx
	orq	%rdx, %rdi
	cvtsi2sdq	%rdi, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L60:
	subsd	%xmm2, %xmm1
	cvttsd2siq	%xmm1, %r9
	btcq	$63, %r9
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L71:
	movsd	.LC0(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L51
	movapd	%xmm1, %xmm0
	testl	%eax, %eax
	jg	.L63
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	_ZN2v88internal17GlobalMemoryTrait5kNameE(%rip), %rdx
	leaq	-37592(%rbx), %rdi
	shrq	$10, %rcx
	shrq	$10, %r8
	movl	$1, %eax
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L68:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20179:
	.size	_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE24CalculateAllocationLimitEPNS0_4HeapEmmmmdNS4_15HeapGrowingModeE, .-_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE24CalculateAllocationLimitEPNS0_4HeapEmmmmdNS4_15HeapGrowingModeE
	.section	.text._ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE16MaxGrowingFactorEm,"axG",@progbits,_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE16MaxGrowingFactorEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE16MaxGrowingFactorEm
	.type	_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE16MaxGrowingFactorEm, @function
_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE16MaxGrowingFactorEm:
.LFB20180:
	.cfi_startproc
	endbr64
	movsd	.LC9(%rip), %xmm0
	cmpq	$2147483647, %rdi
	ja	.L72
	cmpq	$268435456, %rdi
	movl	$268435456, %eax
	pxor	%xmm0, %xmm0
	cmovb	%rax, %rdi
	subq	$268435456, %rdi
	cvtsi2sdq	%rdi, %xmm0
	mulsd	.LC10(%rip), %xmm0
	divsd	.LC11(%rip), %xmm0
	addsd	.LC0(%rip), %xmm0
.L72:
	ret
	.cfi_endproc
.LFE20180:
	.size	_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE16MaxGrowingFactorEm, .-_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE16MaxGrowingFactorEm
	.section	.text._ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE20DynamicGrowingFactorEddd,"axG",@progbits,_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE20DynamicGrowingFactorEddd,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE20DynamicGrowingFactorEddd
	.type	_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE20DynamicGrowingFactorEddd, @function
_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE20DynamicGrowingFactorEddd:
.LFB20181:
	.cfi_startproc
	endbr64
	pxor	%xmm3, %xmm3
	movl	$0, %edx
	ucomisd	%xmm3, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L80
	ucomisd	%xmm3, %xmm1
	setnp	%dl
	cmove	%edx, %eax
	testb	%al, %al
	jne	.L80
	divsd	%xmm1, %xmm0
	movapd	%xmm2, %xmm3
	mulsd	.LC13(%rip), %xmm0
	movapd	%xmm0, %xmm1
	subsd	.LC14(%rip), %xmm1
	mulsd	%xmm1, %xmm3
	comisd	%xmm0, %xmm3
	jbe	.L77
	divsd	%xmm1, %xmm0
	minsd	%xmm2, %xmm0
	movapd	%xmm0, %xmm2
.L77:
	movsd	.LC1(%rip), %xmm0
	maxsd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	movapd	%xmm2, %xmm0
	ret
	.cfi_endproc
.LFE20181:
	.size	_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE20DynamicGrowingFactorEddd, .-_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE20DynamicGrowingFactorEddd
	.section	.text._ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE13GrowingFactorEPNS0_4HeapEmdd,"axG",@progbits,_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE13GrowingFactorEPNS0_4HeapEmdd,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE13GrowingFactorEPNS0_4HeapEmdd
	.type	_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE13GrowingFactorEPNS0_4HeapEmdd, @function
_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE13GrowingFactorEPNS0_4HeapEmdd:
.LFB20178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$24, %rsp
	movsd	%xmm1, -32(%rbp)
	movsd	%xmm0, -24(%rbp)
	call	_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE16MaxGrowingFactorEm
	movsd	-32(%rbp), %xmm1
	movapd	%xmm0, %xmm2
	movsd	-24(%rbp), %xmm0
	call	_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE20DynamicGrowingFactorEddd
	cmpb	$0, _ZN2v88internal21FLAG_trace_gc_verboseE(%rip)
	jne	.L87
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movsd	-24(%rbp), %xmm3
	movsd	-32(%rbp), %xmm4
	leaq	-37592(%rbx), %rdi
	leaq	.LC15(%rip), %rsi
	movsd	.LC14(%rip), %xmm1
	movl	$5, %eax
	movsd	%xmm0, -24(%rbp)
	movq	_ZN2v88internal17GlobalMemoryTrait5kNameE(%rip), %rdx
	movapd	%xmm3, %xmm2
	divsd	%xmm4, %xmm2
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movsd	-24(%rbp), %xmm0
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20178:
	.size	_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE13GrowingFactorEPNS0_4HeapEmdd, .-_ZN2v88internal16MemoryControllerINS0_17GlobalMemoryTraitEE13GrowingFactorEPNS0_4HeapEmdd
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11V8HeapTrait5kNameE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11V8HeapTrait5kNameE, @function
_GLOBAL__sub_I__ZN2v88internal11V8HeapTrait5kNameE:
.LFB22314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22314:
	.size	_GLOBAL__sub_I__ZN2v88internal11V8HeapTrait5kNameE, .-_GLOBAL__sub_I__ZN2v88internal11V8HeapTrait5kNameE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11V8HeapTrait5kNameE
	.globl	_ZN2v88internal17GlobalMemoryTrait5kNameE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC16:
	.string	"GlobalMemoryController"
	.section	.data.rel.local._ZN2v88internal17GlobalMemoryTrait5kNameE,"aw"
	.align 8
	.type	_ZN2v88internal17GlobalMemoryTrait5kNameE, @object
	.size	_ZN2v88internal17GlobalMemoryTrait5kNameE, 8
_ZN2v88internal17GlobalMemoryTrait5kNameE:
	.quad	.LC16
	.globl	_ZN2v88internal11V8HeapTrait5kNameE
	.section	.rodata.str1.1
.LC17:
	.string	"HeapController"
	.section	.data.rel.local._ZN2v88internal11V8HeapTrait5kNameE,"aw"
	.align 8
	.type	_ZN2v88internal11V8HeapTrait5kNameE, @object
	.size	_ZN2v88internal11V8HeapTrait5kNameE, 8
_ZN2v88internal11V8HeapTrait5kNameE:
	.quad	.LC17
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	3435973837
	.long	1073007820
	.align 8
.LC1:
	.long	2576980378
	.long	1072798105
	.align 8
.LC2:
	.long	0
	.long	1079574528
	.align 8
.LC3:
	.long	0
	.long	1072693248
	.align 8
.LC7:
	.long	0
	.long	1138753536
	.align 8
.LC9:
	.long	0
	.long	1074790400
	.align 8
.LC10:
	.long	1717986918
	.long	1072064102
	.align 8
.LC11:
	.long	0
	.long	1104936960
	.align 8
.LC13:
	.long	3951369920
	.long	1067366481
	.align 8
.LC14:
	.long	1889785610
	.long	1072630333
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
