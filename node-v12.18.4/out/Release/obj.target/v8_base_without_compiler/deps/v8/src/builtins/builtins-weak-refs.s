	.file	"builtins-weak-refs.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.rodata._ZN2v88internalL25Builtin_Impl_WeakRefDerefENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"WeakRef.prototype.deref"
.LC1:
	.string	"(location_) != nullptr"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL25Builtin_Impl_WeakRefDerefENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Builtin_Impl_WeakRefDerefENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL25Builtin_Impl_WeakRefDerefENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18689:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L6
.L9:
	leaq	.LC0(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$23, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L22
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L12:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L17
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L17:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L23
	addq	$48, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1081, 11(%rdx)
	jne	.L9
	movq	23(%rax), %rsi
	movq	-1(%rsi), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L11
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L13
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L14:
	leaq	37592(%r12), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal4Heap13KeepDuringJobENS0_6HandleINS0_10JSReceiverEEE@PLT
	movq	0(%r13), %rax
.L11:
	movq	23(%rax), %r13
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%r14, %r8
	cmpq	41096(%r12), %r14
	je	.L24
.L15:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L14
.L24:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L15
.L23:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18689:
	.size	_ZN2v88internalL25Builtin_Impl_WeakRefDerefENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL25Builtin_Impl_WeakRefDerefENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Builtin_Impl_FinalizationGroupCleanupSomeENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"FinalizationGroup.prototype.cleanupSome"
	.section	.text._ZN2v88internalL41Builtin_Impl_FinalizationGroupCleanupSomeENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_FinalizationGroupCleanupSomeENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_FinalizationGroupCleanupSomeENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18680:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L26
.L29:
	leaq	.LC3(%rip), %rax
	leaq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$39, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L49
.L48:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L32:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L42
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L42:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1083, 11(%rdx)
	jne	.L29
	movq	%rdi, %r15
	movq	41112(%r12), %rdi
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L51
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L33:
	leaq	88(%r12), %rcx
	leaq	-8(%r13), %rax
	cmpl	$5, %r15d
	cmovle	%rcx, %rax
	movq	(%rax), %rcx
	cmpq	%rcx, 88(%r12)
	je	.L37
	testb	$1, %cl
	jne	.L52
.L38:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$370, %esi
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%r14, %rdx
	cmpq	41096(%r12), %r14
	je	.L53
.L34:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L33
.L53:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L52:
	movq	-1(%rcx), %rdx
	testb	$2, 13(%rdx)
	je	.L38
	movq	%rax, %rdx
.L37:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19JSFinalizationGroup7CleanupEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L40
	movq	312(%r12), %r13
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L40:
	movq	88(%r12), %r13
	jmp	.L32
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18680:
	.size	_ZN2v88internalL41Builtin_Impl_FinalizationGroupCleanupSomeENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_FinalizationGroupCleanupSomeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internalL41Builtin_Impl_FinalizationGroupConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_FinalizationGroupConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_FinalizationGroupConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18671:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-16(,%rdi,8), %eax
	addl	$1, 41104(%rdx)
	movslq	%eax, %rdx
	addl	$8, %eax
	cltq
	subq	%rdx, %r9
	subq	%rax, %r8
	movq	(%r8), %rax
	cmpq	%rax, 88(%r12)
	je	.L103
	subq	$8, %rsi
	leaq	88(%r12), %rbx
	cmpl	$5, %edi
	cmovg	%rsi, %rbx
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L68
.L69:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$370, %esi
.L102:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r15
.L65:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L81
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L81:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L104
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	-1(%rax), %rax
	testb	$2, 13(%rax)
	je	.L69
	xorl	%edx, %edx
	movq	%r8, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8JSObject3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L105
	movq	(%rax), %r9
	movq	12464(%r12), %rax
	movq	39(%rax), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L71
	movq	%r8, %rsi
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %r9
	movq	(%rax), %r8
.L72:
	movq	%r8, 23(%r9)
	leaq	23(%r9), %rsi
	testb	$1, %r8b
	je	.L83
	movq	%r8, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L106
.L75:
	testb	$24, %al
	je	.L83
	movq	%r9, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L83
	movq	%r8, %rdx
	movq	%r9, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L83:
	movq	(%r15), %rdi
	movq	(%rbx), %rbx
	movq	%rbx, 31(%rdi)
	leaq	31(%rdi), %rsi
	testb	$1, %bl
	je	.L82
	movq	%rbx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L107
.L78:
	testb	$24, %al
	je	.L82
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L82
	movq	%rbx, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L82:
	movq	(%r15), %rax
	movq	$0, 71(%rax)
	movq	(%r15), %r15
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L103:
	movq	(%r9), %rax
	movq	23(%rax), %rbx
	movq	15(%rbx), %rax
	testb	$1, %al
	jne	.L108
.L56:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L57:
	testb	%al, %al
	je	.L61
	movq	15(%rbx), %r15
	testb	$1, %r15b
	jne	.L109
.L59:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L62
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L63:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$40, %esi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L108:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L56
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L62:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L110
.L64:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rdx)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L61:
	andq	$-262144, %rbx
	movq	24(%rbx), %rax
	movq	-37464(%rax), %r15
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L71:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L111
.L73:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%r8, (%rax)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%rbx, %rdx
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	8(%rcx), %rax
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%r8, %rdx
	movq	%r9, %rdi
	movq	%r8, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %r9
	movq	8(%rcx), %rax
	jmp	.L75
.L111:
	movq	%r12, %rdi
	movq	%r8, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %r9
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L109:
	movq	-1(%r15), %rax
	cmpw	$136, 11(%rax)
	jne	.L59
	leaq	-64(%rbp), %rdi
	movq	%r15, -64(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	testb	%al, %al
	je	.L61
	movq	-72(%rbp), %rdi
	movq	%r15, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	%rax, %r15
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L105:
	movq	312(%r12), %r15
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L64
.L104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18671:
	.size	_ZN2v88internalL41Builtin_Impl_FinalizationGroupConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_FinalizationGroupConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internalL31Builtin_Impl_WeakRefConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Builtin_Impl_WeakRefConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL31Builtin_Impl_WeakRefConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18686:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-16(,%rdi,8), %eax
	addl	$1, 41104(%rdx)
	movslq	%eax, %rdx
	addl	$8, %eax
	cltq
	subq	%rdx, %r14
	subq	%rax, %r15
	movq	(%r15), %rax
	cmpq	%rax, 88(%r12)
	je	.L148
	subq	$8, %rsi
	leaq	88(%r12), %rax
	cmpl	$5, %edi
	cmovle	%rax, %rsi
	movq	(%rsi), %rsi
	testb	$1, %sil
	jne	.L126
.L127:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$373, %esi
.L147:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
.L123:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L136
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L136:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L149
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L127
	movq	41112(%r12), %rdi
	leaq	37592(%r12), %r8
	testq	%rdi, %rdi
	je	.L128
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rcx
.L129:
	movq	%rcx, %rsi
	movq	%r8, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal4Heap13KeepDuringJobENS0_6HandleINS0_10JSReceiverEEE@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8JSObject3NewENS0_6HandleINS0_10JSFunctionEEENS2_INS0_10JSReceiverEEENS2_INS0_14AllocationSiteEEE@PLT
	movq	-72(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L150
	movq	(%rax), %rdi
	movq	(%rcx), %r15
	movq	%r15, 23(%rdi)
	leaq	23(%rdi), %rsi
	testb	$1, %r15b
	je	.L137
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L151
.L133:
	testb	$24, %al
	je	.L137
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L137
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L137:
	movq	(%r14), %r14
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L148:
	movq	(%r14), %rax
	movq	23(%rax), %r14
	movq	15(%r14), %rax
	testb	$1, %al
	jne	.L152
.L114:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L115:
	testb	%al, %al
	je	.L119
	movq	15(%r14), %r15
	testb	$1, %r15b
	jne	.L153
.L117:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L120
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L121:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$40, %esi
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L152:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L114
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L120:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L154
.L122:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rdx)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L119:
	andq	$-262144, %r14
	movq	24(%r14), %rax
	movq	-37464(%rax), %r15
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%r13, %rcx
	cmpq	41096(%r12), %r13
	je	.L155
.L130:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L151:
	movq	%r15, %rdx
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	8(%rcx), %rax
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L153:
	movq	-1(%r15), %rax
	cmpw	$136, 11(%rax)
	jne	.L117
	leaq	-64(%rbp), %rdi
	movq	%r15, -64(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	testb	%al, %al
	je	.L119
	movq	-72(%rbp), %rdi
	movq	%r15, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	%rax, %r15
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L150:
	movq	312(%r12), %r14
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L154:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%r12, %rdi
	movq	%r8, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L130
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18686:
	.size	_ZN2v88internalL31Builtin_Impl_WeakRefConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL31Builtin_Impl_WeakRefConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L162
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L165
.L156:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L156
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC5:
	.string	"V8.Builtin_FinalizationGroupCleanupSome"
	.section	.text._ZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateE, @function
_ZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateE:
.LFB18678:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L195
.L167:
	movq	_ZZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateEE28trace_event_unique_atomic124(%rip), %rbx
	testq	%rbx, %rbx
	je	.L196
.L169:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L197
.L171:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL41Builtin_Impl_FinalizationGroupCleanupSomeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L198
.L175:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L199
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L200
.L170:
	movq	%rbx, _ZZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateEE28trace_event_unique_atomic124(%rip)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L197:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L201
.L172:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L173
	movq	(%rdi), %rax
	call	*8(%rax)
.L173:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L174
	movq	(%rdi), %rax
	call	*8(%rax)
.L174:
	leaq	.LC5(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L198:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L195:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$854, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L201:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L200:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L170
.L199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18678:
	.size	_ZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateE, .-_ZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"V8.Builtin_FinalizationGroupConstructor"
	.section	.text._ZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupConstructorEiPmPNS0_7IsolateE:
.LFB18669:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L231
.L203:
	movq	_ZZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic12(%rip), %rbx
	testq	%rbx, %rbx
	je	.L232
.L205:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L233
.L207:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL41Builtin_Impl_FinalizationGroupConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L234
.L211:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L235
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L236
.L206:
	movq	%rbx, _ZZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic12(%rip)
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L233:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L237
.L208:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L209
	movq	(%rdi), %rax
	call	*8(%rax)
.L209:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L210
	movq	(%rdi), %rax
	call	*8(%rax)
.L210:
	leaq	.LC6(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L234:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L231:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$855, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L237:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L236:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L206
.L235:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18669:
	.size	_ZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupConstructorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Builtin_Impl_Stats_WeakRefDerefEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"V8.Builtin_WeakRefDeref"
	.section	.text._ZN2v88internalL31Builtin_Impl_Stats_WeakRefDerefEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Builtin_Impl_Stats_WeakRefDerefEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Builtin_Impl_Stats_WeakRefDerefEiPmPNS0_7IsolateE:
.LFB18687:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L267
.L239:
	movq	_ZZN2v88internalL31Builtin_Impl_Stats_WeakRefDerefEiPmPNS0_7IsolateEE28trace_event_unique_atomic212(%rip), %rbx
	testq	%rbx, %rbx
	je	.L268
.L241:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L269
.L243:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL25Builtin_Impl_WeakRefDerefENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L270
.L247:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L271
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L272
.L242:
	movq	%rbx, _ZZN2v88internalL31Builtin_Impl_Stats_WeakRefDerefEiPmPNS0_7IsolateEE28trace_event_unique_atomic212(%rip)
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L269:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L273
.L244:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L245
	movq	(%rdi), %rax
	call	*8(%rax)
.L245:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L246
	movq	(%rdi), %rax
	call	*8(%rax)
.L246:
	leaq	.LC7(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L270:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L267:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$859, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L273:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L272:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L242
.L271:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18687:
	.size	_ZN2v88internalL31Builtin_Impl_Stats_WeakRefDerefEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Builtin_Impl_Stats_WeakRefDerefEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Builtin_Impl_Stats_WeakRefConstructorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"V8.Builtin_WeakRefConstructor"
	.section	.text._ZN2v88internalL37Builtin_Impl_Stats_WeakRefConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Builtin_Impl_Stats_WeakRefConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL37Builtin_Impl_Stats_WeakRefConstructorEiPmPNS0_7IsolateE:
.LFB18684:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L303
.L275:
	movq	_ZZN2v88internalL37Builtin_Impl_Stats_WeakRefConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic179(%rip), %rbx
	testq	%rbx, %rbx
	je	.L304
.L277:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L305
.L279:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL31Builtin_Impl_WeakRefConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L306
.L283:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L307
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L308
.L278:
	movq	%rbx, _ZZN2v88internalL37Builtin_Impl_Stats_WeakRefConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic179(%rip)
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L305:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L309
.L280:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L281
	movq	(%rdi), %rax
	call	*8(%rax)
.L281:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L282
	movq	(%rdi), %rax
	call	*8(%rax)
.L282:
	leaq	.LC8(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L306:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L303:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$858, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L309:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L308:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L278
.L307:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18684:
	.size	_ZN2v88internalL37Builtin_Impl_Stats_WeakRefConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL37Builtin_Impl_Stats_WeakRefConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19JSFinalizationGroup8RegisterENS0_6HandleIS1_EENS2_INS0_10JSReceiverEEENS2_INS0_6ObjectEEES7_PNS0_7IsolateE,"axG",@progbits,_ZN2v88internal19JSFinalizationGroup8RegisterENS0_6HandleIS1_EENS2_INS0_10JSReceiverEEENS2_INS0_6ObjectEEES7_PNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19JSFinalizationGroup8RegisterENS0_6HandleIS1_EENS2_INS0_10JSReceiverEEENS2_INS0_6ObjectEEES7_PNS0_7IsolateE
	.type	_ZN2v88internal19JSFinalizationGroup8RegisterENS0_6HandleIS1_EENS2_INS0_10JSReceiverEEENS2_INS0_6ObjectEEES7_PNS0_7IsolateE, @function
_ZN2v88internal19JSFinalizationGroup8RegisterENS0_6HandleIS1_EENS2_INS0_10JSReceiverEEENS2_INS0_6ObjectEEES7_PNS0_7IsolateE:
.LFB18661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r8, %rdi
	subq	$72, %rsp
	movq	%rcx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Factory11NewWeakCellEv@PLT
	movq	(%rbx), %rdx
	movq	(%rax), %rdi
	movq	%rax, %r13
	movq	%rdx, 7(%rdi)
	testb	$1, %dl
	je	.L374
	movq	%rdx, %rcx
	leaq	7(%rdi), %rsi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	jne	.L461
	testb	$24, %al
	je	.L374
.L486:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L462
	.p2align 4,,10
	.p2align 3
.L374:
	movq	0(%r13), %rdi
	movq	(%r15), %r15
	movq	%r15, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %r15b
	je	.L373
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	jne	.L463
	testb	$24, %al
	je	.L373
.L488:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L464
	.p2align 4,,10
	.p2align 3
.L373:
	movq	0(%r13), %r15
	movq	(%r14), %r14
	movq	%r14, 23(%r15)
	leaq	23(%r15), %rsi
	testb	$1, %r14b
	je	.L372
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	jne	.L465
	testb	$24, %al
	je	.L372
.L487:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L466
	.p2align 4,,10
	.p2align 3
.L372:
	movq	0(%r13), %r15
	movq	88(%r12), %r14
	movq	%r14, 31(%r15)
	leaq	31(%r15), %rsi
	testb	$1, %r14b
	je	.L371
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	jne	.L467
	testb	$24, %al
	je	.L371
.L492:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L468
	.p2align 4,,10
	.p2align 3
.L371:
	movq	0(%r13), %r15
	movq	88(%r12), %r14
	movq	%r14, 39(%r15)
	leaq	39(%r15), %rsi
	testb	$1, %r14b
	je	.L370
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	jne	.L469
	testb	$24, %al
	je	.L370
.L491:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L470
	.p2align 4,,10
	.p2align 3
.L370:
	movq	-72(%rbp), %rax
	movq	0(%r13), %r15
	movq	(%rax), %r14
	leaq	47(%r15), %rsi
	movq	%r14, 47(%r15)
	testb	$1, %r14b
	je	.L369
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	jne	.L471
	testb	$24, %al
	je	.L369
.L490:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L472
	.p2align 4,,10
	.p2align 3
.L369:
	movq	0(%r13), %r15
	movq	88(%r12), %r14
	movq	%r14, 55(%r15)
	leaq	55(%r15), %rsi
	testb	$1, %r14b
	je	.L368
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	jne	.L473
	testb	$24, %al
	je	.L368
.L489:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L474
	.p2align 4,,10
	.p2align 3
.L368:
	movq	0(%r13), %r15
	movq	88(%r12), %r14
	movq	%r14, 63(%r15)
	leaq	63(%r15), %rsi
	testb	$1, %r14b
	je	.L367
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	jne	.L475
	testb	$24, %al
	je	.L367
.L495:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L476
	.p2align 4,,10
	.p2align 3
.L367:
	movq	(%rbx), %rax
	movq	0(%r13), %r15
	movq	39(%rax), %r14
	leaq	39(%r15), %rsi
	movq	%r14, 39(%r15)
	testb	$1, %r14b
	je	.L366
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	jne	.L477
	testb	$24, %al
	je	.L366
.L494:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L478
	.p2align 4,,10
	.p2align 3
.L366:
	movq	(%rbx), %r15
	movq	39(%r15), %rdi
	leaq	39(%r15), %rsi
	testb	$1, %dil
	jne	.L479
.L338:
	movq	0(%r13), %r14
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L364
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	jne	.L480
	testb	$24, %al
	je	.L364
.L493:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L481
	.p2align 4,,10
	.p2align 3
.L364:
	movq	-72(%rbp), %rdx
	movq	88(%r12), %rax
	cmpq	(%rdx), %rax
	je	.L310
	movq	(%rbx), %rdx
	movq	55(%rdx), %rsi
	cmpq	%rsi, %rax
	je	.L482
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L349
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L348:
	movq	%rsi, -64(%rbp)
	movq	-72(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE6LookupENS0_6HandleINS0_6ObjectEEE@PLT
	movq	%rax, %r12
	testb	$1, %al
	jne	.L483
.L352:
	movq	-72(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE3PutENS0_6HandleIS2_EENS5_INS0_6ObjectEEES8_@PLT
	movq	(%rbx), %r13
	movq	(%rax), %r12
	leaq	55(%r13), %r14
	movq	%r12, 55(%r13)
	testb	$1, %r12b
	je	.L310
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L360
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L360:
	testb	$24, %al
	je	.L310
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L484
	.p2align 4,,10
	.p2align 3
.L310:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L485
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L486
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L465:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L487
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L463:
	movq	%r15, %rdx
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L488
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L473:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L489
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L471:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L490
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L469:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L491
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L467:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L492
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L480:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L493
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L477:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L494
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L475:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L495
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L349:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L496
.L350:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L479:
	movq	-1(%rdi), %rax
	cmpw	$168, 11(%rax)
	jne	.L338
	movq	0(%r13), %r15
	movq	%rdi, %r14
	leaq	31(%rdi), %rsi
	movq	%r15, 31(%rdi)
	testb	$1, %r15b
	je	.L365
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	jne	.L497
.L341:
	testb	$24, %al
	je	.L365
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L365
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L365:
	movq	(%rbx), %r15
	leaq	39(%r15), %rsi
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L483:
	movq	-1(%rax), %rax
	cmpw	$168, 11(%rax)
	jne	.L352
	movq	0(%r13), %r15
	movq	%r12, %rcx
	leaq	55(%r12), %rsi
	andq	$-262144, %rcx
	movq	%r15, 55(%r12)
	testb	$1, %r15b
	je	.L363
	movq	%r15, %r9
	andq	$-262144, %r9
	movq	8(%r9), %rax
	movq	%r9, -80(%rbp)
	testl	$262144, %eax
	je	.L355
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r9
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	8(%r9), %rax
.L355:
	testb	$24, %al
	je	.L363
	testb	$24, 8(%rcx)
	jne	.L363
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L363:
	movq	0(%r13), %r15
	movq	%r12, 63(%r15)
	leaq	63(%r15), %rsi
	movq	%rcx, -80(%rbp)
	movq	8(%rcx), %rax
	testl	$262144, %eax
	jne	.L498
.L357:
	testb	$24, %al
	je	.L352
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L352
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L462:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L464:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L466:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L468:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L470:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L472:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L474:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L476:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L478:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L481:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L484:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L482:
	movl	$1, %esi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9HashTableINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L497:
	movq	%r15, %rdx
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L498:
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L496:
	movq	%r12, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L350
.L485:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18661:
	.size	_ZN2v88internal19JSFinalizationGroup8RegisterENS0_6HandleIS1_EENS2_INS0_10JSReceiverEEENS2_INS0_6ObjectEEES7_PNS0_7IsolateE, .-_ZN2v88internal19JSFinalizationGroup8RegisterENS0_6HandleIS1_EENS2_INS0_10JSReceiverEEENS2_INS0_6ObjectEEES7_PNS0_7IsolateE
	.section	.rodata._ZN2v88internalL38Builtin_Impl_FinalizationGroupRegisterENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"FinalizationGroup.prototype.register"
	.section	.text._ZN2v88internalL38Builtin_Impl_FinalizationGroupRegisterENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Builtin_Impl_FinalizationGroupRegisterENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL38Builtin_Impl_FinalizationGroupRegisterENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18674:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L500
.L503:
	leaq	.LC9(%rip), %rax
	leaq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$36, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L524
.L523:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L506:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L518
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L518:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L525
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L500:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1083, 11(%rax)
	jne	.L503
	leaq	-8(%rsi), %r9
	leaq	88(%rdx), %rdx
	cmpl	$6, %edi
	movq	%rdi, %r15
	cmovl	%rdx, %r9
	movq	(%r9), %rax
	testb	$1, %al
	jne	.L526
.L508:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$371, %esi
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L524:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L526:
	movq	-1(%rax), %rcx
	cmpw	$1023, 11(%rcx)
	jbe	.L508
	cmpl	$6, %edi
	jg	.L510
	movq	88(%r12), %rsi
	leaq	-80(%rbp), %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal6Object9SameValueES1_@PLT
	testb	%al, %al
	jne	.L511
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %r9
	movq	%rdx, %r10
.L514:
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L527
.L515:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$369, %esi
	jmp	.L523
.L510:
	leaq	-16(%rsi), %r10
	movq	-16(%rsi), %rsi
	leaq	-80(%rbp), %rdi
	movq	%rdx, -104(%rbp)
	movq	%r9, -96(%rbp)
	movq	%r10, -88(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal6Object9SameValueES1_@PLT
	testb	%al, %al
	jne	.L511
	cmpl	$7, %r15d
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %rdx
	je	.L514
	leaq	-24(%r13), %rdx
	jmp	.L514
.L511:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$372, %esi
	jmp	.L523
.L527:
	movq	-1(%rax), %rcx
	cmpw	$1023, 11(%rcx)
	jbe	.L516
.L517:
	movq	%rdx, %rcx
	movq	%r13, %rdi
	movq	%r12, %r8
	movq	%r10, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal19JSFinalizationGroup8RegisterENS0_6HandleIS1_EENS2_INS0_10JSReceiverEEENS2_INS0_6ObjectEEES7_PNS0_7IsolateE
	movq	88(%r12), %r13
	jmp	.L506
.L516:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	%rax, -37504(%rcx)
	je	.L517
	jmp	.L515
.L525:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18674:
	.size	_ZN2v88internalL38Builtin_Impl_FinalizationGroupRegisterENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL38Builtin_Impl_FinalizationGroupRegisterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL44Builtin_Impl_Stats_FinalizationGroupRegisterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"V8.Builtin_FinalizationGroupRegister"
	.section	.text._ZN2v88internalL44Builtin_Impl_Stats_FinalizationGroupRegisterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Builtin_Impl_Stats_FinalizationGroupRegisterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL44Builtin_Impl_Stats_FinalizationGroupRegisterEiPmPNS0_7IsolateE:
.LFB18672:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L557
.L529:
	movq	_ZZN2v88internalL44Builtin_Impl_Stats_FinalizationGroupRegisterEiPmPNS0_7IsolateEE27trace_event_unique_atomic47(%rip), %rbx
	testq	%rbx, %rbx
	je	.L558
.L531:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L559
.L533:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL38Builtin_Impl_FinalizationGroupRegisterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L560
.L537:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L561
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L558:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L562
.L532:
	movq	%rbx, _ZZN2v88internalL44Builtin_Impl_Stats_FinalizationGroupRegisterEiPmPNS0_7IsolateEE27trace_event_unique_atomic47(%rip)
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L559:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L563
.L534:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L535
	movq	(%rdi), %rax
	call	*8(%rax)
.L535:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L536
	movq	(%rdi), %rax
	call	*8(%rax)
.L536:
	leaq	.LC10(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L560:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L557:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$856, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L563:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L562:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L532
.L561:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18672:
	.size	_ZN2v88internalL44Builtin_Impl_Stats_FinalizationGroupRegisterEiPmPNS0_7IsolateE, .-_ZN2v88internalL44Builtin_Impl_Stats_FinalizationGroupRegisterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19JSFinalizationGroup22PopClearedCellHoldingsENS0_6HandleIS1_EEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal19JSFinalizationGroup22PopClearedCellHoldingsENS0_6HandleIS1_EEPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19JSFinalizationGroup22PopClearedCellHoldingsENS0_6HandleIS1_EEPNS0_7IsolateE
	.type	_ZN2v88internal19JSFinalizationGroup22PopClearedCellHoldingsENS0_6HandleIS1_EEPNS0_7IsolateE, @function
_ZN2v88internal19JSFinalizationGroup22PopClearedCellHoldingsENS0_6HandleIS1_EEPNS0_7IsolateE:
.LFB18666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	41112(%r12), %rdi
	movq	47(%rax), %rsi
	testq	%rdi, %rdi
	je	.L565
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L566:
	movq	0(%r13), %r15
	movq	39(%rsi), %r14
	movq	%r14, 47(%r15)
	leaq	47(%r15), %rsi
	testb	$1, %r14b
	je	.L630
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L694
	testb	$24, %al
	je	.L630
.L706:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L695
	.p2align 4,,10
	.p2align 3
.L630:
	movq	(%rbx), %r15
	movq	88(%r12), %r14
	movq	%r14, 39(%r15)
	leaq	39(%r15), %rsi
	testb	$1, %r14b
	je	.L629
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L696
	testb	$24, %al
	je	.L629
.L705:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L697
	.p2align 4,,10
	.p2align 3
.L629:
	movq	0(%r13), %rax
	movq	47(%rax), %r14
	testb	$1, %r14b
	jne	.L698
.L575:
	movq	(%rbx), %rax
	movq	88(%r12), %rdx
	cmpq	47(%rax), %rdx
	je	.L580
	movq	55(%rax), %r14
	cmpq	%r14, %rdx
	je	.L699
.L581:
	testb	$1, %r14b
	jne	.L700
.L583:
	movq	63(%rax), %r12
	leaq	63(%r14), %r15
	movq	%r12, 63(%r14)
	testb	$1, %r12b
	je	.L627
	movq	%r12, %r13
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testl	$262144, %eax
	jne	.L701
.L613:
	testb	$24, %al
	je	.L627
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L702
	.p2align 4,,10
	.p2align 3
.L627:
	movq	(%rbx), %rax
	movq	63(%rax), %r12
	testb	$1, %r12b
	jne	.L615
.L619:
	movq	55(%rax), %r13
	leaq	55(%r12), %r15
	movq	%r13, 55(%r12)
	testb	$1, %r13b
	je	.L626
	movq	%r13, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L703
	testb	$24, %al
	je	.L626
.L711:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L626
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L626:
	movq	(%rbx), %rax
.L580:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	movq	23(%rax), %rax
	jne	.L704
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L696:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L705
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L694:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L706
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L699:
	cmpq	63(%rax), %rdx
	jne	.L581
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L585
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L586:
	movq	(%rbx), %rax
	movq	41112(%r12), %rdi
	movq	47(%rax), %rsi
	testq	%rdi, %rdi
	je	.L588
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L589:
	leaq	-57(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE6RemoveEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_6ObjectEEEPb@PLT
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L565:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L707
.L567:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L698:
	movq	-1(%r14), %rax
	cmpw	$168, 11(%rax)
	jne	.L575
	movq	88(%r12), %r15
	leaq	31(%r14), %rsi
	movq	%r15, 31(%r14)
	testb	$1, %r15b
	je	.L575
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L708
.L578:
	testb	$24, %al
	je	.L575
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L575
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L700:
	movq	%r14, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%r14, -37504(%rdx)
	jne	.L583
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L594
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L595:
	movq	(%rbx), %rax
	movq	41112(%r12), %rdi
	movq	47(%rax), %rsi
	testq	%rdi, %rdi
	je	.L597
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r9
.L598:
	movq	(%rbx), %rax
	movq	41112(%r12), %rdi
	movq	63(%rax), %r14
	testq	%rdi, %rdi
	je	.L600
	movq	%r14, %rsi
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %r9
	movq	(%rax), %r14
	movq	%rax, %r8
.L601:
	movq	88(%r12), %rdx
	leaq	55(%r14), %rsi
	movq	%rdx, 55(%r14)
	testb	$1, %dl
	je	.L625
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L709
.L604:
	testb	$24, %al
	je	.L625
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L625
	movq	%r14, %rdi
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L625:
	movq	(%rbx), %r14
	movq	88(%r12), %r12
	movq	%r12, 63(%r14)
	leaq	63(%r14), %rsi
	testb	$1, %r12b
	je	.L624
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L710
.L607:
	testb	$24, %al
	je	.L624
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L624
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L624:
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE3PutENS0_6HandleIS2_EENS5_INS0_6ObjectEEES8_@PLT
.L693:
	movq	0(%r13), %r13
	movq	(%rax), %r12
	movq	%r12, 55(%r13)
	leaq	55(%r13), %r15
	testb	$1, %r12b
	je	.L626
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L610
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
.L610:
	testb	$24, %al
	je	.L626
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L626
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L695:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L697:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L701:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r13), %rax
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L703:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	testb	$24, %al
	jne	.L711
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L702:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L615:
	movq	%r12, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%r12, -37504(%rdx)
	je	.L580
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L707:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L708:
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L585:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L712
.L587:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L710:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r9, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L709:
	movq	%r14, %rdi
	movq	%r9, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	8(%rcx), %rax
	movq	-80(%rbp), %rsi
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L600:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L713
.L602:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%r8)
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L597:
	movq	41088(%r12), %r9
	cmpq	41096(%r12), %r9
	je	.L714
.L599:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r9)
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L594:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L715
.L596:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L588:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L716
.L590:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L589
.L713:
	movq	%r12, %rdi
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r9
	movq	%rax, %r8
	jmp	.L602
.L714:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L599
.L715:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L596
.L712:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L587
.L716:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L590
.L704:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18666:
	.size	_ZN2v88internal19JSFinalizationGroup22PopClearedCellHoldingsENS0_6HandleIS1_EEPNS0_7IsolateE, .-_ZN2v88internal19JSFinalizationGroup22PopClearedCellHoldingsENS0_6HandleIS1_EEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL49Builtin_Impl_FinalizationGroupCleanupIteratorNextENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"next"
	.section	.text._ZN2v88internalL49Builtin_Impl_FinalizationGroupCleanupIteratorNextENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL49Builtin_Impl_FinalizationGroupCleanupIteratorNextENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL49Builtin_Impl_FinalizationGroupCleanupIteratorNextENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18683:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L718
.L721:
	leaq	.LC11(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$4, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L746
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L724:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L738
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L738:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L747
	addq	$48, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L718:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1082, 11(%rdx)
	jne	.L721
	movq	41112(%r12), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L748
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdi
.L725:
	movq	47(%rsi), %rax
	testb	$1, %al
	jne	.L727
.L730:
	movq	41112(%r12), %rdi
	movq	88(%r12), %r13
	testq	%rdi, %rdi
	je	.L749
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L733:
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory19NewJSIteratorResultENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	(%rax), %r13
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L746:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L748:
	movq	%r14, %rdi
	cmpq	41096(%r12), %r14
	je	.L750
.L726:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L725
.L750:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L727:
	movq	-1(%rax), %rax
	cmpw	$168, 11(%rax)
	jne	.L730
	movq	%r12, %rsi
	call	_ZN2v88internal19JSFinalizationGroup22PopClearedCellHoldingsENS0_6HandleIS1_EEPNS0_7IsolateE
	movq	41112(%r12), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L751
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L735:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory19NewJSIteratorResultENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	(%rax), %r13
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L749:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L752
.L734:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L751:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L753
.L736:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L735
.L752:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L734
.L753:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L736
.L747:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18683:
	.size	_ZN2v88internalL49Builtin_Impl_FinalizationGroupCleanupIteratorNextENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL49Builtin_Impl_FinalizationGroupCleanupIteratorNextENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL55Builtin_Impl_Stats_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"V8.Builtin_FinalizationGroupCleanupIteratorNext"
	.section	.text._ZN2v88internalL55Builtin_Impl_Stats_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL55Builtin_Impl_Stats_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateE, @function
_ZN2v88internalL55Builtin_Impl_Stats_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateE:
.LFB18681:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L783
.L755:
	movq	_ZZN2v88internalL55Builtin_Impl_Stats_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateEE28trace_event_unique_atomic162(%rip), %rbx
	testq	%rbx, %rbx
	je	.L784
.L757:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L785
.L759:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL49Builtin_Impl_FinalizationGroupCleanupIteratorNextENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L786
.L763:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L787
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L784:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L788
.L758:
	movq	%rbx, _ZZN2v88internalL55Builtin_Impl_Stats_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateEE28trace_event_unique_atomic162(%rip)
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L785:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L789
.L760:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L761
	movq	(%rdi), %rax
	call	*8(%rax)
.L761:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L762
	movq	(%rdi), %rax
	call	*8(%rax)
.L762:
	leaq	.LC12(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L786:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L783:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$853, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L789:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC12(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L788:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L758
.L787:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18681:
	.size	_ZN2v88internalL55Builtin_Impl_Stats_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateE, .-_ZN2v88internalL55Builtin_Impl_Stats_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal8WeakCell32RemoveFromFinalizationGroupCellsEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal8WeakCell32RemoveFromFinalizationGroupCellsEPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8WeakCell32RemoveFromFinalizationGroupCellsEPNS0_7IsolateE
	.type	_ZN2v88internal8WeakCell32RemoveFromFinalizationGroupCellsEPNS0_7IsolateE, @function
_ZN2v88internal8WeakCell32RemoveFromFinalizationGroupCellsEPNS0_7IsolateE:
.LFB18668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	88(%rsi), %r13
	movq	%r13, 15(%rax)
	movq	(%rdi), %rdi
	testb	$1, %r13b
	je	.L823
	movq	%r13, %r14
	leaq	15(%rdi), %rsi
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L871
	testb	$24, %al
	je	.L823
.L882:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L872
	.p2align 4,,10
	.p2align 3
.L823:
	movq	39(%rdi), %r14
	movq	7(%rdi), %r13
	movq	%r14, %rax
	leaq	39(%r13), %r15
	notq	%rax
	andl	$1, %eax
	cmpq	%rdi, 39(%r13)
	je	.L873
	leaq	47(%r13), %r15
	cmpq	%rdi, 47(%r13)
	je	.L874
	movq	31(%rdi), %r13
	movq	%r14, 39(%r13)
	leaq	39(%r13), %rsi
	testb	%al, %al
	jne	.L798
	movq	%r14, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L875
.L804:
	testb	$24, %al
	je	.L798
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L876
	.p2align 4,,10
	.p2align 3
.L798:
	movq	(%rbx), %rax
	movq	39(%rax), %r13
	testb	$1, %r13b
	jne	.L877
.L806:
	movq	88(%r12), %r13
	movq	%r13, 31(%rax)
	movq	(%rbx), %rdi
	testb	$1, %r13b
	je	.L818
	movq	%r13, %r14
	leaq	31(%rdi), %rsi
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L878
	testb	$24, %al
	je	.L818
.L884:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L879
	.p2align 4,,10
	.p2align 3
.L818:
	movq	88(%r12), %r12
	movq	%r12, 39(%rdi)
	testb	$1, %r12b
	je	.L790
	movq	%r12, %r13
	movq	(%rbx), %rdi
	andq	$-262144, %r13
	movq	8(%r13), %rax
	leaq	39(%rdi), %rsi
	testl	$262144, %eax
	jne	.L880
	testb	$24, %al
	je	.L790
.L883:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L881
.L790:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L871:
	.cfi_restore_state
	movq	%r13, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r14), %rax
	leaq	15(%rdi), %rsi
	testb	$24, %al
	jne	.L882
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L880:
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r13), %rax
	leaq	39(%rdi), %rsi
	testb	$24, %al
	jne	.L883
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L878:
	movq	%r13, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r14), %rax
	leaq	31(%rdi), %rsi
	testb	$24, %al
	jne	.L884
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L877:
	movq	-1(%r13), %rdx
	cmpw	$168, 11(%rdx)
	jne	.L806
	movq	31(%rax), %r14
	leaq	31(%r13), %rsi
	movq	%r14, 31(%r13)
	testb	$1, %r14b
	je	.L819
	movq	%r14, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L885
.L809:
	testb	$24, %al
	je	.L819
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L819
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L819:
	movq	(%rbx), %rax
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L874:
	movq	%r14, 47(%r13)
	testb	%al, %al
	jne	.L798
.L870:
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L886
.L801:
	testb	$24, %al
	je	.L798
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L798
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L873:
	movq	%r14, 39(%r13)
	testb	%al, %al
	je	.L870
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L875:
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L872:
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L879:
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L881:
	addq	$24, %rsp
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L886:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	8(%rcx), %rax
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L876:
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L885:
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	jmp	.L809
	.cfi_endproc
.LFE18668:
	.size	_ZN2v88internal8WeakCell32RemoveFromFinalizationGroupCellsEPNS0_7IsolateE, .-_ZN2v88internal8WeakCell32RemoveFromFinalizationGroupCellsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Builtin_Impl_FinalizationGroupUnregisterENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"FinalizationGroup.prototype.unregister"
	.section	.text._ZN2v88internalL40Builtin_Impl_FinalizationGroupUnregisterENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Builtin_Impl_FinalizationGroupUnregisterENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL40Builtin_Impl_FinalizationGroupUnregisterENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18677:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L888
.L891:
	leaq	.LC13(%rip), %rax
	leaq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$38, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$62, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L944
.L943:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
.L894:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L916
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L916:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L945
	addq	$88, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L888:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1083, 11(%rdx)
	jne	.L891
	leaq	-8(%rsi), %rdx
	leaq	88(%r12), %r10
	cmpl	$6, %edi
	cmovl	%r10, %rdx
	movq	%rdx, %r14
	movq	(%rdx), %rdx
	testb	$1, %dl
	jne	.L946
.L896:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$369, %esi
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L944:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L946:
	movq	-1(%rdx), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L896
	movq	55(%rax), %rsi
	xorl	%r8d, %r8d
	cmpq	88(%r12), %rsi
	je	.L898
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L899
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r9
.L900:
	leaq	-80(%rbp), %rcx
	movq	%rsi, -80(%rbp)
	movq	%r14, %rsi
	movq	%rcx, %rdi
	movq	%r9, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE6LookupENS0_6HandleINS0_6ObjectEEE@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %r9
	testb	$1, %al
	jne	.L902
.L905:
	movq	%r14, %rdx
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19ObjectHashTableBaseINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE6RemoveEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_6ObjectEEEPb@PLT
	movq	(%r15), %r15
	movq	(%rax), %r14
	leaq	55(%r15), %rsi
	movq	%r14, 55(%r15)
	testb	$1, %r14b
	je	.L917
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	je	.L912
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rax
.L912:
	testb	$24, %al
	je	.L917
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L917
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L917:
	movzbl	-80(%rbp), %r8d
.L898:
	movl	%r8d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r14
	jmp	.L894
.L899:
	movq	%r13, %r9
	cmpq	41096(%r12), %r13
	je	.L947
.L901:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r9)
	jmp	.L900
.L902:
	movq	88(%r12), %r8
	movq	%r8, %rdx
	notq	%rdx
	movl	%edx, %edi
	andl	$1, %edi
	movb	%dil, -96(%rbp)
	movq	%r8, %rdi
	andq	$-262144, %rdi
	movq	%rdi, -88(%rbp)
.L911:
	movq	-1(%rax), %rsi
	cmpw	$168, 11(%rsi)
	jne	.L905
	movq	%rcx, %rdi
	movq	%r12, %rsi
	movq	%r8, -120(%rbp)
	movq	%r9, -112(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal8WeakCell32RemoveFromFinalizationGroupCellsEPNS0_7IsolateE
	movq	-80(%rbp), %rsi
	movq	-120(%rbp), %r8
	movq	63(%rsi), %rax
	movq	%r8, 55(%rsi)
	cmpb	$0, -96(%rbp)
	movq	-80(%rbp), %rdi
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %r9
	jne	.L906
	movq	-88(%rbp), %rsi
	movq	8(%rsi), %rsi
	testl	$262144, %esi
	je	.L907
	leaq	55(%rdi), %rsi
	movq	%r8, %rdx
	movq	%rcx, -128(%rbp)
	movq	%r9, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r9
	movq	8(%rsi), %rsi
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %r8
.L907:
	andb	$24, %sil
	je	.L908
	movq	%rdi, %rsi
	andq	$-262144, %rsi
	testb	$24, 8(%rsi)
	jne	.L908
	movq	%r8, %rdx
	leaq	55(%rdi), %rsi
	movq	%rcx, -128(%rbp)
	movq	%r9, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r9
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %r8
.L908:
	movq	-80(%rbp), %rsi
	movq	%r8, 63(%rsi)
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	8(%rsi), %rsi
	testl	$262144, %esi
	je	.L909
	leaq	63(%rdi), %rsi
	movq	%r8, %rdx
	movq	%rcx, -128(%rbp)
	movq	%r9, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r9
	movq	8(%rsi), %rsi
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %r8
.L909:
	andb	$24, %sil
	je	.L914
	movq	%rdi, %rsi
	andq	$-262144, %rsi
	testb	$24, 8(%rsi)
	jne	.L914
	movq	%r8, %rdx
	leaq	63(%rdi), %rsi
	movq	%rcx, -128(%rbp)
	movq	%r9, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r9
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %r8
.L914:
	testb	$1, %al
	je	.L905
	jmp	.L911
.L947:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L901
.L906:
	movq	%r8, 63(%rdi)
	jmp	.L914
.L945:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18677:
	.size	_ZN2v88internalL40Builtin_Impl_FinalizationGroupUnregisterENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL40Builtin_Impl_FinalizationGroupUnregisterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL46Builtin_Impl_Stats_FinalizationGroupUnregisterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"V8.Builtin_FinalizationGroupUnregister"
	.section	.text._ZN2v88internalL46Builtin_Impl_Stats_FinalizationGroupUnregisterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Builtin_Impl_Stats_FinalizationGroupUnregisterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL46Builtin_Impl_Stats_FinalizationGroupUnregisterEiPmPNS0_7IsolateE:
.LFB18675:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L977
.L949:
	movq	_ZZN2v88internalL46Builtin_Impl_Stats_FinalizationGroupUnregisterEiPmPNS0_7IsolateEE27trace_event_unique_atomic95(%rip), %rbx
	testq	%rbx, %rbx
	je	.L978
.L951:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L979
.L953:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL40Builtin_Impl_FinalizationGroupUnregisterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L980
.L957:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L981
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L978:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L982
.L952:
	movq	%rbx, _ZZN2v88internalL46Builtin_Impl_Stats_FinalizationGroupUnregisterEiPmPNS0_7IsolateEE27trace_event_unique_atomic95(%rip)
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L979:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L983
.L954:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L955
	movq	(%rdi), %rax
	call	*8(%rax)
.L955:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L956
	movq	(%rdi), %rax
	call	*8(%rax)
.L956:
	leaq	.LC14(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L980:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L977:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$857, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L983:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L954
	.p2align 4,,10
	.p2align 3
.L982:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L952
.L981:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18675:
	.size	_ZN2v88internalL46Builtin_Impl_Stats_FinalizationGroupUnregisterEiPmPNS0_7IsolateE, .-_ZN2v88internalL46Builtin_Impl_Stats_FinalizationGroupUnregisterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Builtin_FinalizationGroupConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Builtin_FinalizationGroupConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Builtin_FinalizationGroupConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Builtin_FinalizationGroupConstructorEiPmPNS0_7IsolateE:
.LFB18670:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L988
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL41Builtin_Impl_FinalizationGroupConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L988:
	.cfi_restore 6
	jmp	_ZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18670:
	.size	_ZN2v88internal36Builtin_FinalizationGroupConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal36Builtin_FinalizationGroupConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Builtin_FinalizationGroupRegisterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Builtin_FinalizationGroupRegisterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Builtin_FinalizationGroupRegisterEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Builtin_FinalizationGroupRegisterEiPmPNS0_7IsolateE:
.LFB18673:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L993
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL38Builtin_Impl_FinalizationGroupRegisterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L993:
	.cfi_restore 6
	jmp	_ZN2v88internalL44Builtin_Impl_Stats_FinalizationGroupRegisterEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18673:
	.size	_ZN2v88internal33Builtin_FinalizationGroupRegisterEiPmPNS0_7IsolateE, .-_ZN2v88internal33Builtin_FinalizationGroupRegisterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal35Builtin_FinalizationGroupUnregisterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Builtin_FinalizationGroupUnregisterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Builtin_FinalizationGroupUnregisterEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Builtin_FinalizationGroupUnregisterEiPmPNS0_7IsolateE:
.LFB18676:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L998
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL40Builtin_Impl_FinalizationGroupUnregisterENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L998:
	.cfi_restore 6
	jmp	_ZN2v88internalL46Builtin_Impl_Stats_FinalizationGroupUnregisterEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18676:
	.size	_ZN2v88internal35Builtin_FinalizationGroupUnregisterEiPmPNS0_7IsolateE, .-_ZN2v88internal35Builtin_FinalizationGroupUnregisterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Builtin_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Builtin_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Builtin_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Builtin_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateE:
.LFB18679:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1003
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL41Builtin_Impl_FinalizationGroupCleanupSomeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1003:
	.cfi_restore 6
	jmp	_ZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18679:
	.size	_ZN2v88internal36Builtin_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateE, .-_ZN2v88internal36Builtin_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal44Builtin_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal44Builtin_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateE
	.type	_ZN2v88internal44Builtin_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateE, @function
_ZN2v88internal44Builtin_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateE:
.LFB18682:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1008
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL49Builtin_Impl_FinalizationGroupCleanupIteratorNextENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1008:
	.cfi_restore 6
	jmp	_ZN2v88internalL55Builtin_Impl_Stats_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18682:
	.size	_ZN2v88internal44Builtin_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateE, .-_ZN2v88internal44Builtin_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Builtin_WeakRefConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Builtin_WeakRefConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Builtin_WeakRefConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Builtin_WeakRefConstructorEiPmPNS0_7IsolateE:
.LFB18685:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1013
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL31Builtin_Impl_WeakRefConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1013:
	.cfi_restore 6
	jmp	_ZN2v88internalL37Builtin_Impl_Stats_WeakRefConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18685:
	.size	_ZN2v88internal26Builtin_WeakRefConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal26Builtin_WeakRefConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal20Builtin_WeakRefDerefEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Builtin_WeakRefDerefEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Builtin_WeakRefDerefEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Builtin_WeakRefDerefEiPmPNS0_7IsolateE:
.LFB18688:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1018
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL25Builtin_Impl_WeakRefDerefENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1018:
	.cfi_restore 6
	jmp	_ZN2v88internalL31Builtin_Impl_Stats_WeakRefDerefEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18688:
	.size	_ZN2v88internal20Builtin_WeakRefDerefEiPmPNS0_7IsolateE, .-_ZN2v88internal20Builtin_WeakRefDerefEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal36Builtin_FinalizationGroupConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal36Builtin_FinalizationGroupConstructorEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal36Builtin_FinalizationGroupConstructorEiPmPNS0_7IsolateE:
.LFB22822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22822:
	.size	_GLOBAL__sub_I__ZN2v88internal36Builtin_FinalizationGroupConstructorEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal36Builtin_FinalizationGroupConstructorEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal36Builtin_FinalizationGroupConstructorEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL31Builtin_Impl_Stats_WeakRefDerefEiPmPNS0_7IsolateEE28trace_event_unique_atomic212,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Builtin_Impl_Stats_WeakRefDerefEiPmPNS0_7IsolateEE28trace_event_unique_atomic212, @object
	.size	_ZZN2v88internalL31Builtin_Impl_Stats_WeakRefDerefEiPmPNS0_7IsolateEE28trace_event_unique_atomic212, 8
_ZZN2v88internalL31Builtin_Impl_Stats_WeakRefDerefEiPmPNS0_7IsolateEE28trace_event_unique_atomic212:
	.zero	8
	.section	.bss._ZZN2v88internalL37Builtin_Impl_Stats_WeakRefConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic179,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Builtin_Impl_Stats_WeakRefConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic179, @object
	.size	_ZZN2v88internalL37Builtin_Impl_Stats_WeakRefConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic179, 8
_ZZN2v88internalL37Builtin_Impl_Stats_WeakRefConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic179:
	.zero	8
	.section	.bss._ZZN2v88internalL55Builtin_Impl_Stats_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateEE28trace_event_unique_atomic162,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL55Builtin_Impl_Stats_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateEE28trace_event_unique_atomic162, @object
	.size	_ZZN2v88internalL55Builtin_Impl_Stats_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateEE28trace_event_unique_atomic162, 8
_ZZN2v88internalL55Builtin_Impl_Stats_FinalizationGroupCleanupIteratorNextEiPmPNS0_7IsolateEE28trace_event_unique_atomic162:
	.zero	8
	.section	.bss._ZZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateEE28trace_event_unique_atomic124,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateEE28trace_event_unique_atomic124, @object
	.size	_ZZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateEE28trace_event_unique_atomic124, 8
_ZZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupCleanupSomeEiPmPNS0_7IsolateEE28trace_event_unique_atomic124:
	.zero	8
	.section	.bss._ZZN2v88internalL46Builtin_Impl_Stats_FinalizationGroupUnregisterEiPmPNS0_7IsolateEE27trace_event_unique_atomic95,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL46Builtin_Impl_Stats_FinalizationGroupUnregisterEiPmPNS0_7IsolateEE27trace_event_unique_atomic95, @object
	.size	_ZZN2v88internalL46Builtin_Impl_Stats_FinalizationGroupUnregisterEiPmPNS0_7IsolateEE27trace_event_unique_atomic95, 8
_ZZN2v88internalL46Builtin_Impl_Stats_FinalizationGroupUnregisterEiPmPNS0_7IsolateEE27trace_event_unique_atomic95:
	.zero	8
	.section	.bss._ZZN2v88internalL44Builtin_Impl_Stats_FinalizationGroupRegisterEiPmPNS0_7IsolateEE27trace_event_unique_atomic47,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Builtin_Impl_Stats_FinalizationGroupRegisterEiPmPNS0_7IsolateEE27trace_event_unique_atomic47, @object
	.size	_ZZN2v88internalL44Builtin_Impl_Stats_FinalizationGroupRegisterEiPmPNS0_7IsolateEE27trace_event_unique_atomic47, 8
_ZZN2v88internalL44Builtin_Impl_Stats_FinalizationGroupRegisterEiPmPNS0_7IsolateEE27trace_event_unique_atomic47:
	.zero	8
	.section	.bss._ZZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic12,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic12, @object
	.size	_ZZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic12, 8
_ZZN2v88internalL47Builtin_Impl_Stats_FinalizationGroupConstructorEiPmPNS0_7IsolateEE27trace_event_unique_atomic12:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
