	.file	"handler-outside-posix.cc"
	.text
	.section	.rodata._ZN2v88internal12trap_handler26RegisterDefaultTrapHandlerEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"!g_is_default_signal_handler_registered"
	.section	.rodata._ZN2v88internal12trap_handler26RegisterDefaultTrapHandlerEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal12trap_handler26RegisterDefaultTrapHandlerEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12trap_handler26RegisterDefaultTrapHandlerEv
	.type	_ZN2v88internal12trap_handler26RegisterDefaultTrapHandlerEv, @function
_ZN2v88internal12trap_handler26RegisterDefaultTrapHandlerEv:
.LFB3447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$160, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movzbl	_ZN2v88internal12trap_handler12_GLOBAL__N_138g_is_default_signal_handler_registeredE(%rip), %r12d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testb	%r12b, %r12b
	jne	.L7
	movq	_ZN2v88internal12trap_handler12HandleSignalEiP9siginfo_tPv@GOTPCREL(%rip), %rax
	leaq	-168(%rbp), %rdi
	leaq	-176(%rbp), %r13
	movl	$4, -40(%rbp)
	movq	%rax, -176(%rbp)
	call	sigemptyset@PLT
	leaq	_ZN2v88internal12trap_handler12_GLOBAL__N_113g_old_handlerE(%rip), %rdx
	movq	%r13, %rsi
	movl	$11, %edi
	call	sigaction@PLT
	testl	%eax, %eax
	jne	.L1
	movb	$1, _ZN2v88internal12trap_handler12_GLOBAL__N_138g_is_default_signal_handler_registeredE(%rip)
	movl	$1, %r12d
.L1:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L8
	addq	$160, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L8:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3447:
	.size	_ZN2v88internal12trap_handler26RegisterDefaultTrapHandlerEv, .-_ZN2v88internal12trap_handler26RegisterDefaultTrapHandlerEv
	.section	.text._ZN2v88internal12trap_handler17RemoveTrapHandlerEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12trap_handler17RemoveTrapHandlerEv
	.type	_ZN2v88internal12trap_handler17RemoveTrapHandlerEv, @function
_ZN2v88internal12trap_handler17RemoveTrapHandlerEv:
.LFB3448:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal12trap_handler12_GLOBAL__N_138g_is_default_signal_handler_registeredE(%rip)
	jne	.L17
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	leaq	_ZN2v88internal12trap_handler12_GLOBAL__N_113g_old_handlerE(%rip), %rsi
	movl	$11, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	sigaction@PLT
	testl	%eax, %eax
	jne	.L9
	movb	$0, _ZN2v88internal12trap_handler12_GLOBAL__N_138g_is_default_signal_handler_registeredE(%rip)
.L9:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3448:
	.size	_ZN2v88internal12trap_handler17RemoveTrapHandlerEv, .-_ZN2v88internal12trap_handler17RemoveTrapHandlerEv
	.section	.bss._ZN2v88internal12trap_handler12_GLOBAL__N_138g_is_default_signal_handler_registeredE,"aw",@nobits
	.type	_ZN2v88internal12trap_handler12_GLOBAL__N_138g_is_default_signal_handler_registeredE, @object
	.size	_ZN2v88internal12trap_handler12_GLOBAL__N_138g_is_default_signal_handler_registeredE, 1
_ZN2v88internal12trap_handler12_GLOBAL__N_138g_is_default_signal_handler_registeredE:
	.zero	1
	.section	.bss._ZN2v88internal12trap_handler12_GLOBAL__N_113g_old_handlerE,"aw",@nobits
	.align 32
	.type	_ZN2v88internal12trap_handler12_GLOBAL__N_113g_old_handlerE, @object
	.size	_ZN2v88internal12trap_handler12_GLOBAL__N_113g_old_handlerE, 152
_ZN2v88internal12trap_handler12_GLOBAL__N_113g_old_handlerE:
	.zero	152
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
