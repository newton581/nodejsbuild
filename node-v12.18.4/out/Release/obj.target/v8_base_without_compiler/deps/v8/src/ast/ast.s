	.file	"ast.cc"
	.text
	.section	.rodata._ZN2v88internal7Literal5MatchEPvS2_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal7Literal5MatchEPvS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Literal5MatchEPvS2_
	.type	_ZN2v88internal7Literal5MatchEPvS2_, @function
_ZN2v88internal7Literal5MatchEPvS2_:
.LFB19478:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	shrl	$7, %eax
	andl	$15, %eax
	cmpl	$3, %eax
	je	.L20
	cmpl	$1, %eax
	je	.L4
	testl	%eax, %eax
	jne	.L18
	movl	4(%rsi), %eax
	shrl	$7, %eax
	testb	$14, %al
	jne	.L18
	pxor	%xmm0, %xmm0
	andl	$15, %eax
	cvtsi2sdl	8(%rdi), %xmm0
	testl	%eax, %eax
	jne	.L21
.L7:
	pxor	%xmm1, %xmm1
	movl	$0, %edx
	cvtsi2sdl	8(%rsi), %xmm1
	ucomisd	%xmm0, %xmm1
	setnp	%al
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	4(%rsi), %eax
	shrl	$7, %eax
	testb	$14, %al
	jne	.L18
	andl	$15, %eax
	movsd	8(%rdi), %xmm0
	testl	%eax, %eax
	je	.L7
.L21:
	cmpl	$1, %eax
	jne	.L22
	movsd	8(%rsi), %xmm1
	movl	$0, %edx
	ucomisd	%xmm0, %xmm1
	setnp	%al
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movl	4(%rsi), %edx
	xorl	%eax, %eax
	shrl	$7, %edx
	andl	$15, %edx
	cmpl	$3, %edx
	je	.L23
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	8(%rsi), %rax
	cmpq	%rax, 8(%rdi)
	sete	%al
	ret
.L22:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19478:
	.size	_ZN2v88internal7Literal5MatchEPvS2_, .-_ZN2v88internal7Literal5MatchEPvS2_
	.section	.text._ZN2v88internal8Variable16SetMaybeAssignedEv,"axG",@progbits,_ZN2v88internal8Variable16SetMaybeAssignedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8Variable16SetMaybeAssignedEv
	.type	_ZN2v88internal8Variable16SetMaybeAssignedEv, @function
_ZN2v88internal8Variable16SetMaybeAssignedEv:
.LFB9361:
	.cfi_startproc
	endbr64
	movzwl	40(%rdi), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L34
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L26
	testb	$64, %ah
	je	.L37
.L26:
	orb	$64, %ah
	movw	%ax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%rbx), %eax
	orb	$64, %ah
	movw	%ax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9361:
	.size	_ZN2v88internal8Variable16SetMaybeAssignedEv, .-_ZN2v88internal8Variable16SetMaybeAssignedEv
	.section	.text._ZN2v88internal7AstNode20AsIterationStatementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7AstNode20AsIterationStatementEv
	.type	_ZN2v88internal7AstNode20AsIterationStatementEv, @function
_ZN2v88internal7AstNode20AsIterationStatementEv:
.LFB19362:
	.cfi_startproc
	endbr64
	movzbl	4(%rdi), %eax
	andl	$63, %eax
	subl	$2, %eax
	cmpb	$4, %al
	movl	$0, %eax
	cmovbe	%rdi, %rax
	ret
	.cfi_endproc
.LFE19362:
	.size	_ZN2v88internal7AstNode20AsIterationStatementEv, .-_ZN2v88internal7AstNode20AsIterationStatementEv
	.section	.text._ZN2v88internal7AstNode21AsMaterializedLiteralEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7AstNode21AsMaterializedLiteralEv
	.type	_ZN2v88internal7AstNode21AsMaterializedLiteralEv, @function
_ZN2v88internal7AstNode21AsMaterializedLiteralEv:
.LFB19363:
	.cfi_startproc
	endbr64
	movzbl	4(%rdi), %eax
	andl	$63, %eax
	subl	$21, %eax
	cmpb	$2, %al
	movl	$0, %eax
	cmovbe	%rdi, %rax
	ret
	.cfi_endproc
.LFE19363:
	.size	_ZN2v88internal7AstNode21AsMaterializedLiteralEv, .-_ZN2v88internal7AstNode21AsMaterializedLiteralEv
	.section	.text._ZNK2v88internal10Expression12IsSmiLiteralEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10Expression12IsSmiLiteralEv
	.type	_ZNK2v88internal10Expression12IsSmiLiteralEv, @function
_ZNK2v88internal10Expression12IsSmiLiteralEv:
.LFB19364:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %edx
	xorl	%eax, %eax
	movl	%edx, %ecx
	andl	$63, %ecx
	cmpb	$41, %cl
	je	.L47
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	andl	$1920, %edx
	sete	%al
	ret
	.cfi_endproc
.LFE19364:
	.size	_ZNK2v88internal10Expression12IsSmiLiteralEv, .-_ZNK2v88internal10Expression12IsSmiLiteralEv
	.section	.text._ZNK2v88internal10Expression15IsNumberLiteralEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10Expression15IsNumberLiteralEv
	.type	_ZNK2v88internal10Expression15IsNumberLiteralEv, @function
_ZNK2v88internal10Expression15IsNumberLiteralEv:
.LFB19365:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	xorl	%r8d, %r8d
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$41, %dl
	jne	.L48
	shrl	$7, %eax
	testb	$14, %al
	sete	%r8b
.L48:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE19365:
	.size	_ZNK2v88internal10Expression15IsNumberLiteralEv, .-_ZNK2v88internal10Expression15IsNumberLiteralEv
	.section	.text._ZNK2v88internal10Expression15IsStringLiteralEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10Expression15IsStringLiteralEv
	.type	_ZNK2v88internal10Expression15IsStringLiteralEv, @function
_ZNK2v88internal10Expression15IsStringLiteralEv:
.LFB19366:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	xorl	%r8d, %r8d
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$41, %dl
	jne	.L51
	shrl	$7, %eax
	andl	$15, %eax
	cmpl	$3, %eax
	sete	%r8b
.L51:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE19366:
	.size	_ZNK2v88internal10Expression15IsStringLiteralEv, .-_ZNK2v88internal10Expression15IsStringLiteralEv
	.section	.text._ZNK2v88internal10Expression14IsPropertyNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10Expression14IsPropertyNameEv
	.type	_ZNK2v88internal10Expression14IsPropertyNameEv, @function
_ZNK2v88internal10Expression14IsPropertyNameEv:
.LFB19367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	4(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	%edx, %ecx
	andl	$63, %ecx
	cmpb	$41, %cl
	je	.L60
.L54:
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L61
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	shrl	$7, %edx
	andl	$15, %edx
	cmpl	$3, %edx
	jne	.L54
	movq	8(%rdi), %rdi
	leaq	-12(%rbp), %rsi
	call	_ZNK2v88internal12AstRawString12AsArrayIndexEPj@PLT
	xorl	$1, %eax
	jmp	.L54
.L61:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19367:
	.size	_ZNK2v88internal10Expression14IsPropertyNameEv, .-_ZNK2v88internal10Expression14IsPropertyNameEv
	.section	.text._ZNK2v88internal10Expression13IsNullLiteralEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10Expression13IsNullLiteralEv
	.type	_ZNK2v88internal10Expression13IsNullLiteralEv, @function
_ZNK2v88internal10Expression13IsNullLiteralEv:
.LFB19368:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	xorl	%r8d, %r8d
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$41, %dl
	jne	.L62
	shrl	$7, %eax
	andl	$15, %eax
	cmpl	$7, %eax
	sete	%r8b
.L62:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE19368:
	.size	_ZNK2v88internal10Expression13IsNullLiteralEv, .-_ZNK2v88internal10Expression13IsNullLiteralEv
	.section	.text._ZNK2v88internal10Expression16IsTheHoleLiteralEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10Expression16IsTheHoleLiteralEv
	.type	_ZNK2v88internal10Expression16IsTheHoleLiteralEv, @function
_ZNK2v88internal10Expression16IsTheHoleLiteralEv:
.LFB19369:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	xorl	%r8d, %r8d
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$41, %dl
	jne	.L65
	shrl	$7, %eax
	andl	$15, %eax
	cmpl	$8, %eax
	sete	%r8b
.L65:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE19369:
	.size	_ZNK2v88internal10Expression16IsTheHoleLiteralEv, .-_ZNK2v88internal10Expression16IsTheHoleLiteralEv
	.section	.text._ZN2v88internal10Expression18IsCompileTimeValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10Expression18IsCompileTimeValueEv
	.type	_ZN2v88internal10Expression18IsCompileTimeValueEv, @function
_ZN2v88internal10Expression18IsCompileTimeValueEv:
.LFB19370:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	movl	$1, %r8d
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$41, %dl
	je	.L68
	leal	-21(%rdx), %ecx
	xorl	%r8d, %r8d
	cmpb	$2, %cl
	ja	.L68
	shrl	$8, %eax
	cmpb	$23, %dl
	je	.L75
	andl	$1, %eax
	cmpb	$22, %dl
	cmove	%eax, %r8d
.L68:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	movl	%eax, %r8d
	andl	$1, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE19370:
	.size	_ZN2v88internal10Expression18IsCompileTimeValueEv, .-_ZN2v88internal10Expression18IsCompileTimeValueEv
	.section	.rodata._ZNK2v88internal10Expression18IsUndefinedLiteralEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"undefined"
	.section	.text._ZNK2v88internal10Expression18IsUndefinedLiteralEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10Expression18IsUndefinedLiteralEv
	.type	_ZNK2v88internal10Expression18IsUndefinedLiteralEv, @function
_ZNK2v88internal10Expression18IsUndefinedLiteralEv:
.LFB19371:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$41, %dl
	je	.L86
	cmpb	$54, %dl
	jne	.L82
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L82
	testw	$896, 40(%rdi)
	jne	.L82
	testb	$1, %ah
	je	.L79
	movq	8(%rdi), %rdi
.L79:
	leaq	.LC1(%rip), %rsi
	jmp	_ZNK2v88internal12AstRawString16IsOneByteEqualToEPKc@PLT
	.p2align 4,,10
	.p2align 3
.L82:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	shrl	$7, %eax
	andl	$15, %eax
	cmpl	$6, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE19371:
	.size	_ZNK2v88internal10Expression18IsUndefinedLiteralEv, .-_ZNK2v88internal10Expression18IsUndefinedLiteralEv
	.section	.text._ZNK2v88internal10Expression30IsLiteralButNotNullOrUndefinedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10Expression30IsLiteralButNotNullOrUndefinedEv
	.type	_ZNK2v88internal10Expression30IsLiteralButNotNullOrUndefinedEv, @function
_ZNK2v88internal10Expression30IsLiteralButNotNullOrUndefinedEv:
.LFB19372:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	xorl	%r8d, %r8d
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$41, %dl
	je	.L90
.L87:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	shrl	$7, %eax
	andl	$15, %eax
	subl	$6, %eax
	cmpl	$1, %eax
	seta	%r8b
	jmp	.L87
	.cfi_endproc
.LFE19372:
	.size	_ZNK2v88internal10Expression30IsLiteralButNotNullOrUndefinedEv, .-_ZNK2v88internal10Expression30IsLiteralButNotNullOrUndefinedEv
	.section	.text._ZNK2v88internal10Expression15ToBooleanIsTrueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10Expression15ToBooleanIsTrueEv
	.type	_ZNK2v88internal10Expression15ToBooleanIsTrueEv, @function
_ZNK2v88internal10Expression15ToBooleanIsTrueEv:
.LFB19373:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	xorl	%r8d, %r8d
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$41, %dl
	je	.L118
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	shrl	$7, %eax
	andl	$15, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	cmpl	$7, %eax
	ja	.L93
	leaq	.L95(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal10Expression15ToBooleanIsTrueEv,"a",@progbits
	.align 4
	.align 4
.L95:
	.long	.L100-.L95
	.long	.L99-.L95
	.long	.L98-.L95
	.long	.L97-.L95
	.long	.L111-.L95
	.long	.L96-.L95
	.long	.L112-.L95
	.long	.L112-.L95
	.section	.text._ZNK2v88internal10Expression15ToBooleanIsTrueEv
	.p2align 4,,10
	.p2align 3
.L112:
	xorl	%r8d, %r8d
.L91:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	8(%rdi), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	movzbl	(%rbx), %edx
	cmpq	$1, %rax
	je	.L119
	cmpb	$48, %dl
	sete	%dl
	movzbl	%dl, %edx
	addq	%rdx, %rdx
	cmpq	%rdx, %rax
	jbe	.L112
.L103:
	addq	%rbx, %rdx
	addq	%rbx, %rax
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L120:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	je	.L112
.L105:
	cmpb	$48, (%rdx)
	je	.L120
.L111:
	movl	$1, %r8d
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L100:
	movl	8(%rdi), %edx
	testl	%edx, %edx
	setne	%r8b
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L99:
	movsd	8(%rdi), %xmm0
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15DoubleToBooleanEd@PLT
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movl	16(%rax), %eax
	testl	%eax, %eax
	setne	%r8b
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L96:
	movzbl	8(%rdi), %r8d
	jmp	.L91
.L119:
	xorl	%r8d, %r8d
	cmpb	$48, %dl
	je	.L91
	xorl	%edx, %edx
	jmp	.L103
.L93:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19373:
	.size	_ZNK2v88internal10Expression15ToBooleanIsTrueEv, .-_ZNK2v88internal10Expression15ToBooleanIsTrueEv
	.section	.text._ZNK2v88internal10Expression16ToBooleanIsFalseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10Expression16ToBooleanIsFalseEv
	.type	_ZNK2v88internal10Expression16ToBooleanIsFalseEv, @function
_ZNK2v88internal10Expression16ToBooleanIsFalseEv:
.LFB19374:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %edx
	xorl	%eax, %eax
	movl	%edx, %ecx
	andl	$63, %ecx
	cmpb	$41, %cl
	je	.L148
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	shrl	$7, %edx
	andl	$15, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	cmpl	$7, %edx
	ja	.L123
	leaq	.L124(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal10Expression16ToBooleanIsFalseEv,"a",@progbits
	.align 4
	.align 4
.L124:
	.long	.L130-.L124
	.long	.L129-.L124
	.long	.L128-.L124
	.long	.L127-.L124
	.long	.L141-.L124
	.long	.L125-.L124
	.long	.L142-.L124
	.long	.L142-.L124
	.section	.text._ZNK2v88internal10Expression16ToBooleanIsFalseEv
	.p2align 4,,10
	.p2align 3
.L142:
	movl	$1, %eax
.L121:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movq	8(%rdi), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	movzbl	(%rbx), %edx
	movq	%rax, %rcx
	cmpq	$1, %rax
	je	.L149
	cmpb	$48, %dl
	sete	%dl
	movzbl	%dl, %edx
	addq	%rdx, %rdx
	cmpq	%rax, %rdx
	jnb	.L142
.L133:
	addq	%rbx, %rdx
	leaq	(%rbx,%rcx), %rax
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L150:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	je	.L142
.L135:
	cmpb	$48, (%rdx)
	je	.L150
.L141:
	xorl	%eax, %eax
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L130:
	movl	8(%rdi), %edx
	testl	%edx, %edx
	setne	%al
.L131:
	addq	$8, %rsp
	xorl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movsd	8(%rdi), %xmm0
	call	_ZN2v88internal15DoubleToBooleanEd@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L127:
	movq	8(%rdi), %rax
	movl	16(%rax), %eax
	testl	%eax, %eax
	setne	%al
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L125:
	movzbl	8(%rdi), %eax
	jmp	.L131
.L149:
	movl	$1, %eax
	cmpb	$48, %dl
	je	.L121
	xorl	%edx, %edx
	jmp	.L133
.L123:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19374:
	.size	_ZNK2v88internal10Expression16ToBooleanIsFalseEv, .-_ZNK2v88internal10Expression16ToBooleanIsFalseEv
	.section	.text._ZNK2v88internal10Expression13IsPrivateNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10Expression13IsPrivateNameEv
	.type	_ZNK2v88internal10Expression13IsPrivateNameEv, @function
_ZNK2v88internal10Expression13IsPrivateNameEv:
.LFB19375:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %edx
	xorl	%eax, %eax
	movl	%edx, %ecx
	andl	$63, %ecx
	cmpb	$54, %cl
	je	.L164
.L161:
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	movq	8(%rdi), %rdi
	andb	$1, %dh
	je	.L153
	movq	8(%rdi), %rdi
.L153:
	movq	16(%rdi), %rax
	cmpb	$0, 28(%rdi)
	movl	%eax, %edx
	je	.L165
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L161
.L166:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal12AstRawString14FirstCharacterEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpw	$35, %ax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore 6
	shrl	$31, %edx
	addl	%eax, %edx
	xorl	%eax, %eax
	sarl	%edx
	testl	%edx, %edx
	jle	.L161
	jmp	.L166
	.cfi_endproc
.LFE19375:
	.size	_ZNK2v88internal10Expression13IsPrivateNameEv, .-_ZNK2v88internal10Expression13IsPrivateNameEv
	.section	.text._ZNK2v88internal10Expression26IsValidReferenceExpressionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10Expression26IsValidReferenceExpressionEv
	.type	_ZNK2v88internal10Expression26IsValidReferenceExpressionEv, @function
_ZNK2v88internal10Expression26IsValidReferenceExpressionEv:
.LFB19376:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	movl	$1, %r8d
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$44, %dl
	je	.L167
	xorl	%r8d, %r8d
	cmpb	$54, %dl
	je	.L172
.L167:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	shrl	$10, %eax
	xorl	$1, %eax
	movl	%eax, %r8d
	andl	$1, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE19376:
	.size	_ZNK2v88internal10Expression26IsValidReferenceExpressionEv, .-_ZNK2v88internal10Expression26IsValidReferenceExpressionEv
	.section	.text._ZNK2v88internal10Expression29IsAnonymousFunctionDefinitionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10Expression29IsAnonymousFunctionDefinitionEv
	.type	_ZNK2v88internal10Expression29IsAnonymousFunctionDefinitionEv, @function
_ZNK2v88internal10Expression29IsAnonymousFunctionDefinitionEv:
.LFB19377:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$38, %dl
	je	.L177
	shrl	$9, %eax
	andl	$1, %eax
	cmpb	$31, %dl
	movl	$0, %edx
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	testl	$896, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE19377:
	.size	_ZNK2v88internal10Expression29IsAnonymousFunctionDefinitionEv, .-_ZNK2v88internal10Expression29IsAnonymousFunctionDefinitionEv
	.section	.text._ZNK2v88internal10Expression25IsConciseMethodDefinitionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10Expression25IsConciseMethodDefinitionEv
	.type	_ZNK2v88internal10Expression25IsConciseMethodDefinitionEv, @function
_ZNK2v88internal10Expression25IsConciseMethodDefinitionEv:
.LFB19378:
	.cfi_startproc
	endbr64
	movzbl	4(%rdi), %eax
	xorl	%r8d, %r8d
	andl	$63, %eax
	cmpb	$38, %al
	je	.L182
.L178:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	movq	40(%rdi), %rax
	movl	$1, %r8d
	movzbl	133(%rax), %eax
	leal	-11(%rax), %edx
	cmpb	$1, %dl
	jbe	.L178
	subl	$15, %eax
	cmpb	$2, %al
	setbe	%r8b
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE19378:
	.size	_ZNK2v88internal10Expression25IsConciseMethodDefinitionEv, .-_ZNK2v88internal10Expression25IsConciseMethodDefinitionEv
	.section	.text._ZNK2v88internal10Expression28IsAccessorFunctionDefinitionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10Expression28IsAccessorFunctionDefinitionEv
	.type	_ZNK2v88internal10Expression28IsAccessorFunctionDefinitionEv, @function
_ZNK2v88internal10Expression28IsAccessorFunctionDefinitionEv:
.LFB19379:
	.cfi_startproc
	endbr64
	movzbl	4(%rdi), %eax
	xorl	%r8d, %r8d
	andl	$63, %eax
	cmpb	$38, %al
	je	.L186
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	movq	40(%rdi), %rax
	movzbl	133(%rax), %eax
	subl	$6, %eax
	cmpb	$1, %al
	setbe	%r8b
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE19379:
	.size	_ZNK2v88internal10Expression28IsAccessorFunctionDefinitionEv, .-_ZNK2v88internal10Expression28IsAccessorFunctionDefinitionEv
	.section	.text._ZN2v88internal13VariableProxyC2EPNS0_8VariableEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13VariableProxyC2EPNS0_8VariableEi
	.type	_ZN2v88internal13VariableProxyC2EPNS0_8VariableEi, @function
_ZN2v88internal13VariableProxyC2EPNS0_8VariableEi:
.LFB19381:
	.cfi_startproc
	endbr64
	movl	%edx, (%rdi)
	movq	$0, 16(%rdi)
	orw	$2048, 40(%rsi)
	movq	%rsi, 8(%rdi)
	movl	$2358, 4(%rdi)
	ret
	.cfi_endproc
.LFE19381:
	.size	_ZN2v88internal13VariableProxyC2EPNS0_8VariableEi, .-_ZN2v88internal13VariableProxyC2EPNS0_8VariableEi
	.globl	_ZN2v88internal13VariableProxyC1EPNS0_8VariableEi
	.set	_ZN2v88internal13VariableProxyC1EPNS0_8VariableEi,_ZN2v88internal13VariableProxyC2EPNS0_8VariableEi
	.section	.text._ZN2v88internal13VariableProxyC2EPKS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13VariableProxyC2EPKS1_
	.type	_ZN2v88internal13VariableProxyC2EPKS1_, @function
_ZN2v88internal13VariableProxyC2EPKS1_:
.LFB19384:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	movl	$54, 4(%rdi)
	movq	$0, 16(%rdi)
	movl	%eax, (%rdi)
	movl	4(%rsi), %eax
	movl	%eax, 4(%rdi)
	movq	8(%rsi), %rax
	movq	%rax, 8(%rdi)
	ret
	.cfi_endproc
.LFE19384:
	.size	_ZN2v88internal13VariableProxyC2EPKS1_, .-_ZN2v88internal13VariableProxyC2EPKS1_
	.globl	_ZN2v88internal13VariableProxyC1EPKS1_
	.set	_ZN2v88internal13VariableProxyC1EPKS1_,_ZN2v88internal13VariableProxyC2EPKS1_
	.section	.text._ZN2v88internal13VariableProxy6BindToEPNS0_8VariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13VariableProxy6BindToEPNS0_8VariableE
	.type	_ZN2v88internal13VariableProxy6BindToEPNS0_8VariableE, @function
_ZN2v88internal13VariableProxy6BindToEPNS0_8VariableE:
.LFB19386:
	.cfi_startproc
	endbr64
	orl	$256, 4(%rdi)
	movq	%rsi, 8(%rdi)
	movzwl	40(%rsi), %eax
	orb	$8, %ah
	movw	%ax, 40(%rsi)
	testb	$-128, 4(%rdi)
	jne	.L226
.L223:
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	movl	%eax, %edx
	andl	$15, %edx
	cmpb	$1, %dl
	je	.L223
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	16(%rsi), %rbx
	testq	%rbx, %rbx
	je	.L191
	testb	$64, %ah
	jne	.L191
	movzwl	40(%rbx), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L191
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L192
	testb	$64, %dh
	je	.L227
.L192:
	orb	$64, %dh
	movw	%dx, 40(%rbx)
	movzwl	40(%rsi), %eax
.L191:
	orb	$64, %ah
	movw	%ax, 40(%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_restore_state
	movzwl	40(%r12), %eax
	movl	%eax, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L192
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L193
	testb	$64, %ah
	je	.L228
.L193:
	orb	$64, %ah
	movw	%ax, 40(%r12)
	movzwl	40(%rbx), %edx
	jmp	.L192
.L228:
	movzwl	40(%r13), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$1, %cl
	je	.L193
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L194
	testb	$64, %dh
	je	.L229
.L194:
	orb	$64, %dh
	movw	%dx, 40(%r13)
	movzwl	40(%r12), %eax
	jmp	.L193
.L229:
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal8Variable16SetMaybeAssignedEv
	movzwl	40(%r13), %edx
	movq	-40(%rbp), %rsi
	jmp	.L194
	.cfi_endproc
.LFE19386:
	.size	_ZN2v88internal13VariableProxy6BindToEPNS0_8VariableE, .-_ZN2v88internal13VariableProxy6BindToEPNS0_8VariableE
	.section	.text._ZN2v88internal10AssignmentC2ENS0_7AstNode8NodeTypeENS0_5Token5ValueEPNS0_10ExpressionES7_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10AssignmentC2ENS0_7AstNode8NodeTypeENS0_5Token5ValueEPNS0_10ExpressionES7_i
	.type	_ZN2v88internal10AssignmentC2ENS0_7AstNode8NodeTypeENS0_5Token5ValueEPNS0_10ExpressionES7_i, @function
_ZN2v88internal10AssignmentC2ENS0_7AstNode8NodeTypeENS0_5Token5ValueEPNS0_10ExpressionES7_i:
.LFB19388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%dl, %edx
	movzbl	%sil, %esi
	movq	%rcx, %xmm0
	sall	$7, %edx
	orl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%r9d, (%rdi)
	movq	%r8, -8(%rbp)
	movl	%edx, 4(%rdi)
	movhps	-8(%rbp), %xmm0
	movups	%xmm0, 8(%rdi)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19388:
	.size	_ZN2v88internal10AssignmentC2ENS0_7AstNode8NodeTypeENS0_5Token5ValueEPNS0_10ExpressionES7_i, .-_ZN2v88internal10AssignmentC2ENS0_7AstNode8NodeTypeENS0_5Token5ValueEPNS0_10ExpressionES7_i
	.globl	_ZN2v88internal10AssignmentC1ENS0_7AstNode8NodeTypeENS0_5Token5ValueEPNS0_10ExpressionES7_i
	.set	_ZN2v88internal10AssignmentC1ENS0_7AstNode8NodeTypeENS0_5Token5ValueEPNS0_10ExpressionES7_i,_ZN2v88internal10AssignmentC2ENS0_7AstNode8NodeTypeENS0_5Token5ValueEPNS0_10ExpressionES7_i
	.section	.text._ZN2v88internal15FunctionLiteral17set_inferred_nameENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15FunctionLiteral17set_inferred_nameENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal15FunctionLiteral17set_inferred_nameENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal15FunctionLiteral17set_inferred_nameENS0_6HandleINS0_6StringEEE:
.LFB19390:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	%rsi, 72(%rdi)
	movq	$0, 64(%rdi)
	orb	$2, 132(%rax)
	ret
	.cfi_endproc
.LFE19390:
	.size	_ZN2v88internal15FunctionLiteral17set_inferred_nameENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal15FunctionLiteral17set_inferred_nameENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal15FunctionLiteral21set_raw_inferred_nameEPKNS0_13AstConsStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15FunctionLiteral21set_raw_inferred_nameEPKNS0_13AstConsStringE
	.type	_ZN2v88internal15FunctionLiteral21set_raw_inferred_nameEPKNS0_13AstConsStringE, @function
_ZN2v88internal15FunctionLiteral21set_raw_inferred_nameEPKNS0_13AstConsStringE:
.LFB19391:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	%rsi, 64(%rdi)
	movq	$0, 72(%rdi)
	orb	$2, 132(%rax)
	ret
	.cfi_endproc
.LFE19391:
	.size	_ZN2v88internal15FunctionLiteral21set_raw_inferred_nameEPKNS0_13AstConsStringE, .-_ZN2v88internal15FunctionLiteral21set_raw_inferred_nameEPKNS0_13AstConsStringE
	.section	.text._ZNK2v88internal15FunctionLiteral18ShouldEagerCompileEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15FunctionLiteral18ShouldEagerCompileEv
	.type	_ZNK2v88internal15FunctionLiteral18ShouldEagerCompileEv, @function
_ZNK2v88internal15FunctionLiteral18ShouldEagerCompileEv:
.LFB19392:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movzbl	131(%rax), %edx
	movl	%edx, %eax
	shrb	$2, %al
	andl	$1, %eax
	jne	.L234
	movl	%edx, %eax
	shrb	$6, %al
	andl	$1, %eax
.L234:
	ret
	.cfi_endproc
.LFE19392:
	.size	_ZNK2v88internal15FunctionLiteral18ShouldEagerCompileEv, .-_ZNK2v88internal15FunctionLiteral18ShouldEagerCompileEv
	.section	.text._ZN2v88internal15FunctionLiteral21SetShouldEagerCompileEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15FunctionLiteral21SetShouldEagerCompileEv
	.type	_ZN2v88internal15FunctionLiteral21SetShouldEagerCompileEv, @function
_ZN2v88internal15FunctionLiteral21SetShouldEagerCompileEv:
.LFB19393:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	_ZN2v88internal16DeclarationScope24set_should_eager_compileEv@PLT
	.cfi_endproc
.LFE19393:
	.size	_ZN2v88internal15FunctionLiteral21SetShouldEagerCompileEv, .-_ZN2v88internal15FunctionLiteral21SetShouldEagerCompileEv
	.section	.text._ZN2v88internal15FunctionLiteral21AllowsLazyCompilationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15FunctionLiteral21AllowsLazyCompilationEv
	.type	_ZN2v88internal15FunctionLiteral21AllowsLazyCompilationEv, @function
_ZN2v88internal15FunctionLiteral21AllowsLazyCompilationEv:
.LFB19394:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdi
	jmp	_ZNK2v88internal16DeclarationScope21AllowsLazyCompilationEv@PLT
	.cfi_endproc
.LFE19394:
	.size	_ZN2v88internal15FunctionLiteral21AllowsLazyCompilationEv, .-_ZN2v88internal15FunctionLiteral21AllowsLazyCompilationEv
	.section	.text._ZNK2v88internal15FunctionLiteral26SafeToSkipArgumentsAdaptorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15FunctionLiteral26SafeToSkipArgumentsAdaptorEv
	.type	_ZNK2v88internal15FunctionLiteral26SafeToSkipArgumentsAdaptorEv, @function
_ZNK2v88internal15FunctionLiteral26SafeToSkipArgumentsAdaptorEv:
.LFB19395:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdx
	movzbl	129(%rdx), %eax
	andl	$1, %eax
	je	.L238
	cmpq	$0, 200(%rdx)
	je	.L247
	xorl	%eax, %eax
.L238:
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	testb	$8, 131(%rdx)
	je	.L238
	movl	156(%rdx), %eax
	movq	144(%rdx), %rdx
	subl	$1, %eax
	cltq
	cmpq	$0, (%rdx,%rax,8)
	sete	%al
	ret
	.cfi_endproc
.LFE19395:
	.size	_ZNK2v88internal15FunctionLiteral26SafeToSkipArgumentsAdaptorEv, .-_ZNK2v88internal15FunctionLiteral26SafeToSkipArgumentsAdaptorEv
	.section	.text._ZNK2v88internal15FunctionLiteral4nameEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15FunctionLiteral4nameEPNS0_7IsolateE
	.type	_ZNK2v88internal15FunctionLiteral4nameEPNS0_7IsolateE, @function
_ZNK2v88internal15FunctionLiteral4nameEPNS0_7IsolateE:
.LFB19396:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdx
	leaq	128(%rsi), %rax
	testq	%rdx, %rdx
	je	.L250
	movq	(%rdx), %rax
.L250:
	ret
	.cfi_endproc
.LFE19396:
	.size	_ZNK2v88internal15FunctionLiteral4nameEPNS0_7IsolateE, .-_ZNK2v88internal15FunctionLiteral4nameEPNS0_7IsolateE
	.section	.text._ZNK2v88internal15FunctionLiteral14start_positionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15FunctionLiteral14start_positionEv
	.type	_ZNK2v88internal15FunctionLiteral14start_positionEv, @function
_ZNK2v88internal15FunctionLiteral14start_positionEv:
.LFB19397:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	112(%rax), %eax
	ret
	.cfi_endproc
.LFE19397:
	.size	_ZNK2v88internal15FunctionLiteral14start_positionEv, .-_ZNK2v88internal15FunctionLiteral14start_positionEv
	.section	.text._ZNK2v88internal15FunctionLiteral12end_positionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15FunctionLiteral12end_positionEv
	.type	_ZNK2v88internal15FunctionLiteral12end_positionEv, @function
_ZNK2v88internal15FunctionLiteral12end_positionEv:
.LFB19398:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	116(%rax), %eax
	ret
	.cfi_endproc
.LFE19398:
	.size	_ZNK2v88internal15FunctionLiteral12end_positionEv, .-_ZNK2v88internal15FunctionLiteral12end_positionEv
	.section	.text._ZNK2v88internal15FunctionLiteral13language_modeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15FunctionLiteral13language_modeEv
	.type	_ZNK2v88internal15FunctionLiteral13language_modeEv, @function
_ZNK2v88internal15FunctionLiteral13language_modeEv:
.LFB19399:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movzbl	129(%rax), %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE19399:
	.size	_ZNK2v88internal15FunctionLiteral13language_modeEv, .-_ZNK2v88internal15FunctionLiteral13language_modeEv
	.section	.text._ZNK2v88internal15FunctionLiteral4kindEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15FunctionLiteral4kindEv
	.type	_ZNK2v88internal15FunctionLiteral4kindEv, @function
_ZNK2v88internal15FunctionLiteral4kindEv:
.LFB19400:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movzbl	133(%rax), %eax
	ret
	.cfi_endproc
.LFE19400:
	.size	_ZNK2v88internal15FunctionLiteral4kindEv, .-_ZNK2v88internal15FunctionLiteral4kindEv
	.section	.text._ZN2v88internal15FunctionLiteral15NeedsHomeObjectEPNS0_10ExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15FunctionLiteral15NeedsHomeObjectEPNS0_10ExpressionE
	.type	_ZN2v88internal15FunctionLiteral15NeedsHomeObjectEPNS0_10ExpressionE, @function
_ZN2v88internal15FunctionLiteral15NeedsHomeObjectEPNS0_10ExpressionE:
.LFB19401:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L256
	movzbl	4(%rdi), %edx
	andl	$63, %edx
	cmpb	$38, %dl
	je	.L264
.L256:
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	movq	40(%rdi), %rdx
	movzbl	131(%rdx), %eax
	shrb	$5, %al
	andl	$1, %eax
	jne	.L256
	testb	$64, 129(%rdx)
	je	.L256
	movzbl	133(%rdx), %ecx
	cmpb	$17, %cl
	ja	.L256
	movl	$235772, %eax
	shrq	%cl, %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE19401:
	.size	_ZN2v88internal15FunctionLiteral15NeedsHomeObjectEPNS0_10ExpressionE, .-_ZN2v88internal15FunctionLiteral15NeedsHomeObjectEPNS0_10ExpressionE
	.section	.text._ZNK2v88internal15FunctionLiteral29requires_brand_initializationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15FunctionLiteral29requires_brand_initializationEv
	.type	_ZNK2v88internal15FunctionLiteral29requires_brand_initializationEv, @function
_ZNK2v88internal15FunctionLiteral29requires_brand_initializationEv:
.LFB19426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	subq	$8, %rsp
	movq	40(%rdi), %rax
	movq	8(%rax), %rdi
	cmpb	$0, 128(%rdi)
	je	.L271
.L265:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	call	_ZN2v88internal5Scope12AsClassScopeEv@PLT
	movq	136(%rax), %rax
	testq	%rax, %rax
	je	.L265
	cmpq	$0, 40(%rax)
	setne	%r12b
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19426:
	.size	_ZNK2v88internal15FunctionLiteral29requires_brand_initializationEv, .-_ZNK2v88internal15FunctionLiteral29requires_brand_initializationEv
	.section	.text._ZN2v88internal21ObjectLiteralPropertyC2EPNS0_10ExpressionES3_NS1_4KindEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21ObjectLiteralPropertyC2EPNS0_10ExpressionES3_NS1_4KindEb
	.type	_ZN2v88internal21ObjectLiteralPropertyC2EPNS0_10ExpressionES3_NS1_4KindEb, @function
_ZN2v88internal21ObjectLiteralPropertyC2EPNS0_10ExpressionES3_NS1_4KindEb:
.LFB19428:
	.cfi_startproc
	endbr64
	movzbl	%r8b, %r8d
	movq	%rdx, 8(%rdi)
	orq	%r8, %rsi
	movb	%cl, 16(%rdi)
	movq	%rsi, (%rdi)
	movb	$1, 17(%rdi)
	ret
	.cfi_endproc
.LFE19428:
	.size	_ZN2v88internal21ObjectLiteralPropertyC2EPNS0_10ExpressionES3_NS1_4KindEb, .-_ZN2v88internal21ObjectLiteralPropertyC2EPNS0_10ExpressionES3_NS1_4KindEb
	.globl	_ZN2v88internal21ObjectLiteralPropertyC1EPNS0_10ExpressionES3_NS1_4KindEb
	.set	_ZN2v88internal21ObjectLiteralPropertyC1EPNS0_10ExpressionES3_NS1_4KindEb,_ZN2v88internal21ObjectLiteralPropertyC2EPNS0_10ExpressionES3_NS1_4KindEb
	.section	.text.unlikely._ZN2v88internal21ObjectLiteralPropertyC2EPNS0_15AstValueFactoryEPNS0_10ExpressionES5_b,"ax",@progbits
	.align 2
.LCOLDB2:
	.section	.text._ZN2v88internal21ObjectLiteralPropertyC2EPNS0_15AstValueFactoryEPNS0_10ExpressionES5_b,"ax",@progbits
.LHOTB2:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21ObjectLiteralPropertyC2EPNS0_15AstValueFactoryEPNS0_10ExpressionES5_b
	.type	_ZN2v88internal21ObjectLiteralPropertyC2EPNS0_15AstValueFactoryEPNS0_10ExpressionES5_b, @function
_ZN2v88internal21ObjectLiteralPropertyC2EPNS0_15AstValueFactoryEPNS0_10ExpressionES5_b:
.LFB19431:
	.cfi_startproc
	endbr64
	movzbl	%r8b, %eax
	movq	%rcx, 8(%rdi)
	orq	%rdx, %rax
	movb	$1, 17(%rdi)
	movq	%rax, (%rdi)
	testb	%r8b, %r8b
	jne	.L274
	movl	4(%rdx), %eax
	movl	%eax, %r8d
	andl	$63, %r8d
	cmpb	$41, %r8b
	jne	.L275
	shrl	$7, %eax
	andl	$15, %eax
	cmpl	$3, %eax
	je	.L279
.L274:
	movl	4(%rcx), %eax
	andl	$63, %eax
	leal	-21(%rax), %edx
	cmpb	$2, %dl
	ja	.L277
	movb	$2, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	cmpb	$41, %al
	setne	16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	movq	56(%rsi), %rax
	movq	408(%rax), %rax
	cmpq	%rax, 8(%rdx)
	jne	.L274
	movb	$5, 16(%rdi)
	ret
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal21ObjectLiteralPropertyC2EPNS0_15AstValueFactoryEPNS0_10ExpressionES5_b
	.cfi_startproc
	.type	_ZN2v88internal21ObjectLiteralPropertyC2EPNS0_15AstValueFactoryEPNS0_10ExpressionES5_b.cold, @function
_ZN2v88internal21ObjectLiteralPropertyC2EPNS0_15AstValueFactoryEPNS0_10ExpressionES5_b.cold:
.LFSB19431:
.L275:
	movl	4, %eax
	ud2
	.cfi_endproc
.LFE19431:
	.section	.text._ZN2v88internal21ObjectLiteralPropertyC2EPNS0_15AstValueFactoryEPNS0_10ExpressionES5_b
	.size	_ZN2v88internal21ObjectLiteralPropertyC2EPNS0_15AstValueFactoryEPNS0_10ExpressionES5_b, .-_ZN2v88internal21ObjectLiteralPropertyC2EPNS0_15AstValueFactoryEPNS0_10ExpressionES5_b
	.section	.text.unlikely._ZN2v88internal21ObjectLiteralPropertyC2EPNS0_15AstValueFactoryEPNS0_10ExpressionES5_b
	.size	_ZN2v88internal21ObjectLiteralPropertyC2EPNS0_15AstValueFactoryEPNS0_10ExpressionES5_b.cold, .-_ZN2v88internal21ObjectLiteralPropertyC2EPNS0_15AstValueFactoryEPNS0_10ExpressionES5_b.cold
.LCOLDE2:
	.section	.text._ZN2v88internal21ObjectLiteralPropertyC2EPNS0_15AstValueFactoryEPNS0_10ExpressionES5_b
.LHOTE2:
	.globl	_ZN2v88internal21ObjectLiteralPropertyC1EPNS0_15AstValueFactoryEPNS0_10ExpressionES5_b
	.set	_ZN2v88internal21ObjectLiteralPropertyC1EPNS0_15AstValueFactoryEPNS0_10ExpressionES5_b,_ZN2v88internal21ObjectLiteralPropertyC2EPNS0_15AstValueFactoryEPNS0_10ExpressionES5_b
	.section	.text._ZNK2v88internal15LiteralProperty20NeedsSetFunctionNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15LiteralProperty20NeedsSetFunctionNameEv
	.type	_ZNK2v88internal15LiteralProperty20NeedsSetFunctionNameEv, @function
_ZNK2v88internal15LiteralProperty20NeedsSetFunctionNameEv:
.LFB19433:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testb	$3, (%rdi)
	je	.L280
	movq	8(%rdi), %rsi
	movl	4(%rsi), %edx
	movl	%edx, %ecx
	andl	$63, %ecx
	cmpb	$38, %cl
	je	.L294
	shrl	$9, %edx
	andl	$1, %edx
	cmpb	$31, %cl
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	andl	$896, %edx
	movl	$1, %eax
	je	.L280
	movq	40(%rsi), %rax
	movzbl	133(%rax), %ecx
	xorl	%eax, %eax
	cmpb	$17, %cl
	jbe	.L293
.L280:
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	movl	$235712, %eax
	shrq	%cl, %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE19433:
	.size	_ZNK2v88internal15LiteralProperty20NeedsSetFunctionNameEv, .-_ZNK2v88internal15LiteralProperty20NeedsSetFunctionNameEv
	.section	.text._ZN2v88internal20ClassLiteralPropertyC2EPNS0_10ExpressionES3_NS1_4KindEbbb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20ClassLiteralPropertyC2EPNS0_10ExpressionES3_NS1_4KindEbbb
	.type	_ZN2v88internal20ClassLiteralPropertyC2EPNS0_10ExpressionES3_NS1_4KindEbbb, @function
_ZN2v88internal20ClassLiteralPropertyC2EPNS0_10ExpressionES3_NS1_4KindEbbb:
.LFB19435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%r9b, %r9d
	orq	%r9, %rsi
	movq	%rdx, 8(%rdi)
	movq	%rsi, (%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movb	%cl, 16(%rdi)
	movl	16(%rbp), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movb	%r8b, 17(%rdi)
	movq	$0, 24(%rdi)
	movb	%al, 18(%rdi)
	ret
	.cfi_endproc
.LFE19435:
	.size	_ZN2v88internal20ClassLiteralPropertyC2EPNS0_10ExpressionES3_NS1_4KindEbbb, .-_ZN2v88internal20ClassLiteralPropertyC2EPNS0_10ExpressionES3_NS1_4KindEbbb
	.globl	_ZN2v88internal20ClassLiteralPropertyC1EPNS0_10ExpressionES3_NS1_4KindEbbb
	.set	_ZN2v88internal20ClassLiteralPropertyC1EPNS0_10ExpressionES3_NS1_4KindEbbb,_ZN2v88internal20ClassLiteralPropertyC2EPNS0_10ExpressionES3_NS1_4KindEbbb
	.section	.text._ZNK2v88internal21ObjectLiteralProperty18IsCompileTimeValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal21ObjectLiteralProperty18IsCompileTimeValueEv
	.type	_ZNK2v88internal21ObjectLiteralProperty18IsCompileTimeValueEv, @function
_ZNK2v88internal21ObjectLiteralProperty18IsCompileTimeValueEv:
.LFB19437:
	.cfi_startproc
	endbr64
	movzbl	16(%rdi), %edx
	movl	$1, %eax
	testb	%dl, %dl
	je	.L297
	xorl	%eax, %eax
	cmpb	$2, %dl
	je	.L308
.L297:
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	movq	8(%rdi), %rax
	movl	4(%rax), %edx
	movl	$1, %eax
	movl	%edx, %ecx
	andl	$63, %ecx
	cmpb	$41, %cl
	je	.L297
	leal	-21(%rcx), %esi
	xorl	%eax, %eax
	cmpb	$2, %sil
	ja	.L297
	cmpb	$23, %cl
	je	.L309
	shrl	$8, %edx
	andl	$1, %edx
	cmpb	$22, %cl
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	movl	%edx, %eax
	shrl	$8, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE19437:
	.size	_ZNK2v88internal21ObjectLiteralProperty18IsCompileTimeValueEv, .-_ZNK2v88internal21ObjectLiteralProperty18IsCompileTimeValueEv
	.section	.text._ZN2v88internal21ObjectLiteralProperty14set_emit_storeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21ObjectLiteralProperty14set_emit_storeEb
	.type	_ZN2v88internal21ObjectLiteralProperty14set_emit_storeEb, @function
_ZN2v88internal21ObjectLiteralProperty14set_emit_storeEb:
.LFB19438:
	.cfi_startproc
	endbr64
	movb	%sil, 17(%rdi)
	ret
	.cfi_endproc
.LFE19438:
	.size	_ZN2v88internal21ObjectLiteralProperty14set_emit_storeEb, .-_ZN2v88internal21ObjectLiteralProperty14set_emit_storeEb
	.section	.text._ZNK2v88internal21ObjectLiteralProperty10emit_storeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal21ObjectLiteralProperty10emit_storeEv
	.type	_ZNK2v88internal21ObjectLiteralProperty10emit_storeEv, @function
_ZNK2v88internal21ObjectLiteralProperty10emit_storeEv:
.LFB19439:
	.cfi_startproc
	endbr64
	movzbl	17(%rdi), %eax
	ret
	.cfi_endproc
.LFE19439:
	.size	_ZNK2v88internal21ObjectLiteralProperty10emit_storeEv, .-_ZNK2v88internal21ObjectLiteralProperty10emit_storeEv
	.section	.text._ZN2v88internal13ObjectLiteral32InitFlagsForPendingNullPrototypeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ObjectLiteral32InitFlagsForPendingNullPrototypeEi
	.type	_ZN2v88internal13ObjectLiteral32InitFlagsForPendingNullPrototypeEi, @function
_ZN2v88internal13ObjectLiteral32InitFlagsForPendingNullPrototypeEi:
.LFB19444:
	.cfi_startproc
	endbr64
	movl	36(%rdi), %edx
	cmpl	%edx, %esi
	jge	.L312
	subl	$1, %edx
	movq	24(%rdi), %rcx
	movslq	%esi, %r8
	subl	%esi, %edx
	addq	%r8, %rdx
	leaq	(%rcx,%r8,8), %rax
	leaq	8(%rcx,%rdx,8), %rcx
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L314:
	addq	$8, %rax
	cmpq	%rcx, %rax
	je	.L312
.L315:
	movq	(%rax), %rdx
	cmpb	$5, 16(%rdx)
	jne	.L314
	movq	8(%rdx), %rdx
	movl	4(%rdx), %edx
	movl	%edx, %esi
	andl	$63, %esi
	cmpb	$41, %sil
	jne	.L314
	shrl	$7, %edx
	andl	$15, %edx
	cmpl	$7, %edx
	jne	.L314
	orl	$4096, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	ret
	.cfi_endproc
.LFE19444:
	.size	_ZN2v88internal13ObjectLiteral32InitFlagsForPendingNullPrototypeEi, .-_ZN2v88internal13ObjectLiteral32InitFlagsForPendingNullPrototypeEi
	.section	.text._ZN2v88internal13ObjectLiteral17InitDepthAndFlagsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ObjectLiteral17InitDepthAndFlagsEv
	.type	_ZN2v88internal13ObjectLiteral17InitDepthAndFlagsEv, @function
_ZN2v88internal13ObjectLiteral17InitDepthAndFlagsEv:
.LFB19445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	8(%rdi), %eax
	leal	(%rax,%rax), %r9d
	movl	%r9d, %r14d
	sarl	%r14d
	testl	%r14d, %r14d
	jle	.L368
.L317:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L369
	addq	$56, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movl	36(%rdi), %ecx
	movq	%rdi, %r15
	testl	%ecx, %ecx
	jle	.L319
	xorl	%r12d, %r12d
	movl	$1, %r14d
	xorl	%r13d, %r13d
	movb	$0, -69(%rbp)
	movl	$1, %r8d
	movl	%r14d, %r10d
	xorl	%ebx, %ebx
	movl	%r12d, %r14d
	movl	$0, -76(%rbp)
	movl	%r13d, %r9d
	movq	%rdi, %r12
	movl	%r8d, %r15d
	movl	$0, -68(%rbp)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L320:
	cmpl	%r9d, 12(%r12)
	je	.L370
	movq	8(%r13), %rsi
	movl	4(%rsi), %ecx
	movl	%ecx, %edx
	andl	$63, %edx
	cmpb	$22, %dl
	je	.L327
	cmpb	$23, %dl
	je	.L328
	movl	$2, %eax
	cmpb	$21, %dl
	je	.L330
.L329:
	movq	0(%r13), %rsi
	andq	$-4, %rsi
	movl	4(%rsi), %eax
	movl	%eax, %edi
	andl	$63, %edi
	cmpb	$41, %dil
	je	.L335
	movl	4, %eax
	xorl	%esi, %esi
.L335:
	cmpb	$41, %dl
	je	.L336
	leal	-21(%rdx), %edi
	cmpb	$2, %dil
	ja	.L354
	cmpb	$23, %dl
	je	.L371
	shrl	$8, %ecx
	movl	%r15d, %r8d
	andl	%ecx, %r8d
	cmpb	$22, %dl
	movl	$0, %ecx
	cmove	%r8d, %ecx
	movl	%ecx, %r15d
.L336:
	shrl	$7, %eax
	movl	$0, -60(%rbp)
	andl	$15, %eax
	cmpl	$1, %eax
	je	.L338
	cmpl	$3, %eax
	je	.L339
	testl	%eax, %eax
	je	.L372
.L341:
	movl	36(%r12), %ecx
	addq	$1, %rbx
	addl	$1, %r9d
	cmpl	%ebx, %ecx
	jle	.L373
.L348:
	movq	24(%r12), %rax
	movq	(%rax,%rbx,8), %r13
	cmpb	$5, 16(%r13)
	jne	.L320
	movq	8(%r13), %rax
	movl	4(%rax), %eax
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$41, %dl
	je	.L374
.L353:
	xorl	%r15d, %r15d
.L321:
	addq	$1, %rbx
	movb	$1, -69(%rbp)
	cmpl	%ebx, %ecx
	jg	.L348
.L373:
	movl	-76(%rbp), %ecx
	movzbl	%r15b, %eax
	leal	(%r10,%r10), %edx
	movq	%r12, %r15
	sall	$8, %eax
	movzbl	%r14b, %r12d
	sarl	%edx
	movl	4(%r15), %esi
	movl	%eax, %r8d
	sall	$7, %r12d
	xorl	%eax, %eax
	testl	%ecx, %ecx
	setne	%al
	movl	%r10d, %r14d
	sall	$9, %eax
	movl	%eax, %ecx
.L347:
	movl	%edx, %eax
	movl	8(%r15), %edx
	movl	-68(%rbp), %ebx
	andl	$2147483647, %eax
	andl	$-2147483648, %edx
	orl	%eax, %edx
	movl	%esi, %eax
	andl	$-385, %eax
	movl	%edx, 8(%r15)
	movl	$2048, %edx
	orl	%r8d, %eax
	orl	%r12d, %eax
	andb	$-3, %ah
	orl	%ecx, %eax
	cmpl	$32, %ebx
	jbe	.L349
	movl	-76(%rbp), %edx
	addl	%edx, %edx
	cmpl	%ebx, %edx
	setnb	%dl
	movzbl	%dl, %edx
	sall	$11, %edx
.L349:
	andb	$-9, %ah
	orl	%edx, %eax
	movl	%eax, 4(%r15)
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L328:
	movl	8(%rsi), %eax
	addl	%eax, %eax
	sarl	%eax
	testl	%eax, %eax
	jle	.L331
	addl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L330:
	cmpl	%eax, %r10d
	cmovl	%eax, %r10d
	movl	4(%rsi), %eax
	movl	%eax, %esi
	andl	$63, %esi
	cmpb	$23, %sil
	je	.L375
	shrb	$7, %al
	movl	%r14d, %edi
	orl	%r14d, %eax
	cmpb	$22, %sil
	cmove	%eax, %edi
	movl	%edi, %r14d
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L375:
	shrb	$7, %al
	orl	%eax, %r14d
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L372:
	movl	8(%rsi), %eax
	testl	%eax, %eax
	js	.L341
.L343:
	movl	-68(%rbp), %ecx
	cmpl	%eax, %ecx
	cmovnb	%ecx, %eax
	addl	$1, -76(%rbp)
	movl	%eax, -68(%rbp)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L339:
	movq	8(%rsi), %rdi
	leaq	-60(%rbp), %r13
	movl	%r10d, -80(%rbp)
	movq	%r13, %rsi
	movl	%r9d, -88(%rbp)
	call	_ZNK2v88internal12AstRawString12AsArrayIndexEPj@PLT
	movl	-88(%rbp), %r9d
	movl	-80(%rbp), %r10d
.L342:
	testb	%al, %al
	je	.L341
	movl	-60(%rbp), %eax
	cmpl	$-1, %eax
	je	.L341
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L327:
	movq	%rsi, %rdi
	movl	%r10d, -92(%rbp)
	movl	%r9d, -80(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal13ObjectLiteral17InitDepthAndFlagsEv
	movq	8(%r13), %rdx
	movq	-88(%rbp), %rsi
	movl	-80(%rbp), %r9d
	movl	-92(%rbp), %r10d
	addl	$1, %eax
	movl	4(%rdx), %ecx
	movl	%ecx, %edx
	andl	$63, %edx
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L338:
	movsd	8(%rsi), %xmm1
	movsd	.LC3(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L341
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movl	$0, %edx
	movd	%xmm2, -60(%rbp)
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm1, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L371:
	shrl	$8, %ecx
	andl	%ecx, %r15d
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L354:
	xorl	%r15d, %r15d
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L374:
	shrl	$7, %eax
	andl	$15, %eax
	cmpl	$7, %eax
	jne	.L353
	orl	$4096, 4(%r12)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L331:
	movq	%rsi, %rdi
	movl	%r10d, -92(%rbp)
	movl	%r9d, -80(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal12ArrayLiteral17InitDepthAndFlagsEv.part.0
	movq	8(%r13), %rdx
	movl	-92(%rbp), %r10d
	movl	-80(%rbp), %r9d
	movq	-88(%rbp), %rsi
	addl	$1, %eax
	movl	4(%rdx), %ecx
	movl	%ecx, %edx
	andl	$63, %edx
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L370:
	movq	%r12, %r15
	cmpb	$0, -69(%rbp)
	movzbl	%r14b, %r12d
	movl	%r10d, %r14d
	movl	4(%r15), %esi
	je	.L376
.L323:
	movl	-76(%rbp), %edi
	leal	(%r14,%r14), %edx
	xorl	%eax, %eax
	sall	$7, %r12d
	sarl	%edx
	testl	%edi, %edi
	setne	%al
	xorl	%r8d, %r8d
	sall	$9, %eax
	movl	%eax, %ecx
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L376:
	cmpl	%ecx, %ebx
	jge	.L323
	subl	$1, %ecx
	movslq	%ebx, %rdx
	subl	%ebx, %ecx
	leaq	(%rax,%rdx,8), %rdi
	addq	%rcx, %rdx
	leaq	(%rax,%rdx,8), %rdx
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L325:
	addq	$8, %rdi
.L324:
	cmpq	%rdi, %rdx
	je	.L323
	movq	8(%rdi), %rax
	cmpb	$5, 16(%rax)
	jne	.L325
	movq	8(%rax), %rax
	movl	4(%rax), %eax
	movl	%eax, %ecx
	andl	$63, %ecx
	cmpb	$41, %cl
	jne	.L325
	shrl	$7, %eax
	andl	$15, %eax
	cmpl	$7, %eax
	jne	.L325
	orl	$4096, %esi
	jmp	.L323
.L319:
	andl	$-2147483648, %eax
	movl	$1, %r14d
	movl	$2048, %edx
	orl	$1, %eax
	movl	%eax, 8(%rdi)
	movl	4(%rdi), %eax
	andl	$-897, %eax
	orb	$1, %ah
	jmp	.L349
.L369:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19445:
	.size	_ZN2v88internal13ObjectLiteral17InitDepthAndFlagsEv, .-_ZN2v88internal13ObjectLiteral17InitDepthAndFlagsEv
	.section	.text._ZN2v88internal12ArrayLiteral17InitDepthAndFlagsEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12ArrayLiteral17InitDepthAndFlagsEv.part.0, @function
_ZN2v88internal12ArrayLiteral17InitDepthAndFlagsEv.part.0:
.LFB24085:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	12(%rdi), %r15d
	movl	%r15d, %eax
	testl	%r15d, %r15d
	jns	.L378
	movl	36(%rdi), %eax
.L378:
	shrl	$31, %r15d
	testl	%eax, %eax
	jle	.L392
	subl	$1, %eax
	xorl	%r12d, %r12d
	movl	$1, %r14d
	leaq	8(,%rax,8), %r8
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L407:
	cmpb	$21, %dl
	je	.L404
.L382:
	cmpb	$41, %dl
	jne	.L405
.L386:
	addq	$8, %r12
	cmpq	%r12, %r8
	je	.L406
.L389:
	movq	24(%rbx), %rax
	movq	(%rax,%r12), %r13
	movl	4(%r13), %ecx
	movl	%ecx, %edx
	andl	$63, %edx
	cmpb	$22, %dl
	je	.L380
	cmpb	$23, %dl
	jne	.L407
	movl	8(%r13), %eax
	addl	%eax, %eax
	sarl	%eax
	testl	%eax, %eax
	jle	.L385
	addl	$1, %eax
	cmpl	%eax, %r14d
	cmovl	%eax, %r14d
.L403:
	shrl	$8, %ecx
	movl	$0, %eax
	andl	$1, %ecx
	testb	%cl, %cl
	cmove	%eax, %r15d
	addq	$8, %r12
	cmpq	%r12, %r8
	jne	.L389
.L406:
	leal	(%r14,%r14), %eax
	sarl	%eax
.L379:
	andl	$2147483647, %eax
	movzbl	%r15b, %r15d
	movl	%eax, %edx
	movl	8(%rbx), %eax
	sall	$8, %r15d
	andl	$-2147483648, %eax
	orl	%edx, %eax
	movl	%eax, 8(%rbx)
	movl	4(%rbx), %eax
	andl	$-385, %eax
	orl	%eax, %r15d
	movl	%r14d, %eax
	orb	$-128, %r15b
	movl	%r15d, 4(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L404:
	.cfi_restore_state
	cmpl	$2, %r14d
	movl	$2, %eax
	cmovl	%eax, %r14d
.L394:
	xorl	%r15d, %r15d
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L405:
	leal	-21(%rdx), %eax
	cmpb	$2, %al
	ja	.L394
	cmpb	$23, %dl
	je	.L403
	cmpb	$22, %dl
	jne	.L394
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L380:
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal13ObjectLiteral17InitDepthAndFlagsEv
.L402:
	movl	4(%r13), %ecx
	addl	$1, %eax
	movq	-56(%rbp), %r8
	movl	%ecx, %edx
	andl	$63, %edx
	cmpl	%eax, %r14d
	cmovl	%eax, %r14d
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L385:
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal12ArrayLiteral17InitDepthAndFlagsEv.part.0
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L392:
	movl	$1, %eax
	movl	$1, %r14d
	jmp	.L379
	.cfi_endproc
.LFE24085:
	.size	_ZN2v88internal12ArrayLiteral17InitDepthAndFlagsEv.part.0, .-_ZN2v88internal12ArrayLiteral17InitDepthAndFlagsEv.part.0
	.section	.text._ZNK2v88internal13ObjectLiteral22IsFastCloningSupportedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13ObjectLiteral22IsFastCloningSupportedEv
	.type	_ZNK2v88internal13ObjectLiteral22IsFastCloningSupportedEv, @function
_ZNK2v88internal13ObjectLiteral22IsFastCloningSupportedEv:
.LFB19447:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testb	$8, 5(%rdi)
	je	.L408
	movl	8(%rdi), %ecx
	leal	(%rcx,%rcx), %edx
	cmpl	$2, %edx
	je	.L413
.L408:
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	cmpl	$2730, 12(%rdi)
	setle	%al
	ret
	.cfi_endproc
.LFE19447:
	.size	_ZNK2v88internal13ObjectLiteral22IsFastCloningSupportedEv, .-_ZNK2v88internal13ObjectLiteral22IsFastCloningSupportedEv
	.section	.text._ZN2v88internal12ArrayLiteral17InitDepthAndFlagsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12ArrayLiteral17InitDepthAndFlagsEv
	.type	_ZN2v88internal12ArrayLiteral17InitDepthAndFlagsEv, @function
_ZN2v88internal12ArrayLiteral17InitDepthAndFlagsEv:
.LFB19448:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	addl	%eax, %eax
	sarl	%eax
	testl	%eax, %eax
	jle	.L418
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	jmp	_ZN2v88internal12ArrayLiteral17InitDepthAndFlagsEv.part.0
	.cfi_endproc
.LFE19448:
	.size	_ZN2v88internal12ArrayLiteral17InitDepthAndFlagsEv, .-_ZN2v88internal12ArrayLiteral17InitDepthAndFlagsEv
	.section	.text._ZNK2v88internal12ArrayLiteral22IsFastCloningSupportedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12ArrayLiteral22IsFastCloningSupportedEv
	.type	_ZNK2v88internal12ArrayLiteral22IsFastCloningSupportedEv, @function
_ZNK2v88internal12ArrayLiteral22IsFastCloningSupportedEv:
.LFB19450:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	xorl	%r8d, %r8d
	addl	%eax, %eax
	cmpl	$2, %eax
	jg	.L419
	cmpl	$16376, 36(%rdi)
	setle	%r8b
.L419:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE19450:
	.size	_ZNK2v88internal12ArrayLiteral22IsFastCloningSupportedEv, .-_ZNK2v88internal12ArrayLiteral22IsFastCloningSupportedEv
	.section	.text._ZNK2v88internal19MaterializedLiteral8IsSimpleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal19MaterializedLiteral8IsSimpleEv
	.type	_ZNK2v88internal19MaterializedLiteral8IsSimpleEv, @function
_ZNK2v88internal19MaterializedLiteral8IsSimpleEv:
.LFB19451:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	movl	%eax, %edx
	shrl	$8, %eax
	andl	$63, %edx
	andl	$1, %eax
	cmpb	$23, %dl
	je	.L426
	cmpb	$22, %dl
	movl	$0, %edx
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	ret
	.cfi_endproc
.LFE19451:
	.size	_ZNK2v88internal19MaterializedLiteral8IsSimpleEv, .-_ZNK2v88internal19MaterializedLiteral8IsSimpleEv
	.section	.text._ZN2v88internal19MaterializedLiteral17InitDepthAndFlagsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19MaterializedLiteral17InitDepthAndFlagsEv
	.type	_ZN2v88internal19MaterializedLiteral17InitDepthAndFlagsEv, @function
_ZN2v88internal19MaterializedLiteral17InitDepthAndFlagsEv:
.LFB19453:
	.cfi_startproc
	endbr64
	movzbl	4(%rdi), %eax
	andl	$63, %eax
	cmpb	$23, %al
	je	.L432
	movl	$1, %r8d
	cmpb	$22, %al
	je	.L433
.L427:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	movl	8(%rdi), %eax
	leal	(%rax,%rax), %r8d
	sarl	%r8d
	testl	%r8d, %r8d
	jg	.L427
	jmp	_ZN2v88internal12ArrayLiteral17InitDepthAndFlagsEv.part.0
	.p2align 4,,10
	.p2align 3
.L433:
	jmp	_ZN2v88internal13ObjectLiteral17InitDepthAndFlagsEv
	.cfi_endproc
.LFE19453:
	.size	_ZN2v88internal19MaterializedLiteral17InitDepthAndFlagsEv, .-_ZN2v88internal19MaterializedLiteral17InitDepthAndFlagsEv
	.section	.text._ZN2v88internal19MaterializedLiteral26NeedsInitialAllocationSiteEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19MaterializedLiteral26NeedsInitialAllocationSiteEv
	.type	_ZN2v88internal19MaterializedLiteral26NeedsInitialAllocationSiteEv, @function
_ZN2v88internal19MaterializedLiteral26NeedsInitialAllocationSiteEv:
.LFB19454:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	movl	%eax, %edx
	shrb	$7, %al
	andl	$63, %edx
	cmpb	$23, %dl
	je	.L438
	cmpb	$22, %dl
	movl	$0, %edx
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	ret
	.cfi_endproc
.LFE19454:
	.size	_ZN2v88internal19MaterializedLiteral26NeedsInitialAllocationSiteEv, .-_ZN2v88internal19MaterializedLiteral26NeedsInitialAllocationSiteEv
	.section	.text._ZN2v88internal17GetTemplateObject21GetOrBuildDescriptionEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17GetTemplateObject21GetOrBuildDescriptionEPNS0_7IsolateE
	.type	_ZN2v88internal17GetTemplateObject21GetOrBuildDescriptionEPNS0_7IsolateE, @function
_ZN2v88internal17GetTemplateObject21GetOrBuildDescriptionEPNS0_7IsolateE:
.LFB19456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rsi, %rdi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%r15), %rax
	movq	%rsi, -88(%rbp)
	movl	12(%rax), %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %rdi
	movq	%rax, -80(%rbp)
	movq	%rax, %r14
	movl	11(%rdi), %edx
	testl	%edx, %edx
	jle	.L440
	xorl	%ebx, %ebx
	movl	$1, %r13d
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L486:
	movq	(%rax), %rax
	cmpq	%r12, (%rax)
	movl	$0, %eax
	cmovne	%eax, %r13d
.L441:
	leaq	15(%rdi,%rdx), %rsi
	movq	%r12, (%rsi)
	testb	$1, %r12b
	je	.L455
	movq	%r12, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rdx
	movq	%rax, -56(%rbp)
	testl	$262144, %edx
	je	.L443
	movq	%r12, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rax
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rax), %rdx
.L443:
	andl	$24, %edx
	je	.L455
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L455
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L455:
	movq	(%r14), %rdi
	addq	$1, %rbx
	cmpl	%ebx, 11(%rdi)
	jle	.L485
.L445:
	movq	16(%r15), %rsi
	movq	8(%r15), %rax
	leaq	0(,%rbx,8), %rdx
	movq	(%rsi), %rsi
	movq	(%rax), %rax
	movq	(%rsi,%rbx,8), %rsi
	movq	(%rax,%rbx,8), %rax
	movq	(%rsi), %rsi
	movq	(%rsi), %r12
	testq	%rax, %rax
	jne	.L486
	xorl	%r13d, %r13d
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L485:
	testb	%r13b, %r13b
	je	.L487
.L440:
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rdi
	addq	$56, %rsp
	movq	%r14, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory28NewTemplateObjectDescriptionENS0_6HandleINS0_10FixedArrayEEES4_@PLT
	.p2align 4,,10
	.p2align 3
.L487:
	.cfi_restore_state
	movq	8(%r15), %rax
	movq	-88(%rbp), %rdi
	movl	$1, %edx
	movl	12(%rax), %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r12
	movq	%rax, -80(%rbp)
	movl	11(%r12), %eax
	testl	%eax, %eax
	jle	.L440
	xorl	%ebx, %ebx
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L488:
	movq	(%rax), %rax
	movq	(%rax), %r13
	movq	%r13, (%rsi)
	testb	$1, %r13b
	je	.L450
.L484:
	movq	%r13, %r9
	andq	$-262144, %r9
	movq	8(%r9), %rax
	movq	%r9, -56(%rbp)
	testl	$262144, %eax
	je	.L452
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rsi
	movq	8(%r9), %rax
.L452:
	testb	$24, %al
	je	.L450
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L450
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L450:
	movq	-80(%rbp), %rax
	addq	$1, %rbx
	movq	(%rax), %r12
	cmpl	%ebx, 11(%r12)
	jle	.L440
.L454:
	movq	8(%r15), %rax
	leaq	15(%r12,%rbx,8), %rsi
	movq	(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	testq	%rax, %rax
	jne	.L488
	movq	-88(%rbp), %rax
	movq	88(%rax), %r13
	movq	%r13, (%rsi)
	testb	$1, %r13b
	jne	.L484
	jmp	.L450
	.cfi_endproc
.LFE19456:
	.size	_ZN2v88internal17GetTemplateObject21GetOrBuildDescriptionEPNS0_7IsolateE, .-_ZN2v88internal17GetTemplateObject21GetOrBuildDescriptionEPNS0_7IsolateE
	.section	.text.unlikely._ZN2v88internal15BinaryOperation21IsSmiLiteralOperationEPPNS0_10ExpressionEPNS0_3SmiE,"ax",@progbits
	.align 2
.LCOLDB4:
	.section	.text._ZN2v88internal15BinaryOperation21IsSmiLiteralOperationEPPNS0_10ExpressionEPNS0_3SmiE,"ax",@progbits
.LHOTB4:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15BinaryOperation21IsSmiLiteralOperationEPPNS0_10ExpressionEPNS0_3SmiE
	.type	_ZN2v88internal15BinaryOperation21IsSmiLiteralOperationEPPNS0_10ExpressionEPNS0_3SmiE, @function
_ZN2v88internal15BinaryOperation21IsSmiLiteralOperationEPPNS0_10ExpressionEPNS0_3SmiE:
.LFB19459:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %r8
	movq	8(%rdi), %r10
	movl	4(%r8), %eax
	movl	%eax, %ecx
	andl	$63, %ecx
	cmpb	$41, %cl
	je	.L502
.L490:
	movl	4(%rdi), %eax
	shrl	$7, %eax
	andl	$127, %eax
	cmpb	$40, %al
	jne	.L503
.L493:
	movl	4(%r10), %eax
	xorl	%r9d, %r9d
	movl	%eax, %ecx
	andl	$63, %ecx
	cmpb	$41, %cl
	je	.L504
.L489:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	subl	$34, %eax
	xorl	%r9d, %r9d
	cmpb	$2, %al
	jbe	.L493
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L502:
	testl	$1920, %eax
	jne	.L490
	movq	%r10, (%rsi)
	movzbl	4(%r8), %eax
	andl	$63, %eax
	cmpb	$41, %al
	jne	.L491
	movslq	8(%r8), %rax
	movl	$1, %r9d
	salq	$32, %rax
	movq	%rax, (%rdx)
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L504:
	testl	$1920, %eax
	jne	.L489
	movq	%r8, (%rsi)
	movzbl	4(%r10), %eax
	andl	$63, %eax
	cmpb	$41, %al
	jne	.L494
	movslq	8(%r10), %rax
	movl	$1, %r9d
	salq	$32, %rax
	movq	%rax, (%rdx)
	jmp	.L489
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal15BinaryOperation21IsSmiLiteralOperationEPPNS0_10ExpressionEPNS0_3SmiE
	.cfi_startproc
	.type	_ZN2v88internal15BinaryOperation21IsSmiLiteralOperationEPPNS0_10ExpressionEPNS0_3SmiE.cold, @function
_ZN2v88internal15BinaryOperation21IsSmiLiteralOperationEPPNS0_10ExpressionEPNS0_3SmiE.cold:
.LFSB19459:
.L491:
	movl	8, %eax
	ud2
.L494:
	movl	8, %eax
	ud2
	.cfi_endproc
.LFE19459:
	.section	.text._ZN2v88internal15BinaryOperation21IsSmiLiteralOperationEPPNS0_10ExpressionEPNS0_3SmiE
	.size	_ZN2v88internal15BinaryOperation21IsSmiLiteralOperationEPPNS0_10ExpressionEPNS0_3SmiE, .-_ZN2v88internal15BinaryOperation21IsSmiLiteralOperationEPPNS0_10ExpressionEPNS0_3SmiE
	.section	.text.unlikely._ZN2v88internal15BinaryOperation21IsSmiLiteralOperationEPPNS0_10ExpressionEPNS0_3SmiE
	.size	_ZN2v88internal15BinaryOperation21IsSmiLiteralOperationEPPNS0_10ExpressionEPNS0_3SmiE.cold, .-_ZN2v88internal15BinaryOperation21IsSmiLiteralOperationEPPNS0_10ExpressionEPNS0_3SmiE.cold
.LCOLDE4:
	.section	.text._ZN2v88internal15BinaryOperation21IsSmiLiteralOperationEPPNS0_10ExpressionEPNS0_3SmiE
.LHOTE4:
	.section	.text._ZN2v88internal16CompareOperation22IsLiteralCompareTypeofEPPNS0_10ExpressionEPPNS0_7LiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CompareOperation22IsLiteralCompareTypeofEPPNS0_10ExpressionEPPNS0_7LiteralE
	.type	_ZN2v88internal16CompareOperation22IsLiteralCompareTypeofEPPNS0_10ExpressionEPPNS0_7LiteralE, @function
_ZN2v88internal16CompareOperation22IsLiteralCompareTypeofEPPNS0_10ExpressionEPPNS0_7LiteralE:
.LFB19462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %r9
	movl	4(%rdi), %eax
	movq	16(%rdi), %r11
	movl	4(%r9), %ebx
	shrl	$7, %eax
	movl	4(%r11), %r10d
	movl	%eax, %r8d
	movl	%ebx, %r12d
	andl	$63, %r12d
	andl	$127, %r8d
	testq	%r9, %r9
	movl	%r10d, %r13d
	setne	%dil
	cmpb	$53, %r12b
	sete	%cl
	movl	%edi, %eax
	andl	$63, %r13d
	andb	%cl, %al
	jne	.L524
.L506:
	xorl	%eax, %eax
	cmpb	$53, %r13b
	je	.L525
.L505:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	.cfi_restore_state
	movl	%ebx, %ecx
	shrl	$7, %ecx
	andl	$127, %ecx
	cmpb	$49, %cl
	jne	.L506
	cmpb	$41, %r13b
	jne	.L506
	shrl	$7, %r10d
	andl	$15, %r10d
	cmpl	$3, %r10d
	je	.L526
.L511:
	xorl	%eax, %eax
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L525:
	shrl	$7, %r10d
	andl	$127, %r10d
	cmpb	$49, %r10b
	jne	.L505
	cmpb	$41, %r12b
	jne	.L505
	shrl	$7, %ebx
	andl	$15, %ebx
	cmpl	$3, %ebx
	jne	.L505
	subl	$53, %r8d
	cmpb	$1, %r8b
	ja	.L505
	movq	8(%r11), %rax
	movq	%rax, (%rsi)
	movzbl	4(%r9), %eax
	andl	$63, %eax
	cmpb	$41, %al
	movl	$0, %eax
	cmove	%r9, %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L526:
	subl	$53, %r8d
	cmpb	$1, %r8b
	ja	.L511
	movq	8(%r9), %rcx
	movq	%rcx, (%rsi)
	movzbl	4(%r11), %ecx
	andl	$63, %ecx
	cmpb	$41, %cl
	movl	$0, %ecx
	cmovne	%rcx, %r11
	movq	%r11, (%rdx)
	jmp	.L505
	.cfi_endproc
.LFE19462:
	.size	_ZN2v88internal16CompareOperation22IsLiteralCompareTypeofEPPNS0_10ExpressionEPPNS0_7LiteralE, .-_ZN2v88internal16CompareOperation22IsLiteralCompareTypeofEPPNS0_10ExpressionEPPNS0_7LiteralE
	.section	.text._ZN2v88internal16CompareOperation25IsLiteralCompareUndefinedEPPNS0_10ExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CompareOperation25IsLiteralCompareUndefinedEPPNS0_10ExpressionE
	.type	_ZN2v88internal16CompareOperation25IsLiteralCompareUndefinedEPPNS0_10ExpressionE, @function
_ZN2v88internal16CompareOperation25IsLiteralCompareUndefinedEPPNS0_10ExpressionE:
.LFB19465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r14
	movl	4(%rdi), %ebx
	movq	16(%rdi), %r15
	movl	4(%r14), %eax
	shrl	$7, %ebx
	andl	$127, %ebx
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$53, %dl
	jne	.L528
	shrl	$7, %eax
	andl	$127, %eax
	cmpb	$50, %al
	je	.L565
.L529:
	movl	4(%r15), %eax
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$53, %dl
	jne	.L536
	.p2align 4,,10
	.p2align 3
.L566:
	shrl	$7, %eax
	andl	$127, %eax
	cmpb	$50, %al
	je	.L537
.L538:
	xorl	%eax, %eax
.L527:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore_state
	movq	%rdi, %r12
	cmpb	$41, %dl
	jne	.L531
	shrl	$7, %eax
	andl	$15, %eax
	cmpl	$6, %eax
	jne	.L529
.L532:
	subl	$53, %ebx
	cmpb	$1, %bl
	jbe	.L535
.L562:
	movq	16(%r12), %r15
	movl	4(%r12), %ebx
	movq	8(%r12), %r14
	movl	4(%r15), %eax
	shrl	$7, %ebx
	andl	$127, %ebx
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$53, %dl
	je	.L566
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L531:
	cmpb	$54, %dl
	jne	.L529
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L529
	testw	$896, 40(%rdi)
	jne	.L529
	testb	$1, %ah
	je	.L533
	movq	8(%rdi), %rdi
.L533:
	leaq	.LC1(%rip), %rsi
	call	_ZNK2v88internal12AstRawString16IsOneByteEqualToEPKc@PLT
	testb	%al, %al
	jne	.L532
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L565:
	movq	8(%r14), %rax
	movl	4(%rax), %eax
	andl	$63, %eax
	cmpb	$41, %al
	jne	.L529
	leal	-53(%rbx), %eax
	cmpb	$1, %al
	ja	.L529
.L535:
	movq	%r15, 0(%r13)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L536:
	.cfi_restore_state
	cmpb	$41, %dl
	jne	.L540
	shrl	$7, %eax
	andl	$15, %eax
	cmpl	$6, %eax
	jne	.L538
.L541:
	subl	$53, %ebx
	cmpb	$1, %bl
	ja	.L538
	movq	%r14, 0(%r13)
	movl	$1, %eax
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L540:
	cmpb	$54, %dl
	jne	.L538
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L538
	testw	$896, 40(%rdi)
	jne	.L538
	testb	$1, %ah
	je	.L542
	movq	8(%rdi), %rdi
.L542:
	leaq	.LC1(%rip), %rsi
	call	_ZNK2v88internal12AstRawString16IsOneByteEqualToEPKc@PLT
	testb	%al, %al
	jne	.L541
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L537:
	movq	8(%r15), %rax
	movl	4(%rax), %eax
	andl	$63, %eax
	cmpb	$41, %al
	jne	.L538
	jmp	.L541
	.cfi_endproc
.LFE19465:
	.size	_ZN2v88internal16CompareOperation25IsLiteralCompareUndefinedEPPNS0_10ExpressionE, .-_ZN2v88internal16CompareOperation25IsLiteralCompareUndefinedEPPNS0_10ExpressionE
	.section	.text._ZN2v88internal16CompareOperation20IsLiteralCompareNullEPPNS0_10ExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16CompareOperation20IsLiteralCompareNullEPPNS0_10ExpressionE
	.type	_ZN2v88internal16CompareOperation20IsLiteralCompareNullEPPNS0_10ExpressionE, @function
_ZN2v88internal16CompareOperation20IsLiteralCompareNullEPPNS0_10ExpressionE:
.LFB19467:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	4(%rdi), %eax
	movq	16(%rdi), %r8
	movl	4(%rcx), %edx
	shrl	$7, %eax
	andl	$127, %eax
	movl	%edx, %edi
	andl	$63, %edi
	cmpb	$41, %dil
	je	.L573
.L568:
	movl	4(%r8), %edx
	xorl	%r8d, %r8d
	movl	%edx, %edi
	andl	$63, %edi
	cmpb	$41, %dil
	je	.L574
.L567:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L573:
	shrl	$7, %edx
	andl	$15, %edx
	cmpl	$7, %edx
	jne	.L568
	leal	-53(%rax), %edx
	cmpb	$1, %dl
	ja	.L568
	movq	%r8, (%rsi)
	movl	$1, %r8d
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L574:
	shrl	$7, %edx
	andl	$15, %edx
	cmpl	$7, %edx
	jne	.L567
	subl	$53, %eax
	cmpb	$1, %al
	ja	.L567
	movq	%rcx, (%rsi)
	movl	$1, %r8d
	jmp	.L567
	.cfi_endproc
.LFE19467:
	.size	_ZN2v88internal16CompareOperation20IsLiteralCompareNullEPPNS0_10ExpressionE, .-_ZN2v88internal16CompareOperation20IsLiteralCompareNullEPPNS0_10ExpressionE
	.section	.text._ZNK2v88internal4Call11GetCallTypeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4Call11GetCallTypeEv
	.type	_ZNK2v88internal4Call11GetCallTypeEv, @function
_ZNK2v88internal4Call11GetCallTypeEv:
.LFB19468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	4(%rbx), %eax
	andl	$63, %eax
	cmpb	$54, %al
	jne	.L576
	movl	$9, %r8d
	testq	%rbx, %rbx
	je	.L575
	movq	8(%rbx), %rax
	xorl	%r8d, %r8d
	movzwl	40(%rax), %edx
	movl	%edx, %eax
	shrl	$7, %eax
	andl	$7, %eax
	testb	%al, %al
	je	.L575
	movl	$9, %r8d
	cmpb	$4, %al
	je	.L608
	.p2align 4,,10
	.p2align 3
.L575:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L609
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L576:
	.cfi_restore_state
	movl	$7, %r8d
	cmpb	$48, %al
	je	.L575
	cmpb	$44, %al
	jne	.L578
	testq	%rbx, %rbx
	je	.L578
	movq	16(%rbx), %rcx
	movl	4(%rcx), %eax
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$54, %dl
	je	.L610
.L579:
	movq	8(%rbx), %rsi
	movl	4(%rsi), %ebx
	andl	$63, %ebx
	cmpb	$41, %dl
	je	.L611
.L583:
	xorl	%r8d, %r8d
	cmpb	$49, %bl
	sete	%r8b
	leal	3(%r8,%r8), %r8d
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L608:
	andl	$15, %edx
	xorl	%r8d, %r8d
	cmpb	$4, %dl
	setne	%r8b
	leal	1(,%r8,8), %r8d
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L611:
	shrl	$7, %eax
	andl	$15, %eax
	cmpl	$3, %eax
	jne	.L583
	movq	8(%rcx), %rdi
	leaq	-28(%rbp), %rsi
	call	_ZNK2v88internal12AstRawString12AsArrayIndexEPj@PLT
	testb	%al, %al
	jne	.L583
	xorl	%r8d, %r8d
	cmpb	$49, %bl
	sete	%r8b
	leal	2(%r8,%r8), %r8d
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L578:
	xorl	%r8d, %r8d
	cmpb	$45, %al
	setne	%r8b
	addl	$8, %r8d
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L610:
	movq	8(%rcx), %rdi
	testb	$1, %ah
	je	.L580
	movq	8(%rdi), %rdi
.L580:
	movq	16(%rdi), %rdx
	cmpb	$0, 28(%rdi)
	movl	%edx, %eax
	je	.L612
.L581:
	testl	%eax, %eax
	jg	.L613
	movq	8(%rbx), %rax
	movl	4(%rax), %ebx
	andl	$63, %ebx
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L612:
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L613:
	call	_ZNK2v88internal12AstRawString14FirstCharacterEv@PLT
	movl	$6, %r8d
	cmpw	$35, %ax
	je	.L575
	movq	16(%rbx), %rcx
	movl	4(%rcx), %eax
	movl	%eax, %edx
	andl	$63, %edx
	jmp	.L579
.L609:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19468:
	.size	_ZNK2v88internal4Call11GetCallTypeEv, .-_ZNK2v88internal4Call11GetCallTypeEv
	.section	.text._ZN2v88internal10CaseClauseC2EPNS0_4ZoneEPNS0_10ExpressionERKNS0_10ScopedListIPNS0_9StatementEPvEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10CaseClauseC2EPNS0_4ZoneEPNS0_10ExpressionERKNS0_10ScopedListIPNS0_9StatementEPvEE
	.type	_ZN2v88internal10CaseClauseC2EPNS0_4ZoneEPNS0_10ExpressionERKNS0_10ScopedListIPNS0_9StatementEPvEE, @function
_ZN2v88internal10CaseClauseC2EPNS0_4ZoneEPNS0_10ExpressionERKNS0_10ScopedListIPNS0_9StatementEPvEE:
.LFB19470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	16(%rcx), %rsi
	movq	8(%rcx), %rax
	movq	%rdx, (%rdi)
	movl	%esi, %edx
	subl	%eax, %edx
	cmpl	%eax, %esi
	je	.L614
	movq	%rdi, %rbx
	movslq	%edx, %rsi
	testl	%edx, %edx
	jg	.L643
	movl	%edx, 16(%rdi)
.L631:
	movl	%edx, %r13d
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L643:
	movq	16(%r14), %r8
	movq	24(%r14), %rax
	salq	$3, %rsi
	subq	%r8, %rax
	cmpq	%rax, %rsi
	ja	.L644
	addq	%r8, %rsi
	movq	%rsi, 16(%r14)
.L618:
	movq	8(%rcx), %r9
	movl	16(%rcx), %r12d
	movq	%r8, 8(%rbx)
	movq	(%rcx), %rax
	movl	%edx, 16(%rbx)
	subl	%r9d, %r12d
	movl	$0, 20(%rbx)
	movq	(%rax), %r15
	movl	%r12d, %r13d
	movslq	%r12d, %rsi
	cmpl	%r12d, %edx
	jge	.L619
	movq	16(%r14), %r8
	movq	24(%r14), %rdx
	salq	$3, %rsi
	subq	%r8, %rdx
	cmpq	%rdx, %rsi
	ja	.L645
	addq	%r8, %rsi
	movq	%rsi, 16(%r14)
	xorl	%esi, %esi
.L622:
	movq	%r8, 8(%rbx)
	movl	%r12d, 16(%rbx)
.L623:
	movslq	%esi, %rax
	leaq	0(,%r9,8), %rcx
	leal	-1(%r12), %edx
	leaq	(%r15,%rcx), %r10
	leaq	(%r8,%rax,8), %rdi
	leaq	16(%r15,%rcx), %rcx
	leaq	16(%r8,%rax,8), %rax
	cmpq	%rcx, %rdi
	setnb	%cl
	cmpq	%rax, %r10
	setnb	%al
	orb	%al, %cl
	je	.L624
	cmpl	$3, %edx
	jbe	.L624
	movl	%r12d, %ecx
	xorl	%edx, %edx
	shrl	%ecx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L625:
	movdqu	(%r10,%rdx), %xmm0
	movups	%xmm0, (%rdi,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L625
	movl	%r12d, %eax
	andl	$-2, %eax
	andl	$1, %r12d
	je	.L627
	movl	%eax, %edx
	addl	%esi, %eax
	addq	%r9, %rdx
	cltq
	movq	(%r15,%rdx,8), %rdx
	movq	%rdx, (%r8,%rax,8)
.L627:
	movl	%r13d, 20(%rbx)
.L614:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L619:
	.cfi_restore_state
	testl	%r12d, %r12d
	jle	.L641
	xorl	%esi, %esi
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L624:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L629:
	movq	(%r10,%rax,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rdx, %rcx
	jne	.L629
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L645:
	movq	%r14, %rdi
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	20(%rbx), %esi
	movq	-56(%rbp), %r9
	movq	%rax, %r8
	testl	%esi, %esi
	jle	.L622
	movslq	%esi, %rax
	movq	8(%rbx), %rsi
	movq	%r8, %rdi
	movq	%r9, -56(%rbp)
	leaq	0(,%rax,8), %rdx
	call	memcpy@PLT
	movl	20(%rbx), %esi
	movq	-56(%rbp), %r9
	movq	%rax, %r8
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L644:
	movq	%r14, %rdi
	movq	%rcx, -64(%rbp)
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %edx
	movq	-64(%rbp), %rcx
	movq	%rax, %r8
	jmp	.L618
.L641:
	movl	%r12d, %edx
	jmp	.L631
	.cfi_endproc
.LFE19470:
	.size	_ZN2v88internal10CaseClauseC2EPNS0_4ZoneEPNS0_10ExpressionERKNS0_10ScopedListIPNS0_9StatementEPvEE, .-_ZN2v88internal10CaseClauseC2EPNS0_4ZoneEPNS0_10ExpressionERKNS0_10ScopedListIPNS0_9StatementEPvEE
	.globl	_ZN2v88internal10CaseClauseC1EPNS0_4ZoneEPNS0_10ExpressionERKNS0_10ScopedListIPNS0_9StatementEPvEE
	.set	_ZN2v88internal10CaseClauseC1EPNS0_4ZoneEPNS0_10ExpressionERKNS0_10ScopedListIPNS0_9StatementEPvEE,_ZN2v88internal10CaseClauseC2EPNS0_4ZoneEPNS0_10ExpressionERKNS0_10ScopedListIPNS0_9StatementEPvEE
	.section	.text._ZNK2v88internal7Literal14IsPropertyNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Literal14IsPropertyNameEv
	.type	_ZNK2v88internal7Literal14IsPropertyNameEv, @function
_ZNK2v88internal7Literal14IsPropertyNameEv:
.LFB19472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	4(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	shrl	$7, %edx
	andl	$15, %edx
	cmpl	$3, %edx
	je	.L651
.L646:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L652
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L651:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	leaq	-12(%rbp), %rsi
	call	_ZNK2v88internal12AstRawString12AsArrayIndexEPj@PLT
	xorl	$1, %eax
	jmp	.L646
.L652:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19472:
	.size	_ZNK2v88internal7Literal14IsPropertyNameEv, .-_ZNK2v88internal7Literal14IsPropertyNameEv
	.section	.text._ZNK2v88internal7Literal8ToUint32EPj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Literal8ToUint32EPj
	.type	_ZNK2v88internal7Literal8ToUint32EPj, @function
_ZNK2v88internal7Literal8ToUint32EPj:
.LFB19473:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	shrl	$7, %eax
	andl	$15, %eax
	cmpl	$1, %eax
	je	.L654
	cmpl	$3, %eax
	je	.L655
	testl	%eax, %eax
	je	.L656
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L655:
	movq	8(%rdi), %rdi
	jmp	_ZNK2v88internal12AstRawString12AsArrayIndexEPj@PLT
	.p2align 4,,10
	.p2align 3
.L656:
	movl	8(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L653
	movl	%edx, (%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L654:
	movsd	8(%rdi), %xmm1
	movsd	.LC3(%rip), %xmm2
	xorl	%eax, %eax
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rcx
	movq	%xmm2, %rdx
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	jne	.L653
	movl	%edx, %edx
	pxor	%xmm0, %xmm0
	movd	%xmm2, (%rsi)
	cvtsi2sdq	%rdx, %xmm0
	ucomisd	%xmm1, %xmm0
	setnp	%dl
	cmove	%edx, %eax
.L653:
	ret
	.cfi_endproc
.LFE19473:
	.size	_ZNK2v88internal7Literal8ToUint32EPj, .-_ZNK2v88internal7Literal8ToUint32EPj
	.section	.text._ZNK2v88internal7Literal12AsArrayIndexEPj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Literal12AsArrayIndexEPj
	.type	_ZNK2v88internal7Literal12AsArrayIndexEPj, @function
_ZNK2v88internal7Literal12AsArrayIndexEPj:
.LFB19474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	4(%rdi), %eax
	shrl	$7, %eax
	andl	$15, %eax
	cmpl	$1, %eax
	je	.L663
	cmpl	$3, %eax
	je	.L664
	testl	%eax, %eax
	je	.L665
.L670:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L665:
	.cfi_restore_state
	movl	8(%rdi), %eax
	testl	%eax, %eax
	js	.L670
	movl	%eax, (%rsi)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L664:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v88internal12AstRawString12AsArrayIndexEPj@PLT
.L667:
	testb	%al, %al
	je	.L670
	cmpl	$-1, (%rbx)
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L663:
	.cfi_restore_state
	movsd	8(%rdi), %xmm1
	movsd	.LC3(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L670
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movl	$0, %edx
	movd	%xmm2, (%rbx)
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm1, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	jmp	.L667
	.cfi_endproc
.LFE19474:
	.size	_ZNK2v88internal7Literal12AsArrayIndexEPj, .-_ZNK2v88internal7Literal12AsArrayIndexEPj
	.section	.rodata._ZNK2v88internal7Literal10BuildValueEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"(location_) != nullptr"
.LC6:
	.string	"Check failed: %s."
	.section	.text._ZNK2v88internal7Literal10BuildValueEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Literal10BuildValueEPNS0_7IsolateE
	.type	_ZNK2v88internal7Literal10BuildValueEPNS0_7IsolateE, @function
_ZNK2v88internal7Literal10BuildValueEPNS0_7IsolateE:
.LFB19475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movl	4(%rdi), %edx
	shrl	$7, %edx
	andl	$15, %edx
	cmpl	$8, %edx
	ja	.L678
	leaq	.L680(%rip), %rcx
	movq	%rsi, %r12
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal7Literal10BuildValueEPNS0_7IsolateE,"a",@progbits
	.align 4
	.align 4
.L680:
	.long	.L688-.L680
	.long	.L687-.L680
	.long	.L686-.L680
	.long	.L685-.L680
	.long	.L684-.L680
	.long	.L683-.L680
	.long	.L682-.L680
	.long	.L681-.L680
	.long	.L679-.L680
	.section	.text._ZNK2v88internal7Literal10BuildValueEPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L681:
	leaq	104(%rsi), %rax
.L692:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L679:
	.cfi_restore_state
	addq	$24, %rsp
	leaq	96(%rsi), %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L688:
	.cfi_restore_state
	movslq	8(%rdi), %rsi
	movq	41112(%r12), %rdi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L689
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L687:
	movsd	8(%rdi), %xmm0
	addq	$24, %rsp
	movq	%r12, %rdi
	movl	$1, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L686:
	.cfi_restore_state
	movq	8(%rdi), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13BigIntLiteralEPNS0_7IsolateEPKc@PLT
	testq	%rax, %rax
	jne	.L692
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L685:
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L684:
	.cfi_restore_state
	addq	$24, %rsp
	leaq	3720(%rsi), %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L683:
	.cfi_restore_state
	movzbl	8(%rdi), %esi
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory9ToBooleanEb@PLT
	.p2align 4,,10
	.p2align 3
.L682:
	.cfi_restore_state
	addq	$24, %rsp
	leaq	88(%rsi), %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L689:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L695
.L691:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L692
.L695:
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L691
.L678:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19475:
	.size	_ZNK2v88internal7Literal10BuildValueEPNS0_7IsolateE, .-_ZNK2v88internal7Literal10BuildValueEPNS0_7IsolateE
	.section	.text._ZN2v88internal19MaterializedLiteral19GetBoilerplateValueEPNS0_10ExpressionEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19MaterializedLiteral19GetBoilerplateValueEPNS0_10ExpressionEPNS0_7IsolateE
	.type	_ZN2v88internal19MaterializedLiteral19GetBoilerplateValueEPNS0_10ExpressionEPNS0_7IsolateE, @function
_ZN2v88internal19MaterializedLiteral19GetBoilerplateValueEPNS0_10ExpressionEPNS0_7IsolateE:
.LFB19452:
	.cfi_startproc
	endbr64
	movl	4(%rsi), %ecx
	movq	%rsi, %rdi
	movl	%ecx, %eax
	andl	$63, %eax
	cmpb	$41, %al
	je	.L706
	leal	-21(%rax), %esi
	cmpb	$2, %sil
	ja	.L698
	cmpb	$23, %al
	je	.L705
	cmpb	$22, %al
	je	.L705
.L698:
	leaq	80(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L705:
	andb	$1, %ch
	je	.L698
	movq	16(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L706:
	movq	%rdx, %rsi
	jmp	_ZNK2v88internal7Literal10BuildValueEPNS0_7IsolateE
	.cfi_endproc
.LFE19452:
	.size	_ZN2v88internal19MaterializedLiteral19GetBoilerplateValueEPNS0_10ExpressionEPNS0_7IsolateE, .-_ZN2v88internal19MaterializedLiteral19GetBoilerplateValueEPNS0_10ExpressionEPNS0_7IsolateE
	.section	.text.unlikely._ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE,"ax",@progbits
	.align 2
.LCOLDB7:
	.section	.text._ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE,"ax",@progbits
.LHOTB7:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE
	.type	_ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE, @function
_ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE:
.LFB19446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 16(%rdi)
	je	.L782
.L707:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L783
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L782:
	.cfi_restore_state
	movl	36(%rdi), %r10d
	movq	%rdi, %r15
	testl	%r10d, %r10d
	jle	.L752
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L786:
	movq	(%rax), %rax
	testb	$3, %al
	jne	.L710
	andq	$-4, %rax
	movl	4(%rax), %edx
	movl	%edx, %ecx
	andl	$63, %ecx
	cmpb	$41, %cl
	jne	.L711
	shrl	$7, %edx
	andl	$15, %edx
	cmpl	$3, %edx
	je	.L784
.L712:
	addl	$1, %r12d
.L710:
	addq	$1, %rbx
	cmpl	%ebx, %r10d
	jle	.L785
.L714:
	movq	24(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	cmpb	$5, 16(%rax)
	jne	.L786
	addq	$1, %rbx
	movl	$1, %r14d
	cmpl	%ebx, %r10d
	jg	.L714
.L785:
	movzbl	%r14b, %r8d
.L709:
	movl	12(%r15), %esi
	movq	-72(%rbp), %rdi
	movl	%r12d, %ecx
	movl	%r10d, %edx
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	call	_ZN2v88internal7Factory31NewObjectBoilerplateDescriptionEiiib@PLT
	movq	%rax, -80(%rbp)
	movl	36(%r15), %eax
	testl	%eax, %eax
	jle	.L716
	movq	%r15, %rax
	movq	%r13, %r15
	movq	%rax, %r13
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L722:
	movq	(%r12), %r14
	andq	$-4, %r14
	movl	4(%r14), %eax
	movl	$0, -60(%rbp)
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$41, %dl
	jne	.L723
	shrl	$7, %eax
	andl	$15, %eax
	cmpl	$1, %eax
	je	.L724
	cmpl	$3, %eax
	je	.L725
	testl	%eax, %eax
	je	.L787
.L727:
	movq	8(%r14), %rax
	movq	(%rax), %r14
.L734:
	movq	8(%r12), %rdi
	movl	4(%rdi), %edx
	movl	%edx, %eax
	andl	$63, %eax
	cmpb	$41, %al
	je	.L788
	leal	-21(%rax), %esi
	cmpb	$2, %sil
	ja	.L737
	cmpb	$23, %al
	je	.L781
	cmpb	$22, %al
	je	.L781
.L737:
	movq	-72(%rbp), %rax
	addq	$80, %rax
.L736:
	movq	-80(%rbp), %rcx
	leal	3(%rbx,%rbx), %r12d
	movq	(%rax), %r8
	sall	$3, %r12d
	movq	(%r14), %rdx
	leal	1(%rbx), %r14d
	movq	(%rcx), %rdi
	movslq	%r12d, %r12
	leaq	-1(%rdi), %rax
	addq	%rax, %r12
	movq	%rdx, (%r12)
	testb	$1, %dl
	je	.L750
	movq	%rdx, %r10
	andq	$-262144, %r10
	movq	8(%r10), %rsi
	movq	%r10, -88(%rbp)
	testl	$262144, %esi
	je	.L741
	movq	%r12, %rsi
	movq	%rax, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r10
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rdx
	movq	8(%r10), %rsi
	movq	-96(%rbp), %rdi
.L741:
	andl	$24, %esi
	je	.L750
	movq	%rdi, %rsi
	andq	$-262144, %rsi
	testb	$24, 8(%rsi)
	jne	.L750
	movq	%r12, %rsi
	movq	%rax, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L750:
	leal	2(%rbx), %r12d
	sall	$4, %r12d
	movslq	%r12d, %r12
	addq	%rax, %r12
	movq	%r8, (%r12)
	testb	$1, %r8b
	je	.L749
	movq	%r8, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L744
	movq	%r8, %rdx
	movq	%r12, %rsi
	movq	%r8, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %rdi
.L744:
	testb	$24, %al
	je	.L749
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L749
	movq	%r8, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L749:
	movl	%r14d, %ebx
.L718:
	addq	$1, %r15
	cmpl	%r15d, 36(%r13)
	jle	.L779
.L715:
	movq	24(%r13), %rax
	movq	(%rax,%r15,8), %r12
	cmpb	$5, 16(%r12)
	je	.L718
	cmpl	%ebx, 12(%r13)
	je	.L779
	movq	8(%r12), %rdi
	movzbl	4(%rdi), %eax
	andl	$63, %eax
	cmpb	$22, %al
	je	.L720
	cmpb	$23, %al
	jne	.L722
	cmpq	$0, 16(%rdi)
	jne	.L722
	movq	-72(%rbp), %rsi
	call	_ZN2v88internal12ArrayLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE.part.0
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L784:
	movq	8(%rax), %rdi
	leaq	-60(%rbp), %rsi
	call	_ZNK2v88internal12AstRawString12AsArrayIndexEPj@PLT
	movl	36(%r15), %r10d
	testb	%al, %al
	jne	.L712
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L781:
	andb	$1, %dh
	je	.L737
	movq	16(%rdi), %rax
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L787:
	movl	8(%r14), %esi
	testl	%esi, %esi
	js	.L727
	movl	%esi, -60(%rbp)
.L730:
	movq	-72(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L725:
	movq	8(%r14), %rdi
	leaq	-60(%rbp), %rsi
	call	_ZNK2v88internal12AstRawString12AsArrayIndexEPj@PLT
.L728:
	testb	%al, %al
	je	.L727
	movl	-60(%rbp), %esi
	cmpl	$-1, %esi
	je	.L727
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L724:
	movsd	8(%r14), %xmm1
	movsd	.LC3(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rdx
	movq	%xmm2, %rax
	shrq	$32, %rdx
	cmpq	$1127219200, %rdx
	jne	.L727
	movl	%eax, %eax
	pxor	%xmm0, %xmm0
	movl	$0, %edx
	movd	%xmm2, -60(%rbp)
	cvtsi2sdq	%rax, %xmm0
	ucomisd	%xmm1, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L720:
	movq	-72(%rbp), %rsi
	call	_ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L788:
	movq	-72(%rbp), %rsi
	call	_ZNK2v88internal7Literal10BuildValueEPNS0_7IsolateE
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L779:
	movq	%r13, %r15
.L716:
	movl	4(%r15), %ecx
	movq	-80(%rbp), %rbx
	movabsq	$103079215104, %rdi
	movabsq	$68719476736, %rdx
	movl	%ecx, %eax
	movq	(%rbx), %rsi
	andl	$2048, %eax
	movl	$0, %eax
	cmovne	%rdi, %rdx
	movabsq	$34359738368, %rdi
	cmovne	%rdi, %rax
	andb	$16, %ch
	cmovne	%rdx, %rax
	movq	%rax, 15(%rsi)
	movq	%rbx, 16(%r15)
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L752:
	xorl	%r8d, %r8d
	xorl	%r12d, %r12d
	jmp	.L709
.L783:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE
	.cfi_startproc
	.type	_ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE.cold, @function
_ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE.cold:
.LFSB19446:
.L711:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	4, %eax
	ud2
.L723:
	movl	4, %eax
	ud2
	.cfi_endproc
.LFE19446:
	.section	.text._ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE
	.size	_ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE, .-_ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE
	.section	.text.unlikely._ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE
	.size	_ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE.cold, .-_ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE.cold
.LCOLDE7:
	.section	.text._ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE
.LHOTE7:
	.section	.text._ZN2v88internal12ArrayLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12ArrayLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE.part.0, @function
_ZN2v88internal12ArrayLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE.part.0:
.LFB24090:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	12(%rdi), %eax
	movl	%eax, -108(%rbp)
	testl	%eax, %eax
	jns	.L790
	movl	36(%rdi), %eax
	movl	%eax, -108(%rbp)
.L790:
	movl	-108(%rbp), %ebx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE@PLT
	movq	%rax, -80(%rbp)
	testl	%ebx, %ebx
	jle	.L867
	movl	-108(%rbp), %eax
	movq	%r15, -72(%rbp)
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	movb	$0, -109(%rbp)
	movq	%r14, %r15
	subl	$1, %eax
	leaq	8(,%rax,8), %rax
	movq	%rax, -88(%rbp)
	leaq	80(%r14), %rax
	movq	%rax, -120(%rbp)
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L873:
	movq	-1(%rsi), %rax
	xorl	%esi, %esi
	cmpw	$65, 11(%rax)
	sete	%sil
	sete	%r13b
	leal	2(%rsi,%rsi), %esi
	leal	2(%r13,%r13), %r13d
.L806:
	movzbl	%r12b, %edi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
	movq	-64(%rbp), %rdx
	testb	%al, %al
	movq	-80(%rbp), %rax
	cmovne	%r13d, %r12d
	movq	(%rdx), %r13
	movq	(%rax), %rdi
	leal	16(%rbx), %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%r13, (%rsi)
	testb	$1, %r13b
	je	.L812
	movq	%r13, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -64(%rbp)
	testl	$262144, %eax
	je	.L810
	movq	%r13, %rdx
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %r8
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%r8), %rax
.L810:
	testb	$24, %al
	je	.L812
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L812
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L812:
	movq	-56(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %r14
	je	.L802
	movq	%r14, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L802:
	addq	$8, %rbx
	cmpq	-88(%rbp), %rbx
	je	.L868
.L813:
	movq	-72(%rbp), %rax
	movq	24(%rax), %rax
	movq	(%rax,%rbx), %r13
	movzbl	4(%r13), %eax
	andl	$63, %eax
	cmpb	$22, %al
	je	.L793
	cmpb	$23, %al
	je	.L869
.L795:
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movl	4(%r13), %edx
	movq	41096(%r15), %r14
	movq	%rax, -56(%rbp)
	movl	%edx, %eax
	andl	$63, %eax
	cmpb	$41, %al
	je	.L870
	leal	-21(%rax), %esi
	cmpb	$2, %sil
	ja	.L798
	cmpb	$23, %al
	je	.L866
	cmpb	$22, %al
	je	.L866
.L798:
	movq	-120(%rbp), %rdx
.L797:
	movq	(%rdx), %rsi
	cmpq	96(%r15), %rsi
	je	.L871
.L863:
	cmpq	%rsi, 80(%r15)
	je	.L872
.L803:
	testb	$1, %sil
	jne	.L873
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L872:
	movq	41112(%r15), %rdi
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rsi
	testq	%rdi, %rdi
	je	.L804
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L866:
	andb	$1, %dh
	je	.L798
	movq	16(%r13), %rdx
	movq	(%rdx), %rsi
	cmpq	96(%r15), %rsi
	jne	.L863
.L871:
	movq	-56(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %r14
	je	.L801
	movq	%r14, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L801:
	movb	$1, -109(%rbp)
	addq	$8, %rbx
	cmpq	-88(%rbp), %rbx
	jne	.L813
	.p2align 4,,10
	.p2align 3
.L868:
	movq	%r15, %r14
	movq	-72(%rbp), %r15
	movl	4(%r15), %eax
	andl	$256, %eax
	cmpb	$0, -109(%rbp)
	je	.L865
	testb	%r12b, %r12b
	je	.L835
	cmpb	$4, %r12b
	je	.L816
	cmpb	$2, %r12b
	je	.L836
.L865:
	movl	-108(%rbp), %ecx
	movzbl	%r12b, %r13d
.L792:
	leal	-4(%r12), %edx
	testl	%eax, %eax
	jne	.L874
	movq	-80(%rbp), %rbx
	cmpb	$1, %dl
	jbe	.L827
.L824:
	movq	%rbx, %rdx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory30NewArrayBoilerplateDescriptionENS0_12ElementsKindENS0_6HandleINS0_14FixedArrayBaseEEE@PLT
	movq	%rax, 16(%r15)
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L869:
	.cfi_restore_state
	cmpq	$0, 16(%r13)
	jne	.L795
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12ArrayLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE.part.0
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L793:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L870:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal7Literal10BuildValueEPNS0_7IsolateE
	movq	%rax, %rdx
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L804:
	movq	41088(%r15), %rdx
	cmpq	41096(%r15), %rdx
	je	.L875
.L805:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdx)
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L874:
	cmpb	$3, %r12b
	setbe	-109(%rbp)
.L828:
	movl	8(%r15), %eax
	addl	%eax, %eax
	cmpl	$2, %eax
	sete	%al
	testb	%al, -109(%rbp)
	je	.L838
	testl	%ecx, %ecx
	jne	.L818
.L838:
	leal	-4(%r12), %edx
	movq	-80(%rbp), %rbx
	cmpb	$1, %dl
	ja	.L824
.L827:
	movzbl	%r12b, %r12d
	salq	$3, %r12
.L825:
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rax
	movl	-108(%rbp), %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	(%rax,%r12), %r12
	call	_ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE@PLT
	movl	-108(%rbp), %r9d
	movq	-80(%rbp), %rdx
	movq	%r14, %rsi
	movq	%rax, %rbx
	movq	(%r12), %rax
	movl	$3, %ecx
	movq	%r12, %rdi
	movq	%rbx, %r8
	call	*232(%rax)
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L835:
	movl	$1, %r13d
	movl	$1, %r12d
.L815:
	testl	%eax, %eax
	je	.L862
	movl	-108(%rbp), %ecx
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L875:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L805
.L818:
	movq	-80(%rbp), %rax
	movq	160(%r14), %rdx
	movq	(%rax), %rdi
	movq	%rdx, -1(%rdi)
	testq	%rdx, %rdx
	je	.L862
	testb	$1, %dl
	je	.L862
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L862
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L862:
	movq	-80(%rbp), %rbx
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L867:
	movl	4(%r15), %eax
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	xorl	%r12d, %r12d
	andl	$256, %eax
	jmp	.L792
.L836:
	movl	$3, %r13d
	movl	$3, %r12d
	jmp	.L815
.L816:
	testl	%eax, %eax
	je	.L826
	movl	$5, %r13d
	movl	$5, %r12d
	jmp	.L827
.L826:
	movl	$40, %r12d
	movl	$5, %r13d
	jmp	.L825
	.cfi_endproc
.LFE24090:
	.size	_ZN2v88internal12ArrayLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE.part.0, .-_ZN2v88internal12ArrayLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE.part.0
	.section	.text._ZN2v88internal12ArrayLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12ArrayLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE
	.type	_ZN2v88internal12ArrayLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE, @function
_ZN2v88internal12ArrayLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE:
.LFB19449:
	.cfi_startproc
	endbr64
	cmpq	$0, 16(%rdi)
	je	.L878
	ret
	.p2align 4,,10
	.p2align 3
.L878:
	jmp	_ZN2v88internal12ArrayLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE.part.0
	.cfi_endproc
.LFE19449:
	.size	_ZN2v88internal12ArrayLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE, .-_ZN2v88internal12ArrayLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE
	.section	.text._ZN2v88internal19MaterializedLiteral14BuildConstantsEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19MaterializedLiteral14BuildConstantsEPNS0_7IsolateE
	.type	_ZN2v88internal19MaterializedLiteral14BuildConstantsEPNS0_7IsolateE, @function
_ZN2v88internal19MaterializedLiteral14BuildConstantsEPNS0_7IsolateE:
.LFB19455:
	.cfi_startproc
	endbr64
	movzbl	4(%rdi), %eax
	andl	$63, %eax
	cmpb	$23, %al
	je	.L882
	cmpb	$22, %al
	je	.L883
.L879:
	ret
	.p2align 4,,10
	.p2align 3
.L882:
	cmpq	$0, 16(%rdi)
	jne	.L879
	jmp	_ZN2v88internal12ArrayLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE.part.0
	.p2align 4,,10
	.p2align 3
.L883:
	jmp	_ZN2v88internal13ObjectLiteral27BuildBoilerplateDescriptionEPNS0_7IsolateE
	.cfi_endproc
.LFE19455:
	.size	_ZN2v88internal19MaterializedLiteral14BuildConstantsEPNS0_7IsolateE, .-_ZN2v88internal19MaterializedLiteral14BuildConstantsEPNS0_7IsolateE
	.section	.text._ZNK2v88internal7Literal15ToBooleanIsTrueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal7Literal15ToBooleanIsTrueEv
	.type	_ZNK2v88internal7Literal15ToBooleanIsTrueEv, @function
_ZNK2v88internal7Literal15ToBooleanIsTrueEv:
.LFB19476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	4(%rdi), %eax
	shrl	$7, %eax
	andl	$15, %eax
	cmpl	$7, %eax
	ja	.L885
	leaq	.L887(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal7Literal15ToBooleanIsTrueEv,"a",@progbits
	.align 4
	.align 4
.L887:
	.long	.L893-.L887
	.long	.L892-.L887
	.long	.L891-.L887
	.long	.L890-.L887
	.long	.L902-.L887
	.long	.L888-.L887
	.long	.L903-.L887
	.long	.L903-.L887
	.section	.text._ZNK2v88internal7Literal15ToBooleanIsTrueEv
	.p2align 4,,10
	.p2align 3
.L903:
	xorl	%eax, %eax
.L884:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L891:
	.cfi_restore_state
	movq	8(%rdi), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	movzbl	(%rbx), %edx
	movq	%rax, %rcx
	cmpq	$1, %rax
	je	.L906
	cmpb	$48, %dl
	sete	%dl
	movzbl	%dl, %edx
	addq	%rdx, %rdx
	cmpq	%rdx, %rax
	jbe	.L903
.L895:
	addq	%rbx, %rdx
	leaq	(%rbx,%rcx), %rax
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L907:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	je	.L903
.L897:
	cmpb	$48, (%rdx)
	je	.L907
.L902:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L893:
	.cfi_restore_state
	movl	8(%rdi), %edx
	testl	%edx, %edx
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L892:
	.cfi_restore_state
	movsd	8(%rdi), %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal15DoubleToBooleanEd@PLT
	.p2align 4,,10
	.p2align 3
.L890:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movl	16(%rax), %eax
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L888:
	.cfi_restore_state
	movzbl	8(%rdi), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L906:
	.cfi_restore_state
	xorl	%eax, %eax
	cmpb	$48, %dl
	je	.L884
	xorl	%edx, %edx
	jmp	.L895
.L885:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19476:
	.size	_ZNK2v88internal7Literal15ToBooleanIsTrueEv, .-_ZNK2v88internal7Literal15ToBooleanIsTrueEv
	.section	.text._ZN2v88internal7Literal4HashEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Literal4HashEv
	.type	_ZN2v88internal7Literal4HashEv, @function
_ZN2v88internal7Literal4HashEv:
.LFB19477:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	shrl	$7, %eax
	andl	$15, %eax
	cmpl	$3, %eax
	jne	.L909
	movq	8(%rdi), %rax
	movl	24(%rax), %eax
	shrl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L909:
	testl	%eax, %eax
	jne	.L919
	pxor	%xmm0, %xmm0
	cvtsi2sdl	8(%rdi), %xmm0
.L914:
	movq	%xmm0, %rax
	movq	%xmm0, %rdx
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rdx, %rax
	andl	$1073741823, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L919:
	cmpl	$1, %eax
	jne	.L920
	movsd	8(%rdi), %xmm0
	jmp	.L914
.L920:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19477:
	.size	_ZN2v88internal7Literal4HashEv, .-_ZN2v88internal7Literal4HashEv
	.section	.text._ZN2v88internal14AstNodeFactory16NewNumberLiteralEdi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14AstNodeFactory16NewNumberLiteralEdi
	.type	_ZN2v88internal14AstNodeFactory16NewNumberLiteralEdi, @function
_ZN2v88internal14AstNodeFactory16NewNumberLiteralEdi:
.LFB19479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	comisd	.LC8(%rip), %xmm0
	jb	.L922
	movsd	.LC9(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L935
.L922:
	cmpq	$15, %rdx
	jbe	.L936
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L930:
	movl	%ebx, (%rax)
	movl	$169, 4(%rax)
	movsd	%xmm0, 8(%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L935:
	.cfi_restore_state
	movabsq	$-9223372036854775808, %rcx
	movq	%xmm0, %rsi
	cmpq	%rcx, %rsi
	je	.L922
	cvttsd2sil	%xmm0, %r12d
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%r12d, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L922
	jne	.L922
	cmpq	$15, %rdx
	jbe	.L937
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L927:
	movl	%ebx, (%rax)
	movl	$41, 4(%rax)
	movl	%r12d, 8(%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L936:
	.cfi_restore_state
	movl	$16, %esi
	movsd	%xmm0, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movsd	-24(%rbp), %xmm0
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L937:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L927
	.cfi_endproc
.LFE19479:
	.size	_ZN2v88internal14AstNodeFactory16NewNumberLiteralEdi, .-_ZN2v88internal14AstNodeFactory16NewNumberLiteralEdi
	.section	.rodata._ZN2v88internal11CallRuntime10debug_nameEv.str1.1,"aMS",@progbits,1
.LC10:
	.string	"(context function)"
	.section	.text._ZN2v88internal11CallRuntime10debug_nameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CallRuntime10debug_nameEv
	.type	_ZN2v88internal11CallRuntime10debug_nameEv, @function
_ZN2v88internal11CallRuntime10debug_nameEv:
.LFB19480:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	leaq	.LC10(%rip), %rax
	testq	%rdx, %rdx
	je	.L938
	movq	8(%rdx), %rax
.L938:
	ret
	.cfi_endproc
.LFE19480:
	.size	_ZN2v88internal11CallRuntime10debug_nameEv, .-_ZN2v88internal11CallRuntime10debug_nameEv
	.section	.text._ZNK2v88internal18BreakableStatement6labelsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18BreakableStatement6labelsEv
	.type	_ZNK2v88internal18BreakableStatement6labelsEv, @function
_ZNK2v88internal18BreakableStatement6labelsEv:
.LFB19481:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %edx
	movl	%edx, %eax
	andl	$63, %eax
	cmpb	$7, %al
	je	.L943
	andl	$56, %edx
	jne	.L944
	subl	$2, %eax
	cmpb	$4, %al
	ja	.L945
.L946:
	movq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L944:
	cmpb	$8, %al
	je	.L946
.L945:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L943:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	xorl	%eax, %eax
	andb	$1, %dh
	je	.L942
	movq	32(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L942:
	ret
	.cfi_endproc
.LFE19481:
	.size	_ZNK2v88internal18BreakableStatement6labelsEv, .-_ZNK2v88internal18BreakableStatement6labelsEv
	.section	.rodata._ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC11:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	.type	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_, @function
_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_:
.LFB23445:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L969
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L964
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L970
.L966:
	movq	%rcx, %r14
.L957:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L963:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L971
	testq	%rsi, %rsi
	jg	.L959
	testq	%r15, %r15
	jne	.L962
.L960:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L971:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L959
.L962:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L970:
	testq	%r14, %r14
	js	.L966
	jne	.L957
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L959:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L960
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L964:
	movl	$1, %r14d
	jmp	.L957
.L969:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23445:
	.size	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_, .-_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	.section	.text._ZNK2v88internal15FunctionLiteral12GetDebugNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15FunctionLiteral12GetDebugNameEv
	.type	_ZNK2v88internal15FunctionLiteral12GetDebugNameEv, @function
_ZNK2v88internal15FunctionLiteral12GetDebugNameEv:
.LFB19404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	32(%rsi), %rsi
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testq	%rsi, %rsi
	je	.L973
	cmpq	$0, 8(%rsi)
	je	.L973
.L974:
	pxor	%xmm0, %xmm0
	leaq	-88(%rbp), %rdi
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal13AstConsString12ToRawStringsEv@PLT
	movq	-88(%rbp), %r15
	movq	-72(%rbp), %rsi
	testq	%r15, %r15
	je	.L979
	leaq	-89(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L986:
	movq	8(%r15), %r14
	movzbl	28(%r14), %edx
	testb	%dl, %dl
	je	.L979
	xorl	%r13d, %r13d
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L1009:
	movb	%al, (%rsi)
	movq	-72(%rbp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, -72(%rbp)
.L984:
	movzbl	28(%r14), %edx
	addq	$1, %r13
.L985:
	movq	16(%r14), %rax
	testb	%dl, %dl
	jne	.L981
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
.L981:
	cmpl	%r13d, %eax
	jle	.L982
	movq	8(%r14), %rax
	movzbl	(%rax,%r13), %eax
	movb	%al, -89(%rbp)
	cmpq	%rsi, -64(%rbp)
	jne	.L1009
	leaq	-80(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	movq	-72(%rbp), %rsi
	jmp	.L984
.L973:
	movq	64(%rax), %rsi
	testq	%rsi, %rsi
	je	.L975
	cmpq	$0, 8(%rsi)
	jne	.L974
.L975:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L976
	movq	(%rax), %rax
	leaq	-80(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	.p2align 4,,10
	.p2align 3
.L972:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1010
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L982:
	.cfi_restore_state
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.L986
.L979:
	subq	-80(%rbp), %rsi
	leaq	1(%rsi), %rdi
	call	_Znam@PLT
	movq	-80(%rbp), %r13
	movq	-72(%rbp), %rbx
	movq	%rax, %rdi
	subq	%r13, %rbx
	movq	%r13, %rsi
	movq	%rbx, %rdx
	call	memcpy@PLT
	movb	$0, (%rax,%rbx)
	movq	-88(%rbp), %rbx
	movq	%rax, (%r12)
	testq	%rbx, %rbx
	je	.L989
	.p2align 4,,10
	.p2align 3
.L988:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L988
	movq	-80(%rbp), %r13
	testq	%r13, %r13
	je	.L972
.L989:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	jmp	.L972
.L976:
	movl	$1, %edi
	call	_Znam@PLT
	movb	$0, (%rax)
	movq	%rax, (%r12)
	jmp	.L972
.L1010:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19404:
	.size	_ZNK2v88internal15FunctionLiteral12GetDebugNameEv, .-_ZNK2v88internal15FunctionLiteral12GetDebugNameEv
	.section	.rodata._ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"Out of memory: HashMap::Initialize"
	.section	.text._ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_
	.type	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_, @function
_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_:
.LFB23691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	12(%rdi), %eax
	movq	(%rdi), %r13
	movq	%rsi, -64(%rbp)
	movl	%eax, -52(%rbp)
	movl	8(%rdi), %eax
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rsi
	movq	%rax, %rbx
	movq	16(%rdx), %rax
	movq	24(%rdx), %rdx
	salq	$3, %rsi
	movq	%rdx, %rcx
	movq	%rdx, -72(%rbp)
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L1047
	movq	-64(%rbp), %rdx
	addq	%rax, %rsi
	movq	%rsi, 16(%rdx)
.L1013:
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L1048
	movl	%ebx, 8(%r14)
	testl	%ebx, %ebx
	je	.L1015
	movq	$0, (%rax)
	cmpl	$1, 8(%r14)
	jbe	.L1015
	movl	$24, %ecx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	(%r14), %rsi
	addq	$1, %rax
	movq	$0, (%rsi,%rcx)
	movl	8(%r14), %esi
	addq	$24, %rcx
	cmpq	%rax, %rsi
	ja	.L1016
.L1015:
	movl	-52(%rbp), %eax
	movl	$0, 12(%r14)
	testl	%eax, %eax
	je	.L1011
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1018
	movl	8(%r14), %esi
	movl	16(%r13), %r12d
	movq	(%r14), %r9
	subl	$1, %esi
	movl	%esi, %r15d
	andl	%r12d, %r15d
	leaq	(%r15,%r15,2), %rbx
	salq	$3, %rbx
	leaq	(%r9,%rbx), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	je	.L1019
.L1025:
	cmpl	16(%rax), %r12d
	je	.L1049
.L1020:
	addq	$1, %r15
	andl	%esi, %r15d
	leaq	(%r15,%r15,2), %rbx
	salq	$3, %rbx
	leaq	(%r9,%rbx), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	jne	.L1025
	movl	16(%r13), %r12d
.L1019:
	movq	%rdi, %xmm0
	movhps	8(%r13), %xmm0
	movl	%r12d, 16(%rax)
	movups	%xmm0, (%rax)
	movl	12(%r14), %eax
	addl	$1, %eax
	movl	%eax, %esi
	movl	%eax, 12(%r14)
	shrl	$2, %esi
	addl	%esi, %eax
	cmpl	8(%r14), %eax
	jnb	.L1022
.L1026:
	addq	$24, %r13
	subl	$1, -52(%rbp)
	jne	.L1017
.L1011:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1049:
	.cfi_restore_state
	movq	%r8, %rsi
	call	*16(%r14)
	testb	%al, %al
	jne	.L1021
	movl	8(%r14), %esi
	movq	(%r14), %r9
	movq	0(%r13), %rdi
	subl	$1, %esi
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1018:
	addq	$24, %r13
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1021:
	movq	(%r14), %rax
	movl	16(%r13), %r12d
	movq	0(%r13), %rdi
	addq	%rbx, %rax
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1022:
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_
	movl	8(%r14), %edi
	movq	(%r14), %r8
	subl	$1, %edi
	movl	%edi, %ebx
	andl	%r12d, %ebx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%r8,%rax,8), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1026
	cmpl	%r12d, 16(%rax)
	je	.L1050
	.p2align 4,,10
	.p2align 3
.L1027:
	addq	$1, %rbx
	andl	%edi, %ebx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%r8,%rax,8), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1026
	cmpl	%r12d, 16(%rax)
	jne	.L1027
.L1050:
	movq	0(%r13), %rdi
	call	*16(%r14)
	testb	%al, %al
	jne	.L1026
	movl	8(%r14), %edi
	movq	(%r14), %r8
	subl	$1, %edi
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1047:
	movq	-64(%rbp), %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1013
.L1048:
	leaq	.LC12(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23691:
	.size	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_, .-_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_
	.section	.text.unlikely._ZN2v88internal13ObjectLiteral18CalculateEmitStoreEPNS0_4ZoneE,"ax",@progbits
	.align 2
.LCOLDB13:
	.section	.text._ZN2v88internal13ObjectLiteral18CalculateEmitStoreEPNS0_4ZoneE,"ax",@progbits
.LHOTB13:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ObjectLiteral18CalculateEmitStoreEPNS0_4ZoneE
	.type	_ZN2v88internal13ObjectLiteral18CalculateEmitStoreEPNS0_4ZoneE, @function
_ZN2v88internal13ObjectLiteral18CalculateEmitStoreEPNS0_4ZoneE:
.LFB19440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -112(%rbp)
	movq	24(%rsi), %rdi
	movq	%rdi, %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZN2v88internal7Literal5MatchEPvS2_(%rip), %rax
	movq	%rdi, -88(%rbp)
	movq	%rax, -64(%rbp)
	movq	16(%rsi), %rax
	subq	%rax, %rdx
	cmpq	$191, %rdx
	jbe	.L1106
	movq	-112(%rbp), %rdi
	leaq	192(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1053:
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L1107
	movl	$8, -72(%rbp)
	movq	$0, (%rax)
	cmpl	$1, -72(%rbp)
	jbe	.L1055
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1056:
	movq	-80(%rbp), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	-72(%rbp), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L1056
.L1055:
	movslq	36(%r13), %rax
	movl	$0, -68(%rbp)
	movl	%eax, %edx
	subl	$1, %edx
	js	.L1051
	movslq	%edx, %r8
	movl	%edx, %edx
	subq	%rdx, %rax
	leaq	0(,%r8,8), %r14
	leaq	-16(,%rax,8), %rax
	movq	%rax, -104(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L1084:
	movq	24(%r13), %rax
	movq	(%rax,%r14), %r12
	movq	(%r12), %rcx
	testb	$3, %cl
	jne	.L1059
	cmpb	$5, 16(%r12)
	je	.L1059
	andq	$-4, %rcx
	movl	4(%rcx), %eax
	movl	%eax, %edx
	andl	$63, %edx
	cmpb	$41, %dl
	jne	.L1061
	shrl	$7, %eax
	andl	$15, %eax
	cmpl	$3, %eax
	jne	.L1062
	movq	8(%rcx), %rax
	movl	24(%rax), %edx
	shrl	$2, %edx
.L1063:
	movl	-72(%rbp), %esi
	movq	-80(%rbp), %rdi
	subl	$1, %esi
	movl	%esi, %ebx
	andl	%edx, %ebx
.L1103:
	leaq	(%rbx,%rbx,2), %r15
	salq	$3, %r15
	leaq	(%rdi,%r15), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	je	.L1072
	cmpl	16(%rax), %edx
	je	.L1108
.L1069:
	addq	$1, %rbx
	andl	%esi, %ebx
	jmp	.L1103
	.p2align 4,,10
	.p2align 3
.L1070:
	movq	-80(%rbp), %rax
	addq	%r15, %rax
	cmpq	$0, (%rax)
	je	.L1072
.L1071:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1104
	movzbl	16(%r12), %ecx
	movzbl	16(%rdx), %edx
	cmpb	$3, %cl
	je	.L1109
	cmpb	$3, %dl
	jne	.L1081
	cmpb	$4, %cl
	je	.L1059
.L1081:
	subl	$3, %edx
	movb	$0, 17(%r12)
	cmpb	$1, %dl
	ja	.L1059
.L1104:
	movq	%r12, 8(%rax)
.L1059:
	subq	$8, %r14
	cmpq	-104(%rbp), %r14
	jne	.L1084
.L1051:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1110
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1108:
	.cfi_restore_state
	movl	%edx, -92(%rbp)
	movq	%rcx, %rdi
	movq	%r11, %rsi
	movq	%rcx, -88(%rbp)
	call	*-64(%rbp)
	movq	-88(%rbp), %rcx
	movl	-92(%rbp), %edx
	testb	%al, %al
	jne	.L1070
	movl	-72(%rbp), %esi
	movq	-80(%rbp), %rdi
	subl	$1, %esi
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1062:
	testl	%eax, %eax
	jne	.L1111
	pxor	%xmm0, %xmm0
	cvtsi2sdl	8(%rcx), %xmm0
.L1067:
	movq	%xmm0, %rax
	movq	%xmm0, %rdx
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rdx
	movq	%rdx, %rax
	shrq	$11, %rax
	xorq	%rax, %rdx
	movq	%rdx, %rax
	salq	$6, %rax
	addq	%rax, %rdx
	movq	%rdx, %rax
	shrq	$22, %rax
	xorq	%rax, %rdx
	andl	$1073741823, %edx
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1111:
	cmpl	$1, %eax
	jne	.L1112
	movsd	8(%rcx), %xmm0
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1072:
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movl	-68(%rbp), %edi
	movl	%edx, 16(%rax)
	leal	1(%rdi), %esi
	movl	%esi, %edi
	movl	%esi, -68(%rbp)
	shrl	$2, %edi
	addl	%edi, %esi
	cmpl	-72(%rbp), %esi
	jb	.L1071
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movl	%edx, -92(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS_8internal20ZoneAllocationPolicyEE6ResizeES8_
	movl	-72(%rbp), %edi
	movl	-92(%rbp), %edx
	movq	-80(%rbp), %r11
	movq	-88(%rbp), %rcx
	subl	$1, %edi
	movl	%edi, %ebx
	andl	%edx, %ebx
	leaq	(%rbx,%rbx,2), %r15
	salq	$3, %r15
	leaq	(%r11,%r15), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1071
	cmpl	16(%rax), %edx
	je	.L1113
	.p2align 4,,10
	.p2align 3
.L1076:
	addq	$1, %rbx
	andl	%edi, %ebx
	leaq	(%rbx,%rbx,2), %r15
	salq	$3, %r15
	leaq	(%r11,%r15), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1071
	cmpl	16(%rax), %edx
	jne	.L1076
.L1113:
	movl	%edx, -92(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -88(%rbp)
	call	*-64(%rbp)
	testb	%al, %al
	jne	.L1077
	movl	-72(%rbp), %edi
	movq	-80(%rbp), %r11
	movq	-88(%rbp), %rcx
	movl	-92(%rbp), %edx
	subl	$1, %edi
	jmp	.L1076
	.p2align 4,,10
	.p2align 3
.L1109:
	cmpb	$4, %dl
	jne	.L1081
	subq	$8, %r14
	cmpq	-104(%rbp), %r14
	jne	.L1084
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1077:
	movq	-80(%rbp), %rax
	addq	%r15, %rax
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1106:
	movq	-112(%rbp), %rdi
	movl	$192, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1053
.L1112:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1110:
	call	__stack_chk_fail@PLT
.L1107:
	leaq	.LC12(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal13ObjectLiteral18CalculateEmitStoreEPNS0_4ZoneE
	.cfi_startproc
	.type	_ZN2v88internal13ObjectLiteral18CalculateEmitStoreEPNS0_4ZoneE.cold, @function
_ZN2v88internal13ObjectLiteral18CalculateEmitStoreEPNS0_4ZoneE.cold:
.LFSB19440:
.L1061:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	4, %eax
	ud2
	.cfi_endproc
.LFE19440:
	.section	.text._ZN2v88internal13ObjectLiteral18CalculateEmitStoreEPNS0_4ZoneE
	.size	_ZN2v88internal13ObjectLiteral18CalculateEmitStoreEPNS0_4ZoneE, .-_ZN2v88internal13ObjectLiteral18CalculateEmitStoreEPNS0_4ZoneE
	.section	.text.unlikely._ZN2v88internal13ObjectLiteral18CalculateEmitStoreEPNS0_4ZoneE
	.size	_ZN2v88internal13ObjectLiteral18CalculateEmitStoreEPNS0_4ZoneE.cold, .-_ZN2v88internal13ObjectLiteral18CalculateEmitStoreEPNS0_4ZoneE.cold
.LCOLDE13:
	.section	.text._ZN2v88internal13ObjectLiteral18CalculateEmitStoreEPNS0_4ZoneE
.LHOTE13:
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal7AstNode20AsIterationStatementEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal7AstNode20AsIterationStatementEv, @function
_GLOBAL__sub_I__ZN2v88internal7AstNode20AsIterationStatementEv:
.LFB23981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23981:
	.size	_GLOBAL__sub_I__ZN2v88internal7AstNode20AsIterationStatementEv, .-_GLOBAL__sub_I__ZN2v88internal7AstNode20AsIterationStatementEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal7AstNode20AsIterationStatementEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC3:
	.long	0
	.long	1127219200
	.align 8
.LC8:
	.long	0
	.long	-1042284544
	.align 8
.LC9:
	.long	4290772992
	.long	1105199103
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
