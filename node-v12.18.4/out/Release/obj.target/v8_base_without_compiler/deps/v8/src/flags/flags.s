	.file	"flags.cc"
	.text
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB10753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE10753:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB10844:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10844:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB10754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10754:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB10845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE10845:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.rodata._ZN2v88internallsERSoRKNS0_12_GLOBAL__N_14FlagE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"true"
.LC1:
	.string	"false"
.LC2:
	.string	"unset"
.LC3:
	.string	"nullptr"
	.section	.text._ZN2v88internallsERSoRKNS0_12_GLOBAL__N_14FlagE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internallsERSoRKNS0_12_GLOBAL__N_14FlagE, @function
_ZN2v88internallsERSoRKNS0_12_GLOBAL__N_14FlagE:
.LFB9220:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	cmpl	$7, (%rsi)
	movq	%rdi, %r12
	ja	.L28
	movl	(%rsi), %eax
	leaq	.L13(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internallsERSoRKNS0_12_GLOBAL__N_14FlagE,"a",@progbits
	.align 4
	.align 4
.L13:
	.long	.L20-.L13
	.long	.L19-.L13
	.long	.L18-.L13
	.long	.L17-.L13
	.long	.L14-.L13
	.long	.L15-.L13
	.long	.L14-.L13
	.long	.L12-.L13
	.section	.text._ZN2v88internallsERSoRKNS0_12_GLOBAL__N_14FlagE
	.p2align 4,,10
	.p2align 3
.L12:
	movq	16(%rsi), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L27
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
.L23:
	movq	%r13, %rsi
.L30:
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L28:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	16(%rsi), %rax
	movq	(%rax), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	16(%rsi), %rax
	movl	(%rax), %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	16(%rsi), %rax
	movsd	(%rax), %xmm0
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	16(%rsi), %rax
	leaq	.LC0(%rip), %rsi
	cmpb	$1, (%rax)
	sbbq	%rdx, %rdx
	notq	%rdx
	addq	$5, %rdx
	cmpb	$0, (%rax)
	leaq	.LC1(%rip), %rax
	cmove	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	16(%rsi), %rax
	movl	$5, %edx
	leaq	.LC2(%rip), %rsi
	cmpb	$0, (%rax)
	je	.L30
	cmpb	$1, 1(%rax)
	leaq	.LC0(%rip), %rsi
	sbbq	%rdx, %rdx
	notq	%rdx
	addq	$5, %rdx
	cmpb	$0, 1(%rax)
	leaq	.LC1(%rip), %rax
	cmove	%rax, %rsi
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L18:
	movq	16(%rsi), %rax
	movl	(%rax), %esi
	call	_ZNSolsEi@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movl	$7, %edx
	leaq	.LC3(%rip), %r13
	jmp	.L23
	.cfi_endproc
.LFE9220:
	.size	_ZN2v88internallsERSoRKNS0_12_GLOBAL__N_14FlagE, .-_ZN2v88internallsERSoRKNS0_12_GLOBAL__N_14FlagE
	.section	.text._ZN2v88internal8FlagList13ResetAllFlagsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8FlagList13ResetAllFlagsEv
	.type	_ZN2v88internal8FlagList13ResetAllFlagsEv, @function
_ZN2v88internal8FlagList13ResetAllFlagsEv:
.LFB9232:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	.L34(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	_ZN2v88internal12_GLOBAL__N_15flagsE(%rip), %rbx
	leaq	22272(%rbx), %r13
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L43:
	cmpl	$7, (%rbx)
	ja	.L32
	movl	(%rbx), %eax
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8FlagList13ResetAllFlagsEv,"a",@progbits
	.align 4
	.align 4
.L34:
	.long	.L41-.L34
	.long	.L40-.L34
	.long	.L38-.L34
	.long	.L38-.L34
	.long	.L35-.L34
	.long	.L36-.L34
	.long	.L35-.L34
	.long	.L33-.L34
	.section	.text._ZN2v88internal8FlagList13ResetAllFlagsEv
	.p2align 4,,10
	.p2align 3
.L38:
	movq	24(%rbx), %rdx
	movq	16(%rbx), %rax
	movl	(%rdx), %edx
	movl	%edx, (%rax)
.L32:
	addq	$48, %rbx
	cmpq	%r13, %rbx
	jne	.L43
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	24(%rbx), %rdx
	movq	16(%rbx), %rax
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L40:
	movq	16(%rbx), %rax
	xorl	%edx, %edx
	movw	%dx, (%rax)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L41:
	movq	24(%rbx), %rdx
	movq	16(%rbx), %rax
	movzbl	(%rdx), %edx
	movb	%dl, (%rax)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L36:
	movq	24(%rbx), %rdx
	movq	16(%rbx), %rax
	movsd	(%rdx), %xmm0
	movsd	%xmm0, (%rax)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L33:
	movq	24(%rbx), %rax
	cmpb	$0, 40(%rbx)
	movq	16(%rbx), %r14
	movq	(%rax), %r15
	je	.L42
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L42
	call	_ZdaPv@PLT
.L42:
	movq	%r15, (%r14)
	movb	$0, 40(%rbx)
	jmp	.L32
	.cfi_endproc
.LFE9232:
	.size	_ZN2v88internal8FlagList13ResetAllFlagsEv, .-_ZN2v88internal8FlagList13ResetAllFlagsEv
	.section	.rodata._ZN2v88internal8FlagList9PrintHelpEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	"float"
.LC5:
	.string	"size_t"
.LC6:
	.string	"string"
.LC7:
	.string	"maybe_bool"
.LC8:
	.string	"bool"
.LC9:
	.string	"int"
.LC10:
	.string	"uint"
.LC11:
	.string	"uint64"
	.section	.rodata._ZN2v88internal8FlagList9PrintHelpEv.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.ascii	"Synopsis:\n  shell [options] [--shell] [<file>...]\n  d8 [op"
	.ascii	"tions] [-e <string>] [--shell] [[--module] <file>...]\n\n  -"
	.ascii	"e        execute a string in V8\n  --shell   run an interact"
	.ascii	"ive JavaScript shell\n  --module  execute a file as a JavaSc"
	.ascii	"ript module\n\nNote: the --module option is implicitly enabl"
	.ascii	"ed for *.mjs files.\n\nThe following syntax for options is a"
	.ascii	"ccepted (both '-"
	.string	"' and '--' are ok):\n  --flag        (bool flags only)\n  --no-flag     (bool flags only)\n  --flag=value  (non-bool flags only, no spaces around '=')\n  --flag value  (non-bool flags only)\n  --            (captures all remaining args in JavaScript)\n\nOptions:\n"
	.section	.rodata._ZN2v88internal8FlagList9PrintHelpEv.str1.1
.LC13:
	.string	"  --"
.LC14:
	.string	" ("
.LC15:
	.string	"unreachable code"
.LC16:
	.string	"  default: "
.LC17:
	.string	"\n"
.LC18:
	.string	")\n"
.LC19:
	.string	"        type: "
	.section	.text._ZN2v88internal8FlagList9PrintHelpEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8FlagList9PrintHelpEv
	.type	_ZN2v88internal8FlagList9PrintHelpEv, @function
_ZN2v88internal8FlagList9PrintHelpEv:
.LFB9233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal11CpuFeatures12initialized_E(%rip)
	jne	.L50
	xorl	%edi, %edi
	movb	$1, _ZN2v88internal11CpuFeatures12initialized_E(%rip)
	call	_ZN2v88internal11CpuFeatures9ProbeImplEb@PLT
.L50:
	call	_ZN2v88internal11CpuFeatures11PrintTargetEv@PLT
	leaq	-400(%rbp), %rbx
	leaq	_ZN2v88internal12_GLOBAL__N_15flagsE(%rip), %r13
	call	_ZN2v88internal11CpuFeatures13PrintFeaturesEv@PLT
	leaq	-320(%rbp), %rax
	leaq	.L65(%rip), %r14
	movq	%rax, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%rax, -320(%rbp)
	movq	%rbx, %rdi
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	$622, %edx
	movq	%rbx, %rdi
	movq	%rax, -400(%rbp)
	leaq	.LC12(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$4, %edx
	leaq	.LC13(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %r15
	leaq	-401(%rbp), %r12
	movzbl	(%r15), %eax
	testb	%al, %al
	jne	.L51
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L54:
	movb	%al, -401(%rbp)
.L84:
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	addq	$1, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	(%r15), %eax
	testb	%al, %al
	je	.L55
.L51:
	cmpb	$95, %al
	jne	.L54
	movb	$45, -401(%rbp)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$2, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%r13), %r12
	testq	%r12, %r12
	je	.L85
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L57:
	movl	$2, %edx
	leaq	.LC18(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$14, %edx
	leaq	.LC19(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	$7, 0(%r13)
	ja	.L61
	movl	0(%r13), %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8FlagList9PrintHelpEv,"a",@progbits
	.align 4
	.align 4
.L65:
	.long	.L62-.L65
	.long	.L64-.L65
	.long	.L68-.L65
	.long	.L67-.L65
	.long	.L66-.L65
	.long	.L70-.L65
	.long	.L58-.L65
	.long	.L60-.L65
	.section	.text._ZN2v88internal8FlagList9PrintHelpEv
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$4, %edx
	leaq	.LC10(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$11, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	addq	$48, %r13
	call	_ZN2v88internallsERSoRKNS0_12_GLOBAL__N_14FlagE
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	22272+_ZN2v88internal12_GLOBAL__N_15flagsE(%rip), %rax
	cmpq	%r13, %rax
	jne	.L63
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-424(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L86
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movl	$3, %edx
	leaq	.LC9(%rip), %rsi
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$10, %edx
	leaq	.LC7(%rip), %rsi
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L60:
	movl	$6, %edx
	leaq	.LC6(%rip), %rsi
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$6, %edx
	leaq	.LC5(%rip), %rsi
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L70:
	movl	$5, %edx
	leaq	.LC4(%rip), %rsi
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$6, %edx
	leaq	.LC11(%rip), %rsi
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L85:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L57
.L62:
	movl	$4, %edx
	leaq	.LC8(%rip), %rsi
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L61:
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9233:
	.size	_ZN2v88internal8FlagList9PrintHelpEv, .-_ZN2v88internal8FlagList9PrintHelpEv
	.section	.rodata._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"n < static_cast<size_t>(buffer_size)"
	.section	.rodata._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb.str1.1,"aMS",@progbits,1
.LC21:
	.string	"Check failed: %s."
.LC22:
	.string	"Error: unrecognized flag %s\n"
	.section	.rodata._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb.str1.8
	.align 8
.LC23:
	.string	"Error: missing value for flag %s of type %s\n"
	.section	.rodata._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb.str1.1
.LC24:
	.string	""
	.section	.rodata._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb.str1.8
	.align 8
.LC25:
	.string	"Error: Value for flag %s of type %s is out of bounds [0-%lu]\n"
	.align 8
.LC26:
	.string	"Error: illegal value for flag %s of type %s\n"
	.align 8
.LC27:
	.string	"To set or unset a boolean flag, use --flag or --no-flag.\n"
	.align 8
.LC28:
	.string	"The remaining arguments were ignored:"
	.section	.rodata._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb.str1.1
.LC29:
	.string	" %s"
.LC30:
	.string	"Try --help for options\n"
	.section	.text._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb
	.type	_ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb, @function
_ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb:
.LFB9227:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1128, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -1136(%rbp)
	movl	(%rdi), %r8d
	movq	%rdi, -1128(%rbp)
	movq	%rsi, -1112(%rbp)
	movb	%dl, -1130(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %r8d
	jle	.L88
	movl	$0, -1140(%rbp)
	movl	$1, %r12d
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L91:
	movl	%r15d, %r12d
	cmpl	%r8d, %r15d
	jge	.L218
.L89:
	movq	-1112(%rbp), %rax
	movslq	%r12d, %r14
	leal	1(%r12), %r15d
	salq	$3, %r14
	leaq	(%rax,%r14), %r9
	movq	(%r9), %r13
	testq	%r13, %r13
	je	.L91
	cmpb	$45, 0(%r13)
	jne	.L91
	movzbl	1(%r13), %eax
	leaq	1(%r13), %r10
	cmpb	$45, %al
	jne	.L93
	movzbl	2(%r13), %eax
	leaq	2(%r13), %r10
.L93:
	movb	$0, -1129(%rbp)
	cmpb	$110, %al
	je	.L271
.L94:
	testb	%al, %al
	je	.L97
	cmpb	$61, %al
	je	.L232
.L95:
	movq	%r10, %rbx
	.p2align 4,,10
	.p2align 3
.L98:
	movzbl	1(%rbx), %eax
	addq	$1, %rbx
	testb	%al, %al
	je	.L233
	cmpb	$61, %al
	jne	.L98
.L233:
	movq	$0, -1120(%rbp)
	cmpb	$61, %al
	je	.L272
.L100:
	leaq	_ZN2v88internal12_GLOBAL__N_15flagsE(%rip), %rbx
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L212:
	movq	8(%rbx), %r11
	xorl	%eax, %eax
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L273:
	movl	%ecx, %edi
	cmpb	$95, %dl
	je	.L222
.L103:
	cmpb	%dl, %dil
	jne	.L104
.L274:
	testb	%cl, %cl
	je	.L105
.L215:
	addq	$1, %rax
.L106:
	movzbl	(%r10,%rax), %ecx
	movzbl	(%r11,%rax), %edx
	cmpb	$95, %cl
	jne	.L273
	movl	$45, %edi
	cmpb	$95, %dl
	je	.L215
	cmpb	%dl, %dil
	je	.L274
	.p2align 4,,10
	.p2align 3
.L104:
	addq	$1, %rsi
	addq	$48, %rbx
	cmpq	$464, %rsi
	jne	.L212
	cmpb	$0, -1130(%rbp)
	jne	.L91
	movq	stderr(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC22(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L90:
	cmpb	$0, _ZN2v88internal9FLAG_helpE(%rip)
	jne	.L193
.L278:
	cmpb	$0, -1136(%rbp)
	je	.L194
	movq	-1128(%rbp), %rax
	movl	(%rax), %esi
	cmpl	$1, %esi
	jle	.L229
	movq	-1112(%rbp), %r8
	movl	$1, %eax
	movl	$1, %edx
.L199:
	movq	(%r8,%rax,8), %rcx
	testq	%rcx, %rcx
	je	.L196
.L275:
	movq	%rcx, (%r8,%rdx,8)
	movq	-1128(%rbp), %rcx
	addq	$1, %rax
	leal	1(%rdx), %edi
	movl	(%rcx), %esi
	cmpl	%eax, %esi
	jle	.L195
	movq	(%r8,%rax,8), %rcx
	movslq	%edi, %rdx
	testq	%rcx, %rcx
	jne	.L275
.L196:
	addq	$1, %rax
	cmpl	%eax, %esi
	jg	.L199
	movl	%edx, %edi
.L195:
	movq	-1128(%rbp), %rax
	movl	%edi, (%rax)
	testl	%r12d, %r12d
	jne	.L268
.L191:
	xorl	%r12d, %r12d
.L87:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L276
	addq	$1128, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	movl	$45, %edx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L105:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L209
	leaq	.LC24(%rip), %rax
	movzbl	-1129(%rbp), %edx
	movq	%rax, -1096(%rbp)
	movq	16(%rbx), %rax
	xorl	$1, %edx
	movb	%dl, (%rax)
	movl	(%rbx), %eax
.L129:
	cmpl	$1, %eax
	ja	.L168
	cmpq	$0, -1120(%rbp)
	jne	.L166
	movb	$1, -1129(%rbp)
.L167:
	movq	-1096(%rbp), %rdx
	cmpb	$0, (%rdx)
	jne	.L277
	cmpb	$0, -1130(%rbp)
	jne	.L188
.L189:
	movq	-1128(%rbp), %rax
	movl	%r15d, %r12d
	movl	(%rax), %r8d
	cmpl	%r8d, %r15d
	jl	.L89
.L218:
	cmpb	$0, _ZN2v88internal9FLAG_helpE(%rip)
	movl	-1140(%rbp), %r12d
	je	.L278
	.p2align 4,,10
	.p2align 3
.L193:
	call	_ZN2v88internal8FlagList9PrintHelpEv
	xorl	%edi, %edi
	call	exit@PLT
	.p2align 4,,10
	.p2align 3
.L209:
	cmpl	$1, %eax
	je	.L109
	cmpq	$0, -1120(%rbp)
	je	.L279
.L109:
	leaq	.LC24(%rip), %rdi
	movq	%rdi, -1096(%rbp)
	cmpl	$7, %eax
	ja	.L168
	leaq	.L122(%rip), %rdi
	movl	%eax, %ecx
	movslq	(%rdi,%rcx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb,"a",@progbits
	.align 4
	.align 4
.L122:
	.long	.L168-.L122
	.long	.L128-.L122
	.long	.L127-.L122
	.long	.L126-.L122
	.long	.L125-.L122
	.long	.L124-.L122
	.long	.L123-.L122
	.long	.L121-.L122
	.section	.text._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb
	.p2align 4,,10
	.p2align 3
.L124:
	movq	-1120(%rbp), %rdi
	movq	16(%rbx), %r14
	leaq	-1096(%rbp), %rsi
	movq	%r9, -1152(%rbp)
	call	strtod@PLT
	movq	-1152(%rbp), %r9
	movsd	%xmm0, (%r14)
.L130:
	movl	(%rbx), %eax
	cmpl	$1, %eax
	jbe	.L166
.L168:
	cmpb	$0, -1129(%rbp)
	je	.L167
	cmpl	$7, %eax
	ja	.L112
	leaq	.L206(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb
	.align 4
	.align 4
.L206:
	.long	.L112-.L206
	.long	.L112-.L206
	.long	.L182-.L206
	.long	.L183-.L206
	.long	.L184-.L206
	.long	.L185-.L206
	.long	.L186-.L206
	.long	.L187-.L206
	.section	.text._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb
	.p2align 4,,10
	.p2align 3
.L188:
	cmpl	%r15d, %r12d
	jge	.L189
	notl	%r12d
	xorl	%esi, %esi
	movq	%r9, %rdi
	leal	(%r12,%r15), %eax
	leaq	8(,%rax,8), %rdx
	call	memset@PLT
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L271:
	cmpb	$111, 1(%r10)
	jne	.L95
	movzbl	2(%r10), %eax
	cmpb	$45, %al
	sete	%cl
	cmpb	$95, %al
	sete	%dl
	orb	%dl, %cl
	movb	%cl, -1129(%rbp)
	jne	.L96
	movb	$1, -1129(%rbp)
	addq	$2, %r10
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L123:
	movq	16(%rbx), %rax
	movq	%r9, -1152(%rbp)
	movq	%rax, -1160(%rbp)
	call	__errno_location@PLT
	movq	-1120(%rbp), %rdi
	movl	$10, %edx
	leaq	-1096(%rbp), %rsi
	movl	$0, (%rax)
	movq	%rax, %r14
	call	strtoll@PLT
	movq	-1152(%rbp), %r9
	testq	%rax, %rax
	js	.L153
	movl	(%r14), %edx
	testl	%edx, %edx
	jne	.L153
.L154:
	movq	-1160(%rbp), %rcx
	movq	%rax, (%rcx)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L125:
	movq	16(%rbx), %rax
	movq	%r9, -1152(%rbp)
	movq	%rax, -1160(%rbp)
	call	__errno_location@PLT
	movq	-1120(%rbp), %rdi
	movl	$10, %edx
	leaq	-1096(%rbp), %rsi
	movl	$0, (%rax)
	movq	%rax, %r14
	call	strtoll@PLT
	movq	-1152(%rbp), %r9
	testq	%rax, %rax
	js	.L142
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	je	.L154
.L142:
	cmpl	$7, (%rbx)
	ja	.L112
	movl	(%rbx), %eax
	leaq	.L145(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb
	.align 4
	.align 4
.L145:
	.long	.L163-.L145
	.long	.L162-.L145
	.long	.L226-.L145
	.long	.L160-.L145
	.long	.L159-.L145
	.long	.L158-.L145
	.long	.L157-.L145
	.long	.L155-.L145
	.section	.text._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb
	.p2align 4,,10
	.p2align 3
.L126:
	movq	16(%rbx), %rax
	movq	%r9, -1152(%rbp)
	movq	%rax, -1160(%rbp)
	call	__errno_location@PLT
	movq	-1120(%rbp), %rdi
	movl	$10, %edx
	leaq	-1096(%rbp), %rsi
	movl	$0, (%rax)
	movq	%rax, %r14
	call	strtoll@PLT
	movl	$4294967295, %edx
	movq	-1152(%rbp), %r9
	cmpq	%rdx, %rax
	ja	.L131
	movl	(%r14), %esi
	testl	%esi, %esi
	jne	.L131
	movq	-1160(%rbp), %rcx
	movl	%eax, (%rcx)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L127:
	movq	-1120(%rbp), %rdi
	movl	$10, %edx
	leaq	-1096(%rbp), %rsi
	movq	%r9, -1152(%rbp)
	call	strtol@PLT
	movq	16(%rbx), %rdx
	movq	-1152(%rbp), %r9
	movl	%eax, (%rdx)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L128:
	movzbl	-1129(%rbp), %edx
	movq	16(%rbx), %rax
	xorl	$1, %edx
	movb	$1, (%rax)
	movb	%dl, 1(%rax)
	movl	(%rbx), %eax
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L121:
	movq	-1120(%rbp), %rdi
	xorl	%r14d, %r14d
	testq	%rdi, %rdi
	je	.L164
	movq	%rsi, -1160(%rbp)
	movq	%r9, -1152(%rbp)
	call	_ZN2v88internal6StrDupEPKc@PLT
	movq	-1160(%rbp), %rsi
	movq	-1152(%rbp), %r9
	movq	%rax, %r14
.L164:
	leaq	(%rsi,%rsi,2), %rdx
	leaq	_ZN2v88internal12_GLOBAL__N_15flagsE(%rip), %rax
	salq	$4, %rdx
	addq	%rax, %rdx
	cmpb	$0, 40(%rdx)
	movq	16(%rdx), %rax
	je	.L165
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L165
	movq	%rsi, -1168(%rbp)
	movq	%rax, -1160(%rbp)
	movq	%r9, -1152(%rbp)
	call	_ZdaPv@PLT
	movq	-1168(%rbp), %rsi
	movq	-1160(%rbp), %rax
	movq	-1152(%rbp), %r9
.L165:
	movq	%r14, (%rax)
	leaq	(%rsi,%rsi,2), %rax
	leaq	_ZN2v88internal12_GLOBAL__N_15flagsE(%rip), %rcx
	salq	$4, %rax
	movb	$1, 40(%rcx,%rax)
	movl	(%rbx), %eax
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L194:
	testl	%r12d, %r12d
	je	.L191
	movq	-1128(%rbp), %rax
	movq	stderr(%rip), %rdi
	leal	1(%r12), %r14d
	cmpl	(%rax), %r14d
	jl	.L280
.L200:
	leaq	.LC30(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L272:
	movq	%rbx, %rdx
	subq	%r10, %rdx
	cmpq	$1023, %rdx
	ja	.L281
	leaq	-1088(%rbp), %rdi
	leaq	(%rdi,%rdx), %r11
.L216:
	movq	%r10, %rsi
	movl	$1024, %ecx
	movl	%r8d, -1160(%rbp)
	movq	%r11, -1120(%rbp)
	movq	%r9, -1152(%rbp)
	call	__memcpy_chk@PLT
	movq	-1120(%rbp), %r11
	movq	-1152(%rbp), %r9
	movq	%rax, %rdi
	leaq	1(%rbx), %rax
	movl	-1160(%rbp), %r8d
	movb	$0, (%r11)
	movq	%rdi, %r10
	movq	%rax, -1120(%rbp)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L279:
	cmpl	%r8d, %r15d
	jge	.L110
	movq	-1112(%rbp), %rcx
	leal	2(%r12), %r15d
	movq	8(%rcx,%r14), %rdi
	movq	%rdi, -1120(%rbp)
	testq	%rdi, %rdi
	je	.L110
	leaq	.LC24(%rip), %rcx
	movq	%rcx, -1096(%rbp)
	cmpl	$7, %eax
	ja	.L168
	leaq	.L213(%rip), %rdi
	movl	%eax, %ecx
	movslq	(%rdi,%rcx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb
	.align 4
	.align 4
.L213:
	.long	.L168-.L213
	.long	.L168-.L213
	.long	.L127-.L213
	.long	.L126-.L213
	.long	.L125-.L213
	.long	.L124-.L213
	.long	.L123-.L213
	.long	.L121-.L213
	.section	.text._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb
	.p2align 4,,10
	.p2align 3
.L153:
	cmpl	$7, (%rbx)
	ja	.L112
	movl	(%rbx), %eax
	leaq	.L156(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb
	.align 4
	.align 4
.L156:
	.long	.L163-.L156
	.long	.L162-.L156
	.long	.L226-.L156
	.long	.L160-.L156
	.long	.L159-.L156
	.long	.L158-.L156
	.long	.L157-.L156
	.long	.L155-.L156
	.section	.text._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb
	.p2align 4,,10
	.p2align 3
.L131:
	cmpl	$7, (%rbx)
	ja	.L112
	movl	(%rbx), %eax
	leaq	.L134(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb
	.align 4
	.align 4
.L134:
	.long	.L141-.L134
	.long	.L140-.L134
	.long	.L224-.L134
	.long	.L138-.L134
	.long	.L137-.L134
	.long	.L136-.L134
	.long	.L135-.L134
	.long	.L133-.L134
	.section	.text._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb
.L280:
	xorl	%eax, %eax
	leaq	.LC28(%rip), %rsi
	movslq	%r14d, %rbx
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-1128(%rbp), %rax
	leaq	.LC29(%rip), %r13
	cmpl	(%rax), %r14d
	jge	.L202
	movq	-1112(%rbp), %r15
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L203:
	movq	(%r15,%rbx,8), %rdx
	movq	stderr(%rip), %rdi
	movq	%r13, %rsi
	xorl	%eax, %eax
	addq	$1, %rbx
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	cmpl	%ebx, (%r14)
	jg	.L203
.L202:
	movq	stderr(%rip), %rdi
	leaq	.LC17(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L268:
	movq	stderr(%rip), %rdi
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L96:
	movzbl	3(%r10), %eax
	addq	$3, %r10
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L97:
	cmpb	$61, %al
	je	.L232
	movq	$0, -1120(%rbp)
	jmp	.L100
.L277:
	cmpl	$7, %eax
	ja	.L112
	leaq	.L173(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb
	.align 4
	.align 4
.L173:
	.long	.L180-.L173
	.long	.L179-.L173
	.long	.L228-.L173
	.long	.L177-.L173
	.long	.L176-.L173
	.long	.L175-.L173
	.long	.L174-.L173
	.long	.L172-.L173
	.section	.text._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb
	.p2align 4,,10
	.p2align 3
.L232:
	leaq	-1088(%rbp), %rdi
	movq	%r10, %rbx
	xorl	%edx, %edx
	movq	%rdi, %r11
	jmp	.L216
.L281:
	leaq	.LC20(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L110:
	cmpl	$7, %eax
	ja	.L112
	leaq	.L114(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb
	.align 4
	.align 4
.L114:
	.long	.L112-.L114
	.long	.L112-.L114
	.long	.L119-.L114
	.long	.L118-.L114
	.long	.L223-.L114
	.long	.L116-.L114
	.long	.L115-.L114
	.long	.L113-.L114
	.section	.text._ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb
.L166:
	leaq	.LC8(%rip), %rcx
	cmpl	$1, %eax
	jne	.L266
	leaq	.LC7(%rip), %rcx
.L266:
	movq	stderr(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC26(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
.L181:
	movq	stderr(%rip), %rdi
	leaq	.LC27(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	jmp	.L90
.L135:
	leaq	.LC5(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%r9, -1120(%rbp)
	movl	$4294967295, %r8d
.L269:
	movq	stderr(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC25(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movl	%r12d, -1140(%rbp)
	movq	-1120(%rbp), %r9
	jmp	.L130
.L136:
	leaq	.LC4(%rip), %rcx
	jmp	.L139
.L137:
	leaq	.LC11(%rip), %rcx
	jmp	.L139
.L138:
	leaq	.LC10(%rip), %rcx
	jmp	.L139
.L224:
	leaq	.LC9(%rip), %rcx
	jmp	.L139
.L140:
	leaq	.LC7(%rip), %rcx
	jmp	.L139
.L141:
	leaq	.LC8(%rip), %rcx
	jmp	.L139
.L133:
	leaq	.LC6(%rip), %rcx
	jmp	.L139
.L157:
	leaq	.LC5(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%r9, -1120(%rbp)
	movq	$-1, %r8
	jmp	.L269
.L158:
	leaq	.LC4(%rip), %rcx
	jmp	.L161
.L159:
	leaq	.LC11(%rip), %rcx
	jmp	.L161
.L160:
	leaq	.LC10(%rip), %rcx
	jmp	.L161
.L226:
	leaq	.LC9(%rip), %rcx
	jmp	.L161
.L162:
	leaq	.LC7(%rip), %rcx
	jmp	.L161
.L163:
	leaq	.LC8(%rip), %rcx
	jmp	.L161
.L155:
	leaq	.LC6(%rip), %rcx
	jmp	.L161
.L172:
	leaq	.LC6(%rip), %rcx
.L178:
	movq	stderr(%rip), %rdi
	xorl	%eax, %eax
	movq	%r13, %rdx
	leaq	.LC26(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	cmpb	$0, -1129(%rbp)
	je	.L90
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L174:
	leaq	.LC5(%rip), %rcx
	jmp	.L178
.L177:
	leaq	.LC10(%rip), %rcx
	jmp	.L178
.L228:
	leaq	.LC9(%rip), %rcx
	jmp	.L178
.L179:
	leaq	.LC7(%rip), %rcx
	jmp	.L178
.L180:
	leaq	.LC8(%rip), %rcx
	jmp	.L178
.L175:
	leaq	.LC4(%rip), %rcx
	jmp	.L178
.L176:
	leaq	.LC11(%rip), %rcx
	jmp	.L178
.L185:
	leaq	.LC4(%rip), %rcx
.L267:
	movq	stderr(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC26(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	jmp	.L90
.L184:
	leaq	.LC11(%rip), %rcx
	jmp	.L267
.L183:
	leaq	.LC10(%rip), %rcx
	jmp	.L267
.L182:
	leaq	.LC9(%rip), %rcx
	jmp	.L267
.L186:
	leaq	.LC5(%rip), %rcx
	jmp	.L267
.L187:
	leaq	.LC6(%rip), %rcx
	jmp	.L267
.L113:
	leaq	.LC6(%rip), %rcx
.L117:
	movq	stderr(%rip), %rdi
	movq	%r13, %rdx
	leaq	.LC23(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	jmp	.L90
.L115:
	leaq	.LC5(%rip), %rcx
	jmp	.L117
.L116:
	leaq	.LC4(%rip), %rcx
	jmp	.L117
.L223:
	leaq	.LC11(%rip), %rcx
	jmp	.L117
.L118:
	leaq	.LC10(%rip), %rcx
	jmp	.L117
.L119:
	leaq	.LC9(%rip), %rcx
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L229:
	movl	$1, %edi
	jmp	.L195
.L88:
	cmpb	$0, _ZN2v88internal9FLAG_helpE(%rip)
	jne	.L193
	cmpb	$0, -1136(%rbp)
	je	.L191
	movl	$1, (%rdi)
	jmp	.L191
.L112:
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L276:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9227:
	.size	_ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb, .-_ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb
	.section	.rodata._ZN2v88internal8FlagList18SetFlagsFromStringEPKcm.str1.1,"aMS",@progbits,1
.LC31:
	.string	"NewArray"
	.section	.text._ZN2v88internal8FlagList18SetFlagsFromStringEPKcm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8FlagList18SetFlagsFromStringEPKcm
	.type	_ZN2v88internal8FlagList18SetFlagsFromStringEPKcm, @function
_ZN2v88internal8FlagList18SetFlagsFromStringEPKcm:
.LFB9230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	1(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	_ZSt7nothrow(%rip), %rsi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L331
.L283:
	movq	-72(%rbp), %r15
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	memcpy@PLT
	movb	$0, (%r15,%rbx)
	movzbl	(%r15), %eax
	movq	%r15, %rbx
	testb	%al, %al
	jne	.L286
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L332:
	movzbl	1(%rbx), %eax
	addq	$1, %rbx
	testb	%al, %al
	je	.L284
.L286:
	movsbl	%al, %r12d
	movl	%r12d, %edi
	call	isspace@PLT
	testl	%eax, %eax
	jne	.L332
	movl	$1, -60(%rbp)
	movq	%rbx, %r14
	movl	$1, %r13d
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L333:
	movzbl	1(%r14), %eax
	addq	$1, %r14
	testb	%al, %al
	je	.L292
	movsbl	%al, %r12d
.L287:
	movl	%r12d, %edi
	call	isspace@PLT
	testl	%eax, %eax
	je	.L333
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L291:
	movzbl	1(%r14), %eax
	addq	$1, %r14
	testb	%al, %al
	je	.L292
	movsbl	%al, %r12d
.L288:
	movl	%r12d, %edi
	call	isspace@PLT
	testl	%eax, %eax
	jne	.L291
	addl	$1, %r13d
	movl	%r13d, -60(%rbp)
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L292:
	addl	$1, %r13d
	movl	%r13d, -60(%rbp)
	movslq	%r13d, %r13
.L303:
	salq	$3, %r13
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r13, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L334
.L293:
	movsbl	(%rbx), %edi
	movl	$1, -60(%rbp)
	movl	$1, %r12d
	testb	%dil, %dil
	je	.L296
	.p2align 4,,10
	.p2align 3
.L295:
	movq	%rbx, (%r15,%r12,8)
	movl	%r12d, %r14d
	movl	%r12d, %r13d
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L335:
	testb	%dil, %dil
	je	.L299
.L297:
	call	isspace@PLT
	movsbl	1(%rbx), %edi
	movq	%rbx, %rdx
	addq	$1, %rbx
	testl	%eax, %eax
	je	.L335
	movb	$0, (%rdx)
	testb	%dil, %dil
	jne	.L301
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L336:
	movsbl	1(%rbx), %edi
	addq	$1, %rbx
	testb	%dil, %dil
	je	.L299
.L301:
	call	isspace@PLT
	testl	%eax, %eax
	jne	.L336
	movsbl	(%rbx), %edi
	addl	$1, %r14d
	addq	$1, %r12
	movl	%r14d, -60(%rbp)
	testb	%dil, %dil
	jne	.L295
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L299:
	addl	$1, %r13d
	movl	%r13d, -60(%rbp)
.L296:
	xorl	%edx, %edx
	movq	%r15, %rsi
	leaq	-60(%rbp), %rdi
	call	_ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	_ZdaPv@PLT
	movq	-72(%rbp), %rdi
	call	_ZdaPv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L337
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L305:
	.cfi_restore_state
	movq	-72(%rbp), %rbx
.L284:
	movl	$1, -60(%rbp)
	movl	$1, %r13d
	jmp	.L303
.L331:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r13, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	jne	.L283
.L294:
	leaq	.LC31(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L337:
	call	__stack_chk_fail@PLT
.L334:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r13, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L293
	jmp	.L294
	.cfi_endproc
.LFE9230:
	.size	_ZN2v88internal8FlagList18SetFlagsFromStringEPKcm, .-_ZN2v88internal8FlagList18SetFlagsFromStringEPKcm
	.section	.rodata._ZN2v88internal19ComputeFlagListHashEv.str1.1,"aMS",@progbits,1
.LC32:
	.string	"embedded"
	.section	.text._ZN2v88internal19ComputeFlagListHashEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19ComputeFlagListHashEv
	.type	_ZN2v88internal19ComputeFlagListHashEv, @function
_ZN2v88internal19ComputeFlagListHashEv:
.LFB9234:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.L344(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZN2v88internal12_GLOBAL__N_15flagsE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-320(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$456, %rsp
	movq	.LC33(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -488(%rbp)
	movhps	.LC34(%rip), %xmm1
	movaps	%xmm1, -480(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movq	%rax, -432(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movdqa	-480(%rbp), %xmm1
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -496(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rbx, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	leaq	_ZN2v88internal28FLAG_profile_deserializationE(%rip), %rbx
	movq	%rax, -480(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$8, %edx
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L372:
	movq	16(%r13), %rax
	cmpq	%rbx, %rax
	je	.L340
	movq	24(%r13), %rdx
	movzbl	(%rax), %eax
	cmpb	%al, (%rdx)
	sete	%al
	.p2align 4,,10
	.p2align 3
.L351:
	testb	%al, %al
	je	.L353
.L340:
	addq	$1, %r14
	addq	$48, %r13
	cmpq	$464, %r14
	je	.L371
.L354:
	movl	0(%r13), %eax
	testl	%eax, %eax
	je	.L372
	cmpl	$7, %eax
	ja	.L342
	movslq	(%r15,%rax,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal19ComputeFlagListHashEv,"a",@progbits
	.align 4
	.align 4
.L344:
	.long	.L342-.L344
	.long	.L350-.L344
	.long	.L348-.L344
	.long	.L348-.L344
	.long	.L345-.L344
	.long	.L346-.L344
	.long	.L345-.L344
	.long	.L343-.L344
	.section	.text._ZN2v88internal19ComputeFlagListHashEv
.L348:
	movq	16(%r13), %rdx
	movq	24(%r13), %rax
	movl	(%rax), %eax
	cmpl	%eax, (%rdx)
	sete	%al
	testb	%al, %al
	jne	.L340
.L353:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internallsERSoRKNS0_12_GLOBAL__N_14FlagE
	jmp	.L340
.L345:
	movq	16(%r13), %rdx
	movq	24(%r13), %rax
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	sete	%al
	jmp	.L351
.L350:
	movq	16(%r13), %rax
	movzbl	(%rax), %eax
	xorl	$1, %eax
	jmp	.L351
.L343:
	movq	16(%r13), %rax
	movq	(%rax), %rdi
	movq	24(%r13), %rax
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	sete	%al
	testq	%rsi, %rsi
	je	.L351
	testq	%rdi, %rdi
	je	.L353
	call	strcmp@PLT
	testl	%eax, %eax
	sete	%al
	jmp	.L351
.L346:
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	movl	$0, %ecx
	movsd	(%rax), %xmm0
	ucomisd	(%rdx), %xmm0
	setnp	%al
	cmovne	%ecx, %eax
	jmp	.L351
.L371:
	movq	-384(%rbp), %rax
	leaq	-448(%rbp), %r12
	movq	$0, -456(%rbp)
	leaq	-464(%rbp), %rdi
	movq	%r12, -464(%rbp)
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L355
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L373
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L357:
	movq	-464(%rbp), %r13
	movq	-456(%rbp), %rbx
	addq	%r13, %rbx
	cmpq	%r13, %rbx
	je	.L363
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L359:
	call	_ZN2v84base10hash_valueEm@PLT
	movsbl	0(%r13), %edi
	addq	$1, %r13
	movq	%rax, %r14
	call	_ZN2v84base10hash_valueEj@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %rdi
	cmpq	%r13, %rbx
	jne	.L359
	movq	-464(%rbp), %r13
.L358:
	movl	%edi, _ZN2v88internalL9flag_hashE(%rip)
	cmpq	%r12, %r13
	je	.L360
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L360:
	movq	.LC33(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC35(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-480(%rbp), %rdi
	je	.L361
	call	_ZdlPv@PLT
.L361:
	movq	-496(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-488(%rbp), %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L374
	addq	$456, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L373:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L357
.L355:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L357
.L363:
	xorl	%edi, %edi
	jmp	.L358
.L342:
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L374:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9234:
	.size	_ZN2v88internal19ComputeFlagListHashEv, .-_ZN2v88internal19ComputeFlagListHashEv
	.section	.text._ZN2v88internal8FlagList23EnforceFlagImplicationsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8FlagList23EnforceFlagImplicationsEv
	.type	_ZN2v88internal8FlagList23EnforceFlagImplicationsEv, @function
_ZN2v88internal8FlagList23EnforceFlagImplicationsEv:
.LFB9235:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal15FLAG_es_stagingE(%rip)
	movzbl	_ZN2v88internal24FLAG_harmony_import_metaE(%rip), %eax
	je	.L376
	movb	$1, _ZN2v88internal12FLAG_harmonyE(%rip)
	testb	%al, %al
	jne	.L462
.L377:
	movb	$1, _ZN2v88internal47FLAG_harmony_intl_add_calendar_numbering_systemE(%rip)
	movb	$1, _ZN2v88internal39FLAG_harmony_intl_dateformat_day_periodE(%rip)
	movb	$1, _ZN2v88internal53FLAG_harmony_intl_dateformat_fractional_second_digitsE(%rip)
	movb	$1, _ZN2v88internal27FLAG_harmony_intl_segmenterE(%rip)
.L378:
	cmpb	$0, _ZN2v88internal21FLAG_harmony_shippingE(%rip)
	jne	.L380
	movb	$0, _ZN2v88internal30FLAG_harmony_namespace_exportsE(%rip)
	movb	$0, _ZN2v88internal30FLAG_harmony_sharedarraybufferE(%rip)
	movb	$0, _ZN2v88internal24FLAG_harmony_import_metaE(%rip)
	movb	$0, _ZN2v88internal27FLAG_harmony_dynamic_importE(%rip)
	movb	$0, _ZN2v88internal32FLAG_harmony_promise_all_settledE(%rip)
	movb	$0, _ZN2v88internal24FLAG_harmony_intl_bigintE(%rip)
	movb	$0, _ZN2v88internal35FLAG_harmony_intl_date_format_rangeE(%rip)
	movb	$0, _ZN2v88internal32FLAG_harmony_intl_datetime_styleE(%rip)
	movb	$0, _ZN2v88internal38FLAG_harmony_intl_numberformat_unifiedE(%rip)
.L380:
	movzbl	_ZN2v88internal14FLAG_lite_modeE(%rip), %eax
	testb	%al, %al
	jne	.L381
	movzbl	_ZN2v88internal12FLAG_jitlessE(%rip), %eax
	movzbl	_ZN2v88internal22FLAG_optimize_for_sizeE(%rip), %esi
.L382:
	movzbl	_ZN2v88internal11FLAG_futureE(%rip), %edx
	testb	%dl, %dl
	je	.L383
	movb	$1, _ZN2v88internal30FLAG_write_protect_code_memoryE(%rip)
.L383:
	cmpb	$0, _ZN2v88internal24FLAG_track_double_fieldsE(%rip)
	je	.L384
	movb	$1, _ZN2v88internal17FLAG_track_fieldsE(%rip)
.L384:
	cmpb	$0, _ZN2v88internal29FLAG_track_heap_object_fieldsE(%rip)
	je	.L385
	movb	$1, _ZN2v88internal17FLAG_track_fieldsE(%rip)
.L385:
	cmpb	$0, _ZN2v88internal26FLAG_track_computed_fieldsE(%rip)
	je	.L386
	movb	$1, _ZN2v88internal17FLAG_track_fieldsE(%rip)
.L386:
	cmpb	$0, _ZN2v88internal22FLAG_track_field_typesE(%rip)
	je	.L387
	movb	$1, _ZN2v88internal17FLAG_track_fieldsE(%rip)
	movb	$1, _ZN2v88internal29FLAG_track_heap_object_fieldsE(%rip)
.L387:
	testb	%al, %al
	jne	.L388
	movzbl	_ZN2v88internal17FLAG_validate_asmE(%rip), %ecx
	movzbl	_ZN2v88internal23FLAG_wasm_interpret_allE(%rip), %eax
.L389:
	testb	%dl, %dl
	je	.L390
	movb	$1, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
.L390:
	cmpb	$0, _ZN2v88internal30FLAG_trace_heap_broker_verboseE(%rip)
	je	.L391
	movb	$1, _ZN2v88internal22FLAG_trace_heap_brokerE(%rip)
.L391:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_scheduledE(%rip)
	je	.L392
	movb	$1, _ZN2v88internal22FLAG_trace_turbo_graphE(%rip)
.L392:
	cmpb	$0, _ZN2v88internal18FLAG_stress_inlineE(%rip)
	je	.L393
	movl	$999999, _ZN2v88internal30FLAG_max_inlined_bytecode_sizeE(%rip)
	movl	$999999, _ZN2v88internal41FLAG_max_inlined_bytecode_size_cumulativeE(%rip)
	movl	$999999, _ZN2v88internal39FLAG_max_inlined_bytecode_size_absoluteE(%rip)
	movq	$0x000000000, _ZN2v88internal27FLAG_min_inlining_frequencyE(%rip)
	movb	$1, _ZN2v88internal25FLAG_polymorphic_inliningE(%rip)
.L393:
	testb	%sil, %sil
	je	.L394
	movq	$1, _ZN2v88internal24FLAG_max_semi_space_sizeE(%rip)
.L394:
	cmpb	$0, _ZN2v88internal17FLAG_wasm_tier_upE(%rip)
	je	.L395
	movb	$1, _ZN2v88internal12FLAG_liftoffE(%rip)
.L395:
	cmpb	$0, _ZN2v88internal17FLAG_wasm_stagingE(%rip)
	je	.L396
	movb	$1, _ZN2v88internal29FLAG_experimental_wasm_anyrefE(%rip)
	movb	$1, _ZN2v88internal38FLAG_experimental_wasm_type_reflectionE(%rip)
.L396:
	testb	%dl, %dl
	je	.L397
	movb	$1, _ZN2v88internal23FLAG_wasm_shared_engineE(%rip)
	movb	$1, _ZN2v88internal21FLAG_wasm_shared_codeE(%rip)
.L397:
	cmpb	$0, _ZN2v88internal25FLAG_wasm_fuzzer_gen_testE(%rip)
	je	.L398
	movb	$1, _ZN2v88internal20FLAG_single_threadedE(%rip)
.L398:
	testb	%cl, %cl
	je	.L399
	movb	$1, _ZN2v88internal30FLAG_asm_wasm_lazy_compilationE(%rip)
.L399:
	testb	%al, %al
	je	.L400
	movb	$0, _ZN2v88internal30FLAG_asm_wasm_lazy_compilationE(%rip)
	movb	$0, _ZN2v88internal26FLAG_wasm_lazy_compilationE(%rip)
	movb	$0, _ZN2v88internal17FLAG_wasm_tier_upE(%rip)
.L400:
	cmpb	$0, _ZN2v88internal21FLAG_trace_gc_verboseE(%rip)
	je	.L401
	movb	$1, _ZN2v88internal13FLAG_trace_gcE(%rip)
.L401:
	cmpb	$0, _ZN2v88internal31FLAG_trace_gc_freelists_verboseE(%rip)
	je	.L402
	movb	$1, _ZN2v88internal23FLAG_trace_gc_freelistsE(%rip)
.L402:
	cmpb	$0, _ZN2v88internal26FLAG_trace_gc_object_statsE(%rip)
	je	.L403
	movb	$1, _ZN2v88internal26FLAG_track_gc_object_statsE(%rip)
.L404:
	movl	$1, _ZN2v88internal12TracingFlags8gc_statsE(%rip)
	mfence
	cmpb	$0, _ZN2v88internal26FLAG_trace_gc_object_statsE(%rip)
	je	.L407
	movl	$1, _ZN2v88internal12TracingFlags8gc_statsE(%rip)
	mfence
	cmpb	$0, _ZN2v88internal26FLAG_trace_gc_object_statsE(%rip)
	je	.L407
	movb	$0, _ZN2v88internal24FLAG_incremental_markingE(%rip)
.L407:
	cmpb	$0, _ZN2v88internal25FLAG_track_retaining_pathE(%rip)
	je	.L408
	movb	$0, _ZN2v88internal24FLAG_incremental_markingE(%rip)
	movb	$0, _ZN2v88internal21FLAG_parallel_markingE(%rip)
	movb	$0, _ZN2v88internal23FLAG_concurrent_markingE(%rip)
.L408:
	cmpb	$0, _ZN2v88internal28FLAG_trace_detached_contextsE(%rip)
	je	.L409
	movb	$1, _ZN2v88internal28FLAG_track_detached_contextsE(%rip)
.L409:
	cmpb	$0, _ZN2v88internal26FLAG_stress_flush_bytecodeE(%rip)
	je	.L410
	movb	$1, _ZN2v88internal19FLAG_flush_bytecodeE(%rip)
.L410:
	cmpb	$0, _ZN2v88internal23FLAG_fuzzer_gc_analysisE(%rip)
	je	.L411
	movl	$1, _ZN2v88internal19FLAG_stress_markingE(%rip)
	movl	$1, _ZN2v88internal20FLAG_stress_scavengeE(%rip)
.L411:
	cmpq	$0, _ZN2v88internal17FLAG_expose_gc_asE(%rip)
	je	.L412
	movb	$1, _ZN2v88internal14FLAG_expose_gcE(%rip)
.L412:
	cmpb	$0, _ZN2v88internal15FLAG_inline_newE(%rip)
	jne	.L413
	movb	$0, _ZN2v88internal29FLAG_turbo_allocation_foldingE(%rip)
.L413:
	cmpb	$0, _ZN2v88internal13FLAG_max_lazyE(%rip)
	je	.L414
	movb	$1, _ZN2v88internal9FLAG_lazyE(%rip)
.L414:
	cmpb	$0, _ZN2v88internal22FLAG_trace_opt_verboseE(%rip)
	je	.L415
	movb	$1, _ZN2v88internal14FLAG_trace_optE(%rip)
.L415:
	cmpb	$0, _ZN2v88internal27FLAG_parallel_compile_tasksE(%rip)
	je	.L416
	movb	$1, _ZN2v88internal24FLAG_compiler_dispatcherE(%rip)
.L416:
	cmpb	$0, _ZN2v88internal13FLAG_trace_icE(%rip)
	je	.L417
	movb	$1, _ZN2v88internal13FLAG_log_codeE(%rip)
	movl	$1, _ZN2v88internal12TracingFlags8ic_statsE(%rip)
	mfence
.L417:
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	je	.L418
	movb	$1, _ZN2v88internal13FLAG_log_codeE(%rip)
.L418:
	cmpb	$0, _ZN2v88internal23FLAG_runtime_call_statsE(%rip)
	jne	.L463
.L419:
	cmpb	$0, _ZN2v88internal25FLAG_regexp_interpret_allE(%rip)
	je	.L420
	movb	$0, _ZN2v88internal19FLAG_regexp_tier_upE(%rip)
.L420:
	cmpb	$0, _ZN2v88internal9FLAG_profE(%rip)
	je	.L421
	movb	$1, _ZN2v88internal13FLAG_prof_cppE(%rip)
.L421:
	cmpb	$0, _ZN2v88internal20FLAG_perf_basic_profE(%rip)
	je	.L422
	movb	$0, _ZN2v88internal23FLAG_compact_code_spaceE(%rip)
.L422:
	cmpb	$0, _ZN2v88internal35FLAG_perf_basic_prof_only_functionsE(%rip)
	je	.L423
	movb	$1, _ZN2v88internal20FLAG_perf_basic_profE(%rip)
.L423:
	cmpb	$0, _ZN2v88internal14FLAG_perf_profE(%rip)
	je	.L424
	movb	$0, _ZN2v88internal23FLAG_compact_code_spaceE(%rip)
	movb	$0, _ZN2v88internal30FLAG_write_protect_code_memoryE(%rip)
	movb	$0, _ZN2v88internal35FLAG_wasm_write_protect_code_memoryE(%rip)
	movb	$1, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
.L424:
	cmpb	$0, _ZN2v88internal30FLAG_log_internal_timer_eventsE(%rip)
	je	.L425
	movb	$1, _ZN2v88internal9FLAG_profE(%rip)
.L425:
	cmpb	$0, _ZN2v88internal11FLAG_sodiumE(%rip)
	je	.L426
	movb	$1, _ZN2v88internal15FLAG_print_codeE(%rip)
	movb	$1, _ZN2v88internal19FLAG_print_opt_codeE(%rip)
	movb	$1, _ZN2v88internal18FLAG_code_commentsE(%rip)
.L426:
	cmpb	$0, _ZN2v88internal19FLAG_print_all_codeE(%rip)
	je	.L427
	movb	$1, _ZN2v88internal15FLAG_print_codeE(%rip)
	movb	$1, _ZN2v88internal19FLAG_print_opt_codeE(%rip)
	movb	$1, _ZN2v88internal23FLAG_print_code_verboseE(%rip)
	movb	$1, _ZN2v88internal23FLAG_print_builtin_codeE(%rip)
	movb	$1, _ZN2v88internal22FLAG_print_regexp_codeE(%rip)
	movb	$1, _ZN2v88internal18FLAG_code_commentsE(%rip)
.L427:
	cmpb	$0, _ZN2v88internal16FLAG_predictableE(%rip)
	movzbl	_ZN2v88internal28FLAG_predictable_gc_scheduleE(%rip), %eax
	je	.L428
	movb	$1, _ZN2v88internal20FLAG_single_threadedE(%rip)
	movb	$0, _ZN2v88internal19FLAG_memory_reducerE(%rip)
.L429:
	movl	$0, _ZN2v88internal31FLAG_wasm_num_compilation_tasksE(%rip)
	movb	$0, _ZN2v88internal27FLAG_wasm_async_compilationE(%rip)
	testb	%al, %al
	jne	.L431
.L436:
	movb	$1, _ZN2v88internal23FLAG_single_threaded_gcE(%rip)
	movb	$0, _ZN2v88internal29FLAG_concurrent_recompilationE(%rip)
	movb	$0, _ZN2v88internal24FLAG_compiler_dispatcherE(%rip)
.L432:
	movb	$0, _ZN2v88internal23FLAG_concurrent_markingE(%rip)
	movb	$0, _ZN2v88internal24FLAG_concurrent_sweepingE(%rip)
	movb	$0, _ZN2v88internal24FLAG_parallel_compactionE(%rip)
	movb	$0, _ZN2v88internal21FLAG_parallel_markingE(%rip)
	movb	$0, _ZN2v88internal28FLAG_parallel_pointer_updateE(%rip)
	movb	$0, _ZN2v88internal22FLAG_parallel_scavengeE(%rip)
	movb	$0, _ZN2v88internal28FLAG_concurrent_store_bufferE(%rip)
	movb	$0, _ZN2v88internal30FLAG_minor_mc_parallel_markingE(%rip)
	movb	$0, _ZN2v88internal36FLAG_concurrent_array_buffer_freeingE(%rip)
	movb	$1, _ZN2v88internal24FLAG_track_double_fieldsE(%rip)
	jmp	_ZN2v88internal19ComputeFlagListHashEv
	.p2align 4,,10
	.p2align 3
.L403:
	cmpb	$0, _ZN2v88internal26FLAG_track_gc_object_statsE(%rip)
	je	.L407
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L428:
	cmpb	$0, _ZN2v88internal20FLAG_single_threadedE(%rip)
	jne	.L429
	testb	%al, %al
	je	.L435
	movq	$4, _ZN2v88internal24FLAG_min_semi_space_sizeE(%rip)
	movq	$4, _ZN2v88internal24FLAG_max_semi_space_sizeE(%rip)
	movl	$30, _ZN2v88internal25FLAG_heap_growing_percentE(%rip)
	movb	$0, _ZN2v88internal23FLAG_idle_time_scavengeE(%rip)
	movb	$0, _ZN2v88internal19FLAG_memory_reducerE(%rip)
.L435:
	cmpb	$0, _ZN2v88internal23FLAG_single_threaded_gcE(%rip)
	jne	.L432
	movb	$1, _ZN2v88internal24FLAG_track_double_fieldsE(%rip)
	jmp	_ZN2v88internal19ComputeFlagListHashEv
	.p2align 4,,10
	.p2align 3
.L388:
	movb	$0, _ZN2v88internal8FLAG_optE(%rip)
	xorl	%ecx, %ecx
	movb	$0, _ZN2v88internal22FLAG_track_field_typesE(%rip)
	movb	$0, _ZN2v88internal29FLAG_track_heap_object_fieldsE(%rip)
	movb	$1, _ZN2v88internal25FLAG_regexp_interpret_allE(%rip)
	movb	$0, _ZN2v88internal17FLAG_validate_asmE(%rip)
	movb	$1, _ZN2v88internal23FLAG_wasm_interpret_allE(%rip)
	movb	$0, _ZN2v88internal30FLAG_asm_wasm_lazy_compilationE(%rip)
	movb	$0, _ZN2v88internal26FLAG_wasm_lazy_compilationE(%rip)
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L381:
	movb	$1, _ZN2v88internal12FLAG_jitlessE(%rip)
	movl	%eax, %esi
	movb	$1, _ZN2v88internal29FLAG_lazy_feedback_allocationE(%rip)
	movb	$1, _ZN2v88internal22FLAG_optimize_for_sizeE(%rip)
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L431:
	movq	$4, _ZN2v88internal24FLAG_min_semi_space_sizeE(%rip)
	movq	$4, _ZN2v88internal24FLAG_max_semi_space_sizeE(%rip)
	movl	$30, _ZN2v88internal25FLAG_heap_growing_percentE(%rip)
	movb	$0, _ZN2v88internal23FLAG_idle_time_scavengeE(%rip)
	movb	$0, _ZN2v88internal19FLAG_memory_reducerE(%rip)
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L462:
	movb	$1, _ZN2v88internal27FLAG_harmony_dynamic_importE(%rip)
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L376:
	movzbl	_ZN2v88internal12FLAG_harmonyE(%rip), %edx
	testb	%al, %al
	je	.L379
	movb	$1, _ZN2v88internal27FLAG_harmony_dynamic_importE(%rip)
.L379:
	testb	%dl, %dl
	je	.L378
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L463:
	movl	$1, _ZN2v88internal12TracingFlags13runtime_statsE(%rip)
	mfence
	jmp	.L419
	.cfi_endproc
.LFE9235:
	.size	_ZN2v88internal8FlagList23EnforceFlagImplicationsEv, .-_ZN2v88internal8FlagList23EnforceFlagImplicationsEv
	.section	.text._ZN2v88internal8FlagList4HashEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8FlagList4HashEv
	.type	_ZN2v88internal8FlagList4HashEv, @function
_ZN2v88internal8FlagList4HashEv:
.LFB9236:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internalL9flag_hashE(%rip), %eax
	ret
	.cfi_endproc
.LFE9236:
	.size	_ZN2v88internal8FlagList4HashEv, .-_ZN2v88internal8FlagList4HashEv
	.section	.rodata._ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_.str1.1,"aMS",@progbits,1
.LC37:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB10487:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L479
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L475
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L480
.L467:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L474:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L481
	testq	%r13, %r13
	jg	.L470
	testq	%r9, %r9
	jne	.L473
.L471:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L470
.L473:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L480:
	testq	%rsi, %rsi
	jne	.L468
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L470:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L471
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L475:
	movl	$8, %r14d
	jmp	.L467
.L479:
	leaq	.LC37(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L468:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L467
	.cfi_endproc
.LFE10487:
	.size	_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.rodata._ZN2v88internal8FlagList4argvEv.str1.1,"aMS",@progbits,1
.LC38:
	.string	"--no"
.LC39:
	.string	"--"
	.section	.text._ZN2v88internal8FlagList4argvEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8FlagList4argvEv
	.type	_ZN2v88internal8FlagList4argvEv, @function
_ZN2v88internal8FlagList4argvEv:
.LFB9221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	_ZN2v88internal12_GLOBAL__N_15flagsE(%rip), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$520, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	$0, 16(%rax)
	movq	%rax, %r15
	movups	%xmm0, (%rax)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r15, -512(%rbp)
	movq	.LC33(%rip), %xmm5
	leaq	64(%rax), %rdx
	movq	%rax, (%r15)
	movhps	.LC34(%rip), %xmm5
	movq	%rdx, 16(%r15)
	movq	%rdx, 8(%r15)
	movq	%r14, %r15
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movaps	%xmm5, -528(%rbp)
	.p2align 4,,10
	.p2align 3
.L515:
	cmpl	$7, (%r15)
	ja	.L483
	movl	(%r15), %eax
	leaq	.L485(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8FlagList4argvEv,"a",@progbits
	.align 4
	.align 4
.L485:
	.long	.L492-.L485
	.long	.L491-.L485
	.long	.L489-.L485
	.long	.L489-.L485
	.long	.L486-.L485
	.long	.L487-.L485
	.long	.L486-.L485
	.long	.L484-.L485
	.section	.text._ZN2v88internal8FlagList4argvEv
	.p2align 4,,10
	.p2align 3
.L489:
	movq	16(%r15), %rdx
	movq	24(%r15), %rax
	movl	(%rax), %eax
	cmpl	%eax, (%rdx)
	sete	%al
.L495:
	testb	%al, %al
	je	.L497
.L494:
	addq	$48, %r15
	leaq	22272+_ZN2v88internal12_GLOBAL__N_15flagsE(%rip), %rax
	cmpq	%r15, %rax
	jne	.L515
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	movq	-512(%rbp), %r15
	jne	.L526
	addq	$520, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L486:
	.cfi_restore_state
	movq	16(%r15), %rdx
	movq	24(%r15), %rax
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	sete	%al
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L491:
	movq	16(%r15), %rax
	movzbl	(%rax), %eax
	xorl	$1, %eax
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L492:
	movq	16(%r15), %rax
	movq	24(%r15), %rdx
	movzbl	(%rax), %eax
	cmpb	(%rdx), %al
	je	.L494
	xorl	$1, %eax
.L497:
	leaq	-320(%rbp), %r12
	movb	%al, -496(%rbp)
	leaq	-432(%rbp), %r13
	movq	%r12, %rdi
	leaq	-368(%rbp), %r14
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%cx, -96(%rbp)
	movq	%rax, -320(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rbx, -432(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rbx), %rdx
	movq	%rax, -432(%rbp,%rdx)
	movq	-432(%rbp), %rdx
	movq	-24(%rdx), %rdi
	addq	%r13, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-528(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm2, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	movl	$16, -360(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -488(%rbp)
	movq	%rax, -352(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -552(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movzbl	-496(%rbp), %eax
	leaq	.LC38(%rip), %rsi
	movq	%r13, %rdi
	cmpb	$1, %al
	sbbq	%rdx, %rdx
	andq	$-2, %rdx
	addq	$4, %rdx
	testb	%al, %al
	leaq	.LC39(%rip), %rax
	cmove	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r15), %rsi
	testq	%rsi, %rsi
	je	.L527
	movq	%rsi, %rdi
	movq	%rsi, -496(%rbp)
	call	strlen@PLT
	movq	-496(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L499:
	leaq	-448(%rbp), %rax
	leaq	-464(%rbp), %rdi
	movq	$0, -456(%rbp)
	movq	%rax, -496(%rbp)
	movq	%rax, -464(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -504(%rbp)
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L500
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L501
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L502:
	movq	-464(%rbp), %rdi
	call	_ZN2v88internal6StrDupEPKc@PLT
	movq	-512(%rbp), %rdx
	movq	%rax, -472(%rbp)
	movq	8(%rdx), %rsi
	cmpq	16(%rdx), %rsi
	je	.L503
	movq	%rax, (%rsi)
	addq	$8, 8(%rdx)
.L504:
	movq	-464(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	je	.L505
	call	_ZdlPv@PLT
.L505:
	movq	.LC33(%rip), %xmm1
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC35(%rip), %xmm1
	movaps	%xmm1, -544(%rbp)
	movaps	%xmm1, -432(%rbp)
	cmpq	-488(%rbp), %rdi
	je	.L506
	call	_ZdlPv@PLT
.L506:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%rbx, -432(%rbp)
	movq	-24(%rbx), %rax
	movq	%r12, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movl	(%r15), %edx
	testl	%edx, %edx
	je	.L494
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rbx, -432(%rbp)
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	%rsi, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-528(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	movq	-552(%rbp), %rsi
	movq	%rax, -424(%rbp)
	movq	-488(%rbp), %rax
	movl	$16, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internallsERSoRKNS0_12_GLOBAL__N_14FlagE
	movq	-496(%rbp), %rax
	movq	$0, -456(%rbp)
	movb	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	-384(%rbp), %rax
	testq	%rax, %rax
	je	.L508
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L509
	subq	%rcx, %rax
	movq	-504(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L510:
	movq	-464(%rbp), %rdi
	call	_ZN2v88internal6StrDupEPKc@PLT
	movq	-512(%rbp), %rcx
	movq	%rax, -472(%rbp)
	movq	8(%rcx), %rsi
	cmpq	16(%rcx), %rsi
	je	.L511
	movq	%rax, (%rsi)
	addq	$8, 8(%rcx)
.L512:
	movq	-464(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	je	.L513
	call	_ZdlPv@PLT
.L513:
	movdqa	-544(%rbp), %xmm4
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-488(%rbp), %rdi
	je	.L514
	call	_ZdlPv@PLT
.L514:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%rbx, -432(%rbp)
	movq	-24(%rbx), %rax
	movq	%r12, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L484:
	movq	16(%r15), %rax
	movq	(%rax), %rdi
	movq	24(%r15), %rax
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	sete	%al
	testq	%rsi, %rsi
	je	.L495
	testq	%rdi, %rdi
	je	.L519
	call	strcmp@PLT
	testl	%eax, %eax
	sete	%al
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L487:
	movq	16(%r15), %rax
	movq	24(%r15), %rdx
	movsd	(%rax), %xmm0
	ucomisd	(%rdx), %xmm0
	movl	$0, %edx
	setnp	%al
	cmovne	%edx, %eax
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L501:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L527:
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L503:
	movq	%rdx, %rdi
	leaq	-472(%rbp), %rdx
	call	_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L500:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L509:
	movq	-504(%rbp), %rdi
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L508:
	movq	-504(%rbp), %rdi
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L511:
	movq	%rcx, %rdi
	leaq	-472(%rbp), %rdx
	call	_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L519:
	xorl	%eax, %eax
	jmp	.L497
.L483:
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L526:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9221:
	.size	_ZN2v88internal8FlagList4argvEv, .-_ZN2v88internal8FlagList4argvEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal15FLAG_use_strictE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal15FLAG_use_strictE, @function
_GLOBAL__sub_I__ZN2v88internal15FLAG_use_strictE:
.LFB10776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10776:
	.size	_GLOBAL__sub_I__ZN2v88internal15FLAG_use_strictE, .-_GLOBAL__sub_I__ZN2v88internal15FLAG_use_strictE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal15FLAG_use_strictE
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.bss._ZN2v88internalL9flag_hashE,"aw",@nobits
	.align 4
	.type	_ZN2v88internalL9flag_hashE, @object
	.size	_ZN2v88internalL9flag_hashE, 4
_ZN2v88internalL9flag_hashE:
	.zero	4
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC40:
	.string	"use_strict"
.LC41:
	.string	"enforce strict mode"
.LC42:
	.string	"es_staging"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC43:
	.string	"enable test-worthy harmony features (for internal use only)"
	.section	.rodata.str1.1
.LC44:
	.string	"harmony"
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"enable all completed harmony features"
	.section	.rodata.str1.1
.LC46:
	.string	"harmony_shipping"
	.section	.rodata.str1.8
	.align 8
.LC47:
	.string	"enable all shipped harmony features"
	.section	.rodata.str1.1
.LC48:
	.string	"harmony_private_methods"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"enable \"harmony private methods in class literals\" (in progress)"
	.section	.rodata.str1.1
.LC50:
	.string	"harmony_regexp_sequence"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"enable \"RegExp Unicode sequence properties\" (in progress)"
	.section	.rodata.str1.1
.LC52:
	.string	"harmony_weak_refs"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"enable \"harmony weak references\" (in progress)"
	.section	.rodata.str1.1
.LC54:
	.string	"harmony_optional_chaining"
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"enable \"harmony optional chaining syntax\" (in progress)"
	.section	.rodata.str1.1
.LC56:
	.string	"harmony_nullish"
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"enable \"harmony nullish operator\" (in progress)"
	.align 8
.LC58:
	.string	"harmony_intl_dateformat_quarter"
	.align 8
.LC59:
	.string	"enable \"Add quarter option to DateTimeFormat\" (in progress)"
	.align 8
.LC60:
	.string	"harmony_intl_add_calendar_numbering_system"
	.align 8
.LC61:
	.string	"enable \"Add calendar and numberingSystem to DateTimeFormat\""
	.align 8
.LC62:
	.string	"harmony_intl_dateformat_day_period"
	.align 8
.LC63:
	.string	"enable \"Add dayPeriod option to DateTimeFormat\""
	.align 8
.LC64:
	.string	"harmony_intl_dateformat_fractional_second_digits"
	.align 8
.LC65:
	.string	"enable \"Add fractionalSecondDigits option to DateTimeFormat\""
	.section	.rodata.str1.1
.LC66:
	.string	"harmony_intl_segmenter"
.LC67:
	.string	"enable \"Intl.Segmenter\""
.LC68:
	.string	"harmony_namespace_exports"
	.section	.rodata.str1.8
	.align 8
.LC69:
	.string	"enable \"harmony namespace exports (export * as foo from 'bar')\""
	.section	.rodata.str1.1
.LC70:
	.string	"harmony_sharedarraybuffer"
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"enable \"harmony sharedarraybuffer\""
	.section	.rodata.str1.1
.LC72:
	.string	"harmony_import_meta"
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"enable \"harmony import.meta property\""
	.section	.rodata.str1.1
.LC74:
	.string	"harmony_dynamic_import"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"enable \"harmony dynamic import\""
	.section	.rodata.str1.1
.LC76:
	.string	"harmony_promise_all_settled"
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"enable \"harmony Promise.allSettled\""
	.section	.rodata.str1.1
.LC78:
	.string	"harmony_intl_bigint"
	.section	.rodata.str1.8
	.align 8
.LC79:
	.string	"enable \"BigInt.prototype.toLocaleString\""
	.align 8
.LC80:
	.string	"harmony_intl_date_format_range"
	.align 8
.LC81:
	.string	"enable \"DateTimeFormat formatRange\""
	.section	.rodata.str1.1
.LC82:
	.string	"harmony_intl_datetime_style"
	.section	.rodata.str1.8
	.align 8
.LC83:
	.string	"enable \"dateStyle timeStyle for DateTimeFormat\""
	.align 8
.LC84:
	.string	"harmony_intl_numberformat_unified"
	.align 8
.LC85:
	.string	"enable \"Unified Intl.NumberFormat Features\""
	.section	.rodata.str1.1
.LC86:
	.string	"icu_timezone_data"
	.section	.rodata.str1.8
	.align 8
.LC87:
	.string	"get information about timezones from ICU"
	.section	.rodata.str1.1
.LC88:
	.string	"lite_mode"
	.section	.rodata.str1.8
	.align 8
.LC89:
	.string	"enables trade-off of performance for memory savings"
	.section	.rodata.str1.1
.LC90:
	.string	"future"
	.section	.rodata.str1.8
	.align 8
.LC91:
	.string	"Implies all staged features that we want to ship in the not-too-far future"
	.section	.rodata.str1.1
.LC92:
	.string	"assert_types"
	.section	.rodata.str1.8
	.align 8
.LC93:
	.string	"generate runtime type assertions to test the typer"
	.section	.rodata.str1.1
.LC94:
	.string	"allocation_site_pretenuring"
	.section	.rodata.str1.8
	.align 8
.LC95:
	.string	"pretenure with allocation sites"
	.section	.rodata.str1.1
.LC96:
	.string	"page_promotion"
	.section	.rodata.str1.8
	.align 8
.LC97:
	.string	"promote pages based on utilization"
	.section	.rodata.str1.1
.LC98:
	.string	"page_promotion_threshold"
	.section	.rodata.str1.8
	.align 8
.LC99:
	.string	"min percentage of live bytes on a page to enable fast evacuation"
	.section	.rodata.str1.1
.LC100:
	.string	"trace_pretenuring"
	.section	.rodata.str1.8
	.align 8
.LC101:
	.string	"trace pretenuring decisions of HAllocate instructions"
	.section	.rodata.str1.1
.LC102:
	.string	"trace_pretenuring_statistics"
	.section	.rodata.str1.8
	.align 8
.LC103:
	.string	"trace allocation site pretenuring statistics"
	.section	.rodata.str1.1
.LC104:
	.string	"track_fields"
	.section	.rodata.str1.8
	.align 8
.LC105:
	.string	"track fields with only smi values"
	.section	.rodata.str1.1
.LC106:
	.string	"track_double_fields"
	.section	.rodata.str1.8
	.align 8
.LC107:
	.string	"track fields with double values"
	.section	.rodata.str1.1
.LC108:
	.string	"track_heap_object_fields"
.LC109:
	.string	"track fields with heap values"
.LC110:
	.string	"track_computed_fields"
	.section	.rodata.str1.8
	.align 8
.LC111:
	.string	"track computed boilerplate fields"
	.section	.rodata.str1.1
.LC112:
	.string	"track_field_types"
.LC113:
	.string	"track field types"
.LC114:
	.string	"trace_block_coverage"
	.section	.rodata.str1.8
	.align 8
.LC115:
	.string	"trace collected block coverage information"
	.section	.rodata.str1.1
.LC116:
	.string	"trace_protector_invalidation"
	.section	.rodata.str1.8
	.align 8
.LC117:
	.string	"trace protector cell invalidations"
	.section	.rodata.str1.1
.LC118:
	.string	"feedback_normalization"
	.section	.rodata.str1.8
	.align 8
.LC119:
	.string	"feed back normalization to constructors"
	.section	.rodata.str1.1
.LC120:
	.string	"enable_one_shot_optimization"
	.section	.rodata.str1.8
	.align 8
.LC121:
	.string	"Enable size optimizations for the code that will only be executed once"
	.section	.rodata.str1.1
.LC122:
	.string	"unbox_double_arrays"
	.section	.rodata.str1.8
	.align 8
.LC123:
	.string	"automatically unbox arrays of doubles"
	.section	.rodata.str1.1
.LC124:
	.string	"interrupt_budget"
	.section	.rodata.str1.8
	.align 8
.LC125:
	.string	"interrupt budget which should be used for the profiler counter"
	.section	.rodata.str1.1
.LC126:
	.string	"jitless"
	.section	.rodata.str1.8
	.align 8
.LC127:
	.string	"Disable runtime allocation of executable memory."
	.section	.rodata.str1.1
.LC128:
	.string	"use_ic"
.LC129:
	.string	"use inline caching"
	.section	.rodata.str1.8
	.align 8
.LC130:
	.string	"budget_for_feedback_vector_allocation"
	.align 8
.LC131:
	.string	"The budget in amount of bytecode executed by a function before we decide to allocate feedback vectors"
	.section	.rodata.str1.1
.LC132:
	.string	"lazy_feedback_allocation"
	.section	.rodata.str1.8
	.align 8
.LC133:
	.string	"Allocate feedback vectors lazily"
	.align 8
.LC134:
	.string	"ignition_elide_noneffectful_bytecodes"
	.align 8
.LC135:
	.string	"elide bytecodes which won't have any external effect"
	.section	.rodata.str1.1
.LC136:
	.string	"ignition_reo"
	.section	.rodata.str1.8
	.align 8
.LC137:
	.string	"use ignition register equivalence optimizer"
	.align 8
.LC138:
	.string	"ignition_filter_expression_positions"
	.align 8
.LC139:
	.string	"filter expression positions before the bytecode pipeline"
	.align 8
.LC140:
	.string	"ignition_share_named_property_feedback"
	.align 8
.LC141:
	.string	"share feedback slots when loading the same named property from the same object"
	.section	.rodata.str1.1
.LC142:
	.string	"print_bytecode"
	.section	.rodata.str1.8
	.align 8
.LC143:
	.string	"print bytecode generated by ignition interpreter"
	.section	.rodata.str1.1
.LC144:
	.string	"enable_lazy_source_positions"
	.section	.rodata.str1.8
	.align 8
.LC145:
	.string	"skip generating source positions during initial compile but regenerate when actually required"
	.section	.rodata.str1.1
.LC146:
	.string	"stress_lazy_source_positions"
	.section	.rodata.str1.8
	.align 8
.LC147:
	.string	"collect lazy source positions immediately after lazy compile"
	.section	.rodata.str1.1
.LC148:
	.string	"print_bytecode_filter"
	.section	.rodata.str1.8
	.align 8
.LC149:
	.string	"filter for selecting which functions to print bytecode"
	.section	.rodata.str1.1
.LC150:
	.string	"trace_ignition_codegen"
	.section	.rodata.str1.8
	.align 8
.LC151:
	.string	"trace the codegen of ignition interpreter bytecode handlers"
	.section	.rodata.str1.1
.LC152:
	.string	"trace_ignition_dispatches"
	.section	.rodata.str1.8
	.align 8
.LC153:
	.string	"traces the dispatches to bytecode handlers by the ignition interpreter"
	.align 8
.LC154:
	.string	"trace_ignition_dispatches_output_file"
	.align 8
.LC155:
	.string	"the file to which the bytecode handler dispatch table is written (by default, the table is not written to a file)"
	.section	.rodata.str1.1
.LC156:
	.string	"fast_math"
	.section	.rodata.str1.8
	.align 8
.LC157:
	.string	"faster (but maybe less accurate) math functions"
	.section	.rodata.str1.1
.LC158:
	.string	"trace_track_allocation_sites"
	.section	.rodata.str1.8
	.align 8
.LC159:
	.string	"trace the tracking of allocation sites"
	.section	.rodata.str1.1
.LC160:
	.string	"trace_migration"
.LC161:
	.string	"trace object migration"
.LC162:
	.string	"trace_generalization"
.LC163:
	.string	"trace map generalization"
.LC164:
	.string	"concurrent_recompilation"
	.section	.rodata.str1.8
	.align 8
.LC165:
	.string	"optimizing hot functions asynchronously on a separate thread"
	.align 8
.LC166:
	.string	"trace_concurrent_recompilation"
	.align 8
.LC167:
	.string	"track concurrent recompilation"
	.align 8
.LC168:
	.string	"concurrent_recompilation_queue_length"
	.align 8
.LC169:
	.string	"the length of the concurrent compilation queue"
	.align 8
.LC170:
	.string	"concurrent_recompilation_delay"
	.align 8
.LC171:
	.string	"artificial compilation delay in ms"
	.align 8
.LC172:
	.string	"block_concurrent_recompilation"
	.align 8
.LC173:
	.string	"block queued jobs until released"
	.section	.rodata.str1.1
.LC174:
	.string	"concurrent_inlining"
	.section	.rodata.str1.8
	.align 8
.LC175:
	.string	"run optimizing compiler's inlining phase on a separate thread"
	.section	.rodata.str1.1
.LC176:
	.string	"trace_heap_broker_verbose"
	.section	.rodata.str1.8
	.align 8
.LC177:
	.string	"trace the heap broker verbosely (all reports)"
	.section	.rodata.str1.1
.LC178:
	.string	"trace_heap_broker"
	.section	.rodata.str1.8
	.align 8
.LC179:
	.string	"trace the heap broker (reports on missing data only)"
	.section	.rodata.str1.1
.LC180:
	.string	"stress_runs"
.LC181:
	.string	"number of stress runs"
.LC182:
	.string	"deopt_every_n_times"
	.section	.rodata.str1.8
	.align 8
.LC183:
	.string	"deoptimize every n times a deopt point is passed"
	.section	.rodata.str1.1
.LC184:
	.string	"print_deopt_stress"
	.section	.rodata.str1.8
	.align 8
.LC185:
	.string	"print number of possible deopt points"
	.section	.rodata.str1.1
.LC186:
	.string	"opt"
.LC187:
	.string	"use adaptive optimizations"
.LC188:
	.string	"turbo_sp_frame_access"
	.section	.rodata.str1.8
	.align 8
.LC189:
	.string	"use stack pointer-relative access to frame wherever possible"
	.align 8
.LC190:
	.string	"turbo_control_flow_aware_allocation"
	.align 8
.LC191:
	.string	"consider control flow while allocating registers"
	.section	.rodata.str1.1
.LC192:
	.string	"turbo_filter"
	.section	.rodata.str1.8
	.align 8
.LC193:
	.string	"optimization filter for TurboFan compiler"
	.section	.rodata.str1.1
.LC194:
	.string	"trace_turbo"
.LC195:
	.string	"trace generated TurboFan IR"
.LC196:
	.string	"trace_turbo_path"
	.section	.rodata.str1.8
	.align 8
.LC197:
	.string	"directory to dump generated TurboFan IR to"
	.section	.rodata.str1.1
.LC198:
	.string	"trace_turbo_filter"
	.section	.rodata.str1.8
	.align 8
.LC199:
	.string	"filter for tracing turbofan compilation"
	.section	.rodata.str1.1
.LC200:
	.string	"trace_turbo_graph"
	.section	.rodata.str1.8
	.align 8
.LC201:
	.string	"trace generated TurboFan graphs"
	.section	.rodata.str1.1
.LC202:
	.string	"trace_turbo_scheduled"
	.section	.rodata.str1.8
	.align 8
.LC203:
	.string	"trace TurboFan IR with schedule"
	.section	.rodata.str1.1
.LC204:
	.string	"trace_turbo_cfg_file"
	.section	.rodata.str1.8
	.align 8
.LC205:
	.string	"trace turbo cfg graph (for C1 visualizer) to a given file name"
	.section	.rodata.str1.1
.LC206:
	.string	"trace_turbo_types"
.LC207:
	.string	"trace TurboFan's types"
.LC208:
	.string	"trace_turbo_scheduler"
.LC209:
	.string	"trace TurboFan's scheduler"
.LC210:
	.string	"trace_turbo_reduction"
	.section	.rodata.str1.8
	.align 8
.LC211:
	.string	"trace TurboFan's various reducers"
	.section	.rodata.str1.1
.LC212:
	.string	"trace_turbo_trimming"
	.section	.rodata.str1.8
	.align 8
.LC213:
	.string	"trace TurboFan's graph trimmer"
	.section	.rodata.str1.1
.LC214:
	.string	"trace_turbo_jt"
	.section	.rodata.str1.8
	.align 8
.LC215:
	.string	"trace TurboFan's jump threading"
	.section	.rodata.str1.1
.LC216:
	.string	"trace_turbo_ceq"
	.section	.rodata.str1.8
	.align 8
.LC217:
	.string	"trace TurboFan's control equivalence"
	.section	.rodata.str1.1
.LC218:
	.string	"trace_turbo_loop"
	.section	.rodata.str1.8
	.align 8
.LC219:
	.string	"trace TurboFan's loop optimizations"
	.section	.rodata.str1.1
.LC220:
	.string	"trace_turbo_alloc"
	.section	.rodata.str1.8
	.align 8
.LC221:
	.string	"trace TurboFan's register allocator"
	.section	.rodata.str1.1
.LC222:
	.string	"trace_all_uses"
.LC223:
	.string	"trace all use positions"
.LC224:
	.string	"trace_representation"
.LC225:
	.string	"trace representation types"
.LC226:
	.string	"turbo_verify"
	.section	.rodata.str1.8
	.align 8
.LC227:
	.string	"verify TurboFan graphs at each phase"
	.section	.rodata.str1.1
.LC228:
	.string	"turbo_verify_machine_graph"
	.section	.rodata.str1.8
	.align 8
.LC229:
	.string	"verify TurboFan machine graph before instruction selection"
	.section	.rodata.str1.1
.LC230:
	.string	"trace_verify_csa"
.LC231:
	.string	"trace code stubs verification"
.LC232:
	.string	"csa_trap_on_node"
	.section	.rodata.str1.8
	.align 8
.LC233:
	.string	"trigger break point when a node with given id is created in given stub. The format is: StubName,NodeId"
	.section	.rodata.str1.1
.LC234:
	.string	"turbo_stats"
.LC235:
	.string	"print TurboFan statistics"
.LC236:
	.string	"turbo_stats_nvp"
	.section	.rodata.str1.8
	.align 8
.LC237:
	.string	"print TurboFan statistics in machine-readable format"
	.section	.rodata.str1.1
.LC238:
	.string	"turbo_stats_wasm"
	.section	.rodata.str1.8
	.align 8
.LC239:
	.string	"print TurboFan statistics of wasm compilations"
	.section	.rodata.str1.1
.LC240:
	.string	"turbo_splitting"
	.section	.rodata.str1.8
	.align 8
.LC241:
	.string	"split nodes during scheduling in TurboFan"
	.align 8
.LC242:
	.string	"function_context_specialization"
	.align 8
.LC243:
	.string	"enable function context specialization in TurboFan"
	.section	.rodata.str1.1
.LC244:
	.string	"turbo_inlining"
.LC245:
	.string	"enable inlining in TurboFan"
.LC246:
	.string	"max_inlined_bytecode_size"
	.section	.rodata.str1.8
	.align 8
.LC247:
	.string	"maximum size of bytecode for a single inlining"
	.align 8
.LC248:
	.string	"max_inlined_bytecode_size_cumulative"
	.align 8
.LC249:
	.string	"maximum cumulative size of bytecode considered for inlining"
	.align 8
.LC250:
	.string	"max_inlined_bytecode_size_absolute"
	.align 8
.LC251:
	.string	"reserve_inline_budget_scale_factor"
	.align 8
.LC252:
	.string	"max_inlined_bytecode_size_small"
	.align 8
.LC253:
	.string	"maximum size of bytecode considered for small function inlining"
	.section	.rodata.str1.1
.LC254:
	.string	"max_optimized_bytecode_size"
	.section	.rodata.str1.8
	.align 8
.LC255:
	.string	"maximum bytecode size to be considered for optimization; too high values may cause the compiler to hit (release) assertions"
	.section	.rodata.str1.1
.LC256:
	.string	"min_inlining_frequency"
	.section	.rodata.str1.8
	.align 8
.LC257:
	.string	"minimum frequency for inlining"
	.section	.rodata.str1.1
.LC258:
	.string	"polymorphic_inlining"
.LC259:
	.string	"polymorphic inlining"
.LC260:
	.string	"stress_inline"
	.section	.rodata.str1.8
	.align 8
.LC261:
	.string	"set high thresholds for inlining to inline as much as possible"
	.section	.rodata.str1.1
.LC262:
	.string	"trace_turbo_inlining"
.LC263:
	.string	"trace TurboFan inlining"
.LC264:
	.string	"inline_accessors"
.LC265:
	.string	"inline JavaScript accessors"
.LC266:
	.string	"turbo_inline_array_builtins"
	.section	.rodata.str1.8
	.align 8
.LC267:
	.string	"inline array builtins in TurboFan code"
	.section	.rodata.str1.1
.LC268:
	.string	"use_osr"
.LC269:
	.string	"use on-stack replacement"
.LC270:
	.string	"trace_osr"
.LC271:
	.string	"trace on-stack replacement"
.LC272:
	.string	"analyze_environment_liveness"
	.section	.rodata.str1.8
	.align 8
.LC273:
	.string	"analyze liveness of environment slots and zap dead values"
	.section	.rodata.str1.1
.LC274:
	.string	"trace_environment_liveness"
	.section	.rodata.str1.8
	.align 8
.LC275:
	.string	"trace liveness of local variable slots"
	.section	.rodata.str1.1
.LC276:
	.string	"turbo_load_elimination"
	.section	.rodata.str1.8
	.align 8
.LC277:
	.string	"enable load elimination in TurboFan"
	.section	.rodata.str1.1
.LC278:
	.string	"trace_turbo_load_elimination"
	.section	.rodata.str1.8
	.align 8
.LC279:
	.string	"trace TurboFan load elimination"
	.section	.rodata.str1.1
.LC280:
	.string	"turbo_profiling"
.LC281:
	.string	"enable profiling in TurboFan"
.LC282:
	.string	"turbo_verify_allocation"
	.section	.rodata.str1.8
	.align 8
.LC283:
	.string	"verify register allocation in TurboFan"
	.section	.rodata.str1.1
.LC284:
	.string	"turbo_move_optimization"
	.section	.rodata.str1.8
	.align 8
.LC285:
	.string	"optimize gap moves in TurboFan"
	.section	.rodata.str1.1
.LC286:
	.string	"turbo_jt"
	.section	.rodata.str1.8
	.align 8
.LC287:
	.string	"enable jump threading in TurboFan"
	.section	.rodata.str1.1
.LC288:
	.string	"turbo_loop_peeling"
.LC289:
	.string	"Turbofan loop peeling"
.LC290:
	.string	"turbo_loop_variable"
	.section	.rodata.str1.8
	.align 8
.LC291:
	.string	"Turbofan loop variable optimization"
	.section	.rodata.str1.1
.LC292:
	.string	"turbo_loop_rotation"
.LC293:
	.string	"Turbofan loop rotation"
.LC294:
	.string	"turbo_cf_optimization"
	.section	.rodata.str1.8
	.align 8
.LC295:
	.string	"optimize control flow in TurboFan"
	.section	.rodata.str1.1
.LC296:
	.string	"turbo_escape"
.LC297:
	.string	"enable escape analysis"
.LC298:
	.string	"turbo_allocation_folding"
.LC299:
	.string	"Turbofan allocation folding"
.LC300:
	.string	"turbo_instruction_scheduling"
	.section	.rodata.str1.8
	.align 8
.LC301:
	.string	"enable instruction scheduling in TurboFan"
	.align 8
.LC302:
	.string	"turbo_stress_instruction_scheduling"
	.align 8
.LC303:
	.string	"randomly schedule instructions to stress dependency tracking"
	.section	.rodata.str1.1
.LC304:
	.string	"turbo_store_elimination"
	.section	.rodata.str1.8
	.align 8
.LC305:
	.string	"enable store-store elimination in TurboFan"
	.section	.rodata.str1.1
.LC306:
	.string	"trace_store_elimination"
.LC307:
	.string	"trace store elimination"
.LC308:
	.string	"turbo_rewrite_far_jumps"
	.section	.rodata.str1.8
	.align 8
.LC309:
	.string	"rewrite far to near jumps (ia32,x64)"
	.align 8
.LC310:
	.string	"experimental_inline_promise_constructor"
	.align 8
.LC311:
	.string	"inline the Promise constructor in TurboFan"
	.section	.rodata.str1.1
.LC312:
	.string	"stress_gc_during_compilation"
	.section	.rodata.str1.8
	.align 8
.LC313:
	.string	"simulate GC/compiler thread race related to https://crbug.com/v8/8520"
	.section	.rodata.str1.1
.LC314:
	.string	"optimize_for_size"
	.section	.rodata.str1.8
	.align 8
.LC315:
	.string	"Enables optimizations which favor memory size over execution speed"
	.section	.rodata.str1.1
.LC316:
	.string	"untrusted_code_mitigations"
	.section	.rodata.str1.8
	.align 8
.LC317:
	.string	"Enable mitigations for executing untrusted code"
	.section	.rodata.str1.1
.LC318:
	.string	"expose_wasm"
	.section	.rodata.str1.8
	.align 8
.LC319:
	.string	"expose wasm interface to JavaScript"
	.section	.rodata.str1.1
.LC320:
	.string	"assume_asmjs_origin"
	.section	.rodata.str1.8
	.align 8
.LC321:
	.string	"force wasm decoder to assume input is internal asm-wasm format"
	.align 8
.LC322:
	.string	"wasm_disable_structured_cloning"
	.align 8
.LC323:
	.string	"disable wasm structured cloning"
	.section	.rodata.str1.1
.LC324:
	.string	"wasm_num_compilation_tasks"
	.section	.rodata.str1.8
	.align 8
.LC325:
	.string	"number of parallel compilation tasks for wasm"
	.align 8
.LC326:
	.string	"wasm_write_protect_code_memory"
	.align 8
.LC327:
	.string	"write protect code memory on the wasm native heap"
	.section	.rodata.str1.1
.LC328:
	.string	"trace_wasm_serialization"
	.section	.rodata.str1.8
	.align 8
.LC329:
	.string	"trace serialization/deserialization"
	.section	.rodata.str1.1
.LC330:
	.string	"wasm_async_compilation"
	.section	.rodata.str1.8
	.align 8
.LC331:
	.string	"enable actual asynchronous compilation for WebAssembly.compile"
	.section	.rodata.str1.1
.LC332:
	.string	"wasm_test_streaming"
	.section	.rodata.str1.8
	.align 8
.LC333:
	.string	"use streaming compilation instead of async compilation for tests"
	.section	.rodata.str1.1
.LC334:
	.string	"wasm_max_mem_pages"
	.section	.rodata.str1.8
	.align 8
.LC335:
	.string	"maximum number of 64KiB memory pages of a wasm instance"
	.section	.rodata.str1.1
.LC336:
	.string	"wasm_max_table_size"
	.section	.rodata.str1.8
	.align 8
.LC337:
	.string	"maximum table size of a wasm instance"
	.section	.rodata.str1.1
.LC338:
	.string	"wasm_max_code_space"
	.section	.rodata.str1.8
	.align 8
.LC339:
	.string	"maximum committed code space for wasm (in MB)"
	.section	.rodata.str1.1
.LC340:
	.string	"wasm_tier_up"
	.section	.rodata.str1.8
	.align 8
.LC341:
	.string	"enable wasm baseline compilation and tier up to the optimizing compiler"
	.section	.rodata.str1.1
.LC342:
	.string	"trace_wasm_ast_start"
	.section	.rodata.str1.8
	.align 8
.LC343:
	.string	"start function for wasm AST trace (inclusive)"
	.section	.rodata.str1.1
.LC344:
	.string	"trace_wasm_ast_end"
	.section	.rodata.str1.8
	.align 8
.LC345:
	.string	"end function for wasm AST trace (exclusive)"
	.section	.rodata.str1.1
.LC346:
	.string	"liftoff"
	.section	.rodata.str1.8
	.align 8
.LC347:
	.string	"enable Liftoff, the baseline compiler for WebAssembly"
	.section	.rodata.str1.1
.LC348:
	.string	"trace_wasm_memory"
	.section	.rodata.str1.8
	.align 8
.LC349:
	.string	"print all memory updates performed in wasm code"
	.section	.rodata.str1.1
.LC350:
	.string	"wasm_tier_mask_for_testing"
	.section	.rodata.str1.8
	.align 8
.LC351:
	.string	"bitmask of functions to compile with TurboFan instead of Liftoff"
	.section	.rodata.str1.1
.LC352:
	.string	"validate_asm"
	.section	.rodata.str1.8
	.align 8
.LC353:
	.string	"validate asm.js modules before compiling"
	.section	.rodata.str1.1
.LC354:
	.string	"suppress_asm_messages"
	.section	.rodata.str1.8
	.align 8
.LC355:
	.string	"don't emit asm.js related messages (for golden file testing)"
	.section	.rodata.str1.1
.LC356:
	.string	"trace_asm_time"
	.section	.rodata.str1.8
	.align 8
.LC357:
	.string	"log asm.js timing info to the console"
	.section	.rodata.str1.1
.LC358:
	.string	"trace_asm_scanner"
	.section	.rodata.str1.8
	.align 8
.LC359:
	.string	"log tokens encountered by asm.js scanner"
	.section	.rodata.str1.1
.LC360:
	.string	"trace_asm_parser"
	.section	.rodata.str1.8
	.align 8
.LC361:
	.string	"verbose logging of asm.js parse failures"
	.section	.rodata.str1.1
.LC362:
	.string	"stress_validate_asm"
	.section	.rodata.str1.8
	.align 8
.LC363:
	.string	"try to validate everything as asm.js"
	.section	.rodata.str1.1
.LC364:
	.string	"dump_wasm_module_path"
	.section	.rodata.str1.8
	.align 8
.LC365:
	.string	"directory to dump wasm modules to"
	.section	.rodata.str1.1
.LC366:
	.string	"experimental_wasm_mv"
	.section	.rodata.str1.8
	.align 8
.LC367:
	.string	"enable prototype multi-value support for wasm"
	.section	.rodata.str1.1
.LC368:
	.string	"experimental_wasm_eh"
	.section	.rodata.str1.8
	.align 8
.LC369:
	.string	"enable prototype exception handling opcodes for wasm"
	.section	.rodata.str1.1
.LC370:
	.string	"experimental_wasm_threads"
	.section	.rodata.str1.8
	.align 8
.LC371:
	.string	"enable prototype thread opcodes for wasm"
	.section	.rodata.str1.1
.LC372:
	.string	"experimental_wasm_simd"
	.section	.rodata.str1.8
	.align 8
.LC373:
	.string	"enable prototype SIMD opcodes for wasm"
	.section	.rodata.str1.1
.LC374:
	.string	"experimental_wasm_bigint"
	.section	.rodata.str1.8
	.align 8
.LC375:
	.string	"enable prototype JS BigInt support for wasm"
	.section	.rodata.str1.1
.LC376:
	.string	"experimental_wasm_return_call"
	.section	.rodata.str1.8
	.align 8
.LC377:
	.string	"enable prototype return call opcodes for wasm"
	.align 8
.LC378:
	.string	"experimental_wasm_compilation_hints"
	.align 8
.LC379:
	.string	"enable prototype compilation hints section for wasm"
	.section	.rodata.str1.1
.LC380:
	.string	"experimental_wasm_anyref"
	.section	.rodata.str1.8
	.align 8
.LC381:
	.string	"enable prototype anyref opcodes for wasm"
	.align 8
.LC382:
	.string	"experimental_wasm_type_reflection"
	.align 8
.LC383:
	.string	"enable prototype wasm type reflection in JS for wasm"
	.section	.rodata.str1.1
.LC384:
	.string	"experimental_wasm_bulk_memory"
	.section	.rodata.str1.8
	.align 8
.LC385:
	.string	"enable prototype bulk memory opcodes for wasm"
	.align 8
.LC386:
	.string	"experimental_wasm_sat_f2i_conversions"
	.align 8
.LC387:
	.string	"enable prototype saturating float conversion opcodes for wasm"
	.section	.rodata.str1.1
.LC388:
	.string	"experimental_wasm_se"
	.section	.rodata.str1.8
	.align 8
.LC389:
	.string	"enable prototype sign extension opcodes for wasm"
	.section	.rodata.str1.1
.LC390:
	.string	"wasm_staging"
.LC391:
	.string	"enable staged wasm features"
.LC392:
	.string	"wasm_opt"
.LC393:
	.string	"enable wasm optimization"
.LC394:
	.string	"wasm_no_bounds_checks"
	.section	.rodata.str1.8
	.align 8
.LC395:
	.string	"disable bounds checks (performance testing only)"
	.section	.rodata.str1.1
.LC396:
	.string	"wasm_no_stack_checks"
	.section	.rodata.str1.8
	.align 8
.LC397:
	.string	"disable stack checks (performance testing only)"
	.section	.rodata.str1.1
.LC398:
	.string	"wasm_math_intrinsics"
	.section	.rodata.str1.8
	.align 8
.LC399:
	.string	"intrinsify some Math imports into wasm"
	.section	.rodata.str1.1
.LC400:
	.string	"wasm_shared_engine"
	.section	.rodata.str1.8
	.align 8
.LC401:
	.string	"shares one wasm engine between all isolates within a process"
	.section	.rodata.str1.1
.LC402:
	.string	"wasm_shared_code"
	.section	.rodata.str1.8
	.align 8
.LC403:
	.string	"shares code underlying a wasm module when it is transferred"
	.section	.rodata.str1.1
.LC404:
	.string	"wasm_trap_handler"
	.section	.rodata.str1.8
	.align 8
.LC405:
	.string	"use signal handlers to catch out of bounds memory access in wasm (currently Linux x86_64 only)"
	.section	.rodata.str1.1
.LC406:
	.string	"wasm_fuzzer_gen_test"
	.section	.rodata.str1.8
	.align 8
.LC407:
	.string	"generate a test case when running a wasm fuzzer"
	.section	.rodata.str1.1
.LC408:
	.string	"print_wasm_code"
.LC409:
	.string	"Print WebAssembly code"
.LC410:
	.string	"print_wasm_stub_code"
.LC411:
	.string	"Print WebAssembly stub code"
.LC412:
	.string	"wasm_interpret_all"
	.section	.rodata.str1.8
	.align 8
.LC413:
	.string	"execute all wasm code in the wasm interpreter"
	.section	.rodata.str1.1
.LC414:
	.string	"asm_wasm_lazy_compilation"
	.section	.rodata.str1.8
	.align 8
.LC415:
	.string	"enable lazy compilation for asm-wasm modules"
	.section	.rodata.str1.1
.LC416:
	.string	"wasm_lazy_compilation"
	.section	.rodata.str1.8
	.align 8
.LC417:
	.string	"enable lazy compilation for all wasm modules"
	.section	.rodata.str1.1
.LC418:
	.string	"wasm_grow_shared_memory"
	.section	.rodata.str1.8
	.align 8
.LC419:
	.string	"allow growing shared WebAssembly memory objects"
	.section	.rodata.str1.1
.LC420:
	.string	"wasm_lazy_validation"
	.section	.rodata.str1.8
	.align 8
.LC421:
	.string	"enable lazy validation for lazily compiled wasm functions"
	.section	.rodata.str1.1
.LC422:
	.string	"wasm_code_gc"
	.section	.rodata.str1.8
	.align 8
.LC423:
	.string	"enable garbage collection of wasm code"
	.section	.rodata.str1.1
.LC424:
	.string	"trace_wasm_code_gc"
	.section	.rodata.str1.8
	.align 8
.LC425:
	.string	"trace garbage collection of wasm code"
	.section	.rodata.str1.1
.LC426:
	.string	"stress_wasm_code_gc"
	.section	.rodata.str1.8
	.align 8
.LC427:
	.string	"stress test garbage collection of wasm code"
	.section	.rodata.str1.1
.LC428:
	.string	"frame_count"
	.section	.rodata.str1.8
	.align 8
.LC429:
	.string	"number of stack frames inspected by the profiler"
	.align 8
.LC430:
	.string	"stress_sampling_allocation_profiler"
	.align 8
.LC431:
	.string	"Enables sampling allocation profiler with X as a sample interval"
	.section	.rodata.str1.1
.LC432:
	.string	"min_semi_space_size"
	.section	.rodata.str1.8
	.align 8
.LC433:
	.string	"min size of a semi-space (in MBytes), the new space consists of two semi-spaces"
	.section	.rodata.str1.1
.LC434:
	.string	"max_semi_space_size"
	.section	.rodata.str1.8
	.align 8
.LC435:
	.string	"max size of a semi-space (in MBytes), the new space consists of two semi-spaces"
	.section	.rodata.str1.1
.LC436:
	.string	"semi_space_growth_factor"
	.section	.rodata.str1.8
	.align 8
.LC437:
	.string	"factor by which to grow the new space"
	.align 8
.LC438:
	.string	"experimental_new_space_growth_heuristic"
	.align 8
.LC439:
	.string	"Grow the new space based on the percentage of survivors instead of their absolute value."
	.section	.rodata.str1.1
.LC440:
	.string	"max_old_space_size"
	.section	.rodata.str1.8
	.align 8
.LC441:
	.string	"max size of the old space (in Mbytes)"
	.section	.rodata.str1.1
.LC442:
	.string	"max_heap_size"
	.section	.rodata.str1.8
	.align 8
.LC443:
	.string	"max size of the heap (in Mbytes) both max_semi_space_size and max_old_space_size take precedence. All three flags cannot be specified at the same time."
	.section	.rodata.str1.1
.LC444:
	.string	"initial_heap_size"
	.section	.rodata.str1.8
	.align 8
.LC445:
	.string	"initial size of the heap (in Mbytes)"
	.section	.rodata.str1.1
.LC446:
	.string	"huge_max_old_generation_size"
	.section	.rodata.str1.8
	.align 8
.LC447:
	.string	"Increase max size of the old space to 4 GB for x64 systems withthe physical memory bigger than 16 GB"
	.section	.rodata.str1.1
.LC448:
	.string	"initial_old_space_size"
	.section	.rodata.str1.8
	.align 8
.LC449:
	.string	"initial old space size (in Mbytes)"
	.section	.rodata.str1.1
.LC450:
	.string	"global_gc_scheduling"
	.section	.rodata.str1.8
	.align 8
.LC451:
	.string	"enable GC scheduling based on global memory"
	.section	.rodata.str1.1
.LC452:
	.string	"gc_global"
.LC453:
	.string	"always perform global GCs"
.LC454:
	.string	"random_gc_interval"
	.section	.rodata.str1.8
	.align 8
.LC455:
	.string	"Collect garbage after random(0, X) allocations. It overrides gc_interval."
	.section	.rodata.str1.1
.LC456:
	.string	"gc_interval"
	.section	.rodata.str1.8
	.align 8
.LC457:
	.string	"garbage collect after <n> allocations"
	.section	.rodata.str1.1
.LC458:
	.string	"retain_maps_for_n_gc"
	.section	.rodata.str1.8
	.align 8
.LC459:
	.string	"keeps maps alive for <n> old space garbage collections"
	.section	.rodata.str1.1
.LC460:
	.string	"trace_gc"
	.section	.rodata.str1.8
	.align 8
.LC461:
	.string	"print one trace line following each garbage collection"
	.section	.rodata.str1.1
.LC462:
	.string	"trace_gc_nvp"
	.section	.rodata.str1.8
	.align 8
.LC463:
	.string	"print one detailed trace line in name=value format after each garbage collection"
	.section	.rodata.str1.1
.LC464:
	.string	"trace_gc_ignore_scavenger"
	.section	.rodata.str1.8
	.align 8
.LC465:
	.string	"do not print trace line after scavenger collection"
	.section	.rodata.str1.1
.LC466:
	.string	"trace_idle_notification"
	.section	.rodata.str1.8
	.align 8
.LC467:
	.string	"print one trace line following each idle notification"
	.align 8
.LC468:
	.string	"trace_idle_notification_verbose"
	.align 8
.LC469:
	.string	"prints the heap state used by the idle notification"
	.section	.rodata.str1.1
.LC470:
	.string	"trace_gc_verbose"
	.section	.rodata.str1.8
	.align 8
.LC471:
	.string	"print more details following each garbage collection"
	.section	.rodata.str1.1
.LC472:
	.string	"trace_gc_freelists"
	.section	.rodata.str1.8
	.align 8
.LC473:
	.string	"prints details of each freelist before and after each major garbage collection"
	.section	.rodata.str1.1
.LC474:
	.string	"trace_gc_freelists_verbose"
	.section	.rodata.str1.8
	.align 8
.LC475:
	.string	"prints details of freelists of each page before and after each major garbage collection"
	.section	.rodata.str1.1
.LC476:
	.string	"trace_evacuation_candidates"
	.section	.rodata.str1.8
	.align 8
.LC477:
	.string	"Show statistics about the pages evacuation by the compaction"
	.section	.rodata.str1.1
.LC478:
	.string	"trace_allocations_origins"
	.section	.rodata.str1.8
	.align 8
.LC479:
	.string	"Show statistics about the origins of allocations. Combine with --no-inline-new to track allocations from generated code"
	.section	.rodata.str1.1
.LC480:
	.string	"gc_freelist_strategy"
	.section	.rodata.str1.8
	.align 8
.LC481:
	.string	"Freelist strategy to use: 0:FreeListLegacy. 1:FreeListFastAlloc. 2:FreeListMany. 3:FreeListManyCached. 4:FreeListManyCachedFastPath. 5:FreeListManyCachedOrigin. "
	.align 8
.LC482:
	.string	"trace_allocation_stack_interval"
	.align 8
.LC483:
	.string	"print stack trace after <n> free-list allocations"
	.section	.rodata.str1.1
.LC484:
	.string	"trace_duplicate_threshold_kb"
	.section	.rodata.str1.8
	.align 8
.LC485:
	.string	"print duplicate objects in the heap if their size is more than given threshold"
	.section	.rodata.str1.1
.LC486:
	.string	"trace_fragmentation"
	.section	.rodata.str1.8
	.align 8
.LC487:
	.string	"report fragmentation for old space"
	.section	.rodata.str1.1
.LC488:
	.string	"trace_fragmentation_verbose"
	.section	.rodata.str1.8
	.align 8
.LC489:
	.string	"report fragmentation for old space (detailed)"
	.section	.rodata.str1.1
.LC490:
	.string	"trace_evacuation"
.LC491:
	.string	"report evacuation statistics"
.LC492:
	.string	"trace_mutator_utilization"
	.section	.rodata.str1.8
	.align 8
.LC493:
	.string	"print mutator utilization, allocation speed, gc speed"
	.section	.rodata.str1.1
.LC494:
	.string	"incremental_marking"
.LC495:
	.string	"use incremental marking"
.LC496:
	.string	"incremental_marking_wrappers"
	.section	.rodata.str1.8
	.align 8
.LC497:
	.string	"use incremental marking for marking wrappers"
	.section	.rodata.str1.1
.LC498:
	.string	"trace_unmapper"
.LC499:
	.string	"Trace the unmapping"
.LC500:
	.string	"parallel_scavenge"
.LC501:
	.string	"parallel scavenge"
.LC502:
	.string	"trace_parallel_scavenge"
.LC503:
	.string	"trace parallel scavenge"
.LC504:
	.string	"write_protect_code_memory"
.LC505:
	.string	"write protect code memory"
.LC506:
	.string	"concurrent_marking"
.LC507:
	.string	"use concurrent marking"
.LC508:
	.string	"parallel_marking"
	.section	.rodata.str1.8
	.align 8
.LC509:
	.string	"use parallel marking in atomic pause"
	.section	.rodata.str1.1
.LC510:
	.string	"ephemeron_fixpoint_iterations"
	.section	.rodata.str1.8
	.align 8
.LC511:
	.string	"number of fixpoint iterations it takes to switch to linear ephemeron algorithm"
	.section	.rodata.str1.1
.LC512:
	.string	"trace_concurrent_marking"
.LC513:
	.string	"trace concurrent marking"
.LC514:
	.string	"concurrent_store_buffer"
	.section	.rodata.str1.8
	.align 8
.LC515:
	.string	"use concurrent store buffer processing"
	.section	.rodata.str1.1
.LC516:
	.string	"concurrent_sweeping"
.LC517:
	.string	"use concurrent sweeping"
.LC518:
	.string	"parallel_compaction"
.LC519:
	.string	"use parallel compaction"
.LC520:
	.string	"parallel_pointer_update"
	.section	.rodata.str1.8
	.align 8
.LC521:
	.string	"use parallel pointer update during compaction"
	.align 8
.LC522:
	.string	"detect_ineffective_gcs_near_heap_limit"
	.align 8
.LC523:
	.string	"trigger out-of-memory failure to avoid GC storm near heap limit"
	.section	.rodata.str1.1
.LC524:
	.string	"trace_incremental_marking"
	.section	.rodata.str1.8
	.align 8
.LC525:
	.string	"trace progress of the incremental marking"
	.section	.rodata.str1.1
.LC526:
	.string	"trace_stress_marking"
.LC527:
	.string	"trace stress marking progress"
.LC528:
	.string	"trace_stress_scavenge"
	.section	.rodata.str1.8
	.align 8
.LC529:
	.string	"trace stress scavenge progress"
	.section	.rodata.str1.1
.LC530:
	.string	"track_gc_object_stats"
	.section	.rodata.str1.8
	.align 8
.LC531:
	.string	"track object counts and memory usage"
	.section	.rodata.str1.1
.LC532:
	.string	"trace_gc_object_stats"
	.section	.rodata.str1.8
	.align 8
.LC533:
	.string	"trace object counts and memory usage"
	.section	.rodata.str1.1
.LC534:
	.string	"trace_zone_stats"
.LC535:
	.string	"trace zone memory usage"
.LC536:
	.string	"track_retaining_path"
	.section	.rodata.str1.8
	.align 8
.LC537:
	.string	"enable support for tracking retaining path"
	.align 8
.LC538:
	.string	"concurrent_array_buffer_freeing"
	.align 8
.LC539:
	.string	"free array buffer allocations on a background thread"
	.section	.rodata.str1.1
.LC540:
	.string	"gc_stats"
	.section	.rodata.str1.8
	.align 8
.LC541:
	.string	"Used by tracing internally to enable gc statistics"
	.section	.rodata.str1.1
.LC542:
	.string	"track_detached_contexts"
	.section	.rodata.str1.8
	.align 8
.LC543:
	.string	"track native contexts that are expected to be garbage collected"
	.section	.rodata.str1.1
.LC544:
	.string	"trace_detached_contexts"
	.section	.rodata.str1.8
	.align 8
.LC545:
	.string	"trace native contexts that are expected to be garbage collected"
	.section	.rodata.str1.1
.LC546:
	.string	"move_object_start"
	.section	.rodata.str1.8
	.align 8
.LC547:
	.string	"enable moving of object starts"
	.section	.rodata.str1.1
.LC548:
	.string	"memory_reducer"
.LC549:
	.string	"use memory reducer"
	.section	.rodata.str1.8
	.align 8
.LC550:
	.string	"memory_reducer_for_small_heaps"
	.align 8
.LC551:
	.string	"use memory reducer for small heaps"
	.section	.rodata.str1.1
.LC552:
	.string	"heap_growing_percent"
	.section	.rodata.str1.8
	.align 8
.LC553:
	.string	"specifies heap growing factor as (1 + heap_growing_percent/100)"
	.section	.rodata.str1.1
.LC554:
	.string	"v8_os_page_size"
	.section	.rodata.str1.8
	.align 8
.LC555:
	.string	"override OS page size (in KBytes)"
	.section	.rodata.str1.1
.LC556:
	.string	"always_compact"
	.section	.rodata.str1.8
	.align 8
.LC557:
	.string	"Perform compaction on every full GC"
	.section	.rodata.str1.1
.LC558:
	.string	"never_compact"
	.section	.rodata.str1.8
	.align 8
.LC559:
	.string	"Never perform compaction on full GC - testing only"
	.section	.rodata.str1.1
.LC560:
	.string	"compact_code_space"
	.section	.rodata.str1.8
	.align 8
.LC561:
	.string	"Compact code space on full collections"
	.section	.rodata.str1.1
.LC562:
	.string	"flush_bytecode"
	.section	.rodata.str1.8
	.align 8
.LC563:
	.string	"flush of bytecode when it has not been executed recently"
	.section	.rodata.str1.1
.LC564:
	.string	"stress_flush_bytecode"
.LC565:
	.string	"stress bytecode flushing"
.LC566:
	.string	"use_marking_progress_bar"
	.section	.rodata.str1.8
	.align 8
.LC567:
	.string	"Use a progress bar to scan large objects in increments when incremental marking is active."
	.section	.rodata.str1.1
.LC568:
	.string	"force_marking_deque_overflows"
	.section	.rodata.str1.8
	.align 8
.LC569:
	.string	"force overflows of marking deque by reducing it's size to 64 words"
	.section	.rodata.str1.1
.LC570:
	.string	"stress_compaction"
	.section	.rodata.str1.8
	.align 8
.LC571:
	.string	"stress the GC compactor to flush out bugs (implies --force_marking_deque_overflows)"
	.section	.rodata.str1.1
.LC572:
	.string	"stress_compaction_random"
	.section	.rodata.str1.8
	.align 8
.LC573:
	.string	"Stress GC compaction by selecting random percent of pages as evacuation candidates. It overrides stress_compaction."
	.section	.rodata.str1.1
.LC574:
	.string	"stress_incremental_marking"
	.section	.rodata.str1.8
	.align 8
.LC575:
	.string	"force incremental marking for small heaps and run it more often"
	.section	.rodata.str1.1
.LC576:
	.string	"fuzzer_gc_analysis"
	.section	.rodata.str1.8
	.align 8
.LC577:
	.string	"prints number of allocations and enables analysis mode for gc fuzz testing, e.g. --stress-marking, --stress-scavenge"
	.section	.rodata.str1.1
.LC578:
	.string	"stress_marking"
	.section	.rodata.str1.8
	.align 8
.LC579:
	.string	"force marking at random points between 0 and X (inclusive) percent of the regular marking start limit"
	.section	.rodata.str1.1
.LC580:
	.string	"stress_scavenge"
	.section	.rodata.str1.8
	.align 8
.LC581:
	.string	"force scavenge at random points between 0 and X (inclusive) percent of the new space capacity"
	.align 8
.LC582:
	.string	"gc_experiment_background_schedule"
	.align 8
.LC583:
	.string	"new background GC schedule heuristics"
	.section	.rodata.str1.1
.LC584:
	.string	"gc_experiment_less_compaction"
	.section	.rodata.str1.8
	.align 8
.LC585:
	.string	"less compaction in non-memory reducing mode"
	.section	.rodata.str1.1
.LC586:
	.string	"disable_abortjs"
	.section	.rodata.str1.8
	.align 8
.LC587:
	.string	"disables AbortJS runtime function"
	.align 8
.LC588:
	.string	"manual_evacuation_candidates_selection"
	.align 8
.LC589:
	.string	"Test mode only flag. It allows an unit test to select evacuation candidates pages (requires --stress_compaction)."
	.section	.rodata.str1.1
.LC590:
	.string	"fast_promotion_new_space"
	.section	.rodata.str1.8
	.align 8
.LC591:
	.string	"fast promote new space on high survival rates"
	.section	.rodata.str1.1
.LC592:
	.string	"clear_free_memory"
.LC593:
	.string	"initialize free memory with 0"
	.section	.rodata.str1.8
	.align 8
.LC594:
	.string	"young_generation_large_objects"
	.align 8
.LC595:
	.string	"allocates large objects by default in the young generation large object space"
	.section	.rodata.str1.1
.LC596:
	.string	"idle_time_scavenge"
	.section	.rodata.str1.8
	.align 8
.LC597:
	.string	"Perform scavenges in idle time."
	.section	.rodata.str1.1
.LC598:
	.string	"debug_code"
	.section	.rodata.str1.8
	.align 8
.LC599:
	.string	"generate extra code (assertions) for debugging"
	.section	.rodata.str1.1
.LC600:
	.string	"code_comments"
	.section	.rodata.str1.8
	.align 8
.LC601:
	.string	"emit comments in code disassembly; for more readable source positions you should add --no-concurrent_recompilation"
	.section	.rodata.str1.1
.LC602:
	.string	"enable_sse3"
	.section	.rodata.str1.8
	.align 8
.LC603:
	.string	"enable use of SSE3 instructions if available"
	.section	.rodata.str1.1
.LC604:
	.string	"enable_ssse3"
	.section	.rodata.str1.8
	.align 8
.LC605:
	.string	"enable use of SSSE3 instructions if available"
	.section	.rodata.str1.1
.LC606:
	.string	"enable_sse4_1"
	.section	.rodata.str1.8
	.align 8
.LC607:
	.string	"enable use of SSE4.1 instructions if available"
	.section	.rodata.str1.1
.LC608:
	.string	"enable_sse4_2"
	.section	.rodata.str1.8
	.align 8
.LC609:
	.string	"enable use of SSE4.2 instructions if available"
	.section	.rodata.str1.1
.LC610:
	.string	"enable_sahf"
	.section	.rodata.str1.8
	.align 8
.LC611:
	.string	"enable use of SAHF instruction if available (X64 only)"
	.section	.rodata.str1.1
.LC612:
	.string	"enable_avx"
	.section	.rodata.str1.8
	.align 8
.LC613:
	.string	"enable use of AVX instructions if available"
	.section	.rodata.str1.1
.LC614:
	.string	"enable_fma3"
	.section	.rodata.str1.8
	.align 8
.LC615:
	.string	"enable use of FMA3 instructions if available"
	.section	.rodata.str1.1
.LC616:
	.string	"enable_bmi1"
	.section	.rodata.str1.8
	.align 8
.LC617:
	.string	"enable use of BMI1 instructions if available"
	.section	.rodata.str1.1
.LC618:
	.string	"enable_bmi2"
	.section	.rodata.str1.8
	.align 8
.LC619:
	.string	"enable use of BMI2 instructions if available"
	.section	.rodata.str1.1
.LC620:
	.string	"enable_lzcnt"
	.section	.rodata.str1.8
	.align 8
.LC621:
	.string	"enable use of LZCNT instruction if available"
	.section	.rodata.str1.1
.LC622:
	.string	"enable_popcnt"
	.section	.rodata.str1.8
	.align 8
.LC623:
	.string	"enable use of POPCNT instruction if available"
	.section	.rodata.str1.1
.LC624:
	.string	"arm_arch"
	.section	.rodata.str1.8
	.align 8
.LC625:
	.string	"generate instructions for the selected ARM architecture if available: armv6, armv7, armv7+sudiv or armv8"
	.section	.rodata.str1.1
.LC626:
	.string	"force_long_branches"
	.section	.rodata.str1.8
	.align 8
.LC627:
	.string	"force all emitted branches to be in long mode (MIPS/PPC only)"
	.section	.rodata.str1.1
.LC628:
	.string	"mcpu"
	.section	.rodata.str1.8
	.align 8
.LC629:
	.string	"enable optimization for specific cpu"
	.section	.rodata.str1.1
.LC630:
	.string	"partial_constant_pool"
	.section	.rodata.str1.8
	.align 8
.LC631:
	.string	"enable use of partial constant pools (X64 only)"
	.section	.rodata.str1.1
.LC632:
	.string	"enable_source_at_csa_bind"
	.section	.rodata.str1.8
	.align 8
.LC633:
	.string	"Include source information in the binary at CSA bind locations."
	.section	.rodata.str1.1
.LC634:
	.string	"enable_armv7"
	.section	.rodata.str1.8
	.align 8
.LC635:
	.string	"deprecated (use --arm_arch instead)"
	.section	.rodata.str1.1
.LC636:
	.string	"enable_vfp3"
.LC637:
	.string	"enable_32dregs"
.LC638:
	.string	"enable_neon"
.LC639:
	.string	"enable_sudiv"
.LC640:
	.string	"enable_armv8"
	.section	.rodata.str1.8
	.align 8
.LC641:
	.string	"enable_regexp_unaligned_accesses"
	.align 8
.LC642:
	.string	"enable unaligned accesses for the regexp engine"
	.section	.rodata.str1.1
.LC643:
	.string	"script_streaming"
.LC644:
	.string	"enable parsing on background"
.LC645:
	.string	"disable_old_api_accessors"
	.section	.rodata.str1.8
	.align 8
.LC646:
	.string	"Disable old-style API accessors whose setters trigger through the prototype chain"
	.section	.rodata.str1.1
.LC647:
	.string	"expose_free_buffer"
.LC648:
	.string	"expose freeBuffer extension"
.LC649:
	.string	"expose_gc"
.LC650:
	.string	"expose gc extension"
.LC651:
	.string	"expose_gc_as"
	.section	.rodata.str1.8
	.align 8
.LC652:
	.string	"expose gc extension under the specified name"
	.section	.rodata.str1.1
.LC653:
	.string	"expose_externalize_string"
	.section	.rodata.str1.8
	.align 8
.LC654:
	.string	"expose externalize string extension"
	.section	.rodata.str1.1
.LC655:
	.string	"expose_trigger_failure"
	.section	.rodata.str1.8
	.align 8
.LC656:
	.string	"expose trigger-failure extension"
	.section	.rodata.str1.1
.LC657:
	.string	"stack_trace_limit"
	.section	.rodata.str1.8
	.align 8
.LC658:
	.string	"number of stack frames to capture"
	.section	.rodata.str1.1
.LC659:
	.string	"builtins_in_stack_traces"
	.section	.rodata.str1.8
	.align 8
.LC660:
	.string	"show built-in functions in stack traces"
	.align 8
.LC661:
	.string	"experimental_stack_trace_frames"
	.align 8
.LC662:
	.string	"enable experimental frames (API/Builtins) and stack trace layout"
	.align 8
.LC663:
	.string	"disallow_code_generation_from_strings"
	.section	.rodata.str1.1
.LC664:
	.string	"disallow eval and friends"
.LC665:
	.string	"expose_async_hooks"
.LC666:
	.string	"expose async_hooks object"
.LC667:
	.string	"expose_cputracemark_as"
	.section	.rodata.str1.8
	.align 8
.LC668:
	.string	"expose cputracemark extension under the specified name"
	.align 8
.LC669:
	.string	"allow_unsafe_function_constructor"
	.align 8
.LC670:
	.string	"allow invoking the function constructor without security checks"
	.section	.rodata.str1.1
.LC671:
	.string	"force_slow_path"
	.section	.rodata.str1.8
	.align 8
.LC672:
	.string	"always take the slow path for builtins"
	.align 8
.LC673:
	.string	"test_small_max_function_context_stub_size"
	.align 8
.LC674:
	.string	"enable testing the function context size overflow path by making the maximum size smaller"
	.section	.rodata.str1.1
.LC675:
	.string	"inline_new"
.LC676:
	.string	"use fast inline allocation"
.LC677:
	.string	"trace"
.LC678:
	.string	"trace function calls"
.LC679:
	.string	"lazy"
.LC680:
	.string	"use lazy compilation"
.LC681:
	.string	"max_lazy"
	.section	.rodata.str1.8
	.align 8
.LC682:
	.string	"ignore eager compilation hints"
	.section	.rodata.str1.1
.LC683:
	.string	"trace_opt"
.LC684:
	.string	"trace lazy optimization"
.LC685:
	.string	"trace_opt_verbose"
	.section	.rodata.str1.8
	.align 8
.LC686:
	.string	"extra verbose compilation tracing"
	.section	.rodata.str1.1
.LC687:
	.string	"trace_opt_stats"
	.section	.rodata.str1.8
	.align 8
.LC688:
	.string	"trace lazy optimization statistics"
	.section	.rodata.str1.1
.LC689:
	.string	"trace_deopt"
	.section	.rodata.str1.8
	.align 8
.LC690:
	.string	"trace optimize function deoptimization"
	.section	.rodata.str1.1
.LC691:
	.string	"trace_file_names"
	.section	.rodata.str1.8
	.align 8
.LC692:
	.string	"include file names in trace-opt/trace-deopt output"
	.section	.rodata.str1.1
.LC693:
	.string	"always_opt"
	.section	.rodata.str1.8
	.align 8
.LC694:
	.string	"always try to optimize functions"
	.section	.rodata.str1.1
.LC695:
	.string	"always_osr"
.LC696:
	.string	"always try to OSR functions"
.LC697:
	.string	"prepare_always_opt"
	.section	.rodata.str1.8
	.align 8
.LC698:
	.string	"prepare for turning on always opt"
	.section	.rodata.str1.1
.LC699:
	.string	"trace_serializer"
.LC700:
	.string	"print code serializer trace"
.LC701:
	.string	"compilation_cache"
.LC702:
	.string	"enable compilation cache"
.LC703:
	.string	"cache_prototype_transitions"
.LC704:
	.string	"cache prototype transitions"
.LC705:
	.string	"parallel_compile_tasks"
.LC706:
	.string	"enable parallel compile tasks"
.LC707:
	.string	"compiler_dispatcher"
.LC708:
	.string	"enable compiler dispatcher"
.LC709:
	.string	"trace_compiler_dispatcher"
	.section	.rodata.str1.8
	.align 8
.LC710:
	.string	"trace compiler dispatcher activity"
	.align 8
.LC711:
	.string	"cpu_profiler_sampling_interval"
	.align 8
.LC712:
	.string	"CPU profiler sampling interval in microseconds"
	.align 8
.LC713:
	.string	"trace_side_effect_free_debug_evaluate"
	.align 8
.LC714:
	.string	"print debug messages for side-effect-free debug-evaluate for testing"
	.section	.rodata.str1.1
.LC715:
	.string	"hard_abort"
.LC716:
	.string	"abort by crashing"
.LC717:
	.string	"expose_inspector_scripts"
	.section	.rodata.str1.8
	.align 8
.LC718:
	.string	"expose injected-script-source.js for debugging"
	.section	.rodata.str1.1
.LC719:
	.string	"stack_size"
	.section	.rodata.str1.8
	.align 8
.LC720:
	.string	"default size of stack region v8 is allowed to use (in kBytes)"
	.section	.rodata.str1.1
.LC721:
	.string	"max_stack_trace_source_length"
	.section	.rodata.str1.8
	.align 8
.LC722:
	.string	"maximum length of function source code printed in a stack trace."
	.section	.rodata.str1.1
.LC723:
	.string	"clear_exceptions_on_js_entry"
	.section	.rodata.str1.8
	.align 8
.LC724:
	.string	"clear pending exceptions when entering JavaScript"
	.section	.rodata.str1.1
.LC725:
	.string	"histogram_interval"
	.section	.rodata.str1.8
	.align 8
.LC726:
	.string	"time interval in ms for aggregating memory histograms"
	.section	.rodata.str1.1
.LC727:
	.string	"heap_profiler_trace_objects"
	.section	.rodata.str1.8
	.align 8
.LC728:
	.string	"Dump heap object allocations/movements/size_updates"
	.align 8
.LC729:
	.string	"heap_profiler_use_embedder_graph"
	.align 8
.LC730:
	.string	"Use the new EmbedderGraph API to get embedder nodes"
	.section	.rodata.str1.1
.LC731:
	.string	"heap_snapshot_string_limit"
	.section	.rodata.str1.8
	.align 8
.LC732:
	.string	"truncate strings to this length in the heap snapshot"
	.align 8
.LC733:
	.string	"sampling_heap_profiler_suppress_randomness"
	.align 8
.LC734:
	.string	"Use constant sample intervals to eliminate test flakiness"
	.section	.rodata.str1.1
.LC735:
	.string	"use_idle_notification"
	.section	.rodata.str1.8
	.align 8
.LC736:
	.string	"Use idle notification to reduce memory footprint."
	.section	.rodata.str1.1
.LC737:
	.string	"trace_ic"
	.section	.rodata.str1.8
	.align 8
.LC738:
	.string	"trace inline cache state transitions for tools/ic-processor"
	.align 8
.LC739:
	.string	"modify_field_representation_inplace"
	.align 8
.LC740:
	.string	"enable in-place field representation updates"
	.section	.rodata.str1.1
.LC741:
	.string	"max_polymorphic_map_count"
	.section	.rodata.str1.8
	.align 8
.LC742:
	.string	"maximum number of maps to track in POLYMORPHIC state"
	.section	.rodata.str1.1
.LC743:
	.string	"native_code_counters"
	.section	.rodata.str1.8
	.align 8
.LC744:
	.string	"generate extra code for manipulating stats counters"
	.section	.rodata.str1.1
.LC745:
	.string	"thin_strings"
.LC746:
	.string	"Enable ThinString support"
.LC747:
	.string	"trace_prototype_users"
	.section	.rodata.str1.8
	.align 8
.LC748:
	.string	"Trace updates to prototype user tracking"
	.section	.rodata.str1.1
.LC749:
	.string	"use_verbose_printer"
.LC750:
	.string	"allows verbose printing"
.LC751:
	.string	"trace_for_in_enumerate"
	.section	.rodata.str1.8
	.align 8
.LC752:
	.string	"Trace for-in enumerate slow-paths"
	.section	.rodata.str1.1
.LC753:
	.string	"trace_maps"
.LC754:
	.string	"trace map creation"
.LC755:
	.string	"trace_maps_details"
.LC756:
	.string	"also log map details"
.LC757:
	.string	"allow_natives_syntax"
.LC758:
	.string	"allow natives syntax"
.LC759:
	.string	"parse_only"
.LC760:
	.string	"only parse the sources"
.LC761:
	.string	"trace_sim"
.LC762:
	.string	"Trace simulator execution"
.LC763:
	.string	"debug_sim"
	.section	.rodata.str1.8
	.align 8
.LC764:
	.string	"Enable debugging the simulator"
	.section	.rodata.str1.1
.LC765:
	.string	"check_icache"
	.section	.rodata.str1.8
	.align 8
.LC766:
	.string	"Check icache flushes in ARM and MIPS simulator"
	.section	.rodata.str1.1
.LC767:
	.string	"stop_sim_at"
	.section	.rodata.str1.8
	.align 8
.LC768:
	.string	"Simulator stop after x number of instructions"
	.section	.rodata.str1.1
.LC769:
	.string	"sim_stack_alignment"
	.section	.rodata.str1.8
	.align 8
.LC770:
	.string	"Stack alingment in bytes in simulator (4 or 8, 8 is default)"
	.section	.rodata.str1.1
.LC771:
	.string	"sim_stack_size"
	.section	.rodata.str1.8
	.align 8
.LC772:
	.string	"Stack size of the ARM64, MIPS64 and PPC64 simulator in kBytes (default is 2 MB)"
	.section	.rodata.str1.1
.LC773:
	.string	"log_colour"
	.section	.rodata.str1.8
	.align 8
.LC774:
	.string	"When logging, try to use coloured output."
	.align 8
.LC775:
	.string	"ignore_asm_unimplemented_break"
	.align 8
.LC776:
	.string	"Don't break for ASM_UNIMPLEMENTED_BREAK macros."
	.section	.rodata.str1.1
.LC777:
	.string	"trace_sim_messages"
	.section	.rodata.str1.8
	.align 8
.LC778:
	.string	"Trace simulator debug messages. Implied by --trace-sim."
	.section	.rodata.str1.1
.LC779:
	.string	"async_stack_traces"
	.section	.rodata.str1.8
	.align 8
.LC780:
	.string	"include async stack traces in Error.stack"
	.section	.rodata.str1.1
.LC781:
	.string	"stack_trace_on_illegal"
	.section	.rodata.str1.8
	.align 8
.LC782:
	.string	"print stack trace when an illegal exception is thrown"
	.section	.rodata.str1.1
.LC783:
	.string	"abort_on_uncaught_exception"
	.section	.rodata.str1.8
	.align 8
.LC784:
	.string	"abort program (dump core) when an uncaught exception is thrown"
	.align 8
.LC785:
	.string	"correctness_fuzzer_suppressions"
	.align 8
.LC786:
	.string	"Suppress certain unspecified behaviors to ease correctness fuzzing: Abort program when the stack overflows or a string exceeds maximum length (as opposed to throwing RangeError). Use a fixed suppression string for error messages."
	.section	.rodata.str1.1
.LC787:
	.string	"randomize_hashes"
	.section	.rodata.str1.8
	.align 8
.LC788:
	.string	"randomize hashes to avoid predictable hash collisions (with snapshots this option cannot override the baked-in seed)"
	.section	.rodata.str1.1
.LC789:
	.string	"rehash_snapshot"
	.section	.rodata.str1.8
	.align 8
.LC790:
	.string	"rehash strings from the snapshot to override the baked-in seed"
	.section	.rodata.str1.1
.LC791:
	.string	"hash_seed"
	.section	.rodata.str1.8
	.align 8
.LC792:
	.string	"Fixed seed to use to hash property keys (0 means random)(with snapshots this option cannot override the baked-in seed)"
	.section	.rodata.str1.1
.LC793:
	.string	"random_seed"
	.section	.rodata.str1.8
	.align 8
.LC794:
	.string	"Default seed for initializing random generator (0, the default, means to use system random)."
	.section	.rodata.str1.1
.LC795:
	.string	"fuzzer_random_seed"
	.section	.rodata.str1.8
	.align 8
.LC796:
	.string	"Default seed for initializing fuzzer random generator (0, the default, means to use v8's random number generator seed)."
	.section	.rodata.str1.1
.LC797:
	.string	"trace_rail"
.LC798:
	.string	"trace RAIL mode"
.LC799:
	.string	"print_all_exceptions"
	.section	.rodata.str1.8
	.align 8
.LC800:
	.string	"print exception object and stack trace on each thrown exception"
	.section	.rodata.str1.1
.LC801:
	.string	"detailed_error_stack_trace"
	.section	.rodata.str1.8
	.align 8
.LC802:
	.string	"includes arguments for each function call in the error stack frames array"
	.section	.rodata.str1.1
.LC803:
	.string	"runtime_call_stats"
	.section	.rodata.str1.8
	.align 8
.LC804:
	.string	"report runtime call counts and times"
	.section	.rodata.str1.1
.LC805:
	.string	"profile_deserialization"
	.section	.rodata.str1.8
	.align 8
.LC806:
	.string	"Print the time it takes to deserialize the snapshot."
	.section	.rodata.str1.1
.LC807:
	.string	"serialization_statistics"
	.section	.rodata.str1.8
	.align 8
.LC808:
	.string	"Collect statistics on serialized objects."
	.section	.rodata.str1.1
.LC809:
	.string	"serialization_chunk_size"
	.section	.rodata.str1.8
	.align 8
.LC810:
	.string	"Custom size for serialization chunks"
	.section	.rodata.str1.1
.LC811:
	.string	"regexp_optimization"
	.section	.rodata.str1.8
	.align 8
.LC812:
	.string	"generate optimized regexp code"
	.section	.rodata.str1.1
.LC813:
	.string	"regexp_mode_modifiers"
	.section	.rodata.str1.8
	.align 8
.LC814:
	.string	"enable inline flags in regexp."
	.section	.rodata.str1.1
.LC815:
	.string	"regexp_interpret_all"
.LC816:
	.string	"interpret all regexp code"
.LC817:
	.string	"regexp_tier_up"
	.section	.rodata.str1.8
	.align 8
.LC818:
	.string	"enable regexp interpreter and tier up to the compiler"
	.section	.rodata.str1.1
.LC819:
	.string	"testing_bool_flag"
.LC820:
	.string	"testing_maybe_bool_flag"
.LC821:
	.string	"testing_int_flag"
.LC822:
	.string	"testing_float_flag"
.LC823:
	.string	"float-flag"
.LC824:
	.string	"testing_string_flag"
.LC825:
	.string	"string-flag"
.LC826:
	.string	"testing_prng_seed"
	.section	.rodata.str1.8
	.align 8
.LC827:
	.string	"Seed used for threading test randomness"
	.section	.rodata.str1.1
.LC828:
	.string	"testing_d8_test_runner"
	.section	.rodata.str1.8
	.align 8
.LC829:
	.string	"test runner turns on this flag to enable a check that the funciton was prepared for optimization before marking it for optimization"
	.section	.rodata.str1.1
.LC830:
	.string	"embedded_src"
	.section	.rodata.str1.8
	.align 8
.LC831:
	.string	"Path for the generated embedded data file. (mksnapshot only)"
	.section	.rodata.str1.1
.LC832:
	.string	"embedded_variant"
	.section	.rodata.str1.8
	.align 8
.LC833:
	.string	"Label to disambiguate symbols in embedded data file. (mksnapshot only)"
	.section	.rodata.str1.1
.LC834:
	.string	"startup_src"
	.section	.rodata.str1.8
	.align 8
.LC835:
	.string	"Write V8 startup as C++ src. (mksnapshot only)"
	.section	.rodata.str1.1
.LC836:
	.string	"startup_blob"
	.section	.rodata.str1.8
	.align 8
.LC837:
	.string	"Write V8 startup blob file. (mksnapshot only)"
	.section	.rodata.str1.1
.LC838:
	.string	"target_arch"
	.section	.rodata.str1.8
	.align 8
.LC839:
	.string	"The mksnapshot target arch. (mksnapshot only)"
	.section	.rodata.str1.1
.LC840:
	.string	"target_os"
	.section	.rodata.str1.8
	.align 8
.LC841:
	.string	"The mksnapshot target os. (mksnapshot only)"
	.section	.rodata.str1.1
.LC842:
	.string	"minor_mc_parallel_marking"
	.section	.rodata.str1.8
	.align 8
.LC843:
	.string	"use parallel marking for the young generation"
	.align 8
.LC844:
	.string	"trace_minor_mc_parallel_marking"
	.align 8
.LC845:
	.string	"trace parallel marking for the young generation"
	.section	.rodata.str1.1
.LC846:
	.string	"minor_mc"
	.section	.rodata.str1.8
	.align 8
.LC847:
	.string	"perform young generation mark compact GCs"
	.section	.rodata.str1.1
.LC848:
	.string	"help"
	.section	.rodata.str1.8
	.align 8
.LC849:
	.string	"Print usage message, including flags, on console"
	.section	.rodata.str1.1
.LC850:
	.string	"dump_counters"
.LC851:
	.string	"Dump counters on exit"
.LC852:
	.string	"dump_counters_nvp"
	.section	.rodata.str1.8
	.align 8
.LC853:
	.string	"Dump counters as name-value pairs on exit"
	.section	.rodata.str1.1
.LC854:
	.string	"use_external_strings"
	.section	.rodata.str1.8
	.align 8
.LC855:
	.string	"Use external strings for source code"
	.section	.rodata.str1.1
.LC856:
	.string	"map_counters"
.LC857:
	.string	"Map counters to a file"
.LC858:
	.string	"mock_arraybuffer_allocator"
	.section	.rodata.str1.8
	.align 8
.LC859:
	.string	"Use a mock ArrayBuffer allocator for testing."
	.align 8
.LC860:
	.string	"mock_arraybuffer_allocator_limit"
	.align 8
.LC861:
	.string	"Memory limit for mock ArrayBuffer allocator used to simulate OOM for testing."
	.section	.rodata.str1.1
.LC862:
	.string	"log"
	.section	.rodata.str1.8
	.align 8
.LC863:
	.string	"Minimal logging (no API, code, GC, suspect, or handles samples)."
	.section	.rodata.str1.1
.LC864:
	.string	"log_all"
	.section	.rodata.str1.8
	.align 8
.LC865:
	.string	"Log all events to the log file."
	.section	.rodata.str1.1
.LC866:
	.string	"log_api"
	.section	.rodata.str1.8
	.align 8
.LC867:
	.string	"Log API events to the log file."
	.section	.rodata.str1.1
.LC868:
	.string	"log_code"
	.section	.rodata.str1.8
	.align 8
.LC869:
	.string	"Log code events to the log file without profiling."
	.section	.rodata.str1.1
.LC870:
	.string	"log_handles"
.LC871:
	.string	"Log global handle events."
.LC872:
	.string	"log_suspect"
.LC873:
	.string	"Log suspect operations."
.LC874:
	.string	"log_source_code"
.LC875:
	.string	"Log source code."
.LC876:
	.string	"log_function_events"
	.section	.rodata.str1.8
	.align 8
.LC877:
	.string	"Log function events (parse, compile, execute) separately."
	.section	.rodata.str1.1
.LC878:
	.string	"prof"
	.section	.rodata.str1.8
	.align 8
.LC879:
	.string	"Log statistical profiling information (implies --log-code)."
	.section	.rodata.str1.1
.LC880:
	.string	"detailed_line_info"
	.section	.rodata.str1.8
	.align 8
.LC881:
	.string	"Always generate detailed line information for CPU profiling."
	.section	.rodata.str1.1
.LC882:
	.string	"prof_sampling_interval"
	.section	.rodata.str1.8
	.align 8
.LC883:
	.string	"Interval for --prof samples (in microseconds)."
	.section	.rodata.str1.1
.LC884:
	.string	"prof_cpp"
	.section	.rodata.str1.8
	.align 8
.LC885:
	.string	"Like --prof, but ignore generated code."
	.section	.rodata.str1.1
.LC886:
	.string	"prof_browser_mode"
	.section	.rodata.str1.8
	.align 8
.LC887:
	.string	"Used with --prof, turns on browser-compatible mode for profiling."
	.section	.rodata.str1.1
.LC888:
	.string	"logfile"
	.section	.rodata.str1.8
	.align 8
.LC889:
	.string	"Specify the name of the log file."
	.section	.rodata.str1.1
.LC890:
	.string	"logfile_per_isolate"
	.section	.rodata.str1.8
	.align 8
.LC891:
	.string	"Separate log files for each isolate."
	.section	.rodata.str1.1
.LC892:
	.string	"ll_prof"
	.section	.rodata.str1.8
	.align 8
.LC893:
	.string	"Enable low-level linux profiler."
	.section	.rodata.str1.1
.LC894:
	.string	"perf_basic_prof"
	.section	.rodata.str1.8
	.align 8
.LC895:
	.string	"Enable perf linux profiler (basic support)."
	.align 8
.LC896:
	.string	"perf_basic_prof_only_functions"
	.align 8
.LC897:
	.string	"Only report function code ranges to perf (i.e. no stubs)."
	.section	.rodata.str1.1
.LC898:
	.string	"perf_prof"
	.section	.rodata.str1.8
	.align 8
.LC899:
	.string	"Enable perf linux profiler (experimental annotate support)."
	.section	.rodata.str1.1
.LC900:
	.string	"perf_prof_annotate_wasm"
	.section	.rodata.str1.8
	.align 8
.LC901:
	.string	"Used with --perf-prof, load wasm source map and provide annotate support (experimental)."
	.section	.rodata.str1.1
.LC902:
	.string	"perf_prof_unwinding_info"
	.section	.rodata.str1.8
	.align 8
.LC903:
	.string	"Enable unwinding info for perf linux profiler (experimental)."
	.section	.rodata.str1.1
.LC904:
	.string	"gc_fake_mmap"
	.section	.rodata.str1.8
	.align 8
.LC905:
	.string	"Specify the name of the file for fake gc mmap used in ll_prof"
	.section	.rodata.str1.1
.LC906:
	.string	"log_internal_timer_events"
.LC907:
	.string	"Time internal events."
.LC908:
	.string	"log_instruction_stats"
	.section	.rodata.str1.8
	.align 8
.LC909:
	.string	"Log AArch64 instruction statistics."
	.section	.rodata.str1.1
.LC910:
	.string	"log_instruction_file"
	.section	.rodata.str1.8
	.align 8
.LC911:
	.string	"AArch64 instruction statistics log file."
	.section	.rodata.str1.1
.LC912:
	.string	"log_instruction_period"
	.section	.rodata.str1.8
	.align 8
.LC913:
	.string	"AArch64 instruction statistics logging period."
	.section	.rodata.str1.1
.LC914:
	.string	"redirect_code_traces"
	.section	.rodata.str1.8
	.align 8
.LC915:
	.string	"output deopt information and disassembly into file code-<pid>-<isolate id>.asm"
	.section	.rodata.str1.1
.LC916:
	.string	"redirect_code_traces_to"
	.section	.rodata.str1.8
	.align 8
.LC917:
	.string	"output deopt information and disassembly into the given file"
	.section	.rodata.str1.1
.LC918:
	.string	"print_opt_source"
	.section	.rodata.str1.8
	.align 8
.LC919:
	.string	"print source code of optimized and inlined functions"
	.section	.rodata.str1.1
.LC920:
	.string	"win64_unwinding_info"
	.section	.rodata.str1.8
	.align 8
.LC921:
	.string	"Enable unwinding info for Windows/x64"
	.align 8
.LC922:
	.string	"interpreted_frames_native_stack"
	.align 8
.LC923:
	.string	"Show interpreted frames on the native stack (useful for external profilers)."
	.section	.rodata.str1.1
.LC924:
	.string	"trace_elements_transitions"
.LC925:
	.string	"trace elements transitions"
	.section	.rodata.str1.8
	.align 8
.LC926:
	.string	"trace_creation_allocation_sites"
	.align 8
.LC927:
	.string	"trace the creation of allocation sites"
	.section	.rodata.str1.1
.LC928:
	.string	"print_code"
.LC929:
	.string	"print generated code"
.LC930:
	.string	"print_opt_code"
.LC931:
	.string	"print optimized code"
.LC932:
	.string	"print_opt_code_filter"
	.section	.rodata.str1.8
	.align 8
.LC933:
	.string	"filter for printing optimized code"
	.section	.rodata.str1.1
.LC934:
	.string	"print_code_verbose"
	.section	.rodata.str1.8
	.align 8
.LC935:
	.string	"print more information for code"
	.section	.rodata.str1.1
.LC936:
	.string	"print_builtin_code"
	.section	.rodata.str1.8
	.align 8
.LC937:
	.string	"print generated code for builtins"
	.section	.rodata.str1.1
.LC938:
	.string	"print_builtin_code_filter"
	.section	.rodata.str1.8
	.align 8
.LC939:
	.string	"filter for printing builtin code"
	.section	.rodata.str1.1
.LC940:
	.string	"print_regexp_code"
.LC941:
	.string	"print generated regexp code"
.LC942:
	.string	"print_regexp_bytecode"
	.section	.rodata.str1.8
	.align 8
.LC943:
	.string	"print generated regexp bytecode"
	.section	.rodata.str1.1
.LC944:
	.string	"print_builtin_size"
.LC945:
	.string	"print code size for builtins"
.LC946:
	.string	"sodium"
	.section	.rodata.str1.8
	.align 8
.LC947:
	.string	"print generated code output suitable for use with the Sodium code viewer"
	.section	.rodata.str1.1
.LC948:
	.string	"print_all_code"
	.section	.rodata.str1.8
	.align 8
.LC949:
	.string	"enable all flags related to printing code"
	.section	.rodata.str1.1
.LC950:
	.string	"predictable"
.LC951:
	.string	"enable predictable mode"
.LC952:
	.string	"predictable_gc_schedule"
	.section	.rodata.str1.8
	.align 8
.LC953:
	.string	"Predictable garbage collection schedule. Fixes heap growing, idle, and memory reducing behavior."
	.section	.rodata.str1.1
.LC954:
	.string	"single_threaded"
	.section	.rodata.str1.8
	.align 8
.LC955:
	.string	"disable the use of background tasks"
	.section	.rodata.str1.1
.LC956:
	.string	"single_threaded_gc"
	.section	.rodata.str1.8
	.align 8
.LC957:
	.string	"disable the use of background gc tasks"
	.section	.data.rel.local._ZN2v88internal12_GLOBAL__N_15flagsE,"aw"
	.align 32
	.type	_ZN2v88internal12_GLOBAL__N_15flagsE, @object
	.size	_ZN2v88internal12_GLOBAL__N_15flagsE, 22272
_ZN2v88internal12_GLOBAL__N_15flagsE:
	.long	0
	.zero	4
	.quad	.LC40
	.quad	_ZN2v88internal15FLAG_use_strictE
	.quad	_ZN2v88internalL22FLAGDEFAULT_use_strictE
	.quad	.LC41
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC42
	.quad	_ZN2v88internal15FLAG_es_stagingE
	.quad	_ZN2v88internalL22FLAGDEFAULT_es_stagingE
	.quad	.LC43
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC44
	.quad	_ZN2v88internal12FLAG_harmonyE
	.quad	_ZN2v88internalL19FLAGDEFAULT_harmonyE
	.quad	.LC45
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC46
	.quad	_ZN2v88internal21FLAG_harmony_shippingE
	.quad	_ZN2v88internalL28FLAGDEFAULT_harmony_shippingE
	.quad	.LC47
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC48
	.quad	_ZN2v88internal28FLAG_harmony_private_methodsE
	.quad	_ZN2v88internalL35FLAGDEFAULT_harmony_private_methodsE
	.quad	.LC49
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC50
	.quad	_ZN2v88internal28FLAG_harmony_regexp_sequenceE
	.quad	_ZN2v88internalL35FLAGDEFAULT_harmony_regexp_sequenceE
	.quad	.LC51
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC52
	.quad	_ZN2v88internal22FLAG_harmony_weak_refsE
	.quad	_ZN2v88internalL29FLAGDEFAULT_harmony_weak_refsE
	.quad	.LC53
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC54
	.quad	_ZN2v88internal30FLAG_harmony_optional_chainingE
	.quad	_ZN2v88internalL37FLAGDEFAULT_harmony_optional_chainingE
	.quad	.LC55
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC56
	.quad	_ZN2v88internal20FLAG_harmony_nullishE
	.quad	_ZN2v88internalL27FLAGDEFAULT_harmony_nullishE
	.quad	.LC57
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC58
	.quad	_ZN2v88internal36FLAG_harmony_intl_dateformat_quarterE
	.quad	_ZN2v88internalL43FLAGDEFAULT_harmony_intl_dateformat_quarterE
	.quad	.LC59
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC60
	.quad	_ZN2v88internal47FLAG_harmony_intl_add_calendar_numbering_systemE
	.quad	_ZN2v88internalL54FLAGDEFAULT_harmony_intl_add_calendar_numbering_systemE
	.quad	.LC61
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC62
	.quad	_ZN2v88internal39FLAG_harmony_intl_dateformat_day_periodE
	.quad	_ZN2v88internalL46FLAGDEFAULT_harmony_intl_dateformat_day_periodE
	.quad	.LC63
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC64
	.quad	_ZN2v88internal53FLAG_harmony_intl_dateformat_fractional_second_digitsE
	.quad	_ZN2v88internalL60FLAGDEFAULT_harmony_intl_dateformat_fractional_second_digitsE
	.quad	.LC65
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC66
	.quad	_ZN2v88internal27FLAG_harmony_intl_segmenterE
	.quad	_ZN2v88internalL34FLAGDEFAULT_harmony_intl_segmenterE
	.quad	.LC67
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC68
	.quad	_ZN2v88internal30FLAG_harmony_namespace_exportsE
	.quad	_ZN2v88internalL37FLAGDEFAULT_harmony_namespace_exportsE
	.quad	.LC69
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC70
	.quad	_ZN2v88internal30FLAG_harmony_sharedarraybufferE
	.quad	_ZN2v88internalL37FLAGDEFAULT_harmony_sharedarraybufferE
	.quad	.LC71
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC72
	.quad	_ZN2v88internal24FLAG_harmony_import_metaE
	.quad	_ZN2v88internalL31FLAGDEFAULT_harmony_import_metaE
	.quad	.LC73
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC74
	.quad	_ZN2v88internal27FLAG_harmony_dynamic_importE
	.quad	_ZN2v88internalL34FLAGDEFAULT_harmony_dynamic_importE
	.quad	.LC75
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC76
	.quad	_ZN2v88internal32FLAG_harmony_promise_all_settledE
	.quad	_ZN2v88internalL39FLAGDEFAULT_harmony_promise_all_settledE
	.quad	.LC77
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC78
	.quad	_ZN2v88internal24FLAG_harmony_intl_bigintE
	.quad	_ZN2v88internalL31FLAGDEFAULT_harmony_intl_bigintE
	.quad	.LC79
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC80
	.quad	_ZN2v88internal35FLAG_harmony_intl_date_format_rangeE
	.quad	_ZN2v88internalL42FLAGDEFAULT_harmony_intl_date_format_rangeE
	.quad	.LC81
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC82
	.quad	_ZN2v88internal32FLAG_harmony_intl_datetime_styleE
	.quad	_ZN2v88internalL39FLAGDEFAULT_harmony_intl_datetime_styleE
	.quad	.LC83
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC84
	.quad	_ZN2v88internal38FLAG_harmony_intl_numberformat_unifiedE
	.quad	_ZN2v88internalL45FLAGDEFAULT_harmony_intl_numberformat_unifiedE
	.quad	.LC85
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC86
	.quad	_ZN2v88internal22FLAG_icu_timezone_dataE
	.quad	_ZN2v88internalL29FLAGDEFAULT_icu_timezone_dataE
	.quad	.LC87
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC88
	.quad	_ZN2v88internal14FLAG_lite_modeE
	.quad	_ZN2v88internalL21FLAGDEFAULT_lite_modeE
	.quad	.LC89
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC90
	.quad	_ZN2v88internal11FLAG_futureE
	.quad	_ZN2v88internalL18FLAGDEFAULT_futureE
	.quad	.LC91
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC92
	.quad	_ZN2v88internal17FLAG_assert_typesE
	.quad	_ZN2v88internalL24FLAGDEFAULT_assert_typesE
	.quad	.LC93
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC94
	.quad	_ZN2v88internal32FLAG_allocation_site_pretenuringE
	.quad	_ZN2v88internalL39FLAGDEFAULT_allocation_site_pretenuringE
	.quad	.LC95
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC96
	.quad	_ZN2v88internal19FLAG_page_promotionE
	.quad	_ZN2v88internalL26FLAGDEFAULT_page_promotionE
	.quad	.LC97
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC98
	.quad	_ZN2v88internal29FLAG_page_promotion_thresholdE
	.quad	_ZN2v88internalL36FLAGDEFAULT_page_promotion_thresholdE
	.quad	.LC99
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC100
	.quad	_ZN2v88internal22FLAG_trace_pretenuringE
	.quad	_ZN2v88internalL29FLAGDEFAULT_trace_pretenuringE
	.quad	.LC101
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC102
	.quad	_ZN2v88internal33FLAG_trace_pretenuring_statisticsE
	.quad	_ZN2v88internalL40FLAGDEFAULT_trace_pretenuring_statisticsE
	.quad	.LC103
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC104
	.quad	_ZN2v88internal17FLAG_track_fieldsE
	.quad	_ZN2v88internalL24FLAGDEFAULT_track_fieldsE
	.quad	.LC105
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC106
	.quad	_ZN2v88internal24FLAG_track_double_fieldsE
	.quad	_ZN2v88internalL31FLAGDEFAULT_track_double_fieldsE
	.quad	.LC107
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC108
	.quad	_ZN2v88internal29FLAG_track_heap_object_fieldsE
	.quad	_ZN2v88internalL36FLAGDEFAULT_track_heap_object_fieldsE
	.quad	.LC109
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC110
	.quad	_ZN2v88internal26FLAG_track_computed_fieldsE
	.quad	_ZN2v88internalL33FLAGDEFAULT_track_computed_fieldsE
	.quad	.LC111
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC112
	.quad	_ZN2v88internal22FLAG_track_field_typesE
	.quad	_ZN2v88internalL29FLAGDEFAULT_track_field_typesE
	.quad	.LC113
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC114
	.quad	_ZN2v88internal25FLAG_trace_block_coverageE
	.quad	_ZN2v88internalL32FLAGDEFAULT_trace_block_coverageE
	.quad	.LC115
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC116
	.quad	_ZN2v88internal33FLAG_trace_protector_invalidationE
	.quad	_ZN2v88internalL40FLAGDEFAULT_trace_protector_invalidationE
	.quad	.LC117
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC118
	.quad	_ZN2v88internal27FLAG_feedback_normalizationE
	.quad	_ZN2v88internalL34FLAGDEFAULT_feedback_normalizationE
	.quad	.LC119
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC120
	.quad	_ZN2v88internal33FLAG_enable_one_shot_optimizationE
	.quad	_ZN2v88internalL40FLAGDEFAULT_enable_one_shot_optimizationE
	.quad	.LC121
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC122
	.quad	_ZN2v88internal24FLAG_unbox_double_arraysE
	.quad	_ZN2v88internalL31FLAGDEFAULT_unbox_double_arraysE
	.quad	.LC123
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC124
	.quad	_ZN2v88internal21FLAG_interrupt_budgetE
	.quad	_ZN2v88internalL28FLAGDEFAULT_interrupt_budgetE
	.quad	.LC125
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC126
	.quad	_ZN2v88internal12FLAG_jitlessE
	.quad	_ZN2v88internalL19FLAGDEFAULT_jitlessE
	.quad	.LC127
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC128
	.quad	_ZN2v88internal11FLAG_use_icE
	.quad	_ZN2v88internalL18FLAGDEFAULT_use_icE
	.quad	.LC129
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC130
	.quad	_ZN2v88internal42FLAG_budget_for_feedback_vector_allocationE
	.quad	_ZN2v88internalL49FLAGDEFAULT_budget_for_feedback_vector_allocationE
	.quad	.LC131
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC132
	.quad	_ZN2v88internal29FLAG_lazy_feedback_allocationE
	.quad	_ZN2v88internalL36FLAGDEFAULT_lazy_feedback_allocationE
	.quad	.LC133
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC134
	.quad	_ZN2v88internal42FLAG_ignition_elide_noneffectful_bytecodesE
	.quad	_ZN2v88internalL49FLAGDEFAULT_ignition_elide_noneffectful_bytecodesE
	.quad	.LC135
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC136
	.quad	_ZN2v88internal17FLAG_ignition_reoE
	.quad	_ZN2v88internalL24FLAGDEFAULT_ignition_reoE
	.quad	.LC137
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC138
	.quad	_ZN2v88internal41FLAG_ignition_filter_expression_positionsE
	.quad	_ZN2v88internalL48FLAGDEFAULT_ignition_filter_expression_positionsE
	.quad	.LC139
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC140
	.quad	_ZN2v88internal43FLAG_ignition_share_named_property_feedbackE
	.quad	_ZN2v88internalL50FLAGDEFAULT_ignition_share_named_property_feedbackE
	.quad	.LC141
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC142
	.quad	_ZN2v88internal19FLAG_print_bytecodeE
	.quad	_ZN2v88internalL26FLAGDEFAULT_print_bytecodeE
	.quad	.LC143
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC144
	.quad	_ZN2v88internal33FLAG_enable_lazy_source_positionsE
	.quad	_ZN2v88internalL40FLAGDEFAULT_enable_lazy_source_positionsE
	.quad	.LC145
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC146
	.quad	_ZN2v88internal33FLAG_stress_lazy_source_positionsE
	.quad	_ZN2v88internalL40FLAGDEFAULT_stress_lazy_source_positionsE
	.quad	.LC147
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC148
	.quad	_ZN2v88internal26FLAG_print_bytecode_filterE
	.quad	_ZN2v88internalL33FLAGDEFAULT_print_bytecode_filterE
	.quad	.LC149
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC150
	.quad	_ZN2v88internal27FLAG_trace_ignition_codegenE
	.quad	_ZN2v88internalL34FLAGDEFAULT_trace_ignition_codegenE
	.quad	.LC151
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC152
	.quad	_ZN2v88internal30FLAG_trace_ignition_dispatchesE
	.quad	_ZN2v88internalL37FLAGDEFAULT_trace_ignition_dispatchesE
	.quad	.LC153
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC154
	.quad	_ZN2v88internal42FLAG_trace_ignition_dispatches_output_fileE
	.quad	_ZN2v88internalL49FLAGDEFAULT_trace_ignition_dispatches_output_fileE
	.quad	.LC155
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC156
	.quad	_ZN2v88internal14FLAG_fast_mathE
	.quad	_ZN2v88internalL21FLAGDEFAULT_fast_mathE
	.quad	.LC157
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC158
	.quad	_ZN2v88internal33FLAG_trace_track_allocation_sitesE
	.quad	_ZN2v88internalL40FLAGDEFAULT_trace_track_allocation_sitesE
	.quad	.LC159
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC160
	.quad	_ZN2v88internal20FLAG_trace_migrationE
	.quad	_ZN2v88internalL27FLAGDEFAULT_trace_migrationE
	.quad	.LC161
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC162
	.quad	_ZN2v88internal25FLAG_trace_generalizationE
	.quad	_ZN2v88internalL32FLAGDEFAULT_trace_generalizationE
	.quad	.LC163
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC164
	.quad	_ZN2v88internal29FLAG_concurrent_recompilationE
	.quad	_ZN2v88internalL36FLAGDEFAULT_concurrent_recompilationE
	.quad	.LC165
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC166
	.quad	_ZN2v88internal35FLAG_trace_concurrent_recompilationE
	.quad	_ZN2v88internalL42FLAGDEFAULT_trace_concurrent_recompilationE
	.quad	.LC167
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC168
	.quad	_ZN2v88internal42FLAG_concurrent_recompilation_queue_lengthE
	.quad	_ZN2v88internalL49FLAGDEFAULT_concurrent_recompilation_queue_lengthE
	.quad	.LC169
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC170
	.quad	_ZN2v88internal35FLAG_concurrent_recompilation_delayE
	.quad	_ZN2v88internalL42FLAGDEFAULT_concurrent_recompilation_delayE
	.quad	.LC171
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC172
	.quad	_ZN2v88internal35FLAG_block_concurrent_recompilationE
	.quad	_ZN2v88internalL42FLAGDEFAULT_block_concurrent_recompilationE
	.quad	.LC173
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC174
	.quad	_ZN2v88internal24FLAG_concurrent_inliningE
	.quad	_ZN2v88internalL31FLAGDEFAULT_concurrent_inliningE
	.quad	.LC175
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC176
	.quad	_ZN2v88internal30FLAG_trace_heap_broker_verboseE
	.quad	_ZN2v88internalL37FLAGDEFAULT_trace_heap_broker_verboseE
	.quad	.LC177
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC178
	.quad	_ZN2v88internal22FLAG_trace_heap_brokerE
	.quad	_ZN2v88internalL29FLAGDEFAULT_trace_heap_brokerE
	.quad	.LC179
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC180
	.quad	_ZN2v88internal16FLAG_stress_runsE
	.quad	_ZN2v88internalL23FLAGDEFAULT_stress_runsE
	.quad	.LC181
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC182
	.quad	_ZN2v88internal24FLAG_deopt_every_n_timesE
	.quad	_ZN2v88internalL31FLAGDEFAULT_deopt_every_n_timesE
	.quad	.LC183
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC184
	.quad	_ZN2v88internal23FLAG_print_deopt_stressE
	.quad	_ZN2v88internalL30FLAGDEFAULT_print_deopt_stressE
	.quad	.LC185
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC186
	.quad	_ZN2v88internal8FLAG_optE
	.quad	_ZN2v88internalL15FLAGDEFAULT_optE
	.quad	.LC187
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC188
	.quad	_ZN2v88internal26FLAG_turbo_sp_frame_accessE
	.quad	_ZN2v88internalL33FLAGDEFAULT_turbo_sp_frame_accessE
	.quad	.LC189
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC190
	.quad	_ZN2v88internal40FLAG_turbo_control_flow_aware_allocationE
	.quad	_ZN2v88internalL47FLAGDEFAULT_turbo_control_flow_aware_allocationE
	.quad	.LC191
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC192
	.quad	_ZN2v88internal17FLAG_turbo_filterE
	.quad	_ZN2v88internalL24FLAGDEFAULT_turbo_filterE
	.quad	.LC193
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC194
	.quad	_ZN2v88internal16FLAG_trace_turboE
	.quad	_ZN2v88internalL23FLAGDEFAULT_trace_turboE
	.quad	.LC195
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC196
	.quad	_ZN2v88internal21FLAG_trace_turbo_pathE
	.quad	_ZN2v88internalL28FLAGDEFAULT_trace_turbo_pathE
	.quad	.LC197
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC198
	.quad	_ZN2v88internal23FLAG_trace_turbo_filterE
	.quad	_ZN2v88internalL30FLAGDEFAULT_trace_turbo_filterE
	.quad	.LC199
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC200
	.quad	_ZN2v88internal22FLAG_trace_turbo_graphE
	.quad	_ZN2v88internalL29FLAGDEFAULT_trace_turbo_graphE
	.quad	.LC201
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC202
	.quad	_ZN2v88internal26FLAG_trace_turbo_scheduledE
	.quad	_ZN2v88internalL33FLAGDEFAULT_trace_turbo_scheduledE
	.quad	.LC203
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC204
	.quad	_ZN2v88internal25FLAG_trace_turbo_cfg_fileE
	.quad	_ZN2v88internalL32FLAGDEFAULT_trace_turbo_cfg_fileE
	.quad	.LC205
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC206
	.quad	_ZN2v88internal22FLAG_trace_turbo_typesE
	.quad	_ZN2v88internalL29FLAGDEFAULT_trace_turbo_typesE
	.quad	.LC207
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC208
	.quad	_ZN2v88internal26FLAG_trace_turbo_schedulerE
	.quad	_ZN2v88internalL33FLAGDEFAULT_trace_turbo_schedulerE
	.quad	.LC209
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC210
	.quad	_ZN2v88internal26FLAG_trace_turbo_reductionE
	.quad	_ZN2v88internalL33FLAGDEFAULT_trace_turbo_reductionE
	.quad	.LC211
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC212
	.quad	_ZN2v88internal25FLAG_trace_turbo_trimmingE
	.quad	_ZN2v88internalL32FLAGDEFAULT_trace_turbo_trimmingE
	.quad	.LC213
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC214
	.quad	_ZN2v88internal19FLAG_trace_turbo_jtE
	.quad	_ZN2v88internalL26FLAGDEFAULT_trace_turbo_jtE
	.quad	.LC215
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC216
	.quad	_ZN2v88internal20FLAG_trace_turbo_ceqE
	.quad	_ZN2v88internalL27FLAGDEFAULT_trace_turbo_ceqE
	.quad	.LC217
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC218
	.quad	_ZN2v88internal21FLAG_trace_turbo_loopE
	.quad	_ZN2v88internalL28FLAGDEFAULT_trace_turbo_loopE
	.quad	.LC219
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC220
	.quad	_ZN2v88internal22FLAG_trace_turbo_allocE
	.quad	_ZN2v88internalL29FLAGDEFAULT_trace_turbo_allocE
	.quad	.LC221
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC222
	.quad	_ZN2v88internal19FLAG_trace_all_usesE
	.quad	_ZN2v88internalL26FLAGDEFAULT_trace_all_usesE
	.quad	.LC223
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC224
	.quad	_ZN2v88internal25FLAG_trace_representationE
	.quad	_ZN2v88internalL32FLAGDEFAULT_trace_representationE
	.quad	.LC225
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC226
	.quad	_ZN2v88internal17FLAG_turbo_verifyE
	.quad	_ZN2v88internalL24FLAGDEFAULT_turbo_verifyE
	.quad	.LC227
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC228
	.quad	_ZN2v88internal31FLAG_turbo_verify_machine_graphE
	.quad	_ZN2v88internalL38FLAGDEFAULT_turbo_verify_machine_graphE
	.quad	.LC229
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC230
	.quad	_ZN2v88internal21FLAG_trace_verify_csaE
	.quad	_ZN2v88internalL28FLAGDEFAULT_trace_verify_csaE
	.quad	.LC231
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC232
	.quad	_ZN2v88internal21FLAG_csa_trap_on_nodeE
	.quad	_ZN2v88internalL28FLAGDEFAULT_csa_trap_on_nodeE
	.quad	.LC233
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC234
	.quad	_ZN2v88internal16FLAG_turbo_statsE
	.quad	_ZN2v88internalL23FLAGDEFAULT_turbo_statsE
	.quad	.LC235
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC236
	.quad	_ZN2v88internal20FLAG_turbo_stats_nvpE
	.quad	_ZN2v88internalL27FLAGDEFAULT_turbo_stats_nvpE
	.quad	.LC237
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC238
	.quad	_ZN2v88internal21FLAG_turbo_stats_wasmE
	.quad	_ZN2v88internalL28FLAGDEFAULT_turbo_stats_wasmE
	.quad	.LC239
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC240
	.quad	_ZN2v88internal20FLAG_turbo_splittingE
	.quad	_ZN2v88internalL27FLAGDEFAULT_turbo_splittingE
	.quad	.LC241
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC242
	.quad	_ZN2v88internal36FLAG_function_context_specializationE
	.quad	_ZN2v88internalL43FLAGDEFAULT_function_context_specializationE
	.quad	.LC243
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC244
	.quad	_ZN2v88internal19FLAG_turbo_inliningE
	.quad	_ZN2v88internalL26FLAGDEFAULT_turbo_inliningE
	.quad	.LC245
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC246
	.quad	_ZN2v88internal30FLAG_max_inlined_bytecode_sizeE
	.quad	_ZN2v88internalL37FLAGDEFAULT_max_inlined_bytecode_sizeE
	.quad	.LC247
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC248
	.quad	_ZN2v88internal41FLAG_max_inlined_bytecode_size_cumulativeE
	.quad	_ZN2v88internalL48FLAGDEFAULT_max_inlined_bytecode_size_cumulativeE
	.quad	.LC249
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC250
	.quad	_ZN2v88internal39FLAG_max_inlined_bytecode_size_absoluteE
	.quad	_ZN2v88internalL46FLAGDEFAULT_max_inlined_bytecode_size_absoluteE
	.quad	.LC249
	.byte	0
	.zero	7
	.long	5
	.zero	4
	.quad	.LC251
	.quad	_ZN2v88internal39FLAG_reserve_inline_budget_scale_factorE
	.quad	_ZN2v88internalL46FLAGDEFAULT_reserve_inline_budget_scale_factorE
	.quad	.LC249
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC252
	.quad	_ZN2v88internal36FLAG_max_inlined_bytecode_size_smallE
	.quad	_ZN2v88internalL43FLAGDEFAULT_max_inlined_bytecode_size_smallE
	.quad	.LC253
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC254
	.quad	_ZN2v88internal32FLAG_max_optimized_bytecode_sizeE
	.quad	_ZN2v88internalL39FLAGDEFAULT_max_optimized_bytecode_sizeE
	.quad	.LC255
	.byte	0
	.zero	7
	.long	5
	.zero	4
	.quad	.LC256
	.quad	_ZN2v88internal27FLAG_min_inlining_frequencyE
	.quad	_ZN2v88internalL34FLAGDEFAULT_min_inlining_frequencyE
	.quad	.LC257
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC258
	.quad	_ZN2v88internal25FLAG_polymorphic_inliningE
	.quad	_ZN2v88internalL32FLAGDEFAULT_polymorphic_inliningE
	.quad	.LC259
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC260
	.quad	_ZN2v88internal18FLAG_stress_inlineE
	.quad	_ZN2v88internalL25FLAGDEFAULT_stress_inlineE
	.quad	.LC261
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC262
	.quad	_ZN2v88internal25FLAG_trace_turbo_inliningE
	.quad	_ZN2v88internalL32FLAGDEFAULT_trace_turbo_inliningE
	.quad	.LC263
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC264
	.quad	_ZN2v88internal21FLAG_inline_accessorsE
	.quad	_ZN2v88internalL28FLAGDEFAULT_inline_accessorsE
	.quad	.LC265
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC266
	.quad	_ZN2v88internal32FLAG_turbo_inline_array_builtinsE
	.quad	_ZN2v88internalL39FLAGDEFAULT_turbo_inline_array_builtinsE
	.quad	.LC267
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC268
	.quad	_ZN2v88internal12FLAG_use_osrE
	.quad	_ZN2v88internalL19FLAGDEFAULT_use_osrE
	.quad	.LC269
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC270
	.quad	_ZN2v88internal14FLAG_trace_osrE
	.quad	_ZN2v88internalL21FLAGDEFAULT_trace_osrE
	.quad	.LC271
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC272
	.quad	_ZN2v88internal33FLAG_analyze_environment_livenessE
	.quad	_ZN2v88internalL40FLAGDEFAULT_analyze_environment_livenessE
	.quad	.LC273
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC274
	.quad	_ZN2v88internal31FLAG_trace_environment_livenessE
	.quad	_ZN2v88internalL38FLAGDEFAULT_trace_environment_livenessE
	.quad	.LC275
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC276
	.quad	_ZN2v88internal27FLAG_turbo_load_eliminationE
	.quad	_ZN2v88internalL34FLAGDEFAULT_turbo_load_eliminationE
	.quad	.LC277
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC278
	.quad	_ZN2v88internal33FLAG_trace_turbo_load_eliminationE
	.quad	_ZN2v88internalL40FLAGDEFAULT_trace_turbo_load_eliminationE
	.quad	.LC279
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC280
	.quad	_ZN2v88internal20FLAG_turbo_profilingE
	.quad	_ZN2v88internalL27FLAGDEFAULT_turbo_profilingE
	.quad	.LC281
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC282
	.quad	_ZN2v88internal28FLAG_turbo_verify_allocationE
	.quad	_ZN2v88internalL35FLAGDEFAULT_turbo_verify_allocationE
	.quad	.LC283
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC284
	.quad	_ZN2v88internal28FLAG_turbo_move_optimizationE
	.quad	_ZN2v88internalL35FLAGDEFAULT_turbo_move_optimizationE
	.quad	.LC285
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC286
	.quad	_ZN2v88internal13FLAG_turbo_jtE
	.quad	_ZN2v88internalL20FLAGDEFAULT_turbo_jtE
	.quad	.LC287
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC288
	.quad	_ZN2v88internal23FLAG_turbo_loop_peelingE
	.quad	_ZN2v88internalL30FLAGDEFAULT_turbo_loop_peelingE
	.quad	.LC289
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC290
	.quad	_ZN2v88internal24FLAG_turbo_loop_variableE
	.quad	_ZN2v88internalL31FLAGDEFAULT_turbo_loop_variableE
	.quad	.LC291
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC292
	.quad	_ZN2v88internal24FLAG_turbo_loop_rotationE
	.quad	_ZN2v88internalL31FLAGDEFAULT_turbo_loop_rotationE
	.quad	.LC293
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC294
	.quad	_ZN2v88internal26FLAG_turbo_cf_optimizationE
	.quad	_ZN2v88internalL33FLAGDEFAULT_turbo_cf_optimizationE
	.quad	.LC295
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC296
	.quad	_ZN2v88internal17FLAG_turbo_escapeE
	.quad	_ZN2v88internalL24FLAGDEFAULT_turbo_escapeE
	.quad	.LC297
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC298
	.quad	_ZN2v88internal29FLAG_turbo_allocation_foldingE
	.quad	_ZN2v88internalL36FLAGDEFAULT_turbo_allocation_foldingE
	.quad	.LC299
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC300
	.quad	_ZN2v88internal33FLAG_turbo_instruction_schedulingE
	.quad	_ZN2v88internalL40FLAGDEFAULT_turbo_instruction_schedulingE
	.quad	.LC301
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC302
	.quad	_ZN2v88internal40FLAG_turbo_stress_instruction_schedulingE
	.quad	_ZN2v88internalL47FLAGDEFAULT_turbo_stress_instruction_schedulingE
	.quad	.LC303
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC304
	.quad	_ZN2v88internal28FLAG_turbo_store_eliminationE
	.quad	_ZN2v88internalL35FLAGDEFAULT_turbo_store_eliminationE
	.quad	.LC305
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC306
	.quad	_ZN2v88internal28FLAG_trace_store_eliminationE
	.quad	_ZN2v88internalL35FLAGDEFAULT_trace_store_eliminationE
	.quad	.LC307
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC308
	.quad	_ZN2v88internal28FLAG_turbo_rewrite_far_jumpsE
	.quad	_ZN2v88internalL35FLAGDEFAULT_turbo_rewrite_far_jumpsE
	.quad	.LC309
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC310
	.quad	_ZN2v88internal44FLAG_experimental_inline_promise_constructorE
	.quad	_ZN2v88internalL51FLAGDEFAULT_experimental_inline_promise_constructorE
	.quad	.LC311
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC312
	.quad	_ZN2v88internal33FLAG_stress_gc_during_compilationE
	.quad	_ZN2v88internalL40FLAGDEFAULT_stress_gc_during_compilationE
	.quad	.LC313
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC314
	.quad	_ZN2v88internal22FLAG_optimize_for_sizeE
	.quad	_ZN2v88internalL29FLAGDEFAULT_optimize_for_sizeE
	.quad	.LC315
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC316
	.quad	_ZN2v88internal31FLAG_untrusted_code_mitigationsE
	.quad	_ZN2v88internalL38FLAGDEFAULT_untrusted_code_mitigationsE
	.quad	.LC317
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC318
	.quad	_ZN2v88internal16FLAG_expose_wasmE
	.quad	_ZN2v88internalL23FLAGDEFAULT_expose_wasmE
	.quad	.LC319
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC320
	.quad	_ZN2v88internal24FLAG_assume_asmjs_originE
	.quad	_ZN2v88internalL31FLAGDEFAULT_assume_asmjs_originE
	.quad	.LC321
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC322
	.quad	_ZN2v88internal36FLAG_wasm_disable_structured_cloningE
	.quad	_ZN2v88internalL43FLAGDEFAULT_wasm_disable_structured_cloningE
	.quad	.LC323
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC324
	.quad	_ZN2v88internal31FLAG_wasm_num_compilation_tasksE
	.quad	_ZN2v88internalL38FLAGDEFAULT_wasm_num_compilation_tasksE
	.quad	.LC325
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC326
	.quad	_ZN2v88internal35FLAG_wasm_write_protect_code_memoryE
	.quad	_ZN2v88internalL42FLAGDEFAULT_wasm_write_protect_code_memoryE
	.quad	.LC327
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC328
	.quad	_ZN2v88internal29FLAG_trace_wasm_serializationE
	.quad	_ZN2v88internalL36FLAGDEFAULT_trace_wasm_serializationE
	.quad	.LC329
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC330
	.quad	_ZN2v88internal27FLAG_wasm_async_compilationE
	.quad	_ZN2v88internalL34FLAGDEFAULT_wasm_async_compilationE
	.quad	.LC331
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC332
	.quad	_ZN2v88internal24FLAG_wasm_test_streamingE
	.quad	_ZN2v88internalL31FLAGDEFAULT_wasm_test_streamingE
	.quad	.LC333
	.byte	0
	.zero	7
	.long	3
	.zero	4
	.quad	.LC334
	.quad	_ZN2v88internal23FLAG_wasm_max_mem_pagesE
	.quad	_ZN2v88internalL30FLAGDEFAULT_wasm_max_mem_pagesE
	.quad	.LC335
	.byte	0
	.zero	7
	.long	3
	.zero	4
	.quad	.LC336
	.quad	_ZN2v88internal24FLAG_wasm_max_table_sizeE
	.quad	_ZN2v88internalL31FLAGDEFAULT_wasm_max_table_sizeE
	.quad	.LC337
	.byte	0
	.zero	7
	.long	3
	.zero	4
	.quad	.LC338
	.quad	_ZN2v88internal24FLAG_wasm_max_code_spaceE
	.quad	_ZN2v88internalL31FLAGDEFAULT_wasm_max_code_spaceE
	.quad	.LC339
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC340
	.quad	_ZN2v88internal17FLAG_wasm_tier_upE
	.quad	_ZN2v88internalL24FLAGDEFAULT_wasm_tier_upE
	.quad	.LC341
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC342
	.quad	_ZN2v88internal25FLAG_trace_wasm_ast_startE
	.quad	_ZN2v88internalL32FLAGDEFAULT_trace_wasm_ast_startE
	.quad	.LC343
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC344
	.quad	_ZN2v88internal23FLAG_trace_wasm_ast_endE
	.quad	_ZN2v88internalL30FLAGDEFAULT_trace_wasm_ast_endE
	.quad	.LC345
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC346
	.quad	_ZN2v88internal12FLAG_liftoffE
	.quad	_ZN2v88internalL19FLAGDEFAULT_liftoffE
	.quad	.LC347
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC348
	.quad	_ZN2v88internal22FLAG_trace_wasm_memoryE
	.quad	_ZN2v88internalL29FLAGDEFAULT_trace_wasm_memoryE
	.quad	.LC349
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC350
	.quad	_ZN2v88internal31FLAG_wasm_tier_mask_for_testingE
	.quad	_ZN2v88internalL38FLAGDEFAULT_wasm_tier_mask_for_testingE
	.quad	.LC351
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC352
	.quad	_ZN2v88internal17FLAG_validate_asmE
	.quad	_ZN2v88internalL24FLAGDEFAULT_validate_asmE
	.quad	.LC353
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC354
	.quad	_ZN2v88internal26FLAG_suppress_asm_messagesE
	.quad	_ZN2v88internalL33FLAGDEFAULT_suppress_asm_messagesE
	.quad	.LC355
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC356
	.quad	_ZN2v88internal19FLAG_trace_asm_timeE
	.quad	_ZN2v88internalL26FLAGDEFAULT_trace_asm_timeE
	.quad	.LC357
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC358
	.quad	_ZN2v88internal22FLAG_trace_asm_scannerE
	.quad	_ZN2v88internalL29FLAGDEFAULT_trace_asm_scannerE
	.quad	.LC359
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC360
	.quad	_ZN2v88internal21FLAG_trace_asm_parserE
	.quad	_ZN2v88internalL28FLAGDEFAULT_trace_asm_parserE
	.quad	.LC361
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC362
	.quad	_ZN2v88internal24FLAG_stress_validate_asmE
	.quad	_ZN2v88internalL31FLAGDEFAULT_stress_validate_asmE
	.quad	.LC363
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC364
	.quad	_ZN2v88internal26FLAG_dump_wasm_module_pathE
	.quad	_ZN2v88internalL33FLAGDEFAULT_dump_wasm_module_pathE
	.quad	.LC365
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC366
	.quad	_ZN2v88internal25FLAG_experimental_wasm_mvE
	.quad	_ZN2v88internalL32FLAGDEFAULT_experimental_wasm_mvE
	.quad	.LC367
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC368
	.quad	_ZN2v88internal25FLAG_experimental_wasm_ehE
	.quad	_ZN2v88internalL32FLAGDEFAULT_experimental_wasm_ehE
	.quad	.LC369
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC370
	.quad	_ZN2v88internal30FLAG_experimental_wasm_threadsE
	.quad	_ZN2v88internalL37FLAGDEFAULT_experimental_wasm_threadsE
	.quad	.LC371
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC372
	.quad	_ZN2v88internal27FLAG_experimental_wasm_simdE
	.quad	_ZN2v88internalL34FLAGDEFAULT_experimental_wasm_simdE
	.quad	.LC373
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC374
	.quad	_ZN2v88internal29FLAG_experimental_wasm_bigintE
	.quad	_ZN2v88internalL36FLAGDEFAULT_experimental_wasm_bigintE
	.quad	.LC375
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC376
	.quad	_ZN2v88internal34FLAG_experimental_wasm_return_callE
	.quad	_ZN2v88internalL41FLAGDEFAULT_experimental_wasm_return_callE
	.quad	.LC377
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC378
	.quad	_ZN2v88internal40FLAG_experimental_wasm_compilation_hintsE
	.quad	_ZN2v88internalL47FLAGDEFAULT_experimental_wasm_compilation_hintsE
	.quad	.LC379
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC380
	.quad	_ZN2v88internal29FLAG_experimental_wasm_anyrefE
	.quad	_ZN2v88internalL36FLAGDEFAULT_experimental_wasm_anyrefE
	.quad	.LC381
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC382
	.quad	_ZN2v88internal38FLAG_experimental_wasm_type_reflectionE
	.quad	_ZN2v88internalL45FLAGDEFAULT_experimental_wasm_type_reflectionE
	.quad	.LC383
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC384
	.quad	_ZN2v88internal34FLAG_experimental_wasm_bulk_memoryE
	.quad	_ZN2v88internalL41FLAGDEFAULT_experimental_wasm_bulk_memoryE
	.quad	.LC385
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC386
	.quad	_ZN2v88internal42FLAG_experimental_wasm_sat_f2i_conversionsE
	.quad	_ZN2v88internalL49FLAGDEFAULT_experimental_wasm_sat_f2i_conversionsE
	.quad	.LC387
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC388
	.quad	_ZN2v88internal25FLAG_experimental_wasm_seE
	.quad	_ZN2v88internalL32FLAGDEFAULT_experimental_wasm_seE
	.quad	.LC389
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC390
	.quad	_ZN2v88internal17FLAG_wasm_stagingE
	.quad	_ZN2v88internalL24FLAGDEFAULT_wasm_stagingE
	.quad	.LC391
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC392
	.quad	_ZN2v88internal13FLAG_wasm_optE
	.quad	_ZN2v88internalL20FLAGDEFAULT_wasm_optE
	.quad	.LC393
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC394
	.quad	_ZN2v88internal26FLAG_wasm_no_bounds_checksE
	.quad	_ZN2v88internalL33FLAGDEFAULT_wasm_no_bounds_checksE
	.quad	.LC395
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC396
	.quad	_ZN2v88internal25FLAG_wasm_no_stack_checksE
	.quad	_ZN2v88internalL32FLAGDEFAULT_wasm_no_stack_checksE
	.quad	.LC397
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC398
	.quad	_ZN2v88internal25FLAG_wasm_math_intrinsicsE
	.quad	_ZN2v88internalL32FLAGDEFAULT_wasm_math_intrinsicsE
	.quad	.LC399
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC400
	.quad	_ZN2v88internal23FLAG_wasm_shared_engineE
	.quad	_ZN2v88internalL30FLAGDEFAULT_wasm_shared_engineE
	.quad	.LC401
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC402
	.quad	_ZN2v88internal21FLAG_wasm_shared_codeE
	.quad	_ZN2v88internalL28FLAGDEFAULT_wasm_shared_codeE
	.quad	.LC403
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC404
	.quad	_ZN2v88internal22FLAG_wasm_trap_handlerE
	.quad	_ZN2v88internalL29FLAGDEFAULT_wasm_trap_handlerE
	.quad	.LC405
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC406
	.quad	_ZN2v88internal25FLAG_wasm_fuzzer_gen_testE
	.quad	_ZN2v88internalL32FLAGDEFAULT_wasm_fuzzer_gen_testE
	.quad	.LC407
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC408
	.quad	_ZN2v88internal20FLAG_print_wasm_codeE
	.quad	_ZN2v88internalL27FLAGDEFAULT_print_wasm_codeE
	.quad	.LC409
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC410
	.quad	_ZN2v88internal25FLAG_print_wasm_stub_codeE
	.quad	_ZN2v88internalL32FLAGDEFAULT_print_wasm_stub_codeE
	.quad	.LC411
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC412
	.quad	_ZN2v88internal23FLAG_wasm_interpret_allE
	.quad	_ZN2v88internalL30FLAGDEFAULT_wasm_interpret_allE
	.quad	.LC413
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC414
	.quad	_ZN2v88internal30FLAG_asm_wasm_lazy_compilationE
	.quad	_ZN2v88internalL37FLAGDEFAULT_asm_wasm_lazy_compilationE
	.quad	.LC415
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC416
	.quad	_ZN2v88internal26FLAG_wasm_lazy_compilationE
	.quad	_ZN2v88internalL33FLAGDEFAULT_wasm_lazy_compilationE
	.quad	.LC417
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC418
	.quad	_ZN2v88internal28FLAG_wasm_grow_shared_memoryE
	.quad	_ZN2v88internalL35FLAGDEFAULT_wasm_grow_shared_memoryE
	.quad	.LC419
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC420
	.quad	_ZN2v88internal25FLAG_wasm_lazy_validationE
	.quad	_ZN2v88internalL32FLAGDEFAULT_wasm_lazy_validationE
	.quad	.LC421
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC422
	.quad	_ZN2v88internal17FLAG_wasm_code_gcE
	.quad	_ZN2v88internalL24FLAGDEFAULT_wasm_code_gcE
	.quad	.LC423
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC424
	.quad	_ZN2v88internal23FLAG_trace_wasm_code_gcE
	.quad	_ZN2v88internalL30FLAGDEFAULT_trace_wasm_code_gcE
	.quad	.LC425
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC426
	.quad	_ZN2v88internal24FLAG_stress_wasm_code_gcE
	.quad	_ZN2v88internalL31FLAGDEFAULT_stress_wasm_code_gcE
	.quad	.LC427
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC428
	.quad	_ZN2v88internal16FLAG_frame_countE
	.quad	_ZN2v88internalL23FLAGDEFAULT_frame_countE
	.quad	.LC429
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC430
	.quad	_ZN2v88internal40FLAG_stress_sampling_allocation_profilerE
	.quad	_ZN2v88internalL47FLAGDEFAULT_stress_sampling_allocation_profilerE
	.quad	.LC431
	.byte	0
	.zero	7
	.long	6
	.zero	4
	.quad	.LC432
	.quad	_ZN2v88internal24FLAG_min_semi_space_sizeE
	.quad	_ZN2v88internalL31FLAGDEFAULT_min_semi_space_sizeE
	.quad	.LC433
	.byte	0
	.zero	7
	.long	6
	.zero	4
	.quad	.LC434
	.quad	_ZN2v88internal24FLAG_max_semi_space_sizeE
	.quad	_ZN2v88internalL31FLAGDEFAULT_max_semi_space_sizeE
	.quad	.LC435
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC436
	.quad	_ZN2v88internal29FLAG_semi_space_growth_factorE
	.quad	_ZN2v88internalL36FLAGDEFAULT_semi_space_growth_factorE
	.quad	.LC437
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC438
	.quad	_ZN2v88internal44FLAG_experimental_new_space_growth_heuristicE
	.quad	_ZN2v88internalL51FLAGDEFAULT_experimental_new_space_growth_heuristicE
	.quad	.LC439
	.byte	0
	.zero	7
	.long	6
	.zero	4
	.quad	.LC440
	.quad	_ZN2v88internal23FLAG_max_old_space_sizeE
	.quad	_ZN2v88internalL30FLAGDEFAULT_max_old_space_sizeE
	.quad	.LC441
	.byte	0
	.zero	7
	.long	6
	.zero	4
	.quad	.LC442
	.quad	_ZN2v88internal18FLAG_max_heap_sizeE
	.quad	_ZN2v88internalL25FLAGDEFAULT_max_heap_sizeE
	.quad	.LC443
	.byte	0
	.zero	7
	.long	6
	.zero	4
	.quad	.LC444
	.quad	_ZN2v88internal22FLAG_initial_heap_sizeE
	.quad	_ZN2v88internalL29FLAGDEFAULT_initial_heap_sizeE
	.quad	.LC445
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC446
	.quad	_ZN2v88internal33FLAG_huge_max_old_generation_sizeE
	.quad	_ZN2v88internalL40FLAGDEFAULT_huge_max_old_generation_sizeE
	.quad	.LC447
	.byte	0
	.zero	7
	.long	6
	.zero	4
	.quad	.LC448
	.quad	_ZN2v88internal27FLAG_initial_old_space_sizeE
	.quad	_ZN2v88internalL34FLAGDEFAULT_initial_old_space_sizeE
	.quad	.LC449
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC450
	.quad	_ZN2v88internal25FLAG_global_gc_schedulingE
	.quad	_ZN2v88internalL32FLAGDEFAULT_global_gc_schedulingE
	.quad	.LC451
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC452
	.quad	_ZN2v88internal14FLAG_gc_globalE
	.quad	_ZN2v88internalL21FLAGDEFAULT_gc_globalE
	.quad	.LC453
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC454
	.quad	_ZN2v88internal23FLAG_random_gc_intervalE
	.quad	_ZN2v88internalL30FLAGDEFAULT_random_gc_intervalE
	.quad	.LC455
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC456
	.quad	_ZN2v88internal16FLAG_gc_intervalE
	.quad	_ZN2v88internalL23FLAGDEFAULT_gc_intervalE
	.quad	.LC457
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC458
	.quad	_ZN2v88internal25FLAG_retain_maps_for_n_gcE
	.quad	_ZN2v88internalL32FLAGDEFAULT_retain_maps_for_n_gcE
	.quad	.LC459
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC460
	.quad	_ZN2v88internal13FLAG_trace_gcE
	.quad	_ZN2v88internalL20FLAGDEFAULT_trace_gcE
	.quad	.LC461
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC462
	.quad	_ZN2v88internal17FLAG_trace_gc_nvpE
	.quad	_ZN2v88internalL24FLAGDEFAULT_trace_gc_nvpE
	.quad	.LC463
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC464
	.quad	_ZN2v88internal30FLAG_trace_gc_ignore_scavengerE
	.quad	_ZN2v88internalL37FLAGDEFAULT_trace_gc_ignore_scavengerE
	.quad	.LC465
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC466
	.quad	_ZN2v88internal28FLAG_trace_idle_notificationE
	.quad	_ZN2v88internalL35FLAGDEFAULT_trace_idle_notificationE
	.quad	.LC467
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC468
	.quad	_ZN2v88internal36FLAG_trace_idle_notification_verboseE
	.quad	_ZN2v88internalL43FLAGDEFAULT_trace_idle_notification_verboseE
	.quad	.LC469
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC470
	.quad	_ZN2v88internal21FLAG_trace_gc_verboseE
	.quad	_ZN2v88internalL28FLAGDEFAULT_trace_gc_verboseE
	.quad	.LC471
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC472
	.quad	_ZN2v88internal23FLAG_trace_gc_freelistsE
	.quad	_ZN2v88internalL30FLAGDEFAULT_trace_gc_freelistsE
	.quad	.LC473
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC474
	.quad	_ZN2v88internal31FLAG_trace_gc_freelists_verboseE
	.quad	_ZN2v88internalL38FLAGDEFAULT_trace_gc_freelists_verboseE
	.quad	.LC475
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC476
	.quad	_ZN2v88internal32FLAG_trace_evacuation_candidatesE
	.quad	_ZN2v88internalL39FLAGDEFAULT_trace_evacuation_candidatesE
	.quad	.LC477
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC478
	.quad	_ZN2v88internal30FLAG_trace_allocations_originsE
	.quad	_ZN2v88internalL37FLAGDEFAULT_trace_allocations_originsE
	.quad	.LC479
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC480
	.quad	_ZN2v88internal25FLAG_gc_freelist_strategyE
	.quad	_ZN2v88internalL32FLAGDEFAULT_gc_freelist_strategyE
	.quad	.LC481
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC482
	.quad	_ZN2v88internal36FLAG_trace_allocation_stack_intervalE
	.quad	_ZN2v88internalL43FLAGDEFAULT_trace_allocation_stack_intervalE
	.quad	.LC483
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC484
	.quad	_ZN2v88internal33FLAG_trace_duplicate_threshold_kbE
	.quad	_ZN2v88internalL40FLAGDEFAULT_trace_duplicate_threshold_kbE
	.quad	.LC485
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC486
	.quad	_ZN2v88internal24FLAG_trace_fragmentationE
	.quad	_ZN2v88internalL31FLAGDEFAULT_trace_fragmentationE
	.quad	.LC487
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC488
	.quad	_ZN2v88internal32FLAG_trace_fragmentation_verboseE
	.quad	_ZN2v88internalL39FLAGDEFAULT_trace_fragmentation_verboseE
	.quad	.LC489
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC490
	.quad	_ZN2v88internal21FLAG_trace_evacuationE
	.quad	_ZN2v88internalL28FLAGDEFAULT_trace_evacuationE
	.quad	.LC491
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC492
	.quad	_ZN2v88internal30FLAG_trace_mutator_utilizationE
	.quad	_ZN2v88internalL37FLAGDEFAULT_trace_mutator_utilizationE
	.quad	.LC493
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC494
	.quad	_ZN2v88internal24FLAG_incremental_markingE
	.quad	_ZN2v88internalL31FLAGDEFAULT_incremental_markingE
	.quad	.LC495
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC496
	.quad	_ZN2v88internal33FLAG_incremental_marking_wrappersE
	.quad	_ZN2v88internalL40FLAGDEFAULT_incremental_marking_wrappersE
	.quad	.LC497
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC498
	.quad	_ZN2v88internal19FLAG_trace_unmapperE
	.quad	_ZN2v88internalL26FLAGDEFAULT_trace_unmapperE
	.quad	.LC499
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC500
	.quad	_ZN2v88internal22FLAG_parallel_scavengeE
	.quad	_ZN2v88internalL29FLAGDEFAULT_parallel_scavengeE
	.quad	.LC501
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC502
	.quad	_ZN2v88internal28FLAG_trace_parallel_scavengeE
	.quad	_ZN2v88internalL35FLAGDEFAULT_trace_parallel_scavengeE
	.quad	.LC503
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC504
	.quad	_ZN2v88internal30FLAG_write_protect_code_memoryE
	.quad	_ZN2v88internalL37FLAGDEFAULT_write_protect_code_memoryE
	.quad	.LC505
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC506
	.quad	_ZN2v88internal23FLAG_concurrent_markingE
	.quad	_ZN2v88internalL30FLAGDEFAULT_concurrent_markingE
	.quad	.LC507
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC508
	.quad	_ZN2v88internal21FLAG_parallel_markingE
	.quad	_ZN2v88internalL28FLAGDEFAULT_parallel_markingE
	.quad	.LC509
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC510
	.quad	_ZN2v88internal34FLAG_ephemeron_fixpoint_iterationsE
	.quad	_ZN2v88internalL41FLAGDEFAULT_ephemeron_fixpoint_iterationsE
	.quad	.LC511
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC512
	.quad	_ZN2v88internal29FLAG_trace_concurrent_markingE
	.quad	_ZN2v88internalL36FLAGDEFAULT_trace_concurrent_markingE
	.quad	.LC513
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC514
	.quad	_ZN2v88internal28FLAG_concurrent_store_bufferE
	.quad	_ZN2v88internalL35FLAGDEFAULT_concurrent_store_bufferE
	.quad	.LC515
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC516
	.quad	_ZN2v88internal24FLAG_concurrent_sweepingE
	.quad	_ZN2v88internalL31FLAGDEFAULT_concurrent_sweepingE
	.quad	.LC517
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC518
	.quad	_ZN2v88internal24FLAG_parallel_compactionE
	.quad	_ZN2v88internalL31FLAGDEFAULT_parallel_compactionE
	.quad	.LC519
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC520
	.quad	_ZN2v88internal28FLAG_parallel_pointer_updateE
	.quad	_ZN2v88internalL35FLAGDEFAULT_parallel_pointer_updateE
	.quad	.LC521
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC522
	.quad	_ZN2v88internal43FLAG_detect_ineffective_gcs_near_heap_limitE
	.quad	_ZN2v88internalL50FLAGDEFAULT_detect_ineffective_gcs_near_heap_limitE
	.quad	.LC523
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC524
	.quad	_ZN2v88internal30FLAG_trace_incremental_markingE
	.quad	_ZN2v88internalL37FLAGDEFAULT_trace_incremental_markingE
	.quad	.LC525
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC526
	.quad	_ZN2v88internal25FLAG_trace_stress_markingE
	.quad	_ZN2v88internalL32FLAGDEFAULT_trace_stress_markingE
	.quad	.LC527
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC528
	.quad	_ZN2v88internal26FLAG_trace_stress_scavengeE
	.quad	_ZN2v88internalL33FLAGDEFAULT_trace_stress_scavengeE
	.quad	.LC529
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC530
	.quad	_ZN2v88internal26FLAG_track_gc_object_statsE
	.quad	_ZN2v88internalL33FLAGDEFAULT_track_gc_object_statsE
	.quad	.LC531
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC532
	.quad	_ZN2v88internal26FLAG_trace_gc_object_statsE
	.quad	_ZN2v88internalL33FLAGDEFAULT_trace_gc_object_statsE
	.quad	.LC533
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC534
	.quad	_ZN2v88internal21FLAG_trace_zone_statsE
	.quad	_ZN2v88internalL28FLAGDEFAULT_trace_zone_statsE
	.quad	.LC535
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC536
	.quad	_ZN2v88internal25FLAG_track_retaining_pathE
	.quad	_ZN2v88internalL32FLAGDEFAULT_track_retaining_pathE
	.quad	.LC537
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC538
	.quad	_ZN2v88internal36FLAG_concurrent_array_buffer_freeingE
	.quad	_ZN2v88internalL43FLAGDEFAULT_concurrent_array_buffer_freeingE
	.quad	.LC539
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC540
	.quad	_ZN2v88internal13FLAG_gc_statsE
	.quad	_ZN2v88internalL20FLAGDEFAULT_gc_statsE
	.quad	.LC541
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC542
	.quad	_ZN2v88internal28FLAG_track_detached_contextsE
	.quad	_ZN2v88internalL35FLAGDEFAULT_track_detached_contextsE
	.quad	.LC543
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC544
	.quad	_ZN2v88internal28FLAG_trace_detached_contextsE
	.quad	_ZN2v88internalL35FLAGDEFAULT_trace_detached_contextsE
	.quad	.LC545
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC546
	.quad	_ZN2v88internal22FLAG_move_object_startE
	.quad	_ZN2v88internalL29FLAGDEFAULT_move_object_startE
	.quad	.LC547
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC548
	.quad	_ZN2v88internal19FLAG_memory_reducerE
	.quad	_ZN2v88internalL26FLAGDEFAULT_memory_reducerE
	.quad	.LC549
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC550
	.quad	_ZN2v88internal35FLAG_memory_reducer_for_small_heapsE
	.quad	_ZN2v88internalL42FLAGDEFAULT_memory_reducer_for_small_heapsE
	.quad	.LC551
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC552
	.quad	_ZN2v88internal25FLAG_heap_growing_percentE
	.quad	_ZN2v88internalL32FLAGDEFAULT_heap_growing_percentE
	.quad	.LC553
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC554
	.quad	_ZN2v88internal20FLAG_v8_os_page_sizeE
	.quad	_ZN2v88internalL27FLAGDEFAULT_v8_os_page_sizeE
	.quad	.LC555
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC556
	.quad	_ZN2v88internal19FLAG_always_compactE
	.quad	_ZN2v88internalL26FLAGDEFAULT_always_compactE
	.quad	.LC557
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC558
	.quad	_ZN2v88internal18FLAG_never_compactE
	.quad	_ZN2v88internalL25FLAGDEFAULT_never_compactE
	.quad	.LC559
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC560
	.quad	_ZN2v88internal23FLAG_compact_code_spaceE
	.quad	_ZN2v88internalL30FLAGDEFAULT_compact_code_spaceE
	.quad	.LC561
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC562
	.quad	_ZN2v88internal19FLAG_flush_bytecodeE
	.quad	_ZN2v88internalL26FLAGDEFAULT_flush_bytecodeE
	.quad	.LC563
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC564
	.quad	_ZN2v88internal26FLAG_stress_flush_bytecodeE
	.quad	_ZN2v88internalL33FLAGDEFAULT_stress_flush_bytecodeE
	.quad	.LC565
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC566
	.quad	_ZN2v88internal29FLAG_use_marking_progress_barE
	.quad	_ZN2v88internalL36FLAGDEFAULT_use_marking_progress_barE
	.quad	.LC567
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC568
	.quad	_ZN2v88internal34FLAG_force_marking_deque_overflowsE
	.quad	_ZN2v88internalL41FLAGDEFAULT_force_marking_deque_overflowsE
	.quad	.LC569
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC570
	.quad	_ZN2v88internal22FLAG_stress_compactionE
	.quad	_ZN2v88internalL29FLAGDEFAULT_stress_compactionE
	.quad	.LC571
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC572
	.quad	_ZN2v88internal29FLAG_stress_compaction_randomE
	.quad	_ZN2v88internalL36FLAGDEFAULT_stress_compaction_randomE
	.quad	.LC573
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC574
	.quad	_ZN2v88internal31FLAG_stress_incremental_markingE
	.quad	_ZN2v88internalL38FLAGDEFAULT_stress_incremental_markingE
	.quad	.LC575
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC576
	.quad	_ZN2v88internal23FLAG_fuzzer_gc_analysisE
	.quad	_ZN2v88internalL30FLAGDEFAULT_fuzzer_gc_analysisE
	.quad	.LC577
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC578
	.quad	_ZN2v88internal19FLAG_stress_markingE
	.quad	_ZN2v88internalL26FLAGDEFAULT_stress_markingE
	.quad	.LC579
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC580
	.quad	_ZN2v88internal20FLAG_stress_scavengeE
	.quad	_ZN2v88internalL27FLAGDEFAULT_stress_scavengeE
	.quad	.LC581
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC582
	.quad	_ZN2v88internal38FLAG_gc_experiment_background_scheduleE
	.quad	_ZN2v88internalL45FLAGDEFAULT_gc_experiment_background_scheduleE
	.quad	.LC583
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC584
	.quad	_ZN2v88internal34FLAG_gc_experiment_less_compactionE
	.quad	_ZN2v88internalL41FLAGDEFAULT_gc_experiment_less_compactionE
	.quad	.LC585
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC586
	.quad	_ZN2v88internal20FLAG_disable_abortjsE
	.quad	_ZN2v88internalL27FLAGDEFAULT_disable_abortjsE
	.quad	.LC587
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC588
	.quad	_ZN2v88internal43FLAG_manual_evacuation_candidates_selectionE
	.quad	_ZN2v88internalL50FLAGDEFAULT_manual_evacuation_candidates_selectionE
	.quad	.LC589
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC590
	.quad	_ZN2v88internal29FLAG_fast_promotion_new_spaceE
	.quad	_ZN2v88internalL36FLAGDEFAULT_fast_promotion_new_spaceE
	.quad	.LC591
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC592
	.quad	_ZN2v88internal22FLAG_clear_free_memoryE
	.quad	_ZN2v88internalL29FLAGDEFAULT_clear_free_memoryE
	.quad	.LC593
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC594
	.quad	_ZN2v88internal35FLAG_young_generation_large_objectsE
	.quad	_ZN2v88internalL42FLAGDEFAULT_young_generation_large_objectsE
	.quad	.LC595
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC596
	.quad	_ZN2v88internal23FLAG_idle_time_scavengeE
	.quad	_ZN2v88internalL30FLAGDEFAULT_idle_time_scavengeE
	.quad	.LC597
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC598
	.quad	_ZN2v88internal15FLAG_debug_codeE
	.quad	_ZN2v88internalL22FLAGDEFAULT_debug_codeE
	.quad	.LC599
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC600
	.quad	_ZN2v88internal18FLAG_code_commentsE
	.quad	_ZN2v88internalL25FLAGDEFAULT_code_commentsE
	.quad	.LC601
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC602
	.quad	_ZN2v88internal16FLAG_enable_sse3E
	.quad	_ZN2v88internalL23FLAGDEFAULT_enable_sse3E
	.quad	.LC603
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC604
	.quad	_ZN2v88internal17FLAG_enable_ssse3E
	.quad	_ZN2v88internalL24FLAGDEFAULT_enable_ssse3E
	.quad	.LC605
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC606
	.quad	_ZN2v88internal18FLAG_enable_sse4_1E
	.quad	_ZN2v88internalL25FLAGDEFAULT_enable_sse4_1E
	.quad	.LC607
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC608
	.quad	_ZN2v88internal18FLAG_enable_sse4_2E
	.quad	_ZN2v88internalL25FLAGDEFAULT_enable_sse4_2E
	.quad	.LC609
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC610
	.quad	_ZN2v88internal16FLAG_enable_sahfE
	.quad	_ZN2v88internalL23FLAGDEFAULT_enable_sahfE
	.quad	.LC611
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC612
	.quad	_ZN2v88internal15FLAG_enable_avxE
	.quad	_ZN2v88internalL22FLAGDEFAULT_enable_avxE
	.quad	.LC613
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC614
	.quad	_ZN2v88internal16FLAG_enable_fma3E
	.quad	_ZN2v88internalL23FLAGDEFAULT_enable_fma3E
	.quad	.LC615
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC616
	.quad	_ZN2v88internal16FLAG_enable_bmi1E
	.quad	_ZN2v88internalL23FLAGDEFAULT_enable_bmi1E
	.quad	.LC617
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC618
	.quad	_ZN2v88internal16FLAG_enable_bmi2E
	.quad	_ZN2v88internalL23FLAGDEFAULT_enable_bmi2E
	.quad	.LC619
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC620
	.quad	_ZN2v88internal17FLAG_enable_lzcntE
	.quad	_ZN2v88internalL24FLAGDEFAULT_enable_lzcntE
	.quad	.LC621
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC622
	.quad	_ZN2v88internal18FLAG_enable_popcntE
	.quad	_ZN2v88internalL25FLAGDEFAULT_enable_popcntE
	.quad	.LC623
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC624
	.quad	_ZN2v88internal13FLAG_arm_archE
	.quad	_ZN2v88internalL20FLAGDEFAULT_arm_archE
	.quad	.LC625
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC626
	.quad	_ZN2v88internal24FLAG_force_long_branchesE
	.quad	_ZN2v88internalL31FLAGDEFAULT_force_long_branchesE
	.quad	.LC627
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC628
	.quad	_ZN2v88internal9FLAG_mcpuE
	.quad	_ZN2v88internalL16FLAGDEFAULT_mcpuE
	.quad	.LC629
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC630
	.quad	_ZN2v88internal26FLAG_partial_constant_poolE
	.quad	_ZN2v88internalL33FLAGDEFAULT_partial_constant_poolE
	.quad	.LC631
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC632
	.quad	_ZN2v88internal30FLAG_enable_source_at_csa_bindE
	.quad	_ZN2v88internalL37FLAGDEFAULT_enable_source_at_csa_bindE
	.quad	.LC633
	.byte	0
	.zero	7
	.long	1
	.zero	4
	.quad	.LC634
	.quad	_ZN2v88internal17FLAG_enable_armv7E
	.quad	_ZN2v88internalL24FLAGDEFAULT_enable_armv7E
	.quad	.LC635
	.byte	0
	.zero	7
	.long	1
	.zero	4
	.quad	.LC636
	.quad	_ZN2v88internal16FLAG_enable_vfp3E
	.quad	_ZN2v88internalL23FLAGDEFAULT_enable_vfp3E
	.quad	.LC635
	.byte	0
	.zero	7
	.long	1
	.zero	4
	.quad	.LC637
	.quad	_ZN2v88internal19FLAG_enable_32dregsE
	.quad	_ZN2v88internalL26FLAGDEFAULT_enable_32dregsE
	.quad	.LC635
	.byte	0
	.zero	7
	.long	1
	.zero	4
	.quad	.LC638
	.quad	_ZN2v88internal16FLAG_enable_neonE
	.quad	_ZN2v88internalL23FLAGDEFAULT_enable_neonE
	.quad	.LC635
	.byte	0
	.zero	7
	.long	1
	.zero	4
	.quad	.LC639
	.quad	_ZN2v88internal17FLAG_enable_sudivE
	.quad	_ZN2v88internalL24FLAGDEFAULT_enable_sudivE
	.quad	.LC635
	.byte	0
	.zero	7
	.long	1
	.zero	4
	.quad	.LC640
	.quad	_ZN2v88internal17FLAG_enable_armv8E
	.quad	_ZN2v88internalL24FLAGDEFAULT_enable_armv8E
	.quad	.LC635
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC641
	.quad	_ZN2v88internal37FLAG_enable_regexp_unaligned_accessesE
	.quad	_ZN2v88internalL44FLAGDEFAULT_enable_regexp_unaligned_accessesE
	.quad	.LC642
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC643
	.quad	_ZN2v88internal21FLAG_script_streamingE
	.quad	_ZN2v88internalL28FLAGDEFAULT_script_streamingE
	.quad	.LC644
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC645
	.quad	_ZN2v88internal30FLAG_disable_old_api_accessorsE
	.quad	_ZN2v88internalL37FLAGDEFAULT_disable_old_api_accessorsE
	.quad	.LC646
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC647
	.quad	_ZN2v88internal23FLAG_expose_free_bufferE
	.quad	_ZN2v88internalL30FLAGDEFAULT_expose_free_bufferE
	.quad	.LC648
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC649
	.quad	_ZN2v88internal14FLAG_expose_gcE
	.quad	_ZN2v88internalL21FLAGDEFAULT_expose_gcE
	.quad	.LC650
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC651
	.quad	_ZN2v88internal17FLAG_expose_gc_asE
	.quad	_ZN2v88internalL24FLAGDEFAULT_expose_gc_asE
	.quad	.LC652
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC653
	.quad	_ZN2v88internal30FLAG_expose_externalize_stringE
	.quad	_ZN2v88internalL37FLAGDEFAULT_expose_externalize_stringE
	.quad	.LC654
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC655
	.quad	_ZN2v88internal27FLAG_expose_trigger_failureE
	.quad	_ZN2v88internalL34FLAGDEFAULT_expose_trigger_failureE
	.quad	.LC656
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC657
	.quad	_ZN2v88internal22FLAG_stack_trace_limitE
	.quad	_ZN2v88internalL29FLAGDEFAULT_stack_trace_limitE
	.quad	.LC658
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC659
	.quad	_ZN2v88internal29FLAG_builtins_in_stack_tracesE
	.quad	_ZN2v88internalL36FLAGDEFAULT_builtins_in_stack_tracesE
	.quad	.LC660
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC661
	.quad	_ZN2v88internal36FLAG_experimental_stack_trace_framesE
	.quad	_ZN2v88internalL43FLAGDEFAULT_experimental_stack_trace_framesE
	.quad	.LC662
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC663
	.quad	_ZN2v88internal42FLAG_disallow_code_generation_from_stringsE
	.quad	_ZN2v88internalL49FLAGDEFAULT_disallow_code_generation_from_stringsE
	.quad	.LC664
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC665
	.quad	_ZN2v88internal23FLAG_expose_async_hooksE
	.quad	_ZN2v88internalL30FLAGDEFAULT_expose_async_hooksE
	.quad	.LC666
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC667
	.quad	_ZN2v88internal27FLAG_expose_cputracemark_asE
	.quad	_ZN2v88internalL34FLAGDEFAULT_expose_cputracemark_asE
	.quad	.LC668
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC669
	.quad	_ZN2v88internal38FLAG_allow_unsafe_function_constructorE
	.quad	_ZN2v88internalL45FLAGDEFAULT_allow_unsafe_function_constructorE
	.quad	.LC670
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC671
	.quad	_ZN2v88internal20FLAG_force_slow_pathE
	.quad	_ZN2v88internalL27FLAGDEFAULT_force_slow_pathE
	.quad	.LC672
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC673
	.quad	_ZN2v88internal46FLAG_test_small_max_function_context_stub_sizeE
	.quad	_ZN2v88internalL53FLAGDEFAULT_test_small_max_function_context_stub_sizeE
	.quad	.LC674
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC675
	.quad	_ZN2v88internal15FLAG_inline_newE
	.quad	_ZN2v88internalL22FLAGDEFAULT_inline_newE
	.quad	.LC676
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC677
	.quad	_ZN2v88internal10FLAG_traceE
	.quad	_ZN2v88internalL17FLAGDEFAULT_traceE
	.quad	.LC678
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC679
	.quad	_ZN2v88internal9FLAG_lazyE
	.quad	_ZN2v88internalL16FLAGDEFAULT_lazyE
	.quad	.LC680
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC681
	.quad	_ZN2v88internal13FLAG_max_lazyE
	.quad	_ZN2v88internalL20FLAGDEFAULT_max_lazyE
	.quad	.LC682
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC683
	.quad	_ZN2v88internal14FLAG_trace_optE
	.quad	_ZN2v88internalL21FLAGDEFAULT_trace_optE
	.quad	.LC684
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC685
	.quad	_ZN2v88internal22FLAG_trace_opt_verboseE
	.quad	_ZN2v88internalL29FLAGDEFAULT_trace_opt_verboseE
	.quad	.LC686
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC687
	.quad	_ZN2v88internal20FLAG_trace_opt_statsE
	.quad	_ZN2v88internalL27FLAGDEFAULT_trace_opt_statsE
	.quad	.LC688
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC689
	.quad	_ZN2v88internal16FLAG_trace_deoptE
	.quad	_ZN2v88internalL23FLAGDEFAULT_trace_deoptE
	.quad	.LC690
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC691
	.quad	_ZN2v88internal21FLAG_trace_file_namesE
	.quad	_ZN2v88internalL28FLAGDEFAULT_trace_file_namesE
	.quad	.LC692
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC693
	.quad	_ZN2v88internal15FLAG_always_optE
	.quad	_ZN2v88internalL22FLAGDEFAULT_always_optE
	.quad	.LC694
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC695
	.quad	_ZN2v88internal15FLAG_always_osrE
	.quad	_ZN2v88internalL22FLAGDEFAULT_always_osrE
	.quad	.LC696
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC697
	.quad	_ZN2v88internal23FLAG_prepare_always_optE
	.quad	_ZN2v88internalL30FLAGDEFAULT_prepare_always_optE
	.quad	.LC698
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC699
	.quad	_ZN2v88internal21FLAG_trace_serializerE
	.quad	_ZN2v88internalL28FLAGDEFAULT_trace_serializerE
	.quad	.LC700
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC701
	.quad	_ZN2v88internal22FLAG_compilation_cacheE
	.quad	_ZN2v88internalL29FLAGDEFAULT_compilation_cacheE
	.quad	.LC702
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC703
	.quad	_ZN2v88internal32FLAG_cache_prototype_transitionsE
	.quad	_ZN2v88internalL39FLAGDEFAULT_cache_prototype_transitionsE
	.quad	.LC704
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC705
	.quad	_ZN2v88internal27FLAG_parallel_compile_tasksE
	.quad	_ZN2v88internalL34FLAGDEFAULT_parallel_compile_tasksE
	.quad	.LC706
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC707
	.quad	_ZN2v88internal24FLAG_compiler_dispatcherE
	.quad	_ZN2v88internalL31FLAGDEFAULT_compiler_dispatcherE
	.quad	.LC708
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC709
	.quad	_ZN2v88internal30FLAG_trace_compiler_dispatcherE
	.quad	_ZN2v88internalL37FLAGDEFAULT_trace_compiler_dispatcherE
	.quad	.LC710
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC711
	.quad	_ZN2v88internal35FLAG_cpu_profiler_sampling_intervalE
	.quad	_ZN2v88internalL42FLAGDEFAULT_cpu_profiler_sampling_intervalE
	.quad	.LC712
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC713
	.quad	_ZN2v88internal42FLAG_trace_side_effect_free_debug_evaluateE
	.quad	_ZN2v88internalL49FLAGDEFAULT_trace_side_effect_free_debug_evaluateE
	.quad	.LC714
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC715
	.quad	_ZN2v88internal15FLAG_hard_abortE
	.quad	_ZN2v88internalL22FLAGDEFAULT_hard_abortE
	.quad	.LC716
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC717
	.quad	_ZN2v88internal29FLAG_expose_inspector_scriptsE
	.quad	_ZN2v88internalL36FLAGDEFAULT_expose_inspector_scriptsE
	.quad	.LC718
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC719
	.quad	_ZN2v88internal15FLAG_stack_sizeE
	.quad	_ZN2v88internalL22FLAGDEFAULT_stack_sizeE
	.quad	.LC720
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC721
	.quad	_ZN2v88internal34FLAG_max_stack_trace_source_lengthE
	.quad	_ZN2v88internalL41FLAGDEFAULT_max_stack_trace_source_lengthE
	.quad	.LC722
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC723
	.quad	_ZN2v88internal33FLAG_clear_exceptions_on_js_entryE
	.quad	_ZN2v88internalL40FLAGDEFAULT_clear_exceptions_on_js_entryE
	.quad	.LC724
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC725
	.quad	_ZN2v88internal23FLAG_histogram_intervalE
	.quad	_ZN2v88internalL30FLAGDEFAULT_histogram_intervalE
	.quad	.LC726
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC727
	.quad	_ZN2v88internal32FLAG_heap_profiler_trace_objectsE
	.quad	_ZN2v88internalL39FLAGDEFAULT_heap_profiler_trace_objectsE
	.quad	.LC728
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC729
	.quad	_ZN2v88internal37FLAG_heap_profiler_use_embedder_graphE
	.quad	_ZN2v88internalL44FLAGDEFAULT_heap_profiler_use_embedder_graphE
	.quad	.LC730
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC731
	.quad	_ZN2v88internal31FLAG_heap_snapshot_string_limitE
	.quad	_ZN2v88internalL38FLAGDEFAULT_heap_snapshot_string_limitE
	.quad	.LC732
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC733
	.quad	_ZN2v88internal47FLAG_sampling_heap_profiler_suppress_randomnessE
	.quad	_ZN2v88internalL54FLAGDEFAULT_sampling_heap_profiler_suppress_randomnessE
	.quad	.LC734
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC735
	.quad	_ZN2v88internal26FLAG_use_idle_notificationE
	.quad	_ZN2v88internalL33FLAGDEFAULT_use_idle_notificationE
	.quad	.LC736
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC737
	.quad	_ZN2v88internal13FLAG_trace_icE
	.quad	_ZN2v88internalL20FLAGDEFAULT_trace_icE
	.quad	.LC738
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC739
	.quad	_ZN2v88internal40FLAG_modify_field_representation_inplaceE
	.quad	_ZN2v88internalL47FLAGDEFAULT_modify_field_representation_inplaceE
	.quad	.LC740
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC741
	.quad	_ZN2v88internal30FLAG_max_polymorphic_map_countE
	.quad	_ZN2v88internalL37FLAGDEFAULT_max_polymorphic_map_countE
	.quad	.LC742
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC743
	.quad	_ZN2v88internal25FLAG_native_code_countersE
	.quad	_ZN2v88internalL32FLAGDEFAULT_native_code_countersE
	.quad	.LC744
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC745
	.quad	_ZN2v88internal17FLAG_thin_stringsE
	.quad	_ZN2v88internalL24FLAGDEFAULT_thin_stringsE
	.quad	.LC746
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC747
	.quad	_ZN2v88internal26FLAG_trace_prototype_usersE
	.quad	_ZN2v88internalL33FLAGDEFAULT_trace_prototype_usersE
	.quad	.LC748
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC749
	.quad	_ZN2v88internal24FLAG_use_verbose_printerE
	.quad	_ZN2v88internalL31FLAGDEFAULT_use_verbose_printerE
	.quad	.LC750
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC751
	.quad	_ZN2v88internal27FLAG_trace_for_in_enumerateE
	.quad	_ZN2v88internalL34FLAGDEFAULT_trace_for_in_enumerateE
	.quad	.LC752
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC753
	.quad	_ZN2v88internal15FLAG_trace_mapsE
	.quad	_ZN2v88internalL22FLAGDEFAULT_trace_mapsE
	.quad	.LC754
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC755
	.quad	_ZN2v88internal23FLAG_trace_maps_detailsE
	.quad	_ZN2v88internalL30FLAGDEFAULT_trace_maps_detailsE
	.quad	.LC756
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC757
	.quad	_ZN2v88internal25FLAG_allow_natives_syntaxE
	.quad	_ZN2v88internalL32FLAGDEFAULT_allow_natives_syntaxE
	.quad	.LC758
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC759
	.quad	_ZN2v88internal15FLAG_parse_onlyE
	.quad	_ZN2v88internalL22FLAGDEFAULT_parse_onlyE
	.quad	.LC760
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC761
	.quad	_ZN2v88internal14FLAG_trace_simE
	.quad	_ZN2v88internalL21FLAGDEFAULT_trace_simE
	.quad	.LC762
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC763
	.quad	_ZN2v88internal14FLAG_debug_simE
	.quad	_ZN2v88internalL21FLAGDEFAULT_debug_simE
	.quad	.LC764
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC765
	.quad	_ZN2v88internal17FLAG_check_icacheE
	.quad	_ZN2v88internalL24FLAGDEFAULT_check_icacheE
	.quad	.LC766
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC767
	.quad	_ZN2v88internal16FLAG_stop_sim_atE
	.quad	_ZN2v88internalL23FLAGDEFAULT_stop_sim_atE
	.quad	.LC768
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC769
	.quad	_ZN2v88internal24FLAG_sim_stack_alignmentE
	.quad	_ZN2v88internalL31FLAGDEFAULT_sim_stack_alignmentE
	.quad	.LC770
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC771
	.quad	_ZN2v88internal19FLAG_sim_stack_sizeE
	.quad	_ZN2v88internalL26FLAGDEFAULT_sim_stack_sizeE
	.quad	.LC772
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC773
	.quad	_ZN2v88internal15FLAG_log_colourE
	.quad	_ZN2v88internalL22FLAGDEFAULT_log_colourE
	.quad	.LC774
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC775
	.quad	_ZN2v88internal35FLAG_ignore_asm_unimplemented_breakE
	.quad	_ZN2v88internalL42FLAGDEFAULT_ignore_asm_unimplemented_breakE
	.quad	.LC776
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC777
	.quad	_ZN2v88internal23FLAG_trace_sim_messagesE
	.quad	_ZN2v88internalL30FLAGDEFAULT_trace_sim_messagesE
	.quad	.LC778
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC779
	.quad	_ZN2v88internal23FLAG_async_stack_tracesE
	.quad	_ZN2v88internalL30FLAGDEFAULT_async_stack_tracesE
	.quad	.LC780
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC781
	.quad	_ZN2v88internal27FLAG_stack_trace_on_illegalE
	.quad	_ZN2v88internalL34FLAGDEFAULT_stack_trace_on_illegalE
	.quad	.LC782
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC783
	.quad	_ZN2v88internal32FLAG_abort_on_uncaught_exceptionE
	.quad	_ZN2v88internalL39FLAGDEFAULT_abort_on_uncaught_exceptionE
	.quad	.LC784
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC785
	.quad	_ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE
	.quad	_ZN2v88internalL43FLAGDEFAULT_correctness_fuzzer_suppressionsE
	.quad	.LC786
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC787
	.quad	_ZN2v88internal21FLAG_randomize_hashesE
	.quad	_ZN2v88internalL28FLAGDEFAULT_randomize_hashesE
	.quad	.LC788
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC789
	.quad	_ZN2v88internal20FLAG_rehash_snapshotE
	.quad	_ZN2v88internalL27FLAGDEFAULT_rehash_snapshotE
	.quad	.LC790
	.byte	0
	.zero	7
	.long	4
	.zero	4
	.quad	.LC791
	.quad	_ZN2v88internal14FLAG_hash_seedE
	.quad	_ZN2v88internalL21FLAGDEFAULT_hash_seedE
	.quad	.LC792
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC793
	.quad	_ZN2v88internal16FLAG_random_seedE
	.quad	_ZN2v88internalL23FLAGDEFAULT_random_seedE
	.quad	.LC794
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC795
	.quad	_ZN2v88internal23FLAG_fuzzer_random_seedE
	.quad	_ZN2v88internalL30FLAGDEFAULT_fuzzer_random_seedE
	.quad	.LC796
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC797
	.quad	_ZN2v88internal15FLAG_trace_railE
	.quad	_ZN2v88internalL22FLAGDEFAULT_trace_railE
	.quad	.LC798
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC799
	.quad	_ZN2v88internal25FLAG_print_all_exceptionsE
	.quad	_ZN2v88internalL32FLAGDEFAULT_print_all_exceptionsE
	.quad	.LC800
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC801
	.quad	_ZN2v88internal31FLAG_detailed_error_stack_traceE
	.quad	_ZN2v88internalL38FLAGDEFAULT_detailed_error_stack_traceE
	.quad	.LC802
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC803
	.quad	_ZN2v88internal23FLAG_runtime_call_statsE
	.quad	_ZN2v88internalL30FLAGDEFAULT_runtime_call_statsE
	.quad	.LC804
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC805
	.quad	_ZN2v88internal28FLAG_profile_deserializationE
	.quad	_ZN2v88internalL35FLAGDEFAULT_profile_deserializationE
	.quad	.LC806
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC807
	.quad	_ZN2v88internal29FLAG_serialization_statisticsE
	.quad	_ZN2v88internalL36FLAGDEFAULT_serialization_statisticsE
	.quad	.LC808
	.byte	0
	.zero	7
	.long	3
	.zero	4
	.quad	.LC809
	.quad	_ZN2v88internal29FLAG_serialization_chunk_sizeE
	.quad	_ZN2v88internalL36FLAGDEFAULT_serialization_chunk_sizeE
	.quad	.LC810
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC811
	.quad	_ZN2v88internal24FLAG_regexp_optimizationE
	.quad	_ZN2v88internalL31FLAGDEFAULT_regexp_optimizationE
	.quad	.LC812
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC813
	.quad	_ZN2v88internal26FLAG_regexp_mode_modifiersE
	.quad	_ZN2v88internalL33FLAGDEFAULT_regexp_mode_modifiersE
	.quad	.LC814
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC815
	.quad	_ZN2v88internal25FLAG_regexp_interpret_allE
	.quad	_ZN2v88internalL32FLAGDEFAULT_regexp_interpret_allE
	.quad	.LC816
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC817
	.quad	_ZN2v88internal19FLAG_regexp_tier_upE
	.quad	_ZN2v88internalL26FLAGDEFAULT_regexp_tier_upE
	.quad	.LC818
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC819
	.quad	_ZN2v88internal22FLAG_testing_bool_flagE
	.quad	_ZN2v88internalL29FLAGDEFAULT_testing_bool_flagE
	.quad	.LC819
	.byte	0
	.zero	7
	.long	1
	.zero	4
	.quad	.LC820
	.quad	_ZN2v88internal28FLAG_testing_maybe_bool_flagE
	.quad	_ZN2v88internalL35FLAGDEFAULT_testing_maybe_bool_flagE
	.quad	.LC820
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC821
	.quad	_ZN2v88internal21FLAG_testing_int_flagE
	.quad	_ZN2v88internalL28FLAGDEFAULT_testing_int_flagE
	.quad	.LC821
	.byte	0
	.zero	7
	.long	5
	.zero	4
	.quad	.LC822
	.quad	_ZN2v88internal23FLAG_testing_float_flagE
	.quad	_ZN2v88internalL30FLAGDEFAULT_testing_float_flagE
	.quad	.LC823
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC824
	.quad	_ZN2v88internal24FLAG_testing_string_flagE
	.quad	_ZN2v88internalL31FLAGDEFAULT_testing_string_flagE
	.quad	.LC825
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC826
	.quad	_ZN2v88internal22FLAG_testing_prng_seedE
	.quad	_ZN2v88internalL29FLAGDEFAULT_testing_prng_seedE
	.quad	.LC827
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC828
	.quad	_ZN2v88internal27FLAG_testing_d8_test_runnerE
	.quad	_ZN2v88internalL34FLAGDEFAULT_testing_d8_test_runnerE
	.quad	.LC829
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC830
	.quad	_ZN2v88internal17FLAG_embedded_srcE
	.quad	_ZN2v88internalL24FLAGDEFAULT_embedded_srcE
	.quad	.LC831
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC832
	.quad	_ZN2v88internal21FLAG_embedded_variantE
	.quad	_ZN2v88internalL28FLAGDEFAULT_embedded_variantE
	.quad	.LC833
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC834
	.quad	_ZN2v88internal16FLAG_startup_srcE
	.quad	_ZN2v88internalL23FLAGDEFAULT_startup_srcE
	.quad	.LC835
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC836
	.quad	_ZN2v88internal17FLAG_startup_blobE
	.quad	_ZN2v88internalL24FLAGDEFAULT_startup_blobE
	.quad	.LC837
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC838
	.quad	_ZN2v88internal16FLAG_target_archE
	.quad	_ZN2v88internalL23FLAGDEFAULT_target_archE
	.quad	.LC839
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC840
	.quad	_ZN2v88internal14FLAG_target_osE
	.quad	_ZN2v88internalL21FLAGDEFAULT_target_osE
	.quad	.LC841
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC842
	.quad	_ZN2v88internal30FLAG_minor_mc_parallel_markingE
	.quad	_ZN2v88internalL37FLAGDEFAULT_minor_mc_parallel_markingE
	.quad	.LC843
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC844
	.quad	_ZN2v88internal36FLAG_trace_minor_mc_parallel_markingE
	.quad	_ZN2v88internalL43FLAGDEFAULT_trace_minor_mc_parallel_markingE
	.quad	.LC845
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC846
	.quad	_ZN2v88internal13FLAG_minor_mcE
	.quad	_ZN2v88internalL20FLAGDEFAULT_minor_mcE
	.quad	.LC847
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC848
	.quad	_ZN2v88internal9FLAG_helpE
	.quad	_ZN2v88internalL16FLAGDEFAULT_helpE
	.quad	.LC849
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC850
	.quad	_ZN2v88internal18FLAG_dump_countersE
	.quad	_ZN2v88internalL25FLAGDEFAULT_dump_countersE
	.quad	.LC851
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC852
	.quad	_ZN2v88internal22FLAG_dump_counters_nvpE
	.quad	_ZN2v88internalL29FLAGDEFAULT_dump_counters_nvpE
	.quad	.LC853
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC854
	.quad	_ZN2v88internal25FLAG_use_external_stringsE
	.quad	_ZN2v88internalL32FLAGDEFAULT_use_external_stringsE
	.quad	.LC855
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC856
	.quad	_ZN2v88internal17FLAG_map_countersE
	.quad	_ZN2v88internalL24FLAGDEFAULT_map_countersE
	.quad	.LC857
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC858
	.quad	_ZN2v88internal31FLAG_mock_arraybuffer_allocatorE
	.quad	_ZN2v88internalL38FLAGDEFAULT_mock_arraybuffer_allocatorE
	.quad	.LC859
	.byte	0
	.zero	7
	.long	6
	.zero	4
	.quad	.LC860
	.quad	_ZN2v88internal37FLAG_mock_arraybuffer_allocator_limitE
	.quad	_ZN2v88internalL44FLAGDEFAULT_mock_arraybuffer_allocator_limitE
	.quad	.LC861
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC862
	.quad	_ZN2v88internal8FLAG_logE
	.quad	_ZN2v88internalL15FLAGDEFAULT_logE
	.quad	.LC863
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC864
	.quad	_ZN2v88internal12FLAG_log_allE
	.quad	_ZN2v88internalL19FLAGDEFAULT_log_allE
	.quad	.LC865
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC866
	.quad	_ZN2v88internal12FLAG_log_apiE
	.quad	_ZN2v88internalL19FLAGDEFAULT_log_apiE
	.quad	.LC867
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC868
	.quad	_ZN2v88internal13FLAG_log_codeE
	.quad	_ZN2v88internalL20FLAGDEFAULT_log_codeE
	.quad	.LC869
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC870
	.quad	_ZN2v88internal16FLAG_log_handlesE
	.quad	_ZN2v88internalL23FLAGDEFAULT_log_handlesE
	.quad	.LC871
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC872
	.quad	_ZN2v88internal16FLAG_log_suspectE
	.quad	_ZN2v88internalL23FLAGDEFAULT_log_suspectE
	.quad	.LC873
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC874
	.quad	_ZN2v88internal20FLAG_log_source_codeE
	.quad	_ZN2v88internalL27FLAGDEFAULT_log_source_codeE
	.quad	.LC875
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC876
	.quad	_ZN2v88internal24FLAG_log_function_eventsE
	.quad	_ZN2v88internalL31FLAGDEFAULT_log_function_eventsE
	.quad	.LC877
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC878
	.quad	_ZN2v88internal9FLAG_profE
	.quad	_ZN2v88internalL16FLAGDEFAULT_profE
	.quad	.LC879
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC880
	.quad	_ZN2v88internal23FLAG_detailed_line_infoE
	.quad	_ZN2v88internalL30FLAGDEFAULT_detailed_line_infoE
	.quad	.LC881
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC882
	.quad	_ZN2v88internal27FLAG_prof_sampling_intervalE
	.quad	_ZN2v88internalL34FLAGDEFAULT_prof_sampling_intervalE
	.quad	.LC883
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC884
	.quad	_ZN2v88internal13FLAG_prof_cppE
	.quad	_ZN2v88internalL20FLAGDEFAULT_prof_cppE
	.quad	.LC885
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC886
	.quad	_ZN2v88internal22FLAG_prof_browser_modeE
	.quad	_ZN2v88internalL29FLAGDEFAULT_prof_browser_modeE
	.quad	.LC887
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC888
	.quad	_ZN2v88internal12FLAG_logfileE
	.quad	_ZN2v88internalL19FLAGDEFAULT_logfileE
	.quad	.LC889
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC890
	.quad	_ZN2v88internal24FLAG_logfile_per_isolateE
	.quad	_ZN2v88internalL31FLAGDEFAULT_logfile_per_isolateE
	.quad	.LC891
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC892
	.quad	_ZN2v88internal12FLAG_ll_profE
	.quad	_ZN2v88internalL19FLAGDEFAULT_ll_profE
	.quad	.LC893
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC894
	.quad	_ZN2v88internal20FLAG_perf_basic_profE
	.quad	_ZN2v88internalL27FLAGDEFAULT_perf_basic_profE
	.quad	.LC895
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC896
	.quad	_ZN2v88internal35FLAG_perf_basic_prof_only_functionsE
	.quad	_ZN2v88internalL42FLAGDEFAULT_perf_basic_prof_only_functionsE
	.quad	.LC897
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC898
	.quad	_ZN2v88internal14FLAG_perf_profE
	.quad	_ZN2v88internalL21FLAGDEFAULT_perf_profE
	.quad	.LC899
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC900
	.quad	_ZN2v88internal28FLAG_perf_prof_annotate_wasmE
	.quad	_ZN2v88internalL35FLAGDEFAULT_perf_prof_annotate_wasmE
	.quad	.LC901
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC902
	.quad	_ZN2v88internal29FLAG_perf_prof_unwinding_infoE
	.quad	_ZN2v88internalL36FLAGDEFAULT_perf_prof_unwinding_infoE
	.quad	.LC903
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC904
	.quad	_ZN2v88internal17FLAG_gc_fake_mmapE
	.quad	_ZN2v88internalL24FLAGDEFAULT_gc_fake_mmapE
	.quad	.LC905
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC906
	.quad	_ZN2v88internal30FLAG_log_internal_timer_eventsE
	.quad	_ZN2v88internalL37FLAGDEFAULT_log_internal_timer_eventsE
	.quad	.LC907
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC908
	.quad	_ZN2v88internal26FLAG_log_instruction_statsE
	.quad	_ZN2v88internalL33FLAGDEFAULT_log_instruction_statsE
	.quad	.LC909
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC910
	.quad	_ZN2v88internal25FLAG_log_instruction_fileE
	.quad	_ZN2v88internalL32FLAGDEFAULT_log_instruction_fileE
	.quad	.LC911
	.byte	0
	.zero	7
	.long	2
	.zero	4
	.quad	.LC912
	.quad	_ZN2v88internal27FLAG_log_instruction_periodE
	.quad	_ZN2v88internalL34FLAGDEFAULT_log_instruction_periodE
	.quad	.LC913
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC914
	.quad	_ZN2v88internal25FLAG_redirect_code_tracesE
	.quad	_ZN2v88internalL32FLAGDEFAULT_redirect_code_tracesE
	.quad	.LC915
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC916
	.quad	_ZN2v88internal28FLAG_redirect_code_traces_toE
	.quad	_ZN2v88internalL35FLAGDEFAULT_redirect_code_traces_toE
	.quad	.LC917
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC918
	.quad	_ZN2v88internal21FLAG_print_opt_sourceE
	.quad	_ZN2v88internalL28FLAGDEFAULT_print_opt_sourceE
	.quad	.LC919
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC920
	.quad	_ZN2v88internal25FLAG_win64_unwinding_infoE
	.quad	_ZN2v88internalL32FLAGDEFAULT_win64_unwinding_infoE
	.quad	.LC921
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC922
	.quad	_ZN2v88internal36FLAG_interpreted_frames_native_stackE
	.quad	_ZN2v88internalL43FLAGDEFAULT_interpreted_frames_native_stackE
	.quad	.LC923
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC924
	.quad	_ZN2v88internal31FLAG_trace_elements_transitionsE
	.quad	_ZN2v88internalL38FLAGDEFAULT_trace_elements_transitionsE
	.quad	.LC925
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC926
	.quad	_ZN2v88internal36FLAG_trace_creation_allocation_sitesE
	.quad	_ZN2v88internalL43FLAGDEFAULT_trace_creation_allocation_sitesE
	.quad	.LC927
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC928
	.quad	_ZN2v88internal15FLAG_print_codeE
	.quad	_ZN2v88internalL22FLAGDEFAULT_print_codeE
	.quad	.LC929
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC930
	.quad	_ZN2v88internal19FLAG_print_opt_codeE
	.quad	_ZN2v88internalL26FLAGDEFAULT_print_opt_codeE
	.quad	.LC931
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC932
	.quad	_ZN2v88internal26FLAG_print_opt_code_filterE
	.quad	_ZN2v88internalL33FLAGDEFAULT_print_opt_code_filterE
	.quad	.LC933
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC934
	.quad	_ZN2v88internal23FLAG_print_code_verboseE
	.quad	_ZN2v88internalL30FLAGDEFAULT_print_code_verboseE
	.quad	.LC935
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC936
	.quad	_ZN2v88internal23FLAG_print_builtin_codeE
	.quad	_ZN2v88internalL30FLAGDEFAULT_print_builtin_codeE
	.quad	.LC937
	.byte	0
	.zero	7
	.long	7
	.zero	4
	.quad	.LC938
	.quad	_ZN2v88internal30FLAG_print_builtin_code_filterE
	.quad	_ZN2v88internalL37FLAGDEFAULT_print_builtin_code_filterE
	.quad	.LC939
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC940
	.quad	_ZN2v88internal22FLAG_print_regexp_codeE
	.quad	_ZN2v88internalL29FLAGDEFAULT_print_regexp_codeE
	.quad	.LC941
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC942
	.quad	_ZN2v88internal26FLAG_print_regexp_bytecodeE
	.quad	_ZN2v88internalL33FLAGDEFAULT_print_regexp_bytecodeE
	.quad	.LC943
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC944
	.quad	_ZN2v88internal23FLAG_print_builtin_sizeE
	.quad	_ZN2v88internalL30FLAGDEFAULT_print_builtin_sizeE
	.quad	.LC945
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC946
	.quad	_ZN2v88internal11FLAG_sodiumE
	.quad	_ZN2v88internalL18FLAGDEFAULT_sodiumE
	.quad	.LC947
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC948
	.quad	_ZN2v88internal19FLAG_print_all_codeE
	.quad	_ZN2v88internalL26FLAGDEFAULT_print_all_codeE
	.quad	.LC949
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC950
	.quad	_ZN2v88internal16FLAG_predictableE
	.quad	_ZN2v88internalL23FLAGDEFAULT_predictableE
	.quad	.LC951
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC952
	.quad	_ZN2v88internal28FLAG_predictable_gc_scheduleE
	.quad	_ZN2v88internalL35FLAGDEFAULT_predictable_gc_scheduleE
	.quad	.LC953
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC954
	.quad	_ZN2v88internal20FLAG_single_threadedE
	.quad	_ZN2v88internalL27FLAGDEFAULT_single_threadedE
	.quad	.LC955
	.byte	0
	.zero	7
	.long	0
	.zero	4
	.quad	.LC956
	.quad	_ZN2v88internal23FLAG_single_threaded_gcE
	.quad	_ZN2v88internalL30FLAGDEFAULT_single_threaded_gcE
	.quad	.LC957
	.byte	0
	.zero	7
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_single_threaded_gcE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_single_threaded_gcE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_single_threaded_gcE, 1
_ZN2v88internalL30FLAGDEFAULT_single_threaded_gcE:
	.zero	1
	.section	.rodata._ZN2v88internalL27FLAGDEFAULT_single_threadedE,"a"
	.type	_ZN2v88internalL27FLAGDEFAULT_single_threadedE, @object
	.size	_ZN2v88internalL27FLAGDEFAULT_single_threadedE, 1
_ZN2v88internalL27FLAGDEFAULT_single_threadedE:
	.zero	1
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_predictable_gc_scheduleE,"a"
	.type	_ZN2v88internalL35FLAGDEFAULT_predictable_gc_scheduleE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_predictable_gc_scheduleE, 1
_ZN2v88internalL35FLAGDEFAULT_predictable_gc_scheduleE:
	.zero	1
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_predictableE,"a"
	.type	_ZN2v88internalL23FLAGDEFAULT_predictableE, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_predictableE, 1
_ZN2v88internalL23FLAGDEFAULT_predictableE:
	.zero	1
	.section	.rodata._ZN2v88internalL26FLAGDEFAULT_print_all_codeE,"a"
	.type	_ZN2v88internalL26FLAGDEFAULT_print_all_codeE, @object
	.size	_ZN2v88internalL26FLAGDEFAULT_print_all_codeE, 1
_ZN2v88internalL26FLAGDEFAULT_print_all_codeE:
	.zero	1
	.section	.rodata._ZN2v88internalL18FLAGDEFAULT_sodiumE,"a"
	.type	_ZN2v88internalL18FLAGDEFAULT_sodiumE, @object
	.size	_ZN2v88internalL18FLAGDEFAULT_sodiumE, 1
_ZN2v88internalL18FLAGDEFAULT_sodiumE:
	.zero	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_print_builtin_sizeE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_print_builtin_sizeE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_print_builtin_sizeE, 1
_ZN2v88internalL30FLAGDEFAULT_print_builtin_sizeE:
	.zero	1
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_print_regexp_bytecodeE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_print_regexp_bytecodeE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_print_regexp_bytecodeE, 1
_ZN2v88internalL33FLAGDEFAULT_print_regexp_bytecodeE:
	.zero	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_print_regexp_codeE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_print_regexp_codeE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_print_regexp_codeE, 1
_ZN2v88internalL29FLAGDEFAULT_print_regexp_codeE:
	.zero	1
	.section	.rodata.str1.1
.LC958:
	.string	"*"
	.section	.data.rel.ro.local._ZN2v88internalL37FLAGDEFAULT_print_builtin_code_filterE,"aw"
	.align 8
	.type	_ZN2v88internalL37FLAGDEFAULT_print_builtin_code_filterE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_print_builtin_code_filterE, 8
_ZN2v88internalL37FLAGDEFAULT_print_builtin_code_filterE:
	.quad	.LC958
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_print_builtin_codeE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_print_builtin_codeE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_print_builtin_codeE, 1
_ZN2v88internalL30FLAGDEFAULT_print_builtin_codeE:
	.zero	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_print_code_verboseE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_print_code_verboseE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_print_code_verboseE, 1
_ZN2v88internalL30FLAGDEFAULT_print_code_verboseE:
	.zero	1
	.section	.data.rel.ro.local._ZN2v88internalL33FLAGDEFAULT_print_opt_code_filterE,"aw"
	.align 8
	.type	_ZN2v88internalL33FLAGDEFAULT_print_opt_code_filterE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_print_opt_code_filterE, 8
_ZN2v88internalL33FLAGDEFAULT_print_opt_code_filterE:
	.quad	.LC958
	.section	.rodata._ZN2v88internalL26FLAGDEFAULT_print_opt_codeE,"a"
	.type	_ZN2v88internalL26FLAGDEFAULT_print_opt_codeE, @object
	.size	_ZN2v88internalL26FLAGDEFAULT_print_opt_codeE, 1
_ZN2v88internalL26FLAGDEFAULT_print_opt_codeE:
	.zero	1
	.section	.rodata._ZN2v88internalL22FLAGDEFAULT_print_codeE,"a"
	.type	_ZN2v88internalL22FLAGDEFAULT_print_codeE, @object
	.size	_ZN2v88internalL22FLAGDEFAULT_print_codeE, 1
_ZN2v88internalL22FLAGDEFAULT_print_codeE:
	.zero	1
	.section	.rodata._ZN2v88internalL43FLAGDEFAULT_trace_creation_allocation_sitesE,"a"
	.type	_ZN2v88internalL43FLAGDEFAULT_trace_creation_allocation_sitesE, @object
	.size	_ZN2v88internalL43FLAGDEFAULT_trace_creation_allocation_sitesE, 1
_ZN2v88internalL43FLAGDEFAULT_trace_creation_allocation_sitesE:
	.zero	1
	.section	.rodata._ZN2v88internalL38FLAGDEFAULT_trace_elements_transitionsE,"a"
	.type	_ZN2v88internalL38FLAGDEFAULT_trace_elements_transitionsE, @object
	.size	_ZN2v88internalL38FLAGDEFAULT_trace_elements_transitionsE, 1
_ZN2v88internalL38FLAGDEFAULT_trace_elements_transitionsE:
	.zero	1
	.section	.rodata._ZN2v88internalL43FLAGDEFAULT_interpreted_frames_native_stackE,"a"
	.type	_ZN2v88internalL43FLAGDEFAULT_interpreted_frames_native_stackE, @object
	.size	_ZN2v88internalL43FLAGDEFAULT_interpreted_frames_native_stackE, 1
_ZN2v88internalL43FLAGDEFAULT_interpreted_frames_native_stackE:
	.zero	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_win64_unwinding_infoE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_win64_unwinding_infoE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_win64_unwinding_infoE, 1
_ZN2v88internalL32FLAGDEFAULT_win64_unwinding_infoE:
	.byte	1
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_print_opt_sourceE,"a"
	.type	_ZN2v88internalL28FLAGDEFAULT_print_opt_sourceE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_print_opt_sourceE, 1
_ZN2v88internalL28FLAGDEFAULT_print_opt_sourceE:
	.zero	1
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_redirect_code_traces_toE,"a"
	.align 8
	.type	_ZN2v88internalL35FLAGDEFAULT_redirect_code_traces_toE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_redirect_code_traces_toE, 8
_ZN2v88internalL35FLAGDEFAULT_redirect_code_traces_toE:
	.zero	8
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_redirect_code_tracesE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_redirect_code_tracesE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_redirect_code_tracesE, 1
_ZN2v88internalL32FLAGDEFAULT_redirect_code_tracesE:
	.zero	1
	.section	.rodata._ZN2v88internalL34FLAGDEFAULT_log_instruction_periodE,"a"
	.align 4
	.type	_ZN2v88internalL34FLAGDEFAULT_log_instruction_periodE, @object
	.size	_ZN2v88internalL34FLAGDEFAULT_log_instruction_periodE, 4
_ZN2v88internalL34FLAGDEFAULT_log_instruction_periodE:
	.long	4194304
	.section	.rodata.str1.1
.LC959:
	.string	"arm64_inst.csv"
	.section	.data.rel.ro.local._ZN2v88internalL32FLAGDEFAULT_log_instruction_fileE,"aw"
	.align 8
	.type	_ZN2v88internalL32FLAGDEFAULT_log_instruction_fileE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_log_instruction_fileE, 8
_ZN2v88internalL32FLAGDEFAULT_log_instruction_fileE:
	.quad	.LC959
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_log_instruction_statsE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_log_instruction_statsE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_log_instruction_statsE, 1
_ZN2v88internalL33FLAGDEFAULT_log_instruction_statsE:
	.zero	1
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_log_internal_timer_eventsE,"a"
	.type	_ZN2v88internalL37FLAGDEFAULT_log_internal_timer_eventsE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_log_internal_timer_eventsE, 1
_ZN2v88internalL37FLAGDEFAULT_log_internal_timer_eventsE:
	.zero	1
	.section	.rodata.str1.1
.LC960:
	.string	"/tmp/__v8_gc__"
	.section	.data.rel.ro.local._ZN2v88internalL24FLAGDEFAULT_gc_fake_mmapE,"aw"
	.align 8
	.type	_ZN2v88internalL24FLAGDEFAULT_gc_fake_mmapE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_gc_fake_mmapE, 8
_ZN2v88internalL24FLAGDEFAULT_gc_fake_mmapE:
	.quad	.LC960
	.section	.rodata._ZN2v88internalL36FLAGDEFAULT_perf_prof_unwinding_infoE,"a"
	.type	_ZN2v88internalL36FLAGDEFAULT_perf_prof_unwinding_infoE, @object
	.size	_ZN2v88internalL36FLAGDEFAULT_perf_prof_unwinding_infoE, 1
_ZN2v88internalL36FLAGDEFAULT_perf_prof_unwinding_infoE:
	.zero	1
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_perf_prof_annotate_wasmE,"a"
	.type	_ZN2v88internalL35FLAGDEFAULT_perf_prof_annotate_wasmE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_perf_prof_annotate_wasmE, 1
_ZN2v88internalL35FLAGDEFAULT_perf_prof_annotate_wasmE:
	.zero	1
	.section	.rodata._ZN2v88internalL21FLAGDEFAULT_perf_profE,"a"
	.type	_ZN2v88internalL21FLAGDEFAULT_perf_profE, @object
	.size	_ZN2v88internalL21FLAGDEFAULT_perf_profE, 1
_ZN2v88internalL21FLAGDEFAULT_perf_profE:
	.zero	1
	.section	.rodata._ZN2v88internalL42FLAGDEFAULT_perf_basic_prof_only_functionsE,"a"
	.type	_ZN2v88internalL42FLAGDEFAULT_perf_basic_prof_only_functionsE, @object
	.size	_ZN2v88internalL42FLAGDEFAULT_perf_basic_prof_only_functionsE, 1
_ZN2v88internalL42FLAGDEFAULT_perf_basic_prof_only_functionsE:
	.zero	1
	.section	.rodata._ZN2v88internalL27FLAGDEFAULT_perf_basic_profE,"a"
	.type	_ZN2v88internalL27FLAGDEFAULT_perf_basic_profE, @object
	.size	_ZN2v88internalL27FLAGDEFAULT_perf_basic_profE, 1
_ZN2v88internalL27FLAGDEFAULT_perf_basic_profE:
	.zero	1
	.section	.rodata._ZN2v88internalL19FLAGDEFAULT_ll_profE,"a"
	.type	_ZN2v88internalL19FLAGDEFAULT_ll_profE, @object
	.size	_ZN2v88internalL19FLAGDEFAULT_ll_profE, 1
_ZN2v88internalL19FLAGDEFAULT_ll_profE:
	.zero	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_logfile_per_isolateE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_logfile_per_isolateE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_logfile_per_isolateE, 1
_ZN2v88internalL31FLAGDEFAULT_logfile_per_isolateE:
	.byte	1
	.section	.rodata.str1.1
.LC961:
	.string	"v8.log"
	.section	.data.rel.ro.local._ZN2v88internalL19FLAGDEFAULT_logfileE,"aw"
	.align 8
	.type	_ZN2v88internalL19FLAGDEFAULT_logfileE, @object
	.size	_ZN2v88internalL19FLAGDEFAULT_logfileE, 8
_ZN2v88internalL19FLAGDEFAULT_logfileE:
	.quad	.LC961
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_prof_browser_modeE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_prof_browser_modeE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_prof_browser_modeE, 1
_ZN2v88internalL29FLAGDEFAULT_prof_browser_modeE:
	.byte	1
	.section	.rodata._ZN2v88internalL20FLAGDEFAULT_prof_cppE,"a"
	.type	_ZN2v88internalL20FLAGDEFAULT_prof_cppE, @object
	.size	_ZN2v88internalL20FLAGDEFAULT_prof_cppE, 1
_ZN2v88internalL20FLAGDEFAULT_prof_cppE:
	.zero	1
	.section	.rodata._ZN2v88internalL34FLAGDEFAULT_prof_sampling_intervalE,"a"
	.align 4
	.type	_ZN2v88internalL34FLAGDEFAULT_prof_sampling_intervalE, @object
	.size	_ZN2v88internalL34FLAGDEFAULT_prof_sampling_intervalE, 4
_ZN2v88internalL34FLAGDEFAULT_prof_sampling_intervalE:
	.long	1000
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_detailed_line_infoE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_detailed_line_infoE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_detailed_line_infoE, 1
_ZN2v88internalL30FLAGDEFAULT_detailed_line_infoE:
	.zero	1
	.section	.rodata._ZN2v88internalL16FLAGDEFAULT_profE,"a"
	.type	_ZN2v88internalL16FLAGDEFAULT_profE, @object
	.size	_ZN2v88internalL16FLAGDEFAULT_profE, 1
_ZN2v88internalL16FLAGDEFAULT_profE:
	.zero	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_log_function_eventsE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_log_function_eventsE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_log_function_eventsE, 1
_ZN2v88internalL31FLAGDEFAULT_log_function_eventsE:
	.zero	1
	.section	.rodata._ZN2v88internalL27FLAGDEFAULT_log_source_codeE,"a"
	.type	_ZN2v88internalL27FLAGDEFAULT_log_source_codeE, @object
	.size	_ZN2v88internalL27FLAGDEFAULT_log_source_codeE, 1
_ZN2v88internalL27FLAGDEFAULT_log_source_codeE:
	.zero	1
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_log_suspectE,"a"
	.type	_ZN2v88internalL23FLAGDEFAULT_log_suspectE, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_log_suspectE, 1
_ZN2v88internalL23FLAGDEFAULT_log_suspectE:
	.zero	1
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_log_handlesE,"a"
	.type	_ZN2v88internalL23FLAGDEFAULT_log_handlesE, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_log_handlesE, 1
_ZN2v88internalL23FLAGDEFAULT_log_handlesE:
	.zero	1
	.section	.rodata._ZN2v88internalL20FLAGDEFAULT_log_codeE,"a"
	.type	_ZN2v88internalL20FLAGDEFAULT_log_codeE, @object
	.size	_ZN2v88internalL20FLAGDEFAULT_log_codeE, 1
_ZN2v88internalL20FLAGDEFAULT_log_codeE:
	.zero	1
	.section	.rodata._ZN2v88internalL19FLAGDEFAULT_log_apiE,"a"
	.type	_ZN2v88internalL19FLAGDEFAULT_log_apiE, @object
	.size	_ZN2v88internalL19FLAGDEFAULT_log_apiE, 1
_ZN2v88internalL19FLAGDEFAULT_log_apiE:
	.zero	1
	.section	.rodata._ZN2v88internalL19FLAGDEFAULT_log_allE,"a"
	.type	_ZN2v88internalL19FLAGDEFAULT_log_allE, @object
	.size	_ZN2v88internalL19FLAGDEFAULT_log_allE, 1
_ZN2v88internalL19FLAGDEFAULT_log_allE:
	.zero	1
	.section	.rodata._ZN2v88internalL15FLAGDEFAULT_logE,"a"
	.type	_ZN2v88internalL15FLAGDEFAULT_logE, @object
	.size	_ZN2v88internalL15FLAGDEFAULT_logE, 1
_ZN2v88internalL15FLAGDEFAULT_logE:
	.zero	1
	.section	.rodata._ZN2v88internalL44FLAGDEFAULT_mock_arraybuffer_allocator_limitE,"a"
	.align 8
	.type	_ZN2v88internalL44FLAGDEFAULT_mock_arraybuffer_allocator_limitE, @object
	.size	_ZN2v88internalL44FLAGDEFAULT_mock_arraybuffer_allocator_limitE, 8
_ZN2v88internalL44FLAGDEFAULT_mock_arraybuffer_allocator_limitE:
	.zero	8
	.section	.rodata._ZN2v88internalL38FLAGDEFAULT_mock_arraybuffer_allocatorE,"a"
	.type	_ZN2v88internalL38FLAGDEFAULT_mock_arraybuffer_allocatorE, @object
	.size	_ZN2v88internalL38FLAGDEFAULT_mock_arraybuffer_allocatorE, 1
_ZN2v88internalL38FLAGDEFAULT_mock_arraybuffer_allocatorE:
	.zero	1
	.section	.data.rel.ro.local._ZN2v88internalL24FLAGDEFAULT_map_countersE,"aw"
	.align 8
	.type	_ZN2v88internalL24FLAGDEFAULT_map_countersE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_map_countersE, 8
_ZN2v88internalL24FLAGDEFAULT_map_countersE:
	.quad	.LC24
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_use_external_stringsE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_use_external_stringsE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_use_external_stringsE, 1
_ZN2v88internalL32FLAGDEFAULT_use_external_stringsE:
	.zero	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_dump_counters_nvpE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_dump_counters_nvpE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_dump_counters_nvpE, 1
_ZN2v88internalL29FLAGDEFAULT_dump_counters_nvpE:
	.zero	1
	.section	.rodata._ZN2v88internalL25FLAGDEFAULT_dump_countersE,"a"
	.type	_ZN2v88internalL25FLAGDEFAULT_dump_countersE, @object
	.size	_ZN2v88internalL25FLAGDEFAULT_dump_countersE, 1
_ZN2v88internalL25FLAGDEFAULT_dump_countersE:
	.zero	1
	.section	.rodata._ZN2v88internalL16FLAGDEFAULT_helpE,"a"
	.type	_ZN2v88internalL16FLAGDEFAULT_helpE, @object
	.size	_ZN2v88internalL16FLAGDEFAULT_helpE, 1
_ZN2v88internalL16FLAGDEFAULT_helpE:
	.zero	1
	.section	.rodata._ZN2v88internalL20FLAGDEFAULT_minor_mcE,"a"
	.type	_ZN2v88internalL20FLAGDEFAULT_minor_mcE, @object
	.size	_ZN2v88internalL20FLAGDEFAULT_minor_mcE, 1
_ZN2v88internalL20FLAGDEFAULT_minor_mcE:
	.zero	1
	.section	.rodata._ZN2v88internalL43FLAGDEFAULT_trace_minor_mc_parallel_markingE,"a"
	.type	_ZN2v88internalL43FLAGDEFAULT_trace_minor_mc_parallel_markingE, @object
	.size	_ZN2v88internalL43FLAGDEFAULT_trace_minor_mc_parallel_markingE, 1
_ZN2v88internalL43FLAGDEFAULT_trace_minor_mc_parallel_markingE:
	.zero	1
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_minor_mc_parallel_markingE,"a"
	.type	_ZN2v88internalL37FLAGDEFAULT_minor_mc_parallel_markingE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_minor_mc_parallel_markingE, 1
_ZN2v88internalL37FLAGDEFAULT_minor_mc_parallel_markingE:
	.byte	1
	.section	.rodata._ZN2v88internalL21FLAGDEFAULT_target_osE,"a"
	.align 8
	.type	_ZN2v88internalL21FLAGDEFAULT_target_osE, @object
	.size	_ZN2v88internalL21FLAGDEFAULT_target_osE, 8
_ZN2v88internalL21FLAGDEFAULT_target_osE:
	.zero	8
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_target_archE,"a"
	.align 8
	.type	_ZN2v88internalL23FLAGDEFAULT_target_archE, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_target_archE, 8
_ZN2v88internalL23FLAGDEFAULT_target_archE:
	.zero	8
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_startup_blobE,"a"
	.align 8
	.type	_ZN2v88internalL24FLAGDEFAULT_startup_blobE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_startup_blobE, 8
_ZN2v88internalL24FLAGDEFAULT_startup_blobE:
	.zero	8
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_startup_srcE,"a"
	.align 8
	.type	_ZN2v88internalL23FLAGDEFAULT_startup_srcE, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_startup_srcE, 8
_ZN2v88internalL23FLAGDEFAULT_startup_srcE:
	.zero	8
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_embedded_variantE,"a"
	.align 8
	.type	_ZN2v88internalL28FLAGDEFAULT_embedded_variantE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_embedded_variantE, 8
_ZN2v88internalL28FLAGDEFAULT_embedded_variantE:
	.zero	8
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_embedded_srcE,"a"
	.align 8
	.type	_ZN2v88internalL24FLAGDEFAULT_embedded_srcE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_embedded_srcE, 8
_ZN2v88internalL24FLAGDEFAULT_embedded_srcE:
	.zero	8
	.section	.rodata._ZN2v88internalL34FLAGDEFAULT_testing_d8_test_runnerE,"a"
	.type	_ZN2v88internalL34FLAGDEFAULT_testing_d8_test_runnerE, @object
	.size	_ZN2v88internalL34FLAGDEFAULT_testing_d8_test_runnerE, 1
_ZN2v88internalL34FLAGDEFAULT_testing_d8_test_runnerE:
	.zero	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_testing_prng_seedE,"a"
	.align 4
	.type	_ZN2v88internalL29FLAGDEFAULT_testing_prng_seedE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_testing_prng_seedE, 4
_ZN2v88internalL29FLAGDEFAULT_testing_prng_seedE:
	.long	42
	.section	.rodata.str1.1
.LC962:
	.string	"Hello, world!"
	.section	.data.rel.ro.local._ZN2v88internalL31FLAGDEFAULT_testing_string_flagE,"aw"
	.align 8
	.type	_ZN2v88internalL31FLAGDEFAULT_testing_string_flagE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_testing_string_flagE, 8
_ZN2v88internalL31FLAGDEFAULT_testing_string_flagE:
	.quad	.LC962
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_testing_float_flagE,"a"
	.align 8
	.type	_ZN2v88internalL30FLAGDEFAULT_testing_float_flagE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_testing_float_flagE, 8
_ZN2v88internalL30FLAGDEFAULT_testing_float_flagE:
	.long	0
	.long	1074003968
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_testing_int_flagE,"a"
	.align 4
	.type	_ZN2v88internalL28FLAGDEFAULT_testing_int_flagE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_testing_int_flagE, 4
_ZN2v88internalL28FLAGDEFAULT_testing_int_flagE:
	.long	13
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_testing_maybe_bool_flagE,"a"
	.type	_ZN2v88internalL35FLAGDEFAULT_testing_maybe_bool_flagE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_testing_maybe_bool_flagE, 2
_ZN2v88internalL35FLAGDEFAULT_testing_maybe_bool_flagE:
	.zero	2
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_testing_bool_flagE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_testing_bool_flagE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_testing_bool_flagE, 1
_ZN2v88internalL29FLAGDEFAULT_testing_bool_flagE:
	.byte	1
	.section	.rodata._ZN2v88internalL26FLAGDEFAULT_regexp_tier_upE,"a"
	.type	_ZN2v88internalL26FLAGDEFAULT_regexp_tier_upE, @object
	.size	_ZN2v88internalL26FLAGDEFAULT_regexp_tier_upE, 1
_ZN2v88internalL26FLAGDEFAULT_regexp_tier_upE:
	.zero	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_regexp_interpret_allE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_regexp_interpret_allE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_regexp_interpret_allE, 1
_ZN2v88internalL32FLAGDEFAULT_regexp_interpret_allE:
	.zero	1
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_regexp_mode_modifiersE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_regexp_mode_modifiersE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_regexp_mode_modifiersE, 1
_ZN2v88internalL33FLAGDEFAULT_regexp_mode_modifiersE:
	.zero	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_regexp_optimizationE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_regexp_optimizationE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_regexp_optimizationE, 1
_ZN2v88internalL31FLAGDEFAULT_regexp_optimizationE:
	.byte	1
	.section	.rodata._ZN2v88internalL36FLAGDEFAULT_serialization_chunk_sizeE,"a"
	.align 4
	.type	_ZN2v88internalL36FLAGDEFAULT_serialization_chunk_sizeE, @object
	.size	_ZN2v88internalL36FLAGDEFAULT_serialization_chunk_sizeE, 4
_ZN2v88internalL36FLAGDEFAULT_serialization_chunk_sizeE:
	.long	4096
	.section	.rodata._ZN2v88internalL36FLAGDEFAULT_serialization_statisticsE,"a"
	.type	_ZN2v88internalL36FLAGDEFAULT_serialization_statisticsE, @object
	.size	_ZN2v88internalL36FLAGDEFAULT_serialization_statisticsE, 1
_ZN2v88internalL36FLAGDEFAULT_serialization_statisticsE:
	.zero	1
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_profile_deserializationE,"a"
	.type	_ZN2v88internalL35FLAGDEFAULT_profile_deserializationE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_profile_deserializationE, 1
_ZN2v88internalL35FLAGDEFAULT_profile_deserializationE:
	.zero	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_runtime_call_statsE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_runtime_call_statsE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_runtime_call_statsE, 1
_ZN2v88internalL30FLAGDEFAULT_runtime_call_statsE:
	.zero	1
	.section	.rodata._ZN2v88internalL38FLAGDEFAULT_detailed_error_stack_traceE,"a"
	.type	_ZN2v88internalL38FLAGDEFAULT_detailed_error_stack_traceE, @object
	.size	_ZN2v88internalL38FLAGDEFAULT_detailed_error_stack_traceE, 1
_ZN2v88internalL38FLAGDEFAULT_detailed_error_stack_traceE:
	.zero	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_print_all_exceptionsE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_print_all_exceptionsE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_print_all_exceptionsE, 1
_ZN2v88internalL32FLAGDEFAULT_print_all_exceptionsE:
	.zero	1
	.section	.rodata._ZN2v88internalL22FLAGDEFAULT_trace_railE,"a"
	.type	_ZN2v88internalL22FLAGDEFAULT_trace_railE, @object
	.size	_ZN2v88internalL22FLAGDEFAULT_trace_railE, 1
_ZN2v88internalL22FLAGDEFAULT_trace_railE:
	.zero	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_fuzzer_random_seedE,"a"
	.align 4
	.type	_ZN2v88internalL30FLAGDEFAULT_fuzzer_random_seedE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_fuzzer_random_seedE, 4
_ZN2v88internalL30FLAGDEFAULT_fuzzer_random_seedE:
	.zero	4
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_random_seedE,"a"
	.align 4
	.type	_ZN2v88internalL23FLAGDEFAULT_random_seedE, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_random_seedE, 4
_ZN2v88internalL23FLAGDEFAULT_random_seedE:
	.zero	4
	.section	.rodata._ZN2v88internalL21FLAGDEFAULT_hash_seedE,"a"
	.align 8
	.type	_ZN2v88internalL21FLAGDEFAULT_hash_seedE, @object
	.size	_ZN2v88internalL21FLAGDEFAULT_hash_seedE, 8
_ZN2v88internalL21FLAGDEFAULT_hash_seedE:
	.zero	8
	.section	.rodata._ZN2v88internalL27FLAGDEFAULT_rehash_snapshotE,"a"
	.type	_ZN2v88internalL27FLAGDEFAULT_rehash_snapshotE, @object
	.size	_ZN2v88internalL27FLAGDEFAULT_rehash_snapshotE, 1
_ZN2v88internalL27FLAGDEFAULT_rehash_snapshotE:
	.byte	1
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_randomize_hashesE,"a"
	.type	_ZN2v88internalL28FLAGDEFAULT_randomize_hashesE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_randomize_hashesE, 1
_ZN2v88internalL28FLAGDEFAULT_randomize_hashesE:
	.byte	1
	.section	.rodata._ZN2v88internalL43FLAGDEFAULT_correctness_fuzzer_suppressionsE,"a"
	.type	_ZN2v88internalL43FLAGDEFAULT_correctness_fuzzer_suppressionsE, @object
	.size	_ZN2v88internalL43FLAGDEFAULT_correctness_fuzzer_suppressionsE, 1
_ZN2v88internalL43FLAGDEFAULT_correctness_fuzzer_suppressionsE:
	.zero	1
	.section	.rodata._ZN2v88internalL39FLAGDEFAULT_abort_on_uncaught_exceptionE,"a"
	.type	_ZN2v88internalL39FLAGDEFAULT_abort_on_uncaught_exceptionE, @object
	.size	_ZN2v88internalL39FLAGDEFAULT_abort_on_uncaught_exceptionE, 1
_ZN2v88internalL39FLAGDEFAULT_abort_on_uncaught_exceptionE:
	.zero	1
	.section	.rodata._ZN2v88internalL34FLAGDEFAULT_stack_trace_on_illegalE,"a"
	.type	_ZN2v88internalL34FLAGDEFAULT_stack_trace_on_illegalE, @object
	.size	_ZN2v88internalL34FLAGDEFAULT_stack_trace_on_illegalE, 1
_ZN2v88internalL34FLAGDEFAULT_stack_trace_on_illegalE:
	.zero	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_async_stack_tracesE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_async_stack_tracesE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_async_stack_tracesE, 1
_ZN2v88internalL30FLAGDEFAULT_async_stack_tracesE:
	.byte	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_trace_sim_messagesE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_trace_sim_messagesE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_trace_sim_messagesE, 1
_ZN2v88internalL30FLAGDEFAULT_trace_sim_messagesE:
	.zero	1
	.section	.rodata._ZN2v88internalL42FLAGDEFAULT_ignore_asm_unimplemented_breakE,"a"
	.type	_ZN2v88internalL42FLAGDEFAULT_ignore_asm_unimplemented_breakE, @object
	.size	_ZN2v88internalL42FLAGDEFAULT_ignore_asm_unimplemented_breakE, 1
_ZN2v88internalL42FLAGDEFAULT_ignore_asm_unimplemented_breakE:
	.zero	1
	.section	.rodata._ZN2v88internalL22FLAGDEFAULT_log_colourE,"a"
	.type	_ZN2v88internalL22FLAGDEFAULT_log_colourE, @object
	.size	_ZN2v88internalL22FLAGDEFAULT_log_colourE, 1
_ZN2v88internalL22FLAGDEFAULT_log_colourE:
	.byte	1
	.section	.rodata._ZN2v88internalL26FLAGDEFAULT_sim_stack_sizeE,"a"
	.align 4
	.type	_ZN2v88internalL26FLAGDEFAULT_sim_stack_sizeE, @object
	.size	_ZN2v88internalL26FLAGDEFAULT_sim_stack_sizeE, 4
_ZN2v88internalL26FLAGDEFAULT_sim_stack_sizeE:
	.long	2048
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_sim_stack_alignmentE,"a"
	.align 4
	.type	_ZN2v88internalL31FLAGDEFAULT_sim_stack_alignmentE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_sim_stack_alignmentE, 4
_ZN2v88internalL31FLAGDEFAULT_sim_stack_alignmentE:
	.long	8
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_stop_sim_atE,"a"
	.align 4
	.type	_ZN2v88internalL23FLAGDEFAULT_stop_sim_atE, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_stop_sim_atE, 4
_ZN2v88internalL23FLAGDEFAULT_stop_sim_atE:
	.zero	4
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_check_icacheE,"a"
	.type	_ZN2v88internalL24FLAGDEFAULT_check_icacheE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_check_icacheE, 1
_ZN2v88internalL24FLAGDEFAULT_check_icacheE:
	.zero	1
	.section	.rodata._ZN2v88internalL21FLAGDEFAULT_debug_simE,"a"
	.type	_ZN2v88internalL21FLAGDEFAULT_debug_simE, @object
	.size	_ZN2v88internalL21FLAGDEFAULT_debug_simE, 1
_ZN2v88internalL21FLAGDEFAULT_debug_simE:
	.zero	1
	.section	.rodata._ZN2v88internalL21FLAGDEFAULT_trace_simE,"a"
	.type	_ZN2v88internalL21FLAGDEFAULT_trace_simE, @object
	.size	_ZN2v88internalL21FLAGDEFAULT_trace_simE, 1
_ZN2v88internalL21FLAGDEFAULT_trace_simE:
	.zero	1
	.section	.rodata._ZN2v88internalL22FLAGDEFAULT_parse_onlyE,"a"
	.type	_ZN2v88internalL22FLAGDEFAULT_parse_onlyE, @object
	.size	_ZN2v88internalL22FLAGDEFAULT_parse_onlyE, 1
_ZN2v88internalL22FLAGDEFAULT_parse_onlyE:
	.zero	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_allow_natives_syntaxE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_allow_natives_syntaxE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_allow_natives_syntaxE, 1
_ZN2v88internalL32FLAGDEFAULT_allow_natives_syntaxE:
	.zero	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_trace_maps_detailsE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_trace_maps_detailsE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_trace_maps_detailsE, 1
_ZN2v88internalL30FLAGDEFAULT_trace_maps_detailsE:
	.byte	1
	.section	.rodata._ZN2v88internalL22FLAGDEFAULT_trace_mapsE,"a"
	.type	_ZN2v88internalL22FLAGDEFAULT_trace_mapsE, @object
	.size	_ZN2v88internalL22FLAGDEFAULT_trace_mapsE, 1
_ZN2v88internalL22FLAGDEFAULT_trace_mapsE:
	.zero	1
	.section	.rodata._ZN2v88internalL34FLAGDEFAULT_trace_for_in_enumerateE,"a"
	.type	_ZN2v88internalL34FLAGDEFAULT_trace_for_in_enumerateE, @object
	.size	_ZN2v88internalL34FLAGDEFAULT_trace_for_in_enumerateE, 1
_ZN2v88internalL34FLAGDEFAULT_trace_for_in_enumerateE:
	.zero	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_use_verbose_printerE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_use_verbose_printerE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_use_verbose_printerE, 1
_ZN2v88internalL31FLAGDEFAULT_use_verbose_printerE:
	.byte	1
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_trace_prototype_usersE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_trace_prototype_usersE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_trace_prototype_usersE, 1
_ZN2v88internalL33FLAGDEFAULT_trace_prototype_usersE:
	.zero	1
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_thin_stringsE,"a"
	.type	_ZN2v88internalL24FLAGDEFAULT_thin_stringsE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_thin_stringsE, 1
_ZN2v88internalL24FLAGDEFAULT_thin_stringsE:
	.byte	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_native_code_countersE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_native_code_countersE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_native_code_countersE, 1
_ZN2v88internalL32FLAGDEFAULT_native_code_countersE:
	.zero	1
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_max_polymorphic_map_countE,"a"
	.align 4
	.type	_ZN2v88internalL37FLAGDEFAULT_max_polymorphic_map_countE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_max_polymorphic_map_countE, 4
_ZN2v88internalL37FLAGDEFAULT_max_polymorphic_map_countE:
	.long	4
	.section	.rodata._ZN2v88internalL47FLAGDEFAULT_modify_field_representation_inplaceE,"a"
	.type	_ZN2v88internalL47FLAGDEFAULT_modify_field_representation_inplaceE, @object
	.size	_ZN2v88internalL47FLAGDEFAULT_modify_field_representation_inplaceE, 1
_ZN2v88internalL47FLAGDEFAULT_modify_field_representation_inplaceE:
	.byte	1
	.section	.rodata._ZN2v88internalL20FLAGDEFAULT_trace_icE,"a"
	.type	_ZN2v88internalL20FLAGDEFAULT_trace_icE, @object
	.size	_ZN2v88internalL20FLAGDEFAULT_trace_icE, 1
_ZN2v88internalL20FLAGDEFAULT_trace_icE:
	.zero	1
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_use_idle_notificationE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_use_idle_notificationE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_use_idle_notificationE, 1
_ZN2v88internalL33FLAGDEFAULT_use_idle_notificationE:
	.byte	1
	.section	.rodata._ZN2v88internalL54FLAGDEFAULT_sampling_heap_profiler_suppress_randomnessE,"a"
	.type	_ZN2v88internalL54FLAGDEFAULT_sampling_heap_profiler_suppress_randomnessE, @object
	.size	_ZN2v88internalL54FLAGDEFAULT_sampling_heap_profiler_suppress_randomnessE, 1
_ZN2v88internalL54FLAGDEFAULT_sampling_heap_profiler_suppress_randomnessE:
	.zero	1
	.section	.rodata._ZN2v88internalL38FLAGDEFAULT_heap_snapshot_string_limitE,"a"
	.align 4
	.type	_ZN2v88internalL38FLAGDEFAULT_heap_snapshot_string_limitE, @object
	.size	_ZN2v88internalL38FLAGDEFAULT_heap_snapshot_string_limitE, 4
_ZN2v88internalL38FLAGDEFAULT_heap_snapshot_string_limitE:
	.long	1024
	.section	.rodata._ZN2v88internalL44FLAGDEFAULT_heap_profiler_use_embedder_graphE,"a"
	.type	_ZN2v88internalL44FLAGDEFAULT_heap_profiler_use_embedder_graphE, @object
	.size	_ZN2v88internalL44FLAGDEFAULT_heap_profiler_use_embedder_graphE, 1
_ZN2v88internalL44FLAGDEFAULT_heap_profiler_use_embedder_graphE:
	.byte	1
	.section	.rodata._ZN2v88internalL39FLAGDEFAULT_heap_profiler_trace_objectsE,"a"
	.type	_ZN2v88internalL39FLAGDEFAULT_heap_profiler_trace_objectsE, @object
	.size	_ZN2v88internalL39FLAGDEFAULT_heap_profiler_trace_objectsE, 1
_ZN2v88internalL39FLAGDEFAULT_heap_profiler_trace_objectsE:
	.zero	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_histogram_intervalE,"a"
	.align 4
	.type	_ZN2v88internalL30FLAGDEFAULT_histogram_intervalE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_histogram_intervalE, 4
_ZN2v88internalL30FLAGDEFAULT_histogram_intervalE:
	.long	600000
	.section	.rodata._ZN2v88internalL40FLAGDEFAULT_clear_exceptions_on_js_entryE,"a"
	.type	_ZN2v88internalL40FLAGDEFAULT_clear_exceptions_on_js_entryE, @object
	.size	_ZN2v88internalL40FLAGDEFAULT_clear_exceptions_on_js_entryE, 1
_ZN2v88internalL40FLAGDEFAULT_clear_exceptions_on_js_entryE:
	.zero	1
	.section	.rodata._ZN2v88internalL41FLAGDEFAULT_max_stack_trace_source_lengthE,"a"
	.align 4
	.type	_ZN2v88internalL41FLAGDEFAULT_max_stack_trace_source_lengthE, @object
	.size	_ZN2v88internalL41FLAGDEFAULT_max_stack_trace_source_lengthE, 4
_ZN2v88internalL41FLAGDEFAULT_max_stack_trace_source_lengthE:
	.long	300
	.section	.rodata._ZN2v88internalL22FLAGDEFAULT_stack_sizeE,"a"
	.align 4
	.type	_ZN2v88internalL22FLAGDEFAULT_stack_sizeE, @object
	.size	_ZN2v88internalL22FLAGDEFAULT_stack_sizeE, 4
_ZN2v88internalL22FLAGDEFAULT_stack_sizeE:
	.long	984
	.section	.rodata._ZN2v88internalL36FLAGDEFAULT_expose_inspector_scriptsE,"a"
	.type	_ZN2v88internalL36FLAGDEFAULT_expose_inspector_scriptsE, @object
	.size	_ZN2v88internalL36FLAGDEFAULT_expose_inspector_scriptsE, 1
_ZN2v88internalL36FLAGDEFAULT_expose_inspector_scriptsE:
	.zero	1
	.section	.rodata._ZN2v88internalL22FLAGDEFAULT_hard_abortE,"a"
	.type	_ZN2v88internalL22FLAGDEFAULT_hard_abortE, @object
	.size	_ZN2v88internalL22FLAGDEFAULT_hard_abortE, 1
_ZN2v88internalL22FLAGDEFAULT_hard_abortE:
	.byte	1
	.section	.rodata._ZN2v88internalL49FLAGDEFAULT_trace_side_effect_free_debug_evaluateE,"a"
	.type	_ZN2v88internalL49FLAGDEFAULT_trace_side_effect_free_debug_evaluateE, @object
	.size	_ZN2v88internalL49FLAGDEFAULT_trace_side_effect_free_debug_evaluateE, 1
_ZN2v88internalL49FLAGDEFAULT_trace_side_effect_free_debug_evaluateE:
	.zero	1
	.section	.rodata._ZN2v88internalL42FLAGDEFAULT_cpu_profiler_sampling_intervalE,"a"
	.align 4
	.type	_ZN2v88internalL42FLAGDEFAULT_cpu_profiler_sampling_intervalE, @object
	.size	_ZN2v88internalL42FLAGDEFAULT_cpu_profiler_sampling_intervalE, 4
_ZN2v88internalL42FLAGDEFAULT_cpu_profiler_sampling_intervalE:
	.long	1000
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_trace_compiler_dispatcherE,"a"
	.type	_ZN2v88internalL37FLAGDEFAULT_trace_compiler_dispatcherE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_trace_compiler_dispatcherE, 1
_ZN2v88internalL37FLAGDEFAULT_trace_compiler_dispatcherE:
	.zero	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_compiler_dispatcherE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_compiler_dispatcherE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_compiler_dispatcherE, 1
_ZN2v88internalL31FLAGDEFAULT_compiler_dispatcherE:
	.zero	1
	.section	.rodata._ZN2v88internalL34FLAGDEFAULT_parallel_compile_tasksE,"a"
	.type	_ZN2v88internalL34FLAGDEFAULT_parallel_compile_tasksE, @object
	.size	_ZN2v88internalL34FLAGDEFAULT_parallel_compile_tasksE, 1
_ZN2v88internalL34FLAGDEFAULT_parallel_compile_tasksE:
	.zero	1
	.section	.rodata._ZN2v88internalL39FLAGDEFAULT_cache_prototype_transitionsE,"a"
	.type	_ZN2v88internalL39FLAGDEFAULT_cache_prototype_transitionsE, @object
	.size	_ZN2v88internalL39FLAGDEFAULT_cache_prototype_transitionsE, 1
_ZN2v88internalL39FLAGDEFAULT_cache_prototype_transitionsE:
	.byte	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_compilation_cacheE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_compilation_cacheE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_compilation_cacheE, 1
_ZN2v88internalL29FLAGDEFAULT_compilation_cacheE:
	.byte	1
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_trace_serializerE,"a"
	.type	_ZN2v88internalL28FLAGDEFAULT_trace_serializerE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_trace_serializerE, 1
_ZN2v88internalL28FLAGDEFAULT_trace_serializerE:
	.zero	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_prepare_always_optE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_prepare_always_optE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_prepare_always_optE, 1
_ZN2v88internalL30FLAGDEFAULT_prepare_always_optE:
	.zero	1
	.section	.rodata._ZN2v88internalL22FLAGDEFAULT_always_osrE,"a"
	.type	_ZN2v88internalL22FLAGDEFAULT_always_osrE, @object
	.size	_ZN2v88internalL22FLAGDEFAULT_always_osrE, 1
_ZN2v88internalL22FLAGDEFAULT_always_osrE:
	.zero	1
	.section	.rodata._ZN2v88internalL22FLAGDEFAULT_always_optE,"a"
	.type	_ZN2v88internalL22FLAGDEFAULT_always_optE, @object
	.size	_ZN2v88internalL22FLAGDEFAULT_always_optE, 1
_ZN2v88internalL22FLAGDEFAULT_always_optE:
	.zero	1
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_trace_file_namesE,"a"
	.type	_ZN2v88internalL28FLAGDEFAULT_trace_file_namesE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_trace_file_namesE, 1
_ZN2v88internalL28FLAGDEFAULT_trace_file_namesE:
	.zero	1
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_trace_deoptE,"a"
	.type	_ZN2v88internalL23FLAGDEFAULT_trace_deoptE, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_trace_deoptE, 1
_ZN2v88internalL23FLAGDEFAULT_trace_deoptE:
	.zero	1
	.section	.rodata._ZN2v88internalL27FLAGDEFAULT_trace_opt_statsE,"a"
	.type	_ZN2v88internalL27FLAGDEFAULT_trace_opt_statsE, @object
	.size	_ZN2v88internalL27FLAGDEFAULT_trace_opt_statsE, 1
_ZN2v88internalL27FLAGDEFAULT_trace_opt_statsE:
	.zero	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_trace_opt_verboseE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_trace_opt_verboseE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_trace_opt_verboseE, 1
_ZN2v88internalL29FLAGDEFAULT_trace_opt_verboseE:
	.zero	1
	.section	.rodata._ZN2v88internalL21FLAGDEFAULT_trace_optE,"a"
	.type	_ZN2v88internalL21FLAGDEFAULT_trace_optE, @object
	.size	_ZN2v88internalL21FLAGDEFAULT_trace_optE, 1
_ZN2v88internalL21FLAGDEFAULT_trace_optE:
	.zero	1
	.section	.rodata._ZN2v88internalL20FLAGDEFAULT_max_lazyE,"a"
	.type	_ZN2v88internalL20FLAGDEFAULT_max_lazyE, @object
	.size	_ZN2v88internalL20FLAGDEFAULT_max_lazyE, 1
_ZN2v88internalL20FLAGDEFAULT_max_lazyE:
	.zero	1
	.section	.rodata._ZN2v88internalL16FLAGDEFAULT_lazyE,"a"
	.type	_ZN2v88internalL16FLAGDEFAULT_lazyE, @object
	.size	_ZN2v88internalL16FLAGDEFAULT_lazyE, 1
_ZN2v88internalL16FLAGDEFAULT_lazyE:
	.byte	1
	.section	.rodata._ZN2v88internalL17FLAGDEFAULT_traceE,"a"
	.type	_ZN2v88internalL17FLAGDEFAULT_traceE, @object
	.size	_ZN2v88internalL17FLAGDEFAULT_traceE, 1
_ZN2v88internalL17FLAGDEFAULT_traceE:
	.zero	1
	.section	.rodata._ZN2v88internalL22FLAGDEFAULT_inline_newE,"a"
	.type	_ZN2v88internalL22FLAGDEFAULT_inline_newE, @object
	.size	_ZN2v88internalL22FLAGDEFAULT_inline_newE, 1
_ZN2v88internalL22FLAGDEFAULT_inline_newE:
	.byte	1
	.section	.rodata._ZN2v88internalL53FLAGDEFAULT_test_small_max_function_context_stub_sizeE,"a"
	.type	_ZN2v88internalL53FLAGDEFAULT_test_small_max_function_context_stub_sizeE, @object
	.size	_ZN2v88internalL53FLAGDEFAULT_test_small_max_function_context_stub_sizeE, 1
_ZN2v88internalL53FLAGDEFAULT_test_small_max_function_context_stub_sizeE:
	.zero	1
	.section	.rodata._ZN2v88internalL27FLAGDEFAULT_force_slow_pathE,"a"
	.type	_ZN2v88internalL27FLAGDEFAULT_force_slow_pathE, @object
	.size	_ZN2v88internalL27FLAGDEFAULT_force_slow_pathE, 1
_ZN2v88internalL27FLAGDEFAULT_force_slow_pathE:
	.zero	1
	.section	.rodata._ZN2v88internalL45FLAGDEFAULT_allow_unsafe_function_constructorE,"a"
	.type	_ZN2v88internalL45FLAGDEFAULT_allow_unsafe_function_constructorE, @object
	.size	_ZN2v88internalL45FLAGDEFAULT_allow_unsafe_function_constructorE, 1
_ZN2v88internalL45FLAGDEFAULT_allow_unsafe_function_constructorE:
	.zero	1
	.section	.rodata._ZN2v88internalL34FLAGDEFAULT_expose_cputracemark_asE,"a"
	.align 8
	.type	_ZN2v88internalL34FLAGDEFAULT_expose_cputracemark_asE, @object
	.size	_ZN2v88internalL34FLAGDEFAULT_expose_cputracemark_asE, 8
_ZN2v88internalL34FLAGDEFAULT_expose_cputracemark_asE:
	.zero	8
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_expose_async_hooksE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_expose_async_hooksE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_expose_async_hooksE, 1
_ZN2v88internalL30FLAGDEFAULT_expose_async_hooksE:
	.zero	1
	.section	.rodata._ZN2v88internalL49FLAGDEFAULT_disallow_code_generation_from_stringsE,"a"
	.type	_ZN2v88internalL49FLAGDEFAULT_disallow_code_generation_from_stringsE, @object
	.size	_ZN2v88internalL49FLAGDEFAULT_disallow_code_generation_from_stringsE, 1
_ZN2v88internalL49FLAGDEFAULT_disallow_code_generation_from_stringsE:
	.zero	1
	.section	.rodata._ZN2v88internalL43FLAGDEFAULT_experimental_stack_trace_framesE,"a"
	.type	_ZN2v88internalL43FLAGDEFAULT_experimental_stack_trace_framesE, @object
	.size	_ZN2v88internalL43FLAGDEFAULT_experimental_stack_trace_framesE, 1
_ZN2v88internalL43FLAGDEFAULT_experimental_stack_trace_framesE:
	.zero	1
	.section	.rodata._ZN2v88internalL36FLAGDEFAULT_builtins_in_stack_tracesE,"a"
	.type	_ZN2v88internalL36FLAGDEFAULT_builtins_in_stack_tracesE, @object
	.size	_ZN2v88internalL36FLAGDEFAULT_builtins_in_stack_tracesE, 1
_ZN2v88internalL36FLAGDEFAULT_builtins_in_stack_tracesE:
	.zero	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_stack_trace_limitE,"a"
	.align 4
	.type	_ZN2v88internalL29FLAGDEFAULT_stack_trace_limitE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_stack_trace_limitE, 4
_ZN2v88internalL29FLAGDEFAULT_stack_trace_limitE:
	.long	10
	.section	.rodata._ZN2v88internalL34FLAGDEFAULT_expose_trigger_failureE,"a"
	.type	_ZN2v88internalL34FLAGDEFAULT_expose_trigger_failureE, @object
	.size	_ZN2v88internalL34FLAGDEFAULT_expose_trigger_failureE, 1
_ZN2v88internalL34FLAGDEFAULT_expose_trigger_failureE:
	.zero	1
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_expose_externalize_stringE,"a"
	.type	_ZN2v88internalL37FLAGDEFAULT_expose_externalize_stringE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_expose_externalize_stringE, 1
_ZN2v88internalL37FLAGDEFAULT_expose_externalize_stringE:
	.zero	1
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_expose_gc_asE,"a"
	.align 8
	.type	_ZN2v88internalL24FLAGDEFAULT_expose_gc_asE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_expose_gc_asE, 8
_ZN2v88internalL24FLAGDEFAULT_expose_gc_asE:
	.zero	8
	.section	.rodata._ZN2v88internalL21FLAGDEFAULT_expose_gcE,"a"
	.type	_ZN2v88internalL21FLAGDEFAULT_expose_gcE, @object
	.size	_ZN2v88internalL21FLAGDEFAULT_expose_gcE, 1
_ZN2v88internalL21FLAGDEFAULT_expose_gcE:
	.zero	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_expose_free_bufferE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_expose_free_bufferE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_expose_free_bufferE, 1
_ZN2v88internalL30FLAGDEFAULT_expose_free_bufferE:
	.zero	1
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_disable_old_api_accessorsE,"a"
	.type	_ZN2v88internalL37FLAGDEFAULT_disable_old_api_accessorsE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_disable_old_api_accessorsE, 1
_ZN2v88internalL37FLAGDEFAULT_disable_old_api_accessorsE:
	.zero	1
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_script_streamingE,"a"
	.type	_ZN2v88internalL28FLAGDEFAULT_script_streamingE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_script_streamingE, 1
_ZN2v88internalL28FLAGDEFAULT_script_streamingE:
	.byte	1
	.section	.rodata._ZN2v88internalL44FLAGDEFAULT_enable_regexp_unaligned_accessesE,"a"
	.type	_ZN2v88internalL44FLAGDEFAULT_enable_regexp_unaligned_accessesE, @object
	.size	_ZN2v88internalL44FLAGDEFAULT_enable_regexp_unaligned_accessesE, 1
_ZN2v88internalL44FLAGDEFAULT_enable_regexp_unaligned_accessesE:
	.byte	1
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_enable_armv8E,"a"
	.type	_ZN2v88internalL24FLAGDEFAULT_enable_armv8E, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_enable_armv8E, 2
_ZN2v88internalL24FLAGDEFAULT_enable_armv8E:
	.zero	2
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_enable_sudivE,"a"
	.type	_ZN2v88internalL24FLAGDEFAULT_enable_sudivE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_enable_sudivE, 2
_ZN2v88internalL24FLAGDEFAULT_enable_sudivE:
	.zero	2
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_enable_neonE,"a"
	.type	_ZN2v88internalL23FLAGDEFAULT_enable_neonE, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_enable_neonE, 2
_ZN2v88internalL23FLAGDEFAULT_enable_neonE:
	.zero	2
	.section	.rodata._ZN2v88internalL26FLAGDEFAULT_enable_32dregsE,"a"
	.type	_ZN2v88internalL26FLAGDEFAULT_enable_32dregsE, @object
	.size	_ZN2v88internalL26FLAGDEFAULT_enable_32dregsE, 2
_ZN2v88internalL26FLAGDEFAULT_enable_32dregsE:
	.zero	2
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_enable_vfp3E,"a"
	.type	_ZN2v88internalL23FLAGDEFAULT_enable_vfp3E, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_enable_vfp3E, 2
_ZN2v88internalL23FLAGDEFAULT_enable_vfp3E:
	.zero	2
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_enable_armv7E,"a"
	.type	_ZN2v88internalL24FLAGDEFAULT_enable_armv7E, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_enable_armv7E, 2
_ZN2v88internalL24FLAGDEFAULT_enable_armv7E:
	.zero	2
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_enable_source_at_csa_bindE,"a"
	.type	_ZN2v88internalL37FLAGDEFAULT_enable_source_at_csa_bindE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_enable_source_at_csa_bindE, 1
_ZN2v88internalL37FLAGDEFAULT_enable_source_at_csa_bindE:
	.zero	1
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_partial_constant_poolE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_partial_constant_poolE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_partial_constant_poolE, 1
_ZN2v88internalL33FLAGDEFAULT_partial_constant_poolE:
	.byte	1
	.section	.rodata.str1.1
.LC963:
	.string	"auto"
	.section	.data.rel.ro.local._ZN2v88internalL16FLAGDEFAULT_mcpuE,"aw"
	.align 8
	.type	_ZN2v88internalL16FLAGDEFAULT_mcpuE, @object
	.size	_ZN2v88internalL16FLAGDEFAULT_mcpuE, 8
_ZN2v88internalL16FLAGDEFAULT_mcpuE:
	.quad	.LC963
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_force_long_branchesE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_force_long_branchesE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_force_long_branchesE, 1
_ZN2v88internalL31FLAGDEFAULT_force_long_branchesE:
	.zero	1
	.section	.rodata.str1.1
.LC964:
	.string	"armv8"
	.section	.data.rel.ro.local._ZN2v88internalL20FLAGDEFAULT_arm_archE,"aw"
	.align 8
	.type	_ZN2v88internalL20FLAGDEFAULT_arm_archE, @object
	.size	_ZN2v88internalL20FLAGDEFAULT_arm_archE, 8
_ZN2v88internalL20FLAGDEFAULT_arm_archE:
	.quad	.LC964
	.section	.rodata._ZN2v88internalL25FLAGDEFAULT_enable_popcntE,"a"
	.type	_ZN2v88internalL25FLAGDEFAULT_enable_popcntE, @object
	.size	_ZN2v88internalL25FLAGDEFAULT_enable_popcntE, 1
_ZN2v88internalL25FLAGDEFAULT_enable_popcntE:
	.byte	1
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_enable_lzcntE,"a"
	.type	_ZN2v88internalL24FLAGDEFAULT_enable_lzcntE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_enable_lzcntE, 1
_ZN2v88internalL24FLAGDEFAULT_enable_lzcntE:
	.byte	1
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_enable_bmi2E,"a"
	.type	_ZN2v88internalL23FLAGDEFAULT_enable_bmi2E, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_enable_bmi2E, 1
_ZN2v88internalL23FLAGDEFAULT_enable_bmi2E:
	.byte	1
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_enable_bmi1E,"a"
	.type	_ZN2v88internalL23FLAGDEFAULT_enable_bmi1E, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_enable_bmi1E, 1
_ZN2v88internalL23FLAGDEFAULT_enable_bmi1E:
	.byte	1
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_enable_fma3E,"a"
	.type	_ZN2v88internalL23FLAGDEFAULT_enable_fma3E, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_enable_fma3E, 1
_ZN2v88internalL23FLAGDEFAULT_enable_fma3E:
	.byte	1
	.section	.rodata._ZN2v88internalL22FLAGDEFAULT_enable_avxE,"a"
	.type	_ZN2v88internalL22FLAGDEFAULT_enable_avxE, @object
	.size	_ZN2v88internalL22FLAGDEFAULT_enable_avxE, 1
_ZN2v88internalL22FLAGDEFAULT_enable_avxE:
	.byte	1
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_enable_sahfE,"a"
	.type	_ZN2v88internalL23FLAGDEFAULT_enable_sahfE, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_enable_sahfE, 1
_ZN2v88internalL23FLAGDEFAULT_enable_sahfE:
	.byte	1
	.section	.rodata._ZN2v88internalL25FLAGDEFAULT_enable_sse4_2E,"a"
	.type	_ZN2v88internalL25FLAGDEFAULT_enable_sse4_2E, @object
	.size	_ZN2v88internalL25FLAGDEFAULT_enable_sse4_2E, 1
_ZN2v88internalL25FLAGDEFAULT_enable_sse4_2E:
	.byte	1
	.section	.rodata._ZN2v88internalL25FLAGDEFAULT_enable_sse4_1E,"a"
	.type	_ZN2v88internalL25FLAGDEFAULT_enable_sse4_1E, @object
	.size	_ZN2v88internalL25FLAGDEFAULT_enable_sse4_1E, 1
_ZN2v88internalL25FLAGDEFAULT_enable_sse4_1E:
	.byte	1
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_enable_ssse3E,"a"
	.type	_ZN2v88internalL24FLAGDEFAULT_enable_ssse3E, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_enable_ssse3E, 1
_ZN2v88internalL24FLAGDEFAULT_enable_ssse3E:
	.byte	1
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_enable_sse3E,"a"
	.type	_ZN2v88internalL23FLAGDEFAULT_enable_sse3E, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_enable_sse3E, 1
_ZN2v88internalL23FLAGDEFAULT_enable_sse3E:
	.byte	1
	.section	.rodata._ZN2v88internalL25FLAGDEFAULT_code_commentsE,"a"
	.type	_ZN2v88internalL25FLAGDEFAULT_code_commentsE, @object
	.size	_ZN2v88internalL25FLAGDEFAULT_code_commentsE, 1
_ZN2v88internalL25FLAGDEFAULT_code_commentsE:
	.zero	1
	.section	.rodata._ZN2v88internalL22FLAGDEFAULT_debug_codeE,"a"
	.type	_ZN2v88internalL22FLAGDEFAULT_debug_codeE, @object
	.size	_ZN2v88internalL22FLAGDEFAULT_debug_codeE, 1
_ZN2v88internalL22FLAGDEFAULT_debug_codeE:
	.zero	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_idle_time_scavengeE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_idle_time_scavengeE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_idle_time_scavengeE, 1
_ZN2v88internalL30FLAGDEFAULT_idle_time_scavengeE:
	.byte	1
	.section	.rodata._ZN2v88internalL42FLAGDEFAULT_young_generation_large_objectsE,"a"
	.type	_ZN2v88internalL42FLAGDEFAULT_young_generation_large_objectsE, @object
	.size	_ZN2v88internalL42FLAGDEFAULT_young_generation_large_objectsE, 1
_ZN2v88internalL42FLAGDEFAULT_young_generation_large_objectsE:
	.byte	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_clear_free_memoryE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_clear_free_memoryE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_clear_free_memoryE, 1
_ZN2v88internalL29FLAGDEFAULT_clear_free_memoryE:
	.zero	1
	.section	.rodata._ZN2v88internalL36FLAGDEFAULT_fast_promotion_new_spaceE,"a"
	.type	_ZN2v88internalL36FLAGDEFAULT_fast_promotion_new_spaceE, @object
	.size	_ZN2v88internalL36FLAGDEFAULT_fast_promotion_new_spaceE, 1
_ZN2v88internalL36FLAGDEFAULT_fast_promotion_new_spaceE:
	.zero	1
	.section	.rodata._ZN2v88internalL50FLAGDEFAULT_manual_evacuation_candidates_selectionE,"a"
	.type	_ZN2v88internalL50FLAGDEFAULT_manual_evacuation_candidates_selectionE, @object
	.size	_ZN2v88internalL50FLAGDEFAULT_manual_evacuation_candidates_selectionE, 1
_ZN2v88internalL50FLAGDEFAULT_manual_evacuation_candidates_selectionE:
	.zero	1
	.section	.rodata._ZN2v88internalL27FLAGDEFAULT_disable_abortjsE,"a"
	.type	_ZN2v88internalL27FLAGDEFAULT_disable_abortjsE, @object
	.size	_ZN2v88internalL27FLAGDEFAULT_disable_abortjsE, 1
_ZN2v88internalL27FLAGDEFAULT_disable_abortjsE:
	.zero	1
	.section	.rodata._ZN2v88internalL41FLAGDEFAULT_gc_experiment_less_compactionE,"a"
	.type	_ZN2v88internalL41FLAGDEFAULT_gc_experiment_less_compactionE, @object
	.size	_ZN2v88internalL41FLAGDEFAULT_gc_experiment_less_compactionE, 1
_ZN2v88internalL41FLAGDEFAULT_gc_experiment_less_compactionE:
	.zero	1
	.section	.rodata._ZN2v88internalL45FLAGDEFAULT_gc_experiment_background_scheduleE,"a"
	.type	_ZN2v88internalL45FLAGDEFAULT_gc_experiment_background_scheduleE, @object
	.size	_ZN2v88internalL45FLAGDEFAULT_gc_experiment_background_scheduleE, 1
_ZN2v88internalL45FLAGDEFAULT_gc_experiment_background_scheduleE:
	.zero	1
	.section	.rodata._ZN2v88internalL27FLAGDEFAULT_stress_scavengeE,"a"
	.align 4
	.type	_ZN2v88internalL27FLAGDEFAULT_stress_scavengeE, @object
	.size	_ZN2v88internalL27FLAGDEFAULT_stress_scavengeE, 4
_ZN2v88internalL27FLAGDEFAULT_stress_scavengeE:
	.zero	4
	.section	.rodata._ZN2v88internalL26FLAGDEFAULT_stress_markingE,"a"
	.align 4
	.type	_ZN2v88internalL26FLAGDEFAULT_stress_markingE, @object
	.size	_ZN2v88internalL26FLAGDEFAULT_stress_markingE, 4
_ZN2v88internalL26FLAGDEFAULT_stress_markingE:
	.zero	4
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_fuzzer_gc_analysisE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_fuzzer_gc_analysisE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_fuzzer_gc_analysisE, 1
_ZN2v88internalL30FLAGDEFAULT_fuzzer_gc_analysisE:
	.zero	1
	.section	.rodata._ZN2v88internalL38FLAGDEFAULT_stress_incremental_markingE,"a"
	.type	_ZN2v88internalL38FLAGDEFAULT_stress_incremental_markingE, @object
	.size	_ZN2v88internalL38FLAGDEFAULT_stress_incremental_markingE, 1
_ZN2v88internalL38FLAGDEFAULT_stress_incremental_markingE:
	.zero	1
	.section	.rodata._ZN2v88internalL36FLAGDEFAULT_stress_compaction_randomE,"a"
	.type	_ZN2v88internalL36FLAGDEFAULT_stress_compaction_randomE, @object
	.size	_ZN2v88internalL36FLAGDEFAULT_stress_compaction_randomE, 1
_ZN2v88internalL36FLAGDEFAULT_stress_compaction_randomE:
	.zero	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_stress_compactionE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_stress_compactionE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_stress_compactionE, 1
_ZN2v88internalL29FLAGDEFAULT_stress_compactionE:
	.zero	1
	.section	.rodata._ZN2v88internalL41FLAGDEFAULT_force_marking_deque_overflowsE,"a"
	.type	_ZN2v88internalL41FLAGDEFAULT_force_marking_deque_overflowsE, @object
	.size	_ZN2v88internalL41FLAGDEFAULT_force_marking_deque_overflowsE, 1
_ZN2v88internalL41FLAGDEFAULT_force_marking_deque_overflowsE:
	.zero	1
	.section	.rodata._ZN2v88internalL36FLAGDEFAULT_use_marking_progress_barE,"a"
	.type	_ZN2v88internalL36FLAGDEFAULT_use_marking_progress_barE, @object
	.size	_ZN2v88internalL36FLAGDEFAULT_use_marking_progress_barE, 1
_ZN2v88internalL36FLAGDEFAULT_use_marking_progress_barE:
	.byte	1
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_stress_flush_bytecodeE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_stress_flush_bytecodeE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_stress_flush_bytecodeE, 1
_ZN2v88internalL33FLAGDEFAULT_stress_flush_bytecodeE:
	.zero	1
	.section	.rodata._ZN2v88internalL26FLAGDEFAULT_flush_bytecodeE,"a"
	.type	_ZN2v88internalL26FLAGDEFAULT_flush_bytecodeE, @object
	.size	_ZN2v88internalL26FLAGDEFAULT_flush_bytecodeE, 1
_ZN2v88internalL26FLAGDEFAULT_flush_bytecodeE:
	.byte	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_compact_code_spaceE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_compact_code_spaceE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_compact_code_spaceE, 1
_ZN2v88internalL30FLAGDEFAULT_compact_code_spaceE:
	.byte	1
	.section	.rodata._ZN2v88internalL25FLAGDEFAULT_never_compactE,"a"
	.type	_ZN2v88internalL25FLAGDEFAULT_never_compactE, @object
	.size	_ZN2v88internalL25FLAGDEFAULT_never_compactE, 1
_ZN2v88internalL25FLAGDEFAULT_never_compactE:
	.zero	1
	.section	.rodata._ZN2v88internalL26FLAGDEFAULT_always_compactE,"a"
	.type	_ZN2v88internalL26FLAGDEFAULT_always_compactE, @object
	.size	_ZN2v88internalL26FLAGDEFAULT_always_compactE, 1
_ZN2v88internalL26FLAGDEFAULT_always_compactE:
	.zero	1
	.section	.rodata._ZN2v88internalL27FLAGDEFAULT_v8_os_page_sizeE,"a"
	.align 4
	.type	_ZN2v88internalL27FLAGDEFAULT_v8_os_page_sizeE, @object
	.size	_ZN2v88internalL27FLAGDEFAULT_v8_os_page_sizeE, 4
_ZN2v88internalL27FLAGDEFAULT_v8_os_page_sizeE:
	.zero	4
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_heap_growing_percentE,"a"
	.align 4
	.type	_ZN2v88internalL32FLAGDEFAULT_heap_growing_percentE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_heap_growing_percentE, 4
_ZN2v88internalL32FLAGDEFAULT_heap_growing_percentE:
	.zero	4
	.section	.rodata._ZN2v88internalL42FLAGDEFAULT_memory_reducer_for_small_heapsE,"a"
	.type	_ZN2v88internalL42FLAGDEFAULT_memory_reducer_for_small_heapsE, @object
	.size	_ZN2v88internalL42FLAGDEFAULT_memory_reducer_for_small_heapsE, 1
_ZN2v88internalL42FLAGDEFAULT_memory_reducer_for_small_heapsE:
	.byte	1
	.section	.rodata._ZN2v88internalL26FLAGDEFAULT_memory_reducerE,"a"
	.type	_ZN2v88internalL26FLAGDEFAULT_memory_reducerE, @object
	.size	_ZN2v88internalL26FLAGDEFAULT_memory_reducerE, 1
_ZN2v88internalL26FLAGDEFAULT_memory_reducerE:
	.byte	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_move_object_startE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_move_object_startE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_move_object_startE, 1
_ZN2v88internalL29FLAGDEFAULT_move_object_startE:
	.byte	1
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_trace_detached_contextsE,"a"
	.type	_ZN2v88internalL35FLAGDEFAULT_trace_detached_contextsE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_trace_detached_contextsE, 1
_ZN2v88internalL35FLAGDEFAULT_trace_detached_contextsE:
	.zero	1
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_track_detached_contextsE,"a"
	.type	_ZN2v88internalL35FLAGDEFAULT_track_detached_contextsE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_track_detached_contextsE, 1
_ZN2v88internalL35FLAGDEFAULT_track_detached_contextsE:
	.byte	1
	.section	.rodata._ZN2v88internalL20FLAGDEFAULT_gc_statsE,"a"
	.align 4
	.type	_ZN2v88internalL20FLAGDEFAULT_gc_statsE, @object
	.size	_ZN2v88internalL20FLAGDEFAULT_gc_statsE, 4
_ZN2v88internalL20FLAGDEFAULT_gc_statsE:
	.zero	4
	.section	.rodata._ZN2v88internalL43FLAGDEFAULT_concurrent_array_buffer_freeingE,"a"
	.type	_ZN2v88internalL43FLAGDEFAULT_concurrent_array_buffer_freeingE, @object
	.size	_ZN2v88internalL43FLAGDEFAULT_concurrent_array_buffer_freeingE, 1
_ZN2v88internalL43FLAGDEFAULT_concurrent_array_buffer_freeingE:
	.byte	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_track_retaining_pathE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_track_retaining_pathE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_track_retaining_pathE, 1
_ZN2v88internalL32FLAGDEFAULT_track_retaining_pathE:
	.zero	1
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_trace_zone_statsE,"a"
	.type	_ZN2v88internalL28FLAGDEFAULT_trace_zone_statsE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_trace_zone_statsE, 1
_ZN2v88internalL28FLAGDEFAULT_trace_zone_statsE:
	.zero	1
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_trace_gc_object_statsE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_trace_gc_object_statsE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_trace_gc_object_statsE, 1
_ZN2v88internalL33FLAGDEFAULT_trace_gc_object_statsE:
	.zero	1
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_track_gc_object_statsE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_track_gc_object_statsE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_track_gc_object_statsE, 1
_ZN2v88internalL33FLAGDEFAULT_track_gc_object_statsE:
	.zero	1
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_trace_stress_scavengeE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_trace_stress_scavengeE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_trace_stress_scavengeE, 1
_ZN2v88internalL33FLAGDEFAULT_trace_stress_scavengeE:
	.zero	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_trace_stress_markingE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_trace_stress_markingE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_trace_stress_markingE, 1
_ZN2v88internalL32FLAGDEFAULT_trace_stress_markingE:
	.zero	1
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_trace_incremental_markingE,"a"
	.type	_ZN2v88internalL37FLAGDEFAULT_trace_incremental_markingE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_trace_incremental_markingE, 1
_ZN2v88internalL37FLAGDEFAULT_trace_incremental_markingE:
	.zero	1
	.section	.rodata._ZN2v88internalL50FLAGDEFAULT_detect_ineffective_gcs_near_heap_limitE,"a"
	.type	_ZN2v88internalL50FLAGDEFAULT_detect_ineffective_gcs_near_heap_limitE, @object
	.size	_ZN2v88internalL50FLAGDEFAULT_detect_ineffective_gcs_near_heap_limitE, 1
_ZN2v88internalL50FLAGDEFAULT_detect_ineffective_gcs_near_heap_limitE:
	.byte	1
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_parallel_pointer_updateE,"a"
	.type	_ZN2v88internalL35FLAGDEFAULT_parallel_pointer_updateE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_parallel_pointer_updateE, 1
_ZN2v88internalL35FLAGDEFAULT_parallel_pointer_updateE:
	.byte	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_parallel_compactionE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_parallel_compactionE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_parallel_compactionE, 1
_ZN2v88internalL31FLAGDEFAULT_parallel_compactionE:
	.byte	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_concurrent_sweepingE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_concurrent_sweepingE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_concurrent_sweepingE, 1
_ZN2v88internalL31FLAGDEFAULT_concurrent_sweepingE:
	.byte	1
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_concurrent_store_bufferE,"a"
	.type	_ZN2v88internalL35FLAGDEFAULT_concurrent_store_bufferE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_concurrent_store_bufferE, 1
_ZN2v88internalL35FLAGDEFAULT_concurrent_store_bufferE:
	.byte	1
	.section	.rodata._ZN2v88internalL36FLAGDEFAULT_trace_concurrent_markingE,"a"
	.type	_ZN2v88internalL36FLAGDEFAULT_trace_concurrent_markingE, @object
	.size	_ZN2v88internalL36FLAGDEFAULT_trace_concurrent_markingE, 1
_ZN2v88internalL36FLAGDEFAULT_trace_concurrent_markingE:
	.zero	1
	.section	.rodata._ZN2v88internalL41FLAGDEFAULT_ephemeron_fixpoint_iterationsE,"a"
	.align 4
	.type	_ZN2v88internalL41FLAGDEFAULT_ephemeron_fixpoint_iterationsE, @object
	.size	_ZN2v88internalL41FLAGDEFAULT_ephemeron_fixpoint_iterationsE, 4
_ZN2v88internalL41FLAGDEFAULT_ephemeron_fixpoint_iterationsE:
	.long	10
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_parallel_markingE,"a"
	.type	_ZN2v88internalL28FLAGDEFAULT_parallel_markingE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_parallel_markingE, 1
_ZN2v88internalL28FLAGDEFAULT_parallel_markingE:
	.byte	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_concurrent_markingE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_concurrent_markingE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_concurrent_markingE, 1
_ZN2v88internalL30FLAGDEFAULT_concurrent_markingE:
	.byte	1
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_write_protect_code_memoryE,"a"
	.type	_ZN2v88internalL37FLAGDEFAULT_write_protect_code_memoryE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_write_protect_code_memoryE, 1
_ZN2v88internalL37FLAGDEFAULT_write_protect_code_memoryE:
	.byte	1
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_trace_parallel_scavengeE,"a"
	.type	_ZN2v88internalL35FLAGDEFAULT_trace_parallel_scavengeE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_trace_parallel_scavengeE, 1
_ZN2v88internalL35FLAGDEFAULT_trace_parallel_scavengeE:
	.zero	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_parallel_scavengeE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_parallel_scavengeE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_parallel_scavengeE, 1
_ZN2v88internalL29FLAGDEFAULT_parallel_scavengeE:
	.byte	1
	.section	.rodata._ZN2v88internalL26FLAGDEFAULT_trace_unmapperE,"a"
	.type	_ZN2v88internalL26FLAGDEFAULT_trace_unmapperE, @object
	.size	_ZN2v88internalL26FLAGDEFAULT_trace_unmapperE, 1
_ZN2v88internalL26FLAGDEFAULT_trace_unmapperE:
	.zero	1
	.section	.rodata._ZN2v88internalL40FLAGDEFAULT_incremental_marking_wrappersE,"a"
	.type	_ZN2v88internalL40FLAGDEFAULT_incremental_marking_wrappersE, @object
	.size	_ZN2v88internalL40FLAGDEFAULT_incremental_marking_wrappersE, 1
_ZN2v88internalL40FLAGDEFAULT_incremental_marking_wrappersE:
	.byte	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_incremental_markingE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_incremental_markingE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_incremental_markingE, 1
_ZN2v88internalL31FLAGDEFAULT_incremental_markingE:
	.byte	1
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_trace_mutator_utilizationE,"a"
	.type	_ZN2v88internalL37FLAGDEFAULT_trace_mutator_utilizationE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_trace_mutator_utilizationE, 1
_ZN2v88internalL37FLAGDEFAULT_trace_mutator_utilizationE:
	.zero	1
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_trace_evacuationE,"a"
	.type	_ZN2v88internalL28FLAGDEFAULT_trace_evacuationE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_trace_evacuationE, 1
_ZN2v88internalL28FLAGDEFAULT_trace_evacuationE:
	.zero	1
	.section	.rodata._ZN2v88internalL39FLAGDEFAULT_trace_fragmentation_verboseE,"a"
	.type	_ZN2v88internalL39FLAGDEFAULT_trace_fragmentation_verboseE, @object
	.size	_ZN2v88internalL39FLAGDEFAULT_trace_fragmentation_verboseE, 1
_ZN2v88internalL39FLAGDEFAULT_trace_fragmentation_verboseE:
	.zero	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_trace_fragmentationE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_trace_fragmentationE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_trace_fragmentationE, 1
_ZN2v88internalL31FLAGDEFAULT_trace_fragmentationE:
	.zero	1
	.section	.rodata._ZN2v88internalL40FLAGDEFAULT_trace_duplicate_threshold_kbE,"a"
	.align 4
	.type	_ZN2v88internalL40FLAGDEFAULT_trace_duplicate_threshold_kbE, @object
	.size	_ZN2v88internalL40FLAGDEFAULT_trace_duplicate_threshold_kbE, 4
_ZN2v88internalL40FLAGDEFAULT_trace_duplicate_threshold_kbE:
	.zero	4
	.section	.rodata._ZN2v88internalL43FLAGDEFAULT_trace_allocation_stack_intervalE,"a"
	.align 4
	.type	_ZN2v88internalL43FLAGDEFAULT_trace_allocation_stack_intervalE, @object
	.size	_ZN2v88internalL43FLAGDEFAULT_trace_allocation_stack_intervalE, 4
_ZN2v88internalL43FLAGDEFAULT_trace_allocation_stack_intervalE:
	.long	-1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_gc_freelist_strategyE,"a"
	.align 4
	.type	_ZN2v88internalL32FLAGDEFAULT_gc_freelist_strategyE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_gc_freelist_strategyE, 4
_ZN2v88internalL32FLAGDEFAULT_gc_freelist_strategyE:
	.long	5
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_trace_allocations_originsE,"a"
	.type	_ZN2v88internalL37FLAGDEFAULT_trace_allocations_originsE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_trace_allocations_originsE, 1
_ZN2v88internalL37FLAGDEFAULT_trace_allocations_originsE:
	.zero	1
	.section	.rodata._ZN2v88internalL39FLAGDEFAULT_trace_evacuation_candidatesE,"a"
	.type	_ZN2v88internalL39FLAGDEFAULT_trace_evacuation_candidatesE, @object
	.size	_ZN2v88internalL39FLAGDEFAULT_trace_evacuation_candidatesE, 1
_ZN2v88internalL39FLAGDEFAULT_trace_evacuation_candidatesE:
	.zero	1
	.section	.rodata._ZN2v88internalL38FLAGDEFAULT_trace_gc_freelists_verboseE,"a"
	.type	_ZN2v88internalL38FLAGDEFAULT_trace_gc_freelists_verboseE, @object
	.size	_ZN2v88internalL38FLAGDEFAULT_trace_gc_freelists_verboseE, 1
_ZN2v88internalL38FLAGDEFAULT_trace_gc_freelists_verboseE:
	.zero	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_trace_gc_freelistsE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_trace_gc_freelistsE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_trace_gc_freelistsE, 1
_ZN2v88internalL30FLAGDEFAULT_trace_gc_freelistsE:
	.zero	1
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_trace_gc_verboseE,"a"
	.type	_ZN2v88internalL28FLAGDEFAULT_trace_gc_verboseE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_trace_gc_verboseE, 1
_ZN2v88internalL28FLAGDEFAULT_trace_gc_verboseE:
	.zero	1
	.section	.rodata._ZN2v88internalL43FLAGDEFAULT_trace_idle_notification_verboseE,"a"
	.type	_ZN2v88internalL43FLAGDEFAULT_trace_idle_notification_verboseE, @object
	.size	_ZN2v88internalL43FLAGDEFAULT_trace_idle_notification_verboseE, 1
_ZN2v88internalL43FLAGDEFAULT_trace_idle_notification_verboseE:
	.zero	1
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_trace_idle_notificationE,"a"
	.type	_ZN2v88internalL35FLAGDEFAULT_trace_idle_notificationE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_trace_idle_notificationE, 1
_ZN2v88internalL35FLAGDEFAULT_trace_idle_notificationE:
	.zero	1
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_trace_gc_ignore_scavengerE,"a"
	.type	_ZN2v88internalL37FLAGDEFAULT_trace_gc_ignore_scavengerE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_trace_gc_ignore_scavengerE, 1
_ZN2v88internalL37FLAGDEFAULT_trace_gc_ignore_scavengerE:
	.zero	1
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_trace_gc_nvpE,"a"
	.type	_ZN2v88internalL24FLAGDEFAULT_trace_gc_nvpE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_trace_gc_nvpE, 1
_ZN2v88internalL24FLAGDEFAULT_trace_gc_nvpE:
	.zero	1
	.section	.rodata._ZN2v88internalL20FLAGDEFAULT_trace_gcE,"a"
	.type	_ZN2v88internalL20FLAGDEFAULT_trace_gcE, @object
	.size	_ZN2v88internalL20FLAGDEFAULT_trace_gcE, 1
_ZN2v88internalL20FLAGDEFAULT_trace_gcE:
	.zero	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_retain_maps_for_n_gcE,"a"
	.align 4
	.type	_ZN2v88internalL32FLAGDEFAULT_retain_maps_for_n_gcE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_retain_maps_for_n_gcE, 4
_ZN2v88internalL32FLAGDEFAULT_retain_maps_for_n_gcE:
	.long	2
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_gc_intervalE,"a"
	.align 4
	.type	_ZN2v88internalL23FLAGDEFAULT_gc_intervalE, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_gc_intervalE, 4
_ZN2v88internalL23FLAGDEFAULT_gc_intervalE:
	.long	-1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_random_gc_intervalE,"a"
	.align 4
	.type	_ZN2v88internalL30FLAGDEFAULT_random_gc_intervalE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_random_gc_intervalE, 4
_ZN2v88internalL30FLAGDEFAULT_random_gc_intervalE:
	.zero	4
	.section	.rodata._ZN2v88internalL21FLAGDEFAULT_gc_globalE,"a"
	.type	_ZN2v88internalL21FLAGDEFAULT_gc_globalE, @object
	.size	_ZN2v88internalL21FLAGDEFAULT_gc_globalE, 1
_ZN2v88internalL21FLAGDEFAULT_gc_globalE:
	.zero	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_global_gc_schedulingE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_global_gc_schedulingE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_global_gc_schedulingE, 1
_ZN2v88internalL32FLAGDEFAULT_global_gc_schedulingE:
	.byte	1
	.section	.rodata._ZN2v88internalL34FLAGDEFAULT_initial_old_space_sizeE,"a"
	.align 8
	.type	_ZN2v88internalL34FLAGDEFAULT_initial_old_space_sizeE, @object
	.size	_ZN2v88internalL34FLAGDEFAULT_initial_old_space_sizeE, 8
_ZN2v88internalL34FLAGDEFAULT_initial_old_space_sizeE:
	.zero	8
	.section	.rodata._ZN2v88internalL40FLAGDEFAULT_huge_max_old_generation_sizeE,"a"
	.type	_ZN2v88internalL40FLAGDEFAULT_huge_max_old_generation_sizeE, @object
	.size	_ZN2v88internalL40FLAGDEFAULT_huge_max_old_generation_sizeE, 1
_ZN2v88internalL40FLAGDEFAULT_huge_max_old_generation_sizeE:
	.zero	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_initial_heap_sizeE,"a"
	.align 8
	.type	_ZN2v88internalL29FLAGDEFAULT_initial_heap_sizeE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_initial_heap_sizeE, 8
_ZN2v88internalL29FLAGDEFAULT_initial_heap_sizeE:
	.zero	8
	.section	.rodata._ZN2v88internalL25FLAGDEFAULT_max_heap_sizeE,"a"
	.align 8
	.type	_ZN2v88internalL25FLAGDEFAULT_max_heap_sizeE, @object
	.size	_ZN2v88internalL25FLAGDEFAULT_max_heap_sizeE, 8
_ZN2v88internalL25FLAGDEFAULT_max_heap_sizeE:
	.zero	8
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_max_old_space_sizeE,"a"
	.align 8
	.type	_ZN2v88internalL30FLAGDEFAULT_max_old_space_sizeE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_max_old_space_sizeE, 8
_ZN2v88internalL30FLAGDEFAULT_max_old_space_sizeE:
	.zero	8
	.section	.rodata._ZN2v88internalL51FLAGDEFAULT_experimental_new_space_growth_heuristicE,"a"
	.type	_ZN2v88internalL51FLAGDEFAULT_experimental_new_space_growth_heuristicE, @object
	.size	_ZN2v88internalL51FLAGDEFAULT_experimental_new_space_growth_heuristicE, 1
_ZN2v88internalL51FLAGDEFAULT_experimental_new_space_growth_heuristicE:
	.zero	1
	.section	.rodata._ZN2v88internalL36FLAGDEFAULT_semi_space_growth_factorE,"a"
	.align 4
	.type	_ZN2v88internalL36FLAGDEFAULT_semi_space_growth_factorE, @object
	.size	_ZN2v88internalL36FLAGDEFAULT_semi_space_growth_factorE, 4
_ZN2v88internalL36FLAGDEFAULT_semi_space_growth_factorE:
	.long	2
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_max_semi_space_sizeE,"a"
	.align 8
	.type	_ZN2v88internalL31FLAGDEFAULT_max_semi_space_sizeE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_max_semi_space_sizeE, 8
_ZN2v88internalL31FLAGDEFAULT_max_semi_space_sizeE:
	.zero	8
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_min_semi_space_sizeE,"a"
	.align 8
	.type	_ZN2v88internalL31FLAGDEFAULT_min_semi_space_sizeE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_min_semi_space_sizeE, 8
_ZN2v88internalL31FLAGDEFAULT_min_semi_space_sizeE:
	.zero	8
	.section	.rodata._ZN2v88internalL47FLAGDEFAULT_stress_sampling_allocation_profilerE,"a"
	.align 4
	.type	_ZN2v88internalL47FLAGDEFAULT_stress_sampling_allocation_profilerE, @object
	.size	_ZN2v88internalL47FLAGDEFAULT_stress_sampling_allocation_profilerE, 4
_ZN2v88internalL47FLAGDEFAULT_stress_sampling_allocation_profilerE:
	.zero	4
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_frame_countE,"a"
	.align 4
	.type	_ZN2v88internalL23FLAGDEFAULT_frame_countE, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_frame_countE, 4
_ZN2v88internalL23FLAGDEFAULT_frame_countE:
	.long	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_stress_wasm_code_gcE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_stress_wasm_code_gcE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_stress_wasm_code_gcE, 1
_ZN2v88internalL31FLAGDEFAULT_stress_wasm_code_gcE:
	.zero	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_trace_wasm_code_gcE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_trace_wasm_code_gcE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_trace_wasm_code_gcE, 1
_ZN2v88internalL30FLAGDEFAULT_trace_wasm_code_gcE:
	.zero	1
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_wasm_code_gcE,"a"
	.type	_ZN2v88internalL24FLAGDEFAULT_wasm_code_gcE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_wasm_code_gcE, 1
_ZN2v88internalL24FLAGDEFAULT_wasm_code_gcE:
	.byte	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_wasm_lazy_validationE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_wasm_lazy_validationE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_wasm_lazy_validationE, 1
_ZN2v88internalL32FLAGDEFAULT_wasm_lazy_validationE:
	.zero	1
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_wasm_grow_shared_memoryE,"a"
	.type	_ZN2v88internalL35FLAGDEFAULT_wasm_grow_shared_memoryE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_wasm_grow_shared_memoryE, 1
_ZN2v88internalL35FLAGDEFAULT_wasm_grow_shared_memoryE:
	.zero	1
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_wasm_lazy_compilationE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_wasm_lazy_compilationE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_wasm_lazy_compilationE, 1
_ZN2v88internalL33FLAGDEFAULT_wasm_lazy_compilationE:
	.zero	1
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_asm_wasm_lazy_compilationE,"a"
	.type	_ZN2v88internalL37FLAGDEFAULT_asm_wasm_lazy_compilationE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_asm_wasm_lazy_compilationE, 1
_ZN2v88internalL37FLAGDEFAULT_asm_wasm_lazy_compilationE:
	.zero	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_wasm_interpret_allE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_wasm_interpret_allE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_wasm_interpret_allE, 1
_ZN2v88internalL30FLAGDEFAULT_wasm_interpret_allE:
	.zero	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_print_wasm_stub_codeE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_print_wasm_stub_codeE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_print_wasm_stub_codeE, 1
_ZN2v88internalL32FLAGDEFAULT_print_wasm_stub_codeE:
	.zero	1
	.section	.rodata._ZN2v88internalL27FLAGDEFAULT_print_wasm_codeE,"a"
	.type	_ZN2v88internalL27FLAGDEFAULT_print_wasm_codeE, @object
	.size	_ZN2v88internalL27FLAGDEFAULT_print_wasm_codeE, 1
_ZN2v88internalL27FLAGDEFAULT_print_wasm_codeE:
	.zero	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_wasm_fuzzer_gen_testE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_wasm_fuzzer_gen_testE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_wasm_fuzzer_gen_testE, 1
_ZN2v88internalL32FLAGDEFAULT_wasm_fuzzer_gen_testE:
	.zero	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_wasm_trap_handlerE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_wasm_trap_handlerE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_wasm_trap_handlerE, 1
_ZN2v88internalL29FLAGDEFAULT_wasm_trap_handlerE:
	.byte	1
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_wasm_shared_codeE,"a"
	.type	_ZN2v88internalL28FLAGDEFAULT_wasm_shared_codeE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_wasm_shared_codeE, 1
_ZN2v88internalL28FLAGDEFAULT_wasm_shared_codeE:
	.byte	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_wasm_shared_engineE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_wasm_shared_engineE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_wasm_shared_engineE, 1
_ZN2v88internalL30FLAGDEFAULT_wasm_shared_engineE:
	.byte	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_wasm_math_intrinsicsE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_wasm_math_intrinsicsE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_wasm_math_intrinsicsE, 1
_ZN2v88internalL32FLAGDEFAULT_wasm_math_intrinsicsE:
	.byte	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_wasm_no_stack_checksE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_wasm_no_stack_checksE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_wasm_no_stack_checksE, 1
_ZN2v88internalL32FLAGDEFAULT_wasm_no_stack_checksE:
	.zero	1
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_wasm_no_bounds_checksE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_wasm_no_bounds_checksE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_wasm_no_bounds_checksE, 1
_ZN2v88internalL33FLAGDEFAULT_wasm_no_bounds_checksE:
	.zero	1
	.section	.rodata._ZN2v88internalL20FLAGDEFAULT_wasm_optE,"a"
	.type	_ZN2v88internalL20FLAGDEFAULT_wasm_optE, @object
	.size	_ZN2v88internalL20FLAGDEFAULT_wasm_optE, 1
_ZN2v88internalL20FLAGDEFAULT_wasm_optE:
	.zero	1
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_wasm_stagingE,"a"
	.type	_ZN2v88internalL24FLAGDEFAULT_wasm_stagingE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_wasm_stagingE, 1
_ZN2v88internalL24FLAGDEFAULT_wasm_stagingE:
	.zero	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_experimental_wasm_seE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_experimental_wasm_seE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_experimental_wasm_seE, 1
_ZN2v88internalL32FLAGDEFAULT_experimental_wasm_seE:
	.byte	1
	.section	.rodata._ZN2v88internalL49FLAGDEFAULT_experimental_wasm_sat_f2i_conversionsE,"a"
	.type	_ZN2v88internalL49FLAGDEFAULT_experimental_wasm_sat_f2i_conversionsE, @object
	.size	_ZN2v88internalL49FLAGDEFAULT_experimental_wasm_sat_f2i_conversionsE, 1
_ZN2v88internalL49FLAGDEFAULT_experimental_wasm_sat_f2i_conversionsE:
	.byte	1
	.section	.rodata._ZN2v88internalL41FLAGDEFAULT_experimental_wasm_bulk_memoryE,"a"
	.type	_ZN2v88internalL41FLAGDEFAULT_experimental_wasm_bulk_memoryE, @object
	.size	_ZN2v88internalL41FLAGDEFAULT_experimental_wasm_bulk_memoryE, 1
_ZN2v88internalL41FLAGDEFAULT_experimental_wasm_bulk_memoryE:
	.byte	1
	.section	.rodata._ZN2v88internalL45FLAGDEFAULT_experimental_wasm_type_reflectionE,"a"
	.type	_ZN2v88internalL45FLAGDEFAULT_experimental_wasm_type_reflectionE, @object
	.size	_ZN2v88internalL45FLAGDEFAULT_experimental_wasm_type_reflectionE, 1
_ZN2v88internalL45FLAGDEFAULT_experimental_wasm_type_reflectionE:
	.zero	1
	.section	.rodata._ZN2v88internalL36FLAGDEFAULT_experimental_wasm_anyrefE,"a"
	.type	_ZN2v88internalL36FLAGDEFAULT_experimental_wasm_anyrefE, @object
	.size	_ZN2v88internalL36FLAGDEFAULT_experimental_wasm_anyrefE, 1
_ZN2v88internalL36FLAGDEFAULT_experimental_wasm_anyrefE:
	.zero	1
	.section	.rodata._ZN2v88internalL47FLAGDEFAULT_experimental_wasm_compilation_hintsE,"a"
	.type	_ZN2v88internalL47FLAGDEFAULT_experimental_wasm_compilation_hintsE, @object
	.size	_ZN2v88internalL47FLAGDEFAULT_experimental_wasm_compilation_hintsE, 1
_ZN2v88internalL47FLAGDEFAULT_experimental_wasm_compilation_hintsE:
	.zero	1
	.section	.rodata._ZN2v88internalL41FLAGDEFAULT_experimental_wasm_return_callE,"a"
	.type	_ZN2v88internalL41FLAGDEFAULT_experimental_wasm_return_callE, @object
	.size	_ZN2v88internalL41FLAGDEFAULT_experimental_wasm_return_callE, 1
_ZN2v88internalL41FLAGDEFAULT_experimental_wasm_return_callE:
	.zero	1
	.section	.rodata._ZN2v88internalL36FLAGDEFAULT_experimental_wasm_bigintE,"a"
	.type	_ZN2v88internalL36FLAGDEFAULT_experimental_wasm_bigintE, @object
	.size	_ZN2v88internalL36FLAGDEFAULT_experimental_wasm_bigintE, 1
_ZN2v88internalL36FLAGDEFAULT_experimental_wasm_bigintE:
	.zero	1
	.section	.rodata._ZN2v88internalL34FLAGDEFAULT_experimental_wasm_simdE,"a"
	.type	_ZN2v88internalL34FLAGDEFAULT_experimental_wasm_simdE, @object
	.size	_ZN2v88internalL34FLAGDEFAULT_experimental_wasm_simdE, 1
_ZN2v88internalL34FLAGDEFAULT_experimental_wasm_simdE:
	.zero	1
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_experimental_wasm_threadsE,"a"
	.type	_ZN2v88internalL37FLAGDEFAULT_experimental_wasm_threadsE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_experimental_wasm_threadsE, 1
_ZN2v88internalL37FLAGDEFAULT_experimental_wasm_threadsE:
	.zero	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_experimental_wasm_ehE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_experimental_wasm_ehE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_experimental_wasm_ehE, 1
_ZN2v88internalL32FLAGDEFAULT_experimental_wasm_ehE:
	.zero	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_experimental_wasm_mvE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_experimental_wasm_mvE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_experimental_wasm_mvE, 1
_ZN2v88internalL32FLAGDEFAULT_experimental_wasm_mvE:
	.zero	1
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_dump_wasm_module_pathE,"a"
	.align 8
	.type	_ZN2v88internalL33FLAGDEFAULT_dump_wasm_module_pathE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_dump_wasm_module_pathE, 8
_ZN2v88internalL33FLAGDEFAULT_dump_wasm_module_pathE:
	.zero	8
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_stress_validate_asmE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_stress_validate_asmE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_stress_validate_asmE, 1
_ZN2v88internalL31FLAGDEFAULT_stress_validate_asmE:
	.zero	1
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_trace_asm_parserE,"a"
	.type	_ZN2v88internalL28FLAGDEFAULT_trace_asm_parserE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_trace_asm_parserE, 1
_ZN2v88internalL28FLAGDEFAULT_trace_asm_parserE:
	.zero	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_trace_asm_scannerE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_trace_asm_scannerE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_trace_asm_scannerE, 1
_ZN2v88internalL29FLAGDEFAULT_trace_asm_scannerE:
	.zero	1
	.section	.rodata._ZN2v88internalL26FLAGDEFAULT_trace_asm_timeE,"a"
	.type	_ZN2v88internalL26FLAGDEFAULT_trace_asm_timeE, @object
	.size	_ZN2v88internalL26FLAGDEFAULT_trace_asm_timeE, 1
_ZN2v88internalL26FLAGDEFAULT_trace_asm_timeE:
	.zero	1
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_suppress_asm_messagesE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_suppress_asm_messagesE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_suppress_asm_messagesE, 1
_ZN2v88internalL33FLAGDEFAULT_suppress_asm_messagesE:
	.zero	1
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_validate_asmE,"a"
	.type	_ZN2v88internalL24FLAGDEFAULT_validate_asmE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_validate_asmE, 1
_ZN2v88internalL24FLAGDEFAULT_validate_asmE:
	.byte	1
	.section	.rodata._ZN2v88internalL38FLAGDEFAULT_wasm_tier_mask_for_testingE,"a"
	.align 4
	.type	_ZN2v88internalL38FLAGDEFAULT_wasm_tier_mask_for_testingE, @object
	.size	_ZN2v88internalL38FLAGDEFAULT_wasm_tier_mask_for_testingE, 4
_ZN2v88internalL38FLAGDEFAULT_wasm_tier_mask_for_testingE:
	.zero	4
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_trace_wasm_memoryE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_trace_wasm_memoryE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_trace_wasm_memoryE, 1
_ZN2v88internalL29FLAGDEFAULT_trace_wasm_memoryE:
	.zero	1
	.section	.rodata._ZN2v88internalL19FLAGDEFAULT_liftoffE,"a"
	.type	_ZN2v88internalL19FLAGDEFAULT_liftoffE, @object
	.size	_ZN2v88internalL19FLAGDEFAULT_liftoffE, 1
_ZN2v88internalL19FLAGDEFAULT_liftoffE:
	.zero	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_trace_wasm_ast_endE,"a"
	.align 4
	.type	_ZN2v88internalL30FLAGDEFAULT_trace_wasm_ast_endE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_trace_wasm_ast_endE, 4
_ZN2v88internalL30FLAGDEFAULT_trace_wasm_ast_endE:
	.zero	4
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_trace_wasm_ast_startE,"a"
	.align 4
	.type	_ZN2v88internalL32FLAGDEFAULT_trace_wasm_ast_startE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_trace_wasm_ast_startE, 4
_ZN2v88internalL32FLAGDEFAULT_trace_wasm_ast_startE:
	.zero	4
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_wasm_tier_upE,"a"
	.type	_ZN2v88internalL24FLAGDEFAULT_wasm_tier_upE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_wasm_tier_upE, 1
_ZN2v88internalL24FLAGDEFAULT_wasm_tier_upE:
	.byte	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_wasm_max_code_spaceE,"a"
	.align 4
	.type	_ZN2v88internalL31FLAGDEFAULT_wasm_max_code_spaceE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_wasm_max_code_spaceE, 4
_ZN2v88internalL31FLAGDEFAULT_wasm_max_code_spaceE:
	.long	1024
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_wasm_max_table_sizeE,"a"
	.align 4
	.type	_ZN2v88internalL31FLAGDEFAULT_wasm_max_table_sizeE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_wasm_max_table_sizeE, 4
_ZN2v88internalL31FLAGDEFAULT_wasm_max_table_sizeE:
	.long	10000000
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_wasm_max_mem_pagesE,"a"
	.align 4
	.type	_ZN2v88internalL30FLAGDEFAULT_wasm_max_mem_pagesE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_wasm_max_mem_pagesE, 4
_ZN2v88internalL30FLAGDEFAULT_wasm_max_mem_pagesE:
	.long	32767
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_wasm_test_streamingE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_wasm_test_streamingE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_wasm_test_streamingE, 1
_ZN2v88internalL31FLAGDEFAULT_wasm_test_streamingE:
	.zero	1
	.section	.rodata._ZN2v88internalL34FLAGDEFAULT_wasm_async_compilationE,"a"
	.type	_ZN2v88internalL34FLAGDEFAULT_wasm_async_compilationE, @object
	.size	_ZN2v88internalL34FLAGDEFAULT_wasm_async_compilationE, 1
_ZN2v88internalL34FLAGDEFAULT_wasm_async_compilationE:
	.byte	1
	.section	.rodata._ZN2v88internalL36FLAGDEFAULT_trace_wasm_serializationE,"a"
	.type	_ZN2v88internalL36FLAGDEFAULT_trace_wasm_serializationE, @object
	.size	_ZN2v88internalL36FLAGDEFAULT_trace_wasm_serializationE, 1
_ZN2v88internalL36FLAGDEFAULT_trace_wasm_serializationE:
	.zero	1
	.section	.rodata._ZN2v88internalL42FLAGDEFAULT_wasm_write_protect_code_memoryE,"a"
	.type	_ZN2v88internalL42FLAGDEFAULT_wasm_write_protect_code_memoryE, @object
	.size	_ZN2v88internalL42FLAGDEFAULT_wasm_write_protect_code_memoryE, 1
_ZN2v88internalL42FLAGDEFAULT_wasm_write_protect_code_memoryE:
	.zero	1
	.section	.rodata._ZN2v88internalL38FLAGDEFAULT_wasm_num_compilation_tasksE,"a"
	.align 4
	.type	_ZN2v88internalL38FLAGDEFAULT_wasm_num_compilation_tasksE, @object
	.size	_ZN2v88internalL38FLAGDEFAULT_wasm_num_compilation_tasksE, 4
_ZN2v88internalL38FLAGDEFAULT_wasm_num_compilation_tasksE:
	.long	10
	.section	.rodata._ZN2v88internalL43FLAGDEFAULT_wasm_disable_structured_cloningE,"a"
	.type	_ZN2v88internalL43FLAGDEFAULT_wasm_disable_structured_cloningE, @object
	.size	_ZN2v88internalL43FLAGDEFAULT_wasm_disable_structured_cloningE, 1
_ZN2v88internalL43FLAGDEFAULT_wasm_disable_structured_cloningE:
	.zero	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_assume_asmjs_originE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_assume_asmjs_originE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_assume_asmjs_originE, 1
_ZN2v88internalL31FLAGDEFAULT_assume_asmjs_originE:
	.zero	1
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_expose_wasmE,"a"
	.type	_ZN2v88internalL23FLAGDEFAULT_expose_wasmE, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_expose_wasmE, 1
_ZN2v88internalL23FLAGDEFAULT_expose_wasmE:
	.byte	1
	.section	.rodata._ZN2v88internalL38FLAGDEFAULT_untrusted_code_mitigationsE,"a"
	.type	_ZN2v88internalL38FLAGDEFAULT_untrusted_code_mitigationsE, @object
	.size	_ZN2v88internalL38FLAGDEFAULT_untrusted_code_mitigationsE, 1
_ZN2v88internalL38FLAGDEFAULT_untrusted_code_mitigationsE:
	.zero	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_optimize_for_sizeE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_optimize_for_sizeE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_optimize_for_sizeE, 1
_ZN2v88internalL29FLAGDEFAULT_optimize_for_sizeE:
	.zero	1
	.section	.rodata._ZN2v88internalL40FLAGDEFAULT_stress_gc_during_compilationE,"a"
	.type	_ZN2v88internalL40FLAGDEFAULT_stress_gc_during_compilationE, @object
	.size	_ZN2v88internalL40FLAGDEFAULT_stress_gc_during_compilationE, 1
_ZN2v88internalL40FLAGDEFAULT_stress_gc_during_compilationE:
	.zero	1
	.section	.rodata._ZN2v88internalL51FLAGDEFAULT_experimental_inline_promise_constructorE,"a"
	.type	_ZN2v88internalL51FLAGDEFAULT_experimental_inline_promise_constructorE, @object
	.size	_ZN2v88internalL51FLAGDEFAULT_experimental_inline_promise_constructorE, 1
_ZN2v88internalL51FLAGDEFAULT_experimental_inline_promise_constructorE:
	.byte	1
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_turbo_rewrite_far_jumpsE,"a"
	.type	_ZN2v88internalL35FLAGDEFAULT_turbo_rewrite_far_jumpsE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_turbo_rewrite_far_jumpsE, 1
_ZN2v88internalL35FLAGDEFAULT_turbo_rewrite_far_jumpsE:
	.byte	1
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_trace_store_eliminationE,"a"
	.type	_ZN2v88internalL35FLAGDEFAULT_trace_store_eliminationE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_trace_store_eliminationE, 1
_ZN2v88internalL35FLAGDEFAULT_trace_store_eliminationE:
	.zero	1
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_turbo_store_eliminationE,"a"
	.type	_ZN2v88internalL35FLAGDEFAULT_turbo_store_eliminationE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_turbo_store_eliminationE, 1
_ZN2v88internalL35FLAGDEFAULT_turbo_store_eliminationE:
	.byte	1
	.section	.rodata._ZN2v88internalL47FLAGDEFAULT_turbo_stress_instruction_schedulingE,"a"
	.type	_ZN2v88internalL47FLAGDEFAULT_turbo_stress_instruction_schedulingE, @object
	.size	_ZN2v88internalL47FLAGDEFAULT_turbo_stress_instruction_schedulingE, 1
_ZN2v88internalL47FLAGDEFAULT_turbo_stress_instruction_schedulingE:
	.zero	1
	.section	.rodata._ZN2v88internalL40FLAGDEFAULT_turbo_instruction_schedulingE,"a"
	.type	_ZN2v88internalL40FLAGDEFAULT_turbo_instruction_schedulingE, @object
	.size	_ZN2v88internalL40FLAGDEFAULT_turbo_instruction_schedulingE, 1
_ZN2v88internalL40FLAGDEFAULT_turbo_instruction_schedulingE:
	.zero	1
	.section	.rodata._ZN2v88internalL36FLAGDEFAULT_turbo_allocation_foldingE,"a"
	.type	_ZN2v88internalL36FLAGDEFAULT_turbo_allocation_foldingE, @object
	.size	_ZN2v88internalL36FLAGDEFAULT_turbo_allocation_foldingE, 1
_ZN2v88internalL36FLAGDEFAULT_turbo_allocation_foldingE:
	.byte	1
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_turbo_escapeE,"a"
	.type	_ZN2v88internalL24FLAGDEFAULT_turbo_escapeE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_turbo_escapeE, 1
_ZN2v88internalL24FLAGDEFAULT_turbo_escapeE:
	.byte	1
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_turbo_cf_optimizationE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_turbo_cf_optimizationE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_turbo_cf_optimizationE, 1
_ZN2v88internalL33FLAGDEFAULT_turbo_cf_optimizationE:
	.byte	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_turbo_loop_rotationE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_turbo_loop_rotationE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_turbo_loop_rotationE, 1
_ZN2v88internalL31FLAGDEFAULT_turbo_loop_rotationE:
	.byte	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_turbo_loop_variableE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_turbo_loop_variableE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_turbo_loop_variableE, 1
_ZN2v88internalL31FLAGDEFAULT_turbo_loop_variableE:
	.byte	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_turbo_loop_peelingE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_turbo_loop_peelingE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_turbo_loop_peelingE, 1
_ZN2v88internalL30FLAGDEFAULT_turbo_loop_peelingE:
	.byte	1
	.section	.rodata._ZN2v88internalL20FLAGDEFAULT_turbo_jtE,"a"
	.type	_ZN2v88internalL20FLAGDEFAULT_turbo_jtE, @object
	.size	_ZN2v88internalL20FLAGDEFAULT_turbo_jtE, 1
_ZN2v88internalL20FLAGDEFAULT_turbo_jtE:
	.byte	1
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_turbo_move_optimizationE,"a"
	.type	_ZN2v88internalL35FLAGDEFAULT_turbo_move_optimizationE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_turbo_move_optimizationE, 1
_ZN2v88internalL35FLAGDEFAULT_turbo_move_optimizationE:
	.byte	1
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_turbo_verify_allocationE,"a"
	.type	_ZN2v88internalL35FLAGDEFAULT_turbo_verify_allocationE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_turbo_verify_allocationE, 1
_ZN2v88internalL35FLAGDEFAULT_turbo_verify_allocationE:
	.zero	1
	.section	.rodata._ZN2v88internalL27FLAGDEFAULT_turbo_profilingE,"a"
	.type	_ZN2v88internalL27FLAGDEFAULT_turbo_profilingE, @object
	.size	_ZN2v88internalL27FLAGDEFAULT_turbo_profilingE, 1
_ZN2v88internalL27FLAGDEFAULT_turbo_profilingE:
	.zero	1
	.section	.rodata._ZN2v88internalL40FLAGDEFAULT_trace_turbo_load_eliminationE,"a"
	.type	_ZN2v88internalL40FLAGDEFAULT_trace_turbo_load_eliminationE, @object
	.size	_ZN2v88internalL40FLAGDEFAULT_trace_turbo_load_eliminationE, 1
_ZN2v88internalL40FLAGDEFAULT_trace_turbo_load_eliminationE:
	.zero	1
	.section	.rodata._ZN2v88internalL34FLAGDEFAULT_turbo_load_eliminationE,"a"
	.type	_ZN2v88internalL34FLAGDEFAULT_turbo_load_eliminationE, @object
	.size	_ZN2v88internalL34FLAGDEFAULT_turbo_load_eliminationE, 1
_ZN2v88internalL34FLAGDEFAULT_turbo_load_eliminationE:
	.byte	1
	.section	.rodata._ZN2v88internalL38FLAGDEFAULT_trace_environment_livenessE,"a"
	.type	_ZN2v88internalL38FLAGDEFAULT_trace_environment_livenessE, @object
	.size	_ZN2v88internalL38FLAGDEFAULT_trace_environment_livenessE, 1
_ZN2v88internalL38FLAGDEFAULT_trace_environment_livenessE:
	.zero	1
	.section	.rodata._ZN2v88internalL40FLAGDEFAULT_analyze_environment_livenessE,"a"
	.type	_ZN2v88internalL40FLAGDEFAULT_analyze_environment_livenessE, @object
	.size	_ZN2v88internalL40FLAGDEFAULT_analyze_environment_livenessE, 1
_ZN2v88internalL40FLAGDEFAULT_analyze_environment_livenessE:
	.byte	1
	.section	.rodata._ZN2v88internalL21FLAGDEFAULT_trace_osrE,"a"
	.type	_ZN2v88internalL21FLAGDEFAULT_trace_osrE, @object
	.size	_ZN2v88internalL21FLAGDEFAULT_trace_osrE, 1
_ZN2v88internalL21FLAGDEFAULT_trace_osrE:
	.zero	1
	.section	.rodata._ZN2v88internalL19FLAGDEFAULT_use_osrE,"a"
	.type	_ZN2v88internalL19FLAGDEFAULT_use_osrE, @object
	.size	_ZN2v88internalL19FLAGDEFAULT_use_osrE, 1
_ZN2v88internalL19FLAGDEFAULT_use_osrE:
	.byte	1
	.section	.rodata._ZN2v88internalL39FLAGDEFAULT_turbo_inline_array_builtinsE,"a"
	.type	_ZN2v88internalL39FLAGDEFAULT_turbo_inline_array_builtinsE, @object
	.size	_ZN2v88internalL39FLAGDEFAULT_turbo_inline_array_builtinsE, 1
_ZN2v88internalL39FLAGDEFAULT_turbo_inline_array_builtinsE:
	.byte	1
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_inline_accessorsE,"a"
	.type	_ZN2v88internalL28FLAGDEFAULT_inline_accessorsE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_inline_accessorsE, 1
_ZN2v88internalL28FLAGDEFAULT_inline_accessorsE:
	.byte	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_trace_turbo_inliningE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_trace_turbo_inliningE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_trace_turbo_inliningE, 1
_ZN2v88internalL32FLAGDEFAULT_trace_turbo_inliningE:
	.zero	1
	.section	.rodata._ZN2v88internalL25FLAGDEFAULT_stress_inlineE,"a"
	.type	_ZN2v88internalL25FLAGDEFAULT_stress_inlineE, @object
	.size	_ZN2v88internalL25FLAGDEFAULT_stress_inlineE, 1
_ZN2v88internalL25FLAGDEFAULT_stress_inlineE:
	.zero	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_polymorphic_inliningE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_polymorphic_inliningE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_polymorphic_inliningE, 1
_ZN2v88internalL32FLAGDEFAULT_polymorphic_inliningE:
	.byte	1
	.section	.rodata._ZN2v88internalL34FLAGDEFAULT_min_inlining_frequencyE,"a"
	.align 8
	.type	_ZN2v88internalL34FLAGDEFAULT_min_inlining_frequencyE, @object
	.size	_ZN2v88internalL34FLAGDEFAULT_min_inlining_frequencyE, 8
_ZN2v88internalL34FLAGDEFAULT_min_inlining_frequencyE:
	.long	858993459
	.long	1069757235
	.section	.rodata._ZN2v88internalL39FLAGDEFAULT_max_optimized_bytecode_sizeE,"a"
	.align 4
	.type	_ZN2v88internalL39FLAGDEFAULT_max_optimized_bytecode_sizeE, @object
	.size	_ZN2v88internalL39FLAGDEFAULT_max_optimized_bytecode_sizeE, 4
_ZN2v88internalL39FLAGDEFAULT_max_optimized_bytecode_sizeE:
	.long	61440
	.section	.rodata._ZN2v88internalL43FLAGDEFAULT_max_inlined_bytecode_size_smallE,"a"
	.align 4
	.type	_ZN2v88internalL43FLAGDEFAULT_max_inlined_bytecode_size_smallE, @object
	.size	_ZN2v88internalL43FLAGDEFAULT_max_inlined_bytecode_size_smallE, 4
_ZN2v88internalL43FLAGDEFAULT_max_inlined_bytecode_size_smallE:
	.long	30
	.section	.rodata._ZN2v88internalL46FLAGDEFAULT_reserve_inline_budget_scale_factorE,"a"
	.align 8
	.type	_ZN2v88internalL46FLAGDEFAULT_reserve_inline_budget_scale_factorE, @object
	.size	_ZN2v88internalL46FLAGDEFAULT_reserve_inline_budget_scale_factorE, 8
_ZN2v88internalL46FLAGDEFAULT_reserve_inline_budget_scale_factorE:
	.long	858993459
	.long	1072902963
	.section	.rodata._ZN2v88internalL46FLAGDEFAULT_max_inlined_bytecode_size_absoluteE,"a"
	.align 4
	.type	_ZN2v88internalL46FLAGDEFAULT_max_inlined_bytecode_size_absoluteE, @object
	.size	_ZN2v88internalL46FLAGDEFAULT_max_inlined_bytecode_size_absoluteE, 4
_ZN2v88internalL46FLAGDEFAULT_max_inlined_bytecode_size_absoluteE:
	.long	5000
	.section	.rodata._ZN2v88internalL48FLAGDEFAULT_max_inlined_bytecode_size_cumulativeE,"a"
	.align 4
	.type	_ZN2v88internalL48FLAGDEFAULT_max_inlined_bytecode_size_cumulativeE, @object
	.size	_ZN2v88internalL48FLAGDEFAULT_max_inlined_bytecode_size_cumulativeE, 4
_ZN2v88internalL48FLAGDEFAULT_max_inlined_bytecode_size_cumulativeE:
	.long	1000
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_max_inlined_bytecode_sizeE,"a"
	.align 4
	.type	_ZN2v88internalL37FLAGDEFAULT_max_inlined_bytecode_sizeE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_max_inlined_bytecode_sizeE, 4
_ZN2v88internalL37FLAGDEFAULT_max_inlined_bytecode_sizeE:
	.long	500
	.section	.rodata._ZN2v88internalL26FLAGDEFAULT_turbo_inliningE,"a"
	.type	_ZN2v88internalL26FLAGDEFAULT_turbo_inliningE, @object
	.size	_ZN2v88internalL26FLAGDEFAULT_turbo_inliningE, 1
_ZN2v88internalL26FLAGDEFAULT_turbo_inliningE:
	.byte	1
	.section	.rodata._ZN2v88internalL43FLAGDEFAULT_function_context_specializationE,"a"
	.type	_ZN2v88internalL43FLAGDEFAULT_function_context_specializationE, @object
	.size	_ZN2v88internalL43FLAGDEFAULT_function_context_specializationE, 1
_ZN2v88internalL43FLAGDEFAULT_function_context_specializationE:
	.zero	1
	.section	.rodata._ZN2v88internalL27FLAGDEFAULT_turbo_splittingE,"a"
	.type	_ZN2v88internalL27FLAGDEFAULT_turbo_splittingE, @object
	.size	_ZN2v88internalL27FLAGDEFAULT_turbo_splittingE, 1
_ZN2v88internalL27FLAGDEFAULT_turbo_splittingE:
	.byte	1
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_turbo_stats_wasmE,"a"
	.type	_ZN2v88internalL28FLAGDEFAULT_turbo_stats_wasmE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_turbo_stats_wasmE, 1
_ZN2v88internalL28FLAGDEFAULT_turbo_stats_wasmE:
	.zero	1
	.section	.rodata._ZN2v88internalL27FLAGDEFAULT_turbo_stats_nvpE,"a"
	.type	_ZN2v88internalL27FLAGDEFAULT_turbo_stats_nvpE, @object
	.size	_ZN2v88internalL27FLAGDEFAULT_turbo_stats_nvpE, 1
_ZN2v88internalL27FLAGDEFAULT_turbo_stats_nvpE:
	.zero	1
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_turbo_statsE,"a"
	.type	_ZN2v88internalL23FLAGDEFAULT_turbo_statsE, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_turbo_statsE, 1
_ZN2v88internalL23FLAGDEFAULT_turbo_statsE:
	.zero	1
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_csa_trap_on_nodeE,"a"
	.align 8
	.type	_ZN2v88internalL28FLAGDEFAULT_csa_trap_on_nodeE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_csa_trap_on_nodeE, 8
_ZN2v88internalL28FLAGDEFAULT_csa_trap_on_nodeE:
	.zero	8
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_trace_verify_csaE,"a"
	.type	_ZN2v88internalL28FLAGDEFAULT_trace_verify_csaE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_trace_verify_csaE, 1
_ZN2v88internalL28FLAGDEFAULT_trace_verify_csaE:
	.zero	1
	.section	.rodata._ZN2v88internalL38FLAGDEFAULT_turbo_verify_machine_graphE,"a"
	.align 8
	.type	_ZN2v88internalL38FLAGDEFAULT_turbo_verify_machine_graphE, @object
	.size	_ZN2v88internalL38FLAGDEFAULT_turbo_verify_machine_graphE, 8
_ZN2v88internalL38FLAGDEFAULT_turbo_verify_machine_graphE:
	.zero	8
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_turbo_verifyE,"a"
	.type	_ZN2v88internalL24FLAGDEFAULT_turbo_verifyE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_turbo_verifyE, 1
_ZN2v88internalL24FLAGDEFAULT_turbo_verifyE:
	.zero	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_trace_representationE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_trace_representationE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_trace_representationE, 1
_ZN2v88internalL32FLAGDEFAULT_trace_representationE:
	.zero	1
	.section	.rodata._ZN2v88internalL26FLAGDEFAULT_trace_all_usesE,"a"
	.type	_ZN2v88internalL26FLAGDEFAULT_trace_all_usesE, @object
	.size	_ZN2v88internalL26FLAGDEFAULT_trace_all_usesE, 1
_ZN2v88internalL26FLAGDEFAULT_trace_all_usesE:
	.zero	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_trace_turbo_allocE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_trace_turbo_allocE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_trace_turbo_allocE, 1
_ZN2v88internalL29FLAGDEFAULT_trace_turbo_allocE:
	.zero	1
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_trace_turbo_loopE,"a"
	.type	_ZN2v88internalL28FLAGDEFAULT_trace_turbo_loopE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_trace_turbo_loopE, 1
_ZN2v88internalL28FLAGDEFAULT_trace_turbo_loopE:
	.zero	1
	.section	.rodata._ZN2v88internalL27FLAGDEFAULT_trace_turbo_ceqE,"a"
	.type	_ZN2v88internalL27FLAGDEFAULT_trace_turbo_ceqE, @object
	.size	_ZN2v88internalL27FLAGDEFAULT_trace_turbo_ceqE, 1
_ZN2v88internalL27FLAGDEFAULT_trace_turbo_ceqE:
	.zero	1
	.section	.rodata._ZN2v88internalL26FLAGDEFAULT_trace_turbo_jtE,"a"
	.type	_ZN2v88internalL26FLAGDEFAULT_trace_turbo_jtE, @object
	.size	_ZN2v88internalL26FLAGDEFAULT_trace_turbo_jtE, 1
_ZN2v88internalL26FLAGDEFAULT_trace_turbo_jtE:
	.zero	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_trace_turbo_trimmingE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_trace_turbo_trimmingE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_trace_turbo_trimmingE, 1
_ZN2v88internalL32FLAGDEFAULT_trace_turbo_trimmingE:
	.zero	1
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_trace_turbo_reductionE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_trace_turbo_reductionE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_trace_turbo_reductionE, 1
_ZN2v88internalL33FLAGDEFAULT_trace_turbo_reductionE:
	.zero	1
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_trace_turbo_schedulerE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_trace_turbo_schedulerE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_trace_turbo_schedulerE, 1
_ZN2v88internalL33FLAGDEFAULT_trace_turbo_schedulerE:
	.zero	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_trace_turbo_typesE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_trace_turbo_typesE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_trace_turbo_typesE, 1
_ZN2v88internalL29FLAGDEFAULT_trace_turbo_typesE:
	.byte	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_trace_turbo_cfg_fileE,"a"
	.align 8
	.type	_ZN2v88internalL32FLAGDEFAULT_trace_turbo_cfg_fileE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_trace_turbo_cfg_fileE, 8
_ZN2v88internalL32FLAGDEFAULT_trace_turbo_cfg_fileE:
	.zero	8
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_trace_turbo_scheduledE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_trace_turbo_scheduledE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_trace_turbo_scheduledE, 1
_ZN2v88internalL33FLAGDEFAULT_trace_turbo_scheduledE:
	.zero	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_trace_turbo_graphE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_trace_turbo_graphE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_trace_turbo_graphE, 1
_ZN2v88internalL29FLAGDEFAULT_trace_turbo_graphE:
	.zero	1
	.section	.data.rel.ro.local._ZN2v88internalL30FLAGDEFAULT_trace_turbo_filterE,"aw"
	.align 8
	.type	_ZN2v88internalL30FLAGDEFAULT_trace_turbo_filterE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_trace_turbo_filterE, 8
_ZN2v88internalL30FLAGDEFAULT_trace_turbo_filterE:
	.quad	.LC958
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_trace_turbo_pathE,"a"
	.align 8
	.type	_ZN2v88internalL28FLAGDEFAULT_trace_turbo_pathE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_trace_turbo_pathE, 8
_ZN2v88internalL28FLAGDEFAULT_trace_turbo_pathE:
	.zero	8
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_trace_turboE,"a"
	.type	_ZN2v88internalL23FLAGDEFAULT_trace_turboE, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_trace_turboE, 1
_ZN2v88internalL23FLAGDEFAULT_trace_turboE:
	.zero	1
	.section	.data.rel.ro.local._ZN2v88internalL24FLAGDEFAULT_turbo_filterE,"aw"
	.align 8
	.type	_ZN2v88internalL24FLAGDEFAULT_turbo_filterE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_turbo_filterE, 8
_ZN2v88internalL24FLAGDEFAULT_turbo_filterE:
	.quad	.LC958
	.section	.rodata._ZN2v88internalL47FLAGDEFAULT_turbo_control_flow_aware_allocationE,"a"
	.type	_ZN2v88internalL47FLAGDEFAULT_turbo_control_flow_aware_allocationE, @object
	.size	_ZN2v88internalL47FLAGDEFAULT_turbo_control_flow_aware_allocationE, 1
_ZN2v88internalL47FLAGDEFAULT_turbo_control_flow_aware_allocationE:
	.zero	1
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_turbo_sp_frame_accessE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_turbo_sp_frame_accessE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_turbo_sp_frame_accessE, 1
_ZN2v88internalL33FLAGDEFAULT_turbo_sp_frame_accessE:
	.zero	1
	.section	.rodata._ZN2v88internalL15FLAGDEFAULT_optE,"a"
	.type	_ZN2v88internalL15FLAGDEFAULT_optE, @object
	.size	_ZN2v88internalL15FLAGDEFAULT_optE, 1
_ZN2v88internalL15FLAGDEFAULT_optE:
	.byte	1
	.section	.rodata._ZN2v88internalL30FLAGDEFAULT_print_deopt_stressE,"a"
	.type	_ZN2v88internalL30FLAGDEFAULT_print_deopt_stressE, @object
	.size	_ZN2v88internalL30FLAGDEFAULT_print_deopt_stressE, 1
_ZN2v88internalL30FLAGDEFAULT_print_deopt_stressE:
	.zero	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_deopt_every_n_timesE,"a"
	.align 4
	.type	_ZN2v88internalL31FLAGDEFAULT_deopt_every_n_timesE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_deopt_every_n_timesE, 4
_ZN2v88internalL31FLAGDEFAULT_deopt_every_n_timesE:
	.zero	4
	.section	.rodata._ZN2v88internalL23FLAGDEFAULT_stress_runsE,"a"
	.align 4
	.type	_ZN2v88internalL23FLAGDEFAULT_stress_runsE, @object
	.size	_ZN2v88internalL23FLAGDEFAULT_stress_runsE, 4
_ZN2v88internalL23FLAGDEFAULT_stress_runsE:
	.zero	4
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_trace_heap_brokerE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_trace_heap_brokerE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_trace_heap_brokerE, 1
_ZN2v88internalL29FLAGDEFAULT_trace_heap_brokerE:
	.zero	1
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_trace_heap_broker_verboseE,"a"
	.type	_ZN2v88internalL37FLAGDEFAULT_trace_heap_broker_verboseE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_trace_heap_broker_verboseE, 1
_ZN2v88internalL37FLAGDEFAULT_trace_heap_broker_verboseE:
	.zero	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_concurrent_inliningE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_concurrent_inliningE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_concurrent_inliningE, 1
_ZN2v88internalL31FLAGDEFAULT_concurrent_inliningE:
	.zero	1
	.section	.rodata._ZN2v88internalL42FLAGDEFAULT_block_concurrent_recompilationE,"a"
	.type	_ZN2v88internalL42FLAGDEFAULT_block_concurrent_recompilationE, @object
	.size	_ZN2v88internalL42FLAGDEFAULT_block_concurrent_recompilationE, 1
_ZN2v88internalL42FLAGDEFAULT_block_concurrent_recompilationE:
	.zero	1
	.section	.rodata._ZN2v88internalL42FLAGDEFAULT_concurrent_recompilation_delayE,"a"
	.align 4
	.type	_ZN2v88internalL42FLAGDEFAULT_concurrent_recompilation_delayE, @object
	.size	_ZN2v88internalL42FLAGDEFAULT_concurrent_recompilation_delayE, 4
_ZN2v88internalL42FLAGDEFAULT_concurrent_recompilation_delayE:
	.zero	4
	.section	.rodata._ZN2v88internalL49FLAGDEFAULT_concurrent_recompilation_queue_lengthE,"a"
	.align 4
	.type	_ZN2v88internalL49FLAGDEFAULT_concurrent_recompilation_queue_lengthE, @object
	.size	_ZN2v88internalL49FLAGDEFAULT_concurrent_recompilation_queue_lengthE, 4
_ZN2v88internalL49FLAGDEFAULT_concurrent_recompilation_queue_lengthE:
	.long	8
	.section	.rodata._ZN2v88internalL42FLAGDEFAULT_trace_concurrent_recompilationE,"a"
	.type	_ZN2v88internalL42FLAGDEFAULT_trace_concurrent_recompilationE, @object
	.size	_ZN2v88internalL42FLAGDEFAULT_trace_concurrent_recompilationE, 1
_ZN2v88internalL42FLAGDEFAULT_trace_concurrent_recompilationE:
	.zero	1
	.section	.rodata._ZN2v88internalL36FLAGDEFAULT_concurrent_recompilationE,"a"
	.type	_ZN2v88internalL36FLAGDEFAULT_concurrent_recompilationE, @object
	.size	_ZN2v88internalL36FLAGDEFAULT_concurrent_recompilationE, 1
_ZN2v88internalL36FLAGDEFAULT_concurrent_recompilationE:
	.byte	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_trace_generalizationE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_trace_generalizationE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_trace_generalizationE, 1
_ZN2v88internalL32FLAGDEFAULT_trace_generalizationE:
	.zero	1
	.section	.rodata._ZN2v88internalL27FLAGDEFAULT_trace_migrationE,"a"
	.type	_ZN2v88internalL27FLAGDEFAULT_trace_migrationE, @object
	.size	_ZN2v88internalL27FLAGDEFAULT_trace_migrationE, 1
_ZN2v88internalL27FLAGDEFAULT_trace_migrationE:
	.zero	1
	.section	.rodata._ZN2v88internalL40FLAGDEFAULT_trace_track_allocation_sitesE,"a"
	.type	_ZN2v88internalL40FLAGDEFAULT_trace_track_allocation_sitesE, @object
	.size	_ZN2v88internalL40FLAGDEFAULT_trace_track_allocation_sitesE, 1
_ZN2v88internalL40FLAGDEFAULT_trace_track_allocation_sitesE:
	.zero	1
	.section	.rodata._ZN2v88internalL21FLAGDEFAULT_fast_mathE,"a"
	.type	_ZN2v88internalL21FLAGDEFAULT_fast_mathE, @object
	.size	_ZN2v88internalL21FLAGDEFAULT_fast_mathE, 1
_ZN2v88internalL21FLAGDEFAULT_fast_mathE:
	.byte	1
	.section	.rodata._ZN2v88internalL49FLAGDEFAULT_trace_ignition_dispatches_output_fileE,"a"
	.align 8
	.type	_ZN2v88internalL49FLAGDEFAULT_trace_ignition_dispatches_output_fileE, @object
	.size	_ZN2v88internalL49FLAGDEFAULT_trace_ignition_dispatches_output_fileE, 8
_ZN2v88internalL49FLAGDEFAULT_trace_ignition_dispatches_output_fileE:
	.zero	8
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_trace_ignition_dispatchesE,"a"
	.type	_ZN2v88internalL37FLAGDEFAULT_trace_ignition_dispatchesE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_trace_ignition_dispatchesE, 1
_ZN2v88internalL37FLAGDEFAULT_trace_ignition_dispatchesE:
	.zero	1
	.section	.rodata._ZN2v88internalL34FLAGDEFAULT_trace_ignition_codegenE,"a"
	.type	_ZN2v88internalL34FLAGDEFAULT_trace_ignition_codegenE, @object
	.size	_ZN2v88internalL34FLAGDEFAULT_trace_ignition_codegenE, 1
_ZN2v88internalL34FLAGDEFAULT_trace_ignition_codegenE:
	.zero	1
	.section	.data.rel.ro.local._ZN2v88internalL33FLAGDEFAULT_print_bytecode_filterE,"aw"
	.align 8
	.type	_ZN2v88internalL33FLAGDEFAULT_print_bytecode_filterE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_print_bytecode_filterE, 8
_ZN2v88internalL33FLAGDEFAULT_print_bytecode_filterE:
	.quad	.LC958
	.section	.rodata._ZN2v88internalL40FLAGDEFAULT_stress_lazy_source_positionsE,"a"
	.type	_ZN2v88internalL40FLAGDEFAULT_stress_lazy_source_positionsE, @object
	.size	_ZN2v88internalL40FLAGDEFAULT_stress_lazy_source_positionsE, 1
_ZN2v88internalL40FLAGDEFAULT_stress_lazy_source_positionsE:
	.zero	1
	.section	.rodata._ZN2v88internalL40FLAGDEFAULT_enable_lazy_source_positionsE,"a"
	.type	_ZN2v88internalL40FLAGDEFAULT_enable_lazy_source_positionsE, @object
	.size	_ZN2v88internalL40FLAGDEFAULT_enable_lazy_source_positionsE, 1
_ZN2v88internalL40FLAGDEFAULT_enable_lazy_source_positionsE:
	.byte	1
	.section	.rodata._ZN2v88internalL26FLAGDEFAULT_print_bytecodeE,"a"
	.type	_ZN2v88internalL26FLAGDEFAULT_print_bytecodeE, @object
	.size	_ZN2v88internalL26FLAGDEFAULT_print_bytecodeE, 1
_ZN2v88internalL26FLAGDEFAULT_print_bytecodeE:
	.zero	1
	.section	.rodata._ZN2v88internalL50FLAGDEFAULT_ignition_share_named_property_feedbackE,"a"
	.type	_ZN2v88internalL50FLAGDEFAULT_ignition_share_named_property_feedbackE, @object
	.size	_ZN2v88internalL50FLAGDEFAULT_ignition_share_named_property_feedbackE, 1
_ZN2v88internalL50FLAGDEFAULT_ignition_share_named_property_feedbackE:
	.byte	1
	.section	.rodata._ZN2v88internalL48FLAGDEFAULT_ignition_filter_expression_positionsE,"a"
	.type	_ZN2v88internalL48FLAGDEFAULT_ignition_filter_expression_positionsE, @object
	.size	_ZN2v88internalL48FLAGDEFAULT_ignition_filter_expression_positionsE, 1
_ZN2v88internalL48FLAGDEFAULT_ignition_filter_expression_positionsE:
	.byte	1
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_ignition_reoE,"a"
	.type	_ZN2v88internalL24FLAGDEFAULT_ignition_reoE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_ignition_reoE, 1
_ZN2v88internalL24FLAGDEFAULT_ignition_reoE:
	.byte	1
	.section	.rodata._ZN2v88internalL49FLAGDEFAULT_ignition_elide_noneffectful_bytecodesE,"a"
	.type	_ZN2v88internalL49FLAGDEFAULT_ignition_elide_noneffectful_bytecodesE, @object
	.size	_ZN2v88internalL49FLAGDEFAULT_ignition_elide_noneffectful_bytecodesE, 1
_ZN2v88internalL49FLAGDEFAULT_ignition_elide_noneffectful_bytecodesE:
	.byte	1
	.section	.rodata._ZN2v88internalL36FLAGDEFAULT_lazy_feedback_allocationE,"a"
	.type	_ZN2v88internalL36FLAGDEFAULT_lazy_feedback_allocationE, @object
	.size	_ZN2v88internalL36FLAGDEFAULT_lazy_feedback_allocationE, 1
_ZN2v88internalL36FLAGDEFAULT_lazy_feedback_allocationE:
	.byte	1
	.section	.rodata._ZN2v88internalL49FLAGDEFAULT_budget_for_feedback_vector_allocationE,"a"
	.align 4
	.type	_ZN2v88internalL49FLAGDEFAULT_budget_for_feedback_vector_allocationE, @object
	.size	_ZN2v88internalL49FLAGDEFAULT_budget_for_feedback_vector_allocationE, 4
_ZN2v88internalL49FLAGDEFAULT_budget_for_feedback_vector_allocationE:
	.long	1024
	.section	.rodata._ZN2v88internalL18FLAGDEFAULT_use_icE,"a"
	.type	_ZN2v88internalL18FLAGDEFAULT_use_icE, @object
	.size	_ZN2v88internalL18FLAGDEFAULT_use_icE, 1
_ZN2v88internalL18FLAGDEFAULT_use_icE:
	.byte	1
	.section	.rodata._ZN2v88internalL19FLAGDEFAULT_jitlessE,"a"
	.type	_ZN2v88internalL19FLAGDEFAULT_jitlessE, @object
	.size	_ZN2v88internalL19FLAGDEFAULT_jitlessE, 1
_ZN2v88internalL19FLAGDEFAULT_jitlessE:
	.zero	1
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_interrupt_budgetE,"a"
	.align 4
	.type	_ZN2v88internalL28FLAGDEFAULT_interrupt_budgetE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_interrupt_budgetE, 4
_ZN2v88internalL28FLAGDEFAULT_interrupt_budgetE:
	.long	147456
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_unbox_double_arraysE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_unbox_double_arraysE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_unbox_double_arraysE, 1
_ZN2v88internalL31FLAGDEFAULT_unbox_double_arraysE:
	.byte	1
	.section	.rodata._ZN2v88internalL40FLAGDEFAULT_enable_one_shot_optimizationE,"a"
	.type	_ZN2v88internalL40FLAGDEFAULT_enable_one_shot_optimizationE, @object
	.size	_ZN2v88internalL40FLAGDEFAULT_enable_one_shot_optimizationE, 1
_ZN2v88internalL40FLAGDEFAULT_enable_one_shot_optimizationE:
	.byte	1
	.section	.rodata._ZN2v88internalL34FLAGDEFAULT_feedback_normalizationE,"a"
	.type	_ZN2v88internalL34FLAGDEFAULT_feedback_normalizationE, @object
	.size	_ZN2v88internalL34FLAGDEFAULT_feedback_normalizationE, 1
_ZN2v88internalL34FLAGDEFAULT_feedback_normalizationE:
	.zero	1
	.section	.rodata._ZN2v88internalL40FLAGDEFAULT_trace_protector_invalidationE,"a"
	.type	_ZN2v88internalL40FLAGDEFAULT_trace_protector_invalidationE, @object
	.size	_ZN2v88internalL40FLAGDEFAULT_trace_protector_invalidationE, 1
_ZN2v88internalL40FLAGDEFAULT_trace_protector_invalidationE:
	.zero	1
	.section	.rodata._ZN2v88internalL32FLAGDEFAULT_trace_block_coverageE,"a"
	.type	_ZN2v88internalL32FLAGDEFAULT_trace_block_coverageE, @object
	.size	_ZN2v88internalL32FLAGDEFAULT_trace_block_coverageE, 1
_ZN2v88internalL32FLAGDEFAULT_trace_block_coverageE:
	.zero	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_track_field_typesE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_track_field_typesE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_track_field_typesE, 1
_ZN2v88internalL29FLAGDEFAULT_track_field_typesE:
	.byte	1
	.section	.rodata._ZN2v88internalL33FLAGDEFAULT_track_computed_fieldsE,"a"
	.type	_ZN2v88internalL33FLAGDEFAULT_track_computed_fieldsE, @object
	.size	_ZN2v88internalL33FLAGDEFAULT_track_computed_fieldsE, 1
_ZN2v88internalL33FLAGDEFAULT_track_computed_fieldsE:
	.byte	1
	.section	.rodata._ZN2v88internalL36FLAGDEFAULT_track_heap_object_fieldsE,"a"
	.type	_ZN2v88internalL36FLAGDEFAULT_track_heap_object_fieldsE, @object
	.size	_ZN2v88internalL36FLAGDEFAULT_track_heap_object_fieldsE, 1
_ZN2v88internalL36FLAGDEFAULT_track_heap_object_fieldsE:
	.byte	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_track_double_fieldsE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_track_double_fieldsE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_track_double_fieldsE, 1
_ZN2v88internalL31FLAGDEFAULT_track_double_fieldsE:
	.byte	1
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_track_fieldsE,"a"
	.type	_ZN2v88internalL24FLAGDEFAULT_track_fieldsE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_track_fieldsE, 1
_ZN2v88internalL24FLAGDEFAULT_track_fieldsE:
	.byte	1
	.section	.rodata._ZN2v88internalL40FLAGDEFAULT_trace_pretenuring_statisticsE,"a"
	.type	_ZN2v88internalL40FLAGDEFAULT_trace_pretenuring_statisticsE, @object
	.size	_ZN2v88internalL40FLAGDEFAULT_trace_pretenuring_statisticsE, 1
_ZN2v88internalL40FLAGDEFAULT_trace_pretenuring_statisticsE:
	.zero	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_trace_pretenuringE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_trace_pretenuringE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_trace_pretenuringE, 1
_ZN2v88internalL29FLAGDEFAULT_trace_pretenuringE:
	.zero	1
	.section	.rodata._ZN2v88internalL36FLAGDEFAULT_page_promotion_thresholdE,"a"
	.align 4
	.type	_ZN2v88internalL36FLAGDEFAULT_page_promotion_thresholdE, @object
	.size	_ZN2v88internalL36FLAGDEFAULT_page_promotion_thresholdE, 4
_ZN2v88internalL36FLAGDEFAULT_page_promotion_thresholdE:
	.long	70
	.section	.rodata._ZN2v88internalL26FLAGDEFAULT_page_promotionE,"a"
	.type	_ZN2v88internalL26FLAGDEFAULT_page_promotionE, @object
	.size	_ZN2v88internalL26FLAGDEFAULT_page_promotionE, 1
_ZN2v88internalL26FLAGDEFAULT_page_promotionE:
	.byte	1
	.section	.rodata._ZN2v88internalL39FLAGDEFAULT_allocation_site_pretenuringE,"a"
	.type	_ZN2v88internalL39FLAGDEFAULT_allocation_site_pretenuringE, @object
	.size	_ZN2v88internalL39FLAGDEFAULT_allocation_site_pretenuringE, 1
_ZN2v88internalL39FLAGDEFAULT_allocation_site_pretenuringE:
	.byte	1
	.section	.rodata._ZN2v88internalL24FLAGDEFAULT_assert_typesE,"a"
	.type	_ZN2v88internalL24FLAGDEFAULT_assert_typesE, @object
	.size	_ZN2v88internalL24FLAGDEFAULT_assert_typesE, 1
_ZN2v88internalL24FLAGDEFAULT_assert_typesE:
	.zero	1
	.section	.rodata._ZN2v88internalL18FLAGDEFAULT_futureE,"a"
	.type	_ZN2v88internalL18FLAGDEFAULT_futureE, @object
	.size	_ZN2v88internalL18FLAGDEFAULT_futureE, 1
_ZN2v88internalL18FLAGDEFAULT_futureE:
	.zero	1
	.section	.rodata._ZN2v88internalL21FLAGDEFAULT_lite_modeE,"a"
	.type	_ZN2v88internalL21FLAGDEFAULT_lite_modeE, @object
	.size	_ZN2v88internalL21FLAGDEFAULT_lite_modeE, 1
_ZN2v88internalL21FLAGDEFAULT_lite_modeE:
	.zero	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_icu_timezone_dataE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_icu_timezone_dataE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_icu_timezone_dataE, 1
_ZN2v88internalL29FLAGDEFAULT_icu_timezone_dataE:
	.byte	1
	.section	.rodata._ZN2v88internalL45FLAGDEFAULT_harmony_intl_numberformat_unifiedE,"a"
	.type	_ZN2v88internalL45FLAGDEFAULT_harmony_intl_numberformat_unifiedE, @object
	.size	_ZN2v88internalL45FLAGDEFAULT_harmony_intl_numberformat_unifiedE, 1
_ZN2v88internalL45FLAGDEFAULT_harmony_intl_numberformat_unifiedE:
	.byte	1
	.section	.rodata._ZN2v88internalL39FLAGDEFAULT_harmony_intl_datetime_styleE,"a"
	.type	_ZN2v88internalL39FLAGDEFAULT_harmony_intl_datetime_styleE, @object
	.size	_ZN2v88internalL39FLAGDEFAULT_harmony_intl_datetime_styleE, 1
_ZN2v88internalL39FLAGDEFAULT_harmony_intl_datetime_styleE:
	.byte	1
	.section	.rodata._ZN2v88internalL42FLAGDEFAULT_harmony_intl_date_format_rangeE,"a"
	.type	_ZN2v88internalL42FLAGDEFAULT_harmony_intl_date_format_rangeE, @object
	.size	_ZN2v88internalL42FLAGDEFAULT_harmony_intl_date_format_rangeE, 1
_ZN2v88internalL42FLAGDEFAULT_harmony_intl_date_format_rangeE:
	.byte	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_harmony_intl_bigintE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_harmony_intl_bigintE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_harmony_intl_bigintE, 1
_ZN2v88internalL31FLAGDEFAULT_harmony_intl_bigintE:
	.byte	1
	.section	.rodata._ZN2v88internalL39FLAGDEFAULT_harmony_promise_all_settledE,"a"
	.type	_ZN2v88internalL39FLAGDEFAULT_harmony_promise_all_settledE, @object
	.size	_ZN2v88internalL39FLAGDEFAULT_harmony_promise_all_settledE, 1
_ZN2v88internalL39FLAGDEFAULT_harmony_promise_all_settledE:
	.byte	1
	.section	.rodata._ZN2v88internalL34FLAGDEFAULT_harmony_dynamic_importE,"a"
	.type	_ZN2v88internalL34FLAGDEFAULT_harmony_dynamic_importE, @object
	.size	_ZN2v88internalL34FLAGDEFAULT_harmony_dynamic_importE, 1
_ZN2v88internalL34FLAGDEFAULT_harmony_dynamic_importE:
	.byte	1
	.section	.rodata._ZN2v88internalL31FLAGDEFAULT_harmony_import_metaE,"a"
	.type	_ZN2v88internalL31FLAGDEFAULT_harmony_import_metaE, @object
	.size	_ZN2v88internalL31FLAGDEFAULT_harmony_import_metaE, 1
_ZN2v88internalL31FLAGDEFAULT_harmony_import_metaE:
	.byte	1
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_harmony_sharedarraybufferE,"a"
	.type	_ZN2v88internalL37FLAGDEFAULT_harmony_sharedarraybufferE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_harmony_sharedarraybufferE, 1
_ZN2v88internalL37FLAGDEFAULT_harmony_sharedarraybufferE:
	.byte	1
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_harmony_namespace_exportsE,"a"
	.type	_ZN2v88internalL37FLAGDEFAULT_harmony_namespace_exportsE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_harmony_namespace_exportsE, 1
_ZN2v88internalL37FLAGDEFAULT_harmony_namespace_exportsE:
	.byte	1
	.section	.rodata._ZN2v88internalL34FLAGDEFAULT_harmony_intl_segmenterE,"a"
	.type	_ZN2v88internalL34FLAGDEFAULT_harmony_intl_segmenterE, @object
	.size	_ZN2v88internalL34FLAGDEFAULT_harmony_intl_segmenterE, 1
_ZN2v88internalL34FLAGDEFAULT_harmony_intl_segmenterE:
	.zero	1
	.section	.rodata._ZN2v88internalL60FLAGDEFAULT_harmony_intl_dateformat_fractional_second_digitsE,"a"
	.type	_ZN2v88internalL60FLAGDEFAULT_harmony_intl_dateformat_fractional_second_digitsE, @object
	.size	_ZN2v88internalL60FLAGDEFAULT_harmony_intl_dateformat_fractional_second_digitsE, 1
_ZN2v88internalL60FLAGDEFAULT_harmony_intl_dateformat_fractional_second_digitsE:
	.zero	1
	.section	.rodata._ZN2v88internalL46FLAGDEFAULT_harmony_intl_dateformat_day_periodE,"a"
	.type	_ZN2v88internalL46FLAGDEFAULT_harmony_intl_dateformat_day_periodE, @object
	.size	_ZN2v88internalL46FLAGDEFAULT_harmony_intl_dateformat_day_periodE, 1
_ZN2v88internalL46FLAGDEFAULT_harmony_intl_dateformat_day_periodE:
	.zero	1
	.section	.rodata._ZN2v88internalL54FLAGDEFAULT_harmony_intl_add_calendar_numbering_systemE,"a"
	.type	_ZN2v88internalL54FLAGDEFAULT_harmony_intl_add_calendar_numbering_systemE, @object
	.size	_ZN2v88internalL54FLAGDEFAULT_harmony_intl_add_calendar_numbering_systemE, 1
_ZN2v88internalL54FLAGDEFAULT_harmony_intl_add_calendar_numbering_systemE:
	.zero	1
	.section	.rodata._ZN2v88internalL43FLAGDEFAULT_harmony_intl_dateformat_quarterE,"a"
	.type	_ZN2v88internalL43FLAGDEFAULT_harmony_intl_dateformat_quarterE, @object
	.size	_ZN2v88internalL43FLAGDEFAULT_harmony_intl_dateformat_quarterE, 1
_ZN2v88internalL43FLAGDEFAULT_harmony_intl_dateformat_quarterE:
	.zero	1
	.section	.rodata._ZN2v88internalL27FLAGDEFAULT_harmony_nullishE,"a"
	.type	_ZN2v88internalL27FLAGDEFAULT_harmony_nullishE, @object
	.size	_ZN2v88internalL27FLAGDEFAULT_harmony_nullishE, 1
_ZN2v88internalL27FLAGDEFAULT_harmony_nullishE:
	.zero	1
	.section	.rodata._ZN2v88internalL37FLAGDEFAULT_harmony_optional_chainingE,"a"
	.type	_ZN2v88internalL37FLAGDEFAULT_harmony_optional_chainingE, @object
	.size	_ZN2v88internalL37FLAGDEFAULT_harmony_optional_chainingE, 1
_ZN2v88internalL37FLAGDEFAULT_harmony_optional_chainingE:
	.zero	1
	.section	.rodata._ZN2v88internalL29FLAGDEFAULT_harmony_weak_refsE,"a"
	.type	_ZN2v88internalL29FLAGDEFAULT_harmony_weak_refsE, @object
	.size	_ZN2v88internalL29FLAGDEFAULT_harmony_weak_refsE, 1
_ZN2v88internalL29FLAGDEFAULT_harmony_weak_refsE:
	.zero	1
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_harmony_regexp_sequenceE,"a"
	.type	_ZN2v88internalL35FLAGDEFAULT_harmony_regexp_sequenceE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_harmony_regexp_sequenceE, 1
_ZN2v88internalL35FLAGDEFAULT_harmony_regexp_sequenceE:
	.zero	1
	.section	.rodata._ZN2v88internalL35FLAGDEFAULT_harmony_private_methodsE,"a"
	.type	_ZN2v88internalL35FLAGDEFAULT_harmony_private_methodsE, @object
	.size	_ZN2v88internalL35FLAGDEFAULT_harmony_private_methodsE, 1
_ZN2v88internalL35FLAGDEFAULT_harmony_private_methodsE:
	.zero	1
	.section	.rodata._ZN2v88internalL28FLAGDEFAULT_harmony_shippingE,"a"
	.type	_ZN2v88internalL28FLAGDEFAULT_harmony_shippingE, @object
	.size	_ZN2v88internalL28FLAGDEFAULT_harmony_shippingE, 1
_ZN2v88internalL28FLAGDEFAULT_harmony_shippingE:
	.byte	1
	.section	.rodata._ZN2v88internalL19FLAGDEFAULT_harmonyE,"a"
	.type	_ZN2v88internalL19FLAGDEFAULT_harmonyE, @object
	.size	_ZN2v88internalL19FLAGDEFAULT_harmonyE, 1
_ZN2v88internalL19FLAGDEFAULT_harmonyE:
	.zero	1
	.section	.rodata._ZN2v88internalL22FLAGDEFAULT_es_stagingE,"a"
	.type	_ZN2v88internalL22FLAGDEFAULT_es_stagingE, @object
	.size	_ZN2v88internalL22FLAGDEFAULT_es_stagingE, 1
_ZN2v88internalL22FLAGDEFAULT_es_stagingE:
	.zero	1
	.section	.rodata._ZN2v88internalL22FLAGDEFAULT_use_strictE,"a"
	.type	_ZN2v88internalL22FLAGDEFAULT_use_strictE, @object
	.size	_ZN2v88internalL22FLAGDEFAULT_use_strictE, 1
_ZN2v88internalL22FLAGDEFAULT_use_strictE:
	.zero	1
	.globl	_ZN2v88internal23FLAG_single_threaded_gcE
	.section	.bss._ZN2v88internal23FLAG_single_threaded_gcE,"aw",@nobits
	.type	_ZN2v88internal23FLAG_single_threaded_gcE, @object
	.size	_ZN2v88internal23FLAG_single_threaded_gcE, 1
_ZN2v88internal23FLAG_single_threaded_gcE:
	.zero	1
	.globl	_ZN2v88internal20FLAG_single_threadedE
	.section	.bss._ZN2v88internal20FLAG_single_threadedE,"aw",@nobits
	.type	_ZN2v88internal20FLAG_single_threadedE, @object
	.size	_ZN2v88internal20FLAG_single_threadedE, 1
_ZN2v88internal20FLAG_single_threadedE:
	.zero	1
	.globl	_ZN2v88internal28FLAG_predictable_gc_scheduleE
	.section	.bss._ZN2v88internal28FLAG_predictable_gc_scheduleE,"aw",@nobits
	.type	_ZN2v88internal28FLAG_predictable_gc_scheduleE, @object
	.size	_ZN2v88internal28FLAG_predictable_gc_scheduleE, 1
_ZN2v88internal28FLAG_predictable_gc_scheduleE:
	.zero	1
	.globl	_ZN2v88internal16FLAG_predictableE
	.section	.bss._ZN2v88internal16FLAG_predictableE,"aw",@nobits
	.type	_ZN2v88internal16FLAG_predictableE, @object
	.size	_ZN2v88internal16FLAG_predictableE, 1
_ZN2v88internal16FLAG_predictableE:
	.zero	1
	.globl	_ZN2v88internal19FLAG_print_all_codeE
	.section	.bss._ZN2v88internal19FLAG_print_all_codeE,"aw",@nobits
	.type	_ZN2v88internal19FLAG_print_all_codeE, @object
	.size	_ZN2v88internal19FLAG_print_all_codeE, 1
_ZN2v88internal19FLAG_print_all_codeE:
	.zero	1
	.globl	_ZN2v88internal11FLAG_sodiumE
	.section	.bss._ZN2v88internal11FLAG_sodiumE,"aw",@nobits
	.type	_ZN2v88internal11FLAG_sodiumE, @object
	.size	_ZN2v88internal11FLAG_sodiumE, 1
_ZN2v88internal11FLAG_sodiumE:
	.zero	1
	.globl	_ZN2v88internal23FLAG_print_builtin_sizeE
	.section	.bss._ZN2v88internal23FLAG_print_builtin_sizeE,"aw",@nobits
	.type	_ZN2v88internal23FLAG_print_builtin_sizeE, @object
	.size	_ZN2v88internal23FLAG_print_builtin_sizeE, 1
_ZN2v88internal23FLAG_print_builtin_sizeE:
	.zero	1
	.globl	_ZN2v88internal26FLAG_print_regexp_bytecodeE
	.section	.bss._ZN2v88internal26FLAG_print_regexp_bytecodeE,"aw",@nobits
	.type	_ZN2v88internal26FLAG_print_regexp_bytecodeE, @object
	.size	_ZN2v88internal26FLAG_print_regexp_bytecodeE, 1
_ZN2v88internal26FLAG_print_regexp_bytecodeE:
	.zero	1
	.globl	_ZN2v88internal22FLAG_print_regexp_codeE
	.section	.bss._ZN2v88internal22FLAG_print_regexp_codeE,"aw",@nobits
	.type	_ZN2v88internal22FLAG_print_regexp_codeE, @object
	.size	_ZN2v88internal22FLAG_print_regexp_codeE, 1
_ZN2v88internal22FLAG_print_regexp_codeE:
	.zero	1
	.globl	_ZN2v88internal30FLAG_print_builtin_code_filterE
	.section	.data.rel.local._ZN2v88internal30FLAG_print_builtin_code_filterE,"aw"
	.align 8
	.type	_ZN2v88internal30FLAG_print_builtin_code_filterE, @object
	.size	_ZN2v88internal30FLAG_print_builtin_code_filterE, 8
_ZN2v88internal30FLAG_print_builtin_code_filterE:
	.quad	.LC958
	.globl	_ZN2v88internal23FLAG_print_builtin_codeE
	.section	.bss._ZN2v88internal23FLAG_print_builtin_codeE,"aw",@nobits
	.type	_ZN2v88internal23FLAG_print_builtin_codeE, @object
	.size	_ZN2v88internal23FLAG_print_builtin_codeE, 1
_ZN2v88internal23FLAG_print_builtin_codeE:
	.zero	1
	.globl	_ZN2v88internal23FLAG_print_code_verboseE
	.section	.bss._ZN2v88internal23FLAG_print_code_verboseE,"aw",@nobits
	.type	_ZN2v88internal23FLAG_print_code_verboseE, @object
	.size	_ZN2v88internal23FLAG_print_code_verboseE, 1
_ZN2v88internal23FLAG_print_code_verboseE:
	.zero	1
	.globl	_ZN2v88internal26FLAG_print_opt_code_filterE
	.section	.data.rel.local._ZN2v88internal26FLAG_print_opt_code_filterE,"aw"
	.align 8
	.type	_ZN2v88internal26FLAG_print_opt_code_filterE, @object
	.size	_ZN2v88internal26FLAG_print_opt_code_filterE, 8
_ZN2v88internal26FLAG_print_opt_code_filterE:
	.quad	.LC958
	.globl	_ZN2v88internal19FLAG_print_opt_codeE
	.section	.bss._ZN2v88internal19FLAG_print_opt_codeE,"aw",@nobits
	.type	_ZN2v88internal19FLAG_print_opt_codeE, @object
	.size	_ZN2v88internal19FLAG_print_opt_codeE, 1
_ZN2v88internal19FLAG_print_opt_codeE:
	.zero	1
	.globl	_ZN2v88internal15FLAG_print_codeE
	.section	.bss._ZN2v88internal15FLAG_print_codeE,"aw",@nobits
	.type	_ZN2v88internal15FLAG_print_codeE, @object
	.size	_ZN2v88internal15FLAG_print_codeE, 1
_ZN2v88internal15FLAG_print_codeE:
	.zero	1
	.globl	_ZN2v88internal36FLAG_trace_creation_allocation_sitesE
	.section	.bss._ZN2v88internal36FLAG_trace_creation_allocation_sitesE,"aw",@nobits
	.type	_ZN2v88internal36FLAG_trace_creation_allocation_sitesE, @object
	.size	_ZN2v88internal36FLAG_trace_creation_allocation_sitesE, 1
_ZN2v88internal36FLAG_trace_creation_allocation_sitesE:
	.zero	1
	.globl	_ZN2v88internal31FLAG_trace_elements_transitionsE
	.section	.bss._ZN2v88internal31FLAG_trace_elements_transitionsE,"aw",@nobits
	.type	_ZN2v88internal31FLAG_trace_elements_transitionsE, @object
	.size	_ZN2v88internal31FLAG_trace_elements_transitionsE, 1
_ZN2v88internal31FLAG_trace_elements_transitionsE:
	.zero	1
	.globl	_ZN2v88internal36FLAG_interpreted_frames_native_stackE
	.section	.bss._ZN2v88internal36FLAG_interpreted_frames_native_stackE,"aw",@nobits
	.type	_ZN2v88internal36FLAG_interpreted_frames_native_stackE, @object
	.size	_ZN2v88internal36FLAG_interpreted_frames_native_stackE, 1
_ZN2v88internal36FLAG_interpreted_frames_native_stackE:
	.zero	1
	.globl	_ZN2v88internal25FLAG_win64_unwinding_infoE
	.section	.data._ZN2v88internal25FLAG_win64_unwinding_infoE,"aw"
	.type	_ZN2v88internal25FLAG_win64_unwinding_infoE, @object
	.size	_ZN2v88internal25FLAG_win64_unwinding_infoE, 1
_ZN2v88internal25FLAG_win64_unwinding_infoE:
	.byte	1
	.globl	_ZN2v88internal21FLAG_print_opt_sourceE
	.section	.bss._ZN2v88internal21FLAG_print_opt_sourceE,"aw",@nobits
	.type	_ZN2v88internal21FLAG_print_opt_sourceE, @object
	.size	_ZN2v88internal21FLAG_print_opt_sourceE, 1
_ZN2v88internal21FLAG_print_opt_sourceE:
	.zero	1
	.globl	_ZN2v88internal28FLAG_redirect_code_traces_toE
	.section	.bss._ZN2v88internal28FLAG_redirect_code_traces_toE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal28FLAG_redirect_code_traces_toE, @object
	.size	_ZN2v88internal28FLAG_redirect_code_traces_toE, 8
_ZN2v88internal28FLAG_redirect_code_traces_toE:
	.zero	8
	.globl	_ZN2v88internal25FLAG_redirect_code_tracesE
	.section	.bss._ZN2v88internal25FLAG_redirect_code_tracesE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_redirect_code_tracesE, @object
	.size	_ZN2v88internal25FLAG_redirect_code_tracesE, 1
_ZN2v88internal25FLAG_redirect_code_tracesE:
	.zero	1
	.globl	_ZN2v88internal27FLAG_log_instruction_periodE
	.section	.data._ZN2v88internal27FLAG_log_instruction_periodE,"aw"
	.align 4
	.type	_ZN2v88internal27FLAG_log_instruction_periodE, @object
	.size	_ZN2v88internal27FLAG_log_instruction_periodE, 4
_ZN2v88internal27FLAG_log_instruction_periodE:
	.long	4194304
	.globl	_ZN2v88internal25FLAG_log_instruction_fileE
	.section	.data.rel.local._ZN2v88internal25FLAG_log_instruction_fileE,"aw"
	.align 8
	.type	_ZN2v88internal25FLAG_log_instruction_fileE, @object
	.size	_ZN2v88internal25FLAG_log_instruction_fileE, 8
_ZN2v88internal25FLAG_log_instruction_fileE:
	.quad	.LC959
	.globl	_ZN2v88internal26FLAG_log_instruction_statsE
	.section	.bss._ZN2v88internal26FLAG_log_instruction_statsE,"aw",@nobits
	.type	_ZN2v88internal26FLAG_log_instruction_statsE, @object
	.size	_ZN2v88internal26FLAG_log_instruction_statsE, 1
_ZN2v88internal26FLAG_log_instruction_statsE:
	.zero	1
	.globl	_ZN2v88internal30FLAG_log_internal_timer_eventsE
	.section	.bss._ZN2v88internal30FLAG_log_internal_timer_eventsE,"aw",@nobits
	.type	_ZN2v88internal30FLAG_log_internal_timer_eventsE, @object
	.size	_ZN2v88internal30FLAG_log_internal_timer_eventsE, 1
_ZN2v88internal30FLAG_log_internal_timer_eventsE:
	.zero	1
	.globl	_ZN2v88internal17FLAG_gc_fake_mmapE
	.section	.data.rel.local._ZN2v88internal17FLAG_gc_fake_mmapE,"aw"
	.align 8
	.type	_ZN2v88internal17FLAG_gc_fake_mmapE, @object
	.size	_ZN2v88internal17FLAG_gc_fake_mmapE, 8
_ZN2v88internal17FLAG_gc_fake_mmapE:
	.quad	.LC960
	.globl	_ZN2v88internal29FLAG_perf_prof_unwinding_infoE
	.section	.bss._ZN2v88internal29FLAG_perf_prof_unwinding_infoE,"aw",@nobits
	.type	_ZN2v88internal29FLAG_perf_prof_unwinding_infoE, @object
	.size	_ZN2v88internal29FLAG_perf_prof_unwinding_infoE, 1
_ZN2v88internal29FLAG_perf_prof_unwinding_infoE:
	.zero	1
	.globl	_ZN2v88internal28FLAG_perf_prof_annotate_wasmE
	.section	.bss._ZN2v88internal28FLAG_perf_prof_annotate_wasmE,"aw",@nobits
	.type	_ZN2v88internal28FLAG_perf_prof_annotate_wasmE, @object
	.size	_ZN2v88internal28FLAG_perf_prof_annotate_wasmE, 1
_ZN2v88internal28FLAG_perf_prof_annotate_wasmE:
	.zero	1
	.globl	_ZN2v88internal14FLAG_perf_profE
	.section	.bss._ZN2v88internal14FLAG_perf_profE,"aw",@nobits
	.type	_ZN2v88internal14FLAG_perf_profE, @object
	.size	_ZN2v88internal14FLAG_perf_profE, 1
_ZN2v88internal14FLAG_perf_profE:
	.zero	1
	.globl	_ZN2v88internal35FLAG_perf_basic_prof_only_functionsE
	.section	.bss._ZN2v88internal35FLAG_perf_basic_prof_only_functionsE,"aw",@nobits
	.type	_ZN2v88internal35FLAG_perf_basic_prof_only_functionsE, @object
	.size	_ZN2v88internal35FLAG_perf_basic_prof_only_functionsE, 1
_ZN2v88internal35FLAG_perf_basic_prof_only_functionsE:
	.zero	1
	.globl	_ZN2v88internal20FLAG_perf_basic_profE
	.section	.bss._ZN2v88internal20FLAG_perf_basic_profE,"aw",@nobits
	.type	_ZN2v88internal20FLAG_perf_basic_profE, @object
	.size	_ZN2v88internal20FLAG_perf_basic_profE, 1
_ZN2v88internal20FLAG_perf_basic_profE:
	.zero	1
	.globl	_ZN2v88internal12FLAG_ll_profE
	.section	.bss._ZN2v88internal12FLAG_ll_profE,"aw",@nobits
	.type	_ZN2v88internal12FLAG_ll_profE, @object
	.size	_ZN2v88internal12FLAG_ll_profE, 1
_ZN2v88internal12FLAG_ll_profE:
	.zero	1
	.globl	_ZN2v88internal24FLAG_logfile_per_isolateE
	.section	.data._ZN2v88internal24FLAG_logfile_per_isolateE,"aw"
	.type	_ZN2v88internal24FLAG_logfile_per_isolateE, @object
	.size	_ZN2v88internal24FLAG_logfile_per_isolateE, 1
_ZN2v88internal24FLAG_logfile_per_isolateE:
	.byte	1
	.globl	_ZN2v88internal12FLAG_logfileE
	.section	.data.rel.local._ZN2v88internal12FLAG_logfileE,"aw"
	.align 8
	.type	_ZN2v88internal12FLAG_logfileE, @object
	.size	_ZN2v88internal12FLAG_logfileE, 8
_ZN2v88internal12FLAG_logfileE:
	.quad	.LC961
	.globl	_ZN2v88internal22FLAG_prof_browser_modeE
	.section	.data._ZN2v88internal22FLAG_prof_browser_modeE,"aw"
	.type	_ZN2v88internal22FLAG_prof_browser_modeE, @object
	.size	_ZN2v88internal22FLAG_prof_browser_modeE, 1
_ZN2v88internal22FLAG_prof_browser_modeE:
	.byte	1
	.globl	_ZN2v88internal13FLAG_prof_cppE
	.section	.bss._ZN2v88internal13FLAG_prof_cppE,"aw",@nobits
	.type	_ZN2v88internal13FLAG_prof_cppE, @object
	.size	_ZN2v88internal13FLAG_prof_cppE, 1
_ZN2v88internal13FLAG_prof_cppE:
	.zero	1
	.globl	_ZN2v88internal27FLAG_prof_sampling_intervalE
	.section	.data._ZN2v88internal27FLAG_prof_sampling_intervalE,"aw"
	.align 4
	.type	_ZN2v88internal27FLAG_prof_sampling_intervalE, @object
	.size	_ZN2v88internal27FLAG_prof_sampling_intervalE, 4
_ZN2v88internal27FLAG_prof_sampling_intervalE:
	.long	1000
	.globl	_ZN2v88internal23FLAG_detailed_line_infoE
	.section	.bss._ZN2v88internal23FLAG_detailed_line_infoE,"aw",@nobits
	.type	_ZN2v88internal23FLAG_detailed_line_infoE, @object
	.size	_ZN2v88internal23FLAG_detailed_line_infoE, 1
_ZN2v88internal23FLAG_detailed_line_infoE:
	.zero	1
	.globl	_ZN2v88internal9FLAG_profE
	.section	.bss._ZN2v88internal9FLAG_profE,"aw",@nobits
	.type	_ZN2v88internal9FLAG_profE, @object
	.size	_ZN2v88internal9FLAG_profE, 1
_ZN2v88internal9FLAG_profE:
	.zero	1
	.globl	_ZN2v88internal24FLAG_log_function_eventsE
	.section	.bss._ZN2v88internal24FLAG_log_function_eventsE,"aw",@nobits
	.type	_ZN2v88internal24FLAG_log_function_eventsE, @object
	.size	_ZN2v88internal24FLAG_log_function_eventsE, 1
_ZN2v88internal24FLAG_log_function_eventsE:
	.zero	1
	.globl	_ZN2v88internal20FLAG_log_source_codeE
	.section	.bss._ZN2v88internal20FLAG_log_source_codeE,"aw",@nobits
	.type	_ZN2v88internal20FLAG_log_source_codeE, @object
	.size	_ZN2v88internal20FLAG_log_source_codeE, 1
_ZN2v88internal20FLAG_log_source_codeE:
	.zero	1
	.globl	_ZN2v88internal16FLAG_log_suspectE
	.section	.bss._ZN2v88internal16FLAG_log_suspectE,"aw",@nobits
	.type	_ZN2v88internal16FLAG_log_suspectE, @object
	.size	_ZN2v88internal16FLAG_log_suspectE, 1
_ZN2v88internal16FLAG_log_suspectE:
	.zero	1
	.globl	_ZN2v88internal16FLAG_log_handlesE
	.section	.bss._ZN2v88internal16FLAG_log_handlesE,"aw",@nobits
	.type	_ZN2v88internal16FLAG_log_handlesE, @object
	.size	_ZN2v88internal16FLAG_log_handlesE, 1
_ZN2v88internal16FLAG_log_handlesE:
	.zero	1
	.globl	_ZN2v88internal13FLAG_log_codeE
	.section	.bss._ZN2v88internal13FLAG_log_codeE,"aw",@nobits
	.type	_ZN2v88internal13FLAG_log_codeE, @object
	.size	_ZN2v88internal13FLAG_log_codeE, 1
_ZN2v88internal13FLAG_log_codeE:
	.zero	1
	.globl	_ZN2v88internal12FLAG_log_apiE
	.section	.bss._ZN2v88internal12FLAG_log_apiE,"aw",@nobits
	.type	_ZN2v88internal12FLAG_log_apiE, @object
	.size	_ZN2v88internal12FLAG_log_apiE, 1
_ZN2v88internal12FLAG_log_apiE:
	.zero	1
	.globl	_ZN2v88internal12FLAG_log_allE
	.section	.bss._ZN2v88internal12FLAG_log_allE,"aw",@nobits
	.type	_ZN2v88internal12FLAG_log_allE, @object
	.size	_ZN2v88internal12FLAG_log_allE, 1
_ZN2v88internal12FLAG_log_allE:
	.zero	1
	.globl	_ZN2v88internal8FLAG_logE
	.section	.bss._ZN2v88internal8FLAG_logE,"aw",@nobits
	.type	_ZN2v88internal8FLAG_logE, @object
	.size	_ZN2v88internal8FLAG_logE, 1
_ZN2v88internal8FLAG_logE:
	.zero	1
	.globl	_ZN2v88internal37FLAG_mock_arraybuffer_allocator_limitE
	.section	.bss._ZN2v88internal37FLAG_mock_arraybuffer_allocator_limitE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal37FLAG_mock_arraybuffer_allocator_limitE, @object
	.size	_ZN2v88internal37FLAG_mock_arraybuffer_allocator_limitE, 8
_ZN2v88internal37FLAG_mock_arraybuffer_allocator_limitE:
	.zero	8
	.globl	_ZN2v88internal31FLAG_mock_arraybuffer_allocatorE
	.section	.bss._ZN2v88internal31FLAG_mock_arraybuffer_allocatorE,"aw",@nobits
	.type	_ZN2v88internal31FLAG_mock_arraybuffer_allocatorE, @object
	.size	_ZN2v88internal31FLAG_mock_arraybuffer_allocatorE, 1
_ZN2v88internal31FLAG_mock_arraybuffer_allocatorE:
	.zero	1
	.globl	_ZN2v88internal17FLAG_map_countersE
	.section	.data.rel.local._ZN2v88internal17FLAG_map_countersE,"aw"
	.align 8
	.type	_ZN2v88internal17FLAG_map_countersE, @object
	.size	_ZN2v88internal17FLAG_map_countersE, 8
_ZN2v88internal17FLAG_map_countersE:
	.quad	.LC24
	.globl	_ZN2v88internal25FLAG_use_external_stringsE
	.section	.bss._ZN2v88internal25FLAG_use_external_stringsE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_use_external_stringsE, @object
	.size	_ZN2v88internal25FLAG_use_external_stringsE, 1
_ZN2v88internal25FLAG_use_external_stringsE:
	.zero	1
	.globl	_ZN2v88internal22FLAG_dump_counters_nvpE
	.section	.bss._ZN2v88internal22FLAG_dump_counters_nvpE,"aw",@nobits
	.type	_ZN2v88internal22FLAG_dump_counters_nvpE, @object
	.size	_ZN2v88internal22FLAG_dump_counters_nvpE, 1
_ZN2v88internal22FLAG_dump_counters_nvpE:
	.zero	1
	.globl	_ZN2v88internal18FLAG_dump_countersE
	.section	.bss._ZN2v88internal18FLAG_dump_countersE,"aw",@nobits
	.type	_ZN2v88internal18FLAG_dump_countersE, @object
	.size	_ZN2v88internal18FLAG_dump_countersE, 1
_ZN2v88internal18FLAG_dump_countersE:
	.zero	1
	.globl	_ZN2v88internal9FLAG_helpE
	.section	.bss._ZN2v88internal9FLAG_helpE,"aw",@nobits
	.type	_ZN2v88internal9FLAG_helpE, @object
	.size	_ZN2v88internal9FLAG_helpE, 1
_ZN2v88internal9FLAG_helpE:
	.zero	1
	.globl	_ZN2v88internal13FLAG_minor_mcE
	.section	.bss._ZN2v88internal13FLAG_minor_mcE,"aw",@nobits
	.type	_ZN2v88internal13FLAG_minor_mcE, @object
	.size	_ZN2v88internal13FLAG_minor_mcE, 1
_ZN2v88internal13FLAG_minor_mcE:
	.zero	1
	.globl	_ZN2v88internal36FLAG_trace_minor_mc_parallel_markingE
	.section	.bss._ZN2v88internal36FLAG_trace_minor_mc_parallel_markingE,"aw",@nobits
	.type	_ZN2v88internal36FLAG_trace_minor_mc_parallel_markingE, @object
	.size	_ZN2v88internal36FLAG_trace_minor_mc_parallel_markingE, 1
_ZN2v88internal36FLAG_trace_minor_mc_parallel_markingE:
	.zero	1
	.globl	_ZN2v88internal30FLAG_minor_mc_parallel_markingE
	.section	.data._ZN2v88internal30FLAG_minor_mc_parallel_markingE,"aw"
	.type	_ZN2v88internal30FLAG_minor_mc_parallel_markingE, @object
	.size	_ZN2v88internal30FLAG_minor_mc_parallel_markingE, 1
_ZN2v88internal30FLAG_minor_mc_parallel_markingE:
	.byte	1
	.globl	_ZN2v88internal14FLAG_target_osE
	.section	.bss._ZN2v88internal14FLAG_target_osE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal14FLAG_target_osE, @object
	.size	_ZN2v88internal14FLAG_target_osE, 8
_ZN2v88internal14FLAG_target_osE:
	.zero	8
	.globl	_ZN2v88internal16FLAG_target_archE
	.section	.bss._ZN2v88internal16FLAG_target_archE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal16FLAG_target_archE, @object
	.size	_ZN2v88internal16FLAG_target_archE, 8
_ZN2v88internal16FLAG_target_archE:
	.zero	8
	.globl	_ZN2v88internal17FLAG_startup_blobE
	.section	.bss._ZN2v88internal17FLAG_startup_blobE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal17FLAG_startup_blobE, @object
	.size	_ZN2v88internal17FLAG_startup_blobE, 8
_ZN2v88internal17FLAG_startup_blobE:
	.zero	8
	.globl	_ZN2v88internal16FLAG_startup_srcE
	.section	.bss._ZN2v88internal16FLAG_startup_srcE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal16FLAG_startup_srcE, @object
	.size	_ZN2v88internal16FLAG_startup_srcE, 8
_ZN2v88internal16FLAG_startup_srcE:
	.zero	8
	.globl	_ZN2v88internal21FLAG_embedded_variantE
	.section	.bss._ZN2v88internal21FLAG_embedded_variantE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal21FLAG_embedded_variantE, @object
	.size	_ZN2v88internal21FLAG_embedded_variantE, 8
_ZN2v88internal21FLAG_embedded_variantE:
	.zero	8
	.globl	_ZN2v88internal17FLAG_embedded_srcE
	.section	.bss._ZN2v88internal17FLAG_embedded_srcE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal17FLAG_embedded_srcE, @object
	.size	_ZN2v88internal17FLAG_embedded_srcE, 8
_ZN2v88internal17FLAG_embedded_srcE:
	.zero	8
	.globl	_ZN2v88internal27FLAG_testing_d8_test_runnerE
	.section	.bss._ZN2v88internal27FLAG_testing_d8_test_runnerE,"aw",@nobits
	.type	_ZN2v88internal27FLAG_testing_d8_test_runnerE, @object
	.size	_ZN2v88internal27FLAG_testing_d8_test_runnerE, 1
_ZN2v88internal27FLAG_testing_d8_test_runnerE:
	.zero	1
	.globl	_ZN2v88internal22FLAG_testing_prng_seedE
	.section	.data._ZN2v88internal22FLAG_testing_prng_seedE,"aw"
	.align 4
	.type	_ZN2v88internal22FLAG_testing_prng_seedE, @object
	.size	_ZN2v88internal22FLAG_testing_prng_seedE, 4
_ZN2v88internal22FLAG_testing_prng_seedE:
	.long	42
	.globl	_ZN2v88internal24FLAG_testing_string_flagE
	.section	.data.rel.local._ZN2v88internal24FLAG_testing_string_flagE,"aw"
	.align 8
	.type	_ZN2v88internal24FLAG_testing_string_flagE, @object
	.size	_ZN2v88internal24FLAG_testing_string_flagE, 8
_ZN2v88internal24FLAG_testing_string_flagE:
	.quad	.LC962
	.globl	_ZN2v88internal23FLAG_testing_float_flagE
	.section	.data._ZN2v88internal23FLAG_testing_float_flagE,"aw"
	.align 8
	.type	_ZN2v88internal23FLAG_testing_float_flagE, @object
	.size	_ZN2v88internal23FLAG_testing_float_flagE, 8
_ZN2v88internal23FLAG_testing_float_flagE:
	.long	0
	.long	1074003968
	.globl	_ZN2v88internal21FLAG_testing_int_flagE
	.section	.data._ZN2v88internal21FLAG_testing_int_flagE,"aw"
	.align 4
	.type	_ZN2v88internal21FLAG_testing_int_flagE, @object
	.size	_ZN2v88internal21FLAG_testing_int_flagE, 4
_ZN2v88internal21FLAG_testing_int_flagE:
	.long	13
	.globl	_ZN2v88internal28FLAG_testing_maybe_bool_flagE
	.section	.bss._ZN2v88internal28FLAG_testing_maybe_bool_flagE,"aw",@nobits
	.type	_ZN2v88internal28FLAG_testing_maybe_bool_flagE, @object
	.size	_ZN2v88internal28FLAG_testing_maybe_bool_flagE, 2
_ZN2v88internal28FLAG_testing_maybe_bool_flagE:
	.zero	2
	.globl	_ZN2v88internal22FLAG_testing_bool_flagE
	.section	.data._ZN2v88internal22FLAG_testing_bool_flagE,"aw"
	.type	_ZN2v88internal22FLAG_testing_bool_flagE, @object
	.size	_ZN2v88internal22FLAG_testing_bool_flagE, 1
_ZN2v88internal22FLAG_testing_bool_flagE:
	.byte	1
	.globl	_ZN2v88internal19FLAG_regexp_tier_upE
	.section	.bss._ZN2v88internal19FLAG_regexp_tier_upE,"aw",@nobits
	.type	_ZN2v88internal19FLAG_regexp_tier_upE, @object
	.size	_ZN2v88internal19FLAG_regexp_tier_upE, 1
_ZN2v88internal19FLAG_regexp_tier_upE:
	.zero	1
	.globl	_ZN2v88internal25FLAG_regexp_interpret_allE
	.section	.bss._ZN2v88internal25FLAG_regexp_interpret_allE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_regexp_interpret_allE, @object
	.size	_ZN2v88internal25FLAG_regexp_interpret_allE, 1
_ZN2v88internal25FLAG_regexp_interpret_allE:
	.zero	1
	.globl	_ZN2v88internal26FLAG_regexp_mode_modifiersE
	.section	.bss._ZN2v88internal26FLAG_regexp_mode_modifiersE,"aw",@nobits
	.type	_ZN2v88internal26FLAG_regexp_mode_modifiersE, @object
	.size	_ZN2v88internal26FLAG_regexp_mode_modifiersE, 1
_ZN2v88internal26FLAG_regexp_mode_modifiersE:
	.zero	1
	.globl	_ZN2v88internal24FLAG_regexp_optimizationE
	.section	.data._ZN2v88internal24FLAG_regexp_optimizationE,"aw"
	.type	_ZN2v88internal24FLAG_regexp_optimizationE, @object
	.size	_ZN2v88internal24FLAG_regexp_optimizationE, 1
_ZN2v88internal24FLAG_regexp_optimizationE:
	.byte	1
	.globl	_ZN2v88internal29FLAG_serialization_chunk_sizeE
	.section	.data._ZN2v88internal29FLAG_serialization_chunk_sizeE,"aw"
	.align 4
	.type	_ZN2v88internal29FLAG_serialization_chunk_sizeE, @object
	.size	_ZN2v88internal29FLAG_serialization_chunk_sizeE, 4
_ZN2v88internal29FLAG_serialization_chunk_sizeE:
	.long	4096
	.globl	_ZN2v88internal29FLAG_serialization_statisticsE
	.section	.bss._ZN2v88internal29FLAG_serialization_statisticsE,"aw",@nobits
	.type	_ZN2v88internal29FLAG_serialization_statisticsE, @object
	.size	_ZN2v88internal29FLAG_serialization_statisticsE, 1
_ZN2v88internal29FLAG_serialization_statisticsE:
	.zero	1
	.globl	_ZN2v88internal28FLAG_profile_deserializationE
	.section	.bss._ZN2v88internal28FLAG_profile_deserializationE,"aw",@nobits
	.type	_ZN2v88internal28FLAG_profile_deserializationE, @object
	.size	_ZN2v88internal28FLAG_profile_deserializationE, 1
_ZN2v88internal28FLAG_profile_deserializationE:
	.zero	1
	.globl	_ZN2v88internal23FLAG_runtime_call_statsE
	.section	.bss._ZN2v88internal23FLAG_runtime_call_statsE,"aw",@nobits
	.type	_ZN2v88internal23FLAG_runtime_call_statsE, @object
	.size	_ZN2v88internal23FLAG_runtime_call_statsE, 1
_ZN2v88internal23FLAG_runtime_call_statsE:
	.zero	1
	.globl	_ZN2v88internal31FLAG_detailed_error_stack_traceE
	.section	.bss._ZN2v88internal31FLAG_detailed_error_stack_traceE,"aw",@nobits
	.type	_ZN2v88internal31FLAG_detailed_error_stack_traceE, @object
	.size	_ZN2v88internal31FLAG_detailed_error_stack_traceE, 1
_ZN2v88internal31FLAG_detailed_error_stack_traceE:
	.zero	1
	.globl	_ZN2v88internal25FLAG_print_all_exceptionsE
	.section	.bss._ZN2v88internal25FLAG_print_all_exceptionsE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_print_all_exceptionsE, @object
	.size	_ZN2v88internal25FLAG_print_all_exceptionsE, 1
_ZN2v88internal25FLAG_print_all_exceptionsE:
	.zero	1
	.globl	_ZN2v88internal15FLAG_trace_railE
	.section	.bss._ZN2v88internal15FLAG_trace_railE,"aw",@nobits
	.type	_ZN2v88internal15FLAG_trace_railE, @object
	.size	_ZN2v88internal15FLAG_trace_railE, 1
_ZN2v88internal15FLAG_trace_railE:
	.zero	1
	.globl	_ZN2v88internal23FLAG_fuzzer_random_seedE
	.section	.bss._ZN2v88internal23FLAG_fuzzer_random_seedE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal23FLAG_fuzzer_random_seedE, @object
	.size	_ZN2v88internal23FLAG_fuzzer_random_seedE, 4
_ZN2v88internal23FLAG_fuzzer_random_seedE:
	.zero	4
	.globl	_ZN2v88internal16FLAG_random_seedE
	.section	.bss._ZN2v88internal16FLAG_random_seedE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal16FLAG_random_seedE, @object
	.size	_ZN2v88internal16FLAG_random_seedE, 4
_ZN2v88internal16FLAG_random_seedE:
	.zero	4
	.globl	_ZN2v88internal14FLAG_hash_seedE
	.section	.bss._ZN2v88internal14FLAG_hash_seedE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal14FLAG_hash_seedE, @object
	.size	_ZN2v88internal14FLAG_hash_seedE, 8
_ZN2v88internal14FLAG_hash_seedE:
	.zero	8
	.globl	_ZN2v88internal20FLAG_rehash_snapshotE
	.section	.data._ZN2v88internal20FLAG_rehash_snapshotE,"aw"
	.type	_ZN2v88internal20FLAG_rehash_snapshotE, @object
	.size	_ZN2v88internal20FLAG_rehash_snapshotE, 1
_ZN2v88internal20FLAG_rehash_snapshotE:
	.byte	1
	.globl	_ZN2v88internal21FLAG_randomize_hashesE
	.section	.data._ZN2v88internal21FLAG_randomize_hashesE,"aw"
	.type	_ZN2v88internal21FLAG_randomize_hashesE, @object
	.size	_ZN2v88internal21FLAG_randomize_hashesE, 1
_ZN2v88internal21FLAG_randomize_hashesE:
	.byte	1
	.globl	_ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE
	.section	.bss._ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE,"aw",@nobits
	.type	_ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE, @object
	.size	_ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE, 1
_ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE:
	.zero	1
	.globl	_ZN2v88internal32FLAG_abort_on_uncaught_exceptionE
	.section	.bss._ZN2v88internal32FLAG_abort_on_uncaught_exceptionE,"aw",@nobits
	.type	_ZN2v88internal32FLAG_abort_on_uncaught_exceptionE, @object
	.size	_ZN2v88internal32FLAG_abort_on_uncaught_exceptionE, 1
_ZN2v88internal32FLAG_abort_on_uncaught_exceptionE:
	.zero	1
	.globl	_ZN2v88internal27FLAG_stack_trace_on_illegalE
	.section	.bss._ZN2v88internal27FLAG_stack_trace_on_illegalE,"aw",@nobits
	.type	_ZN2v88internal27FLAG_stack_trace_on_illegalE, @object
	.size	_ZN2v88internal27FLAG_stack_trace_on_illegalE, 1
_ZN2v88internal27FLAG_stack_trace_on_illegalE:
	.zero	1
	.globl	_ZN2v88internal23FLAG_async_stack_tracesE
	.section	.data._ZN2v88internal23FLAG_async_stack_tracesE,"aw"
	.type	_ZN2v88internal23FLAG_async_stack_tracesE, @object
	.size	_ZN2v88internal23FLAG_async_stack_tracesE, 1
_ZN2v88internal23FLAG_async_stack_tracesE:
	.byte	1
	.globl	_ZN2v88internal23FLAG_trace_sim_messagesE
	.section	.bss._ZN2v88internal23FLAG_trace_sim_messagesE,"aw",@nobits
	.type	_ZN2v88internal23FLAG_trace_sim_messagesE, @object
	.size	_ZN2v88internal23FLAG_trace_sim_messagesE, 1
_ZN2v88internal23FLAG_trace_sim_messagesE:
	.zero	1
	.globl	_ZN2v88internal35FLAG_ignore_asm_unimplemented_breakE
	.section	.bss._ZN2v88internal35FLAG_ignore_asm_unimplemented_breakE,"aw",@nobits
	.type	_ZN2v88internal35FLAG_ignore_asm_unimplemented_breakE, @object
	.size	_ZN2v88internal35FLAG_ignore_asm_unimplemented_breakE, 1
_ZN2v88internal35FLAG_ignore_asm_unimplemented_breakE:
	.zero	1
	.globl	_ZN2v88internal15FLAG_log_colourE
	.section	.data._ZN2v88internal15FLAG_log_colourE,"aw"
	.type	_ZN2v88internal15FLAG_log_colourE, @object
	.size	_ZN2v88internal15FLAG_log_colourE, 1
_ZN2v88internal15FLAG_log_colourE:
	.byte	1
	.globl	_ZN2v88internal19FLAG_sim_stack_sizeE
	.section	.data._ZN2v88internal19FLAG_sim_stack_sizeE,"aw"
	.align 4
	.type	_ZN2v88internal19FLAG_sim_stack_sizeE, @object
	.size	_ZN2v88internal19FLAG_sim_stack_sizeE, 4
_ZN2v88internal19FLAG_sim_stack_sizeE:
	.long	2048
	.globl	_ZN2v88internal24FLAG_sim_stack_alignmentE
	.section	.data._ZN2v88internal24FLAG_sim_stack_alignmentE,"aw"
	.align 4
	.type	_ZN2v88internal24FLAG_sim_stack_alignmentE, @object
	.size	_ZN2v88internal24FLAG_sim_stack_alignmentE, 4
_ZN2v88internal24FLAG_sim_stack_alignmentE:
	.long	8
	.globl	_ZN2v88internal16FLAG_stop_sim_atE
	.section	.bss._ZN2v88internal16FLAG_stop_sim_atE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal16FLAG_stop_sim_atE, @object
	.size	_ZN2v88internal16FLAG_stop_sim_atE, 4
_ZN2v88internal16FLAG_stop_sim_atE:
	.zero	4
	.globl	_ZN2v88internal17FLAG_check_icacheE
	.section	.bss._ZN2v88internal17FLAG_check_icacheE,"aw",@nobits
	.type	_ZN2v88internal17FLAG_check_icacheE, @object
	.size	_ZN2v88internal17FLAG_check_icacheE, 1
_ZN2v88internal17FLAG_check_icacheE:
	.zero	1
	.globl	_ZN2v88internal14FLAG_debug_simE
	.section	.bss._ZN2v88internal14FLAG_debug_simE,"aw",@nobits
	.type	_ZN2v88internal14FLAG_debug_simE, @object
	.size	_ZN2v88internal14FLAG_debug_simE, 1
_ZN2v88internal14FLAG_debug_simE:
	.zero	1
	.globl	_ZN2v88internal14FLAG_trace_simE
	.section	.bss._ZN2v88internal14FLAG_trace_simE,"aw",@nobits
	.type	_ZN2v88internal14FLAG_trace_simE, @object
	.size	_ZN2v88internal14FLAG_trace_simE, 1
_ZN2v88internal14FLAG_trace_simE:
	.zero	1
	.globl	_ZN2v88internal15FLAG_parse_onlyE
	.section	.bss._ZN2v88internal15FLAG_parse_onlyE,"aw",@nobits
	.type	_ZN2v88internal15FLAG_parse_onlyE, @object
	.size	_ZN2v88internal15FLAG_parse_onlyE, 1
_ZN2v88internal15FLAG_parse_onlyE:
	.zero	1
	.globl	_ZN2v88internal25FLAG_allow_natives_syntaxE
	.section	.bss._ZN2v88internal25FLAG_allow_natives_syntaxE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_allow_natives_syntaxE, @object
	.size	_ZN2v88internal25FLAG_allow_natives_syntaxE, 1
_ZN2v88internal25FLAG_allow_natives_syntaxE:
	.zero	1
	.globl	_ZN2v88internal23FLAG_trace_maps_detailsE
	.section	.data._ZN2v88internal23FLAG_trace_maps_detailsE,"aw"
	.type	_ZN2v88internal23FLAG_trace_maps_detailsE, @object
	.size	_ZN2v88internal23FLAG_trace_maps_detailsE, 1
_ZN2v88internal23FLAG_trace_maps_detailsE:
	.byte	1
	.globl	_ZN2v88internal15FLAG_trace_mapsE
	.section	.bss._ZN2v88internal15FLAG_trace_mapsE,"aw",@nobits
	.type	_ZN2v88internal15FLAG_trace_mapsE, @object
	.size	_ZN2v88internal15FLAG_trace_mapsE, 1
_ZN2v88internal15FLAG_trace_mapsE:
	.zero	1
	.globl	_ZN2v88internal27FLAG_trace_for_in_enumerateE
	.section	.bss._ZN2v88internal27FLAG_trace_for_in_enumerateE,"aw",@nobits
	.type	_ZN2v88internal27FLAG_trace_for_in_enumerateE, @object
	.size	_ZN2v88internal27FLAG_trace_for_in_enumerateE, 1
_ZN2v88internal27FLAG_trace_for_in_enumerateE:
	.zero	1
	.globl	_ZN2v88internal24FLAG_use_verbose_printerE
	.section	.data._ZN2v88internal24FLAG_use_verbose_printerE,"aw"
	.type	_ZN2v88internal24FLAG_use_verbose_printerE, @object
	.size	_ZN2v88internal24FLAG_use_verbose_printerE, 1
_ZN2v88internal24FLAG_use_verbose_printerE:
	.byte	1
	.globl	_ZN2v88internal26FLAG_trace_prototype_usersE
	.section	.bss._ZN2v88internal26FLAG_trace_prototype_usersE,"aw",@nobits
	.type	_ZN2v88internal26FLAG_trace_prototype_usersE, @object
	.size	_ZN2v88internal26FLAG_trace_prototype_usersE, 1
_ZN2v88internal26FLAG_trace_prototype_usersE:
	.zero	1
	.globl	_ZN2v88internal17FLAG_thin_stringsE
	.section	.data._ZN2v88internal17FLAG_thin_stringsE,"aw"
	.type	_ZN2v88internal17FLAG_thin_stringsE, @object
	.size	_ZN2v88internal17FLAG_thin_stringsE, 1
_ZN2v88internal17FLAG_thin_stringsE:
	.byte	1
	.globl	_ZN2v88internal25FLAG_native_code_countersE
	.section	.bss._ZN2v88internal25FLAG_native_code_countersE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_native_code_countersE, @object
	.size	_ZN2v88internal25FLAG_native_code_countersE, 1
_ZN2v88internal25FLAG_native_code_countersE:
	.zero	1
	.globl	_ZN2v88internal30FLAG_max_polymorphic_map_countE
	.section	.data._ZN2v88internal30FLAG_max_polymorphic_map_countE,"aw"
	.align 4
	.type	_ZN2v88internal30FLAG_max_polymorphic_map_countE, @object
	.size	_ZN2v88internal30FLAG_max_polymorphic_map_countE, 4
_ZN2v88internal30FLAG_max_polymorphic_map_countE:
	.long	4
	.globl	_ZN2v88internal40FLAG_modify_field_representation_inplaceE
	.section	.data._ZN2v88internal40FLAG_modify_field_representation_inplaceE,"aw"
	.type	_ZN2v88internal40FLAG_modify_field_representation_inplaceE, @object
	.size	_ZN2v88internal40FLAG_modify_field_representation_inplaceE, 1
_ZN2v88internal40FLAG_modify_field_representation_inplaceE:
	.byte	1
	.globl	_ZN2v88internal13FLAG_trace_icE
	.section	.bss._ZN2v88internal13FLAG_trace_icE,"aw",@nobits
	.type	_ZN2v88internal13FLAG_trace_icE, @object
	.size	_ZN2v88internal13FLAG_trace_icE, 1
_ZN2v88internal13FLAG_trace_icE:
	.zero	1
	.globl	_ZN2v88internal26FLAG_use_idle_notificationE
	.section	.data._ZN2v88internal26FLAG_use_idle_notificationE,"aw"
	.type	_ZN2v88internal26FLAG_use_idle_notificationE, @object
	.size	_ZN2v88internal26FLAG_use_idle_notificationE, 1
_ZN2v88internal26FLAG_use_idle_notificationE:
	.byte	1
	.globl	_ZN2v88internal47FLAG_sampling_heap_profiler_suppress_randomnessE
	.section	.bss._ZN2v88internal47FLAG_sampling_heap_profiler_suppress_randomnessE,"aw",@nobits
	.type	_ZN2v88internal47FLAG_sampling_heap_profiler_suppress_randomnessE, @object
	.size	_ZN2v88internal47FLAG_sampling_heap_profiler_suppress_randomnessE, 1
_ZN2v88internal47FLAG_sampling_heap_profiler_suppress_randomnessE:
	.zero	1
	.globl	_ZN2v88internal31FLAG_heap_snapshot_string_limitE
	.section	.data._ZN2v88internal31FLAG_heap_snapshot_string_limitE,"aw"
	.align 4
	.type	_ZN2v88internal31FLAG_heap_snapshot_string_limitE, @object
	.size	_ZN2v88internal31FLAG_heap_snapshot_string_limitE, 4
_ZN2v88internal31FLAG_heap_snapshot_string_limitE:
	.long	1024
	.globl	_ZN2v88internal37FLAG_heap_profiler_use_embedder_graphE
	.section	.data._ZN2v88internal37FLAG_heap_profiler_use_embedder_graphE,"aw"
	.type	_ZN2v88internal37FLAG_heap_profiler_use_embedder_graphE, @object
	.size	_ZN2v88internal37FLAG_heap_profiler_use_embedder_graphE, 1
_ZN2v88internal37FLAG_heap_profiler_use_embedder_graphE:
	.byte	1
	.globl	_ZN2v88internal32FLAG_heap_profiler_trace_objectsE
	.section	.bss._ZN2v88internal32FLAG_heap_profiler_trace_objectsE,"aw",@nobits
	.type	_ZN2v88internal32FLAG_heap_profiler_trace_objectsE, @object
	.size	_ZN2v88internal32FLAG_heap_profiler_trace_objectsE, 1
_ZN2v88internal32FLAG_heap_profiler_trace_objectsE:
	.zero	1
	.globl	_ZN2v88internal23FLAG_histogram_intervalE
	.section	.data._ZN2v88internal23FLAG_histogram_intervalE,"aw"
	.align 4
	.type	_ZN2v88internal23FLAG_histogram_intervalE, @object
	.size	_ZN2v88internal23FLAG_histogram_intervalE, 4
_ZN2v88internal23FLAG_histogram_intervalE:
	.long	600000
	.globl	_ZN2v88internal33FLAG_clear_exceptions_on_js_entryE
	.section	.bss._ZN2v88internal33FLAG_clear_exceptions_on_js_entryE,"aw",@nobits
	.type	_ZN2v88internal33FLAG_clear_exceptions_on_js_entryE, @object
	.size	_ZN2v88internal33FLAG_clear_exceptions_on_js_entryE, 1
_ZN2v88internal33FLAG_clear_exceptions_on_js_entryE:
	.zero	1
	.globl	_ZN2v88internal34FLAG_max_stack_trace_source_lengthE
	.section	.data._ZN2v88internal34FLAG_max_stack_trace_source_lengthE,"aw"
	.align 4
	.type	_ZN2v88internal34FLAG_max_stack_trace_source_lengthE, @object
	.size	_ZN2v88internal34FLAG_max_stack_trace_source_lengthE, 4
_ZN2v88internal34FLAG_max_stack_trace_source_lengthE:
	.long	300
	.globl	_ZN2v88internal15FLAG_stack_sizeE
	.section	.data._ZN2v88internal15FLAG_stack_sizeE,"aw"
	.align 4
	.type	_ZN2v88internal15FLAG_stack_sizeE, @object
	.size	_ZN2v88internal15FLAG_stack_sizeE, 4
_ZN2v88internal15FLAG_stack_sizeE:
	.long	984
	.globl	_ZN2v88internal29FLAG_expose_inspector_scriptsE
	.section	.bss._ZN2v88internal29FLAG_expose_inspector_scriptsE,"aw",@nobits
	.type	_ZN2v88internal29FLAG_expose_inspector_scriptsE, @object
	.size	_ZN2v88internal29FLAG_expose_inspector_scriptsE, 1
_ZN2v88internal29FLAG_expose_inspector_scriptsE:
	.zero	1
	.globl	_ZN2v88internal15FLAG_hard_abortE
	.section	.data._ZN2v88internal15FLAG_hard_abortE,"aw"
	.type	_ZN2v88internal15FLAG_hard_abortE, @object
	.size	_ZN2v88internal15FLAG_hard_abortE, 1
_ZN2v88internal15FLAG_hard_abortE:
	.byte	1
	.globl	_ZN2v88internal42FLAG_trace_side_effect_free_debug_evaluateE
	.section	.bss._ZN2v88internal42FLAG_trace_side_effect_free_debug_evaluateE,"aw",@nobits
	.type	_ZN2v88internal42FLAG_trace_side_effect_free_debug_evaluateE, @object
	.size	_ZN2v88internal42FLAG_trace_side_effect_free_debug_evaluateE, 1
_ZN2v88internal42FLAG_trace_side_effect_free_debug_evaluateE:
	.zero	1
	.globl	_ZN2v88internal35FLAG_cpu_profiler_sampling_intervalE
	.section	.data._ZN2v88internal35FLAG_cpu_profiler_sampling_intervalE,"aw"
	.align 4
	.type	_ZN2v88internal35FLAG_cpu_profiler_sampling_intervalE, @object
	.size	_ZN2v88internal35FLAG_cpu_profiler_sampling_intervalE, 4
_ZN2v88internal35FLAG_cpu_profiler_sampling_intervalE:
	.long	1000
	.globl	_ZN2v88internal30FLAG_trace_compiler_dispatcherE
	.section	.bss._ZN2v88internal30FLAG_trace_compiler_dispatcherE,"aw",@nobits
	.type	_ZN2v88internal30FLAG_trace_compiler_dispatcherE, @object
	.size	_ZN2v88internal30FLAG_trace_compiler_dispatcherE, 1
_ZN2v88internal30FLAG_trace_compiler_dispatcherE:
	.zero	1
	.globl	_ZN2v88internal24FLAG_compiler_dispatcherE
	.section	.bss._ZN2v88internal24FLAG_compiler_dispatcherE,"aw",@nobits
	.type	_ZN2v88internal24FLAG_compiler_dispatcherE, @object
	.size	_ZN2v88internal24FLAG_compiler_dispatcherE, 1
_ZN2v88internal24FLAG_compiler_dispatcherE:
	.zero	1
	.globl	_ZN2v88internal27FLAG_parallel_compile_tasksE
	.section	.bss._ZN2v88internal27FLAG_parallel_compile_tasksE,"aw",@nobits
	.type	_ZN2v88internal27FLAG_parallel_compile_tasksE, @object
	.size	_ZN2v88internal27FLAG_parallel_compile_tasksE, 1
_ZN2v88internal27FLAG_parallel_compile_tasksE:
	.zero	1
	.globl	_ZN2v88internal32FLAG_cache_prototype_transitionsE
	.section	.data._ZN2v88internal32FLAG_cache_prototype_transitionsE,"aw"
	.type	_ZN2v88internal32FLAG_cache_prototype_transitionsE, @object
	.size	_ZN2v88internal32FLAG_cache_prototype_transitionsE, 1
_ZN2v88internal32FLAG_cache_prototype_transitionsE:
	.byte	1
	.globl	_ZN2v88internal22FLAG_compilation_cacheE
	.section	.data._ZN2v88internal22FLAG_compilation_cacheE,"aw"
	.type	_ZN2v88internal22FLAG_compilation_cacheE, @object
	.size	_ZN2v88internal22FLAG_compilation_cacheE, 1
_ZN2v88internal22FLAG_compilation_cacheE:
	.byte	1
	.globl	_ZN2v88internal21FLAG_trace_serializerE
	.section	.bss._ZN2v88internal21FLAG_trace_serializerE,"aw",@nobits
	.type	_ZN2v88internal21FLAG_trace_serializerE, @object
	.size	_ZN2v88internal21FLAG_trace_serializerE, 1
_ZN2v88internal21FLAG_trace_serializerE:
	.zero	1
	.globl	_ZN2v88internal23FLAG_prepare_always_optE
	.section	.bss._ZN2v88internal23FLAG_prepare_always_optE,"aw",@nobits
	.type	_ZN2v88internal23FLAG_prepare_always_optE, @object
	.size	_ZN2v88internal23FLAG_prepare_always_optE, 1
_ZN2v88internal23FLAG_prepare_always_optE:
	.zero	1
	.globl	_ZN2v88internal15FLAG_always_osrE
	.section	.bss._ZN2v88internal15FLAG_always_osrE,"aw",@nobits
	.type	_ZN2v88internal15FLAG_always_osrE, @object
	.size	_ZN2v88internal15FLAG_always_osrE, 1
_ZN2v88internal15FLAG_always_osrE:
	.zero	1
	.globl	_ZN2v88internal15FLAG_always_optE
	.section	.bss._ZN2v88internal15FLAG_always_optE,"aw",@nobits
	.type	_ZN2v88internal15FLAG_always_optE, @object
	.size	_ZN2v88internal15FLAG_always_optE, 1
_ZN2v88internal15FLAG_always_optE:
	.zero	1
	.globl	_ZN2v88internal21FLAG_trace_file_namesE
	.section	.bss._ZN2v88internal21FLAG_trace_file_namesE,"aw",@nobits
	.type	_ZN2v88internal21FLAG_trace_file_namesE, @object
	.size	_ZN2v88internal21FLAG_trace_file_namesE, 1
_ZN2v88internal21FLAG_trace_file_namesE:
	.zero	1
	.globl	_ZN2v88internal16FLAG_trace_deoptE
	.section	.bss._ZN2v88internal16FLAG_trace_deoptE,"aw",@nobits
	.type	_ZN2v88internal16FLAG_trace_deoptE, @object
	.size	_ZN2v88internal16FLAG_trace_deoptE, 1
_ZN2v88internal16FLAG_trace_deoptE:
	.zero	1
	.globl	_ZN2v88internal20FLAG_trace_opt_statsE
	.section	.bss._ZN2v88internal20FLAG_trace_opt_statsE,"aw",@nobits
	.type	_ZN2v88internal20FLAG_trace_opt_statsE, @object
	.size	_ZN2v88internal20FLAG_trace_opt_statsE, 1
_ZN2v88internal20FLAG_trace_opt_statsE:
	.zero	1
	.globl	_ZN2v88internal22FLAG_trace_opt_verboseE
	.section	.bss._ZN2v88internal22FLAG_trace_opt_verboseE,"aw",@nobits
	.type	_ZN2v88internal22FLAG_trace_opt_verboseE, @object
	.size	_ZN2v88internal22FLAG_trace_opt_verboseE, 1
_ZN2v88internal22FLAG_trace_opt_verboseE:
	.zero	1
	.globl	_ZN2v88internal14FLAG_trace_optE
	.section	.bss._ZN2v88internal14FLAG_trace_optE,"aw",@nobits
	.type	_ZN2v88internal14FLAG_trace_optE, @object
	.size	_ZN2v88internal14FLAG_trace_optE, 1
_ZN2v88internal14FLAG_trace_optE:
	.zero	1
	.globl	_ZN2v88internal13FLAG_max_lazyE
	.section	.bss._ZN2v88internal13FLAG_max_lazyE,"aw",@nobits
	.type	_ZN2v88internal13FLAG_max_lazyE, @object
	.size	_ZN2v88internal13FLAG_max_lazyE, 1
_ZN2v88internal13FLAG_max_lazyE:
	.zero	1
	.globl	_ZN2v88internal9FLAG_lazyE
	.section	.data._ZN2v88internal9FLAG_lazyE,"aw"
	.type	_ZN2v88internal9FLAG_lazyE, @object
	.size	_ZN2v88internal9FLAG_lazyE, 1
_ZN2v88internal9FLAG_lazyE:
	.byte	1
	.globl	_ZN2v88internal10FLAG_traceE
	.section	.bss._ZN2v88internal10FLAG_traceE,"aw",@nobits
	.type	_ZN2v88internal10FLAG_traceE, @object
	.size	_ZN2v88internal10FLAG_traceE, 1
_ZN2v88internal10FLAG_traceE:
	.zero	1
	.globl	_ZN2v88internal15FLAG_inline_newE
	.section	.data._ZN2v88internal15FLAG_inline_newE,"aw"
	.type	_ZN2v88internal15FLAG_inline_newE, @object
	.size	_ZN2v88internal15FLAG_inline_newE, 1
_ZN2v88internal15FLAG_inline_newE:
	.byte	1
	.globl	_ZN2v88internal46FLAG_test_small_max_function_context_stub_sizeE
	.section	.bss._ZN2v88internal46FLAG_test_small_max_function_context_stub_sizeE,"aw",@nobits
	.type	_ZN2v88internal46FLAG_test_small_max_function_context_stub_sizeE, @object
	.size	_ZN2v88internal46FLAG_test_small_max_function_context_stub_sizeE, 1
_ZN2v88internal46FLAG_test_small_max_function_context_stub_sizeE:
	.zero	1
	.globl	_ZN2v88internal20FLAG_force_slow_pathE
	.section	.bss._ZN2v88internal20FLAG_force_slow_pathE,"aw",@nobits
	.type	_ZN2v88internal20FLAG_force_slow_pathE, @object
	.size	_ZN2v88internal20FLAG_force_slow_pathE, 1
_ZN2v88internal20FLAG_force_slow_pathE:
	.zero	1
	.globl	_ZN2v88internal38FLAG_allow_unsafe_function_constructorE
	.section	.bss._ZN2v88internal38FLAG_allow_unsafe_function_constructorE,"aw",@nobits
	.type	_ZN2v88internal38FLAG_allow_unsafe_function_constructorE, @object
	.size	_ZN2v88internal38FLAG_allow_unsafe_function_constructorE, 1
_ZN2v88internal38FLAG_allow_unsafe_function_constructorE:
	.zero	1
	.globl	_ZN2v88internal27FLAG_expose_cputracemark_asE
	.section	.bss._ZN2v88internal27FLAG_expose_cputracemark_asE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal27FLAG_expose_cputracemark_asE, @object
	.size	_ZN2v88internal27FLAG_expose_cputracemark_asE, 8
_ZN2v88internal27FLAG_expose_cputracemark_asE:
	.zero	8
	.globl	_ZN2v88internal23FLAG_expose_async_hooksE
	.section	.bss._ZN2v88internal23FLAG_expose_async_hooksE,"aw",@nobits
	.type	_ZN2v88internal23FLAG_expose_async_hooksE, @object
	.size	_ZN2v88internal23FLAG_expose_async_hooksE, 1
_ZN2v88internal23FLAG_expose_async_hooksE:
	.zero	1
	.globl	_ZN2v88internal42FLAG_disallow_code_generation_from_stringsE
	.section	.bss._ZN2v88internal42FLAG_disallow_code_generation_from_stringsE,"aw",@nobits
	.type	_ZN2v88internal42FLAG_disallow_code_generation_from_stringsE, @object
	.size	_ZN2v88internal42FLAG_disallow_code_generation_from_stringsE, 1
_ZN2v88internal42FLAG_disallow_code_generation_from_stringsE:
	.zero	1
	.globl	_ZN2v88internal36FLAG_experimental_stack_trace_framesE
	.section	.bss._ZN2v88internal36FLAG_experimental_stack_trace_framesE,"aw",@nobits
	.type	_ZN2v88internal36FLAG_experimental_stack_trace_framesE, @object
	.size	_ZN2v88internal36FLAG_experimental_stack_trace_framesE, 1
_ZN2v88internal36FLAG_experimental_stack_trace_framesE:
	.zero	1
	.globl	_ZN2v88internal29FLAG_builtins_in_stack_tracesE
	.section	.bss._ZN2v88internal29FLAG_builtins_in_stack_tracesE,"aw",@nobits
	.type	_ZN2v88internal29FLAG_builtins_in_stack_tracesE, @object
	.size	_ZN2v88internal29FLAG_builtins_in_stack_tracesE, 1
_ZN2v88internal29FLAG_builtins_in_stack_tracesE:
	.zero	1
	.globl	_ZN2v88internal22FLAG_stack_trace_limitE
	.section	.data._ZN2v88internal22FLAG_stack_trace_limitE,"aw"
	.align 4
	.type	_ZN2v88internal22FLAG_stack_trace_limitE, @object
	.size	_ZN2v88internal22FLAG_stack_trace_limitE, 4
_ZN2v88internal22FLAG_stack_trace_limitE:
	.long	10
	.globl	_ZN2v88internal27FLAG_expose_trigger_failureE
	.section	.bss._ZN2v88internal27FLAG_expose_trigger_failureE,"aw",@nobits
	.type	_ZN2v88internal27FLAG_expose_trigger_failureE, @object
	.size	_ZN2v88internal27FLAG_expose_trigger_failureE, 1
_ZN2v88internal27FLAG_expose_trigger_failureE:
	.zero	1
	.globl	_ZN2v88internal30FLAG_expose_externalize_stringE
	.section	.bss._ZN2v88internal30FLAG_expose_externalize_stringE,"aw",@nobits
	.type	_ZN2v88internal30FLAG_expose_externalize_stringE, @object
	.size	_ZN2v88internal30FLAG_expose_externalize_stringE, 1
_ZN2v88internal30FLAG_expose_externalize_stringE:
	.zero	1
	.globl	_ZN2v88internal17FLAG_expose_gc_asE
	.section	.bss._ZN2v88internal17FLAG_expose_gc_asE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal17FLAG_expose_gc_asE, @object
	.size	_ZN2v88internal17FLAG_expose_gc_asE, 8
_ZN2v88internal17FLAG_expose_gc_asE:
	.zero	8
	.globl	_ZN2v88internal14FLAG_expose_gcE
	.section	.bss._ZN2v88internal14FLAG_expose_gcE,"aw",@nobits
	.type	_ZN2v88internal14FLAG_expose_gcE, @object
	.size	_ZN2v88internal14FLAG_expose_gcE, 1
_ZN2v88internal14FLAG_expose_gcE:
	.zero	1
	.globl	_ZN2v88internal23FLAG_expose_free_bufferE
	.section	.bss._ZN2v88internal23FLAG_expose_free_bufferE,"aw",@nobits
	.type	_ZN2v88internal23FLAG_expose_free_bufferE, @object
	.size	_ZN2v88internal23FLAG_expose_free_bufferE, 1
_ZN2v88internal23FLAG_expose_free_bufferE:
	.zero	1
	.globl	_ZN2v88internal30FLAG_disable_old_api_accessorsE
	.section	.bss._ZN2v88internal30FLAG_disable_old_api_accessorsE,"aw",@nobits
	.type	_ZN2v88internal30FLAG_disable_old_api_accessorsE, @object
	.size	_ZN2v88internal30FLAG_disable_old_api_accessorsE, 1
_ZN2v88internal30FLAG_disable_old_api_accessorsE:
	.zero	1
	.globl	_ZN2v88internal21FLAG_script_streamingE
	.section	.data._ZN2v88internal21FLAG_script_streamingE,"aw"
	.type	_ZN2v88internal21FLAG_script_streamingE, @object
	.size	_ZN2v88internal21FLAG_script_streamingE, 1
_ZN2v88internal21FLAG_script_streamingE:
	.byte	1
	.globl	_ZN2v88internal37FLAG_enable_regexp_unaligned_accessesE
	.section	.data._ZN2v88internal37FLAG_enable_regexp_unaligned_accessesE,"aw"
	.type	_ZN2v88internal37FLAG_enable_regexp_unaligned_accessesE, @object
	.size	_ZN2v88internal37FLAG_enable_regexp_unaligned_accessesE, 1
_ZN2v88internal37FLAG_enable_regexp_unaligned_accessesE:
	.byte	1
	.globl	_ZN2v88internal17FLAG_enable_armv8E
	.section	.bss._ZN2v88internal17FLAG_enable_armv8E,"aw",@nobits
	.type	_ZN2v88internal17FLAG_enable_armv8E, @object
	.size	_ZN2v88internal17FLAG_enable_armv8E, 2
_ZN2v88internal17FLAG_enable_armv8E:
	.zero	2
	.globl	_ZN2v88internal17FLAG_enable_sudivE
	.section	.bss._ZN2v88internal17FLAG_enable_sudivE,"aw",@nobits
	.type	_ZN2v88internal17FLAG_enable_sudivE, @object
	.size	_ZN2v88internal17FLAG_enable_sudivE, 2
_ZN2v88internal17FLAG_enable_sudivE:
	.zero	2
	.globl	_ZN2v88internal16FLAG_enable_neonE
	.section	.bss._ZN2v88internal16FLAG_enable_neonE,"aw",@nobits
	.type	_ZN2v88internal16FLAG_enable_neonE, @object
	.size	_ZN2v88internal16FLAG_enable_neonE, 2
_ZN2v88internal16FLAG_enable_neonE:
	.zero	2
	.globl	_ZN2v88internal19FLAG_enable_32dregsE
	.section	.bss._ZN2v88internal19FLAG_enable_32dregsE,"aw",@nobits
	.type	_ZN2v88internal19FLAG_enable_32dregsE, @object
	.size	_ZN2v88internal19FLAG_enable_32dregsE, 2
_ZN2v88internal19FLAG_enable_32dregsE:
	.zero	2
	.globl	_ZN2v88internal16FLAG_enable_vfp3E
	.section	.bss._ZN2v88internal16FLAG_enable_vfp3E,"aw",@nobits
	.type	_ZN2v88internal16FLAG_enable_vfp3E, @object
	.size	_ZN2v88internal16FLAG_enable_vfp3E, 2
_ZN2v88internal16FLAG_enable_vfp3E:
	.zero	2
	.globl	_ZN2v88internal17FLAG_enable_armv7E
	.section	.bss._ZN2v88internal17FLAG_enable_armv7E,"aw",@nobits
	.type	_ZN2v88internal17FLAG_enable_armv7E, @object
	.size	_ZN2v88internal17FLAG_enable_armv7E, 2
_ZN2v88internal17FLAG_enable_armv7E:
	.zero	2
	.globl	_ZN2v88internal30FLAG_enable_source_at_csa_bindE
	.section	.bss._ZN2v88internal30FLAG_enable_source_at_csa_bindE,"aw",@nobits
	.type	_ZN2v88internal30FLAG_enable_source_at_csa_bindE, @object
	.size	_ZN2v88internal30FLAG_enable_source_at_csa_bindE, 1
_ZN2v88internal30FLAG_enable_source_at_csa_bindE:
	.zero	1
	.globl	_ZN2v88internal26FLAG_partial_constant_poolE
	.section	.data._ZN2v88internal26FLAG_partial_constant_poolE,"aw"
	.type	_ZN2v88internal26FLAG_partial_constant_poolE, @object
	.size	_ZN2v88internal26FLAG_partial_constant_poolE, 1
_ZN2v88internal26FLAG_partial_constant_poolE:
	.byte	1
	.globl	_ZN2v88internal9FLAG_mcpuE
	.section	.data.rel.local._ZN2v88internal9FLAG_mcpuE,"aw"
	.align 8
	.type	_ZN2v88internal9FLAG_mcpuE, @object
	.size	_ZN2v88internal9FLAG_mcpuE, 8
_ZN2v88internal9FLAG_mcpuE:
	.quad	.LC963
	.globl	_ZN2v88internal24FLAG_force_long_branchesE
	.section	.bss._ZN2v88internal24FLAG_force_long_branchesE,"aw",@nobits
	.type	_ZN2v88internal24FLAG_force_long_branchesE, @object
	.size	_ZN2v88internal24FLAG_force_long_branchesE, 1
_ZN2v88internal24FLAG_force_long_branchesE:
	.zero	1
	.globl	_ZN2v88internal13FLAG_arm_archE
	.section	.data.rel.local._ZN2v88internal13FLAG_arm_archE,"aw"
	.align 8
	.type	_ZN2v88internal13FLAG_arm_archE, @object
	.size	_ZN2v88internal13FLAG_arm_archE, 8
_ZN2v88internal13FLAG_arm_archE:
	.quad	.LC964
	.globl	_ZN2v88internal18FLAG_enable_popcntE
	.section	.data._ZN2v88internal18FLAG_enable_popcntE,"aw"
	.type	_ZN2v88internal18FLAG_enable_popcntE, @object
	.size	_ZN2v88internal18FLAG_enable_popcntE, 1
_ZN2v88internal18FLAG_enable_popcntE:
	.byte	1
	.globl	_ZN2v88internal17FLAG_enable_lzcntE
	.section	.data._ZN2v88internal17FLAG_enable_lzcntE,"aw"
	.type	_ZN2v88internal17FLAG_enable_lzcntE, @object
	.size	_ZN2v88internal17FLAG_enable_lzcntE, 1
_ZN2v88internal17FLAG_enable_lzcntE:
	.byte	1
	.globl	_ZN2v88internal16FLAG_enable_bmi2E
	.section	.data._ZN2v88internal16FLAG_enable_bmi2E,"aw"
	.type	_ZN2v88internal16FLAG_enable_bmi2E, @object
	.size	_ZN2v88internal16FLAG_enable_bmi2E, 1
_ZN2v88internal16FLAG_enable_bmi2E:
	.byte	1
	.globl	_ZN2v88internal16FLAG_enable_bmi1E
	.section	.data._ZN2v88internal16FLAG_enable_bmi1E,"aw"
	.type	_ZN2v88internal16FLAG_enable_bmi1E, @object
	.size	_ZN2v88internal16FLAG_enable_bmi1E, 1
_ZN2v88internal16FLAG_enable_bmi1E:
	.byte	1
	.globl	_ZN2v88internal16FLAG_enable_fma3E
	.section	.data._ZN2v88internal16FLAG_enable_fma3E,"aw"
	.type	_ZN2v88internal16FLAG_enable_fma3E, @object
	.size	_ZN2v88internal16FLAG_enable_fma3E, 1
_ZN2v88internal16FLAG_enable_fma3E:
	.byte	1
	.globl	_ZN2v88internal15FLAG_enable_avxE
	.section	.data._ZN2v88internal15FLAG_enable_avxE,"aw"
	.type	_ZN2v88internal15FLAG_enable_avxE, @object
	.size	_ZN2v88internal15FLAG_enable_avxE, 1
_ZN2v88internal15FLAG_enable_avxE:
	.byte	1
	.globl	_ZN2v88internal16FLAG_enable_sahfE
	.section	.data._ZN2v88internal16FLAG_enable_sahfE,"aw"
	.type	_ZN2v88internal16FLAG_enable_sahfE, @object
	.size	_ZN2v88internal16FLAG_enable_sahfE, 1
_ZN2v88internal16FLAG_enable_sahfE:
	.byte	1
	.globl	_ZN2v88internal18FLAG_enable_sse4_2E
	.section	.data._ZN2v88internal18FLAG_enable_sse4_2E,"aw"
	.type	_ZN2v88internal18FLAG_enable_sse4_2E, @object
	.size	_ZN2v88internal18FLAG_enable_sse4_2E, 1
_ZN2v88internal18FLAG_enable_sse4_2E:
	.byte	1
	.globl	_ZN2v88internal18FLAG_enable_sse4_1E
	.section	.data._ZN2v88internal18FLAG_enable_sse4_1E,"aw"
	.type	_ZN2v88internal18FLAG_enable_sse4_1E, @object
	.size	_ZN2v88internal18FLAG_enable_sse4_1E, 1
_ZN2v88internal18FLAG_enable_sse4_1E:
	.byte	1
	.globl	_ZN2v88internal17FLAG_enable_ssse3E
	.section	.data._ZN2v88internal17FLAG_enable_ssse3E,"aw"
	.type	_ZN2v88internal17FLAG_enable_ssse3E, @object
	.size	_ZN2v88internal17FLAG_enable_ssse3E, 1
_ZN2v88internal17FLAG_enable_ssse3E:
	.byte	1
	.globl	_ZN2v88internal16FLAG_enable_sse3E
	.section	.data._ZN2v88internal16FLAG_enable_sse3E,"aw"
	.type	_ZN2v88internal16FLAG_enable_sse3E, @object
	.size	_ZN2v88internal16FLAG_enable_sse3E, 1
_ZN2v88internal16FLAG_enable_sse3E:
	.byte	1
	.globl	_ZN2v88internal18FLAG_code_commentsE
	.section	.bss._ZN2v88internal18FLAG_code_commentsE,"aw",@nobits
	.type	_ZN2v88internal18FLAG_code_commentsE, @object
	.size	_ZN2v88internal18FLAG_code_commentsE, 1
_ZN2v88internal18FLAG_code_commentsE:
	.zero	1
	.globl	_ZN2v88internal15FLAG_debug_codeE
	.section	.bss._ZN2v88internal15FLAG_debug_codeE,"aw",@nobits
	.type	_ZN2v88internal15FLAG_debug_codeE, @object
	.size	_ZN2v88internal15FLAG_debug_codeE, 1
_ZN2v88internal15FLAG_debug_codeE:
	.zero	1
	.globl	_ZN2v88internal23FLAG_idle_time_scavengeE
	.section	.data._ZN2v88internal23FLAG_idle_time_scavengeE,"aw"
	.type	_ZN2v88internal23FLAG_idle_time_scavengeE, @object
	.size	_ZN2v88internal23FLAG_idle_time_scavengeE, 1
_ZN2v88internal23FLAG_idle_time_scavengeE:
	.byte	1
	.globl	_ZN2v88internal35FLAG_young_generation_large_objectsE
	.section	.data._ZN2v88internal35FLAG_young_generation_large_objectsE,"aw"
	.type	_ZN2v88internal35FLAG_young_generation_large_objectsE, @object
	.size	_ZN2v88internal35FLAG_young_generation_large_objectsE, 1
_ZN2v88internal35FLAG_young_generation_large_objectsE:
	.byte	1
	.globl	_ZN2v88internal22FLAG_clear_free_memoryE
	.section	.bss._ZN2v88internal22FLAG_clear_free_memoryE,"aw",@nobits
	.type	_ZN2v88internal22FLAG_clear_free_memoryE, @object
	.size	_ZN2v88internal22FLAG_clear_free_memoryE, 1
_ZN2v88internal22FLAG_clear_free_memoryE:
	.zero	1
	.globl	_ZN2v88internal29FLAG_fast_promotion_new_spaceE
	.section	.bss._ZN2v88internal29FLAG_fast_promotion_new_spaceE,"aw",@nobits
	.type	_ZN2v88internal29FLAG_fast_promotion_new_spaceE, @object
	.size	_ZN2v88internal29FLAG_fast_promotion_new_spaceE, 1
_ZN2v88internal29FLAG_fast_promotion_new_spaceE:
	.zero	1
	.globl	_ZN2v88internal43FLAG_manual_evacuation_candidates_selectionE
	.section	.bss._ZN2v88internal43FLAG_manual_evacuation_candidates_selectionE,"aw",@nobits
	.type	_ZN2v88internal43FLAG_manual_evacuation_candidates_selectionE, @object
	.size	_ZN2v88internal43FLAG_manual_evacuation_candidates_selectionE, 1
_ZN2v88internal43FLAG_manual_evacuation_candidates_selectionE:
	.zero	1
	.globl	_ZN2v88internal20FLAG_disable_abortjsE
	.section	.bss._ZN2v88internal20FLAG_disable_abortjsE,"aw",@nobits
	.type	_ZN2v88internal20FLAG_disable_abortjsE, @object
	.size	_ZN2v88internal20FLAG_disable_abortjsE, 1
_ZN2v88internal20FLAG_disable_abortjsE:
	.zero	1
	.globl	_ZN2v88internal34FLAG_gc_experiment_less_compactionE
	.section	.bss._ZN2v88internal34FLAG_gc_experiment_less_compactionE,"aw",@nobits
	.type	_ZN2v88internal34FLAG_gc_experiment_less_compactionE, @object
	.size	_ZN2v88internal34FLAG_gc_experiment_less_compactionE, 1
_ZN2v88internal34FLAG_gc_experiment_less_compactionE:
	.zero	1
	.globl	_ZN2v88internal38FLAG_gc_experiment_background_scheduleE
	.section	.bss._ZN2v88internal38FLAG_gc_experiment_background_scheduleE,"aw",@nobits
	.type	_ZN2v88internal38FLAG_gc_experiment_background_scheduleE, @object
	.size	_ZN2v88internal38FLAG_gc_experiment_background_scheduleE, 1
_ZN2v88internal38FLAG_gc_experiment_background_scheduleE:
	.zero	1
	.globl	_ZN2v88internal20FLAG_stress_scavengeE
	.section	.bss._ZN2v88internal20FLAG_stress_scavengeE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal20FLAG_stress_scavengeE, @object
	.size	_ZN2v88internal20FLAG_stress_scavengeE, 4
_ZN2v88internal20FLAG_stress_scavengeE:
	.zero	4
	.globl	_ZN2v88internal19FLAG_stress_markingE
	.section	.bss._ZN2v88internal19FLAG_stress_markingE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal19FLAG_stress_markingE, @object
	.size	_ZN2v88internal19FLAG_stress_markingE, 4
_ZN2v88internal19FLAG_stress_markingE:
	.zero	4
	.globl	_ZN2v88internal23FLAG_fuzzer_gc_analysisE
	.section	.bss._ZN2v88internal23FLAG_fuzzer_gc_analysisE,"aw",@nobits
	.type	_ZN2v88internal23FLAG_fuzzer_gc_analysisE, @object
	.size	_ZN2v88internal23FLAG_fuzzer_gc_analysisE, 1
_ZN2v88internal23FLAG_fuzzer_gc_analysisE:
	.zero	1
	.globl	_ZN2v88internal31FLAG_stress_incremental_markingE
	.section	.bss._ZN2v88internal31FLAG_stress_incremental_markingE,"aw",@nobits
	.type	_ZN2v88internal31FLAG_stress_incremental_markingE, @object
	.size	_ZN2v88internal31FLAG_stress_incremental_markingE, 1
_ZN2v88internal31FLAG_stress_incremental_markingE:
	.zero	1
	.globl	_ZN2v88internal29FLAG_stress_compaction_randomE
	.section	.bss._ZN2v88internal29FLAG_stress_compaction_randomE,"aw",@nobits
	.type	_ZN2v88internal29FLAG_stress_compaction_randomE, @object
	.size	_ZN2v88internal29FLAG_stress_compaction_randomE, 1
_ZN2v88internal29FLAG_stress_compaction_randomE:
	.zero	1
	.globl	_ZN2v88internal22FLAG_stress_compactionE
	.section	.bss._ZN2v88internal22FLAG_stress_compactionE,"aw",@nobits
	.type	_ZN2v88internal22FLAG_stress_compactionE, @object
	.size	_ZN2v88internal22FLAG_stress_compactionE, 1
_ZN2v88internal22FLAG_stress_compactionE:
	.zero	1
	.globl	_ZN2v88internal34FLAG_force_marking_deque_overflowsE
	.section	.bss._ZN2v88internal34FLAG_force_marking_deque_overflowsE,"aw",@nobits
	.type	_ZN2v88internal34FLAG_force_marking_deque_overflowsE, @object
	.size	_ZN2v88internal34FLAG_force_marking_deque_overflowsE, 1
_ZN2v88internal34FLAG_force_marking_deque_overflowsE:
	.zero	1
	.globl	_ZN2v88internal29FLAG_use_marking_progress_barE
	.section	.data._ZN2v88internal29FLAG_use_marking_progress_barE,"aw"
	.type	_ZN2v88internal29FLAG_use_marking_progress_barE, @object
	.size	_ZN2v88internal29FLAG_use_marking_progress_barE, 1
_ZN2v88internal29FLAG_use_marking_progress_barE:
	.byte	1
	.globl	_ZN2v88internal26FLAG_stress_flush_bytecodeE
	.section	.bss._ZN2v88internal26FLAG_stress_flush_bytecodeE,"aw",@nobits
	.type	_ZN2v88internal26FLAG_stress_flush_bytecodeE, @object
	.size	_ZN2v88internal26FLAG_stress_flush_bytecodeE, 1
_ZN2v88internal26FLAG_stress_flush_bytecodeE:
	.zero	1
	.globl	_ZN2v88internal19FLAG_flush_bytecodeE
	.section	.data._ZN2v88internal19FLAG_flush_bytecodeE,"aw"
	.type	_ZN2v88internal19FLAG_flush_bytecodeE, @object
	.size	_ZN2v88internal19FLAG_flush_bytecodeE, 1
_ZN2v88internal19FLAG_flush_bytecodeE:
	.byte	1
	.globl	_ZN2v88internal23FLAG_compact_code_spaceE
	.section	.data._ZN2v88internal23FLAG_compact_code_spaceE,"aw"
	.type	_ZN2v88internal23FLAG_compact_code_spaceE, @object
	.size	_ZN2v88internal23FLAG_compact_code_spaceE, 1
_ZN2v88internal23FLAG_compact_code_spaceE:
	.byte	1
	.globl	_ZN2v88internal18FLAG_never_compactE
	.section	.bss._ZN2v88internal18FLAG_never_compactE,"aw",@nobits
	.type	_ZN2v88internal18FLAG_never_compactE, @object
	.size	_ZN2v88internal18FLAG_never_compactE, 1
_ZN2v88internal18FLAG_never_compactE:
	.zero	1
	.globl	_ZN2v88internal19FLAG_always_compactE
	.section	.bss._ZN2v88internal19FLAG_always_compactE,"aw",@nobits
	.type	_ZN2v88internal19FLAG_always_compactE, @object
	.size	_ZN2v88internal19FLAG_always_compactE, 1
_ZN2v88internal19FLAG_always_compactE:
	.zero	1
	.globl	_ZN2v88internal20FLAG_v8_os_page_sizeE
	.section	.bss._ZN2v88internal20FLAG_v8_os_page_sizeE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal20FLAG_v8_os_page_sizeE, @object
	.size	_ZN2v88internal20FLAG_v8_os_page_sizeE, 4
_ZN2v88internal20FLAG_v8_os_page_sizeE:
	.zero	4
	.globl	_ZN2v88internal25FLAG_heap_growing_percentE
	.section	.bss._ZN2v88internal25FLAG_heap_growing_percentE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal25FLAG_heap_growing_percentE, @object
	.size	_ZN2v88internal25FLAG_heap_growing_percentE, 4
_ZN2v88internal25FLAG_heap_growing_percentE:
	.zero	4
	.globl	_ZN2v88internal35FLAG_memory_reducer_for_small_heapsE
	.section	.data._ZN2v88internal35FLAG_memory_reducer_for_small_heapsE,"aw"
	.type	_ZN2v88internal35FLAG_memory_reducer_for_small_heapsE, @object
	.size	_ZN2v88internal35FLAG_memory_reducer_for_small_heapsE, 1
_ZN2v88internal35FLAG_memory_reducer_for_small_heapsE:
	.byte	1
	.globl	_ZN2v88internal19FLAG_memory_reducerE
	.section	.data._ZN2v88internal19FLAG_memory_reducerE,"aw"
	.type	_ZN2v88internal19FLAG_memory_reducerE, @object
	.size	_ZN2v88internal19FLAG_memory_reducerE, 1
_ZN2v88internal19FLAG_memory_reducerE:
	.byte	1
	.globl	_ZN2v88internal22FLAG_move_object_startE
	.section	.data._ZN2v88internal22FLAG_move_object_startE,"aw"
	.type	_ZN2v88internal22FLAG_move_object_startE, @object
	.size	_ZN2v88internal22FLAG_move_object_startE, 1
_ZN2v88internal22FLAG_move_object_startE:
	.byte	1
	.globl	_ZN2v88internal28FLAG_trace_detached_contextsE
	.section	.bss._ZN2v88internal28FLAG_trace_detached_contextsE,"aw",@nobits
	.type	_ZN2v88internal28FLAG_trace_detached_contextsE, @object
	.size	_ZN2v88internal28FLAG_trace_detached_contextsE, 1
_ZN2v88internal28FLAG_trace_detached_contextsE:
	.zero	1
	.globl	_ZN2v88internal28FLAG_track_detached_contextsE
	.section	.data._ZN2v88internal28FLAG_track_detached_contextsE,"aw"
	.type	_ZN2v88internal28FLAG_track_detached_contextsE, @object
	.size	_ZN2v88internal28FLAG_track_detached_contextsE, 1
_ZN2v88internal28FLAG_track_detached_contextsE:
	.byte	1
	.globl	_ZN2v88internal13FLAG_gc_statsE
	.section	.bss._ZN2v88internal13FLAG_gc_statsE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal13FLAG_gc_statsE, @object
	.size	_ZN2v88internal13FLAG_gc_statsE, 4
_ZN2v88internal13FLAG_gc_statsE:
	.zero	4
	.globl	_ZN2v88internal36FLAG_concurrent_array_buffer_freeingE
	.section	.data._ZN2v88internal36FLAG_concurrent_array_buffer_freeingE,"aw"
	.type	_ZN2v88internal36FLAG_concurrent_array_buffer_freeingE, @object
	.size	_ZN2v88internal36FLAG_concurrent_array_buffer_freeingE, 1
_ZN2v88internal36FLAG_concurrent_array_buffer_freeingE:
	.byte	1
	.globl	_ZN2v88internal25FLAG_track_retaining_pathE
	.section	.bss._ZN2v88internal25FLAG_track_retaining_pathE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_track_retaining_pathE, @object
	.size	_ZN2v88internal25FLAG_track_retaining_pathE, 1
_ZN2v88internal25FLAG_track_retaining_pathE:
	.zero	1
	.globl	_ZN2v88internal21FLAG_trace_zone_statsE
	.section	.bss._ZN2v88internal21FLAG_trace_zone_statsE,"aw",@nobits
	.type	_ZN2v88internal21FLAG_trace_zone_statsE, @object
	.size	_ZN2v88internal21FLAG_trace_zone_statsE, 1
_ZN2v88internal21FLAG_trace_zone_statsE:
	.zero	1
	.globl	_ZN2v88internal26FLAG_trace_gc_object_statsE
	.section	.bss._ZN2v88internal26FLAG_trace_gc_object_statsE,"aw",@nobits
	.type	_ZN2v88internal26FLAG_trace_gc_object_statsE, @object
	.size	_ZN2v88internal26FLAG_trace_gc_object_statsE, 1
_ZN2v88internal26FLAG_trace_gc_object_statsE:
	.zero	1
	.globl	_ZN2v88internal26FLAG_track_gc_object_statsE
	.section	.bss._ZN2v88internal26FLAG_track_gc_object_statsE,"aw",@nobits
	.type	_ZN2v88internal26FLAG_track_gc_object_statsE, @object
	.size	_ZN2v88internal26FLAG_track_gc_object_statsE, 1
_ZN2v88internal26FLAG_track_gc_object_statsE:
	.zero	1
	.globl	_ZN2v88internal26FLAG_trace_stress_scavengeE
	.section	.bss._ZN2v88internal26FLAG_trace_stress_scavengeE,"aw",@nobits
	.type	_ZN2v88internal26FLAG_trace_stress_scavengeE, @object
	.size	_ZN2v88internal26FLAG_trace_stress_scavengeE, 1
_ZN2v88internal26FLAG_trace_stress_scavengeE:
	.zero	1
	.globl	_ZN2v88internal25FLAG_trace_stress_markingE
	.section	.bss._ZN2v88internal25FLAG_trace_stress_markingE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_trace_stress_markingE, @object
	.size	_ZN2v88internal25FLAG_trace_stress_markingE, 1
_ZN2v88internal25FLAG_trace_stress_markingE:
	.zero	1
	.globl	_ZN2v88internal30FLAG_trace_incremental_markingE
	.section	.bss._ZN2v88internal30FLAG_trace_incremental_markingE,"aw",@nobits
	.type	_ZN2v88internal30FLAG_trace_incremental_markingE, @object
	.size	_ZN2v88internal30FLAG_trace_incremental_markingE, 1
_ZN2v88internal30FLAG_trace_incremental_markingE:
	.zero	1
	.globl	_ZN2v88internal43FLAG_detect_ineffective_gcs_near_heap_limitE
	.section	.data._ZN2v88internal43FLAG_detect_ineffective_gcs_near_heap_limitE,"aw"
	.type	_ZN2v88internal43FLAG_detect_ineffective_gcs_near_heap_limitE, @object
	.size	_ZN2v88internal43FLAG_detect_ineffective_gcs_near_heap_limitE, 1
_ZN2v88internal43FLAG_detect_ineffective_gcs_near_heap_limitE:
	.byte	1
	.globl	_ZN2v88internal28FLAG_parallel_pointer_updateE
	.section	.data._ZN2v88internal28FLAG_parallel_pointer_updateE,"aw"
	.type	_ZN2v88internal28FLAG_parallel_pointer_updateE, @object
	.size	_ZN2v88internal28FLAG_parallel_pointer_updateE, 1
_ZN2v88internal28FLAG_parallel_pointer_updateE:
	.byte	1
	.globl	_ZN2v88internal24FLAG_parallel_compactionE
	.section	.data._ZN2v88internal24FLAG_parallel_compactionE,"aw"
	.type	_ZN2v88internal24FLAG_parallel_compactionE, @object
	.size	_ZN2v88internal24FLAG_parallel_compactionE, 1
_ZN2v88internal24FLAG_parallel_compactionE:
	.byte	1
	.globl	_ZN2v88internal24FLAG_concurrent_sweepingE
	.section	.data._ZN2v88internal24FLAG_concurrent_sweepingE,"aw"
	.type	_ZN2v88internal24FLAG_concurrent_sweepingE, @object
	.size	_ZN2v88internal24FLAG_concurrent_sweepingE, 1
_ZN2v88internal24FLAG_concurrent_sweepingE:
	.byte	1
	.globl	_ZN2v88internal28FLAG_concurrent_store_bufferE
	.section	.data._ZN2v88internal28FLAG_concurrent_store_bufferE,"aw"
	.type	_ZN2v88internal28FLAG_concurrent_store_bufferE, @object
	.size	_ZN2v88internal28FLAG_concurrent_store_bufferE, 1
_ZN2v88internal28FLAG_concurrent_store_bufferE:
	.byte	1
	.globl	_ZN2v88internal29FLAG_trace_concurrent_markingE
	.section	.bss._ZN2v88internal29FLAG_trace_concurrent_markingE,"aw",@nobits
	.type	_ZN2v88internal29FLAG_trace_concurrent_markingE, @object
	.size	_ZN2v88internal29FLAG_trace_concurrent_markingE, 1
_ZN2v88internal29FLAG_trace_concurrent_markingE:
	.zero	1
	.globl	_ZN2v88internal34FLAG_ephemeron_fixpoint_iterationsE
	.section	.data._ZN2v88internal34FLAG_ephemeron_fixpoint_iterationsE,"aw"
	.align 4
	.type	_ZN2v88internal34FLAG_ephemeron_fixpoint_iterationsE, @object
	.size	_ZN2v88internal34FLAG_ephemeron_fixpoint_iterationsE, 4
_ZN2v88internal34FLAG_ephemeron_fixpoint_iterationsE:
	.long	10
	.globl	_ZN2v88internal21FLAG_parallel_markingE
	.section	.data._ZN2v88internal21FLAG_parallel_markingE,"aw"
	.type	_ZN2v88internal21FLAG_parallel_markingE, @object
	.size	_ZN2v88internal21FLAG_parallel_markingE, 1
_ZN2v88internal21FLAG_parallel_markingE:
	.byte	1
	.globl	_ZN2v88internal23FLAG_concurrent_markingE
	.section	.data._ZN2v88internal23FLAG_concurrent_markingE,"aw"
	.type	_ZN2v88internal23FLAG_concurrent_markingE, @object
	.size	_ZN2v88internal23FLAG_concurrent_markingE, 1
_ZN2v88internal23FLAG_concurrent_markingE:
	.byte	1
	.globl	_ZN2v88internal30FLAG_write_protect_code_memoryE
	.section	.data._ZN2v88internal30FLAG_write_protect_code_memoryE,"aw"
	.type	_ZN2v88internal30FLAG_write_protect_code_memoryE, @object
	.size	_ZN2v88internal30FLAG_write_protect_code_memoryE, 1
_ZN2v88internal30FLAG_write_protect_code_memoryE:
	.byte	1
	.globl	_ZN2v88internal28FLAG_trace_parallel_scavengeE
	.section	.bss._ZN2v88internal28FLAG_trace_parallel_scavengeE,"aw",@nobits
	.type	_ZN2v88internal28FLAG_trace_parallel_scavengeE, @object
	.size	_ZN2v88internal28FLAG_trace_parallel_scavengeE, 1
_ZN2v88internal28FLAG_trace_parallel_scavengeE:
	.zero	1
	.globl	_ZN2v88internal22FLAG_parallel_scavengeE
	.section	.data._ZN2v88internal22FLAG_parallel_scavengeE,"aw"
	.type	_ZN2v88internal22FLAG_parallel_scavengeE, @object
	.size	_ZN2v88internal22FLAG_parallel_scavengeE, 1
_ZN2v88internal22FLAG_parallel_scavengeE:
	.byte	1
	.globl	_ZN2v88internal19FLAG_trace_unmapperE
	.section	.bss._ZN2v88internal19FLAG_trace_unmapperE,"aw",@nobits
	.type	_ZN2v88internal19FLAG_trace_unmapperE, @object
	.size	_ZN2v88internal19FLAG_trace_unmapperE, 1
_ZN2v88internal19FLAG_trace_unmapperE:
	.zero	1
	.globl	_ZN2v88internal33FLAG_incremental_marking_wrappersE
	.section	.data._ZN2v88internal33FLAG_incremental_marking_wrappersE,"aw"
	.type	_ZN2v88internal33FLAG_incremental_marking_wrappersE, @object
	.size	_ZN2v88internal33FLAG_incremental_marking_wrappersE, 1
_ZN2v88internal33FLAG_incremental_marking_wrappersE:
	.byte	1
	.globl	_ZN2v88internal24FLAG_incremental_markingE
	.section	.data._ZN2v88internal24FLAG_incremental_markingE,"aw"
	.type	_ZN2v88internal24FLAG_incremental_markingE, @object
	.size	_ZN2v88internal24FLAG_incremental_markingE, 1
_ZN2v88internal24FLAG_incremental_markingE:
	.byte	1
	.globl	_ZN2v88internal30FLAG_trace_mutator_utilizationE
	.section	.bss._ZN2v88internal30FLAG_trace_mutator_utilizationE,"aw",@nobits
	.type	_ZN2v88internal30FLAG_trace_mutator_utilizationE, @object
	.size	_ZN2v88internal30FLAG_trace_mutator_utilizationE, 1
_ZN2v88internal30FLAG_trace_mutator_utilizationE:
	.zero	1
	.globl	_ZN2v88internal21FLAG_trace_evacuationE
	.section	.bss._ZN2v88internal21FLAG_trace_evacuationE,"aw",@nobits
	.type	_ZN2v88internal21FLAG_trace_evacuationE, @object
	.size	_ZN2v88internal21FLAG_trace_evacuationE, 1
_ZN2v88internal21FLAG_trace_evacuationE:
	.zero	1
	.globl	_ZN2v88internal32FLAG_trace_fragmentation_verboseE
	.section	.bss._ZN2v88internal32FLAG_trace_fragmentation_verboseE,"aw",@nobits
	.type	_ZN2v88internal32FLAG_trace_fragmentation_verboseE, @object
	.size	_ZN2v88internal32FLAG_trace_fragmentation_verboseE, 1
_ZN2v88internal32FLAG_trace_fragmentation_verboseE:
	.zero	1
	.globl	_ZN2v88internal24FLAG_trace_fragmentationE
	.section	.bss._ZN2v88internal24FLAG_trace_fragmentationE,"aw",@nobits
	.type	_ZN2v88internal24FLAG_trace_fragmentationE, @object
	.size	_ZN2v88internal24FLAG_trace_fragmentationE, 1
_ZN2v88internal24FLAG_trace_fragmentationE:
	.zero	1
	.globl	_ZN2v88internal33FLAG_trace_duplicate_threshold_kbE
	.section	.bss._ZN2v88internal33FLAG_trace_duplicate_threshold_kbE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal33FLAG_trace_duplicate_threshold_kbE, @object
	.size	_ZN2v88internal33FLAG_trace_duplicate_threshold_kbE, 4
_ZN2v88internal33FLAG_trace_duplicate_threshold_kbE:
	.zero	4
	.globl	_ZN2v88internal36FLAG_trace_allocation_stack_intervalE
	.section	.data._ZN2v88internal36FLAG_trace_allocation_stack_intervalE,"aw"
	.align 4
	.type	_ZN2v88internal36FLAG_trace_allocation_stack_intervalE, @object
	.size	_ZN2v88internal36FLAG_trace_allocation_stack_intervalE, 4
_ZN2v88internal36FLAG_trace_allocation_stack_intervalE:
	.long	-1
	.globl	_ZN2v88internal25FLAG_gc_freelist_strategyE
	.section	.data._ZN2v88internal25FLAG_gc_freelist_strategyE,"aw"
	.align 4
	.type	_ZN2v88internal25FLAG_gc_freelist_strategyE, @object
	.size	_ZN2v88internal25FLAG_gc_freelist_strategyE, 4
_ZN2v88internal25FLAG_gc_freelist_strategyE:
	.long	5
	.globl	_ZN2v88internal30FLAG_trace_allocations_originsE
	.section	.bss._ZN2v88internal30FLAG_trace_allocations_originsE,"aw",@nobits
	.type	_ZN2v88internal30FLAG_trace_allocations_originsE, @object
	.size	_ZN2v88internal30FLAG_trace_allocations_originsE, 1
_ZN2v88internal30FLAG_trace_allocations_originsE:
	.zero	1
	.globl	_ZN2v88internal32FLAG_trace_evacuation_candidatesE
	.section	.bss._ZN2v88internal32FLAG_trace_evacuation_candidatesE,"aw",@nobits
	.type	_ZN2v88internal32FLAG_trace_evacuation_candidatesE, @object
	.size	_ZN2v88internal32FLAG_trace_evacuation_candidatesE, 1
_ZN2v88internal32FLAG_trace_evacuation_candidatesE:
	.zero	1
	.globl	_ZN2v88internal31FLAG_trace_gc_freelists_verboseE
	.section	.bss._ZN2v88internal31FLAG_trace_gc_freelists_verboseE,"aw",@nobits
	.type	_ZN2v88internal31FLAG_trace_gc_freelists_verboseE, @object
	.size	_ZN2v88internal31FLAG_trace_gc_freelists_verboseE, 1
_ZN2v88internal31FLAG_trace_gc_freelists_verboseE:
	.zero	1
	.globl	_ZN2v88internal23FLAG_trace_gc_freelistsE
	.section	.bss._ZN2v88internal23FLAG_trace_gc_freelistsE,"aw",@nobits
	.type	_ZN2v88internal23FLAG_trace_gc_freelistsE, @object
	.size	_ZN2v88internal23FLAG_trace_gc_freelistsE, 1
_ZN2v88internal23FLAG_trace_gc_freelistsE:
	.zero	1
	.globl	_ZN2v88internal21FLAG_trace_gc_verboseE
	.section	.bss._ZN2v88internal21FLAG_trace_gc_verboseE,"aw",@nobits
	.type	_ZN2v88internal21FLAG_trace_gc_verboseE, @object
	.size	_ZN2v88internal21FLAG_trace_gc_verboseE, 1
_ZN2v88internal21FLAG_trace_gc_verboseE:
	.zero	1
	.globl	_ZN2v88internal36FLAG_trace_idle_notification_verboseE
	.section	.bss._ZN2v88internal36FLAG_trace_idle_notification_verboseE,"aw",@nobits
	.type	_ZN2v88internal36FLAG_trace_idle_notification_verboseE, @object
	.size	_ZN2v88internal36FLAG_trace_idle_notification_verboseE, 1
_ZN2v88internal36FLAG_trace_idle_notification_verboseE:
	.zero	1
	.globl	_ZN2v88internal28FLAG_trace_idle_notificationE
	.section	.bss._ZN2v88internal28FLAG_trace_idle_notificationE,"aw",@nobits
	.type	_ZN2v88internal28FLAG_trace_idle_notificationE, @object
	.size	_ZN2v88internal28FLAG_trace_idle_notificationE, 1
_ZN2v88internal28FLAG_trace_idle_notificationE:
	.zero	1
	.globl	_ZN2v88internal30FLAG_trace_gc_ignore_scavengerE
	.section	.bss._ZN2v88internal30FLAG_trace_gc_ignore_scavengerE,"aw",@nobits
	.type	_ZN2v88internal30FLAG_trace_gc_ignore_scavengerE, @object
	.size	_ZN2v88internal30FLAG_trace_gc_ignore_scavengerE, 1
_ZN2v88internal30FLAG_trace_gc_ignore_scavengerE:
	.zero	1
	.globl	_ZN2v88internal17FLAG_trace_gc_nvpE
	.section	.bss._ZN2v88internal17FLAG_trace_gc_nvpE,"aw",@nobits
	.type	_ZN2v88internal17FLAG_trace_gc_nvpE, @object
	.size	_ZN2v88internal17FLAG_trace_gc_nvpE, 1
_ZN2v88internal17FLAG_trace_gc_nvpE:
	.zero	1
	.globl	_ZN2v88internal13FLAG_trace_gcE
	.section	.bss._ZN2v88internal13FLAG_trace_gcE,"aw",@nobits
	.type	_ZN2v88internal13FLAG_trace_gcE, @object
	.size	_ZN2v88internal13FLAG_trace_gcE, 1
_ZN2v88internal13FLAG_trace_gcE:
	.zero	1
	.globl	_ZN2v88internal25FLAG_retain_maps_for_n_gcE
	.section	.data._ZN2v88internal25FLAG_retain_maps_for_n_gcE,"aw"
	.align 4
	.type	_ZN2v88internal25FLAG_retain_maps_for_n_gcE, @object
	.size	_ZN2v88internal25FLAG_retain_maps_for_n_gcE, 4
_ZN2v88internal25FLAG_retain_maps_for_n_gcE:
	.long	2
	.globl	_ZN2v88internal16FLAG_gc_intervalE
	.section	.data._ZN2v88internal16FLAG_gc_intervalE,"aw"
	.align 4
	.type	_ZN2v88internal16FLAG_gc_intervalE, @object
	.size	_ZN2v88internal16FLAG_gc_intervalE, 4
_ZN2v88internal16FLAG_gc_intervalE:
	.long	-1
	.globl	_ZN2v88internal23FLAG_random_gc_intervalE
	.section	.bss._ZN2v88internal23FLAG_random_gc_intervalE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal23FLAG_random_gc_intervalE, @object
	.size	_ZN2v88internal23FLAG_random_gc_intervalE, 4
_ZN2v88internal23FLAG_random_gc_intervalE:
	.zero	4
	.globl	_ZN2v88internal14FLAG_gc_globalE
	.section	.bss._ZN2v88internal14FLAG_gc_globalE,"aw",@nobits
	.type	_ZN2v88internal14FLAG_gc_globalE, @object
	.size	_ZN2v88internal14FLAG_gc_globalE, 1
_ZN2v88internal14FLAG_gc_globalE:
	.zero	1
	.globl	_ZN2v88internal25FLAG_global_gc_schedulingE
	.section	.data._ZN2v88internal25FLAG_global_gc_schedulingE,"aw"
	.type	_ZN2v88internal25FLAG_global_gc_schedulingE, @object
	.size	_ZN2v88internal25FLAG_global_gc_schedulingE, 1
_ZN2v88internal25FLAG_global_gc_schedulingE:
	.byte	1
	.globl	_ZN2v88internal27FLAG_initial_old_space_sizeE
	.section	.bss._ZN2v88internal27FLAG_initial_old_space_sizeE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal27FLAG_initial_old_space_sizeE, @object
	.size	_ZN2v88internal27FLAG_initial_old_space_sizeE, 8
_ZN2v88internal27FLAG_initial_old_space_sizeE:
	.zero	8
	.globl	_ZN2v88internal33FLAG_huge_max_old_generation_sizeE
	.section	.bss._ZN2v88internal33FLAG_huge_max_old_generation_sizeE,"aw",@nobits
	.type	_ZN2v88internal33FLAG_huge_max_old_generation_sizeE, @object
	.size	_ZN2v88internal33FLAG_huge_max_old_generation_sizeE, 1
_ZN2v88internal33FLAG_huge_max_old_generation_sizeE:
	.zero	1
	.globl	_ZN2v88internal22FLAG_initial_heap_sizeE
	.section	.bss._ZN2v88internal22FLAG_initial_heap_sizeE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal22FLAG_initial_heap_sizeE, @object
	.size	_ZN2v88internal22FLAG_initial_heap_sizeE, 8
_ZN2v88internal22FLAG_initial_heap_sizeE:
	.zero	8
	.globl	_ZN2v88internal18FLAG_max_heap_sizeE
	.section	.bss._ZN2v88internal18FLAG_max_heap_sizeE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal18FLAG_max_heap_sizeE, @object
	.size	_ZN2v88internal18FLAG_max_heap_sizeE, 8
_ZN2v88internal18FLAG_max_heap_sizeE:
	.zero	8
	.globl	_ZN2v88internal23FLAG_max_old_space_sizeE
	.section	.bss._ZN2v88internal23FLAG_max_old_space_sizeE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal23FLAG_max_old_space_sizeE, @object
	.size	_ZN2v88internal23FLAG_max_old_space_sizeE, 8
_ZN2v88internal23FLAG_max_old_space_sizeE:
	.zero	8
	.globl	_ZN2v88internal44FLAG_experimental_new_space_growth_heuristicE
	.section	.bss._ZN2v88internal44FLAG_experimental_new_space_growth_heuristicE,"aw",@nobits
	.type	_ZN2v88internal44FLAG_experimental_new_space_growth_heuristicE, @object
	.size	_ZN2v88internal44FLAG_experimental_new_space_growth_heuristicE, 1
_ZN2v88internal44FLAG_experimental_new_space_growth_heuristicE:
	.zero	1
	.globl	_ZN2v88internal29FLAG_semi_space_growth_factorE
	.section	.data._ZN2v88internal29FLAG_semi_space_growth_factorE,"aw"
	.align 4
	.type	_ZN2v88internal29FLAG_semi_space_growth_factorE, @object
	.size	_ZN2v88internal29FLAG_semi_space_growth_factorE, 4
_ZN2v88internal29FLAG_semi_space_growth_factorE:
	.long	2
	.globl	_ZN2v88internal24FLAG_max_semi_space_sizeE
	.section	.bss._ZN2v88internal24FLAG_max_semi_space_sizeE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal24FLAG_max_semi_space_sizeE, @object
	.size	_ZN2v88internal24FLAG_max_semi_space_sizeE, 8
_ZN2v88internal24FLAG_max_semi_space_sizeE:
	.zero	8
	.globl	_ZN2v88internal24FLAG_min_semi_space_sizeE
	.section	.bss._ZN2v88internal24FLAG_min_semi_space_sizeE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal24FLAG_min_semi_space_sizeE, @object
	.size	_ZN2v88internal24FLAG_min_semi_space_sizeE, 8
_ZN2v88internal24FLAG_min_semi_space_sizeE:
	.zero	8
	.globl	_ZN2v88internal40FLAG_stress_sampling_allocation_profilerE
	.section	.bss._ZN2v88internal40FLAG_stress_sampling_allocation_profilerE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal40FLAG_stress_sampling_allocation_profilerE, @object
	.size	_ZN2v88internal40FLAG_stress_sampling_allocation_profilerE, 4
_ZN2v88internal40FLAG_stress_sampling_allocation_profilerE:
	.zero	4
	.globl	_ZN2v88internal16FLAG_frame_countE
	.section	.data._ZN2v88internal16FLAG_frame_countE,"aw"
	.align 4
	.type	_ZN2v88internal16FLAG_frame_countE, @object
	.size	_ZN2v88internal16FLAG_frame_countE, 4
_ZN2v88internal16FLAG_frame_countE:
	.long	1
	.globl	_ZN2v88internal24FLAG_stress_wasm_code_gcE
	.section	.bss._ZN2v88internal24FLAG_stress_wasm_code_gcE,"aw",@nobits
	.type	_ZN2v88internal24FLAG_stress_wasm_code_gcE, @object
	.size	_ZN2v88internal24FLAG_stress_wasm_code_gcE, 1
_ZN2v88internal24FLAG_stress_wasm_code_gcE:
	.zero	1
	.globl	_ZN2v88internal23FLAG_trace_wasm_code_gcE
	.section	.bss._ZN2v88internal23FLAG_trace_wasm_code_gcE,"aw",@nobits
	.type	_ZN2v88internal23FLAG_trace_wasm_code_gcE, @object
	.size	_ZN2v88internal23FLAG_trace_wasm_code_gcE, 1
_ZN2v88internal23FLAG_trace_wasm_code_gcE:
	.zero	1
	.globl	_ZN2v88internal17FLAG_wasm_code_gcE
	.section	.data._ZN2v88internal17FLAG_wasm_code_gcE,"aw"
	.type	_ZN2v88internal17FLAG_wasm_code_gcE, @object
	.size	_ZN2v88internal17FLAG_wasm_code_gcE, 1
_ZN2v88internal17FLAG_wasm_code_gcE:
	.byte	1
	.globl	_ZN2v88internal25FLAG_wasm_lazy_validationE
	.section	.bss._ZN2v88internal25FLAG_wasm_lazy_validationE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_wasm_lazy_validationE, @object
	.size	_ZN2v88internal25FLAG_wasm_lazy_validationE, 1
_ZN2v88internal25FLAG_wasm_lazy_validationE:
	.zero	1
	.globl	_ZN2v88internal28FLAG_wasm_grow_shared_memoryE
	.section	.bss._ZN2v88internal28FLAG_wasm_grow_shared_memoryE,"aw",@nobits
	.type	_ZN2v88internal28FLAG_wasm_grow_shared_memoryE, @object
	.size	_ZN2v88internal28FLAG_wasm_grow_shared_memoryE, 1
_ZN2v88internal28FLAG_wasm_grow_shared_memoryE:
	.zero	1
	.globl	_ZN2v88internal26FLAG_wasm_lazy_compilationE
	.section	.bss._ZN2v88internal26FLAG_wasm_lazy_compilationE,"aw",@nobits
	.type	_ZN2v88internal26FLAG_wasm_lazy_compilationE, @object
	.size	_ZN2v88internal26FLAG_wasm_lazy_compilationE, 1
_ZN2v88internal26FLAG_wasm_lazy_compilationE:
	.zero	1
	.globl	_ZN2v88internal30FLAG_asm_wasm_lazy_compilationE
	.section	.bss._ZN2v88internal30FLAG_asm_wasm_lazy_compilationE,"aw",@nobits
	.type	_ZN2v88internal30FLAG_asm_wasm_lazy_compilationE, @object
	.size	_ZN2v88internal30FLAG_asm_wasm_lazy_compilationE, 1
_ZN2v88internal30FLAG_asm_wasm_lazy_compilationE:
	.zero	1
	.globl	_ZN2v88internal23FLAG_wasm_interpret_allE
	.section	.bss._ZN2v88internal23FLAG_wasm_interpret_allE,"aw",@nobits
	.type	_ZN2v88internal23FLAG_wasm_interpret_allE, @object
	.size	_ZN2v88internal23FLAG_wasm_interpret_allE, 1
_ZN2v88internal23FLAG_wasm_interpret_allE:
	.zero	1
	.globl	_ZN2v88internal25FLAG_print_wasm_stub_codeE
	.section	.bss._ZN2v88internal25FLAG_print_wasm_stub_codeE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_print_wasm_stub_codeE, @object
	.size	_ZN2v88internal25FLAG_print_wasm_stub_codeE, 1
_ZN2v88internal25FLAG_print_wasm_stub_codeE:
	.zero	1
	.globl	_ZN2v88internal20FLAG_print_wasm_codeE
	.section	.bss._ZN2v88internal20FLAG_print_wasm_codeE,"aw",@nobits
	.type	_ZN2v88internal20FLAG_print_wasm_codeE, @object
	.size	_ZN2v88internal20FLAG_print_wasm_codeE, 1
_ZN2v88internal20FLAG_print_wasm_codeE:
	.zero	1
	.globl	_ZN2v88internal25FLAG_wasm_fuzzer_gen_testE
	.section	.bss._ZN2v88internal25FLAG_wasm_fuzzer_gen_testE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_wasm_fuzzer_gen_testE, @object
	.size	_ZN2v88internal25FLAG_wasm_fuzzer_gen_testE, 1
_ZN2v88internal25FLAG_wasm_fuzzer_gen_testE:
	.zero	1
	.globl	_ZN2v88internal22FLAG_wasm_trap_handlerE
	.section	.data._ZN2v88internal22FLAG_wasm_trap_handlerE,"aw"
	.type	_ZN2v88internal22FLAG_wasm_trap_handlerE, @object
	.size	_ZN2v88internal22FLAG_wasm_trap_handlerE, 1
_ZN2v88internal22FLAG_wasm_trap_handlerE:
	.byte	1
	.globl	_ZN2v88internal21FLAG_wasm_shared_codeE
	.section	.data._ZN2v88internal21FLAG_wasm_shared_codeE,"aw"
	.type	_ZN2v88internal21FLAG_wasm_shared_codeE, @object
	.size	_ZN2v88internal21FLAG_wasm_shared_codeE, 1
_ZN2v88internal21FLAG_wasm_shared_codeE:
	.byte	1
	.globl	_ZN2v88internal23FLAG_wasm_shared_engineE
	.section	.data._ZN2v88internal23FLAG_wasm_shared_engineE,"aw"
	.type	_ZN2v88internal23FLAG_wasm_shared_engineE, @object
	.size	_ZN2v88internal23FLAG_wasm_shared_engineE, 1
_ZN2v88internal23FLAG_wasm_shared_engineE:
	.byte	1
	.globl	_ZN2v88internal25FLAG_wasm_math_intrinsicsE
	.section	.data._ZN2v88internal25FLAG_wasm_math_intrinsicsE,"aw"
	.type	_ZN2v88internal25FLAG_wasm_math_intrinsicsE, @object
	.size	_ZN2v88internal25FLAG_wasm_math_intrinsicsE, 1
_ZN2v88internal25FLAG_wasm_math_intrinsicsE:
	.byte	1
	.globl	_ZN2v88internal25FLAG_wasm_no_stack_checksE
	.section	.bss._ZN2v88internal25FLAG_wasm_no_stack_checksE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_wasm_no_stack_checksE, @object
	.size	_ZN2v88internal25FLAG_wasm_no_stack_checksE, 1
_ZN2v88internal25FLAG_wasm_no_stack_checksE:
	.zero	1
	.globl	_ZN2v88internal26FLAG_wasm_no_bounds_checksE
	.section	.bss._ZN2v88internal26FLAG_wasm_no_bounds_checksE,"aw",@nobits
	.type	_ZN2v88internal26FLAG_wasm_no_bounds_checksE, @object
	.size	_ZN2v88internal26FLAG_wasm_no_bounds_checksE, 1
_ZN2v88internal26FLAG_wasm_no_bounds_checksE:
	.zero	1
	.globl	_ZN2v88internal13FLAG_wasm_optE
	.section	.bss._ZN2v88internal13FLAG_wasm_optE,"aw",@nobits
	.type	_ZN2v88internal13FLAG_wasm_optE, @object
	.size	_ZN2v88internal13FLAG_wasm_optE, 1
_ZN2v88internal13FLAG_wasm_optE:
	.zero	1
	.globl	_ZN2v88internal17FLAG_wasm_stagingE
	.section	.bss._ZN2v88internal17FLAG_wasm_stagingE,"aw",@nobits
	.type	_ZN2v88internal17FLAG_wasm_stagingE, @object
	.size	_ZN2v88internal17FLAG_wasm_stagingE, 1
_ZN2v88internal17FLAG_wasm_stagingE:
	.zero	1
	.globl	_ZN2v88internal25FLAG_experimental_wasm_seE
	.section	.data._ZN2v88internal25FLAG_experimental_wasm_seE,"aw"
	.type	_ZN2v88internal25FLAG_experimental_wasm_seE, @object
	.size	_ZN2v88internal25FLAG_experimental_wasm_seE, 1
_ZN2v88internal25FLAG_experimental_wasm_seE:
	.byte	1
	.globl	_ZN2v88internal42FLAG_experimental_wasm_sat_f2i_conversionsE
	.section	.data._ZN2v88internal42FLAG_experimental_wasm_sat_f2i_conversionsE,"aw"
	.type	_ZN2v88internal42FLAG_experimental_wasm_sat_f2i_conversionsE, @object
	.size	_ZN2v88internal42FLAG_experimental_wasm_sat_f2i_conversionsE, 1
_ZN2v88internal42FLAG_experimental_wasm_sat_f2i_conversionsE:
	.byte	1
	.globl	_ZN2v88internal34FLAG_experimental_wasm_bulk_memoryE
	.section	.data._ZN2v88internal34FLAG_experimental_wasm_bulk_memoryE,"aw"
	.type	_ZN2v88internal34FLAG_experimental_wasm_bulk_memoryE, @object
	.size	_ZN2v88internal34FLAG_experimental_wasm_bulk_memoryE, 1
_ZN2v88internal34FLAG_experimental_wasm_bulk_memoryE:
	.byte	1
	.globl	_ZN2v88internal38FLAG_experimental_wasm_type_reflectionE
	.section	.bss._ZN2v88internal38FLAG_experimental_wasm_type_reflectionE,"aw",@nobits
	.type	_ZN2v88internal38FLAG_experimental_wasm_type_reflectionE, @object
	.size	_ZN2v88internal38FLAG_experimental_wasm_type_reflectionE, 1
_ZN2v88internal38FLAG_experimental_wasm_type_reflectionE:
	.zero	1
	.globl	_ZN2v88internal29FLAG_experimental_wasm_anyrefE
	.section	.bss._ZN2v88internal29FLAG_experimental_wasm_anyrefE,"aw",@nobits
	.type	_ZN2v88internal29FLAG_experimental_wasm_anyrefE, @object
	.size	_ZN2v88internal29FLAG_experimental_wasm_anyrefE, 1
_ZN2v88internal29FLAG_experimental_wasm_anyrefE:
	.zero	1
	.globl	_ZN2v88internal40FLAG_experimental_wasm_compilation_hintsE
	.section	.bss._ZN2v88internal40FLAG_experimental_wasm_compilation_hintsE,"aw",@nobits
	.type	_ZN2v88internal40FLAG_experimental_wasm_compilation_hintsE, @object
	.size	_ZN2v88internal40FLAG_experimental_wasm_compilation_hintsE, 1
_ZN2v88internal40FLAG_experimental_wasm_compilation_hintsE:
	.zero	1
	.globl	_ZN2v88internal34FLAG_experimental_wasm_return_callE
	.section	.bss._ZN2v88internal34FLAG_experimental_wasm_return_callE,"aw",@nobits
	.type	_ZN2v88internal34FLAG_experimental_wasm_return_callE, @object
	.size	_ZN2v88internal34FLAG_experimental_wasm_return_callE, 1
_ZN2v88internal34FLAG_experimental_wasm_return_callE:
	.zero	1
	.globl	_ZN2v88internal29FLAG_experimental_wasm_bigintE
	.section	.bss._ZN2v88internal29FLAG_experimental_wasm_bigintE,"aw",@nobits
	.type	_ZN2v88internal29FLAG_experimental_wasm_bigintE, @object
	.size	_ZN2v88internal29FLAG_experimental_wasm_bigintE, 1
_ZN2v88internal29FLAG_experimental_wasm_bigintE:
	.zero	1
	.globl	_ZN2v88internal27FLAG_experimental_wasm_simdE
	.section	.bss._ZN2v88internal27FLAG_experimental_wasm_simdE,"aw",@nobits
	.type	_ZN2v88internal27FLAG_experimental_wasm_simdE, @object
	.size	_ZN2v88internal27FLAG_experimental_wasm_simdE, 1
_ZN2v88internal27FLAG_experimental_wasm_simdE:
	.zero	1
	.globl	_ZN2v88internal30FLAG_experimental_wasm_threadsE
	.section	.bss._ZN2v88internal30FLAG_experimental_wasm_threadsE,"aw",@nobits
	.type	_ZN2v88internal30FLAG_experimental_wasm_threadsE, @object
	.size	_ZN2v88internal30FLAG_experimental_wasm_threadsE, 1
_ZN2v88internal30FLAG_experimental_wasm_threadsE:
	.zero	1
	.globl	_ZN2v88internal25FLAG_experimental_wasm_ehE
	.section	.bss._ZN2v88internal25FLAG_experimental_wasm_ehE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_experimental_wasm_ehE, @object
	.size	_ZN2v88internal25FLAG_experimental_wasm_ehE, 1
_ZN2v88internal25FLAG_experimental_wasm_ehE:
	.zero	1
	.globl	_ZN2v88internal25FLAG_experimental_wasm_mvE
	.section	.bss._ZN2v88internal25FLAG_experimental_wasm_mvE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_experimental_wasm_mvE, @object
	.size	_ZN2v88internal25FLAG_experimental_wasm_mvE, 1
_ZN2v88internal25FLAG_experimental_wasm_mvE:
	.zero	1
	.globl	_ZN2v88internal26FLAG_dump_wasm_module_pathE
	.section	.bss._ZN2v88internal26FLAG_dump_wasm_module_pathE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal26FLAG_dump_wasm_module_pathE, @object
	.size	_ZN2v88internal26FLAG_dump_wasm_module_pathE, 8
_ZN2v88internal26FLAG_dump_wasm_module_pathE:
	.zero	8
	.globl	_ZN2v88internal24FLAG_stress_validate_asmE
	.section	.bss._ZN2v88internal24FLAG_stress_validate_asmE,"aw",@nobits
	.type	_ZN2v88internal24FLAG_stress_validate_asmE, @object
	.size	_ZN2v88internal24FLAG_stress_validate_asmE, 1
_ZN2v88internal24FLAG_stress_validate_asmE:
	.zero	1
	.globl	_ZN2v88internal21FLAG_trace_asm_parserE
	.section	.bss._ZN2v88internal21FLAG_trace_asm_parserE,"aw",@nobits
	.type	_ZN2v88internal21FLAG_trace_asm_parserE, @object
	.size	_ZN2v88internal21FLAG_trace_asm_parserE, 1
_ZN2v88internal21FLAG_trace_asm_parserE:
	.zero	1
	.globl	_ZN2v88internal22FLAG_trace_asm_scannerE
	.section	.bss._ZN2v88internal22FLAG_trace_asm_scannerE,"aw",@nobits
	.type	_ZN2v88internal22FLAG_trace_asm_scannerE, @object
	.size	_ZN2v88internal22FLAG_trace_asm_scannerE, 1
_ZN2v88internal22FLAG_trace_asm_scannerE:
	.zero	1
	.globl	_ZN2v88internal19FLAG_trace_asm_timeE
	.section	.bss._ZN2v88internal19FLAG_trace_asm_timeE,"aw",@nobits
	.type	_ZN2v88internal19FLAG_trace_asm_timeE, @object
	.size	_ZN2v88internal19FLAG_trace_asm_timeE, 1
_ZN2v88internal19FLAG_trace_asm_timeE:
	.zero	1
	.globl	_ZN2v88internal26FLAG_suppress_asm_messagesE
	.section	.bss._ZN2v88internal26FLAG_suppress_asm_messagesE,"aw",@nobits
	.type	_ZN2v88internal26FLAG_suppress_asm_messagesE, @object
	.size	_ZN2v88internal26FLAG_suppress_asm_messagesE, 1
_ZN2v88internal26FLAG_suppress_asm_messagesE:
	.zero	1
	.globl	_ZN2v88internal17FLAG_validate_asmE
	.section	.data._ZN2v88internal17FLAG_validate_asmE,"aw"
	.type	_ZN2v88internal17FLAG_validate_asmE, @object
	.size	_ZN2v88internal17FLAG_validate_asmE, 1
_ZN2v88internal17FLAG_validate_asmE:
	.byte	1
	.globl	_ZN2v88internal31FLAG_wasm_tier_mask_for_testingE
	.section	.bss._ZN2v88internal31FLAG_wasm_tier_mask_for_testingE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal31FLAG_wasm_tier_mask_for_testingE, @object
	.size	_ZN2v88internal31FLAG_wasm_tier_mask_for_testingE, 4
_ZN2v88internal31FLAG_wasm_tier_mask_for_testingE:
	.zero	4
	.globl	_ZN2v88internal22FLAG_trace_wasm_memoryE
	.section	.bss._ZN2v88internal22FLAG_trace_wasm_memoryE,"aw",@nobits
	.type	_ZN2v88internal22FLAG_trace_wasm_memoryE, @object
	.size	_ZN2v88internal22FLAG_trace_wasm_memoryE, 1
_ZN2v88internal22FLAG_trace_wasm_memoryE:
	.zero	1
	.globl	_ZN2v88internal12FLAG_liftoffE
	.section	.bss._ZN2v88internal12FLAG_liftoffE,"aw",@nobits
	.type	_ZN2v88internal12FLAG_liftoffE, @object
	.size	_ZN2v88internal12FLAG_liftoffE, 1
_ZN2v88internal12FLAG_liftoffE:
	.zero	1
	.globl	_ZN2v88internal23FLAG_trace_wasm_ast_endE
	.section	.bss._ZN2v88internal23FLAG_trace_wasm_ast_endE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal23FLAG_trace_wasm_ast_endE, @object
	.size	_ZN2v88internal23FLAG_trace_wasm_ast_endE, 4
_ZN2v88internal23FLAG_trace_wasm_ast_endE:
	.zero	4
	.globl	_ZN2v88internal25FLAG_trace_wasm_ast_startE
	.section	.bss._ZN2v88internal25FLAG_trace_wasm_ast_startE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal25FLAG_trace_wasm_ast_startE, @object
	.size	_ZN2v88internal25FLAG_trace_wasm_ast_startE, 4
_ZN2v88internal25FLAG_trace_wasm_ast_startE:
	.zero	4
	.globl	_ZN2v88internal17FLAG_wasm_tier_upE
	.section	.data._ZN2v88internal17FLAG_wasm_tier_upE,"aw"
	.type	_ZN2v88internal17FLAG_wasm_tier_upE, @object
	.size	_ZN2v88internal17FLAG_wasm_tier_upE, 1
_ZN2v88internal17FLAG_wasm_tier_upE:
	.byte	1
	.globl	_ZN2v88internal24FLAG_wasm_max_code_spaceE
	.section	.data._ZN2v88internal24FLAG_wasm_max_code_spaceE,"aw"
	.align 4
	.type	_ZN2v88internal24FLAG_wasm_max_code_spaceE, @object
	.size	_ZN2v88internal24FLAG_wasm_max_code_spaceE, 4
_ZN2v88internal24FLAG_wasm_max_code_spaceE:
	.long	1024
	.globl	_ZN2v88internal24FLAG_wasm_max_table_sizeE
	.section	.data._ZN2v88internal24FLAG_wasm_max_table_sizeE,"aw"
	.align 4
	.type	_ZN2v88internal24FLAG_wasm_max_table_sizeE, @object
	.size	_ZN2v88internal24FLAG_wasm_max_table_sizeE, 4
_ZN2v88internal24FLAG_wasm_max_table_sizeE:
	.long	10000000
	.globl	_ZN2v88internal23FLAG_wasm_max_mem_pagesE
	.section	.data._ZN2v88internal23FLAG_wasm_max_mem_pagesE,"aw"
	.align 4
	.type	_ZN2v88internal23FLAG_wasm_max_mem_pagesE, @object
	.size	_ZN2v88internal23FLAG_wasm_max_mem_pagesE, 4
_ZN2v88internal23FLAG_wasm_max_mem_pagesE:
	.long	32767
	.globl	_ZN2v88internal24FLAG_wasm_test_streamingE
	.section	.bss._ZN2v88internal24FLAG_wasm_test_streamingE,"aw",@nobits
	.type	_ZN2v88internal24FLAG_wasm_test_streamingE, @object
	.size	_ZN2v88internal24FLAG_wasm_test_streamingE, 1
_ZN2v88internal24FLAG_wasm_test_streamingE:
	.zero	1
	.globl	_ZN2v88internal27FLAG_wasm_async_compilationE
	.section	.data._ZN2v88internal27FLAG_wasm_async_compilationE,"aw"
	.type	_ZN2v88internal27FLAG_wasm_async_compilationE, @object
	.size	_ZN2v88internal27FLAG_wasm_async_compilationE, 1
_ZN2v88internal27FLAG_wasm_async_compilationE:
	.byte	1
	.globl	_ZN2v88internal29FLAG_trace_wasm_serializationE
	.section	.bss._ZN2v88internal29FLAG_trace_wasm_serializationE,"aw",@nobits
	.type	_ZN2v88internal29FLAG_trace_wasm_serializationE, @object
	.size	_ZN2v88internal29FLAG_trace_wasm_serializationE, 1
_ZN2v88internal29FLAG_trace_wasm_serializationE:
	.zero	1
	.globl	_ZN2v88internal35FLAG_wasm_write_protect_code_memoryE
	.section	.bss._ZN2v88internal35FLAG_wasm_write_protect_code_memoryE,"aw",@nobits
	.type	_ZN2v88internal35FLAG_wasm_write_protect_code_memoryE, @object
	.size	_ZN2v88internal35FLAG_wasm_write_protect_code_memoryE, 1
_ZN2v88internal35FLAG_wasm_write_protect_code_memoryE:
	.zero	1
	.globl	_ZN2v88internal31FLAG_wasm_num_compilation_tasksE
	.section	.data._ZN2v88internal31FLAG_wasm_num_compilation_tasksE,"aw"
	.align 4
	.type	_ZN2v88internal31FLAG_wasm_num_compilation_tasksE, @object
	.size	_ZN2v88internal31FLAG_wasm_num_compilation_tasksE, 4
_ZN2v88internal31FLAG_wasm_num_compilation_tasksE:
	.long	10
	.globl	_ZN2v88internal36FLAG_wasm_disable_structured_cloningE
	.section	.bss._ZN2v88internal36FLAG_wasm_disable_structured_cloningE,"aw",@nobits
	.type	_ZN2v88internal36FLAG_wasm_disable_structured_cloningE, @object
	.size	_ZN2v88internal36FLAG_wasm_disable_structured_cloningE, 1
_ZN2v88internal36FLAG_wasm_disable_structured_cloningE:
	.zero	1
	.globl	_ZN2v88internal24FLAG_assume_asmjs_originE
	.section	.bss._ZN2v88internal24FLAG_assume_asmjs_originE,"aw",@nobits
	.type	_ZN2v88internal24FLAG_assume_asmjs_originE, @object
	.size	_ZN2v88internal24FLAG_assume_asmjs_originE, 1
_ZN2v88internal24FLAG_assume_asmjs_originE:
	.zero	1
	.globl	_ZN2v88internal16FLAG_expose_wasmE
	.section	.data._ZN2v88internal16FLAG_expose_wasmE,"aw"
	.type	_ZN2v88internal16FLAG_expose_wasmE, @object
	.size	_ZN2v88internal16FLAG_expose_wasmE, 1
_ZN2v88internal16FLAG_expose_wasmE:
	.byte	1
	.globl	_ZN2v88internal31FLAG_untrusted_code_mitigationsE
	.section	.bss._ZN2v88internal31FLAG_untrusted_code_mitigationsE,"aw",@nobits
	.type	_ZN2v88internal31FLAG_untrusted_code_mitigationsE, @object
	.size	_ZN2v88internal31FLAG_untrusted_code_mitigationsE, 1
_ZN2v88internal31FLAG_untrusted_code_mitigationsE:
	.zero	1
	.globl	_ZN2v88internal22FLAG_optimize_for_sizeE
	.section	.bss._ZN2v88internal22FLAG_optimize_for_sizeE,"aw",@nobits
	.type	_ZN2v88internal22FLAG_optimize_for_sizeE, @object
	.size	_ZN2v88internal22FLAG_optimize_for_sizeE, 1
_ZN2v88internal22FLAG_optimize_for_sizeE:
	.zero	1
	.globl	_ZN2v88internal33FLAG_stress_gc_during_compilationE
	.section	.bss._ZN2v88internal33FLAG_stress_gc_during_compilationE,"aw",@nobits
	.type	_ZN2v88internal33FLAG_stress_gc_during_compilationE, @object
	.size	_ZN2v88internal33FLAG_stress_gc_during_compilationE, 1
_ZN2v88internal33FLAG_stress_gc_during_compilationE:
	.zero	1
	.globl	_ZN2v88internal44FLAG_experimental_inline_promise_constructorE
	.section	.data._ZN2v88internal44FLAG_experimental_inline_promise_constructorE,"aw"
	.type	_ZN2v88internal44FLAG_experimental_inline_promise_constructorE, @object
	.size	_ZN2v88internal44FLAG_experimental_inline_promise_constructorE, 1
_ZN2v88internal44FLAG_experimental_inline_promise_constructorE:
	.byte	1
	.globl	_ZN2v88internal28FLAG_turbo_rewrite_far_jumpsE
	.section	.data._ZN2v88internal28FLAG_turbo_rewrite_far_jumpsE,"aw"
	.type	_ZN2v88internal28FLAG_turbo_rewrite_far_jumpsE, @object
	.size	_ZN2v88internal28FLAG_turbo_rewrite_far_jumpsE, 1
_ZN2v88internal28FLAG_turbo_rewrite_far_jumpsE:
	.byte	1
	.globl	_ZN2v88internal28FLAG_trace_store_eliminationE
	.section	.bss._ZN2v88internal28FLAG_trace_store_eliminationE,"aw",@nobits
	.type	_ZN2v88internal28FLAG_trace_store_eliminationE, @object
	.size	_ZN2v88internal28FLAG_trace_store_eliminationE, 1
_ZN2v88internal28FLAG_trace_store_eliminationE:
	.zero	1
	.globl	_ZN2v88internal28FLAG_turbo_store_eliminationE
	.section	.data._ZN2v88internal28FLAG_turbo_store_eliminationE,"aw"
	.type	_ZN2v88internal28FLAG_turbo_store_eliminationE, @object
	.size	_ZN2v88internal28FLAG_turbo_store_eliminationE, 1
_ZN2v88internal28FLAG_turbo_store_eliminationE:
	.byte	1
	.globl	_ZN2v88internal40FLAG_turbo_stress_instruction_schedulingE
	.section	.bss._ZN2v88internal40FLAG_turbo_stress_instruction_schedulingE,"aw",@nobits
	.type	_ZN2v88internal40FLAG_turbo_stress_instruction_schedulingE, @object
	.size	_ZN2v88internal40FLAG_turbo_stress_instruction_schedulingE, 1
_ZN2v88internal40FLAG_turbo_stress_instruction_schedulingE:
	.zero	1
	.globl	_ZN2v88internal33FLAG_turbo_instruction_schedulingE
	.section	.bss._ZN2v88internal33FLAG_turbo_instruction_schedulingE,"aw",@nobits
	.type	_ZN2v88internal33FLAG_turbo_instruction_schedulingE, @object
	.size	_ZN2v88internal33FLAG_turbo_instruction_schedulingE, 1
_ZN2v88internal33FLAG_turbo_instruction_schedulingE:
	.zero	1
	.globl	_ZN2v88internal29FLAG_turbo_allocation_foldingE
	.section	.data._ZN2v88internal29FLAG_turbo_allocation_foldingE,"aw"
	.type	_ZN2v88internal29FLAG_turbo_allocation_foldingE, @object
	.size	_ZN2v88internal29FLAG_turbo_allocation_foldingE, 1
_ZN2v88internal29FLAG_turbo_allocation_foldingE:
	.byte	1
	.globl	_ZN2v88internal17FLAG_turbo_escapeE
	.section	.data._ZN2v88internal17FLAG_turbo_escapeE,"aw"
	.type	_ZN2v88internal17FLAG_turbo_escapeE, @object
	.size	_ZN2v88internal17FLAG_turbo_escapeE, 1
_ZN2v88internal17FLAG_turbo_escapeE:
	.byte	1
	.globl	_ZN2v88internal26FLAG_turbo_cf_optimizationE
	.section	.data._ZN2v88internal26FLAG_turbo_cf_optimizationE,"aw"
	.type	_ZN2v88internal26FLAG_turbo_cf_optimizationE, @object
	.size	_ZN2v88internal26FLAG_turbo_cf_optimizationE, 1
_ZN2v88internal26FLAG_turbo_cf_optimizationE:
	.byte	1
	.globl	_ZN2v88internal24FLAG_turbo_loop_rotationE
	.section	.data._ZN2v88internal24FLAG_turbo_loop_rotationE,"aw"
	.type	_ZN2v88internal24FLAG_turbo_loop_rotationE, @object
	.size	_ZN2v88internal24FLAG_turbo_loop_rotationE, 1
_ZN2v88internal24FLAG_turbo_loop_rotationE:
	.byte	1
	.globl	_ZN2v88internal24FLAG_turbo_loop_variableE
	.section	.data._ZN2v88internal24FLAG_turbo_loop_variableE,"aw"
	.type	_ZN2v88internal24FLAG_turbo_loop_variableE, @object
	.size	_ZN2v88internal24FLAG_turbo_loop_variableE, 1
_ZN2v88internal24FLAG_turbo_loop_variableE:
	.byte	1
	.globl	_ZN2v88internal23FLAG_turbo_loop_peelingE
	.section	.data._ZN2v88internal23FLAG_turbo_loop_peelingE,"aw"
	.type	_ZN2v88internal23FLAG_turbo_loop_peelingE, @object
	.size	_ZN2v88internal23FLAG_turbo_loop_peelingE, 1
_ZN2v88internal23FLAG_turbo_loop_peelingE:
	.byte	1
	.globl	_ZN2v88internal13FLAG_turbo_jtE
	.section	.data._ZN2v88internal13FLAG_turbo_jtE,"aw"
	.type	_ZN2v88internal13FLAG_turbo_jtE, @object
	.size	_ZN2v88internal13FLAG_turbo_jtE, 1
_ZN2v88internal13FLAG_turbo_jtE:
	.byte	1
	.globl	_ZN2v88internal28FLAG_turbo_move_optimizationE
	.section	.data._ZN2v88internal28FLAG_turbo_move_optimizationE,"aw"
	.type	_ZN2v88internal28FLAG_turbo_move_optimizationE, @object
	.size	_ZN2v88internal28FLAG_turbo_move_optimizationE, 1
_ZN2v88internal28FLAG_turbo_move_optimizationE:
	.byte	1
	.globl	_ZN2v88internal28FLAG_turbo_verify_allocationE
	.section	.bss._ZN2v88internal28FLAG_turbo_verify_allocationE,"aw",@nobits
	.type	_ZN2v88internal28FLAG_turbo_verify_allocationE, @object
	.size	_ZN2v88internal28FLAG_turbo_verify_allocationE, 1
_ZN2v88internal28FLAG_turbo_verify_allocationE:
	.zero	1
	.globl	_ZN2v88internal20FLAG_turbo_profilingE
	.section	.bss._ZN2v88internal20FLAG_turbo_profilingE,"aw",@nobits
	.type	_ZN2v88internal20FLAG_turbo_profilingE, @object
	.size	_ZN2v88internal20FLAG_turbo_profilingE, 1
_ZN2v88internal20FLAG_turbo_profilingE:
	.zero	1
	.globl	_ZN2v88internal33FLAG_trace_turbo_load_eliminationE
	.section	.bss._ZN2v88internal33FLAG_trace_turbo_load_eliminationE,"aw",@nobits
	.type	_ZN2v88internal33FLAG_trace_turbo_load_eliminationE, @object
	.size	_ZN2v88internal33FLAG_trace_turbo_load_eliminationE, 1
_ZN2v88internal33FLAG_trace_turbo_load_eliminationE:
	.zero	1
	.globl	_ZN2v88internal27FLAG_turbo_load_eliminationE
	.section	.data._ZN2v88internal27FLAG_turbo_load_eliminationE,"aw"
	.type	_ZN2v88internal27FLAG_turbo_load_eliminationE, @object
	.size	_ZN2v88internal27FLAG_turbo_load_eliminationE, 1
_ZN2v88internal27FLAG_turbo_load_eliminationE:
	.byte	1
	.globl	_ZN2v88internal31FLAG_trace_environment_livenessE
	.section	.bss._ZN2v88internal31FLAG_trace_environment_livenessE,"aw",@nobits
	.type	_ZN2v88internal31FLAG_trace_environment_livenessE, @object
	.size	_ZN2v88internal31FLAG_trace_environment_livenessE, 1
_ZN2v88internal31FLAG_trace_environment_livenessE:
	.zero	1
	.globl	_ZN2v88internal33FLAG_analyze_environment_livenessE
	.section	.data._ZN2v88internal33FLAG_analyze_environment_livenessE,"aw"
	.type	_ZN2v88internal33FLAG_analyze_environment_livenessE, @object
	.size	_ZN2v88internal33FLAG_analyze_environment_livenessE, 1
_ZN2v88internal33FLAG_analyze_environment_livenessE:
	.byte	1
	.globl	_ZN2v88internal14FLAG_trace_osrE
	.section	.bss._ZN2v88internal14FLAG_trace_osrE,"aw",@nobits
	.type	_ZN2v88internal14FLAG_trace_osrE, @object
	.size	_ZN2v88internal14FLAG_trace_osrE, 1
_ZN2v88internal14FLAG_trace_osrE:
	.zero	1
	.globl	_ZN2v88internal12FLAG_use_osrE
	.section	.data._ZN2v88internal12FLAG_use_osrE,"aw"
	.type	_ZN2v88internal12FLAG_use_osrE, @object
	.size	_ZN2v88internal12FLAG_use_osrE, 1
_ZN2v88internal12FLAG_use_osrE:
	.byte	1
	.globl	_ZN2v88internal32FLAG_turbo_inline_array_builtinsE
	.section	.data._ZN2v88internal32FLAG_turbo_inline_array_builtinsE,"aw"
	.type	_ZN2v88internal32FLAG_turbo_inline_array_builtinsE, @object
	.size	_ZN2v88internal32FLAG_turbo_inline_array_builtinsE, 1
_ZN2v88internal32FLAG_turbo_inline_array_builtinsE:
	.byte	1
	.globl	_ZN2v88internal21FLAG_inline_accessorsE
	.section	.data._ZN2v88internal21FLAG_inline_accessorsE,"aw"
	.type	_ZN2v88internal21FLAG_inline_accessorsE, @object
	.size	_ZN2v88internal21FLAG_inline_accessorsE, 1
_ZN2v88internal21FLAG_inline_accessorsE:
	.byte	1
	.globl	_ZN2v88internal25FLAG_trace_turbo_inliningE
	.section	.bss._ZN2v88internal25FLAG_trace_turbo_inliningE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_trace_turbo_inliningE, @object
	.size	_ZN2v88internal25FLAG_trace_turbo_inliningE, 1
_ZN2v88internal25FLAG_trace_turbo_inliningE:
	.zero	1
	.globl	_ZN2v88internal18FLAG_stress_inlineE
	.section	.bss._ZN2v88internal18FLAG_stress_inlineE,"aw",@nobits
	.type	_ZN2v88internal18FLAG_stress_inlineE, @object
	.size	_ZN2v88internal18FLAG_stress_inlineE, 1
_ZN2v88internal18FLAG_stress_inlineE:
	.zero	1
	.globl	_ZN2v88internal25FLAG_polymorphic_inliningE
	.section	.data._ZN2v88internal25FLAG_polymorphic_inliningE,"aw"
	.type	_ZN2v88internal25FLAG_polymorphic_inliningE, @object
	.size	_ZN2v88internal25FLAG_polymorphic_inliningE, 1
_ZN2v88internal25FLAG_polymorphic_inliningE:
	.byte	1
	.globl	_ZN2v88internal27FLAG_min_inlining_frequencyE
	.section	.data._ZN2v88internal27FLAG_min_inlining_frequencyE,"aw"
	.align 8
	.type	_ZN2v88internal27FLAG_min_inlining_frequencyE, @object
	.size	_ZN2v88internal27FLAG_min_inlining_frequencyE, 8
_ZN2v88internal27FLAG_min_inlining_frequencyE:
	.long	858993459
	.long	1069757235
	.globl	_ZN2v88internal32FLAG_max_optimized_bytecode_sizeE
	.section	.data._ZN2v88internal32FLAG_max_optimized_bytecode_sizeE,"aw"
	.align 4
	.type	_ZN2v88internal32FLAG_max_optimized_bytecode_sizeE, @object
	.size	_ZN2v88internal32FLAG_max_optimized_bytecode_sizeE, 4
_ZN2v88internal32FLAG_max_optimized_bytecode_sizeE:
	.long	61440
	.globl	_ZN2v88internal36FLAG_max_inlined_bytecode_size_smallE
	.section	.data._ZN2v88internal36FLAG_max_inlined_bytecode_size_smallE,"aw"
	.align 4
	.type	_ZN2v88internal36FLAG_max_inlined_bytecode_size_smallE, @object
	.size	_ZN2v88internal36FLAG_max_inlined_bytecode_size_smallE, 4
_ZN2v88internal36FLAG_max_inlined_bytecode_size_smallE:
	.long	30
	.globl	_ZN2v88internal39FLAG_reserve_inline_budget_scale_factorE
	.section	.data._ZN2v88internal39FLAG_reserve_inline_budget_scale_factorE,"aw"
	.align 8
	.type	_ZN2v88internal39FLAG_reserve_inline_budget_scale_factorE, @object
	.size	_ZN2v88internal39FLAG_reserve_inline_budget_scale_factorE, 8
_ZN2v88internal39FLAG_reserve_inline_budget_scale_factorE:
	.long	858993459
	.long	1072902963
	.globl	_ZN2v88internal39FLAG_max_inlined_bytecode_size_absoluteE
	.section	.data._ZN2v88internal39FLAG_max_inlined_bytecode_size_absoluteE,"aw"
	.align 4
	.type	_ZN2v88internal39FLAG_max_inlined_bytecode_size_absoluteE, @object
	.size	_ZN2v88internal39FLAG_max_inlined_bytecode_size_absoluteE, 4
_ZN2v88internal39FLAG_max_inlined_bytecode_size_absoluteE:
	.long	5000
	.globl	_ZN2v88internal41FLAG_max_inlined_bytecode_size_cumulativeE
	.section	.data._ZN2v88internal41FLAG_max_inlined_bytecode_size_cumulativeE,"aw"
	.align 4
	.type	_ZN2v88internal41FLAG_max_inlined_bytecode_size_cumulativeE, @object
	.size	_ZN2v88internal41FLAG_max_inlined_bytecode_size_cumulativeE, 4
_ZN2v88internal41FLAG_max_inlined_bytecode_size_cumulativeE:
	.long	1000
	.globl	_ZN2v88internal30FLAG_max_inlined_bytecode_sizeE
	.section	.data._ZN2v88internal30FLAG_max_inlined_bytecode_sizeE,"aw"
	.align 4
	.type	_ZN2v88internal30FLAG_max_inlined_bytecode_sizeE, @object
	.size	_ZN2v88internal30FLAG_max_inlined_bytecode_sizeE, 4
_ZN2v88internal30FLAG_max_inlined_bytecode_sizeE:
	.long	500
	.globl	_ZN2v88internal19FLAG_turbo_inliningE
	.section	.data._ZN2v88internal19FLAG_turbo_inliningE,"aw"
	.type	_ZN2v88internal19FLAG_turbo_inliningE, @object
	.size	_ZN2v88internal19FLAG_turbo_inliningE, 1
_ZN2v88internal19FLAG_turbo_inliningE:
	.byte	1
	.globl	_ZN2v88internal36FLAG_function_context_specializationE
	.section	.bss._ZN2v88internal36FLAG_function_context_specializationE,"aw",@nobits
	.type	_ZN2v88internal36FLAG_function_context_specializationE, @object
	.size	_ZN2v88internal36FLAG_function_context_specializationE, 1
_ZN2v88internal36FLAG_function_context_specializationE:
	.zero	1
	.globl	_ZN2v88internal20FLAG_turbo_splittingE
	.section	.data._ZN2v88internal20FLAG_turbo_splittingE,"aw"
	.type	_ZN2v88internal20FLAG_turbo_splittingE, @object
	.size	_ZN2v88internal20FLAG_turbo_splittingE, 1
_ZN2v88internal20FLAG_turbo_splittingE:
	.byte	1
	.globl	_ZN2v88internal21FLAG_turbo_stats_wasmE
	.section	.bss._ZN2v88internal21FLAG_turbo_stats_wasmE,"aw",@nobits
	.type	_ZN2v88internal21FLAG_turbo_stats_wasmE, @object
	.size	_ZN2v88internal21FLAG_turbo_stats_wasmE, 1
_ZN2v88internal21FLAG_turbo_stats_wasmE:
	.zero	1
	.globl	_ZN2v88internal20FLAG_turbo_stats_nvpE
	.section	.bss._ZN2v88internal20FLAG_turbo_stats_nvpE,"aw",@nobits
	.type	_ZN2v88internal20FLAG_turbo_stats_nvpE, @object
	.size	_ZN2v88internal20FLAG_turbo_stats_nvpE, 1
_ZN2v88internal20FLAG_turbo_stats_nvpE:
	.zero	1
	.globl	_ZN2v88internal16FLAG_turbo_statsE
	.section	.bss._ZN2v88internal16FLAG_turbo_statsE,"aw",@nobits
	.type	_ZN2v88internal16FLAG_turbo_statsE, @object
	.size	_ZN2v88internal16FLAG_turbo_statsE, 1
_ZN2v88internal16FLAG_turbo_statsE:
	.zero	1
	.globl	_ZN2v88internal21FLAG_csa_trap_on_nodeE
	.section	.bss._ZN2v88internal21FLAG_csa_trap_on_nodeE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal21FLAG_csa_trap_on_nodeE, @object
	.size	_ZN2v88internal21FLAG_csa_trap_on_nodeE, 8
_ZN2v88internal21FLAG_csa_trap_on_nodeE:
	.zero	8
	.globl	_ZN2v88internal21FLAG_trace_verify_csaE
	.section	.bss._ZN2v88internal21FLAG_trace_verify_csaE,"aw",@nobits
	.type	_ZN2v88internal21FLAG_trace_verify_csaE, @object
	.size	_ZN2v88internal21FLAG_trace_verify_csaE, 1
_ZN2v88internal21FLAG_trace_verify_csaE:
	.zero	1
	.globl	_ZN2v88internal31FLAG_turbo_verify_machine_graphE
	.section	.bss._ZN2v88internal31FLAG_turbo_verify_machine_graphE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal31FLAG_turbo_verify_machine_graphE, @object
	.size	_ZN2v88internal31FLAG_turbo_verify_machine_graphE, 8
_ZN2v88internal31FLAG_turbo_verify_machine_graphE:
	.zero	8
	.globl	_ZN2v88internal17FLAG_turbo_verifyE
	.section	.bss._ZN2v88internal17FLAG_turbo_verifyE,"aw",@nobits
	.type	_ZN2v88internal17FLAG_turbo_verifyE, @object
	.size	_ZN2v88internal17FLAG_turbo_verifyE, 1
_ZN2v88internal17FLAG_turbo_verifyE:
	.zero	1
	.globl	_ZN2v88internal25FLAG_trace_representationE
	.section	.bss._ZN2v88internal25FLAG_trace_representationE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_trace_representationE, @object
	.size	_ZN2v88internal25FLAG_trace_representationE, 1
_ZN2v88internal25FLAG_trace_representationE:
	.zero	1
	.globl	_ZN2v88internal19FLAG_trace_all_usesE
	.section	.bss._ZN2v88internal19FLAG_trace_all_usesE,"aw",@nobits
	.type	_ZN2v88internal19FLAG_trace_all_usesE, @object
	.size	_ZN2v88internal19FLAG_trace_all_usesE, 1
_ZN2v88internal19FLAG_trace_all_usesE:
	.zero	1
	.globl	_ZN2v88internal22FLAG_trace_turbo_allocE
	.section	.bss._ZN2v88internal22FLAG_trace_turbo_allocE,"aw",@nobits
	.type	_ZN2v88internal22FLAG_trace_turbo_allocE, @object
	.size	_ZN2v88internal22FLAG_trace_turbo_allocE, 1
_ZN2v88internal22FLAG_trace_turbo_allocE:
	.zero	1
	.globl	_ZN2v88internal21FLAG_trace_turbo_loopE
	.section	.bss._ZN2v88internal21FLAG_trace_turbo_loopE,"aw",@nobits
	.type	_ZN2v88internal21FLAG_trace_turbo_loopE, @object
	.size	_ZN2v88internal21FLAG_trace_turbo_loopE, 1
_ZN2v88internal21FLAG_trace_turbo_loopE:
	.zero	1
	.globl	_ZN2v88internal20FLAG_trace_turbo_ceqE
	.section	.bss._ZN2v88internal20FLAG_trace_turbo_ceqE,"aw",@nobits
	.type	_ZN2v88internal20FLAG_trace_turbo_ceqE, @object
	.size	_ZN2v88internal20FLAG_trace_turbo_ceqE, 1
_ZN2v88internal20FLAG_trace_turbo_ceqE:
	.zero	1
	.globl	_ZN2v88internal19FLAG_trace_turbo_jtE
	.section	.bss._ZN2v88internal19FLAG_trace_turbo_jtE,"aw",@nobits
	.type	_ZN2v88internal19FLAG_trace_turbo_jtE, @object
	.size	_ZN2v88internal19FLAG_trace_turbo_jtE, 1
_ZN2v88internal19FLAG_trace_turbo_jtE:
	.zero	1
	.globl	_ZN2v88internal25FLAG_trace_turbo_trimmingE
	.section	.bss._ZN2v88internal25FLAG_trace_turbo_trimmingE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_trace_turbo_trimmingE, @object
	.size	_ZN2v88internal25FLAG_trace_turbo_trimmingE, 1
_ZN2v88internal25FLAG_trace_turbo_trimmingE:
	.zero	1
	.globl	_ZN2v88internal26FLAG_trace_turbo_reductionE
	.section	.bss._ZN2v88internal26FLAG_trace_turbo_reductionE,"aw",@nobits
	.type	_ZN2v88internal26FLAG_trace_turbo_reductionE, @object
	.size	_ZN2v88internal26FLAG_trace_turbo_reductionE, 1
_ZN2v88internal26FLAG_trace_turbo_reductionE:
	.zero	1
	.globl	_ZN2v88internal26FLAG_trace_turbo_schedulerE
	.section	.bss._ZN2v88internal26FLAG_trace_turbo_schedulerE,"aw",@nobits
	.type	_ZN2v88internal26FLAG_trace_turbo_schedulerE, @object
	.size	_ZN2v88internal26FLAG_trace_turbo_schedulerE, 1
_ZN2v88internal26FLAG_trace_turbo_schedulerE:
	.zero	1
	.globl	_ZN2v88internal22FLAG_trace_turbo_typesE
	.section	.data._ZN2v88internal22FLAG_trace_turbo_typesE,"aw"
	.type	_ZN2v88internal22FLAG_trace_turbo_typesE, @object
	.size	_ZN2v88internal22FLAG_trace_turbo_typesE, 1
_ZN2v88internal22FLAG_trace_turbo_typesE:
	.byte	1
	.globl	_ZN2v88internal25FLAG_trace_turbo_cfg_fileE
	.section	.bss._ZN2v88internal25FLAG_trace_turbo_cfg_fileE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal25FLAG_trace_turbo_cfg_fileE, @object
	.size	_ZN2v88internal25FLAG_trace_turbo_cfg_fileE, 8
_ZN2v88internal25FLAG_trace_turbo_cfg_fileE:
	.zero	8
	.globl	_ZN2v88internal26FLAG_trace_turbo_scheduledE
	.section	.bss._ZN2v88internal26FLAG_trace_turbo_scheduledE,"aw",@nobits
	.type	_ZN2v88internal26FLAG_trace_turbo_scheduledE, @object
	.size	_ZN2v88internal26FLAG_trace_turbo_scheduledE, 1
_ZN2v88internal26FLAG_trace_turbo_scheduledE:
	.zero	1
	.globl	_ZN2v88internal22FLAG_trace_turbo_graphE
	.section	.bss._ZN2v88internal22FLAG_trace_turbo_graphE,"aw",@nobits
	.type	_ZN2v88internal22FLAG_trace_turbo_graphE, @object
	.size	_ZN2v88internal22FLAG_trace_turbo_graphE, 1
_ZN2v88internal22FLAG_trace_turbo_graphE:
	.zero	1
	.globl	_ZN2v88internal23FLAG_trace_turbo_filterE
	.section	.data.rel.local._ZN2v88internal23FLAG_trace_turbo_filterE,"aw"
	.align 8
	.type	_ZN2v88internal23FLAG_trace_turbo_filterE, @object
	.size	_ZN2v88internal23FLAG_trace_turbo_filterE, 8
_ZN2v88internal23FLAG_trace_turbo_filterE:
	.quad	.LC958
	.globl	_ZN2v88internal21FLAG_trace_turbo_pathE
	.section	.bss._ZN2v88internal21FLAG_trace_turbo_pathE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal21FLAG_trace_turbo_pathE, @object
	.size	_ZN2v88internal21FLAG_trace_turbo_pathE, 8
_ZN2v88internal21FLAG_trace_turbo_pathE:
	.zero	8
	.globl	_ZN2v88internal16FLAG_trace_turboE
	.section	.bss._ZN2v88internal16FLAG_trace_turboE,"aw",@nobits
	.type	_ZN2v88internal16FLAG_trace_turboE, @object
	.size	_ZN2v88internal16FLAG_trace_turboE, 1
_ZN2v88internal16FLAG_trace_turboE:
	.zero	1
	.globl	_ZN2v88internal17FLAG_turbo_filterE
	.section	.data.rel.local._ZN2v88internal17FLAG_turbo_filterE,"aw"
	.align 8
	.type	_ZN2v88internal17FLAG_turbo_filterE, @object
	.size	_ZN2v88internal17FLAG_turbo_filterE, 8
_ZN2v88internal17FLAG_turbo_filterE:
	.quad	.LC958
	.globl	_ZN2v88internal40FLAG_turbo_control_flow_aware_allocationE
	.section	.bss._ZN2v88internal40FLAG_turbo_control_flow_aware_allocationE,"aw",@nobits
	.type	_ZN2v88internal40FLAG_turbo_control_flow_aware_allocationE, @object
	.size	_ZN2v88internal40FLAG_turbo_control_flow_aware_allocationE, 1
_ZN2v88internal40FLAG_turbo_control_flow_aware_allocationE:
	.zero	1
	.globl	_ZN2v88internal26FLAG_turbo_sp_frame_accessE
	.section	.bss._ZN2v88internal26FLAG_turbo_sp_frame_accessE,"aw",@nobits
	.type	_ZN2v88internal26FLAG_turbo_sp_frame_accessE, @object
	.size	_ZN2v88internal26FLAG_turbo_sp_frame_accessE, 1
_ZN2v88internal26FLAG_turbo_sp_frame_accessE:
	.zero	1
	.globl	_ZN2v88internal8FLAG_optE
	.section	.data._ZN2v88internal8FLAG_optE,"aw"
	.type	_ZN2v88internal8FLAG_optE, @object
	.size	_ZN2v88internal8FLAG_optE, 1
_ZN2v88internal8FLAG_optE:
	.byte	1
	.globl	_ZN2v88internal23FLAG_print_deopt_stressE
	.section	.bss._ZN2v88internal23FLAG_print_deopt_stressE,"aw",@nobits
	.type	_ZN2v88internal23FLAG_print_deopt_stressE, @object
	.size	_ZN2v88internal23FLAG_print_deopt_stressE, 1
_ZN2v88internal23FLAG_print_deopt_stressE:
	.zero	1
	.globl	_ZN2v88internal24FLAG_deopt_every_n_timesE
	.section	.bss._ZN2v88internal24FLAG_deopt_every_n_timesE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal24FLAG_deopt_every_n_timesE, @object
	.size	_ZN2v88internal24FLAG_deopt_every_n_timesE, 4
_ZN2v88internal24FLAG_deopt_every_n_timesE:
	.zero	4
	.globl	_ZN2v88internal16FLAG_stress_runsE
	.section	.bss._ZN2v88internal16FLAG_stress_runsE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal16FLAG_stress_runsE, @object
	.size	_ZN2v88internal16FLAG_stress_runsE, 4
_ZN2v88internal16FLAG_stress_runsE:
	.zero	4
	.globl	_ZN2v88internal22FLAG_trace_heap_brokerE
	.section	.bss._ZN2v88internal22FLAG_trace_heap_brokerE,"aw",@nobits
	.type	_ZN2v88internal22FLAG_trace_heap_brokerE, @object
	.size	_ZN2v88internal22FLAG_trace_heap_brokerE, 1
_ZN2v88internal22FLAG_trace_heap_brokerE:
	.zero	1
	.globl	_ZN2v88internal30FLAG_trace_heap_broker_verboseE
	.section	.bss._ZN2v88internal30FLAG_trace_heap_broker_verboseE,"aw",@nobits
	.type	_ZN2v88internal30FLAG_trace_heap_broker_verboseE, @object
	.size	_ZN2v88internal30FLAG_trace_heap_broker_verboseE, 1
_ZN2v88internal30FLAG_trace_heap_broker_verboseE:
	.zero	1
	.globl	_ZN2v88internal24FLAG_concurrent_inliningE
	.section	.bss._ZN2v88internal24FLAG_concurrent_inliningE,"aw",@nobits
	.type	_ZN2v88internal24FLAG_concurrent_inliningE, @object
	.size	_ZN2v88internal24FLAG_concurrent_inliningE, 1
_ZN2v88internal24FLAG_concurrent_inliningE:
	.zero	1
	.globl	_ZN2v88internal35FLAG_block_concurrent_recompilationE
	.section	.bss._ZN2v88internal35FLAG_block_concurrent_recompilationE,"aw",@nobits
	.type	_ZN2v88internal35FLAG_block_concurrent_recompilationE, @object
	.size	_ZN2v88internal35FLAG_block_concurrent_recompilationE, 1
_ZN2v88internal35FLAG_block_concurrent_recompilationE:
	.zero	1
	.globl	_ZN2v88internal35FLAG_concurrent_recompilation_delayE
	.section	.bss._ZN2v88internal35FLAG_concurrent_recompilation_delayE,"aw",@nobits
	.align 4
	.type	_ZN2v88internal35FLAG_concurrent_recompilation_delayE, @object
	.size	_ZN2v88internal35FLAG_concurrent_recompilation_delayE, 4
_ZN2v88internal35FLAG_concurrent_recompilation_delayE:
	.zero	4
	.globl	_ZN2v88internal42FLAG_concurrent_recompilation_queue_lengthE
	.section	.data._ZN2v88internal42FLAG_concurrent_recompilation_queue_lengthE,"aw"
	.align 4
	.type	_ZN2v88internal42FLAG_concurrent_recompilation_queue_lengthE, @object
	.size	_ZN2v88internal42FLAG_concurrent_recompilation_queue_lengthE, 4
_ZN2v88internal42FLAG_concurrent_recompilation_queue_lengthE:
	.long	8
	.globl	_ZN2v88internal35FLAG_trace_concurrent_recompilationE
	.section	.bss._ZN2v88internal35FLAG_trace_concurrent_recompilationE,"aw",@nobits
	.type	_ZN2v88internal35FLAG_trace_concurrent_recompilationE, @object
	.size	_ZN2v88internal35FLAG_trace_concurrent_recompilationE, 1
_ZN2v88internal35FLAG_trace_concurrent_recompilationE:
	.zero	1
	.globl	_ZN2v88internal29FLAG_concurrent_recompilationE
	.section	.data._ZN2v88internal29FLAG_concurrent_recompilationE,"aw"
	.type	_ZN2v88internal29FLAG_concurrent_recompilationE, @object
	.size	_ZN2v88internal29FLAG_concurrent_recompilationE, 1
_ZN2v88internal29FLAG_concurrent_recompilationE:
	.byte	1
	.globl	_ZN2v88internal25FLAG_trace_generalizationE
	.section	.bss._ZN2v88internal25FLAG_trace_generalizationE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_trace_generalizationE, @object
	.size	_ZN2v88internal25FLAG_trace_generalizationE, 1
_ZN2v88internal25FLAG_trace_generalizationE:
	.zero	1
	.globl	_ZN2v88internal20FLAG_trace_migrationE
	.section	.bss._ZN2v88internal20FLAG_trace_migrationE,"aw",@nobits
	.type	_ZN2v88internal20FLAG_trace_migrationE, @object
	.size	_ZN2v88internal20FLAG_trace_migrationE, 1
_ZN2v88internal20FLAG_trace_migrationE:
	.zero	1
	.globl	_ZN2v88internal33FLAG_trace_track_allocation_sitesE
	.section	.bss._ZN2v88internal33FLAG_trace_track_allocation_sitesE,"aw",@nobits
	.type	_ZN2v88internal33FLAG_trace_track_allocation_sitesE, @object
	.size	_ZN2v88internal33FLAG_trace_track_allocation_sitesE, 1
_ZN2v88internal33FLAG_trace_track_allocation_sitesE:
	.zero	1
	.globl	_ZN2v88internal14FLAG_fast_mathE
	.section	.data._ZN2v88internal14FLAG_fast_mathE,"aw"
	.type	_ZN2v88internal14FLAG_fast_mathE, @object
	.size	_ZN2v88internal14FLAG_fast_mathE, 1
_ZN2v88internal14FLAG_fast_mathE:
	.byte	1
	.globl	_ZN2v88internal42FLAG_trace_ignition_dispatches_output_fileE
	.section	.bss._ZN2v88internal42FLAG_trace_ignition_dispatches_output_fileE,"aw",@nobits
	.align 8
	.type	_ZN2v88internal42FLAG_trace_ignition_dispatches_output_fileE, @object
	.size	_ZN2v88internal42FLAG_trace_ignition_dispatches_output_fileE, 8
_ZN2v88internal42FLAG_trace_ignition_dispatches_output_fileE:
	.zero	8
	.globl	_ZN2v88internal30FLAG_trace_ignition_dispatchesE
	.section	.bss._ZN2v88internal30FLAG_trace_ignition_dispatchesE,"aw",@nobits
	.type	_ZN2v88internal30FLAG_trace_ignition_dispatchesE, @object
	.size	_ZN2v88internal30FLAG_trace_ignition_dispatchesE, 1
_ZN2v88internal30FLAG_trace_ignition_dispatchesE:
	.zero	1
	.globl	_ZN2v88internal27FLAG_trace_ignition_codegenE
	.section	.bss._ZN2v88internal27FLAG_trace_ignition_codegenE,"aw",@nobits
	.type	_ZN2v88internal27FLAG_trace_ignition_codegenE, @object
	.size	_ZN2v88internal27FLAG_trace_ignition_codegenE, 1
_ZN2v88internal27FLAG_trace_ignition_codegenE:
	.zero	1
	.globl	_ZN2v88internal26FLAG_print_bytecode_filterE
	.section	.data.rel.local._ZN2v88internal26FLAG_print_bytecode_filterE,"aw"
	.align 8
	.type	_ZN2v88internal26FLAG_print_bytecode_filterE, @object
	.size	_ZN2v88internal26FLAG_print_bytecode_filterE, 8
_ZN2v88internal26FLAG_print_bytecode_filterE:
	.quad	.LC958
	.globl	_ZN2v88internal33FLAG_stress_lazy_source_positionsE
	.section	.bss._ZN2v88internal33FLAG_stress_lazy_source_positionsE,"aw",@nobits
	.type	_ZN2v88internal33FLAG_stress_lazy_source_positionsE, @object
	.size	_ZN2v88internal33FLAG_stress_lazy_source_positionsE, 1
_ZN2v88internal33FLAG_stress_lazy_source_positionsE:
	.zero	1
	.globl	_ZN2v88internal33FLAG_enable_lazy_source_positionsE
	.section	.data._ZN2v88internal33FLAG_enable_lazy_source_positionsE,"aw"
	.type	_ZN2v88internal33FLAG_enable_lazy_source_positionsE, @object
	.size	_ZN2v88internal33FLAG_enable_lazy_source_positionsE, 1
_ZN2v88internal33FLAG_enable_lazy_source_positionsE:
	.byte	1
	.globl	_ZN2v88internal19FLAG_print_bytecodeE
	.section	.bss._ZN2v88internal19FLAG_print_bytecodeE,"aw",@nobits
	.type	_ZN2v88internal19FLAG_print_bytecodeE, @object
	.size	_ZN2v88internal19FLAG_print_bytecodeE, 1
_ZN2v88internal19FLAG_print_bytecodeE:
	.zero	1
	.globl	_ZN2v88internal43FLAG_ignition_share_named_property_feedbackE
	.section	.data._ZN2v88internal43FLAG_ignition_share_named_property_feedbackE,"aw"
	.type	_ZN2v88internal43FLAG_ignition_share_named_property_feedbackE, @object
	.size	_ZN2v88internal43FLAG_ignition_share_named_property_feedbackE, 1
_ZN2v88internal43FLAG_ignition_share_named_property_feedbackE:
	.byte	1
	.globl	_ZN2v88internal41FLAG_ignition_filter_expression_positionsE
	.section	.data._ZN2v88internal41FLAG_ignition_filter_expression_positionsE,"aw"
	.type	_ZN2v88internal41FLAG_ignition_filter_expression_positionsE, @object
	.size	_ZN2v88internal41FLAG_ignition_filter_expression_positionsE, 1
_ZN2v88internal41FLAG_ignition_filter_expression_positionsE:
	.byte	1
	.globl	_ZN2v88internal17FLAG_ignition_reoE
	.section	.data._ZN2v88internal17FLAG_ignition_reoE,"aw"
	.type	_ZN2v88internal17FLAG_ignition_reoE, @object
	.size	_ZN2v88internal17FLAG_ignition_reoE, 1
_ZN2v88internal17FLAG_ignition_reoE:
	.byte	1
	.globl	_ZN2v88internal42FLAG_ignition_elide_noneffectful_bytecodesE
	.section	.data._ZN2v88internal42FLAG_ignition_elide_noneffectful_bytecodesE,"aw"
	.type	_ZN2v88internal42FLAG_ignition_elide_noneffectful_bytecodesE, @object
	.size	_ZN2v88internal42FLAG_ignition_elide_noneffectful_bytecodesE, 1
_ZN2v88internal42FLAG_ignition_elide_noneffectful_bytecodesE:
	.byte	1
	.globl	_ZN2v88internal29FLAG_lazy_feedback_allocationE
	.section	.data._ZN2v88internal29FLAG_lazy_feedback_allocationE,"aw"
	.type	_ZN2v88internal29FLAG_lazy_feedback_allocationE, @object
	.size	_ZN2v88internal29FLAG_lazy_feedback_allocationE, 1
_ZN2v88internal29FLAG_lazy_feedback_allocationE:
	.byte	1
	.globl	_ZN2v88internal42FLAG_budget_for_feedback_vector_allocationE
	.section	.data._ZN2v88internal42FLAG_budget_for_feedback_vector_allocationE,"aw"
	.align 4
	.type	_ZN2v88internal42FLAG_budget_for_feedback_vector_allocationE, @object
	.size	_ZN2v88internal42FLAG_budget_for_feedback_vector_allocationE, 4
_ZN2v88internal42FLAG_budget_for_feedback_vector_allocationE:
	.long	1024
	.globl	_ZN2v88internal11FLAG_use_icE
	.section	.data._ZN2v88internal11FLAG_use_icE,"aw"
	.type	_ZN2v88internal11FLAG_use_icE, @object
	.size	_ZN2v88internal11FLAG_use_icE, 1
_ZN2v88internal11FLAG_use_icE:
	.byte	1
	.globl	_ZN2v88internal12FLAG_jitlessE
	.section	.bss._ZN2v88internal12FLAG_jitlessE,"aw",@nobits
	.type	_ZN2v88internal12FLAG_jitlessE, @object
	.size	_ZN2v88internal12FLAG_jitlessE, 1
_ZN2v88internal12FLAG_jitlessE:
	.zero	1
	.globl	_ZN2v88internal21FLAG_interrupt_budgetE
	.section	.data._ZN2v88internal21FLAG_interrupt_budgetE,"aw"
	.align 4
	.type	_ZN2v88internal21FLAG_interrupt_budgetE, @object
	.size	_ZN2v88internal21FLAG_interrupt_budgetE, 4
_ZN2v88internal21FLAG_interrupt_budgetE:
	.long	147456
	.globl	_ZN2v88internal24FLAG_unbox_double_arraysE
	.section	.data._ZN2v88internal24FLAG_unbox_double_arraysE,"aw"
	.type	_ZN2v88internal24FLAG_unbox_double_arraysE, @object
	.size	_ZN2v88internal24FLAG_unbox_double_arraysE, 1
_ZN2v88internal24FLAG_unbox_double_arraysE:
	.byte	1
	.globl	_ZN2v88internal33FLAG_enable_one_shot_optimizationE
	.section	.data._ZN2v88internal33FLAG_enable_one_shot_optimizationE,"aw"
	.type	_ZN2v88internal33FLAG_enable_one_shot_optimizationE, @object
	.size	_ZN2v88internal33FLAG_enable_one_shot_optimizationE, 1
_ZN2v88internal33FLAG_enable_one_shot_optimizationE:
	.byte	1
	.globl	_ZN2v88internal27FLAG_feedback_normalizationE
	.section	.bss._ZN2v88internal27FLAG_feedback_normalizationE,"aw",@nobits
	.type	_ZN2v88internal27FLAG_feedback_normalizationE, @object
	.size	_ZN2v88internal27FLAG_feedback_normalizationE, 1
_ZN2v88internal27FLAG_feedback_normalizationE:
	.zero	1
	.globl	_ZN2v88internal33FLAG_trace_protector_invalidationE
	.section	.bss._ZN2v88internal33FLAG_trace_protector_invalidationE,"aw",@nobits
	.type	_ZN2v88internal33FLAG_trace_protector_invalidationE, @object
	.size	_ZN2v88internal33FLAG_trace_protector_invalidationE, 1
_ZN2v88internal33FLAG_trace_protector_invalidationE:
	.zero	1
	.globl	_ZN2v88internal25FLAG_trace_block_coverageE
	.section	.bss._ZN2v88internal25FLAG_trace_block_coverageE,"aw",@nobits
	.type	_ZN2v88internal25FLAG_trace_block_coverageE, @object
	.size	_ZN2v88internal25FLAG_trace_block_coverageE, 1
_ZN2v88internal25FLAG_trace_block_coverageE:
	.zero	1
	.globl	_ZN2v88internal22FLAG_track_field_typesE
	.section	.data._ZN2v88internal22FLAG_track_field_typesE,"aw"
	.type	_ZN2v88internal22FLAG_track_field_typesE, @object
	.size	_ZN2v88internal22FLAG_track_field_typesE, 1
_ZN2v88internal22FLAG_track_field_typesE:
	.byte	1
	.globl	_ZN2v88internal26FLAG_track_computed_fieldsE
	.section	.data._ZN2v88internal26FLAG_track_computed_fieldsE,"aw"
	.type	_ZN2v88internal26FLAG_track_computed_fieldsE, @object
	.size	_ZN2v88internal26FLAG_track_computed_fieldsE, 1
_ZN2v88internal26FLAG_track_computed_fieldsE:
	.byte	1
	.globl	_ZN2v88internal29FLAG_track_heap_object_fieldsE
	.section	.data._ZN2v88internal29FLAG_track_heap_object_fieldsE,"aw"
	.type	_ZN2v88internal29FLAG_track_heap_object_fieldsE, @object
	.size	_ZN2v88internal29FLAG_track_heap_object_fieldsE, 1
_ZN2v88internal29FLAG_track_heap_object_fieldsE:
	.byte	1
	.globl	_ZN2v88internal24FLAG_track_double_fieldsE
	.section	.data._ZN2v88internal24FLAG_track_double_fieldsE,"aw"
	.type	_ZN2v88internal24FLAG_track_double_fieldsE, @object
	.size	_ZN2v88internal24FLAG_track_double_fieldsE, 1
_ZN2v88internal24FLAG_track_double_fieldsE:
	.byte	1
	.globl	_ZN2v88internal17FLAG_track_fieldsE
	.section	.data._ZN2v88internal17FLAG_track_fieldsE,"aw"
	.type	_ZN2v88internal17FLAG_track_fieldsE, @object
	.size	_ZN2v88internal17FLAG_track_fieldsE, 1
_ZN2v88internal17FLAG_track_fieldsE:
	.byte	1
	.globl	_ZN2v88internal33FLAG_trace_pretenuring_statisticsE
	.section	.bss._ZN2v88internal33FLAG_trace_pretenuring_statisticsE,"aw",@nobits
	.type	_ZN2v88internal33FLAG_trace_pretenuring_statisticsE, @object
	.size	_ZN2v88internal33FLAG_trace_pretenuring_statisticsE, 1
_ZN2v88internal33FLAG_trace_pretenuring_statisticsE:
	.zero	1
	.globl	_ZN2v88internal22FLAG_trace_pretenuringE
	.section	.bss._ZN2v88internal22FLAG_trace_pretenuringE,"aw",@nobits
	.type	_ZN2v88internal22FLAG_trace_pretenuringE, @object
	.size	_ZN2v88internal22FLAG_trace_pretenuringE, 1
_ZN2v88internal22FLAG_trace_pretenuringE:
	.zero	1
	.globl	_ZN2v88internal29FLAG_page_promotion_thresholdE
	.section	.data._ZN2v88internal29FLAG_page_promotion_thresholdE,"aw"
	.align 4
	.type	_ZN2v88internal29FLAG_page_promotion_thresholdE, @object
	.size	_ZN2v88internal29FLAG_page_promotion_thresholdE, 4
_ZN2v88internal29FLAG_page_promotion_thresholdE:
	.long	70
	.globl	_ZN2v88internal19FLAG_page_promotionE
	.section	.data._ZN2v88internal19FLAG_page_promotionE,"aw"
	.type	_ZN2v88internal19FLAG_page_promotionE, @object
	.size	_ZN2v88internal19FLAG_page_promotionE, 1
_ZN2v88internal19FLAG_page_promotionE:
	.byte	1
	.globl	_ZN2v88internal32FLAG_allocation_site_pretenuringE
	.section	.data._ZN2v88internal32FLAG_allocation_site_pretenuringE,"aw"
	.type	_ZN2v88internal32FLAG_allocation_site_pretenuringE, @object
	.size	_ZN2v88internal32FLAG_allocation_site_pretenuringE, 1
_ZN2v88internal32FLAG_allocation_site_pretenuringE:
	.byte	1
	.globl	_ZN2v88internal17FLAG_assert_typesE
	.section	.bss._ZN2v88internal17FLAG_assert_typesE,"aw",@nobits
	.type	_ZN2v88internal17FLAG_assert_typesE, @object
	.size	_ZN2v88internal17FLAG_assert_typesE, 1
_ZN2v88internal17FLAG_assert_typesE:
	.zero	1
	.globl	_ZN2v88internal11FLAG_futureE
	.section	.bss._ZN2v88internal11FLAG_futureE,"aw",@nobits
	.type	_ZN2v88internal11FLAG_futureE, @object
	.size	_ZN2v88internal11FLAG_futureE, 1
_ZN2v88internal11FLAG_futureE:
	.zero	1
	.globl	_ZN2v88internal14FLAG_lite_modeE
	.section	.bss._ZN2v88internal14FLAG_lite_modeE,"aw",@nobits
	.type	_ZN2v88internal14FLAG_lite_modeE, @object
	.size	_ZN2v88internal14FLAG_lite_modeE, 1
_ZN2v88internal14FLAG_lite_modeE:
	.zero	1
	.globl	_ZN2v88internal22FLAG_icu_timezone_dataE
	.section	.data._ZN2v88internal22FLAG_icu_timezone_dataE,"aw"
	.type	_ZN2v88internal22FLAG_icu_timezone_dataE, @object
	.size	_ZN2v88internal22FLAG_icu_timezone_dataE, 1
_ZN2v88internal22FLAG_icu_timezone_dataE:
	.byte	1
	.globl	_ZN2v88internal38FLAG_harmony_intl_numberformat_unifiedE
	.section	.data._ZN2v88internal38FLAG_harmony_intl_numberformat_unifiedE,"aw"
	.type	_ZN2v88internal38FLAG_harmony_intl_numberformat_unifiedE, @object
	.size	_ZN2v88internal38FLAG_harmony_intl_numberformat_unifiedE, 1
_ZN2v88internal38FLAG_harmony_intl_numberformat_unifiedE:
	.byte	1
	.globl	_ZN2v88internal32FLAG_harmony_intl_datetime_styleE
	.section	.data._ZN2v88internal32FLAG_harmony_intl_datetime_styleE,"aw"
	.type	_ZN2v88internal32FLAG_harmony_intl_datetime_styleE, @object
	.size	_ZN2v88internal32FLAG_harmony_intl_datetime_styleE, 1
_ZN2v88internal32FLAG_harmony_intl_datetime_styleE:
	.byte	1
	.globl	_ZN2v88internal35FLAG_harmony_intl_date_format_rangeE
	.section	.data._ZN2v88internal35FLAG_harmony_intl_date_format_rangeE,"aw"
	.type	_ZN2v88internal35FLAG_harmony_intl_date_format_rangeE, @object
	.size	_ZN2v88internal35FLAG_harmony_intl_date_format_rangeE, 1
_ZN2v88internal35FLAG_harmony_intl_date_format_rangeE:
	.byte	1
	.globl	_ZN2v88internal24FLAG_harmony_intl_bigintE
	.section	.data._ZN2v88internal24FLAG_harmony_intl_bigintE,"aw"
	.type	_ZN2v88internal24FLAG_harmony_intl_bigintE, @object
	.size	_ZN2v88internal24FLAG_harmony_intl_bigintE, 1
_ZN2v88internal24FLAG_harmony_intl_bigintE:
	.byte	1
	.globl	_ZN2v88internal32FLAG_harmony_promise_all_settledE
	.section	.data._ZN2v88internal32FLAG_harmony_promise_all_settledE,"aw"
	.type	_ZN2v88internal32FLAG_harmony_promise_all_settledE, @object
	.size	_ZN2v88internal32FLAG_harmony_promise_all_settledE, 1
_ZN2v88internal32FLAG_harmony_promise_all_settledE:
	.byte	1
	.globl	_ZN2v88internal27FLAG_harmony_dynamic_importE
	.section	.data._ZN2v88internal27FLAG_harmony_dynamic_importE,"aw"
	.type	_ZN2v88internal27FLAG_harmony_dynamic_importE, @object
	.size	_ZN2v88internal27FLAG_harmony_dynamic_importE, 1
_ZN2v88internal27FLAG_harmony_dynamic_importE:
	.byte	1
	.globl	_ZN2v88internal24FLAG_harmony_import_metaE
	.section	.data._ZN2v88internal24FLAG_harmony_import_metaE,"aw"
	.type	_ZN2v88internal24FLAG_harmony_import_metaE, @object
	.size	_ZN2v88internal24FLAG_harmony_import_metaE, 1
_ZN2v88internal24FLAG_harmony_import_metaE:
	.byte	1
	.globl	_ZN2v88internal30FLAG_harmony_sharedarraybufferE
	.section	.data._ZN2v88internal30FLAG_harmony_sharedarraybufferE,"aw"
	.type	_ZN2v88internal30FLAG_harmony_sharedarraybufferE, @object
	.size	_ZN2v88internal30FLAG_harmony_sharedarraybufferE, 1
_ZN2v88internal30FLAG_harmony_sharedarraybufferE:
	.byte	1
	.globl	_ZN2v88internal30FLAG_harmony_namespace_exportsE
	.section	.data._ZN2v88internal30FLAG_harmony_namespace_exportsE,"aw"
	.type	_ZN2v88internal30FLAG_harmony_namespace_exportsE, @object
	.size	_ZN2v88internal30FLAG_harmony_namespace_exportsE, 1
_ZN2v88internal30FLAG_harmony_namespace_exportsE:
	.byte	1
	.globl	_ZN2v88internal27FLAG_harmony_intl_segmenterE
	.section	.bss._ZN2v88internal27FLAG_harmony_intl_segmenterE,"aw",@nobits
	.type	_ZN2v88internal27FLAG_harmony_intl_segmenterE, @object
	.size	_ZN2v88internal27FLAG_harmony_intl_segmenterE, 1
_ZN2v88internal27FLAG_harmony_intl_segmenterE:
	.zero	1
	.globl	_ZN2v88internal53FLAG_harmony_intl_dateformat_fractional_second_digitsE
	.section	.bss._ZN2v88internal53FLAG_harmony_intl_dateformat_fractional_second_digitsE,"aw",@nobits
	.type	_ZN2v88internal53FLAG_harmony_intl_dateformat_fractional_second_digitsE, @object
	.size	_ZN2v88internal53FLAG_harmony_intl_dateformat_fractional_second_digitsE, 1
_ZN2v88internal53FLAG_harmony_intl_dateformat_fractional_second_digitsE:
	.zero	1
	.globl	_ZN2v88internal39FLAG_harmony_intl_dateformat_day_periodE
	.section	.bss._ZN2v88internal39FLAG_harmony_intl_dateformat_day_periodE,"aw",@nobits
	.type	_ZN2v88internal39FLAG_harmony_intl_dateformat_day_periodE, @object
	.size	_ZN2v88internal39FLAG_harmony_intl_dateformat_day_periodE, 1
_ZN2v88internal39FLAG_harmony_intl_dateformat_day_periodE:
	.zero	1
	.globl	_ZN2v88internal47FLAG_harmony_intl_add_calendar_numbering_systemE
	.section	.bss._ZN2v88internal47FLAG_harmony_intl_add_calendar_numbering_systemE,"aw",@nobits
	.type	_ZN2v88internal47FLAG_harmony_intl_add_calendar_numbering_systemE, @object
	.size	_ZN2v88internal47FLAG_harmony_intl_add_calendar_numbering_systemE, 1
_ZN2v88internal47FLAG_harmony_intl_add_calendar_numbering_systemE:
	.zero	1
	.globl	_ZN2v88internal36FLAG_harmony_intl_dateformat_quarterE
	.section	.bss._ZN2v88internal36FLAG_harmony_intl_dateformat_quarterE,"aw",@nobits
	.type	_ZN2v88internal36FLAG_harmony_intl_dateformat_quarterE, @object
	.size	_ZN2v88internal36FLAG_harmony_intl_dateformat_quarterE, 1
_ZN2v88internal36FLAG_harmony_intl_dateformat_quarterE:
	.zero	1
	.globl	_ZN2v88internal20FLAG_harmony_nullishE
	.section	.bss._ZN2v88internal20FLAG_harmony_nullishE,"aw",@nobits
	.type	_ZN2v88internal20FLAG_harmony_nullishE, @object
	.size	_ZN2v88internal20FLAG_harmony_nullishE, 1
_ZN2v88internal20FLAG_harmony_nullishE:
	.zero	1
	.globl	_ZN2v88internal30FLAG_harmony_optional_chainingE
	.section	.bss._ZN2v88internal30FLAG_harmony_optional_chainingE,"aw",@nobits
	.type	_ZN2v88internal30FLAG_harmony_optional_chainingE, @object
	.size	_ZN2v88internal30FLAG_harmony_optional_chainingE, 1
_ZN2v88internal30FLAG_harmony_optional_chainingE:
	.zero	1
	.globl	_ZN2v88internal22FLAG_harmony_weak_refsE
	.section	.bss._ZN2v88internal22FLAG_harmony_weak_refsE,"aw",@nobits
	.type	_ZN2v88internal22FLAG_harmony_weak_refsE, @object
	.size	_ZN2v88internal22FLAG_harmony_weak_refsE, 1
_ZN2v88internal22FLAG_harmony_weak_refsE:
	.zero	1
	.globl	_ZN2v88internal28FLAG_harmony_regexp_sequenceE
	.section	.bss._ZN2v88internal28FLAG_harmony_regexp_sequenceE,"aw",@nobits
	.type	_ZN2v88internal28FLAG_harmony_regexp_sequenceE, @object
	.size	_ZN2v88internal28FLAG_harmony_regexp_sequenceE, 1
_ZN2v88internal28FLAG_harmony_regexp_sequenceE:
	.zero	1
	.globl	_ZN2v88internal28FLAG_harmony_private_methodsE
	.section	.bss._ZN2v88internal28FLAG_harmony_private_methodsE,"aw",@nobits
	.type	_ZN2v88internal28FLAG_harmony_private_methodsE, @object
	.size	_ZN2v88internal28FLAG_harmony_private_methodsE, 1
_ZN2v88internal28FLAG_harmony_private_methodsE:
	.zero	1
	.globl	_ZN2v88internal21FLAG_harmony_shippingE
	.section	.data._ZN2v88internal21FLAG_harmony_shippingE,"aw"
	.type	_ZN2v88internal21FLAG_harmony_shippingE, @object
	.size	_ZN2v88internal21FLAG_harmony_shippingE, 1
_ZN2v88internal21FLAG_harmony_shippingE:
	.byte	1
	.globl	_ZN2v88internal12FLAG_harmonyE
	.section	.bss._ZN2v88internal12FLAG_harmonyE,"aw",@nobits
	.type	_ZN2v88internal12FLAG_harmonyE, @object
	.size	_ZN2v88internal12FLAG_harmonyE, 1
_ZN2v88internal12FLAG_harmonyE:
	.zero	1
	.globl	_ZN2v88internal15FLAG_es_stagingE
	.section	.bss._ZN2v88internal15FLAG_es_stagingE,"aw",@nobits
	.type	_ZN2v88internal15FLAG_es_stagingE, @object
	.size	_ZN2v88internal15FLAG_es_stagingE, 1
_ZN2v88internal15FLAG_es_stagingE:
	.zero	1
	.globl	_ZN2v88internal15FLAG_use_strictE
	.section	.bss._ZN2v88internal15FLAG_use_strictE,"aw",@nobits
	.type	_ZN2v88internal15FLAG_use_strictE, @object
	.size	_ZN2v88internal15FLAG_use_strictE, 1
_ZN2v88internal15FLAG_use_strictE:
	.zero	1
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC33:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC34:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC35:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
