	.file	"wasm-objects.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4878:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4878:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4879:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4879:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4881:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4881:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal4wasm7Decoder12onFirstErrorEv,"axG",@progbits,_ZN2v88internal4wasm7Decoder12onFirstErrorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.type	_ZN2v88internal4wasm7Decoder12onFirstErrorEv, @function
_ZN2v88internal4wasm7Decoder12onFirstErrorEv:
.LFB21499:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21499:
	.size	_ZN2v88internal4wasm7Decoder12onFirstErrorEv, .-_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB28825:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28825:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB28832:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28832:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB28834:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28834:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB28827:
	.cfi_startproc
	endbr64
	movl	$80, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28827:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_127FindBreakpointInfoInsertPosEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_127FindBreakpointInfoInsertPosEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEi, @function
_ZN2v88internal12_GLOBAL__N_127FindBreakpointInfoInsertPosEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEi:
.LFB22586:
	.cfi_startproc
	movq	(%rsi), %r11
	movslq	11(%r11), %r10
	movl	%r10d, %eax
	cmpq	$1, %r10
	jle	.L19
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L12:
	sarl	%eax
	leal	(%rax,%r9), %r8d
	leal	2(%r8), %esi
	leal	0(,%rsi,8), %ecx
	movslq	%ecx, %rcx
	movq	-1(%r11,%rcx), %rcx
	cmpq	%rcx, 88(%rdi)
	je	.L21
	movl	11(%rcx), %ecx
.L15:
	cmpl	%ecx, %edx
	jl	.L23
	movl	%r10d, %eax
	subl	%r8d, %eax
	cmpl	$1, %eax
	jle	.L13
	movl	%r8d, %r9d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L23:
	cmpl	$1, %eax
	je	.L20
	movl	%r8d, %r10d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$2147483647, %ecx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L20:
	movl	%r9d, %r8d
	leal	2(%r9), %esi
.L13:
	sall	$3, %esi
	movslq	%esi, %rsi
.L11:
	movq	-1(%r11,%rsi), %rax
	cmpq	%rax, 88(%rdi)
	je	.L10
	cmpl	%edx, 11(%rax)
	setl	%al
	movzbl	%al, %eax
	addl	%eax, %r8d
.L10:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$16, %esi
	xorl	%r8d, %r8d
	jmp	.L11
	.cfi_endproc
.LFE22586:
	.size	_ZN2v88internal12_GLOBAL__N_127FindBreakpointInfoInsertPosEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEi, .-_ZN2v88internal12_GLOBAL__N_127FindBreakpointInfoInsertPosEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEi
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB28836:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE28836:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB28829:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE28829:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB28835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L26
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28835:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN2v88internal4wasm7DecoderD2Ev,"axG",@progbits,_ZN2v88internal4wasm7DecoderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7DecoderD2Ev
	.type	_ZN2v88internal4wasm7DecoderD2Ev, @function
_ZN2v88internal4wasm7DecoderD2Ev:
.LFB28012:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %r8
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	addq	$64, %rdi
	movq	%rax, -64(%rdi)
	cmpq	%rdi, %r8
	je	.L33
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L33:
	ret
	.cfi_endproc
.LFE28012:
	.size	_ZN2v88internal4wasm7DecoderD2Ev, .-_ZN2v88internal4wasm7DecoderD2Ev
	.weak	_ZN2v88internal4wasm7DecoderD1Ev
	.set	_ZN2v88internal4wasm7DecoderD1Ev,_ZN2v88internal4wasm7DecoderD2Ev
	.section	.text._ZN2v88internal4wasm7DecoderD0Ev,"axG",@progbits,_ZN2v88internal4wasm7DecoderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7DecoderD0Ev
	.type	_ZN2v88internal4wasm7DecoderD0Ev, @function
_ZN2v88internal4wasm7DecoderD0Ev:
.LFB28014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L36
	call	_ZdlPv@PLT
.L36:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28014:
	.size	_ZN2v88internal4wasm7DecoderD0Ev, .-_ZN2v88internal4wasm7DecoderD0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB28837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L38
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L38:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28837:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB28830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L42
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L42:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28830:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB28828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	free@PLT
	movq	24(%rbx), %rdi
	call	free@PLT
	movq	32(%rbx), %rdi
	call	free@PLT
	movq	40(%rbx), %rdi
	call	free@PLT
	movq	48(%rbx), %rdi
	call	free@PLT
	movq	56(%rbx), %rdi
	call	free@PLT
	movq	64(%rbx), %rdi
	call	free@PLT
	movq	72(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.cfi_endproc
.LFE28828:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN2v88internal12_GLOBAL__N_123AdjustBufferPermissionsEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEm,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_123AdjustBufferPermissionsEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEm, @function
_ZN2v88internal12_GLOBAL__N_123AdjustBufferPermissionsEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEm:
.LFB22671:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$16, %rsp
	movq	(%rsi), %rax
	movq	31(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L65
.L49:
	cmpq	%rsi, %rbx
	jbe	.L51
.L54:
	xorl	%eax, %eax
.L48:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	23(%rax), %r12
	cmpq	%r12, %rbx
	je	.L57
	movq	31(%rax), %r14
	call	_ZN2v88internal24GetPlatformPageAllocatorEv@PLT
	movl	$2, %ecx
	movq	%rbx, %rdx
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal14SetPermissionsEPNS_13PageAllocatorEPvmNS1_10PermissionE@PLT
	testb	%al, %al
	je	.L54
	subq	%r12, %rbx
	movq	32(%r13), %r12
	addq	%rbx, %r12
	movq	%r12, %rdx
	subq	48(%r13), %rdx
	movq	%r12, 32(%r13)
	cmpq	$33554432, %rdx
	jg	.L66
.L55:
	movq	40(%r13), %rdx
	testq	%rbx, %rbx
	js	.L67
	cmpq	%rdx, %r12
	jg	.L68
.L57:
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movl	39(%rax), %edx
	andl	$16, %edx
	jne	.L69
	movq	23(%rax), %rsi
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L69:
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	8160(%rax), %rdi
	call	_ZN2v88internal4wasm17WasmMemoryTracker18FindAllocationDataEPKv@PLT
	movq	8(%rax), %rsi
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L67:
	addq	%rdx, %rbx
	cmpq	$67108864, %rbx
	jle	.L57
	movq	%rbx, 40(%r13)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%r13, %rdi
	movb	%al, -33(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movzbl	-33(%rbp), %eax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%r13, %rdi
	movb	%al, -33(%rbp)
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	movzbl	-33(%rbp), %eax
	jmp	.L48
	.cfi_endproc
.LFE22671:
	.size	_ZN2v88internal12_GLOBAL__N_123AdjustBufferPermissionsEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEm, .-_ZN2v88internal12_GLOBAL__N_123AdjustBufferPermissionsEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEm
	.section	.text._ZN2v88internal7ManagedINS0_12_GLOBAL__N_129WasmInstanceNativeAllocationsEE10DestructorEPv,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal7ManagedINS0_12_GLOBAL__N_129WasmInstanceNativeAllocationsEE10DestructorEPv, @function
_ZN2v88internal7ManagedINS0_12_GLOBAL__N_129WasmInstanceNativeAllocationsEE10DestructorEPv:
.LFB27216:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L70
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L73
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L74
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L75:
	cmpl	$1, %eax
	je	.L82
.L73:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L70:
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L77
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L78:
	cmpl	$1, %eax
	jne	.L73
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L74:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L77:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L78
	.cfi_endproc
.LFE27216:
	.size	_ZN2v88internal7ManagedINS0_12_GLOBAL__N_129WasmInstanceNativeAllocationsEE10DestructorEPv, .-_ZN2v88internal7ManagedINS0_12_GLOBAL__N_129WasmInstanceNativeAllocationsEE10DestructorEPv
	.section	.text._ZN2v88internal7ManagedINS0_4wasm12NativeModuleEE10DestructorEPv,"axG",@progbits,_ZN2v88internal7ManagedINS0_4wasm12NativeModuleEE10DestructorEPv,comdat
	.p2align 4
	.weak	_ZN2v88internal7ManagedINS0_4wasm12NativeModuleEE10DestructorEPv
	.type	_ZN2v88internal7ManagedINS0_4wasm12NativeModuleEE10DestructorEPv, @function
_ZN2v88internal7ManagedINS0_4wasm12NativeModuleEE10DestructorEPv:
.LFB26204:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L83
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L86
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L87
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L88:
	cmpl	$1, %eax
	je	.L95
.L86:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L83:
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L90
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L91:
	cmpl	$1, %eax
	jne	.L86
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L87:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L90:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L91
	.cfi_endproc
.LFE26204:
	.size	_ZN2v88internal7ManagedINS0_4wasm12NativeModuleEE10DestructorEPv, .-_ZN2v88internal7ManagedINS0_4wasm12NativeModuleEE10DestructorEPv
	.section	.text._ZN2v88internal7ManagedINS0_12_GLOBAL__N_120IftNativeAllocationsEE10DestructorEPv,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal7ManagedINS0_12_GLOBAL__N_120IftNativeAllocationsEE10DestructorEPv, @function
_ZN2v88internal7ManagedINS0_12_GLOBAL__N_120IftNativeAllocationsEE10DestructorEPv:
.LFB27198:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L96
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L99
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L100
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L101:
	cmpl	$1, %eax
	je	.L108
.L99:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L96:
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L103
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L104:
	cmpl	$1, %eax
	jne	.L99
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L100:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L103:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L104
	.cfi_endproc
.LFE27198:
	.size	_ZN2v88internal7ManagedINS0_12_GLOBAL__N_120IftNativeAllocationsEE10DestructorEPv, .-_ZN2v88internal7ManagedINS0_12_GLOBAL__N_120IftNativeAllocationsEE10DestructorEPv
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB21753:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L115
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L118
.L109:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L109
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE21753:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.text._ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEENS8_INS0_10FixedArrayEEEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEENS8_INS0_10FixedArrayEEEm
	.type	_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEENS8_INS0_10FixedArrayEEEm, @function
_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEENS8_INS0_10FixedArrayEEEm:
.LFB22583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %rax
	movq	208(%rax), %rdi
	call	_ZN2v88internal4wasm15WasmCodeManager31EstimateNativeModuleNonCodeSizeEPKNS1_10WasmModuleE@PLT
	addq	%rax, %r13
	movq	32(%rbx), %rax
	addq	%r13, %rax
	movq	%rax, %rdx
	subq	48(%rbx), %rdx
	movq	%rax, 32(%rbx)
	cmpq	$33554432, %rdx
	jg	.L186
.L120:
	testq	%r13, %r13
	js	.L187
	jne	.L188
.L122:
	movl	$16, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm1
	movq	%rax, %rdx
	movq	8(%r12), %rax
	movups	%xmm1, (%rdx)
	testq	%rax, %rax
	je	.L123
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L124
	lock addl	$1, 8(%rax)
.L123:
	movl	$48, %edi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%rax, %r12
	movups	%xmm0, 8(%rax)
	movq	%r13, (%rax)
	movq	%r12, %rsi
	movq	%rdx, 24(%rax)
	leaq	_ZN2v88internal7ManagedINS0_4wasm12NativeModuleEE10DestructorEPv(%rip), %rax
	xorl	%edx, %edx
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	41152(%rbx), %rdi
	movq	(%rax), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE@GOTPCREL(%rip), %rdx
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	movq	%rax, 40(%r12)
	movq	%rax, %rdi
	call	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE@PLT
	movq	12464(%rbx), %rax
	movq	39(%rax), %rax
	movq	1503(%rax), %r12
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L125
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L126:
	movq	%rbx, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%r15), %r15
	movq	(%rax), %rdi
	movq	%rax, %r12
	movq	%r15, 31(%rdi)
	leaq	31(%rdi), %rsi
	testb	$1, %r15b
	je	.L148
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L189
	testb	$24, %al
	je	.L148
.L198:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L190
	.p2align 4,,10
	.p2align 3
.L148:
	movq	(%r14), %r15
	movq	(%r12), %r8
	cmpl	$3, 51(%r15)
	je	.L191
.L131:
	movq	%r15, 39(%r8)
	leaq	39(%r8), %rsi
	testb	$1, %r15b
	je	.L146
	movq	%r15, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L192
	testb	$24, %al
	je	.L146
.L201:
	movq	%r8, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L193
	.p2align 4,,10
	.p2align 3
.L146:
	movq	(%r12), %r15
	movq	1080(%rbx), %r14
	movq	%r14, 47(%r15)
	leaq	47(%r15), %rsi
	testb	$1, %r14b
	je	.L145
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L194
	testb	$24, %al
	je	.L145
.L200:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L195
	.p2align 4,,10
	.p2align 3
.L145:
	movq	(%r12), %r14
	movq	0(%r13), %r13
	movq	%r13, 23(%r14)
	leaq	23(%r14), %r15
	testb	$1, %r13b
	je	.L144
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L196
	testb	$24, %al
	je	.L144
.L199:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L197
.L144:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	addl	$1, 8(%rax)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L189:
	movq	%r15, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L198
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L196:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L199
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L200
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L192:
	movq	%r8, %rdi
	movq	%r15, %rdx
	movq	%rsi, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %r8
	testb	$24, %al
	jne	.L201
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L188:
	cmpq	40(%rbx), %rax
	jle	.L122
	movq	%rbx, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L125:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L202
.L127:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, (%rsi)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L187:
	movq	40(%rbx), %rax
	addq	%r13, %rax
	cmpq	$67108864, %rax
	jle	.L122
	movq	%rax, 40(%rbx)
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L186:
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-56(%rbp), %rax
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L191:
	movq	%r8, 71(%r15)
	leaq	71(%r15), %rsi
	testb	$1, %r8b
	je	.L147
	movq	%r8, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L133
	movq	%r8, %rdx
	movq	%r15, %rdi
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L133:
	testb	$24, %al
	je	.L147
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L203
	.p2align 4,,10
	.p2align 3
.L147:
	movq	(%r14), %r15
	movq	(%r12), %r8
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%r15, %rdx
	movq	%r8, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L195:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L197:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L202:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L203:
	movq	%r8, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L147
	.cfi_endproc
.LFE22583:
	.size	_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEENS8_INS0_10FixedArrayEEEm, .-_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEENS8_INS0_10FixedArrayEEEm
	.section	.text._ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEENS8_INS0_10FixedArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEENS8_INS0_10FixedArrayEEE
	.type	_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEENS8_INS0_10FixedArrayEEE, @function
_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEENS8_INS0_10FixedArrayEEE:
.LFB22582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	208(%rax), %rdi
	call	_ZN2v88internal4wasm15WasmCodeManager28EstimateNativeModuleCodeSizeEPKNS1_10WasmModuleE@PLT
	movdqu	(%rbx), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, %r8
	movups	%xmm0, (%rbx)
	leaq	-64(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movaps	%xmm1, -64(%rbp)
	call	_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEENS8_INS0_10FixedArrayEEEm
	movq	-56(%rbp), %r12
	testq	%r12, %r12
	je	.L206
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L207
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r12)
	cmpl	$1, %edx
	je	.L214
	.p2align 4,,10
	.p2align 3
.L206:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L215
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movl	8(%r12), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r12)
	cmpl	$1, %edx
	jne	.L206
.L214:
	movq	(%r12), %rdx
	movq	%rax, -72(%rbp)
	movq	%r12, %rdi
	call	*16(%rdx)
	testq	%rbx, %rbx
	movq	-72(%rbp), %rax
	je	.L210
	movl	$-1, %edx
	lock xaddl	%edx, 12(%r12)
.L211:
	cmpl	$1, %edx
	jne	.L206
	movq	(%r12), %rdx
	movq	%rax, -72(%rbp)
	movq	%r12, %rdi
	call	*24(%rdx)
	movq	-72(%rbp), %rax
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L210:
	movl	12(%r12), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%r12)
	jmp	.L211
.L215:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22582:
	.size	_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEENS8_INS0_10FixedArrayEEE, .-_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEENS8_INS0_10FixedArrayEEE
	.section	.text._ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEE
	.type	_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEE, @function
_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEE:
.LFB22581:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rbx), %r13
	pxor	%xmm0, %xmm0
	movq	%rax, %r15
	movq	8(%rbx), %rax
	movups	%xmm0, (%rbx)
	movq	208(%r13), %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal4wasm15WasmCodeManager28EstimateNativeModuleCodeSizeEPKNS1_10WasmModuleE@PLT
	movq	%r13, %xmm0
	movq	%r12, %rdi
	movq	%r15, %rcx
	movhps	-88(%rbp), %xmm0
	movq	%rax, %r8
	leaq	-80(%rbp), %rsi
	movq	%r14, %rdx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEENS8_INS0_10FixedArrayEEEm
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	je	.L218
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L219
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r12)
	cmpl	$1, %edx
	je	.L226
	.p2align 4,,10
	.p2align 3
.L218:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L227
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movl	8(%r12), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r12)
	cmpl	$1, %edx
	jne	.L218
.L226:
	movq	(%r12), %rdx
	movq	%rax, -88(%rbp)
	movq	%r12, %rdi
	call	*16(%rdx)
	testq	%rbx, %rbx
	movq	-88(%rbp), %rax
	je	.L222
	movl	$-1, %edx
	lock xaddl	%edx, 12(%r12)
.L223:
	cmpl	$1, %edx
	jne	.L218
	movq	(%r12), %rdx
	movq	%rax, -88(%rbp)
	movq	%r12, %rdi
	call	*24(%rdx)
	movq	-88(%rbp), %rax
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L222:
	movl	12(%r12), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%r12)
	jmp	.L223
.L227:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22581:
	.size	_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEE, .-_ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEE
	.section	.text._ZN2v88internal16WasmModuleObject13AddBreakpointENS0_6HandleIS1_EEiNS2_INS0_10BreakPointEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject13AddBreakpointENS0_6HandleIS1_EEiNS2_INS0_10BreakPointEEE
	.type	_ZN2v88internal16WasmModuleObject13AddBreakpointENS0_6HandleIS1_EEiNS2_INS0_10BreakPointEEE, @function
_ZN2v88internal16WasmModuleObject13AddBreakpointENS0_6HandleIS1_EEiNS2_INS0_10BreakPointEEE:
.LFB22587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rdx, -112(%rbp)
	movq	(%rdi), %rdx
	movl	%esi, -60(%rbp)
	movq	%rdx, %rax
	movq	63(%rdx), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	leaq	-37592(%rax), %r13
	cmpq	-37504(%rax), %rsi
	je	.L229
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L230
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L231:
	movl	-60(%rbp), %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_127FindBreakpointInfoInsertPosEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEi
	movq	(%r15), %rdx
	movl	%eax, %r9d
	movslq	11(%rdx), %rax
	movl	%eax, %ecx
	cmpl	%eax, %r9d
	leal	16(,%r9,8), %eax
	cltq
	movq	%rax, -104(%rbp)
	jge	.L236
	movq	-1(%rdx,%rax), %rax
	cmpq	%rax, 88(%r13)
	je	.L267
	movl	11(%rax), %eax
.L237:
	leaq	-1(%rdx), %rsi
	cmpl	%eax, -60(%rbp)
	je	.L238
	movl	11(%rdx), %ecx
.L239:
	leal	8(,%rcx,8), %eax
	movq	%r15, %r14
	cltq
	movq	(%rax,%rsi), %rax
	cmpq	%rax, 88(%r13)
	je	.L243
	movslq	11(%rdx), %rsi
	movq	%r13, %rdi
	movl	$1, %edx
	movl	%r9d, -56(%rbp)
	addl	%esi, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rbx
	movq	(%rax), %r12
	movq	%rax, %r14
	leaq	63(%rbx), %rsi
	movq	%r12, 63(%rbx)
	testb	$1, %r12b
	movl	-56(%rbp), %r9d
	je	.L265
	movq	%r12, %r10
	andq	$-262144, %r10
	movq	8(%r10), %rax
	movq	%r10, -56(%rbp)
	testl	$262144, %eax
	jne	.L305
.L247:
	testb	$24, %al
	je	.L265
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L306
	.p2align 4,,10
	.p2align 3
.L265:
	testl	%r9d, %r9d
	jle	.L304
	leal	-1(%r9), %eax
	movl	%r9d, -88(%rbp)
	movl	$16, %ebx
	leaq	24(,%rax,8), %r12
	movq	%r14, %rax
	movq	%r13, -96(%rbp)
	movq	%r15, %r14
	movq	%r12, %r13
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L252:
	movq	(%r14), %rax
	movq	(%r15), %rdi
	movq	-1(%rbx,%rax), %r12
	leaq	-1(%rdi,%rbx), %rsi
	movq	%r12, (%rsi)
	testb	$1, %r12b
	je	.L264
	movq	%r12, %r9
	andq	$-262144, %r9
	movq	8(%r9), %rax
	movq	%r9, -56(%rbp)
	testl	$262144, %eax
	je	.L250
	movq	%r12, %rdx
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r9
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movq	8(%r9), %rax
.L250:
	testb	$24, %al
	je	.L264
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L264
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L264:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L252
	movq	%r15, %rax
	movl	-88(%rbp), %r9d
	movq	-96(%rbp), %r13
	movq	%r14, %r15
	movq	%rax, %r14
.L304:
	movq	(%r15), %rdx
.L243:
	movslq	11(%rdx), %rsi
	cmpl	%r9d, 11(%rdx)
	jle	.L253
	notl	%r9d
	leal	8(,%rsi,8), %eax
	movq	%r14, %r8
	movq	%r15, %rcx
	leal	(%r9,%rsi), %edi
	movq	%rsi, %r9
	sall	$3, %esi
	movslq	%eax, %rbx
	subq	%rdi, %r9
	subl	%eax, %esi
	salq	$3, %r9
	leal	16(%rsi), %eax
	movq	%r9, %r14
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L307:
	movq	(%rcx), %rdx
.L258:
	movq	-1(%rdx,%rbx), %r12
	cmpq	%r12, 88(%r13)
	je	.L254
	movq	(%r8), %r15
	leal	(%rax,%rbx), %edx
	movslq	%edx, %rdx
	leaq	-1(%r15,%rdx), %rsi
	movq	%r12, (%rsi)
	testb	$1, %r12b
	je	.L254
	movq	%r12, %r9
	andq	$-262144, %r9
	movq	8(%r9), %rdx
	movq	%r9, -56(%rbp)
	testl	$262144, %edx
	je	.L256
	movq	%r12, %rdx
	movq	%r15, %rdi
	movl	%eax, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r9
	movl	-96(%rbp), %eax
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rcx
	movq	8(%r9), %rdx
	movq	-72(%rbp), %rsi
.L256:
	andl	$24, %edx
	je	.L254
	movq	%r15, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L254
	movq	%r12, %rdx
	movq	%r15, %rdi
	movl	%eax, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	-80(%rbp), %eax
	movq	-72(%rbp), %r8
	movq	-56(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L254:
	subq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L307
	movq	%r8, %r14
.L253:
	movl	-60(%rbp), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory17NewBreakPointInfoEi@PLT
	movq	-112(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal14BreakPointInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE@PLT
	movq	(%r14), %r13
	movq	-104(%rbp), %rax
	movq	(%rbx), %r12
	leaq	-1(%r13,%rax), %r14
	movq	%r12, (%r14)
	testb	$1, %r12b
	je	.L228
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L260
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L260:
	testb	$24, %al
	je	.L228
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L308
.L228:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movq	41088(%r13), %r15
	cmpq	%r15, 41096(%r13)
	je	.L309
.L232:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L236:
	leaq	-1(%rdx), %rsi
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L229:
	movl	$4, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rbx), %r14
	movq	(%rax), %r12
	movq	%rax, %r15
	leaq	63(%r14), %rsi
	movq	%r12, 63(%r14)
	testb	$1, %r12b
	je	.L231
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L234
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	8(%rcx), %rax
.L234:
	testb	$24, %al
	je	.L231
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L231
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L308:
	addq	$72, %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movl	%r9d, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r10
	movl	-80(%rbp), %r9d
	movq	-72(%rbp), %rsi
	movq	8(%r10), %rax
	jmp	.L247
.L306:
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movl	%r9d, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	-56(%rbp), %r9d
	jmp	.L265
.L309:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L232
.L267:
	movl	$2147483647, %eax
	jmp	.L237
.L238:
	movq	-104(%rbp), %rax
	movq	-1(%rax,%rdx), %r12
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L310
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L244:
	movq	-112(%rbp), %rdx
	addq	$72, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14BreakPointInfo13SetBreakPointEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10BreakPointEEE@PLT
.L310:
	.cfi_restore_state
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L311
.L245:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r12, (%rsi)
	jmp	.L244
.L311:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L245
	.cfi_endproc
.LFE22587:
	.size	_ZN2v88internal16WasmModuleObject13AddBreakpointENS0_6HandleIS1_EEiNS2_INS0_10BreakPointEEE, .-_ZN2v88internal16WasmModuleObject13AddBreakpointENS0_6HandleIS1_EEiNS2_INS0_10BreakPointEEE
	.section	.text._ZN2v88internal16WasmModuleObject17GetSourcePositionENS0_6HandleIS1_EEjjb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject17GetSourcePositionENS0_6HandleIS1_EEjjb
	.type	_ZN2v88internal16WasmModuleObject17GetSourcePositionENS0_6HandleIS1_EEjjb, @function
_ZN2v88internal16WasmModuleObject17GetSourcePositionENS0_6HandleIS1_EEjjb:
.LFB22605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$152, %rsp
	movl	%edx, -132(%rbp)
	movl	%ecx, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	movq	24(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	208(%rdx), %r13
	cmpb	$0, 392(%r13)
	jne	.L313
	movq	136(%r13), %rdx
	movq	144(%r13), %rax
	subq	%rdx, %rax
	sarq	$5, %rax
	cmpq	%rbx, %rax
	jbe	.L348
	salq	$5, %rbx
	movl	16(%rdx,%rbx), %eax
.L314:
	addl	-132(%rbp), %eax
.L312:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L366
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	movq	%rax, %rdx
	movq	%rdi, %r12
	movq	55(%rax), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %r14
	movq	3520(%r14), %rdi
	subq	$37592, %r14
	testq	%rdi, %rdi
	je	.L316
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L317:
	movslq	11(%rsi), %rcx
	leaq	-1(%rsi), %rdi
	leal	15(%rcx), %edx
	movl	%ecx, %eax
	movslq	%edx, %rdx
	cmpb	$1, -1(%rsi,%rdx)
	je	.L319
	leaq	-128(%rbp), %rdi
	leaq	14(%rcx,%rsi), %rdx
	addq	$15, %rsi
	call	_ZN2v88internal4wasm18DecodeAsmJsOffsetsEPKhS3_@PLT
	movq	-128(%rbp), %rax
	movq	-96(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	$0, -112(%rbp)
	movq	%rax, -160(%rbp)
	movq	-120(%rbp), %rax
	movaps	%xmm0, -128(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L320
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %r15
	movq	%rax, -152(%rbp)
	cmpq	%r15, %rax
	je	.L321
	.p2align 4,,10
	.p2align 3
.L325:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L322
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	-152(%rbp), %r15
	jne	.L325
.L323:
	movq	-128(%rbp), %r15
.L321:
	testq	%r15, %r15
	je	.L320
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L320:
	movq	(%r12), %rax
	movq	-144(%rbp), %rcx
	movabsq	$-6148914691236517205, %rdi
	movq	-160(%rbp), %rsi
	movq	23(%rax), %rax
	subq	%rsi, %rcx
	movq	7(%rax), %rax
	sarq	$3, %rcx
	imulq	%rdi, %rcx
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	movl	60(%rax), %r15d
	testl	%ecx, %ecx
	jle	.L349
	leal	-1(%rcx), %eax
	movq	%rsi, %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	24(%rsi,%rax,8), %r8
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L327:
	movq	8(%rdx), %rax
	subq	(%rdx), %rax
	addq	$24, %rdx
	sarq	$2, %rax
	imulq	%rdi, %rax
	addl	%eax, %esi
	cmpq	%rdx, %r8
	jne	.L327
	leal	(%rsi,%rsi,2), %eax
	sall	$2, %eax
	leal	1(%rax), %esi
	addl	$16, %eax
	movslq	%eax, %r8
.L326:
	movq	%r14, %rdi
	movl	$1, %edx
	movq	%rcx, -168(%rbp)
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movq	-152(%rbp), %r8
	movq	%rax, %r14
	movq	(%rax), %rax
	movb	$1, -1(%r8,%rax)
	movq	(%r12), %rdi
	movq	(%r14), %rdx
	leaq	55(%rdi), %rsi
	movq	%rdx, 55(%rdi)
	testb	$1, %dl
	movq	-168(%rbp), %rcx
	je	.L346
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r8
	movq	%rax, -152(%rbp)
	testl	$262144, %r8d
	je	.L329
	movq	%rcx, -192(%rbp)
	movq	%rdx, -184(%rbp)
	movq	%rsi, -176(%rbp)
	movq	%rdi, -168(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-152(%rbp), %rax
	movq	-192(%rbp), %rcx
	movq	-184(%rbp), %rdx
	movq	-176(%rbp), %rsi
	movq	8(%rax), %r8
	movq	-168(%rbp), %rdi
.L329:
	andl	$24, %r8d
	je	.L346
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L367
	.p2align 4,,10
	.p2align 3
.L346:
	movq	(%r12), %rax
	movq	-160(%rbp), %r9
	xorl	%r11d, %r11d
	movq	%r13, %rdi
	leal	(%r15,%rcx), %r12d
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rsi
	testl	%ecx, %ecx
	jle	.L336
	.p2align 4,,10
	.p2align 3
.L337:
	movq	8(%r9), %rcx
	movq	(%r9), %r13
	cmpq	%r13, %rcx
	je	.L334
	movslq	%r15d, %rax
	movq	%rsi, -152(%rbp)
	leal	16(,%r11,4), %edx
	salq	$5, %rax
	addq	136(%rsi), %rax
	movslq	%edx, %rdx
	movl	16(%rax), %r10d
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L335:
	movl	(%rax), %r8d
	movq	(%r14), %rsi
	addq	$12, %rax
	addq	$12, %rdx
	addl	%r10d, %r8d
	movl	%r8d, -13(%rdx,%rsi)
	movl	-8(%rax), %r8d
	movq	(%r14), %rsi
	movl	%r8d, -9(%rdx,%rsi)
	movl	-4(%rax), %r8d
	movq	(%r14), %rsi
	movl	%r8d, -5(%rdx,%rsi)
	cmpq	%rax, %rcx
	jne	.L335
	subq	$12, %rcx
	movq	-152(%rbp), %rsi
	subq	%r13, %rcx
	shrq	$2, %rcx
	leal	3(%r11,%rcx), %r11d
.L334:
	addl	$1, %r15d
	addq	$24, %r9
	cmpl	%r15d, %r12d
	jne	.L337
	movq	%rdi, %r13
.L336:
	movq	-160(%rbp), %r12
	movq	-144(%rbp), %rax
	cmpq	%rax, %r12
	je	.L332
	.p2align 4,,10
	.p2align 3
.L333:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L340
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, -144(%rbp)
	jne	.L333
.L332:
	cmpq	$0, -160(%rbp)
	je	.L339
	movq	-160(%rbp), %rdi
	call	_ZdlPv@PLT
.L339:
	movq	(%r14), %rdi
	movl	11(%rdi), %eax
	subq	$1, %rdi
.L319:
	movslq	%eax, %rsi
	salq	$5, %rbx
	addq	136(%r13), %rbx
	xorl	%ecx, %ecx
	imulq	$715827883, %rsi, %rsi
	sarl	$31, %eax
	movl	-132(%rbp), %r8d
	addl	16(%rbx), %r8d
	sarq	$33, %rsi
	subl	%eax, %esi
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L368:
	sarl	%eax
	addl	%ecx, %eax
	leal	4(%rax,%rax,2), %edx
	sall	$2, %edx
	movslq	%edx, %rdx
	cmpl	%r8d, (%rdx,%rdi)
	cmova	%eax, %esi
	cmovbe	%eax, %ecx
.L343:
	movl	%esi, %eax
	subl	%ecx, %eax
	cmpl	$1, %eax
	jg	.L368
	xorl	%edx, %edx
	cmpb	$0, -136(%rbp)
	leal	(%rcx,%rcx,2), %eax
	setne	%dl
	leal	5(%rdx,%rax), %eax
	sall	$2, %eax
	cltq
	movl	(%rax,%rdi), %eax
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L316:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L369
.L318:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L340:
	addq	$24, %r12
	cmpq	%r12, -144(%rbp)
	jne	.L333
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L322:
	addq	$24, %r15
	cmpq	-152(%rbp), %r15
	jne	.L325
	jmp	.L323
.L367:
	movq	%rcx, -152(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-152(%rbp), %rcx
	jmp	.L346
.L369:
	movq	%r14, %rdi
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	jmp	.L318
.L349:
	movl	$16, %r8d
	movl	$1, %esi
	jmp	.L326
.L348:
	movl	$-1, %eax
	jmp	.L314
.L366:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22605:
	.size	_ZN2v88internal16WasmModuleObject17GetSourcePositionENS0_6HandleIS1_EEjjb, .-_ZN2v88internal16WasmModuleObject17GetSourcePositionENS0_6HandleIS1_EEjjb
	.section	.text._ZN2v88internal16WasmModuleObject19DisassembleFunctionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject19DisassembleFunctionEi
	.type	_ZN2v88internal16WasmModuleObject19DisassembleFunctionEi, @function
_ZN2v88internal16WasmModuleObject19DisassembleFunctionEi:
.LFB22606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	testl	%edx, %edx
	js	.L371
	movq	(%rsi), %rcx
	movq	%rsi, %rax
	movslq	%edx, %rsi
	movq	23(%rcx), %rcx
	movq	7(%rcx), %rcx
	movq	24(%rcx), %rcx
	movq	(%rcx), %r8
	movq	208(%r8), %rdi
	movq	144(%rdi), %rcx
	subq	136(%rdi), %rcx
	sarq	$5, %rcx
	cmpq	%rcx, %rsi
	jb	.L372
.L371:
	leaq	16(%r13), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 48(%r13)
	movq	%rax, 0(%r13)
	movq	$0, 8(%r13)
	movups	%xmm0, 16(%r13)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 32(%r13)
.L370:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L382
	addq	$504, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	.cfi_restore_state
	movq	232(%r8), %rcx
	leaq	-320(%rbp), %r12
	movq	.LC0(%rip), %xmm1
	movl	%edx, -540(%rbp)
	movq	%r12, %rdi
	movq	%rax, -536(%rbp)
	leaq	-432(%rbp), %r15
	leaq	-368(%rbp), %r14
	movq	8(%rcx), %rsi
	movq	(%rcx), %rcx
	movhps	.LC1(%rip), %xmm1
	movaps	%xmm1, -528(%rbp)
	movq	%rcx, -512(%rbp)
	movq	%rsi, -504(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rbx, -432(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rbx), %rsi
	movq	%rax, -432(%rbp,%rsi)
	movq	-432(%rbp), %rsi
	movq	-24(%rsi), %rdi
	xorl	%esi, %esi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-528(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -528(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-536(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -480(%rbp)
	movaps	%xmm0, -496(%rbp)
	movl	-540(%rbp), %edx
	movq	%r15, %rcx
	leaq	-512(%rbp), %rsi
	movq	(%rax), %rax
	leaq	-496(%rbp), %r8
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rdi
	call	_ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE@PLT
	movq	-384(%rbp), %rax
	leaq	-448(%rbp), %r10
	movq	$0, -456(%rbp)
	movq	%r10, -464(%rbp)
	leaq	-464(%rbp), %rdi
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L374
	movq	-400(%rbp), %r8
	movq	%r10, -536(%rbp)
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L375
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-536(%rbp), %r10
.L376:
	leaq	16(%r13), %rax
	pxor	%xmm1, %xmm1
	movq	-496(%rbp), %xmm0
	movq	-488(%rbp), %rcx
	movq	%rax, 0(%r13)
	movq	-464(%rbp), %rax
	movq	-480(%rbp), %rdx
	movaps	%xmm1, -496(%rbp)
	movq	$0, -480(%rbp)
	cmpq	%r10, %rax
	je	.L383
	movq	%rax, 0(%r13)
	movq	-448(%rbp), %rax
	movq	%rax, 16(%r13)
.L378:
	movq	%rcx, %xmm2
	movq	-456(%rbp), %rax
	movq	%rdx, 48(%r13)
	punpcklqdq	%xmm2, %xmm0
	movq	-352(%rbp), %rdi
	movups	%xmm0, 32(%r13)
	movq	.LC0(%rip), %xmm0
	movq	%rax, 8(%r13)
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movhps	.LC2(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L379
	call	_ZdlPv@PLT
.L379:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%rbx, -432(%rbp)
	movq	-24(%rbx), %rax
	movq	%r12, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L375:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-536(%rbp), %r10
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L383:
	movdqa	-448(%rbp), %xmm3
	movups	%xmm3, 16(%r13)
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L374:
	leaq	-352(%rbp), %rsi
	movq	%r10, -536(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-536(%rbp), %r10
	jmp	.L376
.L382:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22606:
	.size	_ZN2v88internal16WasmModuleObject19DisassembleFunctionEi, .-_ZN2v88internal16WasmModuleObject19DisassembleFunctionEi
	.section	.rodata._ZN2v88internal16WasmModuleObject22GetPossibleBreakpointsERKNS_5debug8LocationES5_PSt6vectorINS2_13BreakLocationESaIS7_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../deps/v8/src/wasm/wasm-objects.cc:597"
	.section	.rodata._ZN2v88internal16WasmModuleObject22GetPossibleBreakpointsERKNS_5debug8LocationES5_PSt6vectorINS2_13BreakLocationESaIS7_EE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal16WasmModuleObject22GetPossibleBreakpointsERKNS_5debug8LocationES5_PSt6vectorINS2_13BreakLocationESaIS7_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject22GetPossibleBreakpointsERKNS_5debug8LocationES5_PSt6vectorINS2_13BreakLocationESaIS7_EE
	.type	_ZN2v88internal16WasmModuleObject22GetPossibleBreakpointsERKNS_5debug8LocationES5_PSt6vectorINS2_13BreakLocationESaIS7_EE, @function
_ZN2v88internal16WasmModuleObject22GetPossibleBreakpointsERKNS_5debug8LocationES5_PSt6vectorINS2_13BreakLocationESaIS7_EE:
.LFB22619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rsi, %rdi
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	movq	%rax, -296(%rbp)
	call	_ZNK2v85debug8Location13GetLineNumberEv@PLT
	testl	%eax, %eax
	js	.L387
	movq	%r12, %rdi
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	testl	%eax, %eax
	js	.L387
	movq	%r14, %rdi
	call	_ZNK2v85debug8Location7IsEmptyEv@PLT
	testb	%al, %al
	jne	.L390
	movq	%r14, %rdi
	call	_ZNK2v85debug8Location13GetLineNumberEv@PLT
	testl	%eax, %eax
	js	.L387
	movq	%r14, %rdi
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	testl	%eax, %eax
	js	.L387
.L390:
	movq	%r12, %rdi
	call	_ZNK2v85debug8Location13GetLineNumberEv@PLT
	movl	%eax, -324(%rbp)
	movl	%eax, %ebx
	movq	-296(%rbp), %rax
	movq	136(%rax), %rdx
	movq	144(%rax), %rax
	movq	%rax, -304(%rbp)
	subq	%rdx, %rax
	sarq	$5, %rax
	cmpq	%rax, %rbx
	jb	.L433
	.p2align 4,,10
	.p2align 3
.L387:
	xorl	%eax, %eax
.L384:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L434
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	movq	%rbx, %rcx
	movq	%r12, %rdi
	salq	$5, %rcx
	movl	20(%rdx,%rcx), %eax
	movq	%rcx, -312(%rbp)
	movl	%eax, -304(%rbp)
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	cmpl	-304(%rbp), %eax
	jg	.L387
	movq	-296(%rbp), %rax
	movq	-312(%rbp), %rcx
	movq	136(%rax), %rax
	movl	16(%rax,%rcx), %edi
	movl	%edi, -304(%rbp)
	movq	%r12, %rdi
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	movq	%r14, %rdi
	movl	%eax, %r12d
	call	_ZNK2v85debug8Location7IsEmptyEv@PLT
	testb	%al, %al
	je	.L391
	movq	-296(%rbp), %rax
	movq	136(%rax), %rdx
	movq	144(%rax), %rax
	movq	%rax, -312(%rbp)
	subq	%rdx, %rax
	sarq	$5, %rax
	subl	$1, %eax
	movl	%eax, -328(%rbp)
	salq	$5, %rax
	movl	16(%rdx,%rax), %ecx
	addl	20(%rdx,%rax), %ecx
	movl	%ecx, -312(%rbp)
.L392:
	leaq	16+_ZTVN2v88internal19AccountingAllocatorE(%rip), %rax
	pxor	%xmm0, %xmm0
	leaq	-288(%rbp), %rsi
	movq	%rax, -288(%rbp)
	leaq	-208(%rbp), %rax
	leaq	.LC3(%rip), %rdx
	movq	%rax, %rdi
	movq	%rax, -376(%rbp)
	movq	%rsi, -392(%rbp)
	movups	%xmm0, -280(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	0(%r13), %rax
	movl	-324(%rbp), %edi
	addl	-304(%rbp), %r12d
	movq	23(%rax), %rax
	movl	%r12d, -320(%rbp)
	movl	%edi, %r14d
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	232(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, -384(%rbp)
	cmpl	%edi, -328(%rbp)
	jnb	.L399
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L402:
	addl	$1, %r14d
	cmpl	-328(%rbp), %r14d
	ja	.L398
	movl	%r14d, %ebx
.L399:
	movq	-296(%rbp), %rax
	salq	$5, %rbx
	addq	136(%rax), %rbx
	movl	20(%rbx), %eax
	testl	%eax, %eax
	je	.L402
	movq	-376(%rbp), %rax
	movq	$0, -240(%rbp)
	leaq	-256(%rbp), %rcx
	leaq	-144(%rbp), %rdi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movl	16(%rbx), %esi
	movq	%rax, -248(%rbp)
	movq	-384(%rbp), %rax
	movl	$0, -256(%rbp)
	movl	%esi, %edx
	addl	20(%rbx), %edx
	addq	%rax, %rdx
	addq	%rax, %rsi
	call	_ZN2v88internal4wasm16BytecodeIteratorC1EPKhS4_PNS1_14BodyLocalDeclsE@PLT
	movq	-136(%rbp), %rax
	movq	-120(%rbp), %r12
	movq	-128(%rbp), %r13
	movq	%rax, -304(%rbp)
	cmpq	%r13, %r12
	je	.L403
	movl	16(%rbx), %eax
	movq	%r13, %rdx
	subq	-304(%rbp), %rdx
	addl	%edx, %eax
	cmpl	%eax, -312(%rbp)
	jbe	.L403
	movl	%r14d, -324(%rbp)
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L435:
	movl	-324(%rbp), %esi
	movq	%r14, %rdi
	call	_ZN2v85debug8LocationC2Eii@PLT
	movl	$3, 12(%r14)
	addq	$16, 8(%r15)
.L405:
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm12OpcodeLengthEPKhS3_@PLT
	movl	%eax, %eax
	addq	%rax, %r13
	cmpq	%r13, %r12
	je	.L431
	movl	16(%rbx), %eax
	movq	%r13, %rdx
	subq	-304(%rbp), %rdx
	addl	%edx, %eax
	cmpl	%eax, -312(%rbp)
	jbe	.L431
.L404:
	cmpl	%eax, -320(%rbp)
	ja	.L405
	movq	8(%r15), %r14
	cmpq	16(%r15), %r14
	jne	.L435
	movabsq	$576460752303423487, %rcx
	movq	(%r15), %r9
	movq	%r14, %rsi
	subq	%r9, %rsi
	movq	%rsi, %rax
	sarq	$4, %rax
	cmpq	%rcx, %rax
	je	.L436
	testq	%rax, %rax
	je	.L415
	movabsq	$9223372036854775792, %rcx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L437
.L408:
	movq	%rcx, %rdi
	movq	%rdx, -368(%rbp)
	movq	%rsi, -360(%rbp)
	movq	%r9, -352(%rbp)
	movq	%rcx, -336(%rbp)
	call	_Znwm@PLT
	movq	-336(%rbp), %rcx
	movq	-352(%rbp), %r9
	movq	-368(%rbp), %rdx
	leaq	(%rax,%rcx), %rsi
	leaq	16(%rax), %rcx
	movq	%rcx, -336(%rbp)
	movq	%rsi, -344(%rbp)
	movq	-360(%rbp), %rsi
.L409:
	leaq	(%rax,%rsi), %rdi
	movl	-324(%rbp), %esi
	movq	%r9, -368(%rbp)
	movq	%rax, -360(%rbp)
	movq	%rdi, -352(%rbp)
	call	_ZN2v85debug8LocationC2Eii@PLT
	movq	-368(%rbp), %r9
	movq	-352(%rbp), %rdi
	movq	-360(%rbp), %rax
	cmpq	%r9, %r14
	movl	$3, 12(%rdi)
	je	.L410
	movq	%rax, %rsi
	movq	%r9, %rdx
	.p2align 4,,10
	.p2align 3
.L411:
	movdqu	(%rdx), %xmm1
	addq	$16, %rdx
	addq	$16, %rsi
	movups	%xmm1, -16(%rsi)
	cmpq	%rdx, %r14
	jne	.L411
	movq	%r14, %r8
	subq	%r9, %r8
	leaq	16(%rax,%r8), %rsi
	movq	%rsi, -336(%rbp)
.L410:
	testq	%r9, %r9
	je	.L412
	movq	%r9, %rdi
	movq	%rax, -352(%rbp)
	call	_ZdlPv@PLT
	movq	-352(%rbp), %rax
.L412:
	movq	%rax, %xmm0
	movq	-344(%rbp), %rax
	movhps	-336(%rbp), %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L391:
	movq	%r14, %rdi
	call	_ZNK2v85debug8Location13GetLineNumberEv@PLT
	movq	%r14, %rdi
	movl	%eax, -328(%rbp)
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	movl	-328(%rbp), %edx
	testl	%edx, %edx
	je	.L418
	testl	%eax, %eax
	je	.L393
.L418:
	movq	-296(%rbp), %rax
	movl	-328(%rbp), %edx
	movq	136(%rax), %rcx
	movq	144(%rax), %rax
	movq	%rax, -312(%rbp)
	subq	%rcx, %rax
	sarq	$5, %rax
	cmpq	%rax, %rdx
	jnb	.L387
	salq	$5, %rdx
	movq	%r14, %rdi
	movl	16(%rcx,%rdx), %eax
	movq	%rdx, -320(%rbp)
	movl	%eax, -312(%rbp)
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	addl	-312(%rbp), %eax
	movq	-320(%rbp), %rdx
	movl	%eax, -312(%rbp)
	movl	%eax, %edi
	movq	-296(%rbp), %rax
	addq	136(%rax), %rdx
	movl	20(%rdx), %eax
	addl	16(%rdx), %eax
	cmpl	%eax, %edi
	jbe	.L392
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L431:
	movl	-324(%rbp), %r14d
.L403:
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -144(%rbp)
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L402
	call	_ZdlPv@PLT
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L437:
	testq	%rdi, %rdi
	jne	.L438
	movq	$16, -336(%rbp)
	xorl	%eax, %eax
	movq	$0, -344(%rbp)
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L398:
	movq	-376(%rbp), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-392(%rbp), %rdi
	call	_ZN2v88internal19AccountingAllocatorD1Ev@PLT
	movl	$1, %eax
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L415:
	movl	$16, %ecx
	jmp	.L408
.L393:
	subl	$1, -328(%rbp)
	movq	-296(%rbp), %rdi
	movl	-328(%rbp), %eax
	salq	$5, %rax
	addq	136(%rdi), %rax
	movl	20(%rax), %edx
	addl	16(%rax), %edx
	movl	%edx, -312(%rbp)
	jmp	.L392
.L434:
	call	__stack_chk_fail@PLT
.L436:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L438:
	movabsq	$576460752303423487, %rax
	cmpq	%rax, %rdi
	cmovbe	%rdi, %rax
	salq	$4, %rax
	movq	%rax, %rcx
	jmp	.L408
	.cfi_endproc
.LFE22619:
	.size	_ZN2v88internal16WasmModuleObject22GetPossibleBreakpointsERKNS_5debug8LocationES5_PSt6vectorINS2_13BreakLocationESaIS7_EE, .-_ZN2v88internal16WasmModuleObject22GetPossibleBreakpointsERKNS_5debug8LocationES5_PSt6vectorINS2_13BreakLocationESaIS7_EE
	.section	.text._ZN2v88internal16WasmModuleObject16CheckBreakPointsEPNS0_7IsolateENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject16CheckBreakPointsEPNS0_7IsolateENS0_6HandleIS1_EEi
	.type	_ZN2v88internal16WasmModuleObject16CheckBreakPointsEPNS0_7IsolateENS0_6HandleIS1_EEi, @function
_ZN2v88internal16WasmModuleObject16CheckBreakPointsEPNS0_7IsolateENS0_6HandleIS1_EEi:
.LFB22626:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	63(%rax), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37504(%rax), %rsi
	je	.L469
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L442
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L443:
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_127FindBreakpointInfoInsertPosEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEi
	movq	0(%r13), %rdx
	cmpl	%eax, 11(%rdx)
	jg	.L470
.L445:
	xorl	%eax, %eax
.L441:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L471
.L444:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L470:
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L446
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L447:
	cmpq	%rsi, 88(%r12)
	je	.L445
	cmpl	%ebx, 11(%rsi)
	jne	.L445
	movq	41112(%r12), %rdi
	movq	15(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L449
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L450:
	testb	$1, %sil
	jne	.L452
.L455:
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	0(%r13), %r12
	movq	(%rax), %r14
	movq	%rax, %rbx
	movq	%r12, 15(%r14)
	leaq	15(%r14), %r15
	testb	$1, %r12b
	je	.L458
	movq	%r12, %r13
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testl	$262144, %eax
	je	.L456
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r13), %rax
.L456:
	testb	$24, %al
	je	.L458
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L458
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L458:
	movq	%rbx, %rax
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L446:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L472
.L448:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L471:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L449:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L473
.L451:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L472:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L452:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L455
	movq	%r13, %rax
	jmp	.L441
.L473:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L451
	.cfi_endproc
.LFE22626:
	.size	_ZN2v88internal16WasmModuleObject16CheckBreakPointsEPNS0_7IsolateENS0_6HandleIS1_EEi, .-_ZN2v88internal16WasmModuleObject16CheckBreakPointsEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.text._ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6HandleIS1_EENS0_4wasm12WireBytesRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6HandleIS1_EENS0_4wasm12WireBytesRefE
	.type	_ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6HandleIS1_EENS0_4wasm12WireBytesRefE, @function
_ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6HandleIS1_EENS0_4wasm12WireBytesRefE:
.LFB22630:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	movl	%edx, %edx
	leaq	-32(%rbp), %rsi
	movq	23(%rcx), %rcx
	sarq	$32, %rax
	movq	7(%rcx), %rcx
	movq	24(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	232(%rcx), %rcx
	addq	(%rcx), %rdx
	movq	%rax, -24(%rbp)
	movq	%rdx, -32(%rbp)
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L477
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L477:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22630:
	.size	_ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6HandleIS1_EENS0_4wasm12WireBytesRefE, .-_ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6HandleIS1_EENS0_4wasm12WireBytesRefE
	.section	.text._ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6VectorIKhEENS0_4wasm12WireBytesRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6VectorIKhEENS0_4wasm12WireBytesRefE
	.type	_ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6VectorIKhEENS0_4wasm12WireBytesRefE, @function
_ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6VectorIKhEENS0_4wasm12WireBytesRefE:
.LFB22631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rcx, %rax
	movl	%ecx, %ecx
	addq	%rsi, %rcx
	sarq	$32, %rax
	leaq	-32(%rbp), %rsi
	movq	%rcx, -32(%rbp)
	movq	%rax, -24(%rbp)
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L481
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L481:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22631:
	.size	_ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6VectorIKhEENS0_4wasm12WireBytesRefE, .-_ZN2v88internal16WasmModuleObject32ExtractUtf8StringFromModuleBytesEPNS0_7IsolateENS0_6VectorIKhEENS0_4wasm12WireBytesRefE
	.section	.text._ZN2v88internal16WasmModuleObject19GetModuleNameOrNullEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject19GetModuleNameOrNullEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal16WasmModuleObject19GetModuleNameOrNullEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal16WasmModuleObject19GetModuleNameOrNullEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB22632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdx
	movq	208(%rdx), %rcx
	movl	80(%rcx), %eax
	testl	%eax, %eax
	jne	.L483
	xorl	%eax, %eax
.L484:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L487
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore_state
	movq	232(%rdx), %rdx
	movslq	84(%rcx), %rcx
	leaq	-32(%rbp), %rsi
	addq	(%rdx), %rax
	xorl	%edx, %edx
	movq	%rcx, -24(%rbp)
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	jmp	.L484
.L487:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22632:
	.size	_ZN2v88internal16WasmModuleObject19GetModuleNameOrNullEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal16WasmModuleObject19GetModuleNameOrNullEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal16WasmModuleObject21GetFunctionNameOrNullEPNS0_7IsolateENS0_6HandleIS1_EEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject21GetFunctionNameOrNullEPNS0_7IsolateENS0_6HandleIS1_EEj
	.type	_ZN2v88internal16WasmModuleObject21GetFunctionNameOrNullEPNS0_7IsolateENS0_6HandleIS1_EEj, @function
_ZN2v88internal16WasmModuleObject21GetFunctionNameOrNullEPNS0_7IsolateENS0_6HandleIS1_EEj:
.LFB22636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	-64(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%r13, %rsi
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rdi
	movq	232(%rax), %rax
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal4wasm10WasmModule18LookupFunctionNameERKNS1_15ModuleWireBytesEj@PLT
	testl	%eax, %eax
	jne	.L489
	xorl	%eax, %eax
.L490:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L493
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L489:
	.cfi_restore_state
	movq	(%rbx), %rcx
	movq	%rax, %rdx
	movq	%r13, %rsi
	movl	%eax, %eax
	sarq	$32, %rdx
	movq	%r12, %rdi
	movq	23(%rcx), %rcx
	movq	7(%rcx), %rcx
	movq	24(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	232(%rcx), %rcx
	addq	(%rcx), %rax
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	jmp	.L490
.L493:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22636:
	.size	_ZN2v88internal16WasmModuleObject21GetFunctionNameOrNullEPNS0_7IsolateENS0_6HandleIS1_EEj, .-_ZN2v88internal16WasmModuleObject21GetFunctionNameOrNullEPNS0_7IsolateENS0_6HandleIS1_EEj
	.section	.rodata._ZN2v88internal16WasmModuleObject15GetFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EEj.str1.1,"aMS",@progbits,1
.LC5:
	.string	"wasm-function[%u]"
.LC6:
	.string	"(location_) != nullptr"
.LC7:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal16WasmModuleObject15GetFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject15GetFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EEj
	.type	_ZN2v88internal16WasmModuleObject15GetFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EEj, @function
_ZN2v88internal16WasmModuleObject15GetFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EEj:
.LFB22637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	leaq	-112(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%r14, %rsi
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rdi
	movq	232(%rax), %rax
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -104(%rbp)
	movl	%r12d, %edx
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal4wasm10WasmModule18LookupFunctionNameERKNS1_15ModuleWireBytesEj@PLT
	testl	%eax, %eax
	jne	.L503
.L496:
	leaq	-80(%rbp), %rdi
	leaq	.LC5(%rip), %rdx
	xorl	%eax, %eax
	movl	%r12d, %ecx
	movl	$32, %esi
	movq	%rdi, -96(%rbp)
	movq	$32, -88(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-96(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	cltq
	movq	%rdx, -112(%rbp)
	xorl	%edx, %edx
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L504
	.p2align 4,,10
	.p2align 3
.L499:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L505
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	.cfi_restore_state
	movq	(%rbx), %rcx
	movq	%rax, %rdx
	movq	%r14, %rsi
	movl	%eax, %eax
	sarq	$32, %rdx
	movq	%r13, %rdi
	movq	23(%rcx), %rcx
	movq	7(%rcx), %rcx
	movq	24(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	232(%rcx), %rcx
	addq	(%rcx), %rax
	movq	%rdx, -104(%rbp)
	xorl	%edx, %edx
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	jne	.L499
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L504:
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L505:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22637:
	.size	_ZN2v88internal16WasmModuleObject15GetFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EEj, .-_ZN2v88internal16WasmModuleObject15GetFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EEj
	.section	.text._ZN2v88internal16WasmModuleObject18GetRawFunctionNameEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject18GetRawFunctionNameEj
	.type	_ZN2v88internal16WasmModuleObject18GetRawFunctionNameEj, @function
_ZN2v88internal16WasmModuleObject18GetRawFunctionNameEj:
.LFB22638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	23(%rax), %rcx
	movq	7(%rcx), %rcx
	movq	24(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	232(%rcx), %rcx
	movq	8(%rcx), %rsi
	movq	(%rcx), %rcx
	movq	%rsi, -40(%rbp)
	movq	%r12, %rsi
	movq	%rcx, -48(%rbp)
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rdi
	call	_ZNK2v88internal4wasm10WasmModule18LookupFunctionNameERKNS1_15ModuleWireBytesEj@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZNK2v88internal4wasm15ModuleWireBytes13GetNameOrNullENS1_12WireBytesRefE@PLT
	movq	-24(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L509
	addq	$40, %rsp
	movslq	%edx, %rdx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L509:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22638:
	.size	_ZN2v88internal16WasmModuleObject18GetRawFunctionNameEj, .-_ZN2v88internal16WasmModuleObject18GetRawFunctionNameEj
	.section	.text._ZN2v88internal16WasmModuleObject17GetFunctionOffsetEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject17GetFunctionOffsetEj
	.type	_ZN2v88internal16WasmModuleObject17GetFunctionOffsetEj, @function
_ZN2v88internal16WasmModuleObject17GetFunctionOffsetEj:
.LFB22639:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	%esi, %esi
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	movq	136(%rax), %rdx
	movq	144(%rax), %rax
	subq	%rdx, %rax
	sarq	$5, %rax
	cmpq	%rax, %rsi
	jnb	.L512
	salq	$5, %rsi
	movl	16(%rdx,%rsi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L512:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE22639:
	.size	_ZN2v88internal16WasmModuleObject17GetFunctionOffsetEj, .-_ZN2v88internal16WasmModuleObject17GetFunctionOffsetEj
	.section	.text._ZN2v88internal16WasmModuleObject21GetContainingFunctionEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject21GetContainingFunctionEj
	.type	_ZN2v88internal16WasmModuleObject21GetContainingFunctionEj, @function
_ZN2v88internal16WasmModuleObject21GetContainingFunctionEj:
.LFB22640:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	movq	136(%rax), %rdi
	movq	144(%rax), %rax
	subq	%rdi, %rax
	sarq	$5, %rax
	movl	%eax, %r8d
	testl	%eax, %eax
	je	.L513
	xorl	%ecx, %ecx
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L521:
	sarl	%eax
	addl	%ecx, %eax
	movslq	%eax, %rdx
	salq	$5, %rdx
	cmpl	16(%rdi,%rdx), %esi
	cmovb	%eax, %r8d
	cmovnb	%eax, %ecx
.L516:
	movl	%r8d, %eax
	subl	%ecx, %eax
	cmpl	$1, %eax
	jg	.L521
	movslq	%ecx, %rax
	salq	$5, %rax
	addq	%rax, %rdi
	movl	16(%rdi), %eax
	cmpl	%eax, %esi
	jb	.L519
	addl	20(%rdi), %eax
	movl	$-1, %r8d
	cmpl	%eax, %esi
	cmovb	%ecx, %r8d
.L513:
	movl	%r8d, %eax
	ret
.L519:
	movl	$-1, %r8d
	jmp	.L513
	.cfi_endproc
.LFE22640:
	.size	_ZN2v88internal16WasmModuleObject21GetContainingFunctionEj, .-_ZN2v88internal16WasmModuleObject21GetContainingFunctionEj
	.section	.text._ZN2v88internal16WasmModuleObject15GetPositionInfoEjPNS0_6Script12PositionInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject15GetPositionInfoEjPNS0_6Script12PositionInfoE
	.type	_ZN2v88internal16WasmModuleObject15GetPositionInfoEjPNS0_6Script12PositionInfoE, @function
_ZN2v88internal16WasmModuleObject15GetPositionInfoEjPNS0_6Script12PositionInfoE:
.LFB22641:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	39(%rax), %rcx
	movq	111(%rcx), %rcx
	testb	$1, %cl
	jne	.L534
.L523:
	movq	23(%rax), %rax
	xorl	%r8d, %r8d
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rcx
	movq	136(%rcx), %rax
	movq	144(%rcx), %rcx
	subq	%rax, %rcx
	sarq	$5, %rcx
	movl	%ecx, %r9d
	testl	%ecx, %ecx
	jne	.L529
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L535:
	sarl	%ecx
	addl	%r8d, %ecx
	movslq	%ecx, %rdi
	salq	$5, %rdi
	cmpl	16(%rax,%rdi), %esi
	cmovb	%ecx, %r9d
	cmovnb	%ecx, %r8d
.L529:
	movl	%r9d, %ecx
	subl	%r8d, %ecx
	cmpl	$1, %ecx
	jg	.L535
	movslq	%r8d, %rcx
	salq	$5, %rcx
	addq	%rcx, %rax
	movl	16(%rax), %ecx
	cmpl	%ecx, %esi
	jb	.L531
	addl	20(%rax), %ecx
	cmpl	%ecx, %esi
	jnb	.L531
.L527:
	movl	%r8d, (%rdx)
	subl	16(%rax), %esi
	movl	%esi, 4(%rdx)
	movl	16(%rax), %ecx
	movl	%ecx, 8(%rdx)
	movl	20(%rax), %ecx
	addl	16(%rax), %ecx
	movl	$1, %eax
	movl	%ecx, 12(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L534:
	movq	-1(%rcx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L523
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	movq	144(%rax), %rcx
	cmpq	%rcx, 136(%rax)
	je	.L531
	movl	$0, (%rdx)
	movl	%esi, 4(%rdx)
	movq	(%rdi), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	movq	136(%rax), %rax
	movl	16(%rax), %eax
	movl	%eax, 8(%rdx)
	movq	(%rdi), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	movq	144(%rax), %rcx
	movl	-12(%rcx), %eax
	addl	-16(%rcx), %eax
	movl	%eax, 12(%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L531:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22641:
	.size	_ZN2v88internal16WasmModuleObject15GetPositionInfoEjPNS0_6Script12PositionInfoE, .-_ZN2v88internal16WasmModuleObject15GetPositionInfoEjPNS0_6Script12PositionInfoE
	.section	.text._ZN2v88internal15WasmTableObject3NewEPNS0_7IsolateENS0_4wasm9ValueTypeEjbjPNS0_6HandleINS0_10FixedArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15WasmTableObject3NewEPNS0_7IsolateENS0_4wasm9ValueTypeEjbjPNS0_6HandleINS0_10FixedArrayEEE
	.type	_ZN2v88internal15WasmTableObject3NewEPNS0_7IsolateENS0_4wasm9ValueTypeEjbjPNS0_6HandleINS0_10FixedArrayEEE, @function
_ZN2v88internal15WasmTableObject3NewEPNS0_7IsolateENS0_4wasm9ValueTypeEjbjPNS0_6HandleINS0_10FixedArrayEEE:
.LFB22642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	xorl	%edx, %edx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movb	%sil, -72(%rbp)
	movl	%r12d, %esi
	movl	%ecx, -76(%rbp)
	movl	%r8d, -80(%rbp)
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	104(%rbx), %r15
	movq	%rax, %r13
	testl	%r12d, %r12d
	jle	.L543
	leal	-1(%r12), %eax
	testb	$1, %r15b
	je	.L540
	leaq	24(,%rax,8), %rax
	movq	%r15, %r14
	movq	%rbx, -96(%rbp)
	movl	$16, %r12d
	movq	%rax, -56(%rbp)
	andq	$-262144, %r14
	movq	%r15, %rbx
	movq	%r13, %r15
	movq	%r14, %rcx
	.p2align 4,,10
	.p2align 3
.L544:
	movq	(%r15), %r14
	leaq	-1(%r12,%r14), %r13
	movq	%rbx, 0(%r13)
	movq	8(%rcx), %rax
	testl	$262144, %eax
	je	.L546
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rcx
	movq	8(%rcx), %rax
.L546:
	testb	$24, %al
	je	.L545
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L545
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rcx
.L545:
	addq	$8, %r12
	cmpq	-56(%rbp), %r12
	jne	.L544
	movq	-96(%rbp), %rbx
	movq	%r15, %r13
.L543:
	cmpb	$0, -76(%rbp)
	leaq	88(%rbx), %r12
	jne	.L591
.L548:
	movq	12464(%rbx), %rax
	movq	39(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L549
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L550:
	movq	1511(%rsi), %r14
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L552
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L553:
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	%rax, %r14
	movzbl	-72(%rbp), %eax
	salq	$32, %rax
	movq	%rax, 47(%rdx)
	movq	(%r14), %rdi
	movq	0(%r13), %r15
	movq	%r15, 23(%rdi)
	leaq	23(%rdi), %rsi
	testb	$1, %r15b
	je	.L567
	movq	%r15, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -56(%rbp)
	testl	$262144, %eax
	jne	.L592
	testb	$24, %al
	je	.L567
.L598:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L593
	.p2align 4,,10
	.p2align 3
.L567:
	movq	(%r14), %r15
	movq	(%r12), %r12
	movq	%r12, 31(%r15)
	leaq	31(%r15), %rsi
	testb	$1, %r12b
	je	.L566
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L594
	testb	$24, %al
	je	.L566
.L600:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L595
	.p2align 4,,10
	.p2align 3
.L566:
	movq	(%r14), %r15
	movq	288(%rbx), %r12
	movq	%r12, 39(%r15)
	leaq	39(%r15), %rsi
	testb	$1, %r12b
	je	.L565
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L596
	testb	$24, %al
	je	.L565
.L599:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L597
	.p2align 4,,10
	.p2align 3
.L565:
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L564
	movq	%r13, (%rax)
.L564:
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L592:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r8
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%r8), %rax
	testb	$24, %al
	jne	.L598
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L596:
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L599
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L594:
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L600
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L552:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L601
.L554:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r14, (%rsi)
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L549:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L602
.L551:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L591:
	movl	-80(%rbp), %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L540:
	leaq	24(,%rax,8), %rdi
	movl	$16, %eax
	.p2align 4,,10
	.p2align 3
.L547:
	movq	0(%r13), %rdx
	addq	$8, %rax
	movq	%r15, -9(%rax,%rdx)
	cmpq	%rax, %rdi
	jne	.L547
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L593:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L595:
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L597:
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L565
.L602:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L551
.L601:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L554
	.cfi_endproc
.LFE22642:
	.size	_ZN2v88internal15WasmTableObject3NewEPNS0_7IsolateENS0_4wasm9ValueTypeEjbjPNS0_6HandleINS0_10FixedArrayEEE, .-_ZN2v88internal15WasmTableObject3NewEPNS0_7IsolateENS0_4wasm9ValueTypeEjbjPNS0_6HandleINS0_10FixedArrayEEE
	.section	.text._ZN2v88internal15WasmTableObject16AddDispatchTableEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18WasmInstanceObjectEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15WasmTableObject16AddDispatchTableEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18WasmInstanceObjectEEEi
	.type	_ZN2v88internal15WasmTableObject16AddDispatchTableEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18WasmInstanceObjectEEEi, @function
_ZN2v88internal15WasmTableObject16AddDispatchTableEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18WasmInstanceObjectEEEi:
.LFB22643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$40, %rsp
	movq	(%rsi), %rax
	movq	41112(%rdi), %rdi
	movq	39(%rax), %r15
	testq	%rdi, %rdi
	je	.L604
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r15
	movq	%rax, %rsi
.L605:
	testq	%r14, %r14
	je	.L603
	movslq	11(%r15), %r8
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	$3, %edx
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal7Factory21CopyFixedArrayAndGrowENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE@PLT
	movq	-56(%rbp), %r8
	movq	(%r14), %r14
	movq	(%rax), %rdi
	movq	%rax, %r15
	leal	16(,%r8,8), %r12d
	movslq	%r12d, %rax
	leaq	-1(%rdi,%rax), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L615
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L609
	movq	%r14, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
.L609:
	testb	$24, %al
	je	.L615
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L632
	.p2align 4,,10
	.p2align 3
.L615:
	movq	(%r15), %rax
	addl	$8, %r12d
	salq	$32, %rbx
	movslq	%r12d, %r12
	movq	%rbx, -1(%r12,%rax)
	movq	0(%r13), %r13
	movq	(%r15), %r12
	movq	%r12, 39(%r13)
	leaq	39(%r13), %r14
	testb	$1, %r12b
	je	.L603
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L612
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L612:
	testb	$24, %al
	je	.L603
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L633
.L603:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L604:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L634
.L606:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L632:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L633:
	addq	$40, %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L634:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L606
	.cfi_endproc
.LFE22643:
	.size	_ZN2v88internal15WasmTableObject16AddDispatchTableEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18WasmInstanceObjectEEEi, .-_ZN2v88internal15WasmTableObject16AddDispatchTableEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18WasmInstanceObjectEEEi
	.section	.text._ZN2v88internal15WasmTableObject10IsInBoundsEPNS0_7IsolateENS0_6HandleIS1_EEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15WasmTableObject10IsInBoundsEPNS0_7IsolateENS0_6HandleIS1_EEj
	.type	_ZN2v88internal15WasmTableObject10IsInBoundsEPNS0_7IsolateENS0_6HandleIS1_EEj, @function
_ZN2v88internal15WasmTableObject10IsInBoundsEPNS0_7IsolateENS0_6HandleIS1_EEj:
.LFB22645:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$2147483646, %edx
	ja	.L635
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	cmpl	%edx, 11(%rax)
	setg	%al
.L635:
	ret
	.cfi_endproc
.LFE22645:
	.size	_ZN2v88internal15WasmTableObject10IsInBoundsEPNS0_7IsolateENS0_6HandleIS1_EEj, .-_ZN2v88internal15WasmTableObject10IsInBoundsEPNS0_7IsolateENS0_6HandleIS1_EEj
	.section	.text._ZN2v88internal15WasmTableObject27SetFunctionTablePlaceholderEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_18WasmInstanceObjectEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15WasmTableObject27SetFunctionTablePlaceholderEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_18WasmInstanceObjectEEEi
	.type	_ZN2v88internal15WasmTableObject27SetFunctionTablePlaceholderEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_18WasmInstanceObjectEEEi, @function
_ZN2v88internal15WasmTableObject27SetFunctionTablePlaceholderEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_18WasmInstanceObjectEEEi:
.LFB22656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	salq	$32, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$16, %rsp
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L639
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L640:
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory9NewTuple2ENS0_6HandleINS0_6ObjectEEES4_NS0_14AllocationTypeE@PLT
	movq	0(%r13), %rdx
	movq	(%rax), %r12
	leal	16(,%rbx,8), %eax
	movq	23(%rdx), %r13
	cltq
	leaq	-1(%r13,%rax), %r14
	movq	%r12, (%r14)
	testb	$1, %r12b
	je	.L638
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L653
	testb	$24, %al
	je	.L638
.L655:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L654
.L638:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L653:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L655
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L639:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L656
.L641:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rdx)
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L654:
	addq	$16, %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L656:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%r8, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L641
	.cfi_endproc
.LFE22656:
	.size	_ZN2v88internal15WasmTableObject27SetFunctionTablePlaceholderEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_18WasmInstanceObjectEEEi, .-_ZN2v88internal15WasmTableObject27SetFunctionTablePlaceholderEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_18WasmInstanceObjectEEEi
	.section	.text._ZN2v88internal25WasmIndirectFunctionTable3NewEPNS0_7IsolateEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25WasmIndirectFunctionTable3NewEPNS0_7IsolateEj
	.type	_ZN2v88internal25WasmIndirectFunctionTable3NewEPNS0_7IsolateEj, @function
_ZN2v88internal25WasmIndirectFunctionTable3NewEPNS0_7IsolateEj:
.LFB22669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -52(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	xorl	%edx, %edx
	movl	$108, %esi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movl	%r15d, 7(%rax)
	movq	(%r12), %r15
	movq	(%rbx), %r14
	movq	%r14, 39(%r15)
	testb	$1, %r14b
	je	.L685
	movq	%r14, %rbx
	leaq	39(%r15), %rsi
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L710
	testb	$24, %al
	je	.L685
.L717:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L711
	.p2align 4,,10
	.p2align 3
.L685:
	movl	$64, %edi
	movl	-52(%rbp), %r14d
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r15
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	leaq	(%r14,%r14,2), %rbx
	movq	%rax, (%r15)
	leaq	0(,%r14,8), %r8
	salq	$2, %rbx
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r15)
	leaq	0(,%r14,4), %rdx
	movq	$0, 32(%r15)
	movq	%r8, -64(%rbp)
	movups	%xmm0, 16(%r15)
	testq	%r14, %r14
	je	.L661
	movq	%rdx, %rdi
	movq	%rdx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, 16(%r15)
	movq	%rax, %rdi
	leaq	(%rax,%rdx), %r14
	movq	%r14, 32(%r15)
	call	memset@PLT
	movq	-64(%rbp), %r8
	movq	%r14, 24(%r15)
	pxor	%xmm0, %xmm0
	movq	$0, 56(%r15)
	movq	%r8, %rdi
	movups	%xmm0, 40(%r15)
	call	_Znwm@PLT
	movq	-64(%rbp), %r8
	xorl	%esi, %esi
	movq	%rax, 40(%r15)
	movq	%rax, %rdi
	leaq	(%rax,%r8), %r14
	movq	%r8, %rdx
	movq	%r14, 56(%r15)
	call	memset@PLT
	movq	16(%r15), %rax
.L686:
	movq	%r14, 48(%r15)
	movq	(%r12), %rdx
	movq	%rax, 15(%rdx)
	movq	(%r12), %rax
	movq	40(%r15), %rdx
	movq	%rdx, 23(%rax)
	movq	32(%r13), %r14
	addq	%rbx, %r14
	movq	%r14, %rax
	subq	48(%r13), %rax
	movq	%r14, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L712
.L662:
	testq	%rbx, %rbx
	je	.L663
	cmpq	40(%r13), %r14
	jg	.L713
.L663:
	movl	$16, %edi
	call	_Znwm@PLT
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rax, %rdx
	leaq	16(%r15), %rax
	movq	%rax, (%rdx)
	leaq	8(%r15), %rax
	movq	%r15, 8(%rdx)
	movq	%rax, -64(%rbp)
	je	.L714
	leaq	8(%r15), %rax
	lock addl	$1, (%rax)
.L664:
	movl	$48, %edi
	movq	%rdx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	%rax, %r14
	movups	%xmm0, 8(%rax)
	movq	%rbx, (%rax)
	movq	%r14, %rsi
	movq	%rdx, 24(%rax)
	leaq	_ZN2v88internal7ManagedINS0_12_GLOBAL__N_120IftNativeAllocationsEE10DestructorEPv(%rip), %rax
	xorl	%edx, %edx
	movq	%rax, 32(%r14)
	movq	$0, 40(%r14)
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	41152(%r13), %rdi
	movq	(%rax), %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, 40(%r14)
	movq	%rax, %rdi
	call	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE@PLT
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L665
	leaq	8(%r15), %rsi
	movl	$-1, %eax
	lock xaddl	%eax, (%rsi)
	cmpl	$1, %eax
	je	.L715
.L668:
	movq	(%r12), %r14
	movq	(%rbx), %r13
	movq	%r13, 31(%r14)
	leaq	31(%r14), %r15
	testb	$1, %r13b
	je	.L681
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L673
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L673:
	testb	$24, %al
	je	.L681
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L716
	.p2align 4,,10
	.p2align 3
.L681:
	movl	-52(%rbp), %eax
	xorl	%ebx, %ebx
	testl	%eax, %eax
	je	.L694
	.p2align 4,,10
	.p2align 3
.L675:
	movq	(%r12), %rdx
	movslq	%ebx, %rax
	movq	15(%rdx), %rdx
	movl	$-1, (%rdx,%rax,4)
	movq	(%r12), %rdx
	movq	23(%rdx), %rdx
	movq	$0, (%rdx,%rax,8)
	movq	(%r12), %rax
	movq	39(%rax), %r14
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %r13
	leal	16(,%rbx,8), %eax
	cltq
	leaq	-1(%r14,%rax), %r15
	movq	%r13, (%r15)
	testb	$1, %r13b
	je	.L680
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -64(%rbp)
	testl	$262144, %eax
	je	.L677
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rcx
	movq	8(%rcx), %rax
.L677:
	testb	$24, %al
	je	.L680
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L680
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L680:
	addl	$1, %ebx
	cmpl	-52(%rbp), %ebx
	jne	.L675
.L694:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L714:
	.cfi_restore_state
	addl	$1, 8(%r15)
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L710:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-64(%rbp), %rsi
	testb	$24, %al
	jne	.L717
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L712:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L665:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	cmpl	$1, %eax
	jne	.L668
.L715:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L669
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L670:
	cmpl	$1, %eax
	jne	.L668
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L716:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L711:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L713:
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L661:
	movq	$0, 40(%r15)
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	movq	$0, 56(%r15)
	jmp	.L686
.L669:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L670
	.cfi_endproc
.LFE22669:
	.size	_ZN2v88internal25WasmIndirectFunctionTable3NewEPNS0_7IsolateEj, .-_ZN2v88internal25WasmIndirectFunctionTable3NewEPNS0_7IsolateEj
	.section	.text._ZN2v88internal16WasmMemoryObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmMemoryObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEEj
	.type	_ZN2v88internal16WasmMemoryObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEEj, @function
_ZN2v88internal16WasmMemoryObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEEj:
.LFB22677:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	testq	%rsi, %rsi
	je	.L738
.L719:
	movq	12464(%r12), %rax
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L720
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L721:
	movq	1495(%rsi), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L723
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L724:
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	0(%r13), %r13
	movq	(%rax), %r14
	movq	%rax, %r12
	movq	%r13, 23(%r14)
	leaq	23(%r14), %rsi
	testb	$1, %r13b
	je	.L729
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L739
	testb	$24, %al
	je	.L729
.L741:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L740
.L729:
	movq	(%r12), %rax
	salq	$32, %rbx
	movq	%rbx, 31(%rax)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L739:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L741
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L723:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L742
.L725:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L720:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L743
.L722:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L740:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L738:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal4wasm16SetupArrayBufferEPNS0_7IsolateEPvmbNS0_10SharedFlagE@PLT
	movq	%rax, %r13
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L743:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L742:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L725
	.cfi_endproc
.LFE22677:
	.size	_ZN2v88internal16WasmMemoryObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEEj, .-_ZN2v88internal16WasmMemoryObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEEj
	.section	.text._ZN2v88internal16WasmMemoryObject3NewEPNS0_7IsolateEjjb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmMemoryObject3NewEPNS0_7IsolateEjjb
	.type	_ZN2v88internal16WasmMemoryObject3NewEPNS0_7IsolateEjjb, @function
_ZN2v88internal16WasmMemoryObject3NewEPNS0_7IsolateEjjb:
.LFB22678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %esi
	salq	$16, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edx, %r12d
	testb	%cl, %cl
	je	.L745
	movl	%edx, %edx
	salq	$16, %rdx
	call	_ZN2v88internal4wasm20NewSharedArrayBufferEPNS0_7IsolateEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L751
.L748:
	movl	%r12d, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal16WasmMemoryObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEEj
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L745:
	.cfi_restore_state
	call	_ZN2v88internal4wasm14NewArrayBufferEPNS0_7IsolateEm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L748
.L751:
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22678:
	.size	_ZN2v88internal16WasmMemoryObject3NewEPNS0_7IsolateEjjb, .-_ZN2v88internal16WasmMemoryObject3NewEPNS0_7IsolateEjjb
	.section	.rodata._ZN2v88internal16WasmMemoryObject11AddInstanceEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18WasmInstanceObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"mem_size <= wasm::max_mem_bytes()"
	.section	.text._ZN2v88internal16WasmMemoryObject11AddInstanceEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18WasmInstanceObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmMemoryObject11AddInstanceEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18WasmInstanceObjectEEE
	.type	_ZN2v88internal16WasmMemoryObject11AddInstanceEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18WasmInstanceObjectEEE, @function
_ZN2v88internal16WasmMemoryObject11AddInstanceEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18WasmInstanceObjectEEE:
.LFB22682:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	39(%rax), %r14
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37504(%rax), %r14
	jne	.L781
	movq	1080(%rbx), %r14
.L781:
	testq	%rdi, %rdi
	je	.L758
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L757:
	leaq	-80(%rbp), %rdx
	movq	%rbx, %rdi
	movl	$0, -80(%rbp)
	movq	%r12, -72(%rbp)
	call	_ZN2v88internal13WeakArrayList8AddToEndEPNS0_7IsolateENS0_6HandleIS1_EERKNS0_17MaybeObjectHandleE@PLT
	movq	0(%r13), %r15
	movq	(%rax), %r14
	leaq	39(%r15), %rsi
	movq	%r14, 39(%r15)
	testb	$1, %r14b
	je	.L768
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L782
	testb	$24, %al
	je	.L768
.L786:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L783
	.p2align 4,,10
	.p2align 3
.L768:
	movq	0(%r13), %rax
	movq	41112(%rbx), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L764
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L765:
	movq	(%r12), %rbx
	movq	23(%rsi), %r13
	movq	31(%rsi), %r12
	call	_ZN2v88internal4wasm13max_mem_pagesEv@PLT
	movl	%eax, %eax
	salq	$16, %rax
	cmpq	%rax, %r13
	ja	.L784
	movq	%r13, %rdi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	movq	%r12, 23(%rbx)
	subq	$1, %rax
	movq	%r13, 31(%rbx)
	movq	%rax, 39(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L785
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L782:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L786
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L758:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L787
.L760:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r14, (%rsi)
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L764:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L788
.L766:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L783:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L784:
	leaq	.LC8(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L788:
	movq	%rbx, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L787:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L760
.L785:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22682:
	.size	_ZN2v88internal16WasmMemoryObject11AddInstanceEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18WasmInstanceObjectEEE, .-_ZN2v88internal16WasmMemoryObject11AddInstanceEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_18WasmInstanceObjectEEE
	.section	.text._ZN2v88internal16WasmMemoryObject16update_instancesEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmMemoryObject16update_instancesEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEE
	.type	_ZN2v88internal16WasmMemoryObject16update_instancesEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEE, @function
_ZN2v88internal16WasmMemoryObject16update_instancesEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEE:
.LFB22683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	%rdi, -56(%rbp)
	movq	%rax, %rdx
	movq	39(%rax), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L790
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L791
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L792:
	movl	19(%rsi), %eax
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jg	.L794
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L795:
	movq	(%r15), %rsi
	addq	$1, %rbx
	cmpl	%ebx, 19(%rsi)
	jle	.L814
.L794:
	movq	23(%rsi,%rbx,8), %rsi
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	jne	.L795
	cmpl	$3, %esi
	je	.L795
	movq	41112(%r12), %rdi
	andq	$-3, %rsi
	testq	%rdi, %rdi
	je	.L796
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L797:
	movq	(%r14), %rax
	movq	%rsi, -64(%rbp)
	movq	23(%rax), %r13
	movq	31(%rax), %rax
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal4wasm13max_mem_pagesEv@PLT
	movq	-64(%rbp), %rsi
	movl	%eax, %eax
	salq	$16, %rax
	cmpq	%rax, %r13
	ja	.L815
	movq	%r13, %rdi
	movq	%rsi, -64(%rbp)
	addq	$1, %rbx
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %rcx
	subq	$1, %rax
	movq	%rcx, 23(%rsi)
	movq	%r13, 31(%rsi)
	movq	%rax, 39(%rsi)
	movq	(%r15), %rsi
	cmpl	%ebx, 19(%rsi)
	jg	.L794
	.p2align 4,,10
	.p2align 3
.L814:
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
.L790:
	movq	(%r14), %r12
	movq	%r12, 23(%rax)
	testb	$1, %r12b
	je	.L789
	movq	-56(%rbp), %r14
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	(%r14), %rdi
	movq	8(%rbx), %rax
	leaq	23(%rdi), %rsi
	testl	$262144, %eax
	jne	.L816
	testb	$24, %al
	je	.L789
.L819:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L817
.L789:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L791:
	.cfi_restore_state
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L818
.L793:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L816:
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%r14), %rdi
	movq	8(%rbx), %rax
	leaq	23(%rdi), %rsi
	testb	$24, %al
	jne	.L819
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L796:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L820
.L798:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L817:
	addq	$40, %rsp
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L815:
	.cfi_restore_state
	leaq	.LC8(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L820:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	jmp	.L798
.L818:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L793
	.cfi_endproc
.LFE22683:
	.size	_ZN2v88internal16WasmMemoryObject16update_instancesEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEE, .-_ZN2v88internal16WasmMemoryObject16update_instancesEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEE
	.section	.rodata._ZN2v88internal16WasmMemoryObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEj.str1.1,"aMS",@progbits,1
.LC9:
	.string	"disabled-by-default-v8.wasm"
.LC10:
	.string	"GrowMemory"
	.section	.rodata._ZN2v88internal16WasmMemoryObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEj.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"wasm::max_mem_pages() >= maximum_pages"
	.align 8
.LC12:
	.string	"0 == old_size % wasm::kWasmPageSize"
	.align 8
.LC13:
	.string	"wasm::max_mem_pages() >= old_pages"
	.section	.text._ZN2v88internal16WasmMemoryObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmMemoryObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEj
	.type	_ZN2v88internal16WasmMemoryObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEj, @function
_ZN2v88internal16WasmMemoryObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEj:
.LFB22684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movl	%edx, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal16WasmMemoryObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEjE29trace_event_unique_atomic1384(%rip), %rbx
	testq	%rbx, %rbx
	je	.L885
.L823:
	movq	$0, -112(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L886
.L825:
	movq	(%r14), %rax
	movq	41112(%r12), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L829
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L830:
	movl	39(%rsi), %eax
	testb	$8, %al
	je	.L832
	cmpb	$0, _ZN2v88internal28FLAG_wasm_grow_shared_memoryE(%rip)
	je	.L844
.L832:
	movq	45752(%r12), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal4wasm17WasmMemoryTracker20IsWasmMemoryGrowableENS0_6HandleINS0_13JSArrayBufferEEE@PLT
	testb	%al, %al
	je	.L844
	call	_ZN2v88internal4wasm13max_mem_pagesEv@PLT
	movl	%eax, %r15d
	movq	(%r14), %rax
	movslq	35(%rax), %rax
	testq	%rax, %rax
	js	.L835
	cmpl	%eax, %r15d
	cmova	%eax, %r15d
.L835:
	call	_ZN2v88internal4wasm13max_mem_pagesEv@PLT
	cmpl	%eax, %r15d
	ja	.L887
	movq	0(%r13), %rax
	movq	23(%rax), %rbx
	testw	%bx, %bx
	je	.L888
	leaq	.LC12(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L888:
	call	_ZN2v88internal4wasm13max_mem_pagesEv@PLT
	shrq	$16, %rbx
	movl	%eax, %eax
	cmpq	%rax, %rbx
	ja	.L889
	movl	-136(%rbp), %r9d
	movl	%r15d, %ecx
	subq	%rbx, %rcx
	cmpq	%rcx, %r9
	movq	%r9, -136(%rbp)
	ja	.L844
	call	_ZN2v88internal4wasm13max_mem_pagesEv@PLT
	movq	-136(%rbp), %r9
	movl	%eax, %eax
	subq	%rbx, %rax
	cmpq	%rax, %r9
	ja	.L844
	movq	0(%r13), %rax
	addq	%rbx, %r9
	salq	$16, %r9
	movl	39(%rax), %edx
	andl	$8, %edx
	je	.L840
	movq	%r9, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -136(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_123AdjustBufferPermissionsEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEm
	testb	%al, %al
	je	.L844
	movq	0(%r13), %rax
	movq	-144(%rbp), %rdi
	movq	31(%rax), %rsi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal4wasm17WasmMemoryTracker18IsWasmSharedMemoryEPKv@PLT
	movq	-152(%rbp), %rsi
	movq	-136(%rbp), %r9
	testb	%al, %al
	jne	.L890
	movq	0(%r13), %rax
	movq	%r9, %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	leaq	-120(%rbp), %r15
	movl	39(%rax), %ecx
	andl	$1, %ecx
	call	_ZN2v88internal4wasm16SetupArrayBufferEPNS0_7IsolateEPvmbNS0_10SharedFlagE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	movq	(%r14), %rax
	movq	%r13, %rdx
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal16WasmMemoryObject16update_instancesEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEE
.L842:
	movq	(%r14), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal16WasmMemoryObject16update_instancesEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEE
.L833:
	leaq	-112(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L891
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L840:
	.cfi_restore_state
	movl	39(%rax), %edx
	andl	$1, %edx
	jne	.L848
	movq	31(%rax), %rsi
	testq	%rsi, %rsi
	je	.L846
	movl	39(%rax), %edx
	andl	$16, %edx
	jne	.L892
	movq	23(%rax), %rsi
.L846:
	cmpq	%rsi, %r9
	ja	.L848
	movq	%r9, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -136(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_123AdjustBufferPermissionsEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEm
	movq	-136(%rbp), %r9
	testb	%al, %al
	jne	.L893
.L844:
	movl	$-1, %ebx
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L848:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm14NewArrayBufferEPNS0_7IsolateEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L844
	movq	0(%r13), %rax
	movq	23(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L894
.L851:
	movq	%r15, %r13
	leaq	-120(%rbp), %r15
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L829:
	movq	41088(%r12), %r13
	cmpq	%r13, 41096(%r12)
	je	.L895
.L831:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L886:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L896
.L826:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L827
	movq	(%rdi), %rax
	call	*8(%rax)
.L827:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L828
	movq	(%rdi), %rax
	call	*8(%rax)
.L828:
	leaq	.LC10(%rip), %rax
	movq	%rbx, -104(%rbp)
	movq	%rax, -96(%rbp)
	leaq	-104(%rbp), %rax
	movq	%r13, -88(%rbp)
	movq	%rax, -112(%rbp)
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L885:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L897
.L824:
	movq	%rbx, _ZZN2v88internal16WasmMemoryObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEjE29trace_event_unique_atomic1384(%rip)
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L894:
	movq	(%r15), %rcx
	movq	31(%rax), %rsi
	movq	31(%rcx), %rdi
	call	memcpy@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm18DetachMemoryBufferEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEb@PLT
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L890:
	movq	-144(%rbp), %rdi
	movq	%r9, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm17WasmMemoryTracker22SetPendingUpdateOnGrowENS0_6HandleINS0_13JSArrayBufferEEEm@PLT
	leaq	37512(%r12), %rdi
	call	_ZN2v88internal10StackGuard16HandleInterruptsEv@PLT
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L887:
	leaq	.LC11(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L895:
	movq	%r12, %rdi
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L889:
	leaq	.LC13(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L897:
	leaq	.LC9(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L896:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L893:
	movq	0(%r13), %rax
	movq	%r9, -144(%rbp)
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	31(%rax), %r15
	movl	39(%rax), %eax
	movl	%eax, -136(%rbp)
	call	_ZN2v88internal4wasm18DetachMemoryBufferEPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEb@PLT
	movq	%r15, %rsi
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	-136(%rbp), %ecx
	movq	-144(%rbp), %r9
	andl	$1, %ecx
	movq	%r9, %rdx
	call	_ZN2v88internal4wasm16SetupArrayBufferEPNS0_7IsolateEPvmbNS0_10SharedFlagE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L851
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L892:
	andq	$-262144, %rax
	movq	%r9, -136(%rbp)
	movq	24(%rax), %rax
	movq	8160(%rax), %rdi
	call	_ZN2v88internal4wasm17WasmMemoryTracker18FindAllocationDataEPKv@PLT
	movq	-136(%rbp), %r9
	movq	8(%rax), %rsi
	jmp	.L846
.L891:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22684:
	.size	_ZN2v88internal16WasmMemoryObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEj, .-_ZN2v88internal16WasmMemoryObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEj
	.section	.rodata._ZN2v88internal16WasmGlobalObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEENS4_INS0_10FixedArrayEEENS0_4wasm9ValueTypeEib.str1.1,"aMS",@progbits,1
.LC14:
	.string	"offset == 0"
.LC15:
	.string	"unreachable code"
	.section	.rodata._ZN2v88internal16WasmGlobalObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEENS4_INS0_10FixedArrayEEENS0_4wasm9ValueTypeEib.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"offset + type_size <= untagged_buffer->byte_length()"
	.section	.text._ZN2v88internal16WasmGlobalObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEENS4_INS0_10FixedArrayEEENS0_4wasm9ValueTypeEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmGlobalObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEENS4_INS0_10FixedArrayEEENS0_4wasm9ValueTypeEib
	.type	_ZN2v88internal16WasmGlobalObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEENS4_INS0_10FixedArrayEEENS0_4wasm9ValueTypeEib, @function
_ZN2v88internal16WasmGlobalObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEENS4_INS0_10FixedArrayEEENS0_4wasm9ValueTypeEib:
.LFB22685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movzbl	%cl, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$40, %rsp
	movq	12464(%rdi), %rax
	movq	%rsi, -64(%rbp)
	movl	%r8d, -56(%rbp)
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L899
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L900:
	movq	1479(%rsi), %r8
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L902
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L903:
	xorl	%edx, %edx
	movq	%r14, %rdi
	movzbl	%bl, %ebx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movzbl	%r13b, %esi
	sall	$8, %ebx
	movq	%rax, %r10
	movq	(%rax), %rax
	movq	$0, 47(%rax)
	movq	(%r10), %rdx
	movslq	51(%rdx), %rax
	xorb	%al, %al
	orl	%esi, %eax
	salq	$32, %rax
	movq	%rax, 47(%rdx)
	movq	-56(%rbp), %rdx
	movq	(%r10), %rax
	salq	$32, %rdx
	movq	%rdx, 39(%rax)
	movq	(%r10), %rdx
	movslq	51(%rdx), %rax
	andb	$-2, %ah
	orl	%eax, %ebx
	leal	-6(%r13), %eax
	salq	$32, %rbx
	movq	%rbx, 47(%rdx)
	cmpb	$1, %al
	jbe	.L926
	cmpb	$9, %r13b
	jne	.L905
.L926:
	testq	%r12, %r12
	je	.L947
.L907:
	movq	(%r10), %r13
	movq	(%r12), %r12
	movq	%r12, 31(%r13)
	leaq	31(%r13), %r14
	testb	$1, %r12b
	je	.L911
.L946:
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L948
	testb	$24, %al
	je	.L911
.L952:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L949
	.p2align 4,,10
	.p2align 3
.L911:
	movq	%r10, %rax
.L918:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L905:
	.cfi_restore_state
	cmpb	$7, %r13b
	ja	.L912
	leaq	.L914(%rip), %rdx
	movslq	(%rdx,%r15,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal16WasmGlobalObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEENS4_INS0_10FixedArrayEEENS0_4wasm9ValueTypeEib,"a",@progbits
	.align 4
	.align 4
.L914:
	.long	.L912-.L914
	.long	.L916-.L914
	.long	.L913-.L914
	.long	.L916-.L914
	.long	.L913-.L914
	.long	.L925-.L914
	.long	.L913-.L914
	.long	.L913-.L914
	.section	.text._ZN2v88internal16WasmGlobalObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEENS4_INS0_10FixedArrayEEENS0_4wasm9ValueTypeEib
.L925:
	movl	$16, %r12d
	movl	$16, %ebx
.L915:
	movq	-64(%rbp), %r13
	testq	%r13, %r13
	je	.L950
.L917:
	movl	-56(%rbp), %eax
	movq	0(%r13), %r12
	addl	%ebx, %eax
	cmpq	23(%r12), %rax
	ja	.L951
	movq	(%r10), %r13
	movq	%r12, 23(%r13)
	leaq	23(%r13), %r14
	testb	$1, %r12b
	jne	.L946
	jmp	.L911
.L913:
	movl	$8, %r12d
	movl	$8, %ebx
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L948:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %r10
	testb	$24, %al
	jne	.L952
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L899:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L953
.L901:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L902:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L954
.L904:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r8, (%rsi)
	jmp	.L903
.L916:
	movl	$4, %r12d
	movl	$4, %ebx
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L949:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r10
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L950:
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%r10, -64(%rbp)
	call	_ZN2v88internal7Factory16NewJSArrayBufferENS0_10SharedFlagENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal13JSArrayBuffer19SetupAllocatingDataENS0_6HandleIS1_EEPNS0_7IsolateEmbNS0_10SharedFlagE@PLT
	movq	-64(%rbp), %r10
	testb	%al, %al
	jne	.L917
	xorl	%eax, %eax
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L947:
	movl	$1, %edx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%r10, -64(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-64(%rbp), %r10
	movq	%rax, %r12
	movl	-56(%rbp), %eax
	testl	%eax, %eax
	je	.L907
	leaq	.LC14(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L954:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L953:
	movq	%r14, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L901
.L912:
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L951:
	leaq	.LC16(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22685:
	.size	_ZN2v88internal16WasmGlobalObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEENS4_INS0_10FixedArrayEEENS0_4wasm9ValueTypeEib, .-_ZN2v88internal16WasmGlobalObject3NewEPNS0_7IsolateENS0_11MaybeHandleINS0_13JSArrayBufferEEENS4_INS0_10FixedArrayEEENS0_4wasm9ValueTypeEib
	.section	.text._ZN2v88internal26IndirectFunctionTableEntry5clearEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26IndirectFunctionTableEntry5clearEv
	.type	_ZN2v88internal26IndirectFunctionTableEntry5clearEv, @function
_ZN2v88internal26IndirectFunctionTableEntry5clearEv:
.LFB22689:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movslq	16(%rdi), %rdx
	testq	%rax, %rax
	je	.L956
	movq	(%rax), %rax
	movq	87(%rax), %rax
	movl	$-1, (%rax,%rdx,4)
	movq	(%rdi), %rax
	movslq	16(%rdi), %rdx
	movq	(%rax), %rax
	movq	79(%rax), %rax
	movq	$0, (%rax,%rdx,8)
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	71(%rax), %r13
.L986:
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %r12
	movl	16(%rdi), %eax
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%r13,%rax), %r14
	movq	%r12, (%r14)
	testb	$1, %r12b
	je	.L955
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L987
	testb	$24, %al
	je	.L955
.L989:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L988
.L955:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L987:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L989
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L956:
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	15(%rax), %rax
	movl	$-1, (%rax,%rdx,4)
	movq	8(%rdi), %rax
	movslq	16(%rdi), %rdx
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	$0, (%rax,%rdx,8)
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	39(%rax), %r13
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L988:
	popq	%rbx
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE22689:
	.size	_ZN2v88internal26IndirectFunctionTableEntry5clearEv, .-_ZN2v88internal26IndirectFunctionTableEntry5clearEv
	.section	.text._ZN2v88internal15WasmTableObject19ClearDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15WasmTableObject19ClearDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEi
	.type	_ZN2v88internal15WasmTableObject19ClearDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEi, @function
_ZN2v88internal15WasmTableObject19ClearDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEi:
.LFB22655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movl	%edx, -84(%rbp)
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L991
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L992:
	movl	11(%rsi), %eax
	testl	%eax, %eax
	jle	.L990
	movq	0(%r13), %rax
	movl	$23, %r14d
	xorl	%r12d, %r12d
	leaq	-80(%rbp), %r15
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L998:
	movq	$0, -80(%rbp)
	movq	(%rax), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rsi
	leal	16(,%rdx,8), %eax
	movq	207(%rdi), %rdx
	cltq
	subq	$37592, %rsi
	movq	-1(%rax,%rdx), %r8
	movq	41112(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L1000
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1001:
	movq	%rax, -72(%rbp)
.L999:
	movl	-84(%rbp), %eax
	movq	%r15, %rdi
	addq	$24, %r14
	movl	%eax, -64(%rbp)
	call	_ZN2v88internal26IndirectFunctionTableEntry5clearEv
	movq	0(%r13), %rax
	movq	%rax, %rsi
	cmpl	%r12d, 11(%rax)
	jle	.L990
.L994:
	movq	(%rsi,%r14), %rdx
	movq	-8(%r14,%rax), %rsi
	addl	$3, %r12d
	movq	41112(%rbx), %rdi
	sarq	$32, %rdx
	testq	%rdi, %rdi
	je	.L995
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-96(%rbp), %rdx
.L996:
	testq	%rdx, %rdx
	jne	.L998
	movq	%rax, -80(%rbp)
	movq	$0, -72(%rbp)
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L995:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1008
.L997:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1000:
	movq	41088(%rsi), %rax
	cmpq	41096(%rsi), %rax
	je	.L1009
.L1002:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rsi)
	movq	%r8, (%rax)
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L990:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1010
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1008:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -104(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdx
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L991:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L1011
.L993:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	%rsi, %rdi
	movq	%r8, -104(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %rsi
	jmp	.L1002
.L1011:
	movq	%rbx, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L993
.L1010:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22655:
	.size	_ZN2v88internal15WasmTableObject19ClearDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEi, .-_ZN2v88internal15WasmTableObject19ClearDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.text._ZN2v88internal26IndirectFunctionTableEntry3SetEiNS0_6HandleINS0_18WasmInstanceObjectEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26IndirectFunctionTableEntry3SetEiNS0_6HandleINS0_18WasmInstanceObjectEEEi
	.type	_ZN2v88internal26IndirectFunctionTableEntry3SetEiNS0_6HandleINS0_18WasmInstanceObjectEEEi, @function
_ZN2v88internal26IndirectFunctionTableEntry3SetEiNS0_6HandleINS0_18WasmInstanceObjectEEEi:
.LFB22690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdx), %r12
	movq	%rdi, %rbx
	movq	135(%r12), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	movq	208(%rdi), %rax
	cmpl	%ecx, 60(%rax)
	jle	.L1013
	leal	16(,%rcx,8), %eax
	movq	55(%r12), %rcx
	movq	%r12, %rdx
	cltq
	movq	-1(%rax,%rcx), %r12
	movq	63(%rdx), %rdx
	movq	-16(%rdx,%rax), %rax
.L1014:
	movq	%r12, %rdx
	movq	(%rbx), %rcx
	movslq	16(%rbx), %rsi
	notq	%rdx
	andl	$1, %edx
	testq	%rcx, %rcx
	je	.L1017
	movq	(%rcx), %rcx
	movq	87(%rcx), %rcx
	movl	%r13d, (%rcx,%rsi,4)
	movq	(%rbx), %rcx
	movslq	16(%rbx), %rsi
	movq	(%rcx), %rcx
	movq	79(%rcx), %rcx
	movq	%rax, (%rcx,%rsi,8)
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	71(%rax), %r13
.L1045:
	movl	16(%rbx), %eax
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%r13,%rax), %r14
	movq	%r12, (%r14)
	testb	%dl, %dl
	jne	.L1012
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1046
	testb	$24, %al
	je	.L1012
.L1048:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1047
.L1012:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1013:
	.cfi_restore_state
	jbe	.L1015
	movq	63(%r12), %rax
	movl	%ecx, %esi
	movq	(%rax,%rsi,8), %rax
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1046:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1048
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	8(%rbx), %rcx
	movq	(%rcx), %rcx
	movq	15(%rcx), %rcx
	movl	%r13d, (%rcx,%rsi,4)
	movq	8(%rbx), %rcx
	movslq	16(%rbx), %rsi
	movq	(%rcx), %rcx
	movq	23(%rcx), %rcx
	movq	%rax, (%rcx,%rsi,8)
	movq	8(%rbx), %rax
	movq	(%rax), %rax
	movq	39(%rax), %r13
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1015:
	movl	%ecx, %esi
	call	_ZNK2v88internal4wasm12NativeModule24GetCallTargetForFunctionEj@PLT
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1047:
	popq	%rbx
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE22690:
	.size	_ZN2v88internal26IndirectFunctionTableEntry3SetEiNS0_6HandleINS0_18WasmInstanceObjectEEEi, .-_ZN2v88internal26IndirectFunctionTableEntry3SetEiNS0_6HandleINS0_18WasmInstanceObjectEEEi
	.section	.text._ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiPNS0_9SignatureINS0_4wasm9ValueTypeEEENS4_INS0_18WasmInstanceObjectEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiPNS0_9SignatureINS0_4wasm9ValueTypeEEENS4_INS0_18WasmInstanceObjectEEEi
	.type	_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiPNS0_9SignatureINS0_4wasm9ValueTypeEEENS4_INS0_18WasmInstanceObjectEEEi, @function
_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiPNS0_9SignatureINS0_4wasm9ValueTypeEEENS4_INS0_18WasmInstanceObjectEEEi:
.LFB22650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -92(%rbp)
	movq	41112(%rdi), %rdi
	movq	%rcx, -104(%rbp)
	movq	%r8, -112(%rbp)
	movl	%r9d, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1050
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -88(%rbp)
	movq	(%rax), %rsi
.L1051:
	movl	11(%rsi), %eax
	testl	%eax, %eax
	jle	.L1049
	movq	-88(%rbp), %rax
	leaq	-80(%rbp), %rdx
	movl	$23, %r12d
	xorl	%r14d, %r14d
	movq	%rdx, -120(%rbp)
	movq	(%rax), %rax
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1057:
	movq	$0, -80(%rbp)
	movq	(%r15), %rcx
	movq	%rcx, %rax
	movq	207(%rcx), %rcx
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	leal	16(,%rbx,8), %eax
	cltq
	subq	$37592, %rdx
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1059
	movl	%r8d, -128(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-128(%rbp), %r8d
.L1060:
	movq	%rax, -72(%rbp)
.L1058:
	movl	-92(%rbp), %eax
	movl	-96(%rbp), %ecx
	movl	%r8d, %esi
	addq	$24, %r12
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %rdi
	movl	%eax, -64(%rbp)
	call	_ZN2v88internal26IndirectFunctionTableEntry3SetEiNS0_6HandleINS0_18WasmInstanceObjectEEEi
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rsi
	cmpl	%r14d, 11(%rax)
	jle	.L1049
.L1053:
	movq	(%rsi,%r12), %rbx
	movq	-8(%r12,%rax), %rsi
	addl	$3, %r14d
	movq	41112(%r13), %rdi
	sarq	$32, %rbx
	testq	%rdi, %rdi
	je	.L1054
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L1055:
	movq	135(%rsi), %rax
	movq	-104(%rbp), %rsi
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rdi
	addq	$328, %rdi
	call	_ZNK2v88internal4wasm12SignatureMap4FindERKNS0_9SignatureINS1_9ValueTypeEEE@PLT
	movl	%eax, %r8d
	testq	%rbx, %rbx
	jne	.L1057
	movq	%r15, -80(%rbp)
	movq	$0, -72(%rbp)
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1054:
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L1067
.L1056:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1068
.L1061:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1049:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1069
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1067:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1056
	.p2align 4,,10
	.p2align 3
.L1050:
	movq	41088(%r13), %rax
	movq	%rax, -88(%rbp)
	cmpq	%rax, 41096(%r13)
	je	.L1070
.L1052:
	movq	-88(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdx)
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1068:
	movq	%rdx, %rdi
	movq	%rsi, -144(%rbp)
	movl	%r8d, -132(%rbp)
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movl	-132(%rbp), %r8d
	movq	-128(%rbp), %rdx
	jmp	.L1061
.L1070:
	movq	%r13, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, -88(%rbp)
	jmp	.L1052
.L1069:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22650:
	.size	_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiPNS0_9SignatureINS0_4wasm9ValueTypeEEENS4_INS0_18WasmInstanceObjectEEEi, .-_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiPNS0_9SignatureINS0_4wasm9ValueTypeEEENS4_INS0_18WasmInstanceObjectEEEi
	.section	.text._ZN2v88internal26IndirectFunctionTableEntry3SetEimNS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26IndirectFunctionTableEntry3SetEimNS0_6ObjectE
	.type	_ZN2v88internal26IndirectFunctionTableEntry3SetEimNS0_6ObjectE, @function
_ZN2v88internal26IndirectFunctionTableEntry3SetEimNS0_6ObjectE:
.LFB22691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%r12, %rax
	movq	(%rdi), %rcx
	notq	%rax
	movslq	16(%rdi), %r8
	andl	$1, %eax
	testq	%rcx, %rcx
	je	.L1072
	movq	(%rcx), %rcx
	movq	87(%rcx), %rcx
	movl	%esi, (%rcx,%r8,4)
	movq	(%rdi), %rcx
	movslq	16(%rdi), %rsi
	movq	(%rcx), %rcx
	movq	79(%rcx), %rcx
	movq	%rdx, (%rcx,%rsi,8)
	movq	(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	71(%rdx), %r13
.L1100:
	movl	16(%rdi), %edx
	leal	16(,%rdx,8), %edx
	movslq	%edx, %rdx
	leaq	-1(%r13,%rdx), %r14
	movq	%r12, (%r14)
	testb	%al, %al
	jne	.L1071
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1101
	testb	$24, %al
	je	.L1071
.L1103:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1102
.L1071:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1101:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1103
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1072:
	movq	8(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	15(%rcx), %rcx
	movl	%esi, (%rcx,%r8,4)
	movq	8(%rdi), %rcx
	movslq	16(%rdi), %rsi
	movq	(%rcx), %rcx
	movq	23(%rcx), %rcx
	movq	%rdx, (%rcx,%rsi,8)
	movq	8(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	39(%rdx), %r13
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1102:
	popq	%rbx
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE22691:
	.size	_ZN2v88internal26IndirectFunctionTableEntry3SetEimNS0_6ObjectE, .-_ZN2v88internal26IndirectFunctionTableEntry3SetEimNS0_6ObjectE
	.section	.text._ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_16WasmCapiFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_16WasmCapiFunctionEEE
	.type	_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_16WasmCapiFunctionEEE, @function
_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_16WasmCapiFunctionEEE:
.LFB22652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movl	%edx, -208(%rbp)
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1105
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -240(%rbp)
.L1106:
	movq	(%r15), %rax
	leaq	-128(%rbp), %r14
	movq	%r14, %rdi
	movq	23(%rax), %rax
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo23wasm_capi_function_dataEv@PLT
	movq	31(%rax), %rsi
	movslq	11(%rsi), %r13
	movq	%rsi, -200(%rbp)
	subl	$1, %r13d
	movslq	%r13d, %rdi
	call	_Znam@PLT
	movq	%rax, -256(%rbp)
	movq	%rax, %r9
	testl	%r13d, %r13d
	js	.L1108
	movq	-200(%rbp), %rsi
	movl	%r13d, %r8d
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	addq	$15, %rsi
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1128:
	movslq	%ecx, %rdi
	addl	$1, %ecx
	movb	%dl, (%r9,%rdi)
	leaq	1(%rax), %rdx
	cmpq	%rax, %r8
	je	.L1108
.L1123:
	movq	%rdx, %rax
.L1110:
	movzbl	(%rax,%rsi), %edx
	testb	%dl, %dl
	jne	.L1128
	movl	%eax, %r12d
	leaq	1(%rax), %rdx
	cmpq	%rax, %r8
	jne	.L1123
.L1108:
	movslq	%r12d, %rax
	subl	%r12d, %r13d
	movq	%rax, %xmm0
	movq	-256(%rbp), %rax
	movslq	%r13d, %r13
	movq	%r13, %xmm1
	movq	%rax, -176(%rbp)
	movq	-240(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	(%rax), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L1120
	leaq	-192(%rbp), %rsi
	movq	$23, -216(%rbp)
	movq	%rsi, -224(%rbp)
	leaq	-160(%rbp), %rsi
	movl	$0, -204(%rbp)
	movq	%rsi, -248(%rbp)
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1115:
	movq	$0, -160(%rbp)
	movq	0(%r13), %rcx
	movq	%rcx, %rax
	movq	207(%rcx), %rcx
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	movq	-200(%rbp), %rax
	leal	16(,%rax,8), %eax
	subq	$37592, %rdx
	cltq
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1117
	movl	%r10d, -200(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-200(%rbp), %r10d
.L1118:
	movq	%rax, -152(%rbp)
.L1116:
	movl	-208(%rbp), %eax
	movq	-248(%rbp), %rdi
	movl	%r10d, %esi
	movl	%eax, -144(%rbp)
	movq	-232(%rbp), %rax
	movq	(%r12), %rdx
	movq	(%rax), %rcx
	call	_ZN2v88internal26IndirectFunctionTableEntry3SetEimNS0_6ObjectE
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	movq	-240(%rbp), %rax
	addq	$24, -216(%rbp)
	movl	-204(%rbp), %esi
	movq	(%rax), %rax
	cmpl	%esi, 11(%rax)
	jle	.L1120
.L1111:
	movq	-216(%rbp), %rdx
	addl	$3, -204(%rbp)
	movq	(%rdx,%rax), %r12
	sarq	$32, %r12
	movq	%r12, -200(%rbp)
	movq	-8(%rdx,%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1112
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L1113:
	movq	135(%rsi), %rax
	movq	%r14, %rdi
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r12
	movq	(%r15), %rax
	movq	23(%rax), %rax
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo23wasm_capi_function_dataEv@PLT
	movq	%r14, %rdi
	movq	7(%rax), %rcx
	movq	%rcx, -232(%rbp)
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	-232(%rbp), %rcx
	movq	-224(%rbp), %rdx
	movq	%r12, %rsi
	movq	45752(%rbx), %rdi
	call	_ZN2v88internal8compiler26CompileWasmCapiCallWrapperEPNS0_4wasm10WasmEngineEPNS2_12NativeModuleEPNS0_9SignatureINS2_9ValueTypeEEEm@PLT
	movq	%rax, %r12
	movq	40960(%rbx), %rax
	movl	8(%r12), %esi
	leaq	8136(%rax), %rdi
	call	_ZN2v88internal22StatsCounterThreadSafe9IncrementEi@PLT
	movq	40960(%rbx), %rax
	movl	24(%r12), %esi
	leaq	8200(%rax), %rdi
	call	_ZN2v88internal22StatsCounterThreadSafe9IncrementEi@PLT
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory9NewTuple2ENS0_6HandleINS0_6ObjectEEES4_NS0_14AllocationTypeE@PLT
	movq	-224(%rbp), %rsi
	movq	%rax, -232(%rbp)
	movq	0(%r13), %rax
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rdi
	addq	$328, %rdi
	call	_ZNK2v88internal4wasm12SignatureMap4FindERKNS0_9SignatureINS1_9ValueTypeEEE@PLT
	cmpq	$0, -200(%rbp)
	movl	%eax, %r10d
	jne	.L1115
	movq	%r13, -160(%rbp)
	movq	$0, -152(%rbp)
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1112:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L1129
.L1114:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1117:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L1130
.L1119:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1120:
	movq	-256(%rbp), %rdi
	call	_ZdaPv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1131
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1129:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -232(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1105:
	movq	41088(%rbx), %rax
	movq	%rax, -240(%rbp)
	cmpq	%rax, 41096(%rbx)
	je	.L1132
.L1107:
	movq	-240(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1130:
	movq	%rdx, %rdi
	movq	%rsi, -272(%rbp)
	movl	%r10d, -260(%rbp)
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movl	-260(%rbp), %r10d
	movq	-200(%rbp), %rdx
	jmp	.L1119
.L1132:
	movq	%rbx, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	%rax, -240(%rbp)
	jmp	.L1107
.L1131:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22652:
	.size	_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_16WasmCapiFunctionEEE, .-_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_16WasmCapiFunctionEEE
	.section	.text._ZNK2v88internal26IndirectFunctionTableEntry10object_refEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal26IndirectFunctionTableEntry10object_refEv
	.type	_ZNK2v88internal26IndirectFunctionTableEntry10object_refEv, @function
_ZNK2v88internal26IndirectFunctionTableEntry10object_refEv:
.LFB22692:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	movq	(%rdi), %rdx
	leal	16(,%rax,8), %eax
	cltq
	testq	%rdx, %rdx
	je	.L1134
	movq	(%rdx), %rdx
	movq	71(%rdx), %rdx
	movq	-1(%rax,%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1134:
	movq	8(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	39(%rdx), %rdx
	movq	-1(%rax,%rdx), %rax
	ret
	.cfi_endproc
.LFE22692:
	.size	_ZNK2v88internal26IndirectFunctionTableEntry10object_refEv, .-_ZNK2v88internal26IndirectFunctionTableEntry10object_refEv
	.section	.text._ZNK2v88internal26IndirectFunctionTableEntry6sig_idEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal26IndirectFunctionTableEntry6sig_idEv
	.type	_ZNK2v88internal26IndirectFunctionTableEntry6sig_idEv, @function
_ZNK2v88internal26IndirectFunctionTableEntry6sig_idEv:
.LFB22693:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	16(%rdi), %rdx
	testq	%rax, %rax
	je	.L1137
	movq	(%rax), %rax
	movq	87(%rax), %rax
	movl	(%rax,%rdx,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1137:
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	15(%rax), %rax
	movl	(%rax,%rdx,4), %eax
	ret
	.cfi_endproc
.LFE22693:
	.size	_ZNK2v88internal26IndirectFunctionTableEntry6sig_idEv, .-_ZNK2v88internal26IndirectFunctionTableEntry6sig_idEv
	.section	.text._ZNK2v88internal26IndirectFunctionTableEntry6targetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal26IndirectFunctionTableEntry6targetEv
	.type	_ZNK2v88internal26IndirectFunctionTableEntry6targetEv, @function
_ZNK2v88internal26IndirectFunctionTableEntry6targetEv:
.LFB22694:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	16(%rdi), %rdx
	testq	%rax, %rax
	je	.L1140
	movq	(%rax), %rax
	movq	79(%rax), %rax
	movq	(%rax,%rdx,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1140:
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	(%rax,%rdx,8), %rax
	ret
	.cfi_endproc
.LFE22694:
	.size	_ZNK2v88internal26IndirectFunctionTableEntry6targetEv, .-_ZNK2v88internal26IndirectFunctionTableEntry6targetEv
	.section	.text._ZN2v88internal21ImportedFunctionEntry11SetWasmToJsEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKNS0_4wasm8WasmCodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21ImportedFunctionEntry11SetWasmToJsEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKNS0_4wasm8WasmCodeE
	.type	_ZN2v88internal21ImportedFunctionEntry11SetWasmToJsEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKNS0_4wasm8WasmCodeE, @function
_ZN2v88internal21ImportedFunctionEntry11SetWasmToJsEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKNS0_4wasm8WasmCodeE:
.LFB22695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movl	$1, %ecx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$24, %rsp
	movq	(%rbx), %rsi
	call	_ZN2v88internal7Factory9NewTuple2ENS0_6HandleINS0_6ObjectEEES4_NS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdx
	movq	(%rax), %r12
	movl	8(%rbx), %eax
	movq	(%rdx), %rdx
	leal	16(,%rax,8), %eax
	movq	55(%rdx), %r14
	cltq
	leaq	-1(%r14,%rax), %r15
	movq	%r12, (%r15)
	testb	$1, %r12b
	je	.L1146
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L1154
	testb	$24, %al
	je	.L1146
.L1156:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1155
.L1146:
	movq	(%rbx), %rax
	movslq	8(%rbx), %rdx
	movq	0(%r13), %rcx
	movq	(%rax), %rax
	movq	63(%rax), %rax
	movq	%rcx, (%rax,%rdx,8)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1154:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1156
	jmp	.L1146
	.p2align 4,,10
	.p2align 3
.L1155:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1146
	.cfi_endproc
.LFE22695:
	.size	_ZN2v88internal21ImportedFunctionEntry11SetWasmToJsEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKNS0_4wasm8WasmCodeE, .-_ZN2v88internal21ImportedFunctionEntry11SetWasmToJsEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKNS0_4wasm8WasmCodeE
	.section	.text._ZN2v88internal21ImportedFunctionEntry13SetWasmToWasmENS0_18WasmInstanceObjectEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21ImportedFunctionEntry13SetWasmToWasmENS0_18WasmInstanceObjectEm
	.type	_ZN2v88internal21ImportedFunctionEntry13SetWasmToWasmENS0_18WasmInstanceObjectEm, @function
_ZN2v88internal21ImportedFunctionEntry13SetWasmToWasmENS0_18WasmInstanceObjectEm:
.LFB22696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	55(%rax), %r14
	movl	8(%rdi), %eax
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%r14,%rax), %r15
	movq	%rsi, (%r15)
	testb	$1, %sil
	je	.L1161
	movq	%rsi, %rcx
	movq	%rsi, %r12
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L1169
	testb	$24, %al
	je	.L1161
.L1171:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1170
.L1161:
	movq	(%rbx), %rax
	movslq	8(%rbx), %rdx
	movq	(%rax), %rax
	movq	63(%rax), %rax
	movq	%r13, (%rax,%rdx,8)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1169:
	.cfi_restore_state
	movq	%rsi, %rdx
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1171
	jmp	.L1161
	.p2align 4,,10
	.p2align 3
.L1170:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1161
	.cfi_endproc
.LFE22696:
	.size	_ZN2v88internal21ImportedFunctionEntry13SetWasmToWasmENS0_18WasmInstanceObjectEm, .-_ZN2v88internal21ImportedFunctionEntry13SetWasmToWasmENS0_18WasmInstanceObjectEm
	.section	.text._ZN2v88internal21ImportedFunctionEntry8instanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21ImportedFunctionEntry8instanceEv
	.type	_ZN2v88internal21ImportedFunctionEntry8instanceEv, @function
_ZN2v88internal21ImportedFunctionEntry8instanceEv:
.LFB22697:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rdx
	movl	8(%rdi), %eax
	movq	55(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	testb	$1, %al
	jne	.L1173
.L1175:
	movq	7(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1173:
	movq	-1(%rax), %rdx
	cmpw	$1100, 11(%rdx)
	jne	.L1175
	ret
	.cfi_endproc
.LFE22697:
	.size	_ZN2v88internal21ImportedFunctionEntry8instanceEv, .-_ZN2v88internal21ImportedFunctionEntry8instanceEv
	.section	.text._ZN2v88internal21ImportedFunctionEntry8callableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21ImportedFunctionEntry8callableEv
	.type	_ZN2v88internal21ImportedFunctionEntry8callableEv, @function
_ZN2v88internal21ImportedFunctionEntry8callableEv:
.LFB22698:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rdx
	movl	8(%rdi), %eax
	movq	55(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	movq	15(%rax), %rax
	ret
	.cfi_endproc
.LFE22698:
	.size	_ZN2v88internal21ImportedFunctionEntry8callableEv, .-_ZN2v88internal21ImportedFunctionEntry8callableEv
	.section	.text._ZN2v88internal21ImportedFunctionEntry10object_refEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21ImportedFunctionEntry10object_refEv
	.type	_ZN2v88internal21ImportedFunctionEntry10object_refEv, @function
_ZN2v88internal21ImportedFunctionEntry10object_refEv:
.LFB22699:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rdx
	movl	8(%rdi), %eax
	movq	55(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	ret
	.cfi_endproc
.LFE22699:
	.size	_ZN2v88internal21ImportedFunctionEntry10object_refEv, .-_ZN2v88internal21ImportedFunctionEntry10object_refEv
	.section	.text._ZN2v88internal21ImportedFunctionEntry6targetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21ImportedFunctionEntry6targetEv
	.type	_ZN2v88internal21ImportedFunctionEntry6targetEv, @function
_ZN2v88internal21ImportedFunctionEntry6targetEv:
.LFB22700:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	8(%rdi), %rdx
	movq	(%rax), %rax
	movq	63(%rax), %rax
	movq	(%rax,%rdx,8), %rax
	ret
	.cfi_endproc
.LFE22700:
	.size	_ZN2v88internal21ImportedFunctionEntry6targetEv, .-_ZN2v88internal21ImportedFunctionEntry6targetEv
	.section	.text._ZN2v88internal18WasmInstanceObject12SetRawMemoryEPhm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18WasmInstanceObject12SetRawMemoryEPhm
	.type	_ZN2v88internal18WasmInstanceObject12SetRawMemoryEPhm, @function
_ZN2v88internal18WasmInstanceObject12SetRawMemoryEPhm:
.LFB22702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal4wasm13max_mem_pagesEv@PLT
	movl	%eax, %eax
	salq	$16, %rax
	cmpq	%rax, %rbx
	ja	.L1182
	movq	%rbx, %rdi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	movq	0(%r13), %rdx
	subq	$1, %rax
	movq	%r12, 23(%rdx)
	movq	0(%r13), %rdx
	movq	%rbx, 31(%rdx)
	movq	0(%r13), %rdx
	movq	%rax, 39(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1182:
	.cfi_restore_state
	leaq	.LC8(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22702:
	.size	_ZN2v88internal18WasmInstanceObject12SetRawMemoryEPhm, .-_ZN2v88internal18WasmInstanceObject12SetRawMemoryEPhm
	.section	.text._ZN2v88internal18WasmInstanceObject6moduleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18WasmInstanceObject6moduleEv
	.type	_ZN2v88internal18WasmInstanceObject6moduleEv, @function
_ZN2v88internal18WasmInstanceObject6moduleEv:
.LFB22703:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	ret
	.cfi_endproc
.LFE22703:
	.size	_ZN2v88internal18WasmInstanceObject6moduleEv, .-_ZN2v88internal18WasmInstanceObject6moduleEv
	.section	.text._ZN2v88internal18WasmInstanceObject20GetOrCreateDebugInfoENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18WasmInstanceObject20GetOrCreateDebugInfoENS0_6HandleIS1_EE
	.type	_ZN2v88internal18WasmInstanceObject20GetOrCreateDebugInfoENS0_6HandleIS1_EE, @function
_ZN2v88internal18WasmInstanceObject20GetOrCreateDebugInfoENS0_6HandleIS1_EE:
.LFB22704:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	191(%rax), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37504(%rax), %rsi
	je	.L1185
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	-37592(%rax), %rbx
	subq	$24, %rsp
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1186
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1186:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1194
.L1188:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1185:
	.cfi_restore 3
	.cfi_restore 6
	jmp	_ZN2v88internal13WasmDebugInfo3NewENS0_6HandleINS0_18WasmInstanceObjectEEE@PLT
	.p2align 4,,10
	.p2align 3
.L1194:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1188
	.cfi_endproc
.LFE22704:
	.size	_ZN2v88internal18WasmInstanceObject20GetOrCreateDebugInfoENS0_6HandleIS1_EE, .-_ZN2v88internal18WasmInstanceObject20GetOrCreateDebugInfoENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal16WasmModuleObject13SetBreakPointENS0_6HandleIS1_EEPiNS2_INS0_10BreakPointEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject13SetBreakPointENS0_6HandleIS1_EEPiNS2_INS0_10BreakPointEEE
	.type	_ZN2v88internal16WasmModuleObject13SetBreakPointENS0_6HandleIS1_EEPiNS2_INS0_10BreakPointEEE, @function
_ZN2v88internal16WasmModuleObject13SetBreakPointENS0_6HandleIS1_EEPiNS2_INS0_10BreakPointEEE:
.LFB22584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movl	(%rsi), %r8d
	movq	%rax, %rcx
	movq	23(%rax), %rax
	andq	$-262144, %rcx
	movq	7(%rax), %rax
	movq	24(%rcx), %r13
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	movq	136(%rax), %rsi
	movq	144(%rax), %rax
	subq	%rsi, %rax
	sarq	$5, %rax
	movl	%eax, %edi
	testl	%eax, %eax
	jne	.L1199
	jmp	.L1220
	.p2align 4,,10
	.p2align 3
.L1221:
	sarl	%eax
	addl	%ebx, %eax
	movslq	%eax, %rcx
	salq	$5, %rcx
	cmpl	16(%rsi,%rcx), %r8d
	cmovb	%eax, %edi
	cmovnb	%eax, %ebx
.L1199:
	movl	%edi, %eax
	subl	%ebx, %eax
	cmpl	$1, %eax
	jg	.L1221
	movslq	%ebx, %rax
	salq	$5, %rax
	addq	%rax, %rsi
	movl	16(%rsi), %eax
	cmpl	%eax, %r8d
	jb	.L1200
	movl	20(%rsi), %ecx
	addl	%eax, %ecx
	cmpl	%ecx, %r8d
	jnb	.L1200
.L1197:
	movl	%r8d, %r14d
	movl	%r8d, %esi
	movq	%r12, %rdi
	subq	$37592, %r13
	subl	%eax, %r14d
	call	_ZN2v88internal16WasmModuleObject13AddBreakpointENS0_6HandleIS1_EEiNS2_INS0_10BreakPointEEE
	movq	(%r12), %rax
	movq	41112(%r13), %rdi
	movq	47(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1203
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L1204:
	movl	19(%rsi), %eax
	xorl	%r12d, %r12d
	testl	%eax, %eax
	jg	.L1206
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1207:
	movq	(%r15), %rsi
	addq	$1, %r12
	cmpl	%r12d, 19(%rsi)
	jle	.L1211
.L1206:
	movq	23(%rsi,%r12,8), %rsi
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	jne	.L1207
	cmpl	$3, %esi
	je	.L1207
	movq	41112(%r13), %rdi
	andq	$-3, %rsi
	testq	%rdi, %rdi
	je	.L1208
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L1209:
	call	_ZN2v88internal18WasmInstanceObject20GetOrCreateDebugInfoENS0_6HandleIS1_EE
	movl	%ebx, %esi
	movl	%r14d, %edx
	addq	$1, %r12
	movq	%rax, %rdi
	call	_ZN2v88internal13WasmDebugInfo13SetBreakpointENS0_6HandleIS1_EEii@PLT
	movq	(%r15), %rsi
	cmpl	%r12d, 19(%rsi)
	jg	.L1206
	.p2align 4,,10
	.p2align 3
.L1211:
	movl	$1, %eax
.L1195:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1203:
	.cfi_restore_state
	movq	41088(%r13), %r15
	cmpq	%r15, 41096(%r13)
	je	.L1222
.L1205:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1208:
	movq	41088(%r13), %rdi
	cmpq	41096(%r13), %rdi
	je	.L1223
.L1210:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdi)
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1220:
	movl	16(%rsi), %eax
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1223:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L1210
.L1222:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1205
.L1200:
	xorl	%eax, %eax
	jmp	.L1195
	.cfi_endproc
.LFE22584:
	.size	_ZN2v88internal16WasmModuleObject13SetBreakPointENS0_6HandleIS1_EEPiNS2_INS0_10BreakPointEEE, .-_ZN2v88internal16WasmModuleObject13SetBreakPointENS0_6HandleIS1_EEPiNS2_INS0_10BreakPointEEE
	.section	.text._ZN2v88internal16WasmModuleObject27SetBreakpointsOnNewInstanceENS0_6HandleIS1_EENS2_INS0_18WasmInstanceObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmModuleObject27SetBreakpointsOnNewInstanceENS0_6HandleIS1_EENS2_INS0_18WasmInstanceObjectEEE
	.type	_ZN2v88internal16WasmModuleObject27SetBreakpointsOnNewInstanceENS0_6HandleIS1_EENS2_INS0_18WasmInstanceObjectEEE, @function
_ZN2v88internal16WasmModuleObject27SetBreakpointsOnNewInstanceENS0_6HandleIS1_EENS2_INS0_18WasmInstanceObjectEEE:
.LFB22588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	leaq	-37592(%rax), %r14
	movq	-37504(%rax), %rax
	cmpq	%rax, 63(%rdx)
	je	.L1224
	movq	%rsi, %rdi
	call	_ZN2v88internal18WasmInstanceObject20GetOrCreateDebugInfoENS0_6HandleIS1_EE
	movq	41112(%r14), %rdi
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	63(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1226
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L1227:
	movslq	11(%rsi), %rax
	testq	%rax, %rax
	jle	.L1224
	subl	$1, %eax
	movl	$16, %r15d
	leaq	24(,%rax,8), %rax
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L1238:
	movq	-1(%rsi,%r15), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1230
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1231:
	cmpq	%rsi, 88(%r14)
	je	.L1224
	movq	(%rbx), %rax
	movslq	11(%rsi), %rdx
	xorl	%esi, %esi
	movq	23(%rax), %rax
	movl	%edx, %r9d
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	movq	136(%rax), %rdi
	movq	144(%rax), %rax
	subq	%rdi, %rax
	sarq	$5, %rax
	movl	%eax, %r8d
	testl	%eax, %eax
	jne	.L1236
	jmp	.L1245
	.p2align 4,,10
	.p2align 3
.L1246:
	sarl	%eax
	addl	%esi, %eax
	movslq	%eax, %r10
	salq	$5, %r10
	cmpl	16(%rdi,%r10), %r9d
	cmovb	%eax, %r8d
	cmovnb	%eax, %esi
.L1236:
	movl	%r8d, %eax
	subl	%esi, %eax
	cmpl	$1, %eax
	jg	.L1246
	movslq	%esi, %rax
	salq	$5, %rax
	addq	%rdi, %rax
	movl	16(%rax), %r8d
	cmpl	%r8d, %edx
	jb	.L1244
	movl	20(%rax), %ecx
	addl	%r8d, %ecx
	cmpl	%ecx, %edx
	jnb	.L1244
.L1234:
	subl	%r8d, %edx
	movq	%r13, %rdi
	addq	$8, %r15
	call	_ZN2v88internal13WasmDebugInfo13SetBreakpointENS0_6HandleIS1_EEii@PLT
	cmpq	%r15, -56(%rbp)
	je	.L1224
	movq	(%r12), %rsi
	jmp	.L1238
	.p2align 4,,10
	.p2align 3
.L1224:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1226:
	.cfi_restore_state
	movq	41088(%r14), %r12
	cmpq	%r12, 41096(%r14)
	je	.L1247
.L1228:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r12)
	jmp	.L1227
	.p2align 4,,10
	.p2align 3
.L1230:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L1248
.L1232:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1245:
	movl	16(%rdi), %r8d
	jmp	.L1234
	.p2align 4,,10
	.p2align 3
.L1248:
	movq	%r14, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	jmp	.L1232
	.p2align 4,,10
	.p2align 3
.L1244:
	movl	-16(%rdi), %r8d
	movl	$-1, %esi
	jmp	.L1234
.L1247:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1228
	.cfi_endproc
.LFE22588:
	.size	_ZN2v88internal16WasmModuleObject27SetBreakpointsOnNewInstanceENS0_6HandleIS1_EENS2_INS0_18WasmInstanceObjectEEE, .-_ZN2v88internal16WasmModuleObject27SetBreakpointsOnNewInstanceENS0_6HandleIS1_EENS2_INS0_18WasmInstanceObjectEEE
	.section	.text._ZN2v88internal18WasmInstanceObject21InitDataSegmentArraysENS0_6HandleIS1_EENS2_INS0_16WasmModuleObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18WasmInstanceObject21InitDataSegmentArraysENS0_6HandleIS1_EENS2_INS0_16WasmModuleObjectEEE
	.type	_ZN2v88internal18WasmInstanceObject21InitDataSegmentArraysENS0_6HandleIS1_EENS2_INS0_16WasmModuleObjectEEE, @function
_ZN2v88internal18WasmInstanceObject21InitDataSegmentArraysENS0_6HandleIS1_EENS2_INS0_16WasmModuleObjectEEE:
.LFB22706:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %r8
	movq	232(%rax), %rax
	movl	76(%r8), %r9d
	movq	(%rax), %r10
	testq	%r9, %r9
	je	.L1249
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1251:
	movq	(%rdi), %rcx
	movq	%rax, %rdx
	salq	$5, %rdx
	addq	160(%r8), %rdx
	movzbl	24(%rdx), %esi
	movq	263(%rcx), %rcx
	movb	%sil, (%rcx,%rax)
	movq	(%rdi), %rsi
	movl	20(%rdx), %ecx
	movl	16(%rdx), %edx
	movq	247(%rsi), %rsi
	addq	%r10, %rdx
	movq	%rdx, (%rsi,%rax,8)
	movq	(%rdi), %rdx
	movq	255(%rdx), %rdx
	movl	%ecx, (%rdx,%rax,4)
	addq	$1, %rax
	cmpq	%r9, %rax
	jne	.L1251
.L1249:
	ret
	.cfi_endproc
.LFE22706:
	.size	_ZN2v88internal18WasmInstanceObject21InitDataSegmentArraysENS0_6HandleIS1_EENS2_INS0_16WasmModuleObjectEEE, .-_ZN2v88internal18WasmInstanceObject21InitDataSegmentArraysENS0_6HandleIS1_EENS2_INS0_16WasmModuleObjectEEE
	.section	.text._ZN2v88internal18WasmInstanceObject3NewEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18WasmInstanceObject3NewEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE
	.type	_ZN2v88internal18WasmInstanceObject3NewEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE, @function
_ZN2v88internal18WasmInstanceObject3NewEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE:
.LFB22705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdi), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1257
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1258:
	movq	1487(%rsi), %r12
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1260
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1261:
	movq	%r13, %rdi
	movl	$1, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	41112(%r13), %rdi
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1263
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L1264:
	movq	-88(%rbp), %rax
	movl	$0, 99(%rsi)
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rdi
	movl	60(%rdi), %eax
	movl	56(%rdi), %r8d
	movl	76(%rdi), %r14d
	movq	184(%rdi), %rcx
	movl	%eax, -96(%rbp)
	leal	0(,%r8,8), %edx
	sall	$4, %eax
	movq	192(%rdi), %r10
	leaq	64(%rdx,%rax), %rdx
	leaq	(%r14,%r14,2), %rax
	leaq	(%r14,%rax,4), %rax
	leaq	(%rdx,%rax), %r15
	cmpq	%r10, %rcx
	je	.L1266
	leaq	-16(%r10), %rsi
	movq	%rcx, %rax
	subq	%rcx, %rsi
	movq	%rsi, %r11
	shrq	$4, %r11
	cmpq	$96, %rsi
	jbe	.L1267
	andq	$-64, %rsi
	pxor	%xmm2, %xmm2
	pxor	%xmm3, %xmm3
	addq	%rcx, %rsi
	.p2align 4,,10
	.p2align 3
.L1268:
	movdqu	4(%rax), %xmm0
	movdqu	20(%rax), %xmm4
	addq	$64, %rax
	movdqu	-28(%rax), %xmm1
	movdqu	-12(%rax), %xmm5
	shufps	$136, %xmm4, %xmm0
	shufps	$136, %xmm5, %xmm1
	shufps	$136, %xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	pslld	$1, %xmm0
	paddd	%xmm1, %xmm0
	pslld	$3, %xmm0
	movdqa	%xmm0, %xmm1
	punpckldq	%xmm3, %xmm0
	punpckhdq	%xmm3, %xmm1
	paddq	%xmm1, %xmm0
	paddq	%xmm0, %xmm2
	cmpq	%rax, %rsi
	jne	.L1268
	movdqa	%xmm2, %xmm0
	andq	$-4, %r11
	psrldq	$8, %xmm0
	salq	$4, %r11
	paddq	%xmm0, %xmm2
	addq	%r11, %rcx
	movq	%xmm2, %rax
	addq	%rax, %r15
.L1267:
	movl	4(%rcx), %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	addq	%rax, %r15
	leaq	16(%rcx), %rax
	cmpq	%rax, %r10
	je	.L1266
	movl	20(%rcx), %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	addq	%rax, %r15
	leaq	32(%rcx), %rax
	cmpq	%rax, %r10
	je	.L1266
	movl	36(%rcx), %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	addq	%rax, %r15
	leaq	48(%rcx), %rax
	cmpq	%rax, %r10
	je	.L1266
	movl	52(%rcx), %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	addq	%rax, %r15
	leaq	64(%rcx), %rax
	cmpq	%rax, %r10
	je	.L1266
	movl	68(%rcx), %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	addq	%rax, %r15
	leaq	80(%rcx), %rax
	cmpq	%rax, %r10
	je	.L1266
	movl	84(%rcx), %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	addq	%rax, %r15
	leaq	96(%rcx), %rax
	cmpq	%rax, %r10
	je	.L1266
	movl	100(%rcx), %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	addq	%rax, %r15
.L1266:
	movq	288(%rdi), %r10
	subq	280(%rdi), %r10
	movl	$80, %edi
	movabsq	$7905747460161236407, %rax
	sarq	$3, %r10
	movl	%r8d, -108(%rbp)
	imulq	%rax, %r10
	movq	%r10, -104(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movl	-96(%rbp), %edi
	movl	$8, %esi
	movq	%rax, %r12
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, (%r12)
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r12)
	movups	%xmm0, 16(%r12)
	call	calloc@PLT
	movq	(%rbx), %rcx
	movl	-108(%rbp), %edi
	movl	$8, %esi
	movq	%rax, 32(%r12)
	movq	%rax, 63(%rcx)
	call	calloc@PLT
	movq	(%rbx), %rcx
	movq	%r14, %rdi
	movl	$8, %esi
	movq	%rax, 40(%r12)
	movq	%rax, 111(%rcx)
	call	calloc@PLT
	movq	(%rbx), %rcx
	movq	%r14, %rdi
	movl	$4, %esi
	movq	%rax, 48(%r12)
	movq	%rax, 247(%rcx)
	call	calloc@PLT
	movq	(%rbx), %rcx
	movq	%r14, %rdi
	movl	$1, %esi
	movq	%rax, 56(%r12)
	movq	%rax, 255(%rcx)
	call	calloc@PLT
	movq	(%rbx), %rcx
	movq	-104(%rbp), %r10
	movl	$1, %esi
	movq	%rax, 64(%r12)
	movq	%rax, 263(%rcx)
	movq	%r10, %rdi
	call	calloc@PLT
	movq	(%rbx), %rcx
	movq	%rax, 72(%r12)
	movq	%rax, 271(%rcx)
	movq	32(%r13), %r14
	addq	%r15, %r14
	movq	%r14, %rax
	subq	48(%r13), %rax
	movq	%r14, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L1356
.L1270:
	testq	%r15, %r15
	js	.L1357
	je	.L1272
	cmpq	40(%r13), %r14
	jg	.L1358
	.p2align 4,,10
	.p2align 3
.L1272:
	movl	$16, %edi
	call	_Znwm@PLT
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rax, %rdx
	leaq	16(%r12), %rax
	movq	%rax, (%rdx)
	leaq	8(%r12), %rax
	movq	%r12, 8(%rdx)
	movq	%rax, -104(%rbp)
	je	.L1359
	leaq	8(%r12), %rax
	lock addl	$1, (%rax)
.L1273:
	movl	$48, %edi
	movq	%rdx, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	%rax, %r14
	movups	%xmm0, 8(%rax)
	movq	%r15, (%rax)
	movq	%r14, %rsi
	movq	%rdx, 24(%rax)
	leaq	_ZN2v88internal7ManagedINS0_12_GLOBAL__N_129WasmInstanceNativeAllocationsEE10DestructorEPv(%rip), %rax
	xorl	%edx, %edx
	movq	%rax, 32(%r14)
	movq	$0, 40(%r14)
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	41152(%r13), %rdi
	movq	(%rax), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, 40(%r14)
	movq	%rax, %rdi
	call	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE@PLT
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1274
	leaq	8(%r12), %rdx
	movl	$-1, %eax
	lock xaddl	%eax, (%rdx)
	cmpl	$1, %eax
	je	.L1360
.L1277:
	movq	(%rbx), %r14
	movq	(%r15), %r12
	movq	%r12, 215(%r14)
	leaq	215(%r14), %rsi
	testb	$1, %r12b
	je	.L1309
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	je	.L1282
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-104(%rbp), %rsi
.L1282:
	testb	$24, %al
	je	.L1309
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1361
	.p2align 4,,10
	.p2align 3
.L1309:
	movl	-96(%rbp), %esi
	movq	%r13, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rdi
	movq	(%rax), %r12
	leaq	55(%rdi), %r15
	movq	%r12, 55(%rdi)
	testb	$1, %r12b
	je	.L1308
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L1285
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-96(%rbp), %rdi
.L1285:
	testb	$24, %al
	je	.L1308
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1362
	.p2align 4,,10
	.p2align 3
.L1308:
	movq	(%rbx), %r12
	call	_ZN2v88internal4wasm13max_mem_pagesEv@PLT
	xorl	%edi, %edi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	leaq	128(%r13), %rdx
	movq	$0, 23(%r12)
	movq	$0, 31(%r12)
	subq	$1, %rax
	movq	%rax, 39(%r12)
	movq	(%rbx), %rax
	movq	%rdx, 119(%rax)
	movq	(%rbx), %rax
	leaq	37536(%r13), %rdx
	movq	%rdx, 47(%rax)
	movq	(%rbx), %rax
	leaq	37520(%r13), %rdx
	movq	%rdx, 239(%rax)
	movq	(%rbx), %rax
	movq	$0, 103(%rax)
	movq	(%rbx), %rax
	movl	$0, 95(%rax)
	movq	(%rbx), %rax
	movq	$0, 87(%rax)
	movq	(%rbx), %rax
	movq	$0, 79(%rax)
	movq	12464(%r13), %rax
	movq	(%rbx), %r14
	movq	39(%rax), %r12
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1287
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
.L1288:
	movq	%r12, 151(%r14)
	leaq	151(%r14), %r15
	testb	$1, %r12b
	je	.L1307
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -96(%rbp)
	testl	$262144, %eax
	je	.L1291
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rcx
	movq	8(%rcx), %rax
.L1291:
	testb	$24, %al
	je	.L1307
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1363
	.p2align 4,,10
	.p2align 3
.L1307:
	movq	-88(%rbp), %rax
	movq	(%rbx), %rdi
	movq	(%rax), %r12
	leaq	135(%rdi), %r15
	movq	%r12, 135(%rdi)
	testb	$1, %r12b
	je	.L1306
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L1294
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-96(%rbp), %rdi
.L1294:
	testb	$24, %al
	je	.L1306
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1364
	.p2align 4,,10
	.p2align 3
.L1306:
	movq	-88(%rbp), %rax
	movq	(%rbx), %rcx
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	504(%rax), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1296
	movq	(%rdx), %rax
.L1296:
	movq	%rax, 127(%rcx)
	movq	-88(%rbp), %rax
	movq	41112(%r13), %rdi
	movq	(%rax), %rax
	movq	47(%rax), %r12
	testq	%rdi, %rdi
	je	.L1297
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1298:
	movq	%r13, %rdi
	leaq	-80(%rbp), %rdx
	movl	$0, -80(%rbp)
	movq	%rbx, -72(%rbp)
	call	_ZN2v88internal13WeakArrayList8AddToEndEPNS0_7IsolateENS0_6HandleIS1_EERKNS0_17MaybeObjectHandleE@PLT
	movq	-88(%rbp), %rcx
	movq	(%rax), %r12
	movq	(%rcx), %r13
	movq	%r12, 47(%r13)
	leaq	47(%r13), %r15
	testb	$1, %r12b
	je	.L1305
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L1301
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
.L1301:
	testb	$24, %al
	je	.L1305
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1365
	.p2align 4,,10
	.p2align 3
.L1305:
	movq	-88(%rbp), %r11
	movq	%rbx, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal18WasmInstanceObject21InitDataSegmentArraysENS0_6HandleIS1_EENS2_INS0_16WasmModuleObjectEEE
	movabsq	$7905747460161236407, %rdx
	movq	(%r11), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %r9
	movq	280(%r9), %rax
	movq	288(%r9), %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	imulq	%rdx, %rcx
	testq	%rcx, %rcx
	je	.L1303
	movq	(%rbx), %rdx
	movzbl	48(%rax), %eax
	movq	271(%rdx), %rdx
	movb	%al, (%rdx)
	cmpq	$1, %rcx
	jbe	.L1303
	movl	$56, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1304:
	movq	(%rbx), %rsi
	movq	280(%r9), %rdi
	movq	271(%rsi), %rsi
	movzbl	48(%rdi,%rdx), %edi
	addq	$56, %rdx
	movb	%dil, (%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L1304
.L1303:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1366
	addq	$72, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1359:
	.cfi_restore_state
	addl	$1, 8(%r12)
	jmp	.L1273
	.p2align 4,,10
	.p2align 3
.L1257:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L1367
.L1259:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L1258
	.p2align 4,,10
	.p2align 3
.L1263:
	movq	41088(%r13), %rbx
	cmpq	41096(%r13), %rbx
	je	.L1368
.L1265:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rbx)
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1260:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L1369
.L1262:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r12, (%rsi)
	jmp	.L1261
	.p2align 4,,10
	.p2align 3
.L1297:
	movq	41088(%r13), %rsi
	cmpq	%rsi, 41096(%r13)
	je	.L1370
.L1299:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r12, (%rsi)
	jmp	.L1298
	.p2align 4,,10
	.p2align 3
.L1287:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L1371
.L1289:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r12, (%rax)
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1357:
	movq	40(%r13), %rax
	addq	%r15, %rax
	cmpq	$67108864, %rax
	jle	.L1272
	movq	%rax, 40(%r13)
	jmp	.L1272
	.p2align 4,,10
	.p2align 3
.L1356:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L1270
	.p2align 4,,10
	.p2align 3
.L1274:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L1277
.L1360:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1278
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1279:
	cmpl	$1, %eax
	jne	.L1277
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1362:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1361:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1365:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1364:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1363:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1307
	.p2align 4,,10
	.p2align 3
.L1358:
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L1272
.L1278:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1279
.L1369:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1262
.L1368:
	movq	%r13, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L1265
.L1367:
	movq	%r13, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	jmp	.L1259
.L1370:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1299
.L1371:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1289
.L1366:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22705:
	.size	_ZN2v88internal18WasmInstanceObject3NewEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE, .-_ZN2v88internal18WasmInstanceObject3NewEPNS0_7IsolateENS0_6HandleINS0_16WasmModuleObjectEEE
	.section	.text._ZN2v88internal18WasmInstanceObject21InitElemSegmentArraysENS0_6HandleIS1_EENS2_INS0_16WasmModuleObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18WasmInstanceObject21InitElemSegmentArraysENS0_6HandleIS1_EENS2_INS0_16WasmModuleObjectEEE
	.type	_ZN2v88internal18WasmInstanceObject21InitElemSegmentArraysENS0_6HandleIS1_EENS2_INS0_16WasmModuleObjectEEE, @function
_ZN2v88internal18WasmInstanceObject21InitElemSegmentArraysENS0_6HandleIS1_EENS2_INS0_16WasmModuleObjectEEE:
.LFB22707:
	.cfi_startproc
	endbr64
	movabsq	$7905747460161236407, %rdx
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %r9
	movq	280(%r9), %rax
	movq	288(%r9), %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	imulq	%rdx, %rcx
	testq	%rcx, %rcx
	je	.L1372
	movq	(%rdi), %rdx
	movzbl	48(%rax), %eax
	movq	271(%rdx), %rdx
	movb	%al, (%rdx)
	movl	$56, %edx
	movl	$1, %eax
	cmpq	$1, %rcx
	jbe	.L1372
	.p2align 4,,10
	.p2align 3
.L1374:
	movq	(%rdi), %rsi
	movq	280(%r9), %r8
	movq	271(%rsi), %rsi
	movzbl	48(%r8,%rdx), %r8d
	addq	$56, %rdx
	movb	%r8b, (%rsi,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L1374
.L1372:
	ret
	.cfi_endproc
.LFE22707:
	.size	_ZN2v88internal18WasmInstanceObject21InitElemSegmentArraysENS0_6HandleIS1_EENS2_INS0_16WasmModuleObjectEEE, .-_ZN2v88internal18WasmInstanceObject21InitElemSegmentArraysENS0_6HandleIS1_EENS2_INS0_16WasmModuleObjectEEE
	.section	.text._ZN2v88internal18WasmInstanceObject13GetCallTargetEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18WasmInstanceObject13GetCallTargetEj
	.type	_ZN2v88internal18WasmInstanceObject13GetCallTargetEj, @function
_ZN2v88internal18WasmInstanceObject13GetCallTargetEj:
.LFB22708:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	135(%rax), %rdx
	movq	23(%rdx), %rdx
	movq	7(%rdx), %rdx
	movq	24(%rdx), %rdx
	movq	(%rdx), %rdi
	movq	208(%rdi), %rdx
	cmpl	%esi, 60(%rdx)
	jbe	.L1382
	movq	63(%rax), %rax
	movl	%esi, %esi
	movq	(%rax,%rsi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1382:
	jmp	_ZNK2v88internal4wasm12NativeModule24GetCallTargetForFunctionEj@PLT
	.cfi_endproc
.LFE22708:
	.size	_ZN2v88internal18WasmInstanceObject13GetCallTargetEj, .-_ZN2v88internal18WasmInstanceObject13GetCallTargetEj
	.section	.text._ZN2v88internal18WasmInstanceObject25IndirectFunctionTableSizeEPNS0_7IsolateENS0_6HandleIS1_EEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18WasmInstanceObject25IndirectFunctionTableSizeEPNS0_7IsolateENS0_6HandleIS1_EEj
	.type	_ZN2v88internal18WasmInstanceObject25IndirectFunctionTableSizeEPNS0_7IsolateENS0_6HandleIS1_EEj, @function
_ZN2v88internal18WasmInstanceObject25IndirectFunctionTableSizeEPNS0_7IsolateENS0_6HandleIS1_EEj:
.LFB22709:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	testl	%edx, %edx
	jne	.L1386
	movl	95(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1386:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	16(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	207(%rax), %rax
	movq	-1(%rdx,%rax), %rsi
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	je	.L1388
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movl	7(%rsi), %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1388:
	.cfi_restore_state
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L1394
.L1390:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rdi)
	movq	%rsi, (%rax)
	movl	7(%rsi), %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1394:
	.cfi_restore_state
	movq	%rsi, -16(%rbp)
	movq	%rdi, -8(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-16(%rbp), %rsi
	movq	-8(%rbp), %rdi
	jmp	.L1390
	.cfi_endproc
.LFE22709:
	.size	_ZN2v88internal18WasmInstanceObject25IndirectFunctionTableSizeEPNS0_7IsolateENS0_6HandleIS1_EEj, .-_ZN2v88internal18WasmInstanceObject25IndirectFunctionTableSizeEPNS0_7IsolateENS0_6HandleIS1_EEj
	.section	.text._ZN2v88internal18WasmInstanceObject16InitTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18WasmInstanceObject16InitTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj
	.type	_ZN2v88internal18WasmInstanceObject16InitTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj, @function
_ZN2v88internal18WasmInstanceObject16InitTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj:
.LFB22711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	16(%rbp), %eax
	testl	%eax, %eax
	jne	.L1398
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1398:
	.cfi_restore_state
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm15LoadElemSegmentEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEjjjjj@PLT
	.cfi_endproc
.LFE22711:
	.size	_ZN2v88internal18WasmInstanceObject16InitTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj, .-_ZN2v88internal18WasmInstanceObject16InitTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj
	.section	.text._ZN2v88internal18WasmInstanceObject23GetWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18WasmInstanceObject23GetWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi
	.type	_ZN2v88internal18WasmInstanceObject23GetWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi, @function
_ZN2v88internal18WasmInstanceObject23GetWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi:
.LFB22712:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	231(%rax), %rcx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37504(%rax), %rcx
	jne	.L1400
.L1402:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1400:
	leal	16(,%rdx,8), %eax
	cltq
	movq	-1(%rcx,%rax), %rsi
	cmpq	%rsi, 88(%rdi)
	je	.L1402
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	je	.L1403
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1403:
	.cfi_restore_state
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L1409
.L1404:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rdi)
	movq	%rsi, (%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1409:
	.cfi_restore_state
	movq	%rsi, -16(%rbp)
	movq	%rdi, -8(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-16(%rbp), %rsi
	movq	-8(%rbp), %rdi
	jmp	.L1404
	.cfi_endproc
.LFE22712:
	.size	_ZN2v88internal18WasmInstanceObject23GetWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi, .-_ZN2v88internal18WasmInstanceObject23GetWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.text._ZN2v88internal18WasmInstanceObject23SetWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_20WasmExternalFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18WasmInstanceObject23SetWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_20WasmExternalFunctionEEE
	.type	_ZN2v88internal18WasmInstanceObject23SetWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_20WasmExternalFunctionEEE, @function
_ZN2v88internal18WasmInstanceObject23SetWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_20WasmExternalFunctionEEE:
.LFB22717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	%rax, %rdx
	movq	231(%rax), %r12
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %r12
	je	.L1436
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	je	.L1416
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
.L1415:
	leal	16(,%r14,8), %eax
	movq	0(%r13), %r13
	cltq
	leaq	-1(%r12,%rax), %r14
	movq	%r13, (%r14)
	testb	$1, %r13b
	je	.L1410
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L1419
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L1419:
	testb	$24, %al
	je	.L1410
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1437
.L1410:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1416:
	.cfi_restore_state
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L1438
.L1417:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rdi)
	movq	%r12, (%rax)
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1436:
	movq	135(%rax), %rax
	movq	%rsi, %rbx
	xorl	%edx, %edx
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	movq	144(%rax), %rsi
	subq	136(%rax), %rsi
	sarq	$5, %rsi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rbx
	movq	(%rax), %r15
	movq	%rax, %r12
	leaq	231(%rbx), %rsi
	movq	%r15, 231(%rbx)
	testb	$1, %r15b
	je	.L1422
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L1413
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L1413:
	testb	$24, %al
	je	.L1422
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1439
	.p2align 4,,10
	.p2align 3
.L1422:
	movq	(%r12), %r12
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1437:
	addq	$24, %rsp
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1438:
	.cfi_restore_state
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rdi
	jmp	.L1417
	.p2align 4,,10
	.p2align 3
.L1439:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1422
	.cfi_endproc
.LFE22717:
	.size	_ZN2v88internal18WasmInstanceObject23SetWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_20WasmExternalFunctionEEE, .-_ZN2v88internal18WasmInstanceObject23SetWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_20WasmExternalFunctionEEE
	.section	.text._ZN2v88internal19WasmExceptionObject3NewEPNS0_7IsolateEPKNS0_9SignatureINS0_4wasm9ValueTypeEEENS0_6HandleINS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19WasmExceptionObject3NewEPNS0_7IsolateEPKNS0_9SignatureINS0_4wasm9ValueTypeEEENS0_6HandleINS0_10HeapObjectEEE
	.type	_ZN2v88internal19WasmExceptionObject3NewEPNS0_7IsolateEPKNS0_9SignatureINS0_4wasm9ValueTypeEEENS0_6HandleINS0_10HeapObjectEEE, @function
_ZN2v88internal19WasmExceptionObject3NewEPNS0_7IsolateEPKNS0_9SignatureINS0_4wasm9ValueTypeEEENS0_6HandleINS0_10HeapObjectEEE:
.LFB22739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	12464(%rdi), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1441
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1442:
	movq	1471(%rsi), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1444
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L1445:
	movl	8(%r14), %esi
	movl	$1, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movq	(%r14), %rdx
	movq	8(%r14), %rdi
	movq	%rax, %rbx
	movq	16(%r14), %rax
	leaq	(%rax,%rdx), %rsi
	addq	%rdi, %rdx
	addq	%rax, %rdx
	xorl	%eax, %eax
	cmpq	%rdx, %rsi
	je	.L1450
	.p2align 4,,10
	.p2align 3
.L1447:
	movzbl	(%rsi,%rax), %ecx
	movq	(%rbx), %rdx
	addq	$1, %rax
	movb	%cl, 14(%rax,%rdx)
	cmpq	%rax, %rdi
	jne	.L1447
.L1450:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	$1, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	(%rbx), %r14
	movq	(%rax), %r15
	movq	%rax, %r13
	movq	%r14, 23(%r15)
	leaq	23(%r15), %rsi
	testb	$1, %r14b
	je	.L1457
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1474
	testb	$24, %al
	je	.L1457
.L1479:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1475
	.p2align 4,,10
	.p2align 3
.L1457:
	movq	0(%r13), %r14
	movq	(%r12), %r12
	movq	%r12, 31(%r14)
	leaq	31(%r14), %r15
	testb	$1, %r12b
	je	.L1456
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1476
	testb	$24, %al
	je	.L1456
.L1478:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1477
.L1456:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1476:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1478
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1474:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L1479
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	41088(%r13), %r15
	cmpq	%r15, 41096(%r13)
	je	.L1480
.L1446:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L1445
	.p2align 4,,10
	.p2align 3
.L1441:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L1481
.L1443:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1475:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1477:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1456
.L1481:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1443
.L1480:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1446
	.cfi_endproc
.LFE22739:
	.size	_ZN2v88internal19WasmExceptionObject3NewEPNS0_7IsolateEPKNS0_9SignatureINS0_4wasm9ValueTypeEEENS0_6HandleINS0_10HeapObjectEEE, .-_ZN2v88internal19WasmExceptionObject3NewEPNS0_7IsolateEPKNS0_9SignatureINS0_4wasm9ValueTypeEEENS0_6HandleINS0_10HeapObjectEEE
	.section	.text._ZN2v88internal19WasmExceptionObject16IsSignatureEqualEPKNS0_9SignatureINS0_4wasm9ValueTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19WasmExceptionObject16IsSignatureEqualEPKNS0_9SignatureINS0_4wasm9ValueTypeEEE
	.type	_ZN2v88internal19WasmExceptionObject16IsSignatureEqualEPKNS0_9SignatureINS0_4wasm9ValueTypeEEE, @function
_ZN2v88internal19WasmExceptionObject16IsSignatureEqualEPKNS0_9SignatureINS0_4wasm9ValueTypeEEE:
.LFB22740:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	8(%rsi), %rcx
	movq	23(%rax), %rdx
	xorl	%eax, %eax
	cmpl	%ecx, 11(%rdx)
	jne	.L1482
	testl	%ecx, %ecx
	jle	.L1486
	movq	(%rsi), %rdi
	subl	$1, %ecx
	leaq	15(%rdx), %rax
	leaq	16(%rdx,%rcx), %rcx
	subq	%rdx, %rdi
	movq	16(%rsi), %rdx
	addq	%rdi, %rdx
	jmp	.L1484
	.p2align 4,,10
	.p2align 3
.L1489:
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L1486
.L1484:
	movzbl	-15(%rax,%rdx), %edi
	cmpb	%dil, (%rax)
	je	.L1489
	xorl	%eax, %eax
.L1482:
	ret
	.p2align 4,,10
	.p2align 3
.L1486:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE22740:
	.size	_ZN2v88internal19WasmExceptionObject16IsSignatureEqualEPKNS0_9SignatureINS0_4wasm9ValueTypeEEE, .-_ZN2v88internal19WasmExceptionObject16IsSignatureEqualEPKNS0_9SignatureINS0_4wasm9ValueTypeEEE
	.section	.text._ZNK2v88internal16WasmCapiFunction16IsSignatureEqualEPKNS0_9SignatureINS0_4wasm9ValueTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16WasmCapiFunction16IsSignatureEqualEPKNS0_9SignatureINS0_4wasm9ValueTypeEEE
	.type	_ZNK2v88internal16WasmCapiFunction16IsSignatureEqualEPKNS0_9SignatureINS0_4wasm9ValueTypeEEE, @function
_ZNK2v88internal16WasmCapiFunction16IsSignatureEqualEPKNS0_9SignatureINS0_4wasm9ValueTypeEEE:
.LFB22741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rsi), %r12
	movq	(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-48(%rbp), %rdi
	movq	23(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo23wasm_capi_function_dataEv@PLT
	leal	1(%r13,%r12), %ecx
	movq	31(%rax), %rdx
	xorl	%eax, %eax
	cmpl	%ecx, 11(%rdx)
	jne	.L1490
	leaq	15(%rdx), %r8
	testl	%r13d, %r13d
	jle	.L1497
	leal	-1(%r13), %ecx
	movq	16(%rbx), %rdi
	movl	$1, %eax
	addq	$2, %rcx
	jmp	.L1493
	.p2align 4,,10
	.p2align 3
.L1507:
	movslq	%eax, %rsi
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L1506
.L1493:
	movzbl	-1(%rdi,%rax), %esi
	cmpb	%sil, 14(%rax,%rdx)
	je	.L1507
	xorl	%eax, %eax
.L1490:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1508
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1506:
	.cfi_restore_state
	movslq	%esi, %rcx
.L1492:
	xorl	%eax, %eax
	cmpb	$0, 15(%rdx,%rcx)
	jne	.L1490
	leal	1(%rsi), %eax
	testl	%r12d, %r12d
	jle	.L1500
	movq	(%rbx), %rdi
	leal	-1(%r12), %ecx
	cltq
	leaq	17(%rdx,%rcx), %rcx
	addq	%r8, %rax
	subq	%rdx, %rdi
	addq	%rsi, %rcx
	movq	%rdi, %rdx
	subq	%rsi, %rdx
	addq	16(%rbx), %rdx
	jmp	.L1494
	.p2align 4,,10
	.p2align 3
.L1509:
	addq	$1, %rax
	cmpq	%rax, %rcx
	je	.L1500
.L1494:
	movzbl	-16(%rax,%rdx), %ebx
	cmpb	%bl, (%rax)
	je	.L1509
	xorl	%eax, %eax
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1500:
	movl	$1, %eax
	jmp	.L1490
.L1497:
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	jmp	.L1492
.L1508:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22741:
	.size	_ZNK2v88internal16WasmCapiFunction16IsSignatureEqualEPKNS0_9SignatureINS0_4wasm9ValueTypeEEE, .-_ZNK2v88internal16WasmCapiFunction16IsSignatureEqualEPKNS0_9SignatureINS0_4wasm9ValueTypeEEE
	.section	.rodata._ZN2v88internal20WasmExceptionPackage3NewEPNS0_7IsolateENS0_6HandleINS0_16WasmExceptionTagEEEi.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"!Object::SetProperty(isolate, exception, isolate->factory()->wasm_exception_tag_symbol(), exception_tag, StoreOrigin::kMaybeKeyed, Just(ShouldThrow::kThrowOnError)) .is_null()"
	.align 8
.LC18:
	.string	"!Object::SetProperty(isolate, exception, isolate->factory()->wasm_exception_values_symbol(), values, StoreOrigin::kMaybeKeyed, Just(ShouldThrow::kThrowOnError)) .is_null()"
	.section	.text._ZN2v88internal20WasmExceptionPackage3NewEPNS0_7IsolateENS0_6HandleINS0_16WasmExceptionTagEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20WasmExceptionPackage3NewEPNS0_7IsolateENS0_6HandleINS0_16WasmExceptionTagEEEi
	.type	_ZN2v88internal20WasmExceptionPackage3NewEPNS0_7IsolateENS0_6HandleINS0_16WasmExceptionTagEEEi, @function
_ZN2v88internal20WasmExceptionPackage3NewEPNS0_7IsolateENS0_6HandleINS0_16WasmExceptionTagEEEi:
.LFB22742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	xorl	%edx, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$353, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal7Factory19NewWasmRuntimeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdi
	leaq	3824(%r12), %rdx
	movl	$1, %r9d
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L1514
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	3832(%r12), %rdx
	movl	$1, %r9d
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L1515
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1514:
	.cfi_restore_state
	leaq	.LC17(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1515:
	leaq	.LC18(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22742:
	.size	_ZN2v88internal20WasmExceptionPackage3NewEPNS0_7IsolateENS0_6HandleINS0_16WasmExceptionTagEEEi, .-_ZN2v88internal20WasmExceptionPackage3NewEPNS0_7IsolateENS0_6HandleINS0_16WasmExceptionTagEEEi
	.section	.text._ZN2v88internal20WasmExceptionPackage15GetExceptionTagEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20WasmExceptionPackage15GetExceptionTagEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal20WasmExceptionPackage15GetExceptionTagEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal20WasmExceptionPackage15GetExceptionTagEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB22743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1532
.L1518:
	leaq	88(%rbx), %rax
.L1524:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1533
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1532:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1518
	movq	3824(%rdi), %rax
	movq	%rsi, %r12
	movl	$3, %edx
	leaq	3824(%rdi), %rsi
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L1520
	movl	11(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%edx, %edx
	andl	$3, %edx
.L1520:
	movabsq	$824633720832, %rax
	movl	%edx, -112(%rbp)
	movq	%rax, -100(%rbp)
	movq	3824(%rbx), %rax
	movq	%rbx, -88(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L1534
.L1521:
	movq	%r12, -64(%rbp)
	movq	%r12, -48(%rbp)
	leaq	-112(%rbp), %r12
	movq	%r12, %rdi
	movq	%rsi, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -56(%rbp)
	movq	$-1, -40(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -108(%rbp)
	jne	.L1522
	movq	-88(%rbp), %rax
	addq	$88, %rax
	jmp	.L1524
	.p2align 4,,10
	.p2align 3
.L1522:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L1524
	jmp	.L1518
	.p2align 4,,10
	.p2align 3
.L1534:
	movq	%rbx, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L1521
.L1533:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22743:
	.size	_ZN2v88internal20WasmExceptionPackage15GetExceptionTagEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal20WasmExceptionPackage15GetExceptionTagEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal20WasmExceptionPackage18GetExceptionValuesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20WasmExceptionPackage18GetExceptionValuesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal20WasmExceptionPackage18GetExceptionValuesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal20WasmExceptionPackage18GetExceptionValuesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB22744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1551
.L1537:
	leaq	88(%rbx), %rax
.L1543:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1552
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1551:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1537
	movq	3832(%rdi), %rax
	movq	%rsi, %r12
	movl	$3, %edx
	leaq	3832(%rdi), %rsi
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L1539
	movl	11(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%edx, %edx
	andl	$3, %edx
.L1539:
	movabsq	$824633720832, %rax
	movl	%edx, -112(%rbp)
	movq	%rax, -100(%rbp)
	movq	3832(%rbx), %rax
	movq	%rbx, -88(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L1553
.L1540:
	movq	%r12, -64(%rbp)
	movq	%r12, -48(%rbp)
	leaq	-112(%rbp), %r12
	movq	%r12, %rdi
	movq	%rsi, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -56(%rbp)
	movq	$-1, -40(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -108(%rbp)
	jne	.L1541
	movq	-88(%rbp), %rax
	addq	$88, %rax
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1541:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L1543
	jmp	.L1537
	.p2align 4,,10
	.p2align 3
.L1553:
	movq	%rbx, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L1540
.L1552:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22744:
	.size	_ZN2v88internal20WasmExceptionPackage18GetExceptionValuesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal20WasmExceptionPackage18GetExceptionValuesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal20WasmExceptionPackage14GetEncodedSizeEPKNS0_4wasm13WasmExceptionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20WasmExceptionPackage14GetEncodedSizeEPKNS0_4wasm13WasmExceptionE
	.type	_ZN2v88internal20WasmExceptionPackage14GetEncodedSizeEPKNS0_4wasm13WasmExceptionE, @function
_ZN2v88internal20WasmExceptionPackage14GetEncodedSizeEPKNS0_4wasm13WasmExceptionE:
.LFB22745:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1564
	movq	(%rax), %rdx
	movq	16(%rax), %rsi
	xorl	%r8d, %r8d
	leaq	(%rsi,%rdx), %rax
	addq	%rcx, %rdx
	leaq	.L1558(%rip), %rcx
	addq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L1563:
	cmpb	$9, (%rax)
	ja	.L1556
	movzbl	(%rax), %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal20WasmExceptionPackage14GetEncodedSizeEPKNS0_4wasm13WasmExceptionE,"a",@progbits
	.align 4
	.align 4
.L1558:
	.long	.L1556-.L1558
	.long	.L1561-.L1558
	.long	.L1560-.L1558
	.long	.L1561-.L1558
	.long	.L1560-.L1558
	.long	.L1559-.L1558
	.long	.L1557-.L1558
	.long	.L1557-.L1558
	.long	.L1556-.L1558
	.long	.L1557-.L1558
	.section	.text._ZN2v88internal20WasmExceptionPackage14GetEncodedSizeEPKNS0_4wasm13WasmExceptionE
	.p2align 4,,10
	.p2align 3
.L1557:
	addl	$1, %r8d
.L1562:
	addq	$1, %rax
	cmpq	%rsi, %rax
	jne	.L1563
.L1554:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1560:
	addl	$4, %r8d
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1561:
	addl	$2, %r8d
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1559:
	addl	$8, %r8d
	jmp	.L1562
.L1564:
	xorl	%r8d, %r8d
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1556:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22745:
	.size	_ZN2v88internal20WasmExceptionPackage14GetEncodedSizeEPKNS0_4wasm13WasmExceptionE, .-_ZN2v88internal20WasmExceptionPackage14GetEncodedSizeEPKNS0_4wasm13WasmExceptionE
	.section	.text._ZN2v88internal20WasmExportedFunction22IsWasmExportedFunctionENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20WasmExportedFunction22IsWasmExportedFunctionENS0_6ObjectE
	.type	_ZN2v88internal20WasmExportedFunction22IsWasmExportedFunctionENS0_6ObjectE, @function
_ZN2v88internal20WasmExportedFunction22IsWasmExportedFunctionENS0_6ObjectE:
.LFB22746:
	.cfi_startproc
	endbr64
	testb	$1, %dil
	jne	.L1569
.L1571:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1569:
	movq	-1(%rdi), %rax
	cmpw	$1105, 11(%rax)
	jne	.L1571
	movq	47(%rdi), %rax
	movl	43(%rax), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$8, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE22746:
	.size	_ZN2v88internal20WasmExportedFunction22IsWasmExportedFunctionENS0_6ObjectE, .-_ZN2v88internal20WasmExportedFunction22IsWasmExportedFunctionENS0_6ObjectE
	.section	.text._ZN2v88internal16WasmCapiFunction18IsWasmCapiFunctionENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmCapiFunction18IsWasmCapiFunctionENS0_6ObjectE
	.type	_ZN2v88internal16WasmCapiFunction18IsWasmCapiFunctionENS0_6ObjectE, @function
_ZN2v88internal16WasmCapiFunction18IsWasmCapiFunctionENS0_6ObjectE:
.LFB22747:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	notq	%rax
	andl	$1, %eax
	je	.L1573
.L1575:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1573:
	movq	-1(%rdi), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1575
	movq	23(%rdi), %rdx
	movq	7(%rdx), %rdx
	testb	$1, %dl
	jne	.L1577
	ret
	.p2align 4,,10
	.p2align 3
.L1577:
	movq	-1(%rdx), %rax
	cmpw	$104, 11(%rax)
	sete	%al
	ret
	.cfi_endproc
.LFE22747:
	.size	_ZN2v88internal16WasmCapiFunction18IsWasmCapiFunctionENS0_6ObjectE, .-_ZN2v88internal16WasmCapiFunction18IsWasmCapiFunctionENS0_6ObjectE
	.section	.text._ZN2v88internal16WasmCapiFunction3NewEPNS0_7IsolateEmNS0_6HandleINS0_7ForeignEEENS4_INS0_8PodArrayINS0_4wasm9ValueTypeEEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmCapiFunction3NewEPNS0_7IsolateEmNS0_6HandleINS0_7ForeignEEENS4_INS0_8PodArrayINS0_4wasm9ValueTypeEEEEE
	.type	_ZN2v88internal16WasmCapiFunction3NewEPNS0_7IsolateEmNS0_6HandleINS0_7ForeignEEENS4_INS0_8PodArrayINS0_4wasm9ValueTypeEEEEE, @function
_ZN2v88internal16WasmCapiFunction3NewEPNS0_7IsolateEmNS0_6HandleINS0_7ForeignEEENS4_INS0_8PodArrayINS0_4wasm9ValueTypeEEEEE:
.LFB22748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$104, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	$1, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	%r15, 7(%rax)
	movq	0(%r13), %r15
	movq	(%r14), %r14
	movq	%r14, 15(%r15)
	testb	$1, %r14b
	je	.L1593
	movq	%r14, %rcx
	leaq	15(%r15), %rsi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L1613
	testb	$24, %al
	je	.L1593
.L1621:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1614
	.p2align 4,,10
	.p2align 3
.L1593:
	movq	0(%r13), %r15
	movq	(%rbx), %r14
	movq	%r14, 31(%r15)
	leaq	31(%r15), %rsi
	testb	$1, %r14b
	je	.L1592
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1615
	testb	$24, %al
	je	.L1592
.L1620:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1616
	.p2align 4,,10
	.p2align 3
.L1592:
	movq	0(%r13), %r15
	movl	$166, %esi
	leaq	41184(%r12), %rdi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%rax, 23(%r15)
	movq	%rax, %r14
	leaq	23(%r15), %rsi
	testb	$1, %al
	je	.L1591
	movq	%rax, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1617
	testb	$24, %al
	je	.L1591
.L1619:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1618
	.p2align 4,,10
	.p2align 3
.L1591:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory40NewSharedFunctionInfoForWasmCapiFunctionENS0_6HandleINS0_20WasmCapiFunctionDataEEE@PLT
	movq	%rax, %r13
	movq	12464(%r12), %rax
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1588
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1589:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1617:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L1619
	jmp	.L1591
	.p2align 4,,10
	.p2align 3
.L1615:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L1620
	jmp	.L1592
	.p2align 4,,10
	.p2align 3
.L1613:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1621
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1588:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L1622
.L1590:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L1589
	.p2align 4,,10
	.p2align 3
.L1614:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1616:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1592
	.p2align 4,,10
	.p2align 3
.L1618:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1591
	.p2align 4,,10
	.p2align 3
.L1622:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1590
	.cfi_endproc
.LFE22748:
	.size	_ZN2v88internal16WasmCapiFunction3NewEPNS0_7IsolateEmNS0_6HandleINS0_7ForeignEEENS4_INS0_8PodArrayINS0_4wasm9ValueTypeEEEEE, .-_ZN2v88internal16WasmCapiFunction3NewEPNS0_7IsolateEmNS0_6HandleINS0_7ForeignEEENS4_INS0_8PodArrayINS0_4wasm9ValueTypeEEEEE
	.section	.text._ZN2v88internal20WasmExportedFunction8instanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20WasmExportedFunction8instanceEv
	.type	_ZN2v88internal20WasmExportedFunction8instanceEv, @function
_ZN2v88internal20WasmExportedFunction8instanceEv:
.LFB22749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-16(%rbp), %rdi
	movq	23(%rax), %rax
	movq	%rax, -16(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo27wasm_exported_function_dataEv@PLT
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	movq	15(%rax), %rax
	jne	.L1626
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1626:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22749:
	.size	_ZN2v88internal20WasmExportedFunction8instanceEv, .-_ZN2v88internal20WasmExportedFunction8instanceEv
	.section	.text._ZN2v88internal20WasmExportedFunction14function_indexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20WasmExportedFunction14function_indexEv
	.type	_ZN2v88internal20WasmExportedFunction14function_indexEv, @function
_ZN2v88internal20WasmExportedFunction14function_indexEv:
.LFB22750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-16(%rbp), %rdi
	movq	23(%rax), %rax
	movq	%rax, -16(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo27wasm_exported_function_dataEv@PLT
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	movslq	35(%rax), %rax
	jne	.L1630
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1630:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22750:
	.size	_ZN2v88internal20WasmExportedFunction14function_indexEv, .-_ZN2v88internal20WasmExportedFunction14function_indexEv
	.section	.rodata._ZN2v88internal20WasmExportedFunction3NewEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS4_INS0_4CodeEEE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"%d"
	.section	.text._ZN2v88internal20WasmExportedFunction3NewEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS4_INS0_4CodeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20WasmExportedFunction3NewEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS4_INS0_4CodeEEE
	.type	_ZN2v88internal20WasmExportedFunction3NewEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS4_INS0_4CodeEEE, @function
_ZN2v88internal20WasmExportedFunction3NewEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS4_INS0_4CodeEEE:
.LFB22751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movabsq	$-4294967296, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movl	%edx, -168(%rbp)
	movl	%ecx, -172(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	movq	208(%rdi), %rax
	cmpl	%edx, 60(%rax)
	jle	.L1683
.L1632:
	movl	$107, %esi
	movq	%rbx, %rdi
	movl	$1, %edx
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE@PLT
	movq	(%r14), %r14
	movq	(%rax), %rdi
	movq	%rax, %r12
	movq	%r14, 7(%rdi)
	leaq	7(%rdi), %rsi
	testb	$1, %r14b
	je	.L1661
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -184(%rbp)
	testl	$262144, %eax
	jne	.L1684
	testb	$24, %al
	je	.L1661
.L1693:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1685
	.p2align 4,,10
	.p2align 3
.L1661:
	movq	(%r12), %rdi
	movq	0(%r13), %r14
	movq	%r14, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %r14b
	je	.L1660
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -184(%rbp)
	testl	$262144, %eax
	jne	.L1686
	testb	$24, %al
	je	.L1660
.L1692:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1687
	.p2align 4,,10
	.p2align 3
.L1660:
	movq	(%r12), %rax
	movq	%r15, 23(%rax)
	movq	-168(%rbp), %rdx
	movq	(%r12), %rax
	salq	$32, %rdx
	movq	%rdx, 31(%rax)
	movq	(%r12), %rax
	movq	$0, 39(%rax)
	movq	(%r12), %rax
	movq	$0, 47(%rax)
	movq	(%r12), %rax
	movq	$0, 55(%rax)
	movq	0(%r13), %rax
	movq	135(%rax), %rsi
	movq	23(%rsi), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	cmpb	$0, 392(%rax)
	jne	.L1688
.L1640:
	movl	-168(%rbp), %ecx
	leaq	-80(%rbp), %rdi
	movl	$16, %esi
	xorl	%eax, %eax
	leaq	.LC19(%rip), %rdx
	movq	%rdi, -96(%rbp)
	leaq	-160(%rbp), %r15
	movq	$16, -88(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-96(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	cltq
	movq	%rdx, -160(%rbp)
	xorl	%edx, %edx
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1689
.L1647:
	movq	0(%r13), %rax
	xorl	%ecx, %ecx
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	movzbl	392(%rax), %eax
	cmpb	$1, %al
	je	.L1648
	cmpb	$2, %al
	je	.L1649
	testb	%al, %al
	je	.L1690
.L1650:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15NewFunctionArgs7ForWasmENS0_6HandleINS0_6StringEEENS2_INS0_24WasmExportedFunctionDataEEENS2_INS0_3MapEEE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE@PLT
	movq	(%rax), %rdx
	movq	23(%rdx), %rdx
	movl	-172(%rbp), %ebx
	movw	%bx, 39(%rdx)
	movq	(%rax), %rdx
	movq	23(%rdx), %rdx
	movw	%bx, 41(%rdx)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1691
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1688:
	.cfi_restore_state
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1641
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L1642:
	movl	-168(%rbp), %edx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	leaq	-160(%rbp), %r15
	call	_ZN2v88internal16WasmModuleObject21GetFunctionNameOrNullEPNS0_7IsolateENS0_6HandleIS1_EEj
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L1647
	jmp	.L1640
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	%r14, %rdx
	movq	%rsi, -200(%rbp)
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-184(%rbp), %rcx
	movq	-200(%rbp), %rsi
	movq	-192(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1692
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1684:
	movq	%r14, %rdx
	movq	%rsi, -200(%rbp)
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-184(%rbp), %rcx
	movq	-200(%rbp), %rsi
	movq	-192(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1693
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1641:
	movq	41088(%rbx), %r8
	cmpq	41096(%rbx), %r8
	je	.L1694
.L1643:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r8)
	jmp	.L1642
	.p2align 4,,10
	.p2align 3
.L1683:
	movl	%edx, %esi
	call	_ZNK2v88internal4wasm12NativeModule18GetJumpTableOffsetEj@PLT
	movq	%rax, %r15
	salq	$32, %r15
	jmp	.L1632
	.p2align 4,,10
	.p2align 3
.L1648:
	movq	12464(%rbx), %rax
	movq	39(%rax), %rax
	movq	1247(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1657
.L1679:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
	jmp	.L1650
	.p2align 4,,10
	.p2align 3
.L1649:
	movq	12464(%rbx), %rax
	movq	39(%rax), %rax
	movq	1279(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L1679
.L1657:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L1695
.L1659:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L1650
	.p2align 4,,10
	.p2align 3
.L1690:
	movq	12464(%rbx), %rax
	movq	39(%rax), %rax
	movq	1463(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1657
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
	jmp	.L1650
	.p2align 4,,10
	.p2align 3
.L1685:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1687:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1695:
	movq	%rbx, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1659
	.p2align 4,,10
	.p2align 3
.L1694:
	movq	%rbx, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1689:
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1691:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22751:
	.size	_ZN2v88internal20WasmExportedFunction3NewEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS4_INS0_4CodeEEE, .-_ZN2v88internal20WasmExportedFunction3NewEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS4_INS0_4CodeEEE
	.section	.text._ZN2v88internal18WasmInstanceObject31GetOrCreateWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18WasmInstanceObject31GetOrCreateWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi
	.type	_ZN2v88internal18WasmInstanceObject31GetOrCreateWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi, @function
_ZN2v88internal18WasmInstanceObject31GetOrCreateWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi:
.LFB22716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	%rax, %rcx
	movq	231(%rax), %rdx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	-37504(%rcx), %rdx
	jne	.L1726
.L1697:
	movq	41112(%r12), %rdi
	movq	135(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1724
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L1705:
	movq	23(%rsi), %rax
	movslq	%r14d, %rbx
	salq	$5, %rbx
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rdi
	addq	136(%rdi), %rbx
	movzbl	24(%rbx), %edx
	movq	(%rbx), %rsi
	call	_ZN2v88internal4wasm21GetExportWrapperIndexEPKNS1_10WasmModuleEPKNS0_9SignatureINS1_9ValueTypeEEEb@PLT
	movq	(%r15), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	31(%rdx), %rdx
	leaq	-1(%rax), %rsi
	movq	%rsi, -56(%rbp)
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1707
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
.L1708:
	testb	$1, %sil
	jne	.L1727
.L1710:
	movzbl	24(%rbx), %edx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm30JSToWasmWrapperCompilationUnit22CompileJSToWasmWrapperEPNS0_7IsolateEPNS0_9SignatureINS1_9ValueTypeEEEb@PLT
	movq	%rax, %r8
	movq	(%r15), %rax
	movq	-56(%rbp), %r15
	movq	(%r8), %rdx
	movq	31(%rax), %rdi
	addq	%rdi, %r15
	movq	%rdx, (%r15)
	testb	$1, %dl
	je	.L1714
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L1715
	movq	%r15, %rsi
	movq	%r8, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
.L1715:
	testb	$24, %al
	je	.L1714
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1728
.L1714:
	movq	(%rbx), %rax
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	call	_ZN2v88internal20WasmExportedFunction3NewEPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEiiNS4_INS0_4CodeEEE
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, %rbx
	call	_ZN2v88internal18WasmInstanceObject23SetWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_20WasmExternalFunctionEEE
	addq	$40, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1726:
	.cfi_restore_state
	leal	16(,%r14,8), %ecx
	movslq	%ecx, %rcx
	movq	-1(%rdx,%rcx), %rsi
	cmpq	88(%rdi), %rsi
	je	.L1697
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1699
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	testq	%rax, %rax
	je	.L1729
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1699:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1730
.L1701:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1724:
	.cfi_restore_state
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L1731
.L1706:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L1705
	.p2align 4,,10
	.p2align 3
.L1707:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L1732
.L1709:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L1708
	.p2align 4,,10
	.p2align 3
.L1727:
	movq	-1(%rsi), %rax
	cmpw	$69, 11(%rax)
	jne	.L1710
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L1728:
	movq	%r15, %rsi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r8
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L1730:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1701
	.p2align 4,,10
	.p2align 3
.L1731:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1732:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1709
.L1729:
	movq	0(%r13), %rax
	jmp	.L1697
	.cfi_endproc
.LFE22716:
	.size	_ZN2v88internal18WasmInstanceObject31GetOrCreateWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi, .-_ZN2v88internal18WasmInstanceObject31GetOrCreateWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.text._ZN2v88internal20WasmExportedFunction17GetWasmCallTargetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20WasmExportedFunction17GetWasmCallTargetEv
	.type	_ZN2v88internal20WasmExportedFunction17GetWasmCallTargetEv, @function
_ZN2v88internal20WasmExportedFunction17GetWasmCallTargetEv:
.LFB22752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r12, %rdi
	movq	23(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo27wasm_exported_function_dataEv@PLT
	movq	%r12, %rdi
	movq	15(%rax), %r13
	movq	(%rbx), %rax
	movq	23(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo27wasm_exported_function_dataEv@PLT
	movslq	35(%rax), %rsi
	movq	135(%r13), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	movq	208(%rdi), %rax
	cmpl	60(%rax), %esi
	jnb	.L1734
	movq	63(%r13), %rax
	movl	%esi, %esi
	movq	(%rax,%rsi,8), %rax
.L1733:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1738
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1734:
	.cfi_restore_state
	call	_ZNK2v88internal4wasm12NativeModule24GetCallTargetForFunctionEj@PLT
	jmp	.L1733
.L1738:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22752:
	.size	_ZN2v88internal20WasmExportedFunction17GetWasmCallTargetEv, .-_ZN2v88internal20WasmExportedFunction17GetWasmCallTargetEv
	.section	.text._ZN2v88internal20WasmExportedFunction3sigEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20WasmExportedFunction3sigEv
	.type	_ZN2v88internal20WasmExportedFunction3sigEv, @function
_ZN2v88internal20WasmExportedFunction3sigEv:
.LFB22753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	movq	23(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo27wasm_exported_function_dataEv@PLT
	movq	%r13, %rdi
	movslq	35(%rax), %rbx
	movq	(%r12), %rax
	movq	23(%rax), %rax
	salq	$5, %rbx
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo27wasm_exported_function_dataEv@PLT
	movq	15(%rax), %rax
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	addq	136(%rax), %rbx
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	movq	(%rbx), %rax
	jne	.L1742
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1742:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22753:
	.size	_ZN2v88internal20WasmExportedFunction3sigEv, .-_ZN2v88internal20WasmExportedFunction3sigEv
	.section	.text._ZN2v88internal14WasmJSFunction16IsWasmJSFunctionENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14WasmJSFunction16IsWasmJSFunctionENS0_6ObjectE
	.type	_ZN2v88internal14WasmJSFunction16IsWasmJSFunctionENS0_6ObjectE, @function
_ZN2v88internal14WasmJSFunction16IsWasmJSFunctionENS0_6ObjectE:
.LFB22754:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	notq	%rax
	andl	$1, %eax
	je	.L1744
.L1746:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1744:
	movq	-1(%rdi), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1746
	movq	23(%rdi), %rdx
	movq	7(%rdx), %rdx
	testb	$1, %dl
	jne	.L1748
	ret
	.p2align 4,,10
	.p2align 3
.L1748:
	movq	-1(%rdx), %rax
	cmpw	$109, 11(%rax)
	sete	%al
	ret
	.cfi_endproc
.LFE22754:
	.size	_ZN2v88internal14WasmJSFunction16IsWasmJSFunctionENS0_6ObjectE, .-_ZN2v88internal14WasmJSFunction16IsWasmJSFunctionENS0_6ObjectE
	.section	.text._ZN2v88internal15WasmTableObject14IsValidElementEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15WasmTableObject14IsValidElementEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal15WasmTableObject14IsValidElementEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE, @function
_ZN2v88internal15WasmTableObject14IsValidElementEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE:
.LFB22646:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	cmpb	$6, 51(%rax)
	je	.L1758
	movq	(%rdx), %r8
	movq	%rdx, %rcx
	cmpq	%r8, 104(%rdi)
	je	.L1758
	testb	$1, %r8b
	jne	.L1760
.L1754:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal14WasmJSFunction16IsWasmJSFunctionENS0_6ObjectE
	testb	%al, %al
	je	.L1761
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1760:
	.cfi_restore 6
	movq	-1(%r8), %rax
	cmpw	$1105, 11(%rax)
	jne	.L1754
	movq	47(%r8), %rax
	movl	43(%rax), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$8, %eax
	jne	.L1754
.L1758:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1761:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movq	(%rcx), %rdi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal16WasmCapiFunction18IsWasmCapiFunctionENS0_6ObjectE
	.cfi_endproc
.LFE22646:
	.size	_ZN2v88internal15WasmTableObject14IsValidElementEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE, .-_ZN2v88internal15WasmTableObject14IsValidElementEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal15WasmTableObject3GetEPNS0_7IsolateENS0_6HandleIS1_EEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15WasmTableObject3GetEPNS0_7IsolateENS0_6HandleIS1_EEj
	.type	_ZN2v88internal15WasmTableObject3GetEPNS0_7IsolateENS0_6HandleIS1_EEj, @function
_ZN2v88internal15WasmTableObject3GetEPNS0_7IsolateENS0_6HandleIS1_EEj:
.LFB22648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	(%rsi), %rax
	movq	41112(%rdi), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1763
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L1764:
	leal	16(,%rbx,8), %edx
	movslq	%edx, %rdx
	movq	-1(%rsi,%rdx), %rsi
	movq	41112(%r12), %rdi
	leaq	-1(%rdx), %r13
	testq	%rdi, %rdi
	je	.L1766
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L1767:
	movq	(%r14), %rax
	cmpb	$6, 51(%rax)
	jne	.L1769
.L1786:
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1769:
	.cfi_restore_state
	movq	(%rbx), %rdi
	testb	$1, %dil
	jne	.L1795
.L1772:
	call	_ZN2v88internal14WasmJSFunction16IsWasmJSFunctionENS0_6ObjectE
	testb	%al, %al
	jne	.L1786
	movq	(%rbx), %rdi
	call	_ZN2v88internal16WasmCapiFunction18IsWasmCapiFunctionENS0_6ObjectE
	testb	%al, %al
	jne	.L1786
	movq	(%rbx), %rax
	cmpq	%rax, 104(%r12)
	je	.L1786
	movq	41112(%r12), %rdi
	movq	7(%rax), %r14
	testq	%rdi, %rdi
	je	.L1780
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1781:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movslq	19(%rax), %rdx
	call	_ZN2v88internal18WasmInstanceObject31GetOrCreateWasmExternalFunctionEPNS0_7IsolateENS0_6HandleIS1_EEi
	movq	(%r15), %r14
	movq	(%rax), %r12
	movq	%rax, %rbx
	addq	%r14, %r13
	movq	%r12, 0(%r13)
	testb	$1, %r12b
	je	.L1786
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	je	.L1784
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
.L1784:
	testb	$24, %al
	je	.L1786
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1786
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1786
	.p2align 4,,10
	.p2align 3
.L1766:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L1796
.L1768:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L1767
	.p2align 4,,10
	.p2align 3
.L1763:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L1797
.L1765:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L1764
	.p2align 4,,10
	.p2align 3
.L1795:
	movq	-1(%rdi), %rax
	cmpw	$1105, 11(%rax)
	jne	.L1772
	movq	47(%rdi), %rax
	movl	43(%rax), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$8, %eax
	je	.L1786
	jmp	.L1772
	.p2align 4,,10
	.p2align 3
.L1797:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1765
	.p2align 4,,10
	.p2align 3
.L1796:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L1768
.L1780:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1798
.L1782:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L1781
.L1798:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1782
	.cfi_endproc
.LFE22648:
	.size	_ZN2v88internal15WasmTableObject3GetEPNS0_7IsolateENS0_6HandleIS1_EEj, .-_ZN2v88internal15WasmTableObject3GetEPNS0_7IsolateENS0_6HandleIS1_EEj
	.section	.text._ZN2v88internal15WasmTableObject21GetFunctionTableEntryEPNS0_7IsolateENS0_6HandleIS1_EEiPbS6_PNS0_11MaybeHandleINS0_18WasmInstanceObjectEEEPiPNS7_INS0_14WasmJSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15WasmTableObject21GetFunctionTableEntryEPNS0_7IsolateENS0_6HandleIS1_EEiPbS6_PNS0_11MaybeHandleINS0_18WasmInstanceObjectEEEPiPNS7_INS0_14WasmJSFunctionEEE
	.type	_ZN2v88internal15WasmTableObject21GetFunctionTableEntryEPNS0_7IsolateENS0_6HandleIS1_EEiPbS6_PNS0_11MaybeHandleINS0_18WasmInstanceObjectEEEPiPNS7_INS0_14WasmJSFunctionEEE, @function
_ZN2v88internal15WasmTableObject21GetFunctionTableEntryEPNS0_7IsolateENS0_6HandleIS1_EEiPbS6_PNS0_11MaybeHandleINS0_18WasmInstanceObjectEEEPiPNS7_INS0_14WasmJSFunctionEEE:
.LFB22657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	16(%rbp), %rax
	movq	24(%rbp), %r14
	movq	%rax, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$1, (%rcx)
	movq	(%rsi), %rdx
	leal	16(,%r10,8), %eax
	cltq
	movq	23(%rdx), %rdx
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1800
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-80(%rbp), %r8
	movq	(%rax), %rsi
	movq	%rax, %r12
.L1801:
	movq	104(%rbx), %rax
	cmpq	%rsi, %rax
	sete	(%r8)
	jne	.L1821
.L1799:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1822
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1821:
	.cfi_restore_state
	movq	(%r12), %rdi
	testb	$1, %dil
	jne	.L1823
.L1805:
	call	_ZN2v88internal14WasmJSFunction16IsWasmJSFunctionENS0_6ObjectE
	testb	%al, %al
	jne	.L1824
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L1813
.L1814:
	movb	$0, (%r15)
	jmp	.L1799
	.p2align 4,,10
	.p2align 3
.L1800:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L1825
.L1802:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L1801
	.p2align 4,,10
	.p2align 3
.L1813:
	movq	-1(%rax), %rdx
	cmpw	$102, 11(%rdx)
	jne	.L1814
	movq	41112(%rbx), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1815
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1816:
	movq	%rax, 0(%r13)
	movq	(%r12), %rax
	movq	-72(%rbp), %rcx
	movslq	19(%rax), %rax
	movl	%eax, (%rcx)
	movq	$0, (%r14)
	jmp	.L1799
	.p2align 4,,10
	.p2align 3
.L1823:
	movq	-1(%rdi), %rax
	cmpw	$1105, 11(%rax)
	jne	.L1805
	movq	47(%rdi), %rax
	movl	43(%rax), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$8, %eax
	jne	.L1805
	movq	23(%rdi), %rax
	leaq	-64(%rbp), %r15
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo27wasm_exported_function_dataEv@PLT
	movq	41112(%rbx), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1819
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1810:
	movq	%rax, 0(%r13)
	movq	(%r12), %rax
	movq	%r15, %rdi
	movq	23(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo27wasm_exported_function_dataEv@PLT
	movq	-72(%rbp), %rcx
	movslq	35(%rax), %rax
	movl	%eax, (%rcx)
	movq	$0, (%r14)
	jmp	.L1799
	.p2align 4,,10
	.p2align 3
.L1824:
	movq	$0, 0(%r13)
	movq	%r12, (%r14)
	jmp	.L1799
	.p2align 4,,10
	.p2align 3
.L1825:
	movq	%rbx, %rdi
	movq	%r8, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1802
	.p2align 4,,10
	.p2align 3
.L1819:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1826
.L1811:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1810
	.p2align 4,,10
	.p2align 3
.L1826:
	movq	%rbx, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	jmp	.L1811
	.p2align 4,,10
	.p2align 3
.L1815:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1827
.L1817:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1816
.L1827:
	movq	%rbx, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	jmp	.L1817
.L1822:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22657:
	.size	_ZN2v88internal15WasmTableObject21GetFunctionTableEntryEPNS0_7IsolateENS0_6HandleIS1_EEiPbS6_PNS0_11MaybeHandleINS0_18WasmInstanceObjectEEEPiPNS7_INS0_14WasmJSFunctionEEE, .-_ZN2v88internal15WasmTableObject21GetFunctionTableEntryEPNS0_7IsolateENS0_6HandleIS1_EEiPbS6_PNS0_11MaybeHandleINS0_18WasmInstanceObjectEEEPiPNS7_INS0_14WasmJSFunctionEEE
	.section	.text._ZN2v88internal14WasmJSFunction3NewEPNS0_7IsolateEPNS0_9SignatureINS0_4wasm9ValueTypeEEENS0_6HandleINS0_10JSReceiverEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14WasmJSFunction3NewEPNS0_7IsolateEPNS0_9SignatureINS0_4wasm9ValueTypeEEENS0_6HandleINS0_10JSReceiverEEE
	.type	_ZN2v88internal14WasmJSFunction3NewEPNS0_7IsolateEPNS0_9SignatureINS0_4wasm9ValueTypeEEENS0_6HandleINS0_10JSReceiverEEE, @function
_ZN2v88internal14WasmJSFunction3NewEPNS0_7IsolateEPNS0_9SignatureINS0_4wasm9ValueTypeEEENS0_6HandleINS0_10JSReceiverEEE:
.LFB22755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$1, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	leaq	(%r14,%rax), %rbx
	movq	%rax, -136(%rbp)
	movl	%ebx, %esi
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, -144(%rbp)
	testl	%ebx, %ebx
	jle	.L1829
	movq	(%rax), %rax
	movq	16(%r15), %rsi
	movslq	%ebx, %rdx
	leaq	15(%rax), %rdi
	movq	%rax, -152(%rbp)
	call	memcpy@PLT
.L1829:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20CompileJSToJSWrapperEPNS0_7IsolateEPNS0_9SignatureINS0_4wasm9ValueTypeEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1867
	movl	$1, %edx
	movl	$109, %esi
	movq	%r12, %rdi
	salq	$32, %r14
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	%r14, 23(%rax)
	movq	-136(%rbp), %rdx
	movq	(%r15), %rax
	salq	$32, %rdx
	movq	%rdx, 31(%rax)
	movq	-144(%rbp), %rax
	movq	(%r15), %rdi
	movq	(%rax), %r14
	leaq	39(%rdi), %rsi
	movq	%r14, 39(%rdi)
	testb	$1, %r14b
	je	.L1846
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -144(%rbp)
	testl	$262144, %eax
	jne	.L1868
	testb	$24, %al
	je	.L1846
.L1878:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1869
	.p2align 4,,10
	.p2align 3
.L1846:
	movq	(%r15), %rdi
	movq	0(%r13), %r14
	movq	%r14, 7(%rdi)
	leaq	7(%rdi), %rsi
	testb	$1, %r14b
	je	.L1845
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -144(%rbp)
	testl	$262144, %eax
	jne	.L1870
	testb	$24, %al
	je	.L1845
.L1877:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1871
	.p2align 4,,10
	.p2align 3
.L1845:
	movq	(%r15), %rdi
	movq	(%rbx), %r14
	movq	%r14, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %r14b
	je	.L1844
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1872
	testb	$24, %al
	je	.L1844
.L1879:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1873
	.p2align 4,,10
	.p2align 3
.L1844:
	movq	0(%r13), %rax
	leaq	2560(%r12), %r14
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	je	.L1874
.L1840:
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	1463(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1841
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L1842:
	leaq	-128(%rbp), %r13
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal15NewFunctionArgs7ForWasmENS0_6HandleINS0_6StringEEENS2_INS0_18WasmJSFunctionDataEEENS2_INS0_3MapEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE@PLT
	movq	(%rax), %rdx
	movq	23(%rdx), %rdx
	movzwl	-136(%rbp), %ebx
	movw	%bx, 41(%rdx)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1875
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1841:
	.cfi_restore_state
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L1876
.L1843:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L1842
	.p2align 4,,10
	.p2align 3
.L1870:
	movq	%r14, %rdx
	movq	%rsi, -160(%rbp)
	movq	%rdi, -152(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-144(%rbp), %rcx
	movq	-160(%rbp), %rsi
	movq	-152(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1877
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L1868:
	movq	%r14, %rdx
	movq	%rsi, -160(%rbp)
	movq	%rdi, -152(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-144(%rbp), %rcx
	movq	-160(%rbp), %rsi
	movq	-152(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1878
	jmp	.L1846
	.p2align 4,,10
	.p2align 3
.L1872:
	movq	%r14, %rdx
	movq	%rsi, -152(%rbp)
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	testb	$24, %al
	jne	.L1879
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L1871:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L1873:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L1869:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1846
	.p2align 4,,10
	.p2align 3
.L1874:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSFunction7GetNameENS0_6HandleIS1_EE@PLT
	movq	%rax, %r14
	jmp	.L1840
	.p2align 4,,10
	.p2align 3
.L1867:
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1876:
	movq	%r12, %rdi
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1843
.L1875:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22755:
	.size	_ZN2v88internal14WasmJSFunction3NewEPNS0_7IsolateEPNS0_9SignatureINS0_4wasm9ValueTypeEEENS0_6HandleINS0_10JSReceiverEEE, .-_ZN2v88internal14WasmJSFunction3NewEPNS0_7IsolateEPNS0_9SignatureINS0_4wasm9ValueTypeEEENS0_6HandleINS0_10JSReceiverEEE
	.section	.text._ZNK2v88internal14WasmJSFunction11GetCallableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14WasmJSFunction11GetCallableEv
	.type	_ZNK2v88internal14WasmJSFunction11GetCallableEv, @function
_ZNK2v88internal14WasmJSFunction11GetCallableEv:
.LFB22756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-16(%rbp), %rdi
	movq	23(%rax), %rax
	movq	%rax, -16(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo21wasm_js_function_dataEv@PLT
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	movq	7(%rax), %rax
	jne	.L1883
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1883:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22756:
	.size	_ZNK2v88internal14WasmJSFunction11GetCallableEv, .-_ZNK2v88internal14WasmJSFunction11GetCallableEv
	.section	.text._ZN2v88internal14WasmJSFunction12GetSignatureEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14WasmJSFunction12GetSignatureEPNS0_4ZoneE
	.type	_ZN2v88internal14WasmJSFunction12GetSignatureEPNS0_4ZoneE, @function
_ZN2v88internal14WasmJSFunction12GetSignatureEPNS0_4ZoneE:
.LFB22757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-48(%rbp), %rdi
	movq	23(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo21wasm_js_function_dataEv@PLT
	movq	16(%r12), %r13
	movq	%rax, %rbx
	movq	39(%rax), %rax
	movslq	11(%rax), %rdx
	movq	24(%r12), %rax
	leaq	7(%rdx), %rsi
	subq	%r13, %rax
	andq	$-8, %rsi
	cmpq	%rax, %rsi
	ja	.L1892
	addq	%r13, %rsi
	movq	%rsi, 16(%r12)
.L1886:
	testq	%rdx, %rdx
	jle	.L1887
	movq	39(%rbx), %rax
	movq	%r13, %rdi
	leaq	15(%rax), %rsi
	call	memcpy@PLT
.L1887:
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	movslq	27(%rbx), %r14
	movslq	35(%rbx), %rbx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L1893
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%r12)
.L1889:
	movq	%r14, (%rax)
	movq	%rbx, 8(%rax)
	movq	%r13, 16(%rax)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1894
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1892:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1893:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1889
.L1894:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22757:
	.size	_ZN2v88internal14WasmJSFunction12GetSignatureEPNS0_4ZoneE, .-_ZN2v88internal14WasmJSFunction12GetSignatureEPNS0_4ZoneE
	.section	.rodata._ZN2v88internal18WasmInstanceObject29ImportWasmJSFunctionIntoTableEPNS0_7IsolateENS0_6HandleIS1_EEiiNS4_INS0_14WasmJSFunctionEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"../deps/v8/src/wasm/wasm-objects.cc:1939"
	.section	.text._ZN2v88internal18WasmInstanceObject29ImportWasmJSFunctionIntoTableEPNS0_7IsolateENS0_6HandleIS1_EEiiNS4_INS0_14WasmJSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18WasmInstanceObject29ImportWasmJSFunctionIntoTableEPNS0_7IsolateENS0_6HandleIS1_EEiiNS4_INS0_14WasmJSFunctionEEE
	.type	_ZN2v88internal18WasmInstanceObject29ImportWasmJSFunctionIntoTableEPNS0_7IsolateENS0_6HandleIS1_EEiiNS4_INS0_14WasmJSFunctionEEE, @function
_ZN2v88internal18WasmInstanceObject29ImportWasmJSFunctionIntoTableEPNS0_7IsolateENS0_6HandleIS1_EEiiNS4_INS0_14WasmJSFunctionEEE:
.LFB22718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$440, %rsp
	movl	%ecx, -452(%rbp)
	movq	41136(%rdi), %rsi
	movq	%r14, %rdi
	movl	%edx, -448(%rbp)
	leaq	.LC20(%rip), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -472(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	(%r15), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal14WasmJSFunction12GetSignatureEPNS0_4ZoneE
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	%r14, %rsi
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rdi
	addq	$328, %rdi
	call	_ZNK2v88internal4wasm12SignatureMap4FindERKNS0_9SignatureINS1_9ValueTypeEEE@PLT
	movq	%r13, %rdi
	movl	%eax, -444(%rbp)
	movq	(%r15), %rax
	movq	23(%rax), %rax
	movq	%rax, -192(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo21wasm_js_function_dataEv@PLT
	movq	41112(%r12), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1896
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -440(%rbp)
.L1897:
	leaq	-256(%rbp), %rax
	xorl	%r15d, %r15d
	movq	%rax, %rdi
	movq	%rax, -464(%rbp)
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movl	-444(%rbp), %edx
	testl	%edx, %edx
	jns	.L1936
.L1899:
	movq	-440(%rbp), %rdx
	movq	%r12, %rdi
	movl	$1, %ecx
	movq	%rbx, %rsi
	call	_ZN2v88internal7Factory9NewTuple2ENS0_6HandleINS0_6ObjectEEES4_NS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	movl	-448(%rbp), %eax
	testl	%eax, %eax
	je	.L1937
	movq	$0, -192(%rbp)
	movq	(%rbx), %rdx
	movq	%rdx, %rax
	movq	207(%rdx), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movl	-448(%rbp), %eax
	leal	16(,%rax,8), %eax
	subq	$37592, %rbx
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1909
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1910:
	movq	%rax, -184(%rbp)
	jmp	.L1908
	.p2align 4,,10
	.p2align 3
.L1937:
	movq	%rbx, -192(%rbp)
	movq	$0, -184(%rbp)
.L1908:
	movl	-452(%rbp), %eax
	movl	-444(%rbp), %esi
	movq	%r15, %rdx
	movq	%r13, %rdi
	movl	%eax, -176(%rbp)
	movq	(%r12), %rcx
	call	_ZN2v88internal26IndirectFunctionTableEntry3SetEimNS0_6ObjectE
	movq	-464(%rbp), %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	movq	-472(%rbp), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1938
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1896:
	.cfi_restore_state
	movq	41088(%r12), %rax
	movq	%rax, -440(%rbp)
	cmpq	%rax, 41096(%r12)
	je	.L1939
.L1898:
	movq	-440(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L1897
	.p2align 4,,10
	.p2align 3
.L1936:
	movq	(%rbx), %rax
	movq	-440(%rbp), %rdi
	leaq	-413(%rbp), %rdx
	movq	%r14, %rsi
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r15
	movq	192(%r15), %rax
	movq	%rax, -413(%rbp)
	movl	200(%r15), %eax
	movl	%eax, -405(%rbp)
	movzbl	204(%r15), %eax
	movb	%al, -401(%rbp)
	call	_ZN2v88internal8compiler21ResolveWasmImportCallENS0_6HandleINS0_10JSReceiverEEEPNS0_9SignatureINS0_4wasm9ValueTypeEEERKNS6_12WasmFeaturesE@PLT
	movq	%r15, %rsi
	movq	%rdx, -440(%rbp)
	leaq	-368(%rbp), %rdx
	movq	%rdx, %rdi
	movl	%eax, -456(%rbp)
	movq	%rdx, -480(%rbp)
	call	_ZNK2v88internal4wasm12NativeModule20CreateCompilationEnvEv@PLT
	movq	%r14, %r8
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	movq	45752(%r12), %rsi
	movl	-456(%rbp), %ecx
	leaq	-384(%rbp), %r14
	movq	-480(%rbp), %rdx
	call	_ZN2v88internal8compiler28CompileWasmImportCallWrapperEPNS0_4wasm10WasmEngineEPNS2_14CompilationEnvENS1_18WasmImportCallKindEPNS0_9SignatureINS2_9ValueTypeEEEb@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm11GetCodeKindERKNS1_21WasmCompilationResultE@PLT
	movq	-96(%rbp), %rdx
	pushq	$0
	movq	%r13, %rcx
	pushq	%rax
	leaq	-400(%rbp), %rax
	movl	-100(%rbp), %r9d
	movq	%r15, %rsi
	pushq	%r14
	movl	-104(%rbp), %r8d
	leaq	-424(%rbp), %rdi
	movq	%rdx, -384(%rbp)
	movq	-88(%rbp), %rdx
	pushq	%rax
	movq	%rdx, -376(%rbp)
	movq	-80(%rbp), %rdx
	movq	$0, -96(%rbp)
	movq	%rdx, -400(%rbp)
	movq	-72(%rbp), %rdx
	movq	$0, -88(%rbp)
	movq	%rdx, -392(%rbp)
	movl	-64(%rbp), %edx
	movq	$0, -80(%rbp)
	call	_ZN2v88internal4wasm12NativeModule7AddCodeEjRKNS0_8CodeDescEjjNS0_11OwnedVectorINS0_12trap_handler24ProtectedInstructionDataEEENS6_IKhEENS1_8WasmCode4KindENS1_13ExecutionTierE@PLT
	movq	-400(%rbp), %rdi
	addq	$32, %rsp
	testq	%rdi, %rdi
	je	.L1900
	call	_ZdaPv@PLT
.L1900:
	movq	-384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1901
	call	_ZdaPv@PLT
.L1901:
	movq	-424(%rbp), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	$0, -424(%rbp)
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal4wasm12NativeModule11PublishCodeESt10unique_ptrINS1_8WasmCodeESt14default_deleteIS4_EE@PLT
	movq	-384(%rbp), %r15
	movq	%rax, %r14
	testq	%r15, %r15
	je	.L1902
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm8WasmCodeD1Ev@PLT
	movl	$144, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1902:
	movq	40960(%r12), %rax
	movl	8(%r14), %esi
	leaq	8136(%rax), %rdi
	call	_ZN2v88internal22StatsCounterThreadSafe9IncrementEi@PLT
	movq	40960(%r12), %rax
	movl	24(%r14), %esi
	leaq	8200(%rax), %rdi
	call	_ZN2v88internal22StatsCounterThreadSafe9IncrementEi@PLT
	movq	(%r14), %r15
	movq	-424(%rbp), %r14
	testq	%r14, %r14
	je	.L1903
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm8WasmCodeD1Ev@PLT
	movl	$144, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1903:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1904
	call	_ZdaPv@PLT
.L1904:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1905
	call	_ZdaPv@PLT
.L1905:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1899
	call	_ZdaPv@PLT
	jmp	.L1899
	.p2align 4,,10
	.p2align 3
.L1909:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1940
.L1911:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1910
	.p2align 4,,10
	.p2align 3
.L1939:
	movq	%r12, %rdi
	movq	%rsi, -464(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-464(%rbp), %rsi
	movq	%rax, -440(%rbp)
	jmp	.L1898
	.p2align 4,,10
	.p2align 3
.L1940:
	movq	%rbx, %rdi
	movq	%rsi, -440(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-440(%rbp), %rsi
	jmp	.L1911
.L1938:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22718:
	.size	_ZN2v88internal18WasmInstanceObject29ImportWasmJSFunctionIntoTableEPNS0_7IsolateENS0_6HandleIS1_EEiiNS4_INS0_14WasmJSFunctionEEE, .-_ZN2v88internal18WasmInstanceObject29ImportWasmJSFunctionIntoTableEPNS0_7IsolateENS0_6HandleIS1_EEiiNS4_INS0_14WasmJSFunctionEEE
	.section	.text._ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_14WasmJSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_14WasmJSFunctionEEE
	.type	_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_14WasmJSFunctionEEE, @function
_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_14WasmJSFunctionEEE:
.LFB22651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	41112(%rdi), %rdi
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1942
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -56(%rbp)
	movq	(%rax), %rsi
.L1943:
	movl	11(%rsi), %eax
	testl	%eax, %eax
	jle	.L1941
	movq	-56(%rbp), %rax
	movl	$23, %r12d
	xorl	%ebx, %ebx
	movq	(%rax), %rax
	jmp	.L1945
	.p2align 4,,10
	.p2align 3
.L1953:
	movq	%r8, %rsi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-64(%rbp), %rdx
	movq	%rax, %rsi
.L1947:
	movq	%r14, %r8
	movl	%r13d, %ecx
	movq	%r15, %rdi
	addq	$24, %r12
	call	_ZN2v88internal18WasmInstanceObject29ImportWasmJSFunctionIntoTableEPNS0_7IsolateENS0_6HandleIS1_EEiiNS4_INS0_14WasmJSFunctionEEE
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rsi
	cmpl	%ebx, 11(%rax)
	jle	.L1941
.L1945:
	movq	(%r12,%rsi), %rdx
	movq	-8(%r12,%rax), %r8
	addl	$3, %ebx
	movq	41112(%r15), %rdi
	sarq	$32, %rdx
	testq	%rdi, %rdi
	jne	.L1953
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L1954
.L1948:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r8, (%rsi)
	jmp	.L1947
	.p2align 4,,10
	.p2align 3
.L1941:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1954:
	.cfi_restore_state
	movq	%r15, %rdi
	movq	%rdx, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1948
	.p2align 4,,10
	.p2align 3
.L1942:
	movq	41088(%r15), %rax
	movq	%rax, -56(%rbp)
	cmpq	41096(%r15), %rax
	je	.L1955
.L1944:
	movq	-56(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L1943
.L1955:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, -56(%rbp)
	jmp	.L1944
	.cfi_endproc
.LFE22651:
	.size	_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_14WasmJSFunctionEEE, .-_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_14WasmJSFunctionEEE
	.section	.text._ZN2v88internal15WasmTableObject3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15WasmTableObject3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE
	.type	_ZN2v88internal15WasmTableObject3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE, @function
_ZN2v88internal15WasmTableObject3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE:
.LFB22647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1957
	movl	%edx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-72(%rbp), %edx
	movq	%rax, %r15
.L1958:
	movq	0(%r13), %rax
	leal	16(,%rdx,8), %ebx
	movq	(%r14), %r8
	movslq	%ebx, %rbx
	subq	$1, %rbx
	cmpb	$6, 51(%rax)
	je	.L2006
	cmpq	%r8, 104(%r12)
	je	.L2007
	testb	$1, %r8b
	jne	.L2008
.L1970:
	movq	%r8, %rdi
	movl	%edx, -72(%rbp)
	call	_ZN2v88internal14WasmJSFunction16IsWasmJSFunctionENS0_6ObjectE
	movl	-72(%rbp), %edx
	movq	%r14, %rcx
	movq	%r13, %rsi
	testb	%al, %al
	movq	%r12, %rdi
	jne	.L2009
	call	_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_16WasmCapiFunctionEEE
.L1977:
	movq	(%r15), %r15
	movq	(%r14), %r13
	leaq	(%r15,%rbx), %r12
	movq	%r13, (%r12)
	testb	$1, %r13b
	je	.L1956
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L1980
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L1980:
	testb	$24, %al
	je	.L1956
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1956
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1956:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2010
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2008:
	.cfi_restore_state
	movq	-1(%r8), %rax
	cmpw	$1105, 11(%rax)
	jne	.L1970
	movq	47(%r8), %rax
	movl	43(%rax), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$8, %eax
	jne	.L1970
	leaq	-64(%rbp), %r9
	movl	%edx, -80(%rbp)
	movq	23(%r8), %rax
	movq	%r9, %rdi
	movq	%r9, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo27wasm_exported_function_dataEv@PLT
	movq	41112(%r12), %rdi
	movq	-72(%rbp), %r9
	movq	15(%rax), %rsi
	movl	-80(%rbp), %edx
	testq	%rdi, %rdi
	je	.L2004
	movq	%r9, -80(%rbp)
	movl	%edx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-72(%rbp), %edx
	movq	-80(%rbp), %r9
	movq	%rax, %r8
.L1975:
	movq	(%r14), %rax
	movl	%edx, -80(%rbp)
	movq	%r9, %rdi
	movq	%r8, -72(%rbp)
	movq	23(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo27wasm_exported_function_dataEv@PLT
	movq	-72(%rbp), %r8
	movl	-80(%rbp), %edx
	movq	%r13, %rsi
	movslq	35(%rax), %r9
	movq	%r12, %rdi
	movq	(%r8), %rax
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rcx
	movq	%r9, %rax
	salq	$5, %rax
	addq	136(%rcx), %rax
	movq	(%rax), %rcx
	call	_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiPNS0_9SignatureINS0_4wasm9ValueTypeEEENS4_INS0_18WasmInstanceObjectEEEi
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L1957:
	movq	41088(%r12), %r15
	cmpq	%r15, 41096(%r12)
	je	.L2011
.L1959:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L1958
	.p2align 4,,10
	.p2align 3
.L2009:
	call	_ZN2v88internal15WasmTableObject20UpdateDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_14WasmJSFunctionEEE
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L2007:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal15WasmTableObject19ClearDispatchTablesEPNS0_7IsolateENS0_6HandleIS1_EEi
	movq	(%r15), %r13
	movq	104(%r12), %r12
	leaq	0(%r13,%rbx), %r14
	movq	%r12, (%r14)
	testb	$1, %r12b
	je	.L1956
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L1967
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L1967:
	testb	$24, %al
	je	.L1956
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1956
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1956
	.p2align 4,,10
	.p2align 3
.L2004:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L2012
.L1976:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L1975
	.p2align 4,,10
	.p2align 3
.L2006:
	movq	(%r15), %r12
	leaq	(%r12,%rbx), %r13
	movq	%r8, 0(%r13)
	testb	$1, %r8b
	je	.L1956
	movq	%r8, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L2013
.L1962:
	testb	$24, %al
	je	.L1956
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1956
	movq	%r8, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1956
	.p2align 4,,10
	.p2align 3
.L2011:
	movq	%r12, %rdi
	movl	%edx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-80(%rbp), %edx
	movq	-72(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1959
	.p2align 4,,10
	.p2align 3
.L2013:
	movq	%r8, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-72(%rbp), %r8
	jmp	.L1962
	.p2align 4,,10
	.p2align 3
.L2012:
	movq	%r12, %rdi
	movq	%r9, -88(%rbp)
	movl	%edx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r9
	movl	-80(%rbp), %edx
	movq	-72(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1976
.L2010:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22647:
	.size	_ZN2v88internal15WasmTableObject3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE, .-_ZN2v88internal15WasmTableObject3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal15WasmTableObject4FillEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15WasmTableObject4FillEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEEj
	.type	_ZN2v88internal15WasmTableObject4FillEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEEj, @function
_ZN2v88internal15WasmTableObject4FillEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEEj:
.LFB22649:
	.cfi_startproc
	endbr64
	testl	%r8d, %r8d
	je	.L2022
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leal	(%r8,%rdx), %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L2016:
	movl	%ebx, %edx
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal15WasmTableObject3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE
	addl	$1, %ebx
	cmpl	%ebx, %r12d
	jne	.L2016
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2022:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE22649:
	.size	_ZN2v88internal15WasmTableObject4FillEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEEj, .-_ZN2v88internal15WasmTableObject4FillEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEEj
	.section	.rodata._ZN2v88internal18WasmInstanceObject16CopyTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"table_dst_index < instance->tables().length()"
	.align 8
.LC22:
	.string	"table_src_index < instance->tables().length()"
	.section	.text._ZN2v88internal18WasmInstanceObject16CopyTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18WasmInstanceObject16CopyTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj
	.type	_ZN2v88internal18WasmInstanceObject16CopyTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj, @function
_ZN2v88internal18WasmInstanceObject16CopyTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj:
.LFB22710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	16(%rbp), %eax
	testl	%eax, %eax
	je	.L2039
	movq	(%rsi), %rax
	movq	%rsi, %r14
	movl	%r8d, %r12d
	movq	%rdi, %rbx
	movq	199(%rax), %rsi
	movslq	11(%rsi), %rax
	cmpl	%eax, %edx
	setnb	%r8b
	testl	%eax, %eax
	setle	%dil
	orb	%dil, %r8b
	jne	.L2046
	movl	%ecx, %r15d
	cmpl	%eax, %ecx
	jnb	.L2047
	leal	16(,%rdx,8), %eax
	cltq
	movq	-1(%rsi,%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2029
	movl	%r9d, -60(%rbp)
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-56(%rbp), %edx
	movl	-60(%rbp), %r9d
	movq	%rax, %r13
.L2030:
	movq	(%r14), %rsi
	leal	16(,%r15,8), %eax
	cltq
	movq	199(%rsi), %rsi
	movq	-1(%rax,%rsi), %r8
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2032
	movq	%r8, %rsi
	movl	%r9d, -60(%rbp)
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-56(%rbp), %edx
	movl	-60(%rbp), %r9d
	movq	(%rax), %r8
	movq	%rax, %r14
.L2033:
	movq	0(%r13), %rax
	cmpl	%r12d, %r9d
	setb	%dil
	movq	23(%rax), %rax
	movslq	11(%rax), %rsi
	movq	23(%r8), %rax
	movslq	11(%rax), %rax
	cmpl	%r12d, %esi
	jb	.L2040
	subl	%r12d, %esi
	cmpl	16(%rbp), %esi
	movl	%esi, %r8d
	cmova	16(%rbp), %r8d
	setnb	%sil
.L2035:
	cmpl	%r9d, %eax
	jb	.L2041
	subl	%r9d, %eax
	movl	%r8d, %r11d
	cmpl	%r8d, %eax
	setnb	%r10b
	cmovbe	%eax, %r11d
	andl	%esi, %r10d
	movl	%r10d, %eax
	xorl	$1, %eax
	andl	%eax, %edi
.L2036:
	testb	%dil, %dil
	jne	.L2042
	cmpl	%r12d, %r9d
	sete	%sil
	cmpl	%r15d, %edx
	sete	%al
	testb	%al, %sil
	jne	.L2025
	testl	%r11d, %r11d
	je	.L2025
	xorl	%r15d, %r15d
	cmpl	%r12d, %r9d
	jnb	.L2038
	leal	-1(%r9), %eax
	movl	%eax, -68(%rbp)
	leal	-1(%r11,%r12), %eax
	movl	%eax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L2037:
	movl	-68(%rbp), %eax
	movl	-64(%rbp), %r12d
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movb	%r10b, -60(%rbp)
	leal	(%rax,%r11), %edx
	movl	%r11d, -56(%rbp)
	subl	%r15d, %r12d
	subl	%r15d, %edx
	addl	$1, %r15d
	call	_ZN2v88internal15WasmTableObject3GetEPNS0_7IsolateENS0_6HandleIS1_EEj
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal15WasmTableObject3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE
	movl	-56(%rbp), %r11d
	movzbl	-60(%rbp), %r10d
	cmpl	%r11d, %r15d
	jb	.L2037
.L2025:
	addq	$40, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2029:
	.cfi_restore_state
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L2048
.L2031:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L2030
	.p2align 4,,10
	.p2align 3
.L2039:
	movl	$1, %r10d
	jmp	.L2025
	.p2align 4,,10
	.p2align 3
.L2032:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L2049
.L2034:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%r8, (%r14)
	jmp	.L2033
	.p2align 4,,10
	.p2align 3
.L2038:
	leal	(%r9,%r15), %edx
	leal	(%r12,%r15), %r8d
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movb	%r10b, -68(%rbp)
	addl	$1, %r15d
	movl	%r11d, -64(%rbp)
	movl	%r9d, -56(%rbp)
	movl	%r8d, -60(%rbp)
	call	_ZN2v88internal15WasmTableObject3GetEPNS0_7IsolateENS0_6HandleIS1_EEj
	movl	-60(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movl	%r8d, %edx
	call	_ZN2v88internal15WasmTableObject3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE
	movl	-64(%rbp), %r11d
	movl	-56(%rbp), %r9d
	movzbl	-68(%rbp), %r10d
	cmpl	%r15d, %r11d
	ja	.L2038
	jmp	.L2025
	.p2align 4,,10
	.p2align 3
.L2042:
	xorl	%r10d, %r10d
	jmp	.L2025
	.p2align 4,,10
	.p2align 3
.L2041:
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	jmp	.L2036
	.p2align 4,,10
	.p2align 3
.L2040:
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	jmp	.L2035
	.p2align 4,,10
	.p2align 3
.L2046:
	leaq	.LC21(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2047:
	leaq	.LC22(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2049:
	movq	%rbx, %rdi
	movl	%r9d, -64(%rbp)
	movl	%edx, -60(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-64(%rbp), %r9d
	movl	-60(%rbp), %edx
	movq	-56(%rbp), %r8
	movq	%rax, %r14
	jmp	.L2034
	.p2align 4,,10
	.p2align 3
.L2048:
	movq	%rbx, %rdi
	movl	%r9d, -64(%rbp)
	movl	%edx, -60(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-64(%rbp), %r9d
	movl	-60(%rbp), %edx
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L2031
	.cfi_endproc
.LFE22710:
	.size	_ZN2v88internal18WasmInstanceObject16CopyTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj, .-_ZN2v88internal18WasmInstanceObject16CopyTableEntriesEPNS0_7IsolateENS0_6HandleIS1_EEjjjjj
	.section	.text._ZN2v88internal14WasmJSFunction16MatchesSignatureEPNS0_9SignatureINS0_4wasm9ValueTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14WasmJSFunction16MatchesSignatureEPNS0_9SignatureINS0_4wasm9ValueTypeEEE
	.type	_ZN2v88internal14WasmJSFunction16MatchesSignatureEPNS0_9SignatureINS0_4wasm9ValueTypeEEE, @function
_ZN2v88internal14WasmJSFunction16MatchesSignatureEPNS0_9SignatureINS0_4wasm9ValueTypeEEE:
.LFB22758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %r12
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-48(%rbp), %rdi
	movq	23(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo21wasm_js_function_dataEv@PLT
	xorl	%r8d, %r8d
	cmpl	%r12d, 27(%rax)
	jne	.L2050
	cmpl	%r13d, 35(%rax)
	je	.L2057
.L2050:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2058
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2057:
	.cfi_restore_state
	addq	%r13, %r12
	movl	$1, %r8d
	testl	%r12d, %r12d
	je	.L2050
	movq	39(%rax), %rdi
	movq	16(%rbx), %rsi
	movslq	%r12d, %rdx
	addq	$15, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	sete	%r8b
	jmp	.L2050
.L2058:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22758:
	.size	_ZN2v88internal14WasmJSFunction16MatchesSignatureEPNS0_9SignatureINS0_4wasm9ValueTypeEEE, .-_ZN2v88internal14WasmJSFunction16MatchesSignatureEPNS0_9SignatureINS0_4wasm9ValueTypeEEE
	.section	.text._ZNK2v88internal16WasmCapiFunction17GetHostCallTargetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16WasmCapiFunction17GetHostCallTargetEv
	.type	_ZNK2v88internal16WasmCapiFunction17GetHostCallTargetEv, @function
_ZNK2v88internal16WasmCapiFunction17GetHostCallTargetEv:
.LFB22759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-16(%rbp), %rdi
	movq	23(%rax), %rax
	movq	%rax, -16(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo23wasm_capi_function_dataEv@PLT
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	movq	7(%rax), %rax
	jne	.L2062
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2062:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22759:
	.size	_ZNK2v88internal16WasmCapiFunction17GetHostCallTargetEv, .-_ZNK2v88internal16WasmCapiFunction17GetHostCallTargetEv
	.section	.text._ZNK2v88internal16WasmCapiFunction22GetSerializedSignatureEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16WasmCapiFunction22GetSerializedSignatureEv
	.type	_ZNK2v88internal16WasmCapiFunction22GetSerializedSignatureEv, @function
_ZNK2v88internal16WasmCapiFunction22GetSerializedSignatureEv:
.LFB22760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-16(%rbp), %rdi
	movq	23(%rax), %rax
	movq	%rax, -16(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo23wasm_capi_function_dataEv@PLT
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	movq	31(%rax), %rax
	jne	.L2066
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2066:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22760:
	.size	_ZNK2v88internal16WasmCapiFunction22GetSerializedSignatureEv, .-_ZNK2v88internal16WasmCapiFunction22GetSerializedSignatureEv
	.section	.text._ZN2v88internal20WasmExternalFunction22IsWasmExternalFunctionENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20WasmExternalFunction22IsWasmExternalFunctionENS0_6ObjectE
	.type	_ZN2v88internal20WasmExternalFunction22IsWasmExternalFunctionENS0_6ObjectE, @function
_ZN2v88internal20WasmExternalFunction22IsWasmExternalFunctionENS0_6ObjectE:
.LFB22761:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	notq	%rax
	andl	$1, %eax
	je	.L2068
.L2074:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2068:
	movq	-1(%rdi), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L2075
.L2070:
	movq	-1(%rdi), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L2074
	movq	23(%rdi), %rdx
	movq	7(%rdx), %rdx
	testb	$1, %dl
	jne	.L2076
	ret
	.p2align 4,,10
	.p2align 3
.L2075:
	movq	47(%rdi), %rdx
	movl	43(%rdx), %edx
	shrl	%edx
	andl	$31, %edx
	cmpl	$8, %edx
	jne	.L2070
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2076:
	movq	-1(%rdx), %rax
	cmpw	$109, 11(%rax)
	sete	%al
	ret
	.cfi_endproc
.LFE22761:
	.size	_ZN2v88internal20WasmExternalFunction22IsWasmExternalFunctionENS0_6ObjectE, .-_ZN2v88internal20WasmExternalFunction22IsWasmExternalFunctionENS0_6ObjectE
	.section	.text._ZN2v88internal16WasmExceptionTag3NewEPNS0_7IsolateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16WasmExceptionTag3NewEPNS0_7IsolateEi
	.type	_ZN2v88internal16WasmExceptionTag3NewEPNS0_7IsolateEi, @function
_ZN2v88internal16WasmExceptionTag3NewEPNS0_7IsolateEi:
.LFB22762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	movl	$106, %esi
	salq	$32, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	%rbx, 7(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22762:
	.size	_ZN2v88internal16WasmExceptionTag3NewEPNS0_7IsolateEi, .-_ZN2v88internal16WasmExceptionTag3NewEPNS0_7IsolateEi
	.section	.text._ZN2v88internal11AsmWasmData3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_10FixedArrayEEENS8_INS0_9ByteArrayEEENS8_INS0_10HeapNumberEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11AsmWasmData3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_10FixedArrayEEENS8_INS0_9ByteArrayEEENS8_INS0_10HeapNumberEEE
	.type	_ZN2v88internal11AsmWasmData3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_10FixedArrayEEENS8_INS0_9ByteArrayEEENS8_INS0_10HeapNumberEEE, @function
_ZN2v88internal11AsmWasmData3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_10FixedArrayEEENS8_INS0_9ByteArrayEEENS8_INS0_10HeapNumberEEE:
.LFB22763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	%r8, -56(%rbp)
	movq	208(%rax), %rdi
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal4wasm15WasmCodeManager28EstimateNativeModuleCodeSizeEPKNS1_10WasmModuleE@PLT
	movq	-64(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal4wasm15WasmCodeManager31EstimateNativeModuleNonCodeSizeEPKNS1_10WasmModuleE@PLT
	addq	%rax, %rbx
	movq	32(%r12), %rax
	addq	%rbx, %rax
	movq	%rax, %rdx
	subq	48(%r12), %rdx
	movq	%rax, 32(%r12)
	cmpq	$33554432, %rdx
	jg	.L2132
.L2080:
	testq	%rbx, %rbx
	js	.L2133
	jne	.L2134
.L2082:
	movl	$16, %edi
	call	_Znwm@PLT
	movdqu	0(%r13), %xmm1
	movq	%rax, %rdx
	movq	8(%r13), %rax
	movups	%xmm1, (%rdx)
	testq	%rax, %rax
	je	.L2083
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L2084
	lock addl	$1, 8(%rax)
.L2083:
	movl	$48, %edi
	movq	%rdx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movups	%xmm0, 8(%rax)
	movq	%rax, %r13
	movq	%rbx, (%rax)
	movq	%r13, %rsi
	movq	%rdx, 24(%rax)
	leaq	_ZN2v88internal7ManagedINS0_4wasm12NativeModuleEE10DestructorEPv(%rip), %rax
	xorl	%edx, %edx
	movq	%rax, 32(%r13)
	movq	$0, 40(%r13)
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	41152(%r12), %rdi
	movq	(%rax), %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, 40(%r13)
	movq	%rax, %rdi
	call	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE@PLT
	movq	%r12, %rdi
	movl	$83, %esi
	movl	$1, %edx
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE@PLT
	movq	(%rbx), %rbx
	movq	(%rax), %rdi
	movq	%rax, %r12
	movq	%rbx, 7(%rdi)
	leaq	7(%rdi), %rsi
	testb	$1, %bl
	je	.L2100
	movq	%rbx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -64(%rbp)
	testl	$262144, %eax
	jne	.L2135
	testb	$24, %al
	je	.L2100
.L2143:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2136
	.p2align 4,,10
	.p2align 3
.L2100:
	movq	(%r12), %rbx
	movq	(%r15), %r15
	movq	%r15, 15(%rbx)
	leaq	15(%rbx), %rsi
	testb	$1, %r15b
	je	.L2099
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -64(%rbp)
	testl	$262144, %eax
	jne	.L2137
	testb	$24, %al
	je	.L2099
.L2146:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2138
	.p2align 4,,10
	.p2align 3
.L2099:
	movq	(%r12), %r15
	movq	(%r14), %r14
	movq	%r14, 23(%r15)
	leaq	23(%r15), %rsi
	testb	$1, %r14b
	je	.L2098
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L2139
	testb	$24, %al
	je	.L2098
.L2145:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2140
	.p2align 4,,10
	.p2align 3
.L2098:
	movq	-56(%rbp), %rax
	movq	(%r12), %r14
	movq	(%rax), %r13
	leaq	31(%r14), %r15
	movq	%r13, 31(%r14)
	testb	$1, %r13b
	je	.L2097
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L2141
	testb	$24, %al
	je	.L2097
.L2144:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2142
.L2097:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2084:
	.cfi_restore_state
	addl	$1, 8(%rax)
	jmp	.L2083
	.p2align 4,,10
	.p2align 3
.L2135:
	movq	%rbx, %rdx
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L2143
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2141:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L2144
	jmp	.L2097
	.p2align 4,,10
	.p2align 3
.L2139:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-64(%rbp), %rsi
	testb	$24, %al
	jne	.L2145
	jmp	.L2098
	.p2align 4,,10
	.p2align 3
.L2137:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L2146
	jmp	.L2099
	.p2align 4,,10
	.p2align 3
.L2134:
	cmpq	40(%r12), %rax
	jle	.L2082
	movq	%r12, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L2082
	.p2align 4,,10
	.p2align 3
.L2133:
	movq	40(%r12), %rax
	addq	%rbx, %rax
	cmpq	$67108864, %rax
	jle	.L2082
	movq	%rax, 40(%r12)
	jmp	.L2082
	.p2align 4,,10
	.p2align 3
.L2132:
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-64(%rbp), %rax
	jmp	.L2080
	.p2align 4,,10
	.p2align 3
.L2136:
	movq	%rbx, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2138:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2099
	.p2align 4,,10
	.p2align 3
.L2140:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2098
	.p2align 4,,10
	.p2align 3
.L2142:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2097
	.cfi_endproc
.LFE22763:
	.size	_ZN2v88internal11AsmWasmData3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_10FixedArrayEEENS8_INS0_9ByteArrayEEENS8_INS0_10HeapNumberEEE, .-_ZN2v88internal11AsmWasmData3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_10FixedArrayEEENS8_INS0_9ByteArrayEEENS8_INS0_10HeapNumberEEE
	.section	.rodata._ZNSt6vectorIjSaIjEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC23:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIjSaIjEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIjSaIjEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIjSaIjEE17_M_default_appendEm
	.type	_ZNSt6vectorIjSaIjEE17_M_default_appendEm, @function
_ZNSt6vectorIjSaIjEE17_M_default_appendEm:
.LFB26366:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L2166
	movabsq	$2305843009213693951, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%r12), %rax
	movq	%rcx, %rsi
	subq	(%rdi), %rsi
	subq	%rcx, %rax
	movq	%rdx, %rdi
	movq	%rsi, %r13
	sarq	$2, %rax
	sarq	$2, %r13
	subq	%r13, %rdi
	cmpq	%rbx, %rax
	jb	.L2149
	salq	$2, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2166:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L2149:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L2169
	cmpq	%rbx, %r13
	movq	%rbx, %r14
	movq	%rsi, -56(%rbp)
	cmovnb	%r13, %r14
	addq	%r13, %r14
	cmpq	%rdx, %r14
	cmova	%rdx, %r14
	salq	$2, %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	leaq	0(,%rbx,4), %rdx
	movq	%rax, %r15
	leaq	(%rax,%rsi), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%r12), %r8
	movq	8(%r12), %rdx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L2170
	testq	%r8, %r8
	jne	.L2153
.L2154:
	addq	%r13, %rbx
	addq	%r15, %r14
	movq	%r15, (%r12)
	leaq	(%r15,%rbx,4), %rax
	movq	%r14, 16(%r12)
	movq	%rax, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2170:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L2153:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L2154
.L2169:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE26366:
	.size	_ZNSt6vectorIjSaIjEE17_M_default_appendEm, .-_ZNSt6vectorIjSaIjEE17_M_default_appendEm
	.section	.text._ZNSt6vectorImSaImEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorImSaImEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorImSaImEE17_M_default_appendEm
	.type	_ZNSt6vectorImSaImEE17_M_default_appendEm, @function
_ZNSt6vectorImSaImEE17_M_default_appendEm:
.LFB26370:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L2190
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%r12), %rax
	movq	%rcx, %rsi
	subq	(%rdi), %rsi
	subq	%rcx, %rax
	movq	%rdx, %rdi
	movq	%rsi, %r13
	sarq	$3, %rax
	sarq	$3, %r13
	subq	%r13, %rdi
	cmpq	%rbx, %rax
	jb	.L2173
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2190:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L2173:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L2193
	cmpq	%rbx, %r13
	movq	%rbx, %r14
	movq	%rsi, -56(%rbp)
	cmovnb	%r13, %r14
	addq	%r13, %r14
	cmpq	%rdx, %r14
	cmova	%rdx, %r14
	salq	$3, %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	leaq	0(,%rbx,8), %rdx
	movq	%rax, %r15
	leaq	(%rax,%rsi), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%r12), %r8
	movq	8(%r12), %rdx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L2194
	testq	%r8, %r8
	jne	.L2177
.L2178:
	addq	%r13, %rbx
	addq	%r15, %r14
	movq	%r15, (%r12)
	leaq	(%r15,%rbx,8), %rax
	movq	%r14, 16(%r12)
	movq	%rax, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2194:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L2177:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L2178
.L2193:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE26370:
	.size	_ZNSt6vectorImSaImEE17_M_default_appendEm, .-_ZNSt6vectorImSaImEE17_M_default_appendEm
	.section	.text._ZN2v88internal25WasmIndirectFunctionTable6ResizeEPNS0_7IsolateENS0_6HandleIS1_EEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25WasmIndirectFunctionTable6ResizeEPNS0_7IsolateENS0_6HandleIS1_EEj
	.type	_ZN2v88internal25WasmIndirectFunctionTable6ResizeEPNS0_7IsolateENS0_6HandleIS1_EEj, @function
_ZN2v88internal25WasmIndirectFunctionTable6ResizeEPNS0_7IsolateENS0_6HandleIS1_EEj:
.LFB22670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movl	7(%rax), %ebx
	cmpl	%ebx, %edx
	ja	.L2227
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2227:
	.cfi_restore_state
	movq	31(%rax), %rax
	movq	%rsi, %r13
	movl	%edx, %r12d
	movq	%rdi, %r14
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r15
	movl	%edx, %eax
	movq	8(%r15), %rcx
	movq	(%r15), %rsi
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	sarq	$2, %rdx
	cmpq	%rdx, %rax
	ja	.L2228
	jnb	.L2198
	leaq	(%rsi,%rax,4), %rdx
	cmpq	%rdx, %rcx
	je	.L2198
	movq	%rdx, 8(%r15)
.L2198:
	movq	32(%r15), %rcx
	movq	24(%r15), %rsi
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	ja	.L2229
	jb	.L2230
.L2200:
	movq	(%r15), %rdx
	movq	0(%r13), %rax
	movq	%rdx, 15(%rax)
	movq	24(%r15), %rdx
	movq	0(%r13), %rax
	movq	%rdx, 23(%rax)
	movq	0(%r13), %rax
	movq	41112(%r14), %rdi
	movq	39(%rax), %r15
	testq	%rdi, %rdi
	je	.L2201
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2202:
	movl	%r12d, %edx
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	subl	%ebx, %edx
	call	_ZN2v88internal7Factory21CopyFixedArrayAndGrowENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE@PLT
	movq	0(%r13), %r15
	movq	(%rax), %r14
	leaq	39(%r15), %rsi
	movq	%r14, 39(%r15)
	testb	$1, %r14b
	je	.L2213
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L2231
.L2205:
	testb	$24, %al
	je	.L2213
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2232
	.p2align 4,,10
	.p2align 3
.L2213:
	movq	0(%r13), %rax
	movl	%r12d, 7(%rax)
	.p2align 4,,10
	.p2align 3
.L2211:
	movq	0(%r13), %rdx
	movslq	%ebx, %rax
	movq	15(%rdx), %rdx
	movl	$-1, (%rdx,%rax,4)
	movq	0(%r13), %rdx
	movq	23(%rdx), %rdx
	movq	$0, (%rdx,%rax,8)
	movq	0(%r13), %rax
	movq	39(%rax), %r15
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %r14
	leal	16(,%rbx,8), %eax
	cltq
	leaq	-1(%r15,%rax), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L2212
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L2208
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L2208:
	testb	$24, %al
	je	.L2212
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2212
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2212:
	addl	$1, %ebx
	cmpl	%ebx, %r12d
	jne	.L2211
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2201:
	.cfi_restore_state
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L2233
.L2203:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r15, (%rsi)
	jmp	.L2202
	.p2align 4,,10
	.p2align 3
.L2230:
	leaq	(%rsi,%rax,8), %rax
	cmpq	%rax, %rcx
	je	.L2200
	movq	%rax, 32(%r15)
	jmp	.L2200
	.p2align 4,,10
	.p2align 3
.L2231:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L2205
	.p2align 4,,10
	.p2align 3
.L2232:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2213
	.p2align 4,,10
	.p2align 3
.L2229:
	subq	%rdx, %rax
	leaq	24(%r15), %rdi
	movq	%rax, %rsi
	call	_ZNSt6vectorImSaImEE17_M_default_appendEm
	jmp	.L2200
	.p2align 4,,10
	.p2align 3
.L2228:
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%rax, -56(%rbp)
	subq	%rdx, %rsi
	call	_ZNSt6vectorIjSaIjEE17_M_default_appendEm
	movq	-56(%rbp), %rax
	jmp	.L2198
.L2233:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2203
	.cfi_endproc
.LFE22670:
	.size	_ZN2v88internal25WasmIndirectFunctionTable6ResizeEPNS0_7IsolateENS0_6HandleIS1_EEj, .-_ZN2v88internal25WasmIndirectFunctionTable6ResizeEPNS0_7IsolateENS0_6HandleIS1_EEj
	.section	.text._ZN2v88internal18WasmInstanceObject42EnsureIndirectFunctionTableWithMinimumSizeENS0_6HandleIS1_EEij,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18WasmInstanceObject42EnsureIndirectFunctionTableWithMinimumSizeENS0_6HandleIS1_EEij
	.type	_ZN2v88internal18WasmInstanceObject42EnsureIndirectFunctionTableWithMinimumSizeENS0_6HandleIS1_EEij, @function
_ZN2v88internal18WasmInstanceObject42EnsureIndirectFunctionTableWithMinimumSizeENS0_6HandleIS1_EEij:
.LFB22701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %r9
	leaq	-37592(%r9), %r15
	testl	%esi, %esi
	jg	.L2272
	xorl	%r10d, %r10d
	cmpl	95(%rax), %r12d
	ja	.L2273
.L2234:
	addq	$56, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2273:
	.cfi_restore_state
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	%rdi, %r8
	movq	%r8, -64(%rbp)
	movq	%rax, -88(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -80(%rbp)
	movq	(%rdi), %rax
	movq	215(%rax), %rdx
	movl	95(%rax), %r14d
	movl	%r12d, %eax
	leaq	0(,%rax,8), %r11
	leaq	0(,%rax,4), %rsi
	movq	7(%rdx), %rdx
	movq	%r11, -56(%rbp)
	movq	24(%rdx), %rdx
	movq	(%rdx), %r13
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2240
	call	realloc@PLT
	movq	-56(%rbp), %r11
	movq	8(%r13), %rdi
	movq	%rax, %rbx
	movq	%r11, %rsi
	call	realloc@PLT
	movq	-64(%rbp), %r8
	movq	41112(%r15), %rdi
	movq	%rax, %r11
	movq	(%r8), %rax
	movq	71(%rax), %r9
	testq	%rdi, %rdi
	je	.L2241
	movq	%r9, %rsi
	movq	%r11, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r8
	movq	%rax, %rsi
.L2242:
	movl	%r12d, %edx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r8, -64(%rbp)
	subl	%r14d, %edx
	movq	%r11, -56(%rbp)
	call	_ZN2v88internal7Factory21CopyFixedArrayAndGrowENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE@PLT
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r8
.L2244:
	movq	(%r8), %rdx
	movl	%r12d, 95(%rdx)
	movq	%rbx, 0(%r13)
	movq	(%r8), %rdx
	movq	%rbx, 87(%rdx)
	movq	%r11, 8(%r13)
	movq	(%r8), %rdx
	movq	%r11, 79(%rdx)
	movq	(%r8), %rbx
	movq	(%rax), %r13
	leaq	71(%rbx), %rsi
	movq	%r13, 71(%rbx)
	testb	$1, %r13b
	je	.L2256
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L2246
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L2246:
	testb	$24, %al
	je	.L2256
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2256
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L2256:
	cmpl	%r14d, %r12d
	jbe	.L2254
	.p2align 4,,10
	.p2align 3
.L2248:
	movq	(%r8), %rdx
	movslq	%r14d, %rax
	movq	87(%rdx), %rdx
	movl	$-1, (%rdx,%rax,4)
	movq	(%r8), %rdx
	movq	79(%rdx), %rdx
	movq	$0, (%rdx,%rax,8)
	movq	(%r8), %rax
	movq	71(%rax), %rbx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %r13
	leal	16(,%r14,8), %eax
	cltq
	leaq	-1(%rbx,%rax), %rsi
	movq	%r13, (%rsi)
	testb	$1, %r13b
	je	.L2255
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L2252
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L2252:
	testb	$24, %al
	je	.L2255
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2255
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L2255:
	addl	$1, %r14d
	cmpl	%r14d, %r12d
	jne	.L2248
.L2254:
	movq	-88(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-80(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L2250
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2250:
	movl	$1, %r10d
	jmp	.L2234
	.p2align 4,,10
	.p2align 3
.L2272:
	movq	207(%rax), %rax
	leal	16(,%rsi,8), %edx
	movslq	%edx, %rdx
	movq	-1(%rdx,%rax), %r13
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2236
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2237:
	movl	%r12d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal25WasmIndirectFunctionTable6ResizeEPNS0_7IsolateENS0_6HandleIS1_EEj
	movl	$1, %r10d
	jmp	.L2234
	.p2align 4,,10
	.p2align 3
.L2236:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L2274
.L2238:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r13, (%rsi)
	jmp	.L2237
	.p2align 4,,10
	.p2align 3
.L2241:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L2275
.L2243:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r9, (%rsi)
	jmp	.L2242
	.p2align 4,,10
	.p2align 3
.L2240:
	movq	%rsi, %rdi
	call	malloc@PLT
	movq	-56(%rbp), %r11
	movq	%rax, %rbx
	movq	%r11, %rdi
	call	malloc@PLT
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%r15, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r11
	jmp	.L2244
	.p2align 4,,10
	.p2align 3
.L2274:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2238
	.p2align 4,,10
	.p2align 3
.L2275:
	movq	%r15, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r11
	movq	%rax, %rsi
	jmp	.L2243
	.cfi_endproc
.LFE22701:
	.size	_ZN2v88internal18WasmInstanceObject42EnsureIndirectFunctionTableWithMinimumSizeENS0_6HandleIS1_EEij, .-_ZN2v88internal18WasmInstanceObject42EnsureIndirectFunctionTableWithMinimumSizeENS0_6HandleIS1_EEij
	.section	.text._ZN2v88internal15WasmTableObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15WasmTableObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE
	.type	_ZN2v88internal15WasmTableObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE, @function
_ZN2v88internal15WasmTableObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE:
.LFB22644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	%rcx, -72(%rbp)
	movq	%rsi, -56(%rbp)
	movq	23(%rax), %rdx
	movslq	11(%rdx), %rcx
	movq	%rcx, -88(%rbp)
	testl	%r13d, %r13d
	jne	.L2277
	movl	%ecx, %eax
.L2276:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2277:
	.cfi_restore_state
	movq	31(%rax), %rdx
	movq	%rdi, %rbx
	testb	$1, %dl
	jne	.L2279
	sarq	$32, %rdx
	movl	%edx, %ecx
	js	.L2281
.L2282:
	movq	-88(%rbp), %rdx
	subl	%edx, %ecx
	movl	%edx, %r12d
	cmpl	%r13d, %ecx
	jb	.L2305
	movq	41112(%rbx), %rdi
	movq	23(%rax), %r14
	leal	0(%r13,%rdx), %r15d
	testq	%rdi, %rdi
	je	.L2288
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2289:
	xorl	%ecx, %ecx
	movl	%r13d, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory21CopyFixedArrayAndGrowENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE@PLT
	movq	-56(%rbp), %rcx
	movq	(%rax), %r13
	movq	(%rcx), %r14
	movq	%r13, 23(%r14)
	leaq	23(%r14), %rsi
	testb	$1, %r13b
	je	.L2304
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -64(%rbp)
	testl	$262144, %eax
	jne	.L2316
.L2292:
	testb	$24, %al
	je	.L2304
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2317
	.p2align 4,,10
	.p2align 3
.L2304:
	movq	-56(%rbp), %rax
	movq	41112(%rbx), %rdi
	movq	(%rax), %rax
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2294
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -64(%rbp)
	movq	(%rax), %rsi
.L2295:
	movl	11(%rsi), %eax
	testl	%eax, %eax
	jle	.L2302
	movq	-64(%rbp), %rax
	movl	$23, %r14d
	xorl	%r13d, %r13d
	movq	(%rax), %rax
	jmp	.L2297
	.p2align 4,,10
	.p2align 3
.L2318:
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-80(%rbp), %r8
	movq	%rax, %rdi
.L2300:
	movl	%r8d, %esi
	movl	%r15d, %edx
	addq	$24, %r14
	call	_ZN2v88internal18WasmInstanceObject42EnsureIndirectFunctionTableWithMinimumSizeENS0_6HandleIS1_EEij
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rsi
	cmpl	%r13d, 11(%rax)
	jle	.L2302
.L2297:
	movq	(%rsi,%r14), %r8
	movq	-8(%r14,%rax), %rsi
	addl	$3, %r13d
	movq	41112(%rbx), %rdi
	sarq	$32, %r8
	testq	%rdi, %rdi
	jne	.L2318
	movq	41088(%rbx), %rdi
	cmpq	41096(%rbx), %rdi
	je	.L2319
.L2301:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdi)
	jmp	.L2300
	.p2align 4,,10
	.p2align 3
.L2279:
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L2281
	movsd	7(%rdx), %xmm1
	movsd	.LC24(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rcx
	movq	%xmm2, %rdx
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	je	.L2320
	.p2align 4,,10
	.p2align 3
.L2281:
	movl	_ZN2v88internal24FLAG_wasm_max_table_sizeE(%rip), %ecx
	jmp	.L2282
	.p2align 4,,10
	.p2align 3
.L2302:
	cmpl	-88(%rbp), %r15d
	jbe	.L2303
	.p2align 4,,10
	.p2align 3
.L2298:
	movq	-72(%rbp), %rcx
	movq	-56(%rbp), %rsi
	movl	%r12d, %edx
	movq	%rbx, %rdi
	addl	$1, %r12d
	call	_ZN2v88internal15WasmTableObject3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE
	cmpl	%r12d, %r15d
	jne	.L2298
.L2303:
	movl	-88(%rbp), %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2319:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%r8, -96(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L2301
	.p2align 4,,10
	.p2align 3
.L2294:
	movq	41088(%rbx), %rax
	movq	%rax, -64(%rbp)
	cmpq	41096(%rbx), %rax
	je	.L2321
.L2296:
	movq	-64(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L2295
	.p2align 4,,10
	.p2align 3
.L2288:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L2322
.L2290:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r14, (%rsi)
	jmp	.L2289
	.p2align 4,,10
	.p2align 3
.L2316:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L2292
	.p2align 4,,10
	.p2align 3
.L2317:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2304
.L2321:
	movq	%rbx, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, -64(%rbp)
	jmp	.L2296
.L2322:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2290
.L2320:
	movl	%edx, %edx
	pxor	%xmm0, %xmm0
	movd	%xmm2, %ecx
	cvtsi2sdq	%rdx, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L2281
	je	.L2282
	jmp	.L2281
	.p2align 4,,10
	.p2align 3
.L2305:
	movl	$-1, %eax
	jmp	.L2276
	.cfi_endproc
.LFE22644:
	.size	_ZN2v88internal15WasmTableObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE, .-_ZN2v88internal15WasmTableObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEE, @function
_GLOBAL__sub_I__ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEE:
.LFB28849:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28849:
	.size	_GLOBAL__sub_I__ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEE, .-_GLOBAL__sub_I__ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal16WasmModuleObject3NewEPNS0_7IsolateESt10shared_ptrINS0_4wasm12NativeModuleEENS0_6HandleINS0_6ScriptEEE
	.weak	_ZTVN2v88internal4wasm7DecoderE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm7DecoderE,"awG",@progbits,_ZTVN2v88internal4wasm7DecoderE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm7DecoderE, @object
	.size	_ZTVN2v88internal4wasm7DecoderE, 40
_ZTVN2v88internal4wasm7DecoderE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm7DecoderD1Ev
	.quad	_ZN2v88internal4wasm7DecoderD0Ev
	.quad	_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE,"aw"
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_120IftNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE,"aw"
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal12_GLOBAL__N_129WasmInstanceNativeAllocationsESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.globl	_ZN2v88internal18WasmInstanceObject19kTaggedFieldOffsetsE
	.section	.rodata._ZN2v88internal18WasmInstanceObject19kTaggedFieldOffsetsE,"a"
	.align 16
	.type	_ZN2v88internal18WasmInstanceObject19kTaggedFieldOffsetsE, @object
	.size	_ZN2v88internal18WasmInstanceObject19kTaggedFieldOffsetsE, 30
_ZN2v88internal18WasmInstanceObject19kTaggedFieldOffsetsE:
	.value	56
	.value	72
	.value	136
	.value	144
	.value	152
	.value	160
	.value	168
	.value	176
	.value	184
	.value	192
	.value	200
	.value	208
	.value	216
	.value	224
	.value	232
	.section	.bss._ZZN2v88internal16WasmMemoryObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEjE29trace_event_unique_atomic1384,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal16WasmMemoryObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEjE29trace_event_unique_atomic1384, @object
	.size	_ZZN2v88internal16WasmMemoryObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEjE29trace_event_unique_atomic1384, 8
_ZZN2v88internal16WasmMemoryObject4GrowEPNS0_7IsolateENS0_6HandleIS1_EEjE29trace_event_unique_atomic1384:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 8
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	16
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.data.rel.ro,"aw"
	.align 8
.LC0:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC1:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC2:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC24:
	.long	0
	.long	1127219200
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
