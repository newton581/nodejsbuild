	.file	"runtime-forin.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4479:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4479:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4480:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4480:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4482:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4482:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal12_GLOBAL__N_19EnumerateEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_19EnumerateEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE, @function
_ZN2v88internal12_GLOBAL__N_19EnumerateEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE:
.LFB20031:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	leaq	-80(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8JSObject18MakePrototypesFastENS0_6HandleINS0_6ObjectEEENS0_12WhereToStartEPNS0_7IsolateE@PLT
	movq	%r12, %xmm1
	movq	%r13, %rdi
	movq	%rbx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	$0, -64(%rbp)
	movabsq	$77309411329, %rax
	movq	%rax, -56(%rbp)
	movl	$1, -48(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal18FastKeyAccumulator7PrepareEv@PLT
	cmpb	$0, -46(%rbp)
	jne	.L6
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal18FastKeyAccumulator7GetKeysENS0_17GetKeysConversionE@PLT
	testq	%rax, %rax
	je	.L8
	cmpb	$0, -46(%rbp)
	jne	.L6
.L8:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L14
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	-1(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L9
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L15
.L11:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rbx, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L11
.L14:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20031:
	.size	_ZN2v88internal12_GLOBAL__N_19EnumerateEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE, .-_ZN2v88internal12_GLOBAL__N_19EnumerateEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_121HasEnumerablePropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal12_GLOBAL__N_121HasEnumerablePropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_121HasEnumerablePropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal12_GLOBAL__N_121HasEnumerablePropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEE:
.LFB20032:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	movl	$3, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-128(%rbp), %r14
	movq	%rdx, %r13
	leaq	-129(%rbp), %r8
	pushq	%r12
	movq	%rsi, %rdx
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rdi, %rsi
	pushq	%rbx
	movq	%r14, %rdi
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$0, -129(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	cmpb	$0, -129(%rbp)
	je	.L28
	movl	-124(%rbp), %eax
	cmpl	$4, %eax
	je	.L28
	leaq	.L23(%rip), %rbx
.L19:
	cmpl	$7, %eax
	ja	.L21
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal12_GLOBAL__N_121HasEnumerablePropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEE,"a",@progbits
	.align 4
	.align 4
.L23:
	.long	.L29-.L23
	.long	.L28-.L23
	.long	.L27-.L23
	.long	.L26-.L23
	.long	.L21-.L23
	.long	.L25-.L23
	.long	.L24-.L23
	.long	.L22-.L23
	.section	.text._ZN2v88internal12_GLOBAL__N_121HasEnumerablePropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEE
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%r14, %rdi
	call	_ZN2v88internal7JSProxy21GetPropertyAttributesEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	je	.L41
	shrq	$32, %rax
	cmpl	$64, %eax
	je	.L97
	testb	$2, %al
	je	.L24
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	88(%r12), %rax
.L18:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L98
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L99
.L24:
	movq	-96(%rbp), %r12
	testq	%r12, %r12
	je	.L100
.L65:
	movq	%r12, %rax
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject36GetPropertyAttributesWithInterceptorEPNS0_14LookupIteratorE@PLT
	movq	%rax, %rdx
	shrq	$32, %rdx
	testb	%al, %al
	je	.L41
	cmpl	$64, %edx
	jne	.L24
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%r14, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	-124(%rbp), %eax
	cmpl	$4, %eax
	jne	.L19
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r14, %rdi
	call	_ZNK2v88internal14LookupIterator9HasAccessEv@PLT
	testb	%al, %al
	jne	.L21
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject42GetPropertyAttributesWithFailedAccessCheckEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	je	.L41
	shrq	$32, %rax
	cmpl	$64, %eax
	je	.L28
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L99:
	movq	-1(%rax), %rax
	cmpw	$1027, 11(%rax)
	jne	.L24
	movq	%r14, %rdi
	call	_ZN2v88internal17JSModuleNamespace21GetPropertyAttributesEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	jne	.L24
	.p2align 4,,10
	.p2align 3
.L41:
	xorl	%eax, %eax
	jmp	.L18
.L22:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L100:
	movl	-56(%rbp), %r13d
	movq	-104(%rbp), %r12
	testl	%r13d, %r13d
	js	.L66
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	salq	$32, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb@PLT
	movq	%rax, %r12
.L67:
	movq	(%r12), %rax
	movl	11(%rax), %esi
	cmpl	$10, %esi
	jg	.L65
	cmpl	$3, 7(%rax)
	jne	.L65
	movl	%r13d, %edi
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movq	(%r12), %rdx
	movl	%eax, 7(%rdx)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movq	%r12, %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rax, %r12
	jmp	.L67
.L97:
	movq	-72(%rbp), %rdi
	call	_ZN2v88internal7JSProxy12GetPrototypeENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L18
	movq	(%rax), %rbx
	cmpq	%rbx, 104(%r12)
	je	.L28
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_121HasEnumerablePropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEE
	jmp	.L18
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20032:
	.size	_ZN2v88internal12_GLOBAL__N_121HasEnumerablePropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal12_GLOBAL__N_121HasEnumerablePropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8772:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L107
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L110
.L101:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L101
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8772:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL30Stats_Runtime_ForInHasPropertyEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC2:
	.string	"V8.Runtime_Runtime_ForInHasProperty"
	.section	.rodata._ZN2v88internalL30Stats_Runtime_ForInHasPropertyEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"args[0].IsJSReceiver()"
.LC4:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL30Stats_Runtime_ForInHasPropertyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_ForInHasPropertyEiPmPNS0_7IsolateE, @function
_ZN2v88internalL30Stats_Runtime_ForInHasPropertyEiPmPNS0_7IsolateE:
.LFB20036:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L148
.L112:
	movq	_ZZN2v88internalL30Stats_Runtime_ForInHasPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic124(%rip), %rbx
	testq	%rbx, %rbx
	je	.L149
.L114:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L150
.L116:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L120
.L121:
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L149:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L151
.L115:
	movq	%rbx, _ZZN2v88internalL30Stats_Runtime_ForInHasPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic124(%rip)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L120:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L121
	leaq	-8(%r13), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_121HasEnumerablePropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEE
	testq	%rax, %rax
	je	.L152
	movq	88(%r12), %rcx
	cmpq	%rcx, (%rax)
	je	.L124
	movq	112(%r12), %r13
.L123:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L128
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L128:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L153
.L111:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L154
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movq	120(%r12), %r13
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L150:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L155
.L117:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L118
	movq	(%rdi), %rax
	call	*8(%rax)
.L118:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L119
	movq	(%rdi), %rax
	call	*8(%rax)
.L119:
	leaq	.LC2(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L152:
	movq	312(%r12), %r13
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L148:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$294, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L153:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L151:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L155:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC2(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L117
.L154:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20036:
	.size	_ZN2v88internalL30Stats_Runtime_ForInHasPropertyEiPmPNS0_7IsolateE, .-_ZN2v88internalL30Stats_Runtime_ForInHasPropertyEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL28Stats_Runtime_ForInEnumerateEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"V8.Runtime_Runtime_ForInEnumerate"
	.section	.text._ZN2v88internalL28Stats_Runtime_ForInEnumerateEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_ForInEnumerateEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL28Stats_Runtime_ForInEnumerateEiPmPNS0_7IsolateE.isra.0:
.LFB25001:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L192
.L157:
	movq	_ZZN2v88internalL28Stats_Runtime_ForInEnumerateEiPmPNS0_7IsolateEE28trace_event_unique_atomic116(%rip), %rbx
	testq	%rbx, %rbx
	je	.L193
.L159:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L194
.L161:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L195
.L165:
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L193:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L196
.L160:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_ForInEnumerateEiPmPNS0_7IsolateEE28trace_event_unique_atomic116(%rip)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L195:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L165
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_19EnumerateEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE
	testq	%rax, %rax
	je	.L197
	movq	(%rax), %r13
.L168:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L171
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L171:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L198
.L156:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L199
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L200
.L162:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L163
	movq	(%rdi), %rax
	call	*8(%rax)
.L163:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L164
	movq	(%rdi), %rax
	call	*8(%rax)
.L164:
	leaq	.LC5(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L197:
	movq	312(%r12), %r13
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L192:
	movq	40960(%rsi), %rax
	movl	$293, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L198:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L196:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L200:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L162
.L199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25001:
	.size	_ZN2v88internalL28Stats_Runtime_ForInEnumerateEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL28Stats_Runtime_ForInEnumerateEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal22Runtime_ForInEnumerateEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_ForInEnumerateEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_ForInEnumerateEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_ForInEnumerateEiPmPNS0_7IsolateE:
.LFB20034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L211
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	testb	$1, %al
	jne	.L212
.L203:
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L212:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L203
	movq	%rdx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_19EnumerateEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE
	testq	%rax, %rax
	je	.L213
	movq	(%rax), %r14
.L206:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L201
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L201:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L211:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL28Stats_Runtime_ForInEnumerateEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20034:
	.size	_ZN2v88internal22Runtime_ForInEnumerateEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_ForInEnumerateEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_ForInHasPropertyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_ForInHasPropertyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_ForInHasPropertyEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_ForInHasPropertyEiPmPNS0_7IsolateE:
.LFB20037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L225
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	testb	$1, %al
	jne	.L216
.L217:
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L216:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L217
	leaq	-8(%rsi), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_121HasEnumerablePropertyEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6ObjectEEE
	testq	%rax, %rax
	je	.L226
	movq	88(%r12), %rcx
	cmpq	%rcx, (%rax)
	je	.L220
	movq	112(%r12), %r14
.L219:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L214
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L214:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	movq	120(%r12), %r14
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L226:
	movq	312(%r12), %r14
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L225:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL30Stats_Runtime_ForInHasPropertyEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20037:
	.size	_ZN2v88internal24Runtime_ForInHasPropertyEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_ForInHasPropertyEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal22Runtime_ForInEnumerateEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal22Runtime_ForInEnumerateEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal22Runtime_ForInEnumerateEiPmPNS0_7IsolateE:
.LFB24979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24979:
	.size	_GLOBAL__sub_I__ZN2v88internal22Runtime_ForInEnumerateEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal22Runtime_ForInEnumerateEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal22Runtime_ForInEnumerateEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL30Stats_Runtime_ForInHasPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic124,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_ForInHasPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic124, @object
	.size	_ZZN2v88internalL30Stats_Runtime_ForInHasPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic124, 8
_ZZN2v88internalL30Stats_Runtime_ForInHasPropertyEiPmPNS0_7IsolateEE28trace_event_unique_atomic124:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_ForInEnumerateEiPmPNS0_7IsolateEE28trace_event_unique_atomic116,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_ForInEnumerateEiPmPNS0_7IsolateEE28trace_event_unique_atomic116, @object
	.size	_ZZN2v88internalL28Stats_Runtime_ForInEnumerateEiPmPNS0_7IsolateEE28trace_event_unique_atomic116, 8
_ZZN2v88internalL28Stats_Runtime_ForInEnumerateEiPmPNS0_7IsolateEE28trace_event_unique_atomic116:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
