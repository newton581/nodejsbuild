	.file	"strings-storage.cc"
	.text
	.section	.text._ZN2v88internal14StringsStorage12StringsMatchEPvS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14StringsStorage12StringsMatchEPvS2_
	.type	_ZN2v88internal14StringsStorage12StringsMatchEPvS2_, @function
_ZN2v88internal14StringsStorage12StringsMatchEPvS2_:
.LFB17782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	strcmp@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE17782:
	.size	_ZN2v88internal14StringsStorage12StringsMatchEPvS2_, .-_ZN2v88internal14StringsStorage12StringsMatchEPvS2_
	.section	.rodata._ZN2v88internal14StringsStorageC2Ev.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Out of memory: HashMap::Initialize"
	.section	.text._ZN2v88internal14StringsStorageC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14StringsStorageC2Ev
	.type	_ZN2v88internal14StringsStorageC2Ev, @function
_ZN2v88internal14StringsStorageC2Ev:
.LFB17787:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal14StringsStorage12StringsMatchEPvS2_(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	movl	$192, %edi
	call	malloc@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L7
	movq	$0, (%rax)
	movq	$0, 24(%rax)
	movq	$0, 48(%rax)
	movq	$0, 72(%rax)
	movq	$0, 96(%rax)
	movq	$0, 120(%rax)
	movq	$0, 144(%rax)
	movq	$0, 168(%rax)
	movq	$8, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L7:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17787:
	.size	_ZN2v88internal14StringsStorageC2Ev, .-_ZN2v88internal14StringsStorageC2Ev
	.globl	_ZN2v88internal14StringsStorageC1Ev
	.set	_ZN2v88internal14StringsStorageC1Ev,_ZN2v88internal14StringsStorageC2Ev
	.section	.text._ZN2v88internal14StringsStorageD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14StringsStorageD2Ev
	.type	_ZN2v88internal14StringsStorageD2Ev, @function
_ZN2v88internal14StringsStorageD2Ev:
.LFB17790:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %esi
	movq	(%rdi), %r8
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%r8,%rax,8), %rcx
	cmpq	%rcx, %r8
	jnb	.L21
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L25:
	cmpq	%rax, %rcx
	jbe	.L9
.L11:
	movq	(%rax), %rdx
	movq	%rax, %rbx
	addq	$24, %rax
	testq	%rdx, %rdx
	je	.L25
	.p2align 4,,10
	.p2align 3
.L10:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L12
	call	_ZdaPv@PLT
	movq	(%r12), %r8
	movl	8(%r12), %esi
.L12:
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%r8,%rax,8), %rcx
	leaq	24(%rbx), %rax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L26:
	movq	(%rax), %rdx
	movq	%rax, %rbx
	addq	$24, %rax
	testq	%rdx, %rdx
	jne	.L10
.L24:
	cmpq	%rax, %rcx
	ja	.L26
.L9:
	popq	%rbx
	.cfi_restore 3
	movq	%r8, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	free@PLT
.L21:
	movq	%r8, %rdi
	jmp	free@PLT
	.cfi_endproc
.LFE17790:
	.size	_ZN2v88internal14StringsStorageD2Ev, .-_ZN2v88internal14StringsStorageD2Ev
	.globl	_ZN2v88internal14StringsStorageD1Ev
	.set	_ZN2v88internal14StringsStorageD1Ev,_ZN2v88internal14StringsStorageD2Ev
	.section	.text._ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS0_23DefaultAllocationPolicyEE6ResizeES7_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS0_23DefaultAllocationPolicyEE6ResizeES7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS0_23DefaultAllocationPolicyEE6ResizeES7_
	.type	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS0_23DefaultAllocationPolicyEE6ResizeES7_, @function
_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS0_23DefaultAllocationPolicyEE6ResizeES7_:
.LFB21368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	%rax, -64(%rbp)
	movl	12(%rdi), %eax
	movl	%eax, -52(%rbp)
	movl	8(%rdi), %eax
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rdi
	movq	%rax, %rbx
	salq	$3, %rdi
	call	malloc@PLT
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L62
	movl	%ebx, 8(%r14)
	testl	%ebx, %ebx
	je	.L29
	movq	$0, (%rax)
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L30:
	movq	(%r14), %rcx
	addq	$1, %rax
	movq	$0, (%rcx,%rdx)
	movl	8(%r14), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L30
.L29:
	movl	-52(%rbp), %eax
	movq	-64(%rbp), %r13
	movl	$0, 12(%r14)
	testl	%eax, %eax
	je	.L37
	.p2align 4,,10
	.p2align 3
.L31:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L32
	movl	8(%r14), %ecx
	movl	16(%r13), %r12d
	movq	(%r14), %r9
	subl	$1, %ecx
	movl	%ecx, %r15d
	andl	%r12d, %r15d
	leaq	(%r15,%r15,2), %rbx
	salq	$3, %rbx
	leaq	(%r9,%rbx), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L33
.L39:
	cmpl	16(%rax), %r12d
	je	.L63
.L34:
	addq	$1, %r15
	andl	%ecx, %r15d
	leaq	(%r15,%r15,2), %rbx
	salq	$3, %rbx
	leaq	(%r9,%rbx), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L39
	movl	16(%r13), %r12d
.L33:
	movq	8(%r13), %rcx
	movq	%rdi, (%rax)
	movl	%r12d, 16(%rax)
	movq	%rcx, 8(%rax)
	movl	12(%r14), %eax
	addl	$1, %eax
	movl	%eax, %ecx
	movl	%eax, 12(%r14)
	shrl	$2, %ecx
	addl	%ecx, %eax
	cmpl	8(%r14), %eax
	jnb	.L36
.L40:
	addq	$24, %r13
	subl	$1, -52(%rbp)
	jne	.L31
.L37:
	movq	-64(%rbp), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	call	*16(%r14)
	testb	%al, %al
	jne	.L35
	movl	8(%r14), %ecx
	movq	(%r14), %r9
	movq	0(%r13), %rdi
	subl	$1, %ecx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L32:
	addq	$24, %r13
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L35:
	movq	(%r14), %rax
	movl	16(%r13), %r12d
	movq	0(%r13), %rdi
	addq	%rbx, %rax
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%r14, %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS0_23DefaultAllocationPolicyEE6ResizeES7_
	movl	8(%r14), %ecx
	movq	(%r14), %rdi
	subl	$1, %ecx
	movl	%ecx, %ebx
	andl	%r12d, %ebx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rdi,%rax,8), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L40
	cmpl	%r12d, 16(%rax)
	je	.L64
	.p2align 4,,10
	.p2align 3
.L41:
	addq	$1, %rbx
	andl	%ecx, %ebx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rdi,%rax,8), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L40
	cmpl	%r12d, 16(%rax)
	jne	.L41
.L64:
	movq	0(%r13), %rdi
	call	*16(%r14)
	testb	%al, %al
	jne	.L40
	movl	8(%r14), %ecx
	movq	(%r14), %rdi
	subl	$1, %ecx
	jmp	.L41
.L62:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21368:
	.size	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS0_23DefaultAllocationPolicyEE6ResizeES7_, .-_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS0_23DefaultAllocationPolicyEE6ResizeES7_
	.section	.text._ZN2v88internal14StringsStorage8GetEntryEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14StringsStorage8GetEntryEPKci
	.type	_ZN2v88internal14StringsStorage8GetEntryEPKci, @function
_ZN2v88internal14StringsStorage8GetEntryEPKci:
.LFB17802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-1(%rdx), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpl	$9, %eax
	ja	.L66
	movsbl	(%rsi), %edi
	leal	-48(%rdi), %ecx
	cmpl	$9, %ecx
	jbe	.L102
.L67:
	movslq	%edx, %rbx
	addq	%r14, %rbx
	cmpq	%rbx, %r14
	je	.L85
	movq	%r14, %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L74:
	movsbw	(%rcx), %dx
	addq	$1, %rcx
	movzwl	%dx, %edx
	addl	%eax, %edx
	movl	%edx, %eax
	sall	$10, %eax
	addl	%edx, %eax
	movl	%eax, %edx
	shrl	$6, %edx
	xorl	%edx, %eax
	cmpq	%rcx, %rbx
	jne	.L74
	leal	(%rax,%rax,8), %ebx
	movl	%ebx, %eax
	shrl	$11, %eax
	xorl	%ebx, %eax
	movl	%eax, %ebx
	sall	$15, %ebx
	addl	%ebx, %eax
	movl	%eax, %ecx
	andl	$1073741823, %ecx
	leal	-1(%rcx), %ebx
	sarl	$31, %ebx
	andl	$27, %ebx
	orl	%eax, %ebx
	leal	2(,%rbx,4), %ebx
.L70:
	movl	8(%r12), %edx
	movq	(%r12), %rcx
	subl	$1, %edx
	movl	%edx, %r15d
	andl	%ebx, %r15d
.L100:
	leaq	(%r15,%r15,2), %r13
	salq	$3, %r13
	leaq	(%rcx,%r13), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L79
	cmpl	16(%rax), %ebx
	je	.L103
.L76:
	addq	$1, %r15
	andl	%edx, %r15d
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%r14, %rdi
	call	*16(%r12)
	testb	%al, %al
	jne	.L77
	movl	8(%r12), %edx
	movq	(%r12), %rcx
	subl	$1, %edx
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L77:
	movq	(%r12), %rax
	addq	%r13, %rax
	cmpq	$0, (%rax)
	je	.L79
.L65:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	leal	2(,%rdx,4), %ebx
	cmpl	$16383, %edx
	jg	.L70
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L102:
	cmpb	$48, %dil
	jne	.L86
	cmpl	$1, %edx
	jne	.L67
.L86:
	subl	$48, %edi
	cmpl	$1, %edx
	je	.L71
	leal	-2(%rdx), %eax
	leaq	1(%r14), %rcx
	movl	$429496729, %r8d
	leaq	2(%r14,%rax), %r9
.L72:
	movsbl	(%rcx), %eax
	leal	-48(%rax), %esi
	cmpl	$9, %esi
	ja	.L67
	subl	$45, %eax
	movl	%r8d, %ebx
	sarl	$3, %eax
	subl	%eax, %ebx
	cmpl	%ebx, %edi
	ja	.L67
	leal	(%rdi,%rdi,4), %eax
	addq	$1, %rcx
	leal	(%rsi,%rax,2), %edi
	cmpq	%rcx, %r9
	jne	.L72
.L71:
	movl	%edx, %esi
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movl	%eax, %ebx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%r14, (%rax)
	movq	$0, 8(%rax)
	movl	%ebx, 16(%rax)
	movl	12(%r12), %edi
	leal	1(%rdi), %edx
	movl	%edx, %ecx
	movl	%edx, 12(%r12)
	shrl	$2, %ecx
	addl	%ecx, %edx
	cmpl	8(%r12), %edx
	jb	.L65
	movq	%r12, %rdi
	call	_ZN2v84base19TemplateHashMapImplIPvS2_NS0_26HashEqualityThenKeyMatcherIS2_PFbS2_S2_EEENS0_23DefaultAllocationPolicyEE6ResizeES7_
	movl	8(%r12), %edx
	movq	(%r12), %rcx
	subl	$1, %edx
	movl	%edx, %r15d
	andl	%ebx, %r15d
.L101:
	leaq	(%r15,%r15,2), %r13
	salq	$3, %r13
	leaq	(%rcx,%r13), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L65
	cmpl	16(%rax), %ebx
	je	.L104
.L82:
	addq	$1, %r15
	andl	%edx, %r15d
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L104:
	movq	%r14, %rdi
	call	*16(%r12)
	testb	%al, %al
	jne	.L83
	movl	8(%r12), %edx
	movq	(%r12), %rcx
	subl	$1, %edx
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L83:
	movq	(%r12), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	addq	%r13, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movl	$110, %ebx
	jmp	.L70
	.cfi_endproc
.LFE17802:
	.size	_ZN2v88internal14StringsStorage8GetEntryEPKci, .-_ZN2v88internal14StringsStorage8GetEntryEPKci
	.section	.rodata._ZN2v88internal14StringsStorage7GetCopyEPKc.str1.1,"aMS",@progbits,1
.LC1:
	.string	"NewArray"
	.section	.text._ZN2v88internal14StringsStorage7GetCopyEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14StringsStorage7GetCopyEPKc
	.type	_ZN2v88internal14StringsStorage7GetCopyEPKc, @function
_ZN2v88internal14StringsStorage7GetCopyEPKc:
.LFB17792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	call	strlen@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	movl	%eax, %edx
	movq	%rax, %rbx
	call	_ZN2v88internal14StringsStorage8GetEntryEPKci
	movq	%rax, %r13
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L109
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	leal	1(%rbx), %r14d
	leaq	_ZSt7nothrow(%rip), %rsi
	movslq	%r14d, %r14
	movq	%r14, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L110
.L107:
	movslq	%ebx, %rbx
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rcx
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal7StrNCpyENS0_6VectorIcEEPKcm@PLT
	movq	-40(%rbp), %rax
	movq	%rax, %xmm0
	movb	$0, (%rax,%rbx)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L110:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r14, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L107
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE17792:
	.size	_ZN2v88internal14StringsStorage7GetCopyEPKc, .-_ZN2v88internal14StringsStorage7GetCopyEPKc
	.section	.text._ZN2v88internal14StringsStorage18AddOrDisposeStringEPci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14StringsStorage18AddOrDisposeStringEPci
	.type	_ZN2v88internal14StringsStorage18AddOrDisposeStringEPci, @function
_ZN2v88internal14StringsStorage18AddOrDisposeStringEPci:
.LFB17795:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	call	_ZN2v88internal14StringsStorage8GetEntryEPKci
	movq	%rax, %rbx
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L118
	testq	%r12, %r12
	je	.L111
	movq	%r12, %rdi
	call	_ZdaPv@PLT
	movq	8(%rbx), %rax
.L111:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movq	%r12, %xmm0
	movq	%r12, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17795:
	.size	_ZN2v88internal14StringsStorage18AddOrDisposeStringEPci, .-_ZN2v88internal14StringsStorage18AddOrDisposeStringEPci
	.section	.text._ZN2v88internal14StringsStorage12GetFormattedEPKcz,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14StringsStorage12GetFormattedEPKcz
	.type	_ZN2v88internal14StringsStorage12GetFormattedEPKcz, @function
_ZN2v88internal14StringsStorage12GetFormattedEPKcz:
.LFB17794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -208(%rbp)
	movq	%rcx, -200(%rbp)
	movq	%r8, -192(%rbp)
	movq	%r9, -184(%rbp)
	testb	%al, %al
	je	.L120
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movaps	%xmm7, -64(%rbp)
.L120:
	movq	%fs:40, %rax
	movq	%rax, -232(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$1024, %edi
	movq	%rax, -248(%rbp)
	leaq	-224(%rbp), %rax
	movl	$16, -256(%rbp)
	movl	$48, -252(%rbp)
	movq	%rax, -240(%rbp)
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L137
.L121:
	movq	%r13, %rdx
	leaq	-256(%rbp), %rcx
	movq	%r12, %rdi
	movl	$1024, %esi
	call	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag@PLT
	movl	%eax, %edx
	cmpl	$-1, %eax
	je	.L138
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14StringsStorage8GetEntryEPKci
	cmpq	$0, 8(%rax)
	movq	%rax, %rbx
	je	.L139
	movq	%r12, %rdi
	call	_ZdaPv@PLT
	movq	8(%rbx), %r12
.L125:
	movq	-232(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L140
	addq	$216, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZdaPv@PLT
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	%eax, %edx
	movq	%rax, %rbx
	call	_ZN2v88internal14StringsStorage8GetEntryEPKci
	movq	8(%rax), %r12
	movq	%rax, %r14
	testq	%r12, %r12
	jne	.L125
	leal	1(%rbx), %r15d
	leaq	_ZSt7nothrow(%rip), %rsi
	movslq	%r15d, %r15
	movq	%r15, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L141
.L123:
	movslq	%ebx, %rbx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rcx
	call	_ZN2v88internal7StrNCpyENS0_6VectorIcEEPKcm@PLT
	movq	%r12, %xmm0
	movb	$0, (%r12,%rbx)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%r12, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	jmp	.L125
.L140:
	call	__stack_chk_fail@PLT
.L137:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$1024, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L121
.L124:
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L141:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r15, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L123
	jmp	.L124
	.cfi_endproc
.LFE17794:
	.size	_ZN2v88internal14StringsStorage12GetFormattedEPKcz, .-_ZN2v88internal14StringsStorage12GetFormattedEPKcz
	.section	.rodata._ZN2v88internal14StringsStorage7GetNameEi.str1.1,"aMS",@progbits,1
.LC2:
	.string	"%d"
	.section	.text._ZN2v88internal14StringsStorage7GetNameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14StringsStorage7GetNameEi
	.type	_ZN2v88internal14StringsStorage7GetNameEi, @function
_ZN2v88internal14StringsStorage7GetNameEi:
.LFB17800:
	.cfi_startproc
	endbr64
	movl	%esi, %edx
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rsi
	jmp	_ZN2v88internal14StringsStorage12GetFormattedEPKcz
	.cfi_endproc
.LFE17800:
	.size	_ZN2v88internal14StringsStorage7GetNameEi, .-_ZN2v88internal14StringsStorage7GetNameEi
	.section	.rodata._ZN2v88internal14StringsStorage7GetNameENS0_4NameE.str1.1,"aMS",@progbits,1
.LC3:
	.string	""
.LC4:
	.string	"<symbol>"
	.section	.text._ZN2v88internal14StringsStorage7GetNameENS0_4NameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE
	.type	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE, @function
_ZN2v88internal14StringsStorage7GetNameENS0_4NameE:
.LFB17797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L144
	leaq	-60(%rbp), %rax
	movq	%rdi, %r12
	movq	%rsi, -56(%rbp)
	leaq	-48(%rbp), %rdi
	movl	_ZN2v88internal31FLAG_heap_snapshot_string_limitE(%rip), %r9d
	cmpl	%r9d, 11(%rsi)
	movl	$1, %edx
	movl	$0, -60(%rbp)
	cmovle	11(%rsi), %r9d
	subq	$8, %rsp
	leaq	-56(%rbp), %rsi
	xorl	%r8d, %r8d
	pushq	%rax
	xorl	%ecx, %ecx
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi@PLT
	movq	-48(%rbp), %r13
	movl	-60(%rbp), %edx
	movq	%r12, %rdi
	movq	$0, -48(%rbp)
	movq	%r13, %rsi
	call	_ZN2v88internal14StringsStorage8GetEntryEPKci
	movq	8(%rax), %r12
	movq	%rax, %rbx
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L158
	testq	%r13, %r13
	je	.L146
	movq	%r13, %rdi
	call	_ZdaPv@PLT
	movq	8(%rbx), %r12
.L146:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L143
	call	_ZdaPv@PLT
.L143:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L159
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	leaq	.LC3(%rip), %r12
	cmpw	$64, 11(%rax)
	leaq	.LC4(%rip), %rax
	cmove	%rax, %r12
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L158:
	movq	%r13, %xmm0
	movq	%r13, %r12
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	jmp	.L146
.L159:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17797:
	.size	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE, .-_ZN2v88internal14StringsStorage7GetNameENS0_4NameE
	.section	.text._ZN2v88internal14StringsStorage13GetVFormattedEPKcP13__va_list_tag,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14StringsStorage13GetVFormattedEPKcP13__va_list_tag
	.type	_ZN2v88internal14StringsStorage13GetVFormattedEPKcP13__va_list_tag, @function
_ZN2v88internal14StringsStorage13GetVFormattedEPKcP13__va_list_tag:
.LFB17796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$1024, %edi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	_ZSt7nothrow(%rip), %rsi
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L177
	movq	%rax, %r12
.L161:
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movl	$1024, %esi
	call	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag@PLT
	movl	%eax, %edx
	cmpl	$-1, %eax
	je	.L178
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14StringsStorage8GetEntryEPKci
	cmpq	$0, 8(%rax)
	movq	%rax, %r13
	je	.L179
	movq	%r12, %rdi
	call	_ZdaPv@PLT
	movq	8(%r13), %r12
.L170:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZdaPv@PLT
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	%eax, %edx
	movq	%rax, %r15
	call	_ZN2v88internal14StringsStorage8GetEntryEPKci
	movq	8(%rax), %r12
	movq	%rax, %r14
	testq	%r12, %r12
	jne	.L170
	leal	1(%r15), %ebx
	leaq	_ZSt7nothrow(%rip), %rsi
	movslq	%ebx, %rbx
	movq	%rbx, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L180
.L163:
	movslq	%r15d, %r15
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rcx
	call	_ZN2v88internal7StrNCpyENS0_6VectorIcEEPKcm@PLT
	movq	%r12, %xmm0
	movb	$0, (%r12,%r15)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L179:
	movq	%r12, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	jmp	.L170
.L177:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$1024, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L161
.L164:
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L180:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L163
	jmp	.L164
	.cfi_endproc
.LFE17796:
	.size	_ZN2v88internal14StringsStorage13GetVFormattedEPKcP13__va_list_tag, .-_ZN2v88internal14StringsStorage13GetVFormattedEPKcP13__va_list_tag
	.section	.rodata._ZN2v88internal14StringsStorage11GetConsNameEPKcNS0_4NameE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"%s%s"
	.section	.text._ZN2v88internal14StringsStorage11GetConsNameEPKcNS0_4NameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14StringsStorage11GetConsNameEPKcNS0_4NameE
	.type	_ZN2v88internal14StringsStorage11GetConsNameEPKcNS0_4NameE, @function
_ZN2v88internal14StringsStorage11GetConsNameEPKcNS0_4NameE:
.LFB17801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	ja	.L182
	leaq	-76(%rbp), %rax
	movq	%rdx, -72(%rbp)
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movl	_ZN2v88internal31FLAG_heap_snapshot_string_limitE(%rip), %r9d
	cmpl	%r9d, 11(%rdx)
	leaq	-72(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	cmovle	11(%rdx), %r9d
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	pushq	%rax
	movl	$1, %edx
	movl	$0, -76(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEiiPi@PLT
	movq	%rbx, %rdi
	call	strlen@PLT
	movl	-76(%rbp), %r14d
	leaq	_ZSt7nothrow(%rip), %rsi
	addl	%eax, %r14d
	addl	$1, %r14d
	movslq	%r14d, %r15
	movq	%r15, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L196
.L183:
	subq	$8, %rsp
	pushq	-64(%rbp)
	movq	%rbx, %r9
	movl	$1, %edx
	leaq	.LC5(%rip), %r8
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	movq	$-1, %rcx
	call	__snprintf_chk@PLT
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14StringsStorage8GetEntryEPKci
	movq	%rax, %rbx
	popq	%rax
	popq	%rdx
	cmpq	$0, 8(%rbx)
	jne	.L197
	movq	%r12, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
.L184:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L181
	call	_ZdaPv@PLT
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L182:
	movq	-1(%rdx), %rax
	leaq	.LC3(%rip), %r12
	cmpw	$64, 11(%rax)
	leaq	.LC4(%rip), %rax
	cmove	%rax, %r12
.L181:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L198
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZdaPv@PLT
	movq	8(%rbx), %r12
	jmp	.L184
.L196:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r15, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L183
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L198:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17801:
	.size	_ZN2v88internal14StringsStorage11GetConsNameEPKcNS0_4NameE, .-_ZN2v88internal14StringsStorage11GetConsNameEPKcNS0_4NameE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal14StringsStorage12StringsMatchEPvS2_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal14StringsStorage12StringsMatchEPvS2_, @function
_GLOBAL__sub_I__ZN2v88internal14StringsStorage12StringsMatchEPvS2_:
.LFB21545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21545:
	.size	_GLOBAL__sub_I__ZN2v88internal14StringsStorage12StringsMatchEPvS2_, .-_GLOBAL__sub_I__ZN2v88internal14StringsStorage12StringsMatchEPvS2_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal14StringsStorage12StringsMatchEPvS2_
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
