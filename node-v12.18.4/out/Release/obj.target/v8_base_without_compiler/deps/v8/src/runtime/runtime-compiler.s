	.file	"runtime-compiler.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5104:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5104:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5105:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5105:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5107:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5107:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internalL17CompileGlobalEvalEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS3_INS0_18SharedFunctionInfoEEENS0_12LanguageModeEii,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL17CompileGlobalEvalEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS3_INS0_18SharedFunctionInfoEEENS0_12LanguageModeEii, @function
_ZN2v88internalL17CompileGlobalEvalEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS3_INS0_18SharedFunctionInfoEEENS0_12LanguageModeEii:
.LFB19213:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	%ecx, -68(%rbp)
	movq	12464(%rdi), %rsi
	movl	%r8d, -72(%rbp)
	movq	41112(%rdi), %rdi
	movl	%r9d, -76(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L6
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L7:
	movq	39(%rsi), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L9
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L10:
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal8Compiler32ValidateDynamicCompilationSourceEPNS0_7IsolateENS0_6HandleINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movq	%rax, %rdi
	testb	%dl, %dl
	jne	.L23
	testq	%rax, %rax
	je	.L24
	movl	-76(%rbp), %eax
	movzbl	-68(%rbp), %ecx
	movq	%r13, %rdx
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	pushq	%rax
	movl	-72(%rbp), %eax
	pushq	%rax
	call	_ZN2v88internal8Compiler19GetFunctionFromEvalENS0_6HandleINS0_6StringEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_12LanguageModeENS0_16ParseRestrictionEiii@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L22
	movq	(%rax), %rax
.L13:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L25
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	(%r14), %rax
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context40ErrorMessageForCodeGenerationFromStringsEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$337, %esi
	movq	%rax, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory12NewEvalErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	testq	%rax, %rax
	je	.L22
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L22:
	movq	312(%rbx), %rax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L9:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L26
.L11:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L6:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L27
.L8:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L23:
	movq	(%r14), %rax
	movq	1631(%rax), %rax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%rbx, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%rbx, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L11
.L25:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19213:
	.size	_ZN2v88internalL17CompileGlobalEvalEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS3_INS0_18SharedFunctionInfoEEENS0_12LanguageModeEii, .-_ZN2v88internalL17CompileGlobalEvalEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS3_INS0_18SharedFunctionInfoEEENS0_12LanguageModeEii
	.section	.text._ZN2v88internalL31IsSuitableForOnStackReplacementEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31IsSuitableForOnStackReplacementEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internalL31IsSuitableForOnStackReplacementEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE:
.LFB19205:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1456, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movl	47(%rax), %edx
	xorl	%eax, %eax
	andl	$15728640, %edx
	je	.L41
.L28:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L42
	addq	$1456, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	leaq	-1488(%rbp), %rbx
	movq	%rsi, %r13
	movq	%rdi, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -72(%rbp)
	je	.L30
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%rbx, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	je	.L30
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	cmpl	$4, %eax
	jne	.L40
	movq	(%r12), %rax
	movq	0(%r13), %r14
	movq	%r12, %rdi
	call	*152(%rax)
	cmpq	%r14, %rax
	jne	.L40
	xorl	%eax, %eax
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$1, %eax
	jmp	.L28
.L42:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19205:
	.size	_ZN2v88internalL31IsSuitableForOnStackReplacementEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internalL31IsSuitableForOnStackReplacementEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB9722:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L49
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L52
.L43:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L43
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE9722:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL36Stats_Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC1:
	.string	"V8.Runtime_Runtime_EvictOptimizedCodeSlot"
	.section	.rodata._ZN2v88internalL36Stats_Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"args[0].IsJSFunction()"
.LC3:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internalL36Stats_Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateE.str1.8
	.align 8
.LC4:
	.string	"Runtime_EvictOptimizedCodeSlot"
	.section	.text._ZN2v88internalL36Stats_Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateE, @function
_ZN2v88internalL36Stats_Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateE:
.LFB19193:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L84
.L54:
	movq	_ZZN2v88internalL36Stats_Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateEE27trace_event_unique_atomic97(%rip), %rbx
	testq	%rbx, %rbx
	je	.L85
.L56:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L86
.L58:
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L62
.L63:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L85:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L87
.L57:
	movq	%rbx, _ZZN2v88internalL36Stats_Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateEE27trace_event_unique_atomic97(%rip)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L62:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L63
	movq	39(%rax), %rdx
	leaq	-152(%rbp), %rdi
	movq	7(%rdx), %rdx
	movq	%rdx, -152(%rbp)
	movq	23(%rax), %rsi
	leaq	.LC4(%rip), %rdx
	call	_ZN2v88internal14FeedbackVector41EvictOptimizedCodeMarkedForDeoptimizationENS0_18SharedFunctionInfoEPKc@PLT
	movq	(%r12), %rax
	leaq	-144(%rbp), %rdi
	movq	47(%rax), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L88
.L53:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L90
.L59:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L60
	movq	(%rdi), %rax
	call	*8(%rax)
.L60:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L61
	movq	(%rdi), %rax
	call	*8(%rax)
.L61:
	leaq	.LC1(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L84:
	movq	40960(%rdx), %rdi
	leaq	-104(%rbp), %rsi
	movl	$259, %edx
	addq	$23240, %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L88:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L90:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L59
.L89:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19193:
	.size	_ZN2v88internalL36Stats_Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateE, .-_ZN2v88internalL36Stats_Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL25Stats_Runtime_CompileLazyEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"V8.Runtime_Runtime_CompileLazy"
	.section	.text._ZN2v88internalL25Stats_Runtime_CompileLazyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Stats_Runtime_CompileLazyEiPmPNS0_7IsolateE, @function
_ZN2v88internalL25Stats_Runtime_CompileLazyEiPmPNS0_7IsolateE:
.LFB19181:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$144, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L127
.L92:
	movq	_ZZN2v88internalL25Stats_Runtime_CompileLazyEiPmPNS0_7IsolateEE27trace_event_unique_atomic22(%rip), %rbx
	testq	%rbx, %rbx
	je	.L128
.L94:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L129
.L96:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L100
.L101:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L128:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L130
.L95:
	movq	%rbx, _ZZN2v88internalL25Stats_Runtime_CompileLazyEiPmPNS0_7IsolateEE27trace_event_unique_atomic22(%rip)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L100:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L101
	leaq	-168(%rbp), %rdi
	movl	$40960, %esi
	movq	%r12, -168(%rbp)
	call	_ZNK2v88internal15StackLimitCheck15JsHasOverflowedEm@PLT
	testb	%al, %al
	je	.L102
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	movq	%rax, %r13
.L103:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L107
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L107:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L131
.L91:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L132
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	-160(%rbp), %rdx
	movq	$0, -160(%rbp)
	movb	$0, -152(%rbp)
	call	_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_10JSFunctionEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE@PLT
	testb	%al, %al
	jne	.L104
	movq	312(%r12), %r13
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L129:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L133
.L97:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L98
	movq	(%rdi), %rax
	call	*8(%rax)
.L98:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L99
	movq	(%rdi), %rax
	call	*8(%rax)
.L99:
	leaq	.LC5(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L104:
	movq	0(%r13), %rax
	movq	47(%rax), %r13
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L127:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$256, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L131:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L130:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L133:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L97
.L132:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19181:
	.size	_ZN2v88internalL25Stats_Runtime_CompileLazyEiPmPNS0_7IsolateE, .-_ZN2v88internalL25Stats_Runtime_CompileLazyEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Stats_Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"V8.Runtime_Runtime_CompileOptimized_Concurrent"
	.section	.text._ZN2v88internalL41Stats_Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Stats_Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateE, @function
_ZN2v88internalL41Stats_Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateE:
.LFB19184:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L170
.L135:
	movq	_ZZN2v88internalL41Stats_Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateEE27trace_event_unique_atomic48(%rip), %rbx
	testq	%rbx, %rbx
	je	.L171
.L137:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L172
.L139:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L143
.L144:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L171:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L173
.L138:
	movq	%rbx, _ZZN2v88internalL41Stats_Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateEE27trace_event_unique_atomic48(%rip)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L143:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L144
	leaq	-152(%rbp), %rdi
	movl	$40960, %esi
	movq	%r12, -152(%rbp)
	call	_ZNK2v88internal15StackLimitCheck15JsHasOverflowedEm@PLT
	testb	%al, %al
	je	.L145
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	movq	%rax, %r13
.L146:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L150
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L150:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L174
.L134:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L175
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8Compiler16CompileOptimizedENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeE@PLT
	testb	%al, %al
	jne	.L147
	movq	312(%r12), %r13
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L172:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L176
.L140:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L141
	movq	(%rdi), %rax
	call	*8(%rax)
.L141:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L142
	movq	(%rdi), %rax
	call	*8(%rax)
.L142:
	leaq	.LC6(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L147:
	movq	0(%r13), %rax
	movq	47(%rax), %r13
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L170:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$257, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L174:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L173:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L176:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L140
.L175:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19184:
	.size	_ZN2v88internalL41Stats_Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateE, .-_ZN2v88internalL41Stats_Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL44Stats_Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"V8.Runtime_Runtime_CompileOptimized_NotConcurrent"
	.section	.text._ZN2v88internalL44Stats_Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Stats_Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateE, @function
_ZN2v88internalL44Stats_Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateE:
.LFB19190:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L213
.L178:
	movq	_ZZN2v88internalL44Stats_Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateEE27trace_event_unique_atomic82(%rip), %rbx
	testq	%rbx, %rbx
	je	.L214
.L180:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L215
.L182:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L186
.L187:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L214:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L216
.L181:
	movq	%rbx, _ZZN2v88internalL44Stats_Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateEE27trace_event_unique_atomic82(%rip)
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L186:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L187
	leaq	-152(%rbp), %rdi
	movl	$40960, %esi
	movq	%r12, -152(%rbp)
	call	_ZNK2v88internal15StackLimitCheck15JsHasOverflowedEm@PLT
	testb	%al, %al
	je	.L188
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	movq	%rax, %r13
.L189:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L193
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L193:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L217
.L177:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L218
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8Compiler16CompileOptimizedENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeE@PLT
	testb	%al, %al
	jne	.L190
	movq	312(%r12), %r13
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L215:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L219
.L183:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L184
	movq	(%rdi), %rax
	call	*8(%rax)
.L184:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L185
	movq	(%rdi), %rax
	call	*8(%rax)
.L185:
	leaq	.LC7(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L190:
	movq	0(%r13), %rax
	movq	47(%rax), %r13
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L213:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$258, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L217:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L216:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L219:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L183
.L218:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19190:
	.size	_ZN2v88internalL44Stats_Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateE, .-_ZN2v88internalL44Stats_Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Stats_Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"V8.Runtime_Runtime_NotifyDeoptimized"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC9:
	.string	"v8"
.LC10:
	.string	"V8.DeoptimizeCode"
	.section	.text._ZN2v88internalL31Stats_Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateE.isra.0:
.LFB23630:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1624, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1568(%rbp)
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L277
.L221:
	movq	_ZZN2v88internalL31Stats_Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateEE28trace_event_unique_atomic148(%rip), %rbx
	testq	%rbx, %rbx
	je	.L278
.L223:
	movq	$0, -1632(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L279
.L225:
	addl	$1, 41104(%r12)
	movq	41088(%r12), %rax
	movq	%r12, %rdi
	leaq	-1640(%rbp), %r14
	movq	41096(%r12), %rbx
	movq	%rax, -1656(%rbp)
	call	_ZN2v88internal11Deoptimizer4GrabEPNS0_7IsolateE@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r12, -1640(%rbp)
	movq	%rax, %r13
	call	_ZN2v88internal15TimerEventScopeINS0_24TimerEventDeoptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE@PLT
	movq	_ZZN2v88internalL35__RT_impl_Runtime_NotifyDeoptimizedENS0_9ArgumentsEPNS0_7IsolateEE28trace_event_unique_atomic158(%rip), %rdx
	movq	%rdx, %r15
	testq	%rdx, %rdx
	je	.L280
.L230:
	movq	$0, -1600(%rbp)
	movzbl	(%r15), %eax
	testb	$5, %al
	jne	.L281
.L232:
	movq	%r13, %rdi
	call	_ZNK2v88internal11Deoptimizer8functionEv@PLT
	movq	%r13, %rdi
	movzbl	28(%r13), %r15d
	movq	%rax, -1664(%rbp)
	call	_ZNK2v88internal11Deoptimizer8functionEv@PLT
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	31(%rax), %rax
	movq	39(%rax), %rax
	movq	%rax, 12464(%r12)
	call	_ZN2v88internal11Deoptimizer22MaterializeHeapObjectsEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal11DeoptimizerD1Ev@PLT
	movq	%r13, %rdi
	leaq	-1520(%rbp), %r13
	call	_ZN2v88internal8MalloceddlEPv@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L236
	movq	%r13, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-104(%rbp), %rdi
.L236:
	movq	(%rdi), %rax
	call	*96(%rax)
	movq	%rax, 12464(%r12)
	cmpb	$2, %r15b
	je	.L237
	movq	-1664(%rbp), %rax
	xorl	%esi, %esi
	movq	(%rax), %rdi
	call	_ZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeE@PLT
.L237:
	leaq	-1600(%rbp), %rdi
	movq	88(%r12), %r13
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal15TimerEventScopeINS0_24TimerEventDeoptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE@PLT
	movq	-1656(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L240
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L240:
	leaq	-1632(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L282
.L220:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L283
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	$0, -1664(%rbp)
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L284
.L233:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L234
	movq	(%rdi), %rax
	call	*8(%rax)
.L234:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L235
	movq	(%rdi), %rax
	call	*8(%rax)
.L235:
	leaq	.LC10(%rip), %rax
	movq	%r15, -1592(%rbp)
	movq	%rax, -1584(%rbp)
	movq	-1664(%rbp), %rax
	movq	%rax, -1576(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%rax, -1600(%rbp)
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L280:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L285
.L231:
	movq	%r15, _ZZN2v88internalL35__RT_impl_Runtime_NotifyDeoptimizedENS0_9ArgumentsEPNS0_7IsolateEE28trace_event_unique_atomic158(%rip)
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L279:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L286
.L226:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L227
	movq	(%rdi), %rax
	call	*8(%rax)
.L227:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L228
	movq	(%rdi), %rax
	call	*8(%rax)
.L228:
	leaq	.LC8(%rip), %rax
	movq	%rbx, -1624(%rbp)
	movq	%rax, -1616(%rbp)
	leaq	-1624(%rbp), %rax
	movq	%r13, -1608(%rbp)
	movq	%rax, -1632(%rbp)
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L278:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L287
.L224:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateEE28trace_event_unique_atomic148(%rip)
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L277:
	movq	40960(%rdi), %rax
	leaq	-1560(%rbp), %rsi
	movl	$262, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -1568(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L282:
	leaq	-1560(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L287:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L286:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L285:
	leaq	.LC9(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L284:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r15, %rdx
	movl	$88, %esi
	pushq	%rcx
	leaq	.LC10(%rip), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, -1664(%rbp)
	addq	$64, %rsp
	jmp	.L233
.L283:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23630:
	.size	_ZN2v88internalL31Stats_Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL36Stats_Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"V8.Runtime_Runtime_FunctionFirstExecution"
	.section	.rodata._ZN2v88internalL36Stats_Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"first-execution"
	.section	.text._ZN2v88internalL36Stats_Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateE, @function
_ZN2v88internalL36Stats_Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateE:
.LFB19187:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L329
.L289:
	movq	_ZZN2v88internalL36Stats_Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateEE27trace_event_unique_atomic63(%rip), %r13
	testq	%r13, %r13
	je	.L330
.L291:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L331
.L293:
	movq	41096(%r12), %rax
	movq	41088(%r12), %r14
	addl	$1, 41104(%r12)
	movq	%rax, -200(%rbp)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L332
.L297:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L330:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L333
.L292:
	movq	%r13, _ZZN2v88internalL36Stats_Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateEE27trace_event_unique_atomic63(%rip)
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L332:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L297
	movq	23(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L334
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L300:
	movq	41016(%r12), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	leaq	-168(%rbp), %r10
	testb	%al, %al
	jne	.L335
.L302:
	movq	(%rbx), %rax
	movq	%r10, %rdi
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal14FeedbackVector23ClearOptimizationMarkerEv@PLT
	movq	(%rbx), %rax
	movq	47(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	-200(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L304
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L304:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L336
.L288:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L337
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	movq	%r14, %r13
	cmpq	41096(%r12), %r14
	je	.L338
.L301:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L331:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L339
.L294:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L295
	movq	(%rdi), %rax
	call	*8(%rax)
.L295:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L296
	movq	(%rdi), %rax
	call	*8(%rax)
.L296:
	leaq	.LC11(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L335:
	movq	0(%r13), %rax
	movq	%r10, %rdi
	movq	%r10, -224(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	leaq	-176(%rbp), %rdi
	movq	%rax, -216(%rbp)
	movq	0(%r13), %rax
	movq	%rax, -176(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	leaq	-184(%rbp), %rdi
	movl	%eax, -208(%rbp)
	movq	0(%r13), %rax
	movq	%rax, -184(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movl	-208(%rbp), %r8d
	movq	-216(%rbp), %r9
	movl	%eax, %ecx
	movq	0(%r13), %rax
	movq	-224(%rbp), %r10
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L340
.L303:
	movslq	67(%rax), %rdx
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	leaq	.LC12(%rip), %rsi
	movq	%r10, -208(%rbp)
	call	_ZN2v88internal6Logger13FunctionEventEPKcidiiNS0_6StringE@PLT
	movq	-208(%rbp), %r10
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L340:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L303
	movq	23(%rax), %rax
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L329:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$260, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L336:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L338:
	movq	%r12, %rdi
	movq	%rsi, -208(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L339:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L333:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L292
.L337:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19187:
	.size	_ZN2v88internalL36Stats_Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateE, .-_ZN2v88internalL36Stats_Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL30Stats_Runtime_InstantiateAsmJsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"V8.Runtime_Runtime_InstantiateAsmJs"
	.section	.text._ZN2v88internalL30Stats_Runtime_InstantiateAsmJsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_InstantiateAsmJsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL30Stats_Runtime_InstantiateAsmJsEiPmPNS0_7IsolateE:
.LFB19196:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L407
.L342:
	movq	_ZZN2v88internalL30Stats_Runtime_InstantiateAsmJsEiPmPNS0_7IsolateEE28trace_event_unique_atomic109(%rip), %r13
	testq	%r13, %r13
	je	.L408
.L344:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L409
.L346:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L410
.L350:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L408:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L411
.L345:
	movq	%r13, _ZZN2v88internalL30Stats_Runtime_InstantiateAsmJsEiPmPNS0_7IsolateEE28trace_event_unique_atomic109(%rip)
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L410:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L350
	movq	-8(%rbx), %rdx
	xorl	%ecx, %ecx
	leaq	-8(%rbx), %rsi
	testb	$1, %dl
	jne	.L412
.L352:
	movq	-16(%rbx), %rdx
	xorl	%r8d, %r8d
	leaq	-16(%rbx), %rsi
	testb	$1, %dl
	jne	.L413
.L355:
	movq	-24(%rbx), %rdx
	xorl	%r9d, %r9d
	leaq	-24(%rbx), %rsi
	testb	$1, %dl
	jne	.L414
.L358:
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L415
.L362:
	movq	(%rbx), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L416
.L373:
	movq	(%rbx), %rax
	movl	$67, %esi
	leaq	41184(%r12), %rdi
	movq	23(%rax), %rdx
	movl	47(%rdx), %eax
	orb	$64, %ah
	movl	%eax, 47(%rdx)
	movq	(%rbx), %r15
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	leaq	47(%r15), %rsi
	movq	%rax, 47(%r15)
	movq	%rax, %rdx
	testb	$1, %al
	je	.L377
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L377
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L377:
	xorl	%r15d, %r15d
.L370:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L381
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L381:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L417
.L341:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L418
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L419
.L347:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L348
	movq	(%rdi), %rax
	call	*8(%rax)
.L348:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L349
	movq	(%rdi), %rax
	call	*8(%rax)
.L349:
	leaq	.LC14(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L415:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L362
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L363
	movq	%r9, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %r8
	movq	(%rax), %rsi
	movq	-184(%rbp), %r9
	movq	%rax, %r15
.L364:
	movq	7(%rsi), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L366
	movq	%r9, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %r8
	movq	-184(%rbp), %r9
	movq	%rax, %rdx
.L367:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5AsmJs18InstantiateAsmWasmEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_11AsmWasmDataEEENS4_INS0_10JSReceiverEEESA_NS4_INS0_13JSArrayBufferEEE@PLT
	testq	%rax, %rax
	je	.L362
	movq	(%rax), %r15
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L414:
	movq	-1(%rdx), %rdx
	cmpw	$1059, 11(%rdx)
	cmove	%rsi, %r9
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L413:
	movq	-1(%rdx), %rdx
	cmpw	$1024, 11(%rdx)
	cmovnb	%rsi, %r8
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L412:
	movq	-1(%rdx), %rdx
	cmpw	$1024, 11(%rdx)
	cmovnb	%rsi, %rcx
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L416:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L373
	movq	(%rbx), %rax
	movq	23(%rax), %r15
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L374
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L375:
	movq	%r12, %rdi
	call	_ZN2v88internal18SharedFunctionInfo15DiscardCompiledEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L366:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L420
.L368:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L363:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L421
.L365:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L374:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L422
.L376:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L407:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$261, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L417:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L411:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L419:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L420:
	movq	%r12, %rdi
	movq	%r9, -192(%rbp)
	movq	%r8, -184(%rbp)
	movq	%rcx, -176(%rbp)
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %r9
	movq	-184(%rbp), %r8
	movq	-176(%rbp), %rcx
	movq	-168(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L421:
	movq	%r12, %rdi
	movq	%r9, -192(%rbp)
	movq	%r8, -184(%rbp)
	movq	%rcx, -176(%rbp)
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %r9
	movq	-184(%rbp), %r8
	movq	-176(%rbp), %rcx
	movq	-168(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L422:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L376
.L418:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19196:
	.size	_ZN2v88internalL30Stats_Runtime_InstantiateAsmJsEiPmPNS0_7IsolateE, .-_ZN2v88internalL30Stats_Runtime_InstantiateAsmJsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Stats_Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"V8.Runtime_Runtime_CompileForOnStackReplacement"
	.section	.rodata._ZN2v88internalL42Stats_Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"FLAG_use_osr"
.LC17:
	.string	"[OSR - Compiling: "
.LC18:
	.string	" at AST id %d]\n"
	.section	.rodata._ZN2v88internalL42Stats_Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateE.str1.8
	.align 8
.LC19:
	.string	"[OSR - Entry at AST id %d, offset %d in optimized code]\n"
	.section	.rodata._ZN2v88internalL42Stats_Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateE.str1.1
.LC20:
	.string	"[OSR - Re-marking "
	.section	.rodata._ZN2v88internalL42Stats_Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateE.str1.8
	.align 8
.LC21:
	.string	" for non-concurrent optimization]\n"
	.section	.rodata._ZN2v88internalL42Stats_Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateE.str1.1
.LC22:
	.string	"[OSR - Failed: "
	.section	.text._ZN2v88internalL42Stats_Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Stats_Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Stats_Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateE:
.LFB19207:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$1608, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1568(%rbp)
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L529
.L424:
	movq	_ZZN2v88internalL42Stats_Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateEE28trace_event_unique_atomic225(%rip), %r13
	testq	%r13, %r13
	je	.L530
.L426:
	movq	$0, -1600(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L531
.L428:
	movq	41088(%r12), %rax
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	%rax, -1624(%rbp)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L532
.L432:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L530:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L533
.L427:
	movq	%r13, _ZZN2v88internalL42Stats_Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateEE28trace_event_unique_atomic225(%rip)
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L532:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L432
	cmpb	$0, _ZN2v88internal12FLAG_use_osrE(%rip)
	je	.L534
	leaq	-1520(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-104(%rbp), %r14
	testq	%r14, %r14
	je	.L435
	movq	%r15, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-104(%rbp), %r14
.L435:
	movq	16(%r14), %r15
	movq	%r14, %rdi
	call	_ZNK2v88internal16InterpretedFrame16GetBytecodeArrayEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L436
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L437:
	movb	$0, 51(%rsi)
	movq	%r14, %rdi
	call	_ZNK2v88internal16InterpretedFrame17GetBytecodeOffsetEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internalL31IsSuitableForOnStackReplacementEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	testb	%al, %al
	je	.L440
	cmpb	$0, _ZN2v88internal14FLAG_trace_osrE(%rip)
	jne	.L535
.L441:
	movq	%r14, %rdx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8Compiler22GetOptimizedCodeForOSRENS0_6HandleINS0_10JSFunctionEEENS0_9BailoutIdEPNS0_15JavaScriptFrameE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L440
	movq	(%rax), %rax
	testb	$62, 43(%rax)
	je	.L536
.L440:
	cmpb	$0, _ZN2v88internal14FLAG_trace_osrE(%rip)
	jne	.L537
.L473:
	movq	(%rbx), %r15
	movq	47(%r15), %rax
	cmpl	$67, 59(%rax)
	jne	.L538
.L479:
	movq	23(%r15), %rax
	leaq	-1608(%rbp), %rdi
	movq	%rax, -1608(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo7GetCodeEv@PLT
	leaq	47(%r15), %rsi
	movq	%rax, 47(%r15)
	movq	%rax, %rdx
	testb	$1, %al
	je	.L481
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L481
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L481:
	xorl	%r15d, %r15d
.L472:
	subl	$1, 41104(%r12)
	movq	-1624(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L485
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L485:
	leaq	-1600(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L539
.L423:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L540
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L541
.L438:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L538:
	movabsq	$287762808832, %rdx
	movq	23(%r15), %rax
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L528
	testb	$1, %al
	jne	.L542
.L476:
	movq	47(%r15), %rax
	testb	$62, 43(%rax)
	je	.L478
.L528:
	movq	(%rbx), %r15
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L531:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L543
.L429:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L430
	movq	(%rdi), %rax
	call	*8(%rax)
.L430:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L431
	movq	(%rdi), %rax
	call	*8(%rax)
.L431:
	leaq	.LC15(%rip), %rax
	movq	%r13, -1592(%rbp)
	movq	%rax, -1584(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%r14, -1576(%rbp)
	movq	%rax, -1600(%rbp)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L537:
	xorl	%eax, %eax
	leaq	.LC22(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%rbx), %rax
	movq	stdout(%rip), %rsi
	leaq	-1608(%rbp), %rdi
	movq	%rax, -1608(%rbp)
	call	_ZN2v88internal10JSFunction9PrintNameEP8_IO_FILE@PLT
	movl	%r15d, %esi
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L478:
	movq	47(%r15), %rax
	xorl	%r15d, %r15d
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	je	.L472
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L542:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L528
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L476
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L535:
	xorl	%eax, %eax
	leaq	.LC17(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%rbx), %rax
	leaq	-1608(%rbp), %r8
	movq	stdout(%rip), %rsi
	movq	%r8, %rdi
	movq	%rax, -1608(%rbp)
	call	_ZN2v88internal10JSFunction9PrintNameEP8_IO_FILE@PLT
	movl	%r15d, %esi
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L536:
	movq	15(%rax), %rax
	movq	47(%rax), %rdx
	testq	%rdx, %rdx
	js	.L440
	cmpb	$0, _ZN2v88internal14FLAG_trace_osrE(%rip)
	jne	.L544
.L444:
	movq	(%rbx), %rax
	movq	39(%rax), %rsi
	leaq	39(%rax), %rdi
	leaq	23(%rax), %rdx
	movq	7(%rsi), %rsi
	cmpl	$1, 35(%rsi)
	jle	.L545
.L453:
	movq	47(%rax), %rsi
	leaq	47(%rax), %rdi
	cmpl	$67, 59(%rsi)
	jne	.L546
.L456:
	movabsq	$287762808832, %rsi
	movq	(%rdx), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rsi, %rdx
	je	.L468
	testb	$1, %dl
	jne	.L462
.L465:
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	je	.L547
.L468:
	movq	(%rbx), %rax
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	cmpl	$1, 35(%rax)
	jle	.L470
	cmpb	$0, _ZN2v88internal14FLAG_trace_osrE(%rip)
	leaq	-1608(%rbp), %r8
	jne	.L548
.L471:
	movl	$2, %esi
	movq	%r8, %rdi
	movq	%rcx, -1632(%rbp)
	movq	%rax, -1608(%rbp)
	call	_ZN2v88internal14FeedbackVector21SetOptimizationMarkerENS0_18OptimizationMarkerE@PLT
	movq	-1632(%rbp), %rcx
.L470:
	movq	(%rcx), %r15
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L529:
	movq	40960(%rdx), %rax
	leaq	-1560(%rbp), %rsi
	movl	$255, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -1568(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L534:
	leaq	.LC16(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L539:
	leaq	-1560(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L543:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC15(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L533:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L541:
	movq	%r15, %rdi
	movq	%rsi, -1632(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1632(%rbp), %rsi
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L546:
	movabsq	$287762808832, %r8
	movq	(%rdx), %rsi
	movq	7(%rsi), %rsi
	cmpq	%r8, %rsi
	je	.L456
	testb	$1, %sil
	jne	.L549
.L457:
	movq	(%rdi), %rsi
	testb	$62, 43(%rsi)
	jne	.L456
	movq	(%rdi), %rsi
	movq	31(%rsi), %rsi
	movl	15(%rsi), %esi
	andl	$1, %esi
	je	.L470
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L544:
	movq	%rcx, -1632(%rbp)
	movq	47(%rax), %rdx
	xorl	%eax, %eax
	movl	%r15d, %esi
	leaq	.LC19(%rip), %rdi
	sarq	$32, %rdx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-1632(%rbp), %rcx
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L545:
	movabsq	$287762808832, %rdx
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L446
	testb	$1, %al
	jne	.L447
.L450:
	movq	(%rdi), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	je	.L550
.L446:
	movq	(%rbx), %rax
.L527:
	leaq	23(%rax), %rdx
	jmp	.L453
.L547:
	movq	15(%rax), %rax
	testb	$1, %al
	je	.L468
	cmpl	$3, %eax
	je	.L468
	andq	$-3, %rax
	je	.L468
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	je	.L470
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L550:
	movq	15(%rax), %rdx
	movq	(%rbx), %rax
	testb	$1, %dl
	jne	.L527
	sarq	$32, %rdx
	cmpq	$1, %rdx
	jbe	.L527
	movq	39(%rax), %rax
	leaq	-1608(%rbp), %r8
	movq	%rcx, -1632(%rbp)
	movq	%r8, %rdi
	movq	7(%rax), %rax
	movq	%rax, -1608(%rbp)
	call	_ZN2v88internal14FeedbackVector23ClearOptimizationMarkerEv@PLT
	movq	(%rbx), %rax
	movq	-1632(%rbp), %rcx
	leaq	23(%rax), %rdx
	jmp	.L453
.L549:
	movq	-1(%rsi), %r8
	cmpw	$165, 11(%r8)
	je	.L456
	movq	-1(%rsi), %rsi
	cmpw	$166, 11(%rsi)
	jne	.L457
	jmp	.L456
.L462:
	movq	-1(%rdx), %rsi
	cmpw	$165, 11(%rsi)
	je	.L468
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L465
	jmp	.L468
.L548:
	leaq	.LC20(%rip), %rdi
	xorl	%eax, %eax
	movq	%rcx, -1640(%rbp)
	movq	%r8, -1632(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-1632(%rbp), %r8
	movq	(%rbx), %rax
	movq	stdout(%rip), %rsi
	movq	%r8, %rdi
	movq	%rax, -1608(%rbp)
	call	_ZN2v88internal10JSFunction9PrintNameEP8_IO_FILE@PLT
	xorl	%eax, %eax
	leaq	.LC21(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%rbx), %rax
	movq	-1640(%rbp), %rcx
	movq	-1632(%rbp), %r8
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L471
.L447:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L446
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L450
	jmp	.L446
.L540:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19207:
	.size	_ZN2v88internalL42Stats_Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Stats_Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Stats_Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC23:
	.string	"V8.Runtime_Runtime_ResolvePossiblyDirectEval"
	.section	.text._ZN2v88internalL39Stats_Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Stats_Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL39Stats_Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateE.isra.0:
.LFB23647:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L589
.L552:
	movq	_ZZN2v88internalL39Stats_Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateEE28trace_event_unique_atomic357(%rip), %r13
	testq	%r13, %r13
	je	.L590
.L554:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L591
.L556:
	movq	12464(%r12), %rax
	movq	41088(%r12), %r14
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r15
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L560
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L561:
	movq	1631(%rsi), %rax
	movq	(%rbx), %r13
	cmpq	%rax, %r13
	jne	.L563
	movq	-16(%rbx), %rax
	movslq	-20(%rbx), %r13
	movq	23(%rax), %rsi
	movq	41112(%r12), %rdi
	andl	$1, %r13d
	testq	%rdi, %rdi
	je	.L564
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L565:
	movl	-36(%rbx), %r9d
	movl	-28(%rbx), %r8d
	movl	%r13d, %ecx
	leaq	-8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internalL17CompileGlobalEvalEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS3_INS0_18SharedFunctionInfoEEENS0_12LanguageModeEii
	movq	%rax, %r13
.L563:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L569
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L569:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L592
.L551:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L593
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L594
.L566:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L560:
	movq	%r14, %rax
	cmpq	41096(%r12), %r14
	je	.L595
.L562:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L591:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L596
.L557:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L558
	movq	(%rdi), %rax
	call	*8(%rax)
.L558:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L559
	movq	(%rdi), %rax
	call	*8(%rax)
.L559:
	leaq	.LC23(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L590:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L597
.L555:
	movq	%r13, _ZZN2v88internalL39Stats_Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateEE28trace_event_unique_atomic357(%rip)
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L592:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L589:
	movq	40960(%rsi), %rax
	movl	$263, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L595:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L596:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC23(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L597:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L594:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L566
.L593:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23647:
	.size	_ZN2v88internalL39Stats_Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL39Stats_Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal19Runtime_CompileLazyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Runtime_CompileLazyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Runtime_CompileLazyEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Runtime_CompileLazyEiPmPNS0_7IsolateE:
.LFB19182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L610
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L601
.L602:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L601:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L602
	leaq	-72(%rbp), %rdi
	movl	$40960, %esi
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal15StackLimitCheck15JsHasOverflowedEm@PLT
	testb	%al, %al
	je	.L603
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	movq	%rax, %r13
.L604:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L598
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L598:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L611
	addq	$48, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L603:
	.cfi_restore_state
	xorl	%esi, %esi
	leaq	-64(%rbp), %rdx
	movq	%r13, %rdi
	movb	$0, -56(%rbp)
	movq	$0, -64(%rbp)
	call	_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_10JSFunctionEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE@PLT
	testb	%al, %al
	jne	.L605
	movq	312(%r12), %r13
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L605:
	movq	0(%r13), %rax
	movq	47(%rax), %r13
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L610:
	call	_ZN2v88internalL25Stats_Runtime_CompileLazyEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L598
.L611:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19182:
	.size	_ZN2v88internal19Runtime_CompileLazyEiPmPNS0_7IsolateE, .-_ZN2v88internal19Runtime_CompileLazyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal35Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateE:
.LFB19185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L624
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L615
.L616:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L615:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L616
	leaq	-48(%rbp), %rdi
	movl	$40960, %esi
	movq	%rdx, -48(%rbp)
	call	_ZNK2v88internal15StackLimitCheck15JsHasOverflowedEm@PLT
	testb	%al, %al
	je	.L617
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	movq	%rax, %r13
.L618:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L612
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L612:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L625
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L617:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8Compiler16CompileOptimizedENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeE@PLT
	testb	%al, %al
	jne	.L619
	movq	312(%r12), %r13
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L619:
	movq	0(%r13), %rax
	movq	47(%rax), %r13
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L624:
	call	_ZN2v88internalL41Stats_Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L612
.L625:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19185:
	.size	_ZN2v88internal35Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateE, .-_ZN2v88internal35Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateE:
.LFB19188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L643
	movq	41096(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	%rax, -88(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L644
.L629:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L644:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L629
	movq	23(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L645
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L632:
	movq	41016(%r12), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	leaq	-64(%rbp), %r10
	testb	%al, %al
	jne	.L646
.L634:
	movq	0(%r13), %rax
	movq	%r10, %rdi
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal14FeedbackVector23ClearOptimizationMarkerEv@PLT
	movq	0(%r13), %rax
	movq	47(%rax), %r13
	movq	-88(%rbp), %rax
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rax
	je	.L626
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L626:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L647
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L645:
	.cfi_restore_state
	movq	%r14, %rbx
	cmpq	41096(%r12), %r14
	je	.L648
.L633:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L646:
	movq	(%rbx), %rax
	movq	%r10, %rdi
	movq	%r10, -112(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	leaq	-72(%rbp), %rdi
	movq	%rax, -104(%rbp)
	movq	(%rbx), %rax
	movq	%rax, -72(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	leaq	-80(%rbp), %rdi
	movl	%eax, -96(%rbp)
	movq	(%rbx), %rax
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movl	-96(%rbp), %r8d
	movq	-104(%rbp), %r9
	movl	%eax, %ecx
	movq	(%rbx), %rax
	movq	-112(%rbp), %r10
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L649
.L635:
	movslq	67(%rax), %rdx
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	%r10, -96(%rbp)
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal6Logger13FunctionEventEPKcidiiNS0_6StringE@PLT
	movq	-96(%rbp), %r10
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L649:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L635
	movq	23(%rax), %rax
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L643:
	call	_ZN2v88internalL36Stats_Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L648:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L633
.L647:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19188:
	.size	_ZN2v88internal30Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal38Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateE
	.type	_ZN2v88internal38Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateE, @function
_ZN2v88internal38Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateE:
.LFB19191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L662
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L653
.L654:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L653:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L654
	leaq	-48(%rbp), %rdi
	movl	$40960, %esi
	movq	%rdx, -48(%rbp)
	call	_ZNK2v88internal15StackLimitCheck15JsHasOverflowedEm@PLT
	testb	%al, %al
	je	.L655
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	movq	%rax, %r13
.L656:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L650
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L650:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L663
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L655:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8Compiler16CompileOptimizedENS0_6HandleINS0_10JSFunctionEEENS0_15ConcurrencyModeE@PLT
	testb	%al, %al
	jne	.L657
	movq	312(%r12), %r13
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L657:
	movq	0(%r13), %rax
	movq	47(%rax), %r13
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L662:
	call	_ZN2v88internalL44Stats_Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L650
.L663:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19191:
	.size	_ZN2v88internal38Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateE, .-_ZN2v88internal38Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateE:
.LFB19194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L671
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L667
.L668:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L667:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L668
	movq	39(%rax), %rdx
	leaq	-32(%rbp), %rdi
	movq	7(%rdx), %rdx
	movq	%rdx, -32(%rbp)
	movq	23(%rax), %rsi
	leaq	.LC4(%rip), %rdx
	call	_ZN2v88internal14FeedbackVector41EvictOptimizedCodeMarkedForDeoptimizationENS0_18SharedFunctionInfoEPKc@PLT
	movq	(%r12), %rax
	movq	47(%rax), %rax
.L664:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L672
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L671:
	.cfi_restore_state
	call	_ZN2v88internalL36Stats_Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateE
	jmp	.L664
.L672:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19194:
	.size	_ZN2v88internal30Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_InstantiateAsmJsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_InstantiateAsmJsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_InstantiateAsmJsEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_InstantiateAsmJsEiPmPNS0_7IsolateE:
.LFB19197:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L713
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L714
.L675:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L714:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L675
	movq	-8(%r13), %rdx
	xorl	%ecx, %ecx
	leaq	-8(%rsi), %rsi
	testb	$1, %dl
	jne	.L715
.L677:
	movq	-16(%r13), %rdx
	xorl	%r8d, %r8d
	leaq	-16(%r13), %rsi
	testb	$1, %dl
	jne	.L716
.L680:
	movq	-24(%r13), %rdx
	xorl	%r9d, %r9d
	leaq	-24(%r13), %rsi
	testb	$1, %dl
	jne	.L717
.L683:
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L718
.L687:
	movq	0(%r13), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L719
.L698:
	movq	0(%r13), %rax
	movl	$67, %esi
	leaq	41184(%r12), %rdi
	movq	23(%rax), %rdx
	movl	47(%rdx), %eax
	orb	$64, %ah
	movl	%eax, 47(%rdx)
	movq	0(%r13), %r13
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	leaq	47(%r13), %rsi
	movq	%rax, 47(%r13)
	movq	%rax, %rdx
	testb	$1, %al
	je	.L702
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L702
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L702:
	xorl	%r13d, %r13d
.L695:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L673
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L673:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L718:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L687
	movq	0(%r13), %rax
	movq	23(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L688
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	movq	(%rax), %rsi
	movq	-72(%rbp), %r9
	movq	%rax, %r15
.L689:
	movq	7(%rsi), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L691
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %r9
	movq	%rax, %rdx
.L692:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal5AsmJs18InstantiateAsmWasmEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS4_INS0_11AsmWasmDataEEENS4_INS0_10JSReceiverEEESA_NS4_INS0_13JSArrayBufferEEE@PLT
	testq	%rax, %rax
	je	.L687
	movq	(%rax), %r13
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L717:
	movq	-1(%rdx), %rdx
	cmpw	$1059, 11(%rdx)
	cmove	%rsi, %r9
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L716:
	movq	-1(%rdx), %rdx
	cmpw	$1024, 11(%rdx)
	cmovnb	%rsi, %r8
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L715:
	movq	-1(%rdx), %rdx
	cmpw	$1024, 11(%rdx)
	cmovnb	%rsi, %rcx
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L719:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L698
	movq	0(%r13), %rax
	movq	23(%rax), %r15
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L699
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L700:
	movq	%r12, %rdi
	call	_ZN2v88internal18SharedFunctionInfo15DiscardCompiledEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L699:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L720
.L701:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L691:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L721
.L693:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L688:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L722
.L690:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L713:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL30Stats_Runtime_InstantiateAsmJsEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L721:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L722:
	movq	%r12, %rdi
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L720:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L701
	.cfi_endproc
.LFE19197:
	.size	_ZN2v88internal24Runtime_InstantiateAsmJsEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_InstantiateAsmJsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateE:
.LFB19200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$1544, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L756
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rax
	movq	%rdx, %rdi
	leaq	-1560(%rbp), %r14
	movq	41096(%rdx), %rbx
	movq	%rax, -1576(%rbp)
	call	_ZN2v88internal11Deoptimizer4GrabEPNS0_7IsolateE@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r12, -1560(%rbp)
	movq	%rax, %r13
	call	_ZN2v88internal15TimerEventScopeINS0_24TimerEventDeoptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE@PLT
	movq	_ZZN2v88internalL35__RT_impl_Runtime_NotifyDeoptimizedENS0_9ArgumentsEPNS0_7IsolateEE28trace_event_unique_atomic158(%rip), %rdx
	movq	%rdx, %r15
	testq	%rdx, %rdx
	je	.L757
.L727:
	movq	$0, -1552(%rbp)
	movzbl	(%r15), %eax
	testb	$5, %al
	jne	.L758
.L729:
	movq	%r13, %rdi
	call	_ZNK2v88internal11Deoptimizer8functionEv@PLT
	movq	%r13, %rdi
	movzbl	28(%r13), %r15d
	movq	%rax, -1584(%rbp)
	call	_ZNK2v88internal11Deoptimizer8functionEv@PLT
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	31(%rax), %rax
	movq	39(%rax), %rax
	movq	%rax, 12464(%r12)
	call	_ZN2v88internal11Deoptimizer22MaterializeHeapObjectsEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal11DeoptimizerD1Ev@PLT
	movq	%r13, %rdi
	leaq	-1520(%rbp), %r13
	call	_ZN2v88internal8MalloceddlEPv@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L733
	movq	%r13, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-104(%rbp), %rdi
.L733:
	movq	(%rdi), %rax
	call	*96(%rax)
	movq	%rax, 12464(%r12)
	cmpb	$2, %r15b
	jne	.L759
.L734:
	leaq	-1552(%rbp), %rdi
	movq	88(%r12), %r13
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal15TimerEventScopeINS0_24TimerEventDeoptimizeCodeEE13LogTimerEventENS0_6Logger8StartEndE@PLT
	movq	-1576(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L723
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L723:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L760
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L759:
	.cfi_restore_state
	movq	-1584(%rbp), %rax
	xorl	%esi, %esi
	movq	(%rax), %rdi
	call	_ZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeE@PLT
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L758:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	$0, -1584(%rbp)
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L761
.L730:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L731
	movq	(%rdi), %rax
	call	*8(%rax)
.L731:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L732
	movq	(%rdi), %rax
	call	*8(%rax)
.L732:
	leaq	.LC10(%rip), %rax
	movq	%r15, -1544(%rbp)
	movq	%rax, -1536(%rbp)
	movq	-1584(%rbp), %rax
	movq	%rax, -1528(%rbp)
	leaq	-1544(%rbp), %rax
	movq	%rax, -1552(%rbp)
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L757:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L762
.L728:
	movq	%r15, _ZZN2v88internalL35__RT_impl_Runtime_NotifyDeoptimizedENS0_9ArgumentsEPNS0_7IsolateEE28trace_event_unique_atomic158(%rip)
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L756:
	movq	%rdx, %rdi
	call	_ZN2v88internalL31Stats_Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L762:
	leaq	.LC9(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L761:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r15, %rdx
	movl	$88, %esi
	pushq	%rcx
	leaq	.LC10(%rip), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, -1584(%rbp)
	addq	$64, %rsp
	jmp	.L730
.L760:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19200:
	.size	_ZN2v88internal25Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateE:
.LFB19208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$1512, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L845
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41096(%rdx), %rbx
	movq	%rax, -1528(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L846
.L766:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L846:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L766
	cmpb	$0, _ZN2v88internal12FLAG_use_osrE(%rip)
	je	.L847
	leaq	-1504(%rbp), %r14
	movq	%rdx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r15
	testq	%r15, %r15
	je	.L769
	movq	%r14, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %r15
.L769:
	movq	16(%r15), %r14
	movq	%r15, %rdi
	call	_ZNK2v88internal16InterpretedFrame16GetBytecodeArrayEv@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L770
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L771:
	movb	$0, 51(%rsi)
	movq	%r15, %rdi
	call	_ZNK2v88internal16InterpretedFrame17GetBytecodeOffsetEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN2v88internalL31IsSuitableForOnStackReplacementEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE
	testb	%al, %al
	je	.L774
	cmpb	$0, _ZN2v88internal14FLAG_trace_osrE(%rip)
	jne	.L848
.L775:
	movq	%r15, %rdx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8Compiler22GetOptimizedCodeForOSRENS0_6HandleINS0_10JSFunctionEEENS0_9BailoutIdEPNS0_15JavaScriptFrameE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L774
	movq	(%rax), %rax
	testb	$62, 43(%rax)
	je	.L849
.L774:
	cmpb	$0, _ZN2v88internal14FLAG_trace_osrE(%rip)
	jne	.L850
.L807:
	movq	0(%r13), %r14
	movq	47(%r14), %rax
	cmpl	$67, 59(%rax)
	jne	.L851
.L813:
	movq	23(%r14), %rax
	leaq	-1512(%rbp), %rdi
	movq	%rax, -1512(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo7GetCodeEv@PLT
	leaq	47(%r14), %rsi
	movq	%rax, 47(%r14)
	movq	%rax, %rdx
	testb	$1, %al
	je	.L815
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L815
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L815:
	xorl	%r14d, %r14d
.L806:
	subl	$1, 41104(%r12)
	movq	-1528(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L763
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L763:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L852
	addq	$1512, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L770:
	.cfi_restore_state
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L853
.L772:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L851:
	movabsq	$287762808832, %rdx
	movq	23(%r14), %rax
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L844
	testb	$1, %al
	jne	.L854
.L810:
	movq	47(%r14), %rax
	testb	$62, 43(%rax)
	je	.L812
.L844:
	movq	0(%r13), %r14
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L850:
	xorl	%eax, %eax
	leaq	.LC22(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	0(%r13), %rax
	movq	stdout(%rip), %rsi
	leaq	-1512(%rbp), %rdi
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal10JSFunction9PrintNameEP8_IO_FILE@PLT
	movl	%r14d, %esi
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L812:
	movq	47(%r14), %rax
	xorl	%r14d, %r14d
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	je	.L806
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L854:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L844
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L810
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L848:
	xorl	%eax, %eax
	leaq	.LC17(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	0(%r13), %rax
	leaq	-1512(%rbp), %r8
	movq	stdout(%rip), %rsi
	movq	%r8, %rdi
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal10JSFunction9PrintNameEP8_IO_FILE@PLT
	movl	%r14d, %esi
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L845:
	call	_ZN2v88internalL42Stats_Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateE
	movq	%rax, %r14
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L849:
	movq	15(%rax), %rax
	movq	47(%rax), %rdx
	testq	%rdx, %rdx
	js	.L774
	cmpb	$0, _ZN2v88internal14FLAG_trace_osrE(%rip)
	jne	.L855
.L778:
	movq	0(%r13), %rax
	movq	39(%rax), %rsi
	leaq	39(%rax), %rdi
	leaq	23(%rax), %rdx
	movq	7(%rsi), %rsi
	cmpl	$1, 35(%rsi)
	jle	.L856
.L787:
	movq	47(%rax), %rsi
	leaq	47(%rax), %rdi
	cmpl	$67, 59(%rsi)
	jne	.L857
.L790:
	movabsq	$287762808832, %rsi
	movq	(%rdx), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rsi, %rdx
	je	.L802
	testb	$1, %dl
	jne	.L796
.L799:
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	je	.L858
.L802:
	movq	0(%r13), %rax
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	cmpl	$1, 35(%rax)
	jle	.L804
	cmpb	$0, _ZN2v88internal14FLAG_trace_osrE(%rip)
	leaq	-1512(%rbp), %r8
	jne	.L859
.L805:
	movl	$2, %esi
	movq	%r8, %rdi
	movq	%rcx, -1536(%rbp)
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal14FeedbackVector21SetOptimizationMarkerENS0_18OptimizationMarkerE@PLT
	movq	-1536(%rbp), %rcx
.L804:
	movq	(%rcx), %r14
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L847:
	leaq	.LC16(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L853:
	movq	%r14, %rdi
	movq	%rsi, -1536(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1536(%rbp), %rsi
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L857:
	movabsq	$287762808832, %r8
	movq	(%rdx), %rsi
	movq	7(%rsi), %rsi
	cmpq	%r8, %rsi
	je	.L790
	testb	$1, %sil
	jne	.L860
.L791:
	movq	(%rdi), %rsi
	testb	$62, 43(%rsi)
	jne	.L790
	movq	(%rdi), %rsi
	movq	31(%rsi), %rsi
	movl	15(%rsi), %esi
	andl	$1, %esi
	je	.L804
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L855:
	movq	%rcx, -1536(%rbp)
	movq	47(%rax), %rdx
	xorl	%eax, %eax
	movl	%r14d, %esi
	leaq	.LC19(%rip), %rdi
	sarq	$32, %rdx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-1536(%rbp), %rcx
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L856:
	movabsq	$287762808832, %rdx
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L780
	testb	$1, %al
	jne	.L781
.L784:
	movq	(%rdi), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	je	.L861
.L780:
	movq	0(%r13), %rax
.L843:
	leaq	23(%rax), %rdx
	jmp	.L787
.L858:
	movq	15(%rax), %rax
	testb	$1, %al
	je	.L802
	cmpl	$3, %eax
	je	.L802
	andq	$-3, %rax
	je	.L802
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	je	.L804
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L861:
	movq	15(%rax), %rdx
	movq	0(%r13), %rax
	testb	$1, %dl
	jne	.L843
	sarq	$32, %rdx
	cmpq	$1, %rdx
	jbe	.L843
	movq	39(%rax), %rax
	leaq	-1512(%rbp), %r8
	movq	%rcx, -1536(%rbp)
	movq	%r8, %rdi
	movq	7(%rax), %rax
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal14FeedbackVector23ClearOptimizationMarkerEv@PLT
	movq	0(%r13), %rax
	movq	-1536(%rbp), %rcx
	leaq	23(%rax), %rdx
	jmp	.L787
.L860:
	movq	-1(%rsi), %r8
	cmpw	$165, 11(%r8)
	je	.L790
	movq	-1(%rsi), %rsi
	cmpw	$166, 11(%rsi)
	jne	.L791
	jmp	.L790
.L796:
	movq	-1(%rdx), %rsi
	cmpw	$165, 11(%rsi)
	je	.L802
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L799
	jmp	.L802
.L859:
	leaq	.LC20(%rip), %rdi
	xorl	%eax, %eax
	movq	%rcx, -1544(%rbp)
	movq	%r8, -1536(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-1536(%rbp), %r8
	movq	0(%r13), %rax
	movq	stdout(%rip), %rsi
	movq	%r8, %rdi
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal10JSFunction9PrintNameEP8_IO_FILE@PLT
	xorl	%eax, %eax
	leaq	.LC21(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	0(%r13), %rax
	movq	-1544(%rbp), %rcx
	movq	-1536(%rbp), %r8
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L805
.L781:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L780
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L784
	jmp	.L780
.L852:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19208:
	.size	_ZN2v88internal36Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateE, .-_ZN2v88internal36Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateE:
.LFB19226:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L874
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r15
	movq	39(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L864
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L865:
	movq	1631(%rsi), %rax
	movq	0(%r13), %r14
	cmpq	%rax, %r14
	jne	.L867
	movq	-16(%r13), %rax
	movslq	-20(%r13), %r14
	movq	23(%rax), %rsi
	movq	41112(%r12), %rdi
	andl	$1, %r14d
	testq	%rdi, %rdi
	je	.L868
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L869:
	movl	-36(%r13), %r9d
	movl	-28(%r13), %r8d
	movl	%r14d, %ecx
	leaq	-8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internalL17CompileGlobalEvalEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS3_INS0_18SharedFunctionInfoEEENS0_12LanguageModeEii
	movq	%rax, %r14
.L867:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L862
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L862:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L864:
	.cfi_restore_state
	movq	%rbx, %rax
	cmpq	41096(%rdx), %rbx
	je	.L875
.L866:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L868:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L876
.L870:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L874:
	addq	$24, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Stats_Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L875:
	.cfi_restore_state
	movq	%rdx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L876:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L870
	.cfi_endproc
.LFE19226:
	.size	_ZN2v88internal33Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateE, .-_ZN2v88internal33Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal19Runtime_CompileLazyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal19Runtime_CompileLazyEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal19Runtime_CompileLazyEiPmPNS0_7IsolateE:
.LFB23602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23602:
	.size	_GLOBAL__sub_I__ZN2v88internal19Runtime_CompileLazyEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal19Runtime_CompileLazyEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal19Runtime_CompileLazyEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL39Stats_Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateEE28trace_event_unique_atomic357,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Stats_Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateEE28trace_event_unique_atomic357, @object
	.size	_ZZN2v88internalL39Stats_Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateEE28trace_event_unique_atomic357, 8
_ZZN2v88internalL39Stats_Runtime_ResolvePossiblyDirectEvalEiPmPNS0_7IsolateEE28trace_event_unique_atomic357:
	.zero	8
	.section	.bss._ZZN2v88internalL42Stats_Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateEE28trace_event_unique_atomic225,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Stats_Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateEE28trace_event_unique_atomic225, @object
	.size	_ZZN2v88internalL42Stats_Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateEE28trace_event_unique_atomic225, 8
_ZZN2v88internalL42Stats_Runtime_CompileForOnStackReplacementEiPmPNS0_7IsolateEE28trace_event_unique_atomic225:
	.zero	8
	.section	.bss._ZZN2v88internalL35__RT_impl_Runtime_NotifyDeoptimizedENS0_9ArgumentsEPNS0_7IsolateEE28trace_event_unique_atomic158,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35__RT_impl_Runtime_NotifyDeoptimizedENS0_9ArgumentsEPNS0_7IsolateEE28trace_event_unique_atomic158, @object
	.size	_ZZN2v88internalL35__RT_impl_Runtime_NotifyDeoptimizedENS0_9ArgumentsEPNS0_7IsolateEE28trace_event_unique_atomic158, 8
_ZZN2v88internalL35__RT_impl_Runtime_NotifyDeoptimizedENS0_9ArgumentsEPNS0_7IsolateEE28trace_event_unique_atomic158:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateEE28trace_event_unique_atomic148,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateEE28trace_event_unique_atomic148, @object
	.size	_ZZN2v88internalL31Stats_Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateEE28trace_event_unique_atomic148, 8
_ZZN2v88internalL31Stats_Runtime_NotifyDeoptimizedEiPmPNS0_7IsolateEE28trace_event_unique_atomic148:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_InstantiateAsmJsEiPmPNS0_7IsolateEE28trace_event_unique_atomic109,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_InstantiateAsmJsEiPmPNS0_7IsolateEE28trace_event_unique_atomic109, @object
	.size	_ZZN2v88internalL30Stats_Runtime_InstantiateAsmJsEiPmPNS0_7IsolateEE28trace_event_unique_atomic109, 8
_ZZN2v88internalL30Stats_Runtime_InstantiateAsmJsEiPmPNS0_7IsolateEE28trace_event_unique_atomic109:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateEE27trace_event_unique_atomic97,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateEE27trace_event_unique_atomic97, @object
	.size	_ZZN2v88internalL36Stats_Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateEE27trace_event_unique_atomic97, 8
_ZZN2v88internalL36Stats_Runtime_EvictOptimizedCodeSlotEiPmPNS0_7IsolateEE27trace_event_unique_atomic97:
	.zero	8
	.section	.bss._ZZN2v88internalL44Stats_Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateEE27trace_event_unique_atomic82,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Stats_Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateEE27trace_event_unique_atomic82, @object
	.size	_ZZN2v88internalL44Stats_Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateEE27trace_event_unique_atomic82, 8
_ZZN2v88internalL44Stats_Runtime_CompileOptimized_NotConcurrentEiPmPNS0_7IsolateEE27trace_event_unique_atomic82:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateEE27trace_event_unique_atomic63,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateEE27trace_event_unique_atomic63, @object
	.size	_ZZN2v88internalL36Stats_Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateEE27trace_event_unique_atomic63, 8
_ZZN2v88internalL36Stats_Runtime_FunctionFirstExecutionEiPmPNS0_7IsolateEE27trace_event_unique_atomic63:
	.zero	8
	.section	.bss._ZZN2v88internalL41Stats_Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateEE27trace_event_unique_atomic48,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Stats_Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateEE27trace_event_unique_atomic48, @object
	.size	_ZZN2v88internalL41Stats_Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateEE27trace_event_unique_atomic48, 8
_ZZN2v88internalL41Stats_Runtime_CompileOptimized_ConcurrentEiPmPNS0_7IsolateEE27trace_event_unique_atomic48:
	.zero	8
	.section	.bss._ZZN2v88internalL25Stats_Runtime_CompileLazyEiPmPNS0_7IsolateEE27trace_event_unique_atomic22,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL25Stats_Runtime_CompileLazyEiPmPNS0_7IsolateEE27trace_event_unique_atomic22, @object
	.size	_ZZN2v88internalL25Stats_Runtime_CompileLazyEiPmPNS0_7IsolateEE27trace_event_unique_atomic22, 8
_ZZN2v88internalL25Stats_Runtime_CompileLazyEiPmPNS0_7IsolateEE27trace_event_unique_atomic22:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
