	.file	"js-plural-rules.cc"
	.text
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB23894:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE23894:
	.size	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB23895:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23895:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB23983:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE23983:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB24013:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE24013:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB24015:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE24015:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB24018:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L13
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L13:
	ret
	.cfi_endproc
.LFE24018:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB24020:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE24020:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB24019:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE24019:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB24017:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24017:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB23985:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23985:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB24012:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24012:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB24011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L20
	movq	%r12, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24011:
	.size	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.rodata._ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci.str1.1,"aMS",@progbits,1
.LC0:
	.string	"basic_string::append"
	.section	.text._ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci,"axG",@progbits,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci
	.type	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci, @function
_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci:
.LFB24021:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movslq	%edx, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%rdi), %rax
	cmpq	%rax, %rdx
	ja	.L28
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L28:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24021:
	.size	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci, .-_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci
	.section	.text._ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev,"axG",@progbits,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.type	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev, @function
_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev:
.LFB21118:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678ByteSinkD2Ev@PLT
	.cfi_endproc
.LFE21118:
	.size	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev, .-_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev
	.set	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.section	.text._ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev,"axG",@progbits,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.type	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev, @function
_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev:
.LFB21120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678ByteSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE21120:
	.size	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev, .-_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.section	.rodata._ZN2v88internal12_GLOBAL__N_120CreateICUPluralRulesEPNS0_7IsolateERKN6icu_676LocaleENS0_13JSPluralRules4TypeEPSt10unique_ptrINS4_11PluralRulesESt14default_deleteISB_EE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"JSPluralRules::Type::CARDINAL == type"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_120CreateICUPluralRulesEPNS0_7IsolateERKN6icu_676LocaleENS0_13JSPluralRules4TypeEPSt10unique_ptrINS4_11PluralRulesESt14default_deleteISB_EE.isra.0.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal12_GLOBAL__N_120CreateICUPluralRulesEPNS0_7IsolateERKN6icu_676LocaleENS0_13JSPluralRules4TypeEPSt10unique_ptrINS4_11PluralRulesESt14default_deleteISB_EE.isra.0.str1.8
	.align 8
.LC3:
	.string	"(plural_rules.get()) != nullptr"
	.section	.text._ZN2v88internal12_GLOBAL__N_120CreateICUPluralRulesEPNS0_7IsolateERKN6icu_676LocaleENS0_13JSPluralRules4TypeEPSt10unique_ptrINS4_11PluralRulesESt14default_deleteISB_EE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120CreateICUPluralRulesEPNS0_7IsolateERKN6icu_676LocaleENS0_13JSPluralRules4TypeEPSt10unique_ptrINS4_11PluralRulesESt14default_deleteISB_EE.isra.0, @function
_ZN2v88internal12_GLOBAL__N_120CreateICUPluralRulesEPNS0_7IsolateERKN6icu_676LocaleENS0_13JSPluralRules4TypeEPSt10unique_ptrINS4_11PluralRulesESt14default_deleteISB_EE.isra.0:
.LFB24119:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	cmpl	$1, %esi
	je	.L38
	xorl	%r8d, %r8d
	testl	%esi, %esi
	jne	.L45
.L33:
	leaq	-28(%rbp), %rdx
	movl	%r8d, %esi
	call	_ZN6icu_6711PluralRules9forLocaleERKNS_6LocaleE11UPluralTypeR10UErrorCode@PLT
	movq	%rax, %rdi
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jg	.L34
	testq	%rdi, %rdi
	je	.L46
	movq	(%rbx), %r8
	movl	$1, %r12d
	movq	%rdi, (%rbx)
	testq	%r8, %r8
	je	.L32
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L32:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L47
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L41
	movq	(%rdi), %rax
	xorl	%r12d, %r12d
	call	*8(%rax)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$1, %r8d
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L45:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	xorl	%r12d, %r12d
	jmp	.L32
.L47:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24119:
	.size	_ZN2v88internal12_GLOBAL__N_120CreateICUPluralRulesEPNS0_7IsolateERKN6icu_676LocaleENS0_13JSPluralRules4TypeEPSt10unique_ptrINS4_11PluralRulesESt14default_deleteISB_EE.isra.0, .-_ZN2v88internal12_GLOBAL__N_120CreateICUPluralRulesEPNS0_7IsolateERKN6icu_676LocaleENS0_13JSPluralRules4TypeEPSt10unique_ptrINS4_11PluralRulesESt14default_deleteISB_EE.isra.0
	.section	.rodata._ZN2v84base16LazyInstanceImplINS_8internal12_GLOBAL__N_127PluralRulesAvailableLocalesENS0_32StaticallyAllocatedInstanceTraitIS4_EENS0_21DefaultConstructTraitIS4_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS4_EEE12InitInstanceEPv.str1.1,"aMS",@progbits,1
.LC4:
	.string	"U_SUCCESS(status)"
	.section	.text._ZN2v84base16LazyInstanceImplINS_8internal12_GLOBAL__N_127PluralRulesAvailableLocalesENS0_32StaticallyAllocatedInstanceTraitIS4_EENS0_21DefaultConstructTraitIS4_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS4_EEE12InitInstanceEPv,"ax",@progbits
	.p2align 4
	.type	_ZN2v84base16LazyInstanceImplINS_8internal12_GLOBAL__N_127PluralRulesAvailableLocalesENS0_32StaticallyAllocatedInstanceTraitIS4_EENS0_21DefaultConstructTraitIS4_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS4_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS_8internal12_GLOBAL__N_127PluralRulesAvailableLocalesENS0_32StaticallyAllocatedInstanceTraitIS4_EENS0_21DefaultConstructTraitIS4_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS4_EEE12InitInstanceEPv:
.LFB23356:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, 8(%rdi)
	leaq	-112(%rbp), %rax
	movq	$0, 16(%rdi)
	movq	%r15, 24(%rdi)
	movq	%r15, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	%rax, %rdi
	movl	$0, -112(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6711PluralRules19getAvailableLocalesER10UErrorCode@PLT
	movl	-112(%rbp), %edx
	testl	%edx, %edx
	jg	.L106
	movq	%rax, %r14
	leaq	-108(%rbp), %rax
	movl	$0, -108(%rbp)
	movq	%rax, -176(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -184(%rbp)
	.p2align 4,,10
	.p2align 3
.L81:
	movq	(%r14), %rax
	movq	-128(%rbp), %rdx
	movq	%r14, %rdi
	movq	-176(%rbp), %rsi
	call	*40(%rax)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L50
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	jg	.L50
	leaq	-80(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, -120(%rbp)
	movq	%rax, -96(%rbp)
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L107
	cmpq	$1, %rax
	jne	.L53
	movzbl	0(%r13), %edx
	movb	%dl, -80(%rbp)
	movq	-120(%rbp), %rdx
.L54:
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	cmpl	$3, -108(%rbp)
	jg	.L55
.L58:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L108
.L102:
	movq	-96(%rbp), %r9
	movq	%r15, -136(%rbp)
	movq	%r14, -144(%rbp)
	movq	-88(%rbp), %r13
	movq	%rbx, -152(%rbp)
	movq	%r9, %r8
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L66:
	movq	16(%r12), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L62
.L109:
	movq	%rax, %r12
.L61:
	movq	40(%r12), %rbx
	movq	32(%r12), %r15
	cmpq	%rbx, %r13
	movq	%rbx, %r14
	cmovbe	%r13, %r14
	testq	%r14, %r14
	je	.L63
	movq	%r9, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r8, -168(%rbp)
	movq	%r9, -160(%rbp)
	call	memcmp@PLT
	movq	-160(%rbp), %r9
	movq	-168(%rbp), %r8
	testl	%eax, %eax
	jne	.L64
.L63:
	movq	%r13, %rax
	movl	$2147483648, %ecx
	subq	%rbx, %rax
	cmpq	%rcx, %rax
	jge	.L65
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L66
.L64:
	testl	%eax, %eax
	js	.L66
.L65:
	movq	24(%r12), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L109
.L62:
	movq	%rbx, %r10
	movq	%r15, %r11
	movq	%r14, %rdx
	movq	-136(%rbp), %r15
	movq	-144(%rbp), %r14
	movq	-152(%rbp), %rbx
	testb	%sil, %sil
	jne	.L110
.L69:
	testq	%rdx, %rdx
	je	.L70
	movq	%r9, %rsi
	movq	%r11, %rdi
	movq	%r8, -152(%rbp)
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	call	memcmp@PLT
	movq	-136(%rbp), %r9
	movq	-144(%rbp), %r10
	testl	%eax, %eax
	movq	-152(%rbp), %r8
	jne	.L71
.L70:
	movq	%r10, %rax
	movl	$2147483648, %ecx
	subq	%r13, %rax
	cmpq	%rcx, %rax
	jge	.L72
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L73
.L71:
	testl	%eax, %eax
	jns	.L72
.L73:
	testq	%r12, %r12
	je	.L111
.L74:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L112
.L75:
	movl	$64, %edi
	movl	%r8d, -136(%rbp)
	call	_Znwm@PLT
	movl	-136(%rbp), %r8d
	movq	%rax, %rsi
	leaq	48(%rax), %rax
	movq	%rax, 32(%rsi)
	movq	-96(%rbp), %rax
	cmpq	-120(%rbp), %rax
	je	.L113
	movq	%rax, 32(%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 48(%rsi)
.L79:
	movq	-88(%rbp), %rax
	movl	%r8d, %edi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	$0, -88(%rbp)
	movq	%rax, 40(%rsi)
	movq	-120(%rbp), %rax
	movb	$0, -80(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	movq	-96(%rbp), %r8
.L72:
	cmpq	-120(%rbp), %r8
	je	.L81
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L113:
	movdqa	-80(%rbp), %xmm0
	movups	%xmm0, 48(%rsi)
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L110:
	cmpq	%r12, 24(%rbx)
	je	.L74
.L82:
	movq	%r12, %rdi
	movq	%r9, -136(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-136(%rbp), %r9
	movq	40(%rax), %r10
	movq	32(%rax), %r11
	movq	%r9, %r8
	cmpq	%r10, %r13
	movq	%r10, %rdx
	cmovbe	%r13, %rdx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L53:
	testq	%rax, %rax
	jne	.L114
	movq	-120(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	cmpl	$3, -108(%rbp)
	jle	.L58
.L55:
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %rdx
	addq	%rax, %rdx
	cmpq	%rdx, %rax
	je	.L58
	.p2align 4,,10
	.p2align 3
.L60:
	cmpb	$95, (%rax)
	jne	.L59
	movb	$45, (%rax)
.L59:
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L60
	movq	16(%rbx), %r12
	testq	%r12, %r12
	jne	.L102
.L108:
	movq	%r15, %r12
	cmpq	24(%rbx), %r15
	je	.L105
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r13
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L107:
	movq	-184(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L52:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L50:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	40(%r12), %r10
	cmpq	%r10, %r13
	movq	%r10, %rdx
	cmovbe	%r13, %rdx
	testq	%rdx, %rdx
	je	.L76
	movq	32(%r12), %rsi
	movq	%r9, %rdi
	movq	%r10, -136(%rbp)
	call	memcmp@PLT
	movq	-136(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, %r8d
	jne	.L77
.L76:
	movq	%r13, %rcx
	movl	$2147483648, %eax
	xorl	%r8d, %r8d
	subq	%r10, %rcx
	cmpq	%rax, %rcx
	jge	.L75
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rcx
	jle	.L105
	movl	%ecx, %r8d
.L77:
	shrl	$31, %r8d
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L106:
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L105:
	movl	$1, %r8d
	jmp	.L75
.L115:
	call	__stack_chk_fail@PLT
.L114:
	movq	-120(%rbp), %rdi
	jmp	.L52
.L111:
	movq	%r9, %r8
	jmp	.L72
	.cfi_endproc
.LFE23356:
	.size	_ZN2v84base16LazyInstanceImplINS_8internal12_GLOBAL__N_127PluralRulesAvailableLocalesENS0_32StaticallyAllocatedInstanceTraitIS4_EENS0_21DefaultConstructTraitIS4_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS4_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS_8internal12_GLOBAL__N_127PluralRulesAvailableLocalesENS0_32StaticallyAllocatedInstanceTraitIS4_EENS0_21DefaultConstructTraitIS4_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS4_EEE12InitInstanceEPv
	.section	.rodata._ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEPKc.str1.1,"aMS",@progbits,1
.LC5:
	.string	"(location_) != nullptr"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"JSReceiver::CreateDataProperty(isolate, options, key_str, value, Just(kDontThrow)) .FromJust()"
	.section	.text._ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEPKc,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEPKc, @function
_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEPKc:
.LFB18748:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L122
	movq	%rax, %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movabsq	$4294967297, %r8
	call	_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L123
.L118:
	shrw	$8, %ax
	je	.L124
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L123:
	movl	%eax, -68(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-68(%rbp), %eax
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L124:
	leaq	.LC6(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18748:
	.size	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEPKc, .-_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEPKc
	.section	.text._ZN2v88internal7ManagedIN6icu_676number24LocalizedNumberFormatterEE10DestructorEPv,"axG",@progbits,_ZN2v88internal7ManagedIN6icu_676number24LocalizedNumberFormatterEE10DestructorEPv,comdat
	.p2align 4
	.weak	_ZN2v88internal7ManagedIN6icu_676number24LocalizedNumberFormatterEE10DestructorEPv
	.type	_ZN2v88internal7ManagedIN6icu_676number24LocalizedNumberFormatterEE10DestructorEPv, @function
_ZN2v88internal7ManagedIN6icu_676number24LocalizedNumberFormatterEE10DestructorEPv:
.LFB23293:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L126
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L129
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L130
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L131:
	cmpl	$1, %eax
	je	.L138
.L129:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L126:
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L133
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L134:
	cmpl	$1, %eax
	jne	.L129
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L130:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L133:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L134
	.cfi_endproc
.LFE23293:
	.size	_ZN2v88internal7ManagedIN6icu_676number24LocalizedNumberFormatterEE10DestructorEPv, .-_ZN2v88internal7ManagedIN6icu_676number24LocalizedNumberFormatterEE10DestructorEPv
	.section	.text._ZN2v88internal7ManagedIN6icu_6711PluralRulesEE10DestructorEPv,"axG",@progbits,_ZN2v88internal7ManagedIN6icu_6711PluralRulesEE10DestructorEPv,comdat
	.p2align 4
	.weak	_ZN2v88internal7ManagedIN6icu_6711PluralRulesEE10DestructorEPv
	.type	_ZN2v88internal7ManagedIN6icu_6711PluralRulesEE10DestructorEPv, @function
_ZN2v88internal7ManagedIN6icu_6711PluralRulesEE10DestructorEPv:
.LFB23268:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L139
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L142
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L143
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L144:
	cmpl	$1, %eax
	je	.L151
.L142:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L139:
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L146
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L147:
	cmpl	$1, %eax
	jne	.L142
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L143:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L146:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L147
	.cfi_endproc
.LFE23268:
	.size	_ZN2v88internal7ManagedIN6icu_6711PluralRulesEE10DestructorEPv, .-_ZN2v88internal7ManagedIN6icu_6711PluralRulesEE10DestructorEPv
	.section	.text._ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEiPKc,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEiPKc, @function
_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEiPKc:
.LFB18749:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	salq	$32, %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$48, %rsp
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L153
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L154:
	movq	%rbx, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L161
	movabsq	$4294967297, %r8
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver18CreateDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L162
.L157:
	shrw	$8, %ax
	je	.L163
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L164
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L165
.L155:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L161:
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L163:
	leaq	.LC6(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L162:
	movl	%eax, -72(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-72(%rbp), %eax
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L155
.L164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18749:
	.size	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEiPKc, .-_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEiPKc
	.section	.text._ZNK2v88internal13JSPluralRules12TypeAsStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13JSPluralRules12TypeAsStringEv
	.type	_ZNK2v88internal13JSPluralRules12TypeAsStringEv, @function
_ZNK2v88internal13JSPluralRules12TypeAsStringEv:
.LFB18669:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	leaq	-35848(%rax), %rcx
	subq	$36368, %rax
	testb	$1, 35(%rdx)
	cmovne	%rcx, %rax
	ret
	.cfi_endproc
.LFE18669:
	.size	_ZNK2v88internal13JSPluralRules12TypeAsStringEv, .-_ZNK2v88internal13JSPluralRules12TypeAsStringEv
	.section	.text._ZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11Ev
	.type	_ZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11Ev, @function
_ZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11Ev:
.LFB18757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	_ZZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %eax
	cmpb	$2, %al
	jne	.L180
.L170:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L181
	addq	$56, %rsp
	leaq	8+_ZZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal12_GLOBAL__N_127PluralRulesAvailableLocalesENS0_32StaticallyAllocatedInstanceTraitIS4_EENS0_21DefaultConstructTraitIS4_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS4_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rax
	leaq	-64(%rbp), %r12
	movq	%rax, -56(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L170
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L170
.L181:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18757:
	.size	_ZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11Ev, .-_ZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11Ev
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB22430:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L197
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L186:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L184
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L182
.L185:
	movq	%rbx, %r12
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L184:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L185
.L182:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE22430:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E:
.LFB22432:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L216
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L205:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L202
	call	_ZdlPv@PLT
.L202:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L203
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L200
.L204:
	movq	%rbx, %r12
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L203:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L204
.L200:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE22432:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.section	.text._ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC2EPS6_i,"axG",@progbits,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC5EPS6_i,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC2EPS6_i
	.type	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC2EPS6_i, @function
_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC2EPS6_i:
.LFB22488:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rcx
	movq	%rsi, %rdi
	movq	%rcx, (%rax)
	movq	%rsi, 8(%rax)
	testl	%edx, %edx
	jle	.L219
	movslq	%edx, %rdx
	leaq	16(%rsi), %rax
	cmpq	%rax, (%rsi)
	je	.L222
	movq	16(%rsi), %rax
.L221:
	movq	8(%rdi), %rsi
	subq	%rsi, %rax
	cmpq	%rax, %rdx
	ja	.L223
.L219:
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	addq	%rdx, %rsi
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	.p2align 4,,10
	.p2align 3
.L222:
	movl	$15, %eax
	jmp	.L221
	.cfi_endproc
.LFE22488:
	.size	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC2EPS6_i, .-_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC2EPS6_i
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC1EPS6_i
	.set	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC1EPS6_i,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC2EPS6_i
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6711PluralRulesESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5IN6icu_6711PluralRulesESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6711PluralRulesESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6711PluralRulesESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6711PluralRulesESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E:
.LFB23617:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rsi)
	movq	$0, (%rdi)
	je	.L227
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$24, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movq	(%rbx), %rdx
	movq	$0, (%rbx)
	movabsq	$4294967297, %rcx
	movq	%rcx, 8(%rax)
	leaq	16+_ZTVSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, (%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE23617:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6711PluralRulesESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6711PluralRulesESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN6icu_6711PluralRulesESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN6icu_6711PluralRulesESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6711PluralRulesESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal13JSPluralRules12TypeAsStringEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal13JSPluralRules12TypeAsStringEv, @function
_GLOBAL__sub_I__ZNK2v88internal13JSPluralRules12TypeAsStringEv:
.LFB24039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24039:
	.size	_GLOBAL__sub_I__ZNK2v88internal13JSPluralRules12TypeAsStringEv, .-_GLOBAL__sub_I__ZNK2v88internal13JSPluralRules12TypeAsStringEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal13JSPluralRules12TypeAsStringEv
	.section	.rodata._ZN2v88internal13JSPluralRules3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN2v88internal13JSPluralRules3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_.str1.1,"aMS",@progbits,1
.LC8:
	.string	"Intl.PluralRules"
.LC9:
	.string	"cardinal"
.LC10:
	.string	"ordinal"
.LC11:
	.string	"type"
.LC12:
	.string	"unreachable code"
	.section	.rodata._ZN2v88internal13JSPluralRules3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_.str1.8
	.align 8
.LC13:
	.string	"Failed to create ICU PluralRules, are ICU data files missing?"
	.align 8
.LC14:
	.string	"(icu_plural_rules.get()) != nullptr"
	.section	.text._ZN2v88internal13JSPluralRules3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13JSPluralRules3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_
	.type	_ZN2v88internal13JSPluralRules3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_, @function
_ZN2v88internal13JSPluralRules3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_:
.LFB18670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	xorl	%ecx, %ecx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-2048(%rbp), %rdi
	pushq	%rbx
	subq	$2184, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -2160(%rbp)
	movq	%r12, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4Intl22CanonicalizeLocaleListB5cxx11EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEb@PLT
	cmpb	$0, -2048(%rbp)
	jne	.L233
	xorl	%r13d, %r13d
.L234:
	movq	-2032(%rbp), %rbx
	movq	-2040(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L323
	.p2align 4,,10
	.p2align 3
.L327:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L324
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L327
.L325:
	movq	-2040(%rbp), %r12
.L323:
	testq	%r12, %r12
	je	.L328
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L328:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L427
	addq	$2184, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L327
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L233:
	movq	-2032(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-2040(%rbp), %r9
	movq	$0, -2064(%rbp)
	movaps	%xmm0, -2080(%rbp)
	movq	%rax, %r15
	movq	%rax, -2152(%rbp)
	subq	%r9, %r15
	movq	%r15, %rax
	sarq	$5, %rax
	je	.L428
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L429
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	-2040(%rbp), %r9
	movq	%rax, %rbx
	movq	-2032(%rbp), %rax
	movq	%rax, -2152(%rbp)
.L236:
	movq	%rbx, %xmm0
	addq	%rbx, %r15
	punpcklqdq	%xmm0, %xmm0
	movq	%r15, -2064(%rbp)
	movaps	%xmm0, -2080(%rbp)
	cmpq	-2152(%rbp), %r9
	je	.L238
	leaq	-2016(%rbp), %rax
	movq	%r12, -2192(%rbp)
	movq	%rbx, %r15
	movq	%r9, %r12
	movq	%r13, -2168(%rbp)
	movq	%rax, %rbx
	movq	%r14, -2208(%rbp)
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L240:
	cmpq	$1, %r13
	jne	.L242
	movzbl	(%r14), %eax
	movb	%al, 16(%r15)
.L243:
	movq	%r13, 8(%r15)
	addq	$32, %r12
	addq	$32, %r15
	movb	$0, (%rdi,%r13)
	cmpq	%r12, -2152(%rbp)
	je	.L430
.L244:
	leaq	16(%r15), %rdi
	movq	%rdi, (%r15)
	movq	(%r12), %r14
	movq	8(%r12), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L239
	testq	%r14, %r14
	je	.L431
.L239:
	movq	%r13, -2016(%rbp)
	cmpq	$15, %r13
	jbe	.L240
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r15)
	movq	%rax, %rdi
	movq	-2016(%rbp), %rax
	movq	%rax, 16(%r15)
.L241:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-2016(%rbp), %r13
	movq	(%r15), %rdi
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L242:
	testq	%r13, %r13
	je	.L243
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L430:
	movq	-2168(%rbp), %r13
	movq	-2192(%rbp), %r12
	movq	%r15, %rbx
	movq	-2208(%rbp), %r14
.L238:
	movq	%rbx, -2072(%rbp)
	movq	(%r14), %rax
	cmpq	88(%r12), %rax
	je	.L432
	testb	$1, %al
	jne	.L247
.L249:
	leaq	.LC8(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L248
.L246:
	leaq	.LC8(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Intl16GetLocaleMatcherEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKc@PLT
	movq	%rax, %rbx
	testb	%al, %al
	je	.L248
	movl	$8, %edi
	leaq	-2016(%rbp), %r15
	sarq	$32, %rbx
	call	_Znwm@PLT
	movl	$16, %edi
	movabsq	$4294967296, %rcx
	movq	%rcx, (%rax)
	movq	%rax, -2152(%rbp)
	call	_Znwm@PLT
	leaq	.LC9(%rip), %rcx
	movl	$16, %edi
	movq	$0, -2112(%rbp)
	movq	%rax, %r14
	movq	%rcx, %xmm0
	leaq	.LC10(%rip), %rax
	movq	$0, -2000(%rbp)
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r14)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2016(%rbp)
	call	_Znwm@PLT
	movdqu	(%r14), %xmm2
	movq	%r12, %rdi
	movq	%r15, %rcx
	leaq	16(%rax), %rdx
	movq	%rax, -2016(%rbp)
	leaq	.LC8(%rip), %r8
	movq	%r13, %rsi
	movups	%xmm2, (%rax)
	leaq	-2112(%rbp), %rax
	movq	%rdx, -2000(%rbp)
	movq	%rax, %r9
	movq	%rdx, -2008(%rbp)
	leaq	.LC11(%rip), %rdx
	movq	%rax, -2192(%rbp)
	call	_ZN2v88internal4Intl15GetStringOptionEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKcSt6vectorIS8_SaIS8_EES8_PSt10unique_ptrIA_cSt14default_deleteISD_EE@PLT
	movq	-2016(%rbp), %rdi
	movl	%eax, %edx
	testq	%rdi, %rdi
	je	.L251
	movl	%eax, -2208(%rbp)
	movb	%al, -2168(%rbp)
	call	_ZdlPv@PLT
	movl	-2208(%rbp), %eax
	movzbl	-2168(%rbp), %edx
.L251:
	movq	-2112(%rbp), %rdi
	testb	%dl, %dl
	je	.L252
	shrw	$8, %ax
	je	.L253
	movq	(%r14), %rsi
	movq	%rdi, -2168(%rbp)
	call	strcmp@PLT
	movq	-2168(%rbp), %rdi
	testl	%eax, %eax
	je	.L339
	movq	8(%r14), %rsi
	call	strcmp@PLT
	movq	-2168(%rbp), %rdi
	testl	%eax, %eax
	je	.L433
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L252:
	testq	%rdi, %rdi
	jne	.L434
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	movq	-2152(%rbp), %rdi
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L248:
	xorl	%r13d, %r13d
.L250:
	movq	-2072(%rbp), %rbx
	movq	-2080(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L317
	.p2align 4,,10
	.p2align 3
.L321:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L318
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L321
.L319:
	movq	-2080(%rbp), %r12
.L317:
	testq	%r12, %r12
	je	.L234
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L318:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L321
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L432:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L428:
	xorl	%ebx, %ebx
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L247:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L249
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L253:
	testq	%rdi, %rdi
	jne	.L435
.L420:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	movq	-2152(%rbp), %rdi
	call	_ZdlPv@PLT
	movl	$0, -2168(%rbp)
.L256:
	leaq	-2008(%rbp), %rax
	movl	$0, -2008(%rbp)
	movq	$0, -2000(%rbp)
	movq	%rax, -1992(%rbp)
	movq	%rax, -1984(%rbp)
	movq	$0, -1976(%rbp)
	movzbl	_ZZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %eax
	cmpb	$2, %al
	je	.L257
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal12_GLOBAL__N_127PluralRulesAvailableLocalesENS0_32StaticallyAllocatedInstanceTraitIS4_EENS0_21DefaultConstructTraitIS4_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS4_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -512(%rbp)
	leaq	8+_ZZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-512(%rbp), %r14
	movq	%rax, -504(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r14, %rsi
	leaq	_ZZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rdi
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -496(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-496(%rbp), %rax
	testq	%rax, %rax
	je	.L257
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L257:
	leaq	-1744(%rbp), %rdi
	movq	%r15, %r9
	movl	%ebx, %r8d
	movq	%r12, %rsi
	leaq	-2080(%rbp), %rcx
	leaq	8+_ZZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rdx
	call	_ZN2v88internal4Intl13ResolveLocaleEPNS0_7IsolateERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISA_ESaISA_EERKSt6vectorISA_SD_ENS1_13MatcherOptionESG_@PLT
	movq	-2000(%rbp), %r14
	testq	%r14, %r14
	je	.L263
.L259:
	movq	24(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r14), %rdi
	leaq	48(%r14), %rax
	movq	16(%r14), %rbx
	cmpq	%rax, %rdi
	je	.L262
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L263
.L264:
	movq	%rbx, %r14
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L262:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L264
.L263:
	movq	-1744(%rbp), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rbx, -2016(%rbp)
	movq	%rax, -2008(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, -2176(%rbp)
	testq	%rax, %rax
	je	.L436
	leaq	-1712(%rbp), %rax
	leaq	-512(%rbp), %r14
	movq	%rax, %rsi
	movq	%r14, %rdi
	leaq	-1440(%rbp), %rbx
	movq	%rax, -2152(%rbp)
	call	_ZN6icu_676number15NumberFormatter10withLocaleERKNS_6LocaleE@PLT
	movl	$6, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	leaq	-2136(%rbp), %r9
	movl	-2168(%rbp), %esi
	movq	-2152(%rbp), %rdi
	movq	%r9, %rdx
	movq	%r9, -2208(%rbp)
	movq	$0, -2136(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_120CreateICUPluralRulesEPNS0_7IsolateERKN6icu_676LocaleENS0_13JSPluralRules4TypeEPSt10unique_ptrINS4_11PluralRulesESt14default_deleteISB_EE.isra.0
	movq	-2208(%rbp), %r9
	testb	%al, %al
	je	.L437
.L265:
	cmpq	$0, -2136(%rbp)
	je	.L438
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$3, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal4Intl27SetNumberFormatDigitOptionsEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEiib@PLT
	cmpb	$0, -2016(%rbp)
	je	.L269
	movdqu	-2012(%rbp), %xmm4
	movq	-2192(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movl	-1996(%rbp), %eax
	movaps	%xmm4, -2112(%rbp)
	movl	%eax, -2096(%rbp)
	call	_ZN2v88internal14JSNumberFormat26SetDigitOptionsToFormatterERKN6icu_676number24LocalizedNumberFormatterERKNS0_4Intl24NumberFormatDigitOptionsE@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatteraSEOS1_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	movq	-2136(%rbp), %r13
	leaq	-2128(%rbp), %rsi
	leaq	-2120(%rbp), %rdi
	movq	$0, -2136(%rbp)
	movq	%r13, -2128(%rbp)
	call	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN6icu_6711PluralRulesESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	movq	-2120(%rbp), %r15
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r15, %r15
	je	.L270
	testq	%r14, %r14
	je	.L271
	lock addl	$1, 8(%r15)
	movq	-2120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L270
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
.L274:
	cmpl	$1, %eax
	je	.L439
.L270:
	movq	32(%r12), %rax
	subq	48(%r12), %rax
	cmpq	$33554432, %rax
	jg	.L440
.L277:
	movq	%r15, %xmm5
	movq	%r13, %xmm0
	movl	$16, %edi
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -2192(%rbp)
	call	_Znwm@PLT
	movdqa	-2192(%rbp), %xmm0
	movq	%rax, %rdx
	movups	%xmm0, (%rax)
	testq	%r15, %r15
	je	.L278
	testq	%r14, %r14
	je	.L279
	lock addl	$1, 8(%r15)
.L278:
	movl	$48, %edi
	movq	%rdx, -2192(%rbp)
	call	_Znwm@PLT
	movq	-2192(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, %r13
	movups	%xmm0, 8(%rax)
	movq	%rdx, 24(%rax)
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	$0, (%rax)
	leaq	_ZN2v88internal7ManagedIN6icu_6711PluralRulesEE10DestructorEPv(%rip), %rax
	movq	%rax, 32(%r13)
	movq	$0, 40(%r13)
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	41152(%r12), %rdi
	movq	(%rax), %rsi
	movq	%rax, -2192(%rbp)
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, 40(%r13)
	movq	%rax, %rdi
	call	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE@PLT
	testq	%r15, %r15
	je	.L281
	testq	%r14, %r14
	je	.L282
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
.L283:
	cmpl	$1, %eax
	je	.L441
.L281:
	movq	-2128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L287
	movq	(%rdi), %rax
	call	*8(%rax)
.L287:
	movl	$456, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L288
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterC1ERKS1_@PLT
.L288:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%rax, %r15
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r15)
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, (%r15)
	movq	32(%r12), %rax
	subq	48(%r12), %rax
	movq	%r13, 16(%r15)
	cmpq	$33554432, %rax
	jg	.L442
.L289:
	movq	%r13, %xmm0
	movq	%r15, %xmm6
	movl	$16, %edi
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -2208(%rbp)
	call	_Znwm@PLT
	movdqa	-2208(%rbp), %xmm0
	movq	%rax, %r13
	movups	%xmm0, (%rax)
	leaq	8(%r15), %rax
	movq	%rax, -2208(%rbp)
	testq	%r14, %r14
	je	.L443
	leaq	8(%r15), %rax
	lock addl	$1, (%rax)
.L290:
	movl	$48, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %r8
	movups	%xmm0, 8(%rax)
	movq	%r13, 24(%rax)
	movq	%r8, %rsi
	movq	$0, (%rax)
	leaq	_ZN2v88internal7ManagedIN6icu_676number24LocalizedNumberFormatterEE10DestructorEPv(%rip), %rax
	movq	%rax, 32(%r8)
	movq	$0, 40(%r8)
	movq	%r8, -2208(%rbp)
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	41152(%r12), %rdi
	movq	(%rax), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	-2208(%rbp), %r8
	movq	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	%rax, 40(%r8)
	movq	%r8, %rsi
	call	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	-2208(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE@PLT
	testq	%r14, %r14
	je	.L291
	leaq	8(%r15), %rcx
	movl	$-1, %eax
	lock xaddl	%eax, (%rcx)
.L292:
	cmpl	$1, %eax
	je	.L444
.L294:
	movq	-2160(%rbp), %rsi
	movq	(%rsi), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L298
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$2, %edx
	call	_ZN2v88internal7Factory22NewSlowJSObjectFromMapENS0_6HandleINS0_3MapEEEiNS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r12
.L299:
	movq	(%r12), %rax
	movq	$0, 31(%rax)
	movq	(%r12), %rdx
	movslq	35(%rdx), %rax
	andl	$-2, %eax
	orl	-2168(%rbp), %eax
	salq	$32, %rax
	movq	%rax, 31(%rdx)
	movq	-2176(%rbp), %rax
	movq	(%r12), %r15
	movq	(%rax), %r14
	leaq	23(%r15), %rsi
	movq	%r14, 23(%r15)
	testb	$1, %r14b
	je	.L331
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -2160(%rbp)
	testl	$262144, %eax
	je	.L301
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -2168(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-2160(%rbp), %rcx
	movq	-2168(%rbp), %rsi
	movq	8(%rcx), %rax
.L301:
	testb	$24, %al
	je	.L331
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L331
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L331:
	movq	-2192(%rbp), %rax
	movq	(%r12), %r15
	movq	(%rax), %r14
	leaq	39(%r15), %rsi
	movq	%r14, 39(%r15)
	testb	$1, %r14b
	je	.L330
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -2160(%rbp)
	testl	$262144, %eax
	je	.L304
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -2168(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-2160(%rbp), %rcx
	movq	-2168(%rbp), %rsi
	movq	8(%rcx), %rax
.L304:
	testb	$24, %al
	je	.L330
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L330
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L330:
	movq	(%r12), %r14
	movq	0(%r13), %r13
	movq	%r13, 47(%r14)
	leaq	47(%r14), %r15
	testb	$1, %r13b
	je	.L329
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -2160(%rbp)
	testl	$262144, %eax
	je	.L307
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-2160(%rbp), %rcx
	movq	8(%rcx), %rax
.L307:
	testb	$24, %al
	je	.L329
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L329
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L329:
	movq	%r12, %r13
.L269:
	movq	-2136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L309
	movq	(%rdi), %rax
	call	*8(%rax)
.L309:
	movq	%rbx, %rdi
	leaq	-1488(%rbp), %r15
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	movq	-1472(%rbp), %r12
	testq	%r12, %r12
	je	.L315
.L310:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L313
	call	_ZdlPv@PLT
.L313:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L314
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L315
.L316:
	movq	%rbx, %r12
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L314:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L316
.L315:
	movq	-2152(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-1744(%rbp), %rdi
	leaq	-1728(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L250
	call	_ZdlPv@PLT
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L435:
	call	_ZdaPv@PLT
	jmp	.L420
.L433:
	movl	$1, %eax
.L254:
	movq	-2152(%rbp), %rcx
	movl	(%rcx,%rax,4), %eax
	movl	%eax, -2168(%rbp)
	call	_ZdaPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	movq	-2152(%rbp), %rdi
	call	_ZdlPv@PLT
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L434:
	call	_ZdaPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	movq	-2152(%rbp), %rdi
	call	_ZdlPv@PLT
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L437:
	movq	-2152(%rbp), %rdi
	movq	%r9, -2216(%rbp)
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	leaq	-1968(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r10, %rdi
	movq	%r10, -2208(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	-2216(%rbp), %r9
	movq	-2208(%rbp), %r10
	movl	-2168(%rbp), %esi
	movq	%r9, %rdx
	movq	%r10, %rdi
	call	_ZN2v88internal12_GLOBAL__N_120CreateICUPluralRulesEPNS0_7IsolateERKN6icu_676LocaleENS0_13JSPluralRules4TypeEPSt10unique_ptrINS4_11PluralRulesESt14default_deleteISB_EE.isra.0
	movq	-2208(%rbp), %r10
	leaq	-976(%rbp), %r8
	movq	%r8, %rdi
	movb	%al, -2217(%rbp)
	movq	%r10, %rsi
	movq	%r10, -2216(%rbp)
	movq	%r8, -2208(%rbp)
	call	_ZN6icu_676number15NumberFormatter10withLocaleERKNS_6LocaleE@PLT
	movq	-2208(%rbp), %r8
	movl	$6, %edx
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZNO6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE12roundingModeE25UNumberFormatRoundingMode@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatteraSEOS1_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	movq	-2208(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_676number24LocalizedNumberFormatterD1Ev@PLT
	movzbl	-2217(%rbp), %ecx
	movq	-2216(%rbp), %r10
	testb	%cl, %cl
	je	.L445
	movq	%r10, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L298:
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r12
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L442:
	movq	%r12, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L440:
	movq	%r12, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L443:
	addl	$1, 8(%r15)
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L279:
	addl	$1, 8(%r15)
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L282:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L291:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L436:
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L271:
	addl	$1, 8(%r15)
	movl	8(%r15), %eax
	movq	%r15, %rdi
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	jmp	.L274
.L438:
	leaq	.LC14(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L439:
	movq	(%rdi), %rax
	movq	%rdi, -2192(%rbp)
	call	*16(%rax)
	testq	%r14, %r14
	movq	-2192(%rbp), %rdi
	je	.L275
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L276:
	cmpl	$1, %eax
	jne	.L270
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L270
.L441:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L285
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L286:
	cmpl	$1, %eax
	jne	.L281
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L281
.L444:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L295
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L296:
	cmpl	$1, %eax
	jne	.L294
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L294
.L275:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L276
.L295:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L296
.L285:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L286
.L339:
	xorl	%eax, %eax
	jmp	.L254
.L431:
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L427:
	call	__stack_chk_fail@PLT
.L429:
	call	_ZSt17__throw_bad_allocv@PLT
.L445:
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18670:
	.size	_ZN2v88internal13JSPluralRules3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_, .-_ZN2v88internal13JSPluralRules3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_
	.section	.rodata._ZN2v88internal13JSPluralRules15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"locale"
.LC16:
	.string	"minimumIntegerDigits"
.LC17:
	.string	"minimumFractionDigits"
.LC18:
	.string	"maximumFractionDigits"
.LC19:
	.string	"minimumSignificantDigits"
.LC20:
	.string	"maximumSignificantDigits"
.LC21:
	.string	"(icu_plural_rules) != nullptr"
.LC22:
	.string	"pluralCategories"
	.section	.text._ZN2v88internal13JSPluralRules15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13JSPluralRules15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal13JSPluralRules15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal13JSPluralRules15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$232, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r13
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L447
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L448:
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L450
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L451:
	leaq	.LC15(%rip), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEPKc
	movq	(%rbx), %rax
	testb	$1, 35(%rax)
	jne	.L453
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	subq	$36368, %rdx
.L454:
	leaq	.LC11(%rip), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEPKc
	movq	(%rbx), %rax
	leaq	-128(%rbp), %rdi
	movl	$0, -188(%rbp)
	leaq	-188(%rbp), %rdx
	movq	%rdi, -232(%rbp)
	movq	47(%rax), %rax
	movq	%rdx, -208(%rbp)
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	call	_ZNK6icu_676number23NumberFormatterSettingsINS0_24LocalizedNumberFormatterEE10toSkeletonER10UErrorCode@PLT
	movl	-188(%rbp), %edi
	testl	%edi, %edi
	jg	.L458
	movq	-232(%rbp), %r15
	leaq	-184(%rbp), %r13
	movq	%r15, %rdi
	call	_ZN2v88internal14JSNumberFormat32MinimumIntegerDigitsFromSkeletonERKN6icu_6713UnicodeStringE@PLT
	leaq	.LC16(%rip), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEiPKc
	leaq	-180(%rbp), %r8
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r8, %rdx
	movq	%r8, -200(%rbp)
	movl	$0, -184(%rbp)
	movl	$0, -180(%rbp)
	call	_ZN2v88internal14JSNumberFormat26FractionDigitsFromSkeletonERKN6icu_6713UnicodeStringEPiS6_@PLT
	movl	-184(%rbp), %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	.LC17(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEiPKc
	movl	-180(%rbp), %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	.LC18(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEiPKc
	movq	-200(%rbp), %r8
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal14JSNumberFormat29SignificantDigitsFromSkeletonERKN6icu_6713UnicodeStringEPiS6_@PLT
	testb	%al, %al
	jne	.L488
.L456:
	movq	(%rbx), %rax
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L489
	movq	-208(%rbp), %rbx
	movq	%rbx, %rsi
	call	_ZNK6icu_6711PluralRules11getKeywordsER10UErrorCode@PLT
	movl	-188(%rbp), %esi
	movq	%rax, %r13
	testl	%esi, %esi
	jg	.L458
	movq	(%rax), %rax
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	*32(%rax)
	movl	-188(%rbp), %ecx
	movl	%eax, %ebx
	testl	%ecx, %ecx
	jg	.L458
	xorl	%edx, %edx
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, -216(%rbp)
	testl	%ebx, %ebx
	jle	.L460
	leal	-1(%rbx), %eax
	movq	%r14, -264(%rbp)
	movl	$16, %ebx
	leaq	24(,%rax,8), %rax
	movq	%rax, -224(%rbp)
.L471:
	movq	0(%r13), %rax
	movq	-208(%rbp), %rsi
	movq	%r13, %rdi
	call	*56(%rax)
	movl	-188(%rbp), %edx
	testl	%edx, %edx
	jg	.L458
	testq	%rax, %rax
	je	.L486
	movb	$0, -144(%rbp)
	leaq	-144(%rbp), %r15
	movq	$0, -152(%rbp)
	movswl	8(%rax), %edx
	movq	%r15, -160(%rbp)
	testw	%dx, %dx
	js	.L462
	sarl	$5, %edx
.L463:
	leaq	-176(%rbp), %r14
	leaq	-160(%rbp), %rsi
	movq	%rax, -200(%rbp)
	movq	%r14, %rdi
	call	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC1EPS6_i
	movq	-200(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZNK6icu_6713UnicodeString6toUTF8ERNS_8ByteSinkE@PLT
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN6icu_678ByteSinkD2Ev@PLT
	movq	-160(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -200(%rbp)
	call	strlen@PLT
	movq	-200(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	movq	%rdx, -176(%rbp)
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L490
	movq	-216(%rbp), %rcx
	movq	(%rax), %rdx
	movq	(%rcx), %rdi
	leaq	-1(%rdi,%rbx), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L472
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -200(%rbp)
	testl	$262144, %eax
	je	.L466
	movq	%rdx, -256(%rbp)
	movq	%rsi, -248(%rbp)
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-200(%rbp), %rcx
	movq	-256(%rbp), %rdx
	movq	-248(%rbp), %rsi
	movq	-240(%rbp), %rdi
	movq	8(%rcx), %rax
.L466:
	testb	$24, %al
	je	.L472
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L491
	.p2align 4,,10
	.p2align 3
.L472:
	movq	-160(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L468
	call	_ZdlPv@PLT
	addq	$8, %rbx
	cmpq	-224(%rbp), %rbx
	jne	.L471
.L486:
	movq	-264(%rbp), %r14
.L460:
	movq	-216(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movq	(%rsi), %rax
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	leaq	.LC22(%rip), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS4_INS0_6ObjectEEEPKc
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	movq	-232(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L492
	addq	$232, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L468:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, -224(%rbp)
	jne	.L471
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L462:
	movl	12(%rax), %edx
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L458:
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L491:
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L450:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L493
.L452:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L447:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L494
.L449:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L453:
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	subq	$35848, %rdx
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L488:
	movl	-184(%rbp), %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	.LC19(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEiPKc
	movl	-180(%rbp), %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	.LC20(%rip), %rcx
	call	_ZN2v88internal12_GLOBAL__N_128CreateDataPropertyForOptionsEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEEiPKc
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L490:
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L489:
	leaq	.LC21(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L494:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L493:
	movq	%r12, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L452
.L492:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18750:
	.size	_ZN2v88internal13JSPluralRules15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal13JSPluralRules15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.rodata._ZN2v88internal13JSPluralRules13ResolvePluralEPNS0_7IsolateENS0_6HandleIS1_EEd.str1.1,"aMS",@progbits,1
.LC23:
	.string	"(fmt) != nullptr"
	.section	.text._ZN2v88internal13JSPluralRules13ResolvePluralEPNS0_7IsolateENS0_6HandleIS1_EEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13JSPluralRules13ResolvePluralEPNS0_7IsolateENS0_6HandleIS1_EEd
	.type	_ZN2v88internal13JSPluralRules13ResolvePluralEPNS0_7IsolateENS0_6HandleIS1_EEd, @function
_ZN2v88internal13JSPluralRules13ResolvePluralEPNS0_7IsolateENS0_6HandleIS1_EEd:
.LFB18747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	39(%rax), %rdx
	movq	7(%rdx), %rdx
	movq	24(%rdx), %rdx
	movq	(%rdx), %r15
	testq	%r15, %r15
	je	.L502
	movq	47(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L503
	leaq	-160(%rbp), %r13
	movq	%rdi, %r12
	leaq	-164(%rbp), %rbx
	movl	$0, -164(%rbp)
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZNK6icu_676number24LocalizedNumberFormatter12formatDoubleEdR10UErrorCode@PLT
	movl	-164(%rbp), %edx
	testl	%edx, %edx
	jg	.L499
	leaq	-128(%rbp), %r14
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6711PluralRules6selectERKNS_6number15FormattedNumberER10UErrorCode@PLT
	movl	-164(%rbp), %eax
	testl	%eax, %eax
	jg	.L499
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Intl8ToStringEPNS0_7IsolateERKN6icu_6713UnicodeStringE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676number15FormattedNumberD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L504
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L502:
	leaq	.LC21(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L503:
	leaq	.LC23(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L504:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18747:
	.size	_ZN2v88internal13JSPluralRules13ResolvePluralEPNS0_7IsolateENS0_6HandleIS1_EEd, .-_ZN2v88internal13JSPluralRules13ResolvePluralEPNS0_7IsolateENS0_6HandleIS1_EEd
	.weak	_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.data.rel.ro._ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"awG",@progbits,_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,comdat
	.align 8
	.type	_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @object
	.size	_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, 56
_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
	.quad	0
	.quad	0
	.quad	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev
	.quad	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.quad	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci
	.quad	_ZN6icu_678ByteSink15GetAppendBufferEiiPciPi
	.quad	_ZN6icu_678ByteSink5FlushEv
	.weak	_ZTVSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_6711PluralRulesESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt15_Sp_counted_ptrIPN6icu_676number24LocalizedNumberFormatterELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.bss._ZZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11EvE17available_locales,"aw",@nobits
	.align 32
	.type	_ZZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11EvE17available_locales, @object
	.size	_ZZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11EvE17available_locales, 56
_ZZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11EvE17available_locales:
	.zero	56
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
