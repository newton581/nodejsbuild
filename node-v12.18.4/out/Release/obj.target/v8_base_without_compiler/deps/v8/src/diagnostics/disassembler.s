	.file	"disassembler.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB3330:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE3330:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.rodata._ZNK2v88internal15V8NameConverter10NameInCodeEPh.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.section	.text._ZNK2v88internal15V8NameConverter10NameInCodeEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15V8NameConverter10NameInCodeEPh
	.type	_ZNK2v88internal15V8NameConverter10NameInCodeEPh, @function
_ZNK2v88internal15V8NameConverter10NameInCodeEPh:
.LFB19929:
	.cfi_startproc
	endbr64
	movl	160(%rdi), %eax
	testl	%eax, %eax
	leaq	.LC0(%rip), %rax
	cmovne	%rsi, %rax
	ret
	.cfi_endproc
.LFE19929:
	.size	_ZNK2v88internal15V8NameConverter10NameInCodeEPh, .-_ZNK2v88internal15V8NameConverter10NameInCodeEPh
	.section	.rodata._ZNK2v88internal15V8NameConverter13NameOfAddressEPh.str1.1,"aMS",@progbits,1
.LC1:
	.string	"%p  (%s)"
.LC2:
	.string	"%p  <+0x%x>"
	.section	.text._ZNK2v88internal15V8NameConverter13NameOfAddressEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15V8NameConverter13NameOfAddressEPh
	.type	_ZNK2v88internal15V8NameConverter13NameOfAddressEPh, @function
_ZNK2v88internal15V8NameConverter13NameOfAddressEPh:
.LFB19928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	160(%rdi), %eax
	testl	%eax, %eax
	je	.L7
	movq	152(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L8
	addq	$41184, %rdi
	call	_ZN2v88internal8Builtins6LookupEm@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L8
	movq	176(%r12), %rdi
	movq	%r13, %rcx
	leaq	.LC1(%rip), %rdx
	xorl	%eax, %eax
	movq	184(%r12), %rsi
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	176(%r12), %rax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	160(%r12), %r14
	movl	%r13d, %ebx
	movq	%r14, %rdi
	call	_ZNK2v88internal13CodeReference17instruction_startEv@PLT
	subl	%eax, %ebx
	js	.L13
	movq	%r14, %rdi
	call	_ZNK2v88internal13CodeReference16instruction_sizeEv@PLT
	cmpl	%ebx, %eax
	jg	.L26
.L13:
	leaq	-112(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	152(%r12), %rax
	testq	%rax, %rax
	je	.L12
	movq	45752(%rax), %rdi
	movq	%r13, %rsi
	addq	$280, %rdi
	call	_ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm@PLT
	testq	%rax, %rax
	jne	.L27
.L12:
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
.L7:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK6disasm13NameConverter13NameOfAddressEPh@PLT
.L6:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L28
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movl	60(%rax), %edi
	call	_ZN2v88internal4wasm23GetWasmCodeKindAsStringENS1_8WasmCode4KindE@PLT
	movq	176(%r12), %rdi
	movq	%r13, %rcx
	movq	184(%r12), %rsi
	movq	%rax, %r8
	leaq	.LC1(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	176(%r12), %rax
	movq	%r14, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	movq	-120(%rbp), %rax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L26:
	movq	176(%r12), %rdi
	movl	%ebx, %r8d
	movq	%r13, %rcx
	xorl	%eax, %eax
	movq	184(%r12), %rsi
	leaq	.LC2(%rip), %rdx
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	176(%r12), %rax
	jmp	.L6
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19928:
	.size	_ZNK2v88internal15V8NameConverter13NameOfAddressEPh, .-_ZNK2v88internal15V8NameConverter13NameOfAddressEPh
	.section	.text._ZN2v88internalL10DumpBufferEPSoPNS0_13StringBuilderE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL10DumpBufferEPSoPNS0_13StringBuilderE, @function
_ZN2v88internalL10DumpBufferEPSoPNS0_13StringBuilderE:
.LFB19931:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal19SimpleStringBuilder8FinalizeEv@PLT
	testq	%rax, %rax
	je	.L37
	movq	%rax, %rdi
	movq	%rax, %r13
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L31:
	movq	(%r12), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r13
	testq	%r13, %r13
	je	.L38
	cmpb	$0, 56(%r13)
	je	.L33
	movsbl	67(%r13), %esi
.L34:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movl	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L34
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L37:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L31
.L38:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE19931:
	.size	_ZN2v88internalL10DumpBufferEPSoPNS0_13StringBuilderE, .-_ZN2v88internalL10DumpBufferEPSoPNS0_13StringBuilderE
	.section	.text._ZN2v88internal15V8NameConverterD2Ev,"axG",@progbits,_ZN2v88internal15V8NameConverterD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15V8NameConverterD2Ev
	.type	_ZN2v88internal15V8NameConverterD2Ev, @function
_ZN2v88internal15V8NameConverterD2Ev:
.LFB24526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal15V8NameConverterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	336(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L40
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L41
.L40:
	movq	328(%rbx), %rax
	movq	320(%rbx), %rdi
	xorl	%esi, %esi
	addq	$368, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L39
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24526:
	.size	_ZN2v88internal15V8NameConverterD2Ev, .-_ZN2v88internal15V8NameConverterD2Ev
	.weak	_ZN2v88internal15V8NameConverterD1Ev
	.set	_ZN2v88internal15V8NameConverterD1Ev,_ZN2v88internal15V8NameConverterD2Ev
	.section	.text._ZN2v88internal15V8NameConverterD0Ev,"axG",@progbits,_ZN2v88internal15V8NameConverterD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15V8NameConverterD0Ev
	.type	_ZN2v88internal15V8NameConverterD0Ev, @function
_ZN2v88internal15V8NameConverterD0Ev:
.LFB24528:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal15V8NameConverterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	336(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%rbx, %rbx
	je	.L49
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L50
.L49:
	movq	328(%r12), %rax
	movq	320(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	320(%r12), %rdi
	leaq	368(%r12), %rax
	movq	$0, 344(%r12)
	movq	$0, 336(%r12)
	cmpq	%rax, %rdi
	je	.L51
	call	_ZdlPv@PLT
.L51:
	popq	%rbx
	movq	%r12, %rdi
	movl	$376, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24528:
	.size	_ZN2v88internal15V8NameConverterD0Ev, .-_ZN2v88internal15V8NameConverterD0Ev
	.section	.text._ZNK2v88internal15V8NameConverter21InitExternalRefsCacheEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15V8NameConverter21InitExternalRefsCacheEv
	.type	_ZNK2v88internal15V8NameConverter21InitExternalRefsCacheEv, @function
_ZNK2v88internal15V8NameConverter21InitExternalRefsCacheEv:
.LFB19919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	152(%rdi), %rax
	movl	12440(%rax), %edx
	movq	%rax, -56(%rbp)
	testl	%edx, %edx
	je	.L57
	leaq	128(%rax), %rbx
	leaq	4856(%rax), %r12
	movq	%rdi, %r15
	leaq	352(%rdi), %rax
	movq	%rbx, -72(%rbp)
	leaq	_ZN2v88internal22ExternalReferenceTable9ref_name_E(%rip), %rbx
	movq	%rax, -96(%rbp)
	leaq	336(%rdi), %rax
	movq	%rax, -88(%rbp)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L59:
	addq	$8, %rbx
	leaq	7584+_ZN2v88internal22ExternalReferenceTable9ref_name_E(%rip), %rax
	addq	$8, %r12
	cmpq	%rbx, %rax
	je	.L57
.L77:
	movq	(%r12), %r13
	movq	%r13, %rax
	subq	-56(%rbp), %rax
	cmpq	$37583, %rax
	ja	.L59
	movq	(%rbx), %rdx
	movl	$24, %edi
	subl	-72(%rbp), %r13d
	movq	%rdx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rdx
	movq	328(%r15), %r11
	movslq	%r13d, %r9
	movq	$0, (%rax)
	movq	%rax, %r14
	movq	%rdx, 16(%rax)
	xorl	%edx, %edx
	movl	%r13d, 8(%rax)
	movq	%r9, %rax
	divq	%r11
	movq	320(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rdi
	leaq	0(,%rdx,8), %r10
	testq	%rax, %rax
	je	.L60
	movq	(%rax), %rcx
	movl	8(%rcx), %esi
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L99:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L60
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%r11
	cmpq	%rdx, %rdi
	jne	.L60
.L62:
	cmpl	%esi, %r13d
	jne	.L99
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L60:
	movq	-96(%rbp), %rdi
	movl	$1, %ecx
	movq	%r11, %rsi
	movq	%r9, -64(%rbp)
	movq	344(%r15), %rdx
	movq	%r10, -80(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	-64(%rbp), %r9
	testb	%al, %al
	movq	%rdx, %r13
	jne	.L63
	movq	320(%r15), %r11
	movq	-80(%rbp), %r10
	leaq	(%r11,%r10), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L73
.L102:
	movq	(%rdx), %rdx
	movq	%rdx, (%r14)
	movq	(%rax), %rax
	movq	%r14, (%rax)
.L74:
	addq	$1, 344(%r15)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L63:
	cmpq	$1, %rdx
	je	.L100
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L101
	leaq	0(,%rdx,8), %rdx
	movq	%r9, -80(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-80(%rbp), %r9
	leaq	368(%r15), %r8
	movq	%rax, %r11
.L66:
	movq	336(%r15), %rsi
	movq	$0, 336(%r15)
	testq	%rsi, %rsi
	je	.L68
	xorl	%r10d, %r10d
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L70:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%rdi), %rax
	movq	%rcx, (%rax)
.L71:
	testq	%rsi, %rsi
	je	.L68
.L69:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r13
	leaq	(%r11,%rdx,8), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L70
	movq	336(%r15), %rax
	movq	%rax, (%rcx)
	movq	-88(%rbp), %rax
	movq	%rcx, 336(%r15)
	movq	%rax, (%rdi)
	cmpq	$0, (%rcx)
	je	.L79
	movq	%rcx, (%r11,%r10,8)
	movq	%rdx, %r10
	testq	%rsi, %rsi
	jne	.L69
	.p2align 4,,10
	.p2align 3
.L68:
	movq	320(%r15), %rdi
	cmpq	%r8, %rdi
	je	.L72
	movq	%r11, -80(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %r11
	movq	-64(%rbp), %r9
.L72:
	movq	%r9, %rax
	xorl	%edx, %edx
	movq	%r13, 328(%r15)
	divq	%r13
	movq	%r11, 320(%r15)
	leaq	0(,%rdx,8), %r10
	leaq	(%r11,%r10), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L102
.L73:
	movq	336(%r15), %rdx
	movq	%r14, 336(%r15)
	movq	%rdx, (%r14)
	testq	%rdx, %rdx
	je	.L75
	movslq	8(%rdx), %rax
	xorl	%edx, %edx
	divq	328(%r15)
	movq	%r14, (%r11,%rdx,8)
	movq	320(%r15), %rax
	addq	%r10, %rax
.L75:
	movq	-88(%rbp), %rsi
	movq	%rsi, (%rax)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%rdx, %r10
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L57:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L100:
	.cfi_restore_state
	leaq	368(%r15), %r11
	movq	$0, 368(%r15)
	movq	%r11, %r8
	jmp	.L66
.L101:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE19919:
	.size	_ZNK2v88internal15V8NameConverter21InitExternalRefsCacheEv, .-_ZNK2v88internal15V8NameConverter21InitExternalRefsCacheEv
	.section	.rodata._ZNK2v88internal15V8NameConverter16RootRelativeNameEi.str1.1,"aMS",@progbits,1
.LC3:
	.string	"root (%s)"
.LC4:
	.string	"external reference (%s)"
.LC5:
	.string	"builtin (%s)"
.LC6:
	.string	"external value (%s)"
	.section	.text._ZNK2v88internal15V8NameConverter16RootRelativeNameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15V8NameConverter16RootRelativeNameEi
	.type	_ZNK2v88internal15V8NameConverter16RootRelativeNameEi, @function
_ZNK2v88internal15V8NameConverter16RootRelativeNameEi:
.LFB19930:
	.cfi_startproc
	endbr64
	movq	152(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L122
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	72(%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$4799, %eax
	ja	.L106
	testb	$7, %al
	jne	.L107
	shrl	$3, %eax
	leaq	_ZN2v88internal10RootsTable11root_names_E(%rip), %rdx
	movq	176(%rdi), %rdi
	movq	184(%rbx), %rsi
	movq	(%rdx,%rax,8), %rcx
	leaq	.LC3(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	176(%rbx), %rax
.L103:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	leal	-4728(%rsi), %eax
	cmpl	$7591, %eax
	ja	.L108
	testb	$7, %al
	jne	.L107
	movl	12440(%rdx), %edx
	testl	%edx, %edx
	je	.L107
	shrl	$3, %eax
	leaq	_ZN2v88internal22ExternalReferenceTable9ref_name_E(%rip), %rdx
	movq	184(%rbx), %rsi
	movq	176(%rdi), %rdi
	movq	(%rdx,%rax,8), %rcx
	leaq	.LC4(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	176(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	leal	-24936(%rsi), %edi
	cmpl	$12423, %edi
	jbe	.L123
	cmpq	$0, 344(%rbx)
	je	.L124
.L110:
	movq	328(%rbx), %rdi
	movslq	%r12d, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	320(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L107
	movq	(%rax), %rcx
	movl	8(%rcx), %esi
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L125:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L107
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L107
.L112:
	cmpl	%esi, %r12d
	jne	.L125
	movq	176(%rbx), %rdi
	movq	184(%rbx), %rsi
	leaq	.LC6(%rip), %rdx
	xorl	%eax, %eax
	movq	16(%rcx), %rcx
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	176(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	shrl	$3, %edi
	call	_ZN2v88internal8Builtins4nameEi@PLT
	movq	176(%rbx), %rdi
	movq	184(%rbx), %rsi
	leaq	.LC5(%rip), %rdx
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	176(%rbx), %rax
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%rbx, %rdi
	call	_ZNK2v88internal15V8NameConverter21InitExternalRefsCacheEv
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE19930:
	.size	_ZNK2v88internal15V8NameConverter16RootRelativeNameEi, .-_ZNK2v88internal15V8NameConverter16RootRelativeNameEi
	.section	.rodata._ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC7:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.type	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, @function
_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_:
.LFB23866:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L140
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L136
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L141
.L128:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L135:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L142
	testq	%r13, %r13
	jg	.L131
	testq	%r9, %r9
	jne	.L134
.L132:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L131
.L134:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L141:
	testq	%rsi, %rsi
	jne	.L129
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L132
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L136:
	movl	$8, %r14d
	jmp	.L128
.L140:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L129:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L128
	.cfi_endproc
.LFE23866:
	.size	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, .-_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal9RelocInfo4ModeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal9RelocInfo4ModeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal9RelocInfo4ModeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal9RelocInfo4ModeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal9RelocInfo4ModeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB23869:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L157
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L152
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L158
.L154:
	movq	%rcx, %r14
.L145:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L151:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L159
	testq	%rsi, %rsi
	jg	.L147
	testq	%r15, %r15
	jne	.L150
.L148:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L147
.L150:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L158:
	testq	%r14, %r14
	js	.L154
	jne	.L145
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L147:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L148
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L152:
	movl	$1, %r14d
	jmp	.L145
.L157:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23869:
	.size	_ZNSt6vectorIN2v88internal9RelocInfo4ModeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal9RelocInfo4ModeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIlSaIlEE17_M_realloc_insertIJlEEEvN9__gnu_cxx17__normal_iteratorIPlS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIlSaIlEE17_M_realloc_insertIJlEEEvN9__gnu_cxx17__normal_iteratorIPlS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIlSaIlEE17_M_realloc_insertIJlEEEvN9__gnu_cxx17__normal_iteratorIPlS1_EEDpOT_
	.type	_ZNSt6vectorIlSaIlEE17_M_realloc_insertIJlEEEvN9__gnu_cxx17__normal_iteratorIPlS1_EEDpOT_, @function
_ZNSt6vectorIlSaIlEE17_M_realloc_insertIJlEEEvN9__gnu_cxx17__normal_iteratorIPlS1_EEDpOT_:
.LFB23874:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L174
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L170
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L175
.L162:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L169:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L176
	testq	%r13, %r13
	jg	.L165
	testq	%r9, %r9
	jne	.L168
.L166:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L165
.L168:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L175:
	testq	%rsi, %rsi
	jne	.L163
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L166
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L170:
	movl	$8, %r14d
	jmp	.L162
.L174:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L163:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L162
	.cfi_endproc
.LFE23874:
	.size	_ZNSt6vectorIlSaIlEE17_M_realloc_insertIJlEEEvN9__gnu_cxx17__normal_iteratorIPlS1_EEDpOT_, .-_ZNSt6vectorIlSaIlEE17_M_realloc_insertIJlEEEvN9__gnu_cxx17__normal_iteratorIPlS1_EEDpOT_
	.section	.text._ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB23879:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L191
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L187
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L192
.L179:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L186:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L193
	testq	%r13, %r13
	jg	.L182
	testq	%r9, %r9
	jne	.L185
.L183:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L182
.L185:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L192:
	testq	%rsi, %rsi
	jne	.L180
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L182:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L183
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$8, %r14d
	jmp	.L179
.L191:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L180:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L179
	.cfi_endproc
.LFE23879:
	.size	_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.rodata._ZN2v88internalL8DecodeItEPNS0_7IsolateEPNS0_24ExternalReferenceEncoderEPSoNS0_13CodeReferenceERKNS0_15V8NameConverterEPhSA_m.str1.1,"aMS",@progbits,1
.LC8:
	.string	"unknown"
.LC9:
	.string	"!code.is_null()"
.LC10:
	.string	"Check failed: %s."
.LC11:
	.string	"%08x       constant"
	.section	.rodata._ZN2v88internalL8DecodeItEPNS0_7IsolateEPNS0_24ExternalReferenceEncoderEPSoNS0_13CodeReferenceERKNS0_15V8NameConverterEPhSA_m.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"%08x       constant pool begin (num_const = %d)"
	.align 8
.LC13:
	.string	"%08lx      jump table entry %4zu"
	.section	.rodata._ZN2v88internalL8DecodeItEPNS0_7IsolateEPNS0_24ExternalReferenceEncoderEPSoNS0_13CodeReferenceERKNS0_15V8NameConverterEPhSA_m.str1.1
.LC14:
	.string	"                  %s"
.LC15:
	.string	"\033[33;1m"
.LC16:
	.string	"%p  %4tx  "
.LC17:
	.string	"%s"
	.section	.rodata._ZN2v88internalL8DecodeItEPNS0_7IsolateEPNS0_24ExternalReferenceEncoderEPSoNS0_13CodeReferenceERKNS0_15V8NameConverterEPhSA_m.str1.8
	.align 8
.LC18:
	.string	"    ;; debug: deopt position, script offset '%d'"
	.align 8
.LC19:
	.string	"    ;; debug: deopt position, inlining id '%d'"
	.align 8
.LC20:
	.string	"    ;; debug: deopt reason '%s'"
	.section	.rodata._ZN2v88internalL8DecodeItEPNS0_7IsolateEPNS0_24ExternalReferenceEncoderEPSoNS0_13CodeReferenceERKNS0_15V8NameConverterEPhSA_m.str1.1
.LC21:
	.string	"    ;; debug: deopt index %d"
.LC22:
	.string	"    ;; %sobject: %s"
	.section	.rodata._ZN2v88internalL8DecodeItEPNS0_7IsolateEPNS0_24ExternalReferenceEncoderEPSoNS0_13CodeReferenceERKNS0_15V8NameConverterEPhSA_m.str1.8
	.align 8
.LC23:
	.string	"    ;; external reference (%s)"
	.section	.rodata._ZN2v88internalL8DecodeItEPNS0_7IsolateEPNS0_24ExternalReferenceEncoderEPSoNS0_13CodeReferenceERKNS0_15V8NameConverterEPhSA_m.str1.1
.LC24:
	.string	"    ;; code:"
.LC25:
	.string	" Builtin::%s"
.LC26:
	.string	" %s"
.LC27:
	.string	"    ;; wasm stub: %s"
.LC28:
	.string	"    ;; %s"
	.section	.rodata._ZN2v88internalL8DecodeItEPNS0_7IsolateEPNS0_24ExternalReferenceEncoderEPSoNS0_13CodeReferenceERKNS0_15V8NameConverterEPhSA_m.str1.8
	.align 8
.LC29:
	.string	"    ;; %s deoptimization bailout"
	.section	.rodata._ZN2v88internalL8DecodeItEPNS0_7IsolateEPNS0_24ExternalReferenceEncoderEPSoNS0_13CodeReferenceERKNS0_15V8NameConverterEPhSA_m.str1.1
.LC30:
	.string	"unreachable code"
.LC31:
	.string	"\033[m"
	.section	.text._ZN2v88internalL8DecodeItEPNS0_7IsolateEPNS0_24ExternalReferenceEncoderEPSoNS0_13CodeReferenceERKNS0_15V8NameConverterEPhSA_m,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL8DecodeItEPNS0_7IsolateEPNS0_24ExternalReferenceEncoderEPSoNS0_13CodeReferenceERKNS0_15V8NameConverterEPhSA_m, @function
_ZN2v88internalL8DecodeItEPNS0_7IsolateEPNS0_24ExternalReferenceEncoderEPSoNS0_13CodeReferenceERKNS0_15V8NameConverterEPhSA_m:
.LFB19938:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$3688, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rdi, -3680(%rbp)
	movq	%rsi, -3720(%rbp)
	movq	%rax, -3672(%rbp)
	movq	24(%rbp), %rax
	movq	%rcx, -3632(%rbp)
	movq	%r8, -3624(%rbp)
	movq	%rax, -3712(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.L315
	leaq	-3632(%rbp), %rbx
	leaq	-3280(%rbp), %rax
	movq	%rdx, %r15
	movq	%r9, -3600(%rbp)
	movq	%rax, -3296(%rbp)
	movq	%rbx, %rdi
	leaq	-3136(%rbp), %rax
	leaq	-3536(%rbp), %r12
	movq	$128, -3288(%rbp)
	movq	%rax, -3152(%rbp)
	movq	$3072, -3144(%rbp)
	movq	%rax, -3568(%rbp)
	movq	$3072, -3560(%rbp)
	movl	$0, -3552(%rbp)
	movb	$0, -3592(%rbp)
	movq	%rbx, -3664(%rbp)
	call	_ZNK2v88internal13CodeReference18code_comments_sizeEv@PLT
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal13CodeReference13code_commentsEv@PLT
	movl	%r13d, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal20CodeCommentsIteratorC1Emj@PLT
	cmpq	$0, -3680(%rbp)
	je	.L198
	movq	-3672(%rbp), %rsi
	movq	-3680(%rbp), %rdi
	call	_ZN2v88internal17InstructionStream11PcIsOffHeapEPNS0_7IsolateEm@PLT
	testb	%al, %al
	jne	.L316
.L198:
	movq	-3632(%rbp), %r14
	movq	-3624(%rbp), %rbx
	movl	$64, %edi
	call	_ZN2v88internal8MallocednwEm@PLT
	movl	$-1, %ecx
	movq	%r14, %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal13RelocIteratorC1ENS0_13CodeReferenceEi@PLT
.L197:
	movq	-3672(%rbp), %rax
	leaq	-3568(%rbp), %rbx
	movl	$-1, -3684(%rbp)
	movq	%rax, -3656(%rbp)
	cmpq	%rax, -3712(%rbp)
	jbe	.L317
	movl	-3684(%rbp), %r14d
	testl	%r14d, %r14d
	jle	.L201
	.p2align 4,,10
	.p2align 3
.L332:
	movq	-3656(%rbp), %rax
	movq	-3296(%rbp), %rdi
	subl	$1, %r14d
	leaq	.LC11(%rip), %rdx
	movq	-3288(%rbp), %rsi
	movl	(%rax), %ecx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-3656(%rbp), %rax
	movl	%r14d, -3684(%rbp)
	addq	$4, %rax
	movq	%rax, -3648(%rbp)
.L202:
	movq	$0, -3488(%rbp)
	pxor	%xmm0, %xmm0
	movq	$0, -3456(%rbp)
	movq	$0, -3424(%rbp)
	movq	$0, -3392(%rbp)
	movaps	%xmm0, -3504(%rbp)
	movaps	%xmm0, -3472(%rbp)
	movaps	%xmm0, -3440(%rbp)
	movaps	%xmm0, -3408(%rbp)
	testq	%r13, %r13
	jne	.L206
.L208:
	movq	-3648(%rbp), %r14
	subq	-3672(%rbp), %r14
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L318:
	movq	%rax, (%rsi)
	addq	$8, -3496(%rbp)
.L224:
	movq	%r12, %rdi
	call	_ZN2v88internal20CodeCommentsIterator4NextEv@PLT
.L207:
	movq	%r12, %rdi
	call	_ZNK2v88internal20CodeCommentsIterator10HasCurrentEv@PLT
	testb	%al, %al
	je	.L220
	movq	%r12, %rdi
	call	_ZNK2v88internal20CodeCommentsIterator11GetPCOffsetEv@PLT
	movl	%eax, %eax
	cmpq	%r14, %rax
	jnb	.L220
	movq	%r12, %rdi
	call	_ZNK2v88internal20CodeCommentsIterator10GetCommentEv@PLT
	movq	-3496(%rbp), %rsi
	movq	%rax, -3344(%rbp)
	cmpq	-3488(%rbp), %rsi
	jne	.L318
	leaq	-3344(%rbp), %rdx
	leaq	-3504(%rbp), %rdi
	call	_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L220:
	movq	-3504(%rbp), %rdx
	xorl	%r14d, %r14d
	cmpq	-3496(%rbp), %rdx
	je	.L218
	.p2align 4,,10
	.p2align 3
.L217:
	movq	(%rdx,%r14,8), %rdx
	xorl	%eax, %eax
	leaq	.LC14(%rip), %rsi
	movq	%rbx, %rdi
	addq	$1, %r14
	call	_ZN2v88internal13StringBuilder12AddFormattedEPKcz@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internalL10DumpBufferEPSoPNS0_13StringBuilderE
	movq	-3504(%rbp), %rdx
	movq	-3496(%rbp), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r14, %rax
	ja	.L217
.L218:
	cmpb	$0, _ZN2v88internal15FLAG_log_colourE(%rip)
	je	.L222
	movq	-3656(%rbp), %rax
	cmpq	32(%rbp), %rax
	je	.L319
.L222:
	movq	-3656(%rbp), %rdx
	leaq	.LC16(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	movq	%rdx, %rcx
	subq	-3672(%rbp), %rcx
	call	_ZN2v88internal13StringBuilder12AddFormattedEPKcz@PLT
	movq	-3296(%rbp), %rdx
	leaq	.LC17(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	_ZN2v88internal13StringBuilder12AddFormattedEPKcz@PLT
	movq	-3472(%rbp), %rdx
	cmpq	-3464(%rbp), %rdx
	je	.L225
	xorl	%r14d, %r14d
	movq	%r12, -3704(%rbp)
	movq	%r13, -3696(%rbp)
	movq	%r14, %r12
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L330:
	movq	%rbx, %rdi
	movl	$57, %edx
	movl	$32, %esi
	subl	-3552(%rbp), %edx
	call	_ZN2v88internal19SimpleStringBuilder10AddPaddingEci@PLT
	movsbl	-3336(%rbp), %edi
	cmpb	$13, %dil
	je	.L320
.L231:
	cmpb	$14, %dil
	je	.L321
	cmpb	$15, %dil
	je	.L322
	cmpb	$16, %dil
	je	.L323
	leal	-2(%rdi), %edx
	cmpb	$1, %dl
	jbe	.L324
	cmpb	$7, %dil
	je	.L325
	cmpb	$1, %dil
	jle	.L326
	cmpb	$5, %dil
	jne	.L243
	cmpl	$2, %r13d
	je	.L327
.L243:
	cmpb	$6, %dil
	jne	.L314
	movq	-3680(%rbp), %rax
	testq	%rax, %rax
	je	.L314
	cmpq	$0, 41040(%rax)
	je	.L314
	movq	%rax, %rdi
	movq	-3344(%rbp), %rax
	leaq	-3376(%rbp), %rdx
	movslq	(%rax), %rcx
	leaq	4(%rax,%rcx), %rsi
	call	_ZN2v88internal11Deoptimizer21IsDeoptimizationEntryEPNS0_7IsolateEmPNS0_14DeoptimizeKindE@PLT
	testb	%al, %al
	je	.L246
	movzbl	-3376(%rbp), %edi
	call	_ZN2v88internal11Deoptimizer10MessageForENS0_14DeoptimizeKindE@PLT
	leaq	.LC29(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal13StringBuilder12AddFormattedEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L232:
	movq	-3464(%rbp), %rcx
	movq	-3472(%rbp), %rdx
	addq	$1, %r12
	movq	%rcx, %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r12, %rax
	jbe	.L328
.L248:
	movl	-3632(%rbp), %r13d
	testl	%r13d, %r13d
	jne	.L329
	movq	$0, -3640(%rbp)
	movq	-3624(%rbp), %r14
	pxor	%xmm0, %xmm0
.L228:
	movq	-3408(%rbp), %rcx
	movq	-3440(%rbp), %rsi
	movhps	-3640(%rbp), %xmm0
	movq	(%rdx,%r12,8), %rdx
	movq	(%rcx,%r12,8), %rcx
	movzbl	(%rsi,%r12), %esi
	movups	%xmm0, -3320(%rbp)
	movq	%rdx, -3344(%rbp)
	movb	%sil, -3336(%rbp)
	movq	%rcx, -3328(%rbp)
	testq	%r12, %r12
	je	.L330
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internalL10DumpBufferEPSoPNS0_13StringBuilderE
	movq	%rbx, %rdi
	movl	$57, %edx
	movl	$32, %esi
	call	_ZN2v88internal19SimpleStringBuilder10AddPaddingEci@PLT
	movsbl	-3336(%rbp), %edi
	cmpb	$13, %dil
	jne	.L231
.L320:
	movl	-3328(%rbp), %edx
	leaq	.LC18(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal13StringBuilder12AddFormattedEPKcz@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L329:
	movq	-3664(%rbp), %rdi
	call	_ZNK2v88internal13CodeReference13constant_poolEv@PLT
	movl	-3632(%rbp), %r13d
	movq	-3624(%rbp), %r14
	movq	%rax, -3640(%rbp)
	cmpl	$1, %r13d
	je	.L227
	movq	-3472(%rbp), %rdx
	pxor	%xmm0, %xmm0
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L227:
	movq	(%r14), %xmm0
	movq	-3472(%rbp), %rdx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L321:
	movl	-3328(%rbp), %edx
	leaq	.LC19(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal13StringBuilder12AddFormattedEPKcz@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L322:
	movzbl	-3328(%rbp), %edi
	call	_ZN2v88internal24DeoptimizeReasonToStringENS0_16DeoptimizeReasonE@PLT
	leaq	.LC20(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal13StringBuilder12AddFormattedEPKcz@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L246:
	movl	$6, %edi
	.p2align 4,,10
	.p2align 3
.L314:
	call	_ZN2v88internal9RelocInfo13RelocModeNameENS1_4ModeE@PLT
	leaq	.LC28(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal13StringBuilder12AddFormattedEPKcz@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L328:
	movq	-3696(%rbp), %r13
	movq	-3704(%rbp), %r12
	cmpq	%rcx, %rdx
	je	.L225
.L249:
	cmpb	$0, _ZN2v88internal15FLAG_log_colourE(%rip)
	je	.L251
	movq	-3656(%rbp), %rax
	cmpq	32(%rbp), %rax
	je	.L331
.L251:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internalL10DumpBufferEPSoPNS0_13StringBuilderE
	movq	-3408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L252
	call	_ZdlPv@PLT
.L252:
	movq	-3440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L253
	call	_ZdlPv@PLT
.L253:
	movq	-3472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L254
	call	_ZdlPv@PLT
.L254:
	movq	-3504(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L255
	call	_ZdlPv@PLT
	movq	-3712(%rbp), %rcx
	cmpq	%rcx, -3648(%rbp)
	jnb	.L256
.L257:
	movq	-3648(%rbp), %rax
	movl	-3684(%rbp), %r14d
	movq	%rax, -3656(%rbp)
	testl	%r14d, %r14d
	jg	.L332
.L201:
	movq	-3656(%rbp), %rsi
	leaq	-3600(%rbp), %rdi
	movq	%rdi, -3640(%rbp)
	call	_ZN6disasm12Disassembler18ConstantPoolSizeAtEPh@PLT
	movq	-3640(%rbp), %rdi
	testl	%eax, %eax
	movl	%eax, %r14d
	jns	.L333
	testq	%r13, %r13
	je	.L204
	cmpb	$0, 56(%r13)
	jne	.L204
	movq	-3656(%rbp), %rax
	cmpq	16(%r13), %rax
	je	.L334
.L204:
	movq	-3296(%rbp), %rax
	movq	-3656(%rbp), %r14
	movb	$0, (%rax)
	movq	-3296(%rbp), %rsi
	movq	%r14, %rcx
	movq	-3288(%rbp), %rdx
	call	_ZN6disasm12Disassembler17InstructionDecodeEN2v88internal6VectorIcEEPh@PLT
	cltq
	addq	%r14, %rax
	movq	%rax, -3648(%rbp)
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L324:
	leaq	16+_ZTVN2v88internal19HeapStringAllocatorE(%rip), %rax
	movl	$16, %esi
	leaq	-3584(%rbp), %rdi
	movl	$0, -3360(%rbp)
	movq	%rax, -3584(%rbp)
	leaq	-3376(%rbp), %r13
	movabsq	$68719476737, %rax
	leaq	-3608(%rbp), %r14
	movq	%rdi, -3376(%rbp)
	movq	%rax, -3368(%rbp)
	call	_ZN2v88internal19HeapStringAllocator8allocateEj@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -3352(%rbp)
	movb	$0, (%rax)
	movq	-3344(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -3608(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEPNS0_12StringStreamE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal12StringStream9ToCStringEv@PLT
	movq	-3608(%rbp), %rcx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v88internal13StringBuilder12AddFormattedEPKcz@PLT
	movq	-3608(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L237
	call	_ZdaPv@PLT
.L237:
	movq	-3576(%rbp), %rdi
	leaq	16+_ZTVN2v88internal19HeapStringAllocatorE(%rip), %rax
	movq	%rax, -3584(%rbp)
	testq	%rdi, %rdi
	je	.L232
	call	_ZdaPv@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L323:
	movl	-3328(%rbp), %edx
	leaq	.LC21(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal13StringBuilder12AddFormattedEPKcz@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L325:
	movq	-3720(%rbp), %rdi
	leaq	.LC8(%rip), %rdx
	testq	%rdi, %rdi
	je	.L240
	movq	-3344(%rbp), %rax
	movq	-3680(%rbp), %rsi
	movq	(%rax), %rdx
	call	_ZNK2v88internal24ExternalReferenceEncoder13NameOfAddressEPNS0_7IsolateEm@PLT
	movq	%rax, %rdx
.L240:
	leaq	.LC23(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal13StringBuilder12AddFormattedEPKcz@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L255:
	movq	-3712(%rbp), %rcx
	cmpq	%rcx, -3648(%rbp)
	jb	.L257
.L256:
	movq	-3648(%rbp), %rbx
	subq	-3672(%rbp), %rbx
	movl	%ebx, -3640(%rbp)
.L258:
	leaq	-3568(%rbp), %r14
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L259:
	movq	%r12, %rdi
	call	_ZNK2v88internal20CodeCommentsIterator11GetPCOffsetEv@PLT
	movl	%eax, %eax
	cmpq	%rbx, %rax
	jnb	.L262
	movq	%r12, %rdi
	call	_ZNK2v88internal20CodeCommentsIterator10GetCommentEv@PLT
	leaq	.LC14(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal13StringBuilder12AddFormattedEPKcz@PLT
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internalL10DumpBufferEPSoPNS0_13StringBuilderE
	movq	%r12, %rdi
	call	_ZN2v88internal20CodeCommentsIterator4NextEv@PLT
.L200:
	movq	%r12, %rdi
	call	_ZNK2v88internal20CodeCommentsIterator10HasCurrentEv@PLT
	testb	%al, %al
	jne	.L259
.L262:
	testq	%r13, %r13
	je	.L261
	movq	%r13, %rdi
	call	_ZN2v88internal8MalloceddlEPv@PLT
.L261:
	movl	-3552(%rbp), %eax
	testl	%eax, %eax
	js	.L194
	leaq	-3568(%rbp), %rdi
	call	_ZN2v88internal19SimpleStringBuilder8FinalizeEv@PLT
.L194:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L335
	movl	-3640(%rbp), %eax
	addq	$3688, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	cmpb	$0, 56(%r13)
	jne	.L208
	leaq	-3344(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L215:
	movq	16(%r13), %rax
	cmpq	-3648(%rbp), %rax
	jnb	.L208
	movq	%rax, -3344(%rbp)
	movq	-3464(%rbp), %rsi
	cmpq	-3456(%rbp), %rsi
	je	.L209
	movq	%rax, (%rsi)
	addq	$8, -3464(%rbp)
.L210:
	movzbl	24(%r13), %eax
	movq	-3432(%rbp), %rsi
	movb	%al, -3344(%rbp)
	cmpq	-3424(%rbp), %rsi
	je	.L211
	movb	%al, (%rsi)
	addq	$1, -3432(%rbp)
.L212:
	movq	32(%r13), %rax
	movq	-3400(%rbp), %rsi
	movq	%rax, -3344(%rbp)
	cmpq	-3392(%rbp), %rsi
	je	.L213
	movq	%rax, (%rsi)
	movq	%r13, %rdi
	addq	$8, -3400(%rbp)
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, 56(%r13)
	je	.L215
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L209:
	leaq	-3472(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L213:
	leaq	-3408(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIlSaIlEE17_M_realloc_insertIJlEEEvN9__gnu_cxx17__normal_iteratorIPlS1_EEDpOT_
	movq	%r13, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, 56(%r13)
	je	.L215
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L211:
	leaq	-3440(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIN2v88internal9RelocInfo4ModeESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L326:
	leaq	.LC24(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal13StringBuilder12AddFormattedEPKcz@PLT
	movq	-3344(%rbp), %rax
	movslq	(%rax), %rdx
	leaq	4(%rax,%rdx), %rsi
	movq	-3680(%rbp), %rax
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap29GcSafeFindCodeForInnerPointerEm@PLT
	movl	59(%rax), %edi
	cmpl	$-1, %edi
	je	.L242
	call	_ZN2v88internal8Builtins4nameEi@PLT
	leaq	.LC25(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal13StringBuilder12AddFormattedEPKcz@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L225:
	movl	-3632(%rbp), %edx
	testl	%edx, %edx
	je	.L249
	movq	-3656(%rbp), %rax
	pxor	%xmm0, %xmm0
	leaq	-3344(%rbp), %rdi
	movb	$19, -3336(%rbp)
	movq	$0, -3328(%rbp)
	movq	%rax, -3344(%rbp)
	movups	%xmm0, -3320(%rbp)
	call	_ZN2v88internal9RelocInfo16IsInConstantPoolEv@PLT
	testb	%al, %al
	je	.L249
	leaq	.LC30(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L333:
	movl	%eax, %r8d
	movq	-3656(%rbp), %rax
	movq	-3296(%rbp), %rdi
	leaq	.LC12(%rip), %rdx
	movq	-3288(%rbp), %rsi
	movl	(%rax), %ecx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-3656(%rbp), %rax
	movl	%r14d, -3684(%rbp)
	addq	$4, %rax
	movq	%rax, -3648(%rbp)
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L242:
	movl	43(%rax), %edi
	shrl	%edi
	andl	$31, %edi
	call	_ZN2v88internal4Code11Kind2StringENS1_4KindE@PLT
	leaq	.LC26(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal13StringBuilder12AddFormattedEPKcz@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L331:
	leaq	.LC31(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal13StringBuilder12AddFormattedEPKcz@PLT
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L319:
	leaq	.LC15(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal13StringBuilder12AddFormattedEPKcz@PLT
	jmp	.L222
.L327:
	movq	48(%r14), %r14
	leaq	-3344(%rbp), %rdi
	call	_ZNK2v88internal9RelocInfo22wasm_stub_call_addressEv@PLT
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal4wasm12NativeModule18GetRuntimeStubNameEm@PLT
	leaq	.LC27(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal13StringBuilder12AddFormattedEPKcz@PLT
	jmp	.L232
.L334:
	cmpb	$8, 24(%r13)
	jne	.L204
	movq	(%rax), %rcx
	movq	-3296(%rbp), %rdi
	movq	%rax, %r14
	xorl	%eax, %eax
	movq	-3288(%rbp), %rsi
	leaq	.LC13(%rip), %rdx
	movq	%rcx, %r8
	subq	-3672(%rbp), %r8
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	leaq	8(%r14), %rax
	movq	%rax, -3648(%rbp)
	jmp	.L202
.L316:
	xorl	%r13d, %r13d
	jmp	.L197
.L315:
	leaq	.LC9(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L317:
	movl	$0, -3640(%rbp)
	xorl	%ebx, %ebx
	jmp	.L258
.L335:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19938:
	.size	_ZN2v88internalL8DecodeItEPNS0_7IsolateEPNS0_24ExternalReferenceEncoderEPSoNS0_13CodeReferenceERKNS0_15V8NameConverterEPhSA_m, .-_ZN2v88internalL8DecodeItEPNS0_7IsolateEPNS0_24ExternalReferenceEncoderEPSoNS0_13CodeReferenceERKNS0_15V8NameConverterEPhSA_m
	.section	.text._ZN2v88internal12Disassembler6DecodeEPNS0_7IsolateEPSoPhS5_NS0_13CodeReferenceEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12Disassembler6DecodeEPNS0_7IsolateEPSoPhS5_NS0_13CodeReferenceEm
	.type	_ZN2v88internal12Disassembler6DecodeEPNS0_7IsolateEPSoPhS5_NS0_13CodeReferenceEm, @function
_ZN2v88internal12Disassembler6DecodeEPNS0_7IsolateEPSoPhS5_NS0_13CodeReferenceEm:
.LFB19970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-64(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	leaq	-432(%rbp), %r9
	subq	$440, %rsp
	movq	.LC33(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-408(%rbp), %rax
	movq	%rdi, -280(%rbp)
	movq	%rax, %xmm1
	leaq	-240(%rbp), %rax
	movq	%r13, -112(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%r15, -272(%rbp)
	movq	$128, -416(%rbp)
	movq	%rbx, -264(%rbp)
	movq	%rax, -256(%rbp)
	movq	$128, -248(%rbp)
	movq	$1, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movl	$0x3f800000, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -432(%rbp)
	testq	%rdi, %rdi
	je	.L337
	leaq	-440(%rbp), %r14
	movq	%rsi, -456(%rbp)
	movq	%rdi, %r12
	movq	%rdi, %rsi
	movq	%r14, %rdi
	movq	%r9, -480(%rbp)
	movq	%rcx, -472(%rbp)
	movq	%rdx, -464(%rbp)
	call	_ZN2v88internal24ExternalReferenceEncoderC1EPNS0_7IsolateE@PLT
	movq	-472(%rbp), %rcx
	subq	$8, %rsp
	movq	-464(%rbp), %rdx
	pushq	16(%rbp)
	movq	-456(%rbp), %r11
	movq	%r12, %rdi
	movq	%rbx, %r8
	movq	-480(%rbp), %r9
	pushq	%rcx
	movq	%r14, %rsi
	movl	%r15d, %ecx
	pushq	%rdx
	movq	%r11, %rdx
	call	_ZN2v88internalL8DecodeItEPNS0_7IsolateEPNS0_24ExternalReferenceEncoderEPSoNS0_13CodeReferenceERKNS0_15V8NameConverterEPhSA_m
	addq	$32, %rsp
	movq	%r14, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal24ExternalReferenceEncoderD1Ev@PLT
.L338:
	movq	-96(%rbp), %r12
	leaq	16+_ZTVN2v88internal15V8NameConverterE(%rip), %rax
	movq	%rax, -432(%rbp)
	testq	%r12, %r12
	je	.L339
	.p2align 4,,10
	.p2align 3
.L340:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L340
.L339:
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-112(%rbp), %rdi
	movq	$0, -88(%rbp)
	movq	$0, -96(%rbp)
	cmpq	%r13, %rdi
	je	.L336
	call	_ZdlPv@PLT
.L336:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L348
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	subq	$8, %rsp
	pushq	16(%rbp)
	xorl	%edi, %edi
	pushq	%rcx
	movl	%r8d, %ecx
	movq	%rbx, %r8
	pushq	%rdx
	movq	%rsi, %rdx
	xorl	%esi, %esi
	call	_ZN2v88internalL8DecodeItEPNS0_7IsolateEPNS0_24ExternalReferenceEncoderEPSoNS0_13CodeReferenceERKNS0_15V8NameConverterEPhSA_m
	addq	$32, %rsp
	movl	%eax, %r15d
	jmp	.L338
.L348:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19970:
	.size	_ZN2v88internal12Disassembler6DecodeEPNS0_7IsolateEPSoPhS5_NS0_13CodeReferenceEm, .-_ZN2v88internal12Disassembler6DecodeEPNS0_7IsolateEPSoPhS5_NS0_13CodeReferenceEm
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal15V8NameConverter21InitExternalRefsCacheEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal15V8NameConverter21InitExternalRefsCacheEv, @function
_GLOBAL__sub_I__ZNK2v88internal15V8NameConverter21InitExternalRefsCacheEv:
.LFB24679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24679:
	.size	_GLOBAL__sub_I__ZNK2v88internal15V8NameConverter21InitExternalRefsCacheEv, .-_GLOBAL__sub_I__ZNK2v88internal15V8NameConverter21InitExternalRefsCacheEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal15V8NameConverter21InitExternalRefsCacheEv
	.weak	_ZTVN2v88internal15V8NameConverterE
	.section	.data.rel.ro._ZTVN2v88internal15V8NameConverterE,"awG",@progbits,_ZTVN2v88internal15V8NameConverterE,comdat
	.align 8
	.type	_ZTVN2v88internal15V8NameConverterE, @object
	.size	_ZTVN2v88internal15V8NameConverterE, 88
_ZTVN2v88internal15V8NameConverterE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal15V8NameConverterD1Ev
	.quad	_ZN2v88internal15V8NameConverterD0Ev
	.quad	_ZNK6disasm13NameConverter17NameOfCPURegisterEi
	.quad	_ZNK6disasm13NameConverter21NameOfByteCPURegisterEi
	.quad	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi
	.quad	_ZNK2v88internal15V8NameConverter13NameOfAddressEPh
	.quad	_ZNK6disasm13NameConverter14NameOfConstantEPh
	.quad	_ZNK2v88internal15V8NameConverter10NameInCodeEPh
	.quad	_ZNK2v88internal15V8NameConverter16RootRelativeNameEi
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro.local,"aw"
	.align 8
.LC33:
	.quad	_ZTVN2v88internal15V8NameConverterE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
