	.file	"flush-instruction-cache.cc"
	.text
	.section	.text._ZN2v88internal21FlushInstructionCacheEPvm,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21FlushInstructionCacheEPvm
	.type	_ZN2v88internal21FlushInstructionCacheEPvm, @function
_ZN2v88internal21FlushInstructionCacheEPvm:
.LFB6144:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1
	cmpb	$0, _ZN2v88internal12FLAG_jitlessE(%rip)
	je	.L7
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	jmp	_ZN2v88internal11CpuFeatures11FlushICacheEPvm@PLT
	.cfi_endproc
.LFE6144:
	.size	_ZN2v88internal21FlushInstructionCacheEPvm, .-_ZN2v88internal21FlushInstructionCacheEPvm
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal21FlushInstructionCacheEPvm,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal21FlushInstructionCacheEPvm, @function
_GLOBAL__sub_I__ZN2v88internal21FlushInstructionCacheEPvm:
.LFB7005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE7005:
	.size	_GLOBAL__sub_I__ZN2v88internal21FlushInstructionCacheEPvm, .-_GLOBAL__sub_I__ZN2v88internal21FlushInstructionCacheEPvm
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal21FlushInstructionCacheEPvm
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
