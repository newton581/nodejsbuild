	.file	"v8-debugger-agent-impl.cc"
	.text
	.section	.text._ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB3256:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3256:
	.size	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN12v8_inspector17V8InspectorClient17canExecuteScriptsEi,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient17canExecuteScriptsEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient17canExecuteScriptsEi
	.type	_ZN12v8_inspector17V8InspectorClient17canExecuteScriptsEi, @function
_ZN12v8_inspector17V8InspectorClient17canExecuteScriptsEi:
.LFB4236:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4236:
	.size	_ZN12v8_inspector17V8InspectorClient17canExecuteScriptsEi, .-_ZN12v8_inspector17V8InspectorClient17canExecuteScriptsEi
	.section	.text._ZN12v8_inspector12_GLOBAL__N_118positionComparatorERKSt4pairIiiES4_,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_118positionComparatorERKSt4pairIiiES4_, @function
_ZN12v8_inspector12_GLOBAL__N_118positionComparatorERKSt4pairIiiES4_:
.LFB7758:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	cmpl	%eax, (%rdi)
	jne	.L7
	movl	4(%rsi), %eax
	cmpl	%eax, 4(%rdi)
.L7:
	setl	%al
	ret
	.cfi_endproc
.LFE7758:
	.size	_ZN12v8_inspector12_GLOBAL__N_118positionComparatorERKSt4pairIiiES4_, .-_ZN12v8_inspector12_GLOBAL__N_118positionComparatorERKSt4pairIiiES4_
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB11545:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE11545:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN12v8_inspector8protocol8Debugger8LocationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger8LocationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger8LocationD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger8LocationD2Ev, @function
_ZN12v8_inspector8protocol8Debugger8LocationD2Ev:
.LFB6552:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	addq	$24, %rdi
	movq	%rax, -24(%rdi)
	cmpq	%rdi, %r8
	je	.L9
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	ret
	.cfi_endproc
.LFE6552:
	.size	_ZN12v8_inspector8protocol8Debugger8LocationD2Ev, .-_ZN12v8_inspector8protocol8Debugger8LocationD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger8LocationD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger8LocationD1Ev,_ZN12v8_inspector8protocol8Debugger8LocationD2Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger13BreakLocationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger13BreakLocationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger13BreakLocationD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger13BreakLocationD2Ev, @function
_ZN12v8_inspector8protocol8Debugger13BreakLocationD2Ev:
.LFB6770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L11
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6770:
	.size	_ZN12v8_inspector8protocol8Debugger13BreakLocationD2Ev, .-_ZN12v8_inspector8protocol8Debugger13BreakLocationD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger13BreakLocationD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger13BreakLocationD1Ev,_ZN12v8_inspector8protocol8Debugger13BreakLocationD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev, @function
_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev:
.LFB6160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L15
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6160:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev, .-_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev,_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev:
.LFB5647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L20
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5647:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger8LocationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger8LocationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev, @function
_ZN12v8_inspector8protocol8Debugger8LocationD0Ev:
.LFB6554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L25
	call	_ZdlPv@PLT
.L25:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6554:
	.size	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev, .-_ZN12v8_inspector8protocol8Debugger8LocationD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger11SearchMatchD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev, @function
_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev:
.LFB6741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	40(%r12), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	24(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L28
	call	_ZdlPv@PLT
.L28:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6741:
	.size	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev, .-_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl16setSkipAllPausesEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl16setSkipAllPausesEb
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl16setSkipAllPausesEb, @function
_ZN12v8_inspector19V8DebuggerAgentImpl16setSkipAllPausesEb:
.LFB7914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	40(%rsi), %r15
	leaq	_ZN12v8_inspector18DebuggerAgentStateL13skipAllPausesE(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movzbl	%r12b, %edx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L31
	call	_ZdlPv@PLT
.L31:
	movb	%r12b, 408(%rbx)
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L34:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7914:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl16setSkipAllPausesEb, .-_ZN12v8_inspector19V8DebuggerAgentImpl16setSkipAllPausesEb
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl22setAsyncCallStackDepthEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl22setAsyncCallStackDepthEi
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl22setAsyncCallStackDepthEi, @function
_ZN12v8_inspector19V8DebuggerAgentImpl22setAsyncCallStackDepthEi:
.LFB8013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 32(%rsi)
	jne	.L36
	movq	24(%rsi), %rax
	movq	184(%rax), %rax
	cmpb	$0, 40(%rax)
	je	.L44
.L36:
	movq	40(%r12), %rbx
	leaq	-96(%rbp), %r15
	leaq	_ZN12v8_inspector18DebuggerAgentStateL19asyncCallStackDepthE(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movl	%r14d, %edx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setIntegerERKNS_8String16Ei@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L40
	call	_ZdlPv@PLT
.L40:
	movq	16(%r12), %rdi
	movl	%r14d, %edx
	movq	%r12, %rsi
	call	_ZN12v8_inspector10V8Debugger22setAsyncCallStackDepthEPNS_19V8DebuggerAgentImplEi@PLT
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L35:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	leaq	-96(%rbp), %r12
	leaq	_ZN12v8_inspectorL19kDebuggerNotEnabledE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L35
	call	_ZdlPv@PLT
	jmp	.L35
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8013:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl22setAsyncCallStackDepthEi, .-_ZN12v8_inspector19V8DebuggerAgentImpl22setAsyncCallStackDepthEi
	.section	.text._ZN12v8_inspector12_GLOBAL__N_135generateInstrumentationBreakpointIdERKNS_8String16E,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_135generateInstrumentationBreakpointIdERKNS_8String16E, @function
_ZN12v8_inspector12_GLOBAL__N_135generateInstrumentationBreakpointIdERKNS_8String16E:
.LFB7752:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder12appendNumberEi@PLT
	movl	$58, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L46
	call	_ZdlPv@PLT
.L46:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L53:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7752:
	.size	_ZN12v8_inspector12_GLOBAL__N_135generateInstrumentationBreakpointIdERKNS_8String16E, .-_ZN12v8_inspector12_GLOBAL__N_135generateInstrumentationBreakpointIdERKNS_8String16E
	.section	.text._ZN12v8_inspector12_GLOBAL__N_124adjustBreakpointLocationERKNS_16V8DebuggerScriptERKNS_8String16EPiS7_,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_124adjustBreakpointLocationERKNS_16V8DebuggerScriptERKNS_8String16EPiS7_, @function
_ZN12v8_inspector12_GLOBAL__N_124adjustBreakpointLocationERKNS_16V8DebuggerScriptERKNS_8String16EPiS7_:
.LFB7760:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movl	(%rdx), %r15d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*40(%rax)
	cmpl	%eax, %r15d
	jl	.L54
	movq	(%r12), %rax
	movl	(%rbx), %r15d
	movq	%r12, %rdi
	call	*56(%rax)
	cmpl	%eax, %r15d
	jg	.L54
	cmpq	$0, 8(%r14)
	je	.L54
	movq	(%r12), %rax
	movl	0(%r13), %edx
	movq	%r12, %rdi
	movl	(%rbx), %esi
	call	*136(%rax)
	movslq	%eax, %r15
	cmpl	$-1, %eax
	je	.L54
	movq	%r15, %rdi
	movl	$0, %eax
	movq	%r12, %rsi
	subq	$800, %rdi
	cmovns	%rdi, %rax
	leaq	-96(%rbp), %rdi
	movq	%rax, -120(%rbp)
	movq	%rax, %rdx
	subq	%rax, %r15
	movq	(%r12), %rax
	leaq	800(%r15), %rcx
	call	*24(%rax)
	movq	8(%r14), %rcx
	movq	(%r14), %r8
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %r10
	testq	%rcx, %rcx
	je	.L106
	cmpq	%r9, %r15
	jnb	.L60
	movq	%r9, %rdx
	movzwl	(%r8), %r11d
	leaq	(%r10,%r15,2), %rax
	leaq	(%r10,%r9,2), %r14
	subq	%r15, %rdx
	cmpq	%rdx, %rcx
	ja	.L61
	movl	$1, %esi
	subq	%rcx, %rsi
	.p2align 4,,10
	.p2align 3
.L76:
	addq	%rsi, %rdx
	je	.L61
	xorl	%edi, %edi
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L107:
	addq	$1, %rdi
	addq	$2, %rax
	cmpq	%rdi, %rdx
	je	.L61
.L63:
	cmpw	(%rax), %r11w
	jne	.L107
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L62:
	addq	$1, %rdx
	cmpq	%rdx, %rcx
	je	.L108
	movzwl	(%r8,%rdx,2), %edi
	cmpw	%di, (%rax,%rdx,2)
	je	.L62
	addq	$2, %rax
	movq	%r14, %rdx
	subq	%rax, %rdx
	sarq	%rdx
	cmpq	%rdx, %rcx
	jbe	.L76
	.p2align 4,,10
	.p2align 3
.L61:
	movq	$-1, %rax
	cmpq	%r9, %rcx
	jbe	.L59
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	-80(%rbp), %rax
	cmpq	%rax, %r10
	je	.L54
	movq	%r10, %rdi
.L105:
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L54:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L109
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	cmpq	%r9, %r15
	movq	$-1, %rax
	cmovbe	%r15, %rax
.L59:
	movq	%r9, %rsi
	subq	%rcx, %rsi
	cmpq	%r15, %rsi
	cmova	%r15, %rsi
	leaq	(%rsi,%rsi), %rdi
	.p2align 4,,10
	.p2align 3
.L74:
	testq	%rcx, %rcx
	je	.L68
	xorl	%edx, %edx
	leaq	(%r10,%rdi), %r9
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L110:
	addq	$1, %rdx
	cmpq	%rdx, %rcx
	je	.L68
.L70:
	movzwl	(%r8,%rdx,2), %r11d
	cmpw	%r11w, (%r9,%rdx,2)
	je	.L110
	leaq	-1(%rsi), %rdx
	subq	$2, %rdi
	testq	%rsi, %rsi
	je	.L111
	movq	%rdx, %rsi
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%rax, %rdx
	andq	%rsi, %rdx
.L71:
	cmpq	$-1, %rdx
	je	.L67
	cmpq	$-1, %rax
	je	.L72
	cmpq	$-1, %rsi
	je	.L80
	movq	%rax, %rdx
	subq	%r15, %rdx
	subq	%rsi, %r15
	cmpq	%r15, %rdx
	cmovb	%rax, %rsi
.L72:
	movq	(%r12), %rax
	movq	%r12, %rdi
	leaq	-108(%rbp), %r12
	addq	-120(%rbp), %rsi
	call	*144(%rax)
	movq	%r12, %rdi
	movq	%rax, -108(%rbp)
	movl	%edx, -100(%rbp)
	call	_ZNK2v85debug8Location7IsEmptyEv@PLT
	testb	%al, %al
	je	.L73
	movq	-96(%rbp), %r10
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%r12, %rdi
	call	_ZNK2v85debug8Location13GetLineNumberEv@PLT
	movq	%r12, %rdi
	movl	%eax, (%rbx)
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	movq	-96(%rbp), %rdi
	movl	%eax, 0(%r13)
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L105
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L111:
	movq	%rax, %rdx
	movq	$-1, %rsi
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L108:
	subq	%r10, %rax
	sarq	%rax
	movq	%rax, %rsi
	cmpq	%r9, %rcx
	jbe	.L59
	cmpq	$-1, %rsi
	jne	.L72
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L60:
	cmpq	%r9, %rcx
	ja	.L67
	movq	$-1, %rax
	jmp	.L59
.L80:
	movq	%rax, %rsi
	jmp	.L72
.L109:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7760:
	.size	_ZN12v8_inspector12_GLOBAL__N_124adjustBreakpointLocationERKNS_16V8DebuggerScriptERKNS_8String16EPiS7_, .-_ZN12v8_inspector12_GLOBAL__N_124adjustBreakpointLocationERKNS_16V8DebuggerScriptERKNS_8String16EPiS7_
	.section	.text._ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv:
.LFB6563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L112
	movq	(%rdi), %rax
	call	*24(%rax)
.L112:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L119
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L119:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6563:
	.size	_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv:
.LFB6562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L120
	movq	(%rdi), %rax
	call	*24(%rax)
.L120:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L127
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L127:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6562:
	.size	_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol8Debugger13BreakLocation17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger13BreakLocation17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger13BreakLocation17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Debugger13BreakLocation17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Debugger13BreakLocation17serializeToBinaryEv:
.LFB6784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger13BreakLocation7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L128
	movq	(%rdi), %rax
	call	*24(%rax)
.L128:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L135:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6784:
	.size	_ZN12v8_inspector8protocol8Debugger13BreakLocation17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Debugger13BreakLocation17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Debugger13BreakLocation15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger13BreakLocation15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger13BreakLocation15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Debugger13BreakLocation15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Debugger13BreakLocation15serializeToJSONEv:
.LFB6783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger13BreakLocation7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L136
	movq	(%rdi), %rax
	call	*24(%rax)
.L136:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L143
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L143:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6783:
	.size	_ZN12v8_inspector8protocol8Debugger13BreakLocation15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Debugger13BreakLocation15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv:
.LFB6174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L144
	movq	(%rdi), %rax
	call	*24(%rax)
.L144:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L151
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L151:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6174:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv:
.LFB6173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L152
	movq	(%rdi), %rax
	call	*24(%rax)
.L152:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L159
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L159:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6173:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv:
.LFB5656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13CustomPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L160
	movq	(%rdi), %rax
	call	*24(%rax)
.L160:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L167
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L167:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5656:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv:
.LFB5655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13CustomPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L168
	movq	(%rdi), %rax
	call	*24(%rax)
.L168:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L175
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L175:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5655:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv:
.LFB5702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13ObjectPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L176
	movq	(%rdi), %rax
	call	*24(%rax)
.L176:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L183
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L183:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5702:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv:
.LFB5701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13ObjectPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L184
	movq	(%rdi), %rax
	call	*24(%rax)
.L184:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L191
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L191:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5701:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv:
.LFB5803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12EntryPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L192
	movq	(%rdi), %rax
	call	*24(%rax)
.L192:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L199
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L199:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5803:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv:
.LFB5802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12EntryPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L200
	movq	(%rdi), %rax
	call	*24(%rax)
.L200:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L207
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L207:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5802:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv:
.LFB5771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime15PropertyPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L208
	movq	(%rdi), %rax
	call	*24(%rax)
.L208:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L215
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L215:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5771:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv:
.LFB5770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime15PropertyPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L216
	movq	(%rdi), %rax
	call	*24(%rax)
.L216:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L223
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L223:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5770:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv:
.LFB6105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime16ExceptionDetails7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L224
	movq	(%rdi), %rax
	call	*24(%rax)
.L224:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L231
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L231:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6105:
	.size	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv:
.LFB6104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime16ExceptionDetails7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L232
	movq	(%rdi), %rax
	call	*24(%rax)
.L232:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L239
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L239:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6104:
	.size	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol8Debugger9CallFrame17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger9CallFrame17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger9CallFrame17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Debugger9CallFrame17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Debugger9CallFrame17serializeToBinaryEv:
.LFB6641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L240
	movq	(%rdi), %rax
	call	*24(%rax)
.L240:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L247
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L247:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6641:
	.size	_ZN12v8_inspector8protocol8Debugger9CallFrame17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Debugger9CallFrame17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Debugger9CallFrame15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger9CallFrame15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger9CallFrame15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Debugger9CallFrame15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Debugger9CallFrame15serializeToJSONEv:
.LFB6640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L248
	movq	(%rdi), %rax
	call	*24(%rax)
.L248:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L255
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L255:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6640:
	.size	_ZN12v8_inspector8protocol8Debugger9CallFrame15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Debugger9CallFrame15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol8Debugger5Scope17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger5Scope17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger5Scope17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Debugger5Scope17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Debugger5Scope17serializeToBinaryEv:
.LFB6712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger5Scope7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L256
	movq	(%rdi), %rax
	call	*24(%rax)
.L256:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L263
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L263:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6712:
	.size	_ZN12v8_inspector8protocol8Debugger5Scope17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Debugger5Scope17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Debugger5Scope15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger5Scope15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger5Scope15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Debugger5Scope15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Debugger5Scope15serializeToJSONEv:
.LFB6711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger5Scope7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L264
	movq	(%rdi), %rax
	call	*24(%rax)
.L264:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L271
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L271:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6711:
	.size	_ZN12v8_inspector8protocol8Debugger5Scope15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Debugger5Scope15serializeToJSONEv
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl6resumeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl6resumeEv
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl6resumeEv, @function
_ZN12v8_inspector19V8DebuggerAgentImpl6resumeEv:
.LFB8001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	16(%rbx), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	movl	16(%rax), %esi
	call	_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi@PLT
	testb	%al, %al
	jne	.L273
	leaq	-80(%rbp), %r13
	leaq	_ZN12v8_inspectorL18kDebuggerNotPausedE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L272
	call	_ZdlPv@PLT
.L272:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L279
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	movq	24(%rbx), %r14
	leaq	-80(%rbp), %r13
	leaq	_ZN12v8_inspectorL21kBacktraceObjectGroupE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_8String16E@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L276
	call	_ZdlPv@PLT
.L276:
	movq	24(%rbx), %rax
	movq	16(%rbx), %rdi
	movl	16(%rax), %esi
	call	_ZN12v8_inspector10V8Debugger15continueProgramEi@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L272
.L279:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8001:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl6resumeEv, .-_ZN12v8_inspector19V8DebuggerAgentImpl6resumeEv
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl8stepOverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl8stepOverEv
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl8stepOverEv, @function
_ZN12v8_inspector19V8DebuggerAgentImpl8stepOverEv:
.LFB8002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	16(%rbx), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	movl	16(%rax), %esi
	call	_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi@PLT
	testb	%al, %al
	jne	.L281
	leaq	-80(%rbp), %r13
	leaq	_ZN12v8_inspectorL18kDebuggerNotPausedE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L280
	call	_ZdlPv@PLT
.L280:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L287
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	movq	24(%rbx), %r14
	leaq	-80(%rbp), %r13
	leaq	_ZN12v8_inspectorL21kBacktraceObjectGroupE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_8String16E@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L284
	call	_ZdlPv@PLT
.L284:
	movq	24(%rbx), %rax
	movq	16(%rbx), %rdi
	movl	16(%rax), %esi
	call	_ZN12v8_inspector10V8Debugger17stepOverStatementEi@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L280
.L287:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8002:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl8stepOverEv, .-_ZN12v8_inspector19V8DebuggerAgentImpl8stepOverEv
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl8stepIntoEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl8stepIntoEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl8stepIntoEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl8stepIntoEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE:
.LFB8003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	16(%rbx), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	movl	16(%rax), %esi
	call	_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi@PLT
	testb	%al, %al
	jne	.L289
	leaq	-96(%rbp), %r13
	leaq	_ZN12v8_inspectorL18kDebuggerNotPausedE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L288
	call	_ZdlPv@PLT
.L288:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L297
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	movq	24(%rbx), %r15
	leaq	-96(%rbp), %r14
	leaq	_ZN12v8_inspectorL21kBacktraceObjectGroupE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L292
	call	_ZdlPv@PLT
.L292:
	xorl	%edx, %edx
	cmpb	$0, 0(%r13)
	movq	16(%rbx), %rdi
	je	.L293
	movzbl	1(%r13), %edx
.L293:
	movq	24(%rbx), %rax
	movl	16(%rax), %esi
	call	_ZN12v8_inspector10V8Debugger17stepIntoStatementEib@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L288
.L297:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8003:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl8stepIntoEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE, .-_ZN12v8_inspector19V8DebuggerAgentImpl8stepIntoEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl7stepOutEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl7stepOutEv
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl7stepOutEv, @function
_ZN12v8_inspector19V8DebuggerAgentImpl7stepOutEv:
.LFB8004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	16(%rbx), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	movl	16(%rax), %esi
	call	_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi@PLT
	testb	%al, %al
	jne	.L299
	leaq	-80(%rbp), %r13
	leaq	_ZN12v8_inspectorL18kDebuggerNotPausedE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L298
	call	_ZdlPv@PLT
.L298:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L305
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	movq	24(%rbx), %r14
	leaq	-80(%rbp), %r13
	leaq	_ZN12v8_inspectorL21kBacktraceObjectGroupE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_8String16E@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L302
	call	_ZdlPv@PLT
.L302:
	movq	24(%rbx), %rax
	movq	16(%rbx), %rdi
	movl	16(%rax), %esi
	call	_ZN12v8_inspector10V8Debugger17stepOutOfFunctionEi@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L298
.L305:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8004:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl7stepOutEv, .-_ZN12v8_inspector19V8DebuggerAgentImpl7stepOutEv
	.section	.text._ZN12v8_inspector12_GLOBAL__N_117getOrCreateObjectEPNS_8protocol15DictionaryValueERKNS_8String16E,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_117getOrCreateObjectEPNS_8protocol15DictionaryValueERKNS_8String16E, @function
_ZN12v8_inspector12_GLOBAL__N_117getOrCreateObjectEPNS_8protocol15DictionaryValueERKNS_8String16E:
.LFB7765:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L314
.L306:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L315
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r13, %rdi
	leaq	-48(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, -48(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValue9setObjectERKNS_8String16ESt10unique_ptrIS1_St14default_deleteIS1_EE@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L306
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L306
.L315:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7765:
	.size	_ZN12v8_inspector12_GLOBAL__N_117getOrCreateObjectEPNS_8protocol15DictionaryValueERKNS_8String16E, .-_ZN12v8_inspector12_GLOBAL__N_117getOrCreateObjectEPNS_8protocol15DictionaryValueERKNS_8String16E
	.section	.text._ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger13BreakLocationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev, @function
_ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev:
.LFB6772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L317
	call	_ZdlPv@PLT
.L317:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L318
	call	_ZdlPv@PLT
.L318:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$112, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6772:
	.size	_ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev, .-_ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev:
.LFB5649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L321
	call	_ZdlPv@PLT
.L321:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L322
	call	_ZdlPv@PLT
.L322:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5649:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev, @function
_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev:
.LFB6162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L325
	call	_ZdlPv@PLT
.L325:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L326
	call	_ZdlPv@PLT
.L326:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L327
	call	_ZdlPv@PLT
.L327:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6162:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev, .-_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev:
.LFB6280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	80(%r12), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	64(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L330
	call	_ZdlPv@PLT
.L330:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L331
	call	_ZdlPv@PLT
.L331:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6280:
	.size	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, .-_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev:
.LFB5681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	160(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L334
	movq	8(%r13), %rax
	movq	0(%r13), %r12
	movq	%rax, -64(%rbp)
	cmpq	%r12, %rax
	je	.L335
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r14
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L390:
	movq	16(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L338
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L339
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L338:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L340
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L341
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L340:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L336:
	addq	$8, %r12
	cmpq	%r12, -64(%rbp)
	je	.L389
.L342:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L336
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L390
	movq	%r15, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -64(%rbp)
	jne	.L342
	.p2align 4,,10
	.p2align 3
.L389:
	movq	0(%r13), %r12
.L335:
	testq	%r12, %r12
	je	.L343
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L343:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L334:
	movq	152(%rbx), %r13
	testq	%r13, %r13
	je	.L344
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	jne	.L354
	testq	%r12, %r12
	je	.L355
	.p2align 4,,10
	.p2align 3
.L393:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L355:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L344:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L356
	call	_ZdlPv@PLT
.L356:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L357
	call	_ZdlPv@PLT
.L357:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L333
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L348
	call	_ZdlPv@PLT
.L348:
	movq	136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L349
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L350
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L349:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L351
	call	_ZdlPv@PLT
.L351:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L352
	call	_ZdlPv@PLT
.L352:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L353
	call	_ZdlPv@PLT
.L353:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L346:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L391
.L354:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L346
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L392
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r12, %r14
	jne	.L354
	.p2align 4,,10
	.p2align 3
.L391:
	movq	0(%r13), %r12
	testq	%r12, %r12
	jne	.L393
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L333:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L341:
	.cfi_restore_state
	call	*%rax
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L339:
	call	*%rax
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L350:
	call	*%rax
	jmp	.L349
	.cfi_endproc
.LFE5681:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev:
.LFB5683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5683:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev:
.LFB5794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L397
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L398
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L397:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L396
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L400
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	popq	%rbx
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	.cfi_restore_state
	call	*%rax
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L400:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE5794:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev:
.LFB5754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	152(%rdi), %rdi
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L406
	call	_ZdlPv@PLT
.L406:
	movq	136(%rbx), %r12
	testq	%r12, %r12
	je	.L407
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L408
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L407:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L409
	call	_ZdlPv@PLT
.L409:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L410
	call	_ZdlPv@PLT
.L410:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L405
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L405:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	call	*%rax
	jmp	.L407
	.cfi_endproc
.LFE5754:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev:
.LFB5796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	16(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L417
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L418
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L417:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L419
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L420
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L419:
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L418:
	.cfi_restore_state
	call	*%rax
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L420:
	call	*%rax
	jmp	.L419
	.cfi_endproc
.LFE5796:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev:
.LFB5756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	movq	152(%rdi), %rdi
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L429
	call	_ZdlPv@PLT
.L429:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L430
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L431
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L430:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L432
	call	_ZdlPv@PLT
.L432:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L433
	call	_ZdlPv@PLT
.L433:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L434
	call	_ZdlPv@PLT
.L434:
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L431:
	.cfi_restore_state
	call	*%rax
	jmp	.L430
	.cfi_endproc
.LFE5756:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0.str1.1,"aMS",@progbits,1
.LC1:
	.string	"basic_string::_M_create"
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0:
.LFB16110:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	jne	.L440
	testq	%rdx, %rdx
	jne	.L456
.L440:
	subq	%r13, %rbx
	movq	%rbx, %r14
	sarq	%r14
	cmpq	$15, %rbx
	ja	.L441
	movq	(%r12), %rdi
.L442:
	cmpq	$2, %rbx
	je	.L457
	testq	%rbx, %rbx
	je	.L445
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%r12), %rdi
.L445:
	xorl	%eax, %eax
	movq	%r14, 8(%r12)
	movw	%ax, (%rdi,%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L457:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L441:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L458
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r14, 16(%r12)
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L442
.L456:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L458:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE16110:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl13getStackTraceESt10unique_ptrINS_8protocol7Runtime12StackTraceIdESt14default_deleteIS4_EEPS1_INS3_10StackTraceES5_IS8_EE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Invalid stack trace id"
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl13getStackTraceESt10unique_ptrINS_8protocol7Runtime12StackTraceIdESt14default_deleteIS4_EEPS1_INS3_10StackTraceES5_IS8_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"Stack trace with given id is not found"
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl13getStackTraceESt10unique_ptrINS_8protocol7Runtime12StackTraceIdESt14default_deleteIS4_EEPS1_INS3_10StackTraceES5_IS8_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl13getStackTraceESt10unique_ptrINS_8protocol7Runtime12StackTraceIdESt14default_deleteIS4_EEPS1_INS3_10StackTraceES5_IS8_EE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl13getStackTraceESt10unique_ptrINS_8protocol7Runtime12StackTraceIdESt14default_deleteIS4_EEPS1_INS3_10StackTraceES5_IS8_EE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl13getStackTraceESt10unique_ptrINS_8protocol7Runtime12StackTraceIdESt14default_deleteIS4_EEPS1_INS3_10StackTraceES5_IS8_EE:
.LFB7941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	-80(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movq	%r12, %rdi
	subq	$216, %rsp
	movq	%rcx, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movb	$0, -201(%rbp)
	movq	%r15, -96(%rbp)
	movq	16(%rax), %rsi
	movq	24(%rax), %rdx
	movq	%rax, -216(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-216(%rbp), %rax
	movq	%r12, %rdi
	leaq	-201(%rbp), %rsi
	movq	48(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK12v8_inspector8String1611toInteger64EPb@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -216(%rbp)
	cmpq	%r15, %rdi
	je	.L460
	call	_ZdlPv@PLT
.L460:
	movq	(%rbx), %rax
	movq	16(%r14), %r8
	cmpb	$0, 56(%rax)
	je	.L461
	xorl	%edx, %edx
	movq	64(%rax), %rsi
	movq	%r12, %rdi
	leaq	-128(%rbp), %rbx
	movw	%dx, -128(%rbp)
	movq	72(%rax), %rdx
	movq	%r8, -248(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%r15, -96(%rbp)
	movq	%rax, -240(%rbp)
	movq	%rbx, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -112(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-240(%rbp), %rax
	movq	-248(%rbp), %r8
	movq	%r12, %rsi
	movq	96(%rax), %rax
	movq	%r8, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN12v8_inspector10V8Debugger13debuggerIdForERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L462
	movq	%rax, -240(%rbp)
	movq	%rdx, -232(%rbp)
	call	_ZdlPv@PLT
	movq	-240(%rbp), %rax
	movq	-232(%rbp), %rdx
.L462:
	movq	-144(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L464
	movq	%rax, -240(%rbp)
	movq	%rdx, -232(%rbp)
	call	_ZdlPv@PLT
	movq	-240(%rbp), %rax
	movq	-232(%rbp), %rdx
.L464:
	movq	-216(%rbp), %rsi
	leaq	-176(%rbp), %rbx
	movq	%rdx, %rcx
	movq	%rax, %rdx
	movq	%rbx, %rdi
	call	_ZN12v8_inspector14V8StackTraceIdC1EmSt4pairIllE@PLT
	cmpb	$0, -201(%rbp)
	je	.L468
	movq	%rbx, %rdi
	call	_ZNK12v8_inspector14V8StackTraceId9IsInvalidEv@PLT
	testb	%al, %al
	je	.L491
.L468:
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L471
	call	_ZdlPv@PLT
.L471:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L492
	addq	$216, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movq	24(%r14), %rax
	movq	%r8, %rdi
	movl	16(%rax), %esi
	call	_ZN12v8_inspector10V8Debugger13debuggerIdForEi@PLT
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L491:
	movq	24(%r14), %rax
	movq	16(%r14), %rsi
	leaq	-192(%rbp), %rdi
	movq	%rbx, %rcx
	movl	16(%rax), %edx
	call	_ZN12v8_inspector10V8Debugger13stackTraceForEiRKNS_14V8StackTraceIdE@PLT
	movq	-192(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L493
	movq	16(%r14), %rdx
	leaq	-200(%rbp), %rdi
	movl	236(%rdx), %ecx
	call	_ZNK12v8_inspector15AsyncStackTrace20buildInspectorObjectEPNS_10V8DebuggerEi@PLT
	movq	-224(%rbp), %rcx
	movq	-200(%rbp), %rax
	movq	$0, -200(%rbp)
	movq	(%rcx), %rdi
	movq	%rax, (%rcx)
	testq	%rdi, %rdi
	je	.L475
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L475
	movq	(%rdi), %rax
	call	*24(%rax)
.L475:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L473:
	movq	-184(%rbp), %r12
	testq	%r12, %r12
	je	.L471
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L478
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L479:
	cmpl	$1, %eax
	jne	.L471
	movq	(%r12), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L494
.L481:
	testq	%rbx, %rbx
	je	.L482
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L483:
	cmpl	$1, %eax
	jne	.L471
	movq	(%r12), %rax
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L484
	call	*8(%rax)
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L478:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L493:
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L473
	call	_ZdlPv@PLT
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L482:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L483
.L494:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L481
.L484:
	call	*%rdx
	jmp	.L471
.L492:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7941:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl13getStackTraceESt10unique_ptrINS_8protocol7Runtime12StackTraceIdESt14default_deleteIS4_EEPS1_INS3_10StackTraceES5_IS8_EE, .-_ZN12v8_inspector19V8DebuggerAgentImpl13getStackTraceESt10unique_ptrINS_8protocol7Runtime12StackTraceIdESt14default_deleteIS4_EEPS1_INS3_10StackTraceES5_IS8_EE
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl16pauseOnAsyncCallESt10unique_ptrINS_8protocol7Runtime12StackTraceIdESt14default_deleteIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl16pauseOnAsyncCallESt10unique_ptrINS_8protocol7Runtime12StackTraceIdESt14default_deleteIS4_EE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl16pauseOnAsyncCallESt10unique_ptrINS_8protocol7Runtime12StackTraceIdESt14default_deleteIS4_EE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl16pauseOnAsyncCallESt10unique_ptrINS_8protocol7Runtime12StackTraceIdESt14default_deleteIS4_EE:
.LFB8005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$152, %rsp
	movq	%rsi, -168(%rbp)
	movq	(%rdx), %r15
	movq	16(%r15), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%r15), %rax
	movb	$0, -145(%rbp)
	movq	%rbx, -96(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	48(%r15), %rax
	movq	%r14, %rdi
	leaq	-145(%rbp), %rsi
	movq	%rax, -64(%rbp)
	call	_ZNK12v8_inspector8String1611toInteger64EPb@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%rbx, %rdi
	je	.L496
	call	_ZdlPv@PLT
.L496:
	cmpb	$0, -145(%rbp)
	jne	.L497
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L495
	call	_ZdlPv@PLT
.L495:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L506
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_restore_state
	movq	0(%r13), %rdx
	xorl	%ecx, %ecx
	movq	-168(%rbp), %rax
	leaq	-128(%rbp), %r13
	movq	%r13, -144(%rbp)
	cmpb	$0, 56(%rdx)
	movq	16(%rax), %r8
	movw	%cx, -128(%rbp)
	leaq	-144(%rbp), %rax
	movq	$0, -136(%rbp)
	movq	$0, -112(%rbp)
	jne	.L507
	movq	%r13, %rdx
	movq	%r13, %rsi
.L500:
	movq	%r14, %rdi
	movq	%rax, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%rbx, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %rax
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	-176(%rbp), %r8
	movq	32(%rax), %rax
	movq	%r8, %rdi
	movq	%rax, -64(%rbp)
	movq	-168(%rbp), %rax
	movq	24(%rax), %rax
	movl	16(%rax), %esi
	call	_ZN12v8_inspector10V8Debugger16pauseOnAsyncCallEimRKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L501
	call	_ZdlPv@PLT
.L501:
	movq	-144(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L502
	call	_ZdlPv@PLT
.L502:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L507:
	movq	64(%rdx), %rsi
	leaq	64(%rdx), %rax
	movq	72(%rdx), %rdx
	leaq	(%rsi,%rdx,2), %rdx
	jmp	.L500
.L506:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8005:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl16pauseOnAsyncCallESt10unique_ptrINS_8protocol7Runtime12StackTraceIdESt14default_deleteIS4_EE, .-_ZN12v8_inspector19V8DebuggerAgentImpl16pauseOnAsyncCallESt10unique_ptrINS_8protocol7Runtime12StackTraceIdESt14default_deleteIS4_EE
	.section	.rodata._ZNK12v8_inspector8String169substringEmm.str1.1,"aMS",@progbits,1
.LC4:
	.string	"basic_string::substr"
	.section	.rodata._ZNK12v8_inspector8String169substringEmm.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"%s: __pos (which is %zu) > this->size() (which is %zu)"
	.section	.text._ZNK12v8_inspector8String169substringEmm,"axG",@progbits,_ZNK12v8_inspector8String169substringEmm,comdat
	.align 2
	.p2align 4
	.weak	_ZNK12v8_inspector8String169substringEmm
	.type	_ZNK12v8_inspector8String169substringEmm, @function
_ZNK12v8_inspector8String169substringEmm:
.LFB5099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r8, %rdx
	ja	.L528
	subq	%rdx, %r8
	movq	(%rsi), %rax
	movq	%rcx, %rbx
	leaq	-80(%rbp), %r14
	cmpq	%rcx, %r8
	movq	%r14, -96(%rbp)
	movq	%rdi, %r13
	cmovbe	%r8, %rbx
	leaq	(%rax,%rdx,2), %r15
	movq	%r15, %rax
	leaq	(%rbx,%rbx), %r12
	addq	%r12, %rax
	je	.L510
	testq	%r15, %r15
	je	.L529
.L510:
	movq	%r12, %rcx
	movq	%r14, %rdi
	sarq	%rcx
	cmpq	$14, %r12
	ja	.L530
.L511:
	cmpq	$2, %r12
	je	.L531
	testq	%r12, %r12
	je	.L514
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rcx, -104(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %rdi
	movq	-104(%rbp), %rcx
.L514:
	xorl	%eax, %eax
	movq	%rcx, -88(%rbp)
	leaq	-96(%rbp), %rsi
	movw	%ax, (%rdi,%rbx,2)
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L508
	call	_ZdlPv@PLT
.L508:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L532
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L531:
	.cfi_restore_state
	movzwl	(%r15), %eax
	movw	%ax, (%rdi)
	movq	-96(%rbp), %rdi
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L530:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %rcx
	ja	.L533
	leaq	2(%r12), %rdi
	movq	%rcx, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rcx
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	%rcx, -80(%rbp)
	jmp	.L511
.L529:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L528:
	movq	%r8, %rcx
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L532:
	call	__stack_chk_fail@PLT
.L533:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5099:
	.size	_ZNK12v8_inspector8String169substringEmm, .-_ZNK12v8_inspector8String169substringEmm
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev:
.LFB5498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	312(%rdi), %r12
	movq	%rdi, %rbx
	movups	%xmm0, (%rdi)
	testq	%r12, %r12
	je	.L535
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L536
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L537
	call	_ZdlPv@PLT
.L537:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L538
	call	_ZdlPv@PLT
.L538:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L535:
	movq	304(%rbx), %r12
	testq	%r12, %r12
	je	.L539
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L540
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L539:
	movq	264(%rbx), %rdi
	leaq	280(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L541
	call	_ZdlPv@PLT
.L541:
	movq	216(%rbx), %rdi
	leaq	232(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L542
	call	_ZdlPv@PLT
.L542:
	movq	168(%rbx), %rdi
	leaq	184(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L543
	call	_ZdlPv@PLT
.L543:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L544
	movq	(%rdi), %rax
	call	*24(%rax)
.L544:
	movq	112(%rbx), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L545
	call	_ZdlPv@PLT
.L545:
	movq	64(%rbx), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L546
	call	_ZdlPv@PLT
.L546:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L534
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L534:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L536:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L540:
	call	*%rax
	jmp	.L539
	.cfi_endproc
.LFE5498:
	.size	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev, .-_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.set	.LTHUNK0,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev:
.LFB16134:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK0
	.cfi_endproc
.LFE16134:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev:
.LFB5500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$320, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5500:
	.size	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, .-_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger5ScopeD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger5ScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger5ScopeD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger5ScopeD2Ev, @function
_ZN12v8_inspector8protocol8Debugger5ScopeD2Ev:
.LFB6695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	112(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L561
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L562
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L563
	call	_ZdlPv@PLT
.L563:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L561:
	movq	104(%rbx), %r12
	testq	%r12, %r12
	je	.L564
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L565
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L566
	call	_ZdlPv@PLT
.L566:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L564:
	movq	64(%rbx), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L567
	call	_ZdlPv@PLT
.L567:
	movq	48(%rbx), %r12
	testq	%r12, %r12
	je	.L568
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L569
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L568:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L560
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L560:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L569:
	.cfi_restore_state
	call	*%rax
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L562:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L565:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L564
	.cfi_endproc
.LFE6695:
	.size	_ZN12v8_inspector8protocol8Debugger5ScopeD2Ev, .-_ZN12v8_inspector8protocol8Debugger5ScopeD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger5ScopeD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger5ScopeD1Ev,_ZN12v8_inspector8protocol8Debugger5ScopeD2Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger9CallFrameD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger9CallFrameD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger9CallFrameD2Ev, @function
_ZN12v8_inspector8protocol8Debugger9CallFrameD2Ev:
.LFB6617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	160(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L582
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L583
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L582:
	movq	152(%rbx), %r12
	testq	%r12, %r12
	je	.L584
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L585
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L584:
	movq	144(%rbx), %r14
	testq	%r14, %r14
	je	.L586
	movq	8(%r14), %rax
	movq	(%r14), %r13
	movq	%rax, -64(%rbp)
	cmpq	%r13, %rax
	je	.L587
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %r15
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L644:
	movq	112(%r12), %r8
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r8, %r8
	je	.L590
	movq	(%r8), %rax
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	jne	.L591
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L592
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L592:
	movl	$64, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L590:
	movq	104(%r12), %r8
	testq	%r8, %r8
	je	.L593
	movq	(%r8), %rax
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	jne	.L594
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L595
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L595:
	movl	$64, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L593:
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	cmpq	%rax, %rdi
	je	.L596
	call	_ZdlPv@PLT
.L596:
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	je	.L597
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L598
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movq	-56(%rbp), %rdi
	movl	$320, %esi
	call	_ZdlPvm@PLT
.L597:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L599
	call	_ZdlPv@PLT
.L599:
	movl	$120, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L588:
	addq	$8, %r13
	cmpq	%r13, -64(%rbp)
	je	.L643
.L600:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L588
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L644
	movq	%r12, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -64(%rbp)
	jne	.L600
	.p2align 4,,10
	.p2align 3
.L643:
	movq	(%r14), %r13
.L587:
	testq	%r13, %r13
	je	.L601
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L601:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L586:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L602
	call	_ZdlPv@PLT
.L602:
	movq	96(%rbx), %r12
	testq	%r12, %r12
	je	.L603
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L604
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L605
	call	_ZdlPv@PLT
.L605:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L603:
	movq	88(%rbx), %r12
	testq	%r12, %r12
	je	.L606
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L607
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L608
	call	_ZdlPv@PLT
.L608:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L606:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L609
	call	_ZdlPv@PLT
.L609:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L581
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L581:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L598:
	.cfi_restore_state
	call	*%rax
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L594:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L591:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L585:
	call	*%rax
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L604:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L607:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L583:
	call	*%rax
	jmp	.L582
	.cfi_endproc
.LFE6617:
	.size	_ZN12v8_inspector8protocol8Debugger9CallFrameD2Ev, .-_ZN12v8_inspector8protocol8Debugger9CallFrameD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev,_ZN12v8_inspector8protocol8Debugger9CallFrameD2Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev, @function
_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev:
.LFB6619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6619:
	.size	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev, .-_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger5ScopeD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger5ScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev, @function
_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev:
.LFB6697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	112(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L648
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L649
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L650
	call	_ZdlPv@PLT
.L650:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L648:
	movq	104(%r12), %r13
	testq	%r13, %r13
	je	.L651
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L652
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L653
	call	_ZdlPv@PLT
.L653:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L651:
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	cmpq	%rax, %rdi
	je	.L654
	call	_ZdlPv@PLT
.L654:
	movq	48(%r12), %r13
	testq	%r13, %r13
	je	.L655
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L656
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L655:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L657
	call	_ZdlPv@PLT
.L657:
	movq	%r12, %rdi
	movl	$120, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L652:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L656:
	call	*%rax
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L649:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L648
	.cfi_endproc
.LFE6697:
	.size	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev, .-_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev
	.section	.text._ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0, @function
_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0:
.LFB15947:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %rax
	movq	%rdi, -136(%rbp)
	movq	%rbx, -168(%rbp)
	movq	%rax, -88(%rbp)
	cmpq	%rax, %rbx
	je	.L669
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	-88(%rbp), %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	je	.L670
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L671
	movq	160(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger9CallFrameE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L672
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L673
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L672:
	movq	152(%r15), %r12
	testq	%r12, %r12
	je	.L674
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L675
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	movq	312(%r12), %r13
	leaq	-64(%rax), %rbx
	movq	%rax, %xmm1
	movq	%rbx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	testq	%r13, %r13
	je	.L676
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L677
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L678
	call	_ZdlPv@PLT
.L678:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L679
	call	_ZdlPv@PLT
.L679:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L676:
	movq	304(%r12), %r13
	testq	%r13, %r13
	je	.L680
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L681
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L680:
	movq	264(%r12), %rdi
	leaq	280(%r12), %rax
	cmpq	%rax, %rdi
	je	.L682
	call	_ZdlPv@PLT
.L682:
	movq	216(%r12), %rdi
	leaq	232(%r12), %rax
	cmpq	%rax, %rdi
	je	.L683
	call	_ZdlPv@PLT
.L683:
	movq	168(%r12), %rdi
	leaq	184(%r12), %rax
	cmpq	%rax, %rdi
	je	.L684
	call	_ZdlPv@PLT
.L684:
	movq	152(%r12), %rdi
	testq	%rdi, %rdi
	je	.L685
	movq	(%rdi), %rax
	call	*24(%rax)
.L685:
	movq	112(%r12), %rdi
	leaq	128(%r12), %rax
	cmpq	%rax, %rdi
	je	.L686
	call	_ZdlPv@PLT
.L686:
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	cmpq	%rax, %rdi
	je	.L687
	call	_ZdlPv@PLT
.L687:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L688
	call	_ZdlPv@PLT
.L688:
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L674:
	movq	144(%r15), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L689
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -176(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rbx
	je	.L690
	movq	%r15, -184(%rbp)
	.p2align 4,,10
	.p2align 3
.L1041:
	movq	-64(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -72(%rbp)
	testq	%rcx, %rcx
	je	.L691
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L692
	movq	112(%rcx), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%r12, %r12
	je	.L693
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L694
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L695
	call	_ZdlPv@PLT
.L695:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L693:
	movq	-72(%rbp), %rax
	movq	104(%rax), %r12
	testq	%r12, %r12
	je	.L696
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L697
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L698
	call	_ZdlPv@PLT
.L698:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L696:
	movq	-72(%rbp), %rax
	movq	64(%rax), %rdi
	addq	$80, %rax
	cmpq	%rax, %rdi
	je	.L699
	call	_ZdlPv@PLT
.L699:
	movq	-72(%rbp), %rax
	movq	48(%rax), %rbx
	movq	%rbx, -56(%rbp)
	testq	%rbx, %rbx
	je	.L700
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L701
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	movq	312(%rbx), %r12
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm2
	movq	%rcx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	testq	%r12, %r12
	je	.L702
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L703
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L704
	call	_ZdlPv@PLT
.L704:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L705
	call	_ZdlPv@PLT
.L705:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L702:
	movq	-56(%rbp), %rax
	movq	304(%rax), %rdx
	movq	%rdx, -96(%rbp)
	testq	%rdx, %rdx
	je	.L706
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L707
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	160(%rdx), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L708
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -192(%rbp)
	movq	%rax, -80(%rbp)
	cmpq	%rax, %rdx
	je	.L709
	.p2align 4,,10
	.p2align 3
.L916:
	movq	-80(%rbp), %rax
	movq	(%rax), %rdx
	movq	%rdx, -120(%rbp)
	testq	%rdx, %rdx
	je	.L710
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L711
	movq	16(%rdx), %rcx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	%rcx, -104(%rbp)
	testq	%rcx, %rcx
	je	.L712
	movq	(%rcx), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L713
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L714
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -200(%rbp)
	movq	%rax, -112(%rbp)
	cmpq	%rax, %rdx
	je	.L715
	.p2align 4,,10
	.p2align 3
.L772:
	movq	-112(%rbp), %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	je	.L716
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L717
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L718
	movq	(%r12), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L719
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	movq	160(%r12), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L720
	movq	8(%rax), %rdx
	movq	(%rax), %r13
	movq	%rdx, -208(%rbp)
	cmpq	%r13, %rdx
	je	.L721
	movq	%r12, -216(%rbp)
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L1489:
	movq	16(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L724
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L725
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L724:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L726
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L727
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L726:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L722:
	addq	$8, %r13
	cmpq	%r13, -208(%rbp)
	je	.L1488
.L728:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L722
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1489
	movq	%r14, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -208(%rbp)
	jne	.L728
	.p2align 4,,10
	.p2align 3
.L1488:
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	(%rax), %r13
.L721:
	testq	%r13, %r13
	je	.L729
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L729:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L720:
	movq	152(%r12), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L730
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	movq	%rcx, -208(%rbp)
	cmpq	%r13, %rcx
	je	.L731
	movq	%r12, -216(%rbp)
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L1491:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L734
	call	_ZdlPv@PLT
.L734:
	movq	136(%r14), %r12
	testq	%r12, %r12
	je	.L735
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L736
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L735:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L737
	call	_ZdlPv@PLT
.L737:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L738
	call	_ZdlPv@PLT
.L738:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L739
	call	_ZdlPv@PLT
.L739:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L732:
	addq	$8, %r13
	cmpq	%r13, -208(%rbp)
	je	.L1490
.L740:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L732
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1491
	movq	%r14, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -208(%rbp)
	jne	.L740
	.p2align 4,,10
	.p2align 3
.L1490:
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	(%rax), %r13
.L731:
	testq	%r13, %r13
	je	.L741
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L741:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L730:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L742
	call	_ZdlPv@PLT
.L742:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L743
	call	_ZdlPv@PLT
.L743:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L744
	call	_ZdlPv@PLT
.L744:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L718:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L745
	movq	(%r12), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L746
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	movq	160(%r12), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L747
	movq	8(%rax), %rsi
	movq	(%rax), %r13
	movq	%rsi, -208(%rbp)
	cmpq	%r13, %rsi
	je	.L748
	movq	%r12, -216(%rbp)
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L1493:
	movq	16(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L751
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L752
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L751:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L753
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L754
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L753:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L749:
	addq	$8, %r13
	cmpq	%r13, -208(%rbp)
	je	.L1492
.L755:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L749
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1493
	movq	%r14, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -208(%rbp)
	jne	.L755
	.p2align 4,,10
	.p2align 3
.L1492:
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	(%rax), %r13
.L748:
	testq	%r13, %r13
	je	.L756
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L756:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L747:
	movq	152(%r12), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L757
	movq	8(%rax), %rdx
	movq	(%rax), %r13
	movq	%rdx, -208(%rbp)
	cmpq	%r13, %rdx
	je	.L758
	movq	%r12, -216(%rbp)
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L1495:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L761
	call	_ZdlPv@PLT
.L761:
	movq	136(%r14), %r12
	testq	%r12, %r12
	je	.L762
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L763
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L762:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L764
	call	_ZdlPv@PLT
.L764:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L765
	call	_ZdlPv@PLT
.L765:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L766
	call	_ZdlPv@PLT
.L766:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L759:
	addq	$8, %r13
	cmpq	%r13, -208(%rbp)
	je	.L1494
.L767:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L759
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1495
	movq	%r14, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -208(%rbp)
	jne	.L767
	.p2align 4,,10
	.p2align 3
.L1494:
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	(%rax), %r13
.L758:
	testq	%r13, %r13
	je	.L768
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L768:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L757:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L769
	call	_ZdlPv@PLT
.L769:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L770
	call	_ZdlPv@PLT
.L770:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L771
	call	_ZdlPv@PLT
.L771:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L745:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L716:
	addq	$8, -112(%rbp)
	movq	-112(%rbp), %rax
	cmpq	%rax, -200(%rbp)
	jne	.L772
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -112(%rbp)
.L715:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L773
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L773:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L714:
	movq	-104(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L774
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -200(%rbp)
	movq	%rax, -112(%rbp)
	cmpq	%rax, %rcx
	je	.L775
	.p2align 4,,10
	.p2align 3
.L809:
	movq	-112(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L776
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L777
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L778
	call	_ZdlPv@PLT
.L778:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L779
	movq	0(%r13), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L780
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L781
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -208(%rbp)
	cmpq	%r14, %rcx
	je	.L782
	movq	%r12, -216(%rbp)
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L1497:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L785
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L786
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L785:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L787
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L788
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L787:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L783:
	addq	$8, %r14
	cmpq	%r14, -208(%rbp)
	je	.L1496
.L789:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L783
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1497
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -208(%rbp)
	jne	.L789
	.p2align 4,,10
	.p2align 3
.L1496:
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	(%rax), %r14
.L782:
	testq	%r14, %r14
	je	.L790
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L790:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L781:
	movq	152(%r13), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L791
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	movq	%rsi, -208(%rbp)
	cmpq	%r14, %rsi
	je	.L792
	movq	%r12, -216(%rbp)
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L1499:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L795
	call	_ZdlPv@PLT
.L795:
	movq	136(%r15), %r12
	testq	%r12, %r12
	je	.L796
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L797
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L796:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L798
	call	_ZdlPv@PLT
.L798:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L799
	call	_ZdlPv@PLT
.L799:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L800
	call	_ZdlPv@PLT
.L800:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L793:
	addq	$8, %r14
	cmpq	%r14, -208(%rbp)
	je	.L1498
.L801:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L793
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1499
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -208(%rbp)
	jne	.L801
	.p2align 4,,10
	.p2align 3
.L1498:
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	(%rax), %r14
.L792:
	testq	%r14, %r14
	je	.L802
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L802:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L791:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L803
	call	_ZdlPv@PLT
.L803:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L804
	call	_ZdlPv@PLT
.L804:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L805
	call	_ZdlPv@PLT
.L805:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L779:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L806
	call	_ZdlPv@PLT
.L806:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L807
	call	_ZdlPv@PLT
.L807:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L808
	call	_ZdlPv@PLT
.L808:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L776:
	addq	$8, -112(%rbp)
	movq	-112(%rbp), %rax
	cmpq	%rax, -200(%rbp)
	jne	.L809
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -112(%rbp)
.L775:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L810
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L810:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L774:
	movq	-104(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L811
	call	_ZdlPv@PLT
.L811:
	movq	-104(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L812
	call	_ZdlPv@PLT
.L812:
	movq	-104(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L813
	call	_ZdlPv@PLT
.L813:
	movq	-104(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L712:
	movq	-120(%rbp), %rax
	movq	8(%rax), %rdx
	movq	%rdx, -104(%rbp)
	testq	%rdx, %rdx
	je	.L814
	movq	(%rdx), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L815
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	160(%rdx), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L816
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -200(%rbp)
	movq	%rax, -112(%rbp)
	cmpq	%rax, %rdx
	je	.L817
	.p2align 4,,10
	.p2align 3
.L874:
	movq	-112(%rbp), %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	je	.L818
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L819
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L820
	movq	(%r12), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L821
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	movq	160(%r12), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L822
	movq	8(%rax), %rdx
	movq	(%rax), %r13
	movq	%rdx, -208(%rbp)
	cmpq	%r13, %rdx
	je	.L823
	movq	%r12, -216(%rbp)
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L1501:
	movq	16(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L826
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L827
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L826:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L828
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L829
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L828:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L824:
	addq	$8, %r13
	cmpq	%r13, -208(%rbp)
	je	.L1500
.L830:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L824
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1501
	movq	%r14, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -208(%rbp)
	jne	.L830
	.p2align 4,,10
	.p2align 3
.L1500:
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	(%rax), %r13
.L823:
	testq	%r13, %r13
	je	.L831
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L831:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L822:
	movq	152(%r12), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L832
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	movq	%rcx, -208(%rbp)
	cmpq	%r13, %rcx
	je	.L833
	movq	%r12, -216(%rbp)
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L1503:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L836
	call	_ZdlPv@PLT
.L836:
	movq	136(%r14), %r12
	testq	%r12, %r12
	je	.L837
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L838
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L837:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L839
	call	_ZdlPv@PLT
.L839:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L840
	call	_ZdlPv@PLT
.L840:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L841
	call	_ZdlPv@PLT
.L841:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L834:
	addq	$8, %r13
	cmpq	%r13, -208(%rbp)
	je	.L1502
.L842:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L834
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1503
	movq	%r14, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -208(%rbp)
	jne	.L842
	.p2align 4,,10
	.p2align 3
.L1502:
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	(%rax), %r13
.L833:
	testq	%r13, %r13
	je	.L843
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L843:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L832:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L844
	call	_ZdlPv@PLT
.L844:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L845
	call	_ZdlPv@PLT
.L845:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L846
	call	_ZdlPv@PLT
.L846:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L820:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L847
	movq	(%r12), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L848
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	movq	160(%r12), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L849
	movq	8(%rax), %rsi
	movq	(%rax), %r13
	movq	%rsi, -208(%rbp)
	cmpq	%r13, %rsi
	je	.L850
	movq	%r12, -216(%rbp)
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L1505:
	movq	16(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L853
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L854
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L853:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L855
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L856
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L855:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L851:
	addq	$8, %r13
	cmpq	%r13, -208(%rbp)
	je	.L1504
.L857:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L851
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1505
	movq	%r14, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -208(%rbp)
	jne	.L857
	.p2align 4,,10
	.p2align 3
.L1504:
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	(%rax), %r13
.L850:
	testq	%r13, %r13
	je	.L858
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L858:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L849:
	movq	152(%r12), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L859
	movq	8(%rax), %rdx
	movq	(%rax), %r13
	movq	%rdx, -208(%rbp)
	cmpq	%r13, %rdx
	je	.L860
	movq	%r12, -216(%rbp)
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L1507:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L863
	call	_ZdlPv@PLT
.L863:
	movq	136(%r14), %r12
	testq	%r12, %r12
	je	.L864
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L865
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L864:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L866
	call	_ZdlPv@PLT
.L866:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L867
	call	_ZdlPv@PLT
.L867:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L868
	call	_ZdlPv@PLT
.L868:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L861:
	addq	$8, %r13
	cmpq	%r13, -208(%rbp)
	je	.L1506
.L869:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L861
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1507
	movq	%r14, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -208(%rbp)
	jne	.L869
	.p2align 4,,10
	.p2align 3
.L1506:
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	(%rax), %r13
.L860:
	testq	%r13, %r13
	je	.L870
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L870:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L859:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L871
	call	_ZdlPv@PLT
.L871:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L872
	call	_ZdlPv@PLT
.L872:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L873
	call	_ZdlPv@PLT
.L873:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L847:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L818:
	addq	$8, -112(%rbp)
	movq	-112(%rbp), %rax
	cmpq	%rax, -200(%rbp)
	jne	.L874
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -112(%rbp)
.L817:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L875
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L875:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L816:
	movq	-104(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L876
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -200(%rbp)
	movq	%rax, -112(%rbp)
	cmpq	%rax, %rcx
	je	.L877
	.p2align 4,,10
	.p2align 3
.L911:
	movq	-112(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L878
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L879
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L880
	call	_ZdlPv@PLT
.L880:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L881
	movq	0(%r13), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L882
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L883
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -208(%rbp)
	cmpq	%r14, %rcx
	je	.L884
	movq	%r12, -216(%rbp)
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L1509:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L887
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L888
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L887:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L889
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L890
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L889:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L885:
	addq	$8, %r14
	cmpq	%r14, -208(%rbp)
	je	.L1508
.L891:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L885
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1509
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -208(%rbp)
	jne	.L891
	.p2align 4,,10
	.p2align 3
.L1508:
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	(%rax), %r14
.L884:
	testq	%r14, %r14
	je	.L892
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L892:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L883:
	movq	152(%r13), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L893
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	movq	%rsi, -208(%rbp)
	cmpq	%r14, %rsi
	je	.L894
	movq	%r12, -216(%rbp)
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L1511:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L897
	call	_ZdlPv@PLT
.L897:
	movq	136(%r15), %r12
	testq	%r12, %r12
	je	.L898
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L899
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L898:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L900
	call	_ZdlPv@PLT
.L900:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L901
	call	_ZdlPv@PLT
.L901:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L902
	call	_ZdlPv@PLT
.L902:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L895:
	addq	$8, %r14
	cmpq	%r14, -208(%rbp)
	je	.L1510
.L903:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L895
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1511
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -208(%rbp)
	jne	.L903
	.p2align 4,,10
	.p2align 3
.L1510:
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	(%rax), %r14
.L894:
	testq	%r14, %r14
	je	.L904
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L904:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L893:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L905
	call	_ZdlPv@PLT
.L905:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L906
	call	_ZdlPv@PLT
.L906:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L907
	call	_ZdlPv@PLT
.L907:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L881:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L908
	call	_ZdlPv@PLT
.L908:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L909
	call	_ZdlPv@PLT
.L909:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L910
	call	_ZdlPv@PLT
.L910:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L878:
	addq	$8, -112(%rbp)
	movq	-112(%rbp), %rax
	cmpq	%rax, -200(%rbp)
	jne	.L911
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -112(%rbp)
.L877:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L912
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L912:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L876:
	movq	-104(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L913
	call	_ZdlPv@PLT
.L913:
	movq	-104(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L914
	call	_ZdlPv@PLT
.L914:
	movq	-104(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L915
	call	_ZdlPv@PLT
.L915:
	movq	-104(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L814:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L710:
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	%rax, -192(%rbp)
	jne	.L916
	movq	-144(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
.L709:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L917
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L917:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L708:
	movq	-96(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L918
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -192(%rbp)
	movq	%rax, -104(%rbp)
	cmpq	%rax, %rcx
	je	.L919
	.p2align 4,,10
	.p2align 3
.L1028:
	movq	-104(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -80(%rbp)
	testq	%rcx, %rcx
	je	.L920
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L921
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rcx), %rdi
	movq	%rax, (%rcx)
	leaq	168(%rcx), %rax
	cmpq	%rax, %rdi
	je	.L922
	call	_ZdlPv@PLT
.L922:
	movq	-80(%rbp), %rax
	movq	136(%rax), %rcx
	movq	%rcx, -112(%rbp)
	testq	%rcx, %rcx
	je	.L923
	movq	(%rcx), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L924
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L925
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -200(%rbp)
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rsi
	je	.L926
	.p2align 4,,10
	.p2align 3
.L983:
	movq	-120(%rbp), %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	je	.L927
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L928
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L929
	movq	(%r12), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L930
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	movq	160(%r12), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L931
	movq	8(%rax), %rdx
	movq	(%rax), %r13
	movq	%rdx, -208(%rbp)
	cmpq	%r13, %rdx
	je	.L932
	movq	%r12, -216(%rbp)
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L1513:
	movq	16(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L935
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L936
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L935:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L937
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L938
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L937:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L933:
	addq	$8, %r13
	cmpq	%r13, -208(%rbp)
	je	.L1512
.L939:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L933
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1513
	movq	%r14, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -208(%rbp)
	jne	.L939
	.p2align 4,,10
	.p2align 3
.L1512:
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	(%rax), %r13
.L932:
	testq	%r13, %r13
	je	.L940
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L940:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L931:
	movq	152(%r12), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L941
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	movq	%rcx, -208(%rbp)
	cmpq	%r13, %rcx
	je	.L942
	movq	%r12, -216(%rbp)
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L1515:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L945
	call	_ZdlPv@PLT
.L945:
	movq	136(%r14), %r12
	testq	%r12, %r12
	je	.L946
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L947
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L946:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L948
	call	_ZdlPv@PLT
.L948:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L949
	call	_ZdlPv@PLT
.L949:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L950
	call	_ZdlPv@PLT
.L950:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L943:
	addq	$8, %r13
	cmpq	%r13, -208(%rbp)
	je	.L1514
.L951:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L943
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1515
	movq	%r14, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -208(%rbp)
	jne	.L951
	.p2align 4,,10
	.p2align 3
.L1514:
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	(%rax), %r13
.L942:
	testq	%r13, %r13
	je	.L952
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L952:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L941:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L953
	call	_ZdlPv@PLT
.L953:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L954
	call	_ZdlPv@PLT
.L954:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L955
	call	_ZdlPv@PLT
.L955:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L929:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L956
	movq	(%r12), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L957
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	movq	160(%r12), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L958
	movq	8(%rax), %rsi
	movq	(%rax), %r13
	movq	%rsi, -208(%rbp)
	cmpq	%r13, %rsi
	je	.L959
	movq	%r12, -216(%rbp)
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L1517:
	movq	16(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L962
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L963
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L962:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L964
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L965
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L964:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L960:
	addq	$8, %r13
	cmpq	%r13, -208(%rbp)
	je	.L1516
.L966:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L960
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1517
	movq	%r14, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -208(%rbp)
	jne	.L966
	.p2align 4,,10
	.p2align 3
.L1516:
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	(%rax), %r13
.L959:
	testq	%r13, %r13
	je	.L967
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L967:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L958:
	movq	152(%r12), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L968
	movq	8(%rax), %rdx
	movq	(%rax), %r13
	movq	%rdx, -208(%rbp)
	cmpq	%r13, %rdx
	je	.L969
	movq	%r12, -216(%rbp)
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L1519:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L972
	call	_ZdlPv@PLT
.L972:
	movq	136(%r14), %r12
	testq	%r12, %r12
	je	.L973
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L974
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L973:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L975
	call	_ZdlPv@PLT
.L975:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L976
	call	_ZdlPv@PLT
.L976:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L977
	call	_ZdlPv@PLT
.L977:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L970:
	addq	$8, %r13
	cmpq	%r13, -208(%rbp)
	je	.L1518
.L978:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L970
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1519
	movq	%r14, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -208(%rbp)
	jne	.L978
	.p2align 4,,10
	.p2align 3
.L1518:
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	(%rax), %r13
.L969:
	testq	%r13, %r13
	je	.L979
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L979:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L968:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L980
	call	_ZdlPv@PLT
.L980:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L981
	call	_ZdlPv@PLT
.L981:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L982
	call	_ZdlPv@PLT
.L982:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L956:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L927:
	addq	$8, -120(%rbp)
	movq	-120(%rbp), %rax
	cmpq	%rax, -200(%rbp)
	jne	.L983
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -120(%rbp)
.L926:
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	je	.L984
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L984:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L925:
	movq	-112(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L985
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -200(%rbp)
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdx
	je	.L986
	.p2align 4,,10
	.p2align 3
.L1020:
	movq	-120(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L987
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L988
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L989
	call	_ZdlPv@PLT
.L989:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L990
	movq	0(%r13), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L991
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L992
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -208(%rbp)
	cmpq	%r14, %rcx
	je	.L993
	movq	%r12, -216(%rbp)
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1521:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L996
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L997
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L996:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L998
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L999
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L998:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L994:
	addq	$8, %r14
	cmpq	%r14, -208(%rbp)
	je	.L1520
.L1000:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L994
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1521
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -208(%rbp)
	jne	.L1000
	.p2align 4,,10
	.p2align 3
.L1520:
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	(%rax), %r14
.L993:
	testq	%r14, %r14
	je	.L1001
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1001:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L992:
	movq	152(%r13), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L1002
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	movq	%rsi, -208(%rbp)
	cmpq	%r14, %rsi
	je	.L1003
	movq	%r12, -216(%rbp)
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1523:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1006
	call	_ZdlPv@PLT
.L1006:
	movq	136(%r15), %r12
	testq	%r12, %r12
	je	.L1007
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1008
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1007:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1009
	call	_ZdlPv@PLT
.L1009:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1010
	call	_ZdlPv@PLT
.L1010:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1011
	call	_ZdlPv@PLT
.L1011:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1004:
	addq	$8, %r14
	cmpq	%r14, -208(%rbp)
	je	.L1522
.L1012:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L1004
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1523
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -208(%rbp)
	jne	.L1012
	.p2align 4,,10
	.p2align 3
.L1522:
	movq	-160(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	(%rax), %r14
.L1003:
	testq	%r14, %r14
	je	.L1013
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1013:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1002:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1014
	call	_ZdlPv@PLT
.L1014:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1015
	call	_ZdlPv@PLT
.L1015:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1016
	call	_ZdlPv@PLT
.L1016:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L990:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1017
	call	_ZdlPv@PLT
.L1017:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1018
	call	_ZdlPv@PLT
.L1018:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1019
	call	_ZdlPv@PLT
.L1019:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L987:
	addq	$8, -120(%rbp)
	movq	-120(%rbp), %rax
	cmpq	%rax, -200(%rbp)
	jne	.L1020
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -120(%rbp)
.L986:
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	je	.L1021
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1021:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L985:
	movq	-112(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1022
	call	_ZdlPv@PLT
.L1022:
	movq	-112(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1023
	call	_ZdlPv@PLT
.L1023:
	movq	-112(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1024
	call	_ZdlPv@PLT
.L1024:
	movq	-112(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L923:
	movq	-80(%rbp), %rax
	movq	96(%rax), %rdi
	addq	$112, %rax
	cmpq	%rax, %rdi
	je	.L1025
	call	_ZdlPv@PLT
.L1025:
	movq	-80(%rbp), %rax
	movq	48(%rax), %rdi
	addq	$64, %rax
	cmpq	%rax, %rdi
	je	.L1026
	call	_ZdlPv@PLT
.L1026:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1027
	call	_ZdlPv@PLT
.L1027:
	movq	-80(%rbp), %rdi
	movl	$192, %esi
	call	_ZdlPvm@PLT
.L920:
	addq	$8, -104(%rbp)
	movq	-104(%rbp), %rax
	cmpq	%rax, -192(%rbp)
	jne	.L1028
	movq	-144(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -104(%rbp)
.L919:
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L1029
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1029:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L918:
	movq	-96(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1030
	call	_ZdlPv@PLT
.L1030:
	movq	-96(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1031
	call	_ZdlPv@PLT
.L1031:
	movq	-96(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1032
	call	_ZdlPv@PLT
.L1032:
	movq	-96(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L706:
	movq	-56(%rbp), %rax
	movq	264(%rax), %rdi
	addq	$280, %rax
	cmpq	%rax, %rdi
	je	.L1033
	call	_ZdlPv@PLT
.L1033:
	movq	-56(%rbp), %rax
	movq	216(%rax), %rdi
	addq	$232, %rax
	cmpq	%rax, %rdi
	je	.L1034
	call	_ZdlPv@PLT
.L1034:
	movq	-56(%rbp), %rax
	movq	168(%rax), %rdi
	addq	$184, %rax
	cmpq	%rax, %rdi
	je	.L1035
	call	_ZdlPv@PLT
.L1035:
	movq	-56(%rbp), %rax
	movq	152(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1036
	movq	(%rdi), %rax
	call	*24(%rax)
.L1036:
	movq	-56(%rbp), %rax
	movq	112(%rax), %rdi
	subq	$-128, %rax
	cmpq	%rax, %rdi
	je	.L1037
	call	_ZdlPv@PLT
.L1037:
	movq	-56(%rbp), %rax
	movq	64(%rax), %rdi
	addq	$80, %rax
	cmpq	%rax, %rdi
	je	.L1038
	call	_ZdlPv@PLT
.L1038:
	movq	-56(%rbp), %rax
	movq	16(%rax), %rdi
	addq	$32, %rax
	cmpq	%rax, %rdi
	je	.L1039
	call	_ZdlPv@PLT
.L1039:
	movq	-56(%rbp), %rdi
	movl	$320, %esi
	call	_ZdlPvm@PLT
.L700:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1040
	call	_ZdlPv@PLT
.L1040:
	movq	-72(%rbp), %rdi
	movl	$120, %esi
	call	_ZdlPvm@PLT
.L691:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -176(%rbp)
	jne	.L1041
	movq	-128(%rbp), %rax
	movq	-184(%rbp), %r15
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L690:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L1042
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1042:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L689:
	movq	104(%r15), %rdi
	leaq	120(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1043
	call	_ZdlPv@PLT
.L1043:
	movq	96(%r15), %r12
	testq	%r12, %r12
	je	.L1044
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1045
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1046
	call	_ZdlPv@PLT
.L1046:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1044:
	movq	88(%r15), %r12
	testq	%r12, %r12
	je	.L1047
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1048
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1049
	call	_ZdlPv@PLT
.L1049:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1047:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1050
	call	_ZdlPv@PLT
.L1050:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1051
	call	_ZdlPv@PLT
.L1051:
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L670:
	addq	$8, -88(%rbp)
	movq	-88(%rbp), %rax
	cmpq	%rax, -168(%rbp)
	jne	.L1052
	movq	-136(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
.L669:
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L1053
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1053:
	movq	-136(%rbp), %rdi
	addq	$184, %rsp
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L921:
	.cfi_restore_state
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L711:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L701:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L697:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L694:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L692:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L671:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L703:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L707:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L988:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L928:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L777:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L879:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L819:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L717:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L1048:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1047
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L675:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L673:
	call	*%rax
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L924:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L713:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L815:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L991:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L930:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L780:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L821:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L957:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L746:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L719:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L882:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L848:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L681:
	call	*%rax
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L677:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L854:
	call	*%rax
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L797:
	call	*%rax
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L736:
	call	*%rax
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L856:
	call	*%rax
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L727:
	call	*%rax
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L725:
	call	*%rax
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L888:
	call	*%rax
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L788:
	call	*%rax
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L838:
	call	*%rax
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L827:
	call	*%rax
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L829:
	call	*%rax
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L963:
	call	*%rax
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L965:
	call	*%rax
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L752:
	call	*%rax
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L754:
	call	*%rax
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L999:
	call	*%rax
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L936:
	call	*%rax
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L947:
	call	*%rax
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L938:
	call	*%rax
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L1008:
	call	*%rax
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L899:
	call	*%rax
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L865:
	call	*%rax
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L890:
	call	*%rax
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L763:
	call	*%rax
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L786:
	call	*%rax
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L974:
	call	*%rax
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L997:
	call	*%rax
	jmp	.L996
	.cfi_endproc
.LFE15947:
	.size	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0, .-_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev:
.LFB16106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$320, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE16106:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime10StackTraceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev, @function
_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev:
.LFB6206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime10StackTraceE(%rip), %rax
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %r12
	movups	%xmm0, (%rdi)
	testq	%r12, %r12
	je	.L1527
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1528
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rsi
	movq	%rax, %xmm2
	leaq	80(%r12), %rax
	movq	%rsi, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L1529
	call	_ZdlPv@PLT
.L1529:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1530
	call	_ZdlPv@PLT
.L1530:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1527:
	movq	72(%rbx), %r12
	testq	%r12, %r12
	je	.L1531
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1532
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1531:
	movq	64(%rbx), %r13
	testq	%r13, %r13
	je	.L1533
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	jne	.L1540
	testq	%r12, %r12
	je	.L1541
	.p2align 4,,10
	.p2align 3
.L1562:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1541:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1533:
	movq	24(%rbx), %rdi
	addq	$40, %rbx
	cmpq	%rbx, %rdi
	je	.L1526
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1561:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r15), %rdi
	movq	%rax, (%r15)
	leaq	104(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1537
	call	_ZdlPv@PLT
.L1537:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1538
	call	_ZdlPv@PLT
.L1538:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1539
	call	_ZdlPv@PLT
.L1539:
	movl	$136, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1535:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L1560
.L1540:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L1535
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1561
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r12, %r14
	jne	.L1540
	.p2align 4,,10
	.p2align 3
.L1560:
	movq	0(%r13), %r12
	testq	%r12, %r12
	jne	.L1562
	jmp	.L1541
	.p2align 4,,10
	.p2align 3
.L1526:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1532:
	.cfi_restore_state
	call	*%rax
	jmp	.L1531
	.p2align 4,,10
	.p2align 3
.L1528:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1527
	.cfi_endproc
.LFE6206:
	.size	_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev, .-_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev
	.set	.LTHUNK2,_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev:
.LFB16141:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK2
	.cfi_endproc
.LFE16141:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD1Ev
	.weak	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev,_ZN12v8_inspector8protocol7Runtime10StackTraceD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime10StackTraceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev, @function
_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev:
.LFB6208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6208:
	.size	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev, .-_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev, @function
_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev:
.LFB6079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	168(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L1566
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1567
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1566:
	movq	160(%r12), %r13
	testq	%r13, %r13
	je	.L1568
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1569
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1568:
	movq	120(%r12), %rdi
	leaq	136(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1570
	call	_ZdlPv@PLT
.L1570:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1571
	call	_ZdlPv@PLT
.L1571:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1572
	call	_ZdlPv@PLT
.L1572:
	movq	%r12, %rdi
	movl	$184, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1567:
	.cfi_restore_state
	call	*%rax
	jmp	.L1566
	.p2align 4,,10
	.p2align 3
.L1569:
	call	*%rax
	jmp	.L1568
	.cfi_endproc
.LFE6079:
	.size	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev, .-_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev, @function
_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev:
.LFB6077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	168(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L1581
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1582
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1581:
	movq	160(%rbx), %r12
	testq	%r12, %r12
	je	.L1583
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1584
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1583:
	movq	120(%rbx), %rdi
	leaq	136(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1585
	call	_ZdlPv@PLT
.L1585:
	movq	72(%rbx), %rdi
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1586
	call	_ZdlPv@PLT
.L1586:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L1580
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1580:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1584:
	.cfi_restore_state
	call	*%rax
	jmp	.L1583
	.p2align 4,,10
	.p2align 3
.L1582:
	call	*%rax
	jmp	.L1581
	.cfi_endproc
.LFE6077:
	.size	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev, .-_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD1Ev,_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime10StackTraceD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime10StackTraceD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev:
.LFB16108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN12v8_inspector8protocol7Runtime10StackTraceD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE16108:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime10StackTraceD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev:
.LFB6278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	80(%rbx), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	64(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L1598
	call	_ZdlPv@PLT
.L1598:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L1597
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1597:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6278:
	.size	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev, .-_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD1Ev,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD2Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev:
.LFB16105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	72(%rbx), %rax
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	movq	56(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L1602
	call	_ZdlPv@PLT
.L1602:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L1601
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1601:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE16105:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD1Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12StackTraceIdD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev:
.LFB16107:
	.cfi_startproc
	endbr64
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movups	%xmm0, -8(%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1606
	call	_ZdlPv@PLT
.L1606:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L1607
	call	_ZdlPv@PLT
.L1607:
	popq	%rbx
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE16107:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12StackTraceIdD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger11SearchMatchD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev, @function
_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev:
.LFB6739:
	.cfi_startproc
	endbr64
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	movq	24(%rdi), %r8
	addq	$40, %rdi
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -40(%rdi)
	cmpq	%rdi, %r8
	je	.L1609
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1609:
	ret
	.cfi_endproc
.LFE6739:
	.size	_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev, .-_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger11SearchMatchD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger11SearchMatchD1Ev,_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev
	.type	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev, @function
_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev:
.LFB16103:
	.cfi_startproc
	endbr64
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	movq	16(%rdi), %r8
	addq	$32, %rdi
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -40(%rdi)
	cmpq	%rdi, %r8
	je	.L1611
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1611:
	ret
	.cfi_endproc
.LFE16103:
	.size	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev, .-_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger11SearchMatchD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev
	.type	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev, @function
_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev:
.LFB16104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	addq	$32, %rdi
	subq	$8, %rsp
	movq	-16(%rdi), %r8
	movups	%xmm0, -40(%rdi)
	cmpq	%rdi, %r8
	je	.L1614
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L1614:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE16104:
	.size	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev, .-_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.type	_ZN12v8_inspector19V8DebuggerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE, @function
_ZN12v8_inspector19V8DebuggerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE:
.LFB7884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm3
	leaq	16+_ZTVN12v8_inspector19V8DebuggerAgentImplE(%rip), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movss	.LC6(%rip), %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	24(%rsi), %rax
	movq	%rax, 8(%rdi)
	movq	24(%rax), %rdi
	movq	%rsi, 24(%rbx)
	movq	%rdi, 16(%rbx)
	movl	$64, %edi
	movups	%xmm0, 40(%rbx)
	pxor	%xmm0, %xmm0
	movb	$0, 32(%rbx)
	movq	8(%rax), %rax
	movss	%xmm1, 96(%rbx)
	movq	%rax, 56(%rbx)
	leaq	112(%rbx), %rax
	movq	%rax, 64(%rbx)
	leaq	168(%rbx), %rax
	movq	%rax, 120(%rbx)
	leaq	224(%rbx), %rax
	movq	%rax, 176(%rbx)
	leaq	280(%rbx), %rax
	movss	%xmm1, 152(%rbx)
	movq	$1, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 112(%rbx)
	movq	$1, 128(%rbx)
	movq	$0, 136(%rbx)
	movq	$0, 144(%rbx)
	movq	$0, 160(%rbx)
	movq	$0, 168(%rbx)
	movq	$1, 184(%rbx)
	movq	$0, 192(%rbx)
	movq	$0, 200(%rbx)
	movss	%xmm1, 208(%rbx)
	movq	$0, 216(%rbx)
	movq	$0, 224(%rbx)
	movq	%rax, 232(%rbx)
	movq	$1, 240(%rbx)
	movq	$0, 248(%rbx)
	movq	$0, 256(%rbx)
	movq	$0, 272(%rbx)
	movq	$0, 280(%rbx)
	movq	$0, 304(%rbx)
	movq	$8, 312(%rbx)
	movss	%xmm1, 264(%rbx)
	movups	%xmm0, 288(%rbx)
	movups	%xmm0, 320(%rbx)
	movups	%xmm0, 336(%rbx)
	movups	%xmm0, 352(%rbx)
	movups	%xmm0, 368(%rbx)
	call	_Znwm@PLT
	movq	312(%rbx), %rdx
	movl	$480, %edi
	movq	%rax, 304(%rbx)
	leaq	-4(,%rdx,4), %r12
	andq	$-8, %r12
	addq	%rax, %r12
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movss	.LC6(%rip), %xmm1
	movq	%r12, 344(%rbx)
	movq	%rax, (%r12)
	leaq	480(%rax), %rdx
	movq	%rax, %xmm2
	movq	%rax, 360(%rbx)
	punpcklqdq	%xmm2, %xmm2
	movq	%rax, 352(%rbx)
	xorl	%eax, %eax
	movw	%ax, 408(%rbx)
	leaq	472(%rbx), %rax
	movq	%r12, 376(%rbx)
	movq	%rdx, 336(%rbx)
	movq	%rdx, 368(%rbx)
	movq	$0, 400(%rbx)
	movq	$0, 416(%rbx)
	movq	%rax, 424(%rbx)
	movq	$1, 432(%rbx)
	movq	$0, 440(%rbx)
	movq	$0, 448(%rbx)
	movq	$0, 464(%rbx)
	movq	$0, 472(%rbx)
	movups	%xmm2, 320(%rbx)
	movups	%xmm0, 384(%rbx)
	movss	%xmm1, 456(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7884:
	.size	_ZN12v8_inspector19V8DebuggerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE, .-_ZN12v8_inspector19V8DebuggerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.globl	_ZN12v8_inspector19V8DebuggerAgentImplC1EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.set	_ZN12v8_inspector19V8DebuggerAgentImplC1EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE,_ZN12v8_inspector19V8DebuggerAgentImplC2EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE
	.section	.text._ZNK12v8_inspector19V8DebuggerAgentImpl12acceptsPauseEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector19V8DebuggerAgentImpl12acceptsPauseEb
	.type	_ZNK12v8_inspector19V8DebuggerAgentImpl12acceptsPauseEb, @function
_ZNK12v8_inspector19V8DebuggerAgentImpl12acceptsPauseEb:
.LFB7952:
	.cfi_startproc
	endbr64
	movzbl	32(%rdi), %eax
	testb	%al, %al
	je	.L1618
	movl	%esi, %eax
	testb	%sil, %sil
	je	.L1624
.L1618:
	ret
	.p2align 4,,10
	.p2align 3
.L1624:
	movzbl	408(%rdi), %eax
	xorl	$1, %eax
	ret
	.cfi_endproc
.LFE7952:
	.size	_ZNK12v8_inspector19V8DebuggerAgentImpl12acceptsPauseEb, .-_ZNK12v8_inspector19V8DebuggerAgentImpl12acceptsPauseEb
	.section	.text._ZNSt4pairIN12v8_inspector8String16ESt10unique_ptrINS0_8protocol15DictionaryValueESt14default_deleteIS4_EEED2Ev,"axG",@progbits,_ZNSt4pairIN12v8_inspector8String16ESt10unique_ptrINS0_8protocol15DictionaryValueESt14default_deleteIS4_EEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt4pairIN12v8_inspector8String16ESt10unique_ptrINS0_8protocol15DictionaryValueESt14default_deleteIS4_EEED2Ev
	.type	_ZNSt4pairIN12v8_inspector8String16ESt10unique_ptrINS0_8protocol15DictionaryValueESt14default_deleteIS4_EEED2Ev, @function
_ZNSt4pairIN12v8_inspector8String16ESt10unique_ptrINS0_8protocol15DictionaryValueESt14default_deleteIS4_EEED2Ev:
.LFB7994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1626
	movq	(%rdi), %rax
	call	*24(%rax)
.L1626:
	movq	(%rbx), %rdi
	addq	$16, %rbx
	cmpq	%rbx, %rdi
	je	.L1625
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1625:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7994:
	.size	_ZNSt4pairIN12v8_inspector8String16ESt10unique_ptrINS0_8protocol15DictionaryValueESt14default_deleteIS4_EEED2Ev, .-_ZNSt4pairIN12v8_inspector8String16ESt10unique_ptrINS0_8protocol15DictionaryValueESt14default_deleteIS4_EEED2Ev
	.weak	_ZNSt4pairIN12v8_inspector8String16ESt10unique_ptrINS0_8protocol15DictionaryValueESt14default_deleteIS4_EEED1Ev
	.set	_ZNSt4pairIN12v8_inspector8String16ESt10unique_ptrINS0_8protocol15DictionaryValueESt14default_deleteIS4_EEED1Ev,_ZNSt4pairIN12v8_inspector8String16ESt10unique_ptrINS0_8protocol15DictionaryValueESt14default_deleteIS4_EEED2Ev
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl15popBreakDetailsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl15popBreakDetailsEv
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl15popBreakDetailsEv, @function
_ZN12v8_inspector19V8DebuggerAgentImpl15popBreakDetailsEv:
.LFB7996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	392(%rdi), %rbx
	cmpq	384(%rdi), %rbx
	je	.L1632
	leaq	-48(%rbx), %rax
	movq	%rax, 392(%rdi)
	movq	-8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1634
	movq	(%rdi), %rax
	call	*24(%rax)
.L1634:
	movq	-48(%rbx), %rdi
	subq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L1632
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1632:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7996:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl15popBreakDetailsEv, .-_ZN12v8_inspector19V8DebuggerAgentImpl15popBreakDetailsEv
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl17clearBreakDetailsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl17clearBreakDetailsEv
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl17clearBreakDetailsEv, @function
_ZN12v8_inspector19V8DebuggerAgentImpl17clearBreakDetailsEv:
.LFB7997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	384(%rdi), %r12
	movq	392(%rdi), %r13
	movq	$0, 400(%rdi)
	movups	%xmm0, 384(%rdi)
	cmpq	%r13, %r12
	je	.L1641
	movq	%r12, %rbx
	.p2align 4,,10
	.p2align 3
.L1645:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1642
	movq	(%rdi), %rax
	call	*24(%rax)
.L1642:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1643
	call	_ZdlPv@PLT
	addq	$48, %rbx
	cmpq	%rbx, %r13
	jne	.L1645
.L1641:
	testq	%r12, %r12
	je	.L1640
.L1651:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1643:
	.cfi_restore_state
	addq	$48, %rbx
	cmpq	%rbx, %r13
	jne	.L1645
	testq	%r12, %r12
	jne	.L1651
.L1640:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7997:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl17clearBreakDetailsEv, .-_ZN12v8_inspector19V8DebuggerAgentImpl17clearBreakDetailsEv
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl26cancelPauseOnNextStatementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl26cancelPauseOnNextStatementEv
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl26cancelPauseOnNextStatementEv, @function
_ZN12v8_inspector19V8DebuggerAgentImpl26cancelPauseOnNextStatementEv:
.LFB7999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rax
	movq	16(%rdi), %rdi
	movl	16(%rax), %esi
	call	_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi@PLT
	testb	%al, %al
	jne	.L1652
	cmpb	$0, 32(%rbx)
	je	.L1652
	cmpb	$0, 408(%rbx)
	jne	.L1652
	cmpb	$0, 409(%rbx)
	je	.L1652
	movq	392(%rbx), %rdi
	movq	384(%rbx), %rax
	movq	%rdi, %rdx
	subq	%rax, %rdx
	cmpq	$48, %rdx
	je	.L1658
.L1656:
	cmpq	%rax, %rdi
	je	.L1652
	subq	$48, %rdi
	movq	%rdi, 392(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt4pairIN12v8_inspector8String16ESt10unique_ptrINS0_8protocol15DictionaryValueESt14default_deleteIS4_EEED1Ev
	.p2align 4,,10
	.p2align 3
.L1652:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1658:
	.cfi_restore_state
	movq	24(%rbx), %rax
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	movl	16(%rax), %edx
	call	_ZN12v8_inspector10V8Debugger18setPauseOnNextCallEbi@PLT
	movq	392(%rbx), %rdi
	movq	384(%rbx), %rax
	jmp	.L1656
	.cfi_endproc
.LFE7999:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl26cancelPauseOnNextStatementEv, .-_ZN12v8_inspector19V8DebuggerAgentImpl26cancelPauseOnNextStatementEv
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl24setPauseOnExceptionsImplEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl24setPauseOnExceptionsImplEi
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl24setPauseOnExceptionsImplEi, @function
_ZN12v8_inspector19V8DebuggerAgentImpl24setPauseOnExceptionsImplEi:
.LFB8007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector10V8Debugger25setPauseOnExceptionsStateEN2v85debug19ExceptionBreakStateE@PLT
	movq	40(%rbx), %r14
	leaq	_ZN12v8_inspector18DebuggerAgentStateL22pauseOnExceptionsStateE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setIntegerERKNS_8String16Ei@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1659
	call	_ZdlPv@PLT
.L1659:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1663
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1663:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8007:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl24setPauseOnExceptionsImplEi, .-_ZN12v8_inspector19V8DebuggerAgentImpl24setPauseOnExceptionsImplEi
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl25resetBlackboxedStateCacheEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl25resetBlackboxedStateCacheEv
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl25resetBlackboxedStateCacheEv, @function
_ZN12v8_inspector19V8DebuggerAgentImpl25resetBlackboxedStateCacheEv:
.LFB8017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	80(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L1664
	.p2align 4,,10
	.p2align 3
.L1666:
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	call	*128(%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1666
.L1664:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8017:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl25resetBlackboxedStateCacheEv, .-_ZN12v8_inspector19V8DebuggerAgentImpl25resetBlackboxedStateCacheEv
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl22currentAsyncStackTraceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl22currentAsyncStackTraceEv
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl22currentAsyncStackTraceEv, @function
_ZN12v8_inspector19V8DebuggerAgentImpl22currentAsyncStackTraceEv:
.LFB8057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	-64(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	16(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector10V8Debugger18currentAsyncParentEv@PLT
	movq	-64(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L1686
	movq	16(%rbx), %rdx
	movq	%r12, %rdi
	movl	236(%rdx), %eax
	leal	-1(%rax), %ecx
	call	_ZNK12v8_inspector15AsyncStackTrace20buildInspectorObjectEPNS_10V8DebuggerEi@PLT
.L1674:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L1672
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1677
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1687
	.p2align 4,,10
	.p2align 3
.L1672:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1688
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1677:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1672
.L1687:
	movq	0(%r13), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1689
.L1680:
	testq	%rbx, %rbx
	je	.L1681
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1682:
	cmpl	$1, %eax
	jne	.L1672
	movq	0(%r13), %rax
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L1683
	call	*8(%rax)
	jmp	.L1672
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	$0, (%r12)
	jmp	.L1674
	.p2align 4,,10
	.p2align 3
.L1681:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1682
	.p2align 4,,10
	.p2align 3
.L1683:
	call	*%rdx
	jmp	.L1672
	.p2align 4,,10
	.p2align 3
.L1689:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1680
.L1688:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8057:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl22currentAsyncStackTraceEv, .-_ZN12v8_inspector19V8DebuggerAgentImpl22currentAsyncStackTraceEv
	.section	.text._ZNK12v8_inspector19V8DebuggerAgentImpl8isPausedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector19V8DebuggerAgentImpl8isPausedEv
	.type	_ZNK12v8_inspector19V8DebuggerAgentImpl8isPausedEv, @function
_ZNK12v8_inspector19V8DebuggerAgentImpl8isPausedEv:
.LFB8060:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	16(%rdi), %rdi
	movl	16(%rax), %esi
	jmp	_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi@PLT
	.cfi_endproc
.LFE8060:
	.size	_ZNK12v8_inspector19V8DebuggerAgentImpl8isPausedEv, .-_ZNK12v8_inspector19V8DebuggerAgentImpl8isPausedEv
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl11didContinueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl11didContinueEv
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl11didContinueEv, @function
_ZN12v8_inspector19V8DebuggerAgentImpl11didContinueEv:
.LFB8115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	384(%rdi), %r13
	movq	%rdi, %rbx
	movq	392(%rdi), %r14
	movq	$0, 400(%rdi)
	movups	%xmm0, 384(%rdi)
	cmpq	%r14, %r13
	je	.L1692
	movq	%r13, %r12
	.p2align 4,,10
	.p2align 3
.L1696:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1693
	movq	(%rdi), %rax
	call	*24(%rax)
.L1693:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1694
	call	_ZdlPv@PLT
	addq	$48, %r12
	cmpq	%r12, %r14
	jne	.L1696
.L1692:
	testq	%r13, %r13
	je	.L1697
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1697:
	leaq	48(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector8protocol8Debugger8Frontend7resumedEv@PLT
	.p2align 4,,10
	.p2align 3
.L1694:
	.cfi_restore_state
	addq	$48, %r12
	cmpq	%r12, %r14
	jne	.L1696
	jmp	.L1692
	.cfi_endproc
.LFE8115:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl11didContinueEv, .-_ZN12v8_inspector19V8DebuggerAgentImpl11didContinueEv
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_:
.LFB9416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	addq	$16, %rsi
	movq	(%rdi), %rdi
	cmpq	%rax, %rsi
	je	.L1721
	leaq	16(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L1722
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	16(%r12), %rdx
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r12)
	testq	%rdi, %rdi
	je	.L1711
	movq	%rdi, (%rbx)
	movq	%rdx, 16(%rbx)
.L1709:
	xorl	%eax, %eax
	movq	$0, 8(%rbx)
	movw	%ax, (%rdi)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1722:
	.cfi_restore_state
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r12)
.L1711:
	movq	%rsi, (%rbx)
	movq	%rsi, %rdi
	jmp	.L1709
	.p2align 4,,10
	.p2align 3
.L1721:
	movq	8(%rbx), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L1707
	cmpq	$1, %rax
	je	.L1723
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L1707
	call	memmove@PLT
	movq	8(%rbx), %rax
	movq	(%r12), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L1707:
	xorl	%ecx, %ecx
	movq	%rax, 8(%r12)
	movw	%cx, (%rdi,%rdx)
	movq	(%rbx), %rdi
	jmp	.L1709
	.p2align 4,,10
	.p2align 3
.L1723:
	movzwl	16(%rbx), %eax
	movw	%ax, (%rdi)
	movq	8(%rbx), %rax
	movq	(%r12), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L1707
	.cfi_endproc
.LFE9416:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	.section	.text._ZN12v8_inspector12_GLOBAL__N_117parseBreakpointIdERKNS_8String16EPNS0_14BreakpointTypeEPS1_PiS7_,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_117parseBreakpointIdERKNS_8String16EPNS0_14BreakpointTypeEPS1_PiS7_, @function
_ZN12v8_inspector12_GLOBAL__N_117parseBreakpointIdERKNS_8String16EPNS0_14BreakpointTypeEPS1_PiS7_:
.LFB7753:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -136(%rbp)
	movq	8(%rdi), %r15
	movq	%rcx, -144(%rbp)
	movq	%r8, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L1728
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	(%rdi), %rsi
	xorl	%r13d, %r13d
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L1779:
	addq	$1, %r13
	cmpq	%r13, %r15
	je	.L1728
.L1729:
	cmpw	$58, (%rsi,%r13,2)
	leaq	(%r13,%r13), %rax
	jne	.L1779
	movq	%rax, %r14
	sarq	%r14
	cmpq	$-2, %rax
	je	.L1728
	cmpq	%r14, %r15
	leaq	-112(%rbp), %r8
	cmova	%r14, %r15
	movq	%r8, -128(%rbp)
	movq	%r8, %rdi
	leaq	(%r15,%r15), %rdx
	movq	%rdx, %r10
	sarq	%r10
	cmpq	$14, %rdx
	ja	.L1780
.L1730:
	cmpq	$2, %rdx
	je	.L1781
	testq	%rdx, %rdx
	je	.L1733
	movq	%r8, -168(%rbp)
	movq	%r10, -160(%rbp)
	call	memmove@PLT
	movq	-128(%rbp), %rdi
	movq	-168(%rbp), %r8
	movq	-160(%rbp), %r10
.L1733:
	xorl	%eax, %eax
	movq	%r10, -120(%rbp)
	leaq	-128(%rbp), %rsi
	movw	%ax, (%rdi,%r15,2)
	leaq	-96(%rbp), %r15
	movq	%r15, %rdi
	movq	%r8, -160(%rbp)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-128(%rbp), %rdi
	movq	-160(%rbp), %r8
	cmpq	%r8, %rdi
	je	.L1734
	call	_ZdlPv@PLT
.L1734:
	movq	%r15, %rdi
	xorl	%esi, %esi
	call	_ZNK12v8_inspector8String169toIntegerEPb@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r8
	cmpq	%r8, %rdi
	je	.L1735
	movq	%r8, -168(%rbp)
	movl	%eax, -160(%rbp)
	call	_ZdlPv@PLT
	movl	-160(%rbp), %eax
	movq	-168(%rbp), %r8
.L1735:
	leal	-1(%rax), %edx
	cmpl	$7, %edx
	jbe	.L1782
	.p2align 4,,10
	.p2align 3
.L1728:
	xorl	%eax, %eax
.L1724:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1783
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1782:
	.cfi_restore_state
	movl	%eax, (%rbx)
	subl	$5, %eax
	cmpl	$3, %eax
	jbe	.L1746
	movq	8(%r12), %rdi
	leaq	1(%r14), %rbx
	cmpq	%rdi, %rbx
	jnb	.L1728
	movq	%rdi, %r10
	movq	(%r12), %rsi
	leaq	2(%r13,%r13), %rax
	xorl	%edx, %edx
	subq	%rbx, %r10
	.p2align 4,,10
	.p2align 3
.L1738:
	cmpw	$58, (%rsi,%rax)
	je	.L1737
	addq	$1, %rdx
	addq	$2, %rax
	cmpq	%rdx, %r10
	jne	.L1738
	jmp	.L1728
.L1739:
	sarq	%rax
	movq	%rax, -160(%rbp)
	cmpq	$-1, %rax
	je	.L1728
	cmpq	$0, -136(%rbp)
	je	.L1741
	movq	%rax, %rdx
	movl	$4294967295, %ecx
	movq	%r12, %rsi
	movq	%r15, %rdi
	addq	$1, %rdx
	movq	%r8, -176(%rbp)
	movq	%r11, -168(%rbp)
	call	_ZNK12v8_inspector8String169substringEmm
	movq	-136(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-96(%rbp), %rdi
	movq	-64(%rbp), %rax
	movq	-176(%rbp), %r8
	movq	-136(%rbp), %rcx
	movq	-168(%rbp), %r11
	cmpq	%r8, %rdi
	movq	%rax, 32(%rcx)
	je	.L1741
	movq	%r8, -168(%rbp)
	movq	%r11, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %r8
	movq	-136(%rbp), %r11
.L1741:
	cmpq	$0, -144(%rbp)
	je	.L1743
	notq	%r14
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	(%r14,%r13), %rcx
	movq	%r8, -168(%rbp)
	movq	%r11, -136(%rbp)
	call	_ZNK12v8_inspector8String169substringEmm
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNK12v8_inspector8String169toIntegerEPb@PLT
	movq	-96(%rbp), %rdi
	movq	-168(%rbp), %r8
	movq	-144(%rbp), %rbx
	movq	-136(%rbp), %r11
	cmpq	%r8, %rdi
	movl	%eax, (%rbx)
	je	.L1743
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
	movq	-136(%rbp), %r11
.L1743:
	movq	-152(%rbp), %rbx
	movq	%r8, -136(%rbp)
	testq	%rbx, %rbx
	je	.L1746
	movq	-160(%rbp), %rcx
	movq	%r13, %r10
	movq	%r11, %rdx
	movq	%r12, %rsi
	notq	%r10
	movq	%r15, %rdi
	addq	%r10, %rcx
	call	_ZNK12v8_inspector8String169substringEmm
	movq	%r15, %rdi
	xorl	%esi, %esi
	call	_ZNK12v8_inspector8String169toIntegerEPb@PLT
	movq	-96(%rbp), %rdi
	movq	-136(%rbp), %r8
	movl	%eax, (%rbx)
	cmpq	%r8, %rdi
	je	.L1746
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1746:
	movl	$1, %eax
	jmp	.L1724
	.p2align 4,,10
	.p2align 3
.L1781:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-128(%rbp), %rdi
	jmp	.L1733
	.p2align 4,,10
	.p2align 3
.L1780:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r10
	ja	.L1784
	leaq	2(%rdx), %rdi
	movq	%r8, -184(%rbp)
	movq	%r10, -176(%rbp)
	movq	%rsi, -168(%rbp)
	movq	%rdx, -160(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %r10
	movq	-184(%rbp), %r8
	movq	%rax, -128(%rbp)
	movq	-168(%rbp), %rsi
	movq	%rax, %rdi
	movq	%r10, -112(%rbp)
	movq	-160(%rbp), %rdx
	jmp	.L1730
.L1737:
	movq	%rax, %r13
	sarq	%r13
	cmpq	$-1, %r13
	je	.L1728
	leaq	1(%r13), %r11
	cmpq	%rdi, %r11
	jnb	.L1728
	movq	%rdi, %rdx
	addq	$2, %rax
	xorl	%ecx, %ecx
	subq	%r11, %rdx
	.p2align 4,,10
	.p2align 3
.L1740:
	cmpw	$58, (%rsi,%rax)
	je	.L1739
	addq	$1, %rcx
	addq	$2, %rax
	cmpq	%rcx, %rdx
	jne	.L1740
	jmp	.L1728
.L1783:
	call	__stack_chk_fail@PLT
.L1784:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7753:
	.size	_ZN12v8_inspector12_GLOBAL__N_117parseBreakpointIdERKNS_8String16EPNS0_14BreakpointTypeEPS1_PiS7_, .-_ZN12v8_inspector12_GLOBAL__N_117parseBreakpointIdERKNS_8String16EPNS0_14BreakpointTypeEPS1_PiS7_
	.section	.text._ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EED2Ev,"axG",@progbits,_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EED2Ev
	.type	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EED2Ev, @function
_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EED2Ev:
.LFB9494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	movq	(%rdi), %r12
	cmpq	%r12, %rbx
	je	.L1786
	.p2align 4,,10
	.p2align 3
.L1791:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1787
	movq	(%rdi), %rax
	call	*24(%rax)
.L1787:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1788
	call	_ZdlPv@PLT
	addq	$48, %r12
	cmpq	%r12, %rbx
	jne	.L1791
.L1789:
	movq	0(%r13), %r12
.L1786:
	testq	%r12, %r12
	je	.L1785
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1788:
	.cfi_restore_state
	addq	$48, %r12
	cmpq	%r12, %rbx
	jne	.L1791
	jmp	.L1789
	.p2align 4,,10
	.p2align 3
.L1785:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9494:
	.size	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EED2Ev, .-_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EED2Ev
	.weak	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EED1Ev
	.set	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EED1Ev,_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EED2Ev
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl20setBreakpointsActiveEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl20setBreakpointsActiveEb
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl20setBreakpointsActiveEb, @function
_ZN12v8_inspector19V8DebuggerAgentImpl20setBreakpointsActiveEb:
.LFB7913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 32(%rsi)
	je	.L1805
	movq	%rsi, %rbx
	movl	%edx, %r12d
	cmpb	%dl, 409(%rsi)
	je	.L1802
	movb	%dl, 409(%rsi)
	movq	16(%rbx), %rdi
	movzbl	%dl, %esi
	call	_ZN12v8_inspector10V8Debugger20setBreakpointsActiveEb@PLT
	testb	%r12b, %r12b
	jne	.L1802
	movq	392(%rbx), %rdx
	movq	384(%rbx), %rax
	cmpq	%rax, %rdx
	je	.L1802
	movq	400(%rbx), %rcx
	pxor	%xmm0, %xmm0
	movq	%rdx, %xmm1
	leaq	-112(%rbp), %rdi
	movq	$0, 400(%rbx)
	movups	%xmm0, 384(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rcx, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EED1Ev
	movq	24(%rbx), %rax
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	movl	16(%rax), %edx
	call	_ZN12v8_inspector10V8Debugger18setPauseOnNextCallEbi@PLT
	.p2align 4,,10
	.p2align 3
.L1802:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L1797:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1806
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1805:
	.cfi_restore_state
	leaq	-80(%rbp), %r12
	leaq	_ZN12v8_inspectorL19kDebuggerNotEnabledE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1797
	call	_ZdlPv@PLT
	jmp	.L1797
.L1806:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7913:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl20setBreakpointsActiveEb, .-_ZN12v8_inspector19V8DebuggerAgentImpl20setBreakpointsActiveEb
	.section	.text._ZNSt6vectorISt4pairIiiESaIS1_EEaSERKS3_,"axG",@progbits,_ZNSt6vectorISt4pairIiiESaIS1_EEaSERKS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIiiESaIS1_EEaSERKS3_
	.type	_ZNSt6vectorISt4pairIiiESaIS1_EEaSERKS3_, @function
_ZNSt6vectorISt4pairIiiESaIS1_EEaSERKS3_:
.LFB9846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rdi, %rsi
	je	.L1808
	movq	8(%rsi), %r14
	movq	(%rsi), %rbx
	movq	(%rdi), %rdi
	movq	16(%r12), %rax
	movq	%r14, %r13
	subq	%rbx, %r13
	subq	%rdi, %rax
	movq	%r13, %rdx
	sarq	$3, %rax
	sarq	$3, %rdx
	movq	%rdx, %r9
	cmpq	%rax, %rdx
	ja	.L1873
	movq	8(%r12), %rax
	movq	%rax, %r8
	subq	%rdi, %r8
	movq	%r8, %rcx
	sarq	$3, %rcx
	movq	%rcx, %r10
	cmpq	%rcx, %rdx
	ja	.L1823
	testq	%r13, %r13
	jle	.L1824
	leaq	15(%rbx), %rax
	subq	%rdi, %rax
	cmpq	$30, %rax
	jbe	.L1859
	cmpq	$16, %r13
	jle	.L1859
	movq	%rdx, %rcx
	xorl	%eax, %eax
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1826:
	movdqu	(%rbx,%rax), %xmm0
	movups	%xmm0, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L1826
	movq	%rdx, %rcx
	andq	$-2, %rcx
	leaq	0(,%rcx,8), %rax
	addq	%rax, %rbx
	addq	%rax, %rdi
	cmpq	%rcx, %rdx
	je	.L1838
	movl	(%rbx), %eax
	movl	%eax, (%rdi)
	movl	4(%rbx), %eax
	movl	%eax, 4(%rdi)
.L1838:
	addq	(%r12), %r13
.L1822:
	movq	%r13, 8(%r12)
.L1808:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1859:
	.cfi_restore_state
	movl	(%rbx), %eax
	addq	$8, %rdi
	addq	$8, %rbx
	movl	%eax, -8(%rdi)
	movl	-4(%rbx), %eax
	movl	%eax, -4(%rdi)
	subq	$1, %r9
	jne	.L1859
	jmp	.L1838
	.p2align 4,,10
	.p2align 3
.L1873:
	xorl	%r15d, %r15d
	testq	%rdx, %rdx
	je	.L1811
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1874
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	(%r12), %rdi
	movq	%rax, %r15
.L1811:
	cmpq	%r14, %rbx
	je	.L1819
	leaq	-8(%r14), %rdx
	leaq	15(%rbx), %rcx
	movq	%rbx, %rax
	subq	%rbx, %rdx
	subq	%r15, %rcx
	shrq	$3, %rdx
	cmpq	$30, %rcx
	jbe	.L1839
	movabsq	$2305843009213693950, %rcx
	testq	%rcx, %rdx
	je	.L1839
	leaq	1(%rdx), %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1817:
	movdqu	(%rbx,%rdx), %xmm3
	movups	%xmm3, (%r15,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L1817
	movq	%rax, %rcx
	andq	$-2, %rcx
	leaq	0(,%rcx,8), %rdx
	addq	%rdx, %rbx
	addq	%r15, %rdx
	cmpq	%rcx, %rax
	je	.L1819
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L1819:
	testq	%rdi, %rdi
	je	.L1815
	call	_ZdlPv@PLT
.L1815:
	addq	%r15, %r13
	movq	%r15, (%r12)
	movq	%r13, 16(%r12)
	jmp	.L1822
	.p2align 4,,10
	.p2align 3
.L1823:
	testq	%r8, %r8
	jle	.L1829
	leaq	15(%rbx), %rax
	subq	%rdi, %rax
	cmpq	$30, %rax
	jbe	.L1860
	cmpq	$16, %r8
	jle	.L1860
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1831:
	movdqu	(%rbx,%rax), %xmm1
	movups	%xmm1, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1831
	movq	%rcx, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rax
	addq	%rax, %rbx
	addq	%rax, %rdi
	cmpq	%rdx, %rcx
	je	.L1833
	movl	(%rbx), %eax
	movl	%eax, (%rdi)
	movl	4(%rbx), %eax
	movl	%eax, 4(%rdi)
.L1833:
	movq	8(%r12), %rax
	movq	(%r12), %rdi
	movq	8(%rsi), %r14
	movq	(%rsi), %rbx
	movq	%rax, %r8
	subq	%rdi, %r8
.L1829:
	leaq	(%rbx,%r8), %rcx
	cmpq	%r14, %rcx
	je	.L1824
	leaq	-8(%r14), %rsi
	leaq	16(%rbx,%r8), %rdx
	subq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %rax
	leaq	16(%rax), %rdx
	setnb	%dil
	cmpq	%rdx, %rcx
	setnb	%dl
	orb	%dl, %dil
	je	.L1861
	movabsq	$2305843009213693950, %rdx
	testq	%rdx, %rsi
	je	.L1861
	addq	$1, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1836:
	movdqu	(%rcx,%rdx), %xmm2
	movups	%xmm2, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L1836
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rdi
	addq	%rdi, %rcx
	addq	%rdi, %rax
	cmpq	%rdx, %rsi
	je	.L1838
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	jmp	.L1838
	.p2align 4,,10
	.p2align 3
.L1860:
	movl	(%rbx), %eax
	addq	$8, %rdi
	addq	$8, %rbx
	movl	%eax, -8(%rdi)
	movl	-4(%rbx), %eax
	movl	%eax, -4(%rdi)
	subq	$1, %r10
	jne	.L1860
	jmp	.L1833
	.p2align 4,,10
	.p2align 3
.L1861:
	movl	(%rcx), %esi
	movl	4(%rcx), %edx
	addq	$8, %rcx
	addq	$8, %rax
	movl	%esi, -8(%rax)
	movl	%edx, -4(%rax)
	cmpq	%rcx, %r14
	jne	.L1861
	jmp	.L1838
	.p2align 4,,10
	.p2align 3
.L1824:
	addq	%rdi, %r13
	jmp	.L1822
	.p2align 4,,10
	.p2align 3
.L1839:
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L1816:
	movl	(%rax), %esi
	movl	4(%rax), %ecx
	addq	$8, %rax
	addq	$8, %rdx
	movl	%esi, -8(%rdx)
	movl	%ecx, -4(%rdx)
	cmpq	%rax, %r14
	jne	.L1816
	jmp	.L1819
.L1874:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9846:
	.size	_ZNSt6vectorISt4pairIiiESaIS1_EEaSERKS3_, .-_ZNSt6vectorISt4pairIiiESaIS1_EEaSERKS3_
	.section	.text._ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_,"axG",@progbits,_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	.type	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_, @function
_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_:
.LFB10249:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %r9
	movq	8(%rdi), %r8
	movq	(%rsi), %rsi
	movq	(%rdi), %rcx
	cmpq	%r9, %r8
	movq	%r9, %rdx
	cmovbe	%r8, %rdx
	testq	%rdx, %rdx
	je	.L1876
	xorl	%eax, %eax
	jmp	.L1878
	.p2align 4,,10
	.p2align 3
.L1888:
	jb	.L1881
	addq	$1, %rax
	cmpq	%rdx, %rax
	je	.L1876
.L1878:
	movzwl	(%rcx,%rax,2), %edi
	cmpw	%di, (%rsi,%rax,2)
	jbe	.L1888
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1876:
	subq	%r9, %r8
	movl	$2147483647, %eax
	cmpq	$2147483647, %r8
	jg	.L1889
	cmpq	$-2147483648, %r8
	movl	$-2147483648, %eax
	cmovge	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1889:
	ret
	.p2align 4,,10
	.p2align 3
.L1881:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE10249:
	.size	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_, .-_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	.section	.rodata._ZN12v8_inspectorL7matchesEPNS_15V8InspectorImplERKNS_16V8DebuggerScriptENS_12_GLOBAL__N_114BreakpointTypeERKNS_8String16E.str1.1,"aMS",@progbits,1
.LC7:
	.string	"unreachable code"
	.section	.text._ZN12v8_inspectorL7matchesEPNS_15V8InspectorImplERKNS_16V8DebuggerScriptENS_12_GLOBAL__N_114BreakpointTypeERKNS_8String16E,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspectorL7matchesEPNS_15V8InspectorImplERKNS_16V8DebuggerScriptENS_12_GLOBAL__N_114BreakpointTypeERKNS_8String16E, @function
_ZN12v8_inspectorL7matchesEPNS_15V8InspectorImplERKNS_16V8DebuggerScriptENS_12_GLOBAL__N_114BreakpointTypeERKNS_8String16E:
.LFB7915:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$2, %edx
	je	.L1891
	cmpl	$3, %edx
	je	.L1892
	cmpl	$1, %edx
	je	.L1902
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1891:
	leaq	-96(%rbp), %r14
	xorl	%r8d, %r8d
	movq	%rdi, %rsi
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN12v8_inspector7V8RegexC1EPNS_15V8InspectorImplERKNS_8String16Ebb@PLT
	leaq	48(%r12), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZNK12v8_inspector7V8Regex5matchERKNS_8String16EiPi@PLT
	movq	-80(%rbp), %rdi
	cmpl	$-1, %eax
	leaq	-64(%rbp), %rax
	setne	%r12b
	cmpq	%rax, %rdi
	je	.L1895
	call	_ZdlPv@PLT
.L1895:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1890
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1890:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1903
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1902:
	.cfi_restore_state
	leaq	48(%rsi), %rdi
	movq	%rcx, %rsi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	testl	%eax, %eax
	sete	%r12b
	jmp	.L1890
	.p2align 4,,10
	.p2align 3
.L1892:
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	call	*32(%rax)
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	testl	%eax, %eax
	sete	%r12b
	jmp	.L1890
.L1903:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7915:
	.size	_ZN12v8_inspectorL7matchesEPNS_15V8InspectorImplERKNS_16V8DebuggerScriptENS_12_GLOBAL__N_114BreakpointTypeERKNS_8String16E, .-_ZN12v8_inspectorL7matchesEPNS_15V8InspectorImplERKNS_16V8DebuggerScriptENS_12_GLOBAL__N_114BreakpointTypeERKNS_8String16E
	.section	.rodata._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger5ScopeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_.str1.1,"aMS",@progbits,1
.LC8:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger5ScopeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger5ScopeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger5ScopeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger5ScopeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger5ScopeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB10990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdx
	movq	8(%rdi), %rax
	movq	%rdi, -88(%rbp)
	movq	%rax, -80(%rbp)
	subq	%rdx, %rax
	movq	%rdx, -56(%rbp)
	sarq	$3, %rax
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	je	.L1950
	movq	%rsi, %rcx
	movq	%rsi, %r12
	movq	%rsi, %r13
	subq	-56(%rbp), %rcx
	testq	%rax, %rax
	je	.L1928
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1951
.L1906:
	movq	%rbx, %rdi
	movq	%rcx, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%rax, -64(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -72(%rbp)
	leaq	8(%rax), %rbx
.L1927:
	movq	(%r15), %rax
	movq	-64(%rbp), %rsi
	movq	$0, (%r15)
	movq	-56(%rbp), %r15
	movq	%rax, (%rsi,%rcx)
	cmpq	%r15, %r12
	je	.L1908
	movq	%r13, -96(%rbp)
	movq	%rsi, %rbx
	jmp	.L1921
	.p2align 4,,10
	.p2align 3
.L1953:
	movq	112(%r14), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r13, %r13
	je	.L1911
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1912
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1913
	call	_ZdlPv@PLT
.L1913:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1911:
	movq	104(%r14), %r13
	testq	%r13, %r13
	je	.L1914
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1915
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1916
	call	_ZdlPv@PLT
.L1916:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1914:
	movq	64(%r14), %rdi
	leaq	80(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1917
	call	_ZdlPv@PLT
.L1917:
	movq	48(%r14), %r13
	testq	%r13, %r13
	je	.L1918
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1919
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1918:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1920
	call	_ZdlPv@PLT
.L1920:
	movl	$120, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1909:
	addq	$8, %r15
	addq	$8, %rbx
	cmpq	%r15, %r12
	je	.L1952
.L1921:
	movq	(%r15), %rax
	movq	$0, (%r15)
	movq	%rax, (%rbx)
	movq	(%r15), %r14
	testq	%r14, %r14
	je	.L1909
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1953
	addq	$8, %r15
	movq	%r14, %rdi
	addq	$8, %rbx
	call	*%rax
	cmpq	%r15, %r12
	jne	.L1921
	.p2align 4,,10
	.p2align 3
.L1952:
	movq	-64(%rbp), %rbx
	movq	%r12, %rax
	movq	-96(%rbp), %r13
	subq	-56(%rbp), %rax
	leaq	8(%rbx,%rax), %rbx
.L1908:
	movq	-80(%rbp), %rax
	cmpq	%rax, %r12
	je	.L1922
	subq	%r12, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L1930
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1924:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1924
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r13
	leaq	(%rbx,%r13), %rdx
	addq	%r12, %r13
	cmpq	%rax, %rcx
	je	.L1925
.L1923:
	movq	0(%r13), %rax
	movq	%rax, (%rdx)
.L1925:
	leaq	8(%rbx,%rsi), %rbx
.L1922:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L1926
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1926:
	movq	-64(%rbp), %xmm0
	movq	-88(%rbp), %rax
	movq	%rbx, %xmm2
	movq	-72(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1912:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1911
	.p2align 4,,10
	.p2align 3
.L1915:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1914
	.p2align 4,,10
	.p2align 3
.L1919:
	call	*%rax
	jmp	.L1918
	.p2align 4,,10
	.p2align 3
.L1951:
	testq	%rsi, %rsi
	jne	.L1907
	movq	$0, -72(%rbp)
	movl	$8, %ebx
	movq	$0, -64(%rbp)
	jmp	.L1927
	.p2align 4,,10
	.p2align 3
.L1928:
	movl	$8, %ebx
	jmp	.L1906
.L1930:
	movq	%rbx, %rdx
	jmp	.L1923
.L1907:
	cmpq	%rdx, %rsi
	cmovbe	%rsi, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L1906
.L1950:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10990:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger5ScopeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger5ScopeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger8LocationESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger8LocationESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger8LocationESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger8LocationESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger8LocationESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB11145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$1152921504606846975, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -88(%rbp)
	movq	%rax, -80(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L1982
	movq	%rsi, %r13
	movq	%rsi, %rbx
	subq	%r12, %rsi
	testq	%rax, %rax
	je	.L1969
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1983
.L1956:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, -64(%rbp)
	addq	%rax, %r14
	movq	%r14, -72(%rbp)
	leaq	8(%rax), %r14
.L1968:
	movq	(%r15), %rax
	movq	-64(%rbp), %rcx
	movq	$0, (%r15)
	movq	%rax, (%rcx,%rsi)
	cmpq	%r12, %r13
	je	.L1958
	movq	%rcx, %r14
	movq	%r12, %r15
	jmp	.L1962
	.p2align 4,,10
	.p2align 3
.L1985:
	movq	8(%r8), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	leaq	24(%r8), %rsi
	movq	%rax, (%r8)
	cmpq	%rsi, %rdi
	je	.L1961
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1961:
	movl	$64, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1959:
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	je	.L1984
.L1962:
	movq	(%r15), %rsi
	movq	$0, (%r15)
	movq	%rsi, (%r14)
	movq	(%r15), %r8
	testq	%r8, %r8
	je	.L1959
	movq	(%r8), %rsi
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rax
	movq	24(%rsi), %rsi
	cmpq	%rax, %rsi
	je	.L1985
	addq	$8, %r15
	movq	%r8, %rdi
	addq	$8, %r14
	call	*%rsi
	cmpq	%r15, %r13
	jne	.L1962
	.p2align 4,,10
	.p2align 3
.L1984:
	movq	-64(%rbp), %rcx
	movq	%r13, %rax
	subq	%r12, %rax
	leaq	8(%rcx,%rax), %r14
.L1958:
	movq	-80(%rbp), %rax
	cmpq	%rax, %r13
	je	.L1963
	subq	%r13, %rax
	leaq	-8(%rax), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	addq	$1, %rsi
	testq	%rdi, %rdi
	je	.L1971
	movq	%rsi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1965:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1965
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rbx
	leaq	(%r14,%rbx), %rdx
	addq	%r13, %rbx
	cmpq	%rax, %rsi
	je	.L1966
.L1964:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L1966:
	leaq	8(%r14,%rdi), %r14
.L1963:
	testq	%r12, %r12
	je	.L1967
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1967:
	movq	-64(%rbp), %xmm0
	movq	-88(%rbp), %rax
	movq	%r14, %xmm2
	movq	-72(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1983:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L1957
	movq	$0, -72(%rbp)
	movl	$8, %r14d
	movq	$0, -64(%rbp)
	jmp	.L1968
	.p2align 4,,10
	.p2align 3
.L1969:
	movl	$8, %r14d
	jmp	.L1956
.L1971:
	movq	%r14, %rdx
	jmp	.L1964
.L1957:
	cmpq	%rdx, %rdi
	cmovbe	%rdi, %rdx
	leaq	0(,%rdx,8), %r14
	jmp	.L1956
.L1982:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11145:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger8LocationESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger8LocationESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_,"axG",@progbits,_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	.type	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_, @function
_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_:
.LFB11148:
	.cfi_startproc
	endbr64
	movq	32(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.L1987
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %r8
	cmpq	%r8, %rax
	je	.L1990
	.p2align 4,,10
	.p2align 3
.L1989:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
	movsbq	-2(%rax), %rdx
	addq	%rdx, %rcx
	movq	%rcx, 32(%rsi)
	cmpq	%rax, %r8
	jne	.L1989
	testq	%rcx, %rcx
	je	.L1990
.L1987:
	movq	8(%rdi), %r9
	movq	%rcx, %rax
	xorl	%edx, %edx
	divq	%r9
	movq	(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2017
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2147483648, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rax), %rdi
	movabsq	$-2147483649, %rbx
	movq	72(%rdi), %r8
.L1996:
	cmpq	%rcx, %r8
	je	.L2020
.L1992:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1991
	movq	72(%rdi), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%r9
	cmpq	%rdx, %r10
	je	.L1996
.L1991:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2020:
	.cfi_restore_state
	movq	8(%rsi), %rdx
	movq	16(%rdi), %r14
	movq	8(%rdi), %r12
	movq	(%rsi), %r13
	cmpq	%r14, %rdx
	movq	%r14, %r8
	cmovbe	%rdx, %r8
	testq	%r8, %r8
	je	.L1993
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1994:
	movzwl	(%r12,%rax,2), %r15d
	cmpw	%r15w, 0(%r13,%rax,2)
	jne	.L1992
	addq	$1, %rax
	cmpq	%r8, %rax
	jne	.L1994
.L1993:
	subq	%r14, %rdx
	cmpq	%r11, %rdx
	jge	.L1992
	cmpq	%rbx, %rdx
	jle	.L1992
	testl	%edx, %edx
	jne	.L1992
	popq	%rbx
	movq	%rdi, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1990:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movq	$1, 32(%rsi)
	movl	$1, %ecx
	jmp	.L1987
	.p2align 4,,10
	.p2align 3
.L2017:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE11148:
	.size	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_, .-_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl20removeBreakpointImplERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl20removeBreakpointImplERKNS_8String16E
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl20removeBreakpointImplERKNS_8String16E, @function
_ZN12v8_inspector19V8DebuggerAgentImpl20removeBreakpointImplERKNS_8String16E:
.LFB7926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$120, %rdi
	subq	$56, %rsp
	movq	%rsi, -72(%rbp)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L2021
	movq	56(%rax), %rcx
	movq	48(%rax), %r15
	leaq	192(%rbx), %rax
	movq	%rax, -80(%rbp)
	movq	%rcx, -56(%rbp)
	cmpq	%r15, %rcx
	je	.L2034
	.p2align 4,,10
	.p2align 3
.L2033:
	movq	56(%rbx), %rdi
	movl	(%r15), %esi
	call	_ZN2v85debug16RemoveBreakpointEPNS_7IsolateEi@PLT
	movslq	(%r15), %rax
	xorl	%edx, %edx
	movq	184(%rbx), %rcx
	movq	176(%rbx), %r13
	movq	%rax, %r11
	divq	%rcx
	leaq	0(,%rdx,8), %rax
	movq	%rdx, %r10
	leaq	0(%r13,%rax), %r14
	movq	%rax, -64(%rbp)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2026
	movq	(%rdi), %r12
	movq	%rdi, %r9
	movl	8(%r12), %r8d
	jmp	.L2028
	.p2align 4,,10
	.p2align 3
.L2091:
	testq	%rsi, %rsi
	je	.L2026
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	movq	%r12, %r9
	movq	%rax, %r8
	divq	%rcx
	cmpq	%rdx, %r10
	jne	.L2026
	movq	%rsi, %r12
.L2028:
	movq	(%r12), %rsi
	cmpl	%r8d, %r11d
	jne	.L2091
	cmpq	%r9, %rdi
	je	.L2092
	testq	%rsi, %rsi
	je	.L2030
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r10
	je	.L2030
	movq	%r9, 0(%r13,%rdx,8)
	movq	(%r12), %rsi
.L2030:
	movq	%rsi, (%r9)
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2032
	call	_ZdlPv@PLT
.L2032:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 200(%rbx)
.L2026:
	addq	$4, %r15
	cmpq	%r15, -56(%rbp)
	jne	.L2033
.L2034:
	movq	-72(%rbp), %rax
	movq	32(%rax), %r12
	testq	%r12, %r12
	je	.L2024
.L2025:
	movq	128(%rbx), %r15
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%r15
	movq	120(%rbx), %rax
	movq	%rax, -80(%rbp)
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rax
	movq	%rcx, -96(%rbp)
	movq	(%rax), %r11
	movq	%rax, -88(%rbp)
	testq	%r11, %r11
	je	.L2021
	movq	(%r11), %r14
	movq	%rbx, -64(%rbp)
	movq	%rdx, %r13
	movq	%r11, %rbx
	movq	%r11, -56(%rbp)
	movq	72(%r14), %rsi
	cmpq	%r12, %rsi
	je	.L2093
	.p2align 4,,10
	.p2align 3
.L2039:
	movq	(%r14), %rcx
	testq	%rcx, %rcx
	je	.L2021
	movq	72(%rcx), %rsi
	xorl	%edx, %edx
	movq	%r14, %rbx
	movq	%rsi, %rax
	divq	%r15
	cmpq	%rdx, %r13
	jne	.L2021
	movq	%rcx, %r14
	cmpq	%r12, %rsi
	jne	.L2039
.L2093:
	movq	-72(%rbp), %rdi
	leaq	8(%r14), %rsi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	testl	%eax, %eax
	jne	.L2039
	movq	-56(%rbp), %r11
	movq	%r13, %r10
	movq	%rbx, %r13
	movq	(%r14), %rsi
	movq	-64(%rbp), %rbx
	cmpq	%r13, %r11
	je	.L2094
	testq	%rsi, %rsi
	je	.L2043
	movq	72(%rsi), %rax
	xorl	%edx, %edx
	divq	%r15
	cmpq	%rdx, %r10
	je	.L2043
	movq	-80(%rbp), %rax
	movq	%r13, (%rax,%rdx,8)
	movq	(%r14), %rsi
.L2043:
	movq	%rsi, 0(%r13)
	movq	48(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2045
	call	_ZdlPv@PLT
.L2045:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2046
	call	_ZdlPv@PLT
.L2046:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	subq	$1, 144(%rbx)
.L2021:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2092:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L2051
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r10
	je	.L2030
	movq	%r9, 0(%r13,%rdx,8)
	movq	-64(%rbp), %r14
	addq	176(%rbx), %r14
	movq	(%r14), %rax
	cmpq	-80(%rbp), %rax
	je	.L2095
.L2031:
	movq	$0, (%r14)
	movq	(%r12), %rsi
	jmp	.L2030
	.p2align 4,,10
	.p2align 3
.L2024:
	movq	%rax, %rdx
	movq	(%rax), %rax
	movq	-72(%rbp), %rsi
	movq	8(%rdx), %rdx
	leaq	(%rax,%rdx,2), %rcx
	cmpq	%rcx, %rax
	je	.L2037
	.p2align 4,,10
	.p2align 3
.L2036:
	movq	%r12, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%r12, %rdx
	movsbq	-2(%rax), %r12
	addq	%rdx, %r12
	movq	%r12, 32(%rsi)
	cmpq	%rax, %rcx
	jne	.L2036
	testq	%r12, %r12
	jne	.L2025
.L2037:
	movq	-72(%rbp), %rax
	movl	$1, %r12d
	movq	$1, 32(%rax)
	jmp	.L2025
	.p2align 4,,10
	.p2align 3
.L2051:
	movq	%r9, %rax
	cmpq	-80(%rbp), %rax
	jne	.L2031
.L2095:
	movq	%rsi, 192(%rbx)
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2094:
	testq	%rsi, %rsi
	je	.L2052
	movq	72(%rsi), %rax
	xorl	%edx, %edx
	divq	%r15
	cmpq	%rdx, %r10
	je	.L2043
	movq	-80(%rbp), %rax
	movq	%r13, (%rax,%rdx,8)
	movq	-96(%rbp), %rax
	addq	120(%rbx), %rax
	movq	%rax, -88(%rbp)
	movq	(%rax), %rax
.L2042:
	leaq	136(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L2096
.L2044:
	movq	-88(%rbp), %rax
	movq	$0, (%rax)
	movq	(%r14), %rsi
	jmp	.L2043
.L2052:
	movq	%r13, %rax
	jmp	.L2042
.L2096:
	movq	%rsi, 136(%rbx)
	jmp	.L2044
	.cfi_endproc
.LFE7926:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl20removeBreakpointImplERKNS_8String16E, .-_ZN12v8_inspector19V8DebuggerAgentImpl20removeBreakpointImplERKNS_8String16E
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl16removeBreakpointERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl16removeBreakpointERKNS_8String16E
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl16removeBreakpointERKNS_8String16E, @function
_ZN12v8_inspector19V8DebuggerAgentImpl16removeBreakpointERKNS_8String16E:
.LFB7925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 32(%rsi)
	jne	.L2098
	leaq	-96(%rbp), %r13
	leaq	_ZN12v8_inspectorL19kDebuggerNotEnabledE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2097
.L2128:
	call	_ZdlPv@PLT
.L2097:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2134
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2098:
	.cfi_restore_state
	leaq	-144(%rbp), %r9
	movq	%rdx, %r14
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r9, %rdx
	movq	%rsi, %r13
	movq	%r14, %rdi
	leaq	-128(%rbp), %rbx
	movw	%ax, -128(%rbp)
	leaq	-148(%rbp), %rsi
	movq	%r9, -168(%rbp)
	movq	%rbx, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -112(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_117parseBreakpointIdERKNS_8String16EPNS0_14BreakpointTypeEPS1_PiS7_
	movq	-168(%rbp), %r9
	testb	%al, %al
	je	.L2127
	movl	-148(%rbp), %eax
	movq	40(%r13), %r8
	cmpl	$3, %eax
	je	.L2103
	jg	.L2104
	cmpl	$1, %eax
	je	.L2105
	cmpl	$2, %eax
	jne	.L2126
	movq	%r8, -168(%rbp)
	leaq	-96(%rbp), %r15
	leaq	_ZN12v8_inspector18DebuggerAgentStateL18breakpointsByRegexE(%rip), %rsi
.L2130:
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-168(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	cmpq	%rcx, %rdi
	je	.L2111
	movq	%rax, -176(%rbp)
	call	_ZdlPv@PLT
	movq	-176(%rbp), %rax
.L2111:
	testq	%rax, %rax
	je	.L2125
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue6removeERKNS_8String16E@PLT
.L2125:
	movq	40(%r13), %r8
.L2107:
	leaq	_ZN12v8_inspector18DebuggerAgentStateL15breakpointHintsE(%rip), %rsi
	movq	%r15, %rdi
	movq	%r8, -176(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-176(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-168(%rbp), %rdi
	je	.L2117
	movq	%rax, -168(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %rax
.L2117:
	testq	%rax, %rax
	je	.L2118
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue6removeERKNS_8String16E@PLT
.L2118:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl20removeBreakpointImplERKNS_8String16E
.L2127:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-144(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L2128
	jmp	.L2097
	.p2align 4,,10
	.p2align 3
.L2104:
	cmpl	$8, %eax
	jne	.L2126
	movq	%r8, -168(%rbp)
	leaq	-96(%rbp), %r15
	leaq	_ZN12v8_inspector18DebuggerAgentStateL26instrumentationBreakpointsE(%rip), %rsi
	jmp	.L2130
	.p2align 4,,10
	.p2align 3
.L2126:
	leaq	-80(%rbp), %rax
	leaq	-96(%rbp), %r15
	movq	%rax, -168(%rbp)
	jmp	.L2107
	.p2align 4,,10
	.p2align 3
.L2105:
	movq	%r9, -176(%rbp)
	leaq	-96(%rbp), %r15
	leaq	_ZN12v8_inspector18DebuggerAgentStateL16breakpointsByUrlE(%rip), %rsi
	movq	%r8, -168(%rbp)
.L2133:
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-168(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rcx
	movq	-176(%rbp), %r9
	movq	%rcx, -168(%rbp)
	cmpq	%rcx, %rdi
	je	.L2112
	movq	%r9, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZdlPv@PLT
	movq	-184(%rbp), %r9
	movq	-176(%rbp), %rax
.L2112:
	testq	%rax, %rax
	je	.L2125
	movq	%r9, %rsi
	movq	%rax, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	jmp	.L2111
	.p2align 4,,10
	.p2align 3
.L2103:
	movq	%r9, -176(%rbp)
	leaq	-96(%rbp), %r15
	leaq	_ZN12v8_inspector18DebuggerAgentStateL23breakpointsByScriptHashE(%rip), %rsi
	movq	%r8, -168(%rbp)
	jmp	.L2133
.L2134:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7925:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl16removeBreakpointERKNS_8String16E, .-_ZN12v8_inspector19V8DebuggerAgentImpl16removeBreakpointERKNS_8String16E
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl19removeBreakpointForEN2v85LocalINS1_8FunctionEEENS0_16BreakpointSourceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl19removeBreakpointForEN2v85LocalINS1_8FunctionEEENS0_16BreakpointSourceE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl19removeBreakpointForEN2v85LocalINS1_8FunctionEEENS0_16BreakpointSourceE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl19removeBreakpointForEN2v85LocalINS1_8FunctionEEENS0_16BreakpointSourceE:
.LFB8118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %edx
	setne	%r15b
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	addl	$5, %r15d
	movq	%r12, %rdi
	movl	%r15d, %esi
	call	_ZN12v8_inspector15String16Builder12appendNumberEi@PLT
	movl	$58, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	%r14, %rdi
	leaq	-80(%rbp), %r14
	call	_ZN2v85debug14GetDebuggingIdENS_5LocalINS_8FunctionEEE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector15String16Builder12appendNumberEi@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2137
	call	_ZdlPv@PLT
.L2137:
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl20removeBreakpointImplERKNS_8String16E
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2135
	call	_ZdlPv@PLT
.L2135:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2145
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2145:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8118:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl19removeBreakpointForEN2v85LocalINS1_8FunctionEEENS0_16BreakpointSourceE, .-_ZN12v8_inspector19V8DebuggerAgentImpl19removeBreakpointForEN2v85LocalINS1_8FunctionEEENS0_16BreakpointSourceE
	.section	.text._ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_,"axG",@progbits,_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	.type	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_, @function
_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_:
.LFB11161:
	.cfi_startproc
	endbr64
	movq	32(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.L2147
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %r8
	cmpq	%r8, %rax
	je	.L2150
	.p2align 4,,10
	.p2align 3
.L2149:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
	movsbq	-2(%rax), %rdx
	addq	%rdx, %rcx
	movq	%rcx, 32(%rsi)
	cmpq	%rax, %r8
	jne	.L2149
	testq	%rcx, %rcx
	je	.L2150
.L2147:
	movq	8(%rdi), %r9
	movq	%rcx, %rax
	xorl	%edx, %edx
	divq	%r9
	movq	(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2177
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2147483648, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rax), %rdi
	movabsq	$-2147483649, %rbx
	movq	56(%rdi), %r8
.L2156:
	cmpq	%rcx, %r8
	je	.L2180
.L2152:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2151
	movq	56(%rdi), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%r9
	cmpq	%rdx, %r10
	je	.L2156
.L2151:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2180:
	.cfi_restore_state
	movq	8(%rsi), %rdx
	movq	16(%rdi), %r14
	movq	8(%rdi), %r12
	movq	(%rsi), %r13
	cmpq	%r14, %rdx
	movq	%r14, %r8
	cmovbe	%rdx, %r8
	testq	%r8, %r8
	je	.L2153
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2154:
	movzwl	(%r12,%rax,2), %r15d
	cmpw	%r15w, 0(%r13,%rax,2)
	jne	.L2152
	addq	$1, %rax
	cmpq	%r8, %rax
	jne	.L2154
.L2153:
	subq	%r14, %rdx
	cmpq	%r11, %rdx
	jge	.L2152
	cmpq	%rbx, %rdx
	jle	.L2152
	testl	%edx, %edx
	jne	.L2152
	popq	%rbx
	movq	%rdi, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2150:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movq	$1, 32(%rsi)
	movl	$1, %ecx
	jmp	.L2147
	.p2align 4,,10
	.p2align 3
.L2177:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE11161:
	.size	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_, .-_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl18continueToLocationESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"Cannot continue to specified location"
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl18continueToLocationESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl18continueToLocationESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl18continueToLocationESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl18continueToLocationESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEE:
.LFB7940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 32(%rsi)
	jne	.L2182
	leaq	-96(%rbp), %r13
	leaq	_ZN12v8_inspectorL19kDebuggerNotEnabledE(%rip), %rsi
.L2205:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2181
.L2201:
	call	_ZdlPv@PLT
.L2181:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2206
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2182:
	.cfi_restore_state
	movq	24(%rsi), %rax
	movq	%rsi, %r15
	movq	%rdx, %rbx
	movq	%rcx, %r13
	movq	16(%r15), %rdi
	movl	16(%rax), %esi
	call	_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi@PLT
	testb	%al, %al
	jne	.L2185
	leaq	-96(%rbp), %r13
	leaq	_ZN12v8_inspectorL18kDebuggerNotPausedE(%rip), %rsi
	jmp	.L2205
	.p2align 4,,10
	.p2align 3
.L2185:
	movq	(%rbx), %rax
	leaq	-96(%rbp), %r14
	leaq	-80(%rbp), %rcx
	movq	%r14, %rdi
	movq	%rcx, -152(%rbp)
	movq	8(%rax), %rsi
	movq	16(%rax), %rdx
	movq	%rcx, -96(%rbp)
	movq	%rax, -160(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-160(%rbp), %rax
	leaq	64(%r15), %rdi
	movq	%r14, %rsi
	movq	40(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	-96(%rbp), %rdi
	cmpq	-152(%rbp), %rdi
	je	.L2187
	movq	%rax, -160(%rbp)
	call	_ZdlPv@PLT
	movq	-160(%rbp), %rax
.L2187:
	testq	%rax, %rax
	je	.L2203
	movq	48(%rax), %rcx
	movq	8(%r15), %rdi
	movl	92(%rcx), %esi
	movq	%rcx, -160(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEi@PLT
	movq	-160(%rbp), %rcx
	testq	%rax, %rax
	je	.L2203
	movq	56(%r15), %rsi
	movq	%rax, -160(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rcx, -184(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-160(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK12v8_inspector16InspectedContext7contextEv@PLT
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%r15), %r10
	movq	_ZN12v8_inspector8protocol8Debugger18ContinueToLocation20TargetCallFramesEnum3AnyE(%rip), %rsi
	movq	%r14, %rdi
	movq	%r10, -176(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	(%rbx), %rax
	cmpb	$0, 0(%r13)
	movq	%r12, %rdi
	movq	$0, (%rbx)
	movq	-176(%rbp), %r10
	leaq	8(%r13), %r9
	leaq	-136(%rbp), %r8
	movq	%rax, -136(%rbp)
	movq	24(%r15), %rax
	cmove	%r14, %r9
	movq	-184(%rbp), %rcx
	movq	%r10, %rsi
	movl	16(%rax), %edx
	call	_ZN12v8_inspector10V8Debugger18continueToLocationEiPNS_16V8DebuggerScriptESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS6_EERKNS_8String16E@PLT
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2193
	movq	(%rdi), %rax
	call	*24(%rax)
.L2193:
	movq	-96(%rbp), %rdi
	cmpq	-152(%rbp), %rdi
	je	.L2194
	call	_ZdlPv@PLT
.L2194:
	movq	-160(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-168(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L2181
	.p2align 4,,10
	.p2align 3
.L2203:
	leaq	.LC9(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-152(%rbp), %rdi
	jne	.L2201
	jmp	.L2181
.L2206:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7940:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl18continueToLocationESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEE, .-_ZN12v8_inspector19V8DebuggerAgentImpl18continueToLocationESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEE
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl20isFunctionBlackboxedERKNS_8String16ERKN2v85debug8LocationES8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl20isFunctionBlackboxedERKNS_8String16ERKN2v85debug8LocationES8_
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl20isFunctionBlackboxedERKNS_8String16ERKN2v85debug8LocationES8_, @function
_ZN12v8_inspector19V8DebuggerAgentImpl20isFunctionBlackboxedERKNS_8String16ERKN2v85debug8LocationES8_:
.LFB7951:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	addq	$64, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L2214
	movq	416(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2213
	movq	48(%rax), %rsi
	cmpq	$0, 56(%rsi)
	jne	.L2248
.L2213:
	movq	32(%r14), %rbx
	testq	%rbx, %rbx
	je	.L2211
.L2212:
	movq	432(%r13), %r8
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	424(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2221
	movq	(%rax), %r13
	movq	72(%r13), %rcx
.L2222:
	cmpq	%rbx, %rcx
	je	.L2249
.L2219:
	movq	0(%r13), %r13
	testq	%r13, %r13
	je	.L2221
	movq	72(%r13), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r9
	je	.L2222
.L2221:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2249:
	.cfi_restore_state
	leaq	8(%r13), %rsi
	movq	%r14, %rdi
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	testl	%eax, %eax
	jne	.L2219
	movq	%r15, %rdi
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	movq	%r15, %rdi
	movl	%eax, %r14d
	call	_ZNK2v85debug8Location13GetLineNumberEv@PLT
	movq	48(%r13), %rbx
	movq	56(%r13), %rdx
	subq	%rbx, %rdx
	sarq	$3, %rdx
.L2223:
	testq	%rdx, %rdx
	jle	.L2224
.L2250:
	movq	%rdx, %rcx
	sarq	%rcx
	leaq	(%rbx,%rcx,8), %rsi
	cmpl	(%rsi), %eax
	jne	.L2245
	cmpl	4(%rsi), %r14d
.L2245:
	jle	.L2227
	subq	%rcx, %rdx
	leaq	8(%rsi), %rbx
	subq	$1, %rdx
	testq	%rdx, %rdx
	jg	.L2250
.L2224:
	movq	%r12, %rdi
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZNK2v85debug8Location13GetLineNumberEv@PLT
	movq	56(%r13), %rdx
	movq	%rbx, %rdi
	subq	%rbx, %rdx
	sarq	$3, %rdx
	.p2align 4,,10
	.p2align 3
.L2229:
	testq	%rdx, %rdx
	jle	.L2230
.L2251:
	movq	%rdx, %rcx
	sarq	%rcx
	leaq	(%rdi,%rcx,8), %rsi
	cmpl	(%rsi), %eax
	jne	.L2247
	cmpl	4(%rsi), %r14d
.L2247:
	jle	.L2233
	subq	%rcx, %rdx
	leaq	8(%rsi), %rdi
	subq	$1, %rdx
	testq	%rdx, %rdx
	jg	.L2251
.L2230:
	cmpq	%rdi, %rbx
	jne	.L2221
	movq	%rbx, %rax
	subq	48(%r13), %rax
	addq	$24, %rsp
	sarq	$3, %rax
	popq	%rbx
	popq	%r12
	andl	$1, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2227:
	.cfi_restore_state
	movq	%rcx, %rdx
	jmp	.L2223
	.p2align 4,,10
	.p2align 3
.L2211:
	movq	(%r14), %rax
	movq	8(%r14), %rdx
	leaq	(%rax,%rdx,2), %rcx
	cmpq	%rcx, %rax
	je	.L2217
	.p2align 4,,10
	.p2align 3
.L2216:
	movq	%rbx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rbx
	movsbq	-2(%rax), %rdx
	addq	%rdx, %rbx
	movq	%rbx, 32(%r14)
	cmpq	%rax, %rcx
	jne	.L2216
	testq	%rbx, %rbx
	jne	.L2212
.L2217:
	movq	$1, 32(%r14)
	movl	$1, %ebx
	jmp	.L2212
	.p2align 4,,10
	.p2align 3
.L2248:
	addq	$48, %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZNK12v8_inspector7V8Regex5matchERKNS_8String16EiPi@PLT
	cmpl	$-1, %eax
	je	.L2213
.L2214:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2233:
	.cfi_restore_state
	movq	%rcx, %rdx
	jmp	.L2229
	.cfi_endproc
.LFE7951:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl20isFunctionBlackboxedERKNS_8String16ERKN2v85debug8LocationES8_, .-_ZN12v8_inspector19V8DebuggerAgentImpl20isFunctionBlackboxedERKNS_8String16ERKN2v85debug8LocationES8_
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger13BreakLocationESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger13BreakLocationESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger13BreakLocationESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger13BreakLocationESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger13BreakLocationESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB11206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$1152921504606846975, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	8(%rdi), %rax
	movq	%rdi, -88(%rbp)
	movq	%rax, -80(%rbp)
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rcx, -56(%rbp)
	cmpq	%rdx, %rax
	je	.L2281
	movq	%rsi, %r13
	movq	%rsi, %rbx
	subq	-56(%rbp), %rsi
	testq	%rax, %rax
	je	.L2268
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L2282
.L2254:
	movq	%r14, %rdi
	movq	%rsi, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, -64(%rbp)
	addq	%rax, %r14
	movq	%r14, -72(%rbp)
	leaq	8(%rax), %r14
.L2267:
	movq	(%r15), %rax
	movq	-64(%rbp), %rcx
	movq	$0, (%r15)
	movq	-56(%rbp), %r15
	movq	%rax, (%rcx,%rsi)
	cmpq	%r15, %r13
	je	.L2256
	movq	%rcx, %r14
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2284:
	movq	72(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE(%rip), %rax
	leaq	88(%r12), %rsi
	movq	%rax, (%r12)
	cmpq	%rsi, %rdi
	je	.L2259
	call	_ZdlPv@PLT
.L2259:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rsi
	cmpq	%rsi, %rdi
	je	.L2260
	call	_ZdlPv@PLT
.L2260:
	movl	$112, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2257:
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	je	.L2283
.L2261:
	movq	(%r15), %rsi
	movq	$0, (%r15)
	movq	%rsi, (%r14)
	movq	(%r15), %r12
	testq	%r12, %r12
	je	.L2257
	movq	(%r12), %rsi
	leaq	_ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev(%rip), %rax
	movq	24(%rsi), %rsi
	cmpq	%rax, %rsi
	je	.L2284
	addq	$8, %r15
	movq	%r12, %rdi
	addq	$8, %r14
	call	*%rsi
	cmpq	%r15, %r13
	jne	.L2261
	.p2align 4,,10
	.p2align 3
.L2283:
	movq	-64(%rbp), %rcx
	movq	%r13, %rax
	subq	-56(%rbp), %rax
	leaq	8(%rcx,%rax), %r14
.L2256:
	movq	-80(%rbp), %rax
	cmpq	%rax, %r13
	je	.L2262
	subq	%r13, %rax
	leaq	-8(%rax), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	addq	$1, %rsi
	testq	%rdi, %rdi
	je	.L2270
	movq	%rsi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L2264:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L2264
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rbx
	leaq	(%r14,%rbx), %rdx
	addq	%r13, %rbx
	cmpq	%rax, %rsi
	je	.L2265
.L2263:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L2265:
	leaq	8(%r14,%rdi), %r14
.L2262:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L2266
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2266:
	movq	-64(%rbp), %xmm0
	movq	-88(%rbp), %rax
	movq	%r14, %xmm2
	movq	-72(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2282:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L2255
	movq	$0, -72(%rbp)
	movl	$8, %r14d
	movq	$0, -64(%rbp)
	jmp	.L2267
	.p2align 4,,10
	.p2align 3
.L2268:
	movl	$8, %r14d
	jmp	.L2254
.L2270:
	movq	%r14, %rdx
	jmp	.L2263
.L2255:
	cmpq	%rdx, %rdi
	cmovbe	%rdi, %rdx
	leaq	0(,%rdx,8), %r14
	jmp	.L2254
.L2281:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11206:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger13BreakLocationESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger13BreakLocationESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB11246:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L2299
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L2295
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L2300
.L2287:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L2294:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L2301
	testq	%r13, %r13
	jg	.L2290
	testq	%r9, %r9
	jne	.L2293
.L2291:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2301:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L2290
.L2293:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L2291
	.p2align 4,,10
	.p2align 3
.L2300:
	testq	%rsi, %rsi
	jne	.L2288
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L2294
	.p2align 4,,10
	.p2align 3
.L2290:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L2291
	jmp	.L2293
	.p2align 4,,10
	.p2align 3
.L2295:
	movl	$4, %r14d
	jmp	.L2287
.L2299:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2288:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L2287
	.cfi_endproc
.LFE11246:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB11407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdx
	movq	8(%rdi), %rax
	movq	%rdi, -216(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rax, -208(%rbp)
	subq	%rdx, %rax
	movq	%rdx, -160(%rbp)
	sarq	$3, %rax
	movabsq	$1152921504606846975, %rdx
	movq	%rsi, -136(%rbp)
	cmpq	%rdx, %rax
	je	.L2951
	movq	%rsi, %r13
	subq	-160(%rbp), %r13
	testq	%rax, %rax
	je	.L2608
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L2952
.L2304:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rax, -184(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -200(%rbp)
	leaq	8(%rax), %rbx
.L2607:
	movq	(%r12), %rax
	movq	-184(%rbp), %rsi
	movq	$0, (%r12)
	movq	%rax, (%rsi,%r13)
	movq	-160(%rbp), %rax
	cmpq	%rax, -104(%rbp)
	je	.L2306
	movq	%rsi, -96(%rbp)
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L2601:
	movq	-72(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	(%rcx), %rax
	movq	$0, (%rcx)
	movq	%rax, (%rsi)
	movq	(%rcx), %rbx
	testq	%rbx, %rbx
	je	.L2307
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2308
	movq	160(%rbx), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger9CallFrameE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	je	.L2309
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2310
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2309:
	movq	152(%rbx), %r12
	testq	%r12, %r12
	je	.L2311
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2312
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2311:
	movq	144(%rbx), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L2313
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -112(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rdx
	je	.L2314
	movq	%rbx, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L2590:
	movq	-56(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2315
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2316
	movq	112(%rbx), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	je	.L2317
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2318
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2319
	call	_ZdlPv@PLT
.L2319:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2317:
	movq	104(%rbx), %r12
	testq	%r12, %r12
	je	.L2320
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2321
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2322
	call	_ZdlPv@PLT
.L2322:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2320:
	movq	64(%rbx), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2323
	call	_ZdlPv@PLT
.L2323:
	movq	48(%rbx), %r14
	testq	%r14, %r14
	je	.L2324
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2325
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	movq	312(%r14), %r12
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r14)
	testq	%r12, %r12
	je	.L2326
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2327
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2328
	call	_ZdlPv@PLT
.L2328:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2329
	call	_ZdlPv@PLT
.L2329:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2326:
	movq	304(%r14), %rcx
	movq	%rcx, -80(%rbp)
	testq	%rcx, %rcx
	je	.L2330
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2331
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2332
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -128(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L2333
	movq	%rbx, -224(%rbp)
	movq	%r14, -232(%rbp)
	.p2align 4,,10
	.p2align 3
.L2465:
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2334
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2335
	movq	16(%rbx), %rcx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	%rcx, -88(%rbp)
	testq	%rcx, %rcx
	je	.L2336
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2337
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L2338
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -144(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L2339
	movq	%rbx, -240(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2396:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2340
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2341
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2342
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2343
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L2344
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	cmpq	%r14, %rdx
	je	.L2345
	movq	%rbx, -256(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -248(%rbp)
	jmp	.L2352
	.p2align 4,,10
	.p2align 3
.L2954:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L2348
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2349
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2348:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L2350
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2351
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2350:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2346:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2953
.L2352:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L2346
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2954
	addq	$8, %rbx
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%rbx, %r14
	jne	.L2352
	.p2align 4,,10
	.p2align 3
.L2953:
	movq	-192(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %rbx
	movq	(%rax), %r14
.L2345:
	testq	%r14, %r14
	je	.L2353
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2353:
	movq	-192(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2344:
	movq	152(%r13), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L2354
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2355
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L2364
	.p2align 4,,10
	.p2align 3
.L2956:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2358
	call	_ZdlPv@PLT
.L2358:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2359
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2360
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2359:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2361
	call	_ZdlPv@PLT
.L2361:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2362
	call	_ZdlPv@PLT
.L2362:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2363
	call	_ZdlPv@PLT
.L2363:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2356:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2955
.L2364:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2356
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2956
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2364
	.p2align 4,,10
	.p2align 3
.L2955:
	movq	-192(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L2355:
	testq	%r14, %r14
	je	.L2365
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2365:
	movq	-192(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2354:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2366
	call	_ZdlPv@PLT
.L2366:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2367
	call	_ZdlPv@PLT
.L2367:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2368
	call	_ZdlPv@PLT
.L2368:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2342:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2369
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2370
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L2371
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L2372
	movq	%rbx, -256(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r12, -248(%rbp)
	jmp	.L2379
	.p2align 4,,10
	.p2align 3
.L2958:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L2375
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2376
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2375:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L2377
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2378
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2377:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2373:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2957
.L2379:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L2373
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2958
	addq	$8, %rbx
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%rbx, %r14
	jne	.L2379
	.p2align 4,,10
	.p2align 3
.L2957:
	movq	-192(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %rbx
	movq	(%rax), %r14
.L2372:
	testq	%r14, %r14
	je	.L2380
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2380:
	movq	-192(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2371:
	movq	152(%r13), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L2381
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2382
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L2391
	.p2align 4,,10
	.p2align 3
.L2960:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2385
	call	_ZdlPv@PLT
.L2385:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2386
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2387
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2386:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2388
	call	_ZdlPv@PLT
.L2388:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2389
	call	_ZdlPv@PLT
.L2389:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2390
	call	_ZdlPv@PLT
.L2390:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2383:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2959
.L2391:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2383
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2960
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2391
	.p2align 4,,10
	.p2align 3
.L2959:
	movq	-192(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L2382:
	testq	%r14, %r14
	je	.L2392
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2392:
	movq	-192(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2381:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2393
	call	_ZdlPv@PLT
.L2393:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2394
	call	_ZdlPv@PLT
.L2394:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2395
	call	_ZdlPv@PLT
.L2395:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2369:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2340:
	addq	$8, %rbx
	cmpq	%rbx, -144(%rbp)
	jne	.L2396
	movq	-168(%rbp), %rax
	movq	-240(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2339:
	testq	%rdi, %rdi
	je	.L2397
	call	_ZdlPv@PLT
.L2397:
	movq	-168(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2338:
	movq	-88(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L2398
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -144(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L2399
	movq	%rbx, -240(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2433:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2400
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2401
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2402
	call	_ZdlPv@PLT
.L2402:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2403
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2404
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2405
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2406
	movq	%rbx, -248(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -192(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L2413
	.p2align 4,,10
	.p2align 3
.L2962:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2409
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2410
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2409:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2411
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2412
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2411:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2407:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2961
.L2413:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2407
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2962
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L2413
	.p2align 4,,10
	.p2align 3
.L2961:
	movq	-192(%rbp), %r12
	movq	-248(%rbp), %rbx
	movq	-256(%rbp), %r13
	movq	(%r14), %r15
.L2406:
	testq	%r15, %r15
	je	.L2414
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2414:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2405:
	movq	152(%r13), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L2415
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2416
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L2425
	.p2align 4,,10
	.p2align 3
.L2964:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2419
	call	_ZdlPv@PLT
.L2419:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2420
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2421
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2420:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2422
	call	_ZdlPv@PLT
.L2422:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2423
	call	_ZdlPv@PLT
.L2423:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2424
	call	_ZdlPv@PLT
.L2424:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2417:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2963
.L2425:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2417
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2964
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2425
	.p2align 4,,10
	.p2align 3
.L2963:
	movq	-192(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L2416:
	testq	%r14, %r14
	je	.L2426
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2426:
	movq	-192(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2415:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2427
	call	_ZdlPv@PLT
.L2427:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2428
	call	_ZdlPv@PLT
.L2428:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2429
	call	_ZdlPv@PLT
.L2429:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2403:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2430
	call	_ZdlPv@PLT
.L2430:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2431
	call	_ZdlPv@PLT
.L2431:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2432
	call	_ZdlPv@PLT
.L2432:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2400:
	addq	$8, %rbx
	cmpq	%rbx, -144(%rbp)
	jne	.L2433
	movq	-168(%rbp), %rax
	movq	-240(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2399:
	testq	%rdi, %rdi
	je	.L2434
	call	_ZdlPv@PLT
.L2434:
	movq	-168(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2398:
	movq	-88(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2435
	call	_ZdlPv@PLT
.L2435:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2436
	call	_ZdlPv@PLT
.L2436:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2437
	call	_ZdlPv@PLT
.L2437:
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2336:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L2438
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2439
	movq	160(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2440
	movq	8(%r13), %rax
	movq	0(%r13), %r15
	cmpq	%r15, %rax
	je	.L2441
	movq	%rbx, -88(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -144(%rbp)
	jmp	.L2448
	.p2align 4,,10
	.p2align 3
.L2966:
	movq	16(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L2444
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2445
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2444:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L2446
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2447
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2446:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2442:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2965
.L2448:
	movq	(%rbx), %r14
	testq	%r14, %r14
	je	.L2442
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2966
	addq	$8, %rbx
	movq	%r14, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L2448
	.p2align 4,,10
	.p2align 3
.L2965:
	movq	-88(%rbp), %rbx
	movq	-144(%rbp), %r12
	movq	0(%r13), %r15
.L2441:
	testq	%r15, %r15
	je	.L2449
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2449:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2440:
	movq	152(%r12), %r14
	testq	%r14, %r14
	je	.L2450
	movq	8(%r14), %r15
	movq	(%r14), %r13
	cmpq	%r13, %r15
	je	.L2451
	movq	%r12, -88(%rbp)
	movq	%r14, -144(%rbp)
	jmp	.L2460
	.p2align 4,,10
	.p2align 3
.L2968:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2454
	call	_ZdlPv@PLT
.L2454:
	movq	136(%r12), %r14
	testq	%r14, %r14
	je	.L2455
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2456
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2455:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2457
	call	_ZdlPv@PLT
.L2457:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2458
	call	_ZdlPv@PLT
.L2458:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2459
	call	_ZdlPv@PLT
.L2459:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2452:
	addq	$8, %r13
	cmpq	%r13, %r15
	je	.L2967
.L2460:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L2452
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2968
	addq	$8, %r13
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r13, %r15
	jne	.L2460
	.p2align 4,,10
	.p2align 3
.L2967:
	movq	-144(%rbp), %r14
	movq	-88(%rbp), %r12
	movq	(%r14), %r13
.L2451:
	testq	%r13, %r13
	je	.L2461
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2461:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2450:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2462
	call	_ZdlPv@PLT
.L2462:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2463
	call	_ZdlPv@PLT
.L2463:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2464
	call	_ZdlPv@PLT
.L2464:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2438:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2334:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -128(%rbp)
	jne	.L2465
.L2981:
	movq	-152(%rbp), %rax
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r14
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L2333:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L2466
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2466:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2332:
	movq	-80(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2467
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -128(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rdx
	je	.L2468
	movq	%rbx, -224(%rbp)
	movq	%r14, -232(%rbp)
	.p2align 4,,10
	.p2align 3
.L2577:
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2469
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2470
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2471
	call	_ZdlPv@PLT
.L2471:
	movq	136(%rbx), %rsi
	movq	%rsi, -88(%rbp)
	testq	%rsi, %rsi
	je	.L2472
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2473
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	movq	160(%rsi), %rax
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L2474
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -144(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L2475
	movq	%rbx, -240(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2532:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2476
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2477
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2478
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2479
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L2480
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	cmpq	%r14, %rsi
	je	.L2481
	movq	%rbx, -256(%rbp)
	movq	%r14, %rbx
	movq	%rsi, %r14
	movq	%r12, -248(%rbp)
	jmp	.L2488
	.p2align 4,,10
	.p2align 3
.L2970:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L2484
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2485
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2484:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L2486
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2487
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2486:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2482:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2969
.L2488:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L2482
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2970
	addq	$8, %rbx
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%rbx, %r14
	jne	.L2488
	.p2align 4,,10
	.p2align 3
.L2969:
	movq	-192(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %rbx
	movq	(%rax), %r14
.L2481:
	testq	%r14, %r14
	je	.L2489
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2489:
	movq	-192(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2480:
	movq	152(%r13), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L2490
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2491
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L2500
	.p2align 4,,10
	.p2align 3
.L2972:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2494
	call	_ZdlPv@PLT
.L2494:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2495
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2496
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2495:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2497
	call	_ZdlPv@PLT
.L2497:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2498
	call	_ZdlPv@PLT
.L2498:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2499
	call	_ZdlPv@PLT
.L2499:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2492:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2971
.L2500:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2492
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2972
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2500
	.p2align 4,,10
	.p2align 3
.L2971:
	movq	-192(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L2491:
	testq	%r14, %r14
	je	.L2501
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2501:
	movq	-192(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2490:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2502
	call	_ZdlPv@PLT
.L2502:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2503
	call	_ZdlPv@PLT
.L2503:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2504
	call	_ZdlPv@PLT
.L2504:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2478:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2505
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2506
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L2507
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	cmpq	%r14, %rdx
	je	.L2508
	movq	%rbx, -256(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -248(%rbp)
	jmp	.L2515
	.p2align 4,,10
	.p2align 3
.L2974:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L2511
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2512
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2511:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L2513
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2514
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2513:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2509:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2973
.L2515:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L2509
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2974
	addq	$8, %rbx
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%rbx, %r14
	jne	.L2515
	.p2align 4,,10
	.p2align 3
.L2973:
	movq	-192(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %rbx
	movq	(%rax), %r14
.L2508:
	testq	%r14, %r14
	je	.L2516
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2516:
	movq	-192(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2507:
	movq	152(%r13), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L2517
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2518
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L2527
	.p2align 4,,10
	.p2align 3
.L2976:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2521
	call	_ZdlPv@PLT
.L2521:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2522
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2523
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2522:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2524
	call	_ZdlPv@PLT
.L2524:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2525
	call	_ZdlPv@PLT
.L2525:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2526
	call	_ZdlPv@PLT
.L2526:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2519:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2975
.L2527:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2519
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2976
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2527
	.p2align 4,,10
	.p2align 3
.L2975:
	movq	-192(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L2518:
	testq	%r14, %r14
	je	.L2528
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2528:
	movq	-192(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2517:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2529
	call	_ZdlPv@PLT
.L2529:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2530
	call	_ZdlPv@PLT
.L2530:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2531
	call	_ZdlPv@PLT
.L2531:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2505:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2476:
	addq	$8, %rbx
	cmpq	%rbx, -144(%rbp)
	jne	.L2532
	movq	-168(%rbp), %rax
	movq	-240(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2475:
	testq	%rdi, %rdi
	je	.L2533
	call	_ZdlPv@PLT
.L2533:
	movq	-168(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2474:
	movq	-88(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L2534
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -144(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L2535
	movq	%rbx, -240(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2569:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2536
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2537
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2538
	call	_ZdlPv@PLT
.L2538:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2539
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2540
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2541
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2542
	movq	%rbx, -248(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -192(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L2549
	.p2align 4,,10
	.p2align 3
.L2978:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2545
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2546
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2545:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2547
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2548
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2547:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2543:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2977
.L2549:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2543
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2978
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L2549
	.p2align 4,,10
	.p2align 3
.L2977:
	movq	-192(%rbp), %r12
	movq	-248(%rbp), %rbx
	movq	-256(%rbp), %r13
	movq	(%r14), %r15
.L2542:
	testq	%r15, %r15
	je	.L2550
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2550:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2541:
	movq	152(%r13), %rax
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L2551
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2552
	movq	%r12, -248(%rbp)
	movq	%r13, -256(%rbp)
	jmp	.L2561
	.p2align 4,,10
	.p2align 3
.L2980:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2555
	call	_ZdlPv@PLT
.L2555:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2556
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2557
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2556:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2558
	call	_ZdlPv@PLT
.L2558:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2559
	call	_ZdlPv@PLT
.L2559:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2560
	call	_ZdlPv@PLT
.L2560:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2553:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2979
.L2561:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2553
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2980
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2561
	.p2align 4,,10
	.p2align 3
.L2979:
	movq	-192(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	-256(%rbp), %r13
	movq	(%rax), %r14
.L2552:
	testq	%r14, %r14
	je	.L2562
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2562:
	movq	-192(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2551:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2563
	call	_ZdlPv@PLT
.L2563:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2564
	call	_ZdlPv@PLT
.L2564:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2565
	call	_ZdlPv@PLT
.L2565:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2539:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2566
	call	_ZdlPv@PLT
.L2566:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2567
	call	_ZdlPv@PLT
.L2567:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2568
	call	_ZdlPv@PLT
.L2568:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2536:
	addq	$8, %rbx
	cmpq	%rbx, -144(%rbp)
	jne	.L2569
	movq	-168(%rbp), %rax
	movq	-240(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2535:
	testq	%rdi, %rdi
	je	.L2570
	call	_ZdlPv@PLT
.L2570:
	movq	-168(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2534:
	movq	-88(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2571
	call	_ZdlPv@PLT
.L2571:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2572
	call	_ZdlPv@PLT
.L2572:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2573
	call	_ZdlPv@PLT
.L2573:
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2472:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2574
	call	_ZdlPv@PLT
.L2574:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2575
	call	_ZdlPv@PLT
.L2575:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2576
	call	_ZdlPv@PLT
.L2576:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2469:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -128(%rbp)
	jne	.L2577
	movq	-152(%rbp), %rax
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r14
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L2468:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L2578
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2578:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2467:
	movq	-80(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2579
	call	_ZdlPv@PLT
.L2579:
	movq	-80(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2580
	call	_ZdlPv@PLT
.L2580:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2581
	call	_ZdlPv@PLT
.L2581:
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2330:
	movq	264(%r14), %rdi
	leaq	280(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2582
	call	_ZdlPv@PLT
.L2582:
	movq	216(%r14), %rdi
	leaq	232(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2583
	call	_ZdlPv@PLT
.L2583:
	movq	168(%r14), %rdi
	leaq	184(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2584
	call	_ZdlPv@PLT
.L2584:
	movq	152(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2585
	movq	(%rdi), %rax
	call	*24(%rax)
.L2585:
	movq	112(%r14), %rdi
	leaq	128(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2586
	call	_ZdlPv@PLT
.L2586:
	movq	64(%r14), %rdi
	leaq	80(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2587
	call	_ZdlPv@PLT
.L2587:
	movq	16(%r14), %rdi
	leaq	32(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2588
	call	_ZdlPv@PLT
.L2588:
	movl	$320, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2324:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2589
	call	_ZdlPv@PLT
.L2589:
	movl	$120, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2315:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -112(%rbp)
	jne	.L2590
	movq	-120(%rbp), %rax
	movq	-176(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L2314:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L2591
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2591:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2313:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2592
	call	_ZdlPv@PLT
.L2592:
	movq	96(%rbx), %r12
	testq	%r12, %r12
	je	.L2593
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2594
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2595
	call	_ZdlPv@PLT
.L2595:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2593:
	movq	88(%rbx), %r12
	testq	%r12, %r12
	je	.L2596
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2597
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2598
	call	_ZdlPv@PLT
.L2598:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2596:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2599
	call	_ZdlPv@PLT
.L2599:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2600
	call	_ZdlPv@PLT
.L2600:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2307:
	addq	$8, -72(%rbp)
	movq	-72(%rbp), %rax
	addq	$8, -96(%rbp)
	cmpq	%rax, -104(%rbp)
	jne	.L2601
	movq	-184(%rbp), %rsi
	movq	-104(%rbp), %rax
	subq	-160(%rbp), %rax
	leaq	8(%rsi,%rax), %rbx
.L2306:
	movq	-104(%rbp), %rdi
	movq	-208(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2602
	subq	%rdi, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L2610
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L2604:
	movdqu	(%rdi,%rax), %xmm2
	movups	%xmm2, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L2604
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	leaq	(%rbx,%rdi), %rdx
	addq	-104(%rbp), %rdi
	movq	%rdi, -136(%rbp)
	cmpq	%rcx, %rax
	je	.L2605
.L2603:
	movq	-136(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L2605:
	leaq	8(%rbx,%rsi), %rbx
.L2602:
	movq	-160(%rbp), %rax
	testq	%rax, %rax
	je	.L2606
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2606:
	movq	-184(%rbp), %xmm0
	movq	-216(%rbp), %rax
	movq	%rbx, %xmm3
	movq	-200(%rbp), %rsi
	punpcklqdq	%xmm3, %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2316:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2315
	.p2align 4,,10
	.p2align 3
.L2325:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2324
	.p2align 4,,10
	.p2align 3
.L2318:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2317
	.p2align 4,,10
	.p2align 3
.L2321:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2320
	.p2align 4,,10
	.p2align 3
.L2308:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2307
	.p2align 4,,10
	.p2align 3
.L2470:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2469
	.p2align 4,,10
	.p2align 3
.L2335:
	movq	%rbx, %rdi
	call	*%rax
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -128(%rbp)
	jne	.L2465
	jmp	.L2981
	.p2align 4,,10
	.p2align 3
.L2331:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2330
	.p2align 4,,10
	.p2align 3
.L2327:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2326
	.p2align 4,,10
	.p2align 3
.L2401:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2400
	.p2align 4,,10
	.p2align 3
.L2341:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2340
	.p2align 4,,10
	.p2align 3
.L2537:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2536
	.p2align 4,,10
	.p2align 3
.L2477:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2476
	.p2align 4,,10
	.p2align 3
.L2597:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2596
	.p2align 4,,10
	.p2align 3
.L2594:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2593
	.p2align 4,,10
	.p2align 3
.L2310:
	call	*%rax
	jmp	.L2309
	.p2align 4,,10
	.p2align 3
.L2312:
	call	*%rax
	jmp	.L2311
	.p2align 4,,10
	.p2align 3
.L2337:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2336
	.p2align 4,,10
	.p2align 3
.L2473:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L2472
	.p2align 4,,10
	.p2align 3
.L2439:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2334
	.p2align 4,,10
	.p2align 3
.L2952:
	testq	%rcx, %rcx
	jne	.L2305
	movq	$0, -200(%rbp)
	movl	$8, %ebx
	movq	$0, -184(%rbp)
	jmp	.L2607
	.p2align 4,,10
	.p2align 3
.L2445:
	call	*%rdx
	jmp	.L2444
	.p2align 4,,10
	.p2align 3
.L2456:
	call	*%rax
	jmp	.L2455
	.p2align 4,,10
	.p2align 3
.L2506:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2505
	.p2align 4,,10
	.p2align 3
.L2404:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2403
	.p2align 4,,10
	.p2align 3
.L2343:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2342
	.p2align 4,,10
	.p2align 3
.L2479:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2478
	.p2align 4,,10
	.p2align 3
.L2540:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2539
	.p2align 4,,10
	.p2align 3
.L2370:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2369
	.p2align 4,,10
	.p2align 3
.L2447:
	call	*%rdx
	jmp	.L2446
	.p2align 4,,10
	.p2align 3
.L2608:
	movl	$8, %ebx
	jmp	.L2304
	.p2align 4,,10
	.p2align 3
.L2548:
	call	*%rdx
	jmp	.L2547
	.p2align 4,,10
	.p2align 3
.L2376:
	call	*%rdx
	jmp	.L2375
	.p2align 4,,10
	.p2align 3
.L2349:
	call	*%rdx
	jmp	.L2348
	.p2align 4,,10
	.p2align 3
.L2351:
	call	*%rdx
	jmp	.L2350
	.p2align 4,,10
	.p2align 3
.L2514:
	call	*%rdx
	jmp	.L2513
	.p2align 4,,10
	.p2align 3
.L2496:
	call	*%rax
	jmp	.L2495
	.p2align 4,,10
	.p2align 3
.L2487:
	call	*%rdx
	jmp	.L2486
	.p2align 4,,10
	.p2align 3
.L2546:
	call	*%rdx
	jmp	.L2545
	.p2align 4,,10
	.p2align 3
.L2378:
	call	*%rdx
	jmp	.L2377
	.p2align 4,,10
	.p2align 3
.L2410:
	call	*%rdx
	jmp	.L2409
	.p2align 4,,10
	.p2align 3
.L2557:
	call	*%rax
	jmp	.L2556
	.p2align 4,,10
	.p2align 3
.L2485:
	call	*%rdx
	jmp	.L2484
	.p2align 4,,10
	.p2align 3
.L2421:
	call	*%rax
	jmp	.L2420
	.p2align 4,,10
	.p2align 3
.L2512:
	call	*%rdx
	jmp	.L2511
	.p2align 4,,10
	.p2align 3
.L2387:
	call	*%rax
	jmp	.L2386
	.p2align 4,,10
	.p2align 3
.L2412:
	call	*%rdx
	jmp	.L2411
	.p2align 4,,10
	.p2align 3
.L2360:
	call	*%rax
	jmp	.L2359
	.p2align 4,,10
	.p2align 3
.L2523:
	call	*%rax
	jmp	.L2522
.L2610:
	movq	%rbx, %rdx
	jmp	.L2603
.L2305:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L2304
.L2951:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11407:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB11472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	movabsq	$-3689348814741910323, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %r14
	movq	%rdi, -80(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rbx, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$230584300921369395, %rdx
	cmpq	%rdx, %rax
	je	.L3008
	subq	%r14, %rsi
	testq	%rax, %rax
	je	.L3000
	movabsq	$9223372036854775800, %r12
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L3009
.L2984:
	movq	%r12, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rcx
	movq	%rax, -64(%rbp)
	addq	%rax, %r12
	movq	%r12, -72(%rbp)
	leaq	40(%rax), %r12
.L2999:
	movq	-64(%rbp), %r15
	movq	%rcx, -88(%rbp)
	leaq	(%r15,%rsi), %r13
	movq	(%rcx), %rsi
	leaq	16(%r13), %rdx
	movq	%r13, %rdi
	movq	%rdx, 0(%r13)
	movq	8(%rcx), %rdx
	leaq	(%rsi,%rdx,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-88(%rbp), %rcx
	movq	-56(%rbp), %rax
	movq	32(%rcx), %rdx
	movq	%rdx, 32(%r13)
	cmpq	%r14, %rax
	je	.L2986
	leaq	-40(%rax), %rdx
	leaq	16(%r14), %r13
	movabsq	$922337203685477581, %rcx
	subq	%r14, %rdx
	shrq	$3, %rdx
	imulq	%rcx, %rdx
	movabsq	$2305843009213693951, %rcx
	andq	%rcx, %rdx
	movq	%rdx, -88(%rbp)
	leaq	(%rdx,%rdx,4), %rdx
	leaq	56(%r14,%rdx,8), %r12
	.p2align 4,,10
	.p2align 3
.L2992:
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	je	.L3010
.L2987:
	movq	%rcx, (%r15)
	movq	0(%r13), %rcx
	movq	%rcx, 16(%r15)
.L2988:
	movq	-8(%r13), %rcx
	xorl	%eax, %eax
	movq	%rcx, 8(%r15)
	movq	16(%r13), %rcx
	movq	%r13, -16(%r13)
	movq	$0, -8(%r13)
	movw	%ax, 0(%r13)
	movq	%rcx, 32(%r15)
	movq	-16(%r13), %rdi
	cmpq	%r13, %rdi
	je	.L2989
	call	_ZdlPv@PLT
	addq	$40, %r13
	addq	$40, %r15
	cmpq	%r13, %r12
	jne	.L2992
.L2990:
	movq	-88(%rbp), %rax
	movq	-64(%rbp), %rsi
	leaq	10(%rax,%rax,4), %rax
	leaq	(%rsi,%rax,8), %r12
.L2986:
	movq	-56(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L2993
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L2997:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rsi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rsi, %rcx
	je	.L3011
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	addq	$40, %rax
	addq	$40, %rdx
	movq	%rcx, -24(%rdx)
	movq	-32(%rax), %rcx
	movq	%rcx, -32(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rbx, %rax
	jne	.L2997
.L2995:
	subq	-56(%rbp), %rbx
	leaq	-40(%rbx), %rax
	shrq	$3, %rax
	leaq	40(%r12,%rax,8), %r12
.L2993:
	testq	%r14, %r14
	je	.L2998
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2998:
	movq	-64(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%r12, %xmm3
	movq	-72(%rbp), %rbx
	punpcklqdq	%xmm3, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2989:
	.cfi_restore_state
	addq	$40, %r13
	addq	$40, %r15
	cmpq	%r12, %r13
	je	.L2990
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	jne	.L2987
.L3010:
	movdqu	0(%r13), %xmm1
	movups	%xmm1, 16(%r15)
	jmp	.L2988
	.p2align 4,,10
	.p2align 3
.L3011:
	movq	8(%rax), %rcx
	movdqu	16(%rax), %xmm2
	addq	$40, %rax
	addq	$40, %rdx
	movq	%rcx, -32(%rdx)
	movq	-8(%rax), %rcx
	movups	%xmm2, -24(%rdx)
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L2997
	jmp	.L2995
	.p2align 4,,10
	.p2align 3
.L3009:
	testq	%rdi, %rdi
	jne	.L2985
	movq	$0, -72(%rbp)
	movl	$40, %r12d
	movq	$0, -64(%rbp)
	jmp	.L2999
	.p2align 4,,10
	.p2align 3
.L3000:
	movl	$40, %r12d
	jmp	.L2984
.L2985:
	cmpq	%rdx, %rdi
	cmovbe	%rdi, %rdx
	imulq	$40, %rdx, %r12
	jmp	.L2984
.L3008:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11472:
	.size	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_:
.LFB11970:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L3031
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r13
	movq	8(%rsi), %r15
	cmpq	%r13, %rdx
	je	.L3022
	movq	16(%rdi), %rax
.L3014:
	cmpq	%r15, %rax
	jb	.L3034
	leaq	(%r15,%r15), %rdx
	testq	%r15, %r15
	je	.L3018
.L3037:
	movq	(%r12), %rsi
	cmpq	$1, %r15
	je	.L3035
	testq	%rdx, %rdx
	je	.L3018
	movq	%r13, %rdi
	call	memmove@PLT
	movq	(%rbx), %r13
.L3018:
	xorl	%eax, %eax
	movq	%r15, 8(%rbx)
	movw	%ax, 0(%r13,%r15,2)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3034:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r15
	ja	.L3036
	addq	%rax, %rax
	movq	%r15, %r14
	cmpq	%rax, %r15
	jnb	.L3017
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r14
.L3017:
	leaq	2(%r14,%r14), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	cmpq	%rdi, %rdx
	je	.L3021
	call	_ZdlPv@PLT
.L3021:
	movq	%r13, (%rbx)
	leaq	(%r15,%r15), %rdx
	movq	%r14, 16(%rbx)
	testq	%r15, %r15
	je	.L3018
	jmp	.L3037
	.p2align 4,,10
	.p2align 3
.L3031:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L3022:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, %eax
	jmp	.L3014
	.p2align 4,,10
	.p2align 3
.L3035:
	movzwl	(%rsi), %eax
	movw	%ax, 0(%r13)
	movq	(%rbx), %r13
	jmp	.L3018
.L3036:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11970:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl28setInstrumentationBreakpointERKNS_8String16EPS1_.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"Instrumentation breakpoint is already enabled."
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl28setInstrumentationBreakpointERKNS_8String16EPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl28setInstrumentationBreakpointERKNS_8String16EPS1_
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl28setInstrumentationBreakpointERKNS_8String16EPS1_, @function
_ZN12v8_inspector19V8DebuggerAgentImpl28setInstrumentationBreakpointERKNS_8String16EPS1_:
.LFB7924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 32(%rsi)
	jne	.L3039
	leaq	-96(%rbp), %r13
	leaq	_ZN12v8_inspectorL19kDebuggerNotEnabledE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3038
.L3052:
	call	_ZdlPv@PLT
.L3038:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3053
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3039:
	.cfi_restore_state
	leaq	-176(%rbp), %r15
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	%r15, %rdi
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movl	$8, %esi
	movq	%r15, %rdi
	call	_ZN12v8_inspector15String16Builder12appendNumberEi@PLT
	movl	$58, %esi
	movq	%r15, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-144(%rbp), %r13
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3042
	call	_ZdlPv@PLT
.L3042:
	leaq	-96(%rbp), %r15
	leaq	_ZN12v8_inspector18DebuggerAgentStateL26instrumentationBreakpointsE(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	40(%rbx), %rdi
	movq	%r15, %rsi
	leaq	-80(%rbp), %rbx
	call	_ZN12v8_inspector12_GLOBAL__N_117getOrCreateObjectEPNS_8protocol15DictionaryValueERKNS_8String16E
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%rbx, %rdi
	je	.L3043
	call	_ZdlPv@PLT
.L3043:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L3044
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3046
	call	_ZdlPv@PLT
.L3046:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3052
	jmp	.L3038
	.p2align 4,,10
	.p2align 3
.L3044:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-184(%rbp), %rbx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-112(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, 32(%rbx)
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3052
	jmp	.L3038
.L3053:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7924:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl28setInstrumentationBreakpointERKNS_8String16EPS1_, .-_ZN12v8_inspector19V8DebuggerAgentImpl28setInstrumentationBreakpointERKNS_8String16EPS1_
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl25currentScheduledAsyncCallEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl25currentScheduledAsyncCallEv
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl25currentScheduledAsyncCallEv, @function
_ZN12v8_inspector19V8DebuggerAgentImpl25currentScheduledAsyncCallEv:
.LFB8059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	-160(%rbp), %rdi
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	movdqu	520(%rax), %xmm1
	movq	536(%rax), %rax
	movaps	%xmm1, -160(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZNK12v8_inspector14V8StackTraceId9IsInvalidEv@PLT
	testb	%al, %al
	je	.L3055
	movq	$0, (%r12)
.L3054:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3066
	subq	$-128, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3055:
	.cfi_restore_state
	movl	$104, %edi
	leaq	-80(%rbp), %r13
	call	_Znwm@PLT
	movq	-160(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	leaq	-64(%rax), %rcx
	leaq	16(%rbx), %r14
	movq	%rax, %xmm2
	movq	$0, 24(%rbx)
	movq	%rcx, %xmm0
	leaq	32(%rbx), %rax
	movb	$0, 56(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%rbx)
	xorl	%eax, %eax
	movw	%ax, 32(%rbx)
	leaq	80(%rbx), %rax
	movups	%xmm0, (%rbx)
	pxor	%xmm0, %xmm0
	movq	%rax, 64(%rbx)
	movups	%xmm0, 80(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 72(%rbx)
	call	_ZN12v8_inspector20stackTraceIdToStringEm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	leaq	-64(%rbp), %r14
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-48(%rbp), %rax
	movq	-80(%rbp), %rdi
	movq	%rax, 48(%rbx)
	cmpq	%r14, %rdi
	je	.L3057
	call	_ZdlPv@PLT
.L3057:
	movq	-152(%rbp), %rax
	orq	-144(%rbp), %rax
	je	.L3058
	leaq	-128(%rbp), %rdi
	leaq	-152(%rbp), %rsi
	call	_ZN12v8_inspector18debuggerIdToStringERKSt4pairIllE@PLT
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	movq	%r13, %rdi
	movq	%r14, -80(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-96(%rbp), %rax
	leaq	64(%rbx), %rdi
	movq	%r13, %rsi
	movq	%rax, -48(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-48(%rbp), %rax
	movq	-80(%rbp), %rdi
	movb	$1, 56(%rbx)
	movq	%rax, 96(%rbx)
	cmpq	%r14, %rdi
	je	.L3059
	call	_ZdlPv@PLT
.L3059:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3058
	call	_ZdlPv@PLT
.L3058:
	movq	%rbx, (%r12)
	jmp	.L3054
.L3066:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8059:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl25currentScheduledAsyncCallEv, .-_ZN12v8_inspector19V8DebuggerAgentImpl25currentScheduledAsyncCallEv
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl22getPossibleBreakpointsESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeIS4_EENSA_10ValueMaybeIbEEPS1_ISt6vectorIS1_INS3_13BreakLocationES5_ISG_EESaISI_EES5_ISK_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"start.lineNumber and start.columnNumber should be >= 0"
	.align 8
.LC12:
	.string	"Locations should contain the same scriptId"
	.align 8
.LC13:
	.string	"T* v8_inspector_protocol_bindings::glue::detail::PtrMaybe<T>::fromJust() const [with T = v8_inspector::protocol::Debugger::Location]"
	.align 8
.LC14:
	.string	"../deps/v8/../../deps/v8/third_party/inspector_protocol/bindings/bindings.h"
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl22getPossibleBreakpointsESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeIS4_EENSA_10ValueMaybeIbEEPS1_ISt6vectorIS1_INS3_13BreakLocationES5_ISG_EESaISI_EES5_ISK_EE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"value_"
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl22getPossibleBreakpointsESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeIS4_EENSA_10ValueMaybeIbEEPS1_ISt6vectorIS1_INS3_13BreakLocationES5_ISG_EESaISI_EES5_ISK_EE.str1.8
	.align 8
.LC16:
	.string	"end.lineNumber and end.columnNumber should be >= 0"
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl22getPossibleBreakpointsESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeIS4_EENSA_10ValueMaybeIbEEPS1_ISt6vectorIS1_INS3_13BreakLocationES5_ISG_EESaISI_EES5_ISK_EE.str1.1
.LC17:
	.string	"Script not found"
.LC18:
	.string	"Cannot retrive script context"
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl22getPossibleBreakpointsESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeIS4_EENSA_10ValueMaybeIbEEPS1_ISt6vectorIS1_INS3_13BreakLocationES5_ISG_EESaISI_EES5_ISK_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl22getPossibleBreakpointsESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeIS4_EENSA_10ValueMaybeIbEEPS1_ISt6vectorIS1_INS3_13BreakLocationES5_ISG_EESaISI_EES5_ISK_EE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl22getPossibleBreakpointsESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeIS4_EENSA_10ValueMaybeIbEEPS1_ISt6vectorIS1_INS3_13BreakLocationES5_ISG_EESaISI_EES5_ISK_EE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl22getPossibleBreakpointsESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeIS4_EENSA_10ValueMaybeIbEEPS1_ISt6vectorIS1_INS3_13BreakLocationES5_ISG_EESaISI_EES5_ISK_EE:
.LFB7927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-192(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$392, %rsp
	movq	(%rdx), %r14
	movq	%rcx, -392(%rbp)
	movq	%r8, -400(%rbp)
	movq	8(%r14), %rsi
	movq	16(%r14), %rdx
	movq	%r9, -376(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-176(%rbp), %rax
	movq	%rax, -384(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	40(%r14), %rax
	movq	%rax, -160(%rbp)
	movq	(%r15), %rax
	movl	48(%rax), %esi
	testl	%esi, %esi
	js	.L3068
	cmpb	$0, 52(%rax)
	jne	.L3152
	xorl	%edx, %edx
.L3069:
	leaq	-360(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -416(%rbp)
	call	_ZN2v85debug8LocationC1Eii@PLT
	leaq	-348(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -424(%rbp)
	call	_ZN2v85debug8LocationC1Ev@PLT
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L3072
	movq	8(%rax), %rsi
	movq	16(%rax), %rdx
	leaq	-96(%rbp), %r15
	leaq	-80(%rbp), %r14
	movq	%r15, %rdi
	movq	%rax, -408(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%r14, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-408(%rbp), %rax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	40(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	-96(%rbp), %rdi
	movl	%eax, %edx
	cmpq	%r14, %rdi
	je	.L3073
	movl	%eax, -408(%rbp)
	call	_ZdlPv@PLT
	movl	-408(%rbp), %edx
.L3073:
	leaq	.LC12(%rip), %rsi
	testl	%edx, %edx
	jne	.L3151
	movq	-392(%rbp), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L3153
	movzbl	52(%rax), %ecx
	movl	48(%rax), %esi
	testb	%cl, %cl
	jne	.L3154
.L3077:
	testl	%esi, %esi
	js	.L3116
	testb	%cl, %cl
	jne	.L3116
	leaq	-240(%rbp), %rdi
	call	_ZN2v85debug8LocationC1Eii@PLT
	movq	-240(%rbp), %rax
	movq	%rax, -348(%rbp)
	movzbl	-232(%rbp), %eax
	movb	%al, -340(%rbp)
.L3072:
	leaq	64(%r13), %rdi
	movq	%rbx, %rsi
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3155
	movq	56(%r13), %rsi
	pxor	%xmm0, %xmm0
	leaq	-304(%rbp), %r14
	movq	$0, -320(%rbp)
	movq	%r14, %rdi
	movaps	%xmm0, -336(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	48(%r15), %rax
	movq	8(%r13), %rdi
	movl	92(%rax), %esi
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3156
	call	_ZNK12v8_inspector16InspectedContext7contextEv@PLT
	movq	%rax, %rdi
	movq	%rax, -432(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	leaq	-272(%rbp), %r10
	movq	56(%r13), %rsi
	movl	$1, %edx
	movq	%r10, %rdi
	movq	%r10, -392(%rbp)
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	leaq	-240(%rbp), %rax
	movq	56(%r13), %rsi
	movq	%rax, %rdi
	movq	%rax, -408(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	48(%r15), %rdi
	xorl	%ecx, %ecx
	movq	-400(%rbp), %rdx
	movq	-392(%rbp), %r10
	movq	-432(%rbp), %r9
	movq	(%rdi), %rax
	cmpb	$0, (%rdx)
	movq	120(%rax), %rax
	je	.L3086
	movzbl	1(%rdx), %ecx
.L3086:
	movq	-424(%rbp), %rdx
	movq	-416(%rbp), %rsi
	leaq	-336(%rbp), %r8
	movq	%r9, -392(%rbp)
	movq	%r10, -400(%rbp)
	call	*%rax
	movq	-408(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-400(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
	movq	-392(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	movq	-376(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	(%rdx), %rcx
	movups	%xmm0, (%rax)
	movq	%rax, (%rdx)
	movq	%rcx, -392(%rbp)
	testq	%rcx, %rcx
	je	.L3087
	movq	8(%rcx), %r13
	movq	(%rcx), %r15
	cmpq	%r15, %r13
	jne	.L3093
	jmp	.L3088
	.p2align 4,,10
	.p2align 3
.L3158:
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE(%rip), %rax
	movq	72(%r14), %rdi
	movq	%rax, (%r14)
	leaq	88(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3091
	call	_ZdlPv@PLT
.L3091:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3092
	call	_ZdlPv@PLT
.L3092:
	movl	$112, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3089:
	addq	$8, %r15
	cmpq	%r15, %r13
	je	.L3157
.L3093:
	movq	(%r15), %r14
	testq	%r14, %r14
	je	.L3089
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3158
	movq	%r14, %rdi
	call	*%rax
	jmp	.L3089
	.p2align 4,,10
	.p2align 3
.L3152:
	movl	56(%rax), %edx
	testl	%edx, %edx
	jns	.L3069
.L3068:
	leaq	-96(%rbp), %r13
	leaq	.LC11(%rip), %rsi
.L3149:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3071
.L3147:
	call	_ZdlPv@PLT
.L3071:
	movq	-192(%rbp), %rdi
	cmpq	-384(%rbp), %rdi
	je	.L3067
	call	_ZdlPv@PLT
.L3067:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3159
	addq	$392, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3116:
	.cfi_restore_state
	leaq	.LC16(%rip), %rsi
.L3151:
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L3147
	jmp	.L3071
	.p2align 4,,10
	.p2align 3
.L3154:
	movl	56(%rax), %edx
	movl	%edx, %eax
	shrl	$31, %eax
	movl	%eax, %ecx
	jmp	.L3077
	.p2align 4,,10
	.p2align 3
.L3156:
	leaq	-96(%rbp), %r13
	leaq	.LC18(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3084
	call	_ZdlPv@PLT
.L3084:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L3085:
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3147
	jmp	.L3071
	.p2align 4,,10
	.p2align 3
.L3157:
	movq	-392(%rbp), %rax
	movq	(%rax), %r15
.L3088:
	testq	%r15, %r15
	je	.L3094
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3094:
	movq	-392(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3087:
	movq	-328(%rbp), %rax
	xorl	%r13d, %r13d
	cmpq	%rax, -336(%rbp)
	je	.L3109
	movq	%r12, -400(%rbp)
	jmp	.L3095
	.p2align 4,,10
	.p2align 3
.L3161:
	testl	%eax, %eax
	movq	_ZN12v8_inspector8protocol8Debugger13BreakLocation8TypeEnum4CallE(%rip), %rsi
	leaq	-144(%rbp), %rdi
	pxor	%xmm0, %xmm0
	je	.L3146
	leaq	-128(%rbp), %rdx
	xorl	%eax, %eax
	movaps	%xmm0, -128(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rdx, %rsi
	movq	$0, -136(%rbp)
	movw	%ax, -128(%rbp)
	movq	$0, -112(%rbp)
	movq	%rdx, -392(%rbp)
.L3100:
	leaq	-96(%rbp), %r14
	leaq	-80(%rbp), %r12
	movq	%r14, %rdi
	movq	%r12, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rdx
	leaq	72(%r15), %rdi
	movq	%r14, %rsi
	movq	%rdx, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rdx
	movq	-96(%rbp), %rdi
	movb	$1, 64(%r15)
	movq	%rdx, 104(%r15)
	cmpq	%r12, %rdi
	je	.L3101
	call	_ZdlPv@PLT
.L3101:
	movq	-144(%rbp), %rdi
	cmpq	-392(%rbp), %rdi
	je	.L3096
	call	_ZdlPv@PLT
.L3096:
	movq	-376(%rbp), %rax
	movq	(%rax), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L3103
	movq	-240(%rbp), %rax
	movq	$0, -240(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%rdi)
.L3104:
	movq	-240(%rbp), %r14
	testq	%r14, %r14
	je	.L3105
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3106
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE(%rip), %rax
	movq	72(%r14), %rdi
	movq	%rax, (%r14)
	leaq	88(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3107
	call	_ZdlPv@PLT
.L3107:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3108
	call	_ZdlPv@PLT
.L3108:
	movl	$112, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3105:
	movq	-328(%rbp), %rax
	subq	-336(%rbp), %rax
	addq	$1, %r13
	sarq	$4, %rax
	cmpq	%rax, %r13
	jnb	.L3160
.L3095:
	movl	$112, %edi
	movq	%r13, %r14
	call	_Znwm@PLT
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%rax, %r15
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE(%rip), %rax
	salq	$4, %r14
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	leaq	8(%r15), %rdi
	movq	%rax, 8(%r15)
	leaq	88(%r15), %rax
	movw	%dx, 24(%r15)
	movq	%rax, 72(%r15)
	movq	$0, 16(%r15)
	movq	$0, 40(%r15)
	movb	$0, 52(%r15)
	movl	$0, 56(%r15)
	movb	$0, 64(%r15)
	movq	$0, 104(%r15)
	movq	$0, 80(%r15)
	movl	$0, 48(%r15)
	movups	%xmm0, 88(%r15)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-160(%rbp), %rax
	movq	-336(%rbp), %rdi
	movq	%rax, 40(%r15)
	addq	%r14, %rdi
	call	_ZNK2v85debug8Location13GetLineNumberEv@PLT
	movq	-336(%rbp), %rdi
	movl	%eax, 48(%r15)
	addq	%r14, %rdi
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	movb	$1, 52(%r15)
	movl	%eax, 56(%r15)
	movq	-336(%rbp), %rax
	movq	%r15, -240(%rbp)
	movl	12(%rax,%r14), %eax
	cmpl	$3, %eax
	je	.L3096
	cmpl	$1, %eax
	je	.L3097
	cmpl	$2, %eax
	jne	.L3161
	movq	_ZN12v8_inspector8protocol8Debugger13BreakLocation8TypeEnum17DebuggerStatementE(%rip), %rsi
	leaq	-144(%rbp), %rdi
.L3146:
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rsi
	leaq	(%rsi,%rax,2), %rdx
	leaq	-128(%rbp), %rax
	movq	%rax, -392(%rbp)
	jmp	.L3100
	.p2align 4,,10
	.p2align 3
.L3103:
	movq	-408(%rbp), %rdx
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger13BreakLocationESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L3104
	.p2align 4,,10
	.p2align 3
.L3106:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L3105
	.p2align 4,,10
	.p2align 3
.L3160:
	movq	-400(%rbp), %r12
.L3109:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L3085
	.p2align 4,,10
	.p2align 3
.L3097:
	movq	_ZN12v8_inspector8protocol8Debugger13BreakLocation8TypeEnum6ReturnE(%rip), %rsi
	leaq	-144(%rbp), %rdi
	jmp	.L3146
	.p2align 4,,10
	.p2align 3
.L3155:
	leaq	-96(%rbp), %r13
	leaq	.LC17(%rip), %rsi
	jmp	.L3149
.L3159:
	call	__stack_chk_fail@PLT
.L3153:
	leaq	.LC13(%rip), %rcx
	movl	$26, %edx
	leaq	.LC14(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE7927:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl22getPossibleBreakpointsESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeIS4_EENSA_10ValueMaybeIbEEPS1_ISt6vectorIS1_INS3_13BreakLocationES5_ISG_EESaISI_EES5_ISK_EE, .-_ZN12v8_inspector19V8DebuggerAgentImpl22getPossibleBreakpointsESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeIS4_EENSA_10ValueMaybeIbEEPS1_ISt6vectorIS1_INS3_13BreakLocationES5_ISG_EESaISI_EES5_ISK_EE
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl17currentCallFramesEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl17currentCallFramesEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl17currentCallFramesEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl17currentCallFramesEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EE:
.LFB8030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$568, %rsp
	movq	%rdi, -584(%rbp)
	movq	16(%rcx), %rdi
	movq	%rsi, -512(%rbp)
	movq	%rdx, -576(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	movl	16(%rax), %esi
	call	_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi@PLT
	testb	%al, %al
	jne	.L3163
	movl	$24, %edi
	call	_Znwm@PLT
	movq	(%rbx), %r13
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, (%rbx)
	movups	%xmm0, (%rax)
	testq	%r13, %r13
	je	.L3164
	movq	8(%r13), %rbx
	movq	0(%r13), %r12
	cmpq	%r12, %rbx
	je	.L3165
	leaq	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev(%rip), %r14
	jmp	.L3168
	.p2align 4,,10
	.p2align 3
.L3492:
	call	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3166:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L3491
.L3168:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L3166
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L3492
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L3168
	.p2align 4,,10
	.p2align 3
.L3491:
	movq	0(%r13), %r12
.L3165:
	testq	%r12, %r12
	je	.L3169
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3169:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3164:
	movq	-584(%rbp), %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L3162:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3493
	movq	-584(%rbp), %rax
	addq	$568, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3163:
	.cfi_restore_state
	movq	-512(%rbp), %rax
	movq	56(%rax), %rsi
	leaq	-400(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -592(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	movq	-576(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	(%rcx), %r12
	movups	%xmm0, (%rax)
	movq	%rax, (%rcx)
	testq	%r12, %r12
	je	.L3171
	movq	8(%r12), %rbx
	movq	(%r12), %r14
	cmpq	%r14, %rbx
	je	.L3172
	leaq	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev(%rip), %r13
	jmp	.L3175
	.p2align 4,,10
	.p2align 3
.L3495:
	call	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3173:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L3494
.L3175:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L3173
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	je	.L3495
	call	*%rax
	addq	$8, %r14
	cmpq	%r14, %rbx
	jne	.L3175
	.p2align 4,,10
	.p2align 3
.L3494:
	movq	(%r12), %r14
.L3172:
	testq	%r14, %r14
	je	.L3176
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3176:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3171:
	movq	-512(%rbp), %rax
	xorl	%edx, %edx
	leaq	-488(%rbp), %rdi
	movq	56(%rax), %rsi
	call	_ZN2v85debug18StackTraceIterator6CreateEPNS_7IsolateEi@PLT
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	movl	$0, -556(%rbp)
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm4
	movq	%rdx, %xmm2
	punpcklqdq	%xmm4, %xmm2
	movaps	%xmm2, -608(%rbp)
	.p2align 4,,10
	.p2align 3
.L3311:
	movq	-488(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	jne	.L3177
	movq	-488(%rbp), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	$0, -480(%rbp)
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L3496
.L3178:
	movl	-556(%rbp), %edx
	leaq	-368(%rbp), %rax
	movl	%r13d, %esi
	movq	%rax, %rdi
	movq	%rax, -568(%rbp)
	call	_ZN12v8_inspector17RemoteCallFrameId9serializeEii@PLT
	movq	-488(%rbp), %rdi
	movq	(%rdi), %rax
	call	*72(%rax)
	movq	-488(%rbp), %rsi
	leaq	-472(%rbp), %rdi
	movl	%edx, -436(%rbp)
	movq	%rax, -444(%rbp)
	movq	(%rsi), %rax
	call	*88(%rax)
	movq	-512(%rbp), %rax
	movl	$24, %edi
	movq	-480(%rbp), %r14
	movq	-472(%rbp), %rbx
	movq	56(%rax), %rax
	movq	%r14, -544(%rbp)
	movq	%rax, -552(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -520(%rbp)
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	testq	%r14, %r14
	je	.L3182
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*16(%rax)
	testb	%al, %al
	jne	.L3182
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	leaq	-272(%rbp), %r13
	leaq	-176(%rbp), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %r15
	call	*56(%rax)
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	(%rbx), %rax
	movq	%r13, -504(%rbp)
	movq	%rbx, %rdi
	call	*16(%rax)
	testb	%al, %al
	jne	.L3183
	.p2align 4,,10
	.p2align 3
.L3499:
	leaq	_ZN12v8_inspectorL21kBacktraceObjectGroupE(%rip), %rsi
	movq	%r14, %rdi
	movq	$0, -464(%rbp)
	leaq	-160(%rbp), %r12
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*40(%rax)
	leaq	-112(%rbp), %rdi
	movl	$1, %r8d
	movq	%r14, %rcx
	movq	-544(%rbp), %rsi
	movq	%rax, %rdx
	leaq	-464(%rbp), %r9
	call	_ZN12v8_inspector14InjectedScript10wrapObjectEN2v85LocalINS1_5ValueEEERKNS_8String16ENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE@PLT
	movq	-176(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L3184
	call	_ZdlPv@PLT
.L3184:
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	jne	.L3497
	movl	$120, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%rax, %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	movq	%rax, 8(%r13)
	xorl	%eax, %eax
	movw	%ax, 24(%r13)
	leaq	80(%r13), %rax
	movups	%xmm0, 80(%r13)
	pxor	%xmm0, %xmm0
	movq	$0, 16(%r13)
	movq	$0, 40(%r13)
	movq	$0, 48(%r13)
	movb	$0, 56(%r13)
	movq	$0, 96(%r13)
	movq	%rax, 64(%r13)
	movq	$0, 72(%r13)
	movups	%xmm0, 104(%r13)
	movq	(%rbx), %rax
	call	*32(%rax)
	cmpl	$8, %eax
	ja	.L3191
	leaq	.L3193(%rip), %rcx
	movl	%eax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl17currentCallFramesEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EE,"a",@progbits
	.align 4
	.align 4
.L3193:
	.long	.L3201-.L3193
	.long	.L3200-.L3193
	.long	.L3199-.L3193
	.long	.L3198-.L3193
	.long	.L3197-.L3193
	.long	.L3196-.L3193
	.long	.L3195-.L3193
	.long	.L3194-.L3193
	.long	.L3192-.L3193
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl17currentCallFramesEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EE
	.p2align 4,,10
	.p2align 3
.L3194:
	movq	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum4EvalE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	.p2align 4,,10
	.p2align 3
.L3202:
	leaq	8(%r13), %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-144(%rbp), %rax
	movq	48(%r13), %rdi
	movq	%rax, 40(%r13)
	movq	-464(%rbp), %rax
	movq	$0, -464(%rbp)
	movq	%rax, 48(%r13)
	testq	%rdi, %rdi
	je	.L3203
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3204
	movq	%rdi, -528(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movq	-528(%rbp), %rdi
	movl	$320, %esi
	call	_ZdlPvm@PLT
.L3203:
	movq	-176(%rbp), %rdi
	movq	%r13, -456(%rbp)
	cmpq	%r12, %rdi
	je	.L3205
	call	_ZdlPv@PLT
.L3205:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*48(%rax)
	movq	-552(%rbp), %rsi
	leaq	-224(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector29toProtocolStringWithTypeCheckEPN2v87IsolateENS0_5LocalINS0_5ValueEEE@PLT
	movq	-216(%rbp), %rdx
	testq	%rdx, %rdx
	jne	.L3498
.L3206:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*64(%rax)
	testb	%al, %al
	je	.L3208
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*72(%rax)
	movl	$64, %edi
	movl	%edx, -424(%rbp)
	movq	-456(%rbp), %rdx
	movq	%rax, -432(%rbp)
	movq	%rdx, -536(%rbp)
	call	_Znwm@PLT
	movq	-504(%rbp), %rsi
	movq	%rax, %r12
	movq	%r15, (%rax)
	leaq	8(%rax), %rdi
	leaq	24(%rax), %rax
	movq	%rax, 8(%r12)
	xorl	%eax, %eax
	movw	%ax, 24(%r12)
	movq	$0, 16(%r12)
	movq	$0, 40(%r12)
	movb	$0, 52(%r12)
	movl	$0, 56(%r12)
	movl	$0, 48(%r12)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-240(%rbp), %rax
	leaq	-432(%rbp), %rdi
	movq	%rdi, -528(%rbp)
	movq	%rax, 40(%r12)
	call	_ZNK2v85debug8Location13GetLineNumberEv@PLT
	movq	-528(%rbp), %rdi
	movl	%eax, 48(%r12)
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	movq	-536(%rbp), %rdx
	movb	$1, 52(%r12)
	movl	%eax, 56(%r12)
	movq	104(%rdx), %r8
	movq	%r12, 104(%rdx)
	testq	%r8, %r8
	je	.L3209
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3210
	movq	8(%r8), %rdi
	leaq	24(%r8), %rax
	movq	%r15, (%r8)
	cmpq	%rax, %rdi
	je	.L3211
	movq	%r8, -528(%rbp)
	call	_ZdlPv@PLT
	movq	-528(%rbp), %r8
.L3211:
	movl	$64, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L3209:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*80(%rax)
	movl	$64, %edi
	movl	%edx, -408(%rbp)
	movq	-456(%rbp), %rdx
	movq	%rax, -416(%rbp)
	movq	%rdx, -536(%rbp)
	call	_Znwm@PLT
	movq	-504(%rbp), %rsi
	movq	%rax, %r12
	movq	%r15, (%rax)
	leaq	8(%rax), %rdi
	leaq	24(%rax), %rax
	movq	%rax, 8(%r12)
	xorl	%eax, %eax
	movw	%ax, 24(%r12)
	movq	$0, 16(%r12)
	movq	$0, 40(%r12)
	movb	$0, 52(%r12)
	movl	$0, 56(%r12)
	movl	$0, 48(%r12)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-240(%rbp), %rax
	leaq	-416(%rbp), %rdi
	movq	%rdi, -528(%rbp)
	movq	%rax, 40(%r12)
	call	_ZNK2v85debug8Location13GetLineNumberEv@PLT
	movq	-528(%rbp), %rdi
	movl	%eax, 48(%r12)
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	movq	-536(%rbp), %rdx
	movb	$1, 52(%r12)
	movl	%eax, 56(%r12)
	movq	112(%rdx), %r8
	movq	%r12, 112(%rdx)
	testq	%r8, %r8
	je	.L3208
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3213
	movq	8(%r8), %rdi
	leaq	24(%r8), %rax
	movq	%r15, (%r8)
	cmpq	%rax, %rdi
	je	.L3214
	movq	%r8, -528(%rbp)
	call	_ZdlPv@PLT
	movq	-528(%rbp), %r8
.L3214:
	movl	$64, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L3208:
	movq	-520(%rbp), %rcx
	movq	8(%rcx), %rsi
	cmpq	16(%rcx), %rsi
	je	.L3215
	movq	-456(%rbp), %rax
	movq	$0, -456(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%rcx)
.L3216:
	movq	-224(%rbp), %rdi
	leaq	-208(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3217
	call	_ZdlPv@PLT
.L3217:
	movq	-456(%rbp), %r12
	testq	%r12, %r12
	je	.L3218
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3219
	movq	112(%r12), %r8
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r8, %r8
	je	.L3220
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3221
	movq	8(%r8), %rdi
	leaq	24(%r8), %rax
	movq	%r15, (%r8)
	cmpq	%rax, %rdi
	je	.L3222
	movq	%r8, -528(%rbp)
	call	_ZdlPv@PLT
	movq	-528(%rbp), %r8
.L3222:
	movl	$64, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L3220:
	movq	104(%r12), %r8
	testq	%r8, %r8
	je	.L3223
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3224
	movq	8(%r8), %rdi
	leaq	24(%r8), %rax
	movq	%r15, (%r8)
	cmpq	%rax, %rdi
	je	.L3225
	movq	%r8, -528(%rbp)
	call	_ZdlPv@PLT
	movq	-528(%rbp), %r8
.L3225:
	movl	$64, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L3223:
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3226
	call	_ZdlPv@PLT
.L3226:
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3227
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3228
	movq	%rdi, -528(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movq	-528(%rbp), %rdi
	movl	$320, %esi
	call	_ZdlPvm@PLT
.L3227:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3229
	call	_ZdlPv@PLT
.L3229:
	movl	$120, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3218:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3230
	call	_ZdlPv@PLT
.L3230:
	movq	-464(%rbp), %r12
	testq	%r12, %r12
	je	.L3231
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3232
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3231:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*16(%rax)
	testb	%al, %al
	je	.L3499
.L3183:
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	leaq	-152(%rbp), %rax
	movq	%rax, -528(%rbp)
.L3190:
	movq	-272(%rbp), %rdi
	leaq	-256(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3181
	call	_ZdlPv@PLT
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	je	.L3234
.L3501:
	movq	-584(%rbp), %rcx
	movl	%eax, (%rcx)
	leaq	24(%rcx), %rax
	movq	%rax, 8(%rcx)
	movq	-168(%rbp), %rax
	cmpq	-528(%rbp), %rax
	je	.L3500
	movq	%rax, 8(%rcx)
	movq	-152(%rbp), %rax
	movq	%rax, 24(%rcx)
.L3236:
	movq	-160(%rbp), %rax
	movq	-472(%rbp), %rdi
	movq	%rax, 16(%rcx)
	movq	-136(%rbp), %rax
	movq	%rax, 40(%rcx)
	movl	-128(%rbp), %eax
	movl	%eax, 48(%rcx)
	testq	%rdi, %rdi
	je	.L3336
.L3237:
	movq	(%rdi), %rax
	call	*8(%rax)
.L3314:
	cmpq	$0, -520(%rbp)
	jne	.L3336
.L3315:
	movq	-368(%rbp), %rdi
	leaq	-352(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3332
	call	_ZdlPv@PLT
	jmp	.L3332
	.p2align 4,,10
	.p2align 3
.L3195:
	movq	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum6ScriptE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	jmp	.L3202
	.p2align 4,,10
	.p2align 3
.L3196:
	movq	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum5BlockE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	jmp	.L3202
	.p2align 4,,10
	.p2align 3
.L3197:
	movq	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum5CatchE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	jmp	.L3202
	.p2align 4,,10
	.p2align 3
.L3198:
	movq	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum7ClosureE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	jmp	.L3202
	.p2align 4,,10
	.p2align 3
.L3199:
	movq	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum4WithE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	jmp	.L3202
	.p2align 4,,10
	.p2align 3
.L3200:
	movq	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum5LocalE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	jmp	.L3202
	.p2align 4,,10
	.p2align 3
.L3201:
	movq	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum6GlobalE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	jmp	.L3202
	.p2align 4,,10
	.p2align 3
.L3192:
	movq	_ZN12v8_inspector8protocol8Debugger5Scope8TypeEnum6ModuleE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	jmp	.L3202
	.p2align 4,,10
	.p2align 3
.L3498:
	movq	-224(%rbp), %rsi
	movq	-456(%rbp), %rax
	movq	%r14, %rdi
	movq	%r12, -176(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rax, -528(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-528(%rbp), %rax
	movq	-192(%rbp), %rdx
	movq	%r14, %rsi
	leaq	64(%rax), %rdi
	movq	%rdx, -144(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-144(%rbp), %rdx
	movq	-528(%rbp), %rax
	movq	%rdx, 96(%rax)
	movb	$1, 56(%rax)
	movq	-176(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L3206
	call	_ZdlPv@PLT
	jmp	.L3206
	.p2align 4,,10
	.p2align 3
.L3215:
	movq	%rcx, %rdi
	leaq	-456(%rbp), %rdx
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger5ScopeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L3216
	.p2align 4,,10
	.p2align 3
.L3204:
	call	*%rax
	jmp	.L3203
	.p2align 4,,10
	.p2align 3
.L3232:
	call	*%rax
	jmp	.L3231
	.p2align 4,,10
	.p2align 3
.L3219:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3218
	.p2align 4,,10
	.p2align 3
.L3182:
	leaq	-176(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	leaq	-152(%rbp), %rax
	movq	%rax, -528(%rbp)
.L3181:
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	jne	.L3501
.L3234:
	cmpq	$0, -480(%rbp)
	movq	$0, -456(%rbp)
	je	.L3502
	movq	-488(%rbp), %rdi
	leaq	-112(%rbp), %r12
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3242
	movq	-480(%rbp), %r15
	leaq	-224(%rbp), %r14
	leaq	_ZN12v8_inspectorL21kBacktraceObjectGroupE(%rip), %rsi
	movq	%r14, %rdi
	leaq	-112(%rbp), %r12
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	leaq	-456(%rbp), %r9
	movl	$1, %r8d
	movq	%r12, %rdi
	call	_ZN12v8_inspector14InjectedScript10wrapObjectEN2v85LocalINS1_5ValueEEERKNS_8String16ENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE@PLT
	movl	-112(%rbp), %eax
	leaq	-168(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	movl	%eax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	%rax, -136(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -128(%rbp)
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3335
	call	_ZdlPv@PLT
.L3335:
	movq	-224(%rbp), %rdi
	leaq	-208(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3244
	call	_ZdlPv@PLT
.L3244:
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	jne	.L3503
.L3242:
	cmpq	$0, -456(%rbp)
	je	.L3248
	leaq	-96(%rbp), %rax
	movq	%rax, -504(%rbp)
.L3249:
	movq	-488(%rbp), %rdi
	leaq	-444(%rbp), %rbx
	movq	(%rdi), %rax
	call	*64(%rax)
	movl	$64, %edi
	movq	%rax, %r13
	call	_Znwm@PLT
	xorl	%r10d, %r10d
	movq	%r13, %rdi
	movq	%rax, %r15
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	leaq	8(%r15), %r14
	movw	%r10w, 24(%r15)
	movq	%rax, 8(%r15)
	movq	$0, 16(%r15)
	movq	$0, 40(%r15)
	movb	$0, 52(%r15)
	movl	$0, 56(%r15)
	movl	$0, 48(%r15)
	call	_ZNK2v85debug6Script2IdEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, 40(%r15)
	call	_ZNK2v85debug8Location13GetLineNumberEv@PLT
	movq	%rbx, %rdi
	movl	%eax, 48(%r15)
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	movb	$1, 52(%r15)
	movq	-112(%rbp), %rdi
	movl	%eax, 56(%r15)
	cmpq	-504(%rbp), %rdi
	je	.L3252
	call	_ZdlPv@PLT
.L3252:
	movq	-512(%rbp), %rax
	movq	8(%r15), %rsi
	movq	%r12, %rdi
	movq	16(%rax), %rax
	movq	%rax, -536(%rbp)
	leaq	728(%rax), %rbx
	movq	-504(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	16(%r15), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	40(%r15), %rax
	cmpb	$0, 52(%r15)
	movq	%rax, -80(%rbp)
	movl	48(%r15), %eax
	movl	%eax, -432(%rbp)
	movl	$-1, %eax
	je	.L3253
	movl	56(%r15), %eax
.L3253:
	movl	%eax, -416(%rbp)
	leaq	-432(%rbp), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-416(%rbp), %rcx
	movq	%rax, %rdx
	movq	%rax, -552(%rbp)
	movq	%rcx, -544(%rbp)
	call	_ZN12v8_inspector15WasmTranslation45TranslateWasmScriptLocationToProtocolLocationEPNS_8String16EPiS3_@PLT
	testb	%al, %al
	jne	.L3504
.L3254:
	movq	-112(%rbp), %rdi
	cmpq	-504(%rbp), %rdi
	je	.L3255
	call	_ZdlPv@PLT
.L3255:
	movq	%r13, %rdi
	call	_ZNK2v85debug6Script2IdEv@PLT
	leaq	-320(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	8(%r15), %rsi
	movq	16(%r15), %rax
	movq	%r12, %rdi
	movq	-504(%rbp), %rbx
	leaq	(%rsi,%rax,2), %rdx
	movq	%rbx, -112(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	40(%r15), %rax
	movq	%r12, %rsi
	movq	%rax, -80(%rbp)
	movq	-512(%rbp), %rax
	leaq	64(%rax), %rdi
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%rbx, %rdi
	je	.L3256
	call	_ZdlPv@PLT
.L3256:
	leaq	-256(%rbp), %rax
	xorl	%r9d, %r9d
	movq	$0, -264(%rbp)
	leaq	-272(%rbp), %r13
	movq	%rax, -536(%rbp)
	movq	%rax, -272(%rbp)
	movw	%r9w, -256(%rbp)
	movq	$0, -240(%rbp)
	testq	%r14, %r14
	je	.L3257
	movq	48(%r14), %r14
	movq	%r13, %rdi
	leaq	48(%r14), %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	80(%r14), %rax
	movq	%rax, -240(%rbp)
.L3257:
	movl	$168, %edi
	call	_Znwm@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger9CallFrameE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rax
	leaq	8(%rbx), %rdi
	movq	%rax, 8(%rbx)
	leaq	64(%rbx), %rax
	leaq	48(%rbx), %r14
	movq	%rax, 48(%rbx)
	leaq	120(%rbx), %rax
	movw	%cx, 24(%rbx)
	movw	%r8w, 120(%rbx)
	movw	%si, 64(%rbx)
	movq	-568(%rbp), %rsi
	movq	%rax, 104(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 136(%rbx)
	movq	$0, 160(%rbx)
	movups	%xmm0, 88(%rbx)
	movups	%xmm0, 144(%rbx)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-336(%rbp), %rax
	movq	-488(%rbp), %rdi
	movq	%rax, 40(%rbx)
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	-512(%rbp), %rax
	movq	56(%rax), %rsi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	96(%rbx), %r14
	movq	%r15, 96(%rbx)
	movq	%rax, 80(%rbx)
	testq	%r14, %r14
	je	.L3258
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3259
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r14), %rdi
	movq	%rax, (%r14)
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3260
	call	_ZdlPv@PLT
.L3260:
	movl	$64, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3258:
	leaq	104(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-240(%rbp), %rax
	movq	144(%rbx), %r14
	movq	%rax, 136(%rbx)
	movq	-520(%rbp), %rax
	movq	%rax, 144(%rbx)
	testq	%r14, %r14
	je	.L3261
	movq	8(%r14), %rax
	movq	(%r14), %r13
	movq	%rax, -520(%rbp)
	cmpq	%r13, %rax
	je	.L3262
	movq	%r12, -568(%rbp)
	jmp	.L3275
	.p2align 4,,10
	.p2align 3
.L3506:
	movq	112(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3265
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3266
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3267
	call	_ZdlPv@PLT
.L3267:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3265:
	movq	104(%r15), %r12
	testq	%r12, %r12
	je	.L3268
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3269
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3270
	call	_ZdlPv@PLT
.L3270:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3268:
	movq	64(%r15), %rdi
	leaq	80(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3271
	call	_ZdlPv@PLT
.L3271:
	movq	48(%r15), %r12
	testq	%r12, %r12
	je	.L3272
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3273
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3272:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3274
	call	_ZdlPv@PLT
.L3274:
	movl	$120, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3263:
	addq	$8, %r13
	cmpq	%r13, -520(%rbp)
	je	.L3505
.L3275:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L3263
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3506
	movq	%r15, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -520(%rbp)
	jne	.L3275
	.p2align 4,,10
	.p2align 3
.L3505:
	movq	-568(%rbp), %r12
	movq	(%r14), %r13
.L3262:
	testq	%r13, %r13
	je	.L3276
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L3276:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3261:
	movq	-456(%rbp), %rax
	movq	152(%rbx), %r13
	movq	$0, -456(%rbp)
	movq	%rax, 152(%rbx)
	testq	%r13, %r13
	je	.L3277
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3278
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3277:
	movq	%rbx, -432(%rbp)
	movq	-112(%rbp), %rdi
	cmpq	-504(%rbp), %rdi
	je	.L3279
	call	_ZdlPv@PLT
.L3279:
	movq	-488(%rbp), %rdi
	movq	(%rdi), %rax
	call	*80(%rax)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L3280
	movl	$64, %edi
	movq	-432(%rbp), %r15
	call	_Znwm@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	leaq	8(%r13), %r8
	movw	%dx, 24(%r13)
	movq	%rax, 8(%r13)
	movq	$0, 16(%r13)
	movq	$0, 40(%r13)
	movb	$0, 52(%r13)
	movl	$0, 56(%r13)
	movl	$0, 48(%r13)
	movq	%r8, -520(%rbp)
	call	_ZNK2v88Function8ScriptIdEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	-520(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, 40(%r13)
	call	_ZNK2v88Function19GetScriptLineNumberEv@PLT
	movq	%r14, %rdi
	movl	%eax, 48(%r13)
	call	_ZNK2v88Function21GetScriptColumnNumberEv@PLT
	movb	$1, 52(%r13)
	movl	%eax, 56(%r13)
	movq	88(%r15), %rdi
	movq	%r13, 88(%r15)
	testq	%rdi, %rdi
	je	.L3281
	movq	(%rdi), %rax
	call	*24(%rax)
.L3281:
	movq	-112(%rbp), %rdi
	cmpq	-504(%rbp), %rdi
	je	.L3280
	call	_ZdlPv@PLT
.L3280:
	movq	-488(%rbp), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3283
	movq	-480(%rbp), %r14
	testq	%r14, %r14
	je	.L3283
	leaq	-224(%rbp), %r15
	leaq	_ZN12v8_inspectorL21kBacktraceObjectGroupE(%rip), %rsi
	movq	$0, -416(%rbp)
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	-544(%rbp), %r9
	movl	$1, %r8d
	movq	%r12, %rdi
	call	_ZN12v8_inspector14InjectedScript10wrapObjectEN2v85LocalINS1_5ValueEEERKNS_8String16ENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE@PLT
	movl	-112(%rbp), %eax
	leaq	-168(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	movl	%eax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	%rax, -136(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -128(%rbp)
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3284
	call	_ZdlPv@PLT
.L3284:
	movq	-224(%rbp), %rdi
	leaq	-208(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3285
	call	_ZdlPv@PLT
.L3285:
	movl	-176(%rbp), %eax
	movq	-416(%rbp), %rdi
	testl	%eax, %eax
	jne	.L3507
	movq	-432(%rbp), %rax
	movq	$0, -416(%rbp)
	movq	160(%rax), %r12
	movq	%rdi, 160(%rax)
	testq	%r12, %r12
	je	.L3283
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rbx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L3296
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3297:
	movq	-416(%rbp), %r12
	testq	%r12, %r12
	je	.L3283
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L3299
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3283:
	movq	-576(%rbp), %rax
	movq	(%rax), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L3300
	movq	-432(%rbp), %rax
	movq	$0, -432(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%rdi)
.L3301:
	movq	-432(%rbp), %r12
	testq	%r12, %r12
	je	.L3302
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3303
	call	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3302:
	movq	-272(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L3304
	call	_ZdlPv@PLT
.L3304:
	movq	-320(%rbp), %rdi
	leaq	-304(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3305
	call	_ZdlPv@PLT
.L3305:
	movq	-456(%rbp), %r12
	testq	%r12, %r12
	je	.L3306
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3307
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3306:
	movq	-168(%rbp), %rdi
	cmpq	-528(%rbp), %rdi
	je	.L3308
	call	_ZdlPv@PLT
.L3308:
	movq	-472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3309
	movq	(%rdi), %rax
	call	*8(%rax)
.L3309:
	movq	-368(%rbp), %rdi
	leaq	-352(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3310
	call	_ZdlPv@PLT
.L3310:
	movq	-488(%rbp), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	addl	$1, -556(%rbp)
	jmp	.L3311
	.p2align 4,,10
	.p2align 3
.L3224:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L3223
	.p2align 4,,10
	.p2align 3
.L3228:
	call	*%rax
	jmp	.L3227
	.p2align 4,,10
	.p2align 3
.L3221:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L3220
	.p2align 4,,10
	.p2align 3
.L3496:
	movq	-512(%rbp), %rax
	leaq	-112(%rbp), %r12
	leaq	-480(%rbp), %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	call	_ZN12v8_inspector22V8InspectorSessionImpl18findInjectedScriptEiRPNS_14InjectedScriptE@PLT
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3178
	call	_ZdlPv@PLT
	jmp	.L3178
	.p2align 4,,10
	.p2align 3
.L3273:
	call	*%rax
	jmp	.L3272
	.p2align 4,,10
	.p2align 3
.L3269:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3268
	.p2align 4,,10
	.p2align 3
.L3266:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3265
	.p2align 4,,10
	.p2align 3
.L3213:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L3208
	.p2align 4,,10
	.p2align 3
.L3210:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L3209
	.p2align 4,,10
	.p2align 3
.L3504:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movb	$1, 52(%r15)
	movq	%rax, 40(%r15)
	movl	-432(%rbp), %eax
	movl	%eax, 48(%r15)
	movl	-416(%rbp), %eax
	movl	%eax, 56(%r15)
	jmp	.L3254
	.p2align 4,,10
	.p2align 3
.L3502:
	leaq	-112(%rbp), %r12
.L3248:
	movl	$320, %edi
	xorl	%r14d, %r14d
	call	_Znwm@PLT
	movdqa	-608(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, %rbx
	leaq	16(%rax), %r13
	leaq	32(%rax), %rax
	movups	%xmm1, -32(%rax)
	movq	%rax, 16(%rbx)
	leaq	80(%rbx), %rax
	movq	%rax, 64(%rbx)
	leaq	128(%rbx), %rax
	movq	%rax, 112(%rbx)
	leaq	184(%rbx), %rax
	movq	%rax, 168(%rbx)
	leaq	232(%rbx), %rax
	movq	%rax, 216(%rbx)
	leaq	280(%rbx), %rax
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 184(%rbx)
	movups	%xmm0, 232(%rbx)
	movups	%xmm0, 280(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rax, 264(%rbx)
	movq	$0, 24(%rbx)
	movw	%r14w, 32(%rbx)
	movq	$0, 48(%rbx)
	movb	$0, 56(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 72(%rbx)
	movb	$0, 104(%rbx)
	movq	$0, 144(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 152(%rbx)
	movb	$0, 160(%rbx)
	movq	$0, 200(%rbx)
	movq	$0, 176(%rbx)
	movb	$0, 208(%rbx)
	movq	$0, 248(%rbx)
	movq	$0, 224(%rbx)
	movb	$0, 256(%rbx)
	movq	$0, 296(%rbx)
	movq	$0, 272(%rbx)
	movups	%xmm0, 304(%rbx)
	movq	_ZN12v8_inspector8protocol7Runtime12RemoteObject8TypeEnum9UndefinedE(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	-456(%rbp), %r13
	movq	%rbx, -456(%rbp)
	movq	%rax, 48(%rbx)
	testq	%r13, %r13
	je	.L3241
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3250
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3241:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	movq	%rax, -504(%rbp)
	cmpq	%rax, %rdi
	je	.L3249
	call	_ZdlPv@PLT
	jmp	.L3249
	.p2align 4,,10
	.p2align 3
.L3497:
	movl	%eax, -176(%rbp)
	leaq	-152(%rbp), %rax
	leaq	-88(%rbp), %rdx
	movq	%rax, -528(%rbp)
	movq	%rax, -168(%rbp)
	movq	-104(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L3508
	movq	%rax, -168(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -152(%rbp)
.L3187:
	movq	-96(%rbp), %rax
	movq	-464(%rbp), %r12
	movq	%rax, -160(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, -136(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -128(%rbp)
	testq	%r12, %r12
	je	.L3190
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3189
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3190
	.p2align 4,,10
	.p2align 3
.L3300:
	movq	-552(%rbp), %rdx
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L3301
	.p2align 4,,10
	.p2align 3
.L3259:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L3258
	.p2align 4,,10
	.p2align 3
.L3278:
	call	*%rax
	jmp	.L3277
	.p2align 4,,10
	.p2align 3
.L3303:
	call	*%rax
	jmp	.L3302
	.p2align 4,,10
	.p2align 3
.L3307:
	call	*%rax
	jmp	.L3306
	.p2align 4,,10
	.p2align 3
.L3508:
	movdqu	-88(%rbp), %xmm3
	movups	%xmm3, -152(%rbp)
	jmp	.L3187
	.p2align 4,,10
	.p2align 3
.L3299:
	call	*%rax
	jmp	.L3283
	.p2align 4,,10
	.p2align 3
.L3296:
	call	*%rax
	jmp	.L3297
	.p2align 4,,10
	.p2align 3
.L3189:
	call	*%rax
	jmp	.L3190
	.p2align 4,,10
	.p2align 3
.L3177:
	movq	-584(%rbp), %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L3332:
	movq	-488(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3333
	movq	(%rdi), %rax
	call	*8(%rax)
.L3333:
	movq	-592(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L3162
	.p2align 4,,10
	.p2align 3
.L3336:
	movq	-520(%rbp), %rax
	movq	8(%rax), %rbx
	movq	(%rax), %r12
	cmpq	%r12, %rbx
	je	.L3316
	leaq	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev(%rip), %r14
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %r13
	jmp	.L3329
	.p2align 4,,10
	.p2align 3
.L3510:
	movq	112(%r15), %r8
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger5ScopeE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r8, %r8
	je	.L3319
	movq	(%r8), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L3320
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L3321
	movq	%r8, -504(%rbp)
	call	_ZdlPv@PLT
	movq	-504(%rbp), %r8
.L3321:
	movl	$64, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L3319:
	movq	104(%r15), %r8
	testq	%r8, %r8
	je	.L3322
	movq	(%r8), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L3323
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L3324
	movq	%r8, -504(%rbp)
	call	_ZdlPv@PLT
	movq	-504(%rbp), %r8
.L3324:
	movl	$64, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L3322:
	movq	64(%r15), %rdi
	leaq	80(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3325
	call	_ZdlPv@PLT
.L3325:
	movq	48(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3326
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3327
	movq	%rdi, -504(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movq	-504(%rbp), %rdi
	movl	$320, %esi
	call	_ZdlPvm@PLT
.L3326:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3328
	call	_ZdlPv@PLT
.L3328:
	movl	$120, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3317:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L3509
.L3329:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L3317
	movq	(%r15), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L3510
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r12, %rbx
	jne	.L3329
	.p2align 4,,10
	.p2align 3
.L3509:
	movq	-520(%rbp), %rax
	movq	(%rax), %r12
.L3316:
	testq	%r12, %r12
	je	.L3330
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3330:
	movq	-520(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L3315
	.p2align 4,,10
	.p2align 3
.L3250:
	call	*%rax
	jmp	.L3241
	.p2align 4,,10
	.p2align 3
.L3320:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L3319
	.p2align 4,,10
	.p2align 3
.L3323:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L3322
	.p2align 4,,10
	.p2align 3
.L3327:
	call	*%rax
	jmp	.L3326
	.p2align 4,,10
	.p2align 3
.L3507:
	movq	-584(%rbp), %rcx
	movl	%eax, (%rcx)
	leaq	24(%rcx), %rax
	movq	%rax, 8(%rcx)
	movq	-168(%rbp), %rax
	cmpq	-528(%rbp), %rax
	je	.L3511
	movq	%rax, 8(%rcx)
	movq	-152(%rbp), %rax
	movq	%rax, 24(%rcx)
.L3288:
	movq	-160(%rbp), %rax
	movq	$0, -160(%rbp)
	movq	%rax, 16(%rcx)
	movq	-528(%rbp), %rax
	movq	%rax, -168(%rbp)
	xorl	%eax, %eax
	movw	%ax, -152(%rbp)
	movq	-136(%rbp), %rax
	movq	%rax, 40(%rcx)
	movl	-128(%rbp), %eax
	movl	%eax, 48(%rcx)
	testq	%rdi, %rdi
	je	.L3289
	movq	(%rdi), %rax
	call	*24(%rax)
.L3289:
	movq	-432(%rbp), %r12
	testq	%r12, %r12
	je	.L3290
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3291
	call	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3290:
	movq	-272(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L3292
	call	_ZdlPv@PLT
.L3292:
	movq	-320(%rbp), %rdi
	leaq	-304(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3293
	call	_ZdlPv@PLT
.L3293:
	movq	$0, -520(%rbp)
.L3247:
	movq	-456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3312
	movq	(%rdi), %rax
	call	*24(%rax)
.L3312:
	movq	-168(%rbp), %rdi
	cmpq	-528(%rbp), %rdi
	je	.L3313
	call	_ZdlPv@PLT
.L3313:
	movq	-472(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3237
	jmp	.L3314
.L3503:
	movq	-584(%rbp), %rcx
	movl	%eax, (%rcx)
	leaq	24(%rcx), %rax
	movq	%rax, 8(%rcx)
	movq	-168(%rbp), %rax
	cmpq	-528(%rbp), %rax
	je	.L3512
	movq	%rax, 8(%rcx)
	movq	-152(%rbp), %rax
	movq	%rax, 24(%rcx)
.L3246:
	movq	-160(%rbp), %rax
	xorl	%r11d, %r11d
	movq	$0, -160(%rbp)
	movw	%r11w, -152(%rbp)
	movq	%rax, 16(%rcx)
	movq	-528(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-136(%rbp), %rax
	movq	%rax, 40(%rcx)
	movl	-128(%rbp), %eax
	movl	%eax, 48(%rcx)
	jmp	.L3247
.L3500:
	movdqu	-152(%rbp), %xmm5
	movups	%xmm5, 24(%rcx)
	jmp	.L3236
.L3512:
	movdqu	-152(%rbp), %xmm7
	movups	%xmm7, 24(%rcx)
	jmp	.L3246
.L3291:
	call	*%rax
	jmp	.L3290
.L3511:
	movdqu	-152(%rbp), %xmm6
	movups	%xmm6, 24(%rcx)
	jmp	.L3288
.L3191:
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3493:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8030:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl17currentCallFramesEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EE, .-_ZN12v8_inspector19V8DebuggerAgentImpl17currentCallFramesEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EE
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl25currentExternalStackTraceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl25currentExternalStackTraceEv
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl25currentExternalStackTraceEv, @function
_ZN12v8_inspector19V8DebuggerAgentImpl25currentExternalStackTraceEv:
.LFB8058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	16(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector10V8Debugger21currentExternalParentEv@PLT
	movq	%r13, %rdi
	call	_ZNK12v8_inspector14V8StackTraceId9IsInvalidEv@PLT
	testb	%al, %al
	je	.L3514
	movq	$0, (%r12)
.L3513:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3521
	addq	$184, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3514:
	.cfi_restore_state
	movl	$104, %edi
	leaq	-192(%rbp), %r13
	call	_Znwm@PLT
	movq	-224(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	leaq	-64(%rax), %rcx
	leaq	16(%rbx), %r14
	movq	%rax, %xmm1
	movq	$0, 24(%rbx)
	movq	%rcx, %xmm0
	leaq	32(%rbx), %rax
	movb	$0, 56(%rbx)
	leaq	64(%rbx), %r15
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 16(%rbx)
	xorl	%eax, %eax
	movw	%ax, 32(%rbx)
	leaq	80(%rbx), %rax
	movups	%xmm0, (%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 80(%rbx)
	movq	%rax, 64(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 72(%rbx)
	call	_ZN12v8_inspector20stackTraceIdToStringEm@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-96(%rbp), %r14
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-160(%rbp), %rax
	leaq	-144(%rbp), %rdi
	leaq	-216(%rbp), %rsi
	leaq	-80(%rbp), %r13
	movq	%rax, 48(%rbx)
	call	_ZN12v8_inspector18debuggerIdToStringERKSt4pairIllE@PLT
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r13, -96(%rbp)
	movq	-136(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movb	$1, 56(%rbx)
	movq	%rax, 96(%rbx)
	cmpq	%r13, %rdi
	je	.L3516
	call	_ZdlPv@PLT
.L3516:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	movq	%rbx, (%r12)
	cmpq	%rax, %rdi
	je	.L3517
	call	_ZdlPv@PLT
.L3517:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3513
	call	_ZdlPv@PLT
	jmp	.L3513
.L3521:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8058:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl25currentExternalStackTraceEv, .-_ZN12v8_inspector19V8DebuggerAgentImpl25currentExternalStackTraceEv
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl15setScriptSourceERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEEPNS6_8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol8Debugger9CallFrameESt14default_deleteISE_EESaISH_EEEEPS8_PNS9_INSC_7Runtime10StackTraceEEEPNS9_INSN_12StackTraceIdEEEPNS9_INSN_16ExceptionDetailsEEE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"No script with given id found"
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl15setScriptSourceERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEEPNS6_8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol8Debugger9CallFrameESt14default_deleteISE_EESaISH_EEEEPS8_PNS9_INSC_7Runtime10StackTraceEEEPNS9_INSN_12StackTraceIdEEEPNS9_INSN_16ExceptionDetailsEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl15setScriptSourceERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEEPNS6_8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol8Debugger9CallFrameESt14default_deleteISE_EESaISH_EEEEPS8_PNS9_INSC_7Runtime10StackTraceEEEPNS9_INSN_12StackTraceIdEEEPNS9_INSN_16ExceptionDetailsEEE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl15setScriptSourceERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEEPNS6_8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol8Debugger9CallFrameESt14default_deleteISE_EESaISH_EEEEPS8_PNS9_INSC_7Runtime10StackTraceEEEPNS9_INSN_12StackTraceIdEEEPNS9_INSN_16ExceptionDetailsEEE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl15setScriptSourceERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEEPNS6_8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol8Debugger9CallFrameESt14default_deleteISE_EESaISH_EEEEPS8_PNS9_INSC_7Runtime10StackTraceEEEPNS9_INSN_12StackTraceIdEEEPNS9_INSN_16ExceptionDetailsEEE:
.LFB7958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	leaq	_ZN12v8_inspectorL19kDebuggerNotEnabledE(%rip), %rsi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movq	16(%rbp), %r14
	movq	%r9, -208(%rbp)
	movq	%rax, -216(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 32(%r15)
	je	.L3571
	movq	%rdx, %rsi
	leaq	64(%r15), %rdi
	movq	%rcx, %r13
	movq	%r8, %rbx
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3572
	movq	48(%rax), %rax
	movq	8(%r15), %rdi
	movq	%rdx, -232(%rbp)
	movl	92(%rax), %esi
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEi@PLT
	movq	-232(%rbp), %rdx
	testq	%rax, %rax
	je	.L3573
	movq	56(%r15), %rsi
	movq	%rax, -232(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -248(%rbp)
	movq	%rax, -240(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-232(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK12v8_inspector16InspectedContext7contextEv@PLT
	movq	%rax, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-248(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movl	$0, -144(%rbp)
	movb	$0, -140(%rbp)
	movq	48(%rdx), %rdi
	movups	%xmm0, -136(%rbp)
	xorl	%edx, %edx
	cmpb	$0, (%rbx)
	movq	$-1, -120(%rbp)
	movq	(%rdi), %rax
	movq	112(%rax), %rax
	je	.L3529
	movzbl	1(%rbx), %edx
.L3529:
	leaq	-144(%rbp), %rcx
	movq	%r13, %rsi
	call	*%rax
	movl	-144(%rbp), %edx
	testl	%edx, %edx
	je	.L3530
	movl	$184, %edi
	leaq	-112(%rbp), %r13
	call	_Znwm@PLT
	movq	8(%r15), %rdx
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %r14
	movq	%rax, 16(%rbx)
	xorl	%eax, %eax
	movw	%ax, 32(%rbx)
	leaq	88(%rbx), %rax
	movq	%rax, 72(%rbx)
	leaq	136(%rbx), %rax
	movups	%xmm0, 88(%rbx)
	movups	%xmm0, 136(%rbx)
	pxor	%xmm0, %xmm0
	movl	$0, 8(%rbx)
	movups	%xmm0, 160(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 80(%rbx)
	movb	$0, 112(%rbx)
	movq	$0, 152(%rbx)
	movq	%rax, 120(%rbx)
	movq	$0, 128(%rbx)
	movb	$0, 176(%rbx)
	movl	$0, 180(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	movb	$0, 64(%rbx)
	movl	44(%rdx), %eax
	addl	$1, %eax
	movl	%eax, 44(%rdx)
	movq	-128(%rbp), %rdx
	movl	%eax, 8(%rbx)
	movq	56(%r15), %rsi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movl	-120(%rbp), %ecx
	xorl	%edx, %edx
	movq	%rax, 48(%rbx)
	cmpl	$-1, %ecx
	leal	-1(%rcx), %eax
	cmove	%edx, %eax
	movl	%eax, 56(%rbx)
	movl	-116(%rbp), %eax
	cmpl	$-1, %eax
	cmove	%edx, %eax
	movl	%eax, 60(%rbx)
	movq	-200(%rbp), %rax
	movq	(%rax), %r13
	movq	%rbx, (%rax)
	testq	%r13, %r13
	je	.L3533
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3534
	call	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD1Ev
	movl	$184, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3533:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3535
	call	_ZdlPv@PLT
.L3535:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L3536:
	movq	-232(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-240(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L3522:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3574
	addq	$216, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3572:
	.cfi_restore_state
	leaq	-112(%rbp), %r13
	leaq	.LC19(%rip), %rsi
.L3571:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3522
	call	_ZdlPv@PLT
	jmp	.L3522
	.p2align 4,,10
	.p2align 3
.L3530:
	movzbl	-140(%rbp), %eax
	movb	$1, (%r14)
	leaq	-112(%rbp), %rdi
	movq	%r15, %rsi
	leaq	-192(%rbp), %rdx
	movq	$0, -192(%rbp)
	movb	%al, 1(%r14)
	call	_ZN12v8_inspector19V8DebuggerAgentImpl17currentCallFramesEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EE
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L3537
	movl	%eax, (%r12)
	leaq	24(%r12), %rax
	leaq	-88(%rbp), %rdx
	movq	%rax, 8(%r12)
	movq	-104(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L3575
	movq	%rax, 8(%r12)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%r12)
.L3539:
	movq	-96(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-72(%rbp), %rax
	movq	%rax, 40(%r12)
	movl	-64(%rbp), %eax
	movl	%eax, 48(%r12)
	jmp	.L3540
	.p2align 4,,10
	.p2align 3
.L3573:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse13InternalErrorEv@PLT
	jmp	.L3522
	.p2align 4,,10
	.p2align 3
.L3537:
	movq	-208(%rbp), %rcx
	movq	-192(%rbp), %rax
	movq	$0, -192(%rbp)
	movq	(%rcx), %rdi
	movq	%rax, (%rcx)
	testq	%rdi, %rdi
	je	.L3541
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
.L3541:
	leaq	-184(%rbp), %r13
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl22currentAsyncStackTraceEv
	movq	-216(%rbp), %rcx
	movq	-184(%rbp), %rax
	movq	$0, -184(%rbp)
	movq	(%rcx), %rdi
	movq	%rax, (%rcx)
	testq	%rdi, %rdi
	je	.L3543
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3543
	movq	(%rdi), %rax
	call	*24(%rax)
.L3543:
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl25currentExternalStackTraceEv
	movq	-224(%rbp), %rcx
	movq	-184(%rbp), %rax
	movq	$0, -184(%rbp)
	movq	(%rcx), %rdi
	movq	%rax, (%rcx)
	testq	%rdi, %rdi
	je	.L3546
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3546
	movq	(%rdi), %rax
	call	*24(%rax)
.L3546:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3540
	call	_ZdlPv@PLT
.L3540:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3536
	call	_ZNKSt14default_deleteISt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger9CallFrameES_IS5_EESaIS7_EEEclEPS9_.isra.0.part.0
	jmp	.L3536
	.p2align 4,,10
	.p2align 3
.L3575:
	movdqu	-88(%rbp), %xmm1
	movups	%xmm1, 24(%r12)
	jmp	.L3539
	.p2align 4,,10
	.p2align 3
.L3534:
	call	*%rax
	jmp	.L3533
.L3574:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7958:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl15setScriptSourceERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEEPNS6_8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol8Debugger9CallFrameESt14default_deleteISE_EESaISH_EEEEPS8_PNS9_INSC_7Runtime10StackTraceEEEPNS9_INSN_12StackTraceIdEEEPNS9_INSN_16ExceptionDetailsEEE, .-_ZN12v8_inspector19V8DebuggerAgentImpl15setScriptSourceERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEEPNS6_8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol8Debugger9CallFrameESt14default_deleteISE_EESaISH_EEEEPS8_PNS9_INSC_7Runtime10StackTraceEEEPNS9_INSN_12StackTraceIdEEEPNS9_INSN_16ExceptionDetailsEEE
	.section	.text._ZNSt5dequeIN12v8_inspector8String16ESaIS1_EE19_M_destroy_data_auxESt15_Deque_iteratorIS1_RS1_PS1_ES7_,"axG",@progbits,_ZNSt5dequeIN12v8_inspector8String16ESaIS1_EE19_M_destroy_data_auxESt15_Deque_iteratorIS1_RS1_PS1_ES7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN12v8_inspector8String16ESaIS1_EE19_M_destroy_data_auxESt15_Deque_iteratorIS1_RS1_PS1_ES7_
	.type	_ZNSt5dequeIN12v8_inspector8String16ESaIS1_EE19_M_destroy_data_auxESt15_Deque_iteratorIS1_RS1_PS1_ES7_, @function
_ZNSt5dequeIN12v8_inspector8String16ESaIS1_EE19_M_destroy_data_auxESt15_Deque_iteratorIS1_RS1_PS1_ES7_:
.LFB12974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rsi), %rdx
	movq	24(%r14), %rax
	leaq	8(%rdx), %r13
	cmpq	%rax, %r13
	jnb	.L3577
	.p2align 4,,10
	.p2align 3
.L3582:
	movq	0(%r13), %rbx
	leaq	480(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L3581:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3578
	call	_ZdlPv@PLT
	addq	$40, %rbx
	cmpq	%rbx, %r12
	jne	.L3581
	movq	24(%r14), %rax
	addq	$8, %r13
	cmpq	%r13, %rax
	ja	.L3582
.L3599:
	movq	24(%r15), %rdx
.L3577:
	movq	(%r15), %rbx
	cmpq	%rdx, %rax
	je	.L3583
	movq	16(%r15), %r12
	cmpq	%rbx, %r12
	je	.L3588
	.p2align 4,,10
	.p2align 3
.L3584:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3587
	call	_ZdlPv@PLT
	addq	$40, %rbx
	cmpq	%rbx, %r12
	jne	.L3584
.L3588:
	movq	(%r14), %r12
	movq	8(%r14), %rbx
	cmpq	%rbx, %r12
	je	.L3576
	.p2align 4,,10
	.p2align 3
.L3586:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3590
	call	_ZdlPv@PLT
	addq	$40, %rbx
	cmpq	%rbx, %r12
	jne	.L3586
.L3576:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3578:
	.cfi_restore_state
	addq	$40, %rbx
	cmpq	%rbx, %r12
	jne	.L3581
	movq	24(%r14), %rax
	addq	$8, %r13
	cmpq	%r13, %rax
	ja	.L3582
	jmp	.L3599
	.p2align 4,,10
	.p2align 3
.L3590:
	addq	$40, %rbx
	cmpq	%rbx, %r12
	jne	.L3586
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3587:
	.cfi_restore_state
	addq	$40, %rbx
	cmpq	%rbx, %r12
	jne	.L3584
	jmp	.L3588
.L3583:
	movq	(%r14), %r12
	cmpq	%rbx, %r12
	je	.L3576
.L3594:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3592
.L3600:
	call	_ZdlPv@PLT
	addq	$40, %rbx
	cmpq	%rbx, %r12
	je	.L3576
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	jne	.L3600
.L3592:
	addq	$40, %rbx
	cmpq	%rbx, %r12
	jne	.L3594
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12974:
	.size	_ZNSt5dequeIN12v8_inspector8String16ESaIS1_EE19_M_destroy_data_auxESt15_Deque_iteratorIS1_RS1_PS1_ES7_, .-_ZNSt5dequeIN12v8_inspector8String16ESaIS1_EE19_M_destroy_data_auxESt15_Deque_iteratorIS1_RS1_PS1_ES7_
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl7disableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl7disableEv
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl7disableEv, @function
_ZN12v8_inspector19V8DebuggerAgentImpl7disableEv:
.LFB7911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 32(%rsi)
	je	.L3704
	movq	40(%rsi), %r12
	leaq	-96(%rbp), %r13
	movq	%rsi, %rbx
	leaq	-80(%rbp), %r14
	leaq	_ZN12v8_inspector18DebuggerAgentStateL18breakpointsByRegexE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue6removeERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3604
	call	_ZdlPv@PLT
.L3604:
	movq	40(%rbx), %r12
	leaq	_ZN12v8_inspector18DebuggerAgentStateL16breakpointsByUrlE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue6removeERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3605
	call	_ZdlPv@PLT
.L3605:
	movq	40(%rbx), %r12
	leaq	_ZN12v8_inspector18DebuggerAgentStateL23breakpointsByScriptHashE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue6removeERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3606
	call	_ZdlPv@PLT
.L3606:
	movq	40(%rbx), %r12
	leaq	_ZN12v8_inspector18DebuggerAgentStateL15breakpointHintsE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue6removeERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3607
	call	_ZdlPv@PLT
.L3607:
	movq	40(%rbx), %r12
	leaq	_ZN12v8_inspector18DebuggerAgentStateL26instrumentationBreakpointsE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue6removeERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3608
	call	_ZdlPv@PLT
.L3608:
	movq	40(%rbx), %r12
	leaq	_ZN12v8_inspector18DebuggerAgentStateL22pauseOnExceptionsStateE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setIntegerERKNS_8String16Ei@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3609
	call	_ZdlPv@PLT
.L3609:
	movq	40(%rbx), %r12
	leaq	_ZN12v8_inspector18DebuggerAgentStateL19asyncCallStackDepthE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setIntegerERKNS_8String16Ei@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3610
	call	_ZdlPv@PLT
.L3610:
	cmpb	$0, 409(%rbx)
	jne	.L3705
.L3611:
	movq	440(%rbx), %r15
	testq	%r15, %r15
	jne	.L3616
	jmp	.L3612
	.p2align 4,,10
	.p2align 3
.L3706:
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L3612
.L3615:
	movq	%r12, %r15
.L3616:
	movq	48(%r15), %rdi
	movq	(%r15), %r12
	testq	%rdi, %rdi
	je	.L3613
	call	_ZdlPv@PLT
.L3613:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	jne	.L3706
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L3615
.L3612:
	movq	432(%rbx), %rax
	movq	424(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	416(%rbx), %r12
	movq	$0, 448(%rbx)
	movq	$0, 440(%rbx)
	movq	$0, 416(%rbx)
	testq	%r12, %r12
	je	.L3617
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3618
	call	_ZdlPv@PLT
.L3618:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3619
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L3619:
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3617:
	movq	80(%rbx), %r12
	testq	%r12, %r12
	je	.L3620
	.p2align 4,,10
	.p2align 3
.L3621:
	movq	48(%r12), %rdi
	movq	(%rdi), %rax
	call	*128(%rax)
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L3621
	movq	80(%rbx), %r15
	testq	%r15, %r15
	jne	.L3625
	jmp	.L3620
	.p2align 4,,10
	.p2align 3
.L3707:
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L3620
.L3624:
	movq	%r12, %r15
.L3625:
	movq	48(%r15), %rdi
	movq	(%r15), %r12
	testq	%rdi, %rdi
	je	.L3622
	movq	(%rdi), %rax
	call	*8(%rax)
.L3622:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	jne	.L3707
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L3624
.L3620:
	movq	72(%rbx), %rax
	movq	64(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	320(%rbx), %rax
	movq	$0, 88(%rbx)
	leaq	-160(%rbp), %rdx
	movq	$0, 80(%rbx)
	movq	344(%rbx), %r15
	leaq	304(%rbx), %rdi
	movq	%rax, -176(%rbp)
	movq	328(%rbx), %rax
	movdqu	352(%rbx), %xmm2
	movdqu	368(%rbx), %xmm3
	movq	%r15, -200(%rbp)
	addq	$8, %r15
	movq	%rax, -184(%rbp)
	movq	336(%rbx), %rax
	movdqu	320(%rbx), %xmm4
	movdqu	336(%rbx), %xmm5
	movaps	%xmm2, -160(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -208(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	call	_ZNSt5dequeIN12v8_inspector8String16ESaIS1_EE19_M_destroy_data_auxESt15_Deque_iteratorIS1_RS1_PS1_ES7_
	movq	376(%rbx), %rax
	leaq	8(%rax), %r12
	cmpq	%r15, %r12
	jbe	.L3626
	.p2align 4,,10
	.p2align 3
.L3627:
	movq	(%r15), %rdi
	addq	$8, %r15
	call	_ZdlPv@PLT
	cmpq	%r15, %r12
	ja	.L3627
.L3626:
	movq	-176(%rbp), %xmm0
	movq	192(%rbx), %r12
	movq	$0, 296(%rbx)
	movhps	-184(%rbp), %xmm0
	movups	%xmm0, 352(%rbx)
	movq	-192(%rbp), %xmm0
	movhps	-200(%rbp), %xmm0
	movups	%xmm0, 368(%rbx)
	testq	%r12, %r12
	je	.L3628
	.p2align 4,,10
	.p2align 3
.L3629:
	movl	8(%r12), %esi
	movq	56(%rbx), %rdi
	call	_ZN2v85debug16RemoveBreakpointEPNS_7IsolateEi@PLT
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L3629
.L3628:
	movq	136(%rbx), %r15
	testq	%r15, %r15
	jne	.L3634
	jmp	.L3630
	.p2align 4,,10
	.p2align 3
.L3708:
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L3630
.L3633:
	movq	%r12, %r15
.L3634:
	movq	48(%r15), %rdi
	movq	(%r15), %r12
	testq	%rdi, %rdi
	je	.L3631
	call	_ZdlPv@PLT
.L3631:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	jne	.L3708
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L3633
.L3630:
	movq	128(%rbx), %rax
	movq	120(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	192(%rbx), %r12
	movq	$0, 144(%rbx)
	movq	$0, 136(%rbx)
	testq	%r12, %r12
	jne	.L3638
	jmp	.L3635
	.p2align 4,,10
	.p2align 3
.L3709:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	je	.L3635
.L3637:
	movq	%r15, %r12
.L3638:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rdx
	movq	(%r12), %r15
	cmpq	%rdx, %rdi
	jne	.L3709
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	jne	.L3637
.L3635:
	movq	184(%rbx), %rax
	movq	176(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	16(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	$0, 200(%rbx)
	movq	$0, 192(%rbx)
	call	_ZN12v8_inspector10V8Debugger22setAsyncCallStackDepthEPNS_19V8DebuggerAgentImplEi@PLT
	movq	400(%rbx), %rax
	pxor	%xmm1, %xmm1
	movq	$0, 400(%rbx)
	movdqu	384(%rbx), %xmm0
	movq	-208(%rbp), %rdi
	movups	%xmm1, 384(%rbx)
	movq	%rax, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	call	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EED1Ev
	movb	$0, 408(%rbx)
	movq	40(%rbx), %r12
	movq	%r13, %rdi
	leaq	_ZN12v8_inspector18DebuggerAgentStateL13skipAllPausesE(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3639
	call	_ZdlPv@PLT
.L3639:
	movq	40(%rbx), %r12
	leaq	_ZN12v8_inspector18DebuggerAgentStateL15blackboxPatternE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue6removeERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3640
	call	_ZdlPv@PLT
.L3640:
	movb	$0, 32(%rbx)
	movq	40(%rbx), %r12
	leaq	_ZN12v8_inspector18DebuggerAgentStateL15debuggerEnabledE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3641
	call	_ZdlPv@PLT
.L3641:
	movq	16(%rbx), %rdi
	call	_ZN12v8_inspector10V8Debugger7disableEv@PLT
	movq	-168(%rbp), %rdi
.L3704:
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3710
	movq	-168(%rbp), %rax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3705:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN12v8_inspector10V8Debugger20setBreakpointsActiveEb@PLT
	movb	$0, 409(%rbx)
	jmp	.L3611
.L3710:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7911:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl7disableEv, .-_ZN12v8_inspector19V8DebuggerAgentImpl7disableEv
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImplD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImplD2Ev
	.type	_ZN12v8_inspector19V8DebuggerAgentImplD2Ev, @function
_ZN12v8_inspector19V8DebuggerAgentImplD2Ev:
.LFB7887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	440(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN12v8_inspector19V8DebuggerAgentImplE(%rip), %rax
	movq	%rax, (%rdi)
	testq	%r12, %r12
	jne	.L3712
	jmp	.L3717
	.p2align 4,,10
	.p2align 3
.L3825:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L3717
.L3718:
	movq	%r13, %r12
.L3712:
	movq	48(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L3715
	call	_ZdlPv@PLT
.L3715:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L3825
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L3718
.L3717:
	movq	432(%rbx), %rax
	movq	424(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	424(%rbx), %rdi
	leaq	472(%rbx), %rax
	movq	$0, 448(%rbx)
	movq	$0, 440(%rbx)
	cmpq	%rax, %rdi
	je	.L3713
	call	_ZdlPv@PLT
.L3713:
	movq	416(%rbx), %r12
	testq	%r12, %r12
	je	.L3719
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3720
	call	_ZdlPv@PLT
.L3720:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3721
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L3721:
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3719:
	movq	392(%rbx), %r13
	movq	384(%rbx), %r12
	cmpq	%r12, %r13
	je	.L3722
	.p2align 4,,10
	.p2align 3
.L3727:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3723
	movq	(%rdi), %rax
	call	*24(%rax)
.L3723:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3724
	call	_ZdlPv@PLT
	addq	$48, %r12
	cmpq	%r12, %r13
	jne	.L3727
.L3725:
	movq	384(%rbx), %r12
.L3722:
	testq	%r12, %r12
	je	.L3728
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3728:
	movdqu	352(%rbx), %xmm0
	movdqu	368(%rbx), %xmm1
	leaq	304(%rbx), %rdi
	leaq	-112(%rbp), %rdx
	movdqu	320(%rbx), %xmm2
	movdqu	336(%rbx), %xmm3
	leaq	-80(%rbp), %rsi
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm3, -64(%rbp)
	call	_ZNSt5dequeIN12v8_inspector8String16ESaIS1_EE19_M_destroy_data_auxESt15_Deque_iteratorIS1_RS1_PS1_ES7_
	movq	304(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3729
	movq	376(%rbx), %rax
	movq	344(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L3730
	.p2align 4,,10
	.p2align 3
.L3731:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L3731
	movq	304(%rbx), %rdi
.L3730:
	call	_ZdlPv@PLT
.L3729:
	movq	248(%rbx), %r12
	testq	%r12, %r12
	jne	.L3732
	jmp	.L3736
	.p2align 4,,10
	.p2align 3
.L3826:
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L3736
.L3737:
	movq	%r13, %r12
.L3732:
	movq	16(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	jne	.L3826
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L3737
.L3736:
	movq	240(%rbx), %rax
	movq	232(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	232(%rbx), %rdi
	leaq	280(%rbx), %rax
	movq	$0, 256(%rbx)
	movq	$0, 248(%rbx)
	cmpq	%rax, %rdi
	je	.L3733
	call	_ZdlPv@PLT
.L3733:
	movq	192(%rbx), %r12
	testq	%r12, %r12
	jne	.L3738
	jmp	.L3742
	.p2align 4,,10
	.p2align 3
.L3827:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L3742
.L3743:
	movq	%r13, %r12
.L3738:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	movq	(%r12), %r13
	cmpq	%rax, %rdi
	jne	.L3827
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L3743
.L3742:
	movq	184(%rbx), %rax
	movq	176(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	176(%rbx), %rdi
	leaq	224(%rbx), %rax
	movq	$0, 200(%rbx)
	movq	$0, 192(%rbx)
	cmpq	%rax, %rdi
	je	.L3739
	call	_ZdlPv@PLT
.L3739:
	movq	136(%rbx), %r12
	testq	%r12, %r12
	jne	.L3744
	jmp	.L3749
	.p2align 4,,10
	.p2align 3
.L3828:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L3749
.L3750:
	movq	%r13, %r12
.L3744:
	movq	48(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L3747
	call	_ZdlPv@PLT
.L3747:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L3828
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L3750
.L3749:
	movq	128(%rbx), %rax
	movq	120(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	120(%rbx), %rdi
	leaq	168(%rbx), %rax
	movq	$0, 144(%rbx)
	movq	$0, 136(%rbx)
	cmpq	%rax, %rdi
	je	.L3745
	call	_ZdlPv@PLT
.L3745:
	movq	80(%rbx), %r12
	testq	%r12, %r12
	jne	.L3751
	jmp	.L3756
	.p2align 4,,10
	.p2align 3
.L3829:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L3756
.L3757:
	movq	%r13, %r12
.L3751:
	movq	48(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L3754
	movq	(%rdi), %rax
	call	*8(%rax)
.L3754:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L3829
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L3757
.L3756:
	movq	72(%rbx), %rax
	movq	64(%rbx), %rdi
	xorl	%esi, %esi
	addq	$112, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L3711
	call	_ZdlPv@PLT
.L3711:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3830
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3724:
	.cfi_restore_state
	addq	$48, %r12
	cmpq	%r12, %r13
	jne	.L3727
	jmp	.L3725
.L3830:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7887:
	.size	_ZN12v8_inspector19V8DebuggerAgentImplD2Ev, .-_ZN12v8_inspector19V8DebuggerAgentImplD2Ev
	.globl	_ZN12v8_inspector19V8DebuggerAgentImplD1Ev
	.set	_ZN12v8_inspector19V8DebuggerAgentImplD1Ev,_ZN12v8_inspector19V8DebuggerAgentImplD2Ev
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImplD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImplD0Ev
	.type	_ZN12v8_inspector19V8DebuggerAgentImplD0Ev, @function
_ZN12v8_inspector19V8DebuggerAgentImplD0Ev:
.LFB7889:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN12v8_inspector19V8DebuggerAgentImplD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$480, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7889:
	.size	_ZN12v8_inspector19V8DebuggerAgentImplD0Ev, .-_ZN12v8_inspector19V8DebuggerAgentImplD0Ev
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl5resetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl5resetEv
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl5resetEv, @function
_ZN12v8_inspector19V8DebuggerAgentImpl5resetEv:
.LFB8119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 32(%rdi)
	jne	.L3896
.L3833:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3897
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3896:
	.cfi_restore_state
	movq	440(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	jne	.L3839
	jmp	.L3835
	.p2align 4,,10
	.p2align 3
.L3898:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L3835
.L3838:
	movq	%r13, %r12
.L3839:
	movq	48(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L3836
	call	_ZdlPv@PLT
.L3836:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L3898
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L3838
.L3835:
	movq	432(%rbx), %rax
	movq	424(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	80(%rbx), %r12
	movq	$0, 448(%rbx)
	movq	$0, 440(%rbx)
	testq	%r12, %r12
	je	.L3840
	.p2align 4,,10
	.p2align 3
.L3841:
	movq	48(%r12), %rdi
	movq	(%rdi), %rax
	call	*128(%rax)
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L3841
	movq	80(%rbx), %r12
	testq	%r12, %r12
	jne	.L3845
	jmp	.L3840
	.p2align 4,,10
	.p2align 3
.L3899:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L3840
.L3844:
	movq	%r13, %r12
.L3845:
	movq	48(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L3842
	movq	(%rdi), %rax
	call	*8(%rax)
.L3842:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L3899
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L3844
.L3840:
	movq	72(%rbx), %rax
	movq	64(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	328(%rbx), %rax
	movq	$0, 88(%rbx)
	leaq	-128(%rbp), %rdx
	movq	$0, 80(%rbx)
	movdqu	352(%rbx), %xmm1
	leaq	-96(%rbp), %rsi
	leaq	304(%rbx), %rdi
	movq	%rax, -136(%rbp)
	movdqu	368(%rbx), %xmm2
	movq	336(%rbx), %rax
	movdqu	320(%rbx), %xmm3
	movaps	%xmm1, -128(%rbp)
	movdqu	336(%rbx), %xmm4
	movq	344(%rbx), %r15
	movaps	%xmm2, -112(%rbp)
	movq	%rax, -144(%rbp)
	movq	320(%rbx), %r14
	movaps	%xmm3, -96(%rbp)
	leaq	8(%r15), %r13
	movaps	%xmm4, -80(%rbp)
	call	_ZNSt5dequeIN12v8_inspector8String16ESaIS1_EE19_M_destroy_data_auxESt15_Deque_iteratorIS1_RS1_PS1_ES7_
	movq	376(%rbx), %rax
	leaq	8(%rax), %r12
	cmpq	%r13, %r12
	jbe	.L3846
	.p2align 4,,10
	.p2align 3
.L3847:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %r12
	ja	.L3847
.L3846:
	movq	%r14, %xmm0
	movq	%r15, %xmm5
	movq	136(%rbx), %r12
	movq	$0, 296(%rbx)
	movhps	-136(%rbp), %xmm0
	movups	%xmm0, 352(%rbx)
	movq	-144(%rbp), %xmm0
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 368(%rbx)
	testq	%r12, %r12
	jne	.L3852
	jmp	.L3848
	.p2align 4,,10
	.p2align 3
.L3900:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L3848
.L3851:
	movq	%r13, %r12
.L3852:
	movq	48(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L3849
	call	_ZdlPv@PLT
.L3849:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L3900
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L3851
.L3848:
	movq	128(%rbx), %rax
	movq	120(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, 144(%rbx)
	movq	$0, 136(%rbx)
	jmp	.L3833
.L3897:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8119:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl5resetEv, .-_ZN12v8_inspector19V8DebuggerAgentImpl5resetEv
	.section	.text._ZNSt10_HashtableIiSt4pairIKiN12v8_inspector8String16EESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector8String16EESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector8String16EESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector8String16EESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector8String16EESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm:
.LFB13187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L3902
	movq	(%rbx), %r8
.L3903:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L3912
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L3913:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3902:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L3926
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L3927
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L3905:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L3907
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L3908
	.p2align 4,,10
	.p2align 3
.L3909:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L3910:
	testq	%rsi, %rsi
	je	.L3907
.L3908:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L3909
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L3915
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L3908
	.p2align 4,,10
	.p2align 3
.L3907:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L3911
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L3911:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L3903
	.p2align 4,,10
	.p2align 3
.L3912:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L3914
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L3914:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L3913
	.p2align 4,,10
	.p2align 3
.L3915:
	movq	%rdx, %rdi
	jmp	.L3910
	.p2align 4,,10
	.p2align 3
.L3926:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L3905
.L3927:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE13187:
	.size	_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector8String16EESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector8String16EESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	.section	.text._ZNSt8__detail9_Map_baseIiSt4pairIKiN12v8_inspector8String16EESaIS5_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_,"axG",@progbits,_ZNSt8__detail9_Map_baseIiSt4pairIKiN12v8_inspector8String16EESaIS5_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIiSt4pairIKiN12v8_inspector8String16EESaIS5_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	.type	_ZNSt8__detail9_Map_baseIiSt4pairIKiN12v8_inspector8String16EESaIS5_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_, @function
_ZNSt8__detail9_Map_baseIiSt4pairIKiN12v8_inspector8String16EESaIS5_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_:
.LFB11230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	(%rsi), %r14
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	%r14, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r13
	testq	%rax, %rax
	je	.L3929
	movq	(%rax), %rcx
	movq	%r14, %r8
	movl	8(%rcx), %esi
	jmp	.L3931
	.p2align 4,,10
	.p2align 3
.L3941:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L3929
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%rdx, %r13
	jne	.L3929
.L3931:
	cmpl	%esi, %r8d
	jne	.L3941
	popq	%rbx
	leaq	16(%rcx), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3929:
	.cfi_restore_state
	movl	$56, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movl	(%rbx), %eax
	movq	%r12, %rdi
	movq	$0, 48(%rcx)
	movl	$1, %r8d
	movl	%eax, 8(%rcx)
	leaq	32(%rcx), %rax
	movq	%rax, 16(%rcx)
	movq	$0, 24(%rcx)
	movups	%xmm0, 32(%rcx)
	call	_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector8String16EESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	popq	%rbx
	popq	%r12
	addq	$16, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11230:
	.size	_ZNSt8__detail9_Map_baseIiSt4pairIKiN12v8_inspector8String16EESaIS5_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_, .-_ZNSt8__detail9_Map_baseIiSt4pairIKiN12v8_inspector8String16EESaIS5_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	.section	.text._ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb1EEEm
	.type	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb1EEEm, @function
_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb1EEEm:
.LFB13214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L3966
.L3943:
	movq	%r14, 72(%r12)
	movq	(%rbx), %rax
	leaq	0(,%r15,8), %rcx
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.L3952
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r12, (%rax)
.L3953:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3966:
	.cfi_restore_state
	movq	%rdx, %r13
	cmpq	$1, %rdx
	je	.L3967
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L3968
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L3945:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L3947
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L3948
	.p2align 4,,10
	.p2align 3
.L3949:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L3950:
	testq	%rsi, %rsi
	je	.L3947
.L3948:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	72(%rcx), %rax
	divq	%r13
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L3949
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L3955
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L3948
	.p2align 4,,10
	.p2align 3
.L3947:
	movq	(%rbx), %rdi
	cmpq	%r9, %rdi
	je	.L3951
	call	_ZdlPv@PLT
.L3951:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r15, (%rbx)
	divq	%r13
	movq	%r13, 8(%rbx)
	movq	%rdx, %r15
	jmp	.L3943
	.p2align 4,,10
	.p2align 3
.L3952:
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L3954
	movq	72(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L3954:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L3953
	.p2align 4,,10
	.p2align 3
.L3955:
	movq	%rdx, %rdi
	jmp	.L3950
	.p2align 4,,10
	.p2align 3
.L3967:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L3945
.L3968:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE13214:
	.size	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb1EEEm, .-_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb1EEEm
	.section	.text._ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St6vectorIiSaIiEEESaIS8_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_,"axG",@progbits,_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St6vectorIiSaIiEEESaIS8_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St6vectorIiSaIiEEESaIS8_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	.type	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St6vectorIiSaIiEEESaIS8_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_, @function
_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St6vectorIiSaIiEEESaIS8_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_:
.LFB11237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rsi), %r12
	testq	%r12, %r12
	jne	.L3970
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rcx
	cmpq	%rcx, %rax
	je	.L3973
	.p2align 4,,10
	.p2align 3
.L3972:
	movq	%r12, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%r12, %rdx
	movq	%rdx, %r12
	movsbq	-2(%rax), %rdx
	addq	%rdx, %r12
	movq	%r12, 32(%r14)
	cmpq	%rax, %rcx
	jne	.L3972
	testq	%r12, %r12
	je	.L3973
.L3970:
	movq	8(%r13), %r15
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%r15
	movq	0(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L3974
	movq	(%rax), %rbx
	movq	72(%rbx), %rcx
.L3977:
	cmpq	%rcx, %r12
	je	.L3992
.L3975:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3974
	movq	72(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r15
	cmpq	%rdx, %r9
	je	.L3977
.L3974:
	movl	$80, %edi
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	(%r14), %rsi
	movq	%rax, %r15
	movq	$0, (%rax)
	leaq	8(%rax), %rdi
	leaq	24(%rax), %rax
	movq	%rax, 8(%r15)
	movq	8(%r14), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r14), %rax
	movq	-56(%rbp), %r9
	movq	%r15, %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$1, %r8d
	movq	%rax, 40(%r15)
	movq	%r9, %rsi
	movq	$0, 64(%r15)
	movups	%xmm0, 48(%r15)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb1EEEm
	addq	$24, %rsp
	popq	%rbx
	addq	$48, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3992:
	.cfi_restore_state
	leaq	8(%rbx), %rsi
	movq	%r14, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	jne	.L3975
	addq	$24, %rsp
	leaq	48(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3973:
	.cfi_restore_state
	movq	$1, 32(%r14)
	movl	$1, %r12d
	jmp	.L3970
	.cfi_endproc
.LFE11237:
	.size	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St6vectorIiSaIiEEESaIS8_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_, .-_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St6vectorIiSaIiEEESaIS8_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl17setBreakpointImplERKNS_8String16ES3_S3_ii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl17setBreakpointImplERKNS_8String16ES3_S3_ii
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl17setBreakpointImplERKNS_8String16ES3_S3_ii, @function
_ZN12v8_inspector19V8DebuggerAgentImpl17setBreakpointImplERKNS_8String16ES3_S3_ii:
.LFB7953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%rdx, -120(%rbp)
	movq	56(%rsi), %rsi
	movq	%r8, -112(%rbp)
	movl	%r9d, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	64(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L4004
	movq	48(%rax), %r13
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	cmpl	-104(%rbp), %eax
	jle	.L3996
.L4004:
	movq	$0, (%r12)
.L3995:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4005
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3996:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*56(%rax)
	cmpl	-104(%rbp), %eax
	jl	.L4004
	leaq	-92(%rbp), %rax
	movl	16(%rbp), %edx
	movl	-104(%rbp), %esi
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v85debug8LocationC1Eii@PLT
	movq	8(%rbx), %rdi
	movl	92(%r13), %esi
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4004
	call	_ZNK12v8_inspector16InspectedContext7contextEv@PLT
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	leaq	-96(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, -136(%rbp)
	movq	%rax, %rcx
	movq	0(%r13), %rax
	movq	-112(%rbp), %rsi
	call	*152(%rax)
	movq	-104(%rbp), %r9
	testb	%al, %al
	je	.L4006
	movq	%r9, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-136(%rbp), %rsi
	leaq	176(%rbx), %rdi
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiN12v8_inspector8String16EESaIS5_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	movq	-120(%rbp), %r13
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r13), %rdx
	movq	-104(%rbp), %rax
	movq	%r13, %rsi
	leaq	120(%rbx), %rdi
	movq	%rdx, 32(%rax)
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St6vectorIiSaIiEEESaIS8_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L4000
	movl	-96(%rbp), %edx
	movl	%edx, (%rsi)
	addq	$4, 8(%rax)
.L4001:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	%r14, %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rax
	leaq	8(%rbx), %rdi
	movq	%rax, 8(%rbx)
	xorl	%eax, %eax
	movw	%ax, 24(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 40(%rbx)
	movb	$0, 52(%rbx)
	movl	$0, 56(%rbx)
	movl	$0, 48(%rbx)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r14), %rax
	movq	-128(%rbp), %r14
	movq	%rax, 40(%rbx)
	movq	%r14, %rdi
	call	_ZNK2v85debug8Location13GetLineNumberEv@PLT
	movq	%r14, %rdi
	movl	%eax, 48(%rbx)
	call	_ZNK2v85debug8Location15GetColumnNumberEv@PLT
	movb	$1, 52(%rbx)
	movl	%eax, 56(%rbx)
	movq	%rbx, (%r12)
	jmp	.L3995
	.p2align 4,,10
	.p2align 3
.L4006:
	movq	$0, (%r12)
	movq	%r9, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	jmp	.L3995
.L4000:
	movq	-136(%rbp), %rdx
	movq	%rax, %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	jmp	.L4001
.L4005:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7953:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl17setBreakpointImplERKNS_8String16ES3_S3_ii, .-_ZN12v8_inspector19V8DebuggerAgentImpl17setBreakpointImplERKNS_8String16ES3_S3_ii
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl18setBreakpointByUrlEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEES6_S6_NS4_IiEES6_PS5_PSt10unique_ptrISt6vectorIS9_INS_8protocol8Debugger8LocationESt14default_deleteISD_EESaISG_EESE_ISI_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"Either url or urlRegex or scriptHash must be specified."
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl18setBreakpointByUrlEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEES6_S6_NS4_IiEES6_PS5_PSt10unique_ptrISt6vectorIS9_INS_8protocol8Debugger8LocationESt14default_deleteISD_EESaISG_EESE_ISI_EE.str1.1,"aMS",@progbits,1
.LC21:
	.string	"Incorrect column number"
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl18setBreakpointByUrlEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEES6_S6_NS4_IiEES6_PS5_PSt10unique_ptrISt6vectorIS9_INS_8protocol8Debugger8LocationESt14default_deleteISD_EESaISG_EESE_ISI_EE.str1.8
	.align 8
.LC22:
	.string	"Breakpoint at specified location already exists."
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl18setBreakpointByUrlEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEES6_S6_NS4_IiEES6_PS5_PSt10unique_ptrISt6vectorIS9_INS_8protocol8Debugger8LocationESt14default_deleteISD_EESaISG_EESE_ISI_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl18setBreakpointByUrlEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEES6_S6_NS4_IiEES6_PS5_PSt10unique_ptrISt6vectorIS9_INS_8protocol8Debugger8LocationESt14default_deleteISD_EESaISG_EESE_ISI_EE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl18setBreakpointByUrlEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEES6_S6_NS4_IiEES6_PS5_PSt10unique_ptrISt6vectorIS9_INS_8protocol8Debugger8LocationESt14default_deleteISD_EESaISG_EESE_ISI_EE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl18setBreakpointByUrlEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEES6_S6_NS4_IiEES6_PS5_PSt10unique_ptrISt6vectorIS9_INS_8protocol8Debugger8LocationESt14default_deleteISD_EESaISG_EESE_ISI_EE:
.LFB7919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$456, %rsp
	movq	16(%rbp), %rax
	movq	40(%rbp), %r14
	movq	%rdi, -400(%rbp)
	movq	%rsi, -424(%rbp)
	movl	$24, %edi
	movq	%rax, -432(%rbp)
	movq	24(%rbp), %rax
	movl	%edx, -388(%rbp)
	movq	%rax, -448(%rbp)
	movq	32(%rbp), %rax
	movq	%rcx, -408(%rbp)
	movq	%r9, -416(%rbp)
	movq	%rax, -464(%rbp)
	movq	%r14, -440(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	(%r14), %r13
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, (%r14)
	movups	%xmm0, (%rax)
	testq	%r13, %r13
	je	.L4008
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	jne	.L4013
	testq	%r12, %r12
	je	.L4014
	.p2align 4,,10
	.p2align 3
.L4114:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4014:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4008:
	movq	-408(%rbp), %rax
	movq	-416(%rbp), %rcx
	leaq	-96(%rbp), %r12
	movzbl	(%rbx), %r13d
	movzbl	(%rax), %esi
	movzbl	(%rcx), %ecx
	movl	%r13d, %eax
	movl	%esi, %edx
	addl	%esi, %r13d
	movzbl	%cl, %esi
	addl	%esi, %r13d
	leaq	.LC20(%rip), %rsi
	cmpl	$1, %r13d
	jne	.L4106
	movq	-432(%rbp), %rsi
	movl	$0, -372(%rbp)
	cmpb	$0, (%rsi)
	jne	.L4107
.L4018:
	leaq	-336(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	$0, -328(%rbp)
	movq	%rsi, -432(%rbp)
	leaq	-320(%rbp), %rsi
	movq	%rsi, -480(%rbp)
	movq	%rsi, -336(%rbp)
	movw	%r8w, -320(%rbp)
	movq	$0, -304(%rbp)
	testb	%al, %al
	jne	.L4108
	testb	%dl, %dl
	jne	.L4109
	testb	%cl, %cl
	jne	.L4110
.L4021:
	leaq	-80(%rbp), %rax
	xorl	%edi, %edi
	leaq	-96(%rbp), %r12
	movq	$0, -88(%rbp)
	movq	%rax, -408(%rbp)
	movq	%rax, -96(%rbp)
	movq	-448(%rbp), %rax
	movw	%di, -80(%rbp)
	cmpb	$0, (%rax)
	movq	$0, -64(%rbp)
	je	.L4063
	movq	8(%rax), %rsi
	leaq	8(%rax), %rbx
	movq	16(%rax), %rax
	leaq	(%rsi,%rax,2), %rdx
.L4023:
	leaq	-272(%rbp), %rax
	leaq	-288(%rbp), %rdi
	movq	%rdi, -416(%rbp)
	movq	%rax, -488(%rbp)
	movq	%rax, -288(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%rbx), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -256(%rbp)
	cmpq	-408(%rbp), %rdi
	je	.L4024
	call	_ZdlPv@PLT
.L4024:
	leaq	-368(%rbp), %r14
	movl	-372(%rbp), %r15d
	movl	-388(%rbp), %ebx
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16Builder12appendNumberEi@PLT
	movl	$58, %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16Builder12appendNumberEi@PLT
	movl	$58, %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movl	%r15d, %esi
	movq	%r14, %rdi
	leaq	-240(%rbp), %r15
	call	_ZN12v8_inspector15String16Builder12appendNumberEi@PLT
	movl	$58, %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	-432(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4025
	call	_ZdlPv@PLT
.L4025:
	cmpl	$2, %r13d
	je	.L4026
	leaq	_ZN12v8_inspector18DebuggerAgentStateL23breakpointsByScriptHashE(%rip), %rsi
	cmpl	$3, %r13d
	je	.L4104
	leaq	_ZN12v8_inspector18DebuggerAgentStateL16breakpointsByUrlE(%rip), %rsi
.L4104:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-424(%rbp), %rax
	movq	%r12, %rsi
	movq	40(%rax), %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_117getOrCreateObjectEPNS_8protocol15DictionaryValueERKNS_8String16E
	movq	-432(%rbp), %rsi
	movq	%rax, %rdi
.L4102:
	call	_ZN12v8_inspector12_GLOBAL__N_117getOrCreateObjectEPNS_8protocol15DictionaryValueERKNS_8String16E
	movq	-96(%rbp), %rdi
	movq	%rax, -472(%rbp)
	cmpq	-408(%rbp), %rdi
	je	.L4030
	call	_ZdlPv@PLT
.L4030:
	movq	-472(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L4033
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-400(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-408(%rbp), %rdi
	je	.L4035
.L4099:
	call	_ZdlPv@PLT
.L4035:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4060
	call	_ZdlPv@PLT
.L4060:
	movq	-288(%rbp), %rdi
	cmpq	-488(%rbp), %rdi
	je	.L4061
	call	_ZdlPv@PLT
.L4061:
	movq	-336(%rbp), %rdi
	cmpq	-480(%rbp), %rdi
	je	.L4007
.L4100:
	call	_ZdlPv@PLT
.L4007:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4111
	movq	-400(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4113:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r15), %rdi
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L4012
	call	_ZdlPv@PLT
.L4012:
	movl	$64, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4010:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L4112
.L4013:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L4010
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L4113
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r12, %r14
	jne	.L4013
	.p2align 4,,10
	.p2align 3
.L4112:
	movq	0(%r13), %r12
	testq	%r12, %r12
	jne	.L4114
	jmp	.L4014
	.p2align 4,,10
	.p2align 3
.L4107:
	movl	4(%rsi), %esi
	movl	%esi, -372(%rbp)
	testl	%esi, %esi
	jns	.L4018
	leaq	-96(%rbp), %r12
	leaq	.LC21(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L4106:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-400(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4100
	jmp	.L4007
	.p2align 4,,10
	.p2align 3
.L4063:
	movq	-408(%rbp), %rsi
	movq	%r12, %rbx
	movq	%rsi, %rdx
	jmp	.L4023
	.p2align 4,,10
	.p2align 3
.L4033:
	leaq	-176(%rbp), %rax
	xorl	%esi, %esi
	movq	$0, -184(%rbp)
	movq	%rax, -496(%rbp)
	movq	%rax, -192(%rbp)
	movq	-424(%rbp), %rax
	movw	%si, -176(%rbp)
	movq	$0, -160(%rbp)
	movq	80(%rax), %rbx
	testq	%rbx, %rbx
	je	.L4040
	movq	%r14, -456(%rbp)
	movq	-424(%rbp), %r14
	movq	%r12, -448(%rbp)
	movq	-432(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L4036:
	movq	48(%rbx), %rsi
	movq	8(%r14), %rdi
	movq	%r12, %rcx
	movl	%r13d, %edx
	call	_ZN12v8_inspectorL7matchesEPNS_15V8InspectorImplERKNS_16V8DebuggerScriptENS_12_GLOBAL__N_114BreakpointTypeERKNS_8String16E
	testb	%al, %al
	je	.L4054
	cmpq	$0, -184(%rbp)
	jne	.L4115
.L4041:
	movl	-372(%rbp), %eax
	subq	$8, %rsp
	movq	%r15, %rdx
	movq	%r14, %rsi
	movl	-388(%rbp), %r9d
	movq	-416(%rbp), %r8
	leaq	8(%rbx), %rcx
	pushq	%rax
	movq	-456(%rbp), %rdi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl17setBreakpointImplERKNS_8String16ES3_S3_ii
	popq	%rdx
	movq	-368(%rbp), %rax
	popq	%rcx
	cmpl	$2, %r13d
	je	.L4042
	testq	%rax, %rax
	je	.L4054
	movq	48(%rbx), %r8
	movl	-372(%rbp), %edx
	movl	-388(%rbp), %esi
	movq	(%r8), %rax
	movq	%r8, -432(%rbp)
	movq	%r8, %rdi
	call	*136(%rax)
	movq	-432(%rbp), %r8
	cmpl	$-1, %eax
	je	.L4116
	movslq	%eax, %rdx
	movq	(%r8), %rax
	movq	%r8, %rsi
	movl	$128, %ecx
	movq	-448(%rbp), %rdi
	call	*24(%rax)
	leaq	-144(%rbp), %r8
	movq	-448(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -432(%rbp)
	call	_ZNK12v8_inspector8String1615stripWhiteSpaceEv@PLT
	movq	-96(%rbp), %rdi
	cmpq	-408(%rbp), %rdi
	movq	-432(%rbp), %r8
	je	.L4045
	call	_ZdlPv@PLT
	movq	-432(%rbp), %r8
.L4045:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rsi
	testq	%rdx, %rdx
	je	.L4046
	xorl	%ecx, %ecx
	jmp	.L4049
	.p2align 4,,10
	.p2align 3
.L4047:
	addq	$1, %rcx
	cmpq	%rdx, %rcx
	je	.L4046
.L4049:
	movzwl	(%rsi,%rcx,2), %eax
	cmpw	$59, %ax
	ja	.L4047
	movabsq	$576460752303432704, %rdi
	btq	%rax, %rdi
	jnc	.L4047
	movq	-448(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r8, %rsi
	call	_ZNK12v8_inspector8String169substringEmm
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4044
	call	_ZdlPv@PLT
.L4044:
	movq	-448(%rbp), %rsi
	leaq	-192(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -160(%rbp)
	cmpq	-408(%rbp), %rdi
	je	.L4052
	call	_ZdlPv@PLT
.L4052:
	movq	-368(%rbp), %rax
.L4042:
	testq	%rax, %rax
	je	.L4054
	movq	-440(%rbp), %rdx
	movq	(%rdx), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L4055
	movq	$0, -368(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%rdi)
.L4056:
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4054
	movq	(%rdi), %rax
	call	*24(%rax)
.L4054:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L4036
	movq	-448(%rbp), %r12
.L4040:
	movq	-416(%rbp), %rdx
	movq	-472(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue9setStringERKNS_8String16ES4_@PLT
	cmpq	$0, -184(%rbp)
	jne	.L4117
.L4038:
	movq	-464(%rbp), %rbx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-208(%rbp), %rax
	movq	-400(%rbp), %rdi
	movq	%rax, 32(%rbx)
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-192(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	jne	.L4099
	jmp	.L4035
	.p2align 4,,10
	.p2align 3
.L4117:
	movq	%r12, %rdi
	leaq	_ZN12v8_inspector18DebuggerAgentStateL15breakpointHintsE(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-424(%rbp), %rax
	movq	%r12, %rsi
	movq	40(%rax), %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_117getOrCreateObjectEPNS_8protocol15DictionaryValueERKNS_8String16E
	movq	-96(%rbp), %rdi
	movq	%rax, %r12
	cmpq	-408(%rbp), %rdi
	je	.L4058
	call	_ZdlPv@PLT
.L4058:
	leaq	-192(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue9setStringERKNS_8String16ES4_@PLT
	jmp	.L4038
	.p2align 4,,10
	.p2align 3
.L4110:
	movq	-416(%rbp), %rbx
	movq	-432(%rbp), %rdi
	movl	$3, %r13d
	leaq	8(%rbx), %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	40(%rbx), %rax
	movq	%rax, -304(%rbp)
	jmp	.L4021
	.p2align 4,,10
	.p2align 3
.L4115:
	movq	48(%rbx), %rdi
	leaq	-372(%rbp), %rcx
	leaq	-388(%rbp), %rdx
	leaq	-192(%rbp), %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_124adjustBreakpointLocationERKNS_16V8DebuggerScriptERKNS_8String16EPiS7_
	jmp	.L4041
	.p2align 4,,10
	.p2align 3
.L4026:
	leaq	_ZN12v8_inspector18DebuggerAgentStateL18breakpointsByRegexE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-424(%rbp), %rax
	movq	%r12, %rsi
	movq	40(%rax), %rdi
	jmp	.L4102
	.p2align 4,,10
	.p2align 3
.L4108:
	movq	-432(%rbp), %rdi
	leaq	8(%rbx), %rsi
	movl	$2, %r13d
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	40(%rbx), %rax
	movq	%rax, -304(%rbp)
	jmp	.L4021
	.p2align 4,,10
	.p2align 3
.L4116:
	movq	-408(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -88(%rbp)
	movq	$0, -64(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	jmp	.L4044
	.p2align 4,,10
	.p2align 3
.L4055:
	movq	-456(%rbp), %rdx
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger8LocationESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L4056
	.p2align 4,,10
	.p2align 3
.L4109:
	movq	-408(%rbp), %rbx
	movq	-432(%rbp), %rdi
	leaq	8(%rbx), %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	40(%rbx), %rax
	movq	%rax, -304(%rbp)
	jmp	.L4021
	.p2align 4,,10
	.p2align 3
.L4046:
	movq	-408(%rbp), %rax
	movq	%rax, -96(%rbp)
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rsi
	je	.L4118
	movq	-128(%rbp), %rax
	movq	%rsi, -96(%rbp)
	movq	%rax, -80(%rbp)
.L4051:
	movq	-112(%rbp), %rax
	movq	%rdx, -88(%rbp)
	movq	%rax, -64(%rbp)
	jmp	.L4044
	.p2align 4,,10
	.p2align 3
.L4118:
	movdqa	-128(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	jmp	.L4051
.L4111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7919:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl18setBreakpointByUrlEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEES6_S6_NS4_IiEES6_PS5_PSt10unique_ptrISt6vectorIS9_INS_8protocol8Debugger8LocationESt14default_deleteISD_EESaISG_EESE_ISI_EE, .-_ZN12v8_inspector19V8DebuggerAgentImpl18setBreakpointByUrlEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEES6_S6_NS4_IiEES6_PS5_PSt10unique_ptrISt6vectorIS9_INS_8protocol8Debugger8LocationESt14default_deleteISD_EESaISG_EESE_ISI_EE
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl13setBreakpointESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEEPSC_PS7_.str1.1,"aMS",@progbits,1
.LC23:
	.string	"Could not resolve breakpoint"
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl13setBreakpointESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEEPSC_PS7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl13setBreakpointESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEEPSC_PS7_
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl13setBreakpointESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEEPSC_PS7_, @function
_ZN12v8_inspector19V8DebuggerAgentImpl13setBreakpointESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEEPSC_PS7_:
.LFB7922:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -232(%rbp)
	movq	(%rdx), %r12
	movq	%rdx, -256(%rbp)
	movq	%rcx, -264(%rbp)
	movq	%r8, -272(%rbp)
	movq	%r9, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 52(%r12)
	je	.L4120
	movl	56(%r12), %r15d
.L4120:
	movq	8(%r12), %rsi
	movq	16(%r12), %rax
	leaq	-96(%rbp), %r13
	leaq	-80(%rbp), %rbx
	movl	48(%r12), %r8d
	movq	%r13, %rdi
	movq	%rbx, -96(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	movl	%r8d, -240(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	40(%r12), %rax
	leaq	-224(%rbp), %r12
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder12appendNumberEi@PLT
	movl	$58, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movl	-240(%rbp), %r8d
	movq	%r12, %rdi
	movl	%r8d, %esi
	call	_ZN12v8_inspector15String16Builder12appendNumberEi@PLT
	movl	$58, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	leaq	-192(%rbp), %r15
	call	_ZN12v8_inspector15String16Builder12appendNumberEi@PLT
	movl	$58, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4121
	call	_ZdlPv@PLT
.L4121:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4122
	call	_ZdlPv@PLT
.L4122:
	movq	-232(%rbp), %rax
	movq	%r15, %rsi
	leaq	120(%rax), %rdi
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	leaq	.LC22(%rip), %rsi
	testq	%rax, %rax
	je	.L4151
.L4150:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4125
	call	_ZdlPv@PLT
.L4125:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4119
	call	_ZdlPv@PLT
.L4119:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4152
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4151:
	.cfi_restore_state
	movq	-256(%rbp), %rax
	xorl	%r10d, %r10d
	movq	(%rax), %rax
	cmpb	$0, 52(%rax)
	je	.L4126
	movl	56(%rax), %r10d
.L4126:
	xorl	%edx, %edx
	movq	8(%rax), %rsi
	movl	48(%rax), %r9d
	movq	%rbx, -96(%rbp)
	movq	-264(%rbp), %rcx
	movw	%dx, -80(%rbp)
	leaq	-128(%rbp), %rdx
	movq	%rdx, -240(%rbp)
	cmpb	$0, (%rcx)
	movq	%rdx, -144(%rbp)
	leaq	8(%rcx), %r8
	leaq	-144(%rbp), %rcx
	movq	16(%rax), %rdx
	cmove	%r13, %r8
	movq	%rcx, %rdi
	movl	%r10d, -288(%rbp)
	movl	%r9d, -284(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%r8, -280(%rbp)
	movq	%rax, -264(%rbp)
	movq	%rcx, -256(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-264(%rbp), %rax
	subq	$8, %rsp
	movl	-288(%rbp), %r10d
	movq	-256(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r15, %rdx
	movq	-232(%rbp), %rsi
	movq	40(%rax), %rax
	movl	-284(%rbp), %r9d
	pushq	%r10
	movq	-280(%rbp), %r8
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector19V8DebuggerAgentImpl17setBreakpointImplERKNS_8String16ES3_S3_ii
	movq	-248(%rbp), %rcx
	movq	-224(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	(%rcx), %rdi
	movq	%rax, (%rcx)
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L4129
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4129
	movq	(%rdi), %rax
	call	*24(%rax)
.L4129:
	movq	-144(%rbp), %rdi
	cmpq	-240(%rbp), %rdi
	je	.L4131
	call	_ZdlPv@PLT
.L4131:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4132
	call	_ZdlPv@PLT
.L4132:
	movq	-248(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4153
	movq	-272(%rbp), %rbx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-160(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, 32(%rbx)
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L4125
	.p2align 4,,10
	.p2align 3
.L4153:
	leaq	.LC23(%rip), %rsi
	jmp	.L4150
.L4152:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7922:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl13setBreakpointESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEEPSC_PS7_, .-_ZN12v8_inspector19V8DebuggerAgentImpl13setBreakpointESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEEPSC_PS7_
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl17setBreakpointImplERKNS_8String16EN2v85LocalINS4_8FunctionEEENS5_INS4_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl17setBreakpointImplERKNS_8String16EN2v85LocalINS4_8FunctionEEENS5_INS4_6StringEEE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl17setBreakpointImplERKNS_8String16EN2v85LocalINS4_8FunctionEEENS5_INS4_6StringEEE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl17setBreakpointImplERKNS_8String16EN2v85LocalINS4_8FunctionEEENS5_INS4_6StringEEE:
.LFB7954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-44(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%rcx, %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	movq	%r13, %rdx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v85debug21SetFunctionBreakpointENS_5LocalINS_8FunctionEEENS1_INS_6StringEEEPi@PLT
	testb	%al, %al
	jne	.L4160
.L4154:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4161
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4160:
	.cfi_restore_state
	leaq	176(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiN12v8_inspector8String16EESaIS5_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r12), %rax
	movq	%r12, %rsi
	leaq	120(%rbx), %rdi
	movq	%rax, 32(%r14)
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St6vectorIiSaIiEEESaIS8_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L4156
	movl	-44(%rbp), %edx
	movl	%edx, (%rsi)
	addq	$4, 8(%rax)
	jmp	.L4154
	.p2align 4,,10
	.p2align 3
.L4156:
	movq	%r13, %rdx
	movq	%rax, %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	jmp	.L4154
.L4161:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7954:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl17setBreakpointImplERKNS_8String16EN2v85LocalINS4_8FunctionEEENS5_INS4_6StringEEE, .-_ZN12v8_inspector19V8DebuggerAgentImpl17setBreakpointImplERKNS_8String16EN2v85LocalINS4_8FunctionEEENS5_INS4_6StringEEE
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl16setBreakpointForEN2v85LocalINS1_8FunctionEEENS2_INS1_6StringEEENS0_16BreakpointSourceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl16setBreakpointForEN2v85LocalINS1_8FunctionEEENS2_INS1_6StringEEENS0_16BreakpointSourceE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl16setBreakpointForEN2v85LocalINS1_8FunctionEEENS2_INS1_6StringEEENS0_16BreakpointSourceE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl16setBreakpointForEN2v85LocalINS1_8FunctionEEENS2_INS1_6StringEEENS0_16BreakpointSourceE:
.LFB8117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	movq	%r12, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %ecx
	setne	%bl
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	addl	$5, %ebx
	movq	%r12, %rdi
	movl	%ebx, %esi
	leaq	-96(%rbp), %rbx
	call	_ZN12v8_inspector15String16Builder12appendNumberEi@PLT
	movl	$58, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	%r14, %rdi
	call	_ZN2v85debug14GetDebuggingIdENS_5LocalINS_8FunctionEEE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector15String16Builder12appendNumberEi@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4164
	call	_ZdlPv@PLT
.L4164:
	leaq	120(%r13), %rdi
	movq	%rbx, %rsi
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L4165
.L4177:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4162
	call	_ZdlPv@PLT
.L4162:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4178
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4165:
	.cfi_restore_state
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl17setBreakpointImplERKNS_8String16EN2v85LocalINS4_8FunctionEEENS5_INS4_6StringEEE
	jmp	.L4177
.L4178:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8117:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl16setBreakpointForEN2v85LocalINS1_8FunctionEEENS2_INS1_6StringEEENS0_16BreakpointSourceE, .-_ZN12v8_inspector19V8DebuggerAgentImpl16setBreakpointForEN2v85LocalINS1_8FunctionEEENS2_INS1_6StringEEENS0_16BreakpointSourceE
	.section	.text._ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE17_M_realloc_insertIJS9_EEEvN9__gnu_cxx17__normal_iteratorIPS9_SB_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE17_M_realloc_insertIJS9_EEEvN9__gnu_cxx17__normal_iteratorIPS9_SB_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE17_M_realloc_insertIJS9_EEEvN9__gnu_cxx17__normal_iteratorIPS9_SB_EEDpOT_
	.type	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE17_M_realloc_insertIJS9_EEEvN9__gnu_cxx17__normal_iteratorIPS9_SB_EEDpOT_, @function
_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE17_M_realloc_insertIJS9_EEEvN9__gnu_cxx17__normal_iteratorIPS9_SB_EEDpOT_:
.LFB13289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movabsq	$-6148914691236517205, %rdx
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %r14
	movq	%rsi, -56(%rbp)
	movabsq	$192153584101141162, %rsi
	movq	%rdi, -80(%rbp)
	movq	%rbx, %rax
	subq	%r14, %rax
	sarq	$4, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	je	.L4211
	movq	-56(%rbp), %rdx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L4200
	movabsq	$9223372036854775776, %rcx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L4212
.L4181:
	movq	%rcx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%rax, -64(%rbp)
	leaq	48(%rax), %r8
	addq	%rax, %rcx
	movq	%rcx, -72(%rbp)
.L4199:
	movq	-64(%rbp), %rax
	movq	(%r12), %rcx
	addq	%rdx, %rax
	leaq	16(%rax), %rdx
	movq	%rdx, (%rax)
	leaq	16(%r12), %rdx
	cmpq	%rdx, %rcx
	je	.L4213
	movq	%rcx, (%rax)
	movq	16(%r12), %rcx
	movq	%rcx, 16(%rax)
.L4184:
	movq	%rdx, (%r12)
	xorl	%edx, %edx
	movq	8(%r12), %rcx
	movw	%dx, 16(%r12)
	movq	32(%r12), %rdx
	movq	%rcx, 8(%rax)
	movq	%rdx, 32(%rax)
	movq	40(%r12), %rdx
	movq	$0, 8(%r12)
	movq	%rdx, 40(%rax)
	movq	-56(%rbp), %rax
	movq	$0, 40(%r12)
	cmpq	%r14, %rax
	je	.L4185
	leaq	-48(%rax), %rdx
	movq	-64(%rbp), %r15
	leaq	16(%r14), %r13
	movabsq	$768614336404564651, %rcx
	subq	%r14, %rdx
	shrq	$4, %rdx
	imulq	%rcx, %rdx
	movabsq	$1152921504606846975, %rcx
	andq	%rcx, %rdx
	movq	%rdx, -88(%rbp)
	leaq	(%rdx,%rdx,2), %rdx
	salq	$4, %rdx
	leaq	64(%r14,%rdx), %r12
	.p2align 4,,10
	.p2align 3
.L4192:
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	je	.L4214
.L4186:
	movq	%rcx, (%r15)
	movq	0(%r13), %rcx
	movq	%rcx, 16(%r15)
.L4187:
	movq	-8(%r13), %rcx
	xorl	%eax, %eax
	movq	%rcx, 8(%r15)
	movq	16(%r13), %rcx
	movq	%r13, -16(%r13)
	movq	$0, -8(%r13)
	movw	%ax, 0(%r13)
	movq	%rcx, 32(%r15)
	movq	24(%r13), %rcx
	movq	$0, 24(%r13)
	movq	%rcx, 40(%r15)
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L4188
	movq	(%rdi), %rcx
	call	*24(%rcx)
.L4188:
	movq	-16(%r13), %rdi
	cmpq	%r13, %rdi
	je	.L4189
	call	_ZdlPv@PLT
	addq	$48, %r13
	addq	$48, %r15
	cmpq	%r13, %r12
	jne	.L4192
.L4190:
	movq	-88(%rbp), %rax
	leaq	6(%rax,%rax,2), %r8
	salq	$4, %r8
	addq	-64(%rbp), %r8
.L4185:
	movq	-56(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L4193
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L4197:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rsi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rsi, %rcx
	je	.L4215
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	addq	$48, %rax
	addq	$48, %rdx
	movq	%rcx, -32(%rdx)
	movq	-40(%rax), %rcx
	movq	%rcx, -40(%rdx)
	movq	-16(%rax), %rcx
	movq	%rcx, -16(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rbx, %rax
	jne	.L4197
.L4195:
	movabsq	$768614336404564651, %rdx
	subq	-56(%rbp), %rbx
	leaq	-48(%rbx), %rax
	shrq	$4, %rax
	imulq	%rdx, %rax
	movabsq	$1152921504606846975, %rdx
	andq	%rdx, %rax
	leaq	3(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	%rax, %r8
.L4193:
	testq	%r14, %r14
	je	.L4198
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L4198:
	movq	-64(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%r8, %xmm3
	movq	-72(%rbp), %rbx
	punpcklqdq	%xmm3, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4189:
	.cfi_restore_state
	addq	$48, %r13
	addq	$48, %r15
	cmpq	%r13, %r12
	je	.L4190
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	jne	.L4186
.L4214:
	movdqu	0(%r13), %xmm1
	movups	%xmm1, 16(%r15)
	jmp	.L4187
	.p2align 4,,10
	.p2align 3
.L4215:
	movq	8(%rax), %rcx
	movdqu	16(%rax), %xmm2
	addq	$48, %rax
	addq	$48, %rdx
	movq	%rcx, -40(%rdx)
	movq	-16(%rax), %rcx
	movups	%xmm2, -32(%rdx)
	movq	%rcx, -16(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L4197
	jmp	.L4195
	.p2align 4,,10
	.p2align 3
.L4212:
	testq	%rdi, %rdi
	jne	.L4182
	movq	$0, -72(%rbp)
	movl	$48, %r8d
	movq	$0, -64(%rbp)
	jmp	.L4199
	.p2align 4,,10
	.p2align 3
.L4200:
	movl	$48, %ecx
	jmp	.L4181
	.p2align 4,,10
	.p2align 3
.L4213:
	movdqu	16(%r12), %xmm4
	movups	%xmm4, 16(%rax)
	jmp	.L4184
.L4182:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	imulq	$48, %rsi, %rcx
	jmp	.L4181
.L4211:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE13289:
	.size	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE17_M_realloc_insertIJS9_EEEvN9__gnu_cxx17__normal_iteratorIPS9_SB_EEDpOT_, .-_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE17_M_realloc_insertIJS9_EEEvN9__gnu_cxx17__normal_iteratorIPS9_SB_EEDpOT_
	.section	.text._ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE12emplace_backIJS9_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE12emplace_backIJS9_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE12emplace_backIJS9_EEEvDpOT_
	.type	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE12emplace_backIJS9_EEEvDpOT_, @function
_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE12emplace_backIJS9_EEEvDpOT_:
.LFB11305:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L4217
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	(%rdx), %rcx
	leaq	16(%rdx), %rax
	cmpq	%rax, %rcx
	je	.L4222
	movq	%rcx, (%rsi)
	movq	16(%rdx), %rcx
	movq	%rcx, 16(%rsi)
.L4219:
	movq	8(%rdx), %rcx
	movq	%rax, (%rdx)
	xorl	%eax, %eax
	movq	$0, 8(%rdx)
	movq	%rcx, 8(%rsi)
	movw	%ax, 16(%rdx)
	movq	32(%rdx), %rax
	movq	%rax, 32(%rsi)
	movq	40(%rdx), %rax
	movq	$0, 40(%rdx)
	movq	%rax, 40(%rsi)
	addq	$48, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L4222:
	movdqu	16(%rdx), %xmm0
	movups	%xmm0, 16(%rsi)
	jmp	.L4219
	.p2align 4,,10
	.p2align 3
.L4217:
	jmp	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE17_M_realloc_insertIJS9_EEEvN9__gnu_cxx17__normal_iteratorIPS9_SB_EEDpOT_
	.cfi_endproc
.LFE11305:
	.size	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE12emplace_backIJS9_EEEvDpOT_, .-_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE12emplace_backIJS9_EEEvDpOT_
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl16pushBreakDetailsERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl16pushBreakDetailsERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl16pushBreakDetailsERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl16pushBreakDetailsERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE:
.LFB7992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$72, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%r12), %rax
	movq	%r14, -112(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r12), %rax
	leaq	384(%rbx), %rdi
	movq	%r15, %rsi
	movq	%rax, -80(%rbp)
	movq	0(%r13), %rax
	movq	$0, 0(%r13)
	movq	%rax, -72(%rbp)
	call	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE12emplace_backIJS9_EEEvDpOT_
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4224
	movq	(%rdi), %rax
	call	*24(%rax)
.L4224:
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L4223
	call	_ZdlPv@PLT
.L4223:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4231
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4231:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7992:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl16pushBreakDetailsERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE, .-_ZN12v8_inspector19V8DebuggerAgentImpl16pushBreakDetailsERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl28schedulePauseOnNextStatementERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl28schedulePauseOnNextStatementERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl28schedulePauseOnNextStatementERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl28schedulePauseOnNextStatementERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE:
.LFB7998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	16(%rdi), %rdi
	movl	16(%rax), %esi
	call	_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi@PLT
	testb	%al, %al
	jne	.L4232
	cmpb	$0, 32(%r12)
	jne	.L4241
.L4232:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4242
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4241:
	.cfi_restore_state
	cmpb	$0, 408(%r12)
	jne	.L4232
	cmpb	$0, 409(%r12)
	je	.L4232
	movq	384(%r12), %rax
	cmpq	%rax, 392(%r12)
	je	.L4243
.L4234:
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	movq	%r12, %rdi
	movq	%r13, %rsi
	leaq	-48(%rbp), %rdx
	movq	%rax, -48(%rbp)
	call	_ZN12v8_inspector19V8DebuggerAgentImpl16pushBreakDetailsERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4232
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4232
	.p2align 4,,10
	.p2align 3
.L4243:
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	movl	$1, %esi
	movl	16(%rax), %edx
	call	_ZN12v8_inspector10V8Debugger18setPauseOnNextCallEbi@PLT
	jmp	.L4234
.L4242:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7998:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl28schedulePauseOnNextStatementERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE, .-_ZN12v8_inspector19V8DebuggerAgentImpl28schedulePauseOnNextStatementERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl5pauseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl5pauseEv
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl5pauseEv, @function
_ZN12v8_inspector19V8DebuggerAgentImpl5pauseEv:
.LFB8000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$72, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 32(%rsi)
	jne	.L4245
	leaq	-80(%rbp), %r12
	leaq	_ZN12v8_inspectorL19kDebuggerNotEnabledE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4244
	call	_ZdlPv@PLT
.L4244:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4259
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4245:
	.cfi_restore_state
	movq	24(%rsi), %rax
	movq	%rsi, %r12
	movq	16(%r12), %rdi
	movl	16(%rax), %esi
	call	_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi@PLT
	testb	%al, %al
	je	.L4248
.L4250:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L4244
	.p2align 4,,10
	.p2align 3
.L4248:
	movq	16(%r12), %rdi
	call	_ZN12v8_inspector10V8Debugger15canBreakProgramEv@PLT
	testb	%al, %al
	jne	.L4260
	movq	384(%r12), %rax
	cmpq	%rax, 392(%r12)
	je	.L4261
.L4251:
	movq	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum5OtherE(%rip), %rsi
	leaq	-80(%rbp), %r14
	movq	$0, -88(%rbp)
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	leaq	-88(%rbp), %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl16pushBreakDetailsERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4252
	call	_ZdlPv@PLT
.L4252:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4250
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L4244
	.p2align 4,,10
	.p2align 3
.L4260:
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	movl	16(%rax), %esi
	call	_ZN12v8_inspector10V8Debugger17interruptAndBreakEi@PLT
	jmp	.L4250
	.p2align 4,,10
	.p2align 3
.L4261:
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	movl	$1, %esi
	movl	16(%rax), %edx
	call	_ZN12v8_inspector10V8Debugger18setPauseOnNextCallEbi@PLT
	jmp	.L4251
.L4259:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8000:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl5pauseEv, .-_ZN12v8_inspector19V8DebuggerAgentImpl5pauseEv
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl12breakProgramERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl12breakProgramERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl12breakProgramERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl12breakProgramERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE:
.LFB8116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 32(%rdi)
	je	.L4262
	cmpb	$0, 408(%rdi)
	movq	%rdi, %rbx
	je	.L4283
.L4262:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4284
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4283:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	movq	%rsi, %r13
	movq	%rdx, %r12
	call	_ZN12v8_inspector10V8Debugger15canBreakProgramEv@PLT
	testb	%al, %al
	je	.L4262
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	leaq	-72(%rbp), %rdx
	movq	%r13, %rsi
	movq	400(%rbx), %rax
	movdqu	384(%rbx), %xmm1
	movq	$0, 400(%rbx)
	movups	%xmm0, 384(%rbx)
	movq	%rax, -48(%rbp)
	movq	(%r12), %rax
	movq	$0, (%r12)
	movq	%rax, -72(%rbp)
	movaps	%xmm1, -64(%rbp)
	call	_ZN12v8_inspector19V8DebuggerAgentImpl16pushBreakDetailsERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4266
	movq	(%rdi), %rax
	call	*24(%rax)
.L4266:
	movq	24(%rbx), %rax
	movq	8(%rbx), %r13
	movq	16(%rbx), %rdi
	movl	16(%rax), %r12d
	movl	20(%rax), %r14d
	movl	%r12d, %esi
	call	_ZN12v8_inspector10V8Debugger12breakProgramEi@PLT
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN12v8_inspector15V8InspectorImpl11sessionByIdEii@PLT
	testq	%rax, %rax
	je	.L4267
	cmpb	$0, 32(%rbx)
	je	.L4267
	movq	384(%rbx), %rax
	movq	392(%rbx), %rdi
	movq	%rax, %rdx
	cmpq	%rax, %rdi
	je	.L4268
	subq	$48, %rdi
	movq	%rdi, 392(%rbx)
	call	_ZNSt4pairIN12v8_inspector8String16ESt10unique_ptrINS0_8protocol15DictionaryValueESt14default_deleteIS4_EEED1Ev
	movq	384(%rbx), %rax
	movq	392(%rbx), %rdx
.L4268:
	movq	400(%rbx), %rdi
	movdqa	-64(%rbp), %xmm2
	movq	%rax, %xmm0
	movq	%rdx, %xmm3
	movq	-48(%rbp), %r8
	movq	-64(%rbp), %rsi
	punpcklqdq	%xmm3, %xmm0
	movq	-56(%rbp), %rcx
	movq	%rdi, -48(%rbp)
	movq	%r8, 400(%rbx)
	movups	%xmm2, 384(%rbx)
	movaps	%xmm0, -64(%rbp)
	cmpq	%rcx, %rsi
	je	.L4267
	movq	24(%rbx), %rax
	movq	16(%rbx), %rdi
	movl	$1, %esi
	movl	16(%rax), %edx
	call	_ZN12v8_inspector10V8Debugger18setPauseOnNextCallEbi@PLT
.L4267:
	leaq	-64(%rbp), %rdi
	call	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EED1Ev
	jmp	.L4262
.L4284:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8116:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl12breakProgramERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE, .-_ZN12v8_inspector19V8DebuggerAgentImpl12breakProgramERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE
	.section	.text._ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIS2_IiiESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS3_,"axG",@progbits,_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIS2_IiiESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIS2_IiiESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS3_
	.type	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIS2_IiiESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS3_, @function
_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIS2_IiiESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS3_:
.LFB13332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rsi), %rbx
	testq	%rbx, %rbx
	jne	.L4286
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rcx
	cmpq	%rcx, %rax
	je	.L4289
	.p2align 4,,10
	.p2align 3
.L4288:
	movq	%rbx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rbx
	movsbq	-2(%rax), %rdx
	addq	%rdx, %rbx
	movq	%rbx, 32(%rdi)
	cmpq	%rax, %rcx
	jne	.L4288
	testq	%rbx, %rbx
	je	.L4289
.L4286:
	movq	8(%r13), %r15
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%r15
	movq	0(%r13), %rax
	movq	%rax, -80(%rbp)
	leaq	0(,%rdx,8), %rcx
	movq	%rdx, %r10
	addq	%rcx, %rax
	movq	%rcx, -96(%rbp)
	movq	(%rax), %r11
	movq	%rax, -88(%rbp)
	testq	%r11, %r11
	je	.L4303
	movq	(%r11), %r12
	movq	%r11, %r14
	movq	72(%r12), %rcx
	cmpq	%rbx, %rcx
	je	.L4318
	.p2align 4,,10
	.p2align 3
.L4291:
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	je	.L4303
	movq	72(%rsi), %rcx
	xorl	%edx, %edx
	movq	%r12, %r14
	movq	%rcx, %rax
	divq	%r15
	cmpq	%rdx, %r10
	jne	.L4303
	movq	%rsi, %r12
	cmpq	%rbx, %rcx
	jne	.L4291
.L4318:
	leaq	8(%r12), %rsi
	movq	%r10, -72(%rbp)
	movq	%r11, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %r11
	testl	%eax, %eax
	movq	-72(%rbp), %r10
	jne	.L4291
	movq	(%r12), %rcx
	cmpq	%r14, %r11
	je	.L4319
	testq	%rcx, %rcx
	je	.L4295
	movq	72(%rcx), %rax
	xorl	%edx, %edx
	divq	%r15
	cmpq	%rdx, %r10
	je	.L4295
	movq	-80(%rbp), %rax
	movq	%r14, (%rax,%rdx,8)
	movq	(%r12), %rcx
.L4295:
	movq	%rcx, (%r14)
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4297
	call	_ZdlPv@PLT
.L4297:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4298
	call	_ZdlPv@PLT
.L4298:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 24(%r13)
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4319:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L4304
	movq	72(%rcx), %rax
	xorl	%edx, %edx
	divq	%r15
	cmpq	%rdx, %r10
	je	.L4295
	movq	-80(%rbp), %rax
	movq	%r14, (%rax,%rdx,8)
	movq	-96(%rbp), %rax
	addq	0(%r13), %rax
	movq	%rax, -88(%rbp)
	movq	(%rax), %rax
.L4294:
	leaq	16(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L4320
.L4296:
	movq	-88(%rbp), %rax
	movq	$0, (%rax)
	movq	(%r12), %rcx
	jmp	.L4295
	.p2align 4,,10
	.p2align 3
.L4289:
	movq	$1, 32(%rdi)
	movl	$1, %ebx
	jmp	.L4286
	.p2align 4,,10
	.p2align 3
.L4303:
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4304:
	.cfi_restore_state
	movq	%r14, %rax
	jmp	.L4294
.L4320:
	movq	%rcx, 16(%r13)
	jmp	.L4296
	.cfi_endproc
.LFE13332:
	.size	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIS2_IiiESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS3_, .-_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIS2_IiiESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS3_
	.section	.text._ZNSt6vectorISt4pairIiiESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIiiESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIiiESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorISt4pairIiiESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorISt4pairIiiESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB13347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movabsq	$1152921504606846975, %rsi
	subq	$40, %rsp
	movq	8(%rdi), %r8
	movq	(%rdi), %r13
	movq	%r8, %rax
	subq	%r13, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L4353
	movq	%rbx, %r9
	movq	%rdi, %rcx
	subq	%r13, %r9
	testq	%rax, %rax
	je	.L4336
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L4354
.L4323:
	movq	%r14, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdx
	leaq	(%rax,%r14), %rsi
	movq	%rax, %r12
	leaq	8(%rax), %r14
.L4335:
	movq	(%rdx), %rax
	movq	%rax, (%r12,%r9)
	cmpq	%r13, %rbx
	je	.L4325
	leaq	-8(%rbx), %rdi
	leaq	15(%r12), %rax
	subq	%r13, %rdi
	subq	%r13, %rax
	movq	%rdi, %r9
	shrq	$3, %r9
	cmpq	$30, %rax
	jbe	.L4338
	movabsq	$2305843009213693950, %rax
	testq	%rax, %r9
	je	.L4338
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L4327:
	movdqu	0(%r13,%rdx), %xmm2
	movups	%xmm2, (%r12,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L4327
	movq	%r9, %r10
	andq	$-2, %r10
	leaq	0(,%r10,8), %rdx
	leaq	0(%r13,%rdx), %rax
	addq	%r12, %rdx
	cmpq	%r10, %r9
	je	.L4329
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L4329:
	leaq	16(%r12,%rdi), %r14
.L4325:
	cmpq	%r8, %rbx
	je	.L4330
	subq	%rbx, %r8
	subq	$8, %r8
	movq	%r8, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r8, %r8
	je	.L4339
	movq	%rdi, %rax
	xorl	%edx, %edx
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L4332:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L4332
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r15
	leaq	(%r14,%r15), %rdx
	addq	%rbx, %r15
	cmpq	%rax, %rdi
	je	.L4333
.L4331:
	movq	(%r15), %rax
	movq	%rax, (%rdx)
.L4333:
	leaq	8(%r14,%r8), %r14
.L4330:
	testq	%r13, %r13
	je	.L4334
	movq	%r13, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rsi
.L4334:
	movq	%r12, %xmm0
	movq	%r14, %xmm3
	movq	%rsi, 16(%rcx)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%rcx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4354:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L4324
	movl	$8, %r14d
	xorl	%esi, %esi
	xorl	%r12d, %r12d
	jmp	.L4335
	.p2align 4,,10
	.p2align 3
.L4336:
	movl	$8, %r14d
	jmp	.L4323
	.p2align 4,,10
	.p2align 3
.L4338:
	movq	%r12, %rdx
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L4326:
	movl	(%rax), %r10d
	movl	4(%rax), %r9d
	addq	$8, %rax
	addq	$8, %rdx
	movl	%r10d, -8(%rdx)
	movl	%r9d, -4(%rdx)
	cmpq	%rax, %rbx
	jne	.L4326
	jmp	.L4329
.L4339:
	movq	%r14, %rdx
	jmp	.L4331
.L4324:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	leaq	0(,%rsi,8), %r14
	jmp	.L4323
.L4353:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE13347:
	.size	_ZNSt6vectorISt4pairIiiESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorISt4pairIiiESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.text._ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIS2_IiiESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIS2_IiiESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIS2_IiiESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm
	.type	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIS2_IiiESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm, @function
_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIS2_IiiESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm:
.LFB13355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L4379
.L4356:
	movq	%r14, 72(%r12)
	movq	(%rbx), %rax
	leaq	0(,%r15,8), %rcx
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.L4365
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r12, (%rax)
.L4366:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4379:
	.cfi_restore_state
	movq	%rdx, %r13
	cmpq	$1, %rdx
	je	.L4380
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L4381
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L4358:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L4360
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L4361
	.p2align 4,,10
	.p2align 3
.L4362:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L4363:
	testq	%rsi, %rsi
	je	.L4360
.L4361:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	72(%rcx), %rax
	divq	%r13
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L4362
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L4368
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L4361
	.p2align 4,,10
	.p2align 3
.L4360:
	movq	(%rbx), %rdi
	cmpq	%r9, %rdi
	je	.L4364
	call	_ZdlPv@PLT
.L4364:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r15, (%rbx)
	divq	%r13
	movq	%r13, 8(%rbx)
	movq	%rdx, %r15
	jmp	.L4356
	.p2align 4,,10
	.p2align 3
.L4365:
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L4367
	movq	72(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L4367:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L4366
	.p2align 4,,10
	.p2align 3
.L4368:
	movq	%rdx, %rdi
	jmp	.L4363
	.p2align 4,,10
	.p2align 3
.L4380:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L4358
.L4381:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE13355:
	.size	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIS2_IiiESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm, .-_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIS2_IiiESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm
	.section	.text._ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St6vectorIS3_IiiESaIS6_EEESaIS9_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_,"axG",@progbits,_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St6vectorIS3_IiiESaIS6_EEESaIS9_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St6vectorIS3_IiiESaIS6_EEESaIS9_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	.type	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St6vectorIS3_IiiESaIS6_EEESaIS9_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_, @function
_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St6vectorIS3_IiiESaIS6_EEESaIS9_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_:
.LFB11366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rsi), %r12
	testq	%r12, %r12
	jne	.L4383
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rcx
	cmpq	%rcx, %rax
	je	.L4386
	.p2align 4,,10
	.p2align 3
.L4385:
	movq	%r12, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%r12, %rdx
	movq	%rdx, %r12
	movsbq	-2(%rax), %rdx
	addq	%rdx, %r12
	movq	%r12, 32(%r14)
	cmpq	%rax, %rcx
	jne	.L4385
	testq	%r12, %r12
	je	.L4386
.L4383:
	movq	8(%r13), %r15
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%r15
	movq	0(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L4387
	movq	(%rax), %rbx
	movq	72(%rbx), %rcx
.L4390:
	cmpq	%rcx, %r12
	je	.L4405
.L4388:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L4387
	movq	72(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r15
	cmpq	%rdx, %r9
	je	.L4390
.L4387:
	movl	$80, %edi
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	(%r14), %rsi
	movq	%rax, %r15
	movq	$0, (%rax)
	leaq	8(%rax), %rdi
	leaq	24(%rax), %rax
	movq	%rax, 8(%r15)
	movq	8(%r14), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r14), %rax
	movq	-56(%rbp), %r9
	movq	%r15, %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$1, %r8d
	movq	%rax, 40(%r15)
	movq	%r9, %rsi
	movq	$0, 64(%r15)
	movups	%xmm0, 48(%r15)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIS2_IiiESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm
	addq	$24, %rsp
	popq	%rbx
	addq	$48, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4405:
	.cfi_restore_state
	leaq	8(%rbx), %rsi
	movq	%r14, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	jne	.L4388
	addq	$24, %rsp
	leaq	48(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4386:
	.cfi_restore_state
	movq	$1, 32(%r14)
	movl	$1, %r12d
	jmp	.L4383
	.cfi_endproc
.LFE11366:
	.size	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St6vectorIS3_IiiESaIS6_EEESaIS9_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_, .-_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St6vectorIS3_IiiESaIS6_EEESaIS9_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl19setBlackboxedRangesERKNS_8String16ESt10unique_ptrISt6vectorIS4_INS_8protocol8Debugger14ScriptPositionESt14default_deleteIS8_EESaISB_EES9_ISD_EE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"No script with passed id."
.LC25:
	.string	"vector::reserve"
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl19setBlackboxedRangesERKNS_8String16ESt10unique_ptrISt6vectorIS4_INS_8protocol8Debugger14ScriptPositionESt14default_deleteIS8_EESaISB_EES9_ISD_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"Position missing 'line' or 'line' < 0."
	.align 8
.LC27:
	.string	"Position missing 'column' or 'column' < 0."
	.align 8
.LC28:
	.string	"Input positions array is not sorted or contains duplicate values."
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl19setBlackboxedRangesERKNS_8String16ESt10unique_ptrISt6vectorIS4_INS_8protocol8Debugger14ScriptPositionESt14default_deleteIS8_EESaISB_EES9_ISD_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl19setBlackboxedRangesERKNS_8String16ESt10unique_ptrISt6vectorIS4_INS_8protocol8Debugger14ScriptPositionESt14default_deleteIS8_EESaISB_EES9_ISD_EE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl19setBlackboxedRangesERKNS_8String16ESt10unique_ptrISt6vectorIS4_INS_8protocol8Debugger14ScriptPositionESt14default_deleteIS8_EESaISB_EES9_ISD_EE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl19setBlackboxedRangesERKNS_8String16ESt10unique_ptrISt6vectorIS4_INS_8protocol8Debugger14ScriptPositionESt14default_deleteIS8_EESaISB_EES9_ISD_EE:
.LFB8020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	64(%rsi), %rdi
	movq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L4459
	movq	(%rbx), %rdx
	movq	%rax, %r14
	movq	8(%rdx), %rax
	movq	(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4460
	pxor	%xmm0, %xmm0
	movq	$0, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	subq	%rdx, %rax
	js	.L4461
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	call	_Znwm@PLT
	movq	-120(%rbp), %r8
	movq	-128(%rbp), %rdi
	movq	-152(%rbp), %rcx
	cmpq	%rdi, %r8
	je	.L4412
	leaq	-8(%r8), %rsi
	leaq	15(%rdi), %rdx
	subq	%rdi, %rsi
	subq	%rax, %rdx
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L4435
	movabsq	$2305843009213693950, %rdx
	testq	%rdx, %rsi
	je	.L4435
	addq	$1, %rsi
	xorl	%edx, %edx
	movq	%rsi, %r8
	shrq	%r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L4414:
	movdqu	(%rdi,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%r8, %rdx
	jne	.L4414
	movq	%rsi, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %r8
	leaq	(%rdi,%r8), %rdx
	addq	%rax, %r8
	cmpq	%r9, %rsi
	je	.L4416
	movq	(%rdx), %rdx
	movq	%rdx, (%r8)
.L4416:
	movq	%rax, -160(%rbp)
	movq	%rcx, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-160(%rbp), %rax
	movq	-152(%rbp), %rcx
.L4417:
	movq	%rax, %xmm0
	addq	%rcx, %rax
	leaq	-136(%rbp), %r8
	movq	%rax, -112(%rbp)
	movq	(%rbx), %rax
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	cmpq	%rcx, %rbx
	je	.L4419
	.p2align 4,,10
	.p2align 3
.L4429:
	movq	(%rbx), %rdx
	movl	8(%rdx), %eax
	testl	%eax, %eax
	js	.L4462
.L4421:
	movl	12(%rdx), %edx
	testl	%edx, %edx
	js	.L4463
	movl	%eax, -136(%rbp)
	movq	-120(%rbp), %rsi
	movl	%edx, -132(%rbp)
	cmpq	-112(%rbp), %rsi
	je	.L4426
	addq	$8, %rbx
	movl	%eax, (%rsi)
	movl	%edx, 4(%rsi)
	addq	$8, -120(%rbp)
	cmpq	%rbx, %rcx
	jne	.L4429
.L4428:
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %rax
	subq	%rdx, %rax
	movq	%rax, %rdi
	sarq	$3, %rdi
	cmpq	$15, %rax
	jbe	.L4419
	movl	(%rdx), %ecx
	movl	$1, %eax
.L4433:
	movl	%ecx, %esi
	movl	(%rdx,%rax,8), %ecx
	cmpl	%esi, %ecx
	jg	.L4430
	jne	.L4431
	movl	4(%rdx,%rax,8), %ebx
	cmpl	%ebx, -4(%rdx,%rax,8)
	jl	.L4430
.L4431:
	leaq	-96(%rbp), %r13
	leaq	.LC28(%rip), %rsi
.L4456:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4423
	call	_ZdlPv@PLT
.L4423:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4406
.L4454:
	call	_ZdlPv@PLT
	jmp	.L4406
	.p2align 4,,10
	.p2align 3
.L4460:
	leaq	424(%r13), %rdi
	movq	%r15, %rsi
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIS2_IiiESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS3_
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
	call	*128(%rax)
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L4406:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4464
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4435:
	.cfi_restore_state
	movq	%rax, %rsi
	movq	%rdi, %rdx
	.p2align 4,,10
	.p2align 3
.L4413:
	movl	(%rdx), %r10d
	movl	4(%rdx), %r9d
	addq	$8, %rdx
	addq	$8, %rsi
	movl	%r10d, -8(%rsi)
	movl	%r9d, -4(%rsi)
	cmpq	%r8, %rdx
	jne	.L4413
.L4412:
	testq	%rdi, %rdi
	je	.L4417
	jmp	.L4416
	.p2align 4,,10
	.p2align 3
.L4426:
	movq	%r8, %rdx
	leaq	-128(%rbp), %rdi
	addq	$8, %rbx
	movq	%rcx, -160(%rbp)
	movq	%r8, -152(%rbp)
	call	_ZNSt6vectorISt4pairIiiESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	movq	-160(%rbp), %rcx
	movq	-152(%rbp), %r8
	cmpq	%rbx, %rcx
	je	.L4428
	movq	(%rbx), %rdx
	movl	8(%rdx), %eax
	testl	%eax, %eax
	jns	.L4421
	.p2align 4,,10
	.p2align 3
.L4462:
	leaq	-96(%rbp), %r13
	leaq	.LC26(%rip), %rsi
	jmp	.L4456
	.p2align 4,,10
	.p2align 3
.L4430:
	addq	$1, %rax
	cmpq	%rdi, %rax
	jne	.L4433
.L4419:
	leaq	424(%r13), %rdi
	movq	%r15, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St6vectorIS3_IiiESaIS6_EEESaIS9_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	leaq	-128(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZNSt6vectorISt4pairIiiESaIS1_EEaSERKS3_
	movq	48(%r14), %rdi
	movq	(%rdi), %rax
	call	*128(%rax)
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L4423
	.p2align 4,,10
	.p2align 3
.L4463:
	leaq	-96(%rbp), %r13
	leaq	.LC27(%rip), %rsi
	jmp	.L4456
	.p2align 4,,10
	.p2align 3
.L4459:
	leaq	-96(%rbp), %r13
	leaq	.LC24(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4454
	jmp	.L4406
.L4464:
	call	__stack_chk_fail@PLT
.L4461:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8020:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl19setBlackboxedRangesERKNS_8String16ESt10unique_ptrISt6vectorIS4_INS_8protocol8Debugger14ScriptPositionESt14default_deleteIS8_EESaISB_EES9_ISD_EE, .-_ZN12v8_inspector19V8DebuggerAgentImpl19setBlackboxedRangesERKNS_8String16ESt10unique_ptrISt6vectorIS4_INS_8protocol8Debugger14ScriptPositionESt14default_deleteIS8_EESaISB_EES9_ISD_EE
	.section	.text._ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb1EEEm
	.type	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb1EEEm, @function
_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb1EEEm:
.LFB13424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L4489
.L4466:
	movq	%r14, 56(%r12)
	movq	(%rbx), %rax
	leaq	0(,%r15,8), %rcx
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.L4475
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r12, (%rax)
.L4476:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4489:
	.cfi_restore_state
	movq	%rdx, %r13
	cmpq	$1, %rdx
	je	.L4490
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L4491
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L4468:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L4470
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L4471
	.p2align 4,,10
	.p2align 3
.L4472:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L4473:
	testq	%rsi, %rsi
	je	.L4470
.L4471:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	56(%rcx), %rax
	divq	%r13
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L4472
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L4478
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L4471
	.p2align 4,,10
	.p2align 3
.L4470:
	movq	(%rbx), %rdi
	cmpq	%r9, %rdi
	je	.L4474
	call	_ZdlPv@PLT
.L4474:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r15, (%rbx)
	divq	%r13
	movq	%r13, 8(%rbx)
	movq	%rdx, %r15
	jmp	.L4466
	.p2align 4,,10
	.p2align 3
.L4475:
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L4477
	movq	56(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L4477:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L4476
	.p2align 4,,10
	.p2align 3
.L4478:
	movq	%rdx, %rdi
	jmp	.L4473
	.p2align 4,,10
	.p2align 3
.L4490:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L4468
.L4491:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE13424:
	.size	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb1EEEm, .-_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb1EEEm
	.section	.text._ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_16V8DebuggerScriptESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_,"axG",@progbits,_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_16V8DebuggerScriptESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_16V8DebuggerScriptESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	.type	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_16V8DebuggerScriptESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_, @function
_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_16V8DebuggerScriptESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_:
.LFB11411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rsi), %r12
	testq	%r12, %r12
	jne	.L4493
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rcx
	cmpq	%rcx, %rax
	je	.L4496
	.p2align 4,,10
	.p2align 3
.L4495:
	movq	%r12, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%r12, %rdx
	movq	%rdx, %r12
	movsbq	-2(%rax), %rdx
	addq	%rdx, %r12
	movq	%r12, 32(%r14)
	cmpq	%rax, %rcx
	jne	.L4495
	testq	%r12, %r12
	je	.L4496
.L4493:
	movq	8(%r13), %r15
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%r15
	movq	0(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L4497
	movq	(%rax), %rbx
	movq	56(%rbx), %rcx
.L4500:
	cmpq	%rcx, %r12
	je	.L4515
.L4498:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L4497
	movq	56(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r15
	cmpq	%rdx, %r9
	je	.L4500
.L4497:
	movl	$64, %edi
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	(%r14), %rsi
	movq	%rax, %r15
	movq	$0, (%rax)
	leaq	8(%rax), %rdi
	leaq	24(%rax), %rax
	movq	%rax, 8(%r15)
	movq	8(%r14), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r14), %rax
	movq	-56(%rbp), %r9
	movq	%r15, %rcx
	movq	$0, 48(%r15)
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$1, %r8d
	movq	%rax, 40(%r15)
	movq	%r9, %rsi
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb1EEEm
	addq	$24, %rsp
	popq	%rbx
	addq	$48, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4515:
	.cfi_restore_state
	leaq	8(%rbx), %rsi
	movq	%r14, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	jne	.L4498
	addq	$24, %rsp
	leaq	48(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4496:
	.cfi_restore_state
	movq	$1, 32(%r14)
	movl	$1, %r12d
	jmp	.L4493
	.cfi_endproc
.LFE11411:
	.size	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_16V8DebuggerScriptESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_, .-_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_16V8DebuggerScriptESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl15ScriptCollectedEPKNS_16V8DebuggerScriptE.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl15ScriptCollectedEPKNS_16V8DebuggerScriptE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl15ScriptCollectedEPKNS_16V8DebuggerScriptE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl15ScriptCollectedEPKNS_16V8DebuggerScriptE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl15ScriptCollectedEPKNS_16V8DebuggerScriptE:
.LFB8120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	368(%rdi), %rax
	movq	352(%rdi), %rbx
	subq	$40, %rax
	cmpq	%rax, %rbx
	je	.L4517
	leaq	16(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	movq	16(%r12), %rax
	movq	8(%rsi), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	40(%r12), %rax
	movq	%rax, 32(%rbx)
	addq	$40, 352(%r15)
.L4518:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*96(%rax)
	movq	296(%r15), %rdx
	cltq
	leaq	(%rdx,%rax,2), %rax
	movq	%rax, 296(%r15)
	cmpq	288(%r15), %rax
	jbe	.L4516
	leaq	64(%r15), %rax
	movq	320(%r15), %r13
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L4527:
	movq	-80(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_16V8DebuggerScriptESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*96(%rax)
	cltq
	addq	%rax, %rax
	subq	%rax, 296(%r15)
	movq	32(%r13), %rcx
	testq	%rcx, %rcx
	jne	.L4528
	movq	0(%r13), %rax
	movq	8(%r13), %rdx
	leaq	(%rax,%rdx,2), %rsi
	cmpq	%rsi, %rax
	je	.L4531
	.p2align 4,,10
	.p2align 3
.L4530:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movsbq	-2(%rax), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 32(%r13)
	cmpq	%rax, %rsi
	jne	.L4530
	testq	%rcx, %rcx
	je	.L4531
.L4528:
	movq	72(%r15), %rsi
	movq	%rcx, %rax
	xorl	%edx, %edx
	movq	64(%r15), %rbx
	divq	%rsi
	leaq	0(,%rdx,8), %rax
	movq	%rdx, %r10
	movq	%rax, -88(%rbp)
	addq	%rbx, %rax
	movq	(%rax), %rdi
	movq	%rax, -72(%rbp)
	testq	%rdi, %rdi
	je	.L4532
	movq	(%rdi), %r12
	movq	%rdi, -56(%rbp)
	movq	%rdi, %r9
	movq	%rcx, %rdi
	movq	56(%r12), %r8
	cmpq	%r8, %rdi
	je	.L4578
	.p2align 4,,10
	.p2align 3
.L4533:
	movq	(%r12), %rcx
	testq	%rcx, %rcx
	je	.L4532
	movq	56(%rcx), %r8
	xorl	%edx, %edx
	movq	%r12, %r9
	movq	%r8, %rax
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L4532
	movq	%rcx, %r12
	cmpq	%r8, %rdi
	jne	.L4533
.L4578:
	movq	8(%r13), %rdx
	movq	16(%r12), %rcx
	movq	8(%r12), %r11
	movq	0(%r13), %r14
	cmpq	%rcx, %rdx
	movq	%rcx, %r8
	cmovbe	%rdx, %r8
	testq	%r8, %r8
	je	.L4534
	movq	%rdx, -64(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L4535:
	movzwl	(%r11,%rax,2), %edx
	cmpw	%dx, (%r14,%rax,2)
	jne	.L4533
	addq	$1, %rax
	cmpq	%r8, %rax
	jne	.L4535
	movq	-64(%rbp), %rdx
.L4534:
	subq	%rcx, %rdx
	movl	$2147483648, %eax
	cmpq	%rax, %rdx
	jge	.L4533
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rdx
	jle	.L4533
	testl	%edx, %edx
	jne	.L4533
	movq	-56(%rbp), %rdi
	movq	(%r12), %rcx
	cmpq	%r9, %rdi
	je	.L4579
	testq	%rcx, %rcx
	je	.L4539
	movq	56(%rcx), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r10
	je	.L4539
	movq	%r9, (%rbx,%rdx,8)
	movq	(%r12), %rcx
.L4539:
	movq	%rcx, (%r9)
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4541
	movq	(%rdi), %rax
	call	*8(%rax)
.L4541:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4542
	call	_ZdlPv@PLT
.L4542:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 88(%r15)
.L4532:
	movq	336(%r15), %rax
	movq	320(%r15), %r13
	subq	$40, %rax
	movq	0(%r13), %rdi
	leaq	16(%r13), %rdx
	cmpq	%rax, %r13
	je	.L4543
	cmpq	%rdi, %rdx
	je	.L4544
	call	_ZdlPv@PLT
	movq	320(%r15), %r13
.L4544:
	addq	$40, %r13
	movq	%r13, 320(%r15)
.L4545:
	movq	288(%r15), %rax
	cmpq	%rax, 296(%r15)
	ja	.L4527
.L4516:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4531:
	.cfi_restore_state
	movq	$1, 32(%r13)
	movl	$1, %ecx
	jmp	.L4528
	.p2align 4,,10
	.p2align 3
.L4579:
	testq	%rcx, %rcx
	je	.L4550
	movq	56(%rcx), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r10
	je	.L4539
	movq	%r9, (%rbx,%rdx,8)
	movq	-88(%rbp), %rax
	leaq	80(%r15), %rdx
	addq	64(%r15), %rax
	movq	%rax, -72(%rbp)
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	je	.L4580
.L4540:
	movq	-72(%rbp), %rax
	movq	$0, (%rax)
	movq	(%r12), %rcx
	jmp	.L4539
	.p2align 4,,10
	.p2align 3
.L4543:
	cmpq	%rdi, %rdx
	je	.L4546
	call	_ZdlPv@PLT
.L4546:
	movq	328(%r15), %rdi
	call	_ZdlPv@PLT
	movq	344(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 344(%r15)
	movq	8(%rax), %r13
	leaq	480(%r13), %rax
	movq	%r13, 328(%r15)
	movq	%rax, 336(%r15)
	movq	%r13, 320(%r15)
	jmp	.L4545
	.p2align 4,,10
	.p2align 3
.L4550:
	movq	%r9, %rax
	leaq	80(%r15), %rdx
	cmpq	%rdx, %rax
	jne	.L4540
.L4580:
	movq	%rcx, 80(%r15)
	jmp	.L4540
	.p2align 4,,10
	.p2align 3
.L4517:
	movq	376(%rdi), %r14
	movq	344(%rdi), %rsi
	subq	360(%rdi), %rbx
	movabsq	$-3689348814741910323, %rdi
	movq	%r14, %r13
	sarq	$3, %rbx
	subq	%rsi, %r13
	imulq	%rdi, %rbx
	movq	%r13, %rdx
	sarq	$3, %rdx
	leaq	-3(%rdx,%rdx,2), %rax
	leaq	(%rbx,%rax,4), %rcx
	movq	336(%r15), %rax
	subq	320(%r15), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	addq	%rcx, %rax
	movabsq	$230584300921369395, %rcx
	cmpq	%rcx, %rax
	je	.L4581
	movq	304(%r15), %rdi
	movq	312(%r15), %rcx
	movq	%r14, %rax
	subq	%rdi, %rax
	movq	%rcx, %rbx
	sarq	$3, %rax
	subq	%rax, %rbx
	cmpq	$1, %rbx
	jbe	.L4582
.L4520:
	movl	$480, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movq	352(%r15), %rbx
	leaq	16(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	movq	16(%r12), %rax
	movq	8(%r12), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	40(%r12), %rax
	movq	%rax, 32(%rbx)
	movq	376(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 376(%r15)
	movq	8(%rax), %rax
	leaq	480(%rax), %rdx
	movq	%rax, 360(%r15)
	movq	%rdx, 368(%r15)
	movq	%rax, 352(%r15)
	jmp	.L4518
	.p2align 4,,10
	.p2align 3
.L4582:
	leaq	2(%rdx), %rbx
	leaq	(%rbx,%rbx), %rax
	cmpq	%rax, %rcx
	ja	.L4583
	testq	%rcx, %rcx
	movl	$1, %eax
	cmovne	%rcx, %rax
	leaq	2(%rcx,%rax), %r14
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r14
	ja	.L4584
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	344(%r15), %rsi
	movq	%rax, %rcx
	movq	%r14, %rax
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rcx,%rax,8), %rbx
	movq	376(%r15), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L4525
	subq	%rsi, %rdx
	movq	%rbx, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L4525:
	movq	304(%r15), %rdi
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rcx
	movq	%r14, 312(%r15)
	movq	%rcx, 304(%r15)
.L4523:
	movq	%rbx, 344(%r15)
	movq	(%rbx), %rax
	leaq	(%rbx,%r13), %r14
	movq	(%rbx), %xmm0
	movq	%r14, 376(%r15)
	addq	$480, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 328(%r15)
	movq	(%r14), %rax
	movq	%rax, 360(%r15)
	addq	$480, %rax
	movq	%rax, 368(%r15)
	jmp	.L4520
.L4583:
	subq	%rbx, %rcx
	addq	$8, %r14
	shrq	%rcx
	movq	%r14, %rdx
	leaq	(%rdi,%rcx,8), %rbx
	subq	%rsi, %rdx
	cmpq	%rbx, %rsi
	jbe	.L4522
	cmpq	%r14, %rsi
	je	.L4523
	movq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L4523
.L4522:
	cmpq	%r14, %rsi
	je	.L4523
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L4523
.L4584:
	call	_ZSt17__throw_bad_allocv@PLT
.L4581:
	leaq	.LC29(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8120:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl15ScriptCollectedEPKNS_16V8DebuggerScriptE, .-_ZN12v8_inspector19V8DebuggerAgentImpl15ScriptCollectedEPKNS_16V8DebuggerScriptE
	.section	.text._ZNSt6vectorIPN12v8_inspector8protocol15DictionaryValueESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN12v8_inspector8protocol15DictionaryValueESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN12v8_inspector8protocol15DictionaryValueESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIPN12v8_inspector8protocol15DictionaryValueESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIPN12v8_inspector8protocol15DictionaryValueESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB13454:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L4599
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L4595
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L4600
.L4587:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L4594:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L4601
	testq	%r13, %r13
	jg	.L4590
	testq	%r9, %r9
	jne	.L4593
.L4591:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4601:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L4590
.L4593:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L4591
	.p2align 4,,10
	.p2align 3
.L4600:
	testq	%rsi, %rsi
	jne	.L4588
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L4594
	.p2align 4,,10
	.p2align 3
.L4590:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L4591
	jmp	.L4593
	.p2align 4,,10
	.p2align 3
.L4595:
	movl	$8, %r14d
	jmp	.L4587
.L4599:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4588:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L4587
	.cfi_endproc
.LFE13454:
	.size	_ZNSt6vectorIPN12v8_inspector8protocol15DictionaryValueESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIPN12v8_inspector8protocol15DictionaryValueESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIPN12v8_inspector8protocol15DictionaryValueESaIS3_EE12emplace_backIJS3_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIPN12v8_inspector8protocol15DictionaryValueESaIS3_EE12emplace_backIJS3_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN12v8_inspector8protocol15DictionaryValueESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	.type	_ZNSt6vectorIPN12v8_inspector8protocol15DictionaryValueESaIS3_EE12emplace_backIJS3_EEEvDpOT_, @function
_ZNSt6vectorIPN12v8_inspector8protocol15DictionaryValueESaIS3_EE12emplace_backIJS3_EEEvDpOT_:
.LFB11436:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	cmpq	16(%rdi), %r8
	je	.L4603
	movq	(%rsi), %rax
	movq	%rax, (%r8)
	addq	$8, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L4603:
	movq	%rsi, %rdx
	movq	%r8, %rsi
	jmp	_ZNSt6vectorIPN12v8_inspector8protocol15DictionaryValueESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.cfi_endproc
.LFE11436:
	.size	_ZNSt6vectorIPN12v8_inspector8protocol15DictionaryValueESaIS3_EE12emplace_backIJS3_EEEvDpOT_, .-_ZNSt6vectorIPN12v8_inspector8protocol15DictionaryValueESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm:
.LFB13467:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L4607
	movq	(%rbx), %r8
.L4608:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L4617
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L4618:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4607:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L4631
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L4632
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L4610:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L4612
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L4613
	.p2align 4,,10
	.p2align 3
.L4614:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L4615:
	testq	%rsi, %rsi
	je	.L4612
.L4613:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L4614
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L4620
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L4613
	.p2align 4,,10
	.p2align 3
.L4612:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L4616
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L4616:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L4608
	.p2align 4,,10
	.p2align 3
.L4617:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L4619
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L4619:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L4618
	.p2align 4,,10
	.p2align 3
.L4620:
	movq	%rdx, %rdi
	jmp	.L4615
	.p2align 4,,10
	.p2align 3
.L4631:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L4610
.L4632:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE13467:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl42setScriptInstrumentationBreakpointIfNeededEPNS_16V8DebuggerScriptE.str1.1,"aMS",@progbits,1
.LC30:
	.string	"url"
.LC31:
	.string	"scriptId"
.LC32:
	.string	"sourceMapURL"
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl42setScriptInstrumentationBreakpointIfNeededEPNS_16V8DebuggerScriptE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl42setScriptInstrumentationBreakpointIfNeededEPNS_16V8DebuggerScriptE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl42setScriptInstrumentationBreakpointIfNeededEPNS_16V8DebuggerScriptE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl42setScriptInstrumentationBreakpointIfNeededEPNS_16V8DebuggerScriptE:
.LFB8083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	_ZN12v8_inspector18DebuggerAgentStateL26instrumentationBreakpointsE(%rip), %rsi
	subq	$280, %rsp
	movq	40(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -280(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -288(%rbp)
	cmpq	%rax, %rdi
	je	.L4634
	call	_ZdlPv@PLT
.L4634:
	cmpq	$0, -280(%rbp)
	je	.L4633
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	leaq	-252(%rbp), %r14
	call	*64(%rax)
	movq	%rbx, %rdi
	movl	%eax, %r15d
	movq	(%rbx), %rax
	call	*56(%rax)
	movl	%r15d, %edx
	movq	%r14, %rdi
	leaq	-264(%rbp), %r15
	movl	%eax, %esi
	call	_ZN2v85debug8LocationC1Eii@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v85debug8LocationC1Eii@PLT
	leaq	8(%rbx), %rax
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN12v8_inspector19V8DebuggerAgentImpl20isFunctionBlackboxedERKNS_8String16ERKN2v85debug8LocationES8_
	testb	%al, %al
	jne	.L4633
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*16(%rax)
	leaq	-240(%rbp), %rdi
	movq	%rax, %r15
	leaq	-224(%rbp), %rax
	movq	%rdi, -312(%rbp)
	movq	%rax, -240(%rbp)
	movq	(%r15), %rsi
	movq	%rax, -296(%rbp)
	movq	8(%r15), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r15), %rax
	movq	_ZN12v8_inspector8protocol8Debugger28SetInstrumentationBreakpoint19InstrumentationEnum21BeforeScriptExecutionE(%rip), %rsi
	movq	%r13, %rdi
	leaq	-192(%rbp), %r15
	movq	%rax, -208(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_135generateInstrumentationBreakpointIdERKNS_8String16E
	movq	-96(%rbp), %rdi
	cmpq	-288(%rbp), %rdi
	je	.L4638
	call	_ZdlPv@PLT
.L4638:
	movq	-280(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L4639
.L4644:
	movq	(%rbx), %rax
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	*168(%rax)
	testb	%al, %al
	je	.L4641
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	leaq	.LC30(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-280(%rbp), %rdi
	leaq	48(%rbx), %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue9setStringERKNS_8String16ES4_@PLT
	movq	-96(%rbp), %rdi
	cmpq	-288(%rbp), %rdi
	je	.L4645
	call	_ZdlPv@PLT
.L4645:
	leaq	.LC31(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-280(%rbp), %rdi
	movq	-304(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue9setStringERKNS_8String16ES4_@PLT
	movq	-96(%rbp), %rdi
	cmpq	-288(%rbp), %rdi
	je	.L4646
	call	_ZdlPv@PLT
.L4646:
	cmpq	$0, -232(%rbp)
	jne	.L4677
.L4647:
	movslq	-252(%rbp), %rbx
	movq	240(%r12), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rax
	movq	%rbx, %r8
	divq	%rdi
	movq	232(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r13
	testq	%rax, %rax
	je	.L4649
	movq	(%rax), %rcx
	movl	8(%rcx), %esi
	jmp	.L4651
	.p2align 4,,10
	.p2align 3
.L4678:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L4649
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%rdx, %r13
	jne	.L4649
.L4651:
	cmpl	%esi, %r8d
	jne	.L4678
	addq	$16, %rcx
.L4657:
	movq	(%rcx), %rdi
	movq	-280(%rbp), %rax
	movq	%rax, (%rcx)
	testq	%rdi, %rdi
	je	.L4652
	movq	(%rdi), %rax
	call	*24(%rax)
.L4652:
	leaq	176(%r12), %rdi
	movq	%r14, %rsi
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiN12v8_inspector8String16EESaIS5_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-160(%rbp), %rax
	movq	%r15, %rsi
	leaq	120(%r12), %rdi
	movq	%rax, 32(%rbx)
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St6vectorIiSaIiEEESaIS8_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L4653
	movl	-252(%rbp), %edx
	movl	%edx, (%rsi)
	addq	$4, 8(%rax)
.L4641:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4656
	call	_ZdlPv@PLT
.L4656:
	movq	-240(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L4633
	call	_ZdlPv@PLT
.L4633:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4679
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4639:
	.cfi_restore_state
	cmpq	$0, -232(%rbp)
	je	.L4641
	leaq	-144(%rbp), %r8
	movq	_ZN12v8_inspector8protocol8Debugger28SetInstrumentationBreakpoint19InstrumentationEnum34BeforeScriptWithSourceMapExecutionE(%rip), %rsi
	movq	%r8, %rdi
	movq	%r8, -320(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-320(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_135generateInstrumentationBreakpointIdERKNS_8String16E
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -160(%rbp)
	cmpq	-288(%rbp), %rdi
	je	.L4642
	call	_ZdlPv@PLT
.L4642:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4643
	call	_ZdlPv@PLT
.L4643:
	movq	-280(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	jne	.L4644
	jmp	.L4641
	.p2align 4,,10
	.p2align 3
.L4677:
	leaq	.LC32(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-280(%rbp), %rdi
	movq	-312(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue9setStringERKNS_8String16ES4_@PLT
	movq	-96(%rbp), %rdi
	cmpq	-288(%rbp), %rdi
	je	.L4647
	call	_ZdlPv@PLT
	jmp	.L4647
	.p2align 4,,10
	.p2align 3
.L4649:
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	232(%r12), %rdi
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movl	-252(%rbp), %eax
	movl	$1, %r8d
	movq	$0, 16(%rcx)
	movl	%eax, 8(%rcx)
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	leaq	16(%rax), %rcx
	jmp	.L4657
	.p2align 4,,10
	.p2align 3
.L4653:
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	jmp	.L4641
.L4679:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8083:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl42setScriptInstrumentationBreakpointIfNeededEPNS_16V8DebuggerScriptE, .-_ZN12v8_inspector19V8DebuggerAgentImpl42setScriptInstrumentationBreakpointIfNeededEPNS_16V8DebuggerScriptE
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl14didParseSourceESt10unique_ptrINS_16V8DebuggerScriptESt14default_deleteIS2_EEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl14didParseSourceESt10unique_ptrINS_16V8DebuggerScriptESt14default_deleteIS2_EEb
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl14didParseSourceESt10unique_ptrINS_16V8DebuggerScriptESt14default_deleteIS2_EEb, @function
_ZN12v8_inspector19V8DebuggerAgentImpl14didParseSourceESt10unique_ptrINS_16V8DebuggerScriptESt14default_deleteIS2_EEb:
.LFB8061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$712, %rsp
	movl	%edx, -584(%rbp)
	movq	56(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-512(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -736(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	testb	%r14b, %r14b
	je	.L4822
	leaq	-112(%rbp), %rax
	movq	%rax, -608(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -616(%rbp)
.L4681:
	movq	(%rbx), %rax
	movq	8(%r15), %rdi
	movl	92(%rax), %r14d
	movl	%r14d, %esi
	movl	%r14d, -624(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	movq	8(%r15), %rdi
	movl	%r14d, %edx
	leaq	-480(%rbp), %r14
	movl	%eax, %esi
	movl	%eax, %r13d
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEii@PLT
	movq	%r14, -568(%rbp)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4685
	movq	-616(%rbp), %rax
	movq	-608(%rbp), %rdi
	movq	%rax, -112(%rbp)
	movq	104(%r12), %rsi
	movq	112(%r12), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	136(%r12), %rax
	movq	-608(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_8String16E@PLT
	movq	-480(%rbp), %r12
	testq	%r12, %r12
	je	.L4686
	cmpl	$6, 8(%r12)
	movl	$0, %eax
	cmovne	%rax, %r12
.L4686:
	movq	-112(%rbp), %rdi
	cmpq	-616(%rbp), %rdi
	je	.L4685
	call	_ZdlPv@PLT
.L4685:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*72(%rax)
	movq	(%rbx), %rdi
	movb	%al, -632(%rbp)
	movb	%al, -559(%rbp)
	movzbl	88(%rdi), %eax
	movb	%al, -600(%rbp)
	movb	%al, -558(%rbp)
	movq	(%rdi), %rax
	call	*80(%rax)
	movq	(%rbx), %r14
	leaq	-432(%rbp), %rcx
	movb	%al, -592(%rbp)
	movq	8(%r14), %rsi
	movq	16(%r14), %rdx
	movb	%al, -557(%rbp)
	leaq	-448(%rbp), %rax
	movq	%rax, %rdi
	movq	%rcx, -744(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rcx, -448(%rbp)
	movq	%rax, -656(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	40(%r14), %rax
	movq	(%rbx), %r14
	leaq	-384(%rbp), %rcx
	movq	%rcx, -728(%rbp)
	movq	48(%r14), %rsi
	movq	56(%r14), %rdx
	movq	%rax, -416(%rbp)
	leaq	-400(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -576(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rcx, -400(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	80(%r14), %rax
	movq	-656(%rbp), %rsi
	leaq	64(%r15), %r14
	movq	%r14, %rdi
	movq	%rax, -368(%rbp)
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_16V8DebuggerScriptESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	(%rbx), %rdx
	movq	$0, (%rbx)
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L4688
	movq	(%rdi), %rax
	call	*8(%rax)
.L4688:
	movq	-656(%rbp), %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_16V8DebuggerScriptESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*160(%rax)
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	48(%rax), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*128(%rax)
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	movq	-608(%rbp), %rdi
	movq	%rax, %rbx
	movq	-616(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	8(%rbx), %rax
	movq	(%rbx), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%rbx), %rax
	leaq	-136(%rbp), %rcx
	movb	$1, -160(%rbp)
	movq	%rcx, -720(%rbp)
	movq	-112(%rbp), %rdx
	movq	%rax, -80(%rbp)
	movq	%rcx, -152(%rbp)
	cmpq	-616(%rbp), %rdx
	je	.L4850
	movq	%rdx, -152(%rbp)
	movq	-96(%rbp), %rdx
	movq	%rdx, -136(%rbp)
.L4690:
	movq	%rax, -120(%rbp)
	movq	-104(%rbp), %rdx
	movl	$1, %ecx
	leaq	-544(%rbp), %rdi
	movq	8(%r15), %rax
	movq	%rdx, -144(%rbp)
	movl	%r13d, %edx
	movq	24(%rax), %rsi
	call	_ZN12v8_inspector16V8StackTraceImpl7captureEPNS_10V8DebuggerEii@PLT
	movq	-544(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4691
	movq	(%rdi), %rax
	call	*8(%rax)
	testb	%al, %al
	je	.L4851
.L4691:
	movq	$0, -536(%rbp)
.L4692:
	leaq	48(%r15), %rax
	cmpb	$0, -584(%rbp)
	movq	%rax, -696(%rbp)
	je	.L4852
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*88(%rax)
	testb	%al, %al
	movq	-536(%rbp), %rax
	movq	$0, -536(%rbp)
	movq	%rax, -520(%rbp)
	jne	.L4853
	movq	(%r14), %rax
	movq	%r14, %rdi
	leaq	-88(%rbp), %rbx
	call	*96(%rax)
	movb	$1, -480(%rbp)
	movl	%eax, -476(%rbp)
	movzbl	-592(%rbp), %eax
	movb	$1, -548(%rbp)
	movb	%al, -547(%rbp)
	movzbl	-600(%rbp), %eax
	movb	$1, -552(%rbp)
	movb	%al, -551(%rbp)
	movzbl	-160(%rbp), %eax
	movq	%rbx, -104(%rbp)
	movb	%al, -112(%rbp)
	movq	-152(%rbp), %rax
	cmpq	-720(%rbp), %rax
	je	.L4854
	movq	%rax, -104(%rbp)
	movq	-136(%rbp), %rax
	movq	%rax, -88(%rbp)
.L4713:
	movq	-144(%rbp), %rax
	xorl	%r9d, %r9d
	movq	%r14, %rdi
	movq	%r12, -528(%rbp)
	movw	%r9w, -136(%rbp)
	movq	%rax, -96(%rbp)
	movq	-720(%rbp), %rax
	movq	$0, -144(%rbp)
	movq	%rax, -152(%rbp)
	movq	-120(%rbp), %rax
	movb	$1, -556(%rbp)
	movq	%rax, -72(%rbp)
	movzbl	-632(%rbp), %eax
	movb	%al, -555(%rbp)
	movq	(%r14), %rax
	call	*32(%rax)
	movq	%r14, %rdi
	movq	%rax, -592(%rbp)
	movq	(%r14), %rax
	call	*64(%rax)
	movq	%r14, %rdi
	movl	%eax, -584(%rbp)
	movq	(%r14), %rax
	call	*56(%rax)
	movq	%r14, %rdi
	movl	%eax, %r13d
	movq	(%r14), %rax
	call	*48(%rax)
	movq	%r14, %rdi
	movl	%eax, %r12d
	movq	(%r14), %rax
	call	*40(%rax)
	movq	-592(%rbp), %rdx
	movl	%r13d, %r9d
	movl	%r12d, %r8d
	movl	%eax, %ecx
	leaq	-520(%rbp), %rax
	movl	-584(%rbp), %esi
	pushq	%rax
	movq	%rax, -704(%rbp)
	leaq	-548(%rbp), %rax
	pushq	-568(%rbp)
	movq	%rax, -640(%rbp)
	pushq	%rax
	leaq	-552(%rbp), %rax
	pushq	%rax
	movq	%rax, -632(%rbp)
	leaq	-556(%rbp), %rax
	pushq	-608(%rbp)
	movq	%rax, -648(%rbp)
	pushq	%rax
	leaq	-528(%rbp), %rax
	pushq	%rax
	movq	%rax, -688(%rbp)
	movl	-624(%rbp), %eax
	pushq	%rdx
	pushq	%rax
	pushq	%rsi
.L4843:
	movq	-576(%rbp), %rdx
	movq	-656(%rbp), %rsi
	leaq	48(%r15), %rdi
	call	_ZN12v8_inspector8protocol8Debugger8Frontend12scriptParsedERKNS_8String16ES5_iiiiiS5_N30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENS8_10ValueMaybeIbEENSC_IS3_EESD_SD_NSC_IiEENS9_INS0_7Runtime10StackTraceEEE@PLT
	movq	-528(%rbp), %rdi
	addq	$80, %rsp
	testq	%rdi, %rdi
	je	.L4714
	movq	(%rdi), %rax
	call	*24(%rax)
.L4714:
	movq	-104(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4715
	call	_ZdlPv@PLT
.L4715:
	movq	-520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4711
	movq	(%rdi), %rax
	call	*24(%rax)
.L4711:
	pxor	%xmm0, %xmm0
	cmpq	$0, -392(%rbp)
	movq	40(%r15), %r12
	movq	$0, -464(%rbp)
	movaps	%xmm0, -480(%rbp)
	jne	.L4855
.L4717:
	movq	-608(%rbp), %rbx
	leaq	_ZN12v8_inspector18DebuggerAgentStateL23breakpointsByScriptHashE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r12
	cmpq	-616(%rbp), %rdi
	je	.L4721
	call	_ZdlPv@PLT
.L4721:
	testq	%r12, %r12
	je	.L4722
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*32(%rax)
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	-472(%rbp), %rsi
	movq	%rax, -520(%rbp)
	cmpq	-464(%rbp), %rsi
	je	.L4723
	movq	%rax, (%rsi)
	addq	$8, -472(%rbp)
.L4722:
	movq	-608(%rbp), %rbx
	movq	40(%r15), %r12
	leaq	_ZN12v8_inspector18DebuggerAgentStateL15breakpointHintsE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, -680(%rbp)
	cmpq	-616(%rbp), %rdi
	je	.L4725
	call	_ZdlPv@PLT
.L4725:
	movq	-472(%rbp), %rax
	movq	-480(%rbp), %rbx
	leaq	-352(%rbp), %r12
	movq	%rax, -712(%rbp)
	movq	%rbx, -664(%rbp)
	cmpq	%rbx, %rax
	je	.L4731
	movq	%r14, -624(%rbp)
	.p2align 4,,10
	.p2align 3
.L4730:
	movq	-664(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L4732
	cmpq	$0, 40(%r13)
	je	.L4732
	leaq	-336(%rbp), %rax
	xorl	%ebx, %ebx
	leaq	-304(%rbp), %r14
	movq	%rax, -568(%rbp)
	leaq	-288(%rbp), %rax
	movq	%rax, -576(%rbp)
	leaq	-256(%rbp), %rax
	movq	%rax, -584(%rbp)
	leaq	-240(%rbp), %rax
	movq	%rax, -592(%rbp)
	jmp	.L4738
	.p2align 4,,10
	.p2align 3
.L4740:
	movl	-548(%rbp), %eax
	subq	$8, %rsp
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	-656(%rbp), %rcx
	movl	-552(%rbp), %r9d
	movq	-584(%rbp), %r8
	movq	-688(%rbp), %rdi
	pushq	%rax
	call	_ZN12v8_inspector19V8DebuggerAgentImpl17setBreakpointImplERKNS_8String16ES3_S3_ii
	movq	-528(%rbp), %rax
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L4743
	movq	-704(%rbp), %rdx
	movq	%r12, %rsi
	movq	-696(%rbp), %rdi
	movq	$0, -528(%rbp)
	movq	%rax, -520(%rbp)
	call	_ZN12v8_inspector8protocol8Debugger8Frontend18breakpointResolvedERKNS_8String16ESt10unique_ptrINS1_8LocationESt14default_deleteIS7_EE@PLT
	movq	-520(%rbp), %r8
	testq	%r8, %r8
	je	.L4744
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4745
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L4746
	movq	%r8, -672(%rbp)
	call	_ZdlPv@PLT
	movq	-672(%rbp), %r8
.L4746:
	movl	$64, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L4744:
	movq	-528(%rbp), %r8
	testq	%r8, %r8
	je	.L4743
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4748
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L4749
	movq	%r8, -672(%rbp)
	call	_ZdlPv@PLT
	movq	-672(%rbp), %r8
.L4749:
	movl	$64, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L4743:
	movq	-208(%rbp), %rdi
	cmpq	-600(%rbp), %rdi
	je	.L4750
	call	_ZdlPv@PLT
.L4750:
	movq	-256(%rbp), %rdi
	cmpq	-592(%rbp), %rdi
	je	.L4845
	call	_ZdlPv@PLT
.L4845:
	movq	-304(%rbp), %rdi
	cmpq	-576(%rbp), %rdi
	je	.L4734
	call	_ZdlPv@PLT
.L4734:
	movq	-352(%rbp), %rdi
	cmpq	-568(%rbp), %rdi
	je	.L4735
	call	_ZdlPv@PLT
.L4735:
	movq	-112(%rbp), %rdi
	cmpq	-616(%rbp), %rdi
	je	.L4737
	call	_ZdlPv@PLT
.L4737:
	addq	$1, %rbx
	cmpq	40(%r13), %rbx
	jnb	.L4732
.L4738:
	movq	-608(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue2atEm@PLT
	movq	-568(%rbp), %rax
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	movq	-104(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-80(%rbp), %rax
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movw	%r8w, -288(%rbp)
	movq	-632(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, -320(%rbp)
	movq	-648(%rbp), %rsi
	movq	-576(%rbp), %rax
	movq	-640(%rbp), %r8
	movq	$0, -296(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -304(%rbp)
	movl	$0, -552(%rbp)
	movl	$0, -548(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_117parseBreakpointIdERKNS_8String16EPNS0_14BreakpointTypeEPS1_PiS7_
	movq	8(%r15), %rdi
	movl	-556(%rbp), %edx
	movq	%r14, %rcx
	movq	-624(%rbp), %rsi
	call	_ZN12v8_inspectorL7matchesEPNS_15V8InspectorImplERKNS_16V8DebuggerScriptENS_12_GLOBAL__N_114BreakpointTypeERKNS_8String16E
	testb	%al, %al
	je	.L4845
	movq	-592(%rbp), %rax
	movq	-72(%rbp), %rdi
	xorl	%esi, %esi
	movq	$0, -248(%rbp)
	movw	%si, -240(%rbp)
	movq	-584(%rbp), %rsi
	movq	%rax, -256(%rbp)
	movq	$0, -224(%rbp)
	movq	(%rdi), %rax
	call	*56(%rax)
	xorl	%edi, %edi
	leaq	-192(%rbp), %rax
	movq	$0, -200(%rbp)
	movw	%di, -192(%rbp)
	movq	-680(%rbp), %rdi
	leaq	-208(%rbp), %r8
	movq	%rax, -600(%rbp)
	movq	%rax, -208(%rbp)
	movq	$0, -176(%rbp)
	testq	%rdi, %rdi
	je	.L4740
	movq	%r8, %rdx
	movq	%r12, %rsi
	movq	%r8, -672(%rbp)
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getStringERKNS_8String16EPS2_@PLT
	movq	-672(%rbp), %r8
	testb	%al, %al
	je	.L4740
	movq	-640(%rbp), %rcx
	movq	-632(%rbp), %rdx
	movq	%r8, %rsi
	movq	-624(%rbp), %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_124adjustBreakpointLocationERKNS_16V8DebuggerScriptERKNS_8String16EPiS7_
	jmp	.L4740
	.p2align 4,,10
	.p2align 3
.L4822:
	movq	(%rbx), %rsi
	leaq	-160(%rbp), %r12
	movl	$4294967295, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	leaq	-112(%rbp), %rax
	movq	(%rbx), %r13
	xorl	%edx, %edx
	movq	%rax, %r14
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -608(%rbp)
	call	_ZN12v8_inspector13findSourceURLERKNS_8String16Eb@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector16V8DebuggerScript12setSourceURLERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	movq	%rax, -616(%rbp)
	cmpq	%rax, %rdi
	je	.L4682
	call	_ZdlPv@PLT
.L4682:
	movq	(%rbx), %r13
	movq	%r12, %rsi
	movq	-608(%rbp), %r12
	xorl	%edx, %edx
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movq	104(%rax), %r14
	call	_ZN12v8_inspector16findSourceMapURLERKNS_8String16Eb@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	*%r14
	movq	-112(%rbp), %rdi
	cmpq	-616(%rbp), %rdi
	je	.L4683
	call	_ZdlPv@PLT
.L4683:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4681
	call	_ZdlPv@PLT
	jmp	.L4681
	.p2align 4,,10
	.p2align 3
.L4853:
	movzbl	-592(%rbp), %eax
	leaq	-88(%rbp), %rbx
	movb	$1, -480(%rbp)
	movl	$0, -476(%rbp)
	movb	%al, -547(%rbp)
	movzbl	-600(%rbp), %eax
	movb	$1, -548(%rbp)
	movb	%al, -551(%rbp)
	movzbl	-160(%rbp), %eax
	movb	$1, -552(%rbp)
	movb	%al, -112(%rbp)
	movq	-152(%rbp), %rax
	movq	%rbx, -104(%rbp)
	cmpq	-720(%rbp), %rax
	je	.L4856
	movq	%rax, -104(%rbp)
	movq	-136(%rbp), %rax
	movq	%rax, -88(%rbp)
.L4707:
	movq	-144(%rbp), %rax
	xorl	%r10d, %r10d
	movq	%r14, %rdi
	movq	$0, -144(%rbp)
	movw	%r10w, -136(%rbp)
	movq	%rax, -96(%rbp)
	movq	-720(%rbp), %rax
	movb	$1, -556(%rbp)
	movq	%rax, -152(%rbp)
	movq	-120(%rbp), %rax
	movq	%r12, -528(%rbp)
	movq	%rax, -72(%rbp)
	movzbl	-632(%rbp), %eax
	movb	%al, -555(%rbp)
	movq	(%r14), %rax
	call	*32(%rax)
	leaq	-520(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	%rcx
	movq	%rcx, -704(%rbp)
	leaq	-548(%rbp), %rcx
	pushq	-568(%rbp)
	movq	%rcx, -640(%rbp)
	pushq	%rcx
	leaq	-552(%rbp), %rcx
	pushq	%rcx
	movq	%rcx, -632(%rbp)
	leaq	-556(%rbp), %rcx
	pushq	-608(%rbp)
	movq	%rcx, -648(%rbp)
	pushq	%rcx
	leaq	-528(%rbp), %rcx
	pushq	%rcx
	pushq	%rax
	movl	-624(%rbp), %eax
	movq	%rcx, -688(%rbp)
	xorl	%ecx, %ecx
	pushq	%rax
	pushq	$0
	jmp	.L4843
.L4851:
	movq	16(%r15), %rdx
	movq	-544(%rbp), %rsi
	leaq	-536(%rbp), %rdi
	xorl	%ecx, %ecx
	call	_ZNK12v8_inspector16V8StackTraceImpl24buildInspectorObjectImplEPNS_10V8DebuggerEi@PLT
	jmp	.L4692
	.p2align 4,,10
	.p2align 3
.L4732:
	addq	$8, -664(%rbp)
	movq	-664(%rbp), %rax
	cmpq	%rax, -712(%rbp)
	jne	.L4730
	movq	-624(%rbp), %r14
.L4731:
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl42setScriptInstrumentationBreakpointIfNeededEPNS_16V8DebuggerScriptE
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4698
	call	_ZdlPv@PLT
.L4698:
	movq	-536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4699
	movq	(%rdi), %rax
	call	*24(%rax)
.L4699:
	movq	-544(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4700
	movq	(%rdi), %rax
	call	*64(%rax)
.L4700:
	movq	-152(%rbp), %rdi
	cmpq	-720(%rbp), %rdi
	je	.L4701
	call	_ZdlPv@PLT
.L4701:
	movq	-400(%rbp), %rdi
	cmpq	-728(%rbp), %rdi
	je	.L4702
	call	_ZdlPv@PLT
.L4702:
	movq	-448(%rbp), %rdi
	cmpq	-744(%rbp), %rdi
	je	.L4704
	call	_ZdlPv@PLT
.L4704:
	movq	-736(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4857
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4748:
	.cfi_restore_state
	movq	%r8, %rdi
	call	*%rax
	jmp	.L4743
	.p2align 4,,10
	.p2align 3
.L4745:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L4744
	.p2align 4,,10
	.p2align 3
.L4852:
	movq	-536(%rbp), %rax
	movq	%r14, %rdi
	leaq	-88(%rbp), %rbx
	movq	$0, -536(%rbp)
	movq	%rax, -520(%rbp)
	movq	(%r14), %rax
	call	*96(%rax)
	movb	$1, -480(%rbp)
	movl	%eax, -476(%rbp)
	movzbl	-592(%rbp), %eax
	movb	$1, -548(%rbp)
	movb	%al, -547(%rbp)
	movzbl	-600(%rbp), %eax
	movb	$1, -552(%rbp)
	movb	%al, -551(%rbp)
	movzbl	-160(%rbp), %eax
	movq	%rbx, -104(%rbp)
	movb	%al, -112(%rbp)
	movq	-152(%rbp), %rax
	cmpq	-720(%rbp), %rax
	je	.L4858
	movq	%rax, -104(%rbp)
	movq	-136(%rbp), %rax
	movq	%rax, -88(%rbp)
.L4695:
	movq	-144(%rbp), %rax
	xorl	%r11d, %r11d
	movq	%r12, -528(%rbp)
	movq	%r14, %rdi
	movw	%r11w, -136(%rbp)
	movq	%rax, -96(%rbp)
	movq	-720(%rbp), %rax
	movq	$0, -144(%rbp)
	movq	%rax, -152(%rbp)
	movq	-120(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	(%r14), %rax
	call	*32(%rax)
	movq	%r14, %rdi
	movq	%rax, -584(%rbp)
	movq	(%r14), %rax
	call	*64(%rax)
	movq	%r14, %rdi
	movl	%eax, %r15d
	movq	(%r14), %rax
	call	*56(%rax)
	movq	%r14, %rdi
	movl	%eax, %r13d
	movq	(%r14), %rax
	call	*48(%rax)
	movq	%r14, %rdi
	movl	%eax, %r12d
	movq	(%r14), %rax
	call	*40(%rax)
	subq	$8, %rsp
	movl	%r13d, %r9d
	movl	%r12d, %r8d
	movl	%eax, %ecx
	leaq	-520(%rbp), %rax
	movq	-584(%rbp), %rdx
	movq	-696(%rbp), %rdi
	pushq	%rax
	leaq	-548(%rbp), %rax
	movq	-656(%rbp), %rsi
	pushq	-568(%rbp)
	pushq	%rax
	leaq	-552(%rbp), %rax
	pushq	%rax
	leaq	-528(%rbp), %rax
	pushq	-608(%rbp)
	pushq	%rax
	movl	-624(%rbp), %eax
	pushq	%rdx
	movq	-576(%rbp), %rdx
	pushq	%rax
	pushq	%r15
	call	_ZN12v8_inspector8protocol8Debugger8Frontend19scriptFailedToParseERKNS_8String16ES5_iiiiiS5_N30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENS8_10ValueMaybeIS3_EENSC_IbEESE_NSC_IiEENS9_INS0_7Runtime10StackTraceEEE@PLT
	movq	-528(%rbp), %rdi
	addq	$80, %rsp
	testq	%rdi, %rdi
	je	.L4696
	movq	(%rdi), %rax
	call	*24(%rax)
.L4696:
	movq	-104(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4697
	call	_ZdlPv@PLT
.L4697:
	movq	-520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4698
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4698
.L4850:
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, -136(%rbp)
	jmp	.L4690
.L4855:
	movq	-608(%rbp), %rbx
	leaq	_ZN12v8_inspector18DebuggerAgentStateL16breakpointsByUrlE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r12
	cmpq	-616(%rbp), %rdi
	je	.L4718
	call	_ZdlPv@PLT
.L4718:
	testq	%r12, %r12
	je	.L4719
	movq	-576(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	-704(%rbp), %rsi
	movq	-568(%rbp), %rdi
	movq	%rax, -520(%rbp)
	call	_ZNSt6vectorIPN12v8_inspector8protocol15DictionaryValueESaIS3_EE12emplace_backIJS3_EEEvDpOT_
.L4719:
	movq	-608(%rbp), %rbx
	movq	40(%r15), %r12
	leaq	_ZN12v8_inspector18DebuggerAgentStateL18breakpointsByRegexE(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	-568(%rbp), %rdi
	movq	-704(%rbp), %rsi
	movq	%rax, -520(%rbp)
	call	_ZNSt6vectorIPN12v8_inspector8protocol15DictionaryValueESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	movq	-112(%rbp), %rdi
	cmpq	-616(%rbp), %rdi
	je	.L4720
	call	_ZdlPv@PLT
.L4720:
	movq	40(%r15), %r12
	jmp	.L4717
.L4858:
	movdqu	-136(%rbp), %xmm2
	movups	%xmm2, -88(%rbp)
	jmp	.L4695
.L4854:
	movdqu	-136(%rbp), %xmm4
	movups	%xmm4, -88(%rbp)
	jmp	.L4713
.L4856:
	movdqu	-136(%rbp), %xmm3
	movups	%xmm3, -88(%rbp)
	jmp	.L4707
.L4723:
	movq	-704(%rbp), %rdx
	movq	-568(%rbp), %rdi
	call	_ZNSt6vectorIPN12v8_inspector8protocol15DictionaryValueESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L4722
.L4857:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8061:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl14didParseSourceESt10unique_ptrINS_16V8DebuggerScriptESt14default_deleteIS2_EEb, .-_ZN12v8_inspector19V8DebuggerAgentImpl14didParseSourceESt10unique_ptrINS_16V8DebuggerScriptESt14default_deleteIS2_EEb
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSB_20_Node_const_iteratorIS9_Lb0ELb0EEE,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSB_20_Node_const_iteratorIS9_Lb0ELb0EEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSB_20_Node_const_iteratorIS9_Lb0ELb0EEE
	.type	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSB_20_Node_const_iteratorIS9_Lb0ELb0EEE, @function
_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSB_20_Node_const_iteratorIS9_Lb0ELb0EEE:
.LFB13489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rsi
	movslq	8(%r12), %rax
	movq	(%rbx), %r8
	divq	%rsi
	leaq	0(,%rdx,8), %r10
	movq	%rdx, %rdi
	leaq	(%r8,%r10), %r9
	movq	(%r9), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L4860:
	movq	%rdx, %rcx
	movq	(%rdx), %rdx
	cmpq	%rdx, %r12
	jne	.L4860
	movq	(%r12), %r13
	cmpq	%rcx, %rax
	je	.L4875
	testq	%r13, %r13
	je	.L4863
	movslq	8(%r13), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %rdi
	je	.L4863
	movq	%rcx, (%r8,%rdx,8)
	movq	(%r12), %r13
.L4863:
	movq	%r13, (%rcx)
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4865
	movq	(%rdi), %rax
	call	*24(%rax)
.L4865:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4875:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L4866
	movslq	8(%r13), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %rdi
	je	.L4863
	movq	%rcx, (%r8,%rdx,8)
	addq	(%rbx), %r10
	movq	(%r10), %rax
	movq	%r10, %r9
.L4862:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L4876
.L4864:
	movq	$0, (%r9)
	movq	(%r12), %r13
	jmp	.L4863
.L4866:
	movq	%rcx, %rax
	jmp	.L4862
.L4876:
	movq	%r13, 16(%rbx)
	jmp	.L4864
	.cfi_endproc
.LFE13489:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSB_20_Node_const_iteratorIS9_Lb0ELb0EEE, .-_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSB_20_Node_const_iteratorIS9_Lb0ELb0EEE
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl8didPauseEiN2v85LocalINS1_5ValueEEERKSt6vectorIiSaIiEENS1_5debug13ExceptionTypeEbbb.str1.1,"aMS",@progbits,1
.LC33:
	.string	"uncaught"
.LC34:
	.string	"reasons"
.LC35:
	.string	"reason"
.LC36:
	.string	"auxData"
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl8didPauseEiN2v85LocalINS1_5ValueEEERKSt6vectorIiSaIiEENS1_5debug13ExceptionTypeEbbb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl8didPauseEiN2v85LocalINS1_5ValueEEERKSt6vectorIiSaIiEENS1_5debug13ExceptionTypeEbbb
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl8didPauseEiN2v85LocalINS1_5ValueEEERKSt6vectorIiSaIiEENS1_5debug13ExceptionTypeEbbb, @function
_ZN12v8_inspector19V8DebuggerAgentImpl8didPauseEiN2v85LocalINS1_5ValueEEERKSt6vectorIiSaIiEENS1_5debug13ExceptionTypeEbbb:
.LFB8084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$456, %rsp
	movl	16(%rbp), %r14d
	movl	24(%rbp), %r15d
	movl	%esi, -392(%rbp)
	movl	%r8d, -400(%rbp)
	movq	56(%rdi), %rsi
	movl	%r9d, -408(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-304(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -464(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	pxor	%xmm0, %xmm0
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	testb	%r14b, %r14b
	jne	.L5084
	testb	%r15b, %r15b
	jne	.L5085
	testq	%r12, %r12
	je	.L5086
	leaq	-320(%rbp), %rax
	movq	24(%r13), %rsi
	movl	-392(%rbp), %edx
	leaq	-112(%rbp), %r15
	movq	%rax, %rcx
	movq	%r15, %rdi
	movq	%rax, -472(%rbp)
	movq	$0, -320(%rbp)
	call	_ZN12v8_inspector22V8InspectorSessionImpl18findInjectedScriptEiRPNS_14InjectedScriptE@PLT
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	movq	%rax, -456(%rbp)
	cmpq	%rax, %rdi
	je	.L4881
	call	_ZdlPv@PLT
.L4881:
	cmpq	$0, -320(%rbp)
	leaq	-272(%rbp), %r14
	je	.L5081
	cmpl	$1, -400(%rbp)
	movq	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum16PromiseRejectionE(%rip), %rsi
	leaq	-208(%rbp), %rdi
	cmovne	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum9ExceptionE(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-160(%rbp), %rax
	movq	-320(%rbp), %r14
	leaq	_ZN12v8_inspectorL21kBacktraceObjectGroupE(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -440(%rbp)
	movq	$0, -312(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	leaq	-312(%rbp), %rax
	movq	-440(%rbp), %rcx
	movl	$1, %r8d
	movq	%rax, %r9
	movq	%rax, -480(%rbp)
	call	_ZN12v8_inspector14InjectedScript10wrapObjectEN2v85LocalINS1_5ValueEEERKNS_8String16ENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE@PLT
	movq	-104(%rbp), %rdi
	cmpq	-456(%rbp), %rdi
	je	.L4885
	call	_ZdlPv@PLT
.L4885:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	movq	%rax, -488(%rbp)
	cmpq	%rax, %rdi
	je	.L4886
	call	_ZdlPv@PLT
.L4886:
	movq	-312(%rbp), %r12
	leaq	-240(%rbp), %rax
	movq	%rax, -400(%rbp)
	testq	%r12, %r12
	je	.L5087
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol7Runtime12RemoteObject7toValueEv@PLT
	movq	-240(%rbp), %r12
	leaq	.LC33(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movzbl	-408(%rbp), %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	movq	%rax, -392(%rbp)
	cmpq	%rax, %rdi
	je	.L4887
	call	_ZdlPv@PLT
.L4887:
	movq	-392(%rbp), %rax
	movq	-208(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-272(%rbp), %r14
	movq	%rax, -112(%rbp)
	movq	-200(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r12, -72(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, -80(%rbp)
	call	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE12emplace_backIJS9_EEEvDpOT_
	movq	%r15, %rdi
	call	_ZNSt4pairIN12v8_inspector8String16ESt10unique_ptrINS0_8protocol15DictionaryValueESt14default_deleteIS4_EEED1Ev
	movq	-312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4889
	movq	(%rdi), %rax
	call	*24(%rax)
.L4889:
	movq	-208(%rbp), %rdi
	leaq	-192(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4879
	call	_ZdlPv@PLT
	jmp	.L4879
	.p2align 4,,10
	.p2align 3
.L5085:
	movq	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum6AssertE(%rip), %rsi
	leaq	-112(%rbp), %r15
.L5080:
	movq	%r15, %rdi
	leaq	-272(%rbp), %r14
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	$0, -72(%rbp)
	call	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE12emplace_backIJS9_EEEvDpOT_
	movq	%r15, %rdi
	call	_ZNSt4pairIN12v8_inspector8String16ESt10unique_ptrINS0_8protocol15DictionaryValueESt14default_deleteIS4_EEED1Ev
.L5082:
	leaq	-320(%rbp), %rax
	movq	%rax, -472(%rbp)
	leaq	-88(%rbp), %rax
	movq	%rax, -456(%rbp)
.L5081:
	leaq	-160(%rbp), %rax
	movq	%rax, -440(%rbp)
	leaq	-312(%rbp), %rax
	movq	%rax, -480(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, -488(%rbp)
	leaq	-240(%rbp), %rax
	movq	%rax, -400(%rbp)
.L4879:
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	232(%r13), %rcx
	movq	%rax, -416(%rbp)
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	8(%rbx), %rax
	movq	(%rbx), %rbx
	movq	%rcx, -424(%rbp)
	leaq	-96(%rbp), %rcx
	movq	%rax, -408(%rbp)
	movq	%rcx, -432(%rbp)
	cmpq	%rax, %rbx
	je	.L4905
	.p2align 4,,10
	.p2align 3
.L4904:
	movslq	(%rbx), %r9
	movq	240(%r13), %rcx
	xorl	%edx, %edx
	movq	%r9, %rax
	movq	%r9, %rsi
	divq	%rcx
	movq	232(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L4894
	movq	(%rax), %r12
	movl	8(%r12), %edi
	jmp	.L4896
	.p2align 4,,10
	.p2align 3
.L5088:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L4894
	movslq	8(%r12), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rcx
	cmpq	%rdx, %r10
	jne	.L4894
.L4896:
	cmpl	%edi, %esi
	jne	.L5088
	movq	16(%r12), %rax
	movq	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum15InstrumentationE(%rip), %rsi
	movq	%r15, %rdi
	movq	$0, 16(%r12)
	movq	%rax, -392(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-392(%rbp), %rax
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, -72(%rbp)
	call	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE12emplace_backIJS9_EEEvDpOT_
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4972
	movq	(%rdi), %rax
	call	*24(%rax)
.L4972:
	movq	-112(%rbp), %rdi
	cmpq	-432(%rbp), %rdi
	je	.L4897
	call	_ZdlPv@PLT
.L4897:
	movq	-424(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSB_20_Node_const_iteratorIS9_Lb0ELb0EEE
.L4903:
	addq	$4, %rbx
	cmpq	%rbx, -408(%rbp)
	jne	.L4904
.L4905:
	movq	384(%r13), %rax
	xorl	%r12d, %r12d
	movq	392(%r13), %rcx
	movabsq	$-6148914691236517205, %rbx
	cmpq	%rcx, %rax
	je	.L4893
	.p2align 4,,10
	.p2align 3
.L4892:
	leaq	(%r12,%r12,2), %rsi
	movq	%r14, %rdi
	addq	$1, %r12
	salq	$4, %rsi
	addq	%rax, %rsi
	call	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE12emplace_backIJS9_EEEvDpOT_
	movq	392(%r13), %rcx
	movq	384(%r13), %rax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	sarq	$4, %rdx
	imulq	%rbx, %rdx
	cmpq	%rdx, %r12
	jb	.L4892
.L4893:
	movq	400(%r13), %rdx
	pxor	%xmm0, %xmm0
	movq	$0, 400(%r13)
	movq	%rcx, %xmm1
	movq	-400(%rbp), %rdi
	movups	%xmm0, 384(%r13)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rdx, -224(%rbp)
	movaps	%xmm0, -240(%rbp)
	call	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EED1Ev
	movq	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum5OtherE(%rip), %rsi
	movq	-440(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-272(%rbp), %rbx
	movq	-264(%rbp), %rax
	movabsq	$-6148914691236517205, %rcx
	subq	%rbx, %rax
	movq	%rax, %rdx
	sarq	$4, %rdx
	imulq	%rcx, %rdx
	cmpq	$48, %rax
	je	.L5089
	xorl	%ebx, %ebx
	cmpq	$1, %rdx
	ja	.L5090
.L4907:
	leaq	-376(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	$0, -376(%rbp)
	call	_ZN12v8_inspector19V8DebuggerAgentImpl17currentCallFramesEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EE
	movl	-112(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L5091
.L4921:
	leaq	-352(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl25currentScheduledAsyncCallEv
	movq	-352(%rbp), %rax
	leaq	-360(%rbp), %rdi
	movq	%r13, %rsi
	movq	$0, -352(%rbp)
	movq	%rax, -240(%rbp)
	call	_ZN12v8_inspector19V8DebuggerAgentImpl25currentExternalStackTraceEv
	movq	-360(%rbp), %rax
	leaq	-368(%rbp), %rdi
	movq	%r13, %rsi
	movq	$0, -360(%rbp)
	movq	%rax, -312(%rbp)
	call	_ZN12v8_inspector19V8DebuggerAgentImpl22currentAsyncStackTraceEv
	movq	-368(%rbp), %rax
	leaq	48(%r13), %rdi
	pushq	-400(%rbp)
	pushq	-480(%rbp)
	leaq	-336(%rbp), %rcx
	leaq	-344(%rbp), %rsi
	movq	%rax, -320(%rbp)
	leaq	-328(%rbp), %r8
	movq	-416(%rbp), %rax
	movq	-440(%rbp), %rdx
	movq	$0, -368(%rbp)
	movq	-472(%rbp), %r9
	movq	%rbx, -336(%rbp)
	movq	%rax, -328(%rbp)
	movq	-376(%rbp), %rax
	movq	$0, -376(%rbp)
	movq	%rax, -344(%rbp)
	call	_ZN12v8_inspector8protocol8Debugger8Frontend6pausedESt10unique_ptrISt6vectorIS3_INS1_9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS0_15DictionaryValueEEENSJ_IS4_ISD_SaISD_EEEENSJ_INS0_7Runtime10StackTraceEEENSJ_INSP_12StackTraceIdEEEST_@PLT
	movq	-344(%rbp), %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L4928
	movq	8(%r12), %rbx
	movq	(%r12), %r15
	cmpq	%r15, %rbx
	jne	.L4932
	jmp	.L4929
	.p2align 4,,10
	.p2align 3
.L5093:
	call	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4930:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L5092
.L4932:
	movq	(%r15), %r13
	testq	%r13, %r13
	je	.L4930
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L5093
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L4932
	.p2align 4,,10
	.p2align 3
.L5092:
	movq	(%r12), %r15
.L4929:
	testq	%r15, %r15
	je	.L4933
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L4933:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4928:
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4934
	movq	(%rdi), %rax
	call	*24(%rax)
.L4934:
	movq	-328(%rbp), %r13
	testq	%r13, %r13
	je	.L4935
	movq	8(%r13), %rbx
	movq	0(%r13), %r12
	cmpq	%r12, %rbx
	je	.L4936
	.p2align 4,,10
	.p2align 3
.L4940:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4937
	call	_ZdlPv@PLT
	addq	$40, %r12
	cmpq	%r12, %rbx
	jne	.L4940
.L4938:
	movq	0(%r13), %r12
.L4936:
	testq	%r12, %r12
	je	.L4941
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4941:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4935:
	movq	-320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4942
	movq	(%rdi), %rax
	call	*24(%rax)
.L4942:
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4943
	movq	(%rdi), %rax
	call	*24(%rax)
.L4943:
	movq	-312(%rbp), %r12
	testq	%r12, %r12
	je	.L4944
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4945
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rbx
	movq	%rax, %xmm2
	leaq	80(%r12), %rax
	movq	%rbx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L4946
	call	_ZdlPv@PLT
.L4946:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4947
	call	_ZdlPv@PLT
.L4947:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4944:
	movq	-360(%rbp), %r12
	testq	%r12, %r12
	je	.L4948
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4949
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rbx
	movq	%rax, %xmm3
	leaq	80(%r12), %rax
	movq	%rbx, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L4950
	call	_ZdlPv@PLT
.L4950:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4951
	call	_ZdlPv@PLT
.L4951:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4948:
	movq	-240(%rbp), %r12
	testq	%r12, %r12
	je	.L4952
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4953
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rbx
	movq	%rax, %xmm4
	leaq	80(%r12), %rax
	movq	%rbx, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L4954
	call	_ZdlPv@PLT
.L4954:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4955
	call	_ZdlPv@PLT
.L4955:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4952:
	movq	-352(%rbp), %r12
	testq	%r12, %r12
	je	.L4956
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12StackTraceIdD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4957
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12StackTraceIdE(%rip), %rax
	movq	64(%r12), %rdi
	leaq	-64(%rax), %rbx
	movq	%rax, %xmm5
	leaq	80(%r12), %rax
	movq	%rbx, %xmm0
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L4958
	call	_ZdlPv@PLT
.L4958:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4959
	call	_ZdlPv@PLT
.L4959:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4956:
	movq	-104(%rbp), %rdi
	cmpq	-456(%rbp), %rdi
	je	.L4960
	call	_ZdlPv@PLT
.L4960:
	movq	-376(%rbp), %r12
	testq	%r12, %r12
	je	.L4961
	movq	8(%r12), %rbx
	movq	(%r12), %r15
	cmpq	%r15, %rbx
	jne	.L4965
	jmp	.L4962
	.p2align 4,,10
	.p2align 3
.L5095:
	call	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4963:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L5094
.L4965:
	movq	(%r15), %r13
	testq	%r13, %r13
	je	.L4963
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L5095
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L4965
	.p2align 4,,10
	.p2align 3
.L5094:
	movq	(%r12), %r15
.L4962:
	testq	%r15, %r15
	je	.L4966
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L4966:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4961:
	movq	-160(%rbp), %rdi
	cmpq	-488(%rbp), %rdi
	je	.L4967
	call	_ZdlPv@PLT
.L4967:
	movq	%r14, %rdi
	call	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EED1Ev
	movq	-464(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5096
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4894:
	.cfi_restore_state
	movq	184(%r13), %rdi
	movq	%r9, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	176(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L4903
	movq	(%rax), %rcx
	movl	8(%rcx), %r8d
	jmp	.L4901
	.p2align 4,,10
	.p2align 3
.L5097:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L4903
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	cmpq	%r9, %rdx
	jne	.L4903
.L4901:
	cmpl	%r8d, %esi
	jne	.L5097
	movq	-416(%rbp), %rax
	leaq	16(%rcx), %r12
	movq	8(%rax), %r8
	cmpq	16(%rax), %r8
	je	.L5098
	leaq	16(%r8), %rax
	movq	%r8, %rdi
	movq	%rcx, -448(%rbp)
	movq	%rax, (%r8)
	movq	24(%rcx), %rax
	movq	16(%rcx), %rsi
	movq	%r8, -392(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-448(%rbp), %rcx
	movq	-392(%rbp), %r8
	movq	48(%rcx), %rax
	movq	%rax, 32(%r8)
	movq	-416(%rbp), %rax
	addq	$40, 8(%rax)
.L4902:
	movq	-400(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_117parseBreakpointIdERKNS_8String16EPNS0_14BreakpointTypeEPS1_PiS7_
	cmpl	$5, -240(%rbp)
	jne	.L4903
	movq	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum12DebugCommandE(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	$0, -72(%rbp)
	call	_ZNSt6vectorISt4pairIN12v8_inspector8String16ESt10unique_ptrINS1_8protocol15DictionaryValueESt14default_deleteIS5_EEESaIS9_EE12emplace_backIJS9_EEEvDpOT_
	movq	%r15, %rdi
	call	_ZNSt4pairIN12v8_inspector8String16ESt10unique_ptrINS0_8protocol15DictionaryValueESt14default_deleteIS4_EEED1Ev
	jmp	.L4903
	.p2align 4,,10
	.p2align 3
.L4937:
	addq	$40, %r12
	cmpq	%r12, %rbx
	jne	.L4940
	jmp	.L4938
	.p2align 4,,10
	.p2align 3
.L5098:
	movq	%rax, %rdi
	movq	%r12, %rdx
	movq	%r8, %rsi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L4902
	.p2align 4,,10
	.p2align 3
.L5090:
	movq	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum9AmbiguousE(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-440(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movq	%rax, -128(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -392(%rbp)
	cmpq	%rax, %rdi
	je	.L4908
	call	_ZdlPv@PLT
.L4908:
	movl	$40, %edi
	xorl	%ebx, %ebx
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -408(%rbp)
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	-272(%rbp), %rax
	cmpq	%rax, -264(%rbp)
	je	.L4918
	movq	%r14, -432(%rbp)
	movq	%r13, -424(%rbp)
	movq	%rbx, %r13
	.p2align 4,,10
	.p2align 3
.L4909:
	movl	$96, %edi
	leaq	0(%r13,%r13,2), %rbx
	call	_Znwm@PLT
	salq	$4, %rbx
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	-272(%rbp), %r14
	leaq	.LC35(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	addq	%rbx, %r14
	movq	%r14, %rdx
	call	_ZN12v8_inspector8protocol15DictionaryValue9setStringERKNS_8String16ES4_@PLT
	movq	-112(%rbp), %rdi
	cmpq	-392(%rbp), %rdi
	je	.L4912
	call	_ZdlPv@PLT
.L4912:
	addq	-272(%rbp), %rbx
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L4913
	movq	$0, 40(%rbx)
	leaq	.LC36(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-400(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue9setObjectERKNS_8String16ESt10unique_ptrIS1_St14default_deleteIS1_EE@PLT
	movq	-112(%rbp), %rdi
	cmpq	-392(%rbp), %rdi
	je	.L4914
	call	_ZdlPv@PLT
.L4914:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4913
	movq	(%rdi), %rax
	call	*24(%rax)
.L4913:
	movq	-408(%rbp), %rdi
	movq	-400(%rbp), %rsi
	movq	%r12, -240(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4916
	movq	(%rdi), %rax
	addq	$1, %r13
	movabsq	$-6148914691236517205, %rbx
	call	*24(%rax)
	movq	-264(%rbp), %rax
	subq	-272(%rbp), %rax
	sarq	$4, %rax
	imulq	%rbx, %rax
	cmpq	%r13, %rax
	ja	.L4909
.L5083:
	movq	-424(%rbp), %r13
	movq	-432(%rbp), %r14
.L4918:
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	-408(%rbp), %rax
	leaq	.LC34(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-400(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setArrayERKNS_8String16ESt10unique_ptrINS0_9ListValueESt14default_deleteIS6_EE@PLT
	movq	-112(%rbp), %rdi
	cmpq	-392(%rbp), %rdi
	je	.L4911
	call	_ZdlPv@PLT
.L4911:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4907
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4907
	.p2align 4,,10
	.p2align 3
.L4916:
	movq	-264(%rbp), %rax
	subq	-272(%rbp), %rax
	addq	$1, %r13
	movabsq	$-6148914691236517205, %rbx
	sarq	$4, %rax
	imulq	%rbx, %rax
	cmpq	%rax, %r13
	jb	.L4909
	jmp	.L5083
	.p2align 4,,10
	.p2align 3
.L5091:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	-376(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rcx, -392(%rbp)
	movq	%rax, -376(%rbp)
	movups	%xmm0, (%rax)
	testq	%rcx, %rcx
	je	.L4921
	movq	8(%rcx), %r12
	movq	(%rcx), %r15
	cmpq	%r15, %r12
	je	.L4923
	movq	%r13, -408(%rbp)
	jmp	.L4926
	.p2align 4,,10
	.p2align 3
.L5100:
	call	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4924:
	addq	$8, %r15
	cmpq	%r15, %r12
	je	.L5099
.L4926:
	movq	(%r15), %r13
	testq	%r13, %r13
	je	.L4924
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L5100
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %r12
	jne	.L4926
	.p2align 4,,10
	.p2align 3
.L5099:
	movq	-392(%rbp), %rax
	movq	-408(%rbp), %r13
	movq	(%rax), %r15
.L4923:
	testq	%r15, %r15
	je	.L4927
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L4927:
	movq	-392(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L4921
	.p2align 4,,10
	.p2align 3
.L5084:
	movq	_ZN12v8_inspector8protocol8Debugger6Paused10ReasonEnum3OOME(%rip), %rsi
	leaq	-112(%rbp), %r15
	jmp	.L5080
	.p2align 4,,10
	.p2align 3
.L5086:
	leaq	-112(%rbp), %r15
	leaq	-272(%rbp), %r14
	jmp	.L5082
	.p2align 4,,10
	.p2align 3
.L5089:
	movq	-440(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%rbx), %rax
	movq	%rax, -128(%rbp)
	movq	-272(%rbp), %rax
	movq	40(%rax), %rbx
	movq	$0, 40(%rax)
	jmp	.L4907
	.p2align 4,,10
	.p2align 3
.L4957:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4956
	.p2align 4,,10
	.p2align 3
.L4953:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4952
	.p2align 4,,10
	.p2align 3
.L4949:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4948
	.p2align 4,,10
	.p2align 3
.L4945:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4944
.L5087:
	leaq	-96(%rbp), %rax
	movq	%rax, -392(%rbp)
	jmp	.L4887
.L5096:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8084:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl8didPauseEiN2v85LocalINS1_5ValueEEERKSt6vectorIiSaIiEENS1_5debug13ExceptionTypeEbbb, .-_ZN12v8_inspector19V8DebuggerAgentImpl8didPauseEiN2v85LocalINS1_5ValueEEERKSt6vectorIiSaIiEENS1_5debug13ExceptionTypeEbbb
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl10enableImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl10enableImplEv
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl10enableImplEv, @function
_ZN12v8_inspector19V8DebuggerAgentImpl10enableImplEv:
.LFB7890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN12v8_inspector18DebuggerAgentStateL15debuggerEnabledE(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	40(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$1, 32(%rdi)
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5102
	call	_ZdlPv@PLT
.L5102:
	movq	16(%r12), %rdi
	leaq	-112(%rbp), %r14
	call	_ZN12v8_inspector10V8Debugger6enableEv@PLT
	movq	24(%r12), %rax
	movq	16(%r12), %rsi
	movq	%r12, %rcx
	leaq	-144(%rbp), %rdi
	movl	16(%rax), %edx
	call	_ZN12v8_inspector10V8Debugger18getCompiledScriptsEiPNS_19V8DebuggerAgentImplE@PLT
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %rbx
	cmpq	%r13, %rbx
	je	.L5108
	.p2align 4,,10
	.p2align 3
.L5109:
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	$1, %edx
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector19V8DebuggerAgentImpl14didParseSourceESt10unique_ptrINS_16V8DebuggerScriptESt14default_deleteIS2_EEb
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5106
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r13
	jne	.L5109
.L5108:
	movb	$1, 409(%r12)
	movq	16(%r12), %rdi
	movl	$1, %esi
	call	_ZN12v8_inspector10V8Debugger20setBreakpointsActiveEb@PLT
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	movl	16(%rax), %esi
	call	_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi@PLT
	testb	%al, %al
	jne	.L5126
.L5105:
	movq	-136(%rbp), %rbx
	movq	-144(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L5111
	.p2align 4,,10
	.p2align 3
.L5115:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5112
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L5115
.L5113:
	movq	-144(%rbp), %r12
.L5111:
	testq	%r12, %r12
	je	.L5101
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L5101:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5127
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5106:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L5109
	jmp	.L5108
	.p2align 4,,10
	.p2align 3
.L5112:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L5115
	jmp	.L5113
	.p2align 4,,10
	.p2align 3
.L5126:
	pushq	$0
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r12, %rdi
	pushq	$0
	leaq	-112(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	$0, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN12v8_inspector19V8DebuggerAgentImpl8didPauseEiN2v85LocalINS1_5ValueEEERKSt6vectorIiSaIiEENS1_5debug13ExceptionTypeEbbb
	movq	-112(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L5105
	call	_ZdlPv@PLT
	jmp	.L5105
.L5127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7890:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl10enableImplEv, .-_ZN12v8_inspector19V8DebuggerAgentImpl10enableImplEv
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl6enableEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIdEEPNS_8String16E.str1.8,"aMS",@progbits,1
	.align 8
.LC39:
	.string	"Script execution is prohibited"
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl6enableEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIdEEPNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl6enableEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIdEEPNS_8String16E
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl6enableEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIdEEPNS_8String16E, @function
_ZN12v8_inspector19V8DebuggerAgentImpl6enableEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIdEEPNS_8String16E:
.LFB7910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rdx)
	je	.L5142
	movsd	8(%rdx), %xmm0
	movsd	.LC37(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L5146
	xorl	%eax, %eax
	comisd	.LC40(%rip), %xmm0
	jnb	.L5147
.L5129:
	movq	%rax, 288(%r12)
	movq	24(%r12), %rax
	leaq	-80(%rbp), %r14
	movq	16(%r12), %rdi
	movl	16(%rax), %esi
	call	_ZN12v8_inspector10V8Debugger13debuggerIdForEi@PLT
	leaq	-96(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN12v8_inspector18debuggerIdToStringERKSt4pairIllE@PLT
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-48(%rbp), %rax
	movq	-80(%rbp), %rdi
	movq	%rax, 32(%rbx)
	leaq	-64(%rbp), %rbx
	cmpq	%rbx, %rdi
	je	.L5134
	call	_ZdlPv@PLT
.L5134:
	cmpb	$0, 32(%r12)
	jne	.L5145
	movq	8(%r12), %rax
	leaq	_ZN12v8_inspector17V8InspectorClient17canExecuteScriptsEi(%rip), %rdx
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	200(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5148
.L5137:
	movq	%r12, %rdi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl10enableImplEv
.L5145:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L5128:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5149
	addq	$64, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5142:
	.cfi_restore_state
	movq	$-1, %rax
	jmp	.L5129
	.p2align 4,,10
	.p2align 3
.L5147:
	movsd	.LC38(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jb	.L5150
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rax
	btcq	$63, %rax
	jmp	.L5129
.L5146:
	comisd	.LC40(%rip), %xmm0
	jnb	.L5142
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5150:
	cvttsd2siq	%xmm0, %rax
	jmp	.L5129
	.p2align 4,,10
	.p2align 3
.L5148:
	movq	24(%r12), %rdx
	movl	16(%rdx), %esi
	call	*%rax
	testb	%al, %al
	jne	.L5137
	leaq	.LC39(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L5128
	call	_ZdlPv@PLT
	jmp	.L5128
.L5149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7910:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl6enableEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIdEEPNS_8String16E, .-_ZN12v8_inspector19V8DebuggerAgentImpl6enableEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIdEEPNS_8String16E
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm:
.LFB13589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	subq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	addq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, %r13
	leaq	(%r9,%rax), %r15
	subq	%rsi, %r13
	cmpq	(%rdi), %r14
	je	.L5164
	movq	16(%rdi), %rax
.L5152:
	movabsq	$2305843009213693951, %rdx
	cmpq	%rdx, %r15
	ja	.L5185
	cmpq	%rax, %r15
	jbe	.L5154
	addq	%rax, %rax
	cmpq	%rax, %r15
	jnb	.L5154
	movabsq	$4611686018427387904, %rdi
	movq	%rdx, %r15
	cmpq	%rdx, %rax
	ja	.L5155
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L5154:
	leaq	2(%r15,%r15), %rdi
.L5155:
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	testq	%r12, %r12
	movq	(%rbx), %r11
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	movq	%rax, %r10
	je	.L5157
	cmpq	$1, %r12
	je	.L5186
	movq	%r12, %rdx
	addq	%rdx, %rdx
	je	.L5157
	movq	%r11, %rsi
	movq	%rax, %rdi
	movq	%r8, -80(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r11
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L5157:
	testq	%rcx, %rcx
	je	.L5159
	testq	%r8, %r8
	je	.L5159
	leaq	(%r10,%r12,2), %rdi
	cmpq	$1, %r8
	je	.L5187
	movq	%r8, %rdx
	addq	%rdx, %rdx
	je	.L5159
	movq	%rcx, %rsi
	movq	%r8, -80(%rbp)
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
.L5159:
	testq	%r13, %r13
	jne	.L5188
.L5161:
	cmpq	%r11, %r14
	je	.L5163
	movq	%r11, %rdi
	movq	%r10, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r10
.L5163:
	movq	%r15, 16(%rbx)
	movq	%r10, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5188:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	addq	%r12, %r8
	leaq	(%r10,%r8,2), %rdi
	leaq	(%r11,%rax,2), %rsi
	cmpq	$1, %r13
	je	.L5189
	addq	%r13, %r13
	je	.L5161
	movq	%r13, %rdx
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
	jmp	.L5161
	.p2align 4,,10
	.p2align 3
.L5164:
	movl	$7, %eax
	jmp	.L5152
	.p2align 4,,10
	.p2align 3
.L5189:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	jmp	.L5161
	.p2align 4,,10
	.p2align 3
.L5186:
	movzwl	(%r11), %eax
	movw	%ax, (%r10)
	jmp	.L5157
	.p2align 4,,10
	.p2align 3
.L5187:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	testq	%r13, %r13
	je	.L5161
	jmp	.L5188
.L5185:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE13589:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.section	.text._ZN12v8_inspectorplEPKcRKNS_8String16E,"axG",@progbits,_ZN12v8_inspectorplEPKcRKNS_8String16E,comdat
	.p2align 4
	.weak	_ZN12v8_inspectorplEPKcRKNS_8String16E
	.type	_ZN12v8_inspectorplEPKcRKNS_8String16E, @function
_ZN12v8_inspectorplEPKcRKNS_8String16E:
.LFB5117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r14
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rax
	movq	%r14, %rdi
	movq	%r13, -112(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rdx
	movq	8(%rbx), %r8
	movl	$7, %eax
	movq	-104(%rbp), %rsi
	movq	(%rbx), %rcx
	cmpq	%r13, %rdx
	cmovne	-96(%rbp), %rax
	leaq	(%r8,%rsi), %rbx
	cmpq	%rax, %rbx
	ja	.L5192
	testq	%r8, %r8
	jne	.L5206
.L5193:
	xorl	%eax, %eax
	movq	%rbx, -104(%rbp)
	movq	%r12, %rdi
	movq	%r14, %rsi
	movw	%ax, (%rdx,%rbx,2)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5195
	call	_ZdlPv@PLT
.L5195:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5190
	call	_ZdlPv@PLT
.L5190:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5207
	addq	$80, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5206:
	.cfi_restore_state
	leaq	(%rdx,%rsi,2), %rdi
	cmpq	$1, %r8
	je	.L5208
	addq	%r8, %r8
	je	.L5193
	movq	%r8, %rdx
	movq	%rcx, %rsi
	call	memmove@PLT
	movq	-112(%rbp), %rdx
	jmp	.L5193
	.p2align 4,,10
	.p2align 3
.L5192:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-112(%rbp), %rdx
	jmp	.L5193
	.p2align 4,,10
	.p2align 3
.L5208:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	-112(%rbp), %rdx
	jmp	.L5193
.L5207:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5117:
	.size	_ZN12v8_inspectorplEPKcRKNS_8String16E, .-_ZN12v8_inspectorplEPKcRKNS_8String16E
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl20setPauseOnExceptionsERKNS_8String16E.str1.1,"aMS",@progbits,1
.LC41:
	.string	"none"
.LC42:
	.string	"all"
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl20setPauseOnExceptionsERKNS_8String16E.str1.8,"aMS",@progbits,1
	.align 8
.LC43:
	.string	"Unknown pause on exceptions mode: "
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl20setPauseOnExceptionsERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl20setPauseOnExceptionsERKNS_8String16E
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl20setPauseOnExceptionsERKNS_8String16E, @function
_ZN12v8_inspector19V8DebuggerAgentImpl20setPauseOnExceptionsERKNS_8String16E:
.LFB8006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 32(%rsi)
	jne	.L5210
	leaq	-96(%rbp), %r13
	leaq	_ZN12v8_inspectorL19kDebuggerNotEnabledE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5209
.L5226:
	call	_ZdlPv@PLT
.L5209:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5227
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5210:
	.cfi_restore_state
	leaq	-96(%rbp), %r14
	movq	%rdx, %r13
	leaq	-80(%rbp), %r15
	movq	%rsi, %rbx
	movq	%r14, %rdi
	leaq	.LC41(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	-96(%rbp), %rdi
	movl	%eax, %edx
	cmpq	%r15, %rdi
	je	.L5213
	movl	%eax, -100(%rbp)
	call	_ZdlPv@PLT
	movl	-100(%rbp), %edx
.L5213:
	testl	%edx, %edx
	jne	.L5228
	xorl	%esi, %esi
.L5214:
	movq	16(%rbx), %rdi
	movl	%edx, -100(%rbp)
	call	_ZN12v8_inspector10V8Debugger25setPauseOnExceptionsStateEN2v85debug19ExceptionBreakStateE@PLT
	movq	40(%rbx), %r13
	leaq	_ZN12v8_inspector18DebuggerAgentStateL22pauseOnExceptionsStateE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	-100(%rbp), %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setIntegerERKNS_8String16Ei@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5218
	call	_ZdlPv@PLT
.L5218:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L5209
	.p2align 4,,10
	.p2align 3
.L5228:
	leaq	.LC42(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5215
	movl	%eax, -100(%rbp)
	call	_ZdlPv@PLT
	movl	-100(%rbp), %eax
.L5215:
	movl	$2, %edx
	movl	$2, %esi
	testl	%eax, %eax
	je	.L5214
	leaq	.LC33(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZNKSt7__cxx1112basic_stringItSt11char_traitsItESaItEE7compareERKS4_
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L5216
	movl	%eax, -100(%rbp)
	call	_ZdlPv@PLT
	movl	-100(%rbp), %eax
.L5216:
	movl	$1, %edx
	movl	$1, %esi
	testl	%eax, %eax
	je	.L5214
	movq	%r13, %rdx
	leaq	.LC43(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspectorplEPKcRKNS_8String16E
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	jne	.L5226
	jmp	.L5209
.L5227:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8006:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl20setPauseOnExceptionsERKNS_8String16E, .-_ZN12v8_inspector19V8DebuggerAgentImpl20setPauseOnExceptionsERKNS_8String16E
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl18setBlackboxPatternERKNS_8String16E.str1.1,"aMS",@progbits,1
.LC44:
	.string	"Pattern parser error: "
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl18setBlackboxPatternERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl18setBlackboxPatternERKNS_8String16E
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl18setBlackboxPatternERKNS_8String16E, @function
_ZN12v8_inspector19V8DebuggerAgentImpl18setBlackboxPatternERKNS_8String16E:
.LFB8015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$56, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	8(%rbx), %rsi
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movq	%rax, %r12
	movl	$1, %ecx
	movq	%rax, %rdi
	call	_ZN12v8_inspector7V8RegexC1EPNS_15V8InspectorImplERKNS_8String16Ebb@PLT
	cmpq	$0, 8(%r12)
	je	.L5263
	movq	416(%rbx), %r14
	movq	%r12, 416(%rbx)
	testq	%r14, %r14
	je	.L5238
	movq	16(%r14), %rdi
	leaq	32(%r14), %rax
	cmpq	%rax, %rdi
	je	.L5239
	call	_ZdlPv@PLT
.L5239:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L5240
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5240:
	movl	$56, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L5238:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L5229:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5264
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5263:
	.cfi_restore_state
	leaq	-96(%rbp), %rdi
	leaq	.LC44(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rax
	leaq	-176(%rbp), %r15
	leaq	-160(%rbp), %rbx
	movq	%r15, %rdi
	leaq	(%rsi,%rax,2), %rdx
	movq	%rbx, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	24(%r12), %r8
	movl	$7, %eax
	movq	-176(%rbp), %rdx
	movq	-168(%rbp), %rsi
	movq	16(%r12), %rcx
	cmpq	%rbx, %rdx
	cmovne	-160(%rbp), %rax
	leaq	(%r8,%rsi), %r14
	cmpq	%rax, %r14
	ja	.L5232
	testq	%r8, %r8
	je	.L5233
	leaq	(%rdx,%rsi,2), %rdi
	cmpq	$1, %r8
	je	.L5265
	addq	%r8, %r8
	je	.L5233
	movq	%r8, %rdx
	movq	%rcx, %rsi
	call	memmove@PLT
	movq	-176(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L5233:
	xorl	%eax, %eax
	movq	%r14, -168(%rbp)
	movq	%r15, %rsi
	movw	%ax, (%rdx,%r14,2)
	leaq	-144(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-176(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L5235
	call	_ZdlPv@PLT
.L5235:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5236
	call	_ZdlPv@PLT
.L5236:
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5237
	call	_ZdlPv@PLT
.L5237:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L5244
	call	_ZdlPv@PLT
.L5244:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5242
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5242:
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L5229
	.p2align 4,,10
	.p2align 3
.L5232:
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-176(%rbp), %rdx
	jmp	.L5233
	.p2align 4,,10
	.p2align 3
.L5265:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	-176(%rbp), %rdx
	jmp	.L5233
.L5264:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8015:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl18setBlackboxPatternERKNS_8String16E, .-_ZN12v8_inspector19V8DebuggerAgentImpl18setBlackboxPatternERKNS_8String16E
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl19setBlackboxPatternsESt10unique_ptrISt6vectorINS_8String16ESaIS3_EESt14default_deleteIS5_EE.str1.1,"aMS",@progbits,1
.LC45:
	.string	"|"
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl19setBlackboxPatternsESt10unique_ptrISt6vectorINS_8String16ESaIS3_EESt14default_deleteIS5_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl19setBlackboxPatternsESt10unique_ptrISt6vectorINS_8String16ESaIS3_EESt14default_deleteIS5_EE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl19setBlackboxPatternsESt10unique_ptrISt6vectorINS_8String16ESaIS3_EESt14default_deleteIS5_EE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl19setBlackboxPatternsESt10unique_ptrISt6vectorINS_8String16ESaIS3_EESt14default_deleteIS5_EE:
.LFB8014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -256(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	8(%rax), %rcx
	cmpq	%rcx, (%rax)
	je	.L5309
	leaq	-240(%rbp), %r12
	movq	%rdx, %rbx
	leaq	-112(%rbp), %r13
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	%rsi, %rax
	subq	%rcx, %rax
	cmpq	$40, %rax
	je	.L5275
	leaq	-96(%rbp), %rax
	xorl	%r15d, %r15d
	leaq	-112(%rbp), %r13
	movq	%rax, -248(%rbp)
	.p2align 4,,10
	.p2align 3
.L5278:
	leaq	(%r15,%r15,4), %rdx
	movq	%r12, %rdi
	leaq	(%rcx,%rdx,8), %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	leaq	.LC45(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	cmpq	-248(%rbp), %rdi
	je	.L5276
	call	_ZdlPv@PLT
	movq	(%rbx), %rdx
	addq	$1, %r15
	movabsq	$-3689348814741910323, %rax
	movq	8(%rdx), %rsi
	movq	(%rdx), %rcx
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	imulq	%rax, %rdx
	subq	$1, %rdx
	cmpq	%rdx, %r15
	jb	.L5278
.L5275:
	subq	$40, %rsi
	movq	%r12, %rdi
	leaq	-208(%rbp), %rbx
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movl	$41, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-256(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl18setBlackboxPatternERKNS_8String16E
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	jne	.L5310
	movq	-256(%rbp), %rax
	movq	80(%rax), %r12
	testq	%r12, %r12
	je	.L5283
	.p2align 4,,10
	.p2align 3
.L5284:
	movq	48(%r12), %rdi
	movq	(%rdi), %rax
	call	*128(%rax)
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L5284
.L5283:
	movq	-256(%rbp), %rax
	leaq	-160(%rbp), %r12
	leaq	_ZN12v8_inspector18DebuggerAgentStateL15blackboxPatternE(%rip), %rsi
	movq	%r12, %rdi
	movq	40(%rax), %r13
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue9setStringERKNS_8String16ES4_@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5285
	call	_ZdlPv@PLT
.L5285:
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5282
	call	_ZdlPv@PLT
.L5282:
	movq	-208(%rbp), %rdi
	leaq	-192(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5286
	call	_ZdlPv@PLT
.L5286:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5266
	call	_ZdlPv@PLT
.L5266:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5311
	addq	$216, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5310:
	.cfi_restore_state
	movl	%eax, (%r14)
	leaq	24(%r14), %rax
	leaq	-88(%rbp), %rdx
	movq	%rax, 8(%r14)
	movq	-104(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L5312
	movq	%rax, 8(%r14)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%r14)
.L5281:
	movq	-96(%rbp), %rax
	movq	%rax, 16(%r14)
	movq	-72(%rbp), %rax
	movq	%rax, 40(%r14)
	movl	-64(%rbp), %eax
	movl	%eax, 48(%r14)
	jmp	.L5282
	.p2align 4,,10
	.p2align 3
.L5276:
	movq	(%rbx), %rdx
	addq	$1, %r15
	movabsq	$-3689348814741910323, %rax
	movq	8(%rdx), %rsi
	movq	(%rdx), %rcx
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	imulq	%rax, %rdx
	subq	$1, %rdx
	cmpq	%r15, %rdx
	ja	.L5278
	jmp	.L5275
	.p2align 4,,10
	.p2align 3
.L5309:
	movq	416(%rsi), %r12
	movq	$0, 416(%rsi)
	testq	%r12, %r12
	je	.L5268
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L5269
	call	_ZdlPv@PLT
.L5269:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5270
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5270:
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5268:
	movq	-256(%rbp), %rax
	movq	80(%rax), %rbx
	testq	%rbx, %rbx
	je	.L5271
	.p2align 4,,10
	.p2align 3
.L5272:
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	call	*128(%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L5272
.L5271:
	movq	-256(%rbp), %rax
	leaq	-112(%rbp), %r12
	leaq	_ZN12v8_inspector18DebuggerAgentStateL15blackboxPatternE(%rip), %rsi
	movq	%r12, %rdi
	movq	40(%rax), %r13
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue6removeERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5273
	call	_ZdlPv@PLT
.L5273:
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L5266
	.p2align 4,,10
	.p2align 3
.L5312:
	movdqu	-88(%rbp), %xmm0
	movups	%xmm0, 24(%r14)
	jmp	.L5281
.L5311:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8014:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl19setBlackboxPatternsESt10unique_ptrISt6vectorINS_8String16ESaIS3_EESt14default_deleteIS5_EE, .-_ZN12v8_inspector19V8DebuggerAgentImpl19setBlackboxPatternsESt10unique_ptrISt6vectorINS_8String16ESaIS3_EESt14default_deleteIS5_EE
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl7restoreEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl7restoreEv
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl7restoreEv, @function
_ZN12v8_inspector19V8DebuggerAgentImpl7restoreEv:
.LFB7912:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN12v8_inspector18DebuggerAgentStateL15debuggerEnabledE(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	40(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %r14
	movl	%eax, %ebx
	cmpq	%r14, %rdi
	je	.L5314
	call	_ZdlPv@PLT
.L5314:
	testb	%bl, %bl
	je	.L5313
	movq	8(%r12), %rax
	leaq	_ZN12v8_inspector17V8InspectorClient17canExecuteScriptsEi(%rip), %rdx
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	200(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5316
.L5319:
	movq	%r12, %rdi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl10enableImplEv
	movq	40(%r12), %r15
	leaq	_ZN12v8_inspector18DebuggerAgentStateL22pauseOnExceptionsStateE(%rip), %rsi
	movq	%r13, %rdi
	movl	$0, -168(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	leaq	-168(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue10getIntegerERKNS_8String16EPi@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L5318
	call	_ZdlPv@PLT
.L5318:
	movl	-168(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl24setPauseOnExceptionsImplEi
	movq	40(%r12), %r15
	leaq	_ZN12v8_inspector18DebuggerAgentStateL13skipAllPausesE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb@PLT
	movq	-112(%rbp), %rdi
	movb	%al, 408(%r12)
	cmpq	%r14, %rdi
	je	.L5320
	call	_ZdlPv@PLT
.L5320:
	movq	40(%r12), %r15
	movq	%r13, %rdi
	leaq	_ZN12v8_inspector18DebuggerAgentStateL19asyncCallStackDepthE(%rip), %rsi
	movl	$0, -164(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-164(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue10getIntegerERKNS_8String16EPi@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L5321
	call	_ZdlPv@PLT
.L5321:
	movl	-164(%rbp), %edx
	movq	16(%r12), %rdi
	movq	%r12, %rsi
	leaq	-144(%rbp), %rbx
	leaq	-160(%rbp), %r15
	call	_ZN12v8_inspector10V8Debugger22setAsyncCallStackDepthEPNS_19V8DebuggerAgentImplEi@PLT
	movq	40(%r12), %r8
	xorl	%eax, %eax
	movq	%r13, %rdi
	leaq	_ZN12v8_inspector18DebuggerAgentStateL15blackboxPatternE(%rip), %rsi
	movw	%ax, -144(%rbp)
	movq	%r8, -184(%rbp)
	movq	%rbx, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -128(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-184(%rbp), %r8
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getStringERKNS_8String16EPS2_@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L5322
	movb	%al, -184(%rbp)
	call	_ZdlPv@PLT
	movzbl	-184(%rbp), %eax
.L5322:
	testb	%al, %al
	jne	.L5334
.L5323:
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L5313
	call	_ZdlPv@PLT
.L5313:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5335
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5316:
	.cfi_restore_state
	movq	24(%r12), %rdx
	movl	16(%rdx), %esi
	call	*%rax
	testb	%al, %al
	je	.L5313
	jmp	.L5319
	.p2align 4,,10
	.p2align 3
.L5334:
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl18setBlackboxPatternERKNS_8String16E
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5323
	call	_ZdlPv@PLT
	jmp	.L5323
.L5335:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7912:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl7restoreEv, .-_ZN12v8_inspector19V8DebuggerAgentImpl7restoreEv
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl15searchInContentERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_PSt10unique_ptrISt6vectorIS9_INS_8protocol8Debugger11SearchMatchESt14default_deleteISD_EESaISG_EESE_ISI_EE.str1.1,"aMS",@progbits,1
.LC46:
	.string	"No script for id: "
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl15searchInContentERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_PSt10unique_ptrISt6vectorIS9_INS_8protocol8Debugger11SearchMatchESt14default_deleteISD_EESaISG_EESE_ISI_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl15searchInContentERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_PSt10unique_ptrISt6vectorIS9_INS_8protocol8Debugger11SearchMatchESt14default_deleteISD_EESaISG_EESE_ISI_EE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl15searchInContentERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_PSt10unique_ptrISt6vectorIS9_INS_8protocol8Debugger11SearchMatchESt14default_deleteISD_EESaISG_EESE_ISI_EE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl15searchInContentERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_PSt10unique_ptrISt6vectorIS9_INS_8protocol8Debugger11SearchMatchESt14default_deleteISD_EESaISG_EESE_ISI_EE:
.LFB7955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-240(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$248, %rsp
	movq	56(%rsi), %rsi
	movq	16(%rbp), %r15
	movq	%rcx, -280(%rbp)
	movq	%r8, -272(%rbp)
	movq	%r9, -256(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	64(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	-256(%rbp), %rcx
	movq	-272(%rbp), %r8
	testq	%rax, %rax
	je	.L5389
	xorl	%r9d, %r9d
	cmpb	$0, (%rcx)
	je	.L5346
	movzbl	1(%rcx), %r9d
.L5346:
	xorl	%r13d, %r13d
	cmpb	$0, (%r8)
	je	.L5347
	movzbl	1(%r8), %r13d
.L5347:
	movq	48(%rax), %rsi
	leaq	-96(%rbp), %r10
	movl	$4294967295, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	%r9d, -272(%rbp)
	movq	(%rsi), %rax
	movq	%r10, -256(%rbp)
	call	*24(%rax)
	movq	-256(%rbp), %r10
	movq	24(%rbx), %rsi
	movl	%r13d, %r8d
	movl	-272(%rbp), %r9d
	movq	-280(%rbp), %rcx
	leaq	-208(%rbp), %rdi
	movq	%r10, %rdx
	call	_ZN12v8_inspector23searchInTextByLinesImplEPNS_18V8InspectorSessionERKNS_8String16ES4_bb@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm5
	movq	(%r15), %r13
	pxor	%xmm0, %xmm0
	movq	-192(%rbp), %rdx
	movq	%rax, (%r15)
	movq	$0, -192(%rbp)
	movq	%rdx, 16(%rax)
	movups	%xmm5, (%rax)
	movaps	%xmm0, -208(%rbp)
	testq	%r13, %r13
	je	.L5348
	movq	8(%r13), %rbx
	movq	0(%r13), %r15
	cmpq	%r15, %rbx
	je	.L5349
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm7
	movq	%rdx, %xmm4
	punpcklqdq	%xmm7, %xmm4
	movaps	%xmm4, -272(%rbp)
	jmp	.L5353
	.p2align 4,,10
	.p2align 3
.L5391:
	movdqa	-272(%rbp), %xmm2
	movq	24(%r8), %rdi
	leaq	40(%r8), %rax
	movups	%xmm2, (%r8)
	cmpq	%rax, %rdi
	je	.L5352
	movq	%r8, -256(%rbp)
	call	_ZdlPv@PLT
	movq	-256(%rbp), %r8
.L5352:
	movl	$64, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L5350:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L5390
.L5353:
	movq	(%r15), %r8
	testq	%r8, %r8
	je	.L5350
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L5391
	addq	$8, %r15
	movq	%r8, %rdi
	call	*%rax
	cmpq	%r15, %rbx
	jne	.L5353
	.p2align 4,,10
	.p2align 3
.L5390:
	movq	0(%r13), %r15
.L5349:
	testq	%r15, %r15
	je	.L5354
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L5354:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	-200(%rbp), %rbx
	movq	-208(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L5355
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm6
	movq	%rdx, %xmm1
	punpcklqdq	%xmm6, %xmm1
	movaps	%xmm1, -256(%rbp)
	jmp	.L5359
	.p2align 4,,10
	.p2align 3
.L5393:
	movdqa	-256(%rbp), %xmm3
	movq	24(%r13), %rdi
	leaq	40(%r13), %rax
	movups	%xmm3, 0(%r13)
	cmpq	%rax, %rdi
	je	.L5358
	call	_ZdlPv@PLT
.L5358:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L5356:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L5392
.L5359:
	movq	(%r15), %r13
	testq	%r13, %r13
	je	.L5356
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5393
	addq	$8, %r15
	movq	%r13, %rdi
	call	*%rax
	cmpq	%r15, %rbx
	jne	.L5359
	.p2align 4,,10
	.p2align 3
.L5392:
	movq	-208(%rbp), %r15
.L5355:
	testq	%r15, %r15
	je	.L5348
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L5348:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5360
	call	_ZdlPv@PLT
.L5360:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L5345:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5394
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5389:
	.cfi_restore_state
	leaq	-96(%rbp), %rdi
	leaq	.LC46(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rax
	leaq	-176(%rbp), %r15
	leaq	-160(%rbp), %rbx
	movq	%r15, %rdi
	leaq	(%rsi,%rax,2), %rdx
	movq	%rbx, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-176(%rbp), %rdx
	movq	8(%r13), %r8
	movl	$7, %eax
	movq	-168(%rbp), %rsi
	movq	0(%r13), %rcx
	cmpq	%rbx, %rdx
	cmovne	-160(%rbp), %rax
	leaq	(%r8,%rsi), %r13
	cmpq	%rax, %r13
	ja	.L5339
	testq	%r8, %r8
	je	.L5340
	leaq	(%rdx,%rsi,2), %rdi
	cmpq	$1, %r8
	je	.L5395
	addq	%r8, %r8
	je	.L5340
	movq	%r8, %rdx
	movq	%rcx, %rsi
	call	memmove@PLT
	movq	-176(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L5340:
	xorl	%eax, %eax
	movq	%r13, -168(%rbp)
	movq	%r15, %rsi
	movw	%ax, (%rdx,%r13,2)
	leaq	-144(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-176(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L5342
	call	_ZdlPv@PLT
.L5342:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5343
	call	_ZdlPv@PLT
.L5343:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5345
	call	_ZdlPv@PLT
	jmp	.L5345
	.p2align 4,,10
	.p2align 3
.L5339:
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-176(%rbp), %rdx
	jmp	.L5340
	.p2align 4,,10
	.p2align 3
.L5395:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	-176(%rbp), %rdx
	jmp	.L5340
.L5394:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7955:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl15searchInContentERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_PSt10unique_ptrISt6vectorIS9_INS_8protocol8Debugger11SearchMatchESt14default_deleteISD_EESaISG_EESE_ISI_EE, .-_ZN12v8_inspector19V8DebuggerAgentImpl15searchInContentERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_PSt10unique_ptrISt6vectorIS9_INS_8protocol8Debugger11SearchMatchESt14default_deleteISD_EESaISG_EESE_ISI_EE
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl15getScriptSourceERKNS_8String16EPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl15getScriptSourceERKNS_8String16EPS1_
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl15getScriptSourceERKNS_8String16EPS1_, @function
_ZN12v8_inspector19V8DebuggerAgentImpl15getScriptSourceERKNS_8String16EPS1_:
.LFB7991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 32(%rsi)
	jne	.L5397
	leaq	-80(%rbp), %r13
	leaq	_ZN12v8_inspectorL19kDebuggerNotEnabledE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5396
.L5418:
	call	_ZdlPv@PLT
.L5396:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5419
	subq	$-128, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5397:
	.cfi_restore_state
	leaq	64(%rsi), %rdi
	movq	%rdx, %rsi
	movq	%rdx, %r13
	movq	%rcx, %rbx
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_16V8DebuggerScriptESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L5420
	movq	48(%rax), %rsi
	leaq	-80(%rbp), %r13
	movl	$4294967295, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-48(%rbp), %rax
	movq	-80(%rbp), %rdi
	movq	%rax, 32(%rbx)
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5408
	call	_ZdlPv@PLT
.L5408:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L5396
	.p2align 4,,10
	.p2align 3
.L5420:
	leaq	-80(%rbp), %rdi
	leaq	.LC46(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rax
	leaq	-160(%rbp), %r14
	leaq	-144(%rbp), %rbx
	movq	%r14, %rdi
	leaq	(%rsi,%rax,2), %rdx
	movq	%rbx, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-160(%rbp), %rdx
	movq	8(%r13), %r8
	movl	$7, %eax
	movq	-152(%rbp), %rsi
	movq	0(%r13), %rcx
	cmpq	%rbx, %rdx
	cmovne	-144(%rbp), %rax
	leaq	(%r8,%rsi), %r13
	cmpq	%rax, %r13
	ja	.L5402
	testq	%r8, %r8
	je	.L5403
	leaq	(%rdx,%rsi,2), %rdi
	cmpq	$1, %r8
	je	.L5421
	addq	%r8, %r8
	je	.L5403
	movq	%r8, %rdx
	movq	%rcx, %rsi
	call	memmove@PLT
	movq	-160(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L5403:
	xorl	%eax, %eax
	movq	%r13, -152(%rbp)
	movq	%r14, %rsi
	movw	%ax, (%rdx,%r13,2)
	leaq	-128(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L5405
	call	_ZdlPv@PLT
.L5405:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5406
	call	_ZdlPv@PLT
.L5406:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L5418
	jmp	.L5396
	.p2align 4,,10
	.p2align 3
.L5402:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-160(%rbp), %rdx
	jmp	.L5403
.L5421:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	-160(%rbp), %rdx
	jmp	.L5403
.L5419:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7991:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl15getScriptSourceERKNS_8String16EPS1_, .-_ZN12v8_inspector19V8DebuggerAgentImpl15getScriptSourceERKNS_8String16EPS1_
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl27setBreakpointOnFunctionCallERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPS1_.str1.8,"aMS",@progbits,1
	.align 8
.LC47:
	.string	"Could not find function with given id"
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl27setBreakpointOnFunctionCallERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl27setBreakpointOnFunctionCallERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPS1_
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl27setBreakpointOnFunctionCallERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPS1_, @function
_ZN12v8_inspector19V8DebuggerAgentImpl27setBreakpointOnFunctionCallERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPS1_:
.LFB7923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-288(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$472, %rsp
	movq	%rcx, -488(%rbp)
	movq	24(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector14InjectedScript11ObjectScopeC1EPNS_22V8InspectorSessionImplERKNS_8String16E@PLT
	leaq	-352(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-352(%rbp), %eax
	testl	%eax, %eax
	je	.L5423
	movl	%eax, (%r12)
	leaq	24(%r12), %rax
	leaq	-328(%rbp), %rdx
	movq	%rax, 8(%r12)
	movq	-344(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L5444
	movq	%rax, 8(%r12)
	movq	-328(%rbp), %rax
	movq	%rax, 24(%r12)
.L5425:
	movq	-336(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-312(%rbp), %rax
	movq	%rax, 40(%r12)
	movl	-304(%rbp), %eax
	movl	%eax, 48(%r12)
.L5426:
	movq	%r14, %rdi
	call	_ZN12v8_inspector14InjectedScript11ObjectScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5445
	addq	$472, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5444:
	.cfi_restore_state
	movdqu	-328(%rbp), %xmm0
	movups	%xmm0, 24(%r12)
	jmp	.L5425
	.p2align 4,,10
	.p2align 3
.L5423:
	movq	-72(%rbp), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L5427
	leaq	-400(%rbp), %r13
	leaq	.LC47(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-400(%rbp), %rdi
	leaq	-384(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5429
.L5443:
	call	_ZdlPv@PLT
.L5429:
	movq	-344(%rbp), %rdi
	leaq	-328(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5426
	call	_ZdlPv@PLT
	jmp	.L5426
	.p2align 4,,10
	.p2align 3
.L5427:
	movq	-72(%rbp), %rax
	leaq	-480(%rbp), %r15
	movq	%r15, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movl	$7, %esi
	movq	%r15, %rdi
	call	_ZN12v8_inspector15String16Builder12appendNumberEi@PLT
	movl	$58, %esi
	movq	%r15, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	-496(%rbp), %rdi
	call	_ZN2v85debug14GetDebuggingIdENS_5LocalINS_8FunctionEEE@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector15String16Builder12appendNumberEi@PLT
	leaq	-448(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -504(%rbp)
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5430
	call	_ZdlPv@PLT
.L5430:
	movq	-504(%rbp), %rsi
	leaq	120(%r13), %rdi
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St6vectorIiSaIiEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L5431
	leaq	-400(%rbp), %r13
	leaq	.LC22(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-400(%rbp), %rdi
	leaq	-384(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5433
	call	_ZdlPv@PLT
.L5433:
	movq	-448(%rbp), %rdi
	leaq	-432(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L5443
	jmp	.L5429
	.p2align 4,,10
	.p2align 3
.L5431:
	movq	-488(%rbp), %rcx
	leaq	-384(%rbp), %rdx
	leaq	-400(%rbp), %rax
	movq	56(%r13), %rdi
	movq	%rdx, -400(%rbp)
	movq	%rdx, -512(%rbp)
	leaq	8(%rcx), %rsi
	xorl	%edx, %edx
	cmpb	$0, (%rcx)
	cmove	%rax, %rsi
	movw	%dx, -384(%rbp)
	movq	$0, -392(%rbp)
	movq	$0, -368(%rbp)
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-400(%rbp), %rdi
	movq	-512(%rbp), %rdx
	movq	%rax, %r15
	cmpq	%rdx, %rdi
	je	.L5435
	call	_ZdlPv@PLT
.L5435:
	movq	%r15, %rcx
	movq	-504(%rbp), %r15
	movq	-496(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl17setBreakpointImplERKNS_8String16EN2v85LocalINS4_8FunctionEEENS5_INS4_6StringEEE
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-416(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, 32(%rbx)
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L5433
.L5445:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7923:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl27setBreakpointOnFunctionCallERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPS1_, .-_ZN12v8_inspector19V8DebuggerAgentImpl27setBreakpointOnFunctionCallERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPS1_
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl12restartFrameERKNS_8String16EPSt10unique_ptrISt6vectorIS4_INS_8protocol8Debugger9CallFrameESt14default_deleteIS8_EESaISB_EES9_ISD_EEPN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS6_7Runtime10StackTraceEEEPNSK_INSL_12StackTraceIdEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC48:
	.string	"Could not find call frame with given id"
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl12restartFrameERKNS_8String16EPSt10unique_ptrISt6vectorIS4_INS_8protocol8Debugger9CallFrameESt14default_deleteIS8_EESaISB_EES9_ISD_EEPN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS6_7Runtime10StackTraceEEEPNSK_INSL_12StackTraceIdEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl12restartFrameERKNS_8String16EPSt10unique_ptrISt6vectorIS4_INS_8protocol8Debugger9CallFrameESt14default_deleteIS8_EESaISB_EES9_ISD_EEPN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS6_7Runtime10StackTraceEEEPNSK_INSL_12StackTraceIdEEE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl12restartFrameERKNS_8String16EPSt10unique_ptrISt6vectorIS4_INS_8protocol8Debugger9CallFrameESt14default_deleteIS8_EESaISB_EES9_ISD_EEPN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS6_7Runtime10StackTraceEEEPNSK_INSL_12StackTraceIdEEE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl12restartFrameERKNS_8String16EPSt10unique_ptrISt6vectorIS4_INS_8protocol8Debugger9CallFrameESt14default_deleteIS8_EESaISB_EES9_ISD_EEPN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS6_7Runtime10StackTraceEEEPNSK_INSL_12StackTraceIdEEE:
.LFB7987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$360, %rsp
	movq	%rcx, -392(%rbp)
	movq	16(%r14), %rdi
	movq	%r8, -400(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	movl	16(%rax), %esi
	call	_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi@PLT
	testb	%al, %al
	jne	.L5447
	leaq	-240(%rbp), %r13
	leaq	_ZN12v8_inspectorL18kDebuggerNotPausedE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5446
	call	_ZdlPv@PLT
.L5446:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5481
	addq	$360, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5447:
	.cfi_restore_state
	movq	24(%r14), %rsi
	leaq	-240(%rbp), %r15
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN12v8_inspector14InjectedScript14CallFrameScopeC1EPNS_22V8InspectorSessionImplERKNS_8String16E@PLT
	leaq	-368(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-368(%rbp), %eax
	testl	%eax, %eax
	je	.L5450
	movl	%eax, (%r12)
	leaq	24(%r12), %rax
	leaq	-344(%rbp), %rdx
	movq	%rax, 8(%r12)
	movq	-360(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L5482
	movq	%rax, 8(%r12)
	movq	-344(%rbp), %rax
	movq	%rax, 24(%r12)
.L5452:
	movq	-352(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-328(%rbp), %rax
	movq	%rax, 40(%r12)
	movl	-320(%rbp), %eax
	movl	%eax, 48(%r12)
.L5453:
	movq	%r15, %rdi
	call	_ZN12v8_inspector14InjectedScript14CallFrameScopeD1Ev@PLT
	jmp	.L5446
	.p2align 4,,10
	.p2align 3
.L5450:
	movq	56(%r14), %rsi
	movl	-64(%rbp), %edx
	leaq	-384(%rbp), %rdi
	call	_ZN2v85debug18StackTraceIterator6CreateEPNS_7IsolateEi@PLT
	movq	-384(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	jne	.L5483
	movq	-384(%rbp), %rdi
	movq	(%rdi), %rax
	call	*96(%rax)
	testb	%al, %al
	jne	.L5457
	movq	%r12, %rdi
	leaq	-344(%rbp), %rbx
	call	_ZN12v8_inspector8protocol16DispatchResponse13InternalErrorEv@PLT
.L5456:
	movq	-384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5468
	movq	(%rdi), %rax
	call	*8(%rax)
.L5468:
	movq	-360(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L5453
	call	_ZdlPv@PLT
	jmp	.L5453
	.p2align 4,,10
	.p2align 3
.L5482:
	movdqu	-344(%rbp), %xmm0
	movups	%xmm0, 24(%r12)
	jmp	.L5452
	.p2align 4,,10
	.p2align 3
.L5457:
	movq	-392(%rbp), %rdx
	leaq	-304(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl17currentCallFramesEPSt10unique_ptrISt6vectorIS1_INS_8protocol8Debugger9CallFrameESt14default_deleteIS5_EESaIS8_EES6_ISA_EE
	movl	-304(%rbp), %eax
	leaq	-360(%rbp), %rdi
	leaq	-296(%rbp), %rsi
	movl	%eax, -368(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-264(%rbp), %rax
	movq	-296(%rbp), %rdi
	movq	%rax, -328(%rbp)
	movl	-256(%rbp), %eax
	movl	%eax, -320(%rbp)
	leaq	-280(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5458
	call	_ZdlPv@PLT
.L5458:
	movl	-368(%rbp), %eax
	testl	%eax, %eax
	je	.L5459
	movl	%eax, (%r12)
	leaq	24(%r12), %rax
	leaq	-344(%rbp), %rbx
	movq	%rax, 8(%r12)
	movq	-360(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L5484
	movq	%rax, 8(%r12)
	movq	-344(%rbp), %rax
	movq	%rax, 24(%r12)
.L5461:
	movq	-352(%rbp), %rax
	movq	%rbx, -360(%rbp)
	movq	$0, -352(%rbp)
	movq	%rax, 16(%r12)
	xorl	%eax, %eax
	movw	%ax, -344(%rbp)
	movq	-328(%rbp), %rax
	movq	%rax, 40(%r12)
	movl	-320(%rbp), %eax
	movl	%eax, 48(%r12)
	jmp	.L5456
	.p2align 4,,10
	.p2align 3
.L5483:
	leaq	-304(%rbp), %r13
	leaq	.LC48(%rip), %rsi
	movq	%r13, %rdi
	leaq	-344(%rbp), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-304(%rbp), %rdi
	leaq	-288(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5456
	call	_ZdlPv@PLT
	jmp	.L5456
	.p2align 4,,10
	.p2align 3
.L5459:
	leaq	-376(%rbp), %r13
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl22currentAsyncStackTraceEv
	movq	-400(%rbp), %rcx
	movq	-376(%rbp), %rax
	movq	$0, -376(%rbp)
	movq	(%rcx), %rdi
	movq	%rax, (%rcx)
	testq	%rdi, %rdi
	je	.L5463
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5463
	movq	(%rdi), %rax
	call	*24(%rax)
.L5463:
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl25currentExternalStackTraceEv
	movq	-376(%rbp), %rax
	movq	(%rbx), %rdi
	movq	$0, -376(%rbp)
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L5466
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5466
	movq	(%rdi), %rax
	call	*24(%rax)
.L5466:
	movq	%r12, %rdi
	leaq	-344(%rbp), %rbx
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L5456
	.p2align 4,,10
	.p2align 3
.L5484:
	movdqu	-344(%rbp), %xmm1
	movups	%xmm1, 24(%r12)
	jmp	.L5461
.L5481:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7987:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl12restartFrameERKNS_8String16EPSt10unique_ptrISt6vectorIS4_INS_8protocol8Debugger9CallFrameESt14default_deleteIS8_EESaISB_EES9_ISD_EEPN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS6_7Runtime10StackTraceEEEPNSK_INSL_12StackTraceIdEEE, .-_ZN12v8_inspector19V8DebuggerAgentImpl12restartFrameERKNS_8String16EPSt10unique_ptrISt6vectorIS4_INS_8protocol8Debugger9CallFrameESt14default_deleteIS8_EESaISB_EES9_ISD_EEPN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS6_7Runtime10StackTraceEEEPNSK_INSL_12StackTraceIdEEE
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl19evaluateOnCallFrameERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_S9_S9_S9_NS7_IdEEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISE_EEPNS6_8PtrMaybeINSD_16ExceptionDetailsEEE.str1.1,"aMS",@progbits,1
.LC50:
	.string	""
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl19evaluateOnCallFrameERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_S9_S9_S9_NS7_IdEEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISE_EEPNS6_8PtrMaybeINSD_16ExceptionDetailsEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl19evaluateOnCallFrameERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_S9_S9_S9_NS7_IdEEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISE_EEPNS6_8PtrMaybeINSD_16ExceptionDetailsEEE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl19evaluateOnCallFrameERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_S9_S9_S9_NS7_IdEEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISE_EEPNS6_8PtrMaybeINSD_16ExceptionDetailsEEE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl19evaluateOnCallFrameERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_S9_S9_S9_NS7_IdEEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISE_EEPNS6_8PtrMaybeINSD_16ExceptionDetailsEEE:
.LFB8008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$488, %rsp
	movq	16(%rbp), %rax
	movq	16(%r15), %rdi
	movq	%rcx, -456(%rbp)
	movq	%r8, -472(%rbp)
	movq	%rax, -440(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -480(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -488(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -464(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -448(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -496(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -504(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	movl	16(%rax), %esi
	call	_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi@PLT
	testb	%al, %al
	jne	.L5486
	leaq	-240(%rbp), %r13
	leaq	_ZN12v8_inspectorL18kDebuggerNotPausedE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5485
	call	_ZdlPv@PLT
.L5485:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5529
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5486:
	.cfi_restore_state
	movq	24(%r15), %rsi
	leaq	-240(%rbp), %r13
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN12v8_inspector14InjectedScript14CallFrameScopeC1EPNS_22V8InspectorSessionImplERKNS_8String16E@PLT
	leaq	-368(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-368(%rbp), %eax
	testl	%eax, %eax
	je	.L5489
	movl	%eax, (%r12)
	leaq	24(%r12), %rax
	leaq	-344(%rbp), %rdx
	movq	%rax, 8(%r12)
	movq	-360(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L5530
	movq	%rax, 8(%r12)
	movq	-344(%rbp), %rax
	movq	%rax, 24(%r12)
.L5491:
	movq	-352(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-328(%rbp), %rax
	movq	%rax, 40(%r12)
	movl	-320(%rbp), %eax
	movl	%eax, 48(%r12)
.L5492:
	movq	%r13, %rdi
	call	_ZN12v8_inspector14InjectedScript14CallFrameScopeD1Ev@PLT
	jmp	.L5485
	.p2align 4,,10
	.p2align 3
.L5489:
	cmpb	$0, (%rbx)
	je	.L5493
	cmpb	$0, 1(%rbx)
	je	.L5493
	movq	%r13, %rdi
	call	_ZN12v8_inspector14InjectedScript5Scope21installCommandLineAPIEv@PLT
.L5493:
	movq	-440(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L5494
	cmpb	$0, 1(%rax)
	je	.L5494
	movq	%r13, %rdi
	call	_ZN12v8_inspector14InjectedScript5Scope30ignoreExceptionsAndMuteConsoleEv@PLT
.L5494:
	movq	56(%r15), %rsi
	movl	-64(%rbp), %edx
	leaq	-424(%rbp), %rdi
	call	_ZN2v85debug18StackTraceIterator6CreateEPNS_7IsolateEi@PLT
	movq	-424(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	jne	.L5531
	leaq	-416(%rbp), %r8
	movq	%r13, %rsi
	leaq	-304(%rbp), %rbx
	movq	%r8, %rdi
	movq	%r8, -440(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeC1ERKNS_14InjectedScript5ScopeE@PLT
	movq	-448(%rbp), %rax
	movq	-440(%rbp), %r8
	cmpb	$0, (%rax)
	jne	.L5526
	leaq	-280(%rbp), %rax
	leaq	-296(%rbp), %r10
	movq	%rax, -440(%rbp)
	leaq	-360(%rbp), %r9
.L5498:
	movq	-424(%rbp), %r14
	xorl	%edx, %edx
	movq	(%r14), %rax
	movq	104(%rax), %rcx
	movq	-464(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L5502
	movzbl	1(%rax), %edx
.L5502:
	movq	56(%r15), %rdi
	movq	-456(%rbp), %rsi
	movq	%r9, -528(%rbp)
	movq	%r10, -520(%rbp)
	movq	%r8, -512(%rbp)
	movl	%edx, -464(%rbp)
	movq	%rcx, -448(%rbp)
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movl	-464(%rbp), %edx
	movq	-448(%rbp), %rcx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	*%rcx
	movq	-512(%rbp), %r8
	movq	%rax, %r14
	movq	%r8, %rdi
	call	_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeD1Ev@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movq	-520(%rbp), %r10
	movq	-528(%rbp), %r9
	movl	-304(%rbp), %eax
	movq	%r9, %rdi
	movq	%r10, %rsi
	movl	%eax, -368(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-264(%rbp), %rax
	movq	-296(%rbp), %rdi
	movq	%rax, -328(%rbp)
	movl	-256(%rbp), %eax
	movl	%eax, -320(%rbp)
	cmpq	-440(%rbp), %rdi
	je	.L5503
	call	_ZdlPv@PLT
.L5503:
	movl	-368(%rbp), %eax
	testl	%eax, %eax
	je	.L5504
	movl	%eax, (%r12)
	leaq	24(%r12), %rax
	leaq	-344(%rbp), %rbx
	movq	%rax, 8(%r12)
	movq	-360(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L5532
	movq	%rax, 8(%r12)
	movq	-344(%rbp), %rax
	movq	%rax, 24(%r12)
.L5506:
	movq	-352(%rbp), %rax
	xorl	%esi, %esi
	movq	%rbx, -360(%rbp)
	movq	$0, -352(%rbp)
	movq	%rax, 16(%r12)
	movq	-328(%rbp), %rax
	movw	%si, -344(%rbp)
	movq	%rax, 40(%r12)
	movl	-320(%rbp), %eax
	movl	%eax, 48(%r12)
	jmp	.L5497
	.p2align 4,,10
	.p2align 3
.L5531:
	leaq	-304(%rbp), %r14
	leaq	.LC48(%rip), %rsi
	movq	%r14, %rdi
	leaq	-344(%rbp), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-304(%rbp), %rdi
	leaq	-288(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5497
.L5528:
	call	_ZdlPv@PLT
.L5497:
	movq	-424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5511
	movq	(%rdi), %rax
	call	*8(%rax)
.L5511:
	movq	-360(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L5492
	call	_ZdlPv@PLT
	jmp	.L5492
	.p2align 4,,10
	.p2align 3
.L5530:
	movdqu	-344(%rbp), %xmm1
	movups	%xmm1, 24(%r12)
	jmp	.L5491
	.p2align 4,,10
	.p2align 3
.L5504:
	movq	-488(%rbp), %rax
	movl	$1, %r15d
	cmpb	$0, (%rax)
	je	.L5507
	xorl	%r15d, %r15d
	cmpb	$0, 1(%rax)
	setne	%r15b
	addl	$1, %r15d
.L5507:
	movq	-480(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L5508
	cmpb	$0, 1(%rax)
	movl	$0, %eax
	cmovne	%eax, %r15d
.L5508:
	movq	-224(%rbp), %r10
	movq	%rbx, %rdi
	leaq	.LC50(%rip), %rsi
	movq	%r10, -440(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	%r15d, %r9d
	movq	-472(%rbp), %rax
	movq	-440(%rbp), %r10
	leaq	-192(%rbp), %rcx
	cmpb	$0, (%rax)
	leaq	8(%rax), %r8
	pushq	-504(%rbp)
	movq	%r10, %rsi
	pushq	-496(%rbp)
	cmove	%rbx, %r8
	leaq	-344(%rbp), %rbx
	call	_ZN12v8_inspector14InjectedScript18wrapEvaluateResultEN2v810MaybeLocalINS1_5ValueEEERKNS1_8TryCatchERKNS_8String16ENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISF_EEPN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINSE_16ExceptionDetailsEEE@PLT
	movq	-304(%rbp), %rdi
	leaq	-288(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	jne	.L5528
	jmp	.L5497
	.p2align 4,,10
	.p2align 3
.L5532:
	movdqu	-344(%rbp), %xmm2
	movups	%xmm2, 24(%r12)
	jmp	.L5506
	.p2align 4,,10
	.p2align 3
.L5526:
	movq	%r8, %rsi
	movq	%rbx, %rdi
	movsd	8(%rax), %xmm0
	movq	%r8, -520(%rbp)
	divsd	.LC49(%rip), %xmm0
	call	_ZN12v8_inspector15V8InspectorImpl13EvaluateScope10setTimeoutEd@PLT
	leaq	-296(%rbp), %r10
	leaq	-360(%rbp), %r9
	movl	-304(%rbp), %eax
	movq	%r9, %rdi
	movq	%r10, %rsi
	movq	%r10, -512(%rbp)
	movq	%r9, -448(%rbp)
	movl	%eax, -368(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-264(%rbp), %rax
	movq	-296(%rbp), %rdi
	movq	-448(%rbp), %r9
	movq	-512(%rbp), %r10
	movq	%rax, -328(%rbp)
	movl	-256(%rbp), %eax
	movq	-520(%rbp), %r8
	movl	%eax, -320(%rbp)
	leaq	-280(%rbp), %rax
	cmpq	%rax, %rdi
	movq	%rax, -440(%rbp)
	je	.L5499
	movq	%r9, -520(%rbp)
	movq	%r8, -448(%rbp)
	call	_ZdlPv@PLT
	movq	-520(%rbp), %r9
	movq	-512(%rbp), %r10
	movq	-448(%rbp), %r8
.L5499:
	movl	-368(%rbp), %eax
	testl	%eax, %eax
	je	.L5498
	movl	%eax, (%r12)
	leaq	24(%r12), %rax
	leaq	-344(%rbp), %rbx
	movq	%rax, 8(%r12)
	movq	-360(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L5533
	movq	%rax, 8(%r12)
	movq	-344(%rbp), %rax
	movq	%rax, 24(%r12)
.L5501:
	movq	-352(%rbp), %rax
	xorl	%edi, %edi
	movq	%rbx, -360(%rbp)
	movw	%di, -344(%rbp)
	movq	%r8, %rdi
	movq	%rax, 16(%r12)
	movq	-328(%rbp), %rax
	movq	$0, -352(%rbp)
	movq	%rax, 40(%r12)
	movl	-320(%rbp), %eax
	movl	%eax, 48(%r12)
	call	_ZN12v8_inspector15V8InspectorImpl13EvaluateScopeD1Ev@PLT
	jmp	.L5497
.L5533:
	movdqu	-344(%rbp), %xmm3
	movups	%xmm3, 24(%r12)
	jmp	.L5501
.L5529:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8008:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl19evaluateOnCallFrameERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_S9_S9_S9_NS7_IdEEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISE_EEPNS6_8PtrMaybeINSD_16ExceptionDetailsEEE, .-_ZN12v8_inspector19V8DebuggerAgentImpl19evaluateOnCallFrameERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_S9_S9_S9_NS7_IdEEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISE_EEPNS6_8PtrMaybeINSD_16ExceptionDetailsEEE
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl16setVariableValueEiRKNS_8String16ESt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteIS7_EES3_.str1.8,"aMS",@progbits,1
	.align 8
.LC51:
	.string	"Could not find scope with given number"
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl16setVariableValueEiRKNS_8String16ESt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteIS7_EES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl16setVariableValueEiRKNS_8String16ESt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteIS7_EES3_
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl16setVariableValueEiRKNS_8String16ESt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteIS7_EES3_, @function
_ZN12v8_inspector19V8DebuggerAgentImpl16setVariableValueEiRKNS_8String16ESt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteIS7_EES3_:
.LFB8009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -408(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 32(%rsi)
	jne	.L5535
	leaq	-240(%rbp), %r13
	leaq	_ZN12v8_inspectorL19kDebuggerNotEnabledE(%rip), %rsi
.L5579:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5534
	call	_ZdlPv@PLT
.L5534:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5580
	addq	$376, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5535:
	.cfi_restore_state
	movq	24(%rsi), %rax
	movq	%rsi, %r14
	movq	%r9, -416(%rbp)
	movl	%edx, %ebx
	movq	16(%r14), %rdi
	movq	%r8, %r13
	movl	16(%rax), %esi
	call	_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi@PLT
	movq	-416(%rbp), %r9
	testb	%al, %al
	jne	.L5538
	leaq	-240(%rbp), %r13
	leaq	_ZN12v8_inspectorL18kDebuggerNotPausedE(%rip), %rsi
	jmp	.L5579
	.p2align 4,,10
	.p2align 3
.L5538:
	movq	24(%r14), %rsi
	leaq	-240(%rbp), %r15
	movq	%r9, %rdx
	movq	%r15, %rdi
	call	_ZN12v8_inspector14InjectedScript14CallFrameScopeC1EPNS_22V8InspectorSessionImplERKNS_8String16E@PLT
	leaq	-368(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-368(%rbp), %eax
	testl	%eax, %eax
	je	.L5540
	movl	%eax, (%r12)
	leaq	24(%r12), %rax
	leaq	-344(%rbp), %rdx
	movq	%rax, 8(%r12)
	movq	-360(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L5581
.L5546:
	movq	%rax, 8(%r12)
	movq	-344(%rbp), %rax
	movq	%rax, 24(%r12)
.L5547:
	movq	-352(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-328(%rbp), %rax
	movq	%rax, 40(%r12)
	movl	-320(%rbp), %eax
	movl	%eax, 48(%r12)
.L5559:
	movq	%r15, %rdi
	call	_ZN12v8_inspector14InjectedScript14CallFrameScopeD1Ev@PLT
	jmp	.L5534
	.p2align 4,,10
	.p2align 3
.L5540:
	leaq	-304(%rbp), %rax
	movq	0(%r13), %rdx
	movq	-224(%rbp), %rsi
	leaq	-392(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rax, -416(%rbp)
	movq	$0, -392(%rbp)
	call	_ZN12v8_inspector14InjectedScript19resolveCallArgumentEPNS_8protocol7Runtime12CallArgumentEPN2v85LocalINS5_5ValueEEE@PLT
	movl	-304(%rbp), %eax
	leaq	-360(%rbp), %rdi
	leaq	-296(%rbp), %rsi
	movl	%eax, -368(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-264(%rbp), %rax
	movq	-296(%rbp), %rdi
	movq	%rax, -328(%rbp)
	movl	-256(%rbp), %eax
	movl	%eax, -320(%rbp)
	leaq	-280(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5544
	call	_ZdlPv@PLT
.L5544:
	movl	-368(%rbp), %eax
	testl	%eax, %eax
	je	.L5545
	movl	%eax, (%r12)
	leaq	24(%r12), %rax
	leaq	-344(%rbp), %rdx
	movq	%rax, 8(%r12)
	movq	-360(%rbp), %rax
	cmpq	%rdx, %rax
	jne	.L5546
	movdqu	-344(%rbp), %xmm1
	movups	%xmm1, 24(%r12)
	jmp	.L5547
	.p2align 4,,10
	.p2align 3
.L5581:
	movdqu	-344(%rbp), %xmm0
	movups	%xmm0, 24(%r12)
	jmp	.L5547
	.p2align 4,,10
	.p2align 3
.L5545:
	movq	56(%r14), %rsi
	movl	-64(%rbp), %edx
	leaq	-384(%rbp), %rdi
	call	_ZN2v85debug18StackTraceIterator6CreateEPNS_7IsolateEi@PLT
	movq	-384(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	jne	.L5582
	movq	-384(%rbp), %rsi
	leaq	-376(%rbp), %rdi
	movq	(%rsi), %rax
	call	*88(%rax)
	jmp	.L5552
	.p2align 4,,10
	.p2align 3
.L5583:
	cmpb	$1, %al
	je	.L5551
	movq	-376(%rbp), %rdi
	subl	$1, %ebx
	movq	(%rdi), %rax
	call	*24(%rax)
.L5552:
	movq	-376(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	testl	%ebx, %ebx
	jg	.L5583
.L5551:
	testl	%ebx, %ebx
	je	.L5553
	movq	-416(%rbp), %rbx
	leaq	.LC51(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-304(%rbp), %rdi
	leaq	-288(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5555
	call	_ZdlPv@PLT
.L5555:
	movq	-376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5550
	movq	(%rdi), %rax
	call	*8(%rax)
.L5550:
	movq	-384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5558
	movq	(%rdi), %rax
	call	*8(%rax)
.L5558:
	movq	-360(%rbp), %rdi
	leaq	-344(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5559
	call	_ZdlPv@PLT
	jmp	.L5559
.L5553:
	movq	-376(%rbp), %r13
	movq	56(%r14), %rdi
	movq	-408(%rbp), %rsi
	movq	0(%r13), %rax
	movq	88(%rax), %rbx
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-392(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	*%rbx
	testb	%al, %al
	je	.L5556
	leaq	-192(%rbp), %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L5584
.L5556:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse13InternalErrorEv@PLT
	jmp	.L5555
.L5582:
	movq	-416(%rbp), %rbx
	leaq	.LC48(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-304(%rbp), %rdi
	leaq	-288(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5550
	call	_ZdlPv@PLT
	jmp	.L5550
.L5584:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L5555
.L5580:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8009:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl16setVariableValueEiRKNS_8String16ESt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteIS7_EES3_, .-_ZN12v8_inspector19V8DebuggerAgentImpl16setVariableValueEiRKNS_8String16ESt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteIS7_EES3_
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl14setReturnValueESt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteIS4_EE.str1.1,"aMS",@progbits,1
.LC52:
	.string	"Could not find top call frame"
	.section	.rodata._ZN12v8_inspector19V8DebuggerAgentImpl14setReturnValueESt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteIS4_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC53:
	.string	"Could not update return value at non-return position"
	.section	.text._ZN12v8_inspector19V8DebuggerAgentImpl14setReturnValueESt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector19V8DebuggerAgentImpl14setReturnValueESt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteIS4_EE
	.type	_ZN12v8_inspector19V8DebuggerAgentImpl14setReturnValueESt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteIS4_EE, @function
_ZN12v8_inspector19V8DebuggerAgentImpl14setReturnValueESt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteIS4_EE:
.LFB8012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 32(%rsi)
	jne	.L5586
	leaq	-112(%rbp), %r13
	leaq	_ZN12v8_inspectorL19kDebuggerNotEnabledE(%rip), %rsi
.L5614:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5585
	call	_ZdlPv@PLT
.L5585:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5617
	addq	$328, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5586:
	.cfi_restore_state
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	movq	%rdx, %r13
	movq	16(%rbx), %rdi
	movl	16(%rax), %esi
	call	_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi@PLT
	testb	%al, %al
	jne	.L5589
	leaq	-112(%rbp), %r13
	leaq	_ZN12v8_inspectorL18kDebuggerNotPausedE(%rip), %rsi
	jmp	.L5614
	.p2align 4,,10
	.p2align 3
.L5589:
	movq	56(%rbx), %rsi
	leaq	-352(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	56(%rbx), %rsi
	leaq	-368(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v85debug18StackTraceIterator6CreateEPNS_7IsolateEi@PLT
	movq	-368(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	jne	.L5618
	movq	-368(%rbp), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	testq	%rax, %rax
	je	.L5619
	movq	-368(%rbp), %rdi
	leaq	-320(%rbp), %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	movl	%eax, %edx
	call	_ZN12v8_inspector14InjectedScript12ContextScopeC1EPNS_22V8InspectorSessionImplEi@PLT
	leaq	-176(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector14InjectedScript5Scope10initializeEv@PLT
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	je	.L5596
	movl	%eax, (%r12)
	leaq	24(%r12), %rax
	leaq	-152(%rbp), %rdx
	movq	%rax, 8(%r12)
	movq	-168(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L5620
.L5602:
	movq	%rax, 8(%r12)
	movq	-152(%rbp), %rax
	movq	%rax, 24(%r12)
.L5603:
	movq	-160(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-136(%rbp), %rax
	movq	%rax, 40(%r12)
	movl	-128(%rbp), %eax
	movl	%eax, 48(%r12)
.L5605:
	movq	%r15, %rdi
	call	_ZN12v8_inspector14InjectedScript12ContextScopeD1Ev@PLT
.L5593:
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5604
	movq	(%rdi), %rax
	call	*8(%rax)
.L5604:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L5585
	.p2align 4,,10
	.p2align 3
.L5618:
	leaq	-112(%rbp), %r13
	leaq	.LC52(%rip), %rsi
.L5616:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5593
	call	_ZdlPv@PLT
	jmp	.L5593
	.p2align 4,,10
	.p2align 3
.L5596:
	movq	0(%r13), %rdx
	movq	-304(%rbp), %rsi
	leaq	-112(%rbp), %rdi
	leaq	-360(%rbp), %rcx
	movq	$0, -360(%rbp)
	call	_ZN12v8_inspector14InjectedScript19resolveCallArgumentEPNS_8protocol7Runtime12CallArgumentEPN2v85LocalINS5_5ValueEEE@PLT
	movl	-112(%rbp), %eax
	leaq	-168(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	movl	%eax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEEaSEOS4_
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	%rax, -136(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -128(%rbp)
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5600
	call	_ZdlPv@PLT
.L5600:
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	je	.L5601
	movl	%eax, (%r12)
	leaq	24(%r12), %rax
	leaq	-152(%rbp), %rdx
	movq	%rax, 8(%r12)
	movq	-168(%rbp), %rax
	cmpq	%rdx, %rax
	jne	.L5602
	movdqu	-152(%rbp), %xmm1
	movups	%xmm1, 24(%r12)
	jmp	.L5603
	.p2align 4,,10
	.p2align 3
.L5601:
	movq	56(%rbx), %rdi
	movq	-360(%rbp), %rsi
	call	_ZN2v85debug14SetReturnValueEPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-168(%rbp), %rdi
	leaq	-152(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5605
	call	_ZdlPv@PLT
	jmp	.L5605
	.p2align 4,,10
	.p2align 3
.L5619:
	leaq	-112(%rbp), %r13
	leaq	.LC53(%rip), %rsi
	jmp	.L5616
	.p2align 4,,10
	.p2align 3
.L5620:
	movdqu	-152(%rbp), %xmm0
	movups	%xmm0, 24(%r12)
	jmp	.L5603
.L5617:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8012:
	.size	_ZN12v8_inspector19V8DebuggerAgentImpl14setReturnValueESt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteIS4_EE, .-_ZN12v8_inspector19V8DebuggerAgentImpl14setReturnValueESt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteIS4_EE
	.weak	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE, 48
_ZTVN12v8_inspector8protocol7Runtime16ExceptionDetailsE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime16ExceptionDetails17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime16ExceptionDetailsD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime9CallFrameE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime9CallFrameE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE, 48
_ZTVN12v8_inspector8protocol7Runtime9CallFrameE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Debugger8LocationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Debugger8LocationE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Debugger8LocationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Debugger8LocationE, @object
	.size	_ZTVN12v8_inspector8protocol8Debugger8LocationE, 48
_ZTVN12v8_inspector8protocol8Debugger8LocationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Debugger8LocationD1Ev
	.quad	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Debugger9CallFrameE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Debugger9CallFrameE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Debugger9CallFrameE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Debugger9CallFrameE, @object
	.size	_ZTVN12v8_inspector8protocol8Debugger9CallFrameE, 48
_ZTVN12v8_inspector8protocol8Debugger9CallFrameE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Debugger9CallFrame15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Debugger9CallFrame17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Debugger9CallFrameD1Ev
	.quad	_ZN12v8_inspector8protocol8Debugger9CallFrameD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Debugger5ScopeE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Debugger5ScopeE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Debugger5ScopeE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Debugger5ScopeE, @object
	.size	_ZTVN12v8_inspector8protocol8Debugger5ScopeE, 48
_ZTVN12v8_inspector8protocol8Debugger5ScopeE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Debugger5Scope15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Debugger5Scope17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Debugger5ScopeD1Ev
	.quad	_ZN12v8_inspector8protocol8Debugger5ScopeD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Debugger13BreakLocationE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE, @object
	.size	_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE, 48
_ZTVN12v8_inspector8protocol8Debugger13BreakLocationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Debugger13BreakLocation15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Debugger13BreakLocation17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Debugger13BreakLocationD1Ev
	.quad	_ZN12v8_inspector8protocol8Debugger13BreakLocationD0Ev
	.weak	_ZTVN12v8_inspector19V8DebuggerAgentImplE
	.section	.data.rel.ro.local._ZTVN12v8_inspector19V8DebuggerAgentImplE,"awG",@progbits,_ZTVN12v8_inspector19V8DebuggerAgentImplE,comdat
	.align 8
	.type	_ZTVN12v8_inspector19V8DebuggerAgentImplE, @object
	.size	_ZTVN12v8_inspector19V8DebuggerAgentImplE, 264
_ZTVN12v8_inspector19V8DebuggerAgentImplE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector19V8DebuggerAgentImplD1Ev
	.quad	_ZN12v8_inspector19V8DebuggerAgentImplD0Ev
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl18continueToLocationESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEE
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl7disableEv
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl6enableEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIdEEPNS_8String16E
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl19evaluateOnCallFrameERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EENS7_IbEES9_S9_S9_S9_NS7_IdEEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISE_EEPNS6_8PtrMaybeINSD_16ExceptionDetailsEEE
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl22getPossibleBreakpointsESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeIS4_EENSA_10ValueMaybeIbEEPS1_ISt6vectorIS1_INS3_13BreakLocationES5_ISG_EESaISI_EES5_ISK_EE
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl15getScriptSourceERKNS_8String16EPS1_
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl13getStackTraceESt10unique_ptrINS_8protocol7Runtime12StackTraceIdESt14default_deleteIS4_EEPS1_INS3_10StackTraceES5_IS8_EE
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl5pauseEv
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl16pauseOnAsyncCallESt10unique_ptrINS_8protocol7Runtime12StackTraceIdESt14default_deleteIS4_EE
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl16removeBreakpointERKNS_8String16E
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl12restartFrameERKNS_8String16EPSt10unique_ptrISt6vectorIS4_INS_8protocol8Debugger9CallFrameESt14default_deleteIS8_EESaISB_EES9_ISD_EEPN30v8_inspector_protocol_bindings4glue6detail8PtrMaybeINS6_7Runtime10StackTraceEEEPNSK_INSL_12StackTraceIdEEE
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl6resumeEv
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl15searchInContentERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEES8_PSt10unique_ptrISt6vectorIS9_INS_8protocol8Debugger11SearchMatchESt14default_deleteISD_EESaISG_EESE_ISI_EE
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl22setAsyncCallStackDepthEi
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl19setBlackboxPatternsESt10unique_ptrISt6vectorINS_8String16ESaIS3_EESt14default_deleteIS5_EE
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl19setBlackboxedRangesERKNS_8String16ESt10unique_ptrISt6vectorIS4_INS_8protocol8Debugger14ScriptPositionESt14default_deleteIS8_EESaISB_EES9_ISD_EE
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl13setBreakpointESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS4_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEEPSC_PS7_
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl28setInstrumentationBreakpointERKNS_8String16EPS1_
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl18setBreakpointByUrlEiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeINS_8String16EEES6_S6_NS4_IiEES6_PS5_PSt10unique_ptrISt6vectorIS9_INS_8protocol8Debugger8LocationESt14default_deleteISD_EESaISG_EESE_ISI_EE
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl27setBreakpointOnFunctionCallERKNS_8String16EN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS1_EEPS1_
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl20setBreakpointsActiveEb
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl20setPauseOnExceptionsERKNS_8String16E
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl14setReturnValueESt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteIS4_EE
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl15setScriptSourceERKNS_8String16ES3_N30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEEPNS6_8PtrMaybeISt6vectorISt10unique_ptrINS_8protocol8Debugger9CallFrameESt14default_deleteISE_EESaISH_EEEEPS8_PNS9_INSC_7Runtime10StackTraceEEEPNS9_INSN_12StackTraceIdEEEPNS9_INSN_16ExceptionDetailsEEE
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl16setSkipAllPausesEb
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl16setVariableValueEiRKNS_8String16ESt10unique_ptrINS_8protocol7Runtime12CallArgumentESt14default_deleteIS7_EES3_
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl8stepIntoEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl7stepOutEv
	.quad	_ZN12v8_inspector19V8DebuggerAgentImpl8stepOverEv
	.section	.rodata._ZN12v8_inspectorL18kDebuggerNotPausedE,"a"
	.align 32
	.type	_ZN12v8_inspectorL18kDebuggerNotPausedE, @object
	.size	_ZN12v8_inspectorL18kDebuggerNotPausedE, 41
_ZN12v8_inspectorL18kDebuggerNotPausedE:
	.string	"Can only perform operation while paused."
	.section	.rodata._ZN12v8_inspectorL19kDebuggerNotEnabledE,"a"
	.align 16
	.type	_ZN12v8_inspectorL19kDebuggerNotEnabledE, @object
	.size	_ZN12v8_inspectorL19kDebuggerNotEnabledE, 30
_ZN12v8_inspectorL19kDebuggerNotEnabledE:
	.string	"Debugger agent is not enabled"
	.section	.rodata._ZN12v8_inspectorL21kBacktraceObjectGroupE,"a"
	.align 8
	.type	_ZN12v8_inspectorL21kBacktraceObjectGroupE, @object
	.size	_ZN12v8_inspectorL21kBacktraceObjectGroupE, 10
_ZN12v8_inspectorL21kBacktraceObjectGroupE:
	.string	"backtrace"
	.section	.rodata._ZN12v8_inspector18DebuggerAgentStateL26instrumentationBreakpointsE,"a"
	.align 16
	.type	_ZN12v8_inspector18DebuggerAgentStateL26instrumentationBreakpointsE, @object
	.size	_ZN12v8_inspector18DebuggerAgentStateL26instrumentationBreakpointsE, 27
_ZN12v8_inspector18DebuggerAgentStateL26instrumentationBreakpointsE:
	.string	"instrumentationBreakpoints"
	.section	.rodata._ZN12v8_inspector18DebuggerAgentStateL15breakpointHintsE,"a"
	.align 16
	.type	_ZN12v8_inspector18DebuggerAgentStateL15breakpointHintsE, @object
	.size	_ZN12v8_inspector18DebuggerAgentStateL15breakpointHintsE, 16
_ZN12v8_inspector18DebuggerAgentStateL15breakpointHintsE:
	.string	"breakpointHints"
	.section	.rodata._ZN12v8_inspector18DebuggerAgentStateL23breakpointsByScriptHashE,"a"
	.align 16
	.type	_ZN12v8_inspector18DebuggerAgentStateL23breakpointsByScriptHashE, @object
	.size	_ZN12v8_inspector18DebuggerAgentStateL23breakpointsByScriptHashE, 24
_ZN12v8_inspector18DebuggerAgentStateL23breakpointsByScriptHashE:
	.string	"breakpointsByScriptHash"
	.section	.rodata._ZN12v8_inspector18DebuggerAgentStateL16breakpointsByUrlE,"a"
	.align 16
	.type	_ZN12v8_inspector18DebuggerAgentStateL16breakpointsByUrlE, @object
	.size	_ZN12v8_inspector18DebuggerAgentStateL16breakpointsByUrlE, 17
_ZN12v8_inspector18DebuggerAgentStateL16breakpointsByUrlE:
	.string	"breakpointsByUrl"
	.section	.rodata._ZN12v8_inspector18DebuggerAgentStateL18breakpointsByRegexE,"a"
	.align 16
	.type	_ZN12v8_inspector18DebuggerAgentStateL18breakpointsByRegexE, @object
	.size	_ZN12v8_inspector18DebuggerAgentStateL18breakpointsByRegexE, 19
_ZN12v8_inspector18DebuggerAgentStateL18breakpointsByRegexE:
	.string	"breakpointsByRegex"
	.section	.rodata._ZN12v8_inspector18DebuggerAgentStateL13skipAllPausesE,"a"
	.align 8
	.type	_ZN12v8_inspector18DebuggerAgentStateL13skipAllPausesE, @object
	.size	_ZN12v8_inspector18DebuggerAgentStateL13skipAllPausesE, 14
_ZN12v8_inspector18DebuggerAgentStateL13skipAllPausesE:
	.string	"skipAllPauses"
	.section	.rodata._ZN12v8_inspector18DebuggerAgentStateL15debuggerEnabledE,"a"
	.align 16
	.type	_ZN12v8_inspector18DebuggerAgentStateL15debuggerEnabledE, @object
	.size	_ZN12v8_inspector18DebuggerAgentStateL15debuggerEnabledE, 16
_ZN12v8_inspector18DebuggerAgentStateL15debuggerEnabledE:
	.string	"debuggerEnabled"
	.section	.rodata._ZN12v8_inspector18DebuggerAgentStateL15blackboxPatternE,"a"
	.align 16
	.type	_ZN12v8_inspector18DebuggerAgentStateL15blackboxPatternE, @object
	.size	_ZN12v8_inspector18DebuggerAgentStateL15blackboxPatternE, 16
_ZN12v8_inspector18DebuggerAgentStateL15blackboxPatternE:
	.string	"blackboxPattern"
	.section	.rodata._ZN12v8_inspector18DebuggerAgentStateL19asyncCallStackDepthE,"a"
	.align 16
	.type	_ZN12v8_inspector18DebuggerAgentStateL19asyncCallStackDepthE, @object
	.size	_ZN12v8_inspector18DebuggerAgentStateL19asyncCallStackDepthE, 20
_ZN12v8_inspector18DebuggerAgentStateL19asyncCallStackDepthE:
	.string	"asyncCallStackDepth"
	.section	.rodata._ZN12v8_inspector18DebuggerAgentStateL22pauseOnExceptionsStateE,"a"
	.align 16
	.type	_ZN12v8_inspector18DebuggerAgentStateL22pauseOnExceptionsStateE, @object
	.size	_ZN12v8_inspector18DebuggerAgentStateL22pauseOnExceptionsStateE, 23
_ZN12v8_inspector18DebuggerAgentStateL22pauseOnExceptionsStateE:
	.string	"pauseOnExceptionsState"
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC6:
	.long	1065353216
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC37:
	.long	0
	.long	1139802112
	.align 8
.LC38:
	.long	0
	.long	1138753536
	.align 8
.LC40:
	.long	0
	.long	0
	.align 8
.LC49:
	.long	0
	.long	1083129856
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
