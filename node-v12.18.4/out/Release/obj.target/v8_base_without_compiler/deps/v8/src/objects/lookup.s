	.file	"lookup.cc"
	.text
	.section	.rodata._ZN2v88internal8JSObject12WriteToFieldEiNS0_15PropertyDetailsENS0_6ObjectE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.rodata._ZN2v88internal8JSObject12WriteToFieldEiNS0_15PropertyDetailsENS0_6ObjectE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"(!IsSmi() && (*layout_word_index < length())) || (IsSmi() && (*layout_word_index < 1))"
	.section	.rodata._ZN2v88internal8JSObject12WriteToFieldEiNS0_15PropertyDetailsENS0_6ObjectE.str1.1
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8JSObject12WriteToFieldEiNS0_15PropertyDetailsENS0_6ObjectE,"axG",@progbits,_ZN2v88internal8JSObject12WriteToFieldEiNS0_15PropertyDetailsENS0_6ObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8JSObject12WriteToFieldEiNS0_15PropertyDetailsENS0_6ObjectE
	.type	_ZN2v88internal8JSObject12WriteToFieldEiNS0_15PropertyDetailsENS0_6ObjectE, @function
_ZN2v88internal8JSObject12WriteToFieldEiNS0_15PropertyDetailsENS0_6ObjectE:
.LFB11929:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	-1(%rax), %rcx
	leal	3(%rsi,%rsi,2), %eax
	sall	$3, %eax
	movq	39(%rcx), %rsi
	cltq
	movq	7(%rax,%rsi), %r8
	movzbl	7(%rcx), %edi
	movzbl	8(%rcx), %esi
	movq	%r8, %rax
	shrq	$38, %r8
	shrq	$51, %rax
	subl	%esi, %edi
	andl	$7, %r8d
	movq	%rax, %r10
	andl	$1023, %r10d
	cmpl	%edi, %r10d
	setl	%sil
	jl	.L63
	subl	%edi, %r10d
	movl	$16, %r9d
	leal	16(,%r10,8), %r10d
.L3:
	cmpl	$2, %r8d
	je	.L39
	cmpb	$2, %r8b
	jg	.L5
	je	.L6
.L40:
	xorl	%ecx, %ecx
.L4:
	movzbl	%sil, %eax
	movslq	%edi, %rdi
	movslq	%r10d, %r10
	movslq	%r9d, %r9
	salq	$17, %rdi
	salq	$14, %rax
	movq	%rcx, %rsi
	movq	(%rbx), %r8
	salq	$27, %r9
	orq	%rdi, %rax
	shrl	$6, %edx
	movq	%r12, %rdi
	orq	%r10, %rax
	notq	%rdi
	andl	$7, %edx
	orq	%r9, %rax
	andl	$1, %edi
	orq	%rax, %rsi
	andl	$16384, %eax
	cmpl	$2, %edx
	jne	.L7
	testb	%dil, %dil
	je	.L8
	sarq	$32, %r12
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	movq	%xmm0, %rdx
.L9:
	movq	%rsi, %rdi
	movl	%esi, %ecx
	movq	-1(%r8), %r9
	shrq	$30, %rdi
	sarl	$3, %ecx
	andl	$15, %edi
	andl	$2047, %ecx
	subl	%edi, %ecx
	testq	%rax, %rax
	jne	.L10
	movq	%r8, %rax
	movq	7(%r8), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %sil
	jne	.L11
.L24:
	movq	968(%rax), %rsi
.L12:
	leal	16(,%rcx,8), %eax
	cltq
	movq	-1(%rsi,%rax), %rax
.L36:
	movq	%rdx, 7(%rax)
.L1:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	subl	$3, %r8d
	cmpb	$1, %r8b
	jbe	.L40
.L6:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$32768, %ecx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L7:
	testq	%rax, %rax
	je	.L25
	andl	$16376, %esi
	movq	%r12, -1(%r8,%rsi)
	movq	%rsi, %r13
	testb	%dil, %dil
	jne	.L1
	movq	%r12, %r14
	movq	(%rbx), %rdi
	andq	$-262144, %r14
	movq	8(%r14), %rax
	leaq	-1(%rsi,%rdi), %rsi
	testl	$262144, %eax
	je	.L27
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r14), %rax
	leaq	-1(%r13,%rdi), %rsi
.L27:
	testb	$24, %al
	je	.L1
	movq	%rdi, %rax
	movq	%r12, %rdx
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1
.L61:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movzbl	8(%rcx), %r9d
	movzbl	8(%rcx), %eax
	addl	%eax, %r10d
	sall	$3, %r9d
	sall	$3, %r10d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L10:
	movq	47(%r9), %rax
	testq	%rax, %rax
	je	.L22
	movq	%rax, %r8
	movl	$32, %edi
	notq	%r8
	movl	%r8d, %r9d
	andl	$1, %r9d
	jne	.L14
	movslq	11(%rax), %rdi
	sall	$3, %edi
.L14:
	cmpl	%ecx, %edi
	jbe	.L22
	testl	%ecx, %ecx
	leal	31(%rcx), %edi
	cmovns	%ecx, %edi
	sarl	$5, %edi
	andl	$1, %r8d
	jne	.L15
	cmpl	%edi, 11(%rax)
	jle	.L15
	movl	%ecx, %r8d
	sarl	$31, %r8d
	shrl	$27, %r8d
	addl	%r8d, %ecx
	andl	$31, %ecx
	subl	%r8d, %ecx
	movl	$1, %r8d
	sall	%cl, %r8d
	movl	%r8d, %ecx
	testb	%r9b, %r9b
	je	.L64
.L19:
	sarq	$32, %rax
	testl	%eax, %ecx
	sete	%dil
.L21:
	movq	(%rbx), %rcx
	movq	%rsi, %rax
	andl	$16376, %eax
	leaq	-1(%rcx,%rax), %rax
	testb	%dil, %dil
	jne	.L22
	movq	%rdx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	%r8, %rax
	movq	7(%r8), %r13
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %r13b
	jne	.L30
.L32:
	movq	968(%rax), %r13
.L31:
	movq	%rsi, %rax
	movl	%esi, %ecx
	shrq	$30, %rax
	sarl	$3, %ecx
	andl	$15, %eax
	andl	$2047, %ecx
	subl	%eax, %ecx
	leal	16(,%rcx,8), %eax
	cltq
	leaq	-1(%r13,%rax), %r14
	movq	%r12, (%r14)
	testb	%dil, %dil
	jne	.L1
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L34
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L34:
	testb	$24, %al
	je	.L1
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%r12, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rcx
	movabsq	$-2251799814209537, %rdx
	cmpq	%r12, -37512(%rcx)
	je	.L9
	movq	7(%r12), %rdx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L30:
	cmpq	288(%rax), %r13
	jne	.L31
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L22:
	movq	(%rbx), %rax
	andl	$16376, %esi
	movq	-1(%rsi,%rax), %rax
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L11:
	cmpq	288(%rax), %rsi
	jne	.L12
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L64:
	sall	$2, %edi
	movslq	%edi, %rdi
	movl	15(%rax,%rdi), %eax
	testl	%eax, %r8d
	sete	%dil
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L15:
	cmpl	$31, %ecx
	jg	.L17
	movl	$1, %edi
	sall	%cl, %edi
	movl	%edi, %ecx
	testb	%r9b, %r9b
	jne	.L19
.L17:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE11929:
	.size	_ZN2v88internal8JSObject12WriteToFieldEiNS0_15PropertyDetailsENS0_6ObjectE, .-_ZN2v88internal8JSObject12WriteToFieldEiNS0_15PropertyDetailsENS0_6ObjectE
	.section	.text._ZN2v88internal14LookupIteratorC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_3MapEEENS0_15PropertyDetailsEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LookupIteratorC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_3MapEEENS0_15PropertyDetailsEb
	.type	_ZN2v88internal14LookupIteratorC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_3MapEEENS0_15PropertyDetailsEb, @function
_ZN2v88internal14LookupIteratorC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_3MapEEENS0_15PropertyDetailsEb:
.LFB18663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movabsq	$30064771075, %rax
	movq	%rax, (%rdi)
	movl	16(%rbp), %eax
	movq	$0, 56(%rdi)
	movb	%al, 8(%rdi)
	movq	(%rdx), %rax
	movl	$0, 12(%rdi)
	movl	%r9d, 16(%rdi)
	movq	%rsi, 24(%rdi)
	movq	%rcx, 32(%rdi)
	movq	%r8, 40(%rdi)
	movq	%rdx, 48(%rdi)
	testb	$1, %al
	jne	.L77
.L67:
	leaq	-48(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal6Object24GetPrototypeChainRootMapEPNS0_7IsolateE@PLT
	movq	41112(%r12), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L71
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	movq	104(%r12), %rax
	cmpq	%rax, 0(%r13)
	je	.L78
.L70:
	movq	%r13, 64(%rbx)
	movq	$-1, 72(%rbx)
	movq	%r13, 56(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L80
.L73:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	movq	104(%r12), %rax
	cmpq	%rax, 0(%r13)
	jne	.L70
.L78:
	movq	(%r14), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate20PushStackTraceAndDieEPvS2_S2_S2_@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L77:
	movq	-1(%rax), %rdx
	movq	%r14, %r13
	cmpw	$1023, 11(%rdx)
	ja	.L70
	movq	-1(%rax), %rdx
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L80:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L73
.L79:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18663:
	.size	_ZN2v88internal14LookupIteratorC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_3MapEEENS0_15PropertyDetailsEb, .-_ZN2v88internal14LookupIteratorC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_3MapEEENS0_15PropertyDetailsEb
	.globl	_ZN2v88internal14LookupIteratorC1EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_3MapEEENS0_15PropertyDetailsEb
	.set	_ZN2v88internal14LookupIteratorC1EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_3MapEEENS0_15PropertyDetailsEb,_ZN2v88internal14LookupIteratorC2EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS4_INS0_4NameEEENS4_INS0_3MapEEENS0_15PropertyDetailsEb
	.section	.text._ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj
	.type	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj, @function
_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj:
.LFB18669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L107
.L82:
	leaq	-64(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal6Object24GetPrototypeChainRootMapEPNS0_7IsolateE@PLT
	movq	41112(%r12), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L108
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
	movq	(%rbx), %rax
	cmpq	%rax, 104(%r12)
	je	.L109
.L96:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	%rbx, %rax
	jne	.L110
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L111
.L95:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	movq	(%rbx), %rax
	cmpq	%rax, 104(%r12)
	jne	.L96
.L109:
	movq	0(%r13), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate20PushStackTraceAndDieEPvS2_S2_S2_@PLT
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L107:
	movq	-1(%rax), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L82
	cmpl	%edx, 11(%rax)
	jbe	.L82
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	1439(%rax), %r14
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L87
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L88:
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	0(%r13), %r12
	movq	(%rax), %r14
	movq	%rax, %rbx
	movq	%r12, 23(%r14)
	leaq	23(%r14), %r15
	testb	$1, %r12b
	je	.L96
	movq	%r12, %r13
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testl	$262144, %eax
	je	.L91
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r13), %rax
.L91:
	testb	$24, %al
	je	.L96
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L96
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L111:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L87:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L112
.L89:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L88
.L112:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L89
.L110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18669:
	.size	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj, .-_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj
	.section	.text._ZNK2v88internal14LookupIterator14GetReceiverMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14LookupIterator14GetReceiverMapEv
	.type	_ZNK2v88internal14LookupIterator14GetReceiverMapEv, @function
_ZNK2v88internal14LookupIterator14GetReceiverMapEv:
.LFB18670:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	movq	(%rax), %rax
	testb	$1, %al
	je	.L116
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L116
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	24(%rdi), %rbx
	movq	-1(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L117
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore 3
	.cfi_restore 6
	movq	24(%rdi), %rax
	addq	$256, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L124
.L119:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L119
	.cfi_endproc
.LFE18670:
	.size	_ZNK2v88internal14LookupIterator14GetReceiverMapEv, .-_ZNK2v88internal14LookupIterator14GetReceiverMapEv
	.section	.text._ZNK2v88internal14LookupIterator9HasAccessEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14LookupIterator9HasAccessEv
	.type	_ZNK2v88internal14LookupIterator9HasAccessEv, @function
_ZNK2v88internal14LookupIterator9HasAccessEv:
.LFB18671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	24(%rdi), %r12
	movq	56(%rdi), %r13
	movq	41112(%r12), %rdi
	movq	12464(%r12), %r14
	testq	%rdi, %rdi
	je	.L126
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L127:
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L130
.L128:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L130:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L128
	.cfi_endproc
.LFE18671:
	.size	_ZNK2v88internal14LookupIterator9HasAccessEv, .-_ZNK2v88internal14LookupIterator9HasAccessEv
	.section	.text._ZN2v88internal14LookupIterator23InternalUpdateProtectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LookupIterator23InternalUpdateProtectorEv
	.type	_ZN2v88internal14LookupIterator23InternalUpdateProtectorEv, @function
_ZN2v88internal14LookupIterator23InternalUpdateProtectorEv:
.LFB18677:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40936(%r12), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L131
	movq	48(%rdi), %r13
	movq	%rdi, %rbx
	testb	$1, 0(%r13)
	jne	.L334
.L131:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L335
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	movq	12464(%r12), %rax
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L133
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L134:
	movq	32(%rbx), %rax
	movq	24(%rbx), %rdi
	movq	(%rax), %rdx
	cmpq	2304(%rdi), %rdx
	je	.L336
	cmpq	2912(%rdi), %rdx
	je	.L337
	cmpq	3904(%rdi), %rdx
	je	.L338
	cmpq	3944(%rdi), %rdx
	je	.L339
	cmpq	3856(%rdi), %rdx
	je	.L340
	cmpq	3192(%rdi), %rdx
	je	.L341
	cmpq	3408(%rdi), %rdx
	jne	.L131
	call	_ZN2v88internal7Isolate30IsPromiseThenLookupChainIntactEv@PLT
	testb	%al, %al
	je	.L131
	movq	0(%r13), %rsi
	movq	-1(%rsi), %rax
	cmpw	$1074, 11(%rax)
	je	.L187
	movq	24(%rbx), %rdi
	movl	$63, %edx
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	je	.L342
.L187:
	movq	24(%rbx), %rdi
	call	_ZN2v88internal7Isolate30InvalidatePromiseThenProtectorEv@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L133:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L343
.L135:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L336:
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	ja	.L344
.L137:
	movq	4520(%rdi), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L138
	sarq	$32, %rax
	cmpq	$1, %rax
	jne	.L138
.L142:
	movq	0(%r13), %rsi
	movq	-1(%rsi), %rax
	cmpw	$1061, 11(%rax)
	jne	.L345
	movq	4520(%rdi), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L131
	sarq	$32, %rax
	cmpq	$1, %rax
	jne	.L131
	movl	$28, %esi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	24(%rbx), %rdi
	call	_ZN2v88internal10Protectors33InvalidateArraySpeciesLookupChainEPNS0_7IsolateE@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L138:
	movq	4536(%rdi), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L346
.L141:
	movq	(%r14), %rax
	movq	1239(%rax), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L143
	sarq	$32, %rax
	cmpq	$1, %rax
	je	.L142
.L143:
	movq	4528(%rdi), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L131
	sarq	$32, %rax
	cmpq	$1, %rax
	jne	.L131
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L337:
	movq	0(%r13), %rsi
	movq	-1(%rsi), %rax
	cmpw	$1060, 11(%rax)
	jne	.L347
.L310:
	movq	4552(%rdi), %rdx
	movabsq	$4294967296, %rax
	cmpq	%rax, 23(%rdx)
	jne	.L131
	call	_ZN2v88internal7Isolate32InvalidateArrayIteratorProtectorEv@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L346:
	sarq	$32, %rax
	cmpq	$1, %rax
	jne	.L141
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L347:
	movl	$55, %edx
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	je	.L295
	movq	24(%rbx), %rdi
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L344:
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal10JSReceiver18GetCreationContextEv@PLT
	movq	24(%rbx), %rdi
	movq	%rax, %r14
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L338:
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	ja	.L348
.L160:
	movq	4520(%rdi), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L161
	sarq	$32, %rax
	cmpq	$1, %rax
	je	.L162
.L161:
	movq	4536(%rdi), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L163
	sarq	$32, %rax
	cmpq	$1, %rax
	je	.L162
.L163:
	movq	(%r14), %rax
	movq	1239(%rax), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L165
	sarq	$32, %rax
	cmpq	$1, %rax
	je	.L162
.L165:
	movq	4528(%rdi), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L131
	sarq	$32, %rax
	cmpq	$1, %rax
	jne	.L131
	.p2align 4,,10
	.p2align 3
.L162:
	movq	0(%r13), %rsi
	movl	$11, %edx
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	movq	24(%rbx), %rdi
	testb	%al, %al
	je	.L166
	movq	4520(%rdi), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L131
	sarq	$32, %rax
	cmpq	$1, %rax
	jne	.L131
	movl	$25, %esi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	24(%rbx), %rdi
	call	_ZN2v88internal10Protectors33InvalidateArraySpeciesLookupChainEPNS0_7IsolateE@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L339:
	call	_ZN2v88internal7Isolate37IsIsConcatSpreadableLookupChainIntactEv@PLT
	testb	%al, %al
	je	.L131
	movq	24(%rbx), %rdi
	call	_ZN2v88internal7Isolate37InvalidateIsConcatSpreadableProtectorEv@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L340:
	movq	0(%r13), %rsi
	movq	-1(%rsi), %rax
	cmpw	$1061, 11(%rax)
	je	.L310
	movq	-1(%rsi), %rax
	cmpw	$1077, 11(%rax)
	je	.L185
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subw	$1078, %ax
	cmpw	$1, %ax
	ja	.L349
.L185:
	movq	4600(%rdi), %rdx
	movabsq	$4294967296, %rax
	cmpq	%rax, 23(%rdx)
	jne	.L131
	call	_ZN2v88internal7Isolate30InvalidateSetIteratorProtectorEv@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L343:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L345:
	movq	-1(%rsi), %rax
	cmpw	$1074, 11(%rax)
	je	.L329
	movq	-1(%rsi), %rax
	cmpw	$1075, 11(%rax)
	je	.L350
	movq	-1(%rsi), %rax
	cmpw	$1086, 11(%rax)
	je	.L333
	movq	-1(%rsi), %rax
	movl	15(%rax), %eax
	testl	$1048576, %eax
	je	.L131
	movl	$56, %edx
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	movq	24(%rbx), %rdi
	testb	%al, %al
	je	.L148
	movq	4520(%rdi), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L131
	sarq	$32, %rax
	subq	$1, %rax
	jne	.L131
	movl	$26, %esi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	24(%rbx), %rdi
	call	_ZN2v88internal10Protectors33InvalidateArraySpeciesLookupChainEPNS0_7IsolateE@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L341:
	call	_ZN2v88internal7Isolate33IsPromiseResolveLookupChainIntactEv@PLT
	testb	%al, %al
	je	.L131
	movq	24(%rbx), %rdi
	movq	0(%r13), %rsi
	movl	$212, %edx
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	je	.L131
	movq	24(%rbx), %rdi
	call	_ZN2v88internal7Isolate33InvalidatePromiseResolveProtectorEv@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L329:
	movq	4536(%rdi), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L131
	sarq	$32, %rax
	cmpq	$1, %rax
	jne	.L131
	call	_ZN2v88internal7Isolate33InvalidatePromiseSpeciesProtectorEv@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L348:
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal10JSReceiver18GetCreationContextEv@PLT
	movq	24(%rbx), %rdi
	movq	%rax, %r14
	jmp	.L160
.L166:
	movq	0(%r13), %rsi
	movl	$212, %edx
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	movq	24(%rbx), %rdi
	testb	%al, %al
	jne	.L329
	movq	0(%r13), %rsi
	movl	$129, %edx
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	jne	.L327
	movq	0(%r13), %r13
	movq	24(%rbx), %r14
	leaq	_ZZN2v88internal12_GLOBAL__N_132IsTypedArrayFunctionInAnyContextEPNS0_7IsolateENS0_10HeapObjectEE13context_slots(%rip), %r12
	leaq	32(%r12), %r15
	movq	-1(%r13), %rax
	cmpw	$1105, 11(%rax)
	jne	.L131
	jmp	.L174
.L191:
	movq	%rax, %r12
.L174:
	movl	(%r12), %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	jne	.L190
	movl	4(%r12), %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	jne	.L351
	movl	8(%r12), %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	jne	.L352
	movl	12(%r12), %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	jne	.L353
	leaq	16(%r12), %rax
	cmpq	%r15, %rax
	jne	.L191
	movl	16(%r12), %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	jne	.L188
	movl	20(%r12), %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	20(%r12), %r15
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	jne	.L170
	movl	24(%r12), %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	24(%r12), %r15
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	je	.L131
.L170:
	leaq	44+_ZZN2v88internal12_GLOBAL__N_132IsTypedArrayFunctionInAnyContextEPNS0_7IsolateENS0_10HeapObjectEE13context_slots(%rip), %rax
	cmpq	%rax, %r15
	je	.L131
.L188:
	movq	24(%rbx), %rdi
.L333:
	movq	4528(%rdi), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L131
	sarq	$32, %rax
	cmpq	$1, %rax
	jne	.L131
	call	_ZN2v88internal7Isolate36InvalidateTypedArraySpeciesProtectorEv@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L295:
	movq	0(%r13), %rsi
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subw	$1070, %ax
	cmpw	$2, %ax
	ja	.L354
.L181:
	movabsq	$4294967296, %rax
	movq	24(%rbx), %rdi
	movq	4584(%rdi), %rdx
	cmpq	%rax, 23(%rdx)
	jne	.L131
	call	_ZN2v88internal7Isolate30InvalidateMapIteratorProtectorEv@PLT
	jmp	.L131
.L350:
	movq	(%r14), %rax
	movq	1239(%rax), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L131
	sarq	$32, %rax
	cmpq	$1, %rax
	jne	.L131
.L305:
	movq	%r14, %rsi
	call	_ZN2v88internal10Protectors43InvalidateRegExpSpeciesLookupChainProtectorEPNS0_7IsolateENS0_6HandleINS0_13NativeContextEEE@PLT
	jmp	.L131
.L354:
	movq	24(%rbx), %rdi
	movl	$61, %edx
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	jne	.L181
	movq	0(%r13), %rsi
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subw	$1078, %ax
	cmpw	$1, %ax
	ja	.L355
.L313:
	movq	24(%rbx), %rdi
	jmp	.L185
.L349:
	movl	$64, %edx
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	movq	24(%rbx), %rdi
	testb	%al, %al
	jne	.L185
	movq	0(%r13), %rsi
	movl	$65, %edx
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	jne	.L313
	movq	0(%r13), %rsi
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subw	$1070, %ax
	cmpw	$2, %ax
	jbe	.L181
	movq	24(%rbx), %rdi
	movl	$61, %edx
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	jne	.L181
	movq	24(%rbx), %rdi
	movq	0(%r13), %rsi
	movl	$60, %edx
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	je	.L356
	movq	24(%rbx), %rdi
	movl	$1, %eax
	salq	$32, %rax
	movq	4584(%rdi), %rdx
	cmpq	%rax, 23(%rdx)
	jne	.L185
	call	_ZN2v88internal7Isolate30InvalidateMapIteratorProtectorEv@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L342:
	movq	24(%rbx), %rdi
	movq	0(%r13), %rsi
	movl	$127, %edx
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	je	.L131
	jmp	.L187
.L327:
	movq	(%r14), %rax
	movq	1239(%rax), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L131
	sarq	$32, %rax
	subq	$1, %rax
	jne	.L131
	movq	24(%rbx), %rdi
	jmp	.L305
.L148:
	movq	0(%r13), %rsi
	movl	$127, %edx
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	movq	24(%rbx), %rdi
	testb	%al, %al
	jne	.L329
	movq	0(%r13), %rsi
	movl	$133, %edx
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	jne	.L327
	movq	0(%r13), %rax
	movq	24(%rbx), %rdi
	movl	$190, %edx
	movq	-1(%rax), %rax
	movq	23(%rax), %rsi
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	je	.L131
	jmp	.L188
.L355:
	movq	24(%rbx), %rdi
	movl	$64, %edx
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	jne	.L313
	movq	0(%r13), %rsi
	movq	-1(%rsi), %rax
	cmpw	$1080, 11(%rax)
	je	.L315
	movq	24(%rbx), %rdi
	movl	$68, %edx
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	je	.L131
.L315:
	movq	24(%rbx), %rdi
	movl	$1, %eax
	salq	$32, %rax
	movq	4608(%rdi), %rdx
	cmpq	%rax, 23(%rdx)
	jne	.L131
	call	_ZN2v88internal7Isolate33InvalidateStringIteratorProtectorEv@PLT
	jmp	.L131
.L335:
	call	__stack_chk_fail@PLT
.L353:
	leaq	12(%r12), %r15
	jmp	.L170
.L352:
	leaq	8(%r12), %r15
	jmp	.L170
.L351:
	leaq	4(%r12), %r15
	jmp	.L170
.L190:
	movq	%r12, %r15
	jmp	.L170
.L356:
	movq	24(%rbx), %rdi
	movq	0(%r13), %rsi
	movl	$69, %edx
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	je	.L131
	jmp	.L315
	.cfi_endproc
.LFE18677:
	.size	_ZN2v88internal14LookupIterator23InternalUpdateProtectorEv, .-_ZN2v88internal14LookupIterator23InternalUpdateProtectorEv
	.section	.text._ZN2v88internal14LookupIterator31PrepareTransitionToDataPropertyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesENS0_11StoreOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LookupIterator31PrepareTransitionToDataPropertyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesENS0_11StoreOriginE
	.type	_ZN2v88internal14LookupIterator31PrepareTransitionToDataPropertyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesENS0_11StoreOriginE, @function
_ZN2v88internal14LookupIterator31PrepareTransitionToDataPropertyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesENS0_11StoreOriginE:
.LFB18680:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$7, 4(%rdi)
	je	.L357
	cmpl	$-1, 72(%rdi)
	movq	%rdi, %rbx
	movq	%rsi, %r13
	movq	%rdx, %r14
	movl	%ecx, %r12d
	je	.L374
.L360:
	movq	24(%rbx), %rdx
	movq	0(%r13), %rax
	movq	-1(%rax), %r15
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L361
	movq	%r15, %rsi
	movl	%r8d, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-72(%rbp), %r8d
	movq	(%rax), %r15
	movq	%rax, %rsi
	movl	15(%r15), %eax
	testl	$2097152, %eax
	je	.L364
.L378:
	movl	$7, 4(%rbx)
	movq	(%rsi), %rax
	sall	$3, %r12d
	cmpw	$1025, 11(%rax)
	je	.L375
	orb	$-64, %r12b
	movq	%rsi, 40(%rbx)
	movl	%r12d, 16(%rbx)
.L357:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L376
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L377
.L363:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r15, (%rsi)
	movl	15(%r15), %eax
	testl	$2097152, %eax
	jne	.L378
.L364:
	subq	$8, %rsp
	movq	32(%rbx), %rdx
	movq	24(%rbx), %rdi
	movq	%r14, %rcx
	pushq	%r8
	movl	$1, %r9d
	movl	%r12d, %r8d
	call	_ZN2v88internal3Map24TransitionToDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesENS0_17PropertyConstnessENS0_11StoreOriginE@PLT
	movl	$7, 4(%rbx)
	movq	%rax, 40(%rbx)
	movq	(%rax), %rax
	movl	15(%rax), %edx
	popq	%rcx
	popq	%rsi
	andl	$2097152, %edx
	je	.L369
	sall	$3, %r12d
	orb	$-64, %r12b
	movl	%r12d, 16(%rbx)
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L374:
	movq	32(%rdi), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	jne	.L360
	movl	%ecx, %edx
	orl	$2, %edx
	testb	$1, 11(%rax)
	cmovne	%edx, %r12d
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L369:
	movq	39(%rax), %rdx
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	7(%rdx,%rax), %rax
	movb	$1, 8(%rbx)
	sarq	$32, %rax
	movl	%eax, 16(%rbx)
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L375:
	movq	32(%rbx), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	leaq	-60(%rbp), %rcx
	call	_ZN2v88internal14JSGlobalObject23EnsureEmptyPropertyCellENS0_6HandleIS1_EENS2_INS0_4NameEEENS0_16PropertyCellTypeEPi@PLT
	movq	24(%rbx), %rdx
	movq	%rax, %r15
	movq	0(%r13), %rax
	movq	41112(%rdx), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L366
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L367:
	movq	%r15, 40(%rbx)
	movq	24(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal18BaseNameDictionaryINS0_16GlobalDictionaryENS0_21GlobalDictionaryShapeEE20NextEnumerationIndexEPNS0_7IsolateENS0_6HandleIS2_EE@PLT
	movq	0(%r13), %rcx
	movq	%r15, %rsi
	leal	1(%rax), %edx
	sall	$8, %eax
	salq	$32, %rdx
	orl	%r12d, %eax
	movq	%rdx, 39(%rcx)
	movl	%eax, 16(%rbx)
	movq	%r14, %rdx
	movl	%eax, %ecx
	movq	24(%rbx), %rdi
	call	_ZN2v88internal12PropertyCell11UpdatedTypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEENS0_15PropertyDetailsE@PLT
	movl	%eax, %edx
	movl	16(%rbx), %eax
	sall	$6, %edx
	andb	$63, %al
	orl	%edx, %eax
	movl	%eax, 16(%rbx)
	addl	%eax, %eax
	movq	(%r15), %rdx
	sarl	%eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movb	$1, 8(%rbx)
	movl	-60(%rbp), %eax
	movl	%eax, 76(%rbx)
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L377:
	movq	%rdx, %rdi
	movl	%r8d, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-80(%rbp), %r8d
	movq	-72(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L366:
	movq	41088(%rdx), %r13
	cmpq	41096(%rdx), %r13
	je	.L379
.L368:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, 0(%r13)
	jmp	.L367
.L379:
	movq	%rdx, %rdi
	movq	%rsi, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L368
.L376:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18680:
	.size	_ZN2v88internal14LookupIterator31PrepareTransitionToDataPropertyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesENS0_11StoreOriginE, .-_ZN2v88internal14LookupIterator31PrepareTransitionToDataPropertyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesENS0_11StoreOriginE
	.section	.text._ZNK2v88internal14LookupIterator16HolderIsReceiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14LookupIterator16HolderIsReceiverEv
	.type	_ZNK2v88internal14LookupIterator16HolderIsReceiverEv, @function
_ZNK2v88internal14LookupIterator16HolderIsReceiverEv:
.LFB18685:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	testb	$2, (%rdi)
	je	.L380
	movq	48(%rdi), %rdx
	movq	56(%rdi), %rax
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	sete	%al
.L380:
	ret
	.cfi_endproc
.LFE18685:
	.size	_ZNK2v88internal14LookupIterator16HolderIsReceiverEv, .-_ZNK2v88internal14LookupIterator16HolderIsReceiverEv
	.section	.text._ZNK2v88internal14LookupIterator33HolderIsReceiverOrHiddenPrototypeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14LookupIterator33HolderIsReceiverOrHiddenPrototypeEv
	.type	_ZNK2v88internal14LookupIterator33HolderIsReceiverOrHiddenPrototypeEv, @function
_ZNK2v88internal14LookupIterator33HolderIsReceiverOrHiddenPrototypeEv:
.LFB18686:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	testb	$2, (%rdi)
	je	.L384
	movq	48(%rdi), %rdx
	movq	56(%rdi), %rcx
	movq	(%rdx), %rdx
	cmpq	%rdx, (%rcx)
	je	.L384
	testb	$1, %dl
	jne	.L386
.L387:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	movq	-1(%rdx), %rax
	cmpw	$1026, 11(%rax)
	jne	.L387
	movq	-1(%rdx), %rax
	movq	(%rcx), %rsi
	cmpq	%rsi, 23(%rax)
	sete	%al
.L384:
	ret
	.cfi_endproc
.LFE18686:
	.size	_ZNK2v88internal14LookupIterator33HolderIsReceiverOrHiddenPrototypeEv, .-_ZNK2v88internal14LookupIterator33HolderIsReceiverOrHiddenPrototypeEv
	.section	.text._ZNK2v88internal14LookupIterator10FetchValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14LookupIterator10FetchValueEv
	.type	_ZNK2v88internal14LookupIterator10FetchValueEv, @function
_ZNK2v88internal14LookupIterator10FetchValueEv:
.LFB18687:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %r8
	cmpl	$-1, 72(%rdi)
	movq	(%r8), %rax
	je	.L392
	movq	-1(%rax), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movq	%r8, %rsi
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %r9
	movl	76(%rdi), %edx
	movq	(%r9), %rax
	movq	%r9, %rdi
	movq	40(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L392:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	-1(%rax), %rdx
	cmpw	$1025, 11(%rdx)
	je	.L415
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	jne	.L416
	movl	16(%rdi), %esi
	testb	$2, %sil
	jne	.L398
	movl	76(%rdi), %edx
	movq	-1(%rax), %r11
	leal	3(%rdx,%rdx,2), %eax
	movq	39(%r11), %rdx
	sall	$3, %eax
	cltq
	movq	7(%rax,%rdx), %rdi
	movzbl	7(%r11), %ecx
	movq	%rdi, %rax
	shrq	$38, %rdi
	shrq	$51, %rax
	andl	$7, %edi
	movq	%rax, %r9
	movzbl	8(%r11), %eax
	andl	$1023, %r9d
	subl	%eax, %ecx
	cmpl	%ecx, %r9d
	setl	%al
	jl	.L417
	subl	%ecx, %r9d
	movl	$16, %r10d
	leal	16(,%r9,8), %r9d
.L400:
	cmpl	$2, %edi
	je	.L408
	cmpb	$2, %dil
	jg	.L402
	je	.L403
.L409:
	xorl	%edi, %edi
.L401:
	movzbl	%al, %edx
	movslq	%ecx, %rcx
	movslq	%r9d, %r9
	movslq	%r10d, %rax
	salq	$17, %rcx
	salq	$14, %rdx
	salq	$27, %rax
	orq	%rcx, %rdx
	shrl	$6, %esi
	orq	%r9, %rdx
	andl	$7, %esi
	orq	%rax, %rdx
	orq	%rdi, %rdx
	movq	%r8, %rdi
	call	_ZN2v88internal8JSObject14FastPropertyAtENS0_6HandleIS1_EENS0_14RepresentationENS0_10FieldIndexE@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	movq	7(%rax), %rdx
	testb	$1, %dl
	je	.L418
.L397:
	movl	76(%rdi), %eax
	leal	8(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rsi
.L394:
	movq	24(%rdi), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L405
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	subl	$3, %edi
	cmpb	$1, %dil
	jbe	.L409
.L403:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L405:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L419
.L407:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	movl	76(%rdi), %edx
	movq	7(%rax), %rax
	leal	56(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	-1(%rdx,%rax), %rax
	movq	23(%rax), %rsi
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L398:
	movq	-1(%rax), %rdx
	movl	76(%rdi), %eax
	leal	3(%rax,%rax,2), %eax
	movq	39(%rdx), %rdx
	sall	$3, %eax
	cltq
	movq	15(%rax,%rdx), %rsi
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L408:
	movl	$32768, %edi
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L418:
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %rdx
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L417:
	movzbl	8(%r11), %r10d
	movzbl	8(%r11), %edx
	addl	%edx, %r9d
	sall	$3, %r10d
	sall	$3, %r9d
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L419:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L407
	.cfi_endproc
.LFE18687:
	.size	_ZNK2v88internal14LookupIterator10FetchValueEv, .-_ZNK2v88internal14LookupIterator10FetchValueEv
	.section	.text._ZNK2v88internal14LookupIterator24IsConstFieldValueEqualToENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14LookupIterator24IsConstFieldValueEqualToENS0_6ObjectE
	.type	_ZNK2v88internal14LookupIterator24IsConstFieldValueEqualToENS0_6ObjectE, @function
_ZNK2v88internal14LookupIterator24IsConstFieldValueEqualToENS0_6ObjectE:
.LFB18688:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	56(%rdi), %r9
	movl	76(%rdi), %eax
	movq	(%r9), %r10
	leal	3(%rax,%rax,2), %eax
	movq	-1(%r10), %r12
	sall	$3, %eax
	cltq
	movq	39(%r12), %rdx
	movq	7(%rax,%rdx), %r8
	movzbl	7(%r12), %ecx
	movzbl	8(%r12), %edx
	movq	%r8, %rax
	shrq	$38, %r8
	shrq	$51, %rax
	subl	%edx, %ecx
	andl	$7, %r8d
	movq	%rax, %rbx
	andl	$1023, %ebx
	cmpl	%ecx, %ebx
	setl	%dl
	jl	.L489
	subl	%ecx, %ebx
	movl	$16, %r11d
	leal	16(,%rbx,8), %ebx
.L422:
	cmpl	$2, %r8d
	je	.L469
	cmpb	$2, %r8b
	jg	.L424
	je	.L425
.L470:
	xorl	%r8d, %r8d
.L423:
	movzbl	%dl, %eax
	movslq	%ecx, %rcx
	movl	16(%rdi), %edx
	movslq	%ebx, %rbx
	salq	$14, %rax
	salq	$17, %rcx
	movslq	%r11d, %r11
	salq	$27, %r11
	orq	%rcx, %rax
	shrl	$6, %edx
	orq	%rbx, %rax
	andl	$7, %edx
	orq	%r11, %rax
	orq	%r8, %rax
	cmpl	$2, %edx
	jne	.L426
	movq	%rsi, %rdx
	notq	%rdx
	movl	%edx, %r8d
	andl	$1, %r8d
	je	.L427
.L430:
	movq	%rax, %rdi
	movl	%eax, %ecx
	movq	-1(%r10), %rdx
	shrq	$30, %rdi
	sarl	$3, %ecx
	andl	$15, %edi
	andl	$2047, %ecx
	subl	%edi, %ecx
	testb	$64, %ah
	jne	.L490
	movq	7(%r10), %rdx
	andq	$-262144, %r10
	movq	24(%r10), %rax
	subq	$37592, %rax
	testb	$1, %dl
	je	.L445
	cmpq	288(%rax), %rdx
	jne	.L433
	.p2align 4,,10
	.p2align 3
.L445:
	movq	968(%rax), %rdx
.L433:
	leal	16(,%rcx,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
.L467:
	movq	7(%rax), %rdx
.L444:
	movabsq	$-2251799814209537, %rax
	cmpq	%rax, %rdx
	je	.L456
	testb	%r8b, %r8b
	je	.L447
	sarq	$32, %rsi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
.L448:
	movq	%rdx, %xmm0
	ucomisd	%xmm1, %xmm0
	jp	.L473
	je	.L484
.L473:
	ucomisd	%xmm1, %xmm1
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	setp	%al
	ucomisd	%xmm0, %xmm0
	setp	%dl
	andl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	subl	$3, %r8d
	cmpb	$1, %r8b
	jbe	.L470
.L425:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L427:
	movq	-1(%rsi), %rdx
	cmpw	$65, 11(%rdx)
	je	.L430
.L461:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore_state
	movl	$32768, %r8d
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L426:
	testb	$64, %ah
	je	.L451
	andl	$16376, %eax
	movq	-1(%r10,%rax), %rax
.L452:
	movq	24(%rdi), %rdx
	cmpq	%rax, 80(%rdx)
	je	.L456
	cmpq	%rsi, %rax
	je	.L456
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	je	.L457
.L460:
	testb	$1, %sil
	jne	.L491
	sarq	$32, %rsi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
.L468:
	testb	%dl, %dl
	je	.L463
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L464:
	ucomisd	%xmm1, %xmm0
	jp	.L473
	jne	.L473
.L484:
	movmskpd	%xmm0, %eax
	movmskpd	%xmm1, %edx
	popq	%rbx
	popq	%r12
	andl	$1, %eax
	andl	$1, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cmpb	%dl, %al
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L489:
	.cfi_restore_state
	movzbl	8(%r12), %r11d
	movzbl	8(%r12), %eax
	addl	%eax, %ebx
	sall	$3, %r11d
	sall	$3, %ebx
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L456:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	movq	47(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L486
	movq	%rdx, %r10
	movl	$32, %edi
	notq	%r10
	movl	%r10d, %r11d
	andl	$1, %r11d
	jne	.L435
	movslq	11(%rdx), %rdi
	sall	$3, %edi
.L435:
	cmpl	%edi, %ecx
	jnb	.L486
	testl	%ecx, %ecx
	leal	31(%rcx), %edi
	cmovns	%ecx, %edi
	sarl	$5, %edi
	andl	$1, %r10d
	jne	.L436
	cmpl	%edi, 11(%rdx)
	jle	.L436
	movl	%ecx, %r10d
	sarl	$31, %r10d
	shrl	$27, %r10d
	addl	%r10d, %ecx
	andl	$31, %ecx
	subl	%r10d, %ecx
	movl	$1, %r10d
	sall	%cl, %r10d
	movl	%r10d, %ecx
	testb	%r11b, %r11b
	je	.L492
.L440:
	sarq	$32, %rdx
	testl	%edx, %ecx
	sete	%cl
.L442:
	movq	(%r9), %r10
	movq	%rax, %rdx
	andl	$16376, %edx
	movq	-1(%rdx,%r10), %rdx
	testb	%cl, %cl
	je	.L444
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L457:
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L461
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L491:
	movq	-1(%rsi), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L461
	movsd	7(%rsi), %xmm1
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L451:
	movq	7(%r10), %rdx
	andq	$-262144, %r10
	movq	24(%r10), %rcx
	subq	$37592, %rcx
	testb	$1, %dl
	je	.L455
	cmpq	288(%rcx), %rdx
	je	.L455
.L454:
	movq	%rax, %rcx
	sarl	$3, %eax
	shrq	$30, %rcx
	andl	$2047, %eax
	andl	$15, %ecx
	subl	%ecx, %eax
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L447:
	movsd	7(%rsi), %xmm1
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L455:
	movq	968(%rcx), %rdx
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L486:
	movq	(%r9), %r10
.L443:
	andl	$16376, %eax
	movq	-1(%rax,%r10), %rax
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L463:
	movsd	7(%rax), %xmm0
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L492:
	sall	$2, %edi
	movslq	%edi, %rdi
	movl	15(%rdx,%rdi), %edx
	testl	%edx, %r10d
	sete	%cl
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L436:
	cmpl	$31, %ecx
	jg	.L438
	movl	$1, %edi
	sall	%cl, %edi
	movl	%edi, %ecx
	testb	%r11b, %r11b
	jne	.L440
.L438:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18688:
	.size	_ZNK2v88internal14LookupIterator24IsConstFieldValueEqualToENS0_6ObjectE, .-_ZNK2v88internal14LookupIterator24IsConstFieldValueEqualToENS0_6ObjectE
	.section	.text._ZNK2v88internal14LookupIterator23GetFieldDescriptorIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14LookupIterator23GetFieldDescriptorIndexEv
	.type	_ZNK2v88internal14LookupIterator23GetFieldDescriptorIndexEv, @function
_ZNK2v88internal14LookupIterator23GetFieldDescriptorIndexEv:
.LFB18689:
	.cfi_startproc
	endbr64
	movl	76(%rdi), %eax
	ret
	.cfi_endproc
.LFE18689:
	.size	_ZNK2v88internal14LookupIterator23GetFieldDescriptorIndexEv, .-_ZNK2v88internal14LookupIterator23GetFieldDescriptorIndexEv
	.section	.text._ZNK2v88internal14LookupIterator16GetAccessorIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14LookupIterator16GetAccessorIndexEv
	.type	_ZNK2v88internal14LookupIterator16GetAccessorIndexEv, @function
_ZNK2v88internal14LookupIterator16GetAccessorIndexEv:
.LFB22955:
	.cfi_startproc
	endbr64
	movl	76(%rdi), %eax
	ret
	.cfi_endproc
.LFE22955:
	.size	_ZNK2v88internal14LookupIterator16GetAccessorIndexEv, .-_ZNK2v88internal14LookupIterator16GetAccessorIndexEv
	.section	.text._ZNK2v88internal14LookupIterator16GetFieldOwnerMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14LookupIterator16GetFieldOwnerMapEv
	.type	_ZNK2v88internal14LookupIterator16GetFieldOwnerMapEv, @function
_ZNK2v88internal14LookupIterator16GetFieldOwnerMapEv:
.LFB18691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	56(%rdi), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	movq	24(%rdi), %rbx
	movl	76(%rdi), %edx
	leaq	-32(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal3Map14FindFieldOwnerEPNS0_7IsolateEi@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L496
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L497:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L501
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L502
.L498:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L502:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L498
.L501:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18691:
	.size	_ZNK2v88internal14LookupIterator16GetFieldOwnerMapEv, .-_ZNK2v88internal14LookupIterator16GetFieldOwnerMapEv
	.section	.text._ZNK2v88internal14LookupIterator13GetFieldIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14LookupIterator13GetFieldIndexEv
	.type	_ZNK2v88internal14LookupIterator13GetFieldIndexEv, @function
_ZNK2v88internal14LookupIterator13GetFieldIndexEv:
.LFB18692:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rdx
	movl	76(%rdi), %eax
	movq	(%rdx), %rdx
	leal	3(%rax,%rax,2), %eax
	sall	$3, %eax
	movq	-1(%rdx), %r9
	cltq
	movq	39(%r9), %rdx
	movq	7(%rax,%rdx), %rsi
	movzbl	7(%r9), %ecx
	movzbl	8(%r9), %edx
	movq	%rsi, %rax
	shrq	$38, %rsi
	shrq	$51, %rax
	subl	%edx, %ecx
	andl	$7, %esi
	movq	%rax, %rdi
	andl	$1023, %edi
	cmpl	%ecx, %edi
	setl	%dl
	jl	.L513
	subl	%ecx, %edi
	movl	$16, %r8d
	leal	16(,%rdi,8), %edi
.L505:
	cmpl	$2, %esi
	je	.L509
	cmpb	$2, %sil
	jg	.L507
	je	.L508
.L510:
	xorl	%esi, %esi
.L506:
	movzbl	%dl, %eax
	movslq	%ecx, %rcx
	movslq	%edi, %rdi
	movslq	%r8d, %r8
	salq	$14, %rax
	salq	$17, %rcx
	orq	%rcx, %rax
	salq	$27, %r8
	orq	%rdi, %rax
	orq	%r8, %rax
	orq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L507:
	subl	$3, %esi
	cmpb	$1, %sil
	jbe	.L510
.L508:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L509:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$32768, %esi
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L513:
	movzbl	8(%r9), %r8d
	movzbl	8(%r9), %eax
	addl	%eax, %edi
	sall	$3, %r8d
	sall	$3, %edi
	jmp	.L505
	.cfi_endproc
.LFE18692:
	.size	_ZNK2v88internal14LookupIterator13GetFieldIndexEv, .-_ZNK2v88internal14LookupIterator13GetFieldIndexEv
	.section	.text._ZNK2v88internal14LookupIterator12GetFieldTypeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14LookupIterator12GetFieldTypeEv
	.type	_ZNK2v88internal14LookupIterator12GetFieldTypeEv, @function
_ZNK2v88internal14LookupIterator12GetFieldTypeEv:
.LFB18693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	56(%rdi), %rax
	movq	24(%rdi), %rbx
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	movl	76(%rdi), %eax
	leal	3(%rax,%rax,2), %eax
	movq	39(%rdx), %rdx
	sall	$3, %eax
	cltq
	movq	15(%rax,%rdx), %rdi
	call	_ZN2v88internal3Map15UnwrapFieldTypeENS0_11MaybeObjectE@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L515
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L515:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L519
.L517:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L519:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L517
	.cfi_endproc
.LFE18693:
	.size	_ZNK2v88internal14LookupIterator12GetFieldTypeEv, .-_ZNK2v88internal14LookupIterator12GetFieldTypeEv
	.section	.text._ZNK2v88internal14LookupIterator15GetPropertyCellEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14LookupIterator15GetPropertyCellEv
	.type	_ZNK2v88internal14LookupIterator15GetPropertyCellEv, @function
_ZNK2v88internal14LookupIterator15GetPropertyCellEv:
.LFB18694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	56(%rdi), %rax
	movq	24(%rdi), %rbx
	movq	(%rax), %rdx
	movl	76(%rdi), %eax
	movq	7(%rdx), %rdx
	leal	56(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L521
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L521:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L525
.L523:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L523
	.cfi_endproc
.LFE18694:
	.size	_ZNK2v88internal14LookupIterator15GetPropertyCellEv, .-_ZNK2v88internal14LookupIterator15GetPropertyCellEv
	.section	.text._ZNK2v88internal14LookupIterator12GetAccessorsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14LookupIterator12GetAccessorsEv
	.type	_ZNK2v88internal14LookupIterator12GetAccessorsEv, @function
_ZNK2v88internal14LookupIterator12GetAccessorsEv:
.LFB18695:
	.cfi_startproc
	endbr64
	jmp	_ZNK2v88internal14LookupIterator10FetchValueEv
	.cfi_endproc
.LFE18695:
	.size	_ZNK2v88internal14LookupIterator12GetAccessorsEv, .-_ZNK2v88internal14LookupIterator12GetAccessorsEv
	.section	.text._ZNK2v88internal14LookupIterator12GetDataValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14LookupIterator12GetDataValueEv
	.type	_ZNK2v88internal14LookupIterator12GetDataValueEv, @function
_ZNK2v88internal14LookupIterator12GetDataValueEv:
.LFB18696:
	.cfi_startproc
	endbr64
	jmp	_ZNK2v88internal14LookupIterator10FetchValueEv
	.cfi_endproc
.LFE18696:
	.size	_ZNK2v88internal14LookupIterator12GetDataValueEv, .-_ZNK2v88internal14LookupIterator12GetDataValueEv
	.section	.text._ZN2v88internal14LookupIterator14WriteDataValueENS0_6HandleINS0_6ObjectEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LookupIterator14WriteDataValueENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal14LookupIterator14WriteDataValueENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal14LookupIterator14WriteDataValueENS0_6HandleINS0_6ObjectEEEb:
.LFB18697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	56(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, 72(%rdi)
	movq	(%r8), %rax
	je	.L529
	movq	-1(%rax), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movq	(%rsi), %rcx
	movq	%r8, %rsi
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %r9
	movl	76(%rdi), %edx
	movq	(%r9), %rax
	movq	%r9, %rdi
	call	*128(%rax)
.L528:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L560
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L529:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	jne	.L531
	movl	16(%rdi), %edx
	testb	$2, %dl
	jne	.L528
	movq	(%rsi), %rcx
	movl	76(%rdi), %esi
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8JSObject12WriteToFieldEiNS0_15PropertyDetailsENS0_6ObjectE
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L531:
	movq	-1(%rax), %rdx
	cmpw	$1025, 11(%rdx)
	je	.L561
	movq	7(%rax), %r12
	testb	$1, %r12b
	jne	.L537
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %r12
.L537:
	movl	76(%rdi), %eax
	movq	(%rsi), %r13
	leal	8(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	leaq	-1(%r12,%rax), %r14
	movq	%r13, (%r14)
	testb	$1, %r13b
	je	.L528
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L539
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L539:
	testb	$24, %al
	je	.L528
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L528
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L561:
	movl	76(%rdi), %edx
	movq	7(%rax), %rax
	leal	56(,%rdx,8), %edx
	movslq	%edx, %rdx
	movq	-1(%rdx,%rax), %r13
	movq	(%rsi), %r12
	movq	%r12, 23(%r13)
	leaq	23(%r13), %r14
	testb	$1, %r12b
	je	.L528
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L534
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L534:
	testb	$24, %al
	je	.L528
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L528
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L528
.L560:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18697:
	.size	_ZN2v88internal14LookupIterator14WriteDataValueENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal14LookupIterator14WriteDataValueENS0_6HandleINS0_6ObjectEEEb
	.section	.text._ZNK2v88internal14LookupIterator8NotFoundENS0_10JSReceiverE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14LookupIterator8NotFoundENS0_10JSReceiverE
	.type	_ZNK2v88internal14LookupIterator8NotFoundENS0_10JSReceiverE, @function
_ZNK2v88internal14LookupIterator8NotFoundENS0_10JSReceiverE:
.LFB18700:
	.cfi_startproc
	endbr64
	movq	-1(%rsi), %rax
	cmpw	$1086, 11(%rax)
	je	.L563
.L565:
	movl	$4, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L563:
	movq	32(%rdi), %rax
	movq	(%rax), %rdi
	movq	-1(%rdi), %rax
	cmpw	$63, 11(%rax)
	ja	.L565
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal14IsSpecialIndexENS0_6StringE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
	addl	$1, %eax
	ret
	.cfi_endproc
.LFE18700:
	.size	_ZNK2v88internal14LookupIterator8NotFoundENS0_10JSReceiverE, .-_ZNK2v88internal14LookupIterator8NotFoundENS0_10JSReceiverE
	.section	.text._ZNK2v88internal14LookupIterator34GetInterceptorForFailedAccessCheckEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14LookupIterator34GetInterceptorForFailedAccessCheckEv
	.type	_ZNK2v88internal14LookupIterator34GetInterceptorForFailedAccessCheckEv, @function
_ZNK2v88internal14LookupIterator34GetInterceptorForFailedAccessCheckEv:
.LFB18704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	56(%rdi), %rsi
	movq	24(%rdi), %rdi
	call	_ZN2v88internal15AccessCheckInfo3GetEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEE@PLT
	testq	%rax, %rax
	je	.L572
	cmpl	$-1, 72(%rbx)
	je	.L573
	movq	23(%rax), %rsi
.L574:
	testq	%rsi, %rsi
	je	.L572
	movq	24(%rbx), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L575
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L573:
	.cfi_restore_state
	movq	15(%rax), %rsi
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L575:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L586
.L577:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L586:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L577
	.cfi_endproc
.LFE18704:
	.size	_ZNK2v88internal14LookupIterator34GetInterceptorForFailedAccessCheckEv, .-_ZNK2v88internal14LookupIterator34GetInterceptorForFailedAccessCheckEv
	.section	.text._ZN2v88internal14LookupIterator21LookupInSpecialHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE,"axG",@progbits,_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	.type	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE, @function
_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE:
.LFB20641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	4(%rdi), %eax
	movq	%rdi, %rbx
	cmpl	$4, %eax
	je	.L588
	ja	.L589
	testl	%eax, %eax
	je	.L590
	cmpl	$2, %eax
	jne	.L592
.L630:
	movl	12(%rbx), %eax
	cmpl	$2, %eax
	je	.L617
.L607:
	movq	-1(%r12), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movq	24(%rbx), %rsi
	movq	15(%r12), %rcx
	movzbl	14(%rax), %eax
	movl	72(%rbx), %r8d
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %r14
	movq	%r12, %rdx
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*264(%rax)
	movl	%eax, 76(%rbx)
	movl	%eax, %edx
	cmpl	$-1, %eax
	je	.L631
	movq	(%r14), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*272(%rax)
	movl	%eax, 16(%rbx)
	movzbl	14(%r13), %edx
	shrl	$3, %edx
	leal	-10(%rdx), %ecx
	cmpb	$1, %cl
	jbe	.L632
	subl	$8, %edx
	cmpb	$1, %dl
	jbe	.L633
.L612:
	andl	$1, %eax
	movb	$1, 8(%rbx)
	movl	%eax, %edx
	movl	$6, %eax
	subl	%edx, %eax
.L635:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L589:
	.cfi_restore_state
	subl	$5, %eax
	cmpl	$1, %eax
	ja	.L592
.L617:
	movl	$4, %eax
.L587:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L588:
	.cfi_restore_state
	cmpw	$1024, 11(%rsi)
	je	.L614
	xorl	%eax, %eax
	testb	$32, 13(%rsi)
	jne	.L587
.L590:
	testb	$1, (%rbx)
	je	.L630
	testb	$8, 13(%r13)
	je	.L630
	movq	-1(%r12), %rax
.L628:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L634
.L600:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	cmpq	%rax, %rdx
	je	.L601
	movq	39(%rdx), %rax
.L601:
	movl	12(%rbx), %edx
	testb	$4, 75(%rax)
	je	.L602
	cmpl	$1, %edx
	je	.L607
	movl	$2, %eax
	cmpl	$2, %edx
	je	.L587
	testl	%edx, %edx
	jne	.L587
	movl	$1, 12(%rbx)
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L633:
	orl	$32, %eax
	movl	%eax, 16(%rbx)
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L632:
	orl	$40, %eax
	movb	$1, 8(%rbx)
	movl	%eax, 16(%rbx)
	andl	$1, %eax
	movl	%eax, %edx
	movl	$6, %eax
	subl	%edx, %eax
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L631:
	movq	-1(%r12), %rax
	cmpw	$1086, 11(%rax)
	setne	%al
	movzbl	%al, %eax
	leal	1(%rax,%rax,2), %eax
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L602:
	xorl	%eax, %eax
	cmpl	$2, %edx
	sete	%al
	leal	2(%rax,%rax), %eax
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L634:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L628
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L600
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L600
.L592:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L614:
	movl	$3, %eax
	jmp	.L587
	.cfi_endproc
.LFE20641:
	.size	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE, .-_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	.section	.text._ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi,"axG",@progbits,_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi,comdat
	.p2align 4
	.weak	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi
	.type	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi, @function
_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi:
.LFB21110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rbx
	movswl	9(%rbx), %r10d
	movl	7(%rsi), %r9d
	subl	$1, %r10d
	je	.L647
	movl	%r10d, %r12d
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L638:
	movl	%r12d, %ecx
	subl	%r8d, %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	addl	%r8d, %eax
	leal	1(%rax), %r11d
	leal	(%r11,%r11,2), %ecx
	sall	$3, %ecx
	movslq	%ecx, %rcx
	movq	7(%rcx,%rbx), %rcx
	shrq	$41, %rcx
	andl	$1023, %ecx
	leal	3(%rcx,%rcx,2), %ecx
	sall	$3, %ecx
	movslq	%ecx, %rcx
	movq	-1(%rcx,%rbx), %rcx
	cmpl	7(%rcx), %r9d
	ja	.L652
	cmpl	%r8d, %eax
	je	.L637
	movl	%eax, %r12d
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L652:
	cmpl	%r12d, %r11d
	je	.L648
	movl	%r11d, %r8d
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L647:
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L637:
	leal	3(%r8,%r8,2), %r11d
	sall	$3, %r11d
	movslq	%r11d, %r11
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L643:
	movq	(%rdi), %rbx
	addl	$1, %r8d
	movq	7(%r11,%rbx), %rax
	shrq	$41, %rax
	andl	$1023, %eax
	leal	3(%rax,%rax,2), %ecx
	sall	$3, %ecx
	movslq	%ecx, %rcx
	movq	-1(%rcx,%rbx), %rcx
	cmpl	7(%rcx), %r9d
	jne	.L645
	addq	$24, %r11
	cmpq	%rcx, %rsi
	je	.L653
.L646:
	cmpl	%r10d, %r8d
	jle	.L643
.L645:
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L648:
	.cfi_restore_state
	movl	%r12d, %r8d
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L653:
	cmpl	%eax, %edx
	jle	.L645
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21110:
	.size	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi, .-_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi
	.section	.text._ZN2v88internal14LookupIterator21LookupInRegularHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE,"axG",@progbits,_ZN2v88internal14LookupIterator21LookupInRegularHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	.type	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE, @function
_ZN2v88internal14LookupIterator21LookupInRegularHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE:
.LFB20686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 12(%rdi)
	movl	$4, %eax
	je	.L654
	movl	15(%rsi), %eax
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r13
	testl	$2097152, %eax
	jne	.L656
	movq	39(%rsi), %rax
	movq	24(%rdi), %rcx
	movq	%rax, -64(%rbp)
	movq	32(%rdi), %rax
	movq	(%rax), %r14
	movl	15(%rsi), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	je	.L682
	movl	7(%r14), %eax
	movl	%esi, %r8d
	movq	41080(%rcx), %r15
	shrl	$3, %r8d
	xorl	%r8d, %eax
	andl	$63, %eax
	movq	%rax, %rcx
	salq	$4, %rcx
	cmpq	%rsi, (%r15,%rcx)
	jne	.L660
	cmpq	8(%r15,%rcx), %r14
	jne	.L660
	movl	1024(%r15,%rax,4), %eax
	cmpl	$-2, %eax
	je	.L660
.L661:
	cmpl	$-1, %eax
	jne	.L700
.L682:
	movq	-1(%r13), %rax
	cmpw	$1086, 11(%rax)
	je	.L679
.L681:
	movl	$4, %eax
.L654:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L701
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L656:
	.cfi_restore_state
	movq	7(%rdx), %r14
	testb	$1, %r14b
	jne	.L672
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %r14
.L672:
	movq	32(%rbx), %r12
	movq	24(%rbx), %r15
	movq	(%r12), %rdx
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L673
	shrl	$2, %eax
.L674:
	movslq	35(%r14), %rdi
	movq	88(%r15), %r8
	subq	$1, %r14
	movl	$1, %ecx
	subl	$1, %edi
	andl	%edi, %eax
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L702:
	cmpq	(%r12), %rdx
	je	.L676
	addl	%ecx, %eax
	addl	$1, %ecx
	andl	%edi, %eax
.L677:
	leal	(%rax,%rax,2), %esi
	leal	56(,%rsi,8), %edx
	movslq	%edx, %rdx
	movq	(%rdx,%r14), %rdx
	cmpq	%rdx, %r8
	jne	.L702
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L673:
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L700:
	movl	%eax, 76(%rbx)
	leal	3(%rax,%rax,2), %eax
	movq	-64(%rbp), %rdx
	sall	$3, %eax
	cltq
	movq	7(%rax,%rdx), %rax
	sarq	$32, %rax
	movl	%eax, 16(%rbx)
	movl	%eax, %edx
.L670:
	andl	$1, %edx
	movl	$6, %eax
	movb	$1, 8(%rbx)
	subl	%edx, %eax
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L679:
	movq	32(%rbx), %rax
	movq	(%rax), %rdi
	movq	-1(%rdi), %rax
	cmpw	$63, 11(%rax)
	ja	.L681
	call	_ZN2v88internal14IsSpecialIndexENS0_6StringE@PLT
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
	addl	$1, %eax
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L676:
	cmpl	$-1, %eax
	je	.L682
	movl	%eax, 76(%rbx)
	leal	72(,%rsi,8), %eax
	cltq
	movq	(%rax,%r14), %rax
	sarq	$32, %rax
	movl	%eax, 16(%rbx)
	movl	%eax, %edx
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L660:
	cmpl	$8, %edx
	jg	.L703
	movq	-64(%rbp), %r9
	movl	$24, %esi
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L662:
	movq	-1(%rsi,%r9), %rdi
	movl	%ecx, %eax
	leal	1(%rcx), %ecx
	cmpq	%rdi, %r14
	je	.L666
	addq	$24, %rsi
	cmpl	%ecx, %edx
	jne	.L662
	movl	$-1, %eax
.L666:
	xorl	7(%r14), %r8d
	movq	%r12, %xmm0
	movq	%r14, %xmm1
	andl	$63, %r8d
	punpcklqdq	%xmm1, %xmm0
	movq	%r8, %rdx
	salq	$4, %rdx
	movups	%xmm0, (%r15,%rdx)
	movl	%eax, 1024(%r15,%r8,4)
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L703:
	leaq	-64(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movl	%r8d, -68(%rbp)
	call	_ZN2v88internal12BinarySearchILNS0_10SearchModeE1ENS0_15DescriptorArrayEEEiPT0_NS0_4NameEiPi
	movl	-68(%rbp), %r8d
	jmp	.L666
.L701:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20686:
	.size	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE, .-_ZN2v88internal14LookupIterator21LookupInRegularHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	.section	.text._ZN2v88internal14LookupIterator21LookupInRegularHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE,"axG",@progbits,_ZN2v88internal14LookupIterator21LookupInRegularHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	.type	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE, @function
_ZN2v88internal14LookupIterator21LookupInRegularHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE:
.LFB21772:
	.cfi_startproc
	endbr64
	cmpl	$2, 12(%rdi)
	movl	$4, %eax
	je	.L713
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	-1(%rdx), %rax
	movq	%rdi, %rbx
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movq	24(%rdi), %rsi
	movzbl	14(%rax), %eax
	movl	72(%rdi), %r8d
	movq	15(%r12), %rcx
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %r14
	movq	%r12, %rdx
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*264(%rax)
	movl	%eax, 76(%rbx)
	movl	%eax, %edx
	cmpl	$-1, %eax
	je	.L716
	movq	(%r14), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*272(%rax)
	movl	%eax, 16(%rbx)
	movzbl	14(%r13), %edx
	shrl	$3, %edx
	leal	-10(%rdx), %ecx
	cmpb	$1, %cl
	jbe	.L717
	subl	$8, %edx
	cmpb	$1, %dl
	ja	.L709
	orl	$32, %eax
	movl	%eax, 16(%rbx)
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L717:
	orl	$40, %eax
	movl	%eax, 16(%rbx)
.L709:
	andl	$1, %eax
	movl	$6, %edx
	movb	$1, 8(%rbx)
	popq	%rbx
	subl	%eax, %edx
	popq	%r12
	popq	%r13
	movl	%edx, %eax
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L713:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L716:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	-1(%r12), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	cmpw	$1086, 11(%rax)
	popq	%r14
	setne	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	leal	1(%rax,%rax,2), %eax
	ret
	.cfi_endproc
.LFE21772:
	.size	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE, .-_ZN2v88internal14LookupIterator21LookupInRegularHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	.section	.text._ZN2v88internal14LookupIterator12NextInternalILb1EEEvNS0_3MapENS0_10JSReceiverE,"axG",@progbits,_ZN2v88internal14LookupIterator12NextInternalILb1EEEvNS0_3MapENS0_10JSReceiverE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LookupIterator12NextInternalILb1EEEvNS0_3MapENS0_10JSReceiverE
	.type	_ZN2v88internal14LookupIterator12NextInternalILb1EEEvNS0_3MapENS0_10JSReceiverE, @function
_ZN2v88internal14LookupIterator12NextInternalILb1EEEvNS0_3MapENS0_10JSReceiverE:
.LFB20643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rdi), %r14
	movq	%rsi, %rbx
	movq	23(%rsi), %r12
	movq	%rdx, %rsi
	cmpq	104(%r14), %r12
	je	.L789
	.p2align 4,,10
	.p2align 3
.L720:
	movl	0(%r13), %ecx
	testb	$2, %cl
	jne	.L721
	cmpw	$1026, 11(%rbx)
	je	.L721
.L759:
	movq	%rsi, %r12
.L719:
	cmpl	$1, 12(%r13)
	je	.L790
.L723:
	movq	56(%r13), %rax
	movl	$4, 4(%r13)
	cmpq	%r12, (%rax)
	je	.L718
.L752:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L754
.L793:
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L755:
	movq	%rax, 56(%r13)
.L718:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L721:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L759
	movq	-1(%r12), %rbx
	leaq	-1(%r12), %rsi
	movzwl	11(%rbx), %edx
	cmpw	$1040, %dx
	ja	.L728
	movl	4(%r13), %eax
	cmpl	$4, %eax
	je	.L729
	ja	.L730
	testl	%eax, %eax
	jne	.L791
.L731:
	andl	$1, %ecx
	je	.L728
	testb	$8, 13(%rbx)
	jne	.L792
	.p2align 4,,10
	.p2align 3
.L728:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	movq	24(%r13), %r14
	movl	%eax, 4(%r13)
	cmpl	$4, %eax
	jne	.L752
.L753:
	movq	23(%rbx), %rax
	movq	%r12, %rsi
	cmpq	%rax, 104(%r14)
	je	.L719
	movq	%rax, %r12
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L791:
	cmpl	$2, %eax
	je	.L728
.L733:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L730:
	subl	$5, %eax
	cmpl	$1, %eax
	ja	.L733
	movl	$4, 4(%r13)
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L729:
	cmpw	$1024, %dx
	je	.L735
	testb	$32, 13(%rbx)
	je	.L731
	movl	$0, 4(%r13)
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L793
.L754:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L794
.L756:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%r12, (%rax)
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L789:
	cmpl	$1, 12(%r13)
	movq	%rdx, %r12
	jne	.L723
.L790:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$2, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14LookupIterator15RestartInternalILb1EEEvNS1_16InterceptorStateE
	.p2align 4,,10
	.p2align 3
.L792:
	.cfi_restore_state
	movq	(%rsi), %rax
.L784:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L795
.L742:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	cmpq	%rax, %rdx
	je	.L743
	movq	39(%rdx), %rax
.L743:
	movl	12(%r13), %edx
	testb	$4, 75(%rax)
	je	.L744
	cmpl	$1, %edx
	je	.L728
	cmpl	$2, %edx
	je	.L749
	testl	%edx, %edx
	je	.L796
.L749:
	movl	$2, 4(%r13)
	movq	24(%r13), %r14
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L744:
	cmpl	$2, %edx
	je	.L728
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L735:
	movl	$3, 4(%r13)
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L793
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L795:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L784
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L742
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L742
.L794:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L756
.L796:
	movl	$1, 12(%r13)
	jmp	.L728
	.cfi_endproc
.LFE20643:
	.size	_ZN2v88internal14LookupIterator12NextInternalILb1EEEvNS0_3MapENS0_10JSReceiverE, .-_ZN2v88internal14LookupIterator12NextInternalILb1EEEvNS0_3MapENS0_10JSReceiverE
	.section	.text._ZN2v88internal14LookupIterator5StartILb1EEEvv,"axG",@progbits,_ZN2v88internal14LookupIterator5StartILb1EEEvv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LookupIterator5StartILb1EEEvv
	.type	_ZN2v88internal14LookupIterator5StartILb1EEEvv, @function
_ZN2v88internal14LookupIterator5StartILb1EEEvv:
.LFB19346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	64(%rdi), %rax
	movb	$0, 8(%rdi)
	movl	$4, 4(%rdi)
	movq	%rax, 56(%rdi)
	movq	(%rax), %r14
	movq	-1(%r14), %r13
	movq	%r14, %rdx
	cmpw	$1040, 11(%r13)
	movq	%r13, %rsi
	ja	.L798
	call	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	movl	%eax, 4(%r12)
	cmpl	$4, %eax
	je	.L802
.L797:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L798:
	.cfi_restore_state
	call	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	movl	%eax, 4(%r12)
	cmpl	$4, %eax
	jne	.L797
.L802:
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14LookupIterator12NextInternalILb1EEEvNS0_3MapENS0_10JSReceiverE
	.cfi_endproc
.LFE19346:
	.size	_ZN2v88internal14LookupIterator5StartILb1EEEvv, .-_ZN2v88internal14LookupIterator5StartILb1EEEvv
	.section	.text._ZN2v88internal14LookupIterator15RestartInternalILb1EEEvNS1_16InterceptorStateE,"axG",@progbits,_ZN2v88internal14LookupIterator15RestartInternalILb1EEEvNS1_16InterceptorStateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LookupIterator15RestartInternalILb1EEEvNS1_16InterceptorStateE
	.type	_ZN2v88internal14LookupIterator15RestartInternalILb1EEEvNS1_16InterceptorStateE, @function
_ZN2v88internal14LookupIterator15RestartInternalILb1EEEvNS1_16InterceptorStateE:
.LFB19347:
	.cfi_startproc
	endbr64
	movl	%esi, 12(%rdi)
	movl	$192, 16(%rdi)
	movl	$-1, 76(%rdi)
	jmp	_ZN2v88internal14LookupIterator5StartILb1EEEvv
	.cfi_endproc
.LFE19347:
	.size	_ZN2v88internal14LookupIterator15RestartInternalILb1EEEvNS1_16InterceptorStateE, .-_ZN2v88internal14LookupIterator15RestartInternalILb1EEEvNS1_16InterceptorStateE
	.section	.text._ZN2v88internal14LookupIterator15SkipInterceptorILb0EEEbNS0_8JSObjectE,"axG",@progbits,_ZN2v88internal14LookupIterator15SkipInterceptorILb0EEEbNS0_8JSObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LookupIterator15SkipInterceptorILb0EEEbNS0_8JSObjectE
	.type	_ZN2v88internal14LookupIterator15SkipInterceptorILb0EEEbNS0_8JSObjectE, @function
_ZN2v88internal14LookupIterator15SkipInterceptorILb0EEEbNS0_8JSObjectE:
.LFB21774:
	.cfi_startproc
	endbr64
	movq	-1(%rsi), %rax
.L823:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L824
.L808:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	cmpq	%rax, %rdx
	je	.L809
	movq	31(%rdx), %rax
.L809:
	movq	32(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	-1(%rdx), %rdx
	movslq	75(%rax), %rax
	cmpw	$64, 11(%rdx)
	je	.L825
.L811:
	sarl	$2, %eax
	movl	12(%rdi), %edx
	andl	$1, %eax
	je	.L813
	cmpl	$1, %edx
	je	.L804
	cmpl	$2, %edx
	je	.L815
	testl	%edx, %edx
	je	.L826
.L813:
	cmpl	$2, %edx
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L804:
	ret
	.p2align 4,,10
	.p2align 3
.L824:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L823
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L808
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L825:
	testb	$1, %al
	jne	.L811
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L826:
	movl	$1, 12(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L815:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE21774:
	.size	_ZN2v88internal14LookupIterator15SkipInterceptorILb0EEEbNS0_8JSObjectE, .-_ZN2v88internal14LookupIterator15SkipInterceptorILb0EEEbNS0_8JSObjectE
	.section	.text._ZN2v88internal14LookupIterator21LookupInSpecialHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE,"axG",@progbits,_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	.type	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE, @function
_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE:
.LFB20642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	4(%rdi), %eax
	cmpl	$4, %eax
	je	.L828
	ja	.L829
	testl	%eax, %eax
	je	.L830
	cmpl	$2, %eax
	jne	.L832
.L831:
	cmpw	$1025, 11(%r13)
	je	.L869
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L829:
	subl	$5, %eax
	cmpl	$1, %eax
	ja	.L832
.L846:
	movl	$4, %eax
.L827:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L870
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L869:
	.cfi_restore_state
	movq	32(%r12), %r13
	movq	7(%rdx), %rbx
	movq	24(%r12), %r14
	movq	0(%r13), %rdx
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L844
	shrl	$2, %eax
.L845:
	movslq	35(%rbx), %r8
	movq	88(%r14), %r9
	leaq	-1(%rbx), %rdi
	movl	$1, %ecx
	subl	$1, %r8d
	andl	%r8d, %eax
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L871:
	addl	%ecx, %eax
	addl	$1, %ecx
	andl	%r8d, %eax
.L848:
	leal	56(,%rax,8), %edx
	movslq	%edx, %rdx
	addq	%rdi, %rdx
	movq	(%rdx), %rsi
	cmpq	%rsi, %r9
	je	.L846
	movq	0(%r13), %rbx
	cmpq	%rbx, 7(%rsi)
	jne	.L871
	cmpl	$-1, %eax
	je	.L846
	movl	%eax, 76(%r12)
	movq	(%rdx), %rax
	movq	24(%r12), %rdx
	movq	23(%rax), %rbx
	cmpq	%rbx, 96(%rdx)
	je	.L846
	movslq	19(%rax), %rax
	movb	$1, 8(%r12)
	movl	%eax, %edx
	movl	%eax, 16(%r12)
	movl	$6, %eax
	andl	$1, %edx
	subl	%edx, %eax
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L828:
	cmpw	$1024, 11(%rsi)
	je	.L872
.L834:
	testb	$32, 13(%r13)
	jne	.L873
.L830:
	testb	$1, (%r12)
	je	.L831
	testb	$4, 13(%r13)
	je	.L831
	movq	%rdx, %rsi
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal14LookupIterator15SkipInterceptorILb0EEEbNS0_8JSObjectE
	movq	-56(%rbp), %rdx
	testb	%al, %al
	jne	.L831
	movq	32(%r12), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	je	.L874
.L841:
	movl	$2, %eax
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L844:
	leaq	-48(%rbp), %rdi
	movq	%rdx, -48(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L873:
	movq	32(%r12), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	je	.L875
	xorl	%eax, %eax
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L872:
	movq	32(%rdi), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	je	.L835
.L836:
	movl	$3, %eax
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L875:
	testb	$1, 11(%rax)
	jne	.L830
	xorl	%eax, %eax
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L835:
	testb	$1, 11(%rax)
	je	.L836
	jmp	.L834
.L874:
	testb	$1, 11(%rax)
	jne	.L831
	jmp	.L841
.L832:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L870:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20642:
	.size	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE, .-_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	.section	.text._ZN2v88internal14LookupIterator12NextInternalILb0EEEvNS0_3MapENS0_10JSReceiverE,"axG",@progbits,_ZN2v88internal14LookupIterator12NextInternalILb0EEEvNS0_3MapENS0_10JSReceiverE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LookupIterator12NextInternalILb0EEEvNS0_3MapENS0_10JSReceiverE
	.type	_ZN2v88internal14LookupIterator12NextInternalILb0EEEvNS0_3MapENS0_10JSReceiverE, @function
_ZN2v88internal14LookupIterator12NextInternalILb0EEEvNS0_3MapENS0_10JSReceiverE:
.LFB20644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$16, %rsp
	.p2align 4,,10
	.p2align 3
.L890:
	movq	24(%r13), %r14
	movq	23(%rbx), %r12
	cmpq	%r12, 104(%r14)
	je	.L881
	testb	$2, 0(%r13)
	jne	.L880
	cmpw	$1026, 11(%rbx)
	je	.L880
.L881:
	cmpl	$1, 12(%r13)
	je	.L904
	movq	56(%r13), %rax
	movl	$4, 4(%r13)
	cmpq	(%rax), %rsi
	je	.L876
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L903
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L905
.L886:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	movq	%rax, 56(%r13)
.L876:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L880:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L881
	movq	-1(%r12), %rbx
	movq	%r12, %rdx
	movq	%r13, %rdi
	cmpw	$1040, 11(%rbx)
	movq	%rbx, %rsi
	ja	.L887
	call	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	movl	%eax, 4(%r13)
	cmpl	$4, %eax
	jne	.L889
.L888:
	movq	%r12, %rsi
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L887:
	call	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	movl	%eax, 4(%r13)
	cmpl	$4, %eax
	je	.L888
.L889:
	movq	24(%r13), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L891
	movq	%r12, %rsi
.L903:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L892:
	movq	%rax, 56(%r13)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L891:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L906
.L893:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r12, (%rax)
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L904:
	addq	$16, %rsp
	movq	%r13, %rdi
	movl	$2, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14LookupIterator15RestartInternalILb0EEEvNS1_16InterceptorStateE
.L906:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L893
.L905:
	movq	%r14, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L886
	.cfi_endproc
.LFE20644:
	.size	_ZN2v88internal14LookupIterator12NextInternalILb0EEEvNS0_3MapENS0_10JSReceiverE, .-_ZN2v88internal14LookupIterator12NextInternalILb0EEEvNS0_3MapENS0_10JSReceiverE
	.section	.text._ZN2v88internal14LookupIterator5StartILb0EEEvv,"axG",@progbits,_ZN2v88internal14LookupIterator5StartILb0EEEvv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LookupIterator5StartILb0EEEvv
	.type	_ZN2v88internal14LookupIterator5StartILb0EEEvv, @function
_ZN2v88internal14LookupIterator5StartILb0EEEvv:
.LFB20517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	64(%rdi), %rax
	movb	$0, 8(%rdi)
	movl	$4, 4(%rdi)
	movq	%rax, 56(%rdi)
	movq	(%rax), %r14
	movq	-1(%r14), %r13
	movq	%r14, %rdx
	cmpw	$1040, 11(%r13)
	movq	%r13, %rsi
	ja	.L908
	call	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	movl	%eax, 4(%r12)
	cmpl	$4, %eax
	je	.L912
.L907:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L908:
	.cfi_restore_state
	call	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	movl	%eax, 4(%r12)
	cmpl	$4, %eax
	jne	.L907
.L912:
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14LookupIterator12NextInternalILb0EEEvNS0_3MapENS0_10JSReceiverE
	.cfi_endproc
.LFE20517:
	.size	_ZN2v88internal14LookupIterator5StartILb0EEEvv, .-_ZN2v88internal14LookupIterator5StartILb0EEEvv
	.section	.text._ZN2v88internal14LookupIterator15RestartInternalILb0EEEvNS1_16InterceptorStateE,"axG",@progbits,_ZN2v88internal14LookupIterator15RestartInternalILb0EEEvNS1_16InterceptorStateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LookupIterator15RestartInternalILb0EEEvNS1_16InterceptorStateE
	.type	_ZN2v88internal14LookupIterator15RestartInternalILb0EEEvNS1_16InterceptorStateE, @function
_ZN2v88internal14LookupIterator15RestartInternalILb0EEEvNS1_16InterceptorStateE:
.LFB19348:
	.cfi_startproc
	endbr64
	movl	%esi, 12(%rdi)
	movl	$192, 16(%rdi)
	movl	$-1, 76(%rdi)
	jmp	_ZN2v88internal14LookupIterator5StartILb0EEEvv
	.cfi_endproc
.LFE19348:
	.size	_ZN2v88internal14LookupIterator15RestartInternalILb0EEEvNS1_16InterceptorStateE, .-_ZN2v88internal14LookupIterator15RestartInternalILb0EEEvNS1_16InterceptorStateE
	.section	.rodata._ZN2v88internal14LookupIterator20LookupCachedPropertyEv.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"state() == LookupIterator::DATA"
	.section	.text._ZN2v88internal14LookupIterator20LookupCachedPropertyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LookupIterator20LookupCachedPropertyEv
	.type	_ZN2v88internal14LookupIterator20LookupCachedPropertyEv, @function
_ZN2v88internal14LookupIterator20LookupCachedPropertyEv:
.LFB18706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZNK2v88internal14LookupIterator10FetchValueEv
	movq	24(%rbx), %r12
	movq	(%rax), %rax
	movq	41112(%r12), %rdi
	movq	7(%rax), %r13
	testq	%rdi, %rdi
	je	.L915
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	24(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal20FunctionTemplateInfo24TryGetCachedPropertyNameEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L921
.L926:
	xorl	%esi, %esi
	cmpl	$-1, 72(%rbx)
	movq	%rax, 32(%rbx)
	movq	%rbx, %rdi
	je	.L919
	call	_ZN2v88internal14LookupIterator15RestartInternalILb1EEEvNS1_16InterceptorStateE
.L920:
	cmpl	$6, 4(%rbx)
	movl	$1, %eax
	jne	.L924
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L919:
	.cfi_restore_state
	call	_ZN2v88internal14LookupIterator15RestartInternalILb0EEEvNS1_16InterceptorStateE
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L915:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L925
.L917:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	movq	24(%rbx), %rdi
	call	_ZN2v88internal20FunctionTemplateInfo24TryGetCachedPropertyNameEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	testq	%rax, %rax
	jne	.L926
.L921:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L925:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L924:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18706:
	.size	_ZN2v88internal14LookupIterator20LookupCachedPropertyEv, .-_ZN2v88internal14LookupIterator20LookupCachedPropertyEv
	.section	.text._ZN2v88internal14LookupIterator23TryLookupCachedPropertyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LookupIterator23TryLookupCachedPropertyEv
	.type	_ZN2v88internal14LookupIterator23TryLookupCachedPropertyEv, @function
_ZN2v88internal14LookupIterator23TryLookupCachedPropertyEv:
.LFB18705:
	.cfi_startproc
	endbr64
	cmpl	$5, 4(%rdi)
	je	.L935
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L935:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZNK2v88internal14LookupIterator10FetchValueEv
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L936
.L929:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L936:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$79, 11(%rax)
	jne	.L929
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14LookupIterator20LookupCachedPropertyEv
	.cfi_endproc
.LFE18705:
	.size	_ZN2v88internal14LookupIterator23TryLookupCachedPropertyEv, .-_ZN2v88internal14LookupIterator23TryLookupCachedPropertyEv
	.section	.text._ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE
	.type	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE, @function
_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE:
.LFB18661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rcx, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$136, %rsp
	movq	(%rcx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -148(%rbp)
	testb	$1, %dl
	jne	.L938
	sarq	$32, %rdx
	js	.L955
	movl	%edx, -148(%rbp)
	movl	%edx, %ecx
.L940:
	movb	$1, (%r8)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L948
.L950:
	movl	%ecx, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movl	%ecx, -168(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj
	movl	-168(%rbp), %ecx
.L949:
	movabsq	$824633720832, %rdi
	movl	%r14d, (%r12)
	movq	%rdi, 12(%r12)
	movq	%r12, %rdi
	movq	%r13, 24(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	%rbx, 48(%r12)
	movq	$0, 56(%r12)
	movq	%rax, 64(%r12)
	movl	%ecx, 72(%r12)
	movl	$-1, 76(%r12)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv
.L937:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L991
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L938:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	movq	%rcx, %r15
	cmpw	$65, 11(%rax)
	jne	.L990
	movsd	7(%rdx), %xmm0
	movsd	.LC4(%rip), %xmm2
	addsd	%xmm0, %xmm2
	movq	%xmm2, %rcx
	movq	%xmm2, %rax
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	jne	.L990
	movl	%eax, %edi
	pxor	%xmm1, %xmm1
	movd	%xmm2, %ecx
	movd	%xmm2, -148(%rbp)
	cvtsi2sdq	%rdi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L977
	jne	.L977
	cmpl	$-1, %eax
	jne	.L940
.L990:
	xorl	%eax, %eax
.L942:
	testb	%al, %al
	jne	.L955
	movq	-1(%rdx), %rax
	cmpw	$64, 11(%rax)
	jbe	.L954
	.p2align 4,,10
	.p2align 3
.L955:
	movq	%r13, %rdi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal6Object13ConvertToNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L992
.L954:
	movb	$1, (%r8)
	movq	(%r15), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L993
.L963:
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L969
.L971:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj
	movq	%rax, %rdx
.L970:
	movq	(%r15), %rax
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L972
	testb	$1, 11(%rax)
	movl	$0, %eax
	cmovne	%eax, %r14d
.L972:
	movabsq	$824633720832, %rax
	movl	%r14d, (%r12)
	movq	%rax, 12(%r12)
	movq	%r13, 24(%r12)
	movq	(%r15), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L994
.L973:
	movq	%r15, 32(%r12)
	movq	%r12, %rdi
	movq	$0, 40(%r12)
	movq	%rbx, 48(%r12)
	movq	$0, 56(%r12)
	movq	%rdx, 64(%r12)
	movq	$-1, 72(%r12)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L992:
	movb	$0, (%r8)
	movq	(%rbx), %rax
	leaq	128(%r13), %r15
	testb	$1, %al
	jne	.L958
.L960:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj
	movq	%rax, %r14
.L959:
	movq	128(%r13), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L961
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L961:
	movl	%eax, (%r12)
	movabsq	$824633720832, %rax
	movq	%rax, 12(%r12)
	movq	128(%r13), %rax
	movq	%r13, 24(%r12)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L995
.L962:
	movq	%r15, 32(%r12)
	movq	%r12, %rdi
	movq	$0, 40(%r12)
	movq	%rbx, 48(%r12)
	movq	$0, 56(%r12)
	movq	%r14, 64(%r12)
	movq	$-1, 72(%r12)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L993:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L964
	testb	$2, %al
	jne	.L963
.L964:
	leaq	-144(%rbp), %r8
	leaq	-148(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L963
	movq	(%rbx), %rax
	movl	-148(%rbp), %edx
	movq	-168(%rbp), %r8
	testb	$1, %al
	jne	.L966
.L968:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -176(%rbp)
	movl	%edx, -168(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj
	movl	-168(%rbp), %edx
	movq	-176(%rbp), %r8
.L967:
	movabsq	$824633720832, %rcx
	movq	%r8, %rdi
	movq	%r13, -120(%rbp)
	movl	%r14d, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%edx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv
	movq	%r15, -112(%rbp)
	movdqa	-128(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movups	%xmm4, 16(%r12)
	movups	%xmm3, (%r12)
	movups	%xmm5, 32(%r12)
	movups	%xmm6, 48(%r12)
	movups	%xmm7, 64(%r12)
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L958:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L960
	movq	%rbx, %r14
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L948:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L950
	movq	%rbx, %rax
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L995:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r15
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L969:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L971
	movq	%rbx, %rdx
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L966:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L968
	movq	%rbx, %rax
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L994:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-168(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L973
.L977:
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	jmp	.L942
.L991:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18661:
	.size	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE, .-_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE
	.section	.text._ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS4_INS0_10JSReceiverEEENS1_13ConfigurationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS4_INS0_10JSReceiverEEENS1_13ConfigurationE
	.type	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS4_INS0_10JSReceiverEEENS1_13ConfigurationE, @function
_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS4_INS0_10JSReceiverEEENS1_13ConfigurationE:
.LFB18660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rcx, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$136, %rsp
	movq	(%rcx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -148(%rbp)
	testb	$1, %dl
	jne	.L997
	sarq	$32, %rdx
	js	.L1011
	movl	%edx, -148(%rbp)
	movl	%edx, %eax
.L999:
	movl	16(%rbp), %ecx
	movb	$1, (%r15)
	movq	%r12, %rdi
	movq	%r13, 24(%r12)
	movl	%ecx, (%r12)
	movabsq	$824633720832, %rcx
	movq	%rcx, 12(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	%rbx, 48(%r12)
	movq	$0, 56(%r12)
	movq	%r9, 64(%r12)
	movl	%eax, 72(%r12)
	movl	$-1, 76(%r12)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv
.L996:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1041
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L997:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	movq	%rcx, %r14
	cmpw	$65, 11(%rax)
	jne	.L1040
	movsd	7(%rdx), %xmm0
	movsd	.LC4(%rip), %xmm2
	addsd	%xmm0, %xmm2
	movq	%xmm2, %rax
	movq	%xmm2, %rcx
	shrq	$32, %rax
	cmpq	$1127219200, %rax
	jne	.L1040
	movl	%ecx, %edi
	pxor	%xmm1, %xmm1
	movd	%xmm2, %eax
	movd	%xmm2, -148(%rbp)
	cvtsi2sdq	%rdi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1027
	jne	.L1027
	cmpl	$-1, %ecx
	jne	.L999
.L1040:
	xorl	%eax, %eax
.L1001:
	testb	%al, %al
	jne	.L1011
	movq	-1(%rdx), %rax
	cmpw	$64, 11(%rax)
	jbe	.L1010
	.p2align 4,,10
	.p2align 3
.L1011:
	movq	%r13, %rdi
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal6Object13ConvertToNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-168(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L1042
.L1010:
	movb	$1, (%r15)
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1043
.L1019:
	movq	(%r9), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	subq	$37592, %rdi
	cmpw	$64, 11(%rdx)
	jne	.L1022
	testb	$1, 11(%rax)
	movl	$0, %eax
	cmove	16(%rbp), %eax
	movl	%eax, 16(%rbp)
.L1022:
	movl	16(%rbp), %eax
	movq	%rdi, 24(%r12)
	movl	%eax, (%r12)
	movabsq	$824633720832, %rax
	movq	%rax, 12(%r12)
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L1044
.L1023:
	movq	%r14, 32(%r12)
	movq	%r12, %rdi
	movq	$0, 40(%r12)
	movq	%rbx, 48(%r12)
	movq	$0, 56(%r12)
	movq	%r9, 64(%r12)
	movq	$-1, 72(%r12)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1042:
	movb	$0, (%r15)
	movq	(%rbx), %rax
	leaq	128(%r13), %r15
	testb	$1, %al
	jne	.L1014
.L1016:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj
	movq	%rax, %r14
.L1015:
	movq	128(%r13), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L1017
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L1017:
	movl	%eax, (%r12)
	movabsq	$824633720832, %rax
	movq	%rax, 12(%r12)
	movq	128(%r13), %rax
	movq	%r13, 24(%r12)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L1045
.L1018:
	movq	%r15, 32(%r12)
	movq	%r12, %rdi
	movq	$0, 40(%r12)
	movq	%rbx, 48(%r12)
	movq	$0, 56(%r12)
	movq	%r14, 64(%r12)
	movq	$-1, 72(%r12)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1043:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L1020
	testb	$2, %al
	jne	.L1019
.L1020:
	leaq	-144(%rbp), %r15
	leaq	-148(%rbp), %rsi
	movq	%r9, -168(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-168(%rbp), %r9
	testb	%al, %al
	je	.L1019
	movl	16(%rbp), %eax
	movq	%r15, %rdi
	movq	%r13, -120(%rbp)
	movq	$0, -112(%rbp)
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movl	-148(%rbp), %eax
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r9, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv
	movq	%r14, -112(%rbp)
	movdqa	-128(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movups	%xmm4, 16(%r12)
	movups	%xmm3, (%r12)
	movups	%xmm5, 32(%r12)
	movups	%xmm6, 48(%r12)
	movups	%xmm7, 64(%r12)
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1014:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1016
	movq	%rbx, %r14
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r15
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	%r14, %rsi
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-168(%rbp), %r9
	movq	%rax, %r14
	jmp	.L1023
.L1027:
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	jmp	.L1001
.L1041:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18660:
	.size	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS4_INS0_10JSReceiverEEENS1_13ConfigurationE, .-_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS4_INS0_10JSReceiverEEENS1_13ConfigurationE
	.section	.text._ZN2v88internal14LookupIterator4NextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LookupIterator4NextEv
	.type	_ZN2v88internal14LookupIterator4NextEv, @function
_ZN2v88internal14LookupIterator4NextEv:
.LFB18666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	56(%rdi), %rax
	movb	$0, 8(%rdi)
	movq	(%rax), %r14
	movq	-1(%r14), %r13
	cmpw	$1040, 11(%r13)
	ja	.L1053
	cmpl	$-1, 72(%rdi)
	movq	%r14, %rdx
	movq	%r13, %rsi
	je	.L1050
	call	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE
.L1051:
	movl	%eax, 4(%r12)
	cmpl	$4, %eax
	je	.L1053
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1053:
	.cfi_restore_state
	cmpl	$-1, 72(%r12)
	je	.L1056
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14LookupIterator12NextInternalILb1EEEvNS0_3MapENS0_10JSReceiverE
	.p2align 4,,10
	.p2align 3
.L1056:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14LookupIterator12NextInternalILb0EEEvNS0_3MapENS0_10JSReceiverE
	.p2align 4,,10
	.p2align 3
.L1050:
	.cfi_restore_state
	call	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	jmp	.L1051
	.cfi_endproc
.LFE18666:
	.size	_ZN2v88internal14LookupIterator4NextEv, .-_ZN2v88internal14LookupIterator4NextEv
	.section	.text._ZN2v88internal14LookupIterator23ReconfigureDataPropertyENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LookupIterator23ReconfigureDataPropertyENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesE
	.type	_ZN2v88internal14LookupIterator23ReconfigureDataPropertyENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesE, @function
_ZN2v88internal14LookupIterator23ReconfigureDataPropertyENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesE:
.LFB18679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$72, %rsp
	movq	56(%rdi), %r14
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L1057
	cmpl	$-1, 72(%rdi)
	movq	%rdi, %r12
	movq	%rsi, %r13
	je	.L1059
	movq	24(%rdi), %r15
	movq	15(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1060
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1061:
	movq	(%r14), %rax
	movq	%r14, %rsi
	movl	%ebx, %r9d
	movq	%r13, %r8
	movq	-1(%rax), %rax
	movzbl	14(%rax), %ecx
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rax
	shrl	$3, %ecx
	movq	(%rax,%rcx,8), %rdi
	movl	76(%r12), %ecx
	movq	(%rdi), %rax
	call	*280(%rax)
	movq	56(%r12), %rax
	movq	%r12, %rdi
	movl	$2, 4(%r12)
	movl	$0, 12(%r12)
	movq	(%rax), %rdx
	movq	-1(%rdx), %rsi
	cmpw	$1040, 11(%rsi)
	ja	.L1063
	call	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE
.L1071:
	movl	%eax, 4(%r12)
.L1065:
	cmpl	$-1, 72(%r12)
	je	.L1131
.L1075:
	addq	$72, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14LookupIterator14WriteDataValueENS0_6HandleINS0_6ObjectEEEb
	.p2align 4,,10
	.p2align 3
.L1060:
	.cfi_restore_state
	movq	41088(%r15), %rdx
	cmpq	%rdx, 41096(%r15)
	je	.L1132
.L1062:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdx)
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1063:
	call	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1057:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1131:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	je	.L1075
	movq	-1(%rax), %rdx
	leal	0(,%rbx,8), %r8d
	movl	15(%rdx), %edx
	andl	$1048576, %edx
	leaq	-1(%rax), %rdx
	je	.L1077
	testb	$8, 16(%r12)
	jne	.L1077
	andl	$1, %ebx
	je	.L1077
	movl	%r8d, -56(%rbp)
	movq	-1(%rax), %rdi
	call	_ZN2v88internal8JSObject25InvalidatePrototypeChainsENS0_3MapE@PLT
	movq	(%r14), %rax
	movl	-56(%rbp), %r8d
	leaq	-1(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L1077:
	movq	(%rdx), %rdx
	movq	24(%r12), %rbx
	cmpw	$1025, 11(%rdx)
	je	.L1133
	movq	7(%rax), %rsi
	testb	$1, %sil
	jne	.L1087
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %rsi
.L1087:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1088
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-56(%rbp), %r8d
	movq	(%rax), %rsi
.L1089:
	movl	76(%r12), %edx
	andl	$-2147483393, %r8d
	leal	(%rdx,%rdx,2), %edx
	leal	9(%rdx), %r15d
	leal	56(,%rdx,8), %r14d
	leal	0(,%r15,8), %ecx
	movslq	%r14d, %rdx
	addl	$8, %r14d
	movslq	%ecx, %rcx
	movslq	%r14d, %r14
	movq	-1(%rsi,%rcx), %rbx
	movq	(%rax), %rcx
	movq	32(%r12), %rax
	movq	%rcx, %r10
	sarq	$32, %rbx
	andq	$-262144, %r10
	andl	$2147483392, %ebx
	movq	(%rax), %r9
	leaq	-1(%rcx), %rax
	movq	8(%r10), %rsi
	orl	%r8d, %ebx
	leaq	(%r14,%rax), %r11
	movq	0(%r13), %r8
	orb	$-64, %bl
	addq	%rdx, %rax
	testl	$262144, %esi
	jne	.L1093
	andl	$24, %esi
	je	.L1093
	movq	%r9, (%rax)
	movq	%r8, (%r11)
.L1100:
	leal	(%rbx,%rbx), %eax
	sall	$3, %r15d
	sarl	%eax
	movslq	%r15d, %r15
	salq	$32, %rax
	movq	%rax, -1(%rcx,%r15)
	movl	%ebx, 16(%r12)
.L1085:
	movl	$6, 4(%r12)
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	jne	.L1065
	movq	24(%rdi), %r15
	movq	-1(%rax), %r8
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1066
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1067:
	movl	76(%r12), %edx
	movq	24(%r12), %rdi
	xorl	%r9d, %r9d
	movl	%ebx, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal3Map27ReconfigureExistingPropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_12PropertyKindENS0_18PropertyAttributesENS0_17PropertyConstnessE@PLT
	movq	%rax, %r9
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L1134
.L1069:
	movq	24(%r12), %rdi
	xorl	%ecx, %ecx
	movq	%r9, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi@PLT
	movq	56(%r12), %rax
	movq	%r12, %rdi
	movl	$2, 4(%r12)
	movl	$0, 12(%r12)
	movq	(%rax), %rdx
	movq	-1(%rdx), %rsi
	cmpw	$1040, 11(%rsi)
	ja	.L1070
	call	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1132:
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1066:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L1135
.L1068:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r8, (%rsi)
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1133:
	movq	41112(%rbx), %rdi
	movq	7(%rax), %r14
	testq	%rdi, %rdi
	je	.L1079
	movq	%r14, %rsi
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-56(%rbp), %r8d
	movq	%rax, %rsi
.L1080:
	movl	76(%r12), %edx
	movq	24(%r12), %rdi
	orb	$-64, %r8b
	movq	%r13, %rcx
	call	_ZN2v88internal12PropertyCell15PrepareForValueEPNS0_7IsolateENS0_6HandleINS0_16GlobalDictionaryEEEiNS4_INS0_6ObjectEEENS0_15PropertyDetailsE@PLT
	movq	0(%r13), %r14
	movq	(%rax), %r15
	movq	%rax, %rbx
	movq	%r14, 23(%r15)
	leaq	23(%r15), %rsi
	testb	$1, %r14b
	je	.L1099
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L1083
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
.L1083:
	testb	$24, %al
	je	.L1099
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1099
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	(%rbx), %rax
	movslq	19(%rax), %rax
	movl	%eax, 16(%r12)
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1070:
	call	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1134:
	movl	76(%r12), %edx
	movq	24(%r12), %rdi
	movq	%r9, %rsi
	movq	%r13, %r8
	xorl	%ecx, %ecx
	call	_ZN2v88internal3Map22PrepareForDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS4_INS0_6ObjectEEE@PLT
	movq	%rax, %r9
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1093:
	movq	%r9, (%rax)
	testb	$1, %r9b
	je	.L1104
	movq	%r9, %rax
	leaq	-1(%rcx,%rdx), %rsi
	andq	$-262144, %rax
	movq	8(%rax), %rdx
	movq	%rax, -56(%rbp)
	testl	$262144, %edx
	je	.L1094
	movq	%r9, %rdx
	movq	%rcx, %rdi
	movq	%r10, -104(%rbp)
	movq	%r11, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rax
	movq	-104(%rbp), %r10
	movq	-96(%rbp), %r11
	movq	-88(%rbp), %r8
	movq	8(%rax), %rdx
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rcx
.L1094:
	andl	$24, %edx
	je	.L1104
	testb	$24, 8(%r10)
	jne	.L1104
	movq	%rcx, %rdi
	movq	%r9, %rdx
	movq	%r10, -80(%rbp)
	movq	%r11, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r11
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L1104:
	movq	%r8, (%r11)
	testb	$1, %r8b
	je	.L1100
	movq	%r8, %r9
	leaq	-1(%rcx,%r14), %r14
	andq	$-262144, %r9
	movq	8(%r9), %rax
	movq	%r9, -56(%rbp)
	testl	$262144, %eax
	je	.L1097
	movq	%r8, %rdx
	movq	%rcx, %rdi
	movq	%r14, %rsi
	movq	%r10, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r9
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	8(%r9), %rax
.L1097:
	testb	$24, %al
	je	.L1100
	testb	$24, 8(%r10)
	jne	.L1100
	movq	%rcx, %rdi
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1088:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1136
.L1090:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1089
.L1079:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L1137
.L1081:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r14, (%rsi)
	jmp	.L1080
.L1135:
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1068
.L1136:
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movl	-56(%rbp), %r8d
	jmp	.L1090
.L1137:
	movq	%rbx, %rdi
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-56(%rbp), %r8d
	movq	%rax, %rsi
	jmp	.L1081
	.cfi_endproc
.LFE18679:
	.size	_ZN2v88internal14LookupIterator23ReconfigureDataPropertyENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesE, .-_ZN2v88internal14LookupIterator23ReconfigureDataPropertyENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesE
	.section	.rodata._ZN2v88internal14LookupIterator24TransitionToAccessorPairENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"TransitionToAccessorPair"
	.section	.text._ZN2v88internal14LookupIterator24TransitionToAccessorPairENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LookupIterator24TransitionToAccessorPairENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesE
	.type	_ZN2v88internal14LookupIterator24TransitionToAccessorPairENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesE, @function
_ZN2v88internal14LookupIterator24TransitionToAccessorPairENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesE:
.LFB18684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	48(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1196
.L1146:
	leal	0(,%rdx,8), %r12d
	movq	%r13, 56(%rbx)
	orb	$-63, %r12b
	cmpl	$-1, 72(%rbx)
	je	.L1147
	movq	24(%rbx), %rdi
	movl	$42, %esi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject17NormalizeElementsENS0_6HandleIS1_EE@PLT
	movl	72(%rbx), %edx
	movq	24(%rbx), %rdi
	movl	%r12d, %r9d
	movq	%r14, %rcx
	movq	%rax, %rsi
	movq	%r13, %r8
	call	_ZN2v88internal16NumberDictionary3SetEPNS0_7IsolateENS0_6HandleIS1_EEjNS4_INS0_6ObjectEEENS4_INS0_8JSObjectEEENS0_15PropertyDetailsE@PLT
	leaq	-64(%rbp), %rdi
	movq	%rax, %r14
	movq	0(%r13), %rax
	movq	%rax, -64(%rbp)
	movq	(%r14), %rsi
	call	_ZN2v88internal8JSObject19RequireSlowElementsENS0_16NumberDictionaryE@PLT
	movq	0(%r13), %r12
	movq	-1(%r12), %rax
	movzbl	14(%rax), %edx
	shrl	$3, %edx
	cmpl	$14, %edx
	jne	.L1148
	movq	15(%r12), %r12
	movl	76(%rbx), %edx
	movslq	11(%r12), %rax
	subl	$2, %eax
	cmpl	%eax, %edx
	jb	.L1197
.L1149:
	movq	(%r14), %r13
	leaq	23(%r12), %r15
	movq	%r13, 23(%r12)
	testb	$1, %r13b
	je	.L1156
.L1195:
	movq	%r13, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L1158
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
.L1158:
	testb	$24, %al
	je	.L1156
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1198
	.p2align 4,,10
	.p2align 3
.L1156:
	movq	56(%rbx), %rax
	movl	$2, 4(%rbx)
	movq	%rbx, %rdi
	movl	$0, 12(%rbx)
	movq	(%rax), %rdx
	movq	-1(%rdx), %rsi
	cmpw	$1040, 11(%rsi)
	ja	.L1160
	call	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE
.L1165:
	movl	%eax, 4(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1199
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1148:
	.cfi_restore_state
	movq	(%r14), %r13
	leaq	15(%r12), %r15
	movq	%r13, 15(%r12)
	testb	$1, %r13b
	jne	.L1195
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1147:
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$1048576, %edx
	jne	.L1200
.L1163:
	movq	24(%rbx), %rdi
	leaq	.LC5(%rip), %r8
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	call	_ZN2v88internal8JSObject19NormalizePropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeEiPKc@PLT
	movq	32(%rbx), %rsi
	movq	%r14, %rdx
	movl	%r12d, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject21SetNormalizedPropertyENS0_6HandleIS1_EENS2_INS0_4NameEEENS2_INS0_6ObjectEEENS0_15PropertyDetailsE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject21ReoptimizeIfPrototypeENS0_6HandleIS1_EE@PLT
	movq	56(%rbx), %rax
	movl	$2, 4(%rbx)
	movq	%rbx, %rdi
	movl	$0, 12(%rbx)
	movq	(%rax), %rdx
	movq	-1(%rdx), %rsi
	cmpw	$1040, 11(%rsi)
	ja	.L1164
	call	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1196:
	movq	-1(%rax), %rcx
	cmpw	$1026, 11(%rcx)
	jne	.L1146
	movq	-1(%rax), %rax
	movq	23(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$1025, 11(%rax)
	jne	.L1146
	movq	24(%rdi), %r12
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1143
	movl	%edx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-72(%rbp), %edx
	movq	%rax, %r13
	jmp	.L1146
	.p2align 4,,10
	.p2align 3
.L1160:
	call	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb1EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1164:
	call	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1198:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1200:
	movq	-1(%rax), %rdi
	call	_ZN2v88internal8JSObject25InvalidatePrototypeChainsENS0_3MapE@PLT
	movl	$1, %edx
	jmp	.L1163
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	24(%rbx), %rax
	movq	96(%rax), %r15
	leal	32(,%rdx,8), %eax
	cltq
	leaq	-1(%r12,%rax), %rsi
	movq	%r15, (%rsi)
	testb	$1, %r15b
	je	.L1167
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L1201
.L1151:
	testb	$24, %al
	je	.L1167
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1167
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1167:
	movq	0(%r13), %rax
	movq	15(%rax), %r12
	jmp	.L1149
	.p2align 4,,10
	.p2align 3
.L1201:
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1143:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L1202
.L1145:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L1146
.L1202:
	movq	%r12, %rdi
	movl	%edx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-80(%rbp), %edx
	movq	-72(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L1145
.L1199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18684:
	.size	_ZN2v88internal14LookupIterator24TransitionToAccessorPairENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesE, .-_ZN2v88internal14LookupIterator24TransitionToAccessorPairENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesE
	.section	.text._ZN2v88internal14LookupIterator29ApplyTransitionToDataPropertyENS0_6HandleINS0_10JSReceiverEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LookupIterator29ApplyTransitionToDataPropertyENS0_6HandleINS0_10JSReceiverEEE
	.type	_ZN2v88internal14LookupIterator29ApplyTransitionToDataPropertyENS0_6HandleINS0_10JSReceiverEEE, @function
_ZN2v88internal14LookupIterator29ApplyTransitionToDataPropertyENS0_6HandleINS0_10JSReceiverEEE:
.LFB18681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, 56(%rdi)
	movq	(%rsi), %rax
	movq	-1(%rax), %rdx
	cmpw	$1025, 11(%rdx)
	je	.L1250
	movq	40(%rdi), %r14
	movq	-1(%rax), %r15
	movq	%rsi, %r12
	movq	(%r14), %rdx
	movq	31(%rdx), %r13
	testb	$1, %r13b
	jne	.L1206
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	addq	$24, %rcx
.L1207:
	movq	(%rcx), %rcx
	cmpl	$3, (%rbx)
	movq	-37504(%rcx), %r13
	je	.L1251
.L1210:
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	je	.L1216
	movq	24(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi@PLT
.L1216:
	cmpq	%r13, %r15
	je	.L1252
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	je	.L1218
	movq	7(%rax), %r13
	movq	24(%rbx), %r14
	testb	$1, %r13b
	jne	.L1220
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %r13
.L1220:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1221
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1222:
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$1048576, %edx
	jne	.L1253
.L1225:
	movq	24(%rbx), %rdi
	movq	32(%rbx), %rdx
	leaq	-68(%rbp), %r9
	movl	16(%rbx), %r8d
	leaq	80(%rdi), %rcx
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE3AddEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_4NameEEENS7_INS0_6ObjectEEENS0_15PropertyDetailsEPi@PLT
	leaq	-64(%rbp), %rdi
	movq	%rax, %r13
	movq	(%r12), %rax
	movq	%rax, -64(%rbp)
	movq	0(%r13), %rsi
	call	_ZN2v88internal10JSReceiver13SetPropertiesENS0_10HeapObjectE@PLT
	movl	-68(%rbp), %eax
	movq	0(%r13), %rcx
	leal	9(%rax,%rax,2), %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	-1(%rdx,%rcx), %rdx
	movl	%eax, 76(%rbx)
	movb	$1, 8(%rbx)
	sarq	$32, %rdx
	movl	$6, 4(%rbx)
	movl	%edx, 16(%rbx)
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1206:
	movq	%rdx, %rsi
	andq	$-262144, %rsi
	movq	24(%rsi), %rdi
	leaq	24(%rsi), %rcx
	movq	-1(%r13), %rsi
	cmpq	%rsi, -37456(%rdi)
	jne	.L1207
	cmpl	$3, (%rbx)
	jne	.L1210
	.p2align 4,,10
	.p2align 3
.L1251:
	movl	15(%rdx), %ecx
	andl	$2097152, %ecx
	jne	.L1210
	movq	63(%rdx), %rdx
	testb	$1, %dl
	jne	.L1254
.L1212:
	testq	%rdx, %rdx
	je	.L1210
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal3Map37GetOrCreatePrototypeChainValidityCellENS0_6HandleIS1_EEPNS0_7IsolateE@PLT
	movq	(%r14), %rdi
	movq	(%rax), %rdx
	leaq	63(%rdi), %rsi
	movq	%rdx, 63(%rdi)
	testb	$1, %dl
	je	.L1248
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	je	.L1214
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
.L1214:
	testb	$24, %al
	je	.L1248
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1248
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1248:
	movq	(%r12), %rax
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1252:
	movq	(%r14), %rax
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	subl	$1, %eax
	movl	%eax, 76(%rbx)
	movq	(%r14), %rax
	movq	39(%rax), %rdx
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	7(%rdx,%rax), %rax
	movl	$6, 4(%rbx)
	sarq	$32, %rax
	movl	%eax, 16(%rbx)
.L1203:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1255
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1218:
	.cfi_restore_state
	movq	56(%rbx), %rax
	movl	$2, 4(%rbx)
	movq	%rbx, %rdi
	movl	$0, 12(%rbx)
	movq	(%rax), %rdx
	movq	-1(%rdx), %rsi
	cmpw	$1040, 11(%rsi)
	jbe	.L1256
	call	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
.L1228:
	movl	%eax, 4(%rbx)
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1250:
	movq	-1(%rax), %rdi
	call	_ZN2v88internal8JSObject25InvalidatePrototypeChainsENS0_3MapE@PLT
	movl	$6, 4(%rbx)
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1256:
	call	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	jmp	.L1228
	.p2align 4,,10
	.p2align 3
.L1221:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L1257
.L1223:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r13, (%rsi)
	jmp	.L1222
	.p2align 4,,10
	.p2align 3
.L1253:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1225
	movq	%rsi, -88(%rbp)
	movq	-1(%rax), %rdi
	call	_ZN2v88internal8JSObject25InvalidatePrototypeChainsENS0_3MapE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1254:
	movq	7(%rdx), %rdx
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1257:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1223
.L1255:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18681:
	.size	_ZN2v88internal14LookupIterator29ApplyTransitionToDataPropertyENS0_6HandleINS0_10JSReceiverEEE, .-_ZN2v88internal14LookupIterator29ApplyTransitionToDataPropertyENS0_6HandleINS0_10JSReceiverEEE
	.section	.rodata._ZN2v88internal14LookupIterator6DeleteEv.str1.1,"aMS",@progbits,1
.LC6:
	.string	"DeletingProperty"
	.section	.text._ZN2v88internal14LookupIterator6DeleteEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LookupIterator6DeleteEv
	.type	_ZN2v88internal14LookupIterator6DeleteEv, @function
_ZN2v88internal14LookupIterator6DeleteEv:
.LFB18682:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	56(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, 72(%rdi)
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	je	.L1259
	movzbl	14(%rax), %eax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movq	%r12, %rsi
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movl	76(%rbx), %edx
	movq	(%rdi), %rax
	call	*288(%rax)
.L1260:
	movl	$4, 4(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1277
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1259:
	.cfi_restore_state
	movl	15(%rax), %r13d
	pxor	%xmm0, %xmm0
	movq	$0, -48(%rbp)
	movq	24(%rdi), %rcx
	movaps	%xmm0, -80(%rbp)
	andl	$1048576, %r13d
	movaps	%xmm0, -64(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	cmpl	$1, %r13d
	sbbl	%edx, %edx
	andl	$-18, %edx
	addl	$194, %edx
	testl	%eax, %eax
	jne	.L1278
.L1262:
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L1279
.L1263:
	movl	76(%rbx), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver24DeleteNormalizedPropertyENS0_6HandleIS1_EEi@PLT
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	ja	.L1280
.L1266:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1260
	leaq	-72(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject21ReoptimizeIfPrototypeENS0_6HandleIS1_EE@PLT
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1279:
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	testl	%r13d, %r13d
	movq	%r12, %rsi
	setne	%dl
	leaq	.LC6(%rip), %r8
	xorl	%ecx, %ecx
	call	_ZN2v88internal8JSObject19NormalizePropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS0_25PropertyNormalizationModeEiPKc@PLT
	movq	56(%rbx), %rax
	movl	$2, 4(%rbx)
	movq	%rbx, %rdi
	movl	$0, 12(%rbx)
	movq	(%rax), %rdx
	movq	-1(%rdx), %rsi
	cmpw	$1040, 11(%rsi)
	ja	.L1264
	call	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
.L1265:
	movl	%eax, 4(%rbx)
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1264:
	call	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1278:
	movq	40960(%rcx), %rdi
	leaq	-72(%rbp), %rsi
	addq	$23240, %rdi
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1262
.L1277:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18682:
	.size	_ZN2v88internal14LookupIterator6DeleteEv, .-_ZN2v88internal14LookupIterator6DeleteEv
	.section	.text._ZN2v88internal14LookupIterator22PrepareForDataPropertyENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LookupIterator22PrepareForDataPropertyENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal14LookupIterator22PrepareForDataPropertyENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal14LookupIterator22PrepareForDataPropertyENS0_6HandleINS0_6ObjectEEE:
.LFB18678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	56(%rdi), %r13
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L1281
	cmpl	$-1, 72(%rdi)
	movq	%rdi, %rbx
	movq	%rsi, %r12
	je	.L1284
	movq	-1(%rax), %rax
	movzbl	14(%rax), %ebx
	movq	(%rsi), %rax
	shrl	$3, %ebx
	movl	%ebx, %edx
	andl	$1, %edx
	testb	$1, %al
	jne	.L1285
	cmpb	$5, %bl
	setbe	%r12b
	andl	%edx, %r12d
	movzbl	%r12b, %r14d
.L1286:
	movl	%r14d, %esi
	movl	%ebx, %edi
	call	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
	testb	%al, %al
	jne	.L1322
.L1288:
	cmpl	$3, %ebx
	jbe	.L1290
	leal	-8(%rbx), %eax
	cmpb	$1, %al
	ja	.L1323
.L1290:
	addq	$24, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8JSObject26EnsureWritableFastElementsENS0_6HandleIS1_EE@PLT
	.p2align 4,,10
	.p2align 3
.L1284:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1025, 11(%rdx)
	je	.L1324
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	jne	.L1281
	movl	$1, %r14d
	testb	$4, 16(%rdi)
	je	.L1298
	movq	(%rsi), %rsi
	call	_ZNK2v88internal14LookupIterator24IsConstFieldValueEqualToENS0_6ObjectE
	movzbl	%al, %r14d
	movq	0(%r13), %rax
.L1298:
	movq	24(%rbx), %rdx
	movq	-1(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1299
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L1300:
	movq	24(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal3Map6UpdateEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r9
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	jne	.L1302
	movl	76(%rbx), %edx
	movq	24(%rbx), %rdi
	movq	%r9, %rsi
	movq	%r12, %r8
	movl	%r14d, %ecx
	call	_ZN2v88internal3Map22PrepareForDataPropertyEPNS0_7IsolateENS0_6HandleIS1_EEiNS0_17PropertyConstnessENS4_INS0_6ObjectEEE@PLT
	movq	%rax, %r9
	cmpq	%rax, %r15
	je	.L1303
	testq	%r15, %r15
	je	.L1302
	testq	%rax, %rax
	je	.L1302
	movq	(%rax), %rax
	cmpq	%rax, (%r15)
	je	.L1303
.L1302:
	movq	24(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r9, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi@PLT
	movq	56(%rbx), %rax
	movl	$2, 4(%rbx)
	movq	%rbx, %rdi
	movl	$0, 12(%rbx)
	movq	(%rax), %rdx
	movq	-1(%rdx), %rsi
	cmpw	$1040, 11(%rsi)
	ja	.L1305
	call	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
.L1306:
	movl	%eax, 4(%rbx)
.L1281:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1299:
	.cfi_restore_state
	movq	41088(%rdx), %r15
	cmpq	41096(%rdx), %r15
	je	.L1325
.L1301:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r15)
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1323:
	subl	$6, %ebx
	cmpb	$1, %bl
	jbe	.L1290
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1322:
	cmpb	%bl, %r12b
	je	.L1288
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	cmpb	$3, %r12b
	jbe	.L1290
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1285:
	.cfi_restore_state
	cmpb	$5, %bl
	movq	-1(%rax), %rax
	setbe	%r12b
	andl	%edx, %r12d
	cmpb	$1, %r12b
	sbbl	%r14d, %r14d
	cmpw	$65, 11(%rax)
	je	.L1287
	addl	$3, %r14d
	cmpb	$1, %r12b
	sbbl	%r12d, %r12d
	addl	$3, %r12d
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1305:
	call	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1324:
	movq	24(%rdi), %r14
	movq	7(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1292
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L1293:
	movl	76(%rbx), %eax
	movq	24(%rbx), %r14
	leal	56(,%rax,8), %eax
	cltq
	movq	-1(%rsi,%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1295
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1296:
	movslq	19(%rsi), %r8
	movl	76(%rbx), %edx
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	24(%rbx), %rdi
	movl	%r8d, 16(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12PropertyCell15PrepareForValueEPNS0_7IsolateENS0_6HandleINS0_16GlobalDictionaryEEEiNS4_INS0_6ObjectEEENS0_15PropertyDetailsE@PLT
	.p2align 4,,10
	.p2align 3
.L1287:
	.cfi_restore_state
	addl	$5, %r14d
	cmpb	$1, %r12b
	sbbl	%r12d, %r12d
	addl	$5, %r12d
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1292:
	movq	41088(%r14), %r13
	cmpq	41096(%r14), %r13
	je	.L1326
.L1294:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, 0(%r13)
	jmp	.L1293
	.p2align 4,,10
	.p2align 3
.L1303:
	movl	16(%rbx), %edx
	movl	%edx, %eax
	shrl	$2, %eax
	andl	$1, %eax
	cmpl	%eax, %r14d
	jne	.L1304
	andl	$448, %edx
	jne	.L1281
.L1304:
	movl	76(%rbx), %eax
	movq	(%r9), %rdx
	leal	3(%rax,%rax,2), %eax
	movq	39(%rdx), %rdx
	sall	$3, %eax
	cltq
	movq	7(%rax,%rdx), %rax
	sarq	$32, %rax
	movl	%eax, 16(%rbx)
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1295:
	movq	41088(%r14), %rax
	cmpq	%rax, 41096(%r14)
	je	.L1327
.L1297:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1325:
	movq	%rdx, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L1301
.L1326:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L1294
.L1327:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1297
	.cfi_endproc
.LFE18678:
	.size	_ZN2v88internal14LookupIterator22PrepareForDataPropertyENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal14LookupIterator22PrepareForDataPropertyENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal14LookupIterator28TransitionToAccessorPropertyENS0_6HandleINS0_6ObjectEEES4_NS0_18PropertyAttributesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14LookupIterator28TransitionToAccessorPropertyENS0_6HandleINS0_6ObjectEEES4_NS0_18PropertyAttributesE
	.type	_ZN2v88internal14LookupIterator28TransitionToAccessorPropertyENS0_6HandleINS0_6ObjectEEES4_NS0_18PropertyAttributesE, @function
_ZN2v88internal14LookupIterator28TransitionToAccessorPropertyENS0_6HandleINS0_6ObjectEEES4_NS0_18PropertyAttributesE:
.LFB18683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	48(%rdi), %r10
	movq	(%r10), %rax
	testb	$1, %al
	jne	.L1441
.L1336:
	cmpl	$-1, 72(%r12)
	je	.L1442
.L1342:
	cmpl	$5, 4(%r12)
	je	.L1443
.L1358:
	movq	24(%r12), %rdi
	call	_ZN2v88internal7Factory15NewAccessorPairEv@PLT
.L1440:
	movq	(%rbx), %rbx
	movq	(%rax), %rdi
	movq	%rax, %r13
	movq	(%r15), %r15
	testb	$1, %bl
	jne	.L1444
	movq	%rbx, 7(%rdi)
.L1372:
	testb	$1, %r15b
	jne	.L1445
	movq	%r15, 15(%rdi)
.L1369:
	leaq	-40(%rbp), %rsp
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14LookupIterator24TransitionToAccessorPairENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesE
	.p2align 4,,10
	.p2align 3
.L1441:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1026, 11(%rdx)
	jne	.L1336
	movq	-1(%rax), %rax
	movq	23(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$1025, 11(%rax)
	jne	.L1336
	movq	24(%rdi), %r13
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1333
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
	jmp	.L1336
	.p2align 4,,10
	.p2align 3
.L1443:
	movq	%r12, %rdi
	movq	%r10, -56(%rbp)
	call	_ZNK2v88internal14LookupIterator10FetchValueEv
	movq	(%rax), %rax
	testb	$1, %al
	je	.L1358
	movq	-1(%rax), %rax
	cmpw	$79, 11(%rax)
	jne	.L1358
	movq	%r12, %rdi
	call	_ZNK2v88internal14LookupIterator10FetchValueEv
	movq	-56(%rbp), %r10
	movq	%rax, %rsi
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	7(%rax), %rcx
	cmpq	%rcx, (%rbx)
	je	.L1446
.L1360:
	movq	24(%r12), %rdi
	call	_ZN2v88internal12AccessorPair4CopyEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L1440
	.p2align 4,,10
	.p2align 3
.L1445:
	movq	%r15, %rbx
	andq	$-262144, %rbx
	movq	24(%rbx), %rax
	cmpq	-37488(%rax), %r15
	je	.L1369
	movq	%r15, 15(%rdi)
	leaq	15(%rdi), %rsi
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L1385
	movq	%r15, %rdx
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
.L1385:
	testb	$24, %al
	je	.L1369
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1369
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1369
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	%rbx, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rax
	cmpq	-37488(%rax), %rbx
	je	.L1372
	movq	%rbx, 7(%rdi)
	leaq	7(%rdi), %rsi
	movq	8(%rcx), %rax
	testl	$262144, %eax
	je	.L1380
	movq	%rbx, %rdx
	movq	%rcx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	movq	8(%rcx), %rax
.L1380:
	testb	$24, %al
	je	.L1372
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1372
	movq	%rbx, %rdx
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rdi
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1442:
	movq	32(%r12), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	je	.L1447
.L1341:
	movq	(%r10), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	jne	.L1342
	movq	24(%r12), %rdx
	movq	-1(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1344
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %r10
	movq	%rax, %r13
.L1345:
	movq	56(%r12), %rax
	cmpq	%r10, %rax
	je	.L1347
	testq	%rax, %rax
	je	.L1348
	movq	(%rax), %rcx
	cmpq	%rcx, (%r10)
	je	.L1347
.L1348:
	movq	%r10, 56(%r12)
	movl	$-1, %ecx
	movl	$4, 4(%r12)
.L1349:
	subq	$8, %rsp
	movq	32(%r12), %rdx
	movq	%r15, %r9
	movq	%r13, %rsi
	pushq	%r14
	movq	24(%r12), %rdi
	movq	%rbx, %r8
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal3Map28TransitionToAccessorPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEEiNS4_INS0_6ObjectEEES9_NS0_18PropertyAttributesE@PLT
	movq	-56(%rbp), %r10
	movq	%rax, %r13
	movq	(%r10), %rax
	movq	-1(%rax), %rax
	popq	%rdx
	popq	%rcx
	movq	%rax, -56(%rbp)
	movq	0(%r13), %rax
	movq	31(%rax), %r9
	andq	$-262144, %rax
	leaq	24(%rax), %rdx
	testb	$1, %r9b
	je	.L1352
	movq	24(%rax), %rcx
	movq	-1(%r9), %rax
	cmpq	%rax, -37456(%rcx)
	je	.L1353
.L1352:
	movq	(%rdx), %rax
	movq	-37504(%rax), %r9
.L1353:
	movq	24(%r12), %rdi
	xorl	%ecx, %ecx
	movq	%r10, %rsi
	movq	%r13, %rdx
	movq	%r9, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi@PLT
	movq	-72(%rbp), %r9
	cmpq	%r9, -56(%rbp)
	movq	-64(%rbp), %r10
	je	.L1448
	movq	56(%r12), %rax
	movq	%r12, %rdi
	movl	$2, 4(%r12)
	movl	$0, 12(%r12)
	movq	(%rax), %rdx
	movq	-1(%rdx), %rsi
	movq	%r10, -56(%rbp)
	cmpw	$1040, 11(%rsi)
	ja	.L1356
	call	_ZN2v88internal14LookupIterator21LookupInSpecialHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	movq	-56(%rbp), %r10
.L1357:
	movl	%eax, 4(%r12)
	movq	0(%r13), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	jne	.L1342
.L1328:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1447:
	.cfi_restore_state
	movl	%r14d, %edx
	orl	$2, %edx
	testb	$1, 11(%rax)
	cmovne	%edx, %r14d
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1344:
	movq	41088(%rdx), %r13
	cmpq	%r13, 41096(%rdx)
	je	.L1449
.L1346:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, 0(%r13)
	jmp	.L1345
	.p2align 4,,10
	.p2align 3
.L1347:
	movl	4(%r12), %edx
	cmpl	$2, %edx
	je	.L1450
.L1350:
	cmpl	$4, %edx
	je	.L1389
	movl	76(%r12), %ecx
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1356:
	call	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	movq	-56(%rbp), %r10
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1333:
	movq	41088(%r13), %r10
	cmpq	41096(%r13), %r10
	je	.L1451
.L1335:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r10)
	jmp	.L1336
	.p2align 4,,10
	.p2align 3
.L1448:
	movq	0(%r13), %rax
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	subl	$1, %eax
	movl	%eax, 76(%r12)
	movq	0(%r13), %rax
	movq	39(%rax), %rdx
	movl	15(%rax), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	leal	(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	7(%rdx,%rax), %rax
	movl	$5, 4(%r12)
	sarq	$32, %rax
	movl	%eax, 16(%r12)
	jmp	.L1328
	.p2align 4,,10
	.p2align 3
.L1446:
	movq	15(%rax), %rax
	cmpq	%rax, (%r15)
	jne	.L1360
	movl	16(%r12), %eax
	shrl	$3, %eax
	andl	$7, %eax
	cmpl	%eax, %r14d
	jne	.L1369
	cmpl	$-1, 72(%r12)
	jne	.L1328
	leaq	-40(%rbp), %rsp
	movq	%r10, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8JSObject21ReoptimizeIfPrototypeENS0_6HandleIS1_EE@PLT
	.p2align 4,,10
	.p2align 3
.L1450:
	.cfi_restore_state
	movq	(%rax), %rdx
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal14LookupIterator21LookupInRegularHolderILb0EEENS1_5StateENS0_3MapENS0_10JSReceiverE
	movl	4(%r12), %edx
	movq	-56(%rbp), %r10
	jmp	.L1350
.L1449:
	movq	%rdx, %rdi
	movq	%r10, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L1346
.L1389:
	movl	$-1, %ecx
	jmp	.L1349
.L1451:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L1335
	.cfi_endproc
.LFE18683:
	.size	_ZN2v88internal14LookupIterator28TransitionToAccessorPropertyENS0_6HandleINS0_6ObjectEEES4_NS0_18PropertyAttributesE, .-_ZN2v88internal14LookupIterator28TransitionToAccessorPropertyENS0_6HandleINS0_6ObjectEEES4_NS0_18PropertyAttributesE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS4_INS0_10JSReceiverEEENS1_13ConfigurationE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS4_INS0_10JSReceiverEEENS1_13ConfigurationE, @function
_GLOBAL__sub_I__ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS4_INS0_10JSReceiverEEENS1_13ConfigurationE:
.LFB22811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22811:
	.size	_GLOBAL__sub_I__ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS4_INS0_10JSReceiverEEENS1_13ConfigurationE, .-_GLOBAL__sub_I__ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS4_INS0_10JSReceiverEEENS1_13ConfigurationE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS4_INS0_10JSReceiverEEENS1_13ConfigurationE
	.section	.data._ZZN2v88internal12_GLOBAL__N_132IsTypedArrayFunctionInAnyContextEPNS0_7IsolateENS0_10HeapObjectEE13context_slots,"aw"
	.align 32
	.type	_ZZN2v88internal12_GLOBAL__N_132IsTypedArrayFunctionInAnyContextEPNS0_7IsolateENS0_10HeapObjectEE13context_slots, @object
	.size	_ZZN2v88internal12_GLOBAL__N_132IsTypedArrayFunctionInAnyContextEPNS0_7IsolateENS0_10HeapObjectEE13context_slots, 44
_ZZN2v88internal12_GLOBAL__N_132IsTypedArrayFunctionInAnyContextEPNS0_7IsolateENS0_10HeapObjectEE13context_slots:
	.long	193
	.long	74
	.long	191
	.long	72
	.long	192
	.long	73
	.long	48
	.long	49
	.long	194
	.long	29
	.long	28
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC4:
	.long	0
	.long	1127219200
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
